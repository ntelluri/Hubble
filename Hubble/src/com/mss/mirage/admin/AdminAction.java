/*
 * @(#)AdminAction.java	1.0  04/11/2007
 *
 * Copyright 2007 Miracle Software Systems, Inc. All rights reserved.
 * Miracle PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */


/*
 * AdminAction.java
 *
 * Created on November 4, 2007, 3:50 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.mss.mirage.admin;
import com.mss.mirage.util.ApplicationConstants;
import com.mss.mirage.util.DataSourceDataProvider;
import com.mss.mirage.util.HibernateDataProvider;
import com.mss.mirage.util.LdapServiceProvider;
import com.mss.mirage.util.DefaultDataProvider;
import com.mss.mirage.util.AuthorizationManager;
import com.mss.mirage.util.ConnectionProvider;
import com.mss.mirage.util.DateUtility;
import com.mss.mirage.util.ServiceLocator;
import com.mss.mirage.util.ServiceLocatorException;
import com.opensymphony.xwork2.ActionSupport;
import java.sql.Timestamp;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;
import org.json.JSONObject;

import com.mss.mirage.util.Properties;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.CallableStatement;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import org.apache.commons.io.FileUtils;
import org.apache.struts2.interceptor.ParameterAware;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;




/**
 * The <code>AdminAction</code>  class is used for getting Avilable Roles,Avilable Sceens,Avilable Employee details from
 * <i>UserList.jsp</i>
 * <p>
 * @author Arjuna Rao.Sanapathi<a href="mailto:asanapathi@miraclesoft.com">asanapathi@miraclesoft.com</a>
 * @version 1.122, 05/05/04
 * @since 1.0
 */
public class AdminAction extends ActionSupport implements ServletRequestAware,ParameterAware,ServletResponseAware{
    
    /**
     * The EmpId  is Useful to get the Employee Details
     */
    private int empId;
    
    
    /**
     * The resultType  is Useful to get the ResultType of an Action and depend on this
     * resultType the navigation of screens happens.
     */
    private String resultType;
    
    /**
     * The queryString   is Useful to  create the result and sends to the particular screen
     * Depends on this the GRID will show the result.
     */
    private StringBuffer queryStringBuffer;
    
    
    
    /**
     * The resultMessage  capture the resut and it is stored  as the REQUEST ATTRIBUTE.
     */
    private String resultMessage;
    
    
    /**
     *  The httpServletRequest
     * object representing the  HttpServletRequest
     */
    private HttpServletRequest httpServletRequest;
    
    
    /**
     *  The hibernateDataProvider
     * object provides  the data to Screens
     */
    private HibernateDataProvider hibernateDataProvider;
    
    /**
     *  The dataSourceDataProvider
     * object provides  the data to Screens
     */
    private DataSourceDataProvider dataSourceDataProvider;
    
    /**
     *  The defaultDataProvider
     * object provides  the data to Screens
     */
    private DefaultDataProvider defaultDataProvider;
    
    
    /** The id is useful to get EmployeeId  */
    private int id;
    
    /** The firstName is useful to get EmployeeFirstName  */
    private String firstName;
    
    /** The workPhoneNo is useful to get  the Employee  Working Phone  Number */
    private String workPhoneNo;
    
    
    /**
     * The currStatus is useful to get  Employee Current Status
     * @see com.mss.mirage.util.ApplicationConstants
     */
    private String currStatus = ApplicationConstants.DEFAULT_EMP_STATUS;
    
    /**
     * A List object with an currStatusList object of  read from a
     * full List of  avilable Status data.
     */
    private List currStatusList;
    
    
    /** The departmentId is useful to get   the departmentId */
    private String departmentId;
    
    
    /**
     * A List object with an departmentIdList object of  read from a
     * full List of  avilable  Department Ids  data.
     */
    private List departmentIdList;
    
    
    /**
     * The orgId is useful to get  Employee OrgId
     * @see com.mss.mirage.util.ApplicationConstants
     */
    private String orgId = ApplicationConstants.DEFAULT_ORG;
    
    /**
     * A List object with an orgIdList object of  read from a
     * full List of  avilable Organization Ids  data.
     */
    private List orgIdList;
    
    
    /**
     * A Map object with an assignedRolesMap object of  read from a
     * full List of  assigned Roles  data.
     */
    private Map assignedRolesMap;
    
    /**
     * A Map object with an notAssignedRolesMap object of  read from a
     * full List of  notassigned Roles  data.
     */
    private Map notAssignedRolesMap;
    
    
    
    /**
     *  The adminrole
     * object is useful to data persistance
     * @see com.mss.mirage.admin.AdminRolesVTO
     */
    private AdminRolesVTO adminRoleVTO;
    
    /**
     * A Map object with an primaryRolesList object of  read from a
     * full List of  primary Roles  data.
     */
    private Map primaryRolesList;
    
    /** The userName is useful to get  Employee userName */
    private String userName;
    
    /** The loginId is useful to get  Employee loginId */
    private String loginId;
    
    /**
     *  The adminService
     * object is  to get the AdminServiceses.
     */
    private  AdminService adminService;
    
    /**
     * A Map object with an parameters object of  read from a
     * full List of    Submit Form  parameters .
     */
    private Map parameters;
    
    /**
     * A List object with an addedRolesList object of  read from a
     * full List of  avilable  addedRoles Ids  data.
     */
    private  List addedRolesList;
    
    /**
     * empRoleId is useful to hold the EmployeeRoleId
     */
    private int empRoleId;
    
    /**
     * primaryRole is useful to hold the Employee PrimaryRole
     */
    private int primaryRole;
    
    /**
     * RoleName is useful to hold the Employee RoleName
     */
    private String RoleName;
    
    /**
     * roleId is useful to hold the Employee Role
     */
    private int roleId;
    
    /**
     * A Map object with an assignedAllScreensMap object of  read from a
     * full List of  assignedAllScreens  data.
     */
    private Map assignedAllScreensMap;
    
    /**
     * A Map object with an assignedScreensMap object of  read from a
     * full List of  assignedScreens  data.
     */
    private Map assignedScreensMap;
    
    /**
     * A Map object with an roleScreenParameters object of  read from a
     * full List of  avilable  Form Submited roleScreenParameters  data.
     */
    private Map roleScreenParameters;
    
    /**
     * A Map object with an assingScreensToRoleMapParameters object of  read from a
     * full List of  avilable  Form Submited assingScreensToRoleMapParameters  data.
     */
    private Map assingScreensToRoleMapParameters;
    
    /**
     * A Map object with an assingScreensToRoleMapParameters object of  read from a
     * full List of  avilable  Form Submited rightSideRoleScreens  data.
     */
    private List rightSideRoleScreens;
    
    /**
     * A Map object with an moduleMap object of  read from a
     * full List of  avilable  Form Submited moduleMap data.
     */
    private Map moduleMap;
    
    
    //  The Below varibles are Assign Screen varibles
    
    
    /**
     * moduleId is useful to hold the EmployeeRole Module
     */
    private String moduleId;
    
    /**
     * screenName is useful to hold the Employee Role screenNames
     */
    private String screenName;
    
    /**
     * screenAction is useful to hold the Employee Role screenAction
     */
    private String screenAction;
    
    /**
     * screenTitle is useful to hold the Employee Role screenTitle
     */
    private String screenTitle;
    
    /**
     * submitFrom is useful to hold the all values of variables
     */
    private String submitFrom;
    
    /**
     * queryString is useful to define a query to retrive data from database
     */
    private String queryString;
    
    /**
     * userRoleId is useful to hold the Employee Role
     */
    private int userRoleId;
    
    /**
     * newPassword is useful to hold the Employee newPassword
     */
    private String newPassword;
    
    /**
     * cnfPassword is useful to hold the Employee confirmPassword
     */
    private String cnfPassword;
    
    private String workingCountry;
    
    private String userRoleName;
    
    private int roleStatus;
    
    
    private Map<String, String> roleStatusMap;
    private List opportunityStateList;
    
    private String isAdminFlag;
    
    
    private String urlImages;
    private String urlNewsletters;
    private Map bdmMap=new HashMap();
    
    
     private String customerName;
    private Map teamMemberList;
    private String  startDateContacts;
    private String  endDateContacts;
     private String bdmId;
    private String bdmName;
    private Map salesMap;
    private Map bdmTeamAssociateMap;
    private  List assignedBdmTeamMembers;
    private  List availableSalesTeamMembers;
     private Map assignedBdmTeam;
    private Map bdmParameters;
    private String status;
    private String preAssignSalesId;
     private String responseString;

    
/*  LookUp Mngnmt */
     
     private String tableName;
     private String tableId;
     
     /* Release Notes*/
     private String title;
     private String fromDate;
     private String toDate;
     private String currentAction;                     
     private Date date;
     private String subTitle0;
     private String purpose0;
     private File file0;
     private String subTitle1;
     private String purpose1;
     private File file1;
     private String subTitle2;
     private String purpose2;
     private File file2;
     private String subTitle3;
     private String purpose3;
     private File file3;
     private String subTitle4;
     private String purpose4;
     private File file4;
     private String subTitle5;
     private String purpose5;
     private File file5;
     /**
      * The content type of the file
      */
     private String fileContentType;
     /**
      * The uploaded file name
      */
     private String file0FileName;
     private String file1FileName;
     private String file2FileName;
     private String file3FileName;
     private String file4FileName;
     private String file5FileName;
     private String filepath;
     private int isInsertedValueCount;
     private int releaseId;
     private int count;    
     private String subId0;
     private String subId1;
     private String subId2;
     private String subId3;
     private String subId4;
     private String subId5;
     private int isUpdatedValueCount;
     private String minusHiden;
     private String titleSearch;                      
     private String file0FilePath;        
     private String file1FilePath;        
     private String file2FilePath;        
     private String file3FilePath;        
     private String file4FilePath;        
     private String file5FilePath;                          
     private String filex1FilePath;        
     private String filex2FilePath;        
     private String filex3FilePath;        
     private String filex4FilePath;        
     private String filex5FilePath;          
     private HttpServletResponse httpServletResponse;
     private InputStream inputStream;
     private OutputStream outputStream;
     private String subId;
     
     private List roleDetails;
     private String roleAccess;
     private List roleDetailsList;
     
     private Map rolesMap;
     
     private String reqStartDate;
     private String reqEndDate;
     private Map bdmMapReq=new HashMap();
    public void setRoleStatusMap(Map<String, String> roleStatusMap) {
        this.roleStatusMap = roleStatusMap;
    }
    
    
    public Map<String, String> getRoleStatusMap() {
        return roleStatusMap;
    }
    /**
     * Populates user required options to the Screens depending on the  options.
     * @return The  Result type is returned after complete the code of prepare Method.
     * @see com.mss.mirage.ApplicationConstants
     * @throws java.lang.Exception
     */
    public String prepare()throws Exception{
        
        resultType = LOGIN;
        /*
         *This if loop is to check whether there is Session or not
         **/
        if(httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null){
            
            hibernateDataProvider = HibernateDataProvider.getInstance();
            dataSourceDataProvider = DataSourceDataProvider.getInstance();
            
            /**
             *   Populates user required options to the Screens depending on the  options.
             *   @see com.mss.mirage.ApplicationConstants
             *   @param   Taking   getRoleId() method
             *   @return   The AssignedAllScreensMap  returned  depends on the RoleId
             *
             *
             */
            setAssignedAllScreensMap(dataSourceDataProvider.getAssignedAllScreens(getRoleId()));
            
            /**
             *   Populates user required options to the Screens depending on the  options.
             *   @see com.mss.mirage.ApplicationConstants
             *   @param   Taking   getRoleId() method
             *   @return   The AssignedScreensMap  returned  depends on the RoleId
             *
             *
             */
            setAssignedScreensMap(dataSourceDataProvider.getAssignedScreens(getRoleId()));
            
            
            /**
             *   Populates user required options to the Screens depending on the  options.
             *   @see com.mss.mirage.ApplicationConstants
             *   @param   Taking MODULE_OPTIONS key from  ApplicationConstants
             *   @return   The ModuleMap  returned  depends on the MODULE_OPTIONS
             *
             *
             */
            setModuleMap(hibernateDataProvider.getModuleNames(ApplicationConstants.MODULE_OPTIONS));
            resultType = SUCCESS;
            
        }//Close Session Checking
        
        return resultType;
    }
    
    /**
     * Populates user required options to the Screens depending on the  options.
     * @return The Result Type  returned  depends on the MODULE_OPTIONS
     * @see com.mss.mirage.ApplicationConstants
     * @throws java.lang.Exception
     */
    
    public String searchPrepare() throws Exception{
        
        resultType = LOGIN;
        /*
         *This if loop is to check whether there is Session or not
         **/
        if(httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null){
            defaultDataProvider = DefaultDataProvider.getInstance();
            hibernateDataProvider = hibernateDataProvider.getInstance();
            
            /**
             *  Populates user required options to the Screens depending on the  options.
             *   @see com.mss.mirage.ApplicationConstants
             *    @param   Taking CURRENT_STATUS_OPTIONS  key from  ApplicationConstants
             *   @return   The CurrentStatusList  returned  depends on the  CURRENT_STATUS_OPTIONS
             *
             *
             */
            setCurrStatusList(defaultDataProvider.getCurrentStatus(ApplicationConstants.CURRENT_STATUS_OPTIONS));
           // setRoleStatusMap(DefaultDataProvider.getInstance().getRoleStatusMap());
            setRoleStatusMap(DataSourceDataProvider.getInstance().getRoleStatusMap());
            /**
             *   Populates user required options to the Screens depending on the  options.
             *   @see com.mss.mirage.ApplicationConstants
             *   @param   Taking LKORGANIZATION_OPTION  key from  ApplicationConstants
             *   @return   The OrgIdList  returned  depends on the  LKORGANIZATION_OPTION
             *
             *
             */
            setOrgIdList(hibernateDataProvider.getLkOrganization(ApplicationConstants.LKORGANIZATION_OPTION));
            
            /**
             *   Populates user required options to the Screens depending on the  options.
             *   @see com.mss.mirage.ApplicationConstants
             *   @param   Taking DEPARTMENT_OPTION key from  ApplicationConstants
             *   @return   The DepartmentIdList  returned  depends on the DEPARTMENT_OPTION
             *
             *
             */
            setDepartmentIdList(hibernateDataProvider.getDepartment(ApplicationConstants.DEPARTMENT_OPTION));
            resultType = SUCCESS;
        }//Close Session Checking
        
        return resultType;
    }
    
    
    /**
     * Populates user required options to the Screens depending on the  options.
     * @return The Result Type  returned  depends on the MODULE_OPTIONS
     * @see com.mss.mirage.ApplicationConstants
     */
    public String getUsersSearch(){
        
        
        resultType = LOGIN;
        /*
         *This if loop is to check whether there is Session or not
         **/
        if(httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null){
            userRoleId = Integer.parseInt(httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_ROLE_ID).toString());
            String workingCountry = httpServletRequest.getSession(false).getAttribute(ApplicationConstants.WORKING_COUNTRY).toString();
            resultType = "accessFailed";
            try{
                if(AuthorizationManager.getInstance().isAuthorizedUser("GET_USER_SEARCH",userRoleId)){
                    
                    
                    if(getSubmitFrom()==null){
                        queryString = "SELECT Id, DepartmentId, LName, FName, MName, Email1, WorkPhoneNo, AlterPhoneNo, CellPhoneNo, CurStatus ";
                       // queryString = queryString+" FROM tblEmployee WHERE DeletedFlag != 1  and Country like '"+workingCountry+"'AND CurStatus='Active' ORDER BY LName,FName";
                         queryString = queryString+" FROM tblEmployee WHERE DeletedFlag != 1  AND CurStatus='Active' ORDER BY LName,FName";
                        setCurrStatus("Active");
                        setSubmitFrom("searchFormAll");
                        //setRoleStatus("Employee");
                        httpServletRequest.getSession(false).setAttribute(ApplicationConstants.QS_USER_LIST_OPS,queryString);
                    }
                    searchPrepare();
                    resultType = SUCCESS;
                    
                }//END-Authorization Checking
            }catch (Exception ex){
                //List errorMsgList = ExceptionToListUtility.errorMessages(ex);
                httpServletRequest.getSession(false).setAttribute("errorMessage",ex.toString());
                resultType =  ERROR;
            }
        }//Close Session Checking
        return resultType;
    }
    
    /**
     *   Populates user required options to the Grid depends on the serach queryString.
     * @return The Result Type.
     * @see com.mss.mirage.ApplicationConstants
     */
    public String getSearchQuery(){
        
        
        resultType = LOGIN;
        String tempName="";
        String tempCurStatus="";
        String tempDept="";
        String tempOrg="";
        int columnCounter = 0;
        /*
         *This if loop is to check whether there is Session or not
         **/
        if(httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null){
            
            userRoleId = Integer.parseInt(httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_ROLE_ID).toString());
            resultType = "accessFailed";
            workingCountry = httpServletRequest.getSession(false).getAttribute(ApplicationConstants.WORKING_COUNTRY).toString();
            try{
                /*
                 *This if loop is to check the Authentication
                 **/
                
                
                
                if(AuthorizationManager.getInstance().isAuthorizedUser("GET_SEARCH_QUERY_ADMIN",userRoleId)){
                    
                    if("searchFormAll".equalsIgnoreCase(getSubmitFrom())){
                        queryStringBuffer = new StringBuffer();
                        
                        queryStringBuffer.append("SELECT tblEmployee.Id, DepartmentId, LName, FName, MName, Email1, WorkPhoneNo, AlterPhoneNo, CellPhoneNo, CurStatus ");
                        
                        
                        if(getRoleStatus()==0){
                            
                            queryStringBuffer.append(" FROM tblEmployee where ");
                        } else{
                            queryStringBuffer.append(" FROM tblEmpRoles,tblLKRoles,tblEmployee WHERE" +
                                    " tblEmployee.Id=tblEmpRoles.EmpId AND tblLKRoles.Id=tblEmpRoles.RoleId AND RoleId="+roleStatus+" ");
                            columnCounter++;
                        }
                        if(null == getFirstName()) setFirstName("");
                        if(null == getWorkPhoneNo()) setWorkPhoneNo("");
                        if(null == getCurrStatus()) setCurrStatus("");
                        //if(null == getRoleStatus()) setRoleStatus("");
                        if("All".equalsIgnoreCase(getCurrStatus())) setCurrStatus("All");
                        if(null == getOrgId()) setOrgId("");
                        if("All".equalsIgnoreCase(getOrgId())) setOrgId("");
                        if(null== getDepartmentId()) setDepartmentId("");
                        if("All".equalsIgnoreCase(getDepartmentId())) setDepartmentId("");
                        
//                if((!"".equals(getFirstName()))
//                || (!"".equals(getLastName()))
//                || (!"".equals(getWorkPhoneNo()))
//                || (!"".equals(getCurrStatus()))
//                ||(!"".equals(getOrgId()))){
//                    queryStringBuffer.append(" WHERE ");
//                }
                        
                        // System.out.println("userRoleName=-"+userRoleName);
                      /*  if("Admin".equals(userRoleName)) {
                            queryStringBuffer.append(" WHERE  ");
                        } else {
                            queryStringBuffer.append("  AND  Country like '"+workingCountry+"' and  ");
                        }*/
                        
                        
                        if(!"".equals(getFirstName()) && columnCounter==0){
                            tempName=getFirstName();
                            queryStringBuffer.append("(FName LIKE '%" + getFirstName() + "%' OR LName LIKE '%" + getFirstName() + "%') ");
                            columnCounter ++;
                        }else if(!"".equals(getFirstName()) && columnCounter!=0){
                            tempName=getFirstName();
                            queryStringBuffer.append("AND (FName LIKE '%" + getFirstName() + "%' OR LName LIKE '%" + getFirstName() + "%') ");
                        }
                        
                        if(!"".equals(getWorkPhoneNo()) && columnCounter==0){
                            queryStringBuffer.append("WorkPhoneNo LIKE '%" + getWorkPhoneNo() + "%' ");
                            columnCounter ++;
                        }else if(!"".equals(getWorkPhoneNo()) && columnCounter!=0){
                            queryStringBuffer.append("AND WorkPhoneNo LIKE '%" + getWorkPhoneNo() + "%' ");
                        }
                        
                        if(!"".equals(getCurrStatus()) && !"All".equals(getCurrStatus()) && columnCounter==0){
                            queryStringBuffer.append("CurStatus ='" + getCurrStatus() + "' ");
                            columnCounter ++;
                        }else if(!"".equals(getCurrStatus()) && !"All".equals(getCurrStatus()) && columnCounter!=0){
                            queryStringBuffer.append("AND CurStatus ='" + getCurrStatus() + "' ");
                        }
                        
                        if(!"".equals(getOrgId()) && columnCounter==0){
                            tempOrg=getOrgId();
                            queryStringBuffer.append("OrgId='" + getOrgId() + "' ");
                            columnCounter ++;
                        }else if(!"".equals(getOrgId()) && columnCounter!=0){
                            tempOrg=getOrgId();
                            queryStringBuffer.append("AND OrgId='" + getOrgId() + "' ");
                        }
                        
                        if(!"".equals(getDepartmentId()) && columnCounter==0){
                            tempDept=getDepartmentId();
                            queryStringBuffer.append("DepartmentId='" + getDepartmentId() + "' ");
                            columnCounter ++;
                        }else if(!"".equals(getDepartmentId()) && columnCounter!=0){
                            tempDept=getDepartmentId();
                            queryStringBuffer.append("AND DepartmentId='" + getDepartmentId() + "' ");
                        }
                        
                        if(columnCounter == 0){
                            queryStringBuffer.append(" DeletedFlag != 1 ORDER BY LName,FName");
                        }else if(columnCounter != 0){
                            queryStringBuffer.append(" AND DeletedFlag != 1 ORDER BY LName,FName");
                        }
                        
                        
                        httpServletRequest.getSession(false).removeAttribute(ApplicationConstants.QS_USER_LIST_OPS);
                        httpServletRequest.getSession(false).setAttribute(ApplicationConstants.QS_USER_LIST_OPS,queryStringBuffer.toString());
                        
                        
                        // setCurrStatus("Active");
                        queryStringBuffer.delete(0,queryStringBuffer.capacity());
                    }
                    if("dbGrid".equalsIgnoreCase(getSubmitFrom())){
                        
                        
                        queryStringBuffer = new StringBuffer();
                        
                        queryStringBuffer.append("SELECT tblEmployee.Id, DepartmentId, LName, FName, MName, Email1, WorkPhoneNo, AlterPhoneNo, CellPhoneNo, CurStatus");
                        
                        
                        if(getRoleStatus()==0){
                            queryStringBuffer.append(" FROM tblEmployee where ");
                        } else{
                            queryStringBuffer.append(" FROM tblEmpRoles,tblLKRoles,tblEmployee WHERE" +
                                    " tblEmployee.Id=tblEmpRoles.EmpId AND tblLKRoles.Id=tblEmpRoles.RoleId AND RoleId="+roleStatus+" ");
                            columnCounter++;
                        }
                        
                        if(null == getFirstName()) setFirstName("");
                        if(null == getWorkPhoneNo()) setWorkPhoneNo("");
                        if(null == getCurrStatus()) setCurrStatus("");
                        if("All".equalsIgnoreCase(getCurrStatus())) setCurrStatus("All");
                        if(null == getOrgId()) setOrgId("");
                        if("All".equalsIgnoreCase(getOrgId())) setOrgId("");
                        if(null== getDepartmentId()) setDepartmentId("");
                        if("All".equalsIgnoreCase(getDepartmentId())) setDepartmentId("");
                        
//                if((!"".equals(getFirstName()))
//                || (!"".equals(getLastName()))
//                || (!"".equals(getWorkPhoneNo()))
//                || (!"".equals(getCurrStatus()))
//                ||(!"".equals(getOrgId()))){
//                    queryStringBuffer.append(" WHERE ");
//                }
      /*                  if("Admin".equals(userRoleName)) {
                            queryStringBuffer.append(" WHERE  ");
                        } else {
                            queryStringBuffer.append(" WHERE Country like '"+workingCountry+"' and  ");
                        }*/
                        //int columnCounter = 0;
                        
                        if(!"".equals(getFirstName()) && columnCounter==0){
                            tempName=getFirstName();
                            queryStringBuffer.append("(FName LIKE '%" + getFirstName() + "%' OR LName LIKE '%" + getFirstName() + "%') ");
                            columnCounter ++;
                        }else if(!"".equals(getFirstName()) && columnCounter!=0){
                            tempName=getFirstName();
                            queryStringBuffer.append("AND (FName LIKE '%" + getFirstName() + "%' OR LName LIKE '%" + getFirstName() + "%') ");
                        }
                        
                        if(!"".equals(getWorkPhoneNo()) && columnCounter==0){
                            queryStringBuffer.append("WorkPhoneNo LIKE '%" + getWorkPhoneNo() + "%' ");
                            columnCounter ++;
                        }else if(!"".equals(getWorkPhoneNo()) && columnCounter!=0){
                            queryStringBuffer.append("AND WorkPhoneNo LIKE '%" + getWorkPhoneNo() + "%' ");
                        }
                        
                        if(!getCurrStatus().equals("All"))
                        {
                        if(!"".equals(getCurrStatus()) && columnCounter==0){
                            queryStringBuffer.append("CurStatus ='" + getCurrStatus() + "' ");
                            columnCounter ++;
                        }else if(!"".equals(getCurrStatus()) && columnCounter!=0){
                            queryStringBuffer.append("AND CurStatus ='" + getCurrStatus() + "' ");
                        }
                        }
                        if(!"".equals(getOrgId()) && columnCounter==0){
                            tempOrg=getOrgId();
                            queryStringBuffer.append("OrgId='" + getOrgId() + "' ");
                            columnCounter ++;
                        }else if(!"".equals(getOrgId()) && columnCounter!=0){
                            tempOrg=getOrgId();
                            queryStringBuffer.append("AND OrgId='" + getOrgId() + "' ");
                        }
                        
                        if(!"".equals(getDepartmentId()) && columnCounter==0){
                            tempDept=getDepartmentId();
                            queryStringBuffer.append("DepartmentId='" + getDepartmentId() + "' ");
                            columnCounter ++;
                        }else if(!"".equals(getDepartmentId()) && columnCounter!=0){
                            tempDept=getDepartmentId();
                            queryStringBuffer.append("AND DepartmentId='" + getDepartmentId() + "' ");
                        }
                        
                        if(columnCounter == 0){
                            queryStringBuffer.append(" DeletedFlag != 1 ORDER BY LName,FName");
                        }else if(columnCounter != 0){
                            queryStringBuffer.append(" AND DeletedFlag != 1 ORDER BY LName,FName");
                        }
                        
                        //   System.out.println("queryStringBuffer.toString()"+queryStringBuffer.toString());
                        
                        
                        httpServletRequest.getSession(false).removeAttribute(ApplicationConstants.QS_USER_LIST_OPS);
                        httpServletRequest.getSession(false).setAttribute(ApplicationConstants.QS_USER_LIST_OPS,queryStringBuffer.toString());
                        
                        // setCurrStatus("Active");
                        queryStringBuffer.delete(0,queryStringBuffer.capacity());
                    }
                    //Calling searchPrepare() method to populate select components
                    setFirstName(tempName);
                    // System.out.print("Before"+getCurrStatus());
                    setCurrStatus(getCurrStatus());
                    //  System.out.print("End"+getCurrStatus());
                    setOrgId(tempOrg);
                    setDepartmentId(tempDept);
                    searchPrepare();
                    resultType = SUCCESS;
                    
                    
                }//END-Authorization Checking
            }catch (Exception ex){
                //List errorMsgList = ExceptionToListUtility.errorMessages(ex);
                httpServletRequest.getSession(false).setAttribute("errorMessage",ex.toString());
                resultType =  ERROR;
            }
        }//Close Session Checking
        return resultType;
    }
    
    
    /**
     * This method is useful to get Assigned Roles of a user
     * @return The Result Type of SUCCESS.
     * @see com.mss.mirage.ApplicationConstants
     */
    
    public String getAssingnedRoles(){
        
        resultType = LOGIN;
        /*
         *This if loop is to check whether there is Session or not
         **/
        if(httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null){
            
            
            userRoleId = Integer.parseInt(httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_ROLE_ID).toString());
            resultType = "accessFailed";
            try{
                /*
                 *This if loop is to check the Authentication
                 **/
                if(AuthorizationManager.getInstance().isAuthorizedUser("GET_ASSIGNED_ROLES",userRoleId)){
                    
                    hibernateDataProvider = HibernateDataProvider.getInstance();
                    dataSourceDataProvider = DataSourceDataProvider.getInstance();
                   
                    /**
                     *   Populates user required options to the Screens depending on the  options.
                     *   @see com.mss.mirage.ApplicationConstants
                     *   @param   Taking  getEmpId() method  from  ApplicationConstants
                     *   @return   The Adminrole returned  depends on the getEmpId() method
                     *
                     *
                     */
                    setAdminRoleVTO(ServiceLocator.getAdminService().employeeDetails(this.getEmpId()));
                    
                    /**
                     *   Populates user required options to the Screens depending on the  options.
                     *   @see com.mss.mirage.ApplicationConstants
                     *   @param   Taking   getEmpId() method
                     *   @return   The AssignedRolesMap  returned  depends on the EmpId
                     *
                     *
                     */
                    setAssignedRolesMap(dataSourceDataProvider.getAssignedRoles(getEmpId()));
                    
                    /**
                     *   Populates user required options to the Screens depending on the  options.
                     *   @see com.mss.mirage.ApplicationConstants
                     *   @param   Taking   getEmpId() method
                     *   @return   The NotAssignedRolesMap  returned  depends on the EmpId
                     *
                     *
                     */
                    setNotAssignedRolesMap(dataSourceDataProvider.getNotAssignedRoles(getEmpId()));
                     Map RolesMap=(Map)httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_MY_ROLES);
                    if(!RolesMap.containsKey("1")){
                        getNotAssignedRolesMap().remove("1");
                         getNotAssignedRolesMap().remove("13");
                    }
                    if(RolesMap.containsValue("Admin"))
                setIsAdminFlag("YES");
                        else
                setIsAdminFlag("NO");
                    
                    /**
                     *   Populates user required options to the Screens depending on the  options.
                     *   @see com.mss.mirage.ApplicationConstants
                     *   @param   Taking ROLES_OPTIONS key from  ApplicationConstants
                     *   @return   The PrimaryRolesList  returned  depends on the ROLES_OPTIONS
                     *
                     *
                     */
                    setPrimaryRolesList(hibernateDataProvider.getRoles(ApplicationConstants.ROLES_OPTIONS));
                    resultType = SUCCESS;
                    
                }//END-Authorization Checking
            }catch (Exception ex){
                //List errorMsgList = ExceptionToListUtility.errorMessages(ex);
                httpServletRequest.getSession(false).setAttribute("errorMessage",ex.toString());
                ex.printStackTrace();
                resultType =  ERROR;
            }
        }//Close Session Checking
        
        return resultType;
    }
    
    /**
     * This method is useful to get Assigned Role Screen of a user
     * @return The Result Type of SUCCESS.
     * @see com.mss.mirage.ApplicationConstants
     * @throws java.lang.Exception
     */
    
    public String getAssingnRoleScreen() throws Exception{
        
        resultType = LOGIN;
        /*
         *This if loop is to check whether there is Session or not
         **/
        if(httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null){
            
            /**
             *   Populates user required options to the Screens depending on the  options.
             *   @see com.mss.mirage.ApplicationConstants
             *   @param   Taking  getRoleId() method  from  ApplicationConstants
             *   @return   The Adminrole returned  depends on the getRoleId() method
             *
             *
             */
            setAdminRoleVTO(ServiceLocator.getAdminService().getRoleName(getRoleId()));
            
            resultType = prepare();
        }//Close Session Checking
        return resultType;
    }
    
    
    /**
     * This method is useful to Transfer Roles to user
     * @return The Result Type of SUCCESS.
     * @see com.mss.mirage.ApplicationConstants
     * @throws java.lang.Exception
     */
    
    
    public String getTransferRole() throws Exception {
        resultType = LOGIN;
        /*
         *This if loop is to check whether there is Session or not
         **/
        if(httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null){
            
            userRoleId = Integer.parseInt(httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_ROLE_ID).toString());
            resultType = "accessFailed";
            /*
             *This if loop is to check the Authentication
             **/
            if(AuthorizationManager.getInstance().isAuthorizedUser("GET_TRANSFER_ROLE",userRoleId)){
                try{
                    /**
                     *
                     *
                     *   @param   Taking   form submitted values of addedRolesList
                     *   @return   String array rightParams
                     *
                     *
                     */
                    
                    String[] rightParams =
                            (String[])parameters.get("addedRolesList");
                    
                    /**
                     *
                     *
                     *   @param   Taking   form submitted values of leftSideEmployeeRoles
                     *   @return   String array leftParams
                     *
                     *
                     */
                    String[] leftParams = (String[])parameters.get("leftSideEmployeeRoles");
                    
                    /**
                     *
                     *
                     *   @param   Taking   rightParams array,getEmpRoleId method of this object and getPrimaryRole method of this object.
                     *   @return   result of inserted rows in the database.
                     *
                     *
                     */
                    
                     
Map rolesMap=(Map)httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_MY_ROLES);
boolean isValid = true;
if(!rolesMap.containsValue("Admin")){
    for(int i=0;i < rightParams.length;i++){
        if(Integer.parseInt( rightParams[i])==1 || Integer.parseInt( rightParams[i])==13){
            isValid = false;
        }
    }
}

                    if(isValid){
                            int insertedRows = ServiceLocator.getAdminService().insertRoles(rightParams,this.getEmpRoleId(),getPrimaryRole());
                    searchPrepare();
                    resultType = SUCCESS;
                        setResultMessage("<font color=\"green\" size=\"1.5\">Roles has been successfully Added!</font>");
                    httpServletRequest.setAttribute(ApplicationConstants.RESULT_MSG, getResultMessage());
                    }
                
                    
                }catch (Exception ex){
                    //List errorMsgList = ExceptionToListUtility.errorMessages(ex);
                    httpServletRequest.getSession(false).setAttribute("errorMessage",ex.toString());
                    resultType =  ERROR;
                }
                
            }//END-Authorization Checking
            
        }//Close Session Checking
        return resultType;
        
    }
    
    
    
    /**
     * This method is useful to get the Transfer Roles screen
     * @return The Result Type of SUCCESS.
     * @see com.mss.mirage.ApplicationConstants
     * @throws java.lang.Exception
     */
    
    
    public String getTransferScreensRole() throws Exception {
        
        resultType = LOGIN;
        /*
         *This if loop is to check whether there is Session or not
         **/
        if(httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null){
            /**
             *
             *
             *   @param   Taking   form submitted values of rightSideRoleScreens
             *   @return   String array rightParams
             *
             *
             */
            String[] rightParams =
                    (String[])parameters.get("rightSideRoleScreens");
            // Returns the  rightParamsLength
            int   rightParamsLength = rightParams.length;
            
            /**
             *
             *
             *   @param   Taking   rightParams array,getRoleId().
             *   @return   result of inserted rows in the database.
             *
             *
             */
            int insertedRows = ServiceLocator.getAdminService().insertRoleScreens(rightParams,getRoleId());
            resultType = SUCCESS;
        }//Close Session Checking
        return resultType;
        
    }
    
    /**
     * This method is useful to get Assigned Role Screen of a user
     * @return The Result Type of SUCCESS.
     * @see com.mss.mirage.ApplicationConstants
     * @throws java.lang.Exception
     */
    
    
    public String getAssignScreen() throws Exception {
        resultType = LOGIN;
        /*
         *This if loop is to check whether there is Session or not
         **/
        if(httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null){
            /**
             *
             *
             *   @param   Taking  getModuleId(),getScreenName(),getScreenAction(),getScreenTitle()
             *   @return   result of inserted rows in the database.
             *
             *
             */
            int insScreenId = ServiceLocator.getAdminService().insertNewScreen(getModuleId(),getScreenName(),getScreenAction(),getScreenTitle());
            resultType = SUCCESS;
        }//Close Session Checking
        return resultType;
    }
    
    /**
     *
     *   @see com.mss.mirage.ApplicationConstants
     *
     *   @return   The Result Type of SUCCESS.
     *
     *   @throws  NullPointerException
     *          If a NullPointerException exists and its <code>{@link
     *          java.lang.NullPointerException}</code>
     *
     */
    
    /**
     * resetPasswor() method used for mainly to reset to display the message on login page
     * user will provide some security information in resetPassword.jsp page
     * suppose he provide exact infirmation ,then he can reset password successfully,he will get
     * success message, other wise he will get sorry message.
     * @return The Result Type
     */
    public String resetPassword(){
//        applicationDataProvider = applicationDataProvider.getInstance();
    	JSONObject jObj = null;
        resultType = LOGIN;
        /*
         *This if loop is to check whether there is Session or not
         **/
        if(httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null){
            //setLoginId(httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID).toString());
            //boolean isReset = ServiceLocator.getGeneralService().updatePassword(this);
            setUserRoleName(httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_ROLE_NAME).toString());
            setWorkingCountry(httpServletRequest.getSession(false).getAttribute(ApplicationConstants.WORKING_COUNTRY).toString());
            userRoleId = Integer.parseInt(httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_ROLE_ID).toString());
            setRolesMap((Map)httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_MY_ROLES));
           
            
            
 //Map rolesMap = (Map) httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_ROLE_NAME);
            resultType = "accessFailed";
            try{
                /*
                 *This if loop is to check the Authentication
                 **/
                if(AuthorizationManager.getInstance().isAuthorizedUser("GET_RESET_USERPWD",userRoleId)){
                    
                	if(!"".equals(getNewPassword()) && !"".equals(getCnfPassword()))
                	{
                   /* if(!(getNewPassword().equals("")) && !(getCnfPassword().equals(""))  ){*/
                        int updatedRows = ServiceLocator.getAdminService().updatePassword(this);
                    	jObj = new JSONObject();
                        
                       	jObj.put("loginId",getLoginId());
        				jObj.put("upDateAttibute", "userPassword");
        	           	jObj.put("upDateValue", getNewPassword());
        	           	LdapServiceProvider.empLDAPPswdUpdate(jObj);
//                       	 if(!LdapServiceProvider.empLDAPPswdUpdate(jObj)){
//                    		 updatedRows=0;
//                       	 }
                        if(updatedRows == 1){//isReset
                        	System.out.println("updatedRows....."+updatedRows);
                        
                        	
                            setResultMessage("You have changed User password succesfully ");
                            resultType = SUCCESS;
                        }else{
                            setResultMessage("Sorry!Please enter valid password! Or Your are not authorized person to change the above person password!");
                            resultType = INPUT;
                        }
                    }else{
                        setResultMessage("Sorry!Please enter password! ");
                    }
                    httpServletRequest.setAttribute("resultMessage", getResultMessage());
                    
                    resultType = SUCCESS;
                    
                }//Close Authentication Checking
            }catch (Exception ex){
                //List errorMsgList = ExceptionToListUtility.errorMessages(ex);
                httpServletRequest.getSession(false).setAttribute("errorMessage",ex.toString());
                resultType =  ERROR;
            }
        }//Close Session Checking
        return resultType;
        
    }//end of the resetPassword() method
    
     public String resetCustPassword(){
//        applicationDataProvider = applicationDataProvider.getInstance();
        
        resultType = LOGIN;
        /*
         *This if loop is to check whether there is Session or not
         **/
        if(httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null){
            //setLoginId(httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID).toString());
            //boolean isReset = ServiceLocator.getGeneralService().updatePassword(this);
            setUserRoleName(httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_ROLE_NAME).toString());
            setWorkingCountry(httpServletRequest.getSession(false).getAttribute(ApplicationConstants.WORKING_COUNTRY).toString());
            userRoleId = Integer.parseInt(httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_ROLE_ID).toString());
            resultType = "accessFailed";
            try{
                /*
                 *This if loop is to check the Authentication
                 **/
                if(AuthorizationManager.getInstance().isAuthorizedUser("GET_RESET_USERPWD",userRoleId)){
                	
                	if( (!"".equals(getNewPassword())) && (!"".equals(getCnfPassword())) )
                	{
                 /*   if(!(getNewPassword().equals("")) && !(getCnfPassword().equals(""))  ){*/
                        int updatedRows = ServiceLocator.getAdminService().updateCustPassword(this);
                        if(updatedRows == 1){//isReset
                            setResultMessage("You have changed User password succesfully ");
                            resultType = SUCCESS;
                        }else{
                            setResultMessage("Sorry!Please enter valid password! Or Your are not authorized person to change the above person password!");
                            resultType = INPUT;
                        }
                    }else{
                        setResultMessage("Sorry!Please enter password! ");
                    }
                    httpServletRequest.setAttribute("resultMessage", getResultMessage());
                    
                    resultType = SUCCESS;
                    
                }//Close Authentication Checking
            }catch (Exception ex){
                ex.printStackTrace();
                //List errorMsgList = ExceptionToListUtility.errorMessages(ex);
                httpServletRequest.getSession(false).setAttribute("errorMessage",ex.toString());
                resultType =  ERROR;
            }
        }//Close Session Checking
        return resultType;
        
    }
     public String getExecutiveDashBoard() throws Exception {
        resultType = LOGIN;
        /*
         *This if loop is to check whether there is Session or not
         **/
        if(httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null){
            
            userRoleId = Integer.parseInt(httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_ROLE_ID).toString());
            resultType = "accessFailed";
            if(AuthorizationManager.getInstance().isAuthorizedUser("EXECUTIVE_DASHBOARD",userRoleId)){
                try{
                    setBdmMap(DataSourceDataProvider.getInstance().getAllBDMLoginIds());
                   setOpportunityStateList(DataSourceDataProvider.getInstance().getOpportunityStateList());  
                    if(userRoleId==1){
                     
                     setTeamMemberList(DataSourceDataProvider.getInstance().getEmployeeNamesByUserId("Sales"));
                     }else{
                       setTeamMemberList(null);
                     }
                     setStartDateContacts(DateUtility.getInstance().getLastSixtyDaysDateFromCurrentDate());
                                          setEndDateContacts(DateUtility.getInstance().getCurrentSQLDate1());
                                          
                                          String loginId=httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID).toString();
                                          
                                          if(loginId.equalsIgnoreCase("mlokam")){
                                          	setBdmMapReq(DataSourceDataProvider.getInstance().getAllBDMByReportsTo(loginId));
                                          }else{
                                          	setBdmMapReq(DataSourceDataProvider.getInstance().getAllBDMs());
                                          }
                                          setReqStartDate(DateUtility.getInstance().getLastSixtyDaysDateFromCurrentDate());
                                          setReqEndDate(DateUtility.getInstance().getCurrentSQLDate1());
                    resultType = SUCCESS;
                    
                }catch (Exception ex){
                    httpServletRequest.getSession(false).setAttribute("errorMessage",ex.toString());
                    resultType =  ERROR;
                }
                
            }//END-Authorization Checking
            
        }//Close Session Checking
        return resultType;
        
    }

     /** Created By :Tirumala Teja Kadamanti
          Purpose : Newsletters Deployment Automation **/
     
      public String getNewsLetters() throws Exception {
        resultType = LOGIN;
        /*
         *This if loop is to check whether there is Session or not
         **/
        if(httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null){
            
            userRoleId = Integer.parseInt(httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_ROLE_ID).toString());
            resultType = "accessFailed";
            if(AuthorizationManager.getInstance().isAuthorizedUser("NEWSLETTERS_DEPLOYMENT_ACCESS",userRoleId)){
                try{
                    
                    setUrlImages(Properties.getProperty("Images.NewsLetters.URL"));
                    setUrlNewsletters(Properties.getProperty("NewsLetters.URL"));
                    
                    resultType = SUCCESS;
                    
                }catch (Exception ex){
                    httpServletRequest.getSession(false).setAttribute("errorMessage",ex.toString());
                    resultType =  ERROR;
                }
                
            }//END-Authorization Checking
            
        }//Close Session Checking
        return resultType;
        
    }
  /* News Letters Deployment Automation End */  
      
      public String getBdmAssociates() throws Exception {
        //  System.out.println("in getBdmAssociates()");
        resultType = LOGIN;
        /*
         *This if loop is to check whether there is Session or not
         **/
        if(httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null){
            
            userRoleId = Integer.parseInt(httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_ROLE_ID).toString());
            resultType = "accessFailed";
            
                try{
                 setBdmMap(DataSourceDataProvider.getInstance().getAllBDMs());
                    if(getSubmitFrom()==null){
                  queryString = "SELECT Id,CONCAT(TRIM(FName),'.',TRIM(LName)) AS EmployeeName FROM"
                    + " tblEmployee WHERE CurStatus='Active' AND TitleTypeId='BDM'";
                   resultType = SUCCESS;
                  //  System.out.println("queryString is---->"+queryString);
                       httpServletRequest.setAttribute(ApplicationConstants.QUERY_STRING,queryString);
                    }
                }catch (Exception ex){
                       ex.printStackTrace();
                    httpServletRequest.getSession(false).setAttribute("errorMessage",ex.toString());
                    resultType =  ERROR;
                }
                
           //END-Authorization Checking
            
        }//Close Session Checking
        return resultType;
        
    }
      
      
      
      
      public String addBdmTeam() throws Exception {
     //     System.out.println("in addBdmTeam()");
        resultType = LOGIN;
        /*
         *This if loop is to check whether there is Session or not
         **/
        if(httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null){
            
            userRoleId = Integer.parseInt(httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_ROLE_ID).toString());
            resultType = "accessFailed";
 
                try{
//                    setSalesMap(DataSourceDataProvider.getInstance().getEmployeeNames("Sales"));
//                 setBdmMap(DataSourceDataProvider.getInstance().getAllBDMs());
//                setAssignedBdmTeam(DataSourceDataProvider.getInstance().getBdmTeam(getBdmId()));
                    
                      setBdmMap(DataSourceDataProvider.getInstance().getAllBDMs());
                setAssignedBdmTeam(DataSourceDataProvider.getInstance().getBdmTeam(getBdmId()));
                setSalesMap(DataSourceDataProvider.getInstance().getBdmSalesAssignedTeam(getAssignedBdmTeam()));
               //     System.out.println("bdm is is--->"+getBdmId());
//                  queryString = "SELECT Id,CONCAT(TRIM(FName),'.',TRIM(LName)) AS EmployeeName FROM"
//                    + " tblEmployee WHERE CurStatus='Active' AND TitleTypeId='BDM'  ORDER BY EmployeeName";
                  
              queryString = "SELECT tblEmployee.Id,CONCAT(TRIM(FName),'.',TRIM(LName)) AS EmployeeName,tblEmployee.Email1 AS Email,tblEmployee.WorkPhoneNo AS WorkPhnNo,tblEmployee.TitleTypeId AS Title,tblBDMAssociates.STATUS AS Status,tblBDMAssociates.CreatedBy AS CreatedBy ,tblBDMAssociates.BdmId AS BdmId "
                      + " FROM tblEmployee LEFT JOIN tblBDMAssociates ON(tblEmployee.Id=tblBDMAssociates.TeamMemberId) WHERE tblBDMAssociates.BdmId='"+getBdmId()+"'";
                  setBdmName(DataSourceDataProvider.getInstance().getEmpNameByEmpId(Integer.parseInt(getBdmId())));
                  
                //    System.out.println("bdm name is----->"+getBdmName());
                //    System.out.println("queryString is---->"+queryString);
                        httpServletRequest.setAttribute(ApplicationConstants.QUERY_STRING,queryString);
                         resultType = SUCCESS;
                }catch (Exception ex){
                    ex.printStackTrace();
                    httpServletRequest.getSession(false).setAttribute("errorMessage",ex.toString());
                    resultType =  ERROR;
                }
                
           //END-Authorization Checking
            
        }//Close Session Checking
        return resultType;
        
    }
      
   
      
       public String bdmTeamSubmit() throws Exception {
        resultType = LOGIN;
        int insertedRows = 0;
          Connection connection = null;
           CallableStatement callableStatement = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
     connection = ConnectionProvider.getInstance().getConnection();
        Statement statement = null;
        String  assignedBdmId = null;
        //   System.out.println("in bdmTeamSubmit()");
        /*
         *This if loop is to check whether there is Session or not
         **/
        if(httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null){
            
            userRoleId = Integer.parseInt(httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_ROLE_ID).toString());
            resultType = "accessFailed";
          
   
                try{
     
                 String createdBy=httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID).toString();
                Timestamp createdDate = DateUtility.getInstance().getCurrentMySqlDateTime();
            
      String emailId=DataSourceDataProvider.getInstance().getEmailIdForEmployee(Integer.parseInt(getPreAssignSalesId()));
      String workPhn=DataSourceDataProvider.getInstance().getWorkPhNoById(getPreAssignSalesId());
  
                    setStatus("Active");
                     if((ServiceLocator.getAdminService().checkSalesAgainstBdm(this))){
                     connection = ConnectionProvider.getInstance().getConnection();
           callableStatement = connection.prepareCall("{call spBDMAssociates(?,?,?,?,?,?,?)}");
            callableStatement.setString(1,getPreAssignSalesId());
            callableStatement.setString(2,getBdmId());
            callableStatement.setString(3,emailId);
            callableStatement.setString(4,getStatus());
            callableStatement.setString(5,workPhn);
            callableStatement.setString(6,createdBy);
            callableStatement.setString(7,createdBy);
             insertedRows = callableStatement.executeUpdate();
                    setResultMessage("<font size='2' color='green'>Resource has been added successfully</font>");
               
                     }else{
                     //  System.out.println("in else case");
                       String queryString = "SELECT TeamMemberId,BdmId FROM tblBDMAssociates";
                       connection = ConnectionProvider.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(queryString);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
          String  teamMemberId=resultSet.getString("TeamMemberId");
          assignedBdmId=resultSet.getString("BdmId");
          
              //  System.out.println("empName in while loop is---->"+teamMemberId); 
                
            //    System.out.println("final member is--->"+teamMemberId);
            //    System.out.println("adminAction.getPreAssignSalesId() is--->"+getPreAssignSalesId());
                      if(teamMemberId.equals(getPreAssignSalesId())){
                          System.out.println("in equals case");
                 
               
                break;
              }
                     
            }
         //    System.out.println("assignedBdmId final is---->"+assignedBdmId);
      String assignedBdmName =  DataSourceDataProvider.getInstance().getEmployeeNameByEmpNo(Integer.parseInt(assignedBdmId));     
                        setResultMessage("<font size='2' color='red'>Resource Name has been alerady assocaited with BDM '"+assignedBdmName+"'</font>");
                    //     System.out.println("getResponseString is---->"+getResultMessage());
                     }
        //    }
               //     System.out.println("insertedRows is---->"+insertedRows);
               //       System.out.println("getResponseString is---->"+getResultMessage());
                   httpServletRequest.setAttribute(ApplicationConstants.RESULT_MSG, getResultMessage());
                  //  System.out.println("leftParams is---->"+leftParams);
                //    System.out.println("leftParams.length is---->"+leftParams.length);
                    resultType = SUCCESS;
                }catch (Exception ex){
                    ex.printStackTrace();
                    //List errorMsgList = ExceptionToListUtility.errorMessages(ex);
                    httpServletRequest.getSession(false).setAttribute("errorMessage",ex.toString());
                    resultType =  ERROR;
                }
        }//Close Session Checking
        return resultType;
        
    }
       
       
  /* Look Up changes Strat*/     
       
       public String getLookupTableData() throws Exception {
           System.out.println("in getLookupTableData()");
          resultType = LOGIN;
          /*
           *This if loop is to check whether there is Session or not
           **/
          if(httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null){
              
              userRoleId = Integer.parseInt(httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_ROLE_ID).toString());
              resultType = "accessFailed";
              
                  try{
                    queryString = "SELECT * FROM tblLookupTables where 1=1 AND Status='Active' ";
                    if (getTableId() != null && !getTableId().equals("")) {
                   	 queryString = queryString + " AND tblLookupTables.Id = "+getTableId()+" ";
                    }
                    queryString = queryString + "ORDER BY CreatedDate DESC";
                     resultType = SUCCESS;
                         httpServletRequest.setAttribute(ApplicationConstants.QUERY_STRING,queryString);
                     
                  }catch (Exception ex){
                         ex.printStackTrace();
                      httpServletRequest.getSession(false).setAttribute("errorMessage",ex.toString());
                      resultType =  ERROR;
                  }
                  
             //END-Authorization Checking
              
          }//Close Session Checking
          return resultType;
          
      }
      
      public String getLookUpColumnData() throws Exception {
          Statement statement = null;
          ResultSet resultSet = null;
          String query="";
       Connection connection=null;
       String columnNames = "";
       String columnLength="";
       String secondColumnName="";
         resultType = LOGIN;
         /*
          *This if loop is to check whether there is Session or not
          **/
         if(httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null){
             
             userRoleId = Integer.parseInt(httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_ROLE_ID).toString());
             resultType = "accessFailed";
                 try{
//               	  System.out.println("Id is---->"+getId());
//                   queryString = "SELECT * FROM "+getTableName()+" ";
                	 connection=ConnectionProvider.getInstance().getConnection();
                     query="select * from  "+getTableName()+" where 1=1";
                   //  System.out.println("query is====>"+query);  
                    statement=connection.createStatement();
                     resultSet=statement.executeQuery(query);
                     ResultSetMetaData rsMetaData = resultSet.getMetaData();

                     int numberOfColumns = rsMetaData.getColumnCount();
                //     System.out.println("resultSet MetaData column Count=" + numberOfColumns);
                     for (int i = 1; i <= numberOfColumns; i++) {
                   	  columnNames=columnNames+"#^$"+resultSet.getMetaData().getColumnName(i);
                      //   System.out.println("columnNames...."+columnNames); 
                         secondColumnName =resultSet.getMetaData().getColumnName(2);
                         columnLength=columnLength+"#^$"+resultSet.getMetaData().getColumnDisplaySize(i);
                       }
                    // System.out.println("columnNames out...."+columnNames); 
                   //  System.out.println("columnLength out...."+columnLength); 
                   //  System.out.println("secondColumnName out...."+secondColumnName); 
                    resultType = SUCCESS;
                    httpServletRequest.setAttribute(ApplicationConstants.LOOKUP_COLUMN_NAME,secondColumnName);
//                     System.out.println("queryString is---->"+queryString);
//                        httpServletRequest.setAttribute(ApplicationConstants.QUERY_STRING,queryString);
                    
                 }catch (SQLException  ex){
                	 if( ex.getErrorCode()==1146){           		 
                		 resultType = "TableException";
                		 resultMessage = "<font color=\"red\" size=\"1.5\">Sorry! "+ex.getMessage()+" .Please create the table in Database</font>";
                		 httpServletRequest.setAttribute(ApplicationConstants.RESULT_MSG, resultMessage);
                	 }
                	 else{
                		  ex.printStackTrace();
                          httpServletRequest.getSession(false).setAttribute("errorMessage",ex.toString());
                          resultType =  ERROR; 
                	 }
                      
                 }
                 
            //END-Authorization Checking
             
         }//Close Session Checking
         return resultType;
         
     }
       
  /* Look Up changes End*/     

      
      
/*Release Notes Begin*/
      
      public String getReleaseNotesUploadSearch() throws Exception {
       resultType = LOGIN;

       /*
        *This if loop is to check whether there is Session or not
        **/
       if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {

           userRoleId = Integer.parseInt(httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_ROLE_ID).toString());
           resultType = "accessFailed";

           try {
               String queryString = "SELECT tblReleaseNotes.Title,tblReleaseNotes.Date,tblReleaseNotes.Id FROM tblReleaseNotes WHERE 1=1 ";
               if (getFromDate() != null && !(getFromDate().equals("")) && getToDate() != null && !(getToDate().equals(""))) {
                   queryString = queryString + " AND (Date >= '" + (DateUtility.getInstance().convertStringToMySQLDate(getFromDate())) + "' AND Date <= '" + (DateUtility.getInstance().convertStringToMySQLDate(getToDate())) + "' )";
               }
               if (getTitle() != null && !(getTitle().equals(""))) {
                   queryString = queryString + " AND Title LIKE   '%" + getTitle() + "%' ";
               }


               httpServletRequest.setAttribute(ApplicationConstants.QUERY_STRING, queryString);
               resultType = SUCCESS;
//               System.out.println("queryString is---->" + queryString);
           } catch (Exception ex) {
               httpServletRequest.getSession(false).setAttribute("errorMessage", ex.toString());
               ex.printStackTrace();
               resultType = ERROR;
           }

           //END-Authorization Checking

       }//Close Session Checking
       return resultType;

   }
      
      
     public String getReleaseNotesAdd() throws Exception {
       resultType = LOGIN;

       /*
        *This if loop is to check whether there is Session or not
        **/
       if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {

           userRoleId = Integer.parseInt(httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_ROLE_ID).toString());
           resultType = "accessFailed";

           try {
               setCurrentAction("releaseNotesAdd");
               setRoleDetails(DataSourceDataProvider.getInstance().getRoleDetails());
               resultType = SUCCESS;
           } catch (Exception ex) {
               httpServletRequest.getSession(false).setAttribute("errorMessage", ex.toString());
               ex.printStackTrace();
               resultType = ERROR;
           }

           //END-Authorization Checking

       }//Close Session Checking
       return resultType;

   } 
      
    public String releaseNotesAdd() throws Exception {
       resultType = LOGIN;
       Connection connection = null;
       PreparedStatement preparedStatement = null;
       ResultSet resultSet = null;
       String resutMessage = "";
       int isInserted = 0;
       setIsInsertedValueCount(0);
       /*
        *This if loop is to check whether there is Session or not
        **/
       if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {

           userRoleId = Integer.parseInt(httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_ROLE_ID).toString());
           resultType = "accessFailed";

           try {
        	   System.out.println("role details list is====>"+getRoleAccess());
               String loginId = httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID).toString();
               String queryString = "INSERT INTO `mirage`.`tblReleaseNotes` (`Title`, `Date`, `CreatedBy`, `CreatedDate`,`ModifiedBy`, `ModifiedDate`,`RolesAccess`) VALUES(?,?,?,?,?,?,?)";
               connection = ConnectionProvider.getInstance().getConnection();
               preparedStatement = connection.prepareStatement(queryString);
               preparedStatement.setString(1, getTitle());
               preparedStatement.setDate(2, getDate());
               preparedStatement.setString(3, loginId);
               preparedStatement.setTimestamp(4, DateUtility.getInstance().getCurrentMySqlDateTime());
               preparedStatement.setString(5, loginId);
               preparedStatement.setTimestamp(6,DateUtility.getInstance().getCurrentMySqlDateTime());
               preparedStatement.setString(7,getRoleAccess());

               isInserted = preparedStatement.executeUpdate();
               if (isInserted > 0) {
                   preparedStatement = connection.prepareStatement("SELECT  MAX(Id) as relId FROM tblReleaseNotes");
                   resultSet = preparedStatement.executeQuery();
                   resultSet.next();
                   int relId = resultSet.getInt("relId");
                   preparedStatement.close();
                   resultSet.close();
                   String queryString1 = "INSERT INTO `mirage`.`tblReleaseNotesLines` (`RelId`, `SubTitle`, `Purpose`,`FilePath`,`FileName`) VALUES(?,?,?,?,?)";
                preparedStatement = connection.prepareStatement(queryString1);
                   if (getSubTitle0() != null && !(getSubTitle0().equals("")) && getPurpose0() != null && !(getPurpose0().equals("")) && getFile0() != null) {
                       preparedStatement.setInt(1, relId);
                       preparedStatement.setString(2, getSubTitle0());
                       preparedStatement.setString(3, getPurpose0());
                       setFile0FilePath(attachResume(getFile0(), getFile0FileName()));
                       preparedStatement.setString(4, getFile0FilePath());
                       preparedStatement.setString(5, getFile0FileName());
                       preparedStatement.executeUpdate();
                       setIsInsertedValueCount(getIsInsertedValueCount() + 1);
                   };
                   if (getSubTitle1() != null && !(getSubTitle1().equals("")) && getPurpose1() != null && !(getPurpose1().equals("")) && getFile1() != null) {
                       preparedStatement.setInt(1, relId);
                       preparedStatement.setString(2, getSubTitle1());
                       preparedStatement.setString(3, getPurpose1());
                       setFile1FilePath(attachResume(getFile1(), getFile1FileName()));
                       preparedStatement.setString(4, getFile1FilePath());
                       preparedStatement.setString(5, getFile1FileName());
                       preparedStatement.executeUpdate();
                       setIsInsertedValueCount(getIsInsertedValueCount() + 1);
                   }
                   if (getSubTitle2() != null && !(getSubTitle2().equals("")) && getPurpose2() != null && !(getPurpose2().equals("")) && getFile2() != null) {
                       preparedStatement.setInt(1, relId);
                       preparedStatement.setString(2, getSubTitle2());
                       preparedStatement.setString(3, getPurpose2());
                       setFile2FilePath(attachResume(getFile2(), getFile2FileName()));
                       preparedStatement.setString(4, getFile2FilePath());
                       preparedStatement.setString(5, getFile2FileName());
                       preparedStatement.executeUpdate();
                       setIsInsertedValueCount(getIsInsertedValueCount() + 1);
                   }
                   if (getSubTitle3() != null && !(getSubTitle3().equals("")) && getPurpose3() != null && !(getPurpose3().equals("")) && getFile3() != null) {
                       preparedStatement.setInt(1, relId);
                       preparedStatement.setString(2, getSubTitle3());
                       preparedStatement.setString(3, getPurpose3());
                       setFile3FilePath(attachResume(getFile3(), getFile3FileName()));
                       preparedStatement.setString(4, getFile3FilePath());
                       preparedStatement.setString(5, getFile3FileName());
                       preparedStatement.executeUpdate();
                       setIsInsertedValueCount(getIsInsertedValueCount() + 1);
                   }
                   if (getSubTitle4() != null && !(getSubTitle4().equals("")) && getPurpose4() != null && !(getPurpose4().equals("")) && getFile4() != null) {
                       preparedStatement.setInt(1, relId);
                       preparedStatement.setString(2, getSubTitle4());
                       preparedStatement.setString(3, getPurpose4());
                       setFile4FilePath(attachResume(getFile4(), getFile4FileName()));
                       preparedStatement.setString(4, getFile4FilePath());
                       preparedStatement.setString(5, getFile4FileName());
                       preparedStatement.executeUpdate();
                       setIsInsertedValueCount(getIsInsertedValueCount() + 1);
                   }
                   if (getSubTitle5() != null && !(getSubTitle5().equals("")) && getPurpose5() != null && !(getPurpose5().equals("")) && getFile5() != null) {
                       preparedStatement.setInt(1, relId);
                       preparedStatement.setString(2, getSubTitle5());
                       preparedStatement.setString(3, getPurpose5());
                       setFile5FilePath(attachResume(getFile5(), getFile5FileName()));
                       preparedStatement.setString(4, getFile5FilePath());
                       preparedStatement.setString(5, getFile5FileName());
                       preparedStatement.executeUpdate();
                       setIsInsertedValueCount(getIsInsertedValueCount() + 1);
                   }
                   setIsInsertedValueCount(getIsInsertedValueCount());
               }

               resultType = INPUT;
               setResultMessage("<font color=\"green\" size=\"1.5\">Release Notes has been successfully Added!</font>");
               httpServletRequest.setAttribute(ApplicationConstants.RESULT_MSG, getResultMessage());
           } catch (Exception ex) {
               httpServletRequest.getSession(false).setAttribute("errorMessage", ex.toString());
               ex.printStackTrace();
               resultType = ERROR;
           } finally {
               try {
                   if (preparedStatement != null) {
                       preparedStatement.close();
                       preparedStatement = null;
                   }
                   if (connection != null) {
                       connection.close();
                       connection = null;
                   }
               } catch (SQLException se) {
                   se.printStackTrace();
                   //throw new ServiceLocatorException(se);
               }
           }

           //END-Authorization Checking

       }//Close Session Checking
       return resultType;

   }
    
    
     public String getEditReleaseNotesUpload() throws Exception {
       resultType = LOGIN;
       int count = 0;
         Connection connection = null;
       Statement statement = null;
       ResultSet resultSet = null;
       String resutMessage = "";
      
       HashSet content = new HashSet();
       /*
        *This if loop is to check whether there is Session or not
        **/
       if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {

           userRoleId = Integer.parseInt(httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_ROLE_ID).toString());
           resultType = "accessFailed";

           try {

        	   setRoleDetails(DataSourceDataProvider.getInstance().getRoleDetails());
               String queryString = "SELECT tblReleaseNotes.Title,tblReleaseNotes.DATE,tblReleaseNotesLines.SubTitle,tblReleaseNotesLines.Purpose,tblReleaseNotesLines.FilePath,tblReleaseNotesLines.FileName,tblReleaseNotesLines.Id,tblReleaseNotes.RolesAccess FROM tblReleaseNotes LEFT OUTER JOIN tblReleaseNotesLines ON(tblReleaseNotes.Id=tblReleaseNotesLines.RelId) WHERE tblReleaseNotes.Id=" + getReleaseId();
               connection = ConnectionProvider.getInstance().getConnection();
               statement = connection.createStatement();
               resultSet = statement.executeQuery(queryString);
              // System.out.println("queryString====>"+queryString);
               while (resultSet.next()) {
                   if (resultSet.getString("Title") != null) {
                       setTitle(resultSet.getString("Title"));

                   }
                   if (resultSet.getDate("DATE") != null) {
                       setDate(resultSet.getDate("DATE"));
                   }
//                   if (resultSet.getString("RolesAccess") != null) {
//                       setRoleAccess(resultSet.getString("RolesAccess"));
//                   }
//                   System.out.println("role access====>"+getRoleAccess());
                 //  System.out.println("getRoleDetailsList before====>"+getRoleDetailsList());
                   StringTokenizer str1 = new StringTokenizer(resultSet.getString("RolesAccess"), ",");
                   while (str1.hasMoreTokens()) {
                	   content.add(str1.nextToken());
                   }
                
              
           
                   if (count == 0) {
                       if (resultSet.getString("SubTitle") != null) {
                           setSubTitle0(resultSet.getString("SubTitle"));
                       }
                       if (resultSet.getString("Purpose") != null) {
                           setPurpose0(resultSet.getString("Purpose"));
                       }
                        if (resultSet.getString("Id") != null) {
                           setSubId0(resultSet.getString("Id"));
                       }
                        if (resultSet.getString("FileName") != null) {
                           setFile0FileName(resultSet.getString("FileName"));
                       }
                        if (resultSet.getString("FilePath") != null) {
                           setFile0FilePath(resultSet.getString("FilePath"));
                       }
                       
                   }
                   if (count == 1) {
                       if (resultSet.getString("SubTitle") != null) {
                           setSubTitle1(resultSet.getString("SubTitle"));
                       }
                       if (resultSet.getString("Purpose") != null) {
                           setPurpose1(resultSet.getString("Purpose"));
                       }
                       if (resultSet.getString("Id") != null) {
                           setSubId1(resultSet.getString("Id"));
                       }
                       if (resultSet.getString("FileName") != null) {
                           setFile1FileName(resultSet.getString("FileName"));
                       }
                       if (resultSet.getString("FilePath") != null) {
                           setFile1FilePath(resultSet.getString("FilePath"));
                       }
                   }
                   if (count == 2) {
                       if (resultSet.getString("SubTitle") != null) {
                           setSubTitle2(resultSet.getString("SubTitle"));
                       }
                       if (resultSet.getString("Purpose") != null) {
                           setPurpose2(resultSet.getString("Purpose"));
                       }
                       if (resultSet.getString("Id") != null) {
                           setSubId2(resultSet.getString("Id"));
                       }
                       if (resultSet.getString("FileName") != null) {
                           setFile2FileName(resultSet.getString("FileName"));
                       }
                       if (resultSet.getString("FilePath") != null) {
                           setFile2FilePath(resultSet.getString("FilePath"));
                       }
                   }
                   if (count == 3) {
                       if (resultSet.getString("SubTitle") != null) {
                           setSubTitle3(resultSet.getString("SubTitle"));
                       }
                       if (resultSet.getString("Purpose") != null) {
                           setPurpose3(resultSet.getString("Purpose"));
                       }
                       if (resultSet.getString("Id") != null) {
                           setSubId3(resultSet.getString("Id"));
                       }
                       if (resultSet.getString("FileName") != null) {
                           setFile3FileName(resultSet.getString("FileName"));
                       }
                       if (resultSet.getString("FilePath") != null) {
                           setFile3FilePath(resultSet.getString("FilePath"));
                       }
                   }
                   if (count == 4) {
                       if (resultSet.getString("SubTitle") != null) {
                           setSubTitle4(resultSet.getString("SubTitle"));
                       }
                       if (resultSet.getString("Purpose") != null) {
                           setPurpose4(resultSet.getString("Purpose"));
                       }
                       if (resultSet.getString("Id") != null) {
                           setSubId4(resultSet.getString("Id"));
                       }
                       if (resultSet.getString("FileName") != null) {
                           setFile4FileName(resultSet.getString("FileName"));
                       }
                       if (resultSet.getString("FilePath") != null) {
                           setFile4FilePath(resultSet.getString("FilePath"));
                       }
                   }
                   if (count == 5) {
                       if (resultSet.getString("SubTitle") != null) {
                           setSubTitle5(resultSet.getString("SubTitle"));
                       }
                       if (resultSet.getString("Purpose") != null) {
                           setPurpose5(resultSet.getString("Purpose"));
                       }
                       if (resultSet.getString("Id") != null) {
                           setSubId5(resultSet.getString("Id"));
                       }
                       if (resultSet.getString("FileName") != null) {
                           setFile5FileName(resultSet.getString("FileName"));
                       }
                       if (resultSet.getString("FilePath") != null) {
                           setFile5FilePath(resultSet.getString("FilePath"));
                       }
                   }
                   count++;
               }
               setCount(count);
               setRoleDetailsList(new ArrayList(content));
             //  System.out.println("getRoleDetailsList after====>"+getRoleDetailsList());
               setCurrentAction("releaseNotesUploadUpdate");
               resultType = SUCCESS; 
           } catch (Exception ex) {
               httpServletRequest.getSession(false).setAttribute("errorMessage", ex.toString());
               ex.printStackTrace();
               resultType = ERROR;
           } finally {
               try {
                   if (statement != null) {
                       statement.close();
                       statement = null;
                   }
                   if (connection != null) {
                       connection.close();
                       connection = null;
                   }
               } catch (SQLException se) {
                   se.printStackTrace();
                   //throw new ServiceLocatorException(se);
               }
           }

           //END-Authorization Checking

       }//Close Session Checking
       return resultType;

   }
    
   
     public String releaseNotesUploadUpdate() throws Exception {
       resultType = LOGIN;
       Connection connection = null;
       PreparedStatement preparedStatement = null;
       PreparedStatement preparedStatement1 = null;
       PreparedStatement preparedStatement2 = null;
       int updatedRows = 0;
       String resutMessage = "";
       /*
        *This if loop is to check whether there is Session or not
        **/
       if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {

           userRoleId = Integer.parseInt(httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_ROLE_ID).toString());
           resultType = "accessFailed";

           try {
               String loginId = httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID).toString();
              
               String queryString = "UPDATE tblReleaseNotes SET Title=?,Date=?,ModifiedBy=?,ModifiedDate=?,RolesAccess=? WHERE Id=?";
               connection = ConnectionProvider.getInstance().getConnection();
               preparedStatement = connection.prepareStatement(queryString);
               preparedStatement.setString(1, getTitle());
               preparedStatement.setDate(2, getDate());
               preparedStatement.setString(3, loginId);
               preparedStatement.setTimestamp(4,DateUtility.getInstance().getCurrentMySqlDateTime());
               preparedStatement.setString(5, getRoleAccess());
               preparedStatement.setInt(6, getReleaseId());
               updatedRows = preparedStatement.executeUpdate();
               
               if (updatedRows > 0) {
                   resutMessage = "inserted";
                   String queryString1 = "UPDATE tblReleaseNotesLines SET SubTitle=?,Purpose=?,FilePath=?,FileName=? WHERE Id=?"; /*already present values*/
                   String queryString2 = "INSERT INTO `mirage`.`tblReleaseNotesLines` (`RelId`, `SubTitle`, `Purpose`,`FilePath`,`FileName`) VALUES(?,?,?,?,?)"; /*newly added subtitle fields present values*/
                     preparedStatement = connection.prepareStatement(queryString1);
                     preparedStatement1 = connection.prepareStatement(queryString2);
                   if (getMinusHiden() != null && getMinusHiden().length() > 0 && getMinusHiden().charAt(getMinusHiden().length()-1)==',') {
     setMinusHiden(getMinusHiden().substring(0, getMinusHiden().length()-1));
       String queryString3 = "DELETE FROM tblReleaseNotesLines WHERE FIND_IN_SET(Id,'"+getMinusHiden()+"')";
       preparedStatement2 = connection.prepareStatement(queryString3);
                       preparedStatement2.executeUpdate();
   }
                //   System.out.println("getSubId0()---->"+getSubId0());
                   if(getSubId0() != null && !(getSubId0().equals("")) && getSubId0() != "0"){
                     //  System.out.println("in subId0 case");
                    //   System.out.println("getSubTitle0()====>"+getSubTitle0());
                    //   System.out.println("getPurpose0()====>"+getPurpose0());
                     //  System.out.println("getFile0FileName()====>"+getFile0FileName());
                   if (getSubTitle0() != null && !(getSubTitle0().equals("")) && getPurpose0() != null && !(getPurpose0().equals("")) && getFile0FileName() != null && !getFile0FileName().equals("")) {
                    //   System.out.println("in if and update");
                       preparedStatement.setString(1, getSubTitle0());
                       preparedStatement.setString(2, getPurpose0());
                     setFile0FilePath(attachResume(getFile0(), getFile0FileName()));
                      // System.out.println("getFile0FilePath() of subId0 zero case---->"+getFile0FilePath());
                       preparedStatement.setString(3, getFile0FilePath());
                       preparedStatement.setString(4, getFile0FileName());
                       preparedStatement.setString(5, getSubId0());
                   int x=    preparedStatement.executeUpdate();
                      // System.out.println("x---->"+x);
                       setIsUpdatedValueCount(getIsUpdatedValueCount() + 1);
                   }
                   }
                   else{
                       if (getSubTitle0() != null && !(getSubTitle0().equals("")) && getPurpose0() != null && !(getPurpose0().equals("")) && getFile0FileName() != null && !getFile0FileName().equals("")) {
                      // System.out.println("insert to be done");
                           preparedStatement1.setInt(1, getReleaseId());
                       preparedStatement1.setString(2, getSubTitle0());
                       preparedStatement1.setString(3, getPurpose0());
                    //   attachResume(getFile0(), getFile0FileName());
                       //  System.out.println("getFile0FilePath() of subId0 non zero case---->"+getFile0FilePath());
                        setFile0FilePath(attachResume(getFile0(), getFile0FileName()));
                       preparedStatement1.setString(4, getFile0FilePath());
                       preparedStatement1.setString(5, getFile0FileName());
  
                       preparedStatement1.executeUpdate();
                       setIsInsertedValueCount(getIsInsertedValueCount() + 1);
                   }
                   }
                    if(getSubId1() != null && !(getSubId1().equals("")) && getSubId1() != "0"){
                 
                  if (getSubTitle1() != null && !(getSubTitle1().equals("")) && getPurpose1() != null && !(getPurpose1().equals("")) && getFile1FileName() != null && !getFile1FileName().equals("")) {
                       preparedStatement.setString(1, getSubTitle1());
                       preparedStatement.setString(2, getPurpose1());
                       setFile1FilePath(attachResume(getFile1(), getFile1FileName()));
                       preparedStatement.setString(3, getFile1FilePath());
                       preparedStatement.setString(4, getFile1FileName());
                       preparedStatement.setString(5, getSubId1());
                       preparedStatement.executeUpdate();
                       setIsUpdatedValueCount(getIsUpdatedValueCount() + 1);
                   }
                    }
                    else{
                        if (getSubTitle1() != null && !(getSubTitle1().equals("")) && getPurpose1() != null && !(getPurpose1().equals("")) && getFile1FileName() != null && !getFile1FileName().equals("")) {
                          preparedStatement1.setInt(1, getReleaseId());
                       preparedStatement1.setString(2, getSubTitle1());
                       preparedStatement1.setString(3, getPurpose1());
                     //  attachResume(getFile1(), getFile1FileName());
                        setFile1FilePath(attachResume(getFile1(), getFile1FileName()));
                       preparedStatement1.setString(4, getFile1FilePath());
                       preparedStatement1.setString(5, getFile1FileName());
                       preparedStatement1.executeUpdate();
                       setIsInsertedValueCount(getIsInsertedValueCount() + 1);
                    }
                    }
                     if(getSubId2() != null && !(getSubId2().equals("")) && getSubId2() != "0"){
                  if (getSubTitle2() != null && !(getSubTitle2().equals("")) && getPurpose2() != null && !(getPurpose2().equals(""))  && getFile2FileName() != null && !getFile2FileName().equals("")) {
                      preparedStatement.setString(1, getSubTitle2());
                       preparedStatement.setString(2, getPurpose2());
                       setFile2FilePath(attachResume(getFile2(), getFile2FileName()));
                       preparedStatement.setString(3, getFile2FilePath());
                       preparedStatement.setString(4, getFile2FileName());
                      preparedStatement.setString(5, getSubId2());
                       preparedStatement.executeUpdate();
                        setIsUpdatedValueCount(getIsUpdatedValueCount() + 1);
                       
                   }
                     }
                     else{
                         if (getSubTitle2() != null && !(getSubTitle2().equals("")) && getPurpose2() != null && !(getPurpose2().equals("")) && getFile2FileName() != null && !getFile2FileName().equals("")) {
                          preparedStatement1.setInt(1, getReleaseId());
                       preparedStatement1.setString(2, getSubTitle2());
                       preparedStatement1.setString(3, getPurpose2());
                   //    attachResume(getFile2(), getFile2FileName());
                        setFile2FilePath(attachResume(getFile2(), getFile2FileName()));
                       preparedStatement1.setString(4, getFile2FilePath());
                       preparedStatement1.setString(5, getFile2FileName());
  
                       preparedStatement1.executeUpdate();
                         setIsInsertedValueCount(getIsInsertedValueCount() + 1);
                     }
                     }
                     if(getSubId3() != null && !(getSubId3().equals("")) && getSubId3() != "0"){
                       
                  if (getSubTitle3() != null && !(getSubTitle3().equals("")) && getPurpose3() != null && !(getPurpose3().equals("")) && getFile3FileName() != null && !getFile3FileName().equals("")) {
                       preparedStatement.setString(1, getSubTitle3());
                       preparedStatement.setString(2, getPurpose3());
                      setFile3FilePath(attachResume(getFile3(), getFile3FileName()));
                       preparedStatement.setString(3, getFile3FilePath());
                       preparedStatement.setString(4, getFile3FileName());
                       preparedStatement.setString(5, getSubId3());
                       preparedStatement.executeUpdate();
                          setIsUpdatedValueCount(getIsUpdatedValueCount() + 1);
                     
                   }
                     }
                     else{
                         if (getSubTitle3() != null && !(getSubTitle3().equals("")) && getPurpose3() != null && !(getPurpose3().equals("")) && getFile3FileName() != null && !getFile3FileName().equals("")) {
                             preparedStatement1.setInt(1, getReleaseId());
                       preparedStatement1.setString(2, getSubTitle3());
                       preparedStatement1.setString(3, getPurpose3());
                     //  attachResume(getFile3(), getFile3FileName());
                        setFile3FilePath(attachResume(getFile3(), getFile3FileName()));
                       preparedStatement1.setString(4, getFile3FilePath());
                       preparedStatement1.setString(5, getFile3FileName());
  
                       preparedStatement1.executeUpdate();
                        setIsUpdatedValueCount(getIsUpdatedValueCount() + 1);
                     }
                     }
                       if(getSubId4() != null && !(getSubId4().equals("")) && getSubId4() != "0"){
                          
                  if (getSubTitle4() != null && !(getSubTitle4().equals("")) && getPurpose4() != null && !(getPurpose4().equals("")) && getFile4FileName() != null && !getFile4FileName().equals("")) {
                       preparedStatement.setString(1, getSubTitle4());
                       preparedStatement.setString(2, getPurpose4());
                    setFile4FilePath(attachResume(getFile4(), getFile4FileName()));
                       preparedStatement.setString(3, getFile4FilePath());
                       preparedStatement.setString(4, getFile4FileName());
                       preparedStatement.setString(5, getSubId4());
                       preparedStatement.executeUpdate();
                       setIsInsertedValueCount(getIsInsertedValueCount() + 1);
                   }
                       }
                       else{
                           if (getSubTitle4() != null && !(getSubTitle4().equals("")) && getPurpose4() != null && !(getPurpose4().equals("")) && getFile4FileName() != null && !getFile4FileName().equals("")) {
                            preparedStatement1.setInt(1, getReleaseId());
                       preparedStatement1.setString(2, getSubTitle4());
                       preparedStatement1.setString(3, getPurpose4());
                    //   attachResume(getFile4(), getFile4FileName());
                        setFile4FilePath(attachResume(getFile4(), getFile4FileName()));
                       preparedStatement1.setString(4, getFile4FilePath());
                       preparedStatement1.setString(5, getFile4FileName());
  
                       preparedStatement1.executeUpdate();
                           setIsInsertedValueCount(getIsInsertedValueCount() + 1);
                       }
                       }
                        if(getSubId5() != null && !(getSubId5().equals("")) && getSubId5() != "0"){
                  if (getSubTitle5() != null && !(getSubTitle5().equals("")) && getPurpose5() != null && !(getPurpose5().equals("")) && getFile5FileName() != null && !getFile5FileName().equals("")) {
                       preparedStatement.setString(1, getSubTitle5());
                       preparedStatement.setString(2, getPurpose5());
                     setFile5FilePath(attachResume(getFile5(), getFile5FileName()));
                       preparedStatement.setString(3, getFile5FilePath());
                       preparedStatement.setString(4, getFile5FileName());
                       preparedStatement.setString(5, getSubId5());
                       preparedStatement.executeUpdate();
                       setIsUpdatedValueCount(getIsUpdatedValueCount() + 1);
                   }
                        }
                        else{
                            if (getSubTitle5() != null && !(getSubTitle5().equals("")) && getPurpose5() != null && !(getPurpose5().equals("")) && getFile5FileName() != null && !getFile5FileName().equals("")) {
                              preparedStatement1.setInt(1, getReleaseId());
                       preparedStatement1.setString(2, getSubTitle5());
                       preparedStatement1.setString(3, getPurpose5());
                        setFile5FilePath(attachResume(getFile5(), getFile5FileName()));
                       preparedStatement1.setString(4, getFile5FilePath());
                       preparedStatement1.setString(5, getFile5FileName());
  
                       preparedStatement1.executeUpdate();
                             setIsInsertedValueCount(getIsInsertedValueCount() + 1);
                        }
                        }
                   setIsInsertedValueCount(getIsInsertedValueCount());
                setIsUpdatedValueCount(getIsUpdatedValueCount());
               }
                
               resultType = SUCCESS;
                 setResultMessage("<font color=\"green\" size=\"1.5\">Release Notes has been successfully Updated!</font>");
               httpServletRequest.setAttribute(ApplicationConstants.RESULT_MSG, getResultMessage());
           } catch (Exception ex) {
               httpServletRequest.getSession(false).setAttribute("errorMessage", ex.toString());
               ex.printStackTrace();
               resultType = ERROR;
           }
       }
       return resultType;

   }
     
     
     public String attachResume(File file, String fileName) {
       resultType = LOGIN;
       if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
           userRoleId = Integer.parseInt(httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_ROLE_ID).toString());
           // resultType = "accessFailed";


           try {
               String basePath = Properties.getProperty("ReleaseNotes.Attachments");
               File createPath = new File(basePath);
               //  System.err.println("basePath..."+basePath);
               java.util.Date dt = new java.util.Date();
               /*The month is generated from her*/
               String month = "";
               if (dt.getMonth() == 0) {
                   month = "Jan";
               } else if (dt.getMonth() == 1) {
                   month = "Feb";
               } else if (dt.getMonth() == 2) {
                   month = "Mar";
               } else if (dt.getMonth() == 3) {
                   month = "Apr";
               } else if (dt.getMonth() == 4) {
                   month = "May";
               } else if (dt.getMonth() == 5) {
                   month = "Jun";
               } else if (dt.getMonth() == 6) {
                   month = "Jul";
               } else if (dt.getMonth() == 7) {
                   month = "Aug";
               } else if (dt.getMonth() == 8) {
                   month = "Sep";
               } else if (dt.getMonth() == 9) {
                   month = "Oct";
               } else if (dt.getMonth() == 10) {
                   month = "Nov";
               } else if (dt.getMonth() == 11) {
                   month = "Dec";
               }
               short week = (short) (Math.round(dt.getDate() / 7));
               /*getrequestType is used to create a directory of the object type specified in the jsp page*/
               createPath = new File(createPath.getAbsolutePath() + "//MirageV2//" + String.valueOf(dt.getYear() + 1900) + "//" + month + "//" + String.valueOf(week));
               /*This creates a directory forcefully if the directory does not exsist*/
               //   System.err.println("absolute path...."+createPath.getAbsolutePath());
               //   System.err.println("createPath...."+createPath);
               createPath.mkdir();
               /*here it takes the absolute path and the name of the file that is to be uploaded*/
               File theFile = new File(createPath.getAbsolutePath() + "//" + fileName);
               //    System.err.println("the File...."+theFile);
               setFilepath(theFile.toString());
               /*copies the file to the destination*/
               //   System.err.println("Here..."+getUpload());
               FileUtils.copyFile(file, theFile);
               /**
                * To view the consultant details on consultantDetailsView.jsp
                * after attaching a resume of consultant.
                *
                * @see
                * com.mss.mirage.recruitment.consultantdetails.ConsultantVTO.
                */
               //setConsultantDetails( (ConsultantVTO)request.getSession(false).getAttribute("ConsultVTO"));
               resultType = SUCCESS;


           } catch (Exception ex) {
               //List errorMsgList = ExceptionToListUtility.errorMessages(ex);
               ex.printStackTrace();
               //System.err.println("In Action Class Catch"+ex.getMessage());
               //httpServletRequest.getSession(false).setAttribute("errorMessage",ex.toString());
               resultType = ERROR;
           }


       }//Close Session Checking
       return getFilepath();
   }
     
     public String downloadReleaseNotesAttachment() {
         try {
             resultType=INPUT;
             // this.setId(Integer.parseInt(httpServletRequest.getParameter("Id").toString()));

             // this.setAttachmentLocation(ServiceLocator.getProjIssuesService().getAttachmentLocation(this.getId()));
             // setResultMessage();
           
             String location = ServiceLocator.getAdminService().getReleaseNotesLocation(getSubId());
             getHttpServletResponse().setContentType("application/force-download");
       
             File file = new File(location);
             if(file.exists()){
                 
             
            String fileName = file.getName();
                 setInputStream(new FileInputStream(file));
                 setOutputStream(getHttpServletResponse().getOutputStream());
                 getHttpServletResponse().setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
             int noOfBytesRead = 0;
             byte[] byteArray = null;
             while (true) {
                 byteArray = new byte[1024];
                 noOfBytesRead = getInputStream().read(byteArray);
                 if (noOfBytesRead == -1) {
                     break;
                 }
                     getOutputStream().write(byteArray, 0, noOfBytesRead);
             }
         
         }
            else{
                 setResultMessage("<font color=\"red\" size=\"1.5\">Sorry! File Does not Exist!</font>");
                        httpServletRequest.setAttribute(ApplicationConstants.RESULT_MSG, getResultMessage());
                        setResultMessage(getResultMessage());
                     
             }
            
      }catch (FileNotFoundException ex) {
             ex.printStackTrace();
         } catch (IOException ex) {
             ex.printStackTrace();
         } catch (ServiceLocatorException ex) {
             ex.printStackTrace();
         } finally {
             try {
                 if(getInputStream()!=null){
                     getInputStream().close();
                     getOutputStream().close();
                 }
             } catch (IOException ex) {
                 ex.printStackTrace();
             }
         }
        
         return resultType;
     }
        
 
      
      /* Release Notes changes End*/   
      
      

     /**
      * This method is to get NewExecutiveDashboard
      * Created By: Triveni Niddara
      * Created Date : 11/5/2017
      * @return 
      */
     
     public String getNewExecutiveDashBoard(){
           resultType = LOGIN;
       /*
        *This if loop is to check whether there is Session or not
        **/
       if(httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null){
         // System.out.println("i am in"); 
           userRoleId = Integer.parseInt(httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_ROLE_ID).toString());
           resultType = "accessFailed";
           if(AuthorizationManager.getInstance().isAuthorizedUser("EXECUTIVE_DASHBOARD",userRoleId)){
               try{
                 setRolesMap((Map)httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_MY_ROLES));
                   resultType = SUCCESS;
               }catch (Exception ex){
                   httpServletRequest.getSession(false).setAttribute("errorMessage",ex.toString());
                   resultType =  ERROR;
               }
           }//END-Authorization Checking
           
       }//Close Session Checking
       return resultType;
     
    }    
      
     
     
      
    /**
     * Setters and Getters */
    
    public String getFirstName() {
        return firstName;
    }
    
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    
    
    public String getCurrStatus() {
        return currStatus;
    }
    
    public void setCurrStatus(String currStatus) {
        this.currStatus = currStatus;
    }
    
    
    public String getOrgId() {
        return orgId;
    }
    
    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }
    
    public String getDepartmentId() {
        return departmentId;
    }
    
    public void setDepartmentId(String departmentId) {
        this.departmentId = departmentId;
    }
    
    public String getWorkPhoneNo() {
        return workPhoneNo;
    }
    
    public void setWorkPhoneNo(String workPhoneNo) {
        this.workPhoneNo = workPhoneNo;
    }
    
    public List getCurrStatusList() {
        return currStatusList;
    }
    
    public void setCurrStatusList(List currStatusList) {
        this.currStatusList = currStatusList;
    }
    
    public List getOrgIdList() {
        return orgIdList;
    }
    
    public void setOrgIdList(List orgIdList) {
        this.orgIdList = orgIdList;
    }
    
    public List getDepartmentIdList() {
        return departmentIdList;
    }
    
    public void setDepartmentIdList(List departmentIdList) {
        this.departmentIdList = departmentIdList;
    }
    
    public int getId() {
        return id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    
    public void setServletRequest(HttpServletRequest httpServletRequest) {
        
        
        this.httpServletRequest = httpServletRequest;
        //Setting Attribute as   empRoleId of getEmpRoleId method.
        httpServletRequest.setAttribute("empRoleId",String.valueOf(getEmpRoleId()));
        
    }
    
    public void setEmpId(int empId){
        this.empId = empId;
    }
    
    public int getEmpId() {
        return empId;
    }
    
    public Map getPrimaryRolesList() {
        return primaryRolesList;
    }
    
    public void setPrimaryRolesList(Map primaryRolesList) {
        this.primaryRolesList = primaryRolesList;
    }
    
    public String getUserName() {
        return userName;
    }
    
    public void setUserName(String userName) {
        this.userName = userName;
    }
    
    public String getLoginId() {
        return loginId;
    }
    
    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }
    
    public List getAddedRolesList() {
        //   System.out.println("added"+addedRolesList);
        return addedRolesList;
    }
    
    public void setAddedRolesList(List addedRolesList) {
        this.addedRolesList = addedRolesList;
        
    }
    
    public void setParameters(Map parameters) {
        this.parameters=parameters;
    }
    
    public int getEmpRoleId() {
        return empRoleId;
    }
    
    public void setEmpRoleId(int empRoleId) {
        this.empRoleId = empRoleId;
    }
    
    public int getPrimaryRole() {
        return primaryRole;
    }
    
    public void setPrimaryRole(int primaryRole) {
        this.primaryRole = primaryRole;
    }
    
    public Map getAssignedRolesMap() {
        return assignedRolesMap;
    }
    
    public void setAssignedRolesMap(Map assignedRolesMap) {
        this.assignedRolesMap = assignedRolesMap;
    }
    
    public Map getNotAssignedRolesMap() {
        return notAssignedRolesMap;
    }
    
    public void setNotAssignedRolesMap(Map notAssignedRolesMap) {
        this.notAssignedRolesMap = notAssignedRolesMap;
    }
    
    public String getRoleName() {
        return RoleName;
    }
    
    public void setRoleName(String RoleName) {
        this.RoleName = RoleName;
    }
    
    public int getRoleId() {
        return roleId;
    }
    
    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }
    
    public Map getRoleScreenParameters() {
        return roleScreenParameters;
    }
    
    public void setRoleScreenParameters(Map roleScreenParameters) {
        this.roleScreenParameters = roleScreenParameters;
    }
    
    
    
    public List getRightSideRoleScreens() {
        //  System.out.println("Created List");
        return rightSideRoleScreens;
    }
    
    public void setRightSideRoleScreens(List rightSideRoleScreens) {
        this.rightSideRoleScreens = rightSideRoleScreens;
    }
    
    public String getModuleId() {
        return moduleId;
    }
    
    public void setModuleId(String moduleId) {
        this.moduleId = moduleId;
    }
    
    public String getScreenName() {
        return screenName;
    }
    
    public void setScreenName(String screenName) {
        this.screenName = screenName;
    }
    
    public String getScreenAction() {
        return screenAction;
    }
    
    public void setScreenAction(String screenAction) {
        this.screenAction = screenAction;
    }
    
    public String getScreenTitle() {
        return screenTitle;
    }
    
    public void setScreenTitle(String screenTitle) {
        this.screenTitle = screenTitle;
    }
    
    public Map getModuleMap() {
        return moduleMap;
    }
    
    public void setModuleMap(Map moduleMap) {
        this.moduleMap = moduleMap;
    }
    
    public Map getAssignedAllScreensMap() {
        return assignedAllScreensMap;
    }
    
    public void setAssignedAllScreensMap(Map assignedAllScreensMap) {
        this.assignedAllScreensMap = assignedAllScreensMap;
    }
    
    public Map getAssignedScreensMap() {
        return assignedScreensMap;
    }
    
    public void setAssignedScreensMap(Map assignedScreensMap) {
        this.assignedScreensMap = assignedScreensMap;
    }
    
    public AdminRolesVTO getAdminRoleVTO() {
        return adminRoleVTO;
    }
    
    public void setAdminRoleVTO(AdminRolesVTO adminRoleVTO) {
        this.adminRoleVTO = adminRoleVTO;
    }
    
    public String getSubmitFrom() {
        return submitFrom;
    }
    
    public void setSubmitFrom(String submitFrom) {
        this.submitFrom = submitFrom;
    }
    
    public String getNewPassword() {
        return newPassword;
    }
    
    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }
    
    public String getCnfPassword() {
        return cnfPassword;
    }
    
    public void setCnfPassword(String cnfPassword) {
        this.cnfPassword = cnfPassword;
    }
    
    public String getWorkingCountry() {
        return workingCountry;
    }
    
    public void setWorkingCountry(String workingCountry) {
        this.workingCountry = workingCountry;
    }
    
    public String getUserRoleName() {
        return userRoleName;
    }
    
    public void setUserRoleName(String userRoleName) {
        this.userRoleName = userRoleName;
    }
    
    public int getRoleStatus() {
        return roleStatus;
    }
    
    public void setRoleStatus(int roleStatus) {
        this.roleStatus = roleStatus;
    }

    /**
     * @return the opportunityStateList
     */
    public List getOpportunityStateList() {
        return opportunityStateList;
    }

    /**
     * @param opportunityStateList the opportunityStateList to set
     */
    public void setOpportunityStateList(List opportunityStateList) {
        this.opportunityStateList = opportunityStateList;
    }

    /**
     * @return the isAdminFlag
     */
    public String getIsAdminFlag() {
        return isAdminFlag;
    }

    /**
     * @param isAdminFlag the isAdminFlag to set
     */
    public void setIsAdminFlag(String isAdminFlag) {
        this.isAdminFlag = isAdminFlag;
    }

    /**
     * @return the urlImages
     */
    public String getUrlImages() {
        return urlImages;
    }

    /**
     * @param urlImages the urlImages to set
     */
    public void setUrlImages(String urlImages) {
        this.urlImages = urlImages;
    }

    /**
     * @return the urlNewsletters
     */
    public String getUrlNewsletters() {
        return urlNewsletters;
    }

    /**
     * @param urlNewsletters the urlNewsletters to set
     */
    public void setUrlNewsletters(String urlNewsletters) {
        this.urlNewsletters = urlNewsletters;
    }

    /**
     * @return the bdmMap
     */
    public Map getBdmMap() {
        return bdmMap;
    }

    /**
     * @param bdmMap the bdmMap to set
     */
    public void setBdmMap(Map bdmMap) {
        this.bdmMap = bdmMap;
    }

    /**
     * @return the customerName
     */
    public String getCustomerName() {
        return customerName;
    }

    /**
     * @param customerName the customerName to set
     */
    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    /**
     * @return the teamMemberList
     */
    public Map getTeamMemberList() {
        return teamMemberList;
    }

    /**
     * @param teamMemberList the teamMemberList to set
     */
    public void setTeamMemberList(Map teamMemberList) {
        this.teamMemberList = teamMemberList;
    }

    /**
     * @return the startDateContacts
     */
    public String getStartDateContacts() {
        return startDateContacts;
    }

    /**
     * @param startDateContacts the startDateContacts to set
     */
    public void setStartDateContacts(String startDateContacts) {
        this.startDateContacts = startDateContacts;
    }

    /**
     * @return the endDateContacts
     */
    public String getEndDateContacts() {
        return endDateContacts;
    }

    /**
     * @param endDateContacts the endDateContacts to set
     */
    public void setEndDateContacts(String endDateContacts) {
        this.endDateContacts = endDateContacts;
    }

    /**
     * @return the bdmId
     */
    public String getBdmId() {
        return bdmId;
    }

    /**
     * @param bdmId the bdmId to set
     */
    public void setBdmId(String bdmId) {
        this.bdmId = bdmId;
    }

    /**
     * @return the bdmName
     */
    public String getBdmName() {
        return bdmName;
    }

    /**
     * @param bdmName the bdmName to set
     */
    public void setBdmName(String bdmName) {
        this.bdmName = bdmName;
    }

    /**
     * @return the salesMap
     */
    public Map getSalesMap() {
        return salesMap;
    }

    /**
     * @param salesMap the salesMap to set
     */
    public void setSalesMap(Map salesMap) {
        this.salesMap = salesMap;
    }

    /**
     * @return the bdmTeamAssociateMap
     */
    public Map getBdmTeamAssociateMap() {
        return bdmTeamAssociateMap;
    }

    /**
     * @param bdmTeamAssociateMap the bdmTeamAssociateMap to set
     */
    public void setBdmTeamAssociateMap(Map bdmTeamAssociateMap) {
        this.bdmTeamAssociateMap = bdmTeamAssociateMap;
    }

    /**
     * @return the assignedBdmTeamMembers
     */
    public List getAssignedBdmTeamMembers() {
        return assignedBdmTeamMembers;
    }

    /**
     * @param assignedBdmTeamMembers the assignedBdmTeamMembers to set
     */
    public void setAssignedBdmTeamMembers(List assignedBdmTeamMembers) {
        this.assignedBdmTeamMembers = assignedBdmTeamMembers;
    }

    /**
     * @return the availableSalesTeamMembers
     */
    public List getAvailableSalesTeamMembers() {
        return availableSalesTeamMembers;
    }

    /**
     * @param availableSalesTeamMembers the availableSalesTeamMembers to set
     */
    public void setAvailableSalesTeamMembers(List availableSalesTeamMembers) {
        this.availableSalesTeamMembers = availableSalesTeamMembers;
    }

    /**
     * @return the assignedBdmTeam
     */
    public Map getAssignedBdmTeam() {
        return assignedBdmTeam;
    }

    /**
     * @param assignedBdmTeam the assignedBdmTeam to set
     */
    public void setAssignedBdmTeam(Map assignedBdmTeam) {
        this.assignedBdmTeam = assignedBdmTeam;
    }

    /**
     * @return the bdmParameters
     */
    public Map getBdmParameters() {
        return bdmParameters;
    }

    /**
     * @param bdmParameters the bdmParameters to set
     */
    public void setBdmParameters(Map bdmParameters) {
        this.bdmParameters = bdmParameters;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the preAssignSalesId
     */
    public String getPreAssignSalesId() {
        return preAssignSalesId;
    }

    /**
     * @param preAssignSalesId the preAssignSalesId to set
     */
    public void setPreAssignSalesId(String preAssignSalesId) {
        this.preAssignSalesId = preAssignSalesId;
    }

    /**
     * @return the responseString
     */
    public String getResponseString() {
        return responseString;
    }

    /**
     * @param responseString the responseString to set
     */
    public void setResponseString(String responseString) {
        this.responseString = responseString;
    }

    /**
     * @return the resultMessage
     */
    public String getResultMessage() {
        return resultMessage;
    }

    /**
     * @param resultMessage the resultMessage to set
     */
    public void setResultMessage(String resultMessage) {
        this.resultMessage = resultMessage;
    }


	public String getTableName() {
		return tableName;
	}


	public void setTableName(String tableName) {
		this.tableName = tableName;
	}


	public String getTableId() {
		return tableId;
	}


	public void setTableId(String tableId) {
		this.tableId = tableId;
	}
    
    
	 /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the fromDate
     */
    public String getFromDate() {
        return fromDate;
    }

    /**
     * @param fromDate the fromDate to set
     */
    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    /**
     * @return the toDate
     */
    public String getToDate() {
        return toDate;
    }

    /**
     * @param toDate the toDate to set
     */
    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    /**
     * @return the currentAction
     */
    public String getCurrentAction() {
        return currentAction;
    }

    /**
     * @param currentAction the currentAction to set
     */
    public void setCurrentAction(String currentAction) {
        this.currentAction = currentAction;
    }

    /**
     * @return the date
     */
    public Date getDate() {
        return date;
    }

    /**
     * @param date the date to set
     */
    public void setDate(Date date) {
        this.date = date;
    }

    /**
     * @return the subTitle0
     */
    public String getSubTitle0() {
        return subTitle0;
    }

    /**
     * @param subTitle0 the subTitle0 to set
     */
    public void setSubTitle0(String subTitle0) {
        this.subTitle0 = subTitle0;
    }

    /**
     * @return the purpose0
     */
    public String getPurpose0() {
        return purpose0;
    }

    /**
     * @param purpose0 the purpose0 to set
     */
    public void setPurpose0(String purpose0) {
        this.purpose0 = purpose0;
    }

    /**
     * @return the file0
     */
    public File getFile0() {
        return file0;
    }

    /**
     * @param file0 the file0 to set
     */
    public void setFile0(File file0) {
        this.file0 = file0;
    }

    /**
     * @return the subTitle1
     */
    public String getSubTitle1() {
        return subTitle1;
    }

    /**
     * @param subTitle1 the subTitle1 to set
     */
    public void setSubTitle1(String subTitle1) {
        this.subTitle1 = subTitle1;
    }

    /**
     * @return the purpose1
     */
    public String getPurpose1() {
        return purpose1;
    }

    /**
     * @param purpose1 the purpose1 to set
     */
    public void setPurpose1(String purpose1) {
        this.purpose1 = purpose1;
    }

    /**
     * @return the file1
     */
    public File getFile1() {
        return file1;
    }

    /**
     * @param file1 the file1 to set
     */
    public void setFile1(File file1) {
        this.file1 = file1;
    }

    /**
     * @return the subTitle2
     */
    public String getSubTitle2() {
        return subTitle2;
    }

    /**
     * @param subTitle2 the subTitle2 to set
     */
    public void setSubTitle2(String subTitle2) {
        this.subTitle2 = subTitle2;
    }

    /**
     * @return the purpose2
     */
    public String getPurpose2() {
        return purpose2;
    }

    /**
     * @param purpose2 the purpose2 to set
     */
    public void setPurpose2(String purpose2) {
        this.purpose2 = purpose2;
    }

    /**
     * @return the file2
     */
    public File getFile2() {
        return file2;
    }

    /**
     * @param file2 the file2 to set
     */
    public void setFile2(File file2) {
        this.file2 = file2;
    }

    /**
     * @return the subTitle3
     */
    public String getSubTitle3() {
        return subTitle3;
    }

    /**
     * @param subTitle3 the subTitle3 to set
     */
    public void setSubTitle3(String subTitle3) {
        this.subTitle3 = subTitle3;
    }

    /**
     * @return the purpose3
     */
    public String getPurpose3() {
        return purpose3;
    }

    /**
     * @param purpose3 the purpose3 to set
     */
    public void setPurpose3(String purpose3) {
        this.purpose3 = purpose3;
    }

    /**
     * @return the file3
     */
    public File getFile3() {
        return file3;
    }

    /**
     * @param file3 the file3 to set
     */
    public void setFile3(File file3) {
        this.file3 = file3;
    }

    /**
     * @return the subTitle4
     */
    public String getSubTitle4() {
        return subTitle4;
    }

    /**
     * @param subTitle4 the subTitle4 to set
     */
    public void setSubTitle4(String subTitle4) {
        this.subTitle4 = subTitle4;
    }

    /**
     * @return the purpose4
     */
    public String getPurpose4() {
        return purpose4;
    }

    /**
     * @param purpose4 the purpose4 to set
     */
    public void setPurpose4(String purpose4) {
        this.purpose4 = purpose4;
    }

    /**
     * @return the file4
     */
    public File getFile4() {
        return file4;
    }

    /**
     * @param file4 the file4 to set
     */
    public void setFile4(File file4) {
        this.file4 = file4;
    }

    /**
     * @return the subTitle5
     */
    public String getSubTitle5() {
        return subTitle5;
    }

    /**
     * @param subTitle5 the subTitle5 to set
     */
    public void setSubTitle5(String subTitle5) {
        this.subTitle5 = subTitle5;
    }

    /**
     * @return the purpose5
     */
    public String getPurpose5() {
        return purpose5;
    }

    /**
     * @param purpose5 the purpose5 to set
     */
    public void setPurpose5(String purpose5) {
        this.purpose5 = purpose5;
    }

    /**
     * @return the file5
     */
    public File getFile5() {
        return file5;
    }

    /**
     * @param file5 the file5 to set
     */
    public void setFile5(File file5) {
        this.file5 = file5;
    }

    /**
     * @return the fileContentType
     */
    public String getFileContentType() {
        return fileContentType;
    }

    /**
     * @param fileContentType the fileContentType to set
     */
    public void setFileContentType(String fileContentType) {
        this.fileContentType = fileContentType;
    }

    /**
     * @return the file0FileName
     */
    public String getFile0FileName() {
        return file0FileName;
    }

    /**
     * @param file0FileName the file0FileName to set
     */
    public void setFile0FileName(String file0FileName) {
        this.file0FileName = file0FileName;
    }

    /**
     * @return the file1FileName
     */
    public String getFile1FileName() {
        return file1FileName;
    }

    /**
     * @param file1FileName the file1FileName to set
     */
    public void setFile1FileName(String file1FileName) {
        this.file1FileName = file1FileName;
    }

    /**
     * @return the file2FileName
     */
    public String getFile2FileName() {
        return file2FileName;
    }

    /**
     * @param file2FileName the file2FileName to set
     */
    public void setFile2FileName(String file2FileName) {
        this.file2FileName = file2FileName;
    }

    /**
     * @return the file3FileName
     */
    public String getFile3FileName() {
        return file3FileName;
    }

    /**
     * @param file3FileName the file3FileName to set
     */
    public void setFile3FileName(String file3FileName) {
        this.file3FileName = file3FileName;
    }

    /**
     * @return the file4FileName
     */
    public String getFile4FileName() {
        return file4FileName;
    }

    /**
     * @param file4FileName the file4FileName to set
     */
    public void setFile4FileName(String file4FileName) {
        this.file4FileName = file4FileName;
    }

    /**
     * @return the file5FileName
     */
    public String getFile5FileName() {
        return file5FileName;
    }

    /**
     * @param file5FileName the file5FileName to set
     */
    public void setFile5FileName(String file5FileName) {
        this.file5FileName = file5FileName;
    }

    /**
     * @return the filepath
     */
    public String getFilepath() {
        return filepath;
    }

    /**
     * @param filepath the filepath to set
     */
    public void setFilepath(String filepath) {
        this.filepath = filepath;
    }

    /**
     * @return the isInsertedValueCount
     */
    public int getIsInsertedValueCount() {
        return isInsertedValueCount;
    }

    /**
     * @param isInsertedValueCount the isInsertedValueCount to set
     */
    public void setIsInsertedValueCount(int isInsertedValueCount) {
        this.isInsertedValueCount = isInsertedValueCount;
    }

    /**
     * @return the releaseId
     */
    public int getReleaseId() {
        return releaseId;
    }

    /**
     * @param releaseId the releaseId to set
     */
    public void setReleaseId(int releaseId) {
        this.releaseId = releaseId;
    }

    /**
     * @return the count
     */
    public int getCount() {
        return count;
    }

    /**
     * @param count the count to set
     */
    public void setCount(int count) {
        this.count = count;
    }

    /**
     * @return the subId0
     */
    public String getSubId0() {
        return subId0;
    }

    /**
     * @param subId0 the subId0 to set
     */
    public void setSubId0(String subId0) {
        this.subId0 = subId0;
    }

    /**
     * @return the subId1
     */
    public String getSubId1() {
        return subId1;
    }

    /**
     * @param subId1 the subId1 to set
     */
    public void setSubId1(String subId1) {
        this.subId1 = subId1;
    }

    /**
     * @return the subId2
     */
    public String getSubId2() {
        return subId2;
    }

    /**
     * @param subId2 the subId2 to set
     */
    public void setSubId2(String subId2) {
        this.subId2 = subId2;
    }

    /**
     * @return the subId3
     */
    public String getSubId3() {
        return subId3;
    }

    /**
     * @param subId3 the subId3 to set
     */
    public void setSubId3(String subId3) {
        this.subId3 = subId3;
    }

    /**
     * @return the subId4
     */
    public String getSubId4() {
        return subId4;
    }

    /**
     * @param subId4 the subId4 to set
     */
    public void setSubId4(String subId4) {
        this.subId4 = subId4;
    }

    /**
     * @return the subId5
     */
    public String getSubId5() {
        return subId5;
    }

    /**
     * @param subId5 the subId5 to set
     */
    public void setSubId5(String subId5) {
        this.subId5 = subId5;
    }

    /**
     * @return the isUpdatedValueCount
     */
    public int getIsUpdatedValueCount() {
        return isUpdatedValueCount;
    }

    /**
     * @param isUpdatedValueCount the isUpdatedValueCount to set
     */
    public void setIsUpdatedValueCount(int isUpdatedValueCount) {
        this.isUpdatedValueCount = isUpdatedValueCount;
    }

    /**
     * @return the minusHiden
     */
    public String getMinusHiden() {
        return minusHiden;
    }

    /**
     * @param minusHiden the minusHiden to set
     */
    public void setMinusHiden(String minusHiden) {
        this.minusHiden = minusHiden;
    }

    /**
     * @return the titleSearch
     */
    public String getTitleSearch() {
        return titleSearch;
    }

    /**
     * @param titleSearch the titleSearch to set
     */
    public void setTitleSearch(String titleSearch) {
        this.titleSearch = titleSearch;
    }

    /**
     * @return the file0FilePath
     */
    public String getFile0FilePath() {
        return file0FilePath;
    }

    /**
     * @param file0FilePath the file0FilePath to set
     */
    public void setFile0FilePath(String file0FilePath) {
        this.file0FilePath = file0FilePath;
    }

    /**
     * @return the file1FilePath
     */
    public String getFile1FilePath() {
        return file1FilePath;
    }

    /**
     * @param file1FilePath the file1FilePath to set
     */
    public void setFile1FilePath(String file1FilePath) {
        this.file1FilePath = file1FilePath;
    }

    /**
     * @return the file2FilePath
     */
    public String getFile2FilePath() {
        return file2FilePath;
    }

    /**
     * @param file2FilePath the file2FilePath to set
     */
    public void setFile2FilePath(String file2FilePath) {
        this.file2FilePath = file2FilePath;
    }

    /**
     * @return the file3FilePath
     */
    public String getFile3FilePath() {
        return file3FilePath;
    }

    /**
     * @param file3FilePath the file3FilePath to set
     */
    public void setFile3FilePath(String file3FilePath) {
        this.file3FilePath = file3FilePath;
    }

    /**
     * @return the file4FilePath
     */
    public String getFile4FilePath() {
        return file4FilePath;
    }

    /**
     * @param file4FilePath the file4FilePath to set
     */
    public void setFile4FilePath(String file4FilePath) {
        this.file4FilePath = file4FilePath;
    }

    /**
     * @return the file5FilePath
     */
    public String getFile5FilePath() {
        return file5FilePath;
    }

    /**
     * @param file5FilePath the file5FilePath to set
     */
    public void setFile5FilePath(String file5FilePath) {
        this.file5FilePath = file5FilePath;
    }

    /**
     * @return the httpServletResponse
     */
    public HttpServletResponse getHttpServletResponse() {
        return httpServletResponse;
    }

    /**
     * @param httpServletResponse the httpServletResponse to set
     */
    public void setHttpServletResponse(HttpServletResponse httpServletResponse) {
        this.httpServletResponse = httpServletResponse;
    }

    /**
     * @return the inputStream
     */
    public InputStream getInputStream() {
        return inputStream;
    }

    /**
     * @param inputStream the inputStream to set
     */
    public void setInputStream(InputStream inputStream) {
        this.inputStream = inputStream;
    }

    /**
     * @return the outputStream
     */
    public OutputStream getOutputStream() {
        return outputStream;
    }

    /**
     * @param outputStream the outputStream to set
     */
    public void setOutputStream(OutputStream outputStream) {
        this.outputStream = outputStream;
    }

    /**
     * @return the subId
     */
    public String getSubId() {
        return subId;
    }

    /**
     * @param subId the subId to set
     */
    public void setSubId(String subId) {
        this.subId = subId;
    }
    
    
     public void setServletResponse(HttpServletResponse httpServletResponse) {
        this.httpServletResponse = httpServletResponse;

    }

    /**
     * @return the filex1FilePath
     */
    public String getFilex1FilePath() {
        return filex1FilePath;
    }

    /**
     * @param filex1FilePath the filex1FilePath to set
     */
    public void setFilex1FilePath(String filex1FilePath) {
        this.filex1FilePath = filex1FilePath;
    }

    /**
     * @return the filex2FilePath
     */
    public String getFilex2FilePath() {
        return filex2FilePath;
    }

    /**
     * @param filex2FilePath the filex2FilePath to set
     */
    public void setFilex2FilePath(String filex2FilePath) {
        this.filex2FilePath = filex2FilePath;
    }

    /**
     * @return the filex3FilePath
     */
    public String getFilex3FilePath() {
        return filex3FilePath;
    }

    /**
     * @param filex3FilePath the filex3FilePath to set
     */
    public void setFilex3FilePath(String filex3FilePath) {
        this.filex3FilePath = filex3FilePath;
    }

    /**
     * @return the filex4FilePath
     */
    public String getFilex4FilePath() {
        return filex4FilePath;
    }

    /**
     * @param filex4FilePath the filex4FilePath to set
     */
    public void setFilex4FilePath(String filex4FilePath) {
        this.filex4FilePath = filex4FilePath;
    }

    /**
     * @return the filex5FilePath
     */
    public String getFilex5FilePath() {
        return filex5FilePath;
    }

    /**
     * @param filex5FilePath the filex5FilePath to set
     */
    public void setFilex5FilePath(String filex5FilePath) {
        this.filex5FilePath = filex5FilePath;
    }


	public List getRoleDetails() {
		return roleDetails;
	}


	public void setRoleDetails(List roleDetails) {
		this.roleDetails = roleDetails;
	}


	public String getRoleAccess() {
		return roleAccess;
	}


	public void setRoleAccess(String roleAccess) {
		this.roleAccess = roleAccess;
	}


	public List getRoleDetailsList() {
		return roleDetailsList;
	}


	public void setRoleDetailsList(List roleDetailsList) {
		this.roleDetailsList = roleDetailsList;
	}


	public Map getRolesMap() {
		return rolesMap;
	}


	public void setRolesMap(Map rolesMap) {
		this.rolesMap = rolesMap;
	}


	public String getReqStartDate() {
		return reqStartDate;
	}


	public void setReqStartDate(String reqStartDate) {
		this.reqStartDate = reqStartDate;
	}


	public String getReqEndDate() {
		return reqEndDate;
	}


	public void setReqEndDate(String reqEndDate) {
		this.reqEndDate = reqEndDate;
	}


	public Map getBdmMapReq() {
		return bdmMapReq;
	}


	public void setBdmMapReq(Map bdmMapReq) {
		this.bdmMapReq = bdmMapReq;
	}  
    
    
}
