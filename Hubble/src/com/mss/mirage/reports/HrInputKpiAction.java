package com.mss.mirage.reports;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
//import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import com.mss.mirage.ajax.AjaxHandlerAction;
import com.mss.mirage.util.ApplicationConstants;
import com.mss.mirage.util.AuthorizationManager;
import com.mss.mirage.util.ConnectionProvider;
import com.mss.mirage.util.ServiceLocatorException;
import com.opensymphony.xwork2.ActionSupport;


public class HrInputKpiAction extends ActionSupport {

	
	/*hr input data start*/
    private int schdCalls1;
    private int callsExe1;
    private int meetingsScheduled1;
   private int meetingsExecuted1;
   private int  opprPunched1; 
   private int  requirementspunched1;
   private int  quotesProcured_Renewals1;
   private int  quotesSubmitted_Renewals1;
   private int  softwareRenewalsProcured_Renewals1;
   private int  softwareNewLicenseProcured_Renewals1;
   private int  quotesProcured_Federal1;
   private int  quotesSubmitted_Federal1 ;
   private int  softwareRenewalsProcured_Federal1;
   private int  softwareNewLicenseProcured_Federal1;
   private int schdCalls2;
   private int callsExe2;
   private int meetingsScheduled2;
  private int meetingsExecuted2;
  private int  opprPunched2; 
  private int  requirementspunched2;
  private int  quotesProcured_Renewals2;
  private int  quotesSubmitted_Renewals2;
  private int  softwareRenewalsProcured_Renewals2;
  private int  softwareNewLicenseProcured_Renewals2;
  private int  quotesProcured_Federal2;
  private int  quotesSubmitted_Federal2 ;
  private int  softwareRenewalsProcured_Federal2;
  private int  softwareNewLicenseProcured_Federal2;
  private int schdCalls3;
  private int callsExe3;
  private int meetingsScheduled3;
 private int meetingsExecuted3;
 private int  opprPunched3; 
 private int  requirementspunched3;
 private int  quotesProcured_Renewals3;
 private int  quotesSubmitted_Renewals3;
 private int  softwareRenewalsProcured_Renewals3;
 private int  softwareNewLicenseProcured_Renewals3;
 private int  quotesProcured_Federal3;
 private int  quotesSubmitted_Federal3 ;
 private int  softwareRenewalsProcured_Federal3;
 private int  softwareNewLicenseProcured_Federal3;
 private int schdCalls4;
 private int callsExe4;
 private int meetingsScheduled4;
private int meetingsExecuted4;
private int  opprPunched4; 
private int  requirementspunched4;
private int  quotesProcured_Renewals4;
private int  quotesSubmitted_Renewals4;
private int  softwareRenewalsProcured_Renewals4;
private int  softwareNewLicenseProcured_Renewals4;
private int  quotesProcured_Federal4;
private int  quotesSubmitted_Federal4 ;
private int  softwareRenewalsProcured_Federal4;
private int  softwareNewLicenseProcured_Federal4;
private HttpServletRequest httpServletRequest;
private String resultType;
private HttpSession httpSession;
private int userRoleId;

private int quotesProcured_Federal11;
private int quotesProcured_Federal22;
private int quotesProcured_Federal33;
private int quotesProcured_Federal44;
private String resultMessage;
private String userId;

public String getUserId() {
	return userId;
}


public void setUserId(String userId) {
	this.userId = userId;
}


public final int getSchdCalls1() {
	return schdCalls1;
}


public final void setSchdCalls1(int schdCalls1) {
	this.schdCalls1 = schdCalls1;
}


public final int getCallsExe1() {
	return callsExe1;
}


public final void setCallsExe1(int callsExe1) {
	this.callsExe1 = callsExe1;
}


public final int getMeetingsScheduled1() {
	return meetingsScheduled1;
}


public final void setMeetingsScheduled1(int meetingsScheduled1) {
	this.meetingsScheduled1 = meetingsScheduled1;
}


public final int getMeetingsExecuted1() {
	return meetingsExecuted1;
}


public final void setMeetingsExecuted1(int meetingsExecuted1) {
	this.meetingsExecuted1 = meetingsExecuted1;
}


public final int getOpprPunched1() {
	return opprPunched1;
}


public final void setOpprPunched1(int opprPunched1) {
	this.opprPunched1 = opprPunched1;
}


public final int getRequirementspunched1() {
	return requirementspunched1;
}


public final void setRequirementspunched1(int requirementspunched1) {
	this.requirementspunched1 = requirementspunched1;
}


public final int getQuotesProcured_Renewals1() {
	return quotesProcured_Renewals1;
}


public final void setQuotesProcured_Renewals1(int quotesProcured_Renewals1) {
	this.quotesProcured_Renewals1 = quotesProcured_Renewals1;
}


public final int getQuotesSubmitted_Renewals1() {
	return quotesSubmitted_Renewals1;
}


public final void setQuotesSubmitted_Renewals1(int quotesSubmitted_Renewals1) {
	this.quotesSubmitted_Renewals1 = quotesSubmitted_Renewals1;
}


public final int getSoftwareRenewalsProcured_Renewals1() {
	return softwareRenewalsProcured_Renewals1;
}


public final void setSoftwareRenewalsProcured_Renewals1(int softwareRenewalsProcured_Renewals1) {
	this.softwareRenewalsProcured_Renewals1 = softwareRenewalsProcured_Renewals1;
}


public final int getSoftwareNewLicenseProcured_Renewals1() {
	return softwareNewLicenseProcured_Renewals1;
}


public final void setSoftwareNewLicenseProcured_Renewals1(int softwareNewLicenseProcured_Renewals1) {
	this.softwareNewLicenseProcured_Renewals1 = softwareNewLicenseProcured_Renewals1;
}


public final int getQuotesProcured_Federal1() {
	return quotesProcured_Federal1;
}


public final void setQuotesProcured_Federal1(int quotesProcured_Federal1) {
	this.quotesProcured_Federal1 = quotesProcured_Federal1;
}


public final int getQuotesSubmitted_Federal1() {
	return quotesSubmitted_Federal1;
}


public final void setQuotesSubmitted_Federal1(int quotesSubmitted_Federal1) {
	this.quotesSubmitted_Federal1 = quotesSubmitted_Federal1;
}


public final int getSoftwareRenewalsProcured_Federal1() {
	return softwareRenewalsProcured_Federal1;
}


public final void setSoftwareRenewalsProcured_Federal1(int softwareRenewalsProcured_Federal1) {
	this.softwareRenewalsProcured_Federal1 = softwareRenewalsProcured_Federal1;
}


public final int getSoftwareNewLicenseProcured_Federal1() {
	return softwareNewLicenseProcured_Federal1;
}


public final void setSoftwareNewLicenseProcured_Federal1(int softwareNewLicenseProcured_Federal1) {
	this.softwareNewLicenseProcured_Federal1 = softwareNewLicenseProcured_Federal1;
}


public final int getSchdCalls2() {
	return schdCalls2;
}


public int getQuotesProcured_Federal11() {
	return quotesProcured_Federal11;
}


public void setQuotesProcured_Federal11(int quotesProcured_Federal11) {
	this.quotesProcured_Federal11 = quotesProcured_Federal11;
}


public int getQuotesProcured_Federal22() {
	return quotesProcured_Federal22;
}


public void setQuotesProcured_Federal22(int quotesProcured_Federal22) {
	this.quotesProcured_Federal22 = quotesProcured_Federal22;
}


public int getQuotesProcured_Federal33() {
	return quotesProcured_Federal33;
}


public void setQuotesProcured_Federal33(int quotesProcured_Federal33) {
	this.quotesProcured_Federal33 = quotesProcured_Federal33;
}


public int getQuotesProcured_Federal44() {
	return quotesProcured_Federal44;
}


public void setQuotesProcured_Federal44(int quotesProcured_Federal44) {
	this.quotesProcured_Federal44 = quotesProcured_Federal44;
}


public final void setSchdCalls2(int schdCalls2) {
	this.schdCalls2 = schdCalls2;
}


public final int getCallsExe2() {
	return callsExe2;
}


public final void setCallsExe2(int callsExe2) {
	this.callsExe2 = callsExe2;
}


public final int getMeetingsScheduled2() {
	return meetingsScheduled2;
}


public final void setMeetingsScheduled2(int meetingsScheduled2) {
	this.meetingsScheduled2 = meetingsScheduled2;
}


public final int getMeetingsExecuted2() {
	return meetingsExecuted2;
}


public final void setMeetingsExecuted2(int meetingsExecuted2) {
	this.meetingsExecuted2 = meetingsExecuted2;
}


public final int getOpprPunched2() {
	return opprPunched2;
}


public final void setOpprPunched2(int opprPunched2) {
	this.opprPunched2 = opprPunched2;
}


public final int getRequirementspunched2() {
	return requirementspunched2;
}


public final void setRequirementspunched2(int requirementspunched2) {
	this.requirementspunched2 = requirementspunched2;
}


public final int getQuotesProcured_Renewals2() {
	return quotesProcured_Renewals2;
}


public final void setQuotesProcured_Renewals2(int quotesProcured_Renewals2) {
	this.quotesProcured_Renewals2 = quotesProcured_Renewals2;
}


public final int getQuotesSubmitted_Renewals2() {
	return quotesSubmitted_Renewals2;
}


public final void setQuotesSubmitted_Renewals2(int quotesSubmitted_Renewals2) {
	this.quotesSubmitted_Renewals2 = quotesSubmitted_Renewals2;
}


public final int getSoftwareRenewalsProcured_Renewals2() {
	return softwareRenewalsProcured_Renewals2;
}


public final void setSoftwareRenewalsProcured_Renewals2(int softwareRenewalsProcured_Renewals2) {
	this.softwareRenewalsProcured_Renewals2 = softwareRenewalsProcured_Renewals2;
}


public final int getSoftwareNewLicenseProcured_Renewals2() {
	return softwareNewLicenseProcured_Renewals2;
}


public final void setSoftwareNewLicenseProcured_Renewals2(int softwareNewLicenseProcured_Renewals2) {
	this.softwareNewLicenseProcured_Renewals2 = softwareNewLicenseProcured_Renewals2;
}


public final int getQuotesProcured_Federal2() {
	return quotesProcured_Federal2;
}


public final void setQuotesProcured_Federal2(int quotesProcured_Federal2) {
	this.quotesProcured_Federal2 = quotesProcured_Federal2;
}


public final int getQuotesSubmitted_Federal2() {
	return quotesSubmitted_Federal2;
}


public final void setQuotesSubmitted_Federal2(int quotesSubmitted_Federal2) {
	this.quotesSubmitted_Federal2 = quotesSubmitted_Federal2;
}


public final int getSoftwareRenewalsProcured_Federal2() {
	return softwareRenewalsProcured_Federal2;
}


public final void setSoftwareRenewalsProcured_Federal2(int softwareRenewalsProcured_Federal2) {
	this.softwareRenewalsProcured_Federal2 = softwareRenewalsProcured_Federal2;
}


public final int getSoftwareNewLicenseProcured_Federal2() {
	return softwareNewLicenseProcured_Federal2;
}


public final void setSoftwareNewLicenseProcured_Federal2(int softwareNewLicenseProcured_Federal2) {
	this.softwareNewLicenseProcured_Federal2 = softwareNewLicenseProcured_Federal2;
}


public final int getSchdCalls3() {
	return schdCalls3;
}


public final void setSchdCalls3(int schdCalls3) {
	this.schdCalls3 = schdCalls3;
}


public final int getCallsExe3() {
	return callsExe3;
}


public final void setCallsExe3(int callsExe3) {
	this.callsExe3 = callsExe3;
}


public final int getMeetingsScheduled3() {
	return meetingsScheduled3;
}


public final void setMeetingsScheduled3(int meetingsScheduled3) {
	this.meetingsScheduled3 = meetingsScheduled3;
}


public final int getMeetingsExecuted3() {
	return meetingsExecuted3;
}


public final void setMeetingsExecuted3(int meetingsExecuted3) {
	this.meetingsExecuted3 = meetingsExecuted3;
}


public final int getOpprPunched3() {
	return opprPunched3;
}


public final void setOpprPunched3(int opprPunched3) {
	this.opprPunched3 = opprPunched3;
}


public final int getRequirementspunched3() {
	return requirementspunched3;
}


public final void setRequirementspunched3(int requirementspunched3) {
	this.requirementspunched3 = requirementspunched3;
}


public final int getQuotesProcured_Renewals3() {
	return quotesProcured_Renewals3;
}


public final void setQuotesProcured_Renewals3(int quotesProcured_Renewals3) {
	this.quotesProcured_Renewals3 = quotesProcured_Renewals3;
}


public final int getQuotesSubmitted_Renewals3() {
	return quotesSubmitted_Renewals3;
}


public final void setQuotesSubmitted_Renewals3(int quotesSubmitted_Renewals3) {
	this.quotesSubmitted_Renewals3 = quotesSubmitted_Renewals3;
}


public final int getSoftwareRenewalsProcured_Renewals3() {
	return softwareRenewalsProcured_Renewals3;
}


public final void setSoftwareRenewalsProcured_Renewals3(int softwareRenewalsProcured_Renewals3) {
	this.softwareRenewalsProcured_Renewals3 = softwareRenewalsProcured_Renewals3;
}


public final int getSoftwareNewLicenseProcured_Renewals3() {
	return softwareNewLicenseProcured_Renewals3;
}


public final void setSoftwareNewLicenseProcured_Renewals3(int softwareNewLicenseProcured_Renewals3) {
	this.softwareNewLicenseProcured_Renewals3 = softwareNewLicenseProcured_Renewals3;
}


public final int getQuotesProcured_Federal3() {
	return quotesProcured_Federal3;
}


public final void setQuotesProcured_Federal3(int quotesProcured_Federal3) {
	this.quotesProcured_Federal3 = quotesProcured_Federal3;
}


public final int getQuotesSubmitted_Federal3() {
	return quotesSubmitted_Federal3;
}


public final void setQuotesSubmitted_Federal3(int quotesSubmitted_Federal3) {
	this.quotesSubmitted_Federal3 = quotesSubmitted_Federal3;
}


public final int getSoftwareRenewalsProcured_Federal3() {
	return softwareRenewalsProcured_Federal3;
}


public final void setSoftwareRenewalsProcured_Federal3(int softwareRenewalsProcured_Federal3) {
	this.softwareRenewalsProcured_Federal3 = softwareRenewalsProcured_Federal3;
}


public final int getSoftwareNewLicenseProcured_Federal3() {
	return softwareNewLicenseProcured_Federal3;
}


public final void setSoftwareNewLicenseProcured_Federal3(int softwareNewLicenseProcured_Federal3) {
	this.softwareNewLicenseProcured_Federal3 = softwareNewLicenseProcured_Federal3;
}


public final int getSchdCalls4() {
	return schdCalls4;
}


public final void setSchdCalls4(int schdCalls4) {
	this.schdCalls4 = schdCalls4;
}


public final int getCallsExe4() {
	return callsExe4;
}


public final void setCallsExe4(int callsExe4) {
	this.callsExe4 = callsExe4;
}


public final int getMeetingsScheduled4() {
	return meetingsScheduled4;
}


public final void setMeetingsScheduled4(int meetingsScheduled4) {
	this.meetingsScheduled4 = meetingsScheduled4;
}


public final int getMeetingsExecuted4() {
	return meetingsExecuted4;
}


public final void setMeetingsExecuted4(int meetingsExecuted4) {
	this.meetingsExecuted4 = meetingsExecuted4;
}


public final int getOpprPunched4() {
	return opprPunched4;
}


public final void setOpprPunched4(int opprPunched4) {
	this.opprPunched4 = opprPunched4;
}


public final int getRequirementspunched4() {
	return requirementspunched4;
}


public final void setRequirementspunched4(int requirementspunched4) {
	this.requirementspunched4 = requirementspunched4;
}


public final int getQuotesProcured_Renewals4() {
	return quotesProcured_Renewals4;
}


public final void setQuotesProcured_Renewals4(int quotesProcured_Renewals4) {
	this.quotesProcured_Renewals4 = quotesProcured_Renewals4;
}


public final int getQuotesSubmitted_Renewals4() {
	return quotesSubmitted_Renewals4;
}


public final void setQuotesSubmitted_Renewals4(int quotesSubmitted_Renewals4) {
	this.quotesSubmitted_Renewals4 = quotesSubmitted_Renewals4;
}


public final int getSoftwareRenewalsProcured_Renewals4() {
	return softwareRenewalsProcured_Renewals4;
}


public final void setSoftwareRenewalsProcured_Renewals4(int softwareRenewalsProcured_Renewals4) {
	this.softwareRenewalsProcured_Renewals4 = softwareRenewalsProcured_Renewals4;
}


public final int getSoftwareNewLicenseProcured_Renewals4() {
	return softwareNewLicenseProcured_Renewals4;
}


public final void setSoftwareNewLicenseProcured_Renewals4(int softwareNewLicenseProcured_Renewals4) {
	this.softwareNewLicenseProcured_Renewals4 = softwareNewLicenseProcured_Renewals4;
}


public final int getQuotesProcured_Federal4() {
	return quotesProcured_Federal4;
}


public final void setQuotesProcured_Federal4(int quotesProcured_Federal4) {
	this.quotesProcured_Federal4 = quotesProcured_Federal4;
}


public final int getQuotesSubmitted_Federal4() {
	return quotesSubmitted_Federal4;
}


public final void setQuotesSubmitted_Federal4(int quotesSubmitted_Federal4) {
	this.quotesSubmitted_Federal4 = quotesSubmitted_Federal4;
}


public final int getSoftwareRenewalsProcured_Federal4() {
	return softwareRenewalsProcured_Federal4;
}


public final void setSoftwareRenewalsProcured_Federal4(int softwareRenewalsProcured_Federal4) {
	this.softwareRenewalsProcured_Federal4 = softwareRenewalsProcured_Federal4;
}


public final int getSoftwareNewLicenseProcured_Federal4() {
	return softwareNewLicenseProcured_Federal4;
}


public final void setSoftwareNewLicenseProcured_Federal4(int softwareNewLicenseProcured_Federal4) {
	this.softwareNewLicenseProcured_Federal4 = softwareNewLicenseProcured_Federal4;
}


public final HttpServletRequest getHttpServletRequest() {
	return httpServletRequest;
}


public final void setHttpServletRequest(HttpServletRequest httpServletRequest) {
	this.httpServletRequest = httpServletRequest;
}


public final String getResultType() {
	return resultType;
}


public final void setResultType(String resultType) {
	this.resultType = resultType;
}


public final HttpSession getHttpSession() {
	return httpSession;
}


public final void setHttpSession(HttpSession httpSession) {
	this.httpSession = httpSession;
}


public final int getUserRoleId() {
	return userRoleId;
}


public final void setUserRoleId(int userRoleId) {
	this.userRoleId = userRoleId;
}


public String updatedatabyhr() {
	
//	System.out.println("updatedatabyhr");
  //  resultType = LOGIN;
 //   httpSession = httpServletRequest.getSession(false);
  //  if (httpSession.getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
     //   userRoleId = Integer.parseInt(httpSession.getAttribute(ApplicationConstants.SESSION_ROLE_ID).toString());
     //   resultType = "accessFailed";
      int   SchdCalls = 0;
       int  CallsExe = 0;
       int MeetingsScheduled=0;
      int  MeetingsExecuted=0;
      int  OpprPunched=0 ;
      int  Requirementspunched=0;
      int  QuotesProcured_Renewals=0;
      int  QuotesSubmitted_Renewals=0;
       int SoftwareRenewalsProcured_Renewals=0;
     int   SoftwareNewLicenseProcured_Renewals=0;
     int   QuotesProcured_Federal=0;
      int  QuotesSubmitted_Federal=0 ;
      int  SoftwareRenewalsProcured_Federal=0;
       int SoftwareNewLicenseProcured_Federal=0;

   //	System.out.println("getSchdCalls1"+getSchdCalls1());

           
            	Connection connection = null;

            	CallableStatement callableStatement = null;
            	HashMap map = null;

            //	String result=LOGIN;


            	try {



            	 

            	 String resourcetype[]={"Sales Trainee","Jr.BDE","BDE","Sr.BDE"};
for(int i=0;i<resourcetype.length;i++){
	  String SQL_QUERY ="SELECT * FROM tblLkpSalesKPIHRInputs WHERE ResourceType='"+resourcetype[i]+"'";
	// System.out.println("SQL_QUERY"+SQL_QUERY);
          connection = ConnectionProvider.getInstance().getConnection();
      //    System.out.println("connected11");
          
          Statement statement = connection.createStatement();
          ResultSet resultSet = statement.executeQuery(SQL_QUERY);
          if(resultSet.next()) {
        	  SchdCalls= resultSet.getInt("SchdCalls");
         //     System.out.println("SchdCalls"+SchdCalls);
        	 
        	  CallsExe =resultSet.getInt("CallsExe");
        	  MeetingsScheduled =resultSet.getInt("MeetingsScheduled");
        	  MeetingsExecuted = resultSet.getInt("MeetingsExecuted");
        	  OpprPunched = resultSet.getInt("OpprPunched");
        	  Requirementspunched = resultSet.getInt("Requirementspunched");
        	  QuotesProcured_Renewals = resultSet.getInt("QuotesProcured_Renewals");
        	  QuotesSubmitted_Renewals = resultSet.getInt("QuotesSubmitted_Renewals");
        	  SoftwareRenewalsProcured_Renewals = resultSet.getInt("SoftwareRenewalsProcured_Renewals");
        	  SoftwareNewLicenseProcured_Renewals = resultSet.getInt("SoftwareNewLicenseProcured_Renewals");
        	  QuotesProcured_Federal = resultSet.getInt("QuotesProcured_Federal");                        
        	  QuotesSubmitted_Federal = resultSet.getInt("QuotesSubmitted_Federal");
        	  SoftwareRenewalsProcured_Federal = resultSet.getInt("SoftwareRenewalsProcured_Federal");                        
        	  SoftwareNewLicenseProcured_Federal = resultSet.getInt("SoftwareNewLicenseProcured_Federal");
             
      
} 
          if(i==0){
        	  setSchdCalls1(SchdCalls);
        	  
        	//  System.out.println("in i"+SchdCalls+"i"+i);
        	  setCallsExe1(CallsExe);
        	  setMeetingsScheduled1(MeetingsScheduled);
        	  setMeetingsExecuted1(MeetingsExecuted);
        	  setOpprPunched1(OpprPunched);
        	  setRequirementspunched1(Requirementspunched);
        	  setQuotesProcured_Federal1(QuotesProcured_Federal);
        	  setQuotesProcured_Renewals1(QuotesProcured_Renewals);
        	  setQuotesSubmitted_Renewals1(QuotesSubmitted_Renewals);
        	  setSoftwareRenewalsProcured_Renewals1(SoftwareRenewalsProcured_Renewals);
        	  setSoftwareNewLicenseProcured_Renewals1(SoftwareNewLicenseProcured_Renewals);
        	  //setQuotesSubmitted_Federal1(QuotesSubmitted_Federal);
        	  setQuotesProcured_Federal11(QuotesSubmitted_Federal);
        	  setSoftwareRenewalsProcured_Federal1(SoftwareRenewalsProcured_Federal);
        	  setSoftwareNewLicenseProcured_Federal1(SoftwareNewLicenseProcured_Federal);
          }
          if(i==1){
        	  setSchdCalls2(SchdCalls);
        	  setCallsExe2(CallsExe);
        	  setMeetingsScheduled2(MeetingsScheduled);
        	  setMeetingsExecuted2(MeetingsExecuted);
        	  setOpprPunched2(OpprPunched);
        	  setRequirementspunched2(Requirementspunched);
        	  setQuotesProcured_Federal2(QuotesProcured_Federal);
        	  setQuotesProcured_Renewals2(QuotesProcured_Renewals);
        	  setQuotesSubmitted_Renewals2(QuotesSubmitted_Renewals);
        	  setSoftwareRenewalsProcured_Renewals2(SoftwareRenewalsProcured_Renewals);
        	  setSoftwareNewLicenseProcured_Renewals2(SoftwareNewLicenseProcured_Renewals);
        	  //setQuotesSubmitted_Federal2(QuotesSubmitted_Federal);
        	  setQuotesProcured_Federal22(QuotesSubmitted_Federal);
        	  setSoftwareRenewalsProcured_Federal2(SoftwareRenewalsProcured_Federal);
        	  setSoftwareNewLicenseProcured_Federal2(SoftwareNewLicenseProcured_Federal);
          }
          if(i==2){
        	  setSchdCalls3(SchdCalls);
        	  setCallsExe3(CallsExe);
        	  setMeetingsScheduled3(MeetingsScheduled);
        	  setMeetingsExecuted3(MeetingsExecuted);
        	  setOpprPunched3(OpprPunched);
        	  setRequirementspunched3(Requirementspunched);
        	  setQuotesProcured_Federal3(QuotesProcured_Federal);
        	  setQuotesProcured_Renewals3(QuotesProcured_Renewals);
        	  setQuotesSubmitted_Renewals3(QuotesSubmitted_Renewals);
        	  setSoftwareRenewalsProcured_Renewals3(SoftwareRenewalsProcured_Renewals);
        	  setSoftwareNewLicenseProcured_Renewals3(SoftwareNewLicenseProcured_Renewals);
        	 // setQuotesSubmitted_Federal3(QuotesSubmitted_Federal);
        	  setQuotesProcured_Federal33(QuotesSubmitted_Federal);
        	  setSoftwareRenewalsProcured_Federal3(SoftwareRenewalsProcured_Federal);
        	  setSoftwareNewLicenseProcured_Federal3(SoftwareNewLicenseProcured_Federal);
          }
          if(i==3){
        	  setSchdCalls4(SchdCalls);
        	  setCallsExe4(CallsExe);
        	  setMeetingsScheduled4(MeetingsScheduled);
        	  setMeetingsExecuted4(MeetingsExecuted);
        	  setOpprPunched4(OpprPunched);
        	  setRequirementspunched4(Requirementspunched);
        	  setQuotesProcured_Federal4(QuotesProcured_Federal);
        	  setQuotesProcured_Renewals4(QuotesProcured_Renewals);
        	  setQuotesSubmitted_Renewals4(QuotesSubmitted_Renewals);
        	  setSoftwareRenewalsProcured_Renewals4(SoftwareRenewalsProcured_Renewals);
        	  setSoftwareNewLicenseProcured_Renewals4(SoftwareNewLicenseProcured_Renewals);
        	 // setQuotesSubmitted_Federal4(QuotesSubmitted_Federal);
        	  setQuotesProcured_Federal44(QuotesSubmitted_Federal);
        	  setSoftwareRenewalsProcured_Federal4(SoftwareRenewalsProcured_Federal);
        	  setSoftwareNewLicenseProcured_Federal4(SoftwareNewLicenseProcured_Federal);
          }
}
               resultType = SUCCESS;
             //   System.out.println("resultType=========>"+resultType);
            } catch (Exception ex) {
                //List errorMsgList = ExceptionToListUtility.errorMessages(ex);
                httpServletRequest.getSession(false).setAttribute("errorMessage", ex.toString());
               resultType = ERROR;
            }
            //	System.out.println("getSchdCalls1"+getSchdCalls1());

        
  //  }
    // System.out.println("result->"+resultType);
    return resultType;
}






public String submitHrData() {
	
//	System.out.println("usubmitHrData");
//	System.out.println("getSchdCalls1"+getSchdCalls1());
	//updatedatabyhr();
  //  resultType = LOGIN;
 //   httpSession = httpServletRequest.getSession(false);
  //  if (httpSession.getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
     //   userRoleId = Integer.parseInt(httpSession.getAttribute(ApplicationConstants.SESSION_ROLE_ID).toString());
     //   resultType = "accessFailed";
	//HttpServletRequest httpServletRequest;
	
       PreparedStatement preparedStatement1 = null;
		ResultSet resultSet = null;
		String deleteQuery="";
		int updatedrows=0;
		PreparedStatement preparedStatement = null;
           
            	Connection connection = null;

            	CallableStatement callableStatement = null;
            	HashMap map = null;

            //	String result=LOGIN;
            	//httpServletRequest.getSession().getAttribute(arg0)

            //	String userId = httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID).toString();

            	String userId =getUserId();
            //	System.out.println("userId"+userId);
try{
	  connection = ConnectionProvider.getInstance().getConnection();
            		deleteQuery="DELETE FROM tblLkpSalesKPIHRInputs";
        			preparedStatement1 = connection.prepareStatement(deleteQuery);
        			preparedStatement1.executeUpdate();
        			
        		//	System.out.println("getSchdCalls1"+getSchdCalls1());
            	

            	 String resourcetype[]={"Sales Trainee","Jr.BDE","BDE","Sr.BDE"};
            	 for(int i=0;i<resourcetype.length;i++){
            		 String	queryString = "insert into tblLkpSalesKPIHRInputs (ResourceType,SchdCalls,CallsExe,MeetingsScheduled,MeetingsExecuted,OpprPunched,"
         					+ "Requirementspunched,QuotesProcured_Renewals,QuotesSubmitted_Renewals,SoftwareRenewalsProcured_Renewals,"
         					+ "SoftwareNewLicenseProcured_Renewals,QuotesProcured_Federal,"
         					+ "QuotesSubmitted_Federal,SoftwareRenewalsProcured_Federal,SoftwareNewLicenseProcured_Federal,ModifiedBy) "
         					+ "values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ";
         			
            		 
            	/*	 String	queryString = "UPDATE tblLkpSalesKPIHRInputs SET ResourceType=?,SchdCalls=?,CallsExe=?,MeetingsScheduled=?,MeetingsExecuted=?,OpprPunched=?,"
            		 		+ "Requirementspunched=?,QuotesProcured_Renewals=?,QuotesSubmitted_Renewals=?,SoftwareRenewalsProcured_Renewals=?,"
            		 		+ "SoftwareNewLicenseProcured_Renewals=?,QuotesProcured_Federal=?,"
            		 		+ "	QuotesSubmitted_Federal=?,SoftwareRenewalsProcured_Federal=?,SoftwareNewLicenseProcured_Federal=?,ModifiedBy=? WHERE ResourceType='"+resourcetype[i]+"'";
 	*/
         			preparedStatement = connection.prepareStatement(queryString);
         			
         		//	System.out.println("queryString"+queryString);
         			if(i==0){
         			preparedStatement.setString(1, resourcetype[i]);
         			
         			//System.out.println("getSchdCalls1"+getSchdCalls1());
         			preparedStatement.setInt(2, getSchdCalls1());
         			preparedStatement.setInt(3, getCallsExe1());
         			preparedStatement.setInt(4, getMeetingsScheduled1());
         			preparedStatement.setInt(5,getMeetingsExecuted1());
         			preparedStatement.setInt(6, getOpprPunched1());
         			preparedStatement.setInt(7, getRequirementspunched1());
         			preparedStatement.setInt(8, getQuotesProcured_Renewals1());
         			preparedStatement.setInt(9, getQuotesSubmitted_Renewals1());
         			preparedStatement.setInt(10, getSoftwareRenewalsProcured_Renewals1());
         			preparedStatement.setInt(11, getSoftwareNewLicenseProcured_Renewals1());
         			preparedStatement.setInt(12, getQuotesProcured_Federal1());
         			preparedStatement.setInt(13, getQuotesProcured_Federal11());
         			preparedStatement.setInt(14, getSoftwareRenewalsProcured_Federal1());
         			preparedStatement.setInt(15, getSoftwareNewLicenseProcured_Federal1());
         			preparedStatement.setString(16,userId );
         			preparedStatement.executeUpdate();
         			}
         			else if(i==1){
             			preparedStatement.setString(1, resourcetype[i]);
             		//	System.out.println("getSchdCalls2"+getSchdCalls2());
             			preparedStatement.setInt(2, getSchdCalls2());
             			preparedStatement.setInt(3, getCallsExe2());
             			preparedStatement.setInt(4, getMeetingsScheduled2());
             			preparedStatement.setInt(5,getMeetingsExecuted2());
             			preparedStatement.setInt(6, getOpprPunched2());
             			preparedStatement.setInt(7, getRequirementspunched2());
             			preparedStatement.setInt(8, getQuotesProcured_Renewals2());
             			preparedStatement.setInt(9, getQuotesSubmitted_Renewals2());
             			preparedStatement.setInt(10, getSoftwareRenewalsProcured_Renewals2());
             			preparedStatement.setInt(11, getSoftwareNewLicenseProcured_Renewals2());
             			preparedStatement.setInt(12, getQuotesProcured_Federal2());
             			preparedStatement.setInt(13, getQuotesProcured_Federal22());
             			preparedStatement.setInt(14, getSoftwareRenewalsProcured_Federal2());
             			preparedStatement.setInt(15, getSoftwareNewLicenseProcured_Federal2());
             			preparedStatement.setString(16,userId );
             		preparedStatement.executeUpdate();
             			}
         			else if(i==2){
             			preparedStatement.setString(1, resourcetype[i]);
             		//	System.out.println("getSchdCalls3"+getSchdCalls3());
             			preparedStatement.setInt(2, getSchdCalls3());
             			preparedStatement.setInt(3, getCallsExe3());
             			preparedStatement.setInt(4, getMeetingsScheduled3());
             			preparedStatement.setInt(5,getMeetingsExecuted3());
             			preparedStatement.setInt(6, getOpprPunched3());
             			preparedStatement.setInt(7, getRequirementspunched3());
             			preparedStatement.setInt(8, getQuotesProcured_Renewals3());
             			preparedStatement.setInt(9, getQuotesSubmitted_Renewals3());
             			preparedStatement.setInt(10, getSoftwareRenewalsProcured_Renewals3());
             			preparedStatement.setInt(11, getSoftwareNewLicenseProcured_Renewals3());
             			preparedStatement.setInt(12, getQuotesProcured_Federal3());
             			preparedStatement.setInt(13, getQuotesProcured_Federal33());
             			preparedStatement.setInt(14, getSoftwareRenewalsProcured_Federal3());
             			preparedStatement.setInt(15, getSoftwareNewLicenseProcured_Federal3());
             			preparedStatement.setString(16,userId );
             		preparedStatement.executeUpdate();
             			}
         			else if(i==3){
             			preparedStatement.setString(1, resourcetype[i]);
             		//	System.out.println("getSchdCalls4"+getSchdCalls4());
             			preparedStatement.setInt(2, getSchdCalls4());
             			preparedStatement.setInt(3, getCallsExe4());
             			preparedStatement.setInt(4, getMeetingsScheduled4());
             			preparedStatement.setInt(5,getMeetingsExecuted4());
             			preparedStatement.setInt(6, getOpprPunched4());
             			preparedStatement.setInt(7, getRequirementspunched4());
             			preparedStatement.setInt(8, getQuotesProcured_Renewals4());
             			preparedStatement.setInt(9, getQuotesSubmitted_Renewals4());
             			preparedStatement.setInt(10, getSoftwareRenewalsProcured_Renewals4());
             			preparedStatement.setInt(11, getSoftwareNewLicenseProcured_Renewals4());
             			preparedStatement.setInt(12, getQuotesProcured_Federal4());
             			preparedStatement.setInt(13, getQuotesProcured_Federal44());
             			preparedStatement.setInt(14, getSoftwareRenewalsProcured_Federal4());
             			preparedStatement.setInt(15, getSoftwareNewLicenseProcured_Federal4());
             			preparedStatement.setString(16,userId );
             	updatedrows=	preparedStatement.executeUpdate();
             			}
         			
         			
         			
         			
            	 }
            	/* if (updatedrows > 0) {

 					setResultMessage("<font color=\"green\" size=\"2\">updated successfully !</font>");

 				} else {
 					setResultMessage("<font color=\"red\" size=\"2\">Plese try again</font>");

 				}
 				System.out.println(getResultMessage());
 				if(getResultMessage()!=null){
 				httpServletRequest.setAttribute(ApplicationConstants.RESULT_MSG, getResultMessage());
 				}*/
            	 if (updatedrows == 1) {
					
					
					resultType = SUCCESS;
					setResultMessage("Updated successfully ");
				} else {
					resultType = INPUT;
					setResultMessage("Sorry! Please Try again!");
				}
				
 				resultType = SUCCESS;
            	
}
catch(Exception e){
	e.printStackTrace();
	 resultType = ERROR;
}
         			 
  //  }
     System.out.println("result->"+resultType);
    return resultType;
}



























public String getResultMessage() {
	return resultMessage;
}


public void setResultMessage(String resultMessage) {
	this.resultMessage = resultMessage;
}




}
