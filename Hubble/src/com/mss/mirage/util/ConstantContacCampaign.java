/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mss.mirage.util;

import com.constantcontact.components.contacts.ContactList;
import com.constantcontact.components.emailcampaigns.EmailCampaignRequest;
import com.constantcontact.components.emailcampaigns.EmailCampaignResponse;
import com.constantcontact.components.emailcampaigns.MessageFooter;
import com.constantcontact.components.emailcampaigns.SentToContactList;
import com.constantcontact.components.emailcampaigns.schedules.EmailCampaignSchedule;
import com.constantcontact.exceptions.component.ConstantContactComponentException;
import com.constantcontact.exceptions.service.ConstantContactServiceException;
import com.constantcontact.services.contactlists.ContactListService;
import com.constantcontact.services.contacts.ContactService;
import com.constantcontact.services.emailcampaigns.EmailCampaignService;
import com.constantcontact.services.emailcampaigns.schedule.EmailCampaignScheduleService;
import com.constantcontact.util.RawApiRequestError;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author miracle
 */
public class ConstantContacCampaign {

    public static void main(String[] args) {
        try {
            EmailCampaignService ecs = new EmailCampaignService(Properties.getProperty("API_KEY"), Properties.getProperty("ACCESS_TOCKEN"));

            EmailCampaignRequest emailReq = new EmailCampaignRequest();


            ContactService cs = new ContactService(Properties.getProperty("API_KEY"), Properties.getProperty("ACCESS_TOCKEN"));
            emailReq.setSentToContactLists(null);

            ContactListService cls = new ContactListService(Properties.getProperty("API_KEY"), Properties.getProperty("ACCESS_TOCKEN"));



            List<ContactList> cl = cls.getLists(null);
            SentToContactList stcl = new SentToContactList();
            // System.out.println("ltid,,,,,,,,"+ltid);


            for (int i = 0; i < cl.size(); i++) {
                if (cl.get(i).getName().trim().equalsIgnoreCase("Miracle City")) {
                    String ltid = cl.get(i).getId();
                    stcl.setId(ltid);
                }
            }







            emailReq.setName("Star Performers List testing1");
            emailReq.setSubject("REG :Star Performers List");
            emailReq.setFromEmail("newsletter@miraclesoft.com");
            emailReq.setFromName("Miracle Software Systems, Inc.");
            emailReq.setReplyToEmail("newsletter@miraclesoft.com ");

            emailReq.setStatus("DRAFT");



//            emailReq.setPermissionReminderEnabled(true);
//            emailReq.setPermissionReminderText("PermissionReminderText");
           // emailReq.setViewAsWebpageEnabled(false);

            emailReq.setGreetingName("FIRST_NAME");
            emailReq.setGreetingSalutations("Hello");
            emailReq.setGreetingString("Dear");
            emailReq.setEmailContent("<html><body>...</body></html>");
            emailReq.setEmailContentFormat("HTML");
            emailReq.setTextContent("Star Performers");

            MessageFooter messageFooter = new MessageFooter();
            messageFooter.setAddressLine1("143 hate street");
            messageFooter.setOrganizationName("Miracle");
            messageFooter.setCity("vizag");
            messageFooter.setCountry("US");
            messageFooter.setState("MA");
            messageFooter.setPostalCode("01444");
            emailReq.setMessageFooter(messageFooter);

            List<SentToContactList> scl = new ArrayList();




            scl.add(stcl);
            emailReq.setSentToContactLists(scl);


            System.out.println(emailReq.toJSON().toString());

            EmailCampaignResponse ecrq = ecs.addCampaign(emailReq);

            EmailCampaignScheduleService ecsh = new EmailCampaignScheduleService(Properties.getProperty("API_KEY"), Properties.getProperty("ACCESS_TOCKEN"));



            EmailCampaignSchedule Ecs = new EmailCampaignSchedule();
            // Ecs.setScheduledDate("2017-09-25T21:50:19.000Z");
            String campgnStartDate = convertFromElasticToGraphView("07/01/2017 19:00:00");
            Ecs.setScheduledDate(campgnStartDate);

            System.out.println("ecrq.getId()........." + ecrq.getId());
            System.out.println(Ecs.toJSON());
//      EmailCampaignSchedule ecshh= ecsh.updateSchedule("1127025658399", "1", Ecs);
            EmailCampaignSchedule ecshh = ecsh.addSchedule(ecrq.getId(), Ecs);
            System.out.println(ecshh.getId());
            System.out.println(ecshh.getScheduledDate());


            System.out.println(ecrq.getTemplateType());



        } catch (ConstantContactComponentException ex) {
            Logger.getLogger(ConstantContacCampaign.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ConstantContactServiceException e) {
            ConstantContactServiceException c = (ConstantContactServiceException) e.getCause();
            for (Iterator<RawApiRequestError> i = c.getErrorInfo().iterator(); i.hasNext();) {
                RawApiRequestError item = i.next();
                System.out.println(item.getErrorKey() + " : " + item.getErrorMessage());
            }
            e.printStackTrace();
            System.out.println(e.getMessage());
        } catch (Exception ex) {
            // Logger.getLogger(ConstantContacCampaign.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static String convertFromElasticToGraphView(String dateString) {

        SimpleDateFormat formatter, FORMATTER;
        formatter = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss");
        String output = "";
        // String oldDate = "2011-03-10T13:54:30Z";
        try {
            java.util.Date date;
            date = (java.util.Date) formatter.parse(dateString);
            //   System.out.println("NewDate-->"+date);
            FORMATTER = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
            // System.out.println("OldDate-->" + dateString);
            // FORMATTER.setTimeZone(TimeZone.getTimeZone("IST"));


            Calendar cal = new GregorianCalendar();
            cal.setTime(date);

            //Adding 21 Hours to your Date
            cal.add(Calendar.HOUR_OF_DAY, -5);

            java.util.Date DD = cal.getTime();

            //System.out.println("NewDate-->" + FORMATTER.format(DD));

            output = FORMATTER.format(DD);
            //System.out.println("output-->" +output);
        } catch (ParseException ex) {
        }
        return output;
    }
}
