/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mss.mirage.util;


import com.constantcontact.ConstantContactFactory;
import com.constantcontact.components.activities.contacts.request.AddContactsRequest;
import com.constantcontact.components.activities.contacts.request.ContactData;
import com.constantcontact.components.contacts.Contact;
import com.constantcontact.components.contacts.ContactList;
import com.constantcontact.components.emailcampaigns.EmailCampaignRequest;
import com.constantcontact.components.emailcampaigns.EmailCampaignResponse;
import com.constantcontact.components.emailcampaigns.MessageFooter;
import com.constantcontact.components.emailcampaigns.SentToContactList;
import com.constantcontact.components.emailcampaigns.schedules.EmailCampaignSchedule;
import com.constantcontact.exceptions.component.ConstantContactComponentException;
import com.constantcontact.exceptions.service.ConstantContactServiceException;
import com.constantcontact.services.activities.IBulkActivitiesService;
import com.constantcontact.services.contactlists.ContactListService;
import com.constantcontact.services.contacts.ContactService;
import com.constantcontact.services.emailcampaigns.EmailCampaignService;
import com.constantcontact.services.emailcampaigns.schedule.EmailCampaignScheduleService;
import com.constantcontact.util.RawApiRequestError;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author miracle
 */
public class ConstantContactMailManager {
    
    public ConstantContactMailManager(){}
    
     // private static final String ACCESS_TOKEN = com.mss.mirage.util.Properties.getProperty("ACCESS_TOCKEN").toString();
    private static final String SECURITY_KEY  = com.mss.mirage.util.Properties.getProperty("ACCESS_TOCKEN").toString();
     private static final String ACCESS_TOKEN = com.mss.mirage.util.Properties.getProperty("API_KEY").toString();
    public static void doSendNominationsExtendedMail(String eventName,String eventLocation,String eventFromDate,String eventToDate,String nominationEndDatePrev,String nominationEndDateCurrent,String eventLink) throws ConstantContactServiceException, ConstantContactComponentException{
        try{
            
 Calendar cal = Calendar.getInstance();
    int year = cal.get(cal.YEAR);
               EmailCampaignService ecs = new EmailCampaignService(ACCESS_TOKEN,SECURITY_KEY);

                EmailCampaignRequest emailReq = new EmailCampaignRequest();


                ContactService cs = new ContactService(ACCESS_TOKEN, SECURITY_KEY);
                
                ContactListService cls = new ContactListService(ACCESS_TOKEN, SECURITY_KEY);
                List<ContactList> cl = cls.getLists(null);
                          List<SentToContactList> scl = new ArrayList();

                  
              
                // System.out.println("ltid,,,,,,,,"+ltid);
           //Map locationMap=DataSourceDataProvider.getInstance().getEmpLocations();
               // Iterator entries = locationMap.entrySet().iterator();
              //  while (entries.hasNext()) {
                    // Map.Entry entry = (Map.Entry) entries.next();
                for (int i = 0; i < cl.size(); i++) {
                  //  if (cl.get(i).getName().trim().equalsIgnoreCase((String)entry.getValue())) {
                      if (cl.get(i).getName().trim().equalsIgnoreCase("Miracle PreSales") || cl.get(i).getName().trim().equalsIgnoreCase("Miracle Sales")) {
                        String ltid = cl.get(i).getId();
                          SentToContactList stcl = new SentToContactList();
                        stcl.setId(ltid);
                          scl.add(stcl);
                    }
                }

                //}
 String datetime = new SimpleDateFormat("ddMMMyyyy_hh_mm_ss_SSSa").format(new java.util.Date());
                emailReq.setName("Nominations Extended "+datetime);
                emailReq.setSubject("Nominations Extended");
                emailReq.setFromEmail("newsletter@miraclesoft.com");
                emailReq.setFromName("Miracle Software Systems, Inc.");
                emailReq.setReplyToEmail("newsletter@miraclesoft.com");
                emailReq.setStatus("DRAFT");
                emailReq.setPermissionReminderEnabled(false);
              //  emailReq.setPermissionReminderText("PermissionReminderText");
                emailReq.setViewAsWebpageEnabled(false);

                emailReq.setGreetingName("FIRST_NAME");
                emailReq.setGreetingSalutations("Hello");
               // emailReq.set
                emailReq.setGreetingString("Dear");
              //StringBuffer htmlText=new StringBuffer();
             String htmlTxt="<html xmlns='http://www.w3.org/1999/xhtml' class='gr__newsletters-2017-vnallamalla_c9users_io'><head>\n" +
"<meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>\n" +
"<meta name='viewport' content='width=device-width, initial-scale=1'>\n" +
"<meta http-equiv='X-UA-Compatible' content='IE=edge'>\n" +
"<meta name='format-detection' content='telephone=no'>\n" +
"<title>Nominations Extended</title>\n" +
"<style>\n" +
"@import url('https://fonts.googleapis.com/css?family=Montserrat:400,700');\n" +
"@import url('https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i');\n" +
"html{width: 100%;}\n" +
"body{\n" +
"width: 100%;\n" +
"background-color: #ffffff;\n" +
"margin:0; padding:0;\n" +
"-webkit-text-size-adjust:none;\n" +
"-ms-text-size-adjust:none;\n" +
"-webkit-font-smoothing: ;\n" +
"mso-margin-top-alt:0px;\n" +
"mso-margin-bottom-alt:0px;\n" +
"mso-padding-alt: 0px 0px 0px 0px;}\n" +
"div, p, a, li, td { -webkit-text-size-adjust:none; }\n" +
"#outlook a {padding:0;}\n" +
"p,h1,h2,h3,h4{ margin-top:0; margin-bottom:0; padding-top:0; padding-bottom:0;}\n" +
"table{\n" +
"mso-table-lspace:0pt;\n" +
"mso-table-rspace:0pt;\n" +
"}\n" +
"table td {border-collapse: collapse;}\n" +
"span.preheader{display: none;font-size: 1px; visibility: hidden; opacity: 0; color: transparent; height: 0; width: 0;}\n" +
".googlefix {width: 280px!important; text-align:center!important; min-width:0px!important}\n" +
"a {outline:none;}\n" +
"a {text-decoration: none;}\n" +
"a img {border:none;}\n" +
".image_fix {display:block;}\n" +
".ReadMsgBody { width: 100%;}\n" +
".ExternalClass {width: 100%;}\n" +
".ExternalClass,\n" +
".ExternalClass p,\n" +
".ExternalClass span,\n" +
".ExternalClass font,\n" +
".ExternalClass td,\n" +
".ExternalClass div {\n" +
"line-height: 100%;}\n" +
"img{\n" +
"-ms-interpolation-mode:bicubic;}\n" +
"p {\n" +
"margin:0 !important;\n" +
"padding:0 !important;}\n" +
".button:hover {filter: brightness(120%);}\n" +
"@media only screen and (max-width: 600px)\n" +
"{\n" +
"body{width:100% !important;}\n" +
".container           {width:100% !important; min-width:100% !important;}\n" +
".container-wrap      {display:inline-block !important; width:100% !important; height:auto !important; border-radius:0 !important;}\n" +
".image-container     {width: 100% !important; height: auto !important; }\n" +
".disable             {display: none !important;}\n" +
".enable              {display: inline !important;}\n" +
".padding-top-10      {padding-top: 10px !important;}\n" +
".padding-top-20      {padding-top: 20px !important;}\n" +
".padding-top-30      {padding-top: 30px !important;}\n" +
".padding-top-40      {padding-top: 40px !important;}\n" +
".padding-top-60      {padding-top: 60px !important;}\n" +
".padding-bottom-10   {padding-bottom: 10px !important;}\n" +
".padding-bottom-20   {padding-bottom: 20px !important;}\n" +
".padding-bottom-30   {padding-bottom: 30px !important;}\n" +
".padding-bottom-40   {padding-bottom: 40px !important;}\n" +
".padding-bottom-60   {padding-bottom: 60px !important;}\n" +
".padding-none        {padding:0  !important;}\n" +
".text                {width:90% !important; text-align:center !important;}\n" +
".text-on-bg          {width:90% !important; text-align:center !important;}\n" +
".text-center         {text-align: center !important;}\n" +
".text-right          {text-align: right !important;}\n" +
".text-left           {text-align: left !important;}\n" +
".text-white          {color: #ffffff !important;}\n" +
".background-cover    {background-size: cover !important;}\n" +
".background-cover-percent    {background-size: 100% 79% !important;}\n" +
".background-contain  {background-size: contain !important;}\n" +
".background-auto     {background-size: auto !important;}\n" +
".background-center   {background-position: center center !important;}\n" +
".background-none     {background-image: none !important;}\n" +
".border-none         {border:0 !important;}\n" +
".border-white        {border-color: #ffffff !important;}\n" +
"}\n" +
"@media only screen and (max-width: 451px)\n" +
"{\n" +
"body {width:100% !important;}\n" +
".container           {width:100% !important; min-width:100% !important;}\n" +
".container-wrap      {display:inline-block !important; width:100% !important; height:auto !important; border-radius:0 !important;}\n" +
"}\n" +
"@media only screen and (max-width: 341px)\n" +
"{\n" +
"body {width:100%!important;}\n" +
".container           {width:100% !important; min-width:100% !important;}\n" +
".container-wrap      {display:inline-block !important; width:100% !important; height:auto !important; border-radius:0 !important;}\n" +
"}\n" +
"</style>\n" +
"</head>\n" +
"<body marginwidth='0' marginheight='0' style='margin-top: 0; margin-bottom: 0; padding-top: 0; padding-bottom: 0; width: 100%; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%;' offset='0' topmargin='0' leftmargin='0' data-gr-c-s-loaded='true'>\n" +
"<div class='parentOfBg'></div>\n" +
"<div id='edit_link' class='hidden' style='display: none; left: 88px; top: 27956px;'>\n" +
"<div class='close_link'></div>\n" +
"<input type='text' id='edit_link_value' class='createlink' placeholder='Your URL'>\n" +
"<div id='change_image_wrapper' style='display: none;'>\n" +
"<div id='change_image'>\n" +
"<p id='change_image_button'>Change &nbsp; <span class='pixel_result'></span></p>\n" +
"</div>\n" +
"<input type='button' value='' id='change_image_link'>\n" +
"<input type='button' value='' id='remove_image'>\n" +
"</div>\n" +
"<div id='tip'></div>\n" +
"</div>\n" +
"<table mc:repeatable='layout' mc:hideable='' mc:variant='navigation-6' align='center' border='0' cellpadding='0' cellspacing='0' width='100%' style='margin:0 auto; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px; max-width:680px;' data-module='navigation-6'>\n" +
"<tbody>\n" +
"<tr>\n" +
"<td align='center' style='width:100%; height:100%; background-color:#ffffff;' bgcollor='#ffffff' data-bgcolor='nav-6-bg'>\n" +
"<table width='600' align='center' class='container' border='0' cellpadding='0' cellspacing='0' style='width:600px; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px;'>\n" +
"<tbody>\n" +
"<tr>\n" +
"<td align='center' height='20' width='1' style='font-size: 1px; line-height: 20px; height:20px;'>&nbsp;\n" +
"</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td align='center'>\n" +
"<table width='600' align='center' class='container' border='0' cellpadding='0' cellspacing='0' style='width:600px; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px;'>\n" +
"<tbody>\n" +
"<tr>\n" +
"<th width='140' align='left' class='container-wrap' valign='top' style='vertical-align: top;'>\n" +
"<table align='center' cellpadding='0' cellspacing='0'>\n" +
"<tbody>\n" +
"<tr>\n" +
"<td width='160' align='center' valign='top'>\n" +
"<table width='100%' align='center' cellpadding='0' cellspacing='0'>\n" +
"<tbody>\n" +
"<tr>\n" +
"<td align='center' valign='top' style='line-height: 0px !important;'>\n" +
"<a style='text-decoration: none; display: block;' href='https://www.miraclesoft.com/' target='blank'>\n" +
"<img src='https://www.miraclesoft.com/images/newsletters/miracle-logo-dark.png' alt='logo' width='100%' style='width: 100%; max-width: 100%;'>\n" +
"</a>\n" +
"</td>\n" +
"</tr>\n" +
"</tbody>\n" +
"</table>\n" +
"</td>\n" +
"</tr>\n" +
"</tbody>\n" +
"</table>\n" +
"</th>\n" +
"<th width='320' align='right' class='container-wrap' valign='top' style='vertical-align: top;'>\n" +
"<table width='320' align='right' class='container' border='0' cellpadding='0' cellspacing='0' style='width:320px; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px;'>\n" +
"<tbody>\n" +
"<tr>\n" +
"<td align='center' height='1' width='1' style='font-size: 1px; line-height: 1px; height:1px;'>&nbsp;\n" +
"</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td align='right'>\n" +
"<table width='auto' align='right' class='container' border='0' cellpadding='0' cellspacing='0' style='width:autopx; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px;'>\n" +
"<tbody>\n" +
"<tr>\n" +
"<td align='center'>\n" +
"<table width='auto' align='center' class='' border='0' cellpadding='0' cellspacing='0' style='border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px;'>\n" +
"<tbody><tr>\n" +
"<td align='center' height='15'>\n" +
"</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<th align='center' height='1' width='4' style='font-size: 1px; line-height: 1px; width:4px;'>\n" +
"</th>\n" +
"<th style='text-align:center; font-family: oPen sans; font-size:12px; line-height: 20px; text-decoration: none; color: #000000; font-weight:400; text-transform: uppercase; : 0.05em;'>\n" +
"<a href='https://www.miraclesoft.com/company/' target='blank' style='text-align:center; font-family: oPen sans; font-size:12px; line-height: 20px; text-decoration: none; color: #000000; font-weight:600; text-transform: uppercase; : 0.05em;' data-size='nav-6-link-size' data-min='9' data-max='40' data-color='nav-6-link-color' mc:edit='nav-6-link-2'>\n" +
"<multiline><b>About us</b> |</multiline>\n" +
"</a>\n" +
"</th>\n" +
"<th align='center' height='1' width='4' style='font-size: 1px; line-height: 1px; width:4px;'>\n" +
"</th>\n" +
"<th style='text-align:center; font-family: oPen sans; font-size:12px; line-height: 20px; text-decoration: none; color: #000000; font-weight:400; text-transform: uppercase; : 0.05em;'>\n" +
"<a href='https://www.miraclesoft.com/services/' target='blank' style='text-align:center; font-family: oPen sans; font-size:12px; line-height: 20px; text-decoration: none; color: #000000; font-weight:600; text-transform: uppercase; : 0.05em;' data-size='nav-6-link-size' data-min='9' data-max='40' data-color='nav-6-link-color' mc:edit='nav-6-link-3'>\n" +
"<multiline> <b>Our Services</b> |</multiline>\n" +
"</a>\n" +
"</th>\n" +
"<th align='center' height='1' width='4' style='font-size: 1px; line-height: 1px; width:4px;'>\n" +
"</th>\n" +
"<th style='text-align:center; font-family: oPen sans; font-size:12px; line-height: 20px; text-decoration: none; color: #232527; font-weight:600; text-transform: uppercase; : 0.05em;'>\n" +
"<a href='https://www.miraclesoft.com/contact/' target='blank' style='text-align:center; font-family: oPen sans; font-size:12px; line-height: 20px; text-decoration: none; color: #232527; font-weight:600; text-transform: uppercase; : 0.05em;' data-size='nav-6-signUp-size' data-min='9' data-max='40' data-color='nav-6-signUp-color' mc:edit='nav-6-signUp-4'>\n" +
"<multiline><b>Contact us</b></multiline>\n" +
"</a>\n" +
"</th>\n" +
"</tr>\n" +
"</tbody>\n" +
"</table>\n" +
"</td>\n" +
"</tr>\n" +
"</tbody>\n" +
"</table>\n" +
"</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td align='center' height='18' width='1' style='font-size: 1px; line-height: 18px; height:18px;'>&nbsp;\n" +
"</td>\n" +
"</tr>\n" +
"</tbody>\n" +
"</table>\n" +
"</th>\n" +
"</tr>\n" +
"</tbody>\n" +
"</table>\n" +
"</td>\n" +
"</tr>\n" +
"</tbody>\n" +
"</table>\n" +
"</td>\n" +
"</tr>\n" +
"</tbody>\n" +
"</table>\n" +
"<table mc:repeatable='layout' mc:hideable='' mc:variant='navigation-margin' align='center' border='0' cellpadding='0' cellspacing='0' width='100%' style='margin:0 auto; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px; max-width:680px;' data-module='navigation-margin' >\n" +
"<tbody>\n" +
"<tr>\n" +
"<td align='center' style='width:100%; height:100%; background-color:#ffffff;' bgcollor='#ffffff' data-bgcolor='navigation-margin-bg'>\n" +
"<table width='600' align='center' class='container' border='0' cellpadding='0' cellspacing='0' style='width:600px; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px;'>\n" +
"<tbody>\n" +
"<tr>\n" +
"<td align='center' height='20' width='1' style='font-size: 1px;line-height: 10px;height: 10px;'>&nbsp;\n" +
"</td>\n" +
"</tr>\n" +
"</tbody>\n" +
"</table>\n" +
"</td>\n" +
"</tr>\n" +
"</tbody>\n" +
"</table>\n" +
"<div class='parentOfBg'></div>\n" +
"<table align='center' border='0' cellpadding='0' cellspacing='0' width='100%' style='margin:0 auto; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px; max-width:680px;' data-module='text-block-1'>\n" +
"<tbody>\n" +
"<tr>\n" +
"<td align='center' style='width:100%; height:100%; background-color:#f8f8f8;' data-bgcolor='txt-blck-1-bg'>\n" +
"<table width='600' align='center' class='text' border='0' cellpadding='0' cellspacing='0' style='width:600px; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px;'>\n" +
"<tbody>\n" +
"<tr>\n" +
"<td align='center' height='35' width='1' style='font-size: 1px; line-height: 35px; height:35px;'>\n" +
"&nbsp;\n" +
"</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td style='text-align: center;font-family: 'Open Sans' , sans-serif;font-size: 25px;line-height: 20px;text-decoration: none;color: #232527;font-weight:700;' data-size='txt-blck-1-text-size' data-min='9' data-max='40' data-color='txt-blck-1-text-color'>\n" +
"<multiline>Nominations Extended</multiline>\n" +
"</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td align='center' height='25' width='1' style='font-size: 1px;line-height: 25px;height: 25px;'>\n" +
"&nbsp;\n" +
"</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td align='center'>\n" +
"<table width='60' height='2' align='center' border='0' cellpadding='0' cellspacing='0' style='width:60px; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px; background-color: #232527' data-bgcolor='team-6-line-color'>\n" +
"<tbody>\n" +
"<tr>\n" +
"<td align='center' height='2' width='40' style='font-size: 2px; line-height: 2px; width:40px;'></td>\n" +
"</tr>\n" +
"</tbody>\n" +
"</table>\n" +
"</td>\n" +
"</tr>\n" +
"</tbody>\n" +
"</table>\n" +
"</td>\n" +
"</tr>\n" +
"</tbody>\n" +
"</table>\n" +
"<table mc:repeatable='layout' mc:hideable='' mc:variant='text-block-1' align='center' border='0' cellpadding='0' cellspacing='0' width='100%' style='margin:0 auto; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px; max-width:680px;' data-module='text-block-1'>\n" +
"<tbody>\n" +
"<tr>\n" +
"<td align='center' style='width:100%; height:100%; background-color:#f8f8f8;' bgcollor='#f8f8f8' data-bgcolor='txt-blck-1-bg'>\n" +
"<table width='600' align='center' class='text' border='0' cellpadding='0' cellspacing='0' style='width:600px; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px;'>\n" +
"<tbody>\n" +
"<tr>\n" +
"<td align='center' height='25' width='1' style='font-size: 1px; line-height: 25px; height:25px;'>&nbsp;\n" +
"</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td style='text-align:left;font-family: 'Open Sans' , sans-serif;font-size: 14px;line-height: 20px;text-decoration: none;color: #232527;font-weight:400;' data-size='txt-blck-1-text-size' data-min='9' data-max='40' data-color='txt-blck-1-text-color' mc:edit='txt-blck-1-text-1'>\n" +
"<multiline><b>Team,</b>\n" +
"</multiline>\n" +
"</td>\n" +
"</tr>\n" +
"</tbody>\n" +
"</table>\n" +
"</td>\n" +
"</tr>\n" +
"</tbody>\n" +
"</table>\n" +
"<table mc:repeatable='layout' mc:hideable='' mc:variant='text-block-1' align='center' border='0' cellpadding='0' cellspacing='0' width='100%' style='margin:0 auto; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px; max-width:680px;' data-module='text-block-1'>\n" +
"<tbody>\n" +
"<tr>\n" +
"<td align='center' style='width:100%; height:100%; background-color:#f8f8f8;' bgcollor='#f8f8f8' data-bgcolor='txt-blck-1-bg'>\n" +
"<table width='600' align='center' class='text' border='0' cellpadding='0' cellspacing='0' style='width:600px; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px;'>\n" +
"<tbody>\n" +
"<tr>\n" +
"<td align='center' height='25' width='1' style='font-size: 1px;line-height: 15px;height: 15px;'>&nbsp;\n" +
"</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td style='text-align:justify; font-family: 'Open Sans' , sans-serif; font-size:14px; line-height: 24px; text-decoration: none; color: #232527; font-weight:400;' data-size='txt-blck-1-text-size' data-min='9' data-max='40' data-color='txt-blck-1-text-color' mc:edit='txt-blck-1-text-1'>\n" +
"<multiline>This is to inform you that the nominations for <b>"+eventName+"</b> which is happening at <b>"+eventLocation+"</b> from <b>"+DateUtility.getInstance().getDateWithMonthNameFromUsFormat(eventFromDate)+"</b> to <b>"+DateUtility.getInstance().getDateWithMonthNameFromUsFormat(eventToDate)+"</b> have been extended <b>"+DateUtility.getInstance().getDateWithMonthNameFromUsFormat(nominationEndDateCurrent)+"</b>. If you would to participate please go through the below link and submit your application. </multiline>\n" +
"</td>\n" +
"</tr>\n" +
"</tbody>\n" +
"</table>\n" +
"</td>\n" +
"</tr>\n" +
"</tbody>\n" +
"</table>\n" +
"<table mc:repeatable='layout' mc:hideable='' mc:variant='text-block-1' align='center' border='0' cellpadding='0' cellspacing='0' width='100%' style='margin:0 auto; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px; max-width:680px;' data-module='text-block-1'>\n" +
"<tbody>\n" +
"<tr>\n" +
"<td align='center' style='width:100%; height:100%; background-color:#f8f8f8;' bgcollor='#f8f8f8' data-bgcolor='txt-blck-1-bg'>\n" +
"<table width='600' align='center' class='text' border='0' cellpadding='0' cellspacing='0' style='width:600px; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px;'>\n" +
"<tbody>\n" +
"<tr>\n" +
"<td align='center' height='25' width='1' style='font-size: 1px;line-height: 15px;height: 15px;'>&nbsp;\n" +
"</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td align='left' class='button-width'>\n" +
"<table align='left' class='display-button' bgcolor='#232527' style='border-radius: 5px;' data-bgcolor='Header-Butn-BG'>\n" +
"<tbody>\n" +
"<tr>\n" +
"<td align='left' class='MsoNormal' style='color: #ffffff; font-family:'Open Sans'; font-size: 15px; font-weight: 700; padding: 2px 12px;  border-radius: 5px; line-height: 25.5px;' data-color='Header-Butn-Txt' data-size='Header-Butn-Txt' data-min='12' data-max='33'><a href='https://www.miraclesoft.com/events/eventnominations' target='blank' class='' style='color:#ffffff; text-decoration:none;' data-color='Header-Butn-Txt'> Nominate Today!</a></td>\n" +
"</tr>\n" +
"</tbody>\n" +
"</table>\n" +
"</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td align='center' height='25' width='1' style='font-size: 1px;line-height: 15px;height: 15px;'>&nbsp;\n" +
"</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td style='text-align:left; font-family: 'Open Sans';  letter-spaacing:1px; font-size:14px; line-height: 24px; text-decoration: none; color: #232527; font-weight:400;' data-size='txt-blck-1-text-size' data-min='9' data-max='40' data-color='txt-blck-1-text-color' mc:edit='txt-blck-1-text-1'>\n" +
"<multiline><i><b><span style='color:#ef4048;;'>Note :</span></b> Your application must be submitted before <b>"+DateUtility.getInstance().getDateWithMonthNameFromUsFormat(nominationEndDateCurrent)+"</b></i></multiline>\n" +
"</td>\n" +
"</tr>\n" +
"</tbody>\n" +
"</table>\n" +
"</td>\n" +
"</tr>\n" +
"</tbody>\n" +
"</table>\n" +
"<table mc:repeatable='layout' mc:hideable='' mc:variant='text-block-1' align='center' border='0' cellpadding='0' cellspacing='0' width='100%' style='margin:0 auto; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px; max-width:680px;' data-module='text-block-1'>\n" +
"<tbody>\n" +
"<tr>\n" +
"<td align='center' style='width:100%; height:100%; background-color:#f8f8f8;' bgcollor='#f8f8f8' data-bgcolor='txt-blck-1-bg'>\n" +
"<table width='600' align='center' class='text' border='0' cellpadding='0' cellspacing='0' style='width:600px; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px;'>\n" +
"<tbody>\n" +
"<tr>\n" +
"<td align='center' height='10' width='1' style='font-size: 1px;line-height: 15px;height: 15px;'>&nbsp;\n" +
"</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td style='text-align:left; font-family: 'Open Sans'; letter-spaacing:1px; font-size:14px; line-height: 24px; text-decoration: none; color: #232527; font-weight:400;' data-size='txt-blck-1-text-size' data-min='9' data-max='40' data-color='txt-blck-1-text-color' mc:edit='txt-blck-1-text-1'>\n" +
"<multiline>For more details regarding <b>"+eventName+"</b> please visit <a href='"+eventLink+"'><span style='color:#00aae7;'>"+eventLink+"</span></a>.</multiline>\n" +
"</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td align='center' height='15' width='1' style='font-size: 1px;line-height: 15px;height: 15px;'>&nbsp;\n" +
"</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td style='text-align:left; font-family: 'Open Sans' , sans-serif; font-size:14px; line-height: 24px; text-decoration: none; color: #232527; font-weight:400;' data-size='txt-blck-1-text-size' data-min='9' data-max='40' data-color='txt-blck-1-text-color' mc:edit='txt-blck-1-text-1'>\n" +
"<multiline>For more information please reach out to <a href='mailto:partnerships@miraclesoft.com'><span style='color:#00aae7;'>partnerships@miraclesoft.com</span></a> (or) visit <a href='https://www.miraclesoft.com/conferencenominations' target='blank'><span style='color:#00aae7;'>www.miraclesoft.com/conferencenominations.</span></a>\n" +
"</multiline>\n" +
"</td>\n" +
"</tr>\n" +
"</tbody>\n" +
"</table>\n" +
"</td>\n" +
"</tr>\n" +
"</tbody>\n" +
"</table><table mc:repeatable='layout' mc:hideable='' mc:variant='text-block-1' align='center' border='0' cellpadding='0' cellspacing='0' width='100%' style='margin:0 auto; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px; max-width:680px;' data-module='text-block-1'>\n" +
"<tbody>\n" +
"<tr>\n" +
"<td align='center' style='width:100%; height:100%; background-color:#f8f8f8;' bgcollor='#f8f8f8' data-bgcolor='txt-blck-1-bg'>\n" +
"<table width='600' align='center' class='text' border='0' cellpadding='0' cellspacing='0' style='width:600px; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px;'>\n" +
"<tbody>\n" +
"<tr>\n" +
"<td align='center' height='25' width='1' style='font-size: 1px;line-height: 15px;height: 15px;'>&nbsp;\n" +
"</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td style='text-align:left; font-family: 'Open Sans' , sans-serif; font-size:14px; line-height: 24px; text-decoration: none; color: #232527; font-weight:400;' data-size='txt-blck-1-text-size' data-min='9' data-max='40' data-color='txt-blck-1-text-color' mc:edit='txt-blck-1-text-1'>\n" +
"<multiline><b>Thanks &amp; Regards</b><br><b>Partnerships Team</b><br>\n" +
"Phone : (248)-412-0430<br>\n" +
"Email : <a href='mailto:partnerships@miraclesoft.com'><span style='color:#232527;'>partnerships@miraclesoft.com</span></a>\n" +
"</multiline>           </td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td align='center' height='46' width='1' style='font-size: 1px; line-height: 46px; height:46px;'>&nbsp;\n" +
"</td>\n" +
"</tr>\n" +
"</tbody>\n" +
"</table>\n" +
"</td>\n" +
"</tr>\n" +
"</tbody>\n" +
"</table>\n" +
"<table mc:repeatable='layout' mc:hideable='' mc:variant='footer-5' align='center' border='0' cellpadding='0' cellspacing='0' width='100%' style='margin:0 auto; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px; max-width:680px;' data-module='footer-5'>\n" +
"<tbody>\n" +
"<tr>\n" +
"<td align='center' style='width:100%; height:100%; background-color:#ffffff;' bgcollor='#ffffff' data-bgcolor='footer-5-bg'>\n" +
"<table width='600' align='center' class='container' border='0' cellpadding='0' cellspacing='0' style='width:600px; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px;'>\n" +
"<tbody>\n" +
"<tr>\n" +
"<td align='center' height='35' width='1' style='font-size: 1px;line-height: 15px;height: 15px;'>&nbsp;\n" +
"</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td align='center'>\n" +
"<table width='600' align='center' class='text' border='0' cellpadding='0' cellspacing='0' style='width:600px; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px;'>\n" +
"<tbody>\n" +
"<tr>\n" +
"<th width='360' align='left' class='container-wrap' valign='top' style='vertical-align: top;'>\n" +
"<table width='360' align='left' class='container' border='0' cellpadding='0' cellspacing='0' style='width:360px; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px;'>\n" +
"<tbody>\n" +
"<tr>\n" +
"<td class='text-left' style='vertical-align:top;text-align:left;font-family: 'Open Sans', sans-serif;font-size:13px;padding-top: 5px;line-height: 20px;text-decoration: none;color: #232527;font-weight: 400;' data-size='footer-5-rights-size' data-min='9' data-max='40' data-color='footer-5-rights-color' mc:edit='footer-5-rights-1.1'>\n" +
"<multiline>&#169; Copyrights "+year+" | Miracle Software Systems, Inc.</multiline>\n" +
"</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td align='center' height='13' width='1' style='font-size: 1px; line-height: 13px; height:13px;'>&nbsp;\n" +
"</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td align='center' height='39' width='1' style='font-size: 1px;line-height: 20px;height: 20px;'>&nbsp;\n" +
"</td>\n" +
"</tr>\n" +
"</tbody>\n" +
"</table>\n" +
"</th>\n" +
"<th width='280' align='right' class='container-wrap' valign='top' style='vertical-align: top;'>\n" +
"<table width='280' align='right' class='container' border='0' cellpadding='0' cellspacing='0' style='width:280px; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px;'>\n" +
"<tbody>\n" +
"<tr>\n" +
"<td class='disable' align='center' height='5' width='1' style='font-size: 1px; line-height: 5px; height:5px;'>&nbsp;\n" +
"</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td align='right'>\n" +
"<table width='135' align='right' class='container' border='0' cellpadding='0' cellspacing='0' style='width:135px; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px;'>\n" +
"<tbody>\n" +
"<tr>\n" +
"<td align='left'>\n" +
"<table style='border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;border: 0px;' align='left' border='0' cellpadding='0' cellspacing='0' width='auto'>\n" +
"<tbody>\n" +
"<tr>\n" +
"<th>\n" +
"<a href='https://www.facebook.com/miracle45625/' target='blank' style='color:#ef4048; text-decoration: none;'><img src='https://www.miraclesoft.com/images/newsletters/facebook1.png' width='20' height='20' alt='#' style='display:block; vertical-align:middle;'></a>   </th>\n" +
"<td align='center' height='1' width='10' style='font-size: 1px; line-height: 1px; width:10px;'>\n" +
"</td>\n" +
"<th>\n" +
"<a href='https://plus.google.com/+Team_MSS' target='blank' style='color:#ef4048; text-decoration: none;'><img src='https://www.miraclesoft.com/images/newsletters/googleplus1.png' width='20' height='20' alt='#' style='display:block; vertical-align:middle;'></a>     </th>\n" +
"<td align='center' height='1' width='10' style='font-size: 1px; line-height: 1px; width:10px;'>\n" +
"</td>\n" +
"<th>\n" +
"<a href='https://www.linkedin.com/company/miracle-software-systems-inc' target='blank' style='color:#ef4048; text-decoration: none;'><img src='https://www.miraclesoft.com/images/newsletters/Linkedin1.png' width='20' height='20' alt='#' style='display:block; vertical-align:middle;'></a>     </th>\n" +
"<td align='center' height='1' width='10' style='font-size: 1px; line-height: 1px; width:10px;'>\n" +
"</td>\n" +
"<th>\n" +
"</th><td>                        <a href='https://www.youtube.com/c/Team_MSS' target='blank' style='color:#ef4048; text-decoration: none;'><img src='https://www.miraclesoft.com/images/newsletters/youtube1.png' width='20' height='20' alt='#' style='display:block; vertical-align:middle;'></a>     \n" +
"</td><td align='center' height='1' width='10' style='font-size: 1px; line-height: 1px; width:10px;'>\n" +
"</td>\n" +
"<th>\n" +
"</th><td>                        <a href='https://twitter.com/Team_MSS' target='blank' style='color:#ef4048; text-decoration: none;'><img src='https://www.miraclesoft.com/images/newsletters/Twitter1.png' width='20' height='20' alt='#' style='display:block; vertical-align:middle;'></a>     \n" +
"</td><td align='center' height='1' width='10' style='font-size: 1px; line-height: 1px; width:10px;'>\n" +
"</td>\n" +
"<th>\n" +
"</th></tr>\n" +
"</tbody>\n" +
"</table>\n" +
"</td>\n" +
"</tr>\n" +
"</tbody>\n" +
"</table>\n" +
"</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td align='center' height='39' width='1' style='font-size: 1px;line-height: 20px;height: 20px;'>&nbsp;\n" +
"</td>\n" +
"</tr>\n" +
"</tbody>\n" +
"</table>\n" +
"</th>\n" +
"</tr>\n" +
"</tbody>\n" +
"</table>\n" +
"</td>\n" +
"</tr>\n" +
"</tbody>\n" +
"</table>\n" +
"</td>\n" +
"</tr>\n" +
"</tbody>\n" +
"</table>\n" +
"<span class='gr__tooltip'><span class='gr__tooltip-content'></span><i class='gr__tooltip-logo'></i><span class='gr__triangle'></span></span></body></html>";
              
             
             emailReq.setEmailContent(htmlTxt);
emailReq.setEmailContentFormat("HTML");
                emailReq.setTextContent("Nominations Extended");

                MessageFooter messageFooter = new MessageFooter();
                 messageFooter.setAddressLine1("45625 Grand River Avenue");
                messageFooter.setOrganizationName("Miracle Software Systems, Inc.");
                messageFooter.setCity("Novi");
                messageFooter.setCountry("US");
                messageFooter.setState("MI");
                messageFooter.setPostalCode("48374");
                emailReq.setMessageFooter(messageFooter);

      
                emailReq.setSentToContactLists(scl);


               // System.out.println(emailReq.toJSON().toString());

                EmailCampaignResponse ecrq = ecs.addCampaign(emailReq);

                EmailCampaignScheduleService ecsh = new EmailCampaignScheduleService(ACCESS_TOKEN, SECURITY_KEY);



                EmailCampaignSchedule Ecs = new EmailCampaignSchedule();
               
        //        Ecs.setScheduledDate("2017-03-07T14:25:19.000Z");

               // System.out.println("ecrq.getId()........." + ecrq.getId());
              //  System.out.println(Ecs.toJSON());
//      EmailCampaignSchedule ecshh= ecsh.updateSchedule("1127025658399", "1", Ecs);
                EmailCampaignSchedule ecshh = ecsh.addSchedule(ecrq.getId(), Ecs);
             //  System.out.println("ScheduleId-->"+ecshh.getId());
               // System.out.println("Schedul Date-->"+ecshh.getScheduledDate());

              //  System.out.println(ecrq.getTemplateType());

            
        } catch (ConstantContactServiceException e) {
            ConstantContactServiceException c = (ConstantContactServiceException) e.getCause();
            for (Iterator<RawApiRequestError> i = c.getErrorInfo().iterator(); i.hasNext();) {
                RawApiRequestError item = i.next();
                System.out.println(item.getErrorKey() + " : " + item.getErrorMessage());
            }
            e.printStackTrace();
            System.out.println(e.getMessage());
        }catch (ServiceLocatorException ex) {
            System.err.println(ex);
        }catch (Exception ex) {
            System.err.println(ex);
        }
    }
    
    public static void doSendNominationsOpenedMail(String eventName,String eventLocation,String eventFromDate,String eventToDate,String nominationEndDate,String eventLink) throws ConstantContactComponentException{
           try{
               Calendar cal = Calendar.getInstance();
    int year = cal.get(cal.YEAR);
               
               EmailCampaignService ecs = new EmailCampaignService(ACCESS_TOKEN,SECURITY_KEY);

                EmailCampaignRequest emailReq = new EmailCampaignRequest();


                ContactService cs = new ContactService(ACCESS_TOKEN, SECURITY_KEY);
                
                ContactListService cls = new ContactListService(ACCESS_TOKEN, SECURITY_KEY);
                List<ContactList> cl = cls.getLists(null);
                          List<SentToContactList> scl = new ArrayList();

                  
              
                // System.out.println("ltid,,,,,,,,"+ltid);
         //  Map locationMap=DataSourceDataProvider.getInstance().getEmpLocations();
              //  Iterator entries = locationMap.entrySet().iterator();
             //   while (entries.hasNext()) {
               //      Map.Entry entry = (Map.Entry) entries.next();
                for (int i = 0; i < cl.size(); i++) {
                 //   if (cl.get(i).getName().trim().equalsIgnoreCase((String)entry.getValue())) {
                    if (cl.get(i).getName().trim().equalsIgnoreCase("Miracle PreSales") || cl.get(i).getName().trim().equalsIgnoreCase("Miracle Sales")) {
                        String ltid = cl.get(i).getId();
                          SentToContactList stcl = new SentToContactList();
                        stcl.setId(ltid);
                          scl.add(stcl);
                    }
                }

               // }
 String datetime = new SimpleDateFormat("ddMMMyyyy_hh_mm_ss_SSSa").format(new java.util.Date());
                emailReq.setName("Nominations Opened "+datetime);
                emailReq.setSubject("Nominations Opened");
                emailReq.setFromEmail("newsletter@miraclesoft.com");
                emailReq.setFromName("Miracle Software Systems, Inc.");
                emailReq.setReplyToEmail("newsletter@miraclesoft.com");
                emailReq.setStatus("DRAFT");
                emailReq.setPermissionReminderEnabled(false);
              //  emailReq.setPermissionReminderText("PermissionReminderText");
                emailReq.setViewAsWebpageEnabled(false);

                emailReq.setGreetingName("FIRST_NAME");
                emailReq.setGreetingSalutations("Hello");
               // emailReq.set
                emailReq.setGreetingString("Dear");
             String htmlTxt="<html xmlns='http://www.w3.org/1999/xhtml' class='gr__newsletters-2017-vnallamalla_c9users_io'><head>\n" +
"<meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>\n" +
"<meta name='viewport' content='width=device-width, initial-scale=1'>\n" +
"<meta http-equiv='X-UA-Compatible' content='IE=edge'>\n" +
"<meta name='format-detection' content='telephone=no'>\n" +
"<title>Nominations Opened</title>\n" +
"<style>\n" +
"@import url('https://fonts.googleapis.com/css?family=Montserrat:400,700');\n" +
"@import url('https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i');\n" +
"html{width: 100%;}\n" +
"body{\n" +
"width: 100%;\n" +
"background-color: #ffffff;\n" +
"margin:0; padding:0;\n" +
"-webkit-text-size-adjust:none;\n" +
"-ms-text-size-adjust:none;\n" +
"-webkit-font-smoothing: ;\n" +
"mso-margin-top-alt:0px;\n" +
"mso-margin-bottom-alt:0px;\n" +
"mso-padding-alt: 0px 0px 0px 0px;}\n" +
"div, p, a, li, td { -webkit-text-size-adjust:none; }\n" +
"#outlook a {padding:0;}\n" +
"p,h1,h2,h3,h4{ margin-top:0; margin-bottom:0; padding-top:0; padding-bottom:0;}\n" +
"table{\n" +
"mso-table-lspace:0pt;\n" +
"mso-table-rspace:0pt;\n" +
"}\n" +
"table td {border-collapse: collapse;}\n" +
"span.preheader{display: none;font-size: 1px; visibility: hidden; opacity: 0; color: transparent; height: 0; width: 0;}\n" +
".googlefix {width: 280px!important; text-align:center!important; min-width:0px!important}\n" +
"a {outline:none;}\n" +
"a {text-decoration: none;}\n" +
"a img {border:none;}\n" +
".image_fix {display:block;}\n" +
".ReadMsgBody { width: 100%;}\n" +
".ExternalClass {width: 100%;}\n" +
".ExternalClass,\n" +
".ExternalClass p,\n" +
".ExternalClass span,\n" +
".ExternalClass font,\n" +
".ExternalClass td,\n" +
".ExternalClass div {\n" +
"line-height: 100%;}\n" +
"img{\n" +
"-ms-interpolation-mode:bicubic;}\n" +
"p {\n" +
"margin:0 !important;\n" +
"padding:0 !important;}\n" +
".button:hover {filter: brightness(120%);}\n" +
"@media only screen and (max-width: 600px)\n" +
"{\n" +
"body{width:100% !important;}\n" +
".container           {width:100% !important; min-width:100% !important;}\n" +
".container-wrap      {display:inline-block !important; width:100% !important; height:auto !important; border-radius:0 !important;}\n" +
".image-container     {width: 100% !important; height: auto !important; }\n" +
".disable             {display: none !important;}\n" +
".enable              {display: inline !important;}\n" +
".padding-top-10      {padding-top: 10px !important;}\n" +
".padding-top-20      {padding-top: 20px !important;}\n" +
".padding-top-30      {padding-top: 30px !important;}\n" +
".padding-top-40      {padding-top: 40px !important;}\n" +
".padding-top-60      {padding-top: 60px !important;}\n" +
".padding-bottom-10   {padding-bottom: 10px !important;}\n" +
".padding-bottom-20   {padding-bottom: 20px !important;}\n" +
".padding-bottom-30   {padding-bottom: 30px !important;}\n" +
".padding-bottom-40   {padding-bottom: 40px !important;}\n" +
".padding-bottom-60   {padding-bottom: 60px !important;}\n" +
".padding-none        {padding:0  !important;}\n" +
".text                {width:90% !important; text-align:center !important;}\n" +
".text-on-bg          {width:90% !important; text-align:center !important;}\n" +
".text-center         {text-align: center !important;}\n" +
".text-right          {text-align: right !important;}\n" +
".text-left           {text-align: left !important;}\n" +
".text-white          {color: #ffffff !important;}\n" +
".background-cover    {background-size: cover !important;}\n" +
".background-cover-percent    {background-size: 100% 79% !important;}\n" +
".background-contain  {background-size: contain !important;}\n" +
".background-auto     {background-size: auto !important;}\n" +
".background-center   {background-position: center center !important;}\n" +
".background-none     {background-image: none !important;}\n" +
".border-none         {border:0 !important;}\n" +
".border-white        {border-color: #ffffff !important;}\n" +
"}\n" +
"@media only screen and (max-width: 451px)\n" +
"{\n" +
"body {width:100% !important;}\n" +
".container           {width:100% !important; min-width:100% !important;}\n" +
".container-wrap      {display:inline-block !important; width:100% !important; height:auto !important; border-radius:0 !important;}\n" +
"}\n" +
"@media only screen and (max-width: 341px)\n" +
"{\n" +
"body {width:100%!important;}\n" +
".container           {width:100% !important; min-width:100% !important;}\n" +
".container-wrap      {display:inline-block !important; width:100% !important; height:auto !important; border-radius:0 !important;}\n" +
"}\n" +
"</style>\n" +
"</head>\n" +
"<body marginwidth='0' marginheight='0' style='margin-top: 0; margin-bottom: 0; padding-top: 0; padding-bottom: 0; width: 100%; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%;' offset='0' topmargin='0' leftmargin='0' data-gr-c-s-loaded='true'>\n" +
"<div class='parentOfBg'></div>\n" +
"<div id='edit_link' class='hidden' style='display: none; left: 88px; top: 27956px;'>\n" +
"<div class='close_link'></div>\n" +
"<input type='text' id='edit_link_value' class='createlink' placeholder='Your URL'>\n" +
"<div id='change_image_wrapper' style='display: none;'>\n" +
"<div id='change_image'>\n" +
"<p id='change_image_button'>Change &nbsp; <span class='pixel_result'></span></p>\n" +
"</div>\n" +
"<input type='button' value='' id='change_image_link'>\n" +
"<input type='button' value='' id='remove_image'>\n" +
"</div>\n" +
"<div id='tip'></div>\n" +
"</div>\n" +
"<table mc:repeatable='layout' mc:hideable='' mc:variant='navigation-6' align='center' border='0' cellpadding='0' cellspacing='0' width='100%' style='margin:0 auto; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px; max-width:680px;' data-module='navigation-6'>\n" +
"<tbody>\n" +
"<tr>\n" +
"<td align='center' style='width:100%; height:100%; background-color:#ffffff;' bgcollor='#ffffff' data-bgcolor='nav-6-bg'>\n" +
"<table width='600' align='center' class='container' border='0' cellpadding='0' cellspacing='0' style='width:600px; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px;'>\n" +
"<tbody>\n" +
"<tr>\n" +
"<td align='center' height='20' width='1' style='font-size: 1px; line-height: 20px; height:20px;'>&nbsp;\n" +
"</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td align='center'>\n" +
"<table width='600' align='center' class='container' border='0' cellpadding='0' cellspacing='0' style='width:600px; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px;'>\n" +
"<tbody>\n" +
"<tr>\n" +
"<th width='140' align='left' class='container-wrap' valign='top' style='vertical-align: top;'>\n" +
"<table align='center' cellpadding='0' cellspacing='0'>\n" +
"<tbody>\n" +
"<tr>\n" +
"<td width='160' align='center' valign='top'>\n" +
"<table width='100%' align='center' cellpadding='0' cellspacing='0'>\n" +
"<tbody>\n" +
"<tr>\n" +
"<td align='center' valign='top' style='line-height: 0px !important;'>\n" +
"<a style='text-decoration: none; display: block;' href='https://www.miraclesoft.com/' target='blank'>\n" +
"<img src='https://www.miraclesoft.com/images/newsletters/miracle-logo-dark.png' alt='logo' width='100%' style='width: 100%; max-width: 100%;'>\n" +
"</a>\n" +
"</td>\n" +
"</tr>\n" +
"</tbody>\n" +
"</table>\n" +
"</td>\n" +
"</tr>\n" +
"</tbody>\n" +
"</table>\n" +
"</th>\n" +
"<th width='320' align='right' class='container-wrap' valign='top' style='vertical-align: top;'>\n" +
"<table width='320' align='right' class='container' border='0' cellpadding='0' cellspacing='0' style='width:320px; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px;'>\n" +
"<tbody>\n" +
"<tr>\n" +
"<td align='center' height='1' width='1' style='font-size: 1px; line-height: 1px; height:1px;'>&nbsp;\n" +
"</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td align='right'>\n" +
"<table width='auto' align='right' class='container' border='0' cellpadding='0' cellspacing='0' style='width:autopx; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px;'>\n" +
"<tbody>\n" +
"<tr>\n" +
"<td align='center'>\n" +
"<table width='auto' align='center' class='' border='0' cellpadding='0' cellspacing='0' style='border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px;'>\n" +
"<tbody><tr>\n" +
"<td align='center' height='15'>\n" +
"</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<th align='center' height='1' width='4' style='font-size: 1px; line-height: 1px; width:4px;'>\n" +
"</th>\n" +
"<th style='text-align:center; font-family: oPen sans; font-size:12px; line-height: 20px; text-decoration: none; color: #000000; font-weight:400; text-transform: uppercase; : 0.05em;'>\n" +
"<a href='https://www.miraclesoft.com/company/' target='blank' style='text-align:center; font-family: oPen sans; font-size:12px; line-height: 20px; text-decoration: none; color: #000000; font-weight:600; text-transform: uppercase; : 0.05em;' data-size='nav-6-link-size' data-min='9' data-max='40' data-color='nav-6-link-color' mc:edit='nav-6-link-2'>\n" +
"<multiline><b>About us</b> |</multiline>\n" +
"</a>\n" +
"</th>\n" +
"<th align='center' height='1' width='4' style='font-size: 1px; line-height: 1px; width:4px;'>\n" +
"</th>\n" +
"<th style='text-align:center; font-family: oPen sans; font-size:12px; line-height: 20px; text-decoration: none; color: #000000; font-weight:400; text-transform: uppercase; : 0.05em;'>\n" +
"<a href='https://www.miraclesoft.com/services/' target='blank' style='text-align:center; font-family: oPen sans; font-size:12px; line-height: 20px; text-decoration: none; color: #000000; font-weight:600; text-transform: uppercase; : 0.05em;' data-size='nav-6-link-size' data-min='9' data-max='40' data-color='nav-6-link-color' mc:edit='nav-6-link-3'>\n" +
"<multiline> <b>Our Services</b> |</multiline>\n" +
"</a>\n" +
"</th>\n" +
"<th align='center' height='1' width='4' style='font-size: 1px; line-height: 1px; width:4px;'>\n" +
"</th>\n" +
"<th style='text-align:center; font-family: oPen sans; font-size:12px; line-height: 20px; text-decoration: none; color: #232527; font-weight:600; text-transform: uppercase; : 0.05em;'>\n" +
"<a href='https://www.miraclesoft.com/contact/' target='blank' style='text-align:center; font-family: oPen sans; font-size:12px; line-height: 20px; text-decoration: none; color: #232527; font-weight:600; text-transform: uppercase; : 0.05em;' data-size='nav-6-signUp-size' data-min='9' data-max='40' data-color='nav-6-signUp-color' mc:edit='nav-6-signUp-4'>\n" +
"<multiline><b>Contact us</b></multiline>\n" +
"</a>\n" +
"</th>\n" +
"</tr>\n" +
"</tbody>\n" +
"</table>\n" +
"</td>\n" +
"</tr>\n" +
"</tbody>\n" +
"</table>\n" +
"</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td align='center' height='18' width='1' style='font-size: 1px; line-height: 18px; height:18px;'>&nbsp;\n" +
"</td>\n" +
"</tr>\n" +
"</tbody>\n" +
"</table>\n" +
"</th>\n" +
"</tr>\n" +
"</tbody>\n" +
"</table>\n" +
"</td>\n" +
"</tr>\n" +
"</tbody>\n" +
"</table>\n" +
"</td>\n" +
"</tr>\n" +
"</tbody>\n" +
"</table>\n" +
"<table mc:repeatable='layout' mc:hideable='' mc:variant='navigation-margin' align='center' border='0' cellpadding='0' cellspacing='0' width='100%' style='margin:0 auto; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px; max-width:680px;' data-module='navigation-margin'>\n" +
"<tbody>\n" +
"<tr>\n" +
"<td align='center' style='width:100%; height:100%; background-color:#ffffff;' bgcollor='#ffffff' data-bgcolor='navigation-margin-bg'>\n" +
"<table width='600' align='center' class='container' border='0' cellpadding='0' cellspacing='0' style='width:600px; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px;'>\n" +
"<tbody>\n" +
"<tr>\n" +
"<td align='center' height='20' width='1' style='font-size: 1px;line-height: 10px;height: 10px;'>&nbsp; \n" +
"</td>\n" +
"</tr>\n" +
"</tbody>\n" +
"</table>\n" +
"</td>\n" +
"</tr>\n" +
"</tbody>\n" +
"</table>\n" +
"<div class='parentOfBg'></div>\n" +
"<table align='center' border='0' cellpadding='0' cellspacing='0' width='100%' style='margin:0 auto; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px; max-width:680px;' data-module='text-block-1'>\n" +
"<tbody>\n" +
"<tr>\n" +
"<td align='center' style='width:100%; height:100%; background-color:#f8f8f8;' data-bgcolor='txt-blck-1-bg'>\n" +
"<table width='600' align='center' class='text' border='0' cellpadding='0' cellspacing='0' style='width:600px; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px;'>\n" +
"<tbody>\n" +
"<tr>\n" +
"<td align='center' height='35' width='1' style='font-size: 1px; line-height: 35px; height:35px;'>\n" +
"&nbsp;\n" +
"</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td style='text-align: center;font-family: 'Open Sans' , sans-serif;font-size: 25px;line-height: 20px;text-decoration: none;color: #232527;font-weight:700;' data-size='txt-blck-1-text-size' data-min='9' data-max='40' data-color='txt-blck-1-text-color'>\n" +
"<multiline>Nominations Opened</multiline>\n" +
"</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td align='center' height='25' width='1' style='font-size: 1px;line-height: 25px;height: 25px;'>\n" +
"&nbsp;\n" +
"</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td align='center'>\n" +
"<table width='60' height='2' align='center' border='0' cellpadding='0' cellspacing='0' style='width:60px; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px; background-color: #232527' data-bgcolor='team-6-line-color'>\n" +
"<tbody>\n" +
"<tr>\n" +
"<td align='center' height='2' width='40' style='font-size: 2px; line-height: 2px; width:40px;'></td>\n" +
"</tr>\n" +
"</tbody>\n" +
"</table>\n" +
"</td>\n" +
"</tr>\n" +
"</tbody>\n" +
"</table>\n" +
"</td>\n" +
"</tr>\n" +
"</tbody>\n" +
"</table>\n" +
"<table mc:repeatable='layout' mc:hideable='' mc:variant='text-block-1' align='center' border='0' cellpadding='0' cellspacing='0' width='100%' style='margin:0 auto; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px; max-width:680px;' data-module='text-block-1'>\n" +
"<tbody>\n" +
"<tr>\n" +
"<td align='center' style='width:100%; height:100%; background-color:#f8f8f8;' bgcollor='#f8f8f8' data-bgcolor='txt-blck-1-bg'>\n" +
"<table width='600' align='center' class='text' border='0' cellpadding='0' cellspacing='0' style='width:600px; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px;'>\n" +
"<tbody>\n" +
"<tr>\n" +
"<td align='center' height='25' width='1' style='font-size: 1px; line-height: 25px; height:25px;'>&nbsp;\n" +
"</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td style='text-align:left;font-family: 'Open Sans' , sans-serif;font-size: 14px;line-height: 20px;text-decoration: none;color: #232527;font-weight:400;' data-size='txt-blck-1-text-size' data-min='9' data-max='40' data-color='txt-blck-1-text-color' mc:edit='txt-blck-1-text-1'>\n" +
"<multiline><b>Team,</b>\n" +
"</multiline>\n" +
"</td>\n" +
"</tr>\n" +
"</tbody>\n" +
"</table>\n" +
"</td>\n" +
"</tr>\n" +
"</tbody>\n" +
"</table>\n" +
"<table mc:repeatable='layout' mc:hideable='' mc:variant='text-block-1' align='center' border='0' cellpadding='0' cellspacing='0' width='100%' style='margin:0 auto; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px; max-width:680px;' data-module='text-block-1'>\n" +
"<tbody>\n" +
"<tr>\n" +
"<td align='center' style='width:100%; height:100%; background-color:#f8f8f8;' bgcollor='#f8f8f8' data-bgcolor='txt-blck-1-bg'>\n" +
"<table width='600' align='center' class='text' border='0' cellpadding='0' cellspacing='0' style='width:600px; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px;'>\n" +
"<tbody>\n" +
"<tr>\n" +
"<td align='center' height='25' width='1' style='font-size: 1px;line-height: 15px;height: 15px;'>&nbsp;\n" +
"</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td style='text-align:justify; font-family: 'Open Sans' , sans-serif; font-size:14px; line-height: 24px; text-decoration: none; color: #232527; font-weight:400;' data-size='txt-blck-1-text-size' data-min='9' data-max='40' data-color='txt-blck-1-text-color' mc:edit='txt-blck-1-text-1'>\n" +
"<multiline>We are happy to inform you that nominations have been opened for <b>"+eventName+"</b> which is going to take place at <b>"+eventLocation+"</b> from <b>"+DateUtility.getInstance().getDateWithMonthNameFromUsFormat(eventFromDate)+"</b> to <b>"+DateUtility.getInstance().getDateWithMonthNameFromUsFormat(eventToDate)+"</b>. </multiline>\n" +
"</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td align='center' height='25' width='1' style='font-size: 1px;line-height: 15px;height: 15px;'>&nbsp;\n" +
"</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td align='left' class='button-width'>\n" +
"<table align='left' class='display-button' bgcolor='#232527' style='border-radius: 5px;' data-bgcolor='Header-Butn-BG'>\n" +
"<tbody>\n" +
"<tr>\n" +
"<td align='left' class='MsoNormal' style='color: #ffffff; font-family:'Open Sans'; font-size: 15px; font-weight: 700; padding: 2px 12px;  border-radius: 5px; line-height: 25.5px;' data-color='Header-Butn-Txt' data-size='Header-Butn-Txt' data-min='12' data-max='33'><a href='https://www.miraclesoft.com/events/eventnominations' target='blank' class='' style='color:#ffffff; text-decoration:none;' data-color='Header-Butn-Txt'>Nominate Now!</a></td>\n" +
"</tr>\n" +
"</tbody>\n" +
"</table>\n" +
"</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td align='center' height='25' width='1' style='font-size: 1px;line-height: 15px;height: 15px;'>&nbsp;\n" +
"</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td style='text-align:left; font-family: 'Open Sans' , sans-serif; font-size:14px; line-height: 24px; text-decoration: none; color: #232527; font-weight:400;' data-size='txt-blck-1-text-size' data-min='9' data-max='40' data-color='txt-blck-1-text-color' mc:edit='txt-blck-1-text-1'>\n" +
"<multiline><i><b><span style='color:#ef4048;;'>Note:</span></b> Your application must be submitted before <b>"+DateUtility.getInstance().getDateWithMonthNameFromUsFormat(nominationEndDate)+"</b></i></multiline>\n" +
"</td>\n" +
"</tr>\n" +
"</tbody>\n" +
"</table>\n" +
"</td>\n" +
"</tr>\n" +
"</tbody>\n" +
"</table>\n" +
"<table mc:repeatable='layout' mc:hideable='' mc:variant='text-block-1' align='center' border='0' cellpadding='0' cellspacing='0' width='100%' style='margin:0 auto; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px; max-width:680px;' data-module='text-block-1'>\n" +
"<tbody>\n" +
"<tr>\n" +
"<td align='center' style='width:100%; height:100%; background-color:#f8f8f8;' bgcollor='#f8f8f8' data-bgcolor='txt-blck-1-bg'>\n" +
"<table width='600' align='center' class='text' border='0' cellpadding='0' cellspacing='0' style='width:600px; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px;'>\n" +
"<tbody>\n" +
"<tr>\n" +
"<td align='center' height='25' width='1' style='font-size: 1px;line-height: 15px;height: 15px;'>&nbsp;\n" +
"</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td style='text-align:justify; font-family: 'Open Sans' , sans-serif; font-size:14px; line-height: 24px; text-decoration: none; color: #232527; font-weight:400;' data-size='txt-blck-1-text-size' data-min='9' data-max='40' data-color='txt-blck-1-text-color' mc:edit='txt-blck-1-text-1'>\n" +
"<multiline>Once you have submitted the application our team will go through the submissions and select a group to attend the event. For more information please reach out to <a href='mailto:partnerships@miraclesoft.com'><span style='color:#00aae7;'>partnerships@miraclesoft.com</span></a> (or) visit <a href='https://www.miraclesoft.com/conferencenominations' target='blank'><span style='color:#00aae7;'>www.miraclesoft.com/conferencenominations.</span></a>\n" +
"</multiline>\n" +
"</td>\n" +
"</tr>\n" +
"</tbody>\n" +
"</table>\n" +
"</td>\n" +
"</tr>\n" +
"</tbody>\n" +
"</table>\n" +
"<table mc:repeatable='layout' mc:hideable='' mc:variant='text-block-1' align='center' border='0' cellpadding='0' cellspacing='0' width='100%' style='margin:0 auto; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px; max-width:680px;' data-module='text-block-1'>\n" +
"<tbody>\n" +
"<tr>\n" +
"<td align='center' style='width:100%; height:100%; background-color:#f8f8f8;' bgcollor='#f8f8f8' data-bgcolor='txt-blck-1-bg'>\n" +
"<table width='600' align='center' class='text' border='0' cellpadding='0' cellspacing='0' style='width:600px; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px;'>\n" +
"<tbody>\n" +
"<tr>\n" +
"<td align='center' height='25' width='1' style='font-size: 1px;line-height: 15px;height: 15px;'>&nbsp;\n" +
"</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td style='text-align:left; font-family: 'Open Sans' , sans-serif; font-size:14px; line-height: 24px; text-decoration: none; color: #232527; font-weight:400;' data-size='txt-blck-1-text-size' data-min='9' data-max='40' data-color='txt-blck-1-text-color' mc:edit='txt-blck-1-text-1'>\n" +
"<multiline><b>Thanks &amp; Regards</b><br><b>Partnerships Team</b><br>\n" +
"Phone : (248)-412-0430<br>\n" +
"Email : <a href='mailto:partnerships@miraclesoft.com'><span style='color:#232527;'>partnerships@miraclesoft.com</span></a>\n" +
"</multiline>\n" +
"</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td align='center' height='46' width='1' style='font-size: 1px; line-height: 46px; height:46px;'>&nbsp;\n" +
"</td>\n" +
"</tr>\n" +
"</tbody>\n" +
"</table>\n" +
"</td>\n" +
"</tr>\n" +
"</tbody>\n" +
"</table>\n" +
"<table mc:repeatable='layout' mc:hideable='' mc:variant='footer-5' align='center' border='0' cellpadding='0' cellspacing='0' width='100%' style='margin:0 auto; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px; max-width:680px;' data-module='footer-5'>\n" +
"<tbody>\n" +
"<tr>\n" +
"<td align='center' style='width:100%; height:100%; background-color:#ffffff;' bgcollor='#ffffff' data-bgcolor='footer-5-bg'>\n" +
"<table width='600' align='center' class='container' border='0' cellpadding='0' cellspacing='0' style='width:600px; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px;'>\n" +
"<tbody>\n" +
"<tr>\n" +
"<td align='center' height='35' width='1' style='font-size: 1px;line-height: 15px;height: 15px;'>&nbsp;\n" +
"</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td align='center'>\n" +
"<table width='600' align='center' class='text' border='0' cellpadding='0' cellspacing='0' style='width:600px; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px;'>\n" +
"<tbody>\n" +
"<tr>\n" +
"<th width='360' align='left' class='container-wrap' valign='top' style='vertical-align: top;'>\n" +
"<table width='360' align='left' class='container' border='0' cellpadding='0' cellspacing='0' style='width:360px; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px;'>\n" +
"<tbody>\n" +
"<tr>\n" +
"<td class='text-left' style='vertical-align:top;text-align:left;font-family: 'Open Sans', sans-serif;font-size:13px;padding-top: 5px;line-height: 24px;text-decoration: none;color: #232527;font-weight: 400;' data-size='footer-5-rights-size' data-min='9' data-max='40' data-color='footer-5-rights-color' mc:edit='footer-5-rights-1.1'>\n" +
"<multiline>&#169; Copyrights "+year+" | Miracle Software Systems, Inc.</multiline>\n" +
"</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td align='center' height='13' width='1' style='font-size: 1px; line-height: 13px; height:13px;'>&nbsp;\n" +
"</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td align='center' height='39' width='1' style='font-size: 1px;line-height: 20px;height: 20px;'>&nbsp;\n" +
"</td>\n" +
"</tr>\n" +
"</tbody>\n" +
"</table>\n" +
"</th>\n" +
"<th width='280' align='right' class='container-wrap' valign='top' style='vertical-align: top;'>\n" +
"<table width='280' align='right' class='container' border='0' cellpadding='0' cellspacing='0' style='width:280px; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px;'>\n" +
"<tbody>\n" +
"<tr>\n" +
"<td class='disable' align='center' height='5' width='1' style='font-size: 1px; line-height: 5px; height:5px;'>&nbsp;\n" +
"</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td align='right'>\n" +
"<table width='135' align='right' class='container' border='0' cellpadding='0' cellspacing='0' style='width:135px; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px;'>\n" +
"<tbody>\n" +
"<tr>\n" +
"<td align='left'>\n" +
"<table style='border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;border: 0px;' align='left' border='0' cellpadding='0' cellspacing='0' width='auto'>\n" +
"<tbody>\n" +
"<tr>\n" +
"<th>\n" +
"<a href='https://www.facebook.com/miracle45625/' target='blank' style='color:#ef4048; text-decoration: none;'><img src='https://www.miraclesoft.com/images/newsletters/facebook1.png' width='20' height='20' alt='#' style='display:block; vertical-align:middle;'></a>   </th>\n" +
"<td align='center' height='1' width='10' style='font-size: 1px; line-height: 1px; width:10px;'>\n" +
"</td>\n" +
"<th>\n" +
"<a href='https://plus.google.com/+Team_MSS' target='blank' style='color:#ef4048; text-decoration: none;'><img src='https://www.miraclesoft.com/images/newsletters/googleplus1.png' width='20' height='20' alt='#' style='display:block; vertical-align:middle;'></a>     </th>\n" +
"<td align='center' height='1' width='10' style='font-size: 1px; line-height: 1px; width:10px;'>\n" +
"</td>\n" +
"<th>\n" +
"<a href='https://www.linkedin.com/company/miracle-software-systems-inc' target='blank' style='color:#ef4048; text-decoration: none;'><img src='https://www.miraclesoft.com/images/newsletters/Linkedin1.png' width='20' height='20' alt='#' style='display:block; vertical-align:middle;'></a>     </th>\n" +
"<td align='center' height='1' width='10' style='font-size: 1px; line-height: 1px; width:10px;'>\n" +
"</td>\n" +
"<th>\n" +
"</th><td>                        <a href='https://www.youtube.com/c/Team_MSS' target='blank' style='color:#ef4048; text-decoration: none;'><img src='https://www.miraclesoft.com/images/newsletters/youtube1.png' width='20' height='20' alt='#' style='display:block; vertical-align:middle;'></a>     \n" +
"</td><td align='center' height='1' width='10' style='font-size: 1px; line-height: 1px; width:10px;'>\n" +
"</td>\n" +
"<th>\n" +
"</th><td>                        <a href='https://twitter.com/Team_MSS' target='blank' style='color:#ef4048; text-decoration: none;'><img src='https://www.miraclesoft.com/images/newsletters/Twitter1.png' width='20' height='20' alt='#' style='display:block; vertical-align:middle;'></a>     \n" +
"</td><td align='center' height='1' width='10' style='font-size: 1px; line-height: 1px; width:10px;'>\n" +
"</td>\n" +
"<th>\n" +
"</th></tr>\n" +
"</tbody>\n" +
"</table>\n" +
"</td>\n" +
"</tr>\n" +
"</tbody>\n" +
"</table>\n" +
"</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td align='center' height='39' width='1' style='font-size: 1px;line-height: 20px;height: 20px;'>&nbsp;\n" +
"</td>\n" +
"</tr>\n" +
"</tbody>\n" +
"</table>\n" +
"</th>\n" +
"</tr>\n" +
"</tbody>\n" +
"</table>\n" +
"</td>\n" +
"</tr>\n" +
"</tbody>\n" +
"</table>\n" +
"</td>\n" +
"</tr>\n" +
"</tbody>\n" +
"</table>\n" +
"<span class='gr__tooltip'><span class='gr__tooltip-content'></span><i class='gr__tooltip-logo'></i><span class='gr__triangle'></span></span></body></html>";
              
             emailReq.setEmailContent(htmlTxt);
               emailReq.setEmailContentFormat("HTML");
                emailReq.setTextContent("Nominations Opened");

                MessageFooter messageFooter = new MessageFooter();
                messageFooter.setAddressLine1("45625 Grand River Avenue");
                messageFooter.setOrganizationName("Miracle Software Systems, Inc.");
                messageFooter.setCity("Novi");
                messageFooter.setCountry("US");
                messageFooter.setState("MI");
                messageFooter.setPostalCode("48374");
                emailReq.setMessageFooter(messageFooter);

      
                emailReq.setSentToContactLists(scl);

                
              //  System.out.println(emailReq.toJSON().toString());

                EmailCampaignResponse ecrq = ecs.addCampaign(emailReq);

                EmailCampaignScheduleService ecsh = new EmailCampaignScheduleService(ACCESS_TOKEN, SECURITY_KEY);

                EmailCampaignSchedule Ecs = new EmailCampaignSchedule();
               
              //  Ecs.setScheduledDate("2017-03-07T18:55:19.000Z");

              //  System.out.println("ecrq.getId()........." + ecrq.getId());
              //  System.out.println(Ecs.toJSON());
//      EmailCampaignSchedule ecshh= ecsh.updateSchedule("1127025658399", "1", Ecs);
                EmailCampaignSchedule ecshh = ecsh.addSchedule(ecrq.getId(), Ecs);
             //   System.out.println(ecshh.getId());
             //   System.out.println(ecshh.getScheduledDate());


              //  System.out.println(ecrq.getTemplateType());

            
        } catch (ConstantContactServiceException e) {
            ConstantContactServiceException c = (ConstantContactServiceException) e.getCause();
            for (Iterator<RawApiRequestError> i = c.getErrorInfo().iterator(); i.hasNext();) {
                RawApiRequestError item = i.next();
                System.out.println(item.getErrorKey() + " : " + item.getErrorMessage());
            }
            e.printStackTrace();
            System.out.println(e.getMessage());
        }catch (ServiceLocatorException ex) {
            System.err.println(ex);
        }catch (Exception ex) {
            System.err.println(ex);
        }
    }
  
    public static int doSendStarPerformerNomination(int objId, String time) throws ConstantContactComponentException {

        int rows = 0;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Connection connection = null;

        try {



            String contactId = "";

            ContactListService cls = new ContactListService(Properties.getProperty("API_KEY"), Properties.getProperty("ACCESS_TOCKEN"));








            List<ContactList> clst = cls.getLists(null);

            for (int j = 0; j < clst.size(); j++) {


                if ("".equals(contactId)) {
                    if (clst.get(j).getName().equals("SP_OperationList" +time)) {
                        contactId = clst.get(j).getId();
                    }
                }
            }
            //   System.out.println("contactId"+contactId);
            if (!"".equals(contactId.toString().trim())) {
                cls.deleteList(contactId);
                contactId = "";
            }

            if ("".equals(contactId.toString().trim())) {
                ContactList contList = new ContactList();
                contList.setName("SP_OperationList" +time);
                contList.setStatus("ACTIVE");
                contList = cls.addList(contList);
                contactId = contList.getId();
                //      System.out.println("contactId"+contactId);
            }
          

            ConstantContactFactory cf = new ConstantContactFactory(Properties.getProperty("API_KEY"), Properties.getProperty("ACCESS_TOCKEN"));
            IBulkActivitiesService _activityService = cf.createBulkActivitiesService();



            AddContactsRequest contactsToAdd = new AddContactsRequest();
            List<String> contactLists = new ArrayList<String>();
            List<String> columns = new ArrayList<String>();
            List<ContactData> importData = new ArrayList<ContactData>();

            contactLists.add(contactId); // NOTE: Replace this with the IF for the list you wish to use
            columns.add("EMAIL ADDRESS");
            columns.add("FIRST NAME");
            columns.add("LAST NAME");
            columns.add("Company Name");
            contactsToAdd.setColumnNames(columns);
            contactsToAdd.setLists(contactLists);




            connection = ConnectionProvider.getInstance().getConnection();



            //  String query = "SELECT (YEAR(DATE)-2000) AS yr,YEAR(DATE) AS fullYear,MONTHNAME(STR_TO_DATE(MONTH(DATE), '%m')) AS mnth,DATE_FORMAT(DATE, '%b') AS shortMonth,tblStarPerformerLines.Id,tblStarPerformers.Id As spId,tblStarPerformerLines.ObjectId,tblStarPerformerLines.EmpId,tblStarPerformerLines.EmpName AS EmployeeName,tblStarPerformerLines.Title AS Title,tblStarPerformerLines.CreatedBy,tblStarPerformerLines.Comments,DATE,tblStarPerformerLines.FilePath As FilePath,tblStarPerformerLines.Department As Department,tblStarPerformerLines.EmpId,tblEmployee.Email1,tblEmployee.FName,tblEmployee.LName,tblEmployee.OrgId FROM tblStarPerformerLines LEFT OUTER JOIN tblStarPerformers ON(tblStarPerformers.Id=tblStarPerformerLines.ObjectId) left join tblEmployee ON(tblStarPerformerLines.EmpId=tblEmployee.Id)  WHERE tblStarPerformerLines.Status='Approve' AND tblStarPerformerLines.ObjectId="+objectId+" AND tblEmployee.Email1 IN("+email+")";
            String query = "Select Distinct Email1,tblEmployee.FName,tblEmployee.LName,tblEmployee.OrgId  from tblEmployee where IsOperationContactTeam=1 And Country='India' And CurStatus='Active'";
            //     System.out.println("query-----" + query);

            preparedStatement = connection.prepareStatement(query);




            resultSet = preparedStatement.executeQuery();




            int i = 0;

            while (resultSet.next()) {




                ContactData newContact = new ContactData();
                List<String> emails = new ArrayList<String>();
                emails.add(resultSet.getString("Email1"));
                newContact.setEmailAddresses(emails);
                newContact.setFirstName(resultSet.getString("FName"));
                newContact.setLastName(resultSet.getString("LName"));
                newContact.setCompanyName(resultSet.getString("OrgId"));
                importData.add(newContact);


                i++;
            }




            contactsToAdd.setImportData(importData);

            _activityService.addContacts(contactsToAdd);


           
            String htmlText ="<html xmlns='http://www.w3.org/1999/xhtml' class='gr__newsletters-2017-vnallamalla_c9users_io'><head>\n" +
"<meta http-equiv='X-UA-Compatible' content='IE=edge'>\n" +
"<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>\n" +
"<meta name='viewport' content='width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1'>\n" +
"<link rel='stylesheet' type='text/css' href='//fonts.googleapis.com/css?family=Open+Sans'>\n" +
"<title>Star Performers</title>\n" +
"<style>\n" +
".ReadMsgBody {\n" +
"width: 100%;\n" +
"background-color: #ffffff;\n" +
"}\n" +
".ExternalClass {\n" +
"width: 100%;\n" +
"background-color: #ffffff;\n" +
"}\n" +
".ExternalClass,\n" +
".ExternalClass p,\n" +
".ExternalClass span,\n" +
".ExternalClass font,\n" +
".ExternalClass td,\n" +
".ExternalClass div {\n" +
"line-height: 100%;\n" +
"}\n" +
"html {\n" +
"width: 100%;\n" +
"}\n" +
"body {\n" +
"-webkit-text-size-adjust: none;\n" +
"-ms-text-size-adjust: none;\n" +
"margin: 0;\n" +
"padding: 0;\n" +
"}\n" +
"table {\n" +
"border-spacing: 0;\n" +
"border-collapse: collapse;\n" +
"table-layout: fixed;\n" +
"margin: 0 auto;\n" +
"}\n" +
"table table table {\n" +
"table-layout: auto;\n" +
"}\n" +
"img {\n" +
"display: block !important;\n" +
"over-flow: hidden !important;\n" +
"}\n" +
"table td {\n" +
"border-collapse: collapse;\n" +
"}\n" +
".yshortcuts a {\n" +
"border-bottom: none !important;\n" +
"}\n" +
"a {\n" +
"color: #8c8c8c;\n" +
"text-decoration: none;\n" +
"}\n" +
"@media only screen and (max-width: 640px) {\n" +
"body {\n" +
"width: auto !important;\n" +
"}\n" +
"table[class='table-inner'] {\n" +
"width: 90% !important;\n" +
"}\n" +
"table[class='table2-2'] {\n" +
"width: 47% !important;\n" +
"text-align: center !important;\n" +
"}\n" +
"table[class='table-full'] {\n" +
"width: 100% !important;\n" +
"text-align: center !important;\n" +
"}\n" +
"img[class='img1'] {\n" +
"width: 100% !important;\n" +
"height: auto !important;\n" +
"}\n" +
"td[class='hide'] {\n" +
"max-height: 0px !important;\n" +
"height: 0px !important;\n" +
"}\n" +
"td[class='contact-button'] {\n" +
"border-top: 0px !important;\n" +
"}\n" +
"img[class='logo'] {\n" +
"width: 50% !important;\n" +
"height: auto !important;\n" +
"}\n" +
"}\n" +
"@media only screen and (max-width: 479px) {\n" +
"body {\n" +
"width: auto !important;\n" +
"}\n" +
"table[class='table-inner'] {\n" +
"width: 90% !important;\n" +
"}\n" +
"table[class='table2-2'] {\n" +
"width: 100% !important;\n" +
"text-align: center !important;\n" +
"}\n" +
"table[class='table-full'] {\n" +
"width: 100% !important;\n" +
"text-align: center !important;\n" +
"}\n" +
"img[class='img1'] {\n" +
"width: 100% !important;\n" +
"height: auto !important;\n" +
"}\n" +
"td[class='hide'] {\n" +
"max-height: 0px !important;\n" +
"height: 0px !important;\n" +
"}\n" +
"td[class='contact-button'] {\n" +
"border-top: 0px !important;\n" +
"}\n" +
"img[class='logo'] {\n" +
"width: 50% !important;\n" +
"height: auto !important;\n" +
"}\n" +
"}\n" +
"</style>\n" +
"</head>\n" +
"<body marginwidth='0' marginheight='0' style='margin-top: 0; margin-bottom: 0; padding-top: 0; padding-bottom: 0; width: 100%; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%;' offset='0' topmargin='0' leftmargin='0' data-gr-c-s-loaded='true'>\n" +
"<div id='edit_link' class='hidden' style='display: none;'>\n" +
"<div class='close_link'></div>\n" +
"<input type='text' id='edit_link_value' class='createlink' placeholder='Your URL'>\n" +
"<div id='change_image_wrapper'>\n" +
"<div id='change_image'>\n" +
"<p id='change_image_button'>Change &nbsp; <span class='pixel_result'></span>\n" +
"</p>\n" +
"</div>\n" +
"<input type='button' value='' id='change_image_link'>\n" +
"<input type='button' value='' id='remove_image'>\n" +
"</div>\n" +
"<div id='tip'></div>\n" +
"</div>\n" +
"<table data-thumb='https://www.miraclesoft.com/images/newsletters/2017/January/star_banner2.png' data-module='header-bar' data-bgcolor='Main BG' bgcolor='#ffffff' width='100%' border='0' align='center' cellpadding='0' cellspacing='0'>\n" +
"<tbody>\n" +
"<tr>\n" +
"<td align='center'>\n" +
"<table align='center' class='table-inner' width='600' border='0' cellspacing='0' cellpadding='0'>\n" +
"<tbody>\n" +
"<tr>\n" +
"<td>\n" +
"<table class='table-full' border='0' align='left' cellpadding='0' cellspacing='0'>\n" +
"<tbody>\n" +
"<tr>\n" +
"<td height='16'></td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td align='center' style='line-height: 0px;'>\n" +
"<a href='https://www.miraclesoft.com/' target='blank' style='text-decoration:none; color:#232527;' data-color='Menu Link'>\n" +
"<img data-crop='false' mc:edit='header-bar-1' editable='' label='logo' style='display:block; line-height:0px; font-size:0px; border:0px;' src='https://www.miraclesoft.com/images/newsletters/miracle-logo-dark.png' width='160' alt='img'>\n" +
"</a>\n" +
"</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td height='16'></td>\n" +
"</tr>\n" +
"</tbody>\n" +
"</table>\n" +
"<table width='1' border='0' cellpadding='0' cellspacing='0' align='left'>\n" +
"<tbody>\n" +
"<tr>\n" +
"<td height='30' style='font-size: 0;line-height: 0px;border-collapse: collapse;'>\n" +
"<p style='padding-left: 24px;'>&nbsp;</p>\n" +
"</td>\n" +
"</tr>\n" +
"</tbody>\n" +
"</table>\n" +
"<table class='table-full' align='right' width='0' border='0' cellspacing='0' cellpadding='0'>\n" +
"<tbody>\n" +
"<tr>\n" +
"<td align='center'>\n" +
"<table class='table-full' width='0' border='0' align='center' cellpadding='0' cellspacing='0'>\n" +
"<tbody>\n" +
"<tr>\n" +
"<td height='70' align='center'>\n" +
"<table width='0' border='0' align='center' cellpadding='0' cellspacing='0'>\n" +
"<tbody>\n" +
"<tr>\n" +
"<td height='15' class='hide'></td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td data-link-style='text-decoration:none; color:#232527;' data-link-color='Menu Link' data-size='Menu' mc:edit='header-bar-2' align='center' style='text-decoration: none; color:#232527;font-family: oPen sans;  font-size:13px; padding-left: 20px;padding-right: 20px;'>\n" +
"<a href='https://www.miraclesoft.com/company/' target='blank' style='text-decoration:none; color:#232527;' data-color='Menu Link'>\n" +
"<singleline label='menu-1'>ABOUT</singleline>\n" +
"</a>\n" +
"</td>\n" +
"</tr>\n" +
"</tbody>\n" +
"</table>\n" +
"</td>\n" +
"<td height='70' align='center'>\n" +
"<table width='0' border='0' align='center' cellpadding='0' cellspacing='0'>\n" +
"<tbody>\n" +
"<tr>\n" +
"<td height='15' class='hide'></td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td data-link-style='text-decoration:none; color:#232527;' data-link-color='Menu Link' data-size='Menu' mc:edit='header-bar-3' align='center' style='text-decoration: none; color:#232527;font-family: oPen sans;  font-size:13px; padding-left: 20px;padding-right: 20px;'>\n" +
"<a href='https://www.miraclesoft.com/services/' target='blank' style='text-decoration:none; color:#232527;' data-color='Menu Link'>\n" +
"<singleline label='menu-2'>SERVICES</singleline>\n" +
"</a>\n" +
"</td>\n" +
"</tr>\n" +
"</tbody>\n" +
"</table>\n" +
"</td>\n" +
"<td height='70' align='center'>\n" +
"<table width='0' border='0' align='center' cellpadding='0' cellspacing='0'>\n" +
"<tbody>\n" +
"<tr>\n" +
"<td height='15' class='hide'></td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td data-link-style='text-decoration:none; color:#232527;' data-link-color='Menu Link' data-size='Menu' mc:edit='header-bar-3' align='center' style='text-decoration: none; color:#232527;font-family: oPen sans;  font-size:13px; padding-left: 20px;padding-right: 20px;'>\n" +
"<a href='https://www.miraclesoft.com/Hubble/customer/login.action' target='blank' style='text-decoration:none; color:#232527;' data-color='Menu Link'>\n" +
"<singleline label='menu-2'>CONTACT</singleline>\n" +
"</a>\n" +
"</td>\n" +
"</tr>\n" +
"</tbody>\n" +
"</table>\n" +
"</td>\n" +
"</tr>\n" +
"</tbody>\n" +
"</table>\n" +
"</td>\n" +
"</tr>\n" +
"</tbody>\n" +
"</table>\n" +
"</td>\n" +
"</tr>\n" +
"</tbody>\n" +
"</table>\n" +
"</td>\n" +
"</tr>\n" +
"</tbody>\n" +
"</table>\n" +
"<table data-thumb='https://www.miraclesoft.com/images/newsletters/2017/January/star_banner2.png' data-module='header-6' data-bgcolor='Main BG 2' bgcolor='#f6f6f6' width='100%' border='0' align='center' cellpadding='0' cellspacing='0'>\n" +
"<tbody>\n" +
"<tr>\n" +
"<td align='center'>\n" +
"<table data-bg='Header 6 BG' data-bgcolor='Header 6 BG' data-border-bottom-color='Header 6 Border' background='https://www.miraclesoft.com/images/newsletters/2017/May/nominations.png' style='background-size:cover; background-position:center; border-bottom:3px solid #8c8c8c;' bgcolor='#262d32' align='center' width='100%' border='0' cellspacing='0' cellpadding='0'>\n" +
"<tbody>\n" +
"<tr>\n" +
"<td align='center'>\n" +
"<table width='600' border='0' align='center' cellpadding='0' cellspacing='0' class='table-inner'>\n" +
"<tbody>\n" +
"<tr>\n" +
"<td height='70'></td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td data-link-style='text-decoration:none; color:#FFFFFF;' data-link-color='Slogan Link' data-color='Slogan' data-size='Slogan' mc:edit='header-6-2' align='left' style='font-family: Open Sans; color:#FFFFFF; font-size:35px; letter-spacing: 2px;line-height: 38px; font-weight: 600; font-style:normal;'>\n" +
"<singleline label='slogan'>Star Performers | Nominations</singleline>\n" +
"</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td height='60'></td>\n" +
"</tr>\n" +
"</tbody>\n" +
"</table>\n" +
"</td>\n" +
"</tr>\n" +
"</tbody>\n" +
"</table>\n" +
"</td>\n" +
"</tr>\n" +
"</tbody>\n" +
"</table>\n" +
"<table data-module='1-1-cta-3' data-bgcolor='Main BG 2' bgcolor='#ffffff' align='center' width='100%' border='0' cellspacing='0' cellpadding='0' class='' style='position: relative; opacity: 1; top: 0px; left: 0px;'>\n" +
"<tbody>\n" +
"<tr>\n" +
"<td align='center'>\n" +
"<table class='table-inner' width='600' border='0' align='center' cellpadding='0' cellspacing='0'>\n" +
"<tbody>\n" +
"<tr>\n" +
"<td height='35'></td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td data-link-style='text-decoration:none; color:#8c8c8c;' data-link-color='Content Link' data-color='Content' data-size='Content' mc:edit='1/1-cta-3-2' align='justify' style='font-family: oPen sAns; font-size:14px;color:#7f8c8d;line-height: 28px;'>\n" +
"<multiline label='content'>\n" +
"<b>Hello Team,</b>\n" +
"</multiline>\n" +
"</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td height='15'></td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td data-link-style='text-decoration:none; color:#8c8c8c;' data-link-color='Content Link' data-color='Content' data-size='Content' mc:edit='1/1-cta-3-2' align='justify' style='font-family: oPen sAns; font-size:14px;color:#7f8c8d;line-height: 28px;'>\n" +
"<multiline label='content'>\n" +
"Know of someone who is doing a great job? Give them a chance by nominating them to be the next star performer for the month of <b>"+time+"</b>. Click on the below button and it will redirect you to a short and easy form. Dont miss out on this chance as you may have a dedicated employee right in front of you :)\n" +
"</multiline>\n" +
"</td>\n" +
"</tr>\n" +
"</tbody></table>                  <table class='table-inner' width='600' border='0' align='center' cellpadding='0' cellspacing='0'>\n" +
"<tbody>\n" +
"\n" +
"<tr>\n" +
"<td align='center' valign='top' style='padding-top: 15px;'>\n" +
"<table class='textbutton' border='0' align='left' cellpadding='0' cellspacing='0' style='margin: 0;'>\n" +
"<tbody>\n" +
"<tr>\n" +
"<td bgcolor='#00aae7' align='center' valign='middle' style='display: inline-blosck; border-radius:5px; padding: 8px 20px; font-weight: normal; font-size: 15px; line-height: 100%; font-family: 'Open Sans'; color:#ffffff; margin: 0 !important; mso-line-height-rule: exactly;'>\n" +
"<span>\n" +
"<a editable='' label='text button' href='"+com.mss.mirage.util.Properties.getProperty("PROD.URL")+"' target='blank' style='text-decoration: none; font-style: normal; font-weight: normal; color:#ffffff;'>\n" +
"<b>Nominate Here</b>\n" +
"</a>\n" +
"</span>\n" +
"</td>\n" +
"</tr>\n" +
"</tbody>\n" +
"</table>\n" +
"</td>\n" +
"</tr>           </tbody>\n" +
"</table></td></tr></tbody></table>\n" +
"\n" +
"<table class='table-inner' width='600' border='0' align='center' cellpadding='0' cellspacing='0'>\n" +
"<tbody>\n" +
"\n" +
"<tr>\n" +
"<td height='15'></td>\n" +
"</tr> <tr>\n" +
"<td data-link-style='text-decoration:none; color:#ef4048;' data-link-color='Content Link' data-color='Content' data-size='Content' mc:edit='1/1-cta-3-2' align='justify' style='font-family: oPen sAns; font-size:14px;color:#ef4048;line-height: 28px;'>\n" +
"<multiline label='content'>\n" +
"<i>Please note that you are eligible to nominate only three employees per month.</i>\n" +
"</multiline>\n" +
"</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td height='15'></td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td data-link-style='text-decoration:none; color:#8c8c8c;' data-link-color='Content Link' data-color='Content' data-size='Content' mc:edit='1/1-cta-3-2' align='justify' style='font-family: oPen sAns; font-style: normal; font-size:14px;color:#7f8c8d;line-height: 28px;'>\n" +
"<multiline label='content'>\n" +
"<b>Thanks and Regards,\n" +
"<br>Marketing Team </b>\n" +
"<br> Miracle Software Systems, Inc.\n" +
"<br> Email : <a href='mailto:marketing@miraclesoft.com'><span style='color:#7f8c8d;'>marketing@miraclesoft.com</span></a>\n" +
"</multiline>\n" +
"</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td height='35'></td>\n" +
"</tr>\n" +
"</tbody>\n" +
"</table>\n" +
"\n" +
"\n" +
"\n" +
"\n" +
"<table data-module='footer-6' data-bgcolor='Main BG' bgcolor='#FFFFFF' width='100%' border='0' align='center' cellpadding='0' cellspacing='0'>\n" +
"<tbody>\n" +
"<tr>\n" +
"<td data-bgcolor='Footer Bar' data-border-top-color='Footer Border Top' align='center' bgcolor='#414a51' style=' border-top:3px solid #8c8c8c;'>\n" +
"<table class='table-inner' width='600' border='0' align='center' cellpadding='0' cellspacing='0' style='margin-left:20px; margin-right:20px;'>\n" +
"<tbody>\n" +
"<tr>\n" +
"<td height='15'></td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td>\n" +
"<table align='right' class='table-full' border='0' cellspacing='0' cellpadding='0'>\n" +
"<tbody>\n" +
"<tr>\n" +
"<td height='5'></td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td>\n" +
"<table border='0' align='center' cellpadding='0' cellspacing='0' style='padding-left: 15px;padding-right: 15px;'>\n" +
"<tbody>\n" +
"<tr>\n" +
"<th>\n" +
"<a href='https://www.facebook.com/miracle45625/' target='blank' style='color:#ef4048; text-decoration: none;'><img src='https://www.miraclesoft.com/images/newsletters/facebook1.png' width='20' height='20' alt='#' style='display:block; vertical-align:middle;'>\n" +
"</a>\n" +
"</th>\n" +
"<td align='center' height='1' width='10' style='font-size: 1px; line-height: 1px; width:10px;'>\n" +
"</td>\n" +
"<th>\n" +
"<a href='https://plus.google.com/+Team_MSS' target='blank' style='color:#ef4048; text-decoration: none;'><img src='https://www.miraclesoft.com/images/newsletters/googleplus1.png' width='20' height='20' alt='#' style='display:block; vertical-align:middle;'>\n" +
"</a>\n" +
"</th>\n" +
"<td align='center' height='1' width='10' style='font-size: 1px; line-height: 1px; width:10px;'>\n" +
"</td>\n" +
"<th>\n" +
"<a href='https://www.linkedin.com/company/miracle-software-systems-inc' target='blank' style='color:#ef4048; text-decoration: none;'><img src='https://www.miraclesoft.com/images/newsletters/Linkedin1.png' width='20' height='20' alt='#' style='display:block; vertical-align:middle;'>\n" +
"</a>\n" +
"</th>\n" +
"<td align='center' height='1' width='10' style='font-size: 1px; line-height: 1px; width:10px;'>\n" +
"</td>\n" +
"<th>\n" +
"</th>\n" +
"<td>\n" +
"<a href='https://www.youtube.com/c/Team_MSS' target='blank' style='color:#ef4048; text-decoration: none;'><img src='https://www.miraclesoft.com/images/newsletters/youtube1.png' width='20' height='20' alt='#' style='display:block; vertical-align:middle;'>\n" +
"</a>\n" +
"</td>\n" +
"<td align='center' height='1' width='10' style='font-size: 1px; line-height: 1px; width:10px;'>\n" +
"</td>\n" +
"<th>\n" +
"</th>\n" +
"<td>\n" +
"<a href='https://twitter.com/Team_MSS' target='blank' style='color:#ef4048; text-decoration: none;'><img src='https://www.miraclesoft.com/images/newsletters/Twitter1.png' width='20' height='20' alt='#' style='display:block; vertical-align:middle;'>\n" +
"</a>\n" +
"</td>\n" +
"<td align='center' height='1' width='10' style='font-size: 1px; line-height: 1px; width:10px;'>\n" +
"</td>\n" +
"<th>\n" +
"</th>\n" +
"</tr>\n" +
"</tbody>\n" +
"</table>\n" +
"</td>\n" +
"</tr>\n" +
"</tbody>\n" +
"</table>\n" +
"<table width='1' border='0' cellpadding='0' cellspacing='0' align='right'>\n" +
"<tbody>\n" +
"<tr>\n" +
"<td height='15' style='font-size: 0;line-height: 0px;border-collapse: collapse;'>\n" +
"<p style='padding-left: 24px;'>&nbsp;</p>\n" +
"</td>\n" +
"</tr>\n" +
"</tbody>\n" +
"</table>\n" +
"<table class='table-full' border='0' align='left' cellpadding='0' cellspacing='0'>\n" +
"<tbody>\n" +
"<tr>\n" +
"<td data-link-style='text-decoration:none; color:#ffffff;' data-link-color='Preference Link' data-color='Preference' data-size='Preference' mc:edit='footer-6-5' align='center' class='preference-link' style='font-family: oPen sans; color:#ffffff; font-size:12px; line-height: 28px;font-style: italic;'>© 2017 Miracle Software Systems, Inc</td>\n" +
"</tr>\n" +
"</tbody>\n" +
"</table>\n" +
"</td>\n" +
"</tr>\n" +
"<tr>\n" +
"<td height='15'></td>\n" +
"</tr>\n" +
"</tbody>\n" +
"</table>\n" +
"</td>\n" +
"</tr>\n" +
"</tbody>\n" +
"</table>\n" +
"\n" +
"<span class='gr__tooltip'>\n" +
"<span class='gr__tooltip-content'>\n" +
"</span>\n" +
"<i class='gr__tooltip-logo'>\n" +
"</i>\n" +
"<span class='gr__triangle'>\n" +
"</span>\n" +
"</span>\n" +
"</body></html>";
             



            String campaignTemplate = htmlText.toString();
            //System.out.println("campaignTemplate"+campaignTemplate);
            ///////////////////////////////////////////////////////////////////////////////////////////////

            EmailCampaignService ecs = new EmailCampaignService(Properties.getProperty("API_KEY"), Properties.getProperty("ACCESS_TOCKEN"));


            EmailCampaignRequest emailReq = new EmailCampaignRequest();
            String datetime = new SimpleDateFormat("ddMMMyyyy_hh_mm_ss_SSSa").format(new java.util.Date());

            emailReq.setName("Star Performers Nominations Email " + datetime);
            emailReq.setSubject("Star Performers "+time+" month nominations initiation");
            emailReq.setFromEmail("newsletter@miraclesoft.com ");
            emailReq.setFromName("Miracle Software Systems, Inc.");
            emailReq.setReplyToEmail("newsletter@miraclesoft.com ");
            emailReq.setViewAsWebpageEnabled(false);
            emailReq.setGreetingName("FIRST_NAME");
            emailReq.setGreetingSalutations("Hello");
            emailReq.setGreetingString("Dear");
            emailReq.setEmailContent(campaignTemplate);
            emailReq.setEmailContentFormat("HTML");
                       emailReq.setTextContent("Star Performers "+time+" month nominations initiation");
            MessageFooter messageFooter = new MessageFooter();
            messageFooter.setAddressLine1("45625 Grand River Avenue");
            messageFooter.setOrganizationName("Miracle Software Systems, Inc.");
            messageFooter.setCity("Novi");
            messageFooter.setCountry("US");
            messageFooter.setState("MI");
            messageFooter.setPostalCode("48374");
            emailReq.setMessageFooter(messageFooter);
            List<SentToContactList> scl = new ArrayList();



            SentToContactList stcl = new SentToContactList();


            stcl.setId(contactId);


            scl.add(stcl);
            emailReq.setSentToContactLists(scl);


            //   System.out.println(emailReq.toJSON().toString());
            EmailCampaignResponse errs = ecs.addCampaign(emailReq);

            // System.out.println(errs.getSentToContactLists());



            if (errs.getId() != null && !"".equalsIgnoreCase(errs.getId().trim())) {



                EmailCampaignScheduleService ecsh = new EmailCampaignScheduleService(Properties.getProperty("API_KEY"), Properties.getProperty("ACCESS_TOCKEN"));



                EmailCampaignResponse ecrsp = ecs.getCampaign(errs.getId());

                EmailCampaignSchedule Ecs = new EmailCampaignSchedule();

              

                EmailCampaignSchedule finalEcs = null;
                int size = 0;
                while (size <= i) {
                    com.constantcontact.components.generic.response.ResultSet<Contact> c = cls.getContactsFromList(contactId, 500, null);

                    size = c.size();


                    if (size == i) {
                        finalEcs = ecsh.addSchedule(ecrsp.getId(), Ecs);
                        rows = 1;
                        break;
                    }


                }




            }

        }catch (ServiceLocatorException ex) {
            Logger.getLogger(ConstantContactMailManager.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ConstantContactServiceException e) {
            ConstantContactServiceException c = (ConstantContactServiceException) e.getCause();
            for (Iterator<RawApiRequestError> i1 = c.getErrorInfo().iterator(); i1.hasNext();) {
                RawApiRequestError item = i1.next();
                System.out.println(item.getErrorKey() + " : " + item.getErrorMessage());
            }
            e.printStackTrace();
            System.out.println(e.getMessage());
        } catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                    resultSet = null;
                }
                if (preparedStatement != null) {
                    preparedStatement.close();
                    preparedStatement = null;
                }
                if (connection != null) {
                    connection.close();
                    connection = null;
                }
            } catch (Exception ex) {
            	ex.printStackTrace();
            }
        }
        // System.err.println("response string is"+stringBuffer.toString());
        return rows;

    }
}
