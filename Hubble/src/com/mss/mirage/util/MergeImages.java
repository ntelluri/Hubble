package com.mss.mirage.util;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.font.TextAttribute;
import java.awt.geom.Ellipse2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;


import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;

public class MergeImages {
	
private HttpServletResponse httpServletResponse;
public InputStream inputStream;
public OutputStream outputStream;


	public void  mergeImages(String empName,String sMonth,String position,String loginId,String email, File input,File output,String quote,String gender,String titleTypeId)  throws IOException, URISyntaxException, ServiceLocatorException{
		
		String month=sMonth;
		
		
		URL urlInput = null;
		
		
		String image="";
		
		String isEmployeeImageExist = RestRepository.getInstance().isEmployeeImageExistOrNot(loginId);
		if("1".equals(isEmployeeImageExist)){
			 urlInput = new URL(Properties.getProperty("ImagePath")+"/"+loginId+".png");
			// System.out.println(Properties.getProperty("ImagePath")+"/"+loginId+".png");
			 
			 
		}else {
			
			if("Male".equalsIgnoreCase(gender)){
			 urlInput = new URL(Properties.getProperty("ImagePath")+"/"+"NoImage_male.png");
			}
			else if("Female".equalsIgnoreCase(gender)){
			 urlInput = new URL(Properties.getProperty("ImagePath")+"/"+"NoImage_Female.png");
			}
			
		}
		BufferedImage urlImage = resize(ImageIO.read(urlInput), 508, 611);
		int width = urlImage.getWidth();
		BufferedImage circleBuffer = new BufferedImage(width, width, BufferedImage.TYPE_INT_ARGB);
		Graphics2D g2 = circleBuffer.createGraphics();
		g2.setClip(new Ellipse2D.Float(0, 0, width, width));
		g2.drawImage(urlImage, 0, 0, width, width, null);
		
		String positionTitle="";
		
		
		String name= empName;
		if(!"".equals(titleTypeId)&&titleTypeId!=null){
			
			
		 positionTitle = titleTypeId+" - "+position;
		}
		else
			positionTitle =position;
		
		
		
		String tagLine=quote;
		String filePath="";
		
		
			addImage(circleBuffer, "png", input, output, name, positionTitle,tagLine);
		
	}

	private static void addImage(BufferedImage urlImage, String type, File source, File destination, String name,String positionTitle, String tagLine) throws IOException {
		
		BufferedImage image = ImageIO.read(source);

		// determine image type
		int imageType = "png".equalsIgnoreCase(type) ? BufferedImage.TYPE_INT_ARGB : BufferedImage.TYPE_INT_RGB;
		BufferedImage backgroundImg = new BufferedImage(image.getWidth(), image.getHeight(), imageType);

		// Initialises necessary graphic properties
		Graphics2D w = (Graphics2D) backgroundImg.getGraphics();

		w.drawImage(image, 0, 0, null);
		
		// calculates the coordinate where the Image is placed
		int centerX =  (int) (image.getWidth() / 2.93);
		int centerY = (int) (image.getHeight() / 7.22);
		
		w.setColor(new Color(0xf4e388));
		w.setFont(new Font("Century Gothic", Font.BOLD, 125));
		drawCenteredString(name, 1960, 2200, w);

		w.setColor(new Color(0xf4e388));
		w.setFont(new Font("Century Gothic",Font.ITALIC+Font.BOLD, 65));
		drawCenteredString(positionTitle, 1960, 2440, w);


		w.setColor(new Color(0x090909));
		w.setFont(new Font("Century Gothic", Font.BOLD + Font.ITALIC, 70));
		Font font = w.getFont();
		font = font.deriveFont(Collections.singletonMap(
				TextAttribute.WEIGHT, TextAttribute.WEIGHT_HEAVY
				));
		w.setFont(font);
		String linesString=splitToNChar(tagLine);
		String[] terms = linesString.substring(1,linesString.length()-1).split(",");
		int height=2810;
		for(int i=0;i<terms.length;i++)
		{
			drawCenteredString(terms[i], 1960, height, w);
			height+=190;
		}
		w.drawImage(urlImage, centerX, centerY, null);
		ImageIO.write(backgroundImg, type, destination);
		w.dispose();
	}


	public static void drawCenteredString(String s, int w, int h, Graphics g) {
		FontMetrics fm = g.getFontMetrics();
		int x = (w - fm.stringWidth(s)) / 2;
		int y = (fm.getAscent() + (h - (fm.getAscent() + fm.getDescent())) / 2);
		g.drawString(s, x, y);
	}

	private static BufferedImage resize(BufferedImage img, int height, int width) {
		Image tmp = img.getScaledInstance(width, height, Image.SCALE_SMOOTH);
		BufferedImage resized = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		Graphics2D g2d = resized.createGraphics();
		g2d.drawImage(tmp, 0, 0, null);
		g2d.dispose();
		return resized;
	}

	

	private static String splitToNChar(String text) {
		int length = 0;
		String textArray[]=text.split(" ");
		String line="";
		List<String> lines=new ArrayList<String>();
		int textLength=textArray.length-1;
		for (int i = 0; i < textArray.length; i++) {
			length+=textArray[i].length();
			if(length<40)
			{
				line+=textArray[i]+" ";
			}
			else
			{
				lines.add(line);
				line="";
				length=0;
				i=i-1;
			}
			 if(i == textLength)
			{
				lines.add(line);
				line="";
				length=0;
				
			}
		}
		return Arrays.toString(lines.toArray());
	}

}
