package com.mss.mirage.util;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.sql.SQLException;
import java.util.Hashtable;

import org.json.JSONException;
import org.json.JSONObject;
import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.BasicAttributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.ModificationItem;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import jcifs.util.Base64;
import sun.misc.BASE64Decoder;
public class LdapServiceProvider {

	
	 
	 public static boolean empLDAPUpdate(JSONObject jObject) {

			String base = SecurityProperties.getProperty("LDAP_BASIC").trim();
			boolean  resflag=false;
			JSONObject mainJson = new JSONObject();
			
			Hashtable srchEnv = new Hashtable(11);
			srchEnv.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
			srchEnv.put(Context.PROVIDER_URL, SecurityProperties.getProperty("LDAP_PROVIDER_URL").trim()); // ldap://localhost:389
			srchEnv.put(Context.SECURITY_AUTHENTICATION, SecurityProperties.getProperty("LDAP_SECURITY_AUTHENTICATION").trim());
			srchEnv.put(Context.SECURITY_PRINCIPAL, SecurityProperties.getProperty("LDAP_SECURITY_PRINCIPAL").trim());
			srchEnv.put(Context.SECURITY_CREDENTIALS, SecurityProperties.getProperty("LDAP_SECURITY_CREDENTIALS").trim());

			try {
				DirContext srchContext = new InitialDirContext(srchEnv);
			
				  resflag= empLDAPUpdate1(srchContext, base, jObject);
				
				
			} catch (Exception e) {
				e.printStackTrace();

			}

			return resflag;

		}

		public static boolean empLDAPUpdate1(DirContext dirContext, String base, JSONObject jObject) {
			boolean flag = false;

			ModificationItem[] modItemsOne=new ModificationItem[1];
			

			try {
				
				SearchControls srchControls = new SearchControls();
				
				srchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);
				
				String searchFilter = "(&(objectClass=*)(|(cn=" + jObject.getString("loginId") + ")))";
			
				NamingEnumeration srchResponse = dirContext.search(base, searchFilter, srchControls);
		        
				while (srchResponse.hasMore()) {
					SearchResult result = (SearchResult) srchResponse.next();
					String distinguishedName = result.getNameInNamespace();
					
				modItemsOne[0] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE,
						
			    new BasicAttribute(jObject.getString("upDateAttibute"), jObject.getString("upDateValue")));
			 
		    	
				dirContext.modifyAttributes(distinguishedName, modItemsOne);
				flag = true;}
			} catch (Exception e) {
			e.printStackTrace();
			return flag;
			}
			return flag;
		}

 
		
		 public static boolean empLDAPPswdUpdate(JSONObject jObject) {

				String base = SecurityProperties.getProperty("LDAP_BASIC").trim();
				boolean  resflag=false;
				JSONObject mainJson = new JSONObject();
				
				Hashtable srchEnv = new Hashtable(11);
				srchEnv.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
				srchEnv.put(Context.PROVIDER_URL, SecurityProperties.getProperty("LDAP_PROVIDER_URL").trim()); // ldap://localhost:389
				srchEnv.put(Context.SECURITY_AUTHENTICATION, SecurityProperties.getProperty("LDAP_SECURITY_AUTHENTICATION").trim());
				srchEnv.put(Context.SECURITY_PRINCIPAL, SecurityProperties.getProperty("LDAP_SECURITY_PRINCIPAL").trim());
				srchEnv.put(Context.SECURITY_CREDENTIALS, SecurityProperties.getProperty("LDAP_SECURITY_CREDENTIALS").trim());

				try {
					DirContext srchContext = new InitialDirContext(srchEnv);
				
					  resflag= empLDAPPswdUpdate1(srchContext, base, jObject);
					
					
				} catch (Exception e) {
					e.printStackTrace();

				}

				return resflag;

			}

			public static boolean empLDAPPswdUpdate1(DirContext dirContext, String base, JSONObject jObject) {
				boolean flag = false;

				ModificationItem[] modItemsOne=new ModificationItem[1];
				

				try {
					
					SearchControls srchControls = new SearchControls();
					
					srchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);
					
					String searchFilter = "(&(objectClass=*)(|(cn=" + jObject.getString("loginId") + ")))";
				
					NamingEnumeration srchResponse = dirContext.search(base, searchFilter, srchControls);
			        
					while (srchResponse.hasMore()) {
						SearchResult result = (SearchResult) srchResponse.next();
						String distinguishedName = result.getNameInNamespace();
						
					modItemsOne[0] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE,
							
				    new BasicAttribute(jObject.getString("upDateAttibute"), hashMD5Password(jObject.getString("upDateValue"))));
				 
			    	
					dirContext.modifyAttributes(distinguishedName, modItemsOne);
					flag = true;}
				} catch (Exception e) {
				e.printStackTrace();
				return flag;
				}
				return flag;
			}

		
			 public static String hashMD5Password(String password) throws NoSuchAlgorithmException,
			 UnsupportedEncodingException {
			 MessageDigest digest = MessageDigest.getInstance("MD5");
			 digest.update(password.getBytes("UTF8"));
			 String md5Password = Base64.encode(digest.digest());
			// System.out.println("md5Password-->" + md5Password);

			 return "{MD5}" + md5Password;
			 }
			 
			 public static  boolean addUserEntry(JSONObject jObject) {

				 boolean b =false;
				 JSONObject mainJson = null;
				 Hashtable srchEnv = new Hashtable(11);
					srchEnv.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
					srchEnv.put(Context.PROVIDER_URL, SecurityProperties.getProperty("LDAP_PROVIDER_URL").trim()); // ldap://localhost:389
					srchEnv.put(Context.SECURITY_AUTHENTICATION, SecurityProperties.getProperty("LDAP_SECURITY_AUTHENTICATION").trim());
					srchEnv.put(Context.SECURITY_PRINCIPAL, SecurityProperties.getProperty("LDAP_SECURITY_PRINCIPAL").trim());
					srchEnv.put(Context.SECURITY_CREDENTIALS, SecurityProperties.getProperty("LDAP_SECURITY_CREDENTIALS").trim());


				try {
					mainJson=new JSONObject();
					DirContext srchContext = new InitialDirContext(srchEnv);
					
					 b = addEntry(srchContext, jObject);
					
		
				} catch (Exception e) {
					e.printStackTrace();

				}

				return b;

			}

			public static boolean addEntry(DirContext dirContext, JSONObject jObject
					) throws JSONException, NoSuchAlgorithmException, UnsupportedEncodingException {
				
				String base = SecurityProperties.getProperty("LDAP_BASIC").trim();
				boolean flag = false;
				Attribute userCn = new BasicAttribute("cn", jObject.getString("userId"));
				Attribute userSn = new BasicAttribute("sn", jObject.getString("lastName"));
				Attribute userMail = new BasicAttribute("mail",jObject.getString("emailId"));
				Attribute userPhone = new BasicAttribute("Mobile",jObject.getString("mobile"));
				Attribute userGivename = new BasicAttribute("givenname",  jObject.getString("firstName"));
				Attribute employeeType= new BasicAttribute("employeeType", "Active");

				Attribute initials=null;
				if(!"".equals(jObject.get("middleName").toString().trim())){
								initials= new BasicAttribute("initials", jObject.get("middleName"));}
			
			 Attribute userUserPassword = new BasicAttribute("userPassword",  hashMD5Password(jObject.getString("plainPassword")));
			 Attribute uid = new BasicAttribute("uid", jObject.getString("userId"));
			 
			
				// ObjectClass attributes
				Attribute oc = new BasicAttribute("objectClass");
			
				oc.add("inetOrgPerson");
			//	oc.add("posixAccount");
				
							
				
				Attributes entry = new BasicAttributes();
				entry.put(userCn);
				entry.put(userSn);
				entry.put(userMail);
				entry.put(userPhone);
				entry.put(userGivename);
				entry.put(employeeType);
				entry.put(userUserPassword);
				if(initials!=null){
					entry.put(initials);}
				
				
				entry.put(uid);
		
				entry.put(oc);

				String entryDN = "cn=" + jObject.getString("userId") + "," + base;
			
				try {
					DirContext srchContext=	dirContext.createSubcontext(entryDN, entry);
					if(srchContext!=null){
						flag = true;
					}else{
						flag = false;
					}
					
				} catch (Exception e) {
					System.out.println("error: " + e.getMessage());
					return flag;
				}
				return flag;
			}
			  public static JSONObject empProfilePicUpdate(JSONObject jObject) {

				  String base = SecurityProperties.getProperty("LDAP_BASIC").trim();

				  JSONObject mainJson = new JSONObject();


				  Hashtable srchEnv = new Hashtable(11);
				  srchEnv.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
				  srchEnv.put(Context.PROVIDER_URL, SecurityProperties.getProperty("LDAP_PROVIDER_URL").trim()); // ldap://localhost:389
				  srchEnv.put(Context.SECURITY_AUTHENTICATION, SecurityProperties.getProperty("LDAP_SECURITY_AUTHENTICATION").trim());
				  srchEnv.put(Context.SECURITY_PRINCIPAL, SecurityProperties.getProperty("LDAP_SECURITY_PRINCIPAL").trim());
				  srchEnv.put(Context.SECURITY_CREDENTIALS, SecurityProperties.getProperty("LDAP_SECURITY_CREDENTIALS").trim());

					try {
						DirContext srchContext = new InitialDirContext(srchEnv);
						//System.out.println("Boolean value-->" + jObject);
						boolean  resflag= empProfilePictureUpdateNew(srchContext, base, jObject);
						//System.out.println("Boolean value-->" + resflag);
						if(resflag){
							mainJson.put("isAdded","Yes");
							mainJson.put("result","updated Successfully");
						}else{
							mainJson.put("isAdded","No");
							mainJson.put("result","Please try again");
						}
					} catch (Exception e) {
						e.printStackTrace();

					}

					return mainJson;

				}


			public static boolean empProfilePictureUpdate(DirContext dirContext, String base, JSONObject jObject) {
				boolean flag = false;

				ModificationItem[] modItemsOne=new ModificationItem[1];
				

				try {
					
					SearchControls srchControls = new SearchControls();
					
					srchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);
					
					String searchFilter = "(&(objectClass=*)(|(cn=" + jObject.getString("loginId") + ")))";
				
					NamingEnumeration srchResponse = dirContext.search(base, searchFilter, srchControls);
			         //  System.out.println(srchResponse);
					while (srchResponse.hasMore()) {
						SearchResult result = (SearchResult) srchResponse.next();
						String distinguishedName = result.getNameInNamespace();
					
					
					File file=null;
						 file = new File(jObject.getString("ImageDetails"));
//						 if(!file.exists())
//							 file = new File("/opt/lampp/htdocs/msws/images/employee-profile-pics/noImage.jpg");
						FileInputStream fis = null;
						 byte[] binaryData =new byte[(int)file.length()];
						
						try{
						fis = new FileInputStream(file);
						
						    fis.read(binaryData);
						}catch (Exception ex){
							ex.printStackTrace();
						}
					
			
					modItemsOne[0] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE,
							
							new BasicAttribute("jpegPhoto",binaryData));
				    
					 String entryDN = distinguishedName;

				
			     	 // System.out.println("entryDN :" + entryDN);

					dirContext.modifyAttributes(distinguishedName, modItemsOne);
					flag = true;}
				} catch (Exception e) {
				e.printStackTrace();
				return flag;
				}
				return flag;
			}
			
			
			
			public static boolean empProfilePictureUpdateNew(DirContext dirContext, String base, JSONObject jObject) {
				boolean flag = false;

				ModificationItem[] modItemsOne=new ModificationItem[1];

				String basePath = "";
				JSONObject mainJson = new JSONObject();
				try {

				SearchControls srchControls = new SearchControls();

				srchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);

				String searchFilter = "(&(objectClass=*)(|(cn=" + jObject.getString("LoginId") + ")))";

				NamingEnumeration srchResponse = dirContext.search(base, searchFilter, srchControls);
				// System.out.println(srchResponse);
				while (srchResponse.hasMore()) {
				SearchResult result = (SearchResult) srchResponse.next();
				String distinguishedName = result.getNameInNamespace();

				try{


				String fileName =jObject.getString("ImageName");
				String base64String="";
				String loginId=jObject.getString("LoginId");
				String ImageDetails="";


				if (fileName != null && !"".equals(fileName)) {
				base64String = jObject.getString("Base64String");


				//imagePath =getFilePathOfBase64ToImage(Base64String,EventFileFileName);



				String[] parts = base64String.split(",");
				String imageString = parts[1];
				String[] imageType = parts[0].split(";");
				String[] imageType1 = imageType[0].split("/");

				BufferedImage image = null;
				byte[] imageByte=base64String.getBytes();

				BASE64Decoder decoder = new BASE64Decoder();
				imageByte = decoder.decodeBuffer(imageString);

				modItemsOne[0] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE,

				new BasicAttribute("jpegPhoto",imageByte));

				String entryDN = distinguishedName;


				// System.out.println("entryDN :" + entryDN);

				dirContext.modifyAttributes(distinguishedName, modItemsOne);
				flag = true;


				}




				}

				catch (Exception ex){
				flag = false;
				ex.printStackTrace();
				}


				}
				} catch (Exception e) {
				e.printStackTrace();
				return flag;
				}
				return flag;
				}



		
}
