/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mss.mirage.util;
import com.constantcontact.ConstantContactFactory;
import com.constantcontact.components.activities.contacts.request.AddContactsRequest;
import com.constantcontact.components.activities.contacts.request.ContactData;
import com.constantcontact.components.contacts.Contact;
import com.constantcontact.components.contacts.ContactList;
import com.constantcontact.components.contacts.EmailAddress;
import com.constantcontact.exceptions.service.ConstantContactServiceException;
import com.constantcontact.pagination.PaginationHelperService;
import com.constantcontact.services.activities.IBulkActivitiesService;
import com.constantcontact.services.contactlists.ContactListService;
import com.constantcontact.services.contactlists.IContactListService;
import com.constantcontact.services.contacts.ContactService;
import com.constantcontact.util.RawApiRequestError;
import java.sql.*;
import java.util.*;
import java.util.Properties;
import java.util.logging.Level;
import org.apache.log4j.Logger;
/**
 *
 * @author miracle
 */
public class IntegratedHubbleContactsWithCC {
   
    static Properties loadProperties = null;
    public static String monthName = "";
    public static String url;
    public static String databaseName;
    public static String driverName;
    public static String userName;
    public static String password;
    public static String Api_Key;
    public static String Access_Tocken;
    static Logger reportsLog = Logger.getLogger(IntegratedHubbleContactsWithCC.class);

    static {
        // LoggingUtil.loadLoggerClass();

        //loadProperties = PropertiesUtil.loadPropertiesFile("reports.properties");

       // url = loadProperties.getProperty("URL");
        url ="jdbc:mysql://192.168.1.131:3306/";
         databaseName = "mirage";
        driverName = "com.mysql.jdbc.Driver";
        userName = "AppAdmin";
        password = "lokam001";
        Api_Key = "acdd2543-7064-41fe-a4bc-d62ea15b9f3e";
        Access_Tocken = "5q952gpm3sn79yvxc2s9hptq";
        
         
        reportsLog.info("Loading The Properties File");
    }

    public static void main(String args[]) {

        Connection connection = null;
        String resultString = "";
        Statement statement = null;
        ResultSet resultSet = null;
        long end = 0;
        long start = 0;
        try {


            Class.forName(driverName).newInstance();
            connection = DriverManager.getConnection(url + databaseName, userName, password);
            reportsLog.info("Connection established ..");

            if (connection != null) {


                start = System.currentTimeMillis();
                statement = connection.createStatement();
                resultSet = statement.executeQuery("SELECT DISTINCT Location FROM tblLKEmpLocations WHERE STATUS='Active'");

                while (resultSet.next()) {

                    resultString = doSynchronizeToHubble1(resultSet.getString("Location"));
                    reportsLog.info(resultString);

                   // System.out.println("........&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&.........");
                }
                reportsLog.info("Execution time showing");
                end = System.currentTimeMillis();
                //System.out.println(" Execution Time-->"+(end - start) / 1000+" seconds..");

                reportsLog.info("Execution Time-->" + (end - start) / 1000 + " seconds..");


            }
            
            doCreateSalesList("Miracle Sales");
            doCreatePresalesList("Miracle Presales");

        } catch (SQLException ex) {
            ex.printStackTrace();
        } catch (Exception sle) {
            sle.printStackTrace();
        } finally {
            try {

                if (resultSet != null) {
                    resultSet.close();
                    resultSet = null;
                }
                if (statement != null) {
                    statement.close();
                    statement = null;
                }
                if (connection != null) {
                    connection.close();

                    connection = null;
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
            //return connection;
        }
    }

   
	
	public static String doSynchronizeToHubble1(String contactListName) throws ConstantContactServiceException {

String responseString = "";
Connection connection = null;
PreparedStatement preparedStatement = null;
ResultSet resultSet = null;
try {
/* new code */
ContactListService cls=new ContactListService(Api_Key, Access_Tocken);

String contactId="";
List<ContactList> clst= cls.getLists(null);

for(int j=0;j<clst.size();j++){

if(clst.get(j).getName().equals(contactListName)){
contactId=clst.get(j).getId();
}
}
if("".equals(contactId.toString().trim())){
ContactList contList = new ContactList();
contList.setName(contactListName);
contList.setStatus("ACTIVE");
contList = cls.addList(contList);
contactId = contList.getId();
System.out.println("contactId"+contactId);
}



ConstantContactFactory cf = new ConstantContactFactory(Api_Key, Access_Tocken);
IBulkActivitiesService _activityService = cf.createBulkActivitiesService();



AddContactsRequest contactsToAdd = new AddContactsRequest();
List<String> contactLists = new ArrayList<String>();
List<String> columns = new ArrayList<String>();
List<ContactData> importData = new ArrayList<ContactData>();

contactLists.add(contactId); // NOTE: Replace this with the IF for the list you wish to use
columns.add("EMAIL ADDRESS");
columns.add("FIRST NAME");
columns.add("LAST NAME");
columns.add("Company Name");
contactsToAdd.setColumnNames(columns);
contactsToAdd.setLists(contactLists);




connection = DriverManager.getConnection(url + databaseName, userName, password);




String query = "SELECT FName,LName,OrgId,CurStatus,Email1 FROM tblEmployee WHERE CurStatus='Active' And Location IN('" + contactListName + "') AND Email1 NOT LIKE '%_civil%'";
System.out.println("query-----" + query);

preparedStatement = connection.prepareStatement(query);

resultSet = preparedStatement.executeQuery();


int i=0;
while (resultSet.next()) {

ContactData newContact = new ContactData();
List<String> emails = new ArrayList<String>();
emails.add(resultSet.getString("Email1"));
newContact.setEmailAddresses(emails);
newContact.setFirstName(resultSet.getString("FName"));
newContact.setLastName(resultSet.getString("LName"));
newContact.setCompanyName(resultSet.getString("OrgId"));
importData.add(newContact);
i++;
}
//if(resultSet.next()){
if(i>0){
contactsToAdd.setImportData(importData);

_activityService.addContacts(contactsToAdd);
}
//}

// RemoveContactsRequest rcr=new RemoveContactsRequest();

responseString = "successfully synchronized"+contactListName;
// reportsLog.info("successfully synchronized"+contactListName);
} catch (SQLException ex) {
java.util.logging.Logger.getLogger(IntegratedHubbleContactsWithCC .class.getName()).log(Level.SEVERE, null, ex);
} catch (ConstantContactServiceException e) {
ConstantContactServiceException c = (ConstantContactServiceException) e.getCause();
for (Iterator<RawApiRequestError> i = c.getErrorInfo().iterator(); i.hasNext();) {
RawApiRequestError item = i.next();
System.out.println(item.getErrorKey() + " : " + item.getErrorMessage());
}
e.printStackTrace();
System.out.println(e.getMessage());
} finally {
try {
if (preparedStatement != null) {
preparedStatement.close();
preparedStatement = null;
}
if (connection != null) {
connection.close();
connection = null;
}

} catch (Exception e) {
}
}
System.out.println("responseString" + responseString);
return responseString;
}
 
	
	public static String doCreatePresalesList(String contactListName) throws ConstantContactServiceException {

String responseString = "";
Connection connection = null;
PreparedStatement preparedStatement = null;
ResultSet resultSet = null;
try {
/* new code */
ContactListService cls=new ContactListService(Api_Key, Access_Tocken);

String contactId="";
List<ContactList> clst= cls.getLists(null);

for(int j=0;j<clst.size();j++){

if(clst.get(j).getName().equals(contactListName)){
contactId=clst.get(j).getId();
}
}
if("".equals(contactId.toString().trim())){
ContactList contList = new ContactList();
contList.setName(contactListName);
contList.setStatus("ACTIVE");
contList = cls.addList(contList);
contactId = contList.getId();
System.out.println("contactId"+contactId);
}



ConstantContactFactory cf = new ConstantContactFactory(Api_Key, Access_Tocken);
IBulkActivitiesService _activityService = cf.createBulkActivitiesService();



AddContactsRequest contactsToAdd = new AddContactsRequest();
List<String> contactLists = new ArrayList<String>();
List<String> columns = new ArrayList<String>();
List<ContactData> importData = new ArrayList<ContactData>();

contactLists.add(contactId); // NOTE: Replace this with the IF for the list you wish to use
columns.add("EMAIL ADDRESS");
columns.add("FIRST NAME");
columns.add("LAST NAME");
columns.add("Company Name");
contactsToAdd.setColumnNames(columns);
contactsToAdd.setLists(contactLists);




connection = DriverManager.getConnection(url + databaseName, userName, password);




String query = "SELECT FName,LName,OrgId,CurStatus,Email1 FROM tblEmployee WHERE Id IN (SELECT EmpId FROM tblEmpRoles WHERE RoleId IN (10)) AND CurStatus='Active' AND Email1 NOT LIKE '%_civil%'";
System.out.println("query-----" + query);

preparedStatement = connection.prepareStatement(query);

resultSet = preparedStatement.executeQuery();


int i=0;
while (resultSet.next()) {

ContactData newContact = new ContactData();
List<String> emails = new ArrayList<String>();
emails.add(resultSet.getString("Email1"));
newContact.setEmailAddresses(emails);
newContact.setFirstName(resultSet.getString("FName"));
newContact.setLastName(resultSet.getString("LName"));
newContact.setCompanyName(resultSet.getString("OrgId"));
importData.add(newContact);
i++;
}
//if(resultSet.next()){
if(i>0){
contactsToAdd.setImportData(importData);

_activityService.addContacts(contactsToAdd);
}
//}

// RemoveContactsRequest rcr=new RemoveContactsRequest();

responseString = "successfully synchronized"+contactListName;
// reportsLog.info("successfully synchronized"+contactListName);
} catch (SQLException ex) {
java.util.logging.Logger.getLogger(IntegratedHubbleContactsWithCC .class.getName()).log(Level.SEVERE, null, ex);
} catch (ConstantContactServiceException e) {
ConstantContactServiceException c = (ConstantContactServiceException) e.getCause();
for (Iterator<RawApiRequestError> i = c.getErrorInfo().iterator(); i.hasNext();) {
RawApiRequestError item = i.next();
System.out.println(item.getErrorKey() + " : " + item.getErrorMessage());
}
e.printStackTrace();
System.out.println(e.getMessage());
} finally {
try {
if (preparedStatement != null) {
preparedStatement.close();
preparedStatement = null;
}
if (connection != null) {
connection.close();
connection = null;
}

} catch (Exception e) {
}
}
System.out.println("responseString" + responseString);
return responseString;
}	
	
        
 public static String doCreateSalesList(String contactListName) throws ConstantContactServiceException {

String responseString = "";
Connection connection = null;
PreparedStatement preparedStatement = null;
ResultSet resultSet = null;
try {
/* new code */
ContactListService cls=new ContactListService(Api_Key, Access_Tocken);

String contactId="";
List<ContactList> clst= cls.getLists(null);

for(int j=0;j<clst.size();j++){

if(clst.get(j).getName().equals(contactListName)){
contactId=clst.get(j).getId();
}
}
if("".equals(contactId.toString().trim())){
ContactList contList = new ContactList();
contList.setName(contactListName);
contList.setStatus("ACTIVE");
contList = cls.addList(contList);
contactId = contList.getId();
System.out.println("contactId"+contactId);
}



ConstantContactFactory cf = new ConstantContactFactory(Api_Key, Access_Tocken);
IBulkActivitiesService _activityService = cf.createBulkActivitiesService();



AddContactsRequest contactsToAdd = new AddContactsRequest();
List<String> contactLists = new ArrayList<String>();
List<String> columns = new ArrayList<String>();
List<ContactData> importData = new ArrayList<ContactData>();

contactLists.add(contactId); // NOTE: Replace this with the IF for the list you wish to use
columns.add("EMAIL ADDRESS");
columns.add("FIRST NAME");
columns.add("LAST NAME");
columns.add("Company Name");
contactsToAdd.setColumnNames(columns);
contactsToAdd.setLists(contactLists);




connection = DriverManager.getConnection(url + databaseName, userName, password);




String query = "SELECT FName,LName,OrgId,CurStatus,Email1 FROM tblEmployee WHERE Id IN (SELECT EmpId FROM tblEmpRoles WHERE RoleId IN (4)) AND CurStatus='Active' AND Email1 NOT LIKE '%_civil%'";
System.out.println("query-----" + query);

preparedStatement = connection.prepareStatement(query);

resultSet = preparedStatement.executeQuery();


int i=0;
while (resultSet.next()) {
//System.out.println("Email-->"+resultSet.getString("Email1"));
ContactData newContact = new ContactData();
List<String> emails = new ArrayList<String>();
emails.add(resultSet.getString("Email1"));
newContact.setEmailAddresses(emails);
newContact.setFirstName(resultSet.getString("FName"));
newContact.setLastName(resultSet.getString("LName"));
newContact.setCompanyName(resultSet.getString("OrgId"));
importData.add(newContact);
i++;
}
//if(resultSet.next()){
if(i>0){
contactsToAdd.setImportData(importData);

_activityService.addContacts(contactsToAdd);
}
//}

// RemoveContactsRequest rcr=new RemoveContactsRequest();

responseString = "successfully synchronized"+contactListName;
// reportsLog.info("successfully synchronized"+contactListName);
} catch (SQLException ex) {
java.util.logging.Logger.getLogger(IntegratedHubbleContactsWithCC .class.getName()).log(Level.SEVERE, null, ex);
} catch (ConstantContactServiceException e) {
ConstantContactServiceException c = (ConstantContactServiceException) e.getCause();
for (Iterator<RawApiRequestError> i = c.getErrorInfo().iterator(); i.hasNext();) {
RawApiRequestError item = i.next();
System.out.println(item.getErrorKey() + " : " + item.getErrorMessage());
}
e.printStackTrace();
System.out.println(e.getMessage());
} finally {
try {
if (preparedStatement != null) {
preparedStatement.close();
preparedStatement = null;
}
if (connection != null) {
connection.close();
connection = null;
}

} catch (Exception e) {
}
}
System.out.println("responseString" + responseString);
return responseString;
}	       
        
        
        
        
        
}
