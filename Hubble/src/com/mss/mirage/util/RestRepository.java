/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mss.mirage.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.json.JSONObject;

import com.mss.mirage.ajax.AjaxHandlerAction;

/**
 *
 * @author miracle
 */
public class RestRepository {
     private static RestRepository _instance;
     public static RestRepository getInstance() throws ServiceLocatorException {
        try {
            if(_instance==null) {
                _instance = new RestRepository();
            }
        } catch (Exception ex) {
            throw new ServiceLocatorException(ex);
        }
        return _instance;
    }
     
     public String getSrviceUrl(String serviceName) throws ServiceLocatorException{
     //   System.out.println("serviceName---11--->"+serviceName);
        String serviceUrl = "";
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        connection = ConnectionProvider.getInstance().getConnection();
        String queryString = "SELECT ServiceUrl FROM tblRestSrcReg WHERE STATUS = 'Active' AND ServiceName = '"+serviceName+"'";
      //  System.out.println("queryString---11--->"+queryString);
        try {
            String serviceBaseUrl=Properties.getProperty("WEB.REST.URL");
            statement = connection.createStatement();
            resultSet = statement.executeQuery(queryString);
           if(resultSet.next()){
                serviceUrl =  serviceBaseUrl+resultSet.getString("ServiceUrl");
            }            
        }catch (SQLException ex) {
            ex.printStackTrace();
        }finally {
            
            try {
                // resultSet Object Checking if it's null then close and set null
                if(resultSet != null) {
                    resultSet.close();
                    resultSet = null;
                }
                
                if(statement != null) {
                    statement.close();
                    statement = null;
                }
                
                if(connection != null) {
                    connection.close();
                    connection = null;
                }
            } catch (SQLException ex) {
                throw new ServiceLocatorException(ex);
            }
        }
        
      //  System.out.println("serviceUrl---11--->"+serviceUrl);
        return serviceUrl;
    }
     
     
     
     
     public String isEmployeeImageExistOrNot(String  loginId)   {
         
  String message ="";
      
         try {
         	
         	
  JSONObject jb = new JSONObject();
              
              jb.put("LoginId",loginId);
            
          String serviceUrl = RestRepository.getInstance().getSrviceUrl("isEmployeeImageExistOrNot");
               URL url = new URL(serviceUrl);
             URLConnection connection = url.openConnection();
             connection.setDoOutput(true);
             connection.setRequestProperty("Content-Type", "application/json");
             connection.setConnectTimeout(360*5000);
             connection.setReadTimeout(360*5000);
             OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
            out.write(jb.toString());
             out.close();

             BufferedReader in = new BufferedReader(new InputStreamReader(
                     connection.getInputStream()));
             String s = null;
             String data = "";
             while ((s = in.readLine()) != null) {

                 data = data + s;
             }

         JSONObject jObject = new JSONObject(data);

          message = jObject.getString("message");
      
             in.close();
             
         } catch (Exception ex) {
             ex.printStackTrace();
         }
         return message;
     }
     
     
  public String getAttachmentBase64ByPath(String filePath) throws ServiceLocatorException {
         
        
         
         String base64File="";
         try {

        	 
        	 
             JSONObject jb = new JSONObject();

            
                 jb.put("AttachmentPath", filePath);
            
             
             String serviceUrl=Properties.getProperty("WEB.REST.URL")+"/resources/surveyFormSevices/attachment-by-path";
           //  String serviceUrl = RestRepository.getInstance().getSrviceUrl("getSurveyReviewList");

            // System.out.println("serviceUrl-->" + serviceUrl);
             URL url = new URL(serviceUrl);
             URLConnection connection = url.openConnection();
             connection.setDoOutput(true);
             connection.setRequestProperty("Content-Type", "application/json");
             connection.setConnectTimeout(360*5000);
             connection.setReadTimeout(360*5000);
             OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
             out.write(jb.toString());
             out.close();

             BufferedReader in = new BufferedReader(new InputStreamReader(
                     connection.getInputStream()));
             String s = null;
             String data = "";
             while ((s = in.readLine()) != null) {
                 //     System.out.println(s);
                 data = data + s;
             }
 //System.out.println("data-->"+data);
             JSONObject jObject = new JSONObject(data);

             base64File=jObject.getString("Base64String");
             
            
             //  System.out.println("\nREST Service Invoked Successfully..");
             in.close();
         } catch (Exception e) {
             System.out.println("\nError while calling REST Service");
             System.out.println(e);
         }
         // System.out.println("mainList-->"+mainList);
         return base64File;
     }


}
