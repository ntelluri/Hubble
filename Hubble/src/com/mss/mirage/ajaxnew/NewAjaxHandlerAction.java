/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mss.mirage.ajaxnew;

import com.itextpdf.text.pdf.codec.Base64;
import com.mss.mirage.crm.attachments.AttachmentService;
import com.mss.mirage.util.ApplicationConstants;
import com.mss.mirage.util.AuthorizationManager;
import com.mss.mirage.util.DataSourceDataProvider;
import com.mss.mirage.util.DateUtility;
import com.mss.mirage.util.Properties;
import com.mss.mirage.util.RestRepository;
import com.mss.mirage.util.SecurityProperties;
import com.mss.mirage.util.ServiceLocator;
import com.mss.mirage.util.ServiceLocatorException;
import com.opensymphony.xwork2.ActionSupport;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.io.FileUtils;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author miracle
 */
public class NewAjaxHandlerAction extends ActionSupport implements ServletRequestAware, ServletResponseAware {

	private HttpServletRequest httpServletRequest;
	private HttpServletResponse httpServletResponse;
	private String resultType;
	private String responseString;
	private String mcertStartDate;
	private String mcertToDate;
	private String mcertConsultantId;
	private String mcertConsultantStatus;
	private String ExamNameIdList;
	private int questionNo;
	private int selectedAns;
	private String navigation;
	private int remainingQuestions;
	private int onClickStatus;
	private int subTopicId;
	private int specficQuestionNo;
	private String examKeyId;
	private String createdBy;
	private String title;
	private String postedDate1;
	private String postedDate2;
	private String status;
	private String country;
	private String practiceid;
	private int requirementId;
	private String state;
	private String preSalesPerson;
	private String assignedTo;
	private String assignedBy;
	private int accId;
	private String empId;
	private String projectId;
	private int accountId;
	private String empType;
	private int preAssignEmpId;
	private int pgNo = 1;
	private String customerName;
	private String pgFlag;
	private int userRoleId;
	private String startDate;
	private String orderBy;
	private int prjId;
	private String endDate;
	private String requestType;
	private int leadId;
	private File file;
	private String fileFileName;
	private String fileFileContentType;
	private String generatedPath;
	private String fileLocation;
	private String filepath;
	private String objectType;
	private String attachmentLocation;
	private int topicId;
	private String year;
	private String month;
	private String type;
	private String category;
	private String activityType;
	private String bdmId;
	private String activityId;
	private String teamMemberId;
	private String startDateContacts;
	private String endDateContacts;
	private String customerId;
	private String titleType;
	private int teamMemberCheck;
	private String reqId;
	private String statesFromOrg;
	private String bdeLoginId;
	private String lastActivityFrom;
	private String lastActivityTo;
	private String opportunity;
	private String touched;
	private String employeeName;
	private int invId;
	private Timestamp reminderDate;
	private String followUpComments;
	private String nextFollowUpSteps;
	private int id;
	private int reminderFlag;
	private String operationType;
	private String projectName;
	private String projectStartDate;
	private String pmoLoginId;
	private String practiceId;
	private String loginId;
	private int notesId;
	private int graphId;
	private String taskStartDate;
	private String taskEndDate;
	private String reportsTo;
	private String comments;
	private String strengthsComments;
	private String improvementsComments;
	private String shortTermGoalComments;
	private String longTermGoalComments;
	private int rowCount;
	private String curretRole;
	private int appraisalId;
	private int lineId;
	private String quarterly;
	private String shortTermGoal;
	private String longTermGoal;
	private String strength;
	private String improvements;
	private String rejectedComments;
	private String operationTeamStatus;
	private String managerRejectedComments;
	private String operationRejectedComments;
	private int dayCount;
	private int accessCount;
	private String preAssignSalesId;
	private String teamMemberStatus;
	private String salesName;
	private String departmentId;
	private String subPracticeId;
	private String consultantName;
	private String consultantId;
	private String file1FileName;
	private String attachmentLocation1;
	private int requestId;
	private String accessType;
	private String videoDescription;
	private List videoTitlesList = new ArrayList();
	private String location;
	private String quarter;
	private String columnLabel;

	private int event_Id;
	private String nomineeIds;
	private String nomineeName;
	private String nomineeState;
	private String eventName;
	private String eventLocation;
	private String eventLink;
	private String eventStartDate;
	private String eventEndDate;

	/* star performers */
	private String objectId;
	private String overlayStatus;
	private int overlayYear;
	private int overlayMonth;
	private Timestamp createdDate;
	private String ModifiedBy;
	private Timestamp ModifiedDate;
	private String overlayDate;
	private String mail;
	private int objId;
	private String campaignStartDate;
	private String movieName;
	private String movieDates;
	private String expireDate;
	private int survyId;
	private String projectType;

	// for project risk dashboard
	private String impact;
	private String isIncludeTeam;

	private String currId;
	private String stateStartDate;
	private String stateEndDate;

	private int qyear;

	private String costModel;
	private String week;

	/* Look Up Table */
	private String tableName;
	private String headerFields;
	private String stringData;
	private String lookUpId;
	private String columnName;
	private String columnData;
	private String opsContactId;

	private int expMonth;
	private int expYear;

	private int infoId;

	private String qReviewMnagersDetailsJson;

	/* Release Notes */
	private File file0;
	private String file0FileName;
	private File file1;
	private File file2;
	private String file2FileName;
	private File file3;
	private String file3FileName;
	private File file4;
	private String file4FileName;
	private File file5;
	private String file5FileName;
	private String file0FileContentType;
	private String file0Location;
	private AttachmentService attachmentService;
	private int releaseId;

	private String biometricDate;
	private int biometricHrs;
	private int timeSheetHrs;
	private String leaveStatus;
	private String statusCode;

	private String syncYearOverlay;
	private String syncMonthOverlay;
	private Map LocationList;
	private String daysProject;
	private String daysInMonth;
	private String daysWeekends;
	private String daysWorked;
	private String daysVacation;
	private String daysHolidays;
	private String empnameById;
	private String locationId;

	private String reportType;
	private String empName;
	private String empNo;
	private String reportTypeHidden;
	String elasticSearchFlag = Properties.getProperty("ElasticSearch.Flag");

	private String holidayDate;
	private String description;

	private int pastMonths;
	private boolean external;
	private Map rolesMap;
	private String clientId;

	private int shadowFlag;
	private String includeTeamFlag;
	private String reportBy;
	private int includeTeam;
	private String flag;

	private String eventCoordinatorName;
	private String eventCoordinatorEmail;
	private String eventCoordinatorPhone;

	private String onsiteCoordinatorName;
	private String onsiteCoordinatorEmail;
	private String onsiteCoordinatorPhone;
	private int conferenceId;
	private int logisticId;
	private int nomineeId;

	private String addLogisticDetails;

	private String departureflightName;
	private String departureCity;
	private String flyDepartureDate;
	private String flyDepartureTime;
	private String flyDepartureMidDayFrom;
	private String arrivalflightName;
	private String arrivalCity;
	private String flyArrivalDate;
	private String flyArrivalTime;
	private String flyArrivalMidDayFrom;
	private String hotelName;
	private String hotelAddress;
	private String arrivalTimeZone;
	private String departureTimeZone;

	private File fileTransport;
	private String fileTransportFileName;
	private String fileTransportFileContentType;
	private String transportAttachmentLocation;
	private File fileHotel;
	private String fileHotelFileName;
	private String fileHotelFileContentType;
	private String hotelAttachmentLocation;
	private File fileFlight;
	private String fileFlightFileName;
	private String filFlightFileContentType;
	private String flightAttachmentLocation;
	private String transportDetails;
	private String logisticDetails;

	private int attendanceLogId;

	private String meetingstartDate;
	private String meetingendDate;
	private String reportBasedOn;
	private String projstartDate;
	private String imgStatus;
	
	private String msaStartDate;
	private String msaExpiryDate;

	private String msafrmStartDate;
	private String msatoStartDate;
	private String msafrmExpiryDate;
	private String msatoExpiryDate;
	

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getqReviewMnagersDetailsJson() {
		return qReviewMnagersDetailsJson;
	}

	public void setqReviewMnagersDetailsJson(String qReviewMnagersDetailsJson) {
		this.qReviewMnagersDetailsJson = qReviewMnagersDetailsJson;
	}

	private String employeeVerificationDetailsJson;

	public String getEmployeeVerificationDetailsJson() {
		return employeeVerificationDetailsJson;
	}

	public void setEmployeeVerificationDetailsJson(String employeeVerificationDetailsJson) {
		this.employeeVerificationDetailsJson = employeeVerificationDetailsJson;
	}

	public int getInfoId() {
		return infoId;
	}

	public void setInfoId(int infoId) {
		this.infoId = infoId;
	}

	public String getSubPractice() {
		return subPractice;
	}

	public void setSubPractice(String subPractice) {
		this.subPractice = subPractice;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getAvailableUtl() {
		return availableUtl;
	}

	public void setAvailableUtl(String availableUtl) {
		this.availableUtl = availableUtl;
	}

	private String jsonData;
	private String subPractice;
	private String department;
	private String availableUtl;

	public String getMcertRecordsList() {
		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			String loginId1 = httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID)
					.toString();

			try {

				httpServletResponse.setContentType("text/html");
				// httpServletResponse.getWriter().write(ServiceLocator.getAjaxHandlerService().getCreRecordsList(getCreConsultantId(),getCreConsultantName(),getCreStartDate(),getCreToDate(),
				// getCreConsultantStatus(),
				// getCategory(),getLevel(),getInterviewAt()));
				String response = ServiceLocator.getNewAjaxHandlerService().getMcertRecordsList(getMcertStartDate(),
						getMcertToDate(), getMcertConsultantStatus());
				httpServletResponse.getWriter().write(response);
			} catch (ServiceLocatorException ex) {
				ex.printStackTrace();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return null;
	}

	public String mcertRecordStatusUpdate() {

		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			try {
				String loginid = httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID)
						.toString();

				httpServletResponse.setContentType("text/html");

				httpServletResponse.getWriter().write(ServiceLocator.getNewAjaxHandlerService().mcertRecordStatusUpdate(
						getMcertConsultantId(), loginid, getMcertConsultantStatus(), getExamNameIdList()));

			} catch (ServiceLocatorException ex) {
				ex.printStackTrace();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return null;
	}

	public String getMcertQuestion() {
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {

				// responseString =
				// ServiceLocator.getAjaxHandlerService().getQuestion(getQuestionNo(),httpServletRequest,getSelectedAns(),getNavigation(),getRemainingQuestions(),getOnClickStatus(),getSubTopicId());
				responseString = ServiceLocator.getNewAjaxHandlerService().getMcertQuestion(getQuestionNo(),
						httpServletRequest, getSelectedAns(), getNavigation(), getRemainingQuestions(),
						getOnClickStatus(), getSubTopicId(), getSpecficQuestionNo());
				httpServletResponse.setContentType("text/xml");
				// System.out.println("responseString-->"+responseString);
				httpServletResponse.getWriter().write(responseString);
			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return null;
	}

	public String getMcertDetailExamInfo() {
		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			try {
				String result = ServiceLocator.getNewAjaxHandlerService().getMcertDetailExamInfo(getExamKeyId());
				// System.out.println("in action-->"+result);
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(result);
			} catch (ServiceLocatorException ex) {
				ex.printStackTrace();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return null;
	}

	public String searchPreSalesRequirementList() {
		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			// AjaxHandlerAction ajaxHandlerAction = new AjaxHandlerAction();
			try {

				// System.out.println("values :::::::");
				// createdBy,assignedTo,title,postedDate1,postedDate2
				// System.out.println("createdBy----------"+getCreatedBy()+"---assignedTo-------------"+getAssignedTo()+"---title-------------"+getTitle()+"---postedDate1-------"+getPostedDate1()+"----postedDate2----"+getPostedDate2());

				responseString = ServiceLocator.getNewAjaxHandlerService()
						.searchPreSalesRequirementList(httpServletRequest, this);
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} catch (ServiceLocatorException ex) {
				ex.printStackTrace();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return null;
	}

	// getRequirementOtherDetails
	public String getRequirementOtherDetails() {
		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			// AjaxHandlerAction ajaxHandlerAction = new AjaxHandlerAction();
			try {

				// System.out.println("values :::::::");
				// createdBy,assignedTo,title,postedDate1,postedDate2
				// System.out.println("createdBy----------"+getCreatedBy()+"---assignedTo-------------"+getAssignedTo()+"---title-------------"+getTitle()+"---postedDate1-------"+getPostedDate1()+"----postedDate2----"+getPostedDate2());

				responseString = ServiceLocator.getNewAjaxHandlerService()
						.getRequirementOtherDetails(getRequirementId());
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} catch (ServiceLocatorException ex) {
				ex.printStackTrace();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return null;
	}

	public String requirementAjaxListForMyPresales() {
		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			try {

				responseString = ServiceLocator.getNewAjaxHandlerService()
						.searchPreSalesMyRequirementList(httpServletRequest, this);
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} catch (ServiceLocatorException ex) {
				ex.printStackTrace();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return null;
	}

	public String doPopulateAccountDetails() {
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {

				responseString = ServiceLocator.getNewAjaxHandlerService().doPopulateAccountDetails(getAccId());

				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return null;
	}

	public String popupContactsWindow() {
		String str;
		/*
		 * This if loop is to check whether there is Session or not
		 **/
		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			try {

				str = ServiceLocator.getNewAjaxHandlerService().popupContactsWindow(getEmpId());
				httpServletResponse.setContentType("text/html");
				if (str != null) {
					httpServletResponse.getWriter().write((str));
				} else {
					httpServletResponse.getWriter().write(("No Record Present"));
				}

			} catch (ServiceLocatorException ex) {
				ex.printStackTrace();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		} // Close Session Checking
		return null;
	}

	public String getEmployeeProjectDetailsBasedOnStatus() {
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
				responseString = ServiceLocator.getNewAjaxHandlerService().getEmployeeProjectDetailsBasedOnStatus(
						getProjectId(), getAccountId(), getEmpType(), getPreAssignEmpId(), httpServletRequest);
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return null;

	}

	public String getMsafrmStartDate() {
		return msafrmStartDate;
	}

	public void setMsafrmStartDate(String msafrmStartDate) {
		this.msafrmStartDate = msafrmStartDate;
	}

	public String getMsatoStartDate() {
		return msatoStartDate;
	}

	public void setMsatoStartDate(String msatoStartDate) {
		this.msatoStartDate = msatoStartDate;
	}

	public String getMsafrmExpiryDate() {
		return msafrmExpiryDate;
	}

	public void setMsafrmExpiryDate(String msafrmExpiryDate) {
		this.msafrmExpiryDate = msafrmExpiryDate;
	}

	public String getMsatoExpiryDate() {
		return msatoExpiryDate;
	}

	public void setMsatoExpiryDate(String msatoExpiryDate) {
		this.msatoExpiryDate = msatoExpiryDate;
	}

	public String getMsaStartDate() {
		return msaStartDate;
	}

	public void setMsaStartDate(String msaStartDate) {
		this.msaStartDate = msaStartDate;
	}

	public String getMsaExpiryDate() {
		return msaExpiryDate;
	}

	public void setMsaExpiryDate(String msaExpiryDate) {
		this.msaExpiryDate = msaExpiryDate;
	}

	public String getImgStatus() {
		return imgStatus;
	}

	public void setImgStatus(String imgStatus) {
		this.imgStatus = imgStatus;
	}

	public String getProjectStatusById() {
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
				responseString = ServiceLocator.getNewAjaxHandlerService().getProjectStatusById(getProjectId());
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return null;

	}

	public String getProjectProtfolioReport() {
		// System.out.println("getProjectProtfolioReport() in
		// NewAjaxHandlerAction");
		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			userRoleId = Integer.parseInt(
					httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_ROLE_ID).toString());
			if (AuthorizationManager.getInstance().isAuthorizedUser("PRESALES_DASHBOARD_ACCESS", userRoleId)) {
				try {
					// System.out.println("getCustomerName---->"+getCustomerName());
					// System.out.println("getStartDate---->"+getStartDate());
					// System.out.println("getStatus---->"+getStatus());
					responseString = ServiceLocator.getNewAjaxHandlerService().getProjectProtfolioReport(
							getCustomerName(), getStartDate(), getOrderBy(), getStatus(), getProjectType(),
							httpServletRequest);
					httpServletResponse.setContentType("text");
					httpServletResponse.getWriter().write(responseString);
				} catch (ServiceLocatorException ex) {
					ex.printStackTrace();
				} catch (IOException ex) {
					ex.printStackTrace();
				}
			}
		}
		return null;
	}

	public String getProjectDescriptionDetails() {
		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			try {
				responseString = ServiceLocator.getNewAjaxHandlerService().getProjectDescriptionDetails(getPrjId());
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} catch (ServiceLocatorException ex) {
				ex.printStackTrace();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return null;
	}

	public String getProjectCommentDetails() {
		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			// AjaxHandlerAction ajaxHandlerAction = new AjaxHandlerAction();
			try {

				// System.out.println("values :::::::");
				// createdBy,assignedTo,title,postedDate1,postedDate2
				// System.out.println("createdBy----------"+getCreatedBy()+"---assignedTo-------------"+getAssignedTo()+"---title-------------"+getTitle()+"---postedDate1-------"+getPostedDate1()+"----postedDate2----"+getPostedDate2());

				responseString = ServiceLocator.getNewAjaxHandlerService().getProjectCommentDetails(getPrjId());
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} catch (ServiceLocatorException ex) {
				ex.printStackTrace();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return null;
	}

	public String getPSCERDetailsForPresales() {
		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			// AjaxHandlerAction ajaxHandlerAction = new AjaxHandlerAction();
			try {

				// System.out.println("values :::::::");

				responseString = ServiceLocator.getNewAjaxHandlerService()
						.getPSCERDetailsForPresales(httpServletRequest, this);

				httpServletResponse.setContentType("text/xml");
				httpServletResponse.getWriter().write(responseString);
			} catch (ServiceLocatorException ex) {
				ex.printStackTrace();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return null;
	}

	public String getRequestInSightDetails() {
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
				responseString = ServiceLocator.getNewAjaxHandlerService().getRequestInSightDetails(getLeadId());
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return null;

	}
	/*
	 * PSCER dynamic Ajax grid for Sales and Employee Author : Teja Kadamanti Date
	 * :12/05/2016
	 */

	public String getPSCERDetailsForEmployee() {
		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			// AjaxHandlerAction ajaxHandlerAction = new AjaxHandlerAction();
			try {

				// System.out.println("values :::::::");

				responseString = ServiceLocator.getNewAjaxHandlerService()
						.getPSCERDetailsForEmployee(httpServletRequest, this);

				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} catch (ServiceLocatorException ex) {
				ex.printStackTrace();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return null;
	}

	public String getPSCERDetailsForSales() {
		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			// AjaxHandlerAction ajaxHandlerAction = new AjaxHandlerAction();
			try {

				// System.out.println("values :::::::");

				responseString = ServiceLocator.getNewAjaxHandlerService().getPSCERDetailsForSales(httpServletRequest,
						this);

				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} catch (ServiceLocatorException ex) {
				ex.printStackTrace();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return null;
	}

	public String getDomainByExamType() {
		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			// AjaxHandlerAction ajaxHandlerAction = new AjaxHandlerAction();
			try {
				// System.out.println("values :::::::");

				responseString = ServiceLocator.getNewAjaxHandlerService().getDomainByExamType(getPgNo());

				httpServletResponse.setContentType("text/xml");
				httpServletResponse.getWriter().write(responseString);

			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return null;
	}

	public String ajaxQuestionsFileUpload() throws ServiceLocatorException, IOException {
		try {

			// System.out.println(getSubTopicId()+"......gettopicId......."+getTopicId());
			setCreatedBy(
					httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID).toString());
			responseString = ServiceLocator.getNewAjaxHandlerService().ajaxQuestionsFileUpload(this);
			// System.out.println("---->responseString"+responseString);
			httpServletResponse.setContentType("text");
			httpServletResponse.getWriter().write(responseString);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

	/**
	 * Created By :Tirumala Teja Kadamanti Purpose : Newsletters Deployment
	 * Automation Use : To add and search Newsletters and images in a directory
	 **/
	public String getAllFilesInDirectory() {
		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			// AjaxHandlerAction ajaxHandlerAction = new AjaxHandlerAction();
			try {

				// System.out.println("values :::::::");

				responseString = ServiceLocator.getNewAjaxHandlerService().getAllFilesInDirectory(httpServletRequest,
						this);

				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} catch (ServiceLocatorException ex) {
				ex.printStackTrace();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return null;
	}

	public String doAddNewsLetter() throws JSONException, ServiceLocatorException {
		int add = 0;

		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			// AjaxHandlerAction ajaxHandlerAction = new AjaxHandlerAction();
			try {
				JSONObject jb = new JSONObject();
				if (getFileFileName() != null && !"".equals(getFileFileName())) {
					jb.put("ImageName", getFileFileName());
					jb.put("Year", getYear());
					jb.put("Month", getMonth());
					jb.put("Type", getType());
					jb.put("Category", getCategory());

					String encodedBase64 = null;
					try {
						// System.out.println("getFileFileName()====>"+getFileFileName());
						FileInputStream fileInputStreamReader = new FileInputStream(getFile());

						byte[] bytes = new byte[(int) (getFile()).length()];
						// System.out.println("fileInputStreamReader.."+fileInputStreamReader+"bytes.."+bytes);

						fileInputStreamReader.read(bytes);
						encodedBase64 = new String(Base64.encodeBytes(bytes));
					} catch (FileNotFoundException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					}
					encodedBase64 = "data:application/pdf;base64," + encodedBase64;
					jb.put("Base64String", encodedBase64);
					// System.out.println("filename.."+getFileFileName()+"encodedBase64..."+encodedBase64);

				}
				String serviceUrl = RestRepository.getInstance().getSrviceUrl("addNewsLetters");
				URL url = new URL(serviceUrl);
				URLConnection connection = url.openConnection();
				connection.setDoOutput(true);
				connection.setRequestProperty("Content-Type", "application/json");
				connection.setConnectTimeout(360 * 5000);
				connection.setReadTimeout(360 * 5000);
				OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
				out.write(jb.toString());
				out.close();

				BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
				String s = null;
				String data = "";
				while ((s = in.readLine()) != null) {

					data = data + s;
				}

				JSONObject jObject = new JSONObject(data);

				responseString = jObject.getString("ResponseString");
				// System.out.println("responseString...."+responseString);

				in.close();
				add = 1;

				if (add == 1) {

					responseString = "uploaded";
				}
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return null;
	}

	public String getAllFilesInImagesDirectory() {
		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			// AjaxHandlerAction ajaxHandlerAction = new AjaxHandlerAction();
			try {

				// System.out.println("values :::::::");

				responseString = ServiceLocator.getNewAjaxHandlerService()
						.getAllFilesInImagesDirectory(httpServletRequest, this);

				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} catch (ServiceLocatorException ex) {
				ex.printStackTrace();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return null;
	}

	public String doAddNewsLetterImages() {
		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			// AjaxHandlerAction ajaxHandlerAction = new AjaxHandlerAction();
			try {

				// System.out.println("values :::::::");
				String basePath = Properties.getProperty("Images.NewsLetters.Path");
				String monthName = "";
				if (Integer.parseInt(getMonth()) == 0) {
					monthName = "January";
				} else if (Integer.parseInt(getMonth()) == 1) {
					monthName = "February";
				} else if (Integer.parseInt(getMonth()) == 2) {
					monthName = "March";
				} else if (Integer.parseInt(getMonth()) == 3) {
					monthName = "April";
				} else if (Integer.parseInt(getMonth()) == 4 && ("2016".equalsIgnoreCase(getYear()))) {
					monthName = "may";
				} else if (Integer.parseInt(getMonth()) == 4 && (!"2016".equalsIgnoreCase(getYear()))) {
					monthName = "May";
				} else if (Integer.parseInt(getMonth()) == 5 && ("2016".equalsIgnoreCase(getYear()))) {
					monthName = "june";
				} else if (Integer.parseInt(getMonth()) == 5 && (!"2016".equalsIgnoreCase(getYear()))) {
					monthName = "June";
				} else if (Integer.parseInt(getMonth()) == 6) {
					monthName = "July";
				} else if (Integer.parseInt(getMonth()) == 7) {
					monthName = "August";
				} else if (Integer.parseInt(getMonth()) == 8) {
					monthName = "September";
				} else if (Integer.parseInt(getMonth()) == 9) {
					monthName = "October";
				} else if (Integer.parseInt(getMonth()) == 10 && ("2016".equalsIgnoreCase(getYear()))) {
					monthName = "november";
				} else if (Integer.parseInt(getMonth()) == 10 && (!"2016".equalsIgnoreCase(getYear()))) {
					monthName = "November";
				} else if (Integer.parseInt(getMonth()) == 11 && ("2016".equalsIgnoreCase(getYear()))) {
					monthName = "december";
				} else if (Integer.parseInt(getMonth()) == 11 && (!"2016".equalsIgnoreCase(getYear()))) {
					monthName = "December";
				}

				basePath = basePath + "//" + getYear() + "//" + monthName;

				File file = new File(basePath);
				File[] listFiles = file.listFiles();

				if (listFiles != null) {
					for (File files : listFiles) {
						if (files.getName().equalsIgnoreCase(getFileFileName())) {
							responseString = "exists";
						}
					}
				}
				if (!"exists".equalsIgnoreCase(responseString)) {

					File generateFile = new File(basePath);
					generateFile.mkdir();
					File targetDirectory = new File(generateFile + File.separator + getFileFileName());
					FileUtils.copyFile(getFile(), targetDirectory);

					responseString = "uploaded";
				}
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return null;
	}

	/* News Letters Deployment Automation End */
	public String getBDMStatistics() {
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
				responseString = ServiceLocator.getNewAjaxHandlerService().getBDMStatistics(getBdmId(), getStartDate(),
						getEndDate());
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return null;

	}

	public String getBdmStatisticsDetailsByLoginId() {
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
				responseString = ServiceLocator.getNewAjaxHandlerService().getBdmStatisticsDetailsByLoginId(getBdmId(),
						getStartDate(), getEndDate(), getActivityType());
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return null;

	}

	public String getActivityContacts() {
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
				responseString = ServiceLocator.getNewAjaxHandlerService().getActivityContacts(getActivityId());
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return null;

	}

	public String getCustomerContacts() {
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
				responseString = ServiceLocator.getNewAjaxHandlerService().getCustomerContacts(getCustomerName(),
						getTeamMemberId(), getStartDateContacts(), getEndDateContacts(), getTeamMemberCheck(),
						getTitleType());
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return null;

	}

	public String getContactActivities() {
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
				responseString = ServiceLocator.getNewAjaxHandlerService().getContactActivities(getCustomerId(),
						getTeamMemberId(), getStartDateContacts(), getEndDateContacts(), getTeamMemberCheck(),
						getTitleType());
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return null;

	}

	public String getOpsContactId() {
		return opsContactId;
	}

	public void setOpsContactId(String opsContactId) {
		this.opsContactId = opsContactId;
	}

	public String reqOverviewPieChart() {
		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			// AjaxHandlerAction ajaxHandlerAction = new AjaxHandlerAction();
			try {
				// System.out.println("values :::::::");

				responseString = ServiceLocator.getNewAjaxHandlerService().reqOverviewPieChart(httpServletRequest,
						this);
				// System.out.println("responseString...."+responseString);
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);

			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return null;
	}

	public String getRequirementOverviewDetails() {
		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			// AjaxHandlerAction ajaxHandlerAction = new AjaxHandlerAction();
			try {
				// System.out.println("values of table:::::::");

				responseString = ServiceLocator.getNewAjaxHandlerService()
						.getRequirementOverviewDetails(httpServletRequest, this);
				// System.out.println("responseString of
				// table...."+responseString);
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);

			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return null;
	}

	public String getInsideSalesStatusList() {
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
				responseString = ServiceLocator.getNewAjaxHandlerService().getInsideSalesStatusList(getBdeLoginId());
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return null;

	}

	public String getInsideSalesAccountDetailsList() {
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {

				responseString = ServiceLocator.getNewAjaxHandlerService()
						.getInsideSalesAccountDetailsList(httpServletRequest, this);
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return null;

	}

	public String getInvestmentType() {
		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			try {
				String result = ServiceLocator.getNewAjaxHandlerService().getInvestmentType(getInvId());
				// System.out.println("in action-->"+result);
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(result);
			} catch (ServiceLocatorException ex) {
				ex.printStackTrace();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return null;
	}

	public String addLeadHystory() {
		if ((httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null)) {
			try {
				String loginId = httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID)
						.toString();

				responseString = ServiceLocator.getNewAjaxHandlerService().addLeadHystory(loginId, getReminderDate(),
						getFollowUpComments(), getNextFollowUpSteps(), getLeadId(), getOperationType(), getId(),
						getReminderFlag());

				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} catch (ServiceLocatorException ex) {
				ex.printStackTrace();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return null;
	}

	public String getLeadFollowUpHistoryDetails() {
		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			try {
				responseString = ServiceLocator.getNewAjaxHandlerService().getLeadFollowUpHistoryDetails(getId());
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} catch (Exception ex) {
			}

		}
		return null;
	}

	public String customerProjectDetailsDashboard() {
		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			try {
				responseString = ServiceLocator.getNewAjaxHandlerService().customerProjectDetailsDashboard(this,
						httpServletRequest);
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} catch (Exception ex) {
			}

		}
		return null;
	}

	public String getResourceTypeDetailsByCustomer() {
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
				responseString = ServiceLocator.getNewAjaxHandlerService().getResourceTypeDetailsByCustomer(getAccId(),
						getProjectId(), httpServletRequest);
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return null;

	}

	public String getProjectListByCustomer() {
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
				responseString = ServiceLocator.getNewAjaxHandlerService().getProjectListByCustomer(this,
						httpServletRequest);
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return null;

	}

	public String getTaskNotes() {
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
				responseString = ServiceLocator.getNewAjaxHandlerService().getTaskNotes(getNotesId());
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return null;

	}

	/*
	 * Author : sarada tatisetti Date: 10-10-2017
	 */
	public String getResourceTypeDetailsByProject() {
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
				responseString = ServiceLocator.getNewAjaxHandlerService().getResourceTypeDetailsByProject(getAccId(),
						getProjectId(), httpServletRequest);
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return null;

	}

	/*
	 * Task DashBoards functionality
	 * 
	 * Author : phanindra kanuri
	 * 
	 */
	public String getIssuesDashBoardByStatus() {
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
				responseString = ServiceLocator.getNewAjaxHandlerService().getIssuesDashBoardByStatus(
						getTaskStartDate(), getTaskEndDate(), getReportsTo(), getGraphId(), httpServletRequest);
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return null;

	}

	public String getTaskListByStatus() {
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
				responseString = ServiceLocator.getNewAjaxHandlerService().getTaskListByStatus(getTaskStartDate(),
						getTaskEndDate(), getReportsTo(), getActivityType(), getGraphId(), httpServletRequest);
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return null;

	}

	public String doPopulateTaskDetails() {
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {

				responseString = ServiceLocator.getNewAjaxHandlerService().doPopulateTaskDetails(getId());
				// responseString =
				// ServiceLocator.getNewAjaxHandlerService().doPopulateAccountDetails(getId());

				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return null;
	}
	/*
	 * q - reviews
	 */

	public String doSetQReviewManagerComments() {
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {

				int count = ServiceLocator.getNewAjaxHandlerService().doSetQReviewManagerComments(getId(),
						getComments());
				// System.out.println("count==="+count);
				// responseString =
				// ServiceLocator.getNewAjaxHandlerService().doPopulateAccountDetails(getId());
				if (count > 0) {
					responseString = "success";
				} else {
					responseString = "<font color=\"red\" size=\"1.5\">Please try again later!</font>";
				}
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return null;
	}

	public String doSetQReviewPersonalityComments() {
		JSONObject jObject = null;
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {

				jObject = new JSONObject(getJsonData());
				setId(jObject.getInt("id"));
				setStrengthsComments(jObject.getString("strengthsComments"));
				setImprovementsComments(jObject.getString("improvementsComments"));
				setStrength(jObject.getString("strength"));
				setImprovements(jObject.getString("improvements"));
				int count = ServiceLocator.getNewAjaxHandlerService().doSetQReviewPersonalityComments(getId(),
						getStrengthsComments(), getImprovementsComments(), getStrength(), getImprovements());
				// System.out.println("count==="+count);
				// responseString =
				// ServiceLocator.getNewAjaxHandlerService().doPopulateAccountDetails(getId());
				if (count > 0) {
					responseString = "success";
				} else {
					responseString = "<font color=\"red\" size=\"1.5\">Please try again later!</font>";
				}
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public String doSetQReviewGoalsComments() {
		JSONObject jObject = null;
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {

				jObject = new JSONObject(getJsonData());
				setId(jObject.getInt("id"));
				setShortTermGoalComments(jObject.getString("shortTermGoalComments"));
				setLongTermGoalComments(jObject.getString("longTermGoalComments"));
				setShortTermGoal(jObject.getString("shortTermGoal"));
				setLongTermGoal(jObject.getString("longTermGoal"));
				int count = ServiceLocator.getNewAjaxHandlerService().doSetQReviewGoalsComments(getId(),
						getShortTermGoalComments(), getLongTermGoalComments(), getShortTermGoal(), getLongTermGoal());
				// System.out.println("count==="+count);
				// responseString =
				// ServiceLocator.getNewAjaxHandlerService().doPopulateAccountDetails(getId());
				if (count > 0) {
					responseString = "success";
				} else {
					responseString = "<font color=\"red\" size=\"1.5\">Please try again later!</font>";
				}
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public String dummySessionHit() {
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
				responseString = "dummyHit";
				// System.out.println("count===");
				// responseString =
				// ServiceLocator.getNewAjaxHandlerService().doPopulateAccountDetails(getId());
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} // Close Session Checking
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

	public String saveQReviewDetails() {
		JSONObject taxJson = new JSONObject();
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
				userRoleId = Integer.parseInt(httpServletRequest.getSession(false)
						.getAttribute(ApplicationConstants.SESSION_ROLE_ID).toString());
				if (AuthorizationManager.getInstance().isAuthorizedUser("QUARTERLY_APPRAISAL", userRoleId)) {
					try {
						// System.out.println("shortTermGoalComments===" +
						// getShortTermGoalComments());

						String result = ServiceLocator.getNewAjaxHandlerService().empQuarterlyAppraisalSave(this,
								httpServletRequest);
						// System.out.println("result===" + result);
						if (result.contains("added")) {
							taxJson.put("result", "success");
						} else {
							taxJson.put("result", "failed");
						}
						List list = DataSourceDataProvider.getInstance().getAppraisalIds(Integer.parseInt(getEmpId()));
						setLineId((Integer) list.get(0));
						setAppraisalId((Integer) list.get(1));
						// System.out.println("lineId==="+getLineId());
						// System.out.println("getAppraisalId==="+getAppraisalId());
						result = ServiceLocator.getNewAjaxHandlerService()
								.quarterlyAppraisalEdit(Integer.parseInt(getEmpId()), getAppraisalId(), getLineId());

						taxJson.put("QuarterAppraisalDetails", result.split(Pattern.quote("@^$"))[0]);
						taxJson.put("LineId", getLineId());
						taxJson.put("AppraisalId", getAppraisalId());

					} catch (Exception ex) {
						// List errorMsgList =
						// ExceptionToListUtility.errorMessages(ex);
						httpServletRequest.getSession(false).setAttribute("errorMessage", ex.toString());
						resultType = ERROR;
					}
				}
				responseString = taxJson.toString();
				// System.out.println("responseString==="+responseString);
				// responseString =
				// ServiceLocator.getNewAjaxHandlerService().doPopulateAccountDetails(getId());
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} // Close Session Checking
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

	public String setQReviewEmpDescriptions() {
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {

				int count = ServiceLocator.getNewAjaxHandlerService().setQReviewEmpDescriptions(getJsonData());
				// System.out.println("count==="+count);
				// responseString =
				// ServiceLocator.getNewAjaxHandlerService().doPopulateAccountDetails(getId());
				if (count > 0) {
					responseString = "success";
				} else {
					responseString = "<font color=\"red\" size=\"1.5\">Please try again later!</font>";
				}
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return null;
	}

	public String doAddQReviewHrDescriptions() {
		JSONObject taxJson = new JSONObject();
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {

				int id = ServiceLocator.getNewAjaxHandlerService().doAddQReviewHrDescriptions(getJsonData());
				// System.out.println("count==="+count);
				// responseString =
				// ServiceLocator.getNewAjaxHandlerService().doPopulateAccountDetails(getId());
				if (id > 0) {
					taxJson.put("result", "success");
					taxJson.put("id", id);
				} else {
					taxJson.put("result", "<font color=\"red\" size=\"1.5\">Please try again later!</font>");

				}
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(taxJson.toString());
			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public String doSetQReviewHrComments() {
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {

				int count = ServiceLocator.getNewAjaxHandlerService().doSetQReviewHrComments(getId(), getComments());
				// System.out.println("count==="+count);
				// responseString =
				// ServiceLocator.getNewAjaxHandlerService().doPopulateAccountDetails(getId());
				if (count > 0) {
					responseString = "success";
				} else {
					responseString = "<font color=\"red\" size=\"1.5\">Please try again later!</font>";
				}
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return null;
	}

	public String doSetQReviewPersonalityHrComments() {
		JSONObject jObject = null;
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {

				jObject = new JSONObject(getJsonData());
				setId(jObject.getInt("id"));
				setStrengthsComments(jObject.getString("strengthsHrComments"));
				setImprovementsComments(jObject.getString("improvementsHrComments"));

				int count = ServiceLocator.getNewAjaxHandlerService().doSetQReviewPersonalityHrComments(getId(),
						getStrengthsComments(), getImprovementsComments());
				// System.out.println("count==="+count);
				// responseString =
				// ServiceLocator.getNewAjaxHandlerService().doPopulateAccountDetails(getId());
				if (count > 0) {
					responseString = "success";
				} else {
					responseString = "<font color=\"red\" size=\"1.5\">Please try again later!</font>";
				}
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public String doSetQReviewGoalsHrComments() {
		JSONObject jObject = null;
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {

				jObject = new JSONObject(getJsonData());
				setId(jObject.getInt("id"));
				setShortTermGoalComments(jObject.getString("shortTermGoalComments"));
				setLongTermGoalComments(jObject.getString("longTermGoalComments"));

				int count = ServiceLocator.getNewAjaxHandlerService().doSetQReviewGoalsHrComments(getId(),
						getShortTermGoalComments(), getLongTermGoalComments());
				// System.out.println("count==="+count);
				// responseString =
				// ServiceLocator.getNewAjaxHandlerService().doPopulateAccountDetails(getId());
				if (count > 0) {
					responseString = "success";
				} else {
					responseString = "<font color=\"red\" size=\"1.5\">Please try again later!</font>";
				}
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public String getBdmReport() {
		try {
			// System.out.println("in action ajax");
			/*
			 * This if loop is to check whether there is Session or not
			 **/
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {

				responseString = ServiceLocator.getNewAjaxHandlerService().getBdmReport(this, httpServletRequest);
				// responseString =
				// ServiceLocator.getNewAjaxHandlerService().doPopulateAccountDetails(getId());

				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return null;
	}

	public String getSalesTeamforBDMAssociate() {
		// System.out.println("in action-->");
		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			try {
				// String fromId =
				// httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID).toString();
				String result = ServiceLocator.getNewAjaxHandlerService().getSalesTeamforBDMAssociate(getSalesName());
				// System.out.println("in action-->"+result);
				httpServletResponse.setContentType("text/xml");
				httpServletResponse.getWriter().write(result);
			} catch (ServiceLocatorException ex) {
				ex.printStackTrace();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return null;

	}

	public String getSalesTeamDetails() {
		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {

			try {

				responseString = ServiceLocator.getNewAjaxHandlerService().getSalesTeamDetails(getTeamMemberId(),
						getBdmId());
				// System.out.println("responseString---->"+responseString);
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} catch (Exception ex) {
			}

		} // Close Session Checking
		return null;
	}

	public String addSalesToBdm() {

		try {
			setCreatedBy(
					httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID).toString());
			// System.out.println("bdmId---->" + getBdmId());
			// System.out.println("presalesAssignId---->" +
			// getPreAssignSalesId());
			if ((ServiceLocator.getNewAjaxHandlerService().checkSalesAgainstBdm(this))) {
				// System.out.println("gng to add of impl");
				if ((ServiceLocator.getNewAjaxHandlerService().checkBdmStatus(this))) {
					if (ServiceLocator.getNewAjaxHandlerService().addSalesToBdm(this) > 0) {
						// System.out.println("in add of impl");
						responseString = "<font size='2' color='green'>Resource has been successfully added</font>";
					} else {
						responseString = "Error";
					}
				} else {

					responseString = "<font size='2' color='red'>Resouce alrady exists as Inactive status .Please Make the Resource as Active</font>";
				}

			} else {
				responseString = "<font size='2' color='red'>Sorry! The Resource has been already associated</font>";
				// responseString =
				// ServiceLocator.getNewAjaxHandlerService().checkBdmAddedName(this);
				// System.out.println("in sorry case of impl");
				// System.out.println("responseString --------" +
				// responseString);
				// System.out.println("in sorry case of impl after");
				// System.out.println("in else case.........");

			}
			httpServletResponse.setContentType("text");
			httpServletResponse.getWriter().write(responseString);
		} catch (ServiceLocatorException ex) {
			System.err.println(ex);
		} catch (IOException ioe) {
			System.err.println(ioe);
		}
		return null;
	}

	public String updateBdmTeam() {

		try {
			setCreatedBy(
					httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID).toString());
			if (!ServiceLocator.getNewAjaxHandlerService().updateBdmTeam(this)) {
				responseString = "updated";
			} else {
				responseString = "Error";
			}
			httpServletResponse.setContentType("text");
			httpServletResponse.getWriter().write(responseString);
		} catch (ServiceLocatorException ex) {
			System.err.println(ex);
		} catch (IOException ioe) {
			System.err.println(ioe);
		}
		return null;
	}

	/*
	 * Author :Teja Kadamanti Date : 04/20/2017 Description: Multiple Projects
	 * Employee Details start
	 * 
	 */
	public String getMultipleProjectsEmployeeList() {
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
				responseString = ServiceLocator.getNewAjaxHandlerService().getMultipleProjectsEmployeeList(this,
						httpServletRequest);
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return null;

	}

	public String multipleProjectsEmployeeListDetails() {
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
				responseString = ServiceLocator.getNewAjaxHandlerService()
						.multipleProjectsEmployeeListDetails(getPreAssignEmpId(), httpServletRequest);
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return null;

	}
	/*
	 * Author :Teja Kadamanti Date : 04/20/2017 Description: Multiple Projects
	 * Employee Details end
	 * 
	 */

	public String getConsultantNames() {
		try {
			// System.out.println("email--"+getEmail());
			String query = "SELECT CONCAT(TRIM(FName),'.',TRIM(LName)) AS FullName FROM tblRecConsultant WHERE (LName LIKE '"
					+ getConsultantName() + "%' OR FName LIKE '" + getConsultantName() + "%' OR Id LIKE '"
					+ getConsultantName() + "%')";
			// System.out.println("SELECT CONCAT(TRIM(FName),'.',TRIM(LName)) AS
			// FullName,LoginId FROM tblEmployee WHERE (LName LIKE '" +
			// employeeName + "%' OR FName LIKE '" + employeeName + "%')");
			responseString = ServiceLocator.getNewAjaxHandlerService().getConsultantNames(query);
			httpServletResponse.setContentType("text/xml");
			httpServletResponse.getWriter().write(responseString);
		} catch (ServiceLocatorException ex) {
			System.err.println(ex);
		} catch (IOException ioe) {
			System.err.println(ioe);
		}
		return null;
	}

	/*
	 * Author : Phani Kanuri Date : 05/09/2017
	 * 
	 */
	public String getRespectiveVideoTitleMap() {
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
				responseString = ServiceLocator.getNewAjaxHandlerService().getRespectiveVideoTitleMap(getAccessType());
				httpServletResponse.setContentType("text/xml");
				httpServletResponse.getWriter().write(responseString);
			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return null;
	}

	/*
	 * Author : Teja Kadamanti Date : 05/10/2017 Description : Mobile
	 * logged/Download tracking
	 */
	public String getMobileAppDetails() {
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
				responseString = ServiceLocator.getNewAjaxHandlerService().getMobileAppDetails(this,
						httpServletRequest);
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return null;

	}

	/*
	 * Author : Phani Kanuri Date : 05/10/2017 Desc: Qreview Dashboard
	 */
	public String getQReviewDashBoardByStatus() {
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {

				int userRoleId = Integer.parseInt(httpServletRequest.getSession(false)
						.getAttribute(ApplicationConstants.SESSION_ROLE_ID).toString());

				String userId = httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID)
						.toString();

				if (AuthorizationManager.getInstance().isAuthorizedUser("QUARTERLY_APPRAISAL", userRoleId)) {

					if ((httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_IS_ADMIN_ACCESS)
							.toString().equals("1"))
							|| AuthorizationManager.getInstance()
									.isAuthorizedForSurveyForm("QuarterlyAppraisalDashBoards.Access", userId)) {

						responseString = ServiceLocator.getNewAjaxHandlerService()
								.getQReviewDashBoardByStatus(getYear(), getQuarter(), httpServletRequest);
						httpServletResponse.setContentType("text");
						httpServletResponse.getWriter().write(responseString);
					}
				}
			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return null;

	}

	public String getPracticeVsStatusStackChart() {
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
				int userRoleId = Integer.parseInt(httpServletRequest.getSession(false)
						.getAttribute(ApplicationConstants.SESSION_ROLE_ID).toString());

				String userId = httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID)
						.toString();
				if (AuthorizationManager.getInstance().isAuthorizedUser("QUARTERLY_APPRAISAL", userRoleId)) {

					if ((httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_IS_ADMIN_ACCESS)
							.toString().equals("1"))
							|| AuthorizationManager.getInstance()
									.isAuthorizedForSurveyForm("QuarterlyAppraisalDashBoards.Access", userId)) {

						responseString = ServiceLocator.getNewAjaxHandlerService()
								.getPracticeVsStatusStackChart(getYear(), getQuarter(), httpServletRequest);
						httpServletResponse.setContentType("text");
						httpServletResponse.getWriter().write(responseString);
					}
				}
			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return null;

	}

	public String getClosedVsQuarterStackChart() {
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
				int userRoleId = Integer.parseInt(httpServletRequest.getSession(false)
						.getAttribute(ApplicationConstants.SESSION_ROLE_ID).toString());

				String userId = httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID)
						.toString();
				if (AuthorizationManager.getInstance().isAuthorizedUser("QUARTERLY_APPRAISAL", userRoleId)) {

					if ((httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_IS_ADMIN_ACCESS)
							.toString().equals("1"))
							|| AuthorizationManager.getInstance()
									.isAuthorizedForSurveyForm("QuarterlyAppraisalDashBoards.Access", userId)) {

						responseString = ServiceLocator.getNewAjaxHandlerService()
								.getClosedVsQuarterStackChart(getYear(), getQuarter(), httpServletRequest);
						httpServletResponse.setContentType("text");
						httpServletResponse.getWriter().write(responseString);
					}
				}
			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return null;

	}

	public String getQReviewListByStatus() {
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
				int userRoleId = Integer.parseInt(httpServletRequest.getSession(false)
						.getAttribute(ApplicationConstants.SESSION_ROLE_ID).toString());

				String userId = httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID)
						.toString();
				if (AuthorizationManager.getInstance().isAuthorizedUser("QUARTERLY_APPRAISAL", userRoleId)) {

					if ((httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_IS_ADMIN_ACCESS)
							.toString().equals("1"))
							|| AuthorizationManager.getInstance()
									.isAuthorizedForSurveyForm("QuarterlyAppraisalDashBoards.Access", userId)) {

						responseString = ServiceLocator.getNewAjaxHandlerService().getQReviewListByStatus(getYear(),
								getQuarter(), getActivityType(), httpServletRequest);
						httpServletResponse.setContentType("text");
						httpServletResponse.getWriter().write(responseString);
					}
				}
			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return null;

	}

	public String getQReviewListByStatusVsPractice() {
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
				int userRoleId = Integer.parseInt(httpServletRequest.getSession(false)
						.getAttribute(ApplicationConstants.SESSION_ROLE_ID).toString());

				String userId = httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID)
						.toString();
				if (AuthorizationManager.getInstance().isAuthorizedUser("QUARTERLY_APPRAISAL", userRoleId)) {

					if ((httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_IS_ADMIN_ACCESS)
							.toString().equals("1"))
							|| AuthorizationManager.getInstance()
									.isAuthorizedForSurveyForm("QuarterlyAppraisalDashBoards.Access", userId)) {

						responseString = ServiceLocator.getNewAjaxHandlerService().getQReviewListByStatusVsPractice(
								getYear(), getQuarter(), getActivityType(), getColumnLabel(), httpServletRequest);
						httpServletResponse.setContentType("text");
						httpServletResponse.getWriter().write(responseString);
					}
				}
			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return null;

	}

	public String getQReviewListByStatusVsQuarter() {
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
				int userRoleId = Integer.parseInt(httpServletRequest.getSession(false)
						.getAttribute(ApplicationConstants.SESSION_ROLE_ID).toString());

				String userId = httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID)
						.toString();
				if (AuthorizationManager.getInstance().isAuthorizedUser("QUARTERLY_APPRAISAL", userRoleId)) {

					if ((httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_IS_ADMIN_ACCESS)
							.toString().equals("1"))
							|| AuthorizationManager.getInstance()
									.isAuthorizedForSurveyForm("QuarterlyAppraisalDashBoards.Access", userId)) {

						responseString = ServiceLocator.getNewAjaxHandlerService().getQReviewListByStatusVsQuarter(
								getYear(), getQuarter(), getActivityType(), getColumnLabel(), httpServletRequest);
						httpServletResponse.setContentType("text");
						httpServletResponse.getWriter().write(responseString);
					}
				}
			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return null;

	}

	public String getQReviewListByManagers() {
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
				int userRoleId = Integer.parseInt(httpServletRequest.getSession(false)
						.getAttribute(ApplicationConstants.SESSION_ROLE_ID).toString());

				String userId = httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID)
						.toString();
				if (AuthorizationManager.getInstance().isAuthorizedUser("QUARTERLY_APPRAISAL", userRoleId)) {

					if ((httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_IS_ADMIN_ACCESS)
							.toString().equals("1"))
							|| AuthorizationManager.getInstance()
									.isAuthorizedForSurveyForm("QuarterlyAppraisalDashBoards.Access", userId)) {

						responseString = ServiceLocator.getNewAjaxHandlerService().getQReviewListByManagers(this,
								httpServletRequest);
						httpServletResponse.setContentType("text");
						httpServletResponse.getWriter().write(responseString);
					}
				}
			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return null;

	}

	public String getQReviewPendingListByManagers() {

		// System.out.println("Heloooo getQReviewPendingListByManagers");
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
				int userRoleId = Integer.parseInt(httpServletRequest.getSession(false)
						.getAttribute(ApplicationConstants.SESSION_ROLE_ID).toString());

				String userId = httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID)
						.toString();

				if (AuthorizationManager.getInstance().isAuthorizedUser("QUARTERLY_APPRAISAL", userRoleId)) {

					/*
					 * if ((httpServletRequest.getSession(false).getAttribute(
					 * ApplicationConstants.SESSION_IS_ADMIN_ACCESS) .toString().equals("1")) ||
					 * AuthorizationManager.getInstance() .isAuthorizedForSurveyForm(
					 * "QuarterlyAppraisalDashBoards.Access", userId)) {
					 */

					String empId = httpServletRequest.getSession(false)
							.getAttribute(ApplicationConstants.SESSION_EMP_ID).toString();
					responseString = ServiceLocator.getNewAjaxHandlerService().getQReviewPendingListByManagers(this,
							httpServletRequest, empId);
					httpServletResponse.setContentType("text");
					httpServletResponse.getWriter().write(responseString);
				}

			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return null;

	}

	public String getEmployeesForPendingMngrByApprasailStatus() {
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
				int userRoleId = Integer.parseInt(httpServletRequest.getSession(false)
						.getAttribute(ApplicationConstants.SESSION_ROLE_ID).toString());

				String userId = httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID)
						.toString();
				if (AuthorizationManager.getInstance().isAuthorizedUser("QUARTERLY_APPRAISAL", userRoleId)) {

					/*
					 * if ((httpServletRequest.getSession(false).getAttribute(
					 * ApplicationConstants.SESSION_IS_ADMIN_ACCESS) .toString().equals("1")) ||
					 * AuthorizationManager.getInstance() .isAuthorizedForSurveyForm(
					 * "QuarterlyAppraisalDashBoards.Access", userId)) {
					 */

					responseString = ServiceLocator.getNewAjaxHandlerService().getEmployeesForMngrByApprasailStatus(
							getYear(), getQuarter(), getLoginId(), getStatus(), getCountry(), getIsIncludeTeam(),
							httpServletRequest);
					httpServletResponse.setContentType("text");
					httpServletResponse.getWriter().write(responseString);
				}

			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}

		return null;

	}

	public String avaiableEmployeBySubPracticeList() {
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
				// responseString =
				// ServiceLocator.getNewAjaxHandlerService().avaiableEmployeBySubPracticeList();
				responseString = ServiceLocator.getNewAjaxHandlerService()
						.avaiableEmployeBySubPracticeList(getPracticeId());
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return null;

	}

	public String getEmployeesForMngrByApprasailStatus() {
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
				int userRoleId = Integer.parseInt(httpServletRequest.getSession(false)
						.getAttribute(ApplicationConstants.SESSION_ROLE_ID).toString());

				String userId = httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID)
						.toString();
				if (AuthorizationManager.getInstance().isAuthorizedUser("QUARTERLY_APPRAISAL", userRoleId)) {

					if ((httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_IS_ADMIN_ACCESS)
							.toString().equals("1"))
							|| AuthorizationManager.getInstance()
									.isAuthorizedForSurveyForm("QuarterlyAppraisalDashBoards.Access", userId)) {

						responseString = ServiceLocator.getNewAjaxHandlerService().getEmployeesForMngrByApprasailStatus(
								getYear(), getQuarter(), getLoginId(), getStatus(), getCountry(), getIsIncludeTeam(),
								httpServletRequest);
						httpServletResponse.setContentType("text");
						httpServletResponse.getWriter().write(responseString);
					}
				}
			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}

		return null;

	}

	public String getOffshoreEmployeeDetails() {
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
				// responseString =
				// ServiceLocator.getNewAjaxHandlerService().avaiableEmployeBySubPracticeList();
				responseString = ServiceLocator.getNewAjaxHandlerService().getOffshoreEmployeeDetails(getPracticeId(),
						getSubPracticeId(), getStatus());
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return null;

	}

	public String getEventNomineeSearch() throws Exception {
		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			userRoleId = Integer.parseInt(
					httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_ROLE_ID).toString());
			try {

				// setStartDate(DateUtility.getInstance().getLastYearFirstMonth());
				// setEndDate(DateUtility.getInstance().getCurrentMySqlDate());
				String nominee = ServiceLocator.getAjaxHandlerWebService().getEventNomineeSearch(this);
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(nominee);
			} catch (ServiceLocatorException ex) {
				ex.printStackTrace();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return null;
	}

	public String getNomineeDetails() throws Exception {
		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			userRoleId = Integer.parseInt(
					httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_ROLE_ID).toString());
			try {

				// setStartDate(DateUtility.getInstance().getLastYearFirstMonth());
				// setEndDate(DateUtility.getInstance().getCurrentMySqlDate());
				String nominee = ServiceLocator.getAjaxHandlerWebService().getNomineeDetails(getId());
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(nominee);
			} catch (ServiceLocatorException ex) {
				ex.printStackTrace();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return null;
	}

	public String getEmployeeDetailsForEvents() throws Exception {
		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			userRoleId = Integer.parseInt(
					httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_ROLE_ID).toString());
			try {

				// setStartDate(DateUtility.getInstance().getLastYearFirstMonth());
				// setEndDate(DateUtility.getInstance().getCurrentMySqlDate());
				String nominee = ServiceLocator.getAjaxHandlerWebService().getEmployeeDetailsForEvents(getLoginId());
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(nominee);
			} catch (ServiceLocatorException ex) {
				ex.printStackTrace();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return null;
	}

	public String updateNomineeStatus() throws Exception {
		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			userRoleId = Integer.parseInt(
					httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_ROLE_ID).toString());
			try {

				// setStartDate(DateUtility.getInstance().getLastYearFirstMonth());
				// setEndDate(DateUtility.getInstance().getCurrentMySqlDate());
				String nominee = ServiceLocator.getAjaxHandlerWebService().doUpdateNomineeStatus(this);
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(nominee);
			} catch (ServiceLocatorException ex) {
				ex.printStackTrace();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return null;
	}
	// sendEventNominationReminder

	public String sendEventNominationReminder() throws Exception {
		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			// userRoleId =
			// Integer.parseInt(httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_ROLE_ID).toString());
			try {

				String response = ServiceLocator.getAjaxHandlerWebService().sendEventNominationReminder(this);
				// System.out.println("response-->"+response);
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(response);
			} catch (ServiceLocatorException ex) {
				ex.printStackTrace();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return null;
	}

	/* Star Performer related methods */
	public String starPerformers() {
		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			try {

				responseString = ServiceLocator.getNewAjaxHandlerService().starPerformers(this);
				httpServletResponse.setContentType("text/xml");
				httpServletResponse.getWriter().write(responseString);
			} catch (ServiceLocatorException ex) {
				ex.printStackTrace();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return null;
	}

	public String doUpdateStarPerformer() {
		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			try {

				responseString = ServiceLocator.getNewAjaxHandlerService().doUpdateStarPerformer(getObjectId(),
						getOverlayStatus());

				httpServletResponse.setContentType("text/xml");
				httpServletResponse.getWriter().write(responseString);
			} catch (ServiceLocatorException ex) {
				ex.printStackTrace();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return null;
	}

	public String addStarDetails() {

		try {
			setYear(getYear());
			setMonth(getMonth());

			GregorianCalendar gc = new GregorianCalendar(getOverlayYear(), getOverlayMonth() - 1, 1);
			java.util.Date monthEndDate = new java.util.Date(gc.getTime().getTime());

			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");// yyyyMMdd
			setOverlayDate(format.format(monthEndDate));
			String loginId = httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID)
					.toString();
			setCreatedDate(DateUtility.getInstance().getCurrentMySqlDateTime());
			setModifiedDate(DateUtility.getInstance().getCurrentMySqlDateTime());
			setCreatedBy(
					httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID).toString());

			setModifiedBy(
					httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID).toString());

			if (ServiceLocator.getNewAjaxHandlerService().checkStarDetails(this)) {
				int row = 0;
				row = ServiceLocator.getNewAjaxHandlerService().addStarDetails(this);
				if (row > 0) {
					responseString = "<font size='2' color='green'>Star Performer for particular month has been Added successfully</font>";
				} else {
					responseString = "Plese try again!";
				}
			} else {
				responseString = "<font size='2' color='red'>Star Performer for particular month has been recorded already</font>";
			}

			httpServletResponse.setContentType("text");
			httpServletResponse.getWriter().write(responseString);
		} catch (ServiceLocatorException ex) {
			System.err.println(ex);
		} catch (IOException ioe) {
			System.err.println(ioe);
		}
		return null;
	}

	public String dogetNomineesList() {
		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			try {
				String loginId = httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID)
						.toString();
				String statusflag = httpServletRequest.getSession(false)
						.getAttribute(ApplicationConstants.STAR_PERFORMER_STATUS).toString();
				String Accessflag = httpServletRequest.getSession(false)
						.getAttribute(ApplicationConstants.STAR_PERFORMER_ACCESS_FOR_HR_MR).toString();
				Map teamMap = (Map) httpServletRequest.getSession(false)
						.getAttribute(ApplicationConstants.SESSION_MY_TEAM_MAP);

				responseString = ServiceLocator.getNewAjaxHandlerService().dogetNomineesList(getObjectId(), statusflag,
						Accessflag, loginId, teamMap);

				httpServletResponse.setContentType("text/xml");
				httpServletResponse.getWriter().write(responseString);
			} catch (ServiceLocatorException ex) {
				ex.printStackTrace();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return null;
	}

	public String doSubmitForApproval() {

		try {
			// System.out.println("doSubmitForApproval");

			setMail(httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_EMAIL).toString());

			responseString = ServiceLocator.getNewAjaxHandlerService().doSubmitForApproval(this);

			httpServletResponse.setContentType("text");
			httpServletResponse.getWriter().write(responseString);
		} catch (ServiceLocatorException ex) {
			System.err.println(ex);
		} catch (IOException ioe) {
			System.err.println(ioe);
		}
		return null;
	}

	public String getEmployeeDetailStarList() {
		// System.out.println("in action-->");
		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			try {
				String loginId = httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID)
						.toString();
				Map teamMap = (Map) httpServletRequest.getSession(false)
						.getAttribute(ApplicationConstants.SESSION_MY_TEAM_MAP);
				// System.out.println("in loginId-->"+loginId);
				String result = ServiceLocator.getNewAjaxHandlerService().getEmployeeDetailStarList(getCustomerName(),
						loginId, teamMap);
				// System.out.println("in action-->"+result);
				httpServletResponse.setContentType("text/xml");
				httpServletResponse.getWriter().write(result);
			} catch (ServiceLocatorException ex) {
				ex.printStackTrace();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return null;

	}

	public String doSMCampaignIntegration() {
		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			try {

				int rows = 0;
				rows = ServiceLocator.getNewAjaxHandlerService().doSMCampaignIntegration(getObjectId(),
						getCampaignStartDate(), getMonth(), getYear());
				if (rows > 0) {
					responseString = "<font size='2' color='green'>succefully scheduled</font>";
				} else {
					responseString = "<font size='2' color='red'>Please try again later</font>";
				}
				httpServletResponse.setContentType("text/xml");
				httpServletResponse.getWriter().write(responseString);
			} catch (ServiceLocatorException ex) {
				ex.printStackTrace();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return null;
	}

	public String doAddSurveyFormAndQuestionnaire() {
		resultType = LOGIN;

		try {
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {

				setCreatedBy(httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID)
						.toString());
				JSONObject jb = new JSONObject();

				jb = ServiceLocator.getAjaxHandlerWebService().doAddSurveyFormAndQuestionnaire(this);

				int Id = jb.getInt("Id");

				if (Id > 0) {
					// System.out.println("Id"+Id);
					int rows = ServiceLocator.getNewAjaxHandlerService().doRSVPIntegration(getObjectId(), getYear(),
							getMonth(), Id, getMovieDates(), getExpireDate());

					if (rows > 0) {
						responseString = "<font size='2' color='green'>Succefully scheduled</font>";
					} else {
						responseString = "<font size='2' color='red'>Please try again later</font>";
					}
				} else {
					responseString = "<font size='2' color='red'>Please try again later</font>";
				}
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			}
		} catch (JSONException exp) {
			System.err.println(exp);
		} catch (ServiceLocatorException ex) {
			System.err.println(ex);
		} catch (IOException ioe) {
			System.err.println(ioe);
		}

		// System.out.println("addTrackName end");
		return null;
	}

	public String getEmpMovieTicketList() {
		resultType = LOGIN;

		try {
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {

				setCreatedBy(httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID)
						.toString());
				// System.out.println("doAddSurveyFormAndQuestionnaire");
				JSONObject jb = new JSONObject();

				jb = ServiceLocator.getAjaxHandlerWebService().doAddRemainder(this);

				Iterator<?> keys = jb.keys();

				List email = new ArrayList();
				while (keys.hasNext()) {
					String key = (String) keys.next();

					// System.out.println("Sub map-->"+key);
					// System.out.println("Sub map-->"+key);
					String emailID = jb.getString(key);
					email.add(emailID);

				}

				String responseString = ServiceLocator.getNewAjaxHandlerService().doEmpMovieTicketList(getObjectId(),
						getSurvyId(), email);

				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			}
		} catch (JSONException ex) {
			System.err.println(ex);
		} catch (ServiceLocatorException ex) {
			System.err.println(ex);
		} catch (IOException ioe) {
			System.err.println(ioe);
		}

		// System.out.println("addTrackName end");
		return null;
	}

	public String doAddSurveyfeedBack() {
		resultType = LOGIN;

		try {
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {

				setCreatedBy(httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID)
						.toString());
				JSONObject jb = new JSONObject();

				jb = ServiceLocator.getAjaxHandlerWebService().doAddSurveyfeedBack(this);

				int Id = jb.getInt("Id");

				if (Id > 0) {

					JSONObject jbb = new JSONObject();

					jbb = ServiceLocator.getAjaxHandlerWebService().doAddRemainder(this);

					Iterator<?> keys = jbb.keys();

					List email = new ArrayList();
					while (keys.hasNext()) {
						String key = (String) keys.next();

						String emailID = jbb.getString(key);
						email.add(emailID);

					}

					if (email.size() > 0)

					{

						int rows = ServiceLocator.getNewAjaxHandlerService().doFeedBackIntegration(getObjectId(),
								getYear(), getMonth(), Id, email);

						if (rows > 0) {
							responseString = "<font size='2' color='green'>succefully scheduled</font>";
						} else {
							responseString = "<font size='2' color='red'>Please try again later</font>";
						}
					} else {
						responseString = "<font size='2' color='red'>NO Response Addedd</font>";
					}
				} else {
					responseString = "<font size='2' color='red'>Please try again later</font>";
				}
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			}
		} catch (JSONException ex) {
			System.err.println(ex);
		}
		// catch (JSONException ex) {
		// //Logger.getLogger(NewAjaxHandlerAction.class.getName()).log(Level.SEVERE,
		// null, ex);
		// }
		catch (ServiceLocatorException ex) {
			System.err.println(ex);
		} catch (IOException ioe) {
			System.err.println(ioe);
		}

		// System.out.println("addTrackName end");
		return null;
	}

	/*
	 * project risk reports dashboard added by triveni on june 28th 2017
	 * 
	 */
	public String getProjectRiskDetails() {
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
				responseString = ServiceLocator.getNewAjaxHandlerService().getProjectRiskDetails(this);
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return null;

	}

	/*
	 * Performance Date: 08/04/2017 nagalakshmi telluri
	 */

	public String getEmployeePerformance() {
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {

				responseString = ServiceLocator.getNewAjaxHandlerService().getEmployeePerformance(this);
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return null;
	}

	public String getOnProjectEmployeeUtilizationDetails() {
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
				responseString = ServiceLocator.getNewAjaxHandlerService()
						.getOnProjectEmployeeUtilizationDetails(getEmpId());
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return null;

	}

	/*
	 * Description : Get comments for employee reason for exit Author : Teja
	 * Kadamanti Date : 08/31/2017
	 */
	public String getReasonForExit() {
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
				// System.out.println("getEmpNo..."+getEmpNo());
				responseString = ServiceLocator.getNewAjaxHandlerService().getReasonForExit(getEmpId());
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return null;

	}

	/*
	 * Nagalakshmi Telluri 5/17/2017
	 */

	public String getTimeSheettotalhoursbyProjectList() {
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
				// responseString =
				// ServiceLocator.getNewAjaxHandlerService().avaiableEmployeBySubPracticeList();
				responseString = ServiceLocator.getNewAjaxHandlerService().getTimeSheettotalhoursbyProjectList(this);
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return null;

	}

	/* Nagalakshmi Telluri */
	/* 5/17/2017 */

	public String gettotalweekcountbyprojectDetails() {
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
				// responseString =
				// ServiceLocator.getNewAjaxHandlerService().avaiableEmployeBySubPracticeList();
				responseString = ServiceLocator.getNewAjaxHandlerService().gettotalweekcountbyprojectDetails(this);
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return null;

	}

	public String getDateAndTotalHoursByProject() {

		System.out.println("enter into action method");
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {

				responseString = ServiceLocator.getNewAjaxHandlerService().getDateAndTotalHoursByProject(this);
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return null;
	}

	/* LookUp changes */

	public String getlookUpReport() {
		try {
			System.out.println("in action ajax");
			/*
			 * This if loop is to check whether there is Session or not
			 **/
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {

				responseString = ServiceLocator.getNewAjaxHandlerService().getlookUpReport(this, httpServletRequest);
				// responseString =
				// ServiceLocator.getNewAjaxHandlerService().doPopulateAccountDetails(getId());

				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return null;
	}

	public String addColumnNames() {

		try {
			System.out.println("getTableName() of addColumnNames() in NewAjaxHandlerAction====>" + getTableName());
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
				if (ServiceLocator.getNewAjaxHandlerService().addColumnNames(this, httpServletRequest) >= 1) {
					responseString = "<font size='2' color='green'>succefully Added</font>";
				} else {
					responseString = "<font size='2' color='red'>Please try again later</font>";
				}
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			}
		} catch (ServiceLocatorException ex) {
			System.err.println(ex);
		} catch (IOException ioe) {
			System.err.println(ioe);
		}
		return null;
	}

	public String getLookUpDetails() {
		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {

			try {
				responseString = ServiceLocator.getNewAjaxHandlerService().getLookUpDetails(getLookUpId(),
						getTableName());
				// System.out.println("responseString---->"+responseString);
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} catch (Exception ex) {
			}

		} // Close Session Checking
		return null;
	}

	public String updateColumnNames() {

		try {
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
				if (!ServiceLocator.getNewAjaxHandlerService().updateColumnNames(this, httpServletRequest)) {
					responseString = "<font size='2' color='green'>succefully Updated</font>";
				} else {
					responseString = "<font size='2' color='red'>Please try again later</font>";
				}
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			}
		} catch (ServiceLocatorException ex) {
			System.err.println(ex);
		} catch (IOException ioe) {
			System.err.println(ioe);
		}
		return null;
	}

	public String getLookUpTableDetails() {
		try {
			responseString = ServiceLocator.getNewAjaxHandlerService()
					.getLookUpTableDetails("SELECT TableName, Id FROM tblLookupTables where TableName LIKE '%"
							+ getTableName() + "%' LIMIT 30");
			httpServletResponse.setContentType("text/xml");
			httpServletResponse.getWriter().write(responseString);
		} catch (ServiceLocatorException ex) {
			System.err.println(ex);
		} catch (IOException ioe) {
			System.err.println(ioe);
		}
		return null;
	}

	public String doGetExperienceReport() {
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {

				responseString = ServiceLocator.getNewAjaxHandlerService().doGetExperienceReport(this);
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return null;

	}

	public String popupTenurStatus() {
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {

				responseString = ServiceLocator.getNewAjaxHandlerService().popupTenurStatus(this);

				// System.out.println("responseString"+responseString);
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return null;

	}

	public String getCountVsStatusReport() {
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
				responseString = ServiceLocator.getNewAjaxHandlerService().getCountVsStatusReport(getGraphId(),
						getCountry(), getDepartment(), getPracticeId(), getSubPractice(), getAvailableUtl(),
						httpServletRequest);
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return null;

	}

	public String getUtilizationDetails() {
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
				responseString = ServiceLocator.getNewAjaxHandlerService().getUtilizationDetails(getEmpId(),
						httpServletRequest);
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return null;

	}

	public String getResourcesByStatusList() {
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
				responseString = ServiceLocator.getNewAjaxHandlerService().getResourcesByStatusList(getActivityType(),
						getGraphId(), getCountry(), getDepartment(), getPracticeId(), getSubPractice(),
						getAvailableUtl(), httpServletRequest);
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return null;

	}

	/*
	 * Employee Verification Details Author : Santosh Kola Date : 10/17/2017
	 */

	public String getEmployeeVerificationDetails() {
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
				responseString = ServiceLocator.getNewAjaxHandlerService()
						.getEmployeeVerificationDetails(httpServletRequest, getInfoId());
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;

	}

	/*
	 * Update Employee Verification Details Author : Santosh Kola Date : 10/17/2017
	 */

	public String updateEmployeeVerificationDetails() {
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
				String userId = httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID)
						.toString();
				responseString = ServiceLocator.getAjaxHandlerWebService()
						.updateEmployeeVerificationDetails(getEmployeeVerificationDetailsJson(), userId);
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.apache.struts2.interceptor.ServletRequestAware#setServletRequest(
	 * javax.servlet.http.HttpServletRequest)
	 */
	/* Release Notes */

	public String releaseNotesAttachmentUpload() {

		try {
			String generatedPath = null;
			if (getFile0FileName() != null) {
				attachmentService = ServiceLocator.getAttachmentService();
				generatedPath = attachmentService.generatePath(
						com.mss.mirage.util.Properties.getProperty("ReleaseNotes.Attachments"), "ReleaseNotes");
				File targetDirectory = new File(generatedPath + "/" + getFile0FileName());
				setAttachmentLocation(targetDirectory.getAbsolutePath());
				FileUtils.copyFile(getFile0(), targetDirectory);
				// setObjectType("Emp Reviews");
				responseString = getAttachmentLocation();
			}
			if (getFile1FileName() != null) {
				attachmentService = ServiceLocator.getAttachmentService();
				generatedPath = attachmentService.generatePath(
						com.mss.mirage.util.Properties.getProperty("ReleaseNotes.Attachments"), "ReleaseNotes");
				File targetDirectory = new File(generatedPath + "/" + getFile1FileName());
				setAttachmentLocation(targetDirectory.getAbsolutePath());
				FileUtils.copyFile(getFile1(), targetDirectory);
				// setObjectType("Emp Reviews");
				responseString = getAttachmentLocation();
			}
			if (getFile2FileName() != null) {
				attachmentService = ServiceLocator.getAttachmentService();
				generatedPath = attachmentService.generatePath(
						com.mss.mirage.util.Properties.getProperty("ReleaseNotes.Attachments"), "ReleaseNotes");

				File targetDirectory = new File(generatedPath + "/" + getFile2FileName());
				setAttachmentLocation(targetDirectory.getAbsolutePath());
				FileUtils.copyFile(getFile2(), targetDirectory);
				// setObjectType("Emp Reviews");
				responseString = getAttachmentLocation();
			}
			if (getFile3FileName() != null) {
				attachmentService = ServiceLocator.getAttachmentService();
				generatedPath = attachmentService.generatePath(
						com.mss.mirage.util.Properties.getProperty("ReleaseNotes.Attachments"), "ReleaseNotes");

				File targetDirectory = new File(generatedPath + "/" + getFile3FileName());
				setAttachmentLocation(targetDirectory.getAbsolutePath());
				FileUtils.copyFile(getFile3(), targetDirectory);
				// setObjectType("Emp Reviews");
				responseString = getAttachmentLocation();
			}
			if (getFile4FileName() != null) {
				attachmentService = ServiceLocator.getAttachmentService();
				generatedPath = attachmentService.generatePath(
						com.mss.mirage.util.Properties.getProperty("ReleaseNotes.Attachments"), "ReleaseNotes");

				File targetDirectory = new File(generatedPath + "/" + getFile4FileName());
				setAttachmentLocation(targetDirectory.getAbsolutePath());
				FileUtils.copyFile(getFile4(), targetDirectory);
				// setObjectType("Emp Reviews");
				responseString = getAttachmentLocation();
			}
			if (getFile5FileName() != null) {
				attachmentService = ServiceLocator.getAttachmentService();

				generatedPath = attachmentService.generatePath(
						com.mss.mirage.util.Properties.getProperty("ReleaseNotes.Attachments"), "ReleaseNotes");

				File targetDirectory = new File(generatedPath + "/" + getFile5FileName());
				setAttachmentLocation(targetDirectory.getAbsolutePath());

				FileUtils.copyFile(getFile5(), targetDirectory);
				// setObjectType("Emp Reviews");
				responseString = getAttachmentLocation();
			} else {

				setAttachmentLocation("");
				// setFilepath("");
				// attachmentName = "";
			}

			httpServletResponse.setContentType("text");
			httpServletResponse.getWriter().write(responseString);
		} catch (ServiceLocatorException ex) {
			System.err.println(ex);
		} catch (IOException ioe) {
			System.err.println(ioe);
		}
		return null;
	}

	public String popupSubTitleDescWindow() throws Exception {
		String str;
		/*
		 * This if loop is to check whether there is Session or not
		 **/
		// if(httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID)
		// != null){
		if ((httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null)
				|| (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_CUST_ID) != null)) {
			try {
				str = ServiceLocator.getNewAjaxHandlerService().popupSubTitleDescWindow(releaseId);
				httpServletResponse.setContentType("text/html");
				httpServletResponse.getWriter().write((str));
			} catch (ServiceLocatorException ex) {
				ex.printStackTrace();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		} // Close Session Checking

		return null;
	}

	public String popupPurposeDescWindow() throws Exception {
		String str;
		/*
		 * This if loop is to check whether there is Session or not
		 **/
		// if(httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID)
		// != null){
		if ((httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null)
				|| (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_CUST_ID) != null)) {
			try {
				str = ServiceLocator.getNewAjaxHandlerService().popupPurposeDescWindow(releaseId);
				httpServletResponse.setContentType("text/html");
				httpServletResponse.getWriter().write((str));
			} catch (ServiceLocatorException ex) {
				ex.printStackTrace();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		} // Close Session Checking

		return null;
	}

	public String getReleaseNotesData() {
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/

			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {

				responseString = ServiceLocator.getNewAjaxHandlerService().getReleaseNotesData(this,
						httpServletRequest);
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return null;
	}

	public String getReleaseNotesMonthData() {
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/

			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {

				responseString = ServiceLocator.getNewAjaxHandlerService().getReleaseNotesMonthData(this,
						httpServletRequest);
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return null;
	}

	public String getReleaseNotesTitleInfo() {
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/

			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {

				responseString = ServiceLocator.getNewAjaxHandlerService().getReleaseNotesTitleInfo(this,
						httpServletRequest);
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return null;
	}

	public String getReleaseNotesInfoData() {
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/

			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {

				responseString = ServiceLocator.getNewAjaxHandlerService().getReleaseNotesInfoData(this,
						httpServletRequest);
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return null;
	}

	/* Timesheet audit start */
	/*
	 * Author Anand Patnaik Date 10-31-2017
	 */

	/***********************************************************
	 * Customer list by cost Model
	 ********************************************************/
	public String doGetCustomerDetailsByCostModel() {
		// System.out.println("in action-->");
		try {

			String loginId = "";
			String practiceId = httpServletRequest.getSession(false)
					.getAttribute(ApplicationConstants.SESSION_EMP_PRACTICE).toString();
			String access[] = SecurityProperties.getProperty("TIMESHEET_AUDIT_FULL_ACCESS").split(Pattern.quote(","));
			List accessList = Arrays.asList(access);
			loginId = httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID)
					.toString();
			String myTeamMembers = "";
			if (practiceId.equals("PMO")) {
				if (accessList.contains(loginId)) {
					// System.out.println("full access");
					// System.out.println("myTeamMembers=="+myTeamMembers);
				} else {
					department = httpServletRequest.getSession(false)
							.getAttribute(ApplicationConstants.SESSION_MY_DEPT_ID).toString();
					Map teamMap = DataSourceDataProvider.getInstance().getMyTeamMembers(loginId, department);

					myTeamMembers = DataSourceDataProvider.getInstance().getTeamLoginIdList(teamMap);
					myTeamMembers = myTeamMembers.replaceAll("'", "");
					if (myTeamMembers.equals("")) {
						myTeamMembers = loginId;
					} else {
						myTeamMembers = myTeamMembers + "," + loginId;
					}

				}
			}
			// System.out.println("myTeamMembers=="+myTeamMembers);
			JSONObject jObject = new JSONObject(getJsonData());
			String CostModel = jObject.getString("costModel");
			// String fromId =
			// httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID).toString();
			String result = ServiceLocator.getNewAjaxHandlerService().doGetCustomerDetailsByCostModel(CostModel,
					myTeamMembers);
			// System.out.println("in action-->"+result);
			httpServletResponse.setContentType("text");
			httpServletResponse.getWriter().write(result);
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;

	}

	/***********************************************************
	 * Employee list for TImesheet audit
	 ********************************************************/
	public String doGetEmployeeForTimeSheetHrs() {
		// System.out.println("in action-->");
		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			try {
				String result = ServiceLocator.getNewAjaxHandlerService()
						.doGetEmployeeForTimeSheetHrs(getEmployeeName());
				httpServletResponse.setContentType("text/xml");
				httpServletResponse.getWriter().write(result);
			} catch (ServiceLocatorException ex) {
				ex.printStackTrace();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return null;

	}

	/*
	 * TImesheet audit author : anand patnaik date : 15-10-2017
	 * 
	 */

	public String doGetLoadForAudit() {
		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			// AjaxHandlerAction ajaxHandlerAction = new AjaxHandlerAction();

			try {

				// System.out.println("values :::::::");

				responseString = ServiceLocator.getNewAjaxHandlerService().doGetLoadForAudit(getJsonData());
				// System.out.println("responseString "+responseString);
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);

			} catch (ServiceLocatorException ex) {
				ex.printStackTrace();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return null;
	}

	/*
	 * TImesheet audit --> save invoice details author : anand patnaik date :
	 * 15-09-2017
	 * 
	 */

	public String doSaveInvoiceDetails() {
		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			try {

				setCreatedBy(httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID)
						.toString());
				responseString = ServiceLocator.getNewAjaxHandlerService().doSaveInvoiceDetails(getJsonData(),
						getCreatedBy());
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);

			} catch (ServiceLocatorException ex) {
				ex.printStackTrace();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return null;
	}
	/*
	 * author Anand Patnaik Date 10-13-2017
	 */

	public String doGetLoadTimeSheetDataForAudit() {
		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {

			try {

				responseString = ServiceLocator.getNewAjaxHandlerService()
						.doGetLoadTimeSheetDataForAudit(getJsonData());
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);

			} catch (ServiceLocatorException ex) {
				ex.printStackTrace();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return null;
	}

	public String doGetTimeSheetAuditSearch() {
		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			// AjaxHandlerAction ajaxHandlerAction = new AjaxHandlerAction();

			try {

				String loginId = "";
				String practiceId = httpServletRequest.getSession(false)
						.getAttribute(ApplicationConstants.SESSION_EMP_PRACTICE).toString();
				String access[] = SecurityProperties.getProperty("TIMESHEET_AUDIT_FULL_ACCESS")
						.split(Pattern.quote(","));
				List accessList = Arrays.asList(access);
				loginId = httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID)
						.toString();
				String myTeamMembers = "";
				if (practiceId.equals("PMO")) {
					if (accessList.contains(loginId)) {
						// System.out.println("full access");
						// System.out.println("myTeamMembers=="+myTeamMembers);
					} else {
						department = httpServletRequest.getSession(false)
								.getAttribute(ApplicationConstants.SESSION_MY_DEPT_ID).toString();
						Map teamMap = DataSourceDataProvider.getInstance().getMyTeamMembers(loginId, department);

						myTeamMembers = DataSourceDataProvider.getInstance().getTeamLoginIdList(teamMap);
						myTeamMembers = myTeamMembers.replaceAll("'", "");
						if (myTeamMembers.equals("")) {
							myTeamMembers = loginId;
						} else {
							myTeamMembers = myTeamMembers + "," + loginId;
						}

					}
				}
				responseString = ServiceLocator.getNewAjaxHandlerService().doGetTimeSheetAuditSearch(getJsonData(),
						myTeamMembers);
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);

			} catch (ServiceLocatorException ex) {
				ex.printStackTrace();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return null;
	}

	public String doUpdateAuditComment() {
		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			try {

				setCreatedBy(httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID)
						.toString());
				responseString = ServiceLocator.getNewAjaxHandlerService().doUpdateAuditComment(getJsonData(),
						getCreatedBy());
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);

			} catch (ServiceLocatorException ex) {
				ex.printStackTrace();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return null;
	}
	/* Timesheet audit end */

	public String doGetEmployeeNames() {
		try {
			// System.out.println("email--"+getEmail());
			// System.out.println("SELECT CONCAT(TRIM(FName),'.',TRIM(LName)) AS
			// FullName,LoginId FROM tblEmployee WHERE (LName LIKE '" +
			// employeeName + "%' OR FName LIKE '" + employeeName + "%')");
			// responseString =
			// ServiceLocator.getAjaxHandlerService().getConsultantList("SELECT
			// Id,Email2,CellPhoneNo,AvailableFrom,IsReject,RatePerHour,WorkAuthorization
			// FROM tblRecConsultant where Email2 LIKE '" + getEmail() + "%'");
			// responseString =
			// ServiceLocator.getPayrollAjaxHandlerService().doGetEmployeeNames("SELECT
			// CONCAT(TRIM(FName),'.',TRIM(LName)) AS FullName,LoginId FROM
			// tblEmployee WHERE (LName LIKE '" + employeeName + "%' OR FName
			// LIKE '" + employeeName + "%')");

			int empId = Integer.parseInt(
					httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_EMP_ID).toString());

			responseString = ServiceLocator.getNewAjaxHandlerService().doGetEmployeeNames(
					"SELECT CONCAT(TRIM(FName),'.',TRIM(LName)) AS FullName,LoginId,Id,EmpNo,DepartmentId,TitleTypeId FROM tblEmployee WHERE (LName LIKE '"
							+ employeeName + "%' OR FName LIKE '" + employeeName + "%' OR EmpNo LIKE '" + employeeName
							+ "%') AND Country='India' AND CurStatus ='Active' AND OpsContactId = " + empId + "",
					empId);
			// System.out.println("responseString--"+responseString);
			httpServletResponse.setContentType("text/xml");
			httpServletResponse.getWriter().write(responseString);
		} catch (ServiceLocatorException ex) {
			System.err.println(ex);
		} catch (IOException ioe) {
			System.err.println(ioe);
		}
		return null;
	}

	public String getExpencesDetails() {
		try {
			// System.out.println("email--"+getEmail());
			// System.out.println("SELECT CONCAT(TRIM(FName),'.',TRIM(LName)) AS
			// FullName,LoginId FROM tblEmployee WHERE (LName LIKE '" +
			// employeeName + "%' OR FName LIKE '" + employeeName + "%')");
			// responseString =
			// ServiceLocator.getAjaxHandlerService().getConsultantList("SELECT
			// Id,Email2,CellPhoneNo,AvailableFrom,IsReject,RatePerHour,WorkAuthorization
			// FROM tblRecConsultant where Email2 LIKE '" + getEmail() + "%'");
			// responseString =
			// ServiceLocator.getPayrollAjaxHandlerService().doGetEmployeeNames("SELECT
			// CONCAT(TRIM(FName),'.',TRIM(LName)) AS FullName,LoginId FROM
			// tblEmployee WHERE (LName LIKE '" + employeeName + "%' OR FName
			// LIKE '" + employeeName + "%')");
			List empExpencesDetails = new ArrayList();
			empExpencesDetails = DataSourceDataProvider.getInstance().getEmpExpencesThroughEmpId((getEmpId()));
			StringBuffer sb = new StringBuffer();
			sb.append("<xml version=\"1.0\">");
			sb.append("<EMPLOYEES>");
			sb.append("<EMPLOYEE><VALID>true</VALID>");
			if (empExpencesDetails.get(0).equals("NoData")) {
				sb.append("<Accommodation> </Accommodation>");
				sb.append("<RoomNo>" + 0 + "</RoomNo>");
				sb.append("<RoomFee>" + 0.00 + "</RoomFee>");
				sb.append("<Cafeteria> </Cafeteria>");
				sb.append("<CafeteriaFee>" + 0.00 + "</CafeteriaFee>");
				sb.append("<Transportation> </Transportation>");
				sb.append("<TransportLocation> </TransportLocation>");
				sb.append("<TransportFee>" + 0.00 + "</TransportFee>");
				sb.append("<OccupancyType> </OccupancyType>");
				sb.append("<DateOfOccupancy> </DateOfOccupancy>");
				sb.append("<ElectricalCharges>" + 0.00 + "</ElectricalCharges>");
				sb.append("<FromLocation> </FromLocation>");

			} else {
				if ((String) empExpencesDetails.get(0) != null && !"".equals((String) empExpencesDetails.get(0))) {
					sb.append("<Accommodation>" + (String) empExpencesDetails.get(0) + "</Accommodation>");
				} else {
					sb.append("<Accommodation> </Accommodation>");
				}
				sb.append("<RoomNo>" + (Integer) empExpencesDetails.get(1) + "</RoomNo>");
				sb.append("<RoomFee>" + (Double) empExpencesDetails.get(2) + "</RoomFee>");
				if ((String) empExpencesDetails.get(3) != null && !"".equals((String) empExpencesDetails.get(3))) {
					sb.append("<Cafeteria>" + (String) empExpencesDetails.get(3) + "</Cafeteria>");
				} else {
					sb.append("<Cafeteria> </Cafeteria>");
				}
				sb.append("<CafeteriaFee>" + (Double) empExpencesDetails.get(4) + "</CafeteriaFee>");
				if ((String) empExpencesDetails.get(5) != null && !"".equals((String) empExpencesDetails.get(5))) {
					sb.append("<Transportation>" + (String) empExpencesDetails.get(5) + "</Transportation>");
				} else {
					sb.append("<Transportation> </Transportation>");
				}
				if ((String) empExpencesDetails.get(6) != null && !"".equals((String) empExpencesDetails.get(6))) {
					sb.append("<TransportLocation>" + (String) empExpencesDetails.get(6) + "</TransportLocation>");
				} else {
					sb.append("<TransportLocation> </TransportLocation>");
				}
				sb.append("<TransportFee>" + (Double) empExpencesDetails.get(7) + "</TransportFee>");
				if ((String) empExpencesDetails.get(8) != null && !"".equals((String) empExpencesDetails.get(8))) {
					sb.append("<OccupancyType>" + (String) empExpencesDetails.get(8) + "</OccupancyType>");
				} else {
					sb.append("<OccupancyType> </OccupancyType>");

				}
				if ((String) empExpencesDetails.get(9) != null && !"".equals((String) empExpencesDetails.get(9))) {
					sb.append("<DateOfOccupancy>" + (String) empExpencesDetails.get(9) + "</DateOfOccupancy>");
				} else {
					sb.append("<DateOfOccupancy> </DateOfOccupancy>");
				}
				sb.append("<ElectricalCharges>" + (Double) empExpencesDetails.get(10) + "</ElectricalCharges>");
				if ((String) empExpencesDetails.get(11) != null && !"".equals((String) empExpencesDetails.get(11))) {
					sb.append("<FromLocation>" + (String) empExpencesDetails.get(11) + "</FromLocation>");
				} else {
					sb.append("<FromLocation> </FromLocation>");
				}
			}
			sb.append("</EMPLOYEE>");
			sb.append("</EMPLOYEES>");
			sb.append("</xml>");
			responseString = sb.toString();
			// System.out.println("responseString--"+responseString);
			httpServletResponse.setContentType("text/xml");
			httpServletResponse.getWriter().write(responseString);
		} catch (ServiceLocatorException ex) {
			System.err.println(ex);
		} catch (IOException ioe) {
			System.err.println(ioe);
		}
		return null;
	}

	/**
	 * This method is to show Stacked Chart for Employee Count based on countries
	 * Created By: Triveni Niddara Created Date : 11/5/2017
	 * 
	 * @return
	 */
	public String getEmpCountryCount() {
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
				responseString = ServiceLocator.getNewAjaxHandlerService().getEmpCountryCount();
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return null;

	}

	/**
	 * This method is to show Pie Chart for Employee Count based on Practices
	 * Created By: Triveni Niddara Created Date : 11/5/2017
	 * 
	 * @return
	 */

	public String getEmpPracticeCount() {
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
				responseString = ServiceLocator.getNewAjaxHandlerService().getEmpPracticeCount(getCountry(),
						getDepartmentId());
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return null;

	}

	/**
	 * This method is to show Pie Chart for Employee Count based on SubPractices
	 * Created By: Triveni Niddara Created Date : 11/5/2017
	 * 
	 * @return
	 */

	public String getEmployeeSubPracticeCount() {
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
				// System.out.println("im in");
				responseString = ServiceLocator.getNewAjaxHandlerService().getEmpSubPracticeCount(getCountry(),
						getDepartmentId(), getPracticeId());
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return null;

	}

	/**
	 * This method is to show Pie Chart for Employee Count based on CurStatus
	 * Created By: Triveni Niddara Created Date : 11/5/2017
	 * 
	 * @return
	 */
	public String getEmpCurStatePieChart() {
		try {

			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
				responseString = ServiceLocator.getNewAjaxHandlerService().getEmpCurStatePieChart(getGraphId(),
						getCountry(), getDepartmentId(), getPracticeId(), getSubPractice());
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return null;

	}

	/**
	 * This method is to Created By: Triveni Niddara Created Date : 11/5/2017
	 * 
	 * @return
	 */
	public String getEmployeeDetailsThroughState() {
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {

				responseString = ServiceLocator.getNewAjaxHandlerService().getEmployeeDetailsThroughState(getState(),
						getGraphId(), getCountry(), getDepartmentId(), getPracticeId(), getSubPractice());
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return null;

	}

	public String getBiometricLoad() throws ServiceLocatorException {
		String responseString = "";
		try {

			/*
			 * if(elasticSearchFlag.equals("1")){ responseString =
			 * ServiceLocator.getNewAjaxHandlerService().getElasticBiometricLoad(getEmpId(),
			 * Integer.parseInt(getMonth()),Integer.parseInt(getYear()),getReportType());
			 * 
			 * }else{
			 */
			// System.out.println(getEmpId()+"======"+Integer.parseInt(getMonth())+"======"+Integer.parseInt(getYear())+"======"+getReportType());
			responseString = ServiceLocator.getNewAjaxHandlerService().getBiometricLoad(getEmpId(),
					Integer.parseInt(getMonth()), Integer.parseInt(getYear()), getReportType());
			// System.out.println("responseString==>"+responseString);
			// }

			httpServletResponse.setContentType("text");
			httpServletResponse.getWriter().write(responseString);
		} catch (ServiceLocatorException ex) {
			System.err.println(ex);
		} catch (IOException ioe) {
			System.err.println(ioe);
		}
		return null;
	}

	public String getBioAttendanceDetails() throws ServiceLocatorException {
		String responseString = "";
		try {

			/*
			 * if(elasticSearchFlag.equals("1")){ responseString =
			 * ServiceLocator.getNewAjaxHandlerService().getElasticBioAttendanceDetails(
			 * getEmpId(),getBiometricDate(),getStatus());
			 * 
			 * } else{
			 */
			responseString = ServiceLocator.getNewAjaxHandlerService().getBioAttendanceDetails(getEmpId(),
					getBiometricDate(), getStatus());
			// }

			httpServletResponse.setContentType("text");
			httpServletResponse.getWriter().write(responseString);
		} catch (IOException ioe) {
			System.err.println(ioe);
		}
		return null;
	}

	public String updateAttendanceLoad() throws ServiceLocatorException {
		try {

			String result = "";
			setCreatedBy(
					httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID).toString());
			setCreatedDate(DateUtility.getInstance().getCurrentMySqlDateTime());

			// System.out.println("name====>"+httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID).toString()+"==Date===>"+getCreatedDate());
			/*
			 * if(elasticSearchFlag.equals("1")){
			 * 
			 * result =
			 * ServiceLocator.getNewAjaxHandlerService().updateElasticAttendanceLoad(
			 * getEmpId(),this); if(result.equals("Updated")){ responseString =
			 * ServiceLocator.getNewAjaxHandlerService().updateAttendanceLoad(getEmpId(),
			 * this); } } else{
			 */
			responseString = ServiceLocator.getNewAjaxHandlerService().updateAttendanceLoad(getEmpId(), this);
			// }
			setReportTypeHidden("PayrollData");

			httpServletResponse.setContentType("text");
			httpServletResponse.getWriter().write(responseString);
		} catch (IOException ioe) {
			System.err.println(ioe);
		}
		return null;
	}

	public String calculateAttendance() throws ServiceLocatorException {
		try {

			setCreatedBy(
					httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID).toString());
			setCreatedDate(DateUtility.getInstance().getCurrentMySqlDateTime());

			responseString = ServiceLocator.getNewAjaxHandlerService().calculateAttendance(getEmpId(), this);

			httpServletResponse.setContentType("text");
			httpServletResponse.getWriter().write(responseString);
		} catch (IOException ioe) {
			System.err.println(ioe);
		}
		return null;
	}

	public String addSynAttendance() throws ServiceLocatorException {
		try {
			setCreatedBy(
					httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID).toString());
			int empId = Integer.parseInt(
					httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_EMP_ID).toString());
			int isManager = Integer.parseInt(httpServletRequest.getSession(false)
					.getAttribute(ApplicationConstants.SESSION_IS_USER_MANAGER).toString());
			int isAdmin = Integer.parseInt(httpServletRequest.getSession(false)
					.getAttribute(ApplicationConstants.SESSION_IS_ADMIN_ACCESS).toString());

			responseString = ServiceLocator.getNewAjaxHandlerService().addSynAttendance(this, empId, isManager,
					isAdmin);

			httpServletResponse.setContentType("text");
			httpServletResponse.getWriter().write(responseString);
		} catch (IOException ioe) {
			System.err.println(ioe);
		}
		return null;

	}

	public String getEmpNameByLocation() throws ServiceLocatorException {
		try {

			setLocationList(DataSourceDataProvider.getInstance().getEmployeeLocationsList("India"));

			responseString = ServiceLocator.getNewAjaxHandlerService().getEmpNameByLocation(getLocationId());

			httpServletResponse.setContentType("text/xml");
			httpServletResponse.getWriter().write(responseString);
		} catch (IOException ioe) {
			System.err.println(ioe);
		}
		return null;
	}

	public String syncAttendance() throws ServiceLocatorException {
		String responseString = "";
		try {

			// setLocationList(DataSourceDataProvider.getInstance().getEmployeeLocationsList("India"));
			int syncBy = Integer.parseInt(
					httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_EMP_ID).toString());
			responseString = ServiceLocator.getNewAjaxHandlerService().syncAttendance(getLocationId(),
					getEmployeeName(), Integer.parseInt(getMonth()), Integer.parseInt(getYear()), syncBy);

			httpServletResponse.setContentType("text");
			httpServletResponse.getWriter().write(responseString);

		} catch (IOException ioe) {
			System.err.println(ioe);
		}

		return null;
	}

	public String getEmpFullNames() {

		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			try {
				int empId = Integer.parseInt(httpServletRequest.getSession(false)
						.getAttribute(ApplicationConstants.SESSION_EMP_ID).toString());
				int isManager = Integer.parseInt(httpServletRequest.getSession(false)
						.getAttribute(ApplicationConstants.SESSION_IS_USER_MANAGER).toString());
				int isAdmin = Integer.parseInt(httpServletRequest.getSession(false)
						.getAttribute(ApplicationConstants.SESSION_IS_ADMIN_ACCESS).toString());

				String result = ServiceLocator.getNewAjaxHandlerService().getEmpFullNames(getEmpName(), empId,
						isManager, isAdmin);

				httpServletResponse.setContentType("text/xml");
				httpServletResponse.getWriter().write(result);
			} catch (ServiceLocatorException ex) {
				ex.printStackTrace();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return null;

	}

	public String getEmpNameandId() {
		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			try {
				String responseString = ServiceLocator.getNewAjaxHandlerService().getEmpNameandId(getEmpNo());

				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} catch (ServiceLocatorException ex) {
				ex.printStackTrace();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return null;
	}

	public String addHolidays() {

		try {

			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
				if (ServiceLocator.getNewAjaxHandlerService().addHolidays(this) >= 1) {
					responseString = "Added";
				} else {
					responseString = "Please try again later";
				}
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			}
		} catch (ServiceLocatorException ex) {
			System.err.println(ex);
		} catch (IOException ioe) {
			System.err.println(ioe);
		}
		return null;
	}

	public String getEmpHolidaysList() {
		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			try {
				// String fromId =
				// httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID).toString();
				String responseString = ServiceLocator.getNewAjaxHandlerService().getEmpHolidaysList(this);
				// System.out.println("in action-->"+responseString);
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} catch (ServiceLocatorException ex) {
				ex.printStackTrace();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return null;
	}

	/* old executive dashboard */

	public String getEmployeeDetailsStackChart() {
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
				responseString = ServiceLocator.getNewAjaxHandlerService().getEmployeeDetailsStackChart();
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return null;

	}

	public String getEmployeeDetailsThroughCount() {
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
				responseString = ServiceLocator.getNewAjaxHandlerService().getEmployeeDetailsThroughCount(getCountry(),
						getCategory());
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return null;
	}

	public String getRequirementDetailsStackChart() {
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
				// System.out.println("getPastMonths()----->"+getPastMonths());
				responseString = ServiceLocator.getNewAjaxHandlerService()
						.getRequirementDetailsStackChart(getPastMonths());
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return null;

	}

	public String getRequirementDetailsFromStack() {
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
				// System.out.println("getPastMonths()----->"+getPastMonths());
				responseString = ServiceLocator.getNewAjaxHandlerService()
						.getRequirementDetailsFromStack(getPastMonths(), getCountry(), getStatus());
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return null;

	}

	public String getGreenSheetCountStackChart() {
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
				responseString = ServiceLocator.getNewAjaxHandlerService().getGreenSheetCountStackChart(getPastMonths(),
						isExternal());
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return null;
	}

	public String getOpportunitiesCountsStackChart() {
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
				responseString = ServiceLocator.getNewAjaxHandlerService()
						.getOpportunitiesCountsStackChart(getPastMonths());
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return null;
	}

	/**
	 * This method is to get oppertunities count with status Created By: Triveni
	 * Niddara Created Date : 11/5/2017
	 * 
	 * @return
	 */
	public String getEmpSalesOppertunitiesPieChart() {
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {

				responseString = ServiceLocator.getNewAjaxHandlerService().getEmpSalesOppertunitiesPieChart(
						getCountry(), getDepartmentId(), getPracticeid(), getSubPractice());
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return null;
	}

	/**
	 * This method is to get oppertunities count with status Created By: Triveni
	 * Niddara Created Date : 11/5/2017
	 * 
	 * @return
	 */
	public String getSalesAccStatusPieChart() {
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {

				responseString = ServiceLocator.getNewAjaxHandlerService().getSalesAccStatusPieChart(getGraphId());
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return null;
	}

	/**
	 * This method is to get oppertunities count with status Created By: Triveni
	 * Niddara Created Date : 11/5/2017
	 * 
	 * @return
	 */
	public String getSalesAccRegionPieChart() {
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {

				responseString = ServiceLocator.getNewAjaxHandlerService().getSalesAccRegionPieChart(getGraphId());
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return null;
	}

	/**
	 * This method is to get oppertunities count with status Created By: Triveni
	 * Niddara Created Date : 11/5/2017
	 * 
	 * @return
	 */
	public String getSalesAccTerritoryPieChart() {
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {

				responseString = ServiceLocator.getNewAjaxHandlerService().getSalesAccTerritoryPieChart(getGraphId());
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return null;
	}

	/**
	 * This method is to get oppertunities count with status Created By: Triveni
	 * Niddara Created Date : 11/5/2017
	 * 
	 * @return
	 */
	public String getSalesAccStatePieChart() {
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {

				responseString = ServiceLocator.getNewAjaxHandlerService().getSalesAccStatePieChart(getGraphId());
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return null;
	}

	/**
	 * This method is to get oppertunities count with status Created By: Triveni
	 * Niddara Created Date : 11/5/2017
	 * 
	 * @return
	 */
	public String getEmpSalesRequirementPieChart() {
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {

				responseString = ServiceLocator.getNewAjaxHandlerService().getEmpSalesRequirementPieChart(getCountry(),
						getDepartmentId(), getPracticeid(), getSubPractice());
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return null;
	}

	public String getProjectsByAccountIdPmo() {
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/
			if ((httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null)
					|| (httpServletRequest.getSession(false)
							.getAttribute(ApplicationConstants.SESSION_CUST_ID) != null)) {
				String userId = httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID)
						.toString();
				String department = httpServletRequest.getSession(false)
						.getAttribute(ApplicationConstants.SESSION_MY_DEPT_ID).toString();
				// responseString =
				// ServiceLocator.getAjaxHandlerService().getProjectsForAccountId(getAccountId(),resourceType,empId);
				// System.out.println("getProjectsByAccountIdPmo
				// getAccountId().."+getAccountId());
				responseString = ServiceLocator.getNewAjaxHandlerService().getProjectsByAccountIdPmo(getAccountId(),
						userId, department);
				// System.out.println("responseString-->"+responseString);
				httpServletResponse.setContentType("text/xml");
				httpServletResponse.getWriter().write(responseString);
			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return null;
	}

	public String getEmployeesByProjectIdPmo() {
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/
			if ((httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null)
					|| (httpServletRequest.getSession(false)
							.getAttribute(ApplicationConstants.SESSION_CUST_ID) != null)) {
				String userId = httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID)
						.toString();
				// String livingCountry =
				// httpServletRequest.getSession(false).getAttribute(ApplicationConstants.Living_COUNTRY).toString();
				// responseString =
				// ServiceLocator.getAjaxHandlerService().getProjectsForAccountId(getAccountId(),resourceType,empId);
				// responseString =
				// ServiceLocator.getAjaxHandlerService().getEmployeesByProjectId(getProjectId(),
				// workingCountry);
				responseString = ServiceLocator.getNewAjaxHandlerService().getEmployeesByProjectIdPmo(getProjectId(),
						userId, getCustomerId());
				// System.out.println("responseString-->"+responseString);
				httpServletResponse.setContentType("text/xml");
				httpServletResponse.getWriter().write(responseString);
			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return null;
	}

	public String getEmployeesByCustomerIdPmo() {
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/
			if ((httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null)
					|| (httpServletRequest.getSession(false)
							.getAttribute(ApplicationConstants.SESSION_CUST_ID) != null)) {
				String userId = httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID)
						.toString();
				// responseString =
				// ServiceLocator.getAjaxHandlerService().getProjectsForAccountId(getAccountId(),resourceType,empId);
				responseString = ServiceLocator.getNewAjaxHandlerService().getEmployeesByCustomerIdPmo(getAccountId(),
						userId);
				// System.out.println("responseString-->"+responseString);
				httpServletResponse.setContentType("text/xml");
				httpServletResponse.getWriter().write(responseString);
			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return null;
	}

	public String getTimeSheetStatusReportDetailsList() {
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
				// responseString =
				// ServiceLocator.getNewAjaxHandlerService().avaiableEmployeBySubPracticeList();
				String userId = httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID)
						.toString();
				responseString = ServiceLocator.getNewAjaxHandlerService().getTimeSheetStatusReportDetailsList(this,
						userId);
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return null;

	}

	public String getEmpProjectExcelReport() {
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
				responseString = ServiceLocator.getNewAjaxHandlerService().getEmpProjectExcelReport(this);
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return null;

	}

	public String requirementAjaxListVP() {
		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			try {

				responseString = ServiceLocator.getNewAjaxHandlerService().getRequirementAjaxListVP(httpServletRequest);
				httpServletResponse.setContentType("text/xml");
				httpServletResponse.getWriter().write(responseString);
			} catch (ServiceLocatorException ex) {
				ex.printStackTrace();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return null;
	}

	public String searchRequirementAjaxListVP() {
		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			// AjaxHandlerAction ajaxHandlerAction = new AjaxHandlerAction();
			try {

				responseString = ServiceLocator.getNewAjaxHandlerService()
						.getSearchRequirementAjaxListVP(httpServletRequest, this);
				httpServletResponse.setContentType("text/xml");
				httpServletResponse.getWriter().write(responseString);
			} catch (ServiceLocatorException ex) {
				ex.printStackTrace();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return null;
	}

	public String getStarResourceComments() {
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
				responseString = ServiceLocator.getNewAjaxHandlerService().getStarResourceComments(getId());
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return null;

	}

	public String getEmployeeCertificationDetails() {
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
				responseString = ServiceLocator.getNewAjaxHandlerService().getEmployeeCertificationDetails(getLoginId(),
						getStartDate(), getEndDate());
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return null;

	}

	public String getReviewDescription() {
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
				responseString = ServiceLocator.getNewAjaxHandlerService().getReviewDescription(getId());
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return null;

	}

	public String doGetPracticeStatusReport() {
		try {

			/*
			 * This if loop is to check whether there is Session or not
			 **/
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
				responseString = ServiceLocator.getNewAjaxHandlerService().getPracticeStatusReport(getPracticeId(),
						getShadowFlag(), getDepartment(), getOrderBy());
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return null;

	}

	public String getShadowsUtilizationReport() {
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
				responseString = ServiceLocator.getNewAjaxHandlerService().getShadowsUtilizationReport(getCustomerId());
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return null;

	}

	public String getShadowsUtilizationReportByProject() {
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
				responseString = ServiceLocator.getNewAjaxHandlerService().getShadowsUtilizationReportByProject(
						getCustomerId(), getProjectId(), getStatus(), getProjectType(), getCostModel());
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return null;

	}

	public String getResourceTotal() {
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
				responseString = ServiceLocator.getNewAjaxHandlerService().getResourceTotal(getPracticeId(),
						getDepartment(), getState());
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return null;

	}

	public String getReviewListBasedOnActivityType() {
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
				// responseString =
				// ServiceLocator.getAjaxHandlerService().getProjectNamesList(getAccId());
				responseString = ServiceLocator.getNewAjaxHandlerService()
						.getReviewListBasedOnActivityType(getActivityType());

				httpServletResponse.setContentType("text/xml");
				httpServletResponse.getWriter().write(responseString);
			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return null;
	}

	public String reviewSalesStatusReport() throws ServiceLocatorException {
		/*
		 * This if loop is to check whether there is Session or not
		 **/
		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {

			DataSourceDataProvider dataSourceDataProvider = null;

			// if
			// (AuthorizationManager.getInstance().isAuthorizedUser("DASHBOARD_ACCESS",
			// userRoleId)) {
			try {
				responseString = ServiceLocator.getNewAjaxHandlerService().getReviewSalesStatusReport(this,
						httpServletRequest);
				// System.out.println("+++++++++++++++++"+responseString);

				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} catch (Exception ex) {
			}
			// }
		} // Close Session Checking
		return null;
	}

	public String activitySummeryByAccount() throws ServiceLocatorException {
		/*
		 * This if loop is to check whether there is Session or not
		 **/
		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {

			DataSourceDataProvider dataSourceDataProvider = null;

			if (AuthorizationManager.getInstance().isAuthorizedUser("DASHBOARD_ACCESS", userRoleId)) {
				try {
					responseString = ServiceLocator.getNewAjaxHandlerService().getActivitySummeryByAccount(this,
							httpServletRequest);
					// System.out.println("+++++++++++++++++"+responseString);

					httpServletResponse.setContentType("text");
					httpServletResponse.getWriter().write(responseString);
				} catch (Exception ex) {
				}
			}
		} // Close Session Checking
		return null;
	}

	public String accountSummaryOfBdmReport() throws ServiceLocatorException {
		/*
		 * This if loop is to check whether there is Session or not
		 **/
		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {

			DataSourceDataProvider dataSourceDataProvider = null;

			if (AuthorizationManager.getInstance().isAuthorizedUser("DASHBOARD_ACCESS", userRoleId)) {
				try {
					// System.out.println("+++++++++++++++++");
					responseString = ServiceLocator.getNewAjaxHandlerService().accountSummaryOfBdmReport(this,
							httpServletRequest);
					// System.out.println("+++++++++++++++++"+responseString);

					httpServletResponse.setContentType("text");
					httpServletResponse.getWriter().write(responseString);
				} catch (Exception ex) {
				}
			}
		} // Close Session Checking
		return null;
	}

	public String requirementSummaryByBDM() throws ServiceLocatorException {
		/*
		 * This if loop is to check whether there is Session or not
		 **/
		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {

			DataSourceDataProvider dataSourceDataProvider = null;

			if (AuthorizationManager.getInstance().isAuthorizedUser("DASHBOARD_ACCESS", userRoleId)) {
				try {
					// System.out.println("+++++++++++++++++");
					responseString = ServiceLocator.getNewAjaxHandlerService().requirementSummaryOfBdmReport(this,
							httpServletRequest);
					// System.out.println("+++++++++++++++++"+responseString);

					httpServletResponse.setContentType("text");
					httpServletResponse.getWriter().write(responseString);
				} catch (Exception ex) {
				}
			}
		} // Close Session Checking
		return null;
	}

	public String deleteEmpVerification() {
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
				responseString = ServiceLocator.getAjaxHandlerWebService().deleteEmpVerification(getId());
				httpServletResponse.setContentType("text/plain");
				httpServletResponse.getWriter().write(responseString);
			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

	}

	public String getOffshoreStrengthDetails() throws ServiceLocatorException {
		/*
		 * This if loop is to check whether there is Session or not
		 **/
		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {

			DataSourceDataProvider dataSourceDataProvider = null;

			// if
			// (AuthorizationManager.getInstance().isAuthorizedUser("DASHBOARD_ACCESS",
			// userRoleId)) {
			try {
				responseString = ServiceLocator.getNewAjaxHandlerService().getOffshoreStrengthDetails(this,
						httpServletRequest);
				// System.out.println("+++++++++++++++++"+responseString);

				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} catch (Exception ex) {
			}
			// }
		} // Close Session Checking
		return null;
	}

	public String getCustomerProjectRollInRollOffDetails() throws ServiceLocatorException {
		/*
		 * This if loop is to check whether there is Session or not
		 **/
		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {

			DataSourceDataProvider dataSourceDataProvider = null;

			// if
			// (AuthorizationManager.getInstance().isAuthorizedUser("DASHBOARD_ACCESS",
			// userRoleId)) {
			try {
				responseString = ServiceLocator.getNewAjaxHandlerService().getCustomerProjectRollInRollOffDetails(this,
						httpServletRequest);
				// System.out.println("+++++++++++++++++"+responseString);

				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} catch (Exception ex) {
			}
			// }
		} // Close Session Checking
		return null;
	}

	public String activitySummeryDetailsByAccount() throws ServiceLocatorException {
		/*
		 * This if loop is to check whether there is Session or not
		 **/
		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {

			DataSourceDataProvider dataSourceDataProvider = null;

			if (AuthorizationManager.getInstance().isAuthorizedUser("DASHBOARD_ACCESS", userRoleId)) {
				try {
					responseString = ServiceLocator.getNewAjaxHandlerService().activitySummeryDetailsByAccount(this,
							httpServletRequest);
					// System.out.println("+++++++++++++++++"+responseString);

					httpServletResponse.setContentType("text");
					httpServletResponse.getWriter().write(responseString);
				} catch (Exception ex) {
				}
			}
		} // Close Session Checking
		return null;
	}

	public String offshoreDeliveryStrength() throws ServiceLocatorException {
		/*
		 * This if loop is to check whether there is Session or not
		 **/
		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {

			DataSourceDataProvider dataSourceDataProvider = null;

			// if
			// (AuthorizationManager.getInstance().isAuthorizedUser("DASHBOARD_ACCESS",
			// userRoleId)) {
			try {
				responseString = ServiceLocator.getNewAjaxHandlerService().offshoreDeliveryStrength(this,
						httpServletRequest);
				// System.out.println("+++++++++++++++++"+responseString);

				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} catch (Exception ex) {
			}
			// }
		} // Close Session Checking
		return null;
	}

	public String getEmployeeDetailsByLoginId() {
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
				responseString = ServiceLocator.getNewAjaxHandlerService().getEmployeeDetailsByLoginId(getLoginId());
				httpServletResponse.setContentType("text/html");
				httpServletResponse.getWriter().write(responseString);
			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return null;

	}

	public String doUpdateEventDetailsForConference() {
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
				setCreatedBy(httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID)
						.toString());
				responseString = ServiceLocator.getAjaxHandlerWebService().doUpdateEventDetailsForConference(this);
				httpServletResponse.setContentType("text/html");
				httpServletResponse.getWriter().write(responseString);
			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

	}

	public String getLogisticDetailsByConfIdAndNomineeId() {
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
				setCreatedBy(httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID)
						.toString());
				responseString = ServiceLocator.getAjaxHandlerWebService()
						.getLogisticDetailsByConfIdAndNomineeId(getConferenceId(), getLoginId());
				httpServletResponse.setContentType("text/html");
				httpServletResponse.getWriter().write(responseString);
			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

	}

	public String doAddLogisticDetailsForConference() {
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/
			// System.out.println("doAddLogisticDetailsForConference in ctio");
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
				attachmentService = ServiceLocator.getAttachmentService();
				int empId = Integer.parseInt(httpServletRequest.getSession(false)
						.getAttribute(ApplicationConstants.SESSION_EMP_ID).toString());
				setCreatedBy(httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID)
						.toString());
				int year = Calendar.getInstance().get(Calendar.YEAR);
				String path = com.mss.mirage.util.Properties.getProperty("Attachments.Path") + "/" + "Logistics/" + year
						+ "/" + empId;
				java.util.Date date = new java.util.Date();
				String monthName = null;

				if (date.getMonth() == 0)
					monthName = "January";
				else if (date.getMonth() == 1)
					monthName = "February";
				else if (date.getMonth() == 2)
					monthName = "March";
				else if (date.getMonth() == 3)
					monthName = "April";
				else if (date.getMonth() == 4)
					monthName = "May";
				else if (date.getMonth() == 5)
					monthName = "June";
				else if (date.getMonth() == 6)
					monthName = "July";
				else if (date.getMonth() == 7)
					monthName = "August";
				else if (date.getMonth() == 8)
					monthName = "September";
				else if (date.getMonth() == 9)
					monthName = "October";
				else if (date.getMonth() == 10)
					monthName = "November";
				else if (date.getMonth() == 11)
					monthName = "December";
				String datetime = new SimpleDateFormat("ddMMMyyyy_hh_mm_ssa").format(new java.util.Date());
				// setFileFileName(empId+"_"+datetime+"_"+ getFileFileName());
				generatedPath = path + "/" + monthName;
				if (getFileHotelFileName() != null) {

					File targetDirectoryForHotel = new File(
							generatedPath + "/" + empId + "_" + datetime + "_" + getFileHotelFileName());
					// System.out.println("targetDirectoryForHotel==="+targetDirectoryForHotel);
					setHotelAttachmentLocation(targetDirectoryForHotel.toString());
					FileUtils.copyFile(getFileHotel(), targetDirectoryForHotel);
					// setObjectType("Emp Reviews");
				} else {
					objectType = "NoFile";
					setHotelAttachmentLocation("");
					;
					// setFilepath("");
					// attachmentName = "";
				}
				if (getFileTransportFileName() != null) {

					File targetDirectoryForTransport = new File(
							generatedPath + "/" + empId + "_" + datetime + "_" + getFileTransportFileName());
					setTransportAttachmentLocation(targetDirectoryForTransport.toString());
					FileUtils.copyFile(getFileTransport(), targetDirectoryForTransport);
					// setObjectType("Emp Reviews");
				} else {
					objectType = "NoFile";
					setTransportAttachmentLocation("");
					// setFilepath("");
					// attachmentName = "";
				}
				if (getFileFlightFileName() != null) {

					File targetDirectoryForFlight = new File(
							generatedPath + "/" + empId + "_" + datetime + "_" + getFileFlightFileName());
					setFlightAttachmentLocation(targetDirectoryForFlight.toString());
					FileUtils.copyFile(getFileFlight(), targetDirectoryForFlight);
					// setObjectType("Emp Reviews");
				} else {
					objectType = "NoFile";
					setFlightAttachmentLocation(datetime);
					// setFilepath("");
					// attachmentName = "";
				}
				responseString = ServiceLocator.getAjaxHandlerWebService().doAddLogisticDetailsForConference(this);
				// System.out.println("responseString======" + responseString);
				httpServletResponse.setContentType("text/html");
				httpServletResponse.getWriter().write(responseString);

			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

	}

	public String doUpdateLogisticDetailsForConference() {
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
				attachmentService = ServiceLocator.getAttachmentService();
				int empId = Integer.parseInt(httpServletRequest.getSession(false)
						.getAttribute(ApplicationConstants.SESSION_EMP_ID).toString());
				setCreatedBy(httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID)
						.toString());
				int year = Calendar.getInstance().get(Calendar.YEAR);
				String path = com.mss.mirage.util.Properties.getProperty("Attachments.Path") + "/" + "Logistics/" + year
						+ "/" + empId;
				java.util.Date date = new java.util.Date();
				String monthName = null;

				if (date.getMonth() == 0)
					monthName = "January";
				else if (date.getMonth() == 1)
					monthName = "February";
				else if (date.getMonth() == 2)
					monthName = "March";
				else if (date.getMonth() == 3)
					monthName = "April";
				else if (date.getMonth() == 4)
					monthName = "May";
				else if (date.getMonth() == 5)
					monthName = "June";
				else if (date.getMonth() == 6)
					monthName = "July";
				else if (date.getMonth() == 7)
					monthName = "August";
				else if (date.getMonth() == 8)
					monthName = "September";
				else if (date.getMonth() == 9)
					monthName = "October";
				else if (date.getMonth() == 10)
					monthName = "November";
				else if (date.getMonth() == 11)
					monthName = "December";
				String datetime = new SimpleDateFormat("ddMMMyyyy_hh_mm_ssa").format(new java.util.Date());
				// setFileFileName(empId+"_"+datetime+"_"+ getFileFileName());
				generatedPath = path + "/" + monthName;
				if (getFileHotelFileName() != null) {

					File targetDirectoryForHotel = new File(
							generatedPath + "/" + empId + "_" + datetime + "_" + getFileHotelFileName());
					setHotelAttachmentLocation(targetDirectoryForHotel.toString());
					FileUtils.copyFile(getFileHotel(), targetDirectoryForHotel);
					// setObjectType("Emp Reviews");
				} else {
					objectType = "NoFile";
					setHotelAttachmentLocation("");
					;
					// setFilepath("");
					// attachmentName = "";
				}
				if (getFileTransportFileName() != null) {

					File targetDirectoryForTransport = new File(
							generatedPath + "/" + empId + "_" + datetime + "_" + getFileTransportFileName());
					setTransportAttachmentLocation(targetDirectoryForTransport.toString());
					FileUtils.copyFile(getFileTransport(), targetDirectoryForTransport);
					// setObjectType("Emp Reviews");
				} else {
					objectType = "NoFile";
					setTransportAttachmentLocation("");
					// setFilepath("");
					// attachmentName = "";
				}
				if (getFileFlightFileName() != null) {

					File targetDirectoryForFlight = new File(
							generatedPath + "/" + empId + "_" + datetime + "_" + getFileFlightFileName());
					setFlightAttachmentLocation(targetDirectoryForFlight.toString());
					FileUtils.copyFile(getFileFlight(), targetDirectoryForFlight);
					// setObjectType("Emp Reviews");
				} else {
					objectType = "NoFile";
					setFlightAttachmentLocation("");
					// setFilepath("");
					// attachmentName = "";
				}

				responseString = ServiceLocator.getAjaxHandlerWebService().doUpdateLogisticDetailsForConference(this);
				httpServletResponse.setContentType("text/html");
				httpServletResponse.getWriter().write(responseString);
			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

	}

	public String doAddNomineeDetailsForConference() {
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
				setCreatedBy(httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID)
						.toString());
				responseString = ServiceLocator.getAjaxHandlerWebService().doAddNomineeDetailsForConference(this);
				httpServletResponse.setContentType("text/html");
				httpServletResponse.getWriter().write(responseString);
			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

	}

	public String getEmployeeNamesForMcon() {
		try {
			responseString = ServiceLocator.getNewAjaxHandlerService().doGetEmployeeNamesForMcon(
					"SELECT CONCAT(TRIM(FName),'.',TRIM(LName)) AS FullName,LoginId FROM tblEmployee WHERE (LName LIKE '"
							+ employeeName + "%' OR FName LIKE '" + employeeName + "%' OR EmpNo LIKE '" + employeeName
							+ "%') AND CurStatus='Active'");
			httpServletResponse.setContentType("text/xml");
			httpServletResponse.getWriter().write(responseString);
		} catch (ServiceLocatorException ex) {
			System.err.println(ex);
		} catch (IOException ioe) {
			System.err.println(ioe);
		}
		return null;
	}

	public String getLinkedInProfile() {
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/
			System.out.println("getLinkedInProfile");
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
				// responseString =
				// ServiceLocator.getAjaxHandlerService().getProjectNamesList(getAccId());
				responseString = ServiceLocator.getNewAjaxHandlerService().getLinkedInProfile(getId());
				System.out.println("responseString" + responseString);
				httpServletResponse.setContentType("text/xml");
				httpServletResponse.getWriter().write(responseString);
			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return null;
	}

	public String getActivityComments() {
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/
			System.out.println("getLinkedInProfile");
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
				// responseString =
				// ServiceLocator.getAjaxHandlerService().getProjectNamesList(getAccId());
				responseString = ServiceLocator.getNewAjaxHandlerService().getActivityComments(getId(), getFlag());
				System.out.println("responseString" + responseString);
				httpServletResponse.setContentType("text/xml");
				httpServletResponse.getWriter().write(responseString);
			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return null;
	}

	/* Ecertification changes Start */

	public String setFlagStatus() {
		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {

			try {

				// System.out.println("in action price Details");
				String key = httpServletRequest.getSession(false).getAttribute(ApplicationConstants.ECERT_VALIDATE_KEY)
						.toString();

				responseString = ServiceLocator.getNewAjaxHandlerService().setFlagStatus(this, key, httpServletRequest);

				// System.out.println("responseString "+responseString);
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);

			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return null;
	}

	public String getRemainingTime() {
		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			// AjaxHandlerAction ajaxHandlerAction = new AjaxHandlerAction();

			try {

				// System.out.println("in action price Details");
				String key = httpServletRequest.getSession(false).getAttribute(ApplicationConstants.ECERT_VALIDATE_KEY)
						.toString();

				// String
				// userId=httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID).toString();
				// responseString =
				// ServiceLocator.getNewAjaxHandlerService().doGetPriceDetails(getLoginId(),getYearExp(),getMonth());
				responseString = ServiceLocator.getNewAjaxHandlerService().getRemainingTime(this, key);

				// System.out.println("responseString "+responseString);
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);

			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return null;
	}

	public String getEcertCount() {
		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			// AjaxHandlerAction ajaxHandlerAction = new AjaxHandlerAction();

			try {

				// System.out.println("in action price Details");
				// String
				// key=httpServletRequest.getSession(false).getAttribute(ApplicationConstants.ECERT_VALIDATE_KEY).toString();
				String userId = httpServletRequest.getSession(false)
						.getAttribute(ApplicationConstants.ECERT_VALIDATE_KEY).toString();

				// String
				// userId=httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID).toString();
				// responseString =
				// ServiceLocator.getNewAjaxHandlerService().doGetPriceDetails(getLoginId(),getYearExp(),getMonth());
				responseString = ServiceLocator.getNewAjaxHandlerService().getEcertCount(this, userId);

				// System.out.println("responseString "+responseString);
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);

			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return null;
	}

	public String getCreRemainingTime() {
		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			// AjaxHandlerAction ajaxHandlerAction = new AjaxHandlerAction();

			try {

				// System.out.println("in action price Details");
				String userId = httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID)
						.toString();

				// String
				// userId=httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID).toString();
				// responseString =
				// ServiceLocator.getNewAjaxHandlerService().doGetPriceDetails(getLoginId(),getYearExp(),getMonth());
				responseString = ServiceLocator.getNewAjaxHandlerService().getCreRemainingTime(this, userId);

				// System.out.println("responseString "+responseString);
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);

			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return null;
	}

	public String setCreFlagStatus() {
		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			// AjaxHandlerAction ajaxHandlerAction = new AjaxHandlerAction();

			try {

				// System.out.println("in action price Details");

				String userId = httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID)
						.toString();

				// String
				// userId=httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID).toString();
				// responseString =
				// ServiceLocator.getNewAjaxHandlerService().doGetPriceDetails(getLoginId(),getYearExp(),getMonth());
				responseString = ServiceLocator.getNewAjaxHandlerService().setCreFlagStatus(this, userId,
						httpServletRequest);

				// System.out.println("responseString "+responseString);
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);

			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return null;
	}

	public String getCreCount() {
		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			// AjaxHandlerAction ajaxHandlerAction = new AjaxHandlerAction();

			try {

				// System.out.println("in action price Details");
				// String
				// key=httpServletRequest.getSession(false).getAttribute(ApplicationConstants.ECERT_VALIDATE_KEY).toString();

				String userId = httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID)
						.toString();
				// responseString =
				// ServiceLocator.getNewAjaxHandlerService().doGetPriceDetails(getLoginId(),getYearExp(),getMonth());
				responseString = ServiceLocator.getNewAjaxHandlerService().getCreCount(this, userId);

				// System.out.println("responseString "+responseString);
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);

			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return null;
	}

	/* E certification changes end */
	public String doGetLoadForPMOReview() {
		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			// AjaxHandlerAction ajaxHandlerAction = new AjaxHandlerAction();

			try {

				// System.out.println("values :::::::");
				String userId = httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID)
						.toString();
				int isAdmin = Integer.parseInt(httpServletRequest.getSession(false)
						.getAttribute(ApplicationConstants.SESSION_IS_ADMIN_ACCESS).toString());
				// System.out.println(isAdmin+"userId "+userId);
				// responseString =
				// ServiceLocator.getNewAjaxHandlerService().doGetLoadForAudit(getJsonData());
				responseString = ServiceLocator.getNewAjaxHandlerService().doGetLoadForPMOReview(getJsonData(), userId,
						isAdmin);
				// System.out.println("responseString "+responseString);
				httpServletResponse.setContentType("text/plain");
				httpServletResponse.getWriter().write(responseString);

			} catch (ServiceLocatorException ex) {
				ex.printStackTrace();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return null;
	}

	public String saveWeeklyPMOReviews() {
		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			// AjaxHandlerAction ajaxHandlerAction = new AjaxHandlerAction();

			try {
				String userId = httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID)
						.toString();
				// System.out.println("values :::::::");

				// responseString =
				// ServiceLocator.getNewAjaxHandlerService().doGetLoadForAudit(getJsonData());
				responseString = ServiceLocator.getNewAjaxHandlerService().saveWeeklyPMOReviews(getJsonData(), userId);
				// System.out.println("responseString "+responseString);
				httpServletResponse.setContentType("text/plain");
				httpServletResponse.getWriter().write(responseString);

			} catch (ServiceLocatorException ex) {
				ex.printStackTrace();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return null;
	}

	public String updateWeeklyPMOReviews() {
		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			// AjaxHandlerAction ajaxHandlerAction = new AjaxHandlerAction();

			try {
				String userId = httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID)
						.toString();
				// System.out.println("values :::::::");

				// responseString =
				// ServiceLocator.getNewAjaxHandlerService().doGetLoadForAudit(getJsonData());
				responseString = ServiceLocator.getNewAjaxHandlerService().updateWeeklyPMOReviews(getJsonData(),
						userId);
				// System.out.println("responseString "+responseString);
				httpServletResponse.setContentType("text/plain");
				httpServletResponse.getWriter().write(responseString);

			} catch (ServiceLocatorException ex) {
				ex.printStackTrace();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return null;
	}

	public String getPMOWeeklyReportSearch() {
		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			// AjaxHandlerAction ajaxHandlerAction = new AjaxHandlerAction();

			try {
				// String userId =
				// httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID).toString();
				// System.out.println("values :::::::");

				// responseString =
				// ServiceLocator.getNewAjaxHandlerService().doGetLoadForAudit(getJsonData());

				String userId = httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID)
						.toString();
				int isAdmin = Integer.parseInt(httpServletRequest.getSession(false)
						.getAttribute(ApplicationConstants.SESSION_IS_ADMIN_ACCESS).toString());
				responseString = ServiceLocator.getNewAjaxHandlerService().getPMOWeeklyReportSearch(getJsonData(),
						userId, isAdmin);
				// System.out.println("responseString "+responseString);
				httpServletResponse.setContentType("text/plain");
				httpServletResponse.getWriter().write(responseString);

			} catch (ServiceLocatorException ex) {
				ex.printStackTrace();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return null;
	}

	public String updateWeeklyReviewComment() {
		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			try {

				setCreatedBy(httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID)
						.toString());
				responseString = ServiceLocator.getNewAjaxHandlerService().updateWeeklyReviewComment(getJsonData(),
						getCreatedBy());
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);

			} catch (ServiceLocatorException ex) {
				ex.printStackTrace();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return null;
	}

	public String getWeeksForGivenMonth() {
		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			try {

				setCreatedBy(httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID)
						.toString());
				responseString = ServiceLocator.getNewAjaxHandlerService().getWeeksForGivenMonth(getJsonData());
				httpServletResponse.setContentType("text/plain");
				httpServletResponse.getWriter().write(responseString);

			} catch (ServiceLocatorException ex) {
				ex.printStackTrace();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return null;
	}

	/* sales performance review report start */

	public String reviewSalesStatusReview() throws ServiceLocatorException {
		/*
		 * This if loop is to check whether there is Session or not
		 **/
		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {

			DataSourceDataProvider dataSourceDataProvider = null;
			System.out.println("in action class");
			// if
			// (AuthorizationManager.getInstance().isAuthorizedUser("DASHBOARD_ACCESS",
			// userRoleId)) {
			try {
				responseString = ServiceLocator.getNewAjaxHandlerService().getReviewSalesStatusReview(this,
						httpServletRequest);
				// System.out.println("+++++++++++++++++"+responseString);

				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} catch (Exception ex) {
			}
			// }
		} // Close Session Checking
		return null;
	}

	/* sales performance review report end */

	public String getEmpDetailsForBioMetric() {
		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			try {

				responseString = ServiceLocator.getNewAjaxHandlerService().getEmpDetailsForBioMetric(getJsonData(),
						httpServletRequest);
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);

			} catch (ServiceLocatorException ex) {
				ex.printStackTrace();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return null;
	}

	public String getLunchBoxData() {

		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			try {

				responseString = ServiceLocator.getNewAjaxHandlerService().getLunchBoxData(getJsonData());
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);

			} catch (ServiceLocatorException ex) {
				ex.printStackTrace();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return null;
	}

	public String updateLunchBoxStatus() {

		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			try {

				setCreatedBy(httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID)
						.toString());

				JSONObject jo = new JSONObject(getJsonData());
				jo.put("modifiedBy", getCreatedBy());
				// System.out.println("json-->"+jo.toString());
				responseString = ServiceLocator.getNewAjaxHandlerService().updateLunchBoxStatus(jo.toString());
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);

			} catch (ServiceLocatorException ex) {
				ex.printStackTrace();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return null;
	}

	/*
	 * Author:Sarada Tatisetti Date:07/01/2019 Des:Roll in Roll Off Overall analysis
	 * Changes start
	 */

	public String getRollInRollOffAnalysisDetails() throws ServiceLocatorException {
		/*
		 * This if loop is to check whether there is Session or not
		 **/

		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {

			DataSourceDataProvider dataSourceDataProvider = null;

			// if
			// (AuthorizationManager.getInstance().isAuthorizedUser("DASHBOARD_ACCESS",
			// userRoleId)) {
			try {
				responseString = ServiceLocator.getNewAjaxHandlerService().getRollInRollOffAnalysisDetails(this,
						httpServletRequest);
				// System.out.println("+++++++++++++++++"+responseString);

				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} catch (Exception ex) {
			}
			// }
		} // Close Session Checking
		return null;
	}

	public String getRollInRollOffAnalysisDetailsPractice() throws ServiceLocatorException {
		/*
		 * This if loop is to check whether there is Session or not
		 **/

		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {

			DataSourceDataProvider dataSourceDataProvider = null;

			// if
			// (AuthorizationManager.getInstance().isAuthorizedUser("DASHBOARD_ACCESS",
			// userRoleId)) {
			try {
				responseString = ServiceLocator.getNewAjaxHandlerService().getRollInRollOffAnalysisDetailsPractice(this,
						httpServletRequest);
				// System.out.println("+++++++++++++++++"+responseString);

				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} catch (Exception ex) {
			}
			// }
		} // Close Session Checking
		return null;
	}

	// Roll in Roll Off Overall analysis Changes end

	/*
	 * 
	 * Author:Sarada Tatisetti Created date:2nd Aug 2019
	 */

	public String getProjectResourcesRollInRollOffReportDetails() throws ServiceLocatorException {
		/*
		 * This if loop is to check whether there is Session or not
		 **/
		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {

			DataSourceDataProvider dataSourceDataProvider = null;

			// if
			// (AuthorizationManager.getInstance().isAuthorizedUser("DASHBOARD_ACCESS",
			// userRoleId)) {
			try {
				responseString = ServiceLocator.getNewAjaxHandlerService()
						.getProjectResourcesRollInRollOffReportDetails(this, httpServletRequest);
				// System.out.println("+++++++++++++++++"+responseString);

				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);
			} catch (Exception ex) {
			}
			// }
		} // Close Session Checking
		return null;
	}

	/*
	 * sarada tatisetti
	 * 
	 * 9/17/2019
	 * 
	 */

	public String getConferenceCallsAndF2FVisits() throws ServiceLocatorException {
		/*
		 * This if loop is to check whether there is Session or not
		 **/
		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {

			DataSourceDataProvider dataSourceDataProvider = null;

			if (AuthorizationManager.getInstance().isAuthorizedUser("DASHBOARD_ACCESS", userRoleId)) {
				try {
					responseString = ServiceLocator.getNewAjaxHandlerService().getConferenceCallsAndF2FVisits(this,
							httpServletRequest);
					// System.out.println("+++++++++++++++++"+responseString);

					httpServletResponse.setContentType("text");
					httpServletResponse.getWriter().write(responseString);
				} catch (Exception ex) {
				}
			}
		} // Close Session Checking
		return null;
	}

	public String getTeamMemberByTitleType() {
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
				responseString = ServiceLocator.getNewAjaxHandlerService().getTeamMemberByTitleType(getTitleType());
				httpServletResponse.setContentType("text/xml");
				httpServletResponse.getWriter().write(responseString);
				// System.out.println("responseString"+responseString);

			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return null;
	}

	public String popupConferenceCallsWindow() {
		String str;
		/*
		 * This if loop is to check whether there is Session or not
		 **/
		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			try {

				str = ServiceLocator.getNewAjaxHandlerService().popupConferenceCallsWindow(this, httpServletRequest);
				httpServletResponse.setContentType("text/html");
				if (str != null) {
					httpServletResponse.getWriter().write((str));
				} else {
					httpServletResponse.getWriter().write(("No Record Present"));
				}

			} catch (ServiceLocatorException ex) {
				ex.printStackTrace();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		} // Close Session Checking
		return null;
	}

	public String popupF2FvisitsWindow() {
		String str;
		/*
		 * This if loop is to check whether there is Session or not
		 **/
		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			try {

				str = ServiceLocator.getNewAjaxHandlerService().popupF2FvisitsWindow(this, httpServletRequest);
				httpServletResponse.setContentType("text/html");
				if (str != null) {
					httpServletResponse.getWriter().write((str));
				} else {
					httpServletResponse.getWriter().write(("No Record Present"));
				}

			} catch (ServiceLocatorException ex) {
				ex.printStackTrace();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		} // Close Session Checking
		return null;
	}

	
	public String getSalesTeamMemberByCountry() {
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
				responseString = ServiceLocator.getNewAjaxHandlerService().getSalesTeamMemberByCountry(getCountry());
				httpServletResponse.setContentType("text/xml");
				httpServletResponse.getWriter().write(responseString);
				// System.out.println("responseString"+responseString);

			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return null;
	}
	
	/*NagaLakshmi Telluri
	 * 10/14/2019
	 * 
	 */
	
	
	
	
	public String getImmigrationDetails() {
		try {
			/*
			 * This if loop is to check whether there is Session or not
			 **/
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
				responseString = ServiceLocator.getNewAjaxHandlerService().getImmigrationDetails(getImgStatus());
				httpServletResponse.setContentType("text/xml");
				httpServletResponse.getWriter().write(responseString);
				// System.out.println("responseString"+responseString);

			} // Close Session Checking
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return null;
	}
	
	
	
	public String popuppassportDetailsWindow() {
		String str;
		/*
		 * This if loop is to check whether there is Session or not
		 **/
		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			try {

				str = ServiceLocator.getNewAjaxHandlerService().popuppassportDetailsWindow(this, httpServletRequest);
				httpServletResponse.setContentType("text/html");
				if (str != null) {
					httpServletResponse.getWriter().write((str));
				} else {
					httpServletResponse.getWriter().write(("No Record Present"));
				}

			} catch (ServiceLocatorException ex) {
				ex.printStackTrace();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		} // Close Session Checking
		return null;
	}
	
	
	/*NagaLakshmi Telluri
	*Adding MSAdates to Customer
	 * 10/24/2019
	 */

	
	public String addMSADatesToCustomer() {

		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			try {

				setModifiedBy(httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID)
						.toString());

			
				responseString = ServiceLocator.getNewAjaxHandlerService().addMSADatesToCustomer(this, httpServletRequest);
				httpServletResponse.setContentType("text");
				httpServletResponse.getWriter().write(responseString);

			} catch (ServiceLocatorException ex) {
				ex.printStackTrace();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return null;
	}	
	
	
	
	public String searchMSACustomerProjectsAjaxList() {
		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
		//AjaxHandlerAction ajaxHandlerAction = new AjaxHandlerAction();
		try {
		responseString = ServiceLocator.getNewAjaxHandlerService().searchMSACustomerProjectsAjaxList(this,httpServletRequest);

		httpServletResponse.setContentType("text/xml");
		httpServletResponse.getWriter().write(responseString);
		} catch (ServiceLocatorException ex) {
		ex.printStackTrace();
		} catch (IOException ex) {
		ex.printStackTrace();
		}
		}
		return null;
		} 
	
	@Override
	public void setServletRequest(HttpServletRequest httpServletRequest) {
		this.httpServletRequest = httpServletRequest;
	}

	@Override
	public void setServletResponse(HttpServletResponse httpServletResponse) {
		this.httpServletResponse = httpServletResponse;
	}

	/**
	 * @return the mcertStartDate
	 */
	public String getMcertStartDate() {
		return mcertStartDate;
	}

	/**
	 * @param mcertStartDate
	 *            the mcertStartDate to set
	 */
	public void setMcertStartDate(String mcertStartDate) {
		this.mcertStartDate = mcertStartDate;
	}

	/**
	 * @return the mcertToDate
	 */
	public String getMcertToDate() {
		return mcertToDate;
	}

	/**
	 * @param mcertToDate
	 *            the mcertToDate to set
	 */
	public void setMcertToDate(String mcertToDate) {
		this.mcertToDate = mcertToDate;
	}

	/**
	 * @return the mcertConsultantId
	 */
	public String getMcertConsultantId() {
		return mcertConsultantId;
	}

	/**
	 * @param mcertConsultantId
	 *            the mcertConsultantId to set
	 */
	public void setMcertConsultantId(String mcertConsultantId) {
		this.mcertConsultantId = mcertConsultantId;
	}

	/**
	 * @return the mcertConsultantStatus
	 */
	public String getMcertConsultantStatus() {
		return mcertConsultantStatus;
	}

	/**
	 * @param mcertConsultantStatus
	 *            the mcertConsultantStatus to set
	 */
	public void setMcertConsultantStatus(String mcertConsultantStatus) {
		this.mcertConsultantStatus = mcertConsultantStatus;
	}

	/**
	 * @return the ExamNameIdList
	 */
	public String getExamNameIdList() {
		return ExamNameIdList;
	}

	/**
	 * @param ExamNameIdList
	 *            the ExamNameIdList to set
	 */
	public void setExamNameIdList(String ExamNameIdList) {
		this.ExamNameIdList = ExamNameIdList;
	}

	/**
	 * @return the questionNo
	 */
	public int getQuestionNo() {
		return questionNo;
	}

	/**
	 * @param questionNo
	 *            the questionNo to set
	 */
	public void setQuestionNo(int questionNo) {
		this.questionNo = questionNo;
	}

	/**
	 * @return the selectedAns
	 */
	public int getSelectedAns() {
		return selectedAns;
	}

	/**
	 * @param selectedAns
	 *            the selectedAns to set
	 */
	public void setSelectedAns(int selectedAns) {
		this.selectedAns = selectedAns;
	}

	/**
	 * @return the navigation
	 */
	public String getNavigation() {
		return navigation;
	}

	/**
	 * @param navigation
	 *            the navigation to set
	 */
	public void setNavigation(String navigation) {
		this.navigation = navigation;
	}

	/**
	 * @return the remainingQuestions
	 */
	public int getRemainingQuestions() {
		return remainingQuestions;
	}

	/**
	 * @param remainingQuestions
	 *            the remainingQuestions to set
	 */
	public void setRemainingQuestions(int remainingQuestions) {
		this.remainingQuestions = remainingQuestions;
	}

	/**
	 * @return the onClickStatus
	 */
	public int getOnClickStatus() {
		return onClickStatus;
	}

	/**
	 * @param onClickStatus
	 *            the onClickStatus to set
	 */
	public void setOnClickStatus(int onClickStatus) {
		this.onClickStatus = onClickStatus;
	}

	/**
	 * @return the subTopicId
	 */
	public int getSubTopicId() {
		return subTopicId;
	}

	/**
	 * @param subTopicId
	 *            the subTopicId to set
	 */
	public void setSubTopicId(int subTopicId) {
		this.subTopicId = subTopicId;
	}

	/**
	 * @return the specficQuestionNo
	 */
	public int getSpecficQuestionNo() {
		return specficQuestionNo;
	}

	/**
	 * @param specficQuestionNo
	 *            the specficQuestionNo to set
	 */
	public void setSpecficQuestionNo(int specficQuestionNo) {
		this.specficQuestionNo = specficQuestionNo;
	}

	/**
	 * @return the examKeyId
	 */
	public String getExamKeyId() {
		return examKeyId;
	}

	/**
	 * @param examKeyId
	 *            the examKeyId to set
	 */
	public void setExamKeyId(String examKeyId) {
		this.examKeyId = examKeyId;
	}

	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy
	 *            the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the postedDate1
	 */
	public String getPostedDate1() {
		return postedDate1;
	}

	/**
	 * @param postedDate1
	 *            the postedDate1 to set
	 */
	public void setPostedDate1(String postedDate1) {
		this.postedDate1 = postedDate1;
	}

	/**
	 * @return the postedDate2
	 */
	public String getPostedDate2() {
		return postedDate2;
	}

	/**
	 * @param postedDate2
	 *            the postedDate2 to set
	 */
	public void setPostedDate2(String postedDate2) {
		this.postedDate2 = postedDate2;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * @param country
	 *            the country to set
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 * @return the practiceid
	 */
	public String getPracticeid() {
		return practiceid;
	}

	/**
	 * @param practiceid
	 *            the practiceid to set
	 */
	public void setPracticeid(String practiceid) {
		this.practiceid = practiceid;
	}

	/**
	 * @return the requirementId
	 */
	public int getRequirementId() {
		return requirementId;
	}

	/**
	 * @param requirementId
	 *            the requirementId to set
	 */
	public void setRequirementId(int requirementId) {
		this.requirementId = requirementId;
	}

	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	/**
	 * @param state
	 *            the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * @return the preSalesPerson
	 */
	public String getPreSalesPerson() {
		return preSalesPerson;
	}

	/**
	 * @param preSalesPerson
	 *            the preSalesPerson to set
	 */
	public void setPreSalesPerson(String preSalesPerson) {
		this.preSalesPerson = preSalesPerson;
	}

	/**
	 * @return the assignedTo
	 */
	public String getAssignedTo() {
		return assignedTo;
	}

	/**
	 * @param assignedTo
	 *            the assignedTo to set
	 */
	public void setAssignedTo(String assignedTo) {
		this.assignedTo = assignedTo;
	}

	/**
	 * @return the assignedBy
	 */
	public String getAssignedBy() {
		return assignedBy;
	}

	/**
	 * @param assignedBy
	 *            the assignedBy to set
	 */
	public void setAssignedBy(String assignedBy) {
		this.assignedBy = assignedBy;
	}

	/**
	 * @return the accId
	 */
	public int getAccId() {
		return accId;
	}

	/**
	 * @param accId
	 *            the accId to set
	 */
	public void setAccId(int accId) {
		this.accId = accId;
	}

	/**
	 * @return the empId
	 */
	public String getEmpId() {
		return empId;
	}

	/**
	 * @param empId
	 *            the empId to set
	 */
	public void setEmpId(String empId) {
		this.empId = empId;
	}

	/**
	 * @return the projectId
	 */
	public String getProjectId() {
		return projectId;
	}

	/**
	 * @param projectId
	 *            the projectId to set
	 */
	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	/**
	 * @return the accountId
	 */
	public int getAccountId() {
		return accountId;
	}

	/**
	 * @param accountId
	 *            the accountId to set
	 */
	public void setAccountId(int accountId) {
		this.accountId = accountId;
	}

	/**
	 * @return the empType
	 */
	public String getEmpType() {
		return empType;
	}

	/**
	 * @param empType
	 *            the empType to set
	 */
	public void setEmpType(String empType) {
		this.empType = empType;
	}

	/**
	 * @return the preAssignEmpId
	 */
	public int getPreAssignEmpId() {
		return preAssignEmpId;
	}

	/**
	 * @param preAssignEmpId
	 *            the preAssignEmpId to set
	 */
	public void setPreAssignEmpId(int preAssignEmpId) {
		this.preAssignEmpId = preAssignEmpId;
	}

	/**
	 * @return the pgNo
	 */
	public int getPgNo() {
		return pgNo;
	}

	/**
	 * @param pgNo
	 *            the pgNo to set
	 */
	public void setPgNo(int pgNo) {
		this.pgNo = pgNo;
	}

	/**
	 * @return the customerName
	 */
	public String getCustomerName() {
		return customerName;
	}

	/**
	 * @param customerName
	 *            the customerName to set
	 */
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	/**
	 * @return the pgFlag
	 */
	public String getPgFlag() {
		return pgFlag;
	}

	/**
	 * @param pgFlag
	 *            the pgFlag to set
	 */
	public void setPgFlag(String pgFlag) {
		this.pgFlag = pgFlag;
	}

	/**
	 * @return the startDate
	 */
	public String getStartDate() {
		return startDate;
	}

	/**
	 * @param startDate
	 *            the startDate to set
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	/**
	 * @return the orderBy
	 */
	public String getOrderBy() {
		return orderBy;
	}

	/**
	 * @param orderBy
	 *            the orderBy to set
	 */
	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}

	/**
	 * @return the prjId
	 */
	public int getPrjId() {
		return prjId;
	}

	/**
	 * @param prjId
	 *            the prjId to set
	 */
	public void setPrjId(int prjId) {
		this.prjId = prjId;
	}

	/**
	 * @return the endDate
	 */
	public String getEndDate() {
		return endDate;
	}

	/**
	 * @param endDate
	 *            the endDate to set
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	/**
	 * @return the requestType
	 */
	public String getRequestType() {
		return requestType;
	}

	/**
	 * @param requestType
	 *            the requestType to set
	 */
	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}

	/**
	 * @return the leadId
	 */
	public int getLeadId() {
		return leadId;
	}

	/**
	 * @param leadId
	 *            the leadId to set
	 */
	public void setLeadId(int leadId) {
		this.leadId = leadId;
	}

	/**
	 * @return the file
	 */
	public File getFile() {
		return file;
	}

	/**
	 * @param file
	 *            the file to set
	 */
	public void setFile(File file) {
		this.file = file;
	}

	/**
	 * @return the fileFileName
	 */
	public String getFileFileName() {
		return fileFileName;
	}

	/**
	 * @param fileFileName
	 *            the fileFileName to set
	 */
	public void setFileFileName(String fileFileName) {
		this.fileFileName = fileFileName;
	}

	/**
	 * @return the fileFileContentType
	 */
	public String getFileFileContentType() {
		return fileFileContentType;
	}

	/**
	 * @param fileFileContentType
	 *            the fileFileContentType to set
	 */
	public void setFileFileContentType(String fileFileContentType) {
		this.fileFileContentType = fileFileContentType;
	}

	/**
	 * @return the generatedPath
	 */
	public String getGeneratedPath() {
		return generatedPath;
	}

	/**
	 * @param generatedPath
	 *            the generatedPath to set
	 */
	public void setGeneratedPath(String generatedPath) {
		this.generatedPath = generatedPath;
	}

	/**
	 * @return the fileLocation
	 */
	public String getFileLocation() {
		return fileLocation;
	}

	/**
	 * @param fileLocation
	 *            the fileLocation to set
	 */
	public void setFileLocation(String fileLocation) {
		this.fileLocation = fileLocation;
	}

	/**
	 * @return the filepath
	 */
	public String getFilepath() {
		return filepath;
	}

	/**
	 * @param filepath
	 *            the filepath to set
	 */
	public void setFilepath(String filepath) {
		this.filepath = filepath;
	}

	/**
	 * @return the objectType
	 */
	public String getObjectType() {
		return objectType;
	}

	/**
	 * @param objectType
	 *            the objectType to set
	 */
	public void setObjectType(String objectType) {
		this.objectType = objectType;
	}

	/**
	 * @return the attachmentLocation
	 */
	public String getAttachmentLocation() {
		return attachmentLocation;
	}

	/**
	 * @param attachmentLocation
	 *            the attachmentLocation to set
	 */
	public void setAttachmentLocation(String attachmentLocation) {
		this.attachmentLocation = attachmentLocation;
	}

	/**
	 * @return the topicId
	 */
	public int getTopicId() {
		return topicId;
	}

	/**
	 * @param topicId
	 *            the topicId to set
	 */
	public void setTopicId(int topicId) {
		this.topicId = topicId;
	}

	/**
	 * @return the year
	 */
	public String getYear() {
		return year;
	}

	/**
	 * @param year
	 *            the year to set
	 */
	public void setYear(String year) {
		this.year = year;
	}

	/**
	 * @return the month
	 */
	public String getMonth() {
		return month;
	}

	/**
	 * @param month
	 *            the month to set
	 */
	public void setMonth(String month) {
		this.month = month;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type
	 *            the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the category
	 */
	public String getCategory() {
		return category;
	}

	/**
	 * @param category
	 *            the category to set
	 */
	public void setCategory(String category) {
		this.category = category;
	}

	/**
	 * @return the activityType
	 */
	public String getActivityType() {
		return activityType;
	}

	/**
	 * @param activityType
	 *            the activityType to set
	 */
	public void setActivityType(String activityType) {
		this.activityType = activityType;
	}

	/**
	 * @return the bdmId
	 */
	public String getBdmId() {
		return bdmId;
	}

	/**
	 * @param bdmId
	 *            the bdmId to set
	 */
	public void setBdmId(String bdmId) {
		this.bdmId = bdmId;
	}

	/**
	 * @return the activityId
	 */
	public String getActivityId() {
		return activityId;
	}

	/**
	 * @param activityId
	 *            the activityId to set
	 */
	public void setActivityId(String activityId) {
		this.activityId = activityId;
	}

	/**
	 * @return the teamMemberId
	 */
	public String getTeamMemberId() {
		return teamMemberId;
	}

	/**
	 * @param teamMemberId
	 *            the teamMemberId to set
	 */
	public void setTeamMemberId(String teamMemberId) {
		this.teamMemberId = teamMemberId;
	}

	/**
	 * @return the startDateContacts
	 */
	public String getStartDateContacts() {
		return startDateContacts;
	}

	/**
	 * @param startDateContacts
	 *            the startDateContacts to set
	 */
	public void setStartDateContacts(String startDateContacts) {
		this.startDateContacts = startDateContacts;
	}

	/**
	 * @return the endDateContacts
	 */
	public String getEndDateContacts() {
		return endDateContacts;
	}

	/**
	 * @param endDateContacts
	 *            the endDateContacts to set
	 */
	public void setEndDateContacts(String endDateContacts) {
		this.endDateContacts = endDateContacts;
	}

	/**
	 * @return the customerId
	 */
	public String getCustomerId() {
		return customerId;
	}

	/**
	 * @param customerId
	 *            the customerId to set
	 */
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	/**
	 * @return the titleType
	 */
	public String getTitleType() {
		return titleType;
	}

	/**
	 * @param titleType
	 *            the titleType to set
	 */
	public void setTitleType(String titleType) {
		this.titleType = titleType;
	}

	/**
	 * @return the teamMemberCheck
	 */
	public int getTeamMemberCheck() {
		return teamMemberCheck;
	}

	/**
	 * @param teamMemberCheck
	 *            the teamMemberCheck to set
	 */
	public void setTeamMemberCheck(int teamMemberCheck) {
		this.teamMemberCheck = teamMemberCheck;
	}

	/**
	 * @return the reqId
	 */
	public String getReqId() {
		return reqId;
	}

	/**
	 * @param reqId
	 *            the reqId to set
	 */
	public void setReqId(String reqId) {
		this.reqId = reqId;
	}

	/**
	 * @return the statesFromOrg
	 */
	public String getStatesFromOrg() {
		return statesFromOrg;
	}

	/**
	 * @param statesFromOrg
	 *            the statesFromOrg to set
	 */
	public void setStatesFromOrg(String statesFromOrg) {
		this.statesFromOrg = statesFromOrg;
	}

	/**
	 * @return the bdeLoginId
	 */
	public String getBdeLoginId() {
		return bdeLoginId;
	}

	/**
	 * @param bdeLoginId
	 *            the bdeLoginId to set
	 */
	public void setBdeLoginId(String bdeLoginId) {
		this.bdeLoginId = bdeLoginId;
	}

	/**
	 * @return the lastActivityFrom
	 */
	public String getLastActivityFrom() {
		return lastActivityFrom;
	}

	/**
	 * @param lastActivityFrom
	 *            the lastActivityFrom to set
	 */
	public void setLastActivityFrom(String lastActivityFrom) {
		this.lastActivityFrom = lastActivityFrom;
	}

	/**
	 * @return the lastActivityTo
	 */
	public String getLastActivityTo() {
		return lastActivityTo;
	}

	/**
	 * @param lastActivityTo
	 *            the lastActivityTo to set
	 */
	public void setLastActivityTo(String lastActivityTo) {
		this.lastActivityTo = lastActivityTo;
	}

	/**
	 * @return the opportunity
	 */
	public String getOpportunity() {
		return opportunity;
	}

	/**
	 * @param opportunity
	 *            the opportunity to set
	 */
	public void setOpportunity(String opportunity) {
		this.opportunity = opportunity;
	}

	/**
	 * @return the touched
	 */
	public String getTouched() {
		return touched;
	}

	/**
	 * @param touched
	 *            the touched to set
	 */
	public void setTouched(String touched) {
		this.touched = touched;
	}

	/**
	 * @return the employeeName
	 */
	public String getEmployeeName() {
		return employeeName;
	}

	/**
	 * @param employeeName
	 *            the employeeName to set
	 */
	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	/**
	 * @return the invId
	 */
	public int getInvId() {
		return invId;
	}

	/**
	 * @param invId
	 *            the invId to set
	 */
	public void setInvId(int invId) {
		this.invId = invId;
	}

	/**
	 * @return the reminderDate
	 */
	public Timestamp getReminderDate() {
		return reminderDate;
	}

	/**
	 * @param reminderDate
	 *            the reminderDate to set
	 */
	public void setReminderDate(Timestamp reminderDate) {
		this.reminderDate = reminderDate;
	}

	/**
	 * @return the followUpComments
	 */
	public String getFollowUpComments() {
		return followUpComments;
	}

	/**
	 * @param followUpComments
	 *            the followUpComments to set
	 */
	public void setFollowUpComments(String followUpComments) {
		this.followUpComments = followUpComments;
	}

	/**
	 * @return the nextFollowUpSteps
	 */
	public String getNextFollowUpSteps() {
		return nextFollowUpSteps;
	}

	/**
	 * @param nextFollowUpSteps
	 *            the nextFollowUpSteps to set
	 */
	public void setNextFollowUpSteps(String nextFollowUpSteps) {
		this.nextFollowUpSteps = nextFollowUpSteps;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the reminderFlag
	 */
	public int getReminderFlag() {
		return reminderFlag;
	}

	/**
	 * @param reminderFlag
	 *            the reminderFlag to set
	 */
	public void setReminderFlag(int reminderFlag) {
		this.reminderFlag = reminderFlag;
	}

	/**
	 * @return the operationType
	 */
	public String getOperationType() {
		return operationType;
	}

	/**
	 * @param operationType
	 *            the operationType to set
	 */
	public void setOperationType(String operationType) {
		this.operationType = operationType;
	}

	/**
	 * @return the projectName
	 */
	public String getProjectName() {
		return projectName;
	}

	/**
	 * @param projectName
	 *            the projectName to set
	 */
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	/**
	 * @return the projectStartDate
	 */
	public String getProjectStartDate() {
		return projectStartDate;
	}

	/**
	 * @param projectStartDate
	 *            the projectStartDate to set
	 */
	public void setProjectStartDate(String projectStartDate) {
		this.projectStartDate = projectStartDate;
	}

	/**
	 * @return the pmoLoginId
	 */
	public String getPmoLoginId() {
		return pmoLoginId;
	}

	/**
	 * @param pmoLoginId
	 *            the pmoLoginId to set
	 */
	public void setPmoLoginId(String pmoLoginId) {
		this.pmoLoginId = pmoLoginId;
	}

	/**
	 * @return the practiceId
	 */
	public String getPracticeId() {
		return practiceId;
	}

	/**
	 * @param practiceId
	 *            the practiceId to set
	 */
	public void setPracticeId(String practiceId) {
		this.practiceId = practiceId;
	}

	/**
	 * @return the loginId
	 */
	public String getLoginId() {
		return loginId;
	}

	/**
	 * @param loginId
	 *            the loginId to set
	 */
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	/**
	 * @return the notesId
	 */
	public int getNotesId() {
		return notesId;
	}

	/**
	 * @param notesId
	 *            the notesId to set
	 */
	public void setNotesId(int notesId) {
		this.notesId = notesId;
	}

	/**
	 * @return the graphId
	 */
	public int getGraphId() {
		return graphId;
	}

	/**
	 * @param graphId
	 *            the graphId to set
	 */
	public void setGraphId(int graphId) {
		this.graphId = graphId;
	}

	/**
	 * @return the taskStartDate
	 */
	public String getTaskStartDate() {
		return taskStartDate;
	}

	/**
	 * @param taskStartDate
	 *            the taskStartDate to set
	 */
	public void setTaskStartDate(String taskStartDate) {
		this.taskStartDate = taskStartDate;
	}

	/**
	 * @return the taskEndDate
	 */
	public String getTaskEndDate() {
		return taskEndDate;
	}

	/**
	 * @param taskEndDate
	 *            the taskEndDate to set
	 */
	public void setTaskEndDate(String taskEndDate) {
		this.taskEndDate = taskEndDate;
	}

	/**
	 * @return the reportsTo
	 */
	public String getReportsTo() {
		return reportsTo;
	}

	/**
	 * @param reportsTo
	 *            the reportsTo to set
	 */
	public void setReportsTo(String reportsTo) {
		this.reportsTo = reportsTo;
	}

	/**
	 * @return the comments
	 */
	public String getComments() {
		return comments;
	}

	/**
	 * @param comments
	 *            the comments to set
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}

	/**
	 * @return the strengthsComments
	 */
	public String getStrengthsComments() {
		return strengthsComments;
	}

	/**
	 * @param strengthsComments
	 *            the strengthsComments to set
	 */
	public void setStrengthsComments(String strengthsComments) {
		this.strengthsComments = strengthsComments;
	}

	/**
	 * @return the improvementsComments
	 */
	public String getImprovementsComments() {
		return improvementsComments;
	}

	/**
	 * @param improvementsComments
	 *            the improvementsComments to set
	 */
	public void setImprovementsComments(String improvementsComments) {
		this.improvementsComments = improvementsComments;
	}

	/**
	 * @return the shortTermGoalComments
	 */
	public String getShortTermGoalComments() {
		return shortTermGoalComments;
	}

	/**
	 * @param shortTermGoalComments
	 *            the shortTermGoalComments to set
	 */
	public void setShortTermGoalComments(String shortTermGoalComments) {
		this.shortTermGoalComments = shortTermGoalComments;
	}

	/**
	 * @return the longTermGoalComments
	 */
	public String getLongTermGoalComments() {
		return longTermGoalComments;
	}

	/**
	 * @param longTermGoalComments
	 *            the longTermGoalComments to set
	 */
	public void setLongTermGoalComments(String longTermGoalComments) {
		this.longTermGoalComments = longTermGoalComments;
	}

	/**
	 * @return the rowCount
	 */
	public int getRowCount() {
		return rowCount;
	}

	/**
	 * @param rowCount
	 *            the rowCount to set
	 */
	public void setRowCount(int rowCount) {
		this.rowCount = rowCount;
	}

	/**
	 * @return the curretRole
	 */
	public String getCurretRole() {
		return curretRole;
	}

	/**
	 * @param curretRole
	 *            the curretRole to set
	 */
	public void setCurretRole(String curretRole) {
		this.curretRole = curretRole;
	}

	/**
	 * @return the appraisalId
	 */
	public int getAppraisalId() {
		return appraisalId;
	}

	/**
	 * @param appraisalId
	 *            the appraisalId to set
	 */
	public void setAppraisalId(int appraisalId) {
		this.appraisalId = appraisalId;
	}

	/**
	 * @return the lineId
	 */
	public int getLineId() {
		return lineId;
	}

	/**
	 * @param lineId
	 *            the lineId to set
	 */
	public void setLineId(int lineId) {
		this.lineId = lineId;
	}

	/**
	 * @return the quarterly
	 */
	public String getQuarterly() {
		return quarterly;
	}

	/**
	 * @param quarterly
	 *            the quarterly to set
	 */
	public void setQuarterly(String quarterly) {
		this.quarterly = quarterly;
	}

	/**
	 * @return the shortTermGoal
	 */
	public String getShortTermGoal() {
		return shortTermGoal;
	}

	/**
	 * @param shortTermGoal
	 *            the shortTermGoal to set
	 */
	public void setShortTermGoal(String shortTermGoal) {
		this.shortTermGoal = shortTermGoal;
	}

	/**
	 * @return the longTermGoal
	 */
	public String getLongTermGoal() {
		return longTermGoal;
	}

	/**
	 * @param longTermGoal
	 *            the longTermGoal to set
	 */
	public void setLongTermGoal(String longTermGoal) {
		this.longTermGoal = longTermGoal;
	}

	/**
	 * @return the strength
	 */
	public String getStrength() {
		return strength;
	}

	/**
	 * @param strength
	 *            the strength to set
	 */
	public void setStrength(String strength) {
		this.strength = strength;
	}

	/**
	 * @return the improvements
	 */
	public String getImprovements() {
		return improvements;
	}

	/**
	 * @param improvements
	 *            the improvements to set
	 */
	public void setImprovements(String improvements) {
		this.improvements = improvements;
	}

	/**
	 * @return the rejectedComments
	 */
	public String getRejectedComments() {
		return rejectedComments;
	}

	/**
	 * @param rejectedComments
	 *            the rejectedComments to set
	 */
	public void setRejectedComments(String rejectedComments) {
		this.rejectedComments = rejectedComments;
	}

	/**
	 * @return the operationTeamStatus
	 */
	public String getOperationTeamStatus() {
		return operationTeamStatus;
	}

	/**
	 * @param operationTeamStatus
	 *            the operationTeamStatus to set
	 */
	public void setOperationTeamStatus(String operationTeamStatus) {
		this.operationTeamStatus = operationTeamStatus;
	}

	/**
	 * @return the managerRejectedComments
	 */
	public String getManagerRejectedComments() {
		return managerRejectedComments;
	}

	/**
	 * @param managerRejectedComments
	 *            the managerRejectedComments to set
	 */
	public void setManagerRejectedComments(String managerRejectedComments) {
		this.managerRejectedComments = managerRejectedComments;
	}

	/**
	 * @return the operationRejectedComments
	 */
	public String getOperationRejectedComments() {
		return operationRejectedComments;
	}

	/**
	 * @param operationRejectedComments
	 *            the operationRejectedComments to set
	 */
	public void setOperationRejectedComments(String operationRejectedComments) {
		this.operationRejectedComments = operationRejectedComments;
	}

	/**
	 * @return the dayCount
	 */
	public int getDayCount() {
		return dayCount;
	}

	/**
	 * @param dayCount
	 *            the dayCount to set
	 */
	public void setDayCount(int dayCount) {
		this.dayCount = dayCount;
	}

	/**
	 * @return the accessCount
	 */
	public int getAccessCount() {
		return accessCount;
	}

	/**
	 * @param accessCount
	 *            the accessCount to set
	 */
	public void setAccessCount(int accessCount) {
		this.accessCount = accessCount;
	}

	/**
	 * @return the preAssignSalesId
	 */
	public String getPreAssignSalesId() {
		return preAssignSalesId;
	}

	/**
	 * @param preAssignSalesId
	 *            the preAssignSalesId to set
	 */
	public void setPreAssignSalesId(String preAssignSalesId) {
		this.preAssignSalesId = preAssignSalesId;
	}

	/**
	 * @return the teamMemberStatus
	 */
	public String getTeamMemberStatus() {
		return teamMemberStatus;
	}

	/**
	 * @param teamMemberStatus
	 *            the teamMemberStatus to set
	 */
	public void setTeamMemberStatus(String teamMemberStatus) {
		this.teamMemberStatus = teamMemberStatus;
	}

	/**
	 * @return the salesName
	 */
	public String getSalesName() {
		return salesName;
	}

	/**
	 * @param salesName
	 *            the salesName to set
	 */
	public void setSalesName(String salesName) {
		this.salesName = salesName;
	}

	/**
	 * @return the departmentId
	 */
	public String getDepartmentId() {
		return departmentId;
	}

	/**
	 * @param departmentId
	 *            the departmentId to set
	 */
	public void setDepartmentId(String departmentId) {
		this.departmentId = departmentId;
	}

	/**
	 * @return the subPracticeId
	 */
	public String getSubPracticeId() {
		return subPracticeId;
	}

	/**
	 * @param subPracticeId
	 *            the subPracticeId to set
	 */
	public void setSubPracticeId(String subPracticeId) {
		this.subPracticeId = subPracticeId;
	}

	/**
	 * @return the consultantId
	 */
	public String getConsultantId() {
		return consultantId;
	}

	/**
	 * @param consultantId
	 *            the consultantId to set
	 */
	public void setConsultantId(String consultantId) {
		this.consultantId = consultantId;
	}

	/**
	 * @return the file1FileName
	 */
	public String getFile1FileName() {
		return file1FileName;
	}

	/**
	 * @param file1FileName
	 *            the file1FileName to set
	 */
	public void setFile1FileName(String file1FileName) {
		this.file1FileName = file1FileName;
	}

	/**
	 * @return the attachmentLocation1
	 */
	public String getAttachmentLocation1() {
		return attachmentLocation1;
	}

	/**
	 * @param attachmentLocation1
	 *            the attachmentLocation1 to set
	 */
	public void setAttachmentLocation1(String attachmentLocation1) {
		this.attachmentLocation1 = attachmentLocation1;
	}

	/**
	 * @return the consultantName
	 */
	public String getConsultantName() {
		return consultantName;
	}

	/**
	 * @param consultantName
	 *            the consultantName to set
	 */
	public void setConsultantName(String consultantName) {
		this.consultantName = consultantName;
	}

	/**
	 * @return the requestId
	 */
	public int getRequestId() {
		return requestId;
	}

	/**
	 * @param requestId
	 *            the requestId to set
	 */
	public void setRequestId(int requestId) {
		this.requestId = requestId;
	}

	/**
	 * @return the accessType
	 */
	public String getAccessType() {
		return accessType;
	}

	/**
	 * @param accessType
	 *            the accessType to set
	 */
	public void setAccessType(String accessType) {
		this.accessType = accessType;
	}

	/**
	 * @return the videoDescription
	 */
	public String getVideoDescription() {
		return videoDescription;
	}

	/**
	 * @param videoDescription
	 *            the videoDescription to set
	 */
	public void setVideoDescription(String videoDescription) {
		this.videoDescription = videoDescription;
	}

	/**
	 * @return the videoTitlesList
	 */
	public List getVideoTitlesList() {
		return videoTitlesList;
	}

	/**
	 * @param videoTitlesList
	 *            the videoTitlesList to set
	 */
	public void setVideoTitlesList(List videoTitlesList) {
		this.videoTitlesList = videoTitlesList;
	}

	/**
	 * @return the location
	 */
	public String getLocation() {
		return location;
	}

	/**
	 * @param location
	 *            the location to set
	 */
	public void setLocation(String location) {
		this.location = location;
	}

	/**
	 * @return the quarter
	 */
	public String getQuarter() {
		return quarter;
	}

	/**
	 * @param quarter
	 *            the quarter to set
	 */
	public void setQuarter(String quarter) {
		this.quarter = quarter;
	}

	/**
	 * @return the columnLabel
	 */
	public String getColumnLabel() {
		return columnLabel;
	}

	/**
	 * @param columnLabel
	 *            the columnLabel to set
	 */
	public void setColumnLabel(String columnLabel) {
		this.columnLabel = columnLabel;
	}

	/**
	 * @return the event_Id
	 */
	public int getEvent_Id() {
		return event_Id;
	}

	/**
	 * @param event_Id
	 *            the event_Id to set
	 */
	public void setEvent_Id(int event_Id) {
		this.event_Id = event_Id;
	}

	/**
	 * @return the nomineeIds
	 */
	public String getNomineeIds() {
		return nomineeIds;
	}

	/**
	 * @param nomineeIds
	 *            the nomineeIds to set
	 */
	public void setNomineeIds(String nomineeIds) {
		this.nomineeIds = nomineeIds;
	}

	/**
	 * @return the nomineeName
	 */
	public String getNomineeName() {
		return nomineeName;
	}

	/**
	 * @param nomineeName
	 *            the nomineeName to set
	 */
	public void setNomineeName(String nomineeName) {
		this.nomineeName = nomineeName;
	}

	/**
	 * @return the nomineeState
	 */
	public String getNomineeState() {
		return nomineeState;
	}

	/**
	 * @param nomineeState
	 *            the nomineeState to set
	 */
	public void setNomineeState(String nomineeState) {
		this.nomineeState = nomineeState;
	}

	/**
	 * @return the eventName
	 */
	public String getEventName() {
		return eventName;
	}

	/**
	 * @param eventName
	 *            the eventName to set
	 */
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	/**
	 * @return the eventLocation
	 */
	public String getEventLocation() {
		return eventLocation;
	}

	/**
	 * @param eventLocation
	 *            the eventLocation to set
	 */
	public void setEventLocation(String eventLocation) {
		this.eventLocation = eventLocation;
	}

	/**
	 * @return the eventLink
	 */
	public String getEventLink() {
		return eventLink;
	}

	/**
	 * @param eventLink
	 *            the eventLink to set
	 */
	public void setEventLink(String eventLink) {
		this.eventLink = eventLink;
	}

	/**
	 * @return the eventStartDate
	 */
	public String getEventStartDate() {
		return eventStartDate;
	}

	/**
	 * @param eventStartDate
	 *            the eventStartDate to set
	 */
	public void setEventStartDate(String eventStartDate) {
		this.eventStartDate = eventStartDate;
	}

	/**
	 * @return the eventEndDate
	 */
	public String getEventEndDate() {
		return eventEndDate;
	}

	/**
	 * @param eventEndDate
	 *            the eventEndDate to set
	 */
	public void setEventEndDate(String eventEndDate) {
		this.eventEndDate = eventEndDate;
	}

	/**
	 * @return the objectId
	 */
	public String getObjectId() {
		return objectId;
	}

	/**
	 * @param objectId
	 *            the objectId to set
	 */
	public void setObjectId(String objectId) {
		this.objectId = objectId;
	}

	/**
	 * @return the overlayStatus
	 */
	public String getOverlayStatus() {
		return overlayStatus;
	}

	/**
	 * @param overlayStatus
	 *            the overlayStatus to set
	 */
	public void setOverlayStatus(String overlayStatus) {
		this.overlayStatus = overlayStatus;
	}

	/**
	 * @return the overlayYear
	 */
	public int getOverlayYear() {
		return overlayYear;
	}

	/**
	 * @param overlayYear
	 *            the overlayYear to set
	 */
	public void setOverlayYear(int overlayYear) {
		this.overlayYear = overlayYear;
	}

	/**
	 * @return the overlayMonth
	 */
	public int getOverlayMonth() {
		return overlayMonth;
	}

	/**
	 * @param overlayMonth
	 *            the overlayMonth to set
	 */
	public void setOverlayMonth(int overlayMonth) {
		this.overlayMonth = overlayMonth;
	}

	/**
	 * @return the createdDate
	 */
	public Timestamp getCreatedDate() {
		return createdDate;
	}

	/**
	 * @param createdDate
	 *            the createdDate to set
	 */
	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	/**
	 * @return the ModifiedBy
	 */
	public String getModifiedBy() {
		return ModifiedBy;
	}

	/**
	 * @param ModifiedBy
	 *            the ModifiedBy to set
	 */
	public void setModifiedBy(String ModifiedBy) {
		this.ModifiedBy = ModifiedBy;
	}

	/**
	 * @return the ModifiedDate
	 */
	public Timestamp getModifiedDate() {
		return ModifiedDate;
	}

	/**
	 * @param ModifiedDate
	 *            the ModifiedDate to set
	 */
	public void setModifiedDate(Timestamp ModifiedDate) {
		this.ModifiedDate = ModifiedDate;
	}

	/**
	 * @return the overlayDate
	 */
	public String getOverlayDate() {
		return overlayDate;
	}

	/**
	 * @param overlayDate
	 *            the overlayDate to set
	 */
	public void setOverlayDate(String overlayDate) {
		this.overlayDate = overlayDate;
	}

	/**
	 * @return the mail
	 */
	public String getMail() {
		return mail;
	}

	/**
	 * @param mail
	 *            the mail to set
	 */
	public void setMail(String mail) {
		this.mail = mail;
	}

	/**
	 * @return the objId
	 */
	public int getObjId() {
		return objId;
	}

	/**
	 * @param objId
	 *            the objId to set
	 */
	public void setObjId(int objId) {
		this.objId = objId;
	}

	/**
	 * @return the campaignStartDate
	 */
	public String getCampaignStartDate() {
		return campaignStartDate;
	}

	/**
	 * @param campaignStartDate
	 *            the campaignStartDate to set
	 */
	public void setCampaignStartDate(String campaignStartDate) {
		this.campaignStartDate = campaignStartDate;
	}

	/**
	 * @return the movieName
	 */
	public String getMovieName() {
		return movieName;
	}

	/**
	 * @param movieName
	 *            the movieName to set
	 */
	public void setMovieName(String movieName) {
		this.movieName = movieName;
	}

	/**
	 * @return the movieDates
	 */
	public String getMovieDates() {
		return movieDates;
	}

	/**
	 * @param movieDates
	 *            the movieDates to set
	 */
	public void setMovieDates(String movieDates) {
		this.movieDates = movieDates;
	}

	/**
	 * @return the expireDate
	 */
	public String getExpireDate() {
		return expireDate;
	}

	/**
	 * @param expireDate
	 *            the expireDate to set
	 */
	public void setExpireDate(String expireDate) {
		this.expireDate = expireDate;
	}

	/**
	 * @return the survyId
	 */
	public int getSurvyId() {
		return survyId;
	}

	/**
	 * @param survyId
	 *            the survyId to set
	 */
	public void setSurvyId(int survyId) {
		this.survyId = survyId;
	}

	public String getProjectType() {
		return projectType;
	}

	public void setProjectType(String projectType) {
		this.projectType = projectType;
	}

	public String getImpact() {
		return impact;
	}

	public void setImpact(String impact) {
		this.impact = impact;
	}

	public String getIsIncludeTeam() {
		return isIncludeTeam;
	}

	public void setIsIncludeTeam(String isIncludeTeam) {
		this.isIncludeTeam = isIncludeTeam;
	}

	public int getQyear() {
		return qyear;
	}

	public void setQyear(int qyear) {
		this.qyear = qyear;
	}

	public String getWeek() {
		return week;
	}

	public void setWeek(String week) {
		this.week = week;
	}

	public String getCostModel() {
		return costModel;
	}

	public void setCostModel(String costModel) {
		this.costModel = costModel;
	}

	public String getEmpNo() {
		return empNo;
	}

	public void setEmpNo(String empNo) {
		this.empNo = empNo;
	}

	public String getCurrId() {
		return currId;
	}

	public void setCurrId(String currId) {
		this.currId = currId;
	}

	public String getStateStartDate() {
		return stateStartDate;
	}

	public void setStateStartDate(String stateStartDate) {
		this.stateStartDate = stateStartDate;
	}

	public String getStateEndDate() {
		return stateEndDate;
	}

	public void setStateEndDate(String stateEndDate) {
		this.stateEndDate = stateEndDate;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public String getHeaderFields() {
		return headerFields;
	}

	public void setHeaderFields(String headerFields) {
		this.headerFields = headerFields;
	}

	public String getStringData() {
		return stringData;
	}

	public void setStringData(String stringData) {
		this.stringData = stringData;
	}

	public String getLookUpId() {
		return lookUpId;
	}

	public void setLookUpId(String lookUpId) {
		this.lookUpId = lookUpId;
	}

	public String getColumnName() {
		return columnName;
	}

	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}

	public String getColumnData() {
		return columnData;
	}

	public void setColumnData(String columnData) {
		this.columnData = columnData;
	}

	public int getExpMonth() {
		return expMonth;
	}

	public void setExpMonth(int expMonth) {
		this.expMonth = expMonth;
	}

	public int getExpYear() {
		return expYear;
	}

	public void setExpYear(int expYear) {
		this.expYear = expYear;
	}

	public String getJsonData() {
		return jsonData;
	}

	public void setJsonData(String jsonData) {
		this.jsonData = jsonData;
	}

	/**
	 * @return the file0
	 */
	public File getFile0() {
		return file0;
	}

	/**
	 * @param file0
	 *            the file0 to set
	 */
	public void setFile0(File file0) {
		this.file0 = file0;
	}

	/**
	 * @return the file0FileName
	 */
	public String getFile0FileName() {
		return file0FileName;
	}

	/**
	 * @param file0FileName
	 *            the file0FileName to set
	 */
	public void setFile0FileName(String file0FileName) {
		this.file0FileName = file0FileName;
	}

	/**
	 * @return the file1
	 */
	public File getFile1() {
		return file1;
	}

	/**
	 * @param file1
	 *            the file1 to set
	 */
	public void setFile1(File file1) {
		this.file1 = file1;
	}

	/**
	 * @return the file2
	 */
	public File getFile2() {
		return file2;
	}

	/**
	 * @param file2
	 *            the file2 to set
	 */
	public void setFile2(File file2) {
		this.file2 = file2;
	}

	/**
	 * @return the file2FileName
	 */
	public String getFile2FileName() {
		return file2FileName;
	}

	/**
	 * @param file2FileName
	 *            the file2FileName to set
	 */
	public void setFile2FileName(String file2FileName) {
		this.file2FileName = file2FileName;
	}

	/**
	 * @return the file3
	 */
	public File getFile3() {
		return file3;
	}

	/**
	 * @param file3
	 *            the file3 to set
	 */
	public void setFile3(File file3) {
		this.file3 = file3;
	}

	/**
	 * @return the file3FileName
	 */
	public String getFile3FileName() {
		return file3FileName;
	}

	/**
	 * @param file3FileName
	 *            the file3FileName to set
	 */
	public void setFile3FileName(String file3FileName) {
		this.file3FileName = file3FileName;
	}

	/**
	 * @return the file4
	 */
	public File getFile4() {
		return file4;
	}

	/**
	 * @param file4
	 *            the file4 to set
	 */
	public void setFile4(File file4) {
		this.file4 = file4;
	}

	/**
	 * @return the file4FileName
	 */
	public String getFile4FileName() {
		return file4FileName;
	}

	/**
	 * @param file4FileName
	 *            the file4FileName to set
	 */
	public void setFile4FileName(String file4FileName) {
		this.file4FileName = file4FileName;
	}

	/**
	 * @return the file5
	 */
	public File getFile5() {
		return file5;
	}

	/**
	 * @param file5
	 *            the file5 to set
	 */
	public void setFile5(File file5) {
		this.file5 = file5;
	}

	/**
	 * @return the file5FileName
	 */
	public String getFile5FileName() {
		return file5FileName;
	}

	/**
	 * @param file5FileName
	 *            the file5FileName to set
	 */
	public void setFile5FileName(String file5FileName) {
		this.file5FileName = file5FileName;
	}

	/**
	 * @return the file0FileContentType
	 */
	public String getFile0FileContentType() {
		return file0FileContentType;
	}

	/**
	 * @param file0FileContentType
	 *            the file0FileContentType to set
	 */
	public void setFile0FileContentType(String file0FileContentType) {
		this.file0FileContentType = file0FileContentType;
	}

	/**
	 * @return the file0Location
	 */
	public String getFile0Location() {
		return file0Location;
	}

	/**
	 * @param file0Location
	 *            the file0Location to set
	 */
	public void setFile0Location(String file0Location) {
		this.file0Location = file0Location;
	}

	/**
	 * @return the releaseId
	 */
	public int getReleaseId() {
		return releaseId;
	}

	/**
	 * @param releaseId
	 *            the releaseId to set
	 */
	public void setReleaseId(int releaseId) {
		this.releaseId = releaseId;
	}

	public AttachmentService getAttachmentService() {
		return attachmentService;
	}

	public void setAttachmentService(AttachmentService attachmentService) {
		this.attachmentService = attachmentService;
	}

	public String getBiometricDate() {
		return biometricDate;
	}

	public void setBiometricDate(String biometricDate) {
		this.biometricDate = biometricDate;
	}

	public int getBiometricHrs() {
		return biometricHrs;
	}

	public void setBiometricHrs(int biometricHrs) {
		this.biometricHrs = biometricHrs;
	}

	public int getTimeSheetHrs() {
		return timeSheetHrs;
	}

	public void setTimeSheetHrs(int timeSheetHrs) {
		this.timeSheetHrs = timeSheetHrs;
	}

	public String getLeaveStatus() {
		return leaveStatus;
	}

	public void setLeaveStatus(String leaveStatus) {
		this.leaveStatus = leaveStatus;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getSyncYearOverlay() {
		return syncYearOverlay;
	}

	public void setSyncYearOverlay(String syncYearOverlay) {
		this.syncYearOverlay = syncYearOverlay;
	}

	public String getSyncMonthOverlay() {
		return syncMonthOverlay;
	}

	public void setSyncMonthOverlay(String syncMonthOverlay) {
		this.syncMonthOverlay = syncMonthOverlay;
	}

	public Map getLocationList() {
		return LocationList;
	}

	public void setLocationList(Map locationList) {
		LocationList = locationList;
	}

	public String getDaysProject() {
		return daysProject;
	}

	public void setDaysProject(String daysProject) {
		this.daysProject = daysProject;
	}

	public String getDaysInMonth() {
		return daysInMonth;
	}

	public void setDaysInMonth(String daysInMonth) {
		this.daysInMonth = daysInMonth;
	}

	public String getDaysWeekends() {
		return daysWeekends;
	}

	public void setDaysWeekends(String daysWeekends) {
		this.daysWeekends = daysWeekends;
	}

	public String getDaysWorked() {
		return daysWorked;
	}

	public void setDaysWorked(String daysWorked) {
		this.daysWorked = daysWorked;
	}

	public String getDaysVacation() {
		return daysVacation;
	}

	public void setDaysVacation(String daysVacation) {
		this.daysVacation = daysVacation;
	}

	public String getDaysHolidays() {
		return daysHolidays;
	}

	public void setDaysHolidays(String daysHolidays) {
		this.daysHolidays = daysHolidays;
	}

	public String getEmpnameById() {
		return empnameById;
	}

	public void setEmpnameById(String empnameById) {
		this.empnameById = empnameById;
	}

	public String getLocationId() {
		return locationId;
	}

	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}

	public String getReportType() {
		return reportType;
	}

	public void setReportType(String reportType) {
		this.reportType = reportType;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public String getReportTypeHidden() {
		return reportTypeHidden;
	}

	public void setReportTypeHidden(String reportTypeHidden) {
		this.reportTypeHidden = reportTypeHidden;
	}

	public String getHolidayDate() {
		return holidayDate;
	}

	public void setHolidayDate(String holidayDate) {
		this.holidayDate = holidayDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getPastMonths() {
		return pastMonths;
	}

	public void setPastMonths(int pastMonths) {
		this.pastMonths = pastMonths;
	}

	public boolean isExternal() {
		return external;
	}

	public void setExternal(boolean external) {
		this.external = external;
	}

	public Map getRolesMap() {
		return rolesMap;
	}

	public void setRolesMap(Map rolesMap) {
		this.rolesMap = rolesMap;
	}

	public int getShadowFlag() {
		return shadowFlag;
	}

	public void setShadowFlag(int shadowFlag) {
		this.shadowFlag = shadowFlag;
	}

	public String getIncludeTeamFlag() {
		return includeTeamFlag;
	}

	public void setIncludeTeamFlag(String includeTeamFlag) {
		this.includeTeamFlag = includeTeamFlag;
	}

	public String getReportBy() {
		return reportBy;
	}

	public void setReportBy(String reportBy) {
		this.reportBy = reportBy;
	}

	public int getIncludeTeam() {
		return includeTeam;
	}

	public void setIncludeTeam(int includeTeam) {
		this.includeTeam = includeTeam;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public String getEventCoordinatorName() {
		return eventCoordinatorName;
	}

	public void setEventCoordinatorName(String eventCoordinatorName) {
		this.eventCoordinatorName = eventCoordinatorName;
	}

	public String getEventCoordinatorEmail() {
		return eventCoordinatorEmail;
	}

	public void setEventCoordinatorEmail(String eventCoordinatorEmail) {
		this.eventCoordinatorEmail = eventCoordinatorEmail;
	}

	public String getEventCoordinatorPhone() {
		return eventCoordinatorPhone;
	}

	public void setEventCoordinatorPhone(String eventCoordinatorPhone) {
		this.eventCoordinatorPhone = eventCoordinatorPhone;
	}

	public String getOnsiteCoordinatorName() {
		return onsiteCoordinatorName;
	}

	public void setOnsiteCoordinatorName(String onsiteCoordinatorName) {
		this.onsiteCoordinatorName = onsiteCoordinatorName;
	}

	public String getOnsiteCoordinatorEmail() {
		return onsiteCoordinatorEmail;
	}

	public void setOnsiteCoordinatorEmail(String onsiteCoordinatorEmail) {
		this.onsiteCoordinatorEmail = onsiteCoordinatorEmail;
	}

	public String getOnsiteCoordinatorPhone() {
		return onsiteCoordinatorPhone;
	}

	public void setOnsiteCoordinatorPhone(String onsiteCoordinatorPhone) {
		this.onsiteCoordinatorPhone = onsiteCoordinatorPhone;
	}

	public int getConferenceId() {
		return conferenceId;
	}

	public void setConferenceId(int conferenceId) {
		this.conferenceId = conferenceId;
	}

	public int getLogisticId() {
		return logisticId;
	}

	public void setLogisticId(int logisticId) {
		this.logisticId = logisticId;
	}

	public int getNomineeId() {
		return nomineeId;
	}

	public void setNomineeId(int nomineeId) {
		this.nomineeId = nomineeId;
	}

	public String getAddLogisticDetails() {
		return addLogisticDetails;
	}

	public void setAddLogisticDetails(String addLogisticDetails) {
		this.addLogisticDetails = addLogisticDetails;
	}

	public String getDepartureflightName() {
		return departureflightName;
	}

	public void setDepartureflightName(String departureflightName) {
		this.departureflightName = departureflightName;
	}

	public String getDepartureCity() {
		return departureCity;
	}

	public void setDepartureCity(String departureCity) {
		this.departureCity = departureCity;
	}

	public String getFlyDepartureDate() {
		return flyDepartureDate;
	}

	public void setFlyDepartureDate(String flyDepartureDate) {
		this.flyDepartureDate = flyDepartureDate;
	}

	public String getFlyDepartureTime() {
		return flyDepartureTime;
	}

	public void setFlyDepartureTime(String flyDepartureTime) {
		this.flyDepartureTime = flyDepartureTime;
	}

	public String getFlyDepartureMidDayFrom() {
		return flyDepartureMidDayFrom;
	}

	public void setFlyDepartureMidDayFrom(String flyDepartureMidDayFrom) {
		this.flyDepartureMidDayFrom = flyDepartureMidDayFrom;
	}

	public String getArrivalflightName() {
		return arrivalflightName;
	}

	public void setArrivalflightName(String arrivalflightName) {
		this.arrivalflightName = arrivalflightName;
	}

	public String getArrivalCity() {
		return arrivalCity;
	}

	public void setArrivalCity(String arrivalCity) {
		this.arrivalCity = arrivalCity;
	}

	public String getFlyArrivalDate() {
		return flyArrivalDate;
	}

	public void setFlyArrivalDate(String flyArrivalDate) {
		this.flyArrivalDate = flyArrivalDate;
	}

	public String getFlyArrivalTime() {
		return flyArrivalTime;
	}

	public void setFlyArrivalTime(String flyArrivalTime) {
		this.flyArrivalTime = flyArrivalTime;
	}

	public String getFlyArrivalMidDayFrom() {
		return flyArrivalMidDayFrom;
	}

	public void setFlyArrivalMidDayFrom(String flyArrivalMidDayFrom) {
		this.flyArrivalMidDayFrom = flyArrivalMidDayFrom;
	}

	public String getHotelName() {
		return hotelName;
	}

	public void setHotelName(String hotelName) {
		this.hotelName = hotelName;
	}

	public String getHotelAddress() {
		return hotelAddress;
	}

	public void setHotelAddress(String hotelAddress) {
		this.hotelAddress = hotelAddress;
	}

	public String getArrivalTimeZone() {
		return arrivalTimeZone;
	}

	public void setArrivalTimeZone(String arrivalTimeZone) {
		this.arrivalTimeZone = arrivalTimeZone;
	}

	public String getDepartureTimeZone() {
		return departureTimeZone;
	}

	public void setDepartureTimeZone(String departureTimeZone) {
		this.departureTimeZone = departureTimeZone;
	}

	public File getFileTransport() {
		return fileTransport;
	}

	public void setFileTransport(File fileTransport) {
		this.fileTransport = fileTransport;
	}

	public String getFileTransportFileName() {
		return fileTransportFileName;
	}

	public void setFileTransportFileName(String fileTransportFileName) {
		this.fileTransportFileName = fileTransportFileName;
	}

	public String getFileTransportFileContentType() {
		return fileTransportFileContentType;
	}

	public void setFileTransportFileContentType(String fileTransportFileContentType) {
		this.fileTransportFileContentType = fileTransportFileContentType;
	}

	public String getTransportAttachmentLocation() {
		return transportAttachmentLocation;
	}

	public void setTransportAttachmentLocation(String transportAttachmentLocation) {
		this.transportAttachmentLocation = transportAttachmentLocation;
	}

	public File getFileHotel() {
		return fileHotel;
	}

	public void setFileHotel(File fileHotel) {
		this.fileHotel = fileHotel;
	}

	public String getFileHotelFileName() {
		return fileHotelFileName;
	}

	public void setFileHotelFileName(String fileHotelFileName) {
		this.fileHotelFileName = fileHotelFileName;
	}

	public String getFileHotelFileContentType() {
		return fileHotelFileContentType;
	}

	public void setFileHotelFileContentType(String fileHotelFileContentType) {
		this.fileHotelFileContentType = fileHotelFileContentType;
	}

	public String getHotelAttachmentLocation() {
		return hotelAttachmentLocation;
	}

	public void setHotelAttachmentLocation(String hotelAttachmentLocation) {
		this.hotelAttachmentLocation = hotelAttachmentLocation;
	}

	public File getFileFlight() {
		return fileFlight;
	}

	public void setFileFlight(File fileFlight) {
		this.fileFlight = fileFlight;
	}

	public String getFileFlightFileName() {
		return fileFlightFileName;
	}

	public void setFileFlightFileName(String fileFlightFileName) {
		this.fileFlightFileName = fileFlightFileName;
	}

	public String getFilFlightFileContentType() {
		return filFlightFileContentType;
	}

	public void setFilFlightFileContentType(String filFlightFileContentType) {
		this.filFlightFileContentType = filFlightFileContentType;
	}

	public String getFlightAttachmentLocation() {
		return flightAttachmentLocation;
	}

	public void setFlightAttachmentLocation(String flightAttachmentLocation) {
		this.flightAttachmentLocation = flightAttachmentLocation;
	}

	public String getTransportDetails() {
		return transportDetails;
	}

	public void setTransportDetails(String transportDetails) {
		this.transportDetails = transportDetails;
	}

	public String getLogisticDetails() {
		return logisticDetails;
	}

	public void setLogisticDetails(String logisticDetails) {
		this.logisticDetails = logisticDetails;
	}

	public int getAttendanceLogId() {
		return attendanceLogId;
	}

	public void setAttendanceLogId(int attendanceLogId) {
		this.attendanceLogId = attendanceLogId;
	}

	public String getMeetingstartDate() {
		return meetingstartDate;
	}

	public void setMeetingstartDate(String meetingstartDate) {
		this.meetingstartDate = meetingstartDate;
	}

	public String getMeetingendDate() {
		return meetingendDate;
	}

	public void setMeetingendDate(String meetingendDate) {
		this.meetingendDate = meetingendDate;
	}

	public String getReportBasedOn() {
		return reportBasedOn;
	}

	public void setReportBasedOn(String reportBasedOn) {
		this.reportBasedOn = reportBasedOn;
	}

	public String getProjstartDate() {
		return projstartDate;
	}

	public void setProjstartDate(String projstartDate) {
		this.projstartDate = projstartDate;
	}

}
