/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mss.mirage.ajaxnew;

import com.mss.mirage.util.ServiceLocatorException;
import java.sql.Timestamp;
import javax.servlet.http.HttpServletRequest;

import org.json.JSONObject;

import java.util.List;
import java.util.Map;

/**
 *
 * @author miracle
 */
public interface NewAjaxHandlerService {

	public String getMcertRecordsList(String startDate, String toDate, String status) throws ServiceLocatorException;

	public String mcertRecordStatusUpdate(String consultantIds, String loginId, String status, String examNameIdList)
			throws ServiceLocatorException;

	public String getMcertQuestion(int questionNo, HttpServletRequest httpServletRequest, int selectedAns,
			String navigation, int remainingQuestions, int onClickStatus, int subTopicId, int specficQuestionNo)
			throws ServiceLocatorException;

	public String getMcertDetailExamInfo(String examKeyId) throws ServiceLocatorException;

	public String searchPreSalesRequirementList(HttpServletRequest httpServletRequest,
			NewAjaxHandlerAction ajaxHandlerAction) throws ServiceLocatorException;

	public String getRequirementOtherDetails(int reqId) throws ServiceLocatorException;

	public String searchPreSalesMyRequirementList(HttpServletRequest httpServletRequest,
			NewAjaxHandlerAction ajaxHandlerAction) throws ServiceLocatorException;

	public String doPopulateAccountDetails(int accId) throws ServiceLocatorException;

	public String popupContactsWindow(String empId) throws ServiceLocatorException;

	public String getEmployeeProjectDetailsBasedOnStatus(String projectId, int accountId, String empType, int empId,
			HttpServletRequest httpServletRequest) throws ServiceLocatorException;

	public String getProjectStatusById(String projectId) throws ServiceLocatorException;

	public String getProjectProtfolioReport(String customerName, String startDate, String orderBy, String status,
			String projectType, HttpServletRequest httpServletRequest) throws ServiceLocatorException;

	public String getProjectDescriptionDetails(int prjId) throws ServiceLocatorException;

	public String getProjectCommentDetails(int prjId) throws ServiceLocatorException;

	public String getPSCERDetailsForPresales(HttpServletRequest httpServletRequest,
			NewAjaxHandlerAction ajaxHandlerAction) throws ServiceLocatorException;

	public String getRequestInSightDetails(int leadId) throws ServiceLocatorException;

	public String getPSCERDetailsForEmployee(HttpServletRequest httpServletRequest,
			NewAjaxHandlerAction ajaxHandlerAction) throws ServiceLocatorException;

	public String getPSCERDetailsForSales(HttpServletRequest httpServletRequest, NewAjaxHandlerAction ajaxHandlerAction)
			throws ServiceLocatorException;

	public String getDomainByExamType(int flag);

	public String ajaxQuestionsFileUpload(NewAjaxHandlerAction ajaxHandlerAction);

	public String getAllFilesInDirectory(HttpServletRequest httpServletRequest, NewAjaxHandlerAction ajaxHandlerAction)
			throws ServiceLocatorException;

	public String getAllFilesInImagesDirectory(HttpServletRequest httpServletRequest,
			NewAjaxHandlerAction ajaxHandlerAction) throws ServiceLocatorException;

	public String getBDMStatistics(String bdmId, String startDate, String endDate) throws ServiceLocatorException;

	public String getBdmStatisticsDetailsByLoginId(String bdmId, String startDate, String endDate, String activityType)
			throws ServiceLocatorException;

	public String getActivityContacts(String actitvityId) throws ServiceLocatorException;

	public String getCustomerContacts(String customerName, String teamMemberId, String startDate, String endDate,
			int teamMemberCheck, String titleType) throws ServiceLocatorException;

	public String getContactActivities(String customerId, String teamMemberId, String startDate, String endDate,
			int teamMemberCheck, String titleType) throws ServiceLocatorException;

	public String reqOverviewPieChart(HttpServletRequest httpServletRequest, NewAjaxHandlerAction aThis);

	public String getRequirementOverviewDetails(HttpServletRequest httpServletRequest, NewAjaxHandlerAction aThis);

	public String getInsideSalesStatusList(String bdeLoginId) throws ServiceLocatorException;

	public String getInsideSalesAccountDetailsList(HttpServletRequest httpServletRequest,
			NewAjaxHandlerAction ajaxHandlerAction) throws ServiceLocatorException;

	public String getInvestmentType(int invId) throws ServiceLocatorException;

	public String addLeadHystory(String loginId, Timestamp remainderDate, String followUpComments,
			String nextfollowUpComments, int leadId, String opt, int id, int reminderFlag)
			throws ServiceLocatorException;

	public String getLeadFollowUpHistoryDetails(int id) throws ServiceLocatorException;

	public String customerProjectDetailsDashboard(NewAjaxHandlerAction aThis, HttpServletRequest httpServletRequest)
			throws ServiceLocatorException;

	public String getResourceTypeDetailsByCustomer(int accId, String projectId, HttpServletRequest httpServletRequest)
			throws ServiceLocatorException;

	public String getResourceTypeDetailsByProject(int accId, String projectId, HttpServletRequest httpServletRequest)
			throws ServiceLocatorException;

	public String getProjectListByCustomer(NewAjaxHandlerAction newAjaxHandlerAction,
			HttpServletRequest httpServletRequest) throws ServiceLocatorException;

	public String getTaskNotes(int notesId) throws ServiceLocatorException;

	/*
	 * task dashboard methods
	 */
	public String getIssuesDashBoardByStatus(String taskStartDate, String taskEndDate, String reportsTo, int graphId,
			HttpServletRequest httpServletRequest) throws ServiceLocatorException;

	public String getTaskListByStatus(String taskStartDate, String taskEndDate, String reportsTo, String activityType,
			int graphId, HttpServletRequest httpServletRequest) throws ServiceLocatorException;

	public String doPopulateTaskDetails(int taskId) throws ServiceLocatorException;

	public int doSetQReviewManagerComments(int Id, String comments) throws ServiceLocatorException;

	public int doSetQReviewPersonalityComments(int Id, String strengthsComments, String improvementsComments,
			String strengths, String improvements) throws ServiceLocatorException;

	public int doSetQReviewGoalsComments(int Id, String shortTermGoalComments, String longTermGoalComments,
			String shortTermGoal, String longTermGoal) throws ServiceLocatorException;

	public String empQuarterlyAppraisalSave(NewAjaxHandlerAction newAjaxHandlerAction,
			HttpServletRequest httpServletRequest) throws ServiceLocatorException;

	public String quarterlyAppraisalEdit(int empId, int appraisalId, int lineId) throws ServiceLocatorException;

	public int setQReviewEmpDescriptions(String jsonData) throws ServiceLocatorException;

	public int doAddQReviewHrDescriptions(String jsonData) throws ServiceLocatorException;

	public int doSetQReviewHrComments(int Id, String comments) throws ServiceLocatorException;

	public int doSetQReviewPersonalityHrComments(int Id, String strengthsComments, String improvementsComments)
			throws ServiceLocatorException;

	public int doSetQReviewGoalsHrComments(int Id, String shortTermGoalComments, String longTermGoalComments)
			throws ServiceLocatorException;

	public String getBdmReport(NewAjaxHandlerAction newAjaxHandlerAction, HttpServletRequest httpServletRequest)
			throws ServiceLocatorException;

	public String getSalesTeamforBDMAssociate(String salesName) throws ServiceLocatorException;

	public String getSalesTeamDetails(String teamMemberId, String bdmId) throws ServiceLocatorException;

	public int addSalesToBdm(NewAjaxHandlerAction newAjaxHandlerAction) throws ServiceLocatorException;

	public boolean checkSalesAgainstBdm(NewAjaxHandlerAction newAjaxHandlerAction) throws ServiceLocatorException;

	public String checkBdmAddedName(NewAjaxHandlerAction newAjaxHandlerAction) throws ServiceLocatorException;

	public boolean updateBdmTeam(NewAjaxHandlerAction newAjaxHandlerAction) throws ServiceLocatorException;

	public boolean checkBdmStatus(NewAjaxHandlerAction newAjaxHandlerAction) throws ServiceLocatorException;

	/*
	 * Author :Teja Kadamanti Date : 04/20/2017 Description: Multiple Projects
	 * Employee Details
	 * 
	 * 
	 */
	public String getMultipleProjectsEmployeeList(NewAjaxHandlerAction newAjaxHandlerAction,
			HttpServletRequest httpServletRequest) throws ServiceLocatorException;

	public String multipleProjectsEmployeeListDetails(int preAssignedEmpId, HttpServletRequest httpServletRequest)
			throws ServiceLocatorException;

	public String getConsultantNames(String query) throws ServiceLocatorException;

	/*
	 * Author : Phani Kanuri Date : 05/09/2017
	 * 
	 */
	public String getRespectiveVideoTitleMap(String accesType) throws ServiceLocatorException;

	/*
	 * Author : Teja Kadamanti Date : 05/10/2017
	 * 
	 */
	public String getMobileAppDetails(NewAjaxHandlerAction newAjaxHandlerAction, HttpServletRequest httpServletRequest)
			throws ServiceLocatorException;

	/*
	 * Author : Phani Kanuri Date : 05/10/2017
	 * 
	 */
	public String getQReviewDashBoardByStatus(String year, String quarter, HttpServletRequest httpServletRequest)
			throws ServiceLocatorException;

	public String getPracticeVsStatusStackChart(String year, String quarter, HttpServletRequest httpServletRequest)
			throws ServiceLocatorException;

	public String getClosedVsQuarterStackChart(String year, String quarter, HttpServletRequest httpServletRequest)
			throws ServiceLocatorException;

	public String getQReviewListByStatus(String qReviewYear, String qReviewQuarter, String activityType,
			HttpServletRequest httpServletRequest) throws ServiceLocatorException;

	public String getQReviewListByStatusVsPractice(String qReviewYear, String qReviewQuarter, String activityType,
			String columnLabel, HttpServletRequest httpServletRequest) throws ServiceLocatorException;

	public String getQReviewListByStatusVsQuarter(String qReviewYear, String qReviewQuarter, String activityType,
			String columnLabel, HttpServletRequest httpServletRequest) throws ServiceLocatorException;

	public String getQReviewListByManagers(NewAjaxHandlerAction ajaxHandlerAction,
			HttpServletRequest httpServletRequest) throws ServiceLocatorException;

	// public String avaiableEmployeBySubPracticeList() throws
	// ServiceLocatorException;
	public String avaiableEmployeBySubPracticeList(String practiceName) throws ServiceLocatorException;

	public String getEmployeesForMngrByApprasailStatus(String year, String quarter, String loginId, String status,
			String country, String isIncludeTeam, HttpServletRequest httpServletRequest) throws ServiceLocatorException;

	public String getOffshoreEmployeeDetails(String practiceName, String subPractice, String status)
			throws ServiceLocatorException;

	/* Star Performer Related Methods start */
	public String starPerformers(NewAjaxHandlerAction ajaxHandlerAction) throws ServiceLocatorException;

	public String doUpdateStarPerformer(String objectId, String Status) throws ServiceLocatorException;

	public boolean checkStarDetails(NewAjaxHandlerAction ajaxHandlerAction) throws ServiceLocatorException;

	public int addStarDetails(NewAjaxHandlerAction ajaxHandlerAction) throws ServiceLocatorException;

	public String dogetNomineesList(String objectId, String statusFlag, String accessFlag, String loginId, Map teamMap)
			throws ServiceLocatorException;

	public String doSubmitForApproval(NewAjaxHandlerAction ajaxHandlerAction) throws ServiceLocatorException;

	public String getEmployeeDetailStarList(String resourceName, String loginId, Map teamMap)
			throws ServiceLocatorException;

	public int doSMCampaignIntegration(String objectId, String campaignStartDate, String month, String year)
			throws ServiceLocatorException;

	public int doRSVPIntegration(String objectId, String year, String month, int survyId, String movieDates,
			String expireDate) throws ServiceLocatorException;

	public String doEmpMovieTicketList(String objectId, int survyId, List emailList) throws ServiceLocatorException;

	public int doFeedBackIntegration(String objectId, String year, String month, int survyId, List emailList)
			throws ServiceLocatorException;

	public String getProjectRiskDetails(NewAjaxHandlerAction ajaxHandlerAction) throws ServiceLocatorException;

	public String getEmployeePerformance(NewAjaxHandlerAction ajaxHandlerAction) throws ServiceLocatorException;

	public String getOnProjectEmployeeUtilizationDetails(String empId) throws ServiceLocatorException;

	public String getReasonForExit(String empId) throws ServiceLocatorException;

	public String getTimeSheettotalhoursbyProjectList(NewAjaxHandlerAction ajaxhandleraction)
			throws ServiceLocatorException;

	public String gettotalweekcountbyprojectDetails(NewAjaxHandlerAction ajaxhandleraction)
			throws ServiceLocatorException;

	public String getDateAndTotalHoursByProject(NewAjaxHandlerAction ajaxhandleraction) throws ServiceLocatorException;

	/* LookUp Management Changes */
	public String getlookUpReport(NewAjaxHandlerAction newAjaxHandlerAction, HttpServletRequest httpServletRequest)
			throws ServiceLocatorException;

	public int addColumnNames(NewAjaxHandlerAction newAjaxHandlerAction, HttpServletRequest httpServletRequest)
			throws ServiceLocatorException;

	public String getLookUpDetails(String lookUpId, String tableName) throws ServiceLocatorException;

	public boolean updateColumnNames(NewAjaxHandlerAction newAjaxHandlerAction, HttpServletRequest httpServletRequest)
			throws ServiceLocatorException;

	public String getLookUpTableDetails(String query) throws ServiceLocatorException;

	public String popupTenurStatus(NewAjaxHandlerAction ajaxHandlerAction) throws ServiceLocatorException;

	public String doGetExperienceReport(NewAjaxHandlerAction ajaxHandlerAction) throws ServiceLocatorException;

	public String getCountVsStatusReport(int graphId, String country, String departmentId, String practiceId,
			String subpractice, String availableUtl, HttpServletRequest httpServletRequest)
			throws ServiceLocatorException;

	public String getResourcesByStatusList(String activityType, int graphId, String country, String departmentId,
			String practiceId, String subpractice, String availableUtl, HttpServletRequest httpServletRequest)
			throws ServiceLocatorException;

	public String getUtilizationDetails(String empId, HttpServletRequest httpServletRequest)
			throws ServiceLocatorException;

	public String getEmployeeVerificationDetails(HttpServletRequest httpServletRequest, int infoId) throws Exception;

	/* Release Notes Start */
	public String popupSubTitleDescWindow(int releaseId) throws ServiceLocatorException;

	public String popupPurposeDescWindow(int releaseId) throws ServiceLocatorException;

	public String getReleaseNotesData(NewAjaxHandlerAction newAjaxHandlerAction, HttpServletRequest httpServletRequest)
			throws ServiceLocatorException;

	public String getReleaseNotesMonthData(NewAjaxHandlerAction newAjaxHandlerAction,
			HttpServletRequest httpServletRequest) throws ServiceLocatorException;

	public String getReleaseNotesTitleInfo(NewAjaxHandlerAction newAjaxHandlerAction,
			HttpServletRequest httpServletRequest) throws ServiceLocatorException;

	public String getReleaseNotesInfoData(NewAjaxHandlerAction newAjaxHandlerAction,
			HttpServletRequest httpServletRequest) throws ServiceLocatorException;
	/* Release Notes End */

	/* Time sheet audit start */
	public String doGetCustomerDetailsByCostModel(String costModel, String pmo) throws ServiceLocatorException;

	public String doGetEmployeeForTimeSheetHrs(String empName) throws ServiceLocatorException;

	public String doGetLoadForAudit(String jsonData) throws ServiceLocatorException;

	public String doSaveInvoiceDetails(String invoiceDetails, String createdBy) throws ServiceLocatorException;

	public String doGetLoadTimeSheetDataForAudit(String jsonData) throws ServiceLocatorException;

	public String doGetTimeSheetAuditSearch(String jsonData, String pmo) throws ServiceLocatorException;

	public String doUpdateAuditComment(String jsonData, String createdBy) throws ServiceLocatorException;
	/* Time sheet audit end */

	public String doGetEmployeeNames(String string, int empId) throws ServiceLocatorException;

	public String getEmpCountryCount() throws ServiceLocatorException;

	public String getEmpPracticeCount(String country, String category) throws ServiceLocatorException;

	// public String getEmpStatePieChart(String country, String category,String
	// practiceId, String subPractice) throws ServiceLocatorException;

	public String getEmpSubPracticeCount(String country, String category, String practiceId)
			throws ServiceLocatorException;

	public String getEmployeeDetailsThroughState(String state, int graphId, String country, String departmentId,
			String practiceId, String subpractice) throws ServiceLocatorException;

	public String getEmpCurStatePieChart(int graphId, String country, String departmentId, String practiceId,
			String subpractice) throws ServiceLocatorException;

	public String getUtilizationDetails(String empId) throws ServiceLocatorException;

	public String getEmpSalesOppertunitiesPieChart(String country, String category, String practiceId,
			String subPractice) throws ServiceLocatorException;

	public String getEmpSalesRequirementPieChart(String country, String category, String practiceId, String subPractice)
			throws ServiceLocatorException;

	public String getSalesAccStatusPieChart(int graphId) throws ServiceLocatorException;

	public String getSalesAccRegionPieChart(int graphId) throws ServiceLocatorException;

	public String getSalesAccTerritoryPieChart(int graphId) throws ServiceLocatorException;

	public String getSalesAccStatePieChart(int graphId) throws ServiceLocatorException;

	public String getBiometricLoad(String empId, int month, int year, String reportType) throws ServiceLocatorException;

	public String getBioAttendanceDetails(String empId, String biometricDate, String status)
			throws ServiceLocatorException;

	public String calculateAttendance(String empId, NewAjaxHandlerAction payrollAjaxHandlerAction);

	public String updateAttendanceLoad(String empId, NewAjaxHandlerAction payrollAjaxHandlerAction)
			throws ServiceLocatorException;

	public String addSynAttendance(NewAjaxHandlerAction payrollAjaxHandlerAction, int empId, int isManager, int isAdmin)
			throws ServiceLocatorException;

	public String getEmpNameByLocation(String locationId) throws ServiceLocatorException;

	public String syncAttendance(String locationId, String empnameById, int month, int year, int syncBy)
			throws ServiceLocatorException;

	public String getEmpFullNames(String empName, int empId, int isManager, int isAdmin) throws ServiceLocatorException;

	public String getEmpNameandId(String empNo) throws ServiceLocatorException;

	public int addHolidays(NewAjaxHandlerAction newAjaxHandlerAction) throws ServiceLocatorException;

	public String getEmpHolidaysList(NewAjaxHandlerAction newAjaxHandlerAction) throws ServiceLocatorException;
	/*
	 * public String getElasticBiometricLoad(String empId, int month, int year,
	 * String reportType) throws ServiceLocatorException;
	 * 
	 * 
	 * public String updateElasticAttendanceLoad(String empId, NewAjaxHandlerAction
	 * newAjaxHandlerAction) throws ServiceLocatorException;
	 */
	/* old executive dashboard */

	public String getEmployeeDetailsStackChart() throws ServiceLocatorException;

	public String getRequirementDetailsStackChart(int pastMonths) throws ServiceLocatorException;

	public String getEmployeeDetailsThroughCount(String country, String category) throws ServiceLocatorException;

	public String getRequirementDetailsFromStack(int pastMonths, String country, String status)
			throws ServiceLocatorException;

	public String getGreenSheetCountStackChart(int pastMonths, boolean external) throws ServiceLocatorException;

	public String getOpportunitiesCountsStackChart(int pastMonths) throws ServiceLocatorException;

	public String getProjectsByAccountIdPmo(int accountId, String userId, String department)
			throws ServiceLocatorException;

	public String getEmployeesByProjectIdPmo(String projectId, String userId, String accountId)
			throws ServiceLocatorException;

	public String getEmployeesByCustomerIdPmo(int accountId, String userId) throws ServiceLocatorException;

	public String getTimeSheetStatusReportDetailsList(NewAjaxHandlerAction ajaxhandleraction, String userId)
			throws ServiceLocatorException;

	public String getEmpProjectExcelReport(NewAjaxHandlerAction ajaxHandlerAction) throws ServiceLocatorException;

	public String getRequirementAjaxListVP(HttpServletRequest httpServletRequest) throws ServiceLocatorException;

	public String getSearchRequirementAjaxListVP(HttpServletRequest httpServletRequest,
			NewAjaxHandlerAction newAjaxHandlerAction) throws ServiceLocatorException;

	public String getStarResourceComments(int starId) throws ServiceLocatorException;

	public String getEmployeeCertificationDetails(String loginId, String startDate, String endDate)
			throws ServiceLocatorException;

	public String getReviewDescription(int reviewId) throws ServiceLocatorException;

	public String getPracticeStatusReport(String practiceId, int shadowFlag, String department, String orderBy)
			throws ServiceLocatorException;

	public String getShadowsUtilizationReport(String accId) throws ServiceLocatorException;

	public String getShadowsUtilizationReportByProject(String accId, String projectId, String status, String type,
			String costModel) throws ServiceLocatorException;

	public String getResourceTotal(String practiceId, String department, String state) throws ServiceLocatorException;

	public String getReviewListBasedOnActivityType(String activityType) throws ServiceLocatorException;

	public String getReviewSalesStatusReport(NewAjaxHandlerAction ajaxHandlerAction,
			HttpServletRequest httpServletRequest) throws ServiceLocatorException;

	public String getActivitySummeryByAccount(NewAjaxHandlerAction ajaxHandlerAction,
			HttpServletRequest httpServletRequest) throws ServiceLocatorException;

	public String accountSummaryOfBdmReport(NewAjaxHandlerAction ajaxHandlerAction,
			HttpServletRequest httpServletRequest) throws ServiceLocatorException;

	public String requirementSummaryOfBdmReport(NewAjaxHandlerAction ajaxHandlerAction,
			HttpServletRequest httpServletRequest) throws ServiceLocatorException;

	public String getOffshoreStrengthDetails(NewAjaxHandlerAction ajaxHandlerAction,
			HttpServletRequest httpServletRequest) throws ServiceLocatorException;

	public String getCustomerProjectRollInRollOffDetails(NewAjaxHandlerAction ajaxHandlerAction,
			HttpServletRequest httpServletRequest) throws ServiceLocatorException;

	public String activitySummeryDetailsByAccount(NewAjaxHandlerAction ajaxHandlerAction,
			HttpServletRequest httpServletRequest) throws ServiceLocatorException;

	public String offshoreDeliveryStrength(NewAjaxHandlerAction ajaxHandlerAction,
			HttpServletRequest httpServletRequest) throws ServiceLocatorException;

	public String getEmployeeDetailsByLoginId(String loginid) throws ServiceLocatorException;

	public String doGetEmployeeNamesForMcon(String query) throws ServiceLocatorException;

	public String getLinkedInProfile(int contactId) throws ServiceLocatorException;

	public String getActivityComments(int id, String flag) throws ServiceLocatorException;

	/* E certification changes */
	public String setFlagStatus(NewAjaxHandlerAction ajaxHandlerAction, String key,
			HttpServletRequest httpServletRequest) throws ServiceLocatorException;

	public String getRemainingTime(NewAjaxHandlerAction ajaxHandlerAction, String key) throws ServiceLocatorException;

	public String getEcertCount(NewAjaxHandlerAction ajaxHandlerAction, String userId) throws ServiceLocatorException;

	public String getCreRemainingTime(NewAjaxHandlerAction ajaxHandlerAction, String userId)
			throws ServiceLocatorException;

	public String setCreFlagStatus(NewAjaxHandlerAction ajaxHandlerAction, String userId,
			HttpServletRequest httpServletRequest) throws ServiceLocatorException;

	public String getCreCount(NewAjaxHandlerAction ajaxHandlerAction, String userId) throws ServiceLocatorException;

	public String getQReviewPendingListByManagers(NewAjaxHandlerAction ajaxHandlerAction,
			HttpServletRequest httpServletRequest, String empId) throws ServiceLocatorException;
	/* e certification changes end */

	public String doGetLoadForPMOReview(String jsonData, String userId, int isAdmin) throws ServiceLocatorException;

	public String saveWeeklyPMOReviews(String jsonData, String userId) throws ServiceLocatorException;

	public String updateWeeklyPMOReviews(String jsonData, String userId) throws ServiceLocatorException;

	public String getPMOWeeklyReportSearch(String jsonData, String userId, int isAdmin) throws ServiceLocatorException;

	public String updateWeeklyReviewComment(String jsonData, String createdBy) throws ServiceLocatorException;

	public String getWeeksForGivenMonth(String jsonData) throws ServiceLocatorException;

	public String getReviewSalesStatusReview(NewAjaxHandlerAction ajaxHandlerAction,
			HttpServletRequest httpServletRequest) throws ServiceLocatorException;

	public String getEmpDetailsForBioMetric(String jsonData, HttpServletRequest httpServletRequest)
			throws ServiceLocatorException;

	public String getLunchBoxData(String jsonData) throws ServiceLocatorException;

	public String updateLunchBoxStatus(String jsonData) throws ServiceLocatorException;

	public String getRollInRollOffAnalysisDetails(NewAjaxHandlerAction ajaxHandlerAction,
			HttpServletRequest httpServletRequest) throws ServiceLocatorException;

	public String getRollInRollOffAnalysisDetailsPractice(NewAjaxHandlerAction ajaxHandlerAction,
			HttpServletRequest httpServletRequest) throws ServiceLocatorException;

	public String getProjectResourcesRollInRollOffReportDetails(NewAjaxHandlerAction ajaxHandlerAction,
			HttpServletRequest httpServletRequest) throws ServiceLocatorException;

	public String getConferenceCallsAndF2FVisits(NewAjaxHandlerAction ajaxHandlerAction,
			HttpServletRequest httpServletRequest) throws ServiceLocatorException;

	public String getTeamMemberByTitleType(String titleType) throws ServiceLocatorException;

	public String popupConferenceCallsWindow(NewAjaxHandlerAction ajaxHandlerAction,
			HttpServletRequest httpServletRequest) throws ServiceLocatorException;

	public String popupF2FvisitsWindow(NewAjaxHandlerAction ajaxHandlerAction, HttpServletRequest httpServletRequest)
			throws ServiceLocatorException;

	public String getSalesTeamMemberByCountry(String country) throws ServiceLocatorException;
	
	public String getImmigrationDetails(String status) throws ServiceLocatorException;
	
	public String popuppassportDetailsWindow(NewAjaxHandlerAction ajaxHandlerAction, HttpServletRequest httpServletRequest)
			throws ServiceLocatorException;
	
	public String addMSADatesToCustomer(NewAjaxHandlerAction ajaxHandlerAction, HttpServletRequest httpServletRequest)
			throws ServiceLocatorException;
	
	public String searchMSACustomerProjectsAjaxList(NewAjaxHandlerAction ajaxHandlerAction, HttpServletRequest httpServletRequest) throws ServiceLocatorException; 
	
}
