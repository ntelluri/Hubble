﻿/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mss.mirage.ajaxnew;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TreeMap;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import java.text.DateFormat;
import java.text.DateFormatSymbols;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.comparator.LastModifiedFileComparator;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.constantcontact.ConstantContactFactory;
import com.constantcontact.components.activities.contacts.request.AddContactsRequest;
import com.constantcontact.components.activities.contacts.request.ContactData;
import com.constantcontact.components.contacts.Contact;
import com.constantcontact.components.contacts.ContactList;
import com.constantcontact.components.emailcampaigns.EmailCampaignRequest;
import com.constantcontact.components.emailcampaigns.EmailCampaignResponse;
import com.constantcontact.components.emailcampaigns.MessageFooter;
import com.constantcontact.components.emailcampaigns.SentToContactList;
import com.constantcontact.components.emailcampaigns.schedules.EmailCampaignSchedule;
import com.constantcontact.exceptions.component.ConstantContactComponentException;
import com.constantcontact.exceptions.service.ConstantContactServiceException;
import com.constantcontact.services.activities.IBulkActivitiesService;
import com.constantcontact.services.contactlists.ContactListService;
import com.constantcontact.services.emailcampaigns.EmailCampaignService;
import com.constantcontact.services.emailcampaigns.schedule.EmailCampaignScheduleService;
import com.constantcontact.util.RawApiRequestError;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mss.mirage.ecertification.QuestionsVTO;
import com.mss.mirage.employee.timesheets.BiometricPayrollElasticVTO;
import com.mss.mirage.util.ApplicationConstants;
import com.mss.mirage.util.AuthorizationManager;
import com.mss.mirage.util.ConnectionProvider;
import com.mss.mirage.util.ConstantContactMailManager;
import com.mss.mirage.util.DataSourceDataProvider;
import com.mss.mirage.util.DateUtility;
import com.mss.mirage.util.MailManager;
import com.mss.mirage.util.Properties;
import com.mss.mirage.util.RestRepository;
import com.mss.mirage.util.SecurityProperties;
import com.mss.mirage.util.ServiceLocatorException;

/*import io.searchbox.client.JestClient;
import io.searchbox.client.JestClientFactory;
import io.searchbox.client.config.HttpClientConfig;
import io.searchbox.core.Index;
import io.searchbox.core.Search;
import io.searchbox.core.SearchResult;
import io.searchbox.core.Update;
import io.searchbox.params.Parameters;
import net.sf.json.JSONSerializer;*/
import jxl.Sheet;
import jxl.Workbook;

import java.util.Calendar;
import java.sql.ResultSetMetaData;

/*
import io.searchbox.client.JestClient;
import io.searchbox.client.JestClientFactory;
import io.searchbox.client.config.HttpClientConfig;
import io.searchbox.core.Index;
import io.searchbox.core.Search;
import io.searchbox.core.SearchResult;
import io.searchbox.core.Update;
import io.searchbox.params.Parameters; */

import java.text.DecimalFormat;
import java.text.ParseException;

/**
 *
 * @author miracle
 */
public class NewAjaxHandlerServiceImpl implements NewAjaxHandlerService {
	private String queryString = "";
	/**
	 *
	 * Creating a reference variable for Connection
	 */
	private Connection connection;
	private StringBuffer stringBuffer;

	private PreparedStatement preparedStatement;
	private ResultSet resultSet;

	public String getMcertRecordsList(String startDate, String toDate, String status) throws ServiceLocatorException {
		// System.out.println("hiiii");
		StringBuffer queryString = new StringBuffer();
		String resultString = "";
		String resultString1 = "";
		String state = "";
		String salesRepTerritory = "";
		// CallableStatement callableStatement = null;
		// DataSourceDataProvider dataSourceDataProvider = null;
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			connection = ConnectionProvider.getInstance().getConnection();

			queryString.append(
					"SELECT Id, LoginId,Email,concat(FName,' ',Mname,'.',LName) as consultantName,Status FROM tblMcertConsultant WHERE 1=1 ");

			/*
			 * if(category != 0 ) {
			 * queryString.append("AND Category = "+category+" "); }
			 * if(consultantName != null && !"".equals(consultantName)) {
			 * queryString.append("AND (FName LIKE '%"
			 * +consultantName+"%' OR  LName LIKE '%"
			 * +consultantName+"%' OR MName LIKE '%"+consultantName+"%')"); }
			 */
			if ((startDate != null && !"".equals(startDate)) && (toDate != null && !"".equals(toDate))) {
				queryString.append(
						" AND datediff(CreatedDate ,'" + DateUtility.getInstance().convertStringToMySQLDate(startDate)
								+ "')>=0 and datediff(CreatedDate ,'"
								+ DateUtility.getInstance().convertStringToMySQLDate(toDate) + "')<=0");
			}
			if (status != null && !"".equals(status)) {
				queryString.append(" AND Status='" + status + "'");

			}

			queryString.append(" ORDER BY Id");

			preparedStatement = connection.prepareStatement(queryString.toString());

			resultSet = preparedStatement.executeQuery();
			String strConsId = "";
			String strConsName = "";
			String strEmail = "";
			String strCategory = "";
			String strConsLoginId = "";
			String strStaus = "";
			String qualDetails = "";
			int count = 0;
			while (resultSet.next()) {

				strConsId = strConsId + resultSet.getString("Id") + "!";
				strConsLoginId = strConsLoginId + resultSet.getString("LoginId") + "!";
				strConsName = strConsName + resultSet.getString("consultantName") + "!";
				strEmail = strEmail + resultSet.getString("Email") + "!";
				strStaus = strStaus + resultSet.getString("Status") + "!";
			}
			// resultString1 = strConsId+"@"+strConsName+"@"+strStatus;

			resultString1 = strConsId + "#^$" + strConsName + "#^$" + strEmail + "#^$" + strConsLoginId + "#^$"
					+ strStaus;

			// System.out.println("resultString1-->"+resultString1);

		} catch (Exception e) {

			throw new ServiceLocatorException(e);

		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException ex) {
				throw new ServiceLocatorException(ex);
			}
		}

		// return resultString;
		return resultString1;
	}

	public String mcertRecordStatusUpdate(String consultantIds, String loginId, String status, String examNameIdList)
			throws ServiceLocatorException {

		// System.out.println("consultantIds..." + consultantIds +
		// "examNameIdList..." + examNameIdList + "status..." + status);
		PreparedStatement preparedStatement1 = null;
		PreparedStatement preparedStatement2 = null;

		PreparedStatement preparedStatement3 = null;

		String activateRecords = "";
		String SubTopicIds = "";

		Connection connection1 = null;
		Connection connection2 = null;
		Connection connection3 = null;
		try {
			connection1 = ConnectionProvider.getInstance().getConnection();
			connection2 = ConnectionProvider.getInstance().getConnection();
			preparedStatement1 = connection1.prepareStatement(
					"UPDATE tblMcertConsExamTopics SET Status = 'InActive' WHERE McertConsultantId = ?");
			preparedStatement2 = connection2.prepareStatement(
					"UPDATE tblMcertConsultant SET STATUS =?, ModifiedBy = '" + loginId + "' , ModifiedDate = '"
							+ DateUtility.getInstance().getCurrentMySqlDateTime() + "'  WHERE Id = ?");

			// String consutantIsArray [] = consultantIds.split("!");
			if (status.equals("Active")) {
				connection3 = ConnectionProvider.getInstance().getConnection();
				// System.out.println("consultantIds-->"+consultantIds);
				String Str = examNameIdList;
				// System.out.println("Str-->"+Str);

				preparedStatement3 = connection3.prepareStatement(
						"INSERT INTO tblMcertConsExamTopics(McertConsultantId,Status,Examtypeid) VALUES(?,?,?)");
				// for(int i=0;i<consutantIsArray.length;i++) {
				for (String cerID : consultantIds.split("!")) {
					// System.out.println("cerID-->"+cerID);

					preparedStatement1.setInt(1, Integer.parseInt(cerID));
					preparedStatement1.executeUpdate();

					preparedStatement2.setString(1, "Active");
					preparedStatement2.setInt(2, Integer.parseInt(cerID));
					preparedStatement2.executeUpdate();

					for (String retval : Str.split(",")) {
						// System.out.println("retval-->"+retval);

						preparedStatement3.setInt(1, Integer.parseInt(cerID));
						preparedStatement3.setString(2, "Active");
						preparedStatement3.setInt(3, Integer.parseInt(retval));
						preparedStatement3.executeUpdate();
					}

					activateRecords = activateRecords + " " + cerID;
				}
			} else {
				// preparedStatement = connection.prepareStatement("UPDATE
				// tblCreConsultentDetails SET STATUS =?, ModifiedBy =
				// '"+loginId+"' , ModifiedDate =
				// '"+DateUtility.getInstance().getCurrentMySqlDateTime()+"'
				// WHERE Id = ?");
				// preparedStatement2 = connection2.prepareStatement("UPDATE
				// tblCreConsExamTopics SET Status = 'InActive' WHERE CreId = ?
				// ");
				// for(int i=0;i<consutantIsArray.length;i++) {
				// System.out.println("status-->"+status);
				for (String cerID : consultantIds.split("!")) {

					preparedStatement1.setInt(1, Integer.parseInt(cerID));
					preparedStatement1.executeUpdate();

					preparedStatement2.setString(1, status);
					preparedStatement2.setInt(2, Integer.parseInt(cerID));
					preparedStatement2.executeUpdate();

					activateRecords = activateRecords + " " + cerID;
				}
			}

		} catch (Exception sqe) {
			sqe.printStackTrace();
		} finally {
			try {

				if (preparedStatement3 != null) {
					preparedStatement3.close();
					preparedStatement3 = null;
				}
				if (preparedStatement2 != null) {
					preparedStatement2.close();
					preparedStatement2 = null;
				}
				if (preparedStatement1 != null) {
					preparedStatement1.close();
					preparedStatement1 = null;
				}
				if (connection3 != null) {
					connection3.close();
					connection3 = null;
				}
				if (connection2 != null) {
					connection2.close();
					connection2 = null;
				}
				if (connection1 != null) {
					connection1.close();
					connection1 = null;
				}
			} catch (SQLException sqle) {
				sqle.printStackTrace();
			}
		}

		return activateRecords;
	}

	public String getMcertQuestion(int questionNo, HttpServletRequest httpServletRequest, int selectedAns,
			String navigation, int remainingQuestions, int onClickStatus, int subTopicId, int specficQuestionNo)
			throws ServiceLocatorException {
		// System.out.println("in getMcertQuestion impl ......" + questionNo +
		// "selectedAns.." + selectedAns);

		StringBuffer stringBuffer = new StringBuffer();
		// QuestionsVTO questionVTO = null,nextQuestionVTO =
		// null,previousQuestionVTO = null,startQuestionVTO = null;
		QuestionsVTO questionVTO = null, nextQuestionVTO = null, previousQuestionVTO = null, specificQuestionVTO = null,
				startQuestionVTO = null;
		int empId = 0, examKeyId = 0, answer = 0, attemptedQuestions = 0, questionId = 0;

		try {
			empId = Integer.parseInt(
					httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_EMP_ID).toString());
			examKeyId = Integer.parseInt(httpServletRequest.getSession(false)
					.getAttribute(ApplicationConstants.MCERT_CURRENT_EXAM_KEY).toString());
			Map questionVtoMap = (Map) httpServletRequest.getSession(false)
					.getAttribute(ApplicationConstants.MCERT_QUESTIONS_MAP);
			/* In start Exam Start */
			if (navigation.equalsIgnoreCase("I")) {
				int qId = 0;
				/*
				 * if(onClickStatus != 0 ){ questionVTO =
				 * (QuestionsVTO)questionVtoMap.get(questionNo); qId =
				 * questionVTO.getId();
				 * 
				 * attemptedQuestions =
				 * DataSourceDataProvider.getInstance().isQuestionAttempt(
				 * examKeyId,qId); }
				 */

				/** Display Question */
				startQuestionVTO = (QuestionsVTO) questionVtoMap.get(1);
				int startQId = startQuestionVTO.getId();

				int mapsize = questionVtoMap.size();
				remainingQuestions = mapsize;

				/** XML start **/
				int startQuestionNo = 1;
				stringBuffer.append("<xml version=\"1.0\">");
				stringBuffer.append("<QUESTIONDETAILS >");
				stringBuffer.append("<QUESTIONSTATUS>true</QUESTIONSTATUS>");
				stringBuffer.append("<QUESTIONID>" + startQId + "</QUESTIONID>");
				stringBuffer.append("<QUESTIONNAME><![CDATA[" + startQuestionVTO.getQuestion() + "]]></QUESTIONNAME>");
				stringBuffer.append("<OPTION1><![CDATA[" + startQuestionVTO.getOption1() + "]]></OPTION1>");
				stringBuffer.append("<OPTION2><![CDATA[" + startQuestionVTO.getOption2() + "]]></OPTION2>");
				stringBuffer.append("<OPTION3><![CDATA[" + startQuestionVTO.getOption3() + "]]></OPTION3>");
				stringBuffer.append("<OPTION4><![CDATA[" + startQuestionVTO.getOption4() + "]]></OPTION4>");

				stringBuffer.append("<MAPQUESTIONID>" + startQuestionNo + "</MAPQUESTIONID>");
				stringBuffer.append("<EMPANSWER>" + answer + "</EMPANSWER>");
				stringBuffer.append("<SUBTOPICID>" + startQuestionVTO.getSubtopicId() + "</SUBTOPICID>");
				stringBuffer.append("<REMAININGQUESTIONS>" + remainingQuestions + "</REMAININGQUESTIONS>");
				if (!"".equals(startQuestionVTO.getSubTopicName())) {
					stringBuffer.append("<SECTION>" + startQuestionVTO.getSubTopicName() + "</SECTION>");
				}
				stringBuffer.append("</QUESTIONDETAILS>");
				stringBuffer.append("</xml>");

			} /* In start exam end */ /*
										 * Getting Specific Question start
										 */ else if (navigation.equalsIgnoreCase("R")) {
				int qId = 0;
				/* Insert Question into db */
				// if(onClickStatus != 0 ){
				questionVTO = (QuestionsVTO) questionVtoMap.get(questionNo);
				qId = questionVTO.getId();
				/** answered by user or not */
				attemptedQuestions = DataSourceDataProvider.getInstance().isMcertQuestionAttempt(examKeyId, qId);
				// }
				/** Display Question */
				// nextQuestionVTO =
				// (QuestionsVTO)questionVtoMap.get(questionNo+1);
				// System.out.println("specficQuestionNo--->"+specficQuestionNo);
				specificQuestionVTO = (QuestionsVTO) questionVtoMap.get(specficQuestionNo);
				// int nextQId = nextQuestionVTO.getId();
				int specificQId = specificQuestionVTO.getId();
				// System.out.println("specificQId--->"+specificQId);
				// if(onClickStatus == 0 ){
				// int mapsize = questionVtoMap.size();
				// remainingQuestions = mapsize;
				// }

				if (selectedAns != 0) {
					if (attemptedQuestions == 0) {
						insertAnswer(qId, selectedAns, empId, examKeyId, subTopicId);
						remainingQuestions = remainingQuestions - 1;
					} else {

						updateAnswer(qId, selectedAns, empId, examKeyId);
					}
				}
				// System.out.println("before getting ANSWER
				// specificQId-->"+specificQId);

				// if(questionNo < questionVtoMap.size()){
				answer = DataSourceDataProvider.getInstance().getMcertAnswer(examKeyId, specificQId, empId);
				// }
				// System.out.println("Answer-->"+answer);

				/** XML start **/
				// int specificQuestionNo = questionNo + 1;
				stringBuffer.append("<xml version=\"1.0\">");
				stringBuffer.append("<QUESTIONDETAILS >");
				stringBuffer.append("<QUESTIONSTATUS>true</QUESTIONSTATUS>");
				stringBuffer.append("<QUESTIONID>" + specificQId + "</QUESTIONID>");
				stringBuffer
						.append("<QUESTIONNAME><![CDATA[" + specificQuestionVTO.getQuestion() + "]]></QUESTIONNAME>");
				stringBuffer.append("<OPTION1><![CDATA[" + specificQuestionVTO.getOption1() + "]]></OPTION1>");
				stringBuffer.append("<OPTION2><![CDATA[" + specificQuestionVTO.getOption2() + "]]></OPTION2>");
				stringBuffer.append("<OPTION3><![CDATA[" + specificQuestionVTO.getOption3() + "]]></OPTION3>");
				stringBuffer.append("<OPTION4><![CDATA[" + specificQuestionVTO.getOption4() + "]]></OPTION4>");

				stringBuffer.append("<MAPQUESTIONID>" + specficQuestionNo + "</MAPQUESTIONID>");
				stringBuffer.append("<EMPANSWER>" + answer + "</EMPANSWER>");
				stringBuffer.append("<SUBTOPICID>" + specificQuestionVTO.getSubtopicId() + "</SUBTOPICID>");
				stringBuffer.append("<REMAININGQUESTIONS>" + remainingQuestions + "</REMAININGQUESTIONS>");
				if (!"".equals(specificQuestionVTO.getSubTopicName())) {
					stringBuffer.append("<SECTION>" + specificQuestionVTO.getSubTopicName() + "</SECTION>");
				}
				stringBuffer.append("</QUESTIONDETAILS>");
				stringBuffer.append("</xml>");

			} /*
				 * 
				 * Getting Specific Question end
				 */ /** in Next if */
			else if (navigation.equalsIgnoreCase("N")) {
				int qId = 0;
				nextQuestionVTO = (QuestionsVTO) questionVtoMap.get(questionNo + 1);
				int nextQId = nextQuestionVTO.getId();
				// System.out.println(nextQId);
				/* Insert Question into db */
				// if(onClickStatus != 0 ){
				questionVTO = (QuestionsVTO) questionVtoMap.get(questionNo);
				qId = questionVTO.getId();
				/** answered by user or not */
				attemptedQuestions = DataSourceDataProvider.getInstance().isMcertQuestionAttempt(examKeyId, qId);
				// }
				/** Display Question */
				// else {
				// int mapsize = questionVtoMap.size();
				// remainingQuestions = mapsize;
				// }
				if (selectedAns != 0) {
					if (attemptedQuestions == 0) {
						insertAnswer(qId, selectedAns, empId, examKeyId, subTopicId);
						remainingQuestions = remainingQuestions - 1;
					} else {
						updateAnswer(qId, selectedAns, empId, examKeyId);
					}
				}

				// if(questionNo < questionVtoMap.size()){
				answer = DataSourceDataProvider.getInstance().getMcertAnswer(examKeyId, nextQId, empId);
				// }

				/** XML start **/
				int nextQuestionNo = questionNo + 1;
				stringBuffer.append("<xml version=\"1.0\">");
				stringBuffer.append("<QUESTIONDETAILS >");
				stringBuffer.append("<QUESTIONSTATUS>true</QUESTIONSTATUS>");
				stringBuffer.append("<QUESTIONID>" + nextQId + "</QUESTIONID>");
				stringBuffer.append("<QUESTIONNAME><![CDATA[" + nextQuestionVTO.getQuestion() + "]]></QUESTIONNAME>");
				stringBuffer.append("<OPTION1><![CDATA[" + nextQuestionVTO.getOption1() + "]]></OPTION1>");
				stringBuffer.append("<OPTION2><![CDATA[" + nextQuestionVTO.getOption2() + "]]></OPTION2>");
				stringBuffer.append("<OPTION3><![CDATA[" + nextQuestionVTO.getOption3() + "]]></OPTION3>");
				stringBuffer.append("<OPTION4><![CDATA[" + nextQuestionVTO.getOption4() + "]]></OPTION4>");

				stringBuffer.append("<MAPQUESTIONID>" + nextQuestionNo + "</MAPQUESTIONID>");
				stringBuffer.append("<EMPANSWER>" + answer + "</EMPANSWER>");
				stringBuffer.append("<SUBTOPICID>" + nextQuestionVTO.getSubtopicId() + "</SUBTOPICID>");
				stringBuffer.append("<REMAININGQUESTIONS>" + remainingQuestions + "</REMAININGQUESTIONS>");
				if (!"".equals(nextQuestionVTO.getSubTopicName())) {
					stringBuffer.append("<SECTION>" + nextQuestionVTO.getSubTopicName() + "</SECTION>");
				}
				stringBuffer.append("</QUESTIONDETAILS>");
				stringBuffer.append("</xml>");

			} /** End of next If */
			/* in prevoius if */ else if (navigation.equalsIgnoreCase("P")) {
				questionVTO = (QuestionsVTO) questionVtoMap.get(questionNo);
				int qId = questionVTO.getId();
				/** answered by user or not */
				attemptedQuestions = DataSourceDataProvider.getInstance().isMcertQuestionAttempt(examKeyId, qId);
				previousQuestionVTO = (QuestionsVTO) questionVtoMap.get(questionNo - 1);
				int prevoiusQId = previousQuestionVTO.getId();

				if (selectedAns != 0) {
					if (attemptedQuestions == 0) {
						insertAnswer(qId, selectedAns, empId, examKeyId, subTopicId);
						remainingQuestions = remainingQuestions - 1;
					} else {
						updateAnswer(qId, selectedAns, empId, examKeyId);
					}
				}
				answer = DataSourceDataProvider.getInstance().getMcertAnswer(examKeyId, prevoiusQId, empId);

				/** XML start **/
				int previousQuestionNo = questionNo - 1;
				stringBuffer.append("<xml version=\"1.0\">");
				stringBuffer.append("<QUESTIONDETAILS >");
				stringBuffer.append("<QUESTIONSTATUS>true</QUESTIONSTATUS>");
				stringBuffer.append("<QUESTIONID>" + prevoiusQId + "</QUESTIONID>");
				stringBuffer
						.append("<QUESTIONNAME><![CDATA[" + previousQuestionVTO.getQuestion() + "]]></QUESTIONNAME>");
				stringBuffer.append("<OPTION1><![CDATA[" + previousQuestionVTO.getOption1() + "]]></OPTION1>");
				stringBuffer.append("<OPTION2><![CDATA[" + previousQuestionVTO.getOption2() + "]]></OPTION2>");
				stringBuffer.append("<OPTION3><![CDATA[" + previousQuestionVTO.getOption3() + "]]></OPTION3>");
				stringBuffer.append("<OPTION4><![CDATA[" + previousQuestionVTO.getOption4() + "]]></OPTION4>");

				stringBuffer.append("<MAPQUESTIONID>" + previousQuestionNo + "</MAPQUESTIONID>");
				stringBuffer.append("<EMPANSWER>" + answer + "</EMPANSWER>");
				stringBuffer.append("<SUBTOPICID>" + previousQuestionVTO.getSubtopicId() + "</SUBTOPICID>");
				stringBuffer.append("<REMAININGQUESTIONS>" + remainingQuestions + "</REMAININGQUESTIONS>");
				if (!"".equals(previousQuestionVTO.getSubTopicName())) {
					stringBuffer.append("<SECTION>" + previousQuestionVTO.getSubTopicName() + "</SECTION>");
				}
				stringBuffer.append("</QUESTIONDETAILS>");
				stringBuffer.append("</xml>");

			} /* end of Previous if */ /* in submit if */ else if (navigation.equalsIgnoreCase("S")) {
				// System.out.println("in sumbmit cond..");
				questionVTO = (QuestionsVTO) questionVtoMap.get(questionNo);
				int qId = questionVTO.getId();
				/** answered by user or not */
				attemptedQuestions = DataSourceDataProvider.getInstance().isMcertQuestionAttempt(examKeyId, qId);
				/*
				 * if(onClickStatus == 0 ){ int mapsize = questionVtoMap.size();
				 * remainingQuestions = mapsize; }
				 */
				// System.out.println("attemptedQuestions-->"+attemptedQuestions);
				if (selectedAns != 0) {
					if (attemptedQuestions == 0) {
						insertAnswer(qId, selectedAns, empId, examKeyId, subTopicId);
						remainingQuestions = remainingQuestions - 1;
					} else {
						updateAnswer(qId, selectedAns, empId, examKeyId);
					}
				}
				stringBuffer.append("<xml version=\"1.0\">");
				stringBuffer.append("<QUESTIONDETAILS >");
				stringBuffer.append("<QUESTIONSTATUS>false</QUESTIONSTATUS>");
				stringBuffer.append("<REMAININGQUESTIONS>" + remainingQuestions + "</REMAININGQUESTIONS>");
				stringBuffer.append("</QUESTIONDETAILS>");
				stringBuffer.append("</xml>");
			}

			/* end of subbmit if */

		} catch (Exception sle) {
			sle.printStackTrace();
		}
		return stringBuffer.toString();

	}

	public void insertAnswer(int questionNo, int selectedAns, int empId, int examKeyId, int subTopicId)
			throws ServiceLocatorException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		int i = 0;
		try {
			connection = ConnectionProvider.getInstance().getConnection();

			preparedStatement = connection.prepareStatement(
					"INSERT INTO tblMcertSummary (EmpId,ExamKeyId,QuestionId,EmpAns,DateSubmitted,SubtopicId) VALUES(?,?,?,?,?,? )");
			preparedStatement.setInt(1, empId);
			preparedStatement.setInt(2, examKeyId);
			preparedStatement.setInt(3, questionNo);
			preparedStatement.setInt(4, selectedAns);
			preparedStatement.setTimestamp(5, DateUtility.getInstance().getCurrentMySqlDateTime());
			preparedStatement.setInt(6, subTopicId);
			i = preparedStatement.executeUpdate();
		} catch (Exception e) {
			System.err.println("Exception is-->" + e.getMessage());
		} finally {
			try {
				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (Exception sqle) {
				System.err.println("SQL Exception is-->" + sqle.getMessage());
			}
		}

	}

	public void updateAnswer(int questionNo, int selectedAns, int empId, int examKeyId) throws ServiceLocatorException {

		Connection connection = null;
		PreparedStatement preparedStatement = null;
		int i = 0;
		try {
			connection = ConnectionProvider.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(
					"UPDATE tblMcertSummary SET EmpAns=?  WHERE EmpId = ? AND ExamKeyId = ? AND QuestionId = ?");
			preparedStatement.setInt(1, selectedAns);
			preparedStatement.setInt(2, empId);
			preparedStatement.setInt(3, examKeyId);
			preparedStatement.setInt(4, questionNo);

			i = preparedStatement.executeUpdate();
		} catch (Exception e) {
			System.err.println("Exception is-->" + e.getMessage());
		} finally {
			try {
				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (Exception sqle) {
				System.err.println("SQL Exception is-->" + sqle.getMessage());
			}
		}

	}

	public String getMcertDetailExamInfo(String examKeyId) throws ServiceLocatorException {

		String subTopicDetailResult = "";
		// Connection connection = null;
		CallableStatement callableStatement = null;
		Connection connection = null;
		try {
			connection = ConnectionProvider.getInstance().getConnection();
			callableStatement = connection.prepareCall("{call spMcertResult(?,?,?,?,?,?,?,?)}");
			callableStatement.setInt(1, Integer.parseInt(examKeyId));
			callableStatement.registerOutParameter(2, Types.VARCHAR);
			callableStatement.registerOutParameter(3, Types.VARCHAR);
			callableStatement.registerOutParameter(4, Types.INTEGER);
			callableStatement.registerOutParameter(5, Types.INTEGER);
			callableStatement.registerOutParameter(6, Types.INTEGER);
			callableStatement.registerOutParameter(7, Types.VARCHAR);
			callableStatement.registerOutParameter(8, Types.VARCHAR);
			callableStatement.execute();

			subTopicDetailResult = callableStatement.getString(8);

		} catch (SQLException se) {
			se.printStackTrace();
		} finally {
			try {

				if (callableStatement != null) {
					callableStatement.close();
					callableStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		return subTopicDetailResult;
	}

	public String searchPreSalesRequirementList(HttpServletRequest httpServletRequest,
			NewAjaxHandlerAction ajaxHandlerAction) throws ServiceLocatorException {
		Connection connection = null;
		StringBuffer stringBuffer = new StringBuffer();
		CallableStatement callableStatement = null;
		PreparedStatement preparedStatement = null;
		Statement statement = null;
		Statement statement1 = null;
		ResultSet resultSet = null;
		ResultSet resultSet1 = null;
		String createdBy = "";
		String totalStream = "";
		String queryString = "";
		int i = 0;
		int totalRecords = 0;

		// resultType = "accessFailed";

		DateUtility dateUtility = new DateUtility();

		// System.out.println(" impl ---
		// createdBy----------"+ajaxHandlerAction.getCreatedBy()+"--assignedTo-------------"+ajaxHandlerAction.getAssignedTo()+"--title-------------"+ajaxHandlerAction.getTitle()+"--postedDate1-------"+ajaxHandlerAction.getPostedDate1()+"---postedDate2----"+ajaxHandlerAction.getPostedDate2()+"---status----"+ajaxHandlerAction.getStatus());

		try {
			/*
			 * queryString
			 * ="SELECT Id,ActivityType,Status,Description,DateDue,CreatedDate,AssignedToId,Comments FROM tblCrmActivity"
			 * ; queryString = queryString + " WHERE AccountId ="
			 * +accId+" AND ContactId =0 GROUP BY ActivityType,STATUS,Description,AssignedToId ORDER BY CreatedDate DESC"
			 * ;
			 */

			// System.out.println("ajaxHandlerAction.getTitle()==="+ajaxHandlerAction.getTitle());

			int columnCounter = 0;

			String territory = "";
			connection = ConnectionProvider.getInstance().getConnection();
			StringBuffer queryStringBuffer = new StringBuffer();
			;

			// queryStringBuffer.append("SELECT
			// tblRecRequirement.Id,tblCrmAccount.NAME,JobTitle,tblRecRequirement.STATUS,tblRecRequirement.CreatedBy,AssignedTo,AssignToTechLead,tblCrmAccount.Id
			// AS
			// AccountId,tblRecRequirement.Country,CONCAT(`tblRecRequirement`.`State`,',',`tblRecRequirement`.`Country`)
			// AS Location,tblRecRequirement.Location As ReqLocation
			// ,tblRecRequirement.Practice,tblRecRequirement.StartDate,tblRecRequirement.Duration,tblRecRequirement.TargetRate,tblRecRequirement.TaxTerm,tblRecRequirement.AssignedDate,COUNT(tblRec.Id)
			// AS ConsultantsCount,SUM(CASE WHEN tblRec.STATUS='Submitted to
			// Sales' THEN 1 ELSE 0 END) AS StatusCount FROM tblRecRequirement
			// LEFT OUTER JOIN tblCrmAccount ON
			// (tblRecRequirement.CustomerId=tblCrmAccount.Id) LEFT OUTER JOIN
			// tblRec ON (tblRecRequirement.Id=tblRec.RequirementId) ");
			queryStringBuffer
					.append("SELECT tblRecRequirement.Id,tblCrmAccount.NAME,JobTitle,tblRecRequirement.STATUS,tblRecRequirement.CreatedBy,AssignedTo,AssignToTechLead,tblCrmAccount.Id AS AccountId,tblRecRequirement.Country,CONCAT(`tblRecRequirement`.`State`,',',`tblRecRequirement`.`Country`) AS Location,tblRecRequirement.Location As ReqLocation ,tblRecRequirement.Practice,tblRecRequirement.StartDate,tblRecRequirement.Duration,tblRecRequirement.TargetRate,tblRecRequirement.TaxTerm,tblRecRequirement.AssignedDate,COUNT(tblRec.Id) AS ConsultantsCount,"
							+ "SUM(CASE WHEN tblRec.STATUS='Assigned' THEN 1 ELSE 0 END) AS ACount,SUM(CASE WHEN tblRec.STATUS='Tech Screen  - Phone' THEN 1 ELSE 0 END) AS TSPCount,SUM(CASE WHEN tblRec.STATUS='Tech Screen shotlisted' THEN 1 ELSE 0 END) AS TSSCount,SUM(CASE WHEN tblRec.STATUS='Tech Screen Reject' THEN 1 ELSE 0 END) AS TSRCount,SUM(CASE WHEN tblRec.STATUS='Client Submission' THEN 1 ELSE 0 END) AS CSCount,SUM(CASE WHEN tblRec.STATUS='Client Interview' THEN 1 ELSE 0 END) AS CICount,SUM(CASE WHEN tblRec.STATUS='Client Reject' THEN 1 ELSE 0 END) AS CRCount,SUM(CASE WHEN tblRec.STATUS='Joined' THEN 1 ELSE 0 END) AS JCount,SUM(CASE WHEN tblRec.STATUS='Client Interview Reject' THEN 1 ELSE 0 END) AS CIRCount,"
							+ "SUM(CASE WHEN tblRec.STATUS='Submitted to Sales' THEN 1 ELSE 0 END) AS StatusCount FROM tblRecRequirement LEFT OUTER JOIN tblCrmAccount ON (tblRecRequirement.CustomerId=tblCrmAccount.Id) LEFT OUTER JOIN tblRec ON (tblRecRequirement.Id=tblRec.RequirementId) ");
			// queryStringBuffer.append("WHERE tblRecRequirement.Country LIKE
			// '"+userWorkCountry+"' " );
			queryStringBuffer.append("WHERE 1=1  ");

			if (!"All".equals(ajaxHandlerAction.getCreatedBy())) {
				queryStringBuffer
						.append(" AND tblRecRequirement.CreatedBy='" + ajaxHandlerAction.getCreatedBy() + "' ");
			}
			if (ajaxHandlerAction.getTitle() != null && !"".equals(ajaxHandlerAction.getTitle())) {
				queryStringBuffer
						.append(" AND `tblRecRequirement`.`JobTitle` LIKE '%" + ajaxHandlerAction.getTitle() + "%'");
			}
			if (ajaxHandlerAction.getPostedDate1() != null && !"".equals(ajaxHandlerAction.getPostedDate1())) {
				queryStringBuffer.append(" AND DATE(`tblRecRequirement`.`DatePosted`) >= DATE('"
						+ DateUtility.getInstance().convertStringToMySQLDate(ajaxHandlerAction.getPostedDate1())
						+ "')");
			}

			if (ajaxHandlerAction.getPostedDate2() != null && !"".equals(ajaxHandlerAction.getPostedDate2())) {
				queryStringBuffer.append(" AND DATE(`tblRecRequirement`.`DatePosted`) <= DATE('"
						+ DateUtility.getInstance().convertStringToMySQLDate(ajaxHandlerAction.getPostedDate2())
						+ "')");
			}

			if (!"All".equals(ajaxHandlerAction.getStatus())) {
				queryStringBuffer
						.append("AND `tblRecRequirement`.`Status` LIKE '" + ajaxHandlerAction.getStatus() + "'");
			}
			if (!"-1".equals(ajaxHandlerAction.getCountry())) {
				queryStringBuffer
						.append(" AND `tblRecRequirement`.`Country` like '" + ajaxHandlerAction.getCountry() + "' ");
			}

			if (ajaxHandlerAction.getCustomerName() != null && !"".equals(ajaxHandlerAction.getCustomerName())) // queryStringBuffer.append("
																												// AND
																												// `tblCrmAccount`.`Name`
																												// like
																												// '"
																												// +
																												// ajaxHandlerAction.getCustomerName()
																												// +
																												// "'
																												// ");
			{
				queryStringBuffer
						.append(" AND `tblCrmAccount`.`Name` LIKE '%" + ajaxHandlerAction.getCustomerName() + "%'");
			}

			if (ajaxHandlerAction.getState() != null && !"".equals(ajaxHandlerAction.getState())) {
				queryStringBuffer
						.append(" AND `tblRecRequirement`.`state` like '" + ajaxHandlerAction.getState() + "' ");
			}

			if (!"-1".equals(ajaxHandlerAction.getPracticeid())) {
				queryStringBuffer
						.append("  AND tblRecRequirement.Practice like '" + ajaxHandlerAction.getPracticeid() + "' ");
			}
			if (ajaxHandlerAction.getRequirementId() != 0) {
				queryStringBuffer.append("  AND tblRecRequirement .Id =" + ajaxHandlerAction.getRequirementId() + " ");
			}
			if (ajaxHandlerAction.getPreSalesPerson() != null && !"".equals(ajaxHandlerAction.getPreSalesPerson())) {
				queryStringBuffer.append("AND (tblRecRequirement.AssignToTechLead ='"
						+ ajaxHandlerAction.getPreSalesPerson() + "' || tblRecRequirement.SecondaryTechLead ='"
						+ ajaxHandlerAction.getPreSalesPerson() + "') ");
			} else {
				Map rolesMap = (Map) httpServletRequest.getSession(false)
						.getAttribute(ApplicationConstants.SESSION_MY_ROLES);
				List finalTeachLeadList;

				if (rolesMap.containsValue("Admin") || AuthorizationManager.getInstance()
						.isAuthorizedForSurveyForm("PRESALES_REQUIREMENT_ACCESS", httpServletRequest.getSession(false)
								.getAttribute(ApplicationConstants.SESSION_USER_ID).toString())) {
					finalTeachLeadList = DataSourceDataProvider.getInstance()
							.getListFromMap(DataSourceDataProvider.getInstance().getTechLead());

				} else {
					Map teamMap = (Map) httpServletRequest.getSession(false)
							.getAttribute(ApplicationConstants.SESSION_MY_TEAM_MAP);
					// List TechLeadList =
					// DataSourceDataProvider.getInstance().getTechLead();
					List TechLeadList = DataSourceDataProvider.getInstance()
							.getListFromMap(DataSourceDataProvider.getInstance().getTechLead());
					List tempTechLeadList = new ArrayList();
					for (int j = 0; j < TechLeadList.size(); j++) {
						if (teamMap.containsValue(TechLeadList.get(j))) {
							tempTechLeadList.add(TechLeadList.get(j));
						}
					}
					// tempTechLeadList.add(httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_NAME));
					tempTechLeadList.add(
							httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID));

					finalTeachLeadList = tempTechLeadList;
				}
				String teamList = DataSourceDataProvider.getInstance().getStringByList(finalTeachLeadList);

				queryStringBuffer.append("AND (FIND_IN_SET(tblRecRequirement.AssignToTechLead,'" + teamList
						+ "' )  || FIND_IN_SET(tblRecRequirement.SecondaryTechLead,'" + teamList + "' ) )");

			}
			queryStringBuffer.append(" GROUP BY tblRecRequirement.Id ");
			queryStringBuffer.append(" ORDER BY `tblRecRequirement`.`DatePosted` DESC  LIMIT "
					+ (ajaxHandlerAction.getPgNo() - 1) * 20 + ", 20");

			// System.out.println("queryStringBuffer"+queryStringBuffer.toString());
			// for getting total counts (for pagination) ...> Start
			if ("1".equals(ajaxHandlerAction.getPgFlag().trim())) {

				StringBuffer queryStringBuf = new StringBuffer();
				;

				queryStringBuf.append(
						"Select COUNT(id) As count from (SELECT tblRecRequirement.Id As id FROM tblRecRequirement LEFT OUTER JOIN tblCrmAccount ON (tblRecRequirement.CustomerId=tblCrmAccount.Id) LEFT OUTER JOIN tblRec ON (tblRecRequirement.Id=tblRec.RequirementId) ");

				queryStringBuf.append("WHERE 1=1  ");

				if (!"All".equals(ajaxHandlerAction.getCreatedBy())) {
					queryStringBuf
							.append(" AND tblRecRequirement.CreatedBy='" + ajaxHandlerAction.getCreatedBy() + "' ");
				}
				if (ajaxHandlerAction.getTitle() != null && !"".equals(ajaxHandlerAction.getTitle())) {
					queryStringBuf.append(
							" AND `tblRecRequirement`.`JobTitle` LIKE '%" + ajaxHandlerAction.getTitle() + "%'");
				}

				if (ajaxHandlerAction.getPostedDate1() != null && !"".equals(ajaxHandlerAction.getPostedDate1())) {
					queryStringBuf.append(" AND DATE(`tblRecRequirement`.`DatePosted`) >= DATE('"
							+ DateUtility.getInstance().convertStringToMySQLDate(ajaxHandlerAction.getPostedDate1())
							+ "')");
				}

				if (ajaxHandlerAction.getPostedDate2() != null && !"".equals(ajaxHandlerAction.getPostedDate2())) {
					queryStringBuf.append(" AND DATE(`tblRecRequirement`.`DatePosted`) <= DATE('"
							+ DateUtility.getInstance().convertStringToMySQLDate(ajaxHandlerAction.getPostedDate2())
							+ "')");
				}

				if (!"All".equals(ajaxHandlerAction.getStatus())) {
					queryStringBuf
							.append("AND `tblRecRequirement`.`Status` LIKE '" + ajaxHandlerAction.getStatus() + "'");
				}
				if (!"-1".equals(ajaxHandlerAction.getCountry())) {
					queryStringBuf.append(
							" AND `tblRecRequirement`.`Country` like '" + ajaxHandlerAction.getCountry() + "' ");
				}

				if (ajaxHandlerAction.getCustomerName() != null && !"".equals(ajaxHandlerAction.getCustomerName())) {
					queryStringBuf
							.append(" AND `tblCrmAccount`.`Name` LIKE '%" + ajaxHandlerAction.getCustomerName() + "%'");
				}

				if (ajaxHandlerAction.getState() != null && !"".equals(ajaxHandlerAction.getState())) {
					queryStringBuf
							.append(" AND `tblRecRequirement`.`state` like '" + ajaxHandlerAction.getState() + "' ");
				}

				if (!"-1".equals(ajaxHandlerAction.getPracticeid())) {
					queryStringBuf.append(
							"  AND tblRecRequirement.Practice like '" + ajaxHandlerAction.getPracticeid() + "' ");
				}
				if (ajaxHandlerAction.getRequirementId() != 0) {
					queryStringBuf.append("  AND tblRecRequirement .Id =" + ajaxHandlerAction.getRequirementId() + " ");
				}
				if (ajaxHandlerAction.getPreSalesPerson() != null
						&& !"".equals(ajaxHandlerAction.getPreSalesPerson())) {
					queryStringBuf.append("AND (tblRecRequirement.AssignToTechLead ='"
							+ ajaxHandlerAction.getPreSalesPerson() + "' || tblRecRequirement.SecondaryTechLead ='"
							+ ajaxHandlerAction.getPreSalesPerson() + "') ");
				} else {
					Map rolesMap = (Map) httpServletRequest.getSession(false)
							.getAttribute(ApplicationConstants.SESSION_MY_ROLES);
					List finalTeachLeadList;
					if (rolesMap.containsValue("Admin") || AuthorizationManager.getInstance()
							.isAuthorizedForSurveyForm("PRESALES_REQUIREMENT_ACCESS", httpServletRequest
									.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID).toString())) {

						// finalTeachLeadList =
						// DataSourceDataProvider.getInstance().getTechLead();
						finalTeachLeadList = DataSourceDataProvider.getInstance()
								.getListFromMap(DataSourceDataProvider.getInstance().getTechLead());
					} else {
						Map teamMap = (Map) httpServletRequest.getSession(false)
								.getAttribute(ApplicationConstants.SESSION_MY_TEAM_MAP);
						// List TechLeadList =
						// DataSourceDataProvider.getInstance().getTechLead();
						List TechLeadList = DataSourceDataProvider.getInstance()
								.getListFromMap(DataSourceDataProvider.getInstance().getTechLead());
						List tempTechLeadList = new ArrayList();
						for (int j = 0; j < TechLeadList.size(); j++) {
							if (teamMap.containsValue(TechLeadList.get(j))) {
								tempTechLeadList.add(TechLeadList.get(j));
							}
						}
						// tempTechLeadList.add(httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_NAME));
						tempTechLeadList.add(httpServletRequest.getSession(false)
								.getAttribute(ApplicationConstants.SESSION_USER_ID));

						finalTeachLeadList = tempTechLeadList;
					}
					String teamList = DataSourceDataProvider.getInstance().getStringByList(finalTeachLeadList);

					queryStringBuf.append("AND (FIND_IN_SET(tblRecRequirement.AssignToTechLead,'" + teamList
							+ "' )  || FIND_IN_SET(tblRecRequirement.SecondaryTechLead,'" + teamList + "' ) )");

				}
				queryStringBuf.append(" GROUP BY tblRecRequirement.Id ");
				queryStringBuf.append(" ORDER BY `tblRecRequirement`.`DatePosted` DESC) As tbl");

				// System.out.println("REQ_SEARCH_QUERY --->" +
				// queryStringBuf.toString());

				statement1 = connection.createStatement();
				resultSet1 = statement1.executeQuery(queryStringBuf.toString());
				while (resultSet1.next()) {
					totalRecords = resultSet1.getInt("count");
				}

			}

			// for getting total counts (for pagination) ...> End

			statement = connection.createStatement();
			resultSet = statement.executeQuery(queryStringBuffer.toString());

			while (resultSet.next()) {
				int RequirementId = resultSet.getInt("Id");
				String accountName = resultSet.getString("NAME");
				String JobTitle = resultSet.getString("JobTitle");
				String status = resultSet.getString("STATUS");
				String CreatedBy = resultSet.getString("CreatedBy");
				String AccountId = resultSet.getString("AccountId");
				// AccountId

				String Recruiter = "-";
				if (resultSet.getString("AssignedTo") != null && !"".equals(resultSet.getString("AssignedTo"))) {
					Recruiter = resultSet.getString("AssignedTo");
				}

				String PreSales = "-";
				if (resultSet.getString("AssignToTechLead") != null
						&& !"".equals(resultSet.getString("AssignToTechLead"))) {
					PreSales = resultSet.getString("AssignToTechLead");
				}

				String Location = "-";
				if (resultSet.getString("Location") != null && !"".equals(resultSet.getString("Location"))) {
					Location = resultSet.getString("Location");
				}

				String Practice = "-";
				if (resultSet.getString("Practice") != null && !"".equals(resultSet.getString("Practice"))) {
					Practice = resultSet.getString("Practice");
				}

				String StartDate = "-";
				if (resultSet.getString("StartDate") != null && !"".equals(resultSet.getString("StartDate"))) {
					StartDate = resultSet.getString("StartDate");
				}

				String Duration = "-";
				if (resultSet.getString("Duration") != null && !"".equals(resultSet.getString("Duration"))) {
					Duration = resultSet.getString("Duration");
				}

				String TargetRate = "-";
				if (resultSet.getString("TargetRate") != null && !"".equals(resultSet.getString("TargetRate"))) {
					TargetRate = resultSet.getString("TargetRate");
				}

				String TaxTerm = "-";
				if (resultSet.getString("TaxTerm") != null && !"".equals(resultSet.getString("TaxTerm").trim())
						&& !"-1".equals(resultSet.getString("TaxTerm").trim())) {
					// System.out.println("resultSet.getString" +
					// resultSet.getString("TaxTerm"));
					TaxTerm = resultSet.getString("TaxTerm");
				}
				String AssignedDate = "-";
				if (resultSet.getString("AssignedDate") != null && !"".equals(resultSet.getString("AssignedDate"))) {
					AssignedDate = resultSet.getString("AssignedDate");
				}

				String ConsultantsCount = "-";
				if (resultSet.getString("ConsultantsCount") != null
						&& !"".equals(resultSet.getString("ConsultantsCount"))) {
					ConsultantsCount = resultSet.getString("ConsultantsCount");
				}
				String ACount = "-";
				if (resultSet.getString("ACount") != null && !"".equals(resultSet.getString("ACount"))) {
					ACount = resultSet.getString("ACount");
				}
				String TSPCount = "-";
				if (resultSet.getString("TSPCount") != null && !"".equals(resultSet.getString("TSPCount"))) {
					TSPCount = resultSet.getString("TSPCount");
				}
				String TSSCount = "-";
				if (resultSet.getString("TSSCount") != null && !"".equals(resultSet.getString("TSSCount"))) {
					TSSCount = resultSet.getString("TSSCount");
				}
				String TSRCount = "-";
				if (resultSet.getString("TSRCount") != null && !"".equals(resultSet.getString("TSRCount"))) {
					TSRCount = resultSet.getString("TSRCount");
				}
				String CSCount = "-";
				if (resultSet.getString("CSCount") != null && !"".equals(resultSet.getString("CSCount"))) {
					CSCount = resultSet.getString("CSCount");
				}
				String CICount = "-";
				if (resultSet.getString("CICount") != null && !"".equals(resultSet.getString("CICount"))) {
					CICount = resultSet.getString("CICount");
				}
				String CRCount = "-";
				if (resultSet.getString("CRCount") != null && !"".equals(resultSet.getString("CRCount"))) {
					CRCount = resultSet.getString("CRCount");
				}
				String JCount = "-";
				if (resultSet.getString("JCount") != null && !"".equals(resultSet.getString("JCount"))) {
					JCount = resultSet.getString("JCount");
				}
				String CIRCount = "-";
				if (resultSet.getString("CIRCount") != null && !"".equals(resultSet.getString("CIRCount"))) {
					CIRCount = resultSet.getString("CIRCount");
				}
				String StatusCount = "-";
				if (resultSet.getString("StatusCount") != null && !"".equals(resultSet.getString("StatusCount"))) {
					StatusCount = resultSet.getString("StatusCount");
				}

				String ReqLocation = "-";
				if (resultSet.getString("ReqLocation") != null && !"".equals(resultSet.getString("ReqLocation"))) {

					if ("1".equals(resultSet.getString("ReqLocation"))) {
						ReqLocation = "Onsite";
					}
					if ("2".equals(resultSet.getString("ReqLocation"))) {
						ReqLocation = "Off Site";
					}
					if ("3".equals(resultSet.getString("ReqLocation"))) {
						ReqLocation = "Off Shore";
					}

				}

				i++;
				// totalStream = totalStream + i + "#^$" + RequirementId + "#^$"
				// + accountName + "#^$" + JobTitle + "#^$" + status + "#^$" +
				// CreatedBy + "#^$" + Recruiter + "#^$" + PreSales + "#^$" +
				// AccountId + "#^$" + Location + "#^$" + ReqLocation + "#^$" +
				// Practice + "#^$" + StartDate + "#^$" + AssignedDate + "#^$" +
				// Duration + "#^$" + TargetRate + "#^$" + TaxTerm + "#^$" +
				// ConsultantsCount + "#^$" + StatusCount + "*@!";
				totalStream = totalStream + i + "#^$" + RequirementId + "#^$" + accountName + "#^$" + JobTitle + "#^$"
						+ status + "#^$" + CreatedBy + "#^$" + Recruiter + "#^$" + PreSales + "#^$" + AccountId + "#^$"
						+ Location + "#^$" + ReqLocation + "#^$" + Practice + "#^$" + StartDate + "#^$" + AssignedDate
						+ "#^$" + Duration + "#^$" + TargetRate + "#^$" + TaxTerm + "#^$" + ConsultantsCount + "#^$"
						+ ACount + "#^$" + TSPCount + "#^$" + TSSCount + "#^$" + TSRCount + "#^$" + CSCount + "#^$"
						+ CICount + "#^$" + CRCount + "#^$" + JCount + "#^$" + CIRCount + "#^$" + StatusCount + "*@!";
			}
			stringBuffer.append(totalStream + "###" + totalRecords);

			// System.out.println(i+"totalRecords...."+totalRecords);
		} catch (Exception sqe) {
			sqe.printStackTrace();
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (statement != null) {
					statement.close();
					statement = null;
				}
				if (resultSet1 != null) {
					resultSet1.close();
					resultSet1 = null;
				}
				if (statement1 != null) {
					statement1.close();
					statement1 = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException sqle) {
			}
		}

		return stringBuffer.toString();
	}

	public String searchPreSalesMyRequirementList(HttpServletRequest httpServletRequest,
			NewAjaxHandlerAction ajaxHandlerAction) throws ServiceLocatorException {
		Connection connection = null;
		StringBuffer stringBuffer = new StringBuffer();
		CallableStatement callableStatement = null;
		PreparedStatement preparedStatement = null;
		Statement statement = null;
		ResultSet resultSet = null;
		String createdBy = "";
		String totalStream = "";
		String queryString = "";
		int i = 0;

		// resultType = "accessFailed";

		DateUtility dateUtility = new DateUtility();

		// System.out.println(" impl ---
		// createdBy----------"+ajaxHandlerAction.getCreatedBy()+"--assignedTo-------------"+ajaxHandlerAction.getAssignedTo()+"--title-------------"+ajaxHandlerAction.getTitle()+"--postedDate1-------"+ajaxHandlerAction.getPostedDate1()+"---postedDate2----"+ajaxHandlerAction.getPostedDate2()+"---status----"+ajaxHandlerAction.getStatus());

		try {
			/*
			 * queryString
			 * ="SELECT Id,ActivityType,Status,Description,DateDue,CreatedDate,AssignedToId,Comments FROM tblCrmActivity"
			 * ; queryString = queryString + " WHERE AccountId ="
			 * +accId+" AND ContactId =0 GROUP BY ActivityType,STATUS,Description,AssignedToId ORDER BY CreatedDate DESC"
			 * ;
			 */

			// System.out.println("ajaxHandlerAction.getTitle()==="+ajaxHandlerAction.getTitle());

			int columnCounter = 0;

			String territory = "";
			StringBuffer queryStringBuffer = new StringBuffer();
			;

			queryStringBuffer.append(
					"SELECT tblRecRequirement.Id,tblCrmAccount.NAME,JobTitle,tblRecRequirement.STATUS,CONCAT(`tblRecRequirement`.`State`,',',`tblRecRequirement`.`Country`) AS Location,tblRecRequirement.AssignedDate as AssignedDate,tblRecRequirement.NoResumes AS noofresumes,CONCAT(FName,' ',MName,'.',LName) AS CreatedBy,AssignedTo,tblCrmAccount.Id AS AccountId,tblRecRequirement.CreatedBy AS SalesPersonLoginId FROM tblRecRequirement LEFT OUTER JOIN tblCrmAccount ON (tblRecRequirement.CustomerId=tblCrmAccount.Id)  LEFT OUTER JOIN tblEmployee ON (tblRecRequirement.CreatedBy=tblEmployee.LoginId)  ");

			// queryStringBuffer.append("WHERE tblRecRequirement.Country LIKE
			// '"+userWorkCountry+"' " );
			queryStringBuffer.append("WHERE 1=1  ");
			if (ajaxHandlerAction.getAssignedBy() != null && !"All".equals(ajaxHandlerAction.getAssignedBy())) {
				queryStringBuffer
						.append(" AND tblRecRequirement.AssignedBy='" + ajaxHandlerAction.getAssignedBy() + "' ");
			}

			if (ajaxHandlerAction.getAssignedTo() != null && !"All".equals(ajaxHandlerAction.getAssignedTo())) {

				queryStringBuffer
						.append(" AND tblRecRequirement.AssignedTo='" + ajaxHandlerAction.getAssignedTo() + "' ");
			}

			if (ajaxHandlerAction.getCreatedBy() != null && !"All".equals(ajaxHandlerAction.getCreatedBy())) {
				queryStringBuffer
						.append(" AND tblRecRequirement.CreatedBy='" + ajaxHandlerAction.getCreatedBy() + "' ");
			}
			if (ajaxHandlerAction.getTitle() != null && !"".equals(ajaxHandlerAction.getTitle())) {
				queryStringBuffer
						.append("`tblRecRequirement`.`JobTitle` LIKE '%" + ajaxHandlerAction.getTitle() + "%'");
			}

			if (!"".equalsIgnoreCase(ajaxHandlerAction.getPostedDate1())) {
				ajaxHandlerAction.setPostedDate1(
						DateUtility.getInstance().convertStringToMySQLDate(ajaxHandlerAction.getPostedDate1()));
			}

			if (!"".equalsIgnoreCase(ajaxHandlerAction.getPostedDate2())) {
				ajaxHandlerAction.setPostedDate2(
						DateUtility.getInstance().convertStringToMySQLDate(ajaxHandlerAction.getPostedDate2()));
			}
			if (ajaxHandlerAction.getPostedDate1() != null && !"".equals(ajaxHandlerAction.getPostedDate1())) {
				queryStringBuffer.append("DATE(`tblRecRequirement`.`DatePosted`) >= DATE('"
						+ DateUtility.getInstance().convertStringToMySQLDate(ajaxHandlerAction.getPostedDate1())
						+ "')");
			}

			if (ajaxHandlerAction.getPostedDate2() != null && !"".equals(ajaxHandlerAction.getPostedDate2())) {
				queryStringBuffer.append("DATE(`tblRecRequirement`.`DatePosted`) <= DATE('"
						+ DateUtility.getInstance().convertStringToMySQLDate(ajaxHandlerAction.getPostedDate2())
						+ "')");
			}

			if (!"All".equals(ajaxHandlerAction.getStatus())) {
				queryStringBuffer
						.append("AND `tblRecRequirement`.`Status` LIKE '" + ajaxHandlerAction.getStatus() + "'");
			}
			if (!"-1".equals(ajaxHandlerAction.getCountry())) {
				queryStringBuffer
						.append(" AND `tblRecRequirement`.`Country` like '" + ajaxHandlerAction.getCountry() + "' ");
			}

			if (ajaxHandlerAction.getState() != null && !"".equals(ajaxHandlerAction.getState())) {
				queryStringBuffer
						.append(" AND `tblRecRequirement`.`state` like '" + ajaxHandlerAction.getState() + "' ");
			}

			if (!"-1".equals(ajaxHandlerAction.getPracticeid())) {
				queryStringBuffer
						.append("  AND tblRecRequirement.Practice like '" + ajaxHandlerAction.getPracticeid() + "' ");
			}
			if (ajaxHandlerAction.getRequirementId() != 0) {
				queryStringBuffer.append("  AND tblRecRequirement .Id =" + ajaxHandlerAction.getRequirementId() + " ");
			}

			// queryStringBuffer.append(" AND
			// (tblRecRequirement.AssignToTechLead ='" +
			// httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_NAME)
			// + "' || tblRecRequirement.SecondaryTechLead ='" +
			// httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_NAME)
			// + "') ");
			queryStringBuffer.append(" AND (tblRecRequirement.AssignToTechLead ='"
					+ httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID)
					+ "' || tblRecRequirement.SecondaryTechLead ='"
					+ httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) + "') ");

			queryStringBuffer.append(" ORDER BY `tblRecRequirement`.`DatePosted` DESC Limit 100");

			// System.out.println("REQ_SEARCH_QUERY
			// --->"+queryStringBuffer.toString());
			/*
			 *
			 * End of where
			 */

			System.out.println("Search Query ---" + queryStringBuffer.toString());

			connection = ConnectionProvider.getInstance().getConnection();
			statement = connection.createStatement();
			resultSet = statement.executeQuery(queryStringBuffer.toString());

			// System.err.println("Account Activities:"+queryString);
			while (resultSet.next()) {
				// tblRecRequirement.Id,tblCrmAccount.NAME,JobTitle,tblRecRequirement.STATUS,tblRecRequirement.CreatedBy,AssignedTo,AssignToTechLead

				int RequirementId = resultSet.getInt("Id");
				String accountName = resultSet.getString("NAME");
				String JobTitle = resultSet.getString("JobTitle");
				String status = resultSet.getString("STATUS");
				String Location = resultSet.getString("Location");
				String AssignedDate = resultSet.getString("AssignedDate");
				String noOfPos = resultSet.getString("noofresumes");
				String CreatedBy = resultSet.getString("CreatedBy");

				String AccountId = resultSet.getString("AccountId");
				// AccountId

				String Recruiter = "-";
				if (resultSet.getString("AssignedTo") != null || resultSet.getString("AssignedTo") != "") {
					Recruiter = resultSet.getString("AssignedTo");
				}

				String SalesPersonLoginId = resultSet.getString("SalesPersonLoginId");

				// #^$
				// *@!

				i++;
				/*
				 * createdBy=resultSet.getString("CreatedById"); count
				 * =resultSet.getInt("total");
				 */
				// totalStream=totalStream+i+"|"+createdDate+"|"+actType+"|"+description+"|"+comments+"|"+assignedToId+"|"+status+"|"+datedue+"|"+contactId+"|"+accountId+"|"+"^";
				// totalStream=totalStream+i+"|"+createdDate+"|"+actType+"|"+description+"|"+comments+"|"+assignedToId+"|"+status+"|"+datedue+"|"+"^";
				totalStream = totalStream + i + "#^$" + RequirementId + "#^$" + accountName + "#^$" + JobTitle + "#^$"
						+ status + "#^$" + Location + "#^$" + AssignedDate + "#^$" + noOfPos + "#^$" + CreatedBy + "#^$"
						+ Recruiter + "#^$" + AccountId + "#^$" + SalesPersonLoginId + "*@!";
				// totalActivities=totalActivities+count;
			}
			stringBuffer.append(totalStream);
			stringBuffer.append("addto");

			stringBuffer.append(i);

		} catch (Exception sqe) {
			sqe.printStackTrace();
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (statement != null) {
					statement.close();
					statement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException sqle) {
			}
		}
		// System.err.println("response string is"+stringBuffer.toString());
		return stringBuffer.toString();
	}

	public String doPopulateAccountDetails(int accId) throws ServiceLocatorException {
		StringBuffer reportsToBuffer = new StringBuffer("");
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		Connection connection = null;

		try {
			connection = ConnectionProvider.getInstance().getConnection();

			String query = "Select Revenues,NoOfEmployees from tblCrmAccount where Id=" + accId;

			preparedStatement = connection.prepareStatement(query);
			resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {

				reportsToBuffer.append(resultSet.getString("Revenues") + "@#" + resultSet.getString("NoOfEmployees"));

			}

		} catch (SQLException sle) {
			sle.printStackTrace();
		} catch (ServiceLocatorException sle) {
			sle.printStackTrace();
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
		}
		// System.out.println("Team: "+reportsToBuffer.toString());
		return reportsToBuffer.toString();
	}

	public String popupContactsWindow(String empId) throws ServiceLocatorException {
		String phoneNo = null;
		String emailaddr = null;
		String empDetails = null;
		String reportsTo = null;
		String location = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		Connection connection = null;
		try {
			connection = ConnectionProvider.getInstance().getConnection();
			// SELECT Comments FROM tblEmpIssues WHERE Id=810
			preparedStatement = connection.prepareStatement(
					"SELECT emp1.Email1,emp1.WorkPhoneNo,emp1.Location,CONCAT(emp2.fname,' ',emp2.lname) AS ReportsTo FROM tblEmployee AS emp1 LEFT OUTER JOIN tblEmployee emp2 ON (emp2.loginId=emp1.ReportsTo) WHERE emp1.Id =?");
			preparedStatement.setInt(1, Integer.parseInt(empId));
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				emailaddr = resultSet.getString("Email1");
				phoneNo = resultSet.getString("WorkPhoneNo");
				reportsTo = resultSet.getString("ReportsTo");
				location = resultSet.getString("Location");
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		} catch (ServiceLocatorException sle) {
			sle.printStackTrace();
		} finally {
			try {

				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
		}

		// System.out.println("reportsTo"+reportsTo);
		empDetails = reportsTo + "^" + location + "^" + emailaddr + "^" + phoneNo;

		// System.out.println("empDetails"+empDetails);
		return empDetails;

	}

	public String getEmployeeProjectDetailsBasedOnStatus(String projectId, int accountId, String empType, int empId,
			HttpServletRequest httpServletRequest) throws ServiceLocatorException {
		// String qsTitle = "";
		StringBuffer stringBuffer = new StringBuffer();
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;
		String query = "";
		DateUtility dateUtil;
		dateUtil = DateUtility.getInstance();
		String response = "";
		String actStartDate = "";
		String actEndDate = "";
		try {
			connection = ConnectionProvider.getInstance().getConnection();
			query = "SELECT Id,ObjectType,Email,Billable,StartDate,EndDate,STATUS,ResourceType,ResourceTitle,Utilization,RateType,Rate,ReportsTo,SecondReportTo,Comments,SkillSet FROM tblProjectContacts WHERE ObjectId='"
					+ empId + "' AND ProjectId='" + projectId + "' AND AccountId='" + accountId
					+ "' AND STATUS='Active'";
			statement = connection.createStatement();
			resultSet = statement.executeQuery(query);
			while (resultSet.next()) {

				if (resultSet.getString("StartDate") != null) {
					String startDate = dateUtil.sqlTimeStampTousTimeStamp(resultSet.getString("StartDate"));
					StringTokenizer st = new StringTokenizer(startDate, " ");
					actStartDate = st.nextToken();
				}
				if (resultSet.getString("EndDate") != null) {
					String endDate = dateUtil.sqlTimeStampTousTimeStamp(resultSet.getString("EndDate"));
					StringTokenizer st = new StringTokenizer(endDate, " ");
					actEndDate = st.nextToken();
				}
				response = response + resultSet.getInt("Id") + "#^$" + resultSet.getString("ObjectType") + "#^$"
						+ resultSet.getString("Email") + "#^$" + resultSet.getBoolean("Billable") + "#^$" + actStartDate
						+ "#^$" + actEndDate + "#^$" + resultSet.getString("STATUS") + "#^$"
						+ resultSet.getBoolean("ResourceType") + "#^$" + resultSet.getBoolean("ResourceTitle") + "#^$"
						+ resultSet.getInt("Utilization") + "#^$" + resultSet.getString("RateType") + "#^$"
						+ resultSet.getString("Rate") + "#^$" + resultSet.getInt("ReportsTo") + "#^$"
						+ resultSet.getInt("SecondReportTo") + "#^$" + resultSet.getString("Comments") + "#^$"
						+ resultSet.getString("SkillSet");
			}
			if ("".equals(response)) {

				response = "StatusNotActive";
			}

		} catch (Exception sqe) {
			sqe.printStackTrace();
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (statement != null) {
					statement.close();
					statement = null;
				}

				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException sqle) {
				sqle.printStackTrace();
			}
		}

		return response;
	}

	public String getProjectStatusById(String projectId) throws ServiceLocatorException {

		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		Connection connection = null;
		String response = "";
		try {
			connection = ConnectionProvider.getInstance().getConnection();
			// SELECT Comments FROM tblEmpIssues WHERE Id=810
			preparedStatement = connection
					.prepareStatement("Select Status,ProjectStartDate,ProjectEndDate from tblProjects where Id=?");
			preparedStatement.setInt(1, Integer.parseInt(projectId));
			resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				String startDate = "-", endDate = "-";
				if (resultSet.getString("ProjectStartDate") != null) {
					// startDate = resultSet.getString("ProjectStartDate");
					startDate = DateUtility.getInstance().convertDateToView(resultSet.getDate("ProjectStartDate"));
				}

				if (resultSet.getString("ProjectEndDate") != null) {
					endDate = DateUtility.getInstance().convertDateToView(resultSet.getDate("ProjectEndDate"));
				}

				response = resultSet.getString("Status") + "#^$" + startDate + "#^$" + endDate;
			}

		} catch (SQLException ex) {
			ex.printStackTrace();
		} catch (ServiceLocatorException sle) {
			sle.printStackTrace();
		} finally {
			try {

				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
		}

		return response;
	}

	public String getProjectProtfolioReport(String customerName, String startDate, String orderBy, String status,
			String projectType, HttpServletRequest httpServletRequest) throws ServiceLocatorException {

		// System.out.println("getProjectProtfolioReport() in
		// NewAjaxHandlerServiceImpl");
		Connection connection = null;
		StringBuffer stringBuffer = new StringBuffer();
		PreparedStatement preparedStatement = null;
		Statement statement = null;
		ResultSet resultSet = null;
		DataSourceDataProvider dataSourceDataProvider = null;

		String responseString = "";

		int i = 0;
		// System.err.println(days+"Diff in Dyas...");
		try {
			String loginId = httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID)
					.toString();

			//
			// System.out.println("customerName---->"+customerName);
			// System.out.println("startDate---->"+startDate);
			// System.out.println("orderBy---->"+orderBy);
			// System.out.println("status---->"+status);
			StringBuffer queryStringBuffer = new StringBuffer();
			;
			// queryString = "SELECT Id,CONCAT(FName,'.',LName) AS
			// employeeName,ReportsTo, TitleTypeId,Email1,WorkPhoneNo FROM
			// tblEmployee WHERE CurStatus='Active' AND LoginId IN (" +
			// TeamLoginIdList + ") ";

			queryStringBuffer.append(
					"SELECT tblProjects.Id,tblCrmAccount.NAME,tblProjects.ProjectName,tblProjects.CostModel,tblProjects.Sector,tblProjects.Practice,tblProjects.ProjectType,tblProjects.ProjectStartDate,tblProjects.ProjectEndDate,tblProjects.EndDatePlan,tblProjects.State,tblProjects.Software,tblProjects.ProjectDescription,tblProjects.Comments FROM tblCrmAccount LEFT OUTER JOIN tblProjects ON (tblCrmAccount.Id=tblProjects.CustomerId)");

			// queryString = queryString + " AND Country like '" + country + "'
			// ORDER BY TRIM(ReportsTo),TRIM(employeeName) LIMIT 250";
			// queryStringBuffer.append("WHERE 1=1 ");
			Map rolesMap = (Map) httpServletRequest.getSession(false)
					.getAttribute(ApplicationConstants.SESSION_MY_ROLES);
			if (rolesMap.containsValue("Admin") || httpServletRequest.getSession(false)
					.getAttribute(ApplicationConstants.SESSION_PMO_ACTIVITY_ACCESS).toString().equals("1")) {
				queryStringBuffer.append("WHERE 1=1 ");
			} else {
				if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_IS_TEAM_LEAD)
						.toString().equals("1")
						|| httpServletRequest.getSession(false)
								.getAttribute(ApplicationConstants.SESSION_IS_USER_MANAGER).toString().equals("1")) {
					Map teamMap = (Map) httpServletRequest.getSession(false)
							.getAttribute(ApplicationConstants.SESSION_MY_TEAM_MAP);
					// teamMap.put(loginId,
					// httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_NAME).toString());
					String TeamLoginIdList = DataSourceDataProvider.getInstance().getTeamLoginIdList(teamMap);
					// TeamLoginIdList = TeamLoginIdList.replaceAll("\\'", "");
					TeamLoginIdList = TeamLoginIdList + ",'" + loginId + "'";
					queryStringBuffer.append(" WHERE 1=1 AND tblProjects.PMO IN (" + TeamLoginIdList + ") ");

					// queryStringBuffer.append( " WHERE 1=1 AND tblProjects.PMO
					// IN (" +
					// DataSourceDataProvider.getInstance().getTeamLoginIdList(teamMap)
					// + ") ");
				} else {
					queryStringBuffer.append(" WHERE 1=1 AND tblProjects.PMO = '" + loginId + "'");
				}
			}
			if (!"All".equals(customerName)) {
				queryStringBuffer.append(" AND tblCrmAccount.Id='" + customerName + "' ");
			}
			if (status != null && !"".equals(status)) {
				queryStringBuffer.append(" AND tblProjects.Status='" + status + "' ");
			}
			if (startDate != null && !"".equals(startDate)) {
				queryStringBuffer.append(" AND DATE(`tblProjects`.`ProjectStartDate`) >= DATE('"
						+ DateUtility.getInstance().convertStringToMySQLDate(startDate) + "')");
			}
			if (projectType != null && !"".equals(projectType)) {
				queryStringBuffer.append(" AND tblProjects.ProjectType=\"" + projectType + "\" ");
			}
			// queryStringBuffer.append("ORDER BY `tblCrmAccount`.`Name` DESC
			// LIMIT 100");
			if (orderBy.equals("CustomerName")) {
				queryStringBuffer.append("ORDER BY `tblCrmAccount`.`NAME` ");
			}
			if (orderBy.equals("ProjectName")) {
				queryStringBuffer.append("ORDER BY `tblProjects`.`ProjectName`  ");
			}
			if (orderBy.equals("CostModel")) {
				queryStringBuffer.append("ORDER BY  `tblProjects`.`CostModel` ");
			}
			if (orderBy.equals("Sector")) {
				queryStringBuffer.append("ORDER BY `tblProjects`.`Sector`  ");
			}
			if (orderBy.equals("Practice")) {
				queryStringBuffer.append("ORDER BY `tblProjects`.`Practice` ");
			}
			if (orderBy.equals("ProjectType")) {
				queryStringBuffer.append("ORDER BY `tblProjects`.`ProjectType` ");
			}
			if (orderBy.equals("StartDate")) {
				queryStringBuffer.append("ORDER BY `tblProjects`.`ProjectStartDate` ");
			}
			if (orderBy.equals("EndDate")) {
				queryStringBuffer.append("ORDER BY `tblProjects`.`ProjectEndDate` ");
			}
			if (orderBy.equals("OverallState")) {
				queryStringBuffer.append("ORDER BY `tblProjects`.`State` ");
			}
			if (orderBy.equals("Software")) {
				queryStringBuffer.append("ORDER BY `tblProjects`.`Software`  ");
			}
			if (orderBy.equals("Description")) {
				queryStringBuffer.append("ORDER BY `tblProjects`.`ProjectDescription` ");
			}
			if (orderBy.equals("Comments")) {
				queryStringBuffer.append("ORDER BY `tblProjects`.`Comments` ");
			}
			// System.out.println("query is---->" +
			// queryStringBuffer.toString());
			connection = ConnectionProvider.getInstance().getConnection();
			statement = connection.createStatement();
			resultSet = statement.executeQuery(queryStringBuffer.toString());

			int count = 0;
			while (resultSet.next()) {
				count++;
				responseString = responseString + resultSet.getString("Id") + "#^$";
				if (resultSet.getString("NAME") != null && !"".equals(resultSet.getString("NAME"))) {
					responseString = responseString + resultSet.getString("NAME") + "#^$";

				} else {
					responseString = responseString + "-" + "#^$";
				}
				if (resultSet.getString("ProjectName") != null && !"".equals(resultSet.getString("ProjectName"))) {
					responseString = responseString + resultSet.getString("ProjectName") + "#^$";

				} else {
					responseString = responseString + "-" + "#^$";
				}

				if (resultSet.getString("CostModel") != null && !"".equals(resultSet.getString("CostModel"))
						&& !"-1".equals(resultSet.getString("CostModel"))) {
					responseString = responseString + resultSet.getString("CostModel") + "#^$";

				} else {
					responseString = responseString + "-" + "#^$";
				}
				if (resultSet.getString("Sector") != null && !"".equals(resultSet.getString("Sector"))
						&& !"-1".equals(resultSet.getString("Sector"))) {
					responseString = responseString + resultSet.getString("Sector") + "#^$";

				} else {
					responseString = responseString + "-" + "#^$";
				}
				if (resultSet.getString("Practice") != null && !"".equals(resultSet.getString("Practice"))) {
					responseString = responseString + resultSet.getString("Practice") + "#^$";

				} else {
					responseString = responseString + "-" + "#^$";
				}
				if (resultSet.getString("ProjectType") != null && !"".equals(resultSet.getString("ProjectType"))) {
					responseString = responseString + resultSet.getString("ProjectType") + "#^$";

				} else {
					responseString = responseString + "-" + "#^$";
				}
				if (resultSet.getString("ProjectStartDate") != null
						&& !"".equals(resultSet.getString("ProjectStartDate"))) {
					responseString = responseString + resultSet.getString("ProjectStartDate") + "#^$";

				} else {
					responseString = responseString + "-" + "#^$";
				}

				if (resultSet.getString("ProjectEndDate") != null
						&& !"".equals(resultSet.getString("ProjectEndDate"))) {
					responseString = responseString + resultSet.getString("ProjectEndDate") + "#^$";

				} else {
					responseString = responseString + "-" + "#^$";
				}
				if (resultSet.getString("EndDatePlan") != null && !"".equals(resultSet.getString("EndDatePlan"))) {
					responseString = responseString + resultSet.getString("EndDatePlan") + "#^$";

				} else {
					responseString = responseString + "-" + "#^$";
				}
				if (resultSet.getString("State") != null && !"".equals(resultSet.getString("State"))) {
					responseString = responseString + resultSet.getString("State") + "#^$";

				} else {
					responseString = responseString + "-" + "#^$";
				}
				if (resultSet.getString("Software") != null && !"".equals(resultSet.getString("Software"))) {
					responseString = responseString + resultSet.getString("Software") + "#^$";

				} else {
					responseString = responseString + "-" + "#^$";
				}
				if (resultSet.getString("ProjectDescription") != null
						&& !"".equals(resultSet.getString("ProjectDescription"))) {
					responseString = responseString + resultSet.getString("ProjectDescription") + "#^$";

				} else {
					responseString = responseString + "-" + "#^$";
				}
				if (resultSet.getString("Comments") != null && !"".equals(resultSet.getString("Comments"))) {
					responseString = responseString + resultSet.getString("Comments") + "#^$";

				} else {
					responseString = responseString + "-" + "#^$";
				}
				// responseString = responseString +
				// resultSet.getString("Email1") + "#^$";
				// responseString = responseString + "view" + "#^$";
				// if (resultSet.getString("WorkPhoneNo") != null &&
				// !"".equals(resultSet.getString("WorkPhoneNo"))) {
				// responseString = responseString +
				// resultSet.getString("WorkPhoneNo");
				// } else {
				// responseString = responseString + "-";
				// }
				responseString = responseString + "*@!";
			}
			// System.out.println("response is: " + responseString);
		} catch (Exception sqe) {
			sqe.printStackTrace();
		} finally {
			try {

				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}

			} catch (SQLException sqle) {
				sqle.printStackTrace();
			}
		}
		return responseString;

	}

	public String getProjectDescriptionDetails(int prjId) throws ServiceLocatorException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		String totalStream = "";
		int i = 0;
		try {
			String query = "SELECT tblProjects.ProjectDescription FROM tblProjects  WHERE tblProjects.Id=  ?";

			// System.out.println("query-->"+query);
			connection = ConnectionProvider.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(query);
			preparedStatement.setInt(1, prjId);
			resultSet = preparedStatement.executeQuery();

			String PrjDescription = "-";

			while (resultSet.next()) {

				// if(resultSet.getString("NoOfResumesSubmitted")!=null)
				// NoOfResumesSubmitted =
				// resultSet.getString("NoOfResumesSubmitted");
				if (resultSet.getString("ProjectDescription") != null) {
					PrjDescription = resultSet.getString("ProjectDescription");
					i++;
				}

			}

			totalStream = PrjDescription;
		} catch (Exception e) {
			System.err.println("Exception is-->" + e.getMessage());
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (Exception sqle) {
				System.err.println("SQL Exception is-->" + sqle.getMessage());
			}
		}
		return totalStream;
	}

	public String getProjectCommentDetails(int prjId) throws ServiceLocatorException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		String totalStream = "";
		int i = 0;
		try {
			String query = "SELECT Comments FROM tblProjects  WHERE Id=  ?";

			// System.out.println("query-->"+query);
			connection = ConnectionProvider.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(query);
			preparedStatement.setInt(1, prjId);
			resultSet = preparedStatement.executeQuery();

			String PrjComments = "-";

			while (resultSet.next()) {

				// if(resultSet.getString("NoOfResumesSubmitted")!=null)
				// NoOfResumesSubmitted =
				// resultSet.getString("NoOfResumesSubmitted");
				if (resultSet.getString("Comments") != null) {
					PrjComments = resultSet.getString("Comments");
					i++;
				}

			}

			totalStream = PrjComments;
		} catch (Exception e) {
			System.err.println("Exception is-->" + e.getMessage());
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (Exception sqle) {
				System.err.println("SQL Exception is-->" + sqle.getMessage());
			}
		}
		return totalStream;
	}

	public String getPSCERDetailsForPresales(HttpServletRequest httpServletRequest,
			NewAjaxHandlerAction ajaxHandlerAction) throws ServiceLocatorException {
		StringBuffer stringBuffer = new StringBuffer();
		String totalStream = "";
		Statement statement = null;
		Statement statement1 = null;
		Connection connection = null;
		ResultSet resultSet = null;
		ResultSet resultSet1 = null;
		PreparedStatement preparedStatement = null;
		int totalRecords = 0;

		String loginId = httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID)
				.toString();
		Map rolesMap = (Map) httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_MY_ROLES);
		connection = ConnectionProvider.getInstance().getConnection();

		httpServletRequest.getSession(false).setAttribute("tempRequestType", ajaxHandlerAction.getRequestType());
		httpServletRequest.getSession(false).setAttribute("tempStartDate", ajaxHandlerAction.getStartDate());
		httpServletRequest.getSession(false).setAttribute("tempEndDate", ajaxHandlerAction.getEndDate());
		httpServletRequest.getSession(false).setAttribute("temPgNo", ajaxHandlerAction.getPgNo());

		// query = "SELECT
		// tblProjects.ProjectName,tblPmoAuthors.AccountId,tblProjects.ProjectStartDate,tblProjects.STATUS,
		// tblCrmAccount.NAME,tblPmoAuthors.ProjectId FROM tblPmoAuthors JOIN
		// tblProjects ON (tblProjects.Id=tblPmoAuthors.ProjectId) JOIN
		// tblCrmAccount ON (tblCrmAccount.Id = tblPmoAuthors.AccountId) WHERE
		// tblPmoAuthors.AuthorId = '" + ajaxHandlerAction.getLoginId() + "' AND
		// tblPmoAuthors.STATUS = 'Active' ";
		StringBuffer queryStringBuffer = new StringBuffer(
				"SELECT tblPSCER.Id AS Id,AccountName AS CustomerName,tblPSCER.Stage AS State,IF(MeetingType = '' ,'N/A',MeetingType ) AS MeetingType,IF(MeetingDate IS NULL ,'N/A',DATE_FORMAT(MeetingDate,'%m/%d/%Y') ) AS MeetingDate,tblPSCER.Comments,DATE_FORMAT(tblPSCER.CreatedDate,'%m/%d/%Y') AS CreatedDate,tblPSCER.RequestorId,tblPSCER.RequestType,tblPSCER.ResourceIds,CONCAT(FName,' ',MName,'.',LName) AS EmpName FROM tblPSCER LEFT OUTER JOIN tblCrmAccount ON tblPSCER.AccountId = tblCrmAccount.Id LEFT OUTER JOIN tblEmployee ON tblPSCER.RequestorId=tblEmployee.LoginId WHERE RecordType=1  ");
		if (!rolesMap.containsValue("Admin")) {
			// queryStringBuffer.append(" AND FIND_IN_SET(ResourceIds,'" +
			// loginId + "') ");
			queryStringBuffer.append(" AND FIND_IN_SET('" + loginId + "', ResourceIds) > 0 ");
		}

		if (ajaxHandlerAction.getStartDate() != null && !"".equals(ajaxHandlerAction.getStartDate())) {
			queryStringBuffer.append(" AND DATE(tblPSCER.CreatedDate)>=DATE('"
					+ DateUtility.getInstance().convertStringToMySQLDate(ajaxHandlerAction.getStartDate()) + "') ");
		}

		if (ajaxHandlerAction.getEndDate() != null && !"".equals(ajaxHandlerAction.getEndDate())) {
			queryStringBuffer.append(" AND DATE(tblPSCER.CreatedDate)<=DATE('"
					+ DateUtility.getInstance().convertStringToMySQLDate(ajaxHandlerAction.getEndDate()) + "') ");
		}
		if (!"".equals(ajaxHandlerAction.getRequestType()) && ajaxHandlerAction.getRequestType() != null) {
			queryStringBuffer.append(" AND  RequestType='" + ajaxHandlerAction.getRequestType() + "'");

		}
		if (ajaxHandlerAction.getRequestId() != 0) {
			queryStringBuffer.append(" AND  tblPSCER.Id='" + ajaxHandlerAction.getRequestId() + "'");

		}
		if (ajaxHandlerAction.getMeetingstartDate() != null && !"".equals(ajaxHandlerAction.getMeetingstartDate())) {
			queryStringBuffer.append(" AND DATE(tblPSCER.MeetingDate)>=DATE('"
					+ DateUtility.getInstance().convertStringToMySQLDate(ajaxHandlerAction.getMeetingstartDate())
					+ "') ");
		}
		if (ajaxHandlerAction.getMeetingendDate() != null && !"".equals(ajaxHandlerAction.getMeetingendDate())) {
			queryStringBuffer.append(" AND DATE(tblPSCER.MeetingDate)<=DATE('"
					+ DateUtility.getInstance().convertStringToMySQLDate(ajaxHandlerAction.getMeetingendDate())
					+ "') ");
		}
		queryStringBuffer.append(
				" ORDER BY tblPSCER.CreatedDate DESC  LIMIT " + (ajaxHandlerAction.getPgNo() - 1) * 10 + ", 10");

		// System.out.println("queryStringBuffer..."+queryStringBuffer.toString());
		try {

			int index = (ajaxHandlerAction.getPgNo() - 1) * 10;

			if ("1".equals(ajaxHandlerAction.getPgFlag().trim())) {
				StringBuffer queryStringBufferCount = new StringBuffer(
						"SELECT COUNT(tblPSCER.Id) AS count FROM tblPSCER LEFT OUTER JOIN tblCrmAccount ON tblPSCER.AccountId = tblCrmAccount.Id  WHERE  RecordType=1 ");
				if (!rolesMap.containsValue("Admin")) {
					// queryStringBufferCount.append(" AND
					// FIND_IN_SET(ResourceIds,'" + loginId + "') ");
					queryStringBufferCount.append(" AND FIND_IN_SET('" + loginId + "', ResourceIds) > 0 ");
				}

				if (ajaxHandlerAction.getStartDate() != null && !"".equals(ajaxHandlerAction.getStartDate())) {
					queryStringBufferCount.append(" AND DATE(CreatedDate)>=DATE('"
							+ DateUtility.getInstance().convertStringToMySQLDate(ajaxHandlerAction.getStartDate())
							+ "') ");
				}

				if (ajaxHandlerAction.getEndDate() != null && !"".equals(ajaxHandlerAction.getEndDate())) {
					queryStringBufferCount.append(" AND DATE(CreatedDate)<=DATE('"
							+ DateUtility.getInstance().convertStringToMySQLDate(ajaxHandlerAction.getEndDate())
							+ "') ");
				}
				if (!"".equals(ajaxHandlerAction.getRequestType()) && ajaxHandlerAction.getRequestType() != null) {
					queryStringBufferCount.append(" AND  RequestType='" + ajaxHandlerAction.getRequestType() + "'");

				}
				if (ajaxHandlerAction.getRequestId() != 0) {
					queryStringBufferCount.append(" AND  tblPSCER.Id='" + ajaxHandlerAction.getRequestId() + "'");

				}

				statement1 = connection.createStatement();
				resultSet1 = statement1.executeQuery(queryStringBufferCount.toString());
				while (resultSet1.next()) {
					totalRecords = resultSet1.getInt("count");
				}
			}

			// System.out.println("Query -->"+query);
			statement = connection.createStatement();
			resultSet = statement.executeQuery(queryStringBuffer.toString());

			String QUERY_STRING = "SELECT CONCAT(FName,'',MName,'.',LName) AS NAME FROM tblEmployee WHERE FIND_IN_SET(LoginId,?) ";
			preparedStatement = connection.prepareStatement(QUERY_STRING);

			while (resultSet.next()) {
				int Id = resultSet.getInt("Id");

				String CustomerName = resultSet.getString("CustomerName");

				String State = resultSet.getString("State");

				String MeetingType = resultSet.getString("MeetingType");

				index = index + 1;
				String MeetingDate = resultSet.getString("MeetingDate");
				String Comments = resultSet.getString("Comments");
				String CreatedDate = resultSet.getString("CreatedDate");
				String EmpName = resultSet.getString("EmpName");
				String RequestType = resultSet.getString("RequestType");
				String ResourceIds = resultSet.getString("ResourceIds");

				String ResourceIdsNames = com.mss.mirage.util.DataSourceDataProvider.getInstance()
						.getEmployeeNamesByLoginIdList(preparedStatement, ResourceIds);
				// totalStream = totalStream + "#^$" + Id + "#^$" + CustomerName
				// + "#^$" + State + "#^$" + MeetingType + "#^$" + MeetingDate +
				// "#^$" + Comments + "#^$" + CreatedDate + "#^$" + EmpName
				// +"#^$" + RequestType + "#^$" + ResourceIdsNames + "*@!";
				totalStream = totalStream + "#^$" + Id + "#^$" + CustomerName + "#^$" + State + "#^$" + MeetingType
						+ "#^$" + MeetingDate + "#^$" + Comments + "#^$" + CreatedDate + "#^$" + EmpName + "#^$"
						+ RequestType + "#^$" + ResourceIdsNames + "#^$" + index + "*@!";
			}

			stringBuffer.append(totalStream + "###" + totalRecords);
			// System.out.println("stringBuffer.toString()...."+stringBuffer.toString());

		} catch (Exception sqe) {
			sqe.printStackTrace();
		} finally {
			try {
				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}
				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (resultSet1 != null) {
					resultSet1.close();
					resultSet1 = null;
				}
				if (statement != null) {
					statement.close();
					statement = null;
				}
				if (statement1 != null) {
					statement1.close();
					statement1 = null;
				}

				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException sqle) {
			}
		}
		return stringBuffer.toString();
	}

	public String getRequestInSightDetails(int leadId) throws ServiceLocatorException {

		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		Connection connection = null;
		String response = "-";
		try {
			connection = ConnectionProvider.getInstance().getConnection();
			// SELECT Comments FROM tblEmpIssues WHERE Id=810
			preparedStatement = connection
					.prepareStatement("Select InSightDetails from tblPSCER WHERE RequestId=? AND RecordType=1");
			preparedStatement.setInt(1, leadId);
			resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {

				if (resultSet.getString("InSightDetails") != null) {
					// startDate = resultSet.getString("ProjectStartDate");
					response = resultSet.getString("InSightDetails");
				}

			}

		} catch (SQLException ex) {
			ex.printStackTrace();
		} catch (ServiceLocatorException sle) {
			sle.printStackTrace();
		} finally {
			try {

				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
		}

		return response;
	}

	public String getPSCERDetailsForEmployee(HttpServletRequest httpServletRequest,
			NewAjaxHandlerAction ajaxHandlerAction) throws ServiceLocatorException {

		StringBuffer stringBuffer = new StringBuffer();
		String totalStream = "";
		Statement statement = null;
		Statement statement1 = null;
		Connection connection = null;
		ResultSet resultSet = null;
		ResultSet resultSet1 = null;
		PreparedStatement preparedStatement = null;
		int index = (ajaxHandlerAction.getPgNo() - 1) * 10;
		int totalRecords = 0;

		String loginId = httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID)
				.toString();
		connection = ConnectionProvider.getInstance().getConnection();

		httpServletRequest.getSession(false).setAttribute("tempRequestType", ajaxHandlerAction.getRequestType());
		httpServletRequest.getSession(false).setAttribute("tempStartDate", ajaxHandlerAction.getStartDate());
		httpServletRequest.getSession(false).setAttribute("tempEndDate", ajaxHandlerAction.getEndDate());
		httpServletRequest.getSession(false).setAttribute("temPgNo", ajaxHandlerAction.getPgNo());
		httpServletRequest.getSession(false).setAttribute("tempState", ajaxHandlerAction.getState());

		// query = "SELECT
		// tblProjects.ProjectName,tblPmoAuthors.AccountId,tblProjects.ProjectStartDate,tblProjects.STATUS,
		// tblCrmAccount.NAME,tblPmoAuthors.ProjectId FROM tblPmoAuthors JOIN
		// tblProjects ON (tblProjects.Id=tblPmoAuthors.ProjectId) JOIN
		// tblCrmAccount ON (tblCrmAccount.Id = tblPmoAuthors.AccountId) WHERE
		// tblPmoAuthors.AuthorId = '" + ajaxHandlerAction.getLoginId() + "' AND
		// tblPmoAuthors.STATUS = 'Active' ";
		StringBuffer queryStringBuffer = new StringBuffer(
				"SELECT tblPSCER.Id AS Id,AccountName AS CustomerName,tblPSCER.Stage AS State,IF(MeetingType = '' ,'N/A',MeetingType ) AS MeetingType,IF(MeetingDate IS NULL ,'N/A',DATE_FORMAT(MeetingDate,'%m/%d/%Y') ) AS MeetingDate,tblPSCER.Comments,DATE_FORMAT(tblPSCER.CreatedDate,'%m/%d/%Y') AS CreatedDate,tblPSCER.RequestorId,tblPSCER.RequestType,tblPSCER.ResourceIds,CONCAT(FName,' ',MName,'.',LName) AS EmpName FROM tblPSCER LEFT OUTER JOIN tblCrmAccount ON tblPSCER.AccountId = tblCrmAccount.Id LEFT OUTER JOIN tblEmployee ON tblPSCER.RequestorId=tblEmployee.LoginId WHERE Stage NOT IN('Creation') AND RecordType=1 ");

		if (ajaxHandlerAction.getStartDate() != null && !"".equals(ajaxHandlerAction.getStartDate())) {
			queryStringBuffer.append(" AND DATE(tblPSCER.CreatedDate)>=DATE('"
					+ DateUtility.getInstance().convertStringToMySQLDate(ajaxHandlerAction.getStartDate()) + "') ");
		}

		if (ajaxHandlerAction.getEndDate() != null && !"".equals(ajaxHandlerAction.getEndDate())) {
			queryStringBuffer.append(" AND DATE(tblPSCER.CreatedDate)<=DATE('"
					+ DateUtility.getInstance().convertStringToMySQLDate(ajaxHandlerAction.getEndDate()) + "') ");
		}

		if (!"".equals(ajaxHandlerAction.getState()) && ajaxHandlerAction.getState() != null) {
			queryStringBuffer.append(" AND  Stage ='" + ajaxHandlerAction.getState() + "' ");

		}

		if (!"".equals(ajaxHandlerAction.getRequestType()) && ajaxHandlerAction.getRequestType() != null) {
			queryStringBuffer.append(" AND  RequestType='" + ajaxHandlerAction.getRequestType() + "'");

		}
		if (ajaxHandlerAction.getRequestId() != 0) {
			queryStringBuffer.append(" AND  tblPSCER.Id='" + ajaxHandlerAction.getRequestId() + "'");

		}

		if (ajaxHandlerAction.getAccountId() != -1) {
			queryStringBuffer.append(" AND  tblPSCER.AccountId='" + ajaxHandlerAction.getAccountId() + "'");

		}
		if (ajaxHandlerAction.getMeetingstartDate() != null && !"".equals(ajaxHandlerAction.getMeetingstartDate())) {
			queryStringBuffer.append(" AND DATE(tblPSCER.MeetingDate)>=DATE('"
					+ DateUtility.getInstance().convertStringToMySQLDate(ajaxHandlerAction.getMeetingstartDate())
					+ "') ");
		}
		if (ajaxHandlerAction.getMeetingendDate() != null && !"".equals(ajaxHandlerAction.getMeetingendDate())) {
			queryStringBuffer.append(" AND DATE(tblPSCER.MeetingDate)<=DATE('"
					+ DateUtility.getInstance().convertStringToMySQLDate(ajaxHandlerAction.getMeetingendDate())
					+ "') ");
		}
		queryStringBuffer.append(
				" ORDER BY tblPSCER.CreatedDate DESC  LIMIT " + (ajaxHandlerAction.getPgNo() - 1) * 10 + ", 10");

		try {

			if ("1".equals(ajaxHandlerAction.getPgFlag().trim())) {
				StringBuffer queryStringBufferCount = new StringBuffer(
						"SELECT COUNT(tblPSCER.Id) AS count FROM tblPSCER LEFT OUTER JOIN tblCrmAccount ON tblPSCER.AccountId = tblCrmAccount.Id  WHERE Stage NOT IN('Creation') AND RecordType=1 ");

				if (ajaxHandlerAction.getStartDate() != null && !"".equals(ajaxHandlerAction.getStartDate())) {
					queryStringBufferCount.append(" AND DATE(CreatedDate)>=DATE('"
							+ DateUtility.getInstance().convertStringToMySQLDate(ajaxHandlerAction.getStartDate())
							+ "') ");
				}

				if (ajaxHandlerAction.getEndDate() != null && !"".equals(ajaxHandlerAction.getEndDate())) {
					queryStringBufferCount.append(" AND DATE(CreatedDate)<=DATE('"
							+ DateUtility.getInstance().convertStringToMySQLDate(ajaxHandlerAction.getEndDate())
							+ "') ");
				}

				if (!"".equals(ajaxHandlerAction.getState()) && ajaxHandlerAction.getState() != null) {
					queryStringBufferCount.append(" AND  Stage ='" + ajaxHandlerAction.getState() + "'");

				}

				if (!"".equals(ajaxHandlerAction.getRequestType()) && ajaxHandlerAction.getRequestType() != null) {
					queryStringBufferCount.append(" AND  RequestType='" + ajaxHandlerAction.getRequestType() + "'");

				}
				if (ajaxHandlerAction.getRequestId() != 0) {
					queryStringBufferCount.append(" AND  tblPSCER.Id='" + ajaxHandlerAction.getRequestId() + "'");

				}

				statement1 = connection.createStatement();
				resultSet1 = statement1.executeQuery(queryStringBufferCount.toString());
				while (resultSet1.next()) {
					totalRecords = resultSet1.getInt("count");
				}
			}

			// System.out.println("Query -->"+query);
			statement = connection.createStatement();
			resultSet = statement.executeQuery(queryStringBuffer.toString());

			String QUERY_STRING = "SELECT CONCAT(FName,'',MName,'.',LName) AS NAME FROM tblEmployee WHERE FIND_IN_SET(LoginId,?) ";
			preparedStatement = connection.prepareStatement(QUERY_STRING);

			while (resultSet.next()) {
				index = index + 1;
				int Id = resultSet.getInt("Id");

				String CustomerName = resultSet.getString("CustomerName");

				String State = resultSet.getString("State");

				String MeetingType = resultSet.getString("MeetingType");

				String MeetingDate = resultSet.getString("MeetingDate");
				String Comments = resultSet.getString("Comments");
				String CreatedDate = resultSet.getString("CreatedDate");
				String EmpName = resultSet.getString("EmpName");
				String RequestType = resultSet.getString("RequestType");
				String ResourceIds = resultSet.getString("ResourceIds");

				String ResourceIdsNames = com.mss.mirage.util.DataSourceDataProvider.getInstance()
						.getEmployeeNamesByLoginIdList(preparedStatement, ResourceIds);
				totalStream = totalStream + "#^$" + Id + "#^$" + CustomerName + "#^$" + State + "#^$" + MeetingType
						+ "#^$" + MeetingDate + "#^$" + Comments + "#^$" + CreatedDate + "#^$" + EmpName + "#^$"
						+ RequestType + "#^$" + ResourceIdsNames + "#^$" + index + "*@!";
			}

			stringBuffer.append(totalStream + "###" + totalRecords);

		} catch (Exception sqe) {
			sqe.printStackTrace();
		} finally {
			try {
				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}
				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (resultSet1 != null) {
					resultSet1.close();
					resultSet1 = null;
				}
				if (statement != null) {
					statement.close();
					statement = null;
				}
				if (statement1 != null) {
					statement1.close();
					statement1 = null;
				}

				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException sqle) {
			}
		}
		return stringBuffer.toString();
	}

	public String getPSCERDetailsForSales(HttpServletRequest httpServletRequest, NewAjaxHandlerAction ajaxHandlerAction)
			throws ServiceLocatorException {
		StringBuffer stringBuffer = new StringBuffer();
		String totalStream = "";
		Statement statement = null;
		Statement statement1 = null;
		Connection connection = null;
		ResultSet resultSet = null;
		ResultSet resultSet1 = null;
		PreparedStatement preparedStatement = null;
		int index = (ajaxHandlerAction.getPgNo() - 1) * 10;
		int totalRecords = 0;

		String loginId = httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID)
				.toString();
		int empId = Integer.parseInt(
				httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_EMP_ID).toString());
		connection = ConnectionProvider.getInstance().getConnection();

		httpServletRequest.getSession(false).setAttribute("tempRequestType", ajaxHandlerAction.getRequestType());
		httpServletRequest.getSession(false).setAttribute("tempStartDate", ajaxHandlerAction.getStartDate());
		httpServletRequest.getSession(false).setAttribute("tempEndDate", ajaxHandlerAction.getEndDate());
		httpServletRequest.getSession(false).setAttribute("temPgNo", ajaxHandlerAction.getPgNo());
		httpServletRequest.getSession(false).setAttribute("tempState", ajaxHandlerAction.getState());

		String practice = httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_EMP_PRACTICE)
				.toString();
		// query = "SELECT
		// tblProjects.ProjectName,tblPmoAuthors.AccountId,tblProjects.ProjectStartDate,tblProjects.STATUS,
		// tblCrmAccount.NAME,tblPmoAuthors.ProjectId FROM tblPmoAuthors JOIN
		// tblProjects ON (tblProjects.Id=tblPmoAuthors.ProjectId) JOIN
		// tblCrmAccount ON (tblCrmAccount.Id = tblPmoAuthors.AccountId) WHERE
		// tblPmoAuthors.AuthorId = '" + ajaxHandlerAction.getLoginId() + "' AND
		// tblPmoAuthors.STATUS = 'Active' ";
		// StringBuffer queryStringBuffer = new StringBuffer("SELECT tblPSCER.Id
		// AS Id,AccountName AS CustomerName,tblPSCER.Stage AS
		// State,IF(MeetingType = '' ,'N/A',MeetingType ) AS
		// MeetingType,IF(MeetingDate IS NULL
		// ,'N/A',DATE_FORMAT(MeetingDate,'%m/%d/%Y') ) AS
		// MeetingDate,tblPSCER.Comments,DATE_FORMAT(tblPSCER.CreatedDate,'%m/%d/%Y')
		// AS
		// CreatedDate,tblPSCER.RequestorId,tblPSCER.RequestType,tblPSCER.ResourceIds,CONCAT(FName,'
		// ',MName,'.',LName) AS EmpName FROM tblPSCER LEFT OUTER JOIN
		// tblCrmAccount ON tblPSCER.AccountId = tblCrmAccount.Id LEFT OUTER
		// JOIN tblEmployee ON tblPSCER.RequestorId=tblEmployee.LoginId WHERE
		// tblPSCER.CreatedBy='" +
		// httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID).toString()
		// + "' AND RecordType=1");
		StringBuffer queryStringBuffer = new StringBuffer(
				"SELECT tblPSCER.Id AS Id,AccountName AS CustomerName,tblPSCER.Stage AS State,IF(MeetingType = '' ,'N/A',MeetingType ) AS MeetingType,IF(MeetingDate IS NULL ,'N/A',DATE_FORMAT(MeetingDate,'%m/%d/%Y') ) AS MeetingDate,tblPSCER.Comments,DATE_FORMAT(tblPSCER.CreatedDate,'%m/%d/%Y') AS CreatedDate,tblPSCER.RequestorId,tblPSCER.RequestType,tblPSCER.ResourceIds,CONCAT(FName,' ',MName,'.',LName) AS EmpName FROM tblPSCER LEFT OUTER JOIN tblCrmAccount ON tblPSCER.AccountId = tblCrmAccount.Id LEFT OUTER JOIN tblEmployee ON tblPSCER.RequestorId=tblEmployee.LoginId WHERE RecordType=1");

		if (ajaxHandlerAction.getStartDate() != null && !"".equals(ajaxHandlerAction.getStartDate())) {
			queryStringBuffer.append(" AND DATE(tblPSCER.CreatedDate)>=DATE('"
					+ DateUtility.getInstance().convertStringToMySQLDate(ajaxHandlerAction.getStartDate()) + "') ");
		}

		if (ajaxHandlerAction.getEndDate() != null && !"".equals(ajaxHandlerAction.getEndDate())) {
			queryStringBuffer.append(" AND DATE(tblPSCER.CreatedDate)<=DATE('"
					+ DateUtility.getInstance().convertStringToMySQLDate(ajaxHandlerAction.getEndDate()) + "') ");
		}

		if (!"".equals(ajaxHandlerAction.getState()) && ajaxHandlerAction.getState() != null) {
			queryStringBuffer.append(" AND  Stage ='" + ajaxHandlerAction.getState() + "' ");

		}

		if (!"".equals(ajaxHandlerAction.getRequestType()) && ajaxHandlerAction.getRequestType() != null) {
			queryStringBuffer.append(" AND  RequestType='" + ajaxHandlerAction.getRequestType() + "'");

		}
		if (ajaxHandlerAction.getRequestId() != 0) {
			queryStringBuffer.append(" AND  tblPSCER.Id='" + ajaxHandlerAction.getRequestId() + "'");

		}

		if ("Practice".equals(practice)) {
			queryStringBuffer.append(" AND  tblPSCER.RegionalMgrId='" + empId + "'");

		} else {
			queryStringBuffer.append(" AND tblPSCER.CreatedBy='"
					+ httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID).toString()
					+ "'");
		}
		if (ajaxHandlerAction.getMeetingstartDate() != null && !"".equals(ajaxHandlerAction.getMeetingstartDate())) {
			queryStringBuffer.append(" AND DATE(tblPSCER.MeetingDate)>=DATE('"
					+ DateUtility.getInstance().convertStringToMySQLDate(ajaxHandlerAction.getMeetingstartDate())
					+ "') ");
		}
		if (ajaxHandlerAction.getMeetingendDate() != null && !"".equals(ajaxHandlerAction.getMeetingendDate())) {
			queryStringBuffer.append(" AND DATE(tblPSCER.MeetingDate)<=DATE('"
					+ DateUtility.getInstance().convertStringToMySQLDate(ajaxHandlerAction.getMeetingendDate())
					+ "') ");
		}
		queryStringBuffer.append(
				" ORDER BY tblPSCER.CreatedDate DESC  LIMIT " + (ajaxHandlerAction.getPgNo() - 1) * 10 + ", 10");

		try {

			if ("1".equals(ajaxHandlerAction.getPgFlag().trim())) {
				// StringBuffer queryStringBufferCount = new
				// StringBuffer("SELECT COUNT(tblPSCER.Id) AS count FROM
				// tblPSCER LEFT OUTER JOIN tblCrmAccount ON tblPSCER.AccountId
				// = tblCrmAccount.Id WHERE tblPSCER.CreatedBy='" +
				// httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID).toString()
				// + "' AND RecordType=1 ");

				StringBuffer queryStringBufferCount = new StringBuffer(
						"SELECT COUNT(tblPSCER.Id) AS count FROM tblPSCER LEFT OUTER JOIN tblCrmAccount ON tblPSCER.AccountId = tblCrmAccount.Id  WHERE  RecordType=1 ");

				if (ajaxHandlerAction.getStartDate() != null && !"".equals(ajaxHandlerAction.getStartDate())) {
					queryStringBufferCount.append(" AND DATE(CreatedDate)>=DATE('"
							+ DateUtility.getInstance().convertStringToMySQLDate(ajaxHandlerAction.getStartDate())
							+ "') ");
				}

				if (ajaxHandlerAction.getEndDate() != null && !"".equals(ajaxHandlerAction.getEndDate())) {
					queryStringBufferCount.append(" AND DATE(CreatedDate)<=DATE('"
							+ DateUtility.getInstance().convertStringToMySQLDate(ajaxHandlerAction.getEndDate())
							+ "') ");
				}

				if (!"".equals(ajaxHandlerAction.getState()) && ajaxHandlerAction.getState() != null) {
					queryStringBufferCount.append(" AND  Stage ='" + ajaxHandlerAction.getState() + "'");

				}

				if (!"".equals(ajaxHandlerAction.getRequestType()) && ajaxHandlerAction.getRequestType() != null) {
					queryStringBufferCount.append(" AND  RequestType='" + ajaxHandlerAction.getRequestType() + "'");

				}
				if (ajaxHandlerAction.getRequestId() != 0) {
					queryStringBufferCount.append(" AND  tblPSCER.Id='" + ajaxHandlerAction.getRequestId() + "'");

				}

				if ("Practice".equals(practice)) {
					queryStringBufferCount.append(" AND tblPSCER.RegionalMgrId='" + empId + "'");

				} else {
					queryStringBufferCount.append(" AND tblPSCER.CreatedBy='" + httpServletRequest.getSession(false)
							.getAttribute(ApplicationConstants.SESSION_USER_ID).toString() + "'");
				}

				statement1 = connection.createStatement();
				resultSet1 = statement1.executeQuery(queryStringBufferCount.toString());
				while (resultSet1.next()) {
					totalRecords = resultSet1.getInt("count");
				}
			}

			// System.out.println("Query -->"+query);
			statement = connection.createStatement();
			resultSet = statement.executeQuery(queryStringBuffer.toString());

			String QUERY_STRING = "SELECT CONCAT(FName,'',MName,'.',LName) AS NAME FROM tblEmployee WHERE FIND_IN_SET(LoginId,?) ";
			preparedStatement = connection.prepareStatement(QUERY_STRING);

			while (resultSet.next()) {
				index = index + 1;
				int Id = resultSet.getInt("Id");

				String CustomerName = resultSet.getString("CustomerName");

				String State = resultSet.getString("State");

				String MeetingType = resultSet.getString("MeetingType");

				String MeetingDate = resultSet.getString("MeetingDate");
				String Comments = resultSet.getString("Comments");
				String CreatedDate = resultSet.getString("CreatedDate");
				String EmpName = resultSet.getString("EmpName");
				String RequestType = resultSet.getString("RequestType");
				String ResourceIds = resultSet.getString("ResourceIds");

				String ResourceIdsNames = com.mss.mirage.util.DataSourceDataProvider.getInstance()
						.getEmployeeNamesByLoginIdList(preparedStatement, ResourceIds);
				totalStream = totalStream + "#^$" + Id + "#^$" + CustomerName + "#^$" + State + "#^$" + MeetingType
						+ "#^$" + MeetingDate + "#^$" + Comments + "#^$" + CreatedDate + "#^$" + EmpName + "#^$"
						+ RequestType + "#^$" + ResourceIdsNames + "#^$" + index + "*@!";
			}

			stringBuffer.append(totalStream + "###" + totalRecords);

		} catch (Exception sqe) {
			sqe.printStackTrace();
		} finally {
			try {
				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}
				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (resultSet1 != null) {
					resultSet1.close();
					resultSet1 = null;
				}
				if (statement != null) {
					statement.close();
					statement = null;
				}
				if (statement1 != null) {
					statement1.close();
					statement1 = null;
				}

				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException sqle) {
			}
		}
		return stringBuffer.toString();
	}

	public String getDomainByExamType(int flag) {
		StringBuffer stringBuffer = new StringBuffer();
		String queryString = null;
		Connection connection = null;
		queryString = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		boolean isGetting = false;
		String domainName = null;
		int count = 0;
		try {
			connection = ConnectionProvider.getInstance().getConnection();

			// System.out.println("getPgNo values :::::::" + flag);
			if (flag == 1) {
				queryString = "SELECT Id,DomainName FROM tblEcertDomain WHERE status='Active' AND Id != "
						+ Properties.getProperty("CRE.DomainId") + " ORDER BY DomainName";
			} else {
				queryString = "SELECT Id,DomainName FROM tblEcertDomain WHERE status='Active' AND Id = "
						+ Properties.getProperty("CRE.DomainId") + " ORDER BY DomainName";
			}
			preparedStatement = connection.prepareStatement(queryString);
			resultSet = preparedStatement.executeQuery();

			// stringBuffer.append("<?xml version=\"1.0\"
			// encoding=\"UTF-8\"?>");
			stringBuffer.append("<xml version=\"1.0\">");
			stringBuffer.append("<TOPICS>");
			stringBuffer.append("<TOPIC TOPICID=\"-1\">");
			stringBuffer.append("--Please select--");
			stringBuffer.append("</TOPIC>");
			while (resultSet.next()) {
				domainName = resultSet.getString("DomainName");
				if (domainName != null && !"".equals(domainName)) {
					stringBuffer.append("<TOPIC TOPICID=\"" + resultSet.getInt("Id") + "\">");

					if (domainName.contains("&")) {
						domainName = domainName.replaceAll("&", "&amp;");
					}
					stringBuffer.append(domainName);
					stringBuffer.append("</TOPIC>");

				}
				isGetting = true;
				count++;
			}
			if (!isGetting) {
				// sb.append("<EMPLOYEES>" + sb.toString() + "</EMPLOYEES>");
				// } else {
				isGetting = false;
				// nothing to show
				// response.setStatus(HttpServletResponse.SC_NO_CONTENT);
				// stringBuffer.append("<TOPIC TOPICID=\"
				// \"><VALID>false</VALID></TOPIC>");
			}
			stringBuffer.append("</TOPICS>");
			stringBuffer.append("</xml>");

		} catch (SQLException ex) {
			ex.printStackTrace();
		} catch (ServiceLocatorException sle) {
			sle.printStackTrace();
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException ex) {
				ex.printStackTrace();
			}

		}
		// System.out.println("Practice List: "+stringBuffer.toString());
		return stringBuffer.toString();
	}

	public String ajaxQuestionsFileUpload(NewAjaxHandlerAction ajaxHandlerAction) {

		Connection connection = null;
		PreparedStatement prestatement = null;
		ResultSet resultSet = null;
		int flag = 0;

		String resultMessage = "";

		int rowsInserted = 0;

		try {
			connection = ConnectionProvider.getInstance().getConnection();

			prestatement = connection.prepareStatement(
					"INSERT INTO tblEcertQuestion(Question,option1,option2,option3,option4,AnwerOption,Explanation,TopicId,CreatedBy,CreatedDate,STATUS,SubtopicId) VALUES(?,?,?,?,?,?,?,?,?,?,?,?)");
			FileInputStream fs = new FileInputStream(ajaxHandlerAction.getFile());
			// System.out.println("fs FileInputStream........."+fs);
			Workbook wb = Workbook.getWorkbook(fs);
			int sheet1Rows = 0;
			int sheet2Rows = 0;
			// for (int i = 0; i < 2; i++) {
			Sheet sh = wb.getSheet(0);
			sheet1Rows = sh.getRows();

			int totalRowsInTheUploadedExcel = sheet1Rows + sheet2Rows - 1;

			sh = wb.getSheet(0);

			// To get the number of rows present in sheet
			int totalNoOfRows = sh.getRows();

			int totalNoOfCols = sh.getColumns();
			if (ajaxHandlerAction.getFileFileName() != null) {

				if (totalNoOfCols != 8 || !"Question".equals(sh.getCell(1, 0).getContents())
						|| !"Option1".equals(sh.getCell(2, 0).getContents())
						|| !"Option2".equals(sh.getCell(3, 0).getContents())
						|| !"Option3".equals(sh.getCell(4, 0).getContents())
						|| !"Option4".equals(sh.getCell(5, 0).getContents())
						|| !"AnswerOption".equals(sh.getCell(6, 0).getContents())
						|| !"Explanation".equals(sh.getCell(7, 0).getContents())) {

					resultMessage = "InvalidFormat";
				} else {

					for (int row1 = 1; row1 < totalNoOfRows; row1++) {

						if (sh.getCell(1, row1).getContents() == null || "".equals(sh.getCell(1, row1).getContents())
								|| sh.getCell(2, row1).getContents() == null
								|| "".equals(sh.getCell(2, row1).getContents())
								|| sh.getCell(3, row1).getContents() == null
								|| "".equals(sh.getCell(3, row1).getContents())
								|| sh.getCell(4, row1).getContents() == null
								|| "".equals(sh.getCell(4, row1).getContents())
								|| sh.getCell(5, row1).getContents() == null
								|| "".equals(sh.getCell(5, row1).getContents())
								|| sh.getCell(6, row1).getContents() == null
								|| "".equals(sh.getCell(6, row1).getContents())
								|| sh.getCell(7, row1).getContents() == null
								|| "".equals(sh.getCell(7, row1).getContents())) {
							flag = 1;
							resultMessage = "Empty";
						}

					}

					if (flag != 1) {
						for (int row = 1; row < totalNoOfRows; row++) {

							String question = sh.getCell(1, row).getContents();
							// String code = sh.getCell(1, row).getContents();

							String option1 = sh.getCell(2, row).getContents();
							String option2 = sh.getCell(3, row).getContents();
							String option3 = sh.getCell(4, row).getContents();
							String option4 = sh.getCell(5, row).getContents();
							String anwerOption = sh.getCell(6, row).getContents();
							String explanation = sh.getCell(7, row).getContents();

							// System.out.println("---->question........."+question);
							// System.out.println("---->Integer.parseInt(\"5\"+empId)"+Integer.parseInt("5"+empId));
							// System.out.println("---->anwerOption........."+anwerOption);
							prestatement.setString(1, question);
							prestatement.setString(2, option1);
							prestatement.setString(3, option2);
							prestatement.setString(4, option3);
							prestatement.setString(5, option4);
							prestatement.setString(6, anwerOption);
							prestatement.setString(7, explanation);
							prestatement.setInt(8, ajaxHandlerAction.getTopicId());
							prestatement.setString(9, ajaxHandlerAction.getCreatedBy());
							prestatement.setTimestamp(10, DateUtility.getInstance().getCurrentMySqlDateTime());
							prestatement.setString(11, "Active");
							prestatement.setInt(12, ajaxHandlerAction.getSubTopicId());
							// System.out.println(".................."+DateUtility.getInstance().getCurrentMySqlDateTime());
							prestatement.executeUpdate();
							rowsInserted++;
							/*
							 * for (int col = 0; col < totalNoOfCols; col++) {
							 * System.out.print(sh.getCell(col,
							 * row).getContents() + "\t"); }
							 */

							// statement.addBatch("UPDATE tblEmpWages SET
							// DaysVacationFromBiometric ="+vacations+" WHERE
							// PayRoll_Id ="+payrollId+" AND
							// MONTH(PayPeriodStartDate) = 4 AND
							// YEAR(PayPeriodStartDate)=2015 AND IsBlock=0");
						}

						resultMessage = "uploaded^" + rowsInserted;

						// FileUtils.copyFile(getFile(), targetDirectory);
						// setObjectType("Emp Reviews");

					}
				}
			} else {

				resultMessage = "Error";
			}

		} catch (Exception ioe) {
			resultMessage = "<font style='color:red;'>Please try again later</font>";
			ioe.printStackTrace();
			System.err.println(ioe);
		} finally {
			try {

				if (prestatement != null) {
					prestatement.close();
					prestatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}

			} catch (SQLException ex) {
				ex.printStackTrace();
			}
		}
		return resultMessage;

	}

	/**
	 * Created By :Tirumala Teja Kadamanti Purpose : Newsletters Deployment
	 * Automation Use : To add and search Newsletters and images in a directory
	 **/
	public String getAllFilesInDirectory(HttpServletRequest httpServletRequest, NewAjaxHandlerAction ajaxHandlerAction)
			throws ServiceLocatorException {
		String response = "";
		try {
			JSONObject jb = new JSONObject();
			jb.put("Year", ajaxHandlerAction.getYear());
			jb.put("Month", ajaxHandlerAction.getMonth());
			jb.put("Type", ajaxHandlerAction.getType());
			jb.put("Category", ajaxHandlerAction.getCategory());
			String serviceUrl = RestRepository.getInstance().getSrviceUrl("getNewsLettersInDirectory");
			URL url = new URL(serviceUrl);
			URLConnection connection = url.openConnection();
			connection.setDoOutput(true);
			connection.setRequestProperty("Content-Type", "application/json");
			connection.setConnectTimeout(360 * 5000);
			connection.setReadTimeout(360 * 5000);
			OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
			out.write(jb.toString());
			out.close();

			BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			String s = null;
			String data = "";
			while ((s = in.readLine()) != null) {

				data = data + s;
			}

			JSONObject jObject = new JSONObject(data);

			response = jObject.getString("Response");
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return response;
	}

	public String getAllFilesInImagesDirectory(HttpServletRequest httpServletRequest,
			NewAjaxHandlerAction ajaxHandlerAction) throws ServiceLocatorException {
		String response = "";
		String basePath = Properties.getProperty("Images.NewsLetters.Path");
		String monthName = "";
		if (Integer.parseInt(ajaxHandlerAction.getMonth()) == 0) {
			monthName = "January";
		} else if (Integer.parseInt(ajaxHandlerAction.getMonth()) == 1) {
			monthName = "February";
		} else if (Integer.parseInt(ajaxHandlerAction.getMonth()) == 2) {
			monthName = "March";
		} else if (Integer.parseInt(ajaxHandlerAction.getMonth()) == 3) {
			monthName = "April";
		} else if (Integer.parseInt(ajaxHandlerAction.getMonth()) == 4
				&& ("2016".equalsIgnoreCase(ajaxHandlerAction.getYear()))) {
			monthName = "may";
		} else if (Integer.parseInt(ajaxHandlerAction.getMonth()) == 4
				&& (!"2016".equalsIgnoreCase(ajaxHandlerAction.getYear()))) {
			monthName = "May";
		} else if (Integer.parseInt(ajaxHandlerAction.getMonth()) == 5
				&& ("2016".equalsIgnoreCase(ajaxHandlerAction.getYear()))) {
			monthName = "june";
		} else if (Integer.parseInt(ajaxHandlerAction.getMonth()) == 5
				&& (!"2016".equalsIgnoreCase(ajaxHandlerAction.getYear()))) {
			monthName = "June";
		} else if (Integer.parseInt(ajaxHandlerAction.getMonth()) == 6) {
			monthName = "July";
		} else if (Integer.parseInt(ajaxHandlerAction.getMonth()) == 7) {
			monthName = "August";
		} else if (Integer.parseInt(ajaxHandlerAction.getMonth()) == 8) {
			monthName = "September";
		} else if (Integer.parseInt(ajaxHandlerAction.getMonth()) == 9) {
			monthName = "October";
		} else if (Integer.parseInt(ajaxHandlerAction.getMonth()) == 10
				&& ("2016".equalsIgnoreCase(ajaxHandlerAction.getYear()))) {
			monthName = "november";
		} else if (Integer.parseInt(ajaxHandlerAction.getMonth()) == 10
				&& (!"2016".equalsIgnoreCase(ajaxHandlerAction.getYear()))) {
			monthName = "November";
		} else if (Integer.parseInt(ajaxHandlerAction.getMonth()) == 11
				&& ("2016".equalsIgnoreCase(ajaxHandlerAction.getYear()))) {
			monthName = "december";
		} else if (Integer.parseInt(ajaxHandlerAction.getMonth()) == 11
				&& (!"2016".equalsIgnoreCase(ajaxHandlerAction.getYear()))) {
			monthName = "December";
		}
		basePath = basePath + "//" + ajaxHandlerAction.getYear() + "//" + monthName;
		File file = new File(basePath);
		File[] listFiles = file.listFiles();

		if (listFiles != null) {
			Arrays.sort(listFiles, LastModifiedFileComparator.LASTMODIFIED_REVERSE);
			for (File files : listFiles) {
				response = response + files.getName() + "*@!";
			}
		}
		return response;
	}

	/* News Letters Deployment End */
	public String getBDMStatistics(String bdmId, String startDate, String endDate) throws ServiceLocatorException {

		String response = "";
		// Connection connection = null;
		CallableStatement callableStatement = null;
		Connection connection = null;

		try {
			connection = ConnectionProvider.getInstance().getConnection();
			callableStatement = connection.prepareCall("{call spBDMStatisticsByActivityType(?,?,?,?)}");
			Map bdmLoginIdsMap = new HashMap();
			if (bdmId.equalsIgnoreCase("All")) {
				bdmLoginIdsMap = DataSourceDataProvider.getInstance().getAllBDMLoginIds();
				bdmId = getKeys(bdmLoginIdsMap, ",");
				bdmId = bdmId.replaceAll("'", "");

			}
			callableStatement.setString(1, bdmId);

			if (!"".equalsIgnoreCase(startDate)) {
				callableStatement.setString(2,
						com.mss.mirage.util.DateUtility.getInstance().convertStringToMySQLDate(startDate));
			} else {
				callableStatement.setString(2, "");
			}
			if (!"".equalsIgnoreCase(endDate)) {
				callableStatement.setString(3,
						com.mss.mirage.util.DateUtility.getInstance().convertStringToMySQLDate(endDate));
			} else {
				callableStatement.setString(3, "");
			}
			callableStatement.registerOutParameter(4, Types.VARCHAR);
			callableStatement.execute();

			response = callableStatement.getString(4);

		} catch (SQLException se) {
			se.printStackTrace();
		} finally {
			try {

				if (callableStatement != null) {
					callableStatement.close();
					callableStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		return response;
	}

	public String getBdmStatisticsDetailsByLoginId(String bdmId, String startDate, String endDate, String activityType)
			throws ServiceLocatorException {

		String response = "";
		// Connection connection = null;
		CallableStatement callableStatement = null;
		Connection connection = null;

		try {
			connection = ConnectionProvider.getInstance().getConnection();
			callableStatement = connection.prepareCall("{call spBDMStatisticsDetailsByActivityType(?,?,?,?,?)}");
			Map bdmLoginIdsMap = new HashMap();
			if (bdmId.equalsIgnoreCase("All")) {
				bdmLoginIdsMap = DataSourceDataProvider.getInstance().getAllBDMLoginIds();
				bdmId = getKeys(bdmLoginIdsMap, ",");
				bdmId = bdmId.replaceAll("'", "");

			}
			callableStatement.setString(1, bdmId);
			if (!"".equalsIgnoreCase(startDate)) {
				callableStatement.setString(2,
						com.mss.mirage.util.DateUtility.getInstance().convertStringToMySQLDate(startDate));
			} else {
				callableStatement.setString(2, "");
			}
			if (!"".equalsIgnoreCase(endDate)) {
				callableStatement.setString(3,
						com.mss.mirage.util.DateUtility.getInstance().convertStringToMySQLDate(endDate));
			} else {
				callableStatement.setString(3, "");
			}

			callableStatement.setString(4, activityType);
			callableStatement.registerOutParameter(5, Types.VARCHAR);
			callableStatement.execute();

			response = callableStatement.getString(5);

		} catch (SQLException se) {
			se.printStackTrace();
		} finally {
			try {

				if (callableStatement != null) {
					callableStatement.close();
					callableStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		return response;
	}

	private String getKeys(Map map, String delimiter) {
		Iterator tempIterator = map.entrySet().iterator();
		String keys = "";
		String key = "";
		int cnt = 0;

		while (tempIterator.hasNext()) {
			Map.Entry entry = (Map.Entry) tempIterator.next();
			key = entry.getKey().toString();
			entry = null;

			// Add the Delimiter to the Keys Field for the Second Key onwards
			if (cnt > 0) {
				keys = keys + delimiter;
			}

			keys = keys + "'" + key + "'";
			cnt++;
		}
		tempIterator = null;
		return (keys);
	}

	public String getActivityContacts(String activityId) throws ServiceLocatorException {
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		Connection connection = null;
		String ContactNames = "";
		try {
			connection = ConnectionProvider.getInstance().getConnection();
			// SELECT Comments FROM tblEmpIssues WHERE Id=810
			preparedStatement = connection
					.prepareStatement("SELECT tblCrmActivity.ContactId FROM tblCrmActivity WHERE Id=?");
			preparedStatement.setString(1, activityId);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {

				ContactNames = DataSourceDataProvider.getInstance()
						.getContactNamesByIdsForBdm(resultSet.getString("ContactId"));

			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		} catch (ServiceLocatorException sle) {
			sle.printStackTrace();
		} finally {
			try {

				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
		}

		return ContactNames;
	}

	public String getRequirementOtherDetails(int reqId) throws ServiceLocatorException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		String totalStream = "";
		int i = 0;
		try {
			String query = "SELECT AssignedDate,NoResumes AS NoOfPos,tblRec.CreatedDate,RequirementId,CONCAT(FName,' ',MName,'.',LName) AS CreatedBy,AssignedTo  FROM tblRecRequirement LEFT OUTER JOIN tblRec ON (tblRecRequirement.Id=tblRec.RequirementId)  LEFT OUTER JOIN tblEmployee ON (tblRecRequirement.CreatedBy=tblEmployee.LoginId) WHERE tblRecRequirement.Id=  "
					+ reqId;

			// System.out.println("query-->"+query);
			connection = ConnectionProvider.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(query);
			resultSet = preparedStatement.executeQuery();
			String AssignedDate = "-";
			String NoOfPos = "-";
			String NoOfResumesSubmitted = "-";
			String CreatedDate = "-";
			String CreatedBy = "-";
			String AssignedTo = "-";
			while (resultSet.next()) {
				if (resultSet.getString("AssignedDate") != null) {
					AssignedDate = resultSet.getString("AssignedDate");
				}
				if (resultSet.getString("NoOfPos") != null) {
					NoOfPos = resultSet.getString("NoOfPos");
				}
				// if(resultSet.getString("NoOfResumesSubmitted")!=null)
				// NoOfResumesSubmitted =
				// resultSet.getString("NoOfResumesSubmitted");
				if (resultSet.getString("CreatedDate") != null) {
					CreatedDate = resultSet.getString("CreatedDate");
					i++;
				}

				if (resultSet.getString("CreatedBy") != null) {
					CreatedBy = resultSet.getString("CreatedBy");
				}

				if (resultSet.getString("AssignedTo") != null) {
					AssignedTo = resultSet.getString("AssignedTo");
				}

			}

			totalStream = AssignedDate + "@" + NoOfPos + "@" + i + "@" + CreatedDate + "@" + CreatedBy + "@"
					+ AssignedTo;
		} catch (Exception e) {
			System.err.println("Exception is-->" + e.getMessage());
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (Exception sqle) {
				System.err.println("SQL Exception is-->" + sqle.getMessage());
			}
		}
		return totalStream;
	}

	/*
	 * author: harsha this method is for getting number of contacts related to
	 * activities of related accounts
	 * 
	 * used: in admin role {Executive dashbords under customer contacts}
	 */
	public String getCustomerContacts(String customerName, String teamMemberId, String startDate, String endDate,
			int teamMemberCheck, String titleType) throws ServiceLocatorException {

		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		PreparedStatement preparedStatement1 = null;
		ResultSet resultSet1 = null;
		Connection connection = null;
		String result = "";
		String query = "";
		try {
			connection = ConnectionProvider.getInstance().getConnection();

			String empNamesList = "'" + teamMemberId + "'";
			if (teamMemberCheck == 1) {
				String departmentId = DataSourceDataProvider.getInstance().getDepartmentName(teamMemberId);
				Map teamMap = DataSourceDataProvider.getInstance().getMyTeamMembers(teamMemberId, departmentId);
				String teamKeys = getKeys(teamMap, ",");

				// if(empNamesList.equalsIgnoreCase(""))
				if (!"".equals(teamKeys)) {
					empNamesList = empNamesList + ',' + getKeys(teamMap, ",");
				}

			}

			if ("BDM".equals(titleType) || "Vice President".equalsIgnoreCase(titleType)) {
				query = "Select tblCrmAccount.Id AS Id,tblCrmAccount.Name As Name from tblCrmAccount where tblCrmAccount.Name LIKE ? And tblCrmAccount.Id in (Select AccountId from tblCrmAccountTeam Where tblCrmAccountTeam.TeamMemberId In("
						+ empNamesList + ") )  ORDER BY tblCrmAccount.Name Limit 50;";
			} else {
				query = "Select tblCrmAccount.Id AS Id,tblCrmAccount.Name As Name from tblCrmAccount where  tblCrmAccount.Name LIKE ?  ORDER BY tblCrmAccount.Name Limit 50";
			}

			preparedStatement1 = connection.prepareStatement(query);

			preparedStatement1.setString(1, customerName + "%");
			resultSet1 = preparedStatement1.executeQuery();

			Map accountMap = new HashMap();

			while (resultSet1.next()) {
				accountMap.put(resultSet1.getString("Name"), resultSet1.getString("Id"));

			}

			List accountList = new ArrayList(accountMap.values());
			String customerId = "";
			for (int n = 0; n < accountList.size(); n++) {
				customerId = customerId + accountList.get(n) + ",";

			}

			if (customerId.trim().length() > 0) {

				customerId = customerId.substring(0, customerId.length() - 1);

				StringBuffer queryStringBuffer = new StringBuffer(
						"Select AccountId,ContactId,tblCrmAccount.Name As Name from tblCrmAccount right join tblCrmActivity  on (tblCrmActivity.AccountId=tblCrmAccount.Id) where  tblCrmAccount.Id in("
								+ customerId + ")");

				if (!"BDM".equals(titleType) && !"Vice President".equalsIgnoreCase(titleType)) {

					if (teamMemberId != null && !"All".equals(teamMemberId)) {
						queryStringBuffer.append(" AND tblCrmActivity.CreatedById IN (" + empNamesList + ") ");
					}
				}

				if (startDate != null && !"".equals(startDate)) {
					queryStringBuffer.append(" AND DATE(tblCrmActivity.CreatedDate)>=DATE('"
							+ DateUtility.getInstance().convertStringToMySQLDate(startDate) + "') ");
				}

				if (endDate != null && !"".equals(endDate)) {
					queryStringBuffer.append(" AND DATE(tblCrmActivity.CreatedDate)<=DATE('"
							+ DateUtility.getInstance().convertStringToMySQLDate(endDate) + "') ");
				}
				queryStringBuffer.append(" And ContactId!=0  ORDER BY tblCrmAccount.Name");

				// System.out.println("queryStringBuffer.toString()"+queryStringBuffer.toString());
				preparedStatement = connection.prepareStatement(queryStringBuffer.toString());

				resultSet = preparedStatement.executeQuery();

				Map map = new HashMap();
				Set s = null;

				while (resultSet.next()) {

					String con[] = resultSet.getString("ContactId").split(",");

					for (int j = 0; j < con.length; j++) {

						if (!"0".equalsIgnoreCase(con[j])) {

							if (map.containsKey(resultSet.getString("Name"))) {

								if (s.add(con[j])) {
									int i = (Integer) map.get(resultSet.getString("Name"));

									map.put(resultSet.getString("Name"), (i + 1));

								}

							} else {

								s = new HashSet();
								if (s.add(con[j])) {

									map.put(resultSet.getString("Name"), 1);

								}

							}
						}
					}

				}
				// System.out.println("ContactNames..res.."+map.entrySet());

				Map activeContactMap = DataSourceDataProvider.getInstance()
						.getActiveIndividualCustomerContacts(customerId);

				Iterator iterator = accountMap.entrySet().iterator();
				while (iterator.hasNext()) {
					Map.Entry entry = (Map.Entry) iterator.next();
					result = result + entry.getKey() + "#^$";
					if (map.containsKey(entry.getKey())) {
						result = result + map.get(entry.getKey().toString()) + "#^$";
					} else {
						result = result + 0 + "#^$";
					}
					if (activeContactMap.containsKey(entry.getKey())) {
						result = result + activeContactMap.get(entry.getKey().toString()) + "#^$";
					} else {
						result = result + 0 + "#^$";
					}
					result = result + entry.getValue() + "*@!";
				}
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		} catch (ServiceLocatorException sle) {
			sle.printStackTrace();
		} finally {
			try {

				if (resultSet1 != null) {
					resultSet1.close();
					resultSet1 = null;
				}
				if (preparedStatement1 != null) {
					preparedStatement1.close();
					preparedStatement1 = null;
				}
				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
		}

		return result;
	}

	/*
	 * author: harsha this method is for getting all activities for account
	 * 
	 * used : in admin role {Executive DashBords under customer contacts}
	 */
	public String getContactActivities(String customerId, String teamMemberId, String startDate, String endDate,
			int teamMemberCheck, String titleType) throws ServiceLocatorException {

		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		Connection connection = null;
		String result = "";
		try {
			connection = ConnectionProvider.getInstance().getConnection();

			StringBuffer queryStringBuffer = new StringBuffer(
					"Select ActivityType,Priority,ContactId,Status,Id As ActivityId,CreatedDate,CreatedById from tblCrmActivity where AccountId="
							+ customerId);

			if (!"BDM".equalsIgnoreCase(titleType) && !"Vice President".equalsIgnoreCase(titleType)) {

				String empNamesList = "'" + teamMemberId + "'";
				if (teamMemberCheck == 1) {
					String departmentId = DataSourceDataProvider.getInstance().getDepartmentName(teamMemberId);
					Map teamMap = DataSourceDataProvider.getInstance().getMyTeamMembers(teamMemberId, departmentId);
					String teamKeys = getKeys(teamMap, ",");

					// if(empNamesList.equalsIgnoreCase(""))
					if (!"".equals(teamKeys)) {
						empNamesList = empNamesList + ',' + getKeys(teamMap, ",");
					}

				}

				// System.out.println("empNamesList"+empNamesList);

				if (teamMemberId != null && !"All".equals(teamMemberId)) {
					queryStringBuffer.append(" AND tblCrmActivity.CreatedById IN (" + empNamesList + ") ");
				}
			}

			if (startDate != null && !"".equals(startDate)) {
				queryStringBuffer.append(" AND DATE(tblCrmActivity.CreatedDate)>=DATE('"
						+ DateUtility.getInstance().convertStringToMySQLDate(startDate) + "') ");
			}

			if (endDate != null && !"".equals(endDate)) {
				queryStringBuffer.append(" AND DATE(tblCrmActivity.CreatedDate)<=DATE('"
						+ DateUtility.getInstance().convertStringToMySQLDate(endDate) + "') ");
			}
			queryStringBuffer.append(" And ContactId!=0 ORDER BY tblCrmActivity.CreatedDate");

			// System.out.println("queryStringBuffer.toString()"+queryStringBuffer.toString());
			preparedStatement = connection.prepareStatement(queryStringBuffer.toString());

			resultSet = preparedStatement.executeQuery();

			String ContactNames = "";
			while (resultSet.next()) {

				if (!"".equals(resultSet.getString("ContactId"))) {
					ContactNames = DataSourceDataProvider.getInstance()
							.getContactNamesByIds(resultSet.getString("ContactId"));
				} else {
					ContactNames = "-";
				}

				result = result + "#^$" + resultSet.getString("CreatedById") + "#^$"
						+ resultSet.getString("ActivityType") + "#^$" + resultSet.getString("Status") + "#^$"
						+ resultSet.getString("Priority") + "#^$" + resultSet.getString("CreatedDate") + "#^$"
						+ ContactNames + "#^$" + resultSet.getString("ActivityId") + "*@!";

			}

		} catch (SQLException ex) {
			ex.printStackTrace();
		} catch (ServiceLocatorException sle) {
			sle.printStackTrace();
		} finally {
			try {

				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
		}

		return result;
	}

	public String reqOverviewPieChart(HttpServletRequest httpServletRequest, NewAjaxHandlerAction ajaxHandlerAction) {

		PreparedStatement preparedStatement = null;
		// CallableStatement callableStatement = null;
		ResultSet resultSet = null;
		Connection connection = null;
		String response = "";
		String query = "";
		try {
			connection = ConnectionProvider.getInstance().getConnection();

			query = "SELECT COUNT(DISTINCT(tblRec.ConsultantId)) AS COUNT,tblRec.STATUS AS STATUS FROM tblRecRequirement LEFT OUTER JOIN tblCrmAccount ON (tblRecRequirement.CustomerId=tblCrmAccount.Id)"
					+ "LEFT OUTER JOIN tblRec ON (tblRecRequirement.Id=tblRec.RequirementId)"
					+ " LEFT OUTER JOIN tblRecConsultantActivity ON"
					+ "(tblRecRequirement.Id = tblRecConsultantActivity.RequirementId)";

			query = query + "WHERE 1=1 ";

			if (!"All".equals(ajaxHandlerAction.getCreatedBy())) {
				query = query + " AND tblRecRequirement.CreatedBy='" + ajaxHandlerAction.getCreatedBy() + "' ";
			}
			if (ajaxHandlerAction.getTitle() != null && !"".equals(ajaxHandlerAction.getTitle())) {
				query = query + " AND `tblRecRequirement`.`JobTitle` LIKE '%" + ajaxHandlerAction.getTitle() + "%'";
			}

			if (ajaxHandlerAction.getPostedDate1() != null && !"".equals(ajaxHandlerAction.getPostedDate1())) {
				query = query + " AND DATE(`tblRecRequirement`.`DatePosted`) >= DATE('"
						+ DateUtility.getInstance().convertStringToMySQLDate(ajaxHandlerAction.getPostedDate1()) + "')";
			}

			if (ajaxHandlerAction.getPostedDate2() != null && !"".equals(ajaxHandlerAction.getPostedDate2())) {
				query = query + " AND DATE(`tblRecRequirement`.`DatePosted`) <= DATE('"
						+ DateUtility.getInstance().convertStringToMySQLDate(ajaxHandlerAction.getPostedDate2()) + "')";
			}

			if (!"All".equals(ajaxHandlerAction.getStatus())) {
				query = query + " AND `tblRecRequirement`.`Status` LIKE '" + ajaxHandlerAction.getStatus() + "'";
			}
			if (!"-1".equals(ajaxHandlerAction.getCountry())) {
				query = query + " AND `tblRecRequirement`.`Country` like '" + ajaxHandlerAction.getCountry() + "' ";
			}

			if (ajaxHandlerAction.getCustomerName() != null && !"".equals(ajaxHandlerAction.getCustomerName())) {
				query = query + " AND `tblCrmAccount`.`Name` LIKE '%" + ajaxHandlerAction.getCustomerName() + "%'";
			}

			if (ajaxHandlerAction.getState() != null && !"".equals(ajaxHandlerAction.getState())) {
				query = query + " AND `tblRecRequirement`.`state` like '" + ajaxHandlerAction.getState() + "' ";
			}

			if (!"-1".equals(ajaxHandlerAction.getPracticeid())) {
				query = query + "  AND tblRecRequirement.Practice like '" + ajaxHandlerAction.getPracticeid() + "' ";
			}
			// if (ajaxHandlerAction.getRequirementId() != 0) {
			if (ajaxHandlerAction.getReqId() != null && !"".equals(ajaxHandlerAction.getReqId())) {
				query = query + "  AND tblRecRequirement .Id =" + ajaxHandlerAction.getReqId() + " ";
			}
			if (ajaxHandlerAction.getPreSalesPerson() != null && !"".equals(ajaxHandlerAction.getPreSalesPerson())) {
				query = query + " AND (tblRecRequirement.AssignToTechLead ='" + ajaxHandlerAction.getPreSalesPerson()
						+ "' || tblRecRequirement.SecondaryTechLead ='" + ajaxHandlerAction.getPreSalesPerson() + "') ";
			} else {
				Map rolesMap = (Map) httpServletRequest.getSession(false)
						.getAttribute(ApplicationConstants.SESSION_MY_ROLES);
				List finalTeachLeadList;
				if (rolesMap.containsValue("Admin") || AuthorizationManager.getInstance()
						.isAuthorizedForSurveyForm("PRESALES_REQUIREMENT_ACCESS", httpServletRequest.getSession(false)
								.getAttribute(ApplicationConstants.SESSION_USER_ID).toString())) {

					// finalTeachLeadList =
					// DataSourceDataProvider.getInstance().getTechLead();
					finalTeachLeadList = DataSourceDataProvider.getInstance()
							.getListFromMap(DataSourceDataProvider.getInstance().getTechLead());
				} else {
					Map teamMap = (Map) httpServletRequest.getSession(false)
							.getAttribute(ApplicationConstants.SESSION_MY_TEAM_MAP);
					// List TechLeadList =
					// DataSourceDataProvider.getInstance().getTechLead();
					List TechLeadList = DataSourceDataProvider.getInstance()
							.getListFromMap(DataSourceDataProvider.getInstance().getTechLead());
					List tempTechLeadList = new ArrayList();
					for (int j = 0; j < TechLeadList.size(); j++) {
						if (teamMap.containsValue(TechLeadList.get(j))) {
							tempTechLeadList.add(TechLeadList.get(j));
						}
					}
					// tempTechLeadList.add(httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_NAME));
					tempTechLeadList.add(
							httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID));

					finalTeachLeadList = tempTechLeadList;
				}
				String teamList = DataSourceDataProvider.getInstance().getStringByList(finalTeachLeadList);

				query = query + " AND (FIND_IN_SET(tblRecRequirement.AssignToTechLead,'" + teamList
						+ "' )  || FIND_IN_SET(tblRecRequirement.SecondaryTechLead,'" + teamList + "' ) )";

			}

			query = query
					+ " AND tblRec.STATUS IS NOT NULL AND tblRec.STATUS !='' AND tblRec.STATUS != '-1' GROUP BY tblRec.STATUS";

			// System.out.println("query........"+query);
			preparedStatement = connection.prepareStatement(query);

			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {

				response = response + resultSet.getString("status") + "#^$";
				response = response + resultSet.getInt("count") + "*@!";

			}
			if (response.equals("")) {
				response = "NoData";
			}
			// System.out.println("resposnse...."+response);
		} catch (SQLException ex) {
			ex.printStackTrace();
		} catch (ServiceLocatorException sle) {
			sle.printStackTrace();
		} finally {
			try {

				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
		}
		return response;
	}

	@Override
	public String getRequirementOverviewDetails(HttpServletRequest httpServletRequest,
			NewAjaxHandlerAction ajaxHandlerAction) {
		Connection connection = null;
		StringBuffer stringBuffer = new StringBuffer();
		CallableStatement callableStatement = null;
		PreparedStatement preparedStatement = null;
		Statement statement = null;
		Statement statement1 = null;
		ResultSet resultSet = null;
		ResultSet resultSet1 = null;
		String createdBy = "";
		String totalStream = "";
		String queryString = "";
		int i = 0;
		int totalRecords = 0;

		String response = "";

		// resultType = "accessFailed";

		DateUtility dateUtility = new DateUtility();

		// System.out.println(" impl ---
		// createdBy----------"+ajaxHandlerAction.getCreatedBy()+"--assignedTo-------------"+ajaxHandlerAction.getAssignedTo()+"--title-------------"+ajaxHandlerAction.getTitle()+"--postedDate1-------"+ajaxHandlerAction.getPostedDate1()+"---postedDate2----"+ajaxHandlerAction.getPostedDate2()+"---status----"+ajaxHandlerAction.getStatus());

		try {
			connection = ConnectionProvider.getInstance().getConnection();
			if ("1".equals(ajaxHandlerAction.getPgFlag().trim())) {

				StringBuffer queryStringBuf = new StringBuffer();
				;

				statement1 = connection.createStatement();
				queryStringBuf.append(
						"Select COUNT(id) As count from (SELECT DISTINCT(tblRec.ConsultantId) As id FROM tblRecRequirement LEFT OUTER JOIN tblCrmAccount ON (tblRecRequirement.CustomerId=tblCrmAccount.Id) LEFT OUTER JOIN tblRec ON (tblRecRequirement.Id=tblRec.RequirementId)  LEFT OUTER JOIN tblRecConsultantActivity ON (tblRecRequirement.Id = tblRecConsultantActivity.RequirementId) ");

				queryStringBuf.append(" WHERE tblRec.STATUS = '" + ajaxHandlerAction.getState() + "' ");

				if (!"All".equals(ajaxHandlerAction.getCreatedBy())) {
					queryStringBuf
							.append(" AND tblRecRequirement.CreatedBy='" + ajaxHandlerAction.getCreatedBy() + "' ");
				}
				if (ajaxHandlerAction.getTitle() != null && !"".equals(ajaxHandlerAction.getTitle())) {
					queryStringBuf.append(
							" AND `tblRecRequirement`.`JobTitle` LIKE '%" + ajaxHandlerAction.getTitle() + "%'");
				}

				if (ajaxHandlerAction.getPostedDate1() != null && !"".equals(ajaxHandlerAction.getPostedDate1())) {
					queryStringBuf.append(" AND DATE(`tblRecRequirement`.`DatePosted`) >= DATE('"
							+ DateUtility.getInstance().convertStringToMySQLDate(ajaxHandlerAction.getPostedDate1())
							+ "')");
				}

				if (ajaxHandlerAction.getPostedDate2() != null && !"".equals(ajaxHandlerAction.getPostedDate2())) {
					queryStringBuf.append(" AND DATE(`tblRecRequirement`.`DatePosted`) <= DATE('"
							+ DateUtility.getInstance().convertStringToMySQLDate(ajaxHandlerAction.getPostedDate2())
							+ "')");
				}

				if (!"All".equals(ajaxHandlerAction.getStatus())) {
					queryStringBuf
							.append(" AND `tblRecRequirement`.`Status` LIKE '" + ajaxHandlerAction.getStatus() + "'");
				}
				if (!"-1".equals(ajaxHandlerAction.getCountry())) {
					queryStringBuf.append(
							" AND `tblRecRequirement`.`Country` like '" + ajaxHandlerAction.getCountry() + "' ");
				}

				if (ajaxHandlerAction.getCustomerName() != null && !"".equals(ajaxHandlerAction.getCustomerName())) {
					queryStringBuf
							.append(" AND `tblCrmAccount`.`Name` LIKE '%" + ajaxHandlerAction.getCustomerName() + "%'");
				}

				if (ajaxHandlerAction.getStatesFromOrg() != null && !"".equals(ajaxHandlerAction.getStatesFromOrg())) {
					queryStringBuf.append(
							" AND `tblRecRequirement`.`state` like '" + ajaxHandlerAction.getStatesFromOrg() + "' ");
				}

				if (!"-1".equals(ajaxHandlerAction.getPracticeid())) {
					queryStringBuf.append(
							"  AND tblRecRequirement.Practice like '" + ajaxHandlerAction.getPracticeid() + "' ");
				}
				// if (ajaxHandlerAction.getRequirementId() != 0) {
				if (ajaxHandlerAction.getReqId() != null && !"".equals(ajaxHandlerAction.getReqId())) {

					queryStringBuf.append("  AND tblRecRequirement .Id =" + ajaxHandlerAction.getReqId() + " ");
				}
				if (ajaxHandlerAction.getPreSalesPerson() != null
						&& !"".equals(ajaxHandlerAction.getPreSalesPerson())) {
					queryStringBuf.append(" AND (tblRecRequirement.AssignToTechLead ='"
							+ ajaxHandlerAction.getPreSalesPerson() + "' || tblRecRequirement.SecondaryTechLead ='"
							+ ajaxHandlerAction.getPreSalesPerson() + "') ");
				} else {
					Map rolesMap = (Map) httpServletRequest.getSession(false)
							.getAttribute(ApplicationConstants.SESSION_MY_ROLES);
					List finalTeachLeadList;
					if (rolesMap.containsValue("Admin") || AuthorizationManager.getInstance()
							.isAuthorizedForSurveyForm("PRESALES_REQUIREMENT_ACCESS", httpServletRequest
									.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID).toString())) {

						// finalTeachLeadList =
						// DataSourceDataProvider.getInstance().getTechLead();
						finalTeachLeadList = DataSourceDataProvider.getInstance()
								.getListFromMap(DataSourceDataProvider.getInstance().getTechLead());
					} else {
						Map teamMap = (Map) httpServletRequest.getSession(false)
								.getAttribute(ApplicationConstants.SESSION_MY_TEAM_MAP);
						// List TechLeadList =
						// DataSourceDataProvider.getInstance().getTechLead();
						List TechLeadList = DataSourceDataProvider.getInstance()
								.getListFromMap(DataSourceDataProvider.getInstance().getTechLead());
						List tempTechLeadList = new ArrayList();
						for (int j = 0; j < TechLeadList.size(); j++) {
							if (teamMap.containsValue(TechLeadList.get(j))) {
								tempTechLeadList.add(TechLeadList.get(j));
							}
						}
						// tempTechLeadList.add(httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_NAME));
						tempTechLeadList.add(httpServletRequest.getSession(false)
								.getAttribute(ApplicationConstants.SESSION_USER_ID));

						finalTeachLeadList = tempTechLeadList;
					}
					String teamList = DataSourceDataProvider.getInstance().getStringByList(finalTeachLeadList);

					queryStringBuf.append(" AND (FIND_IN_SET(tblRecRequirement.AssignToTechLead,'" + teamList
							+ "' )  || FIND_IN_SET(tblRecRequirement.SecondaryTechLead,'" + teamList + "' ) )");

				}
				queryStringBuf.append(" GROUP BY tblRec.ConsultantId  ");
				queryStringBuf.append(" ORDER BY `tblRecRequirement`.`DatePosted` DESC) As tbl");

				// System.out.println("REQ_SEARCH_QUERY --->" +
				// queryStringBuf.toString());
				// preparedStatement =
				// connection.prepareStatement(queryStringBuf.toString());

				// resultSet = preparedStatement.executeQuery();

				resultSet = statement1.executeQuery(queryStringBuf.toString());
				while (resultSet.next()) {
					totalRecords = resultSet.getInt("count");
				}

			}

			// for getting total counts (for pagination) ...> End

			callableStatement = connection.prepareCall("{call spRequirementListDetails(?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");

			if (!"All".equals(ajaxHandlerAction.getCreatedBy())) {
				callableStatement.setString(1, ajaxHandlerAction.getCreatedBy());
			} else {
				callableStatement.setString(1, "%");
			}
			if (ajaxHandlerAction.getTitle() != null && !"".equals(ajaxHandlerAction.getTitle())) {
				callableStatement.setString(2, "%" + ajaxHandlerAction.getTitle() + "%");
			} else {
				callableStatement.setString(2, "%");
			}
			if (ajaxHandlerAction.getPostedDate1() != null && !"".equals(ajaxHandlerAction.getPostedDate1())) {
				callableStatement.setString(3,
						DateUtility.getInstance().convertStringToMySQLDate(ajaxHandlerAction.getPostedDate1()));
			} else {
				callableStatement.setString(3, "1950-01-31");
			}
			if (ajaxHandlerAction.getPostedDate1() != null && !"".equals(ajaxHandlerAction.getPostedDate1())) {
				callableStatement.setString(4,
						DateUtility.getInstance().convertStringToMySQLDate(ajaxHandlerAction.getPostedDate2()));
			} else {
				callableStatement.setString(4, "2030-01-31");
			}
			if (!"All".equals(ajaxHandlerAction.getStatus())) {
				callableStatement.setString(5, ajaxHandlerAction.getStatus());
			} else {
				callableStatement.setString(5, "%");
			}
			if (!"-1".equals(ajaxHandlerAction.getCountry())) {
				callableStatement.setString(6, ajaxHandlerAction.getCountry());
			} else {
				callableStatement.setString(6, "%");
			}
			if (ajaxHandlerAction.getCustomerName() != null && !"".equals(ajaxHandlerAction.getCustomerName())) {
				callableStatement.setString(7, "%" + ajaxHandlerAction.getCustomerName() + "%");
			} else {
				callableStatement.setString(7, "%");
			}
			if (ajaxHandlerAction.getStatesFromOrg() != null && !"".equals(ajaxHandlerAction.getStatesFromOrg())) {
				callableStatement.setString(8, ajaxHandlerAction.getStatesFromOrg());
			} else {
				callableStatement.setString(8, "%");
			}
			if (!"-1".equals(ajaxHandlerAction.getPracticeid())) {
				callableStatement.setString(9, ajaxHandlerAction.getPracticeid());
			} else {
				callableStatement.setString(9, "%");
			}
			if (ajaxHandlerAction.getReqId() != null && !"".equals(ajaxHandlerAction.getReqId())) {
				callableStatement.setString(10, ajaxHandlerAction.getReqId());
			} else {
				callableStatement.setString(10, "%");
			}
			if (ajaxHandlerAction.getPreSalesPerson() != null && !"".equals(ajaxHandlerAction.getPreSalesPerson())) {
				callableStatement.setString(11, ajaxHandlerAction.getPreSalesPerson());
			} else {
				Map rolesMap = (Map) httpServletRequest.getSession(false)
						.getAttribute(ApplicationConstants.SESSION_MY_ROLES);
				List finalTeachLeadList;
				if (rolesMap.containsValue("Admin") || AuthorizationManager.getInstance()
						.isAuthorizedForSurveyForm("PRESALES_REQUIREMENT_ACCESS", httpServletRequest.getSession(false)
								.getAttribute(ApplicationConstants.SESSION_USER_ID).toString())) {

					// finalTeachLeadList =
					// DataSourceDataProvider.getInstance().getTechLead();
					finalTeachLeadList = DataSourceDataProvider.getInstance()
							.getListFromMap(DataSourceDataProvider.getInstance().getTechLead());
				} else {
					Map teamMap = (Map) httpServletRequest.getSession(false)
							.getAttribute(ApplicationConstants.SESSION_MY_TEAM_MAP);
					// List TechLeadList =
					// DataSourceDataProvider.getInstance().getTechLead();
					List TechLeadList = DataSourceDataProvider.getInstance()
							.getListFromMap(DataSourceDataProvider.getInstance().getTechLead());
					List tempTechLeadList = new ArrayList();
					for (int j = 0; j < TechLeadList.size(); j++) {
						if (teamMap.containsValue(TechLeadList.get(j))) {
							tempTechLeadList.add(TechLeadList.get(j));
						}
					}
					// tempTechLeadList.add(httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_NAME));
					tempTechLeadList.add(
							httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID));

					finalTeachLeadList = tempTechLeadList;
				}
				String teamList = DataSourceDataProvider.getInstance().getStringByList(finalTeachLeadList);
				callableStatement.setString(11, teamList);
			}
			if (ajaxHandlerAction.getState() != null && !"".equals(ajaxHandlerAction.getState())) {
				callableStatement.setString(12, ajaxHandlerAction.getState());
			} else {
				callableStatement.setString(12, "%");
			}

			// callableStatement.setString(13,ajaxHandlerAction.getPgFlag());
			callableStatement.setInt(13, (ajaxHandlerAction.getPgNo() - 1) * 20);
			// System.out.println("pgNo...."+(ajaxHandlerAction.getPgNo() - 1) *
			// 20);
			callableStatement.registerOutParameter(14, java.sql.Types.VARCHAR);

			callableStatement.execute();
			// response = callableStatement.getString(15);
			totalStream = callableStatement.getString(14);
			// System.out.println("totalStream....."+totalStream);
			// System.out.println("totalRecords....."+totalRecords);
			response = totalStream + "###" + totalRecords;

		} catch (SQLException ex) {
			ex.printStackTrace();
		} catch (ServiceLocatorException sle) {
			sle.printStackTrace();
		} finally {
			try {

				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
		}
		return response;
	}

	public String getInsideSalesStatusList(String bdeLoginId) throws ServiceLocatorException {

		String response = "";
		// Connection connection = null;
		CallableStatement callableStatement = null;
		Connection connection = null;

		try {
			connection = ConnectionProvider.getInstance().getConnection();
			callableStatement = connection.prepareCall("{call spInsideSalesStatusDashBoard(?,?)}");
			Map bdeLoginIdsMap = new HashMap();
			if (bdeLoginId.equalsIgnoreCase("All")) {
				bdeLoginIdsMap = DataSourceDataProvider.getInstance().getInsideSalesBDEByLoginId();
				bdeLoginId = getKeys(bdeLoginIdsMap, ",");
				bdeLoginId = bdeLoginId.replaceAll("'", "");

			}
			callableStatement.setString(1, bdeLoginId);

			callableStatement.registerOutParameter(2, Types.VARCHAR);

			callableStatement.execute();

			response = callableStatement.getString(2);

		} catch (SQLException se) {
			se.printStackTrace();
		} finally {
			try {

				if (callableStatement != null) {
					callableStatement.close();
					callableStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		return response;
	}

	public String getInsideSalesAccountDetailsList(HttpServletRequest httpServletRequest,
			NewAjaxHandlerAction ajaxHandlerAction) throws ServiceLocatorException {

		String response = "";
		// Connection connection = null;
		CallableStatement callableStatement = null;
		Connection connection = null;

		try {
			connection = ConnectionProvider.getInstance().getConnection();

			httpServletRequest.getSession(false).setAttribute("tempBdeLoginId", ajaxHandlerAction.getBdeLoginId());
			httpServletRequest.getSession(false).setAttribute("tempEmployeeName", ajaxHandlerAction.getEmployeeName());

			httpServletRequest.getSession(false).setAttribute("tempAccId", ajaxHandlerAction.getAccountId());
			if (!"".equalsIgnoreCase(ajaxHandlerAction.getLastActivityFrom())) {
				httpServletRequest.getSession(false).setAttribute("tempLastActivityFrom",
						ajaxHandlerAction.getLastActivityFrom());
			} else {
				httpServletRequest.getSession(false).setAttribute("tempLastActivityFrom", "");
			}
			if (!"".equalsIgnoreCase(ajaxHandlerAction.getLastActivityTo())) {
				httpServletRequest.getSession(false).setAttribute("tempLastActivityTo",
						ajaxHandlerAction.getLastActivityTo());
			} else {
				httpServletRequest.getSession(false).setAttribute("tempLastActivityTo", "");
			}

			httpServletRequest.getSession(false).setAttribute("tempOpportunity", ajaxHandlerAction.getOpportunity());
			httpServletRequest.getSession(false).setAttribute("tempTouched", ajaxHandlerAction.getTouched());

			callableStatement = connection.prepareCall("{call spInsideSalesAccountDetailsSearch(?,?,?,?,?,?,?)}");

			callableStatement.setString(1, ajaxHandlerAction.getBdeLoginId());
			callableStatement.setInt(2, ajaxHandlerAction.getAccountId());
			if (!"".equalsIgnoreCase(ajaxHandlerAction.getLastActivityFrom())) {
				callableStatement.setString(3, com.mss.mirage.util.DateUtility.getInstance()
						.convertStringToMySQLDate(ajaxHandlerAction.getLastActivityFrom()));
			} else {
				callableStatement.setString(3, "");
			}
			if (!"".equalsIgnoreCase(ajaxHandlerAction.getLastActivityTo())) {
				callableStatement.setString(4, com.mss.mirage.util.DateUtility.getInstance()
						.convertStringToMySQLDate(ajaxHandlerAction.getLastActivityTo()));
			} else {
				callableStatement.setString(4, "");
			}
			callableStatement.setString(5, ajaxHandlerAction.getOpportunity());
			callableStatement.setString(6, ajaxHandlerAction.getTouched());
			callableStatement.registerOutParameter(7, Types.VARCHAR);
			callableStatement.execute();

			response = callableStatement.getString(7);

		} catch (SQLException se) {
			se.printStackTrace();
		} finally {
			try {

				if (callableStatement != null) {
					callableStatement.close();
					callableStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		return response;
	}

	public String getInvestmentType(int invId) throws ServiceLocatorException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		String responseString = "";
		try {

			// System.out.println("invId......."+invId);
			String queryString1 = "SELECT InvestmentType FROM tblInvestments Where Inv_Id=?";
			connection = ConnectionProvider.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(queryString1);
			preparedStatement.setInt(1, invId);

			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				responseString = resultSet.getString("InvestmentType");
			}
			// System.out.println("responseString"+responseString);

		} catch (SQLException sql) {
			sql.printStackTrace();
			throw new ServiceLocatorException(sql);
		} finally {
			try {
				// resultSet Object Checking if it's null then close and set
				// null
				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}

				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}

				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException ex) {
				throw new ServiceLocatorException(ex);
			}
		}
		return responseString; // returning the object.
	}

	public String addLeadHystory(String loginId, Timestamp remainderDate, String followUpComments,
			String nextfollowUpComments, int leadId, String opt, int id, int reminderFlag)
			throws ServiceLocatorException {

		Connection connection = null;
		PreparedStatement preparedStatement = null;
		CallableStatement callableStatement = null;
		ResultSet resultSet = null;
		String resutMessage = "";
		boolean isInserted = false;

		// String queryString = "INSERT INTO `tblInvestments` ( `Inv_Name`,
		// `Country`, `StartDate`, `EndDate`,
		// `TotalExpenses`,`Currency`,`Location`,`Description`,`AttachmentFileName`,`AttachmentLocation`,`CreatedBy`,InvestmentType)"
		// + " VALUES(?,?,?,?,?,?,?,?,?,?,?,?)";
		String queryString = "{call spAddOrUpdateLeadHystory(?,?,?,?,?,?,?,?,?)}";

		try {
			connection = ConnectionProvider.getInstance().getConnection();

			callableStatement = connection.prepareCall(queryString);
			callableStatement.setInt(1, leadId);
			callableStatement.setString(2, loginId);
			callableStatement.setString(3, opt);
			callableStatement.setTimestamp(4, remainderDate);
			callableStatement.setString(5, followUpComments);
			callableStatement.setString(6, nextfollowUpComments);
			// System.out.println("iddd......." + id);
			callableStatement.setInt(7, id);
			callableStatement.setInt(8, reminderFlag);

			callableStatement.registerOutParameter(9, Types.VARCHAR);
			callableStatement.executeUpdate();
			resutMessage = "<font color=green>" + callableStatement.getString(9) + "</font>";

		} catch (SQLException se) {
			se.printStackTrace();
			resutMessage = "<font color=red>" + se.getMessage() + "</font>";
			throw new ServiceLocatorException(se);
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException se) {
				se.printStackTrace();
				throw new ServiceLocatorException(se);
			}
		}
		return resutMessage;
	}

	public String getLeadFollowUpHistoryDetails(int id) throws ServiceLocatorException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		String queryString = "";
		JSONObject subJson = null;
		try {
			queryString = "SELECT * FROM tblCrmLeadFollowupHistory WHERE Id=" + id;
			connection = ConnectionProvider.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(queryString);
			resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				subJson = new JSONObject();
				subJson.put("LeadId", resultSet.getString("LeadId"));
				subJson.put("Id", resultSet.getString("Id"));
				subJson.put("FollowUpComments", resultSet.getString("FollowUpComments"));
				subJson.put("FollowUpBy", resultSet.getString("FollowUpBy"));
				subJson.put("ReminderFlag", resultSet.getString("ReminderFlag"));
				// System.out.println("resultSet.getString(\"FollowUpBy\")"+resultSet.getString("FollowUpBy"));
				if (resultSet.getString("ReminderDate") != null && !"".equals(resultSet.getString("ReminderDate"))) {
					subJson.put("ReminderDate",
							DateUtility.getInstance().convertToviewFormat(resultSet.getString("ReminderDate")));
				} else {
					subJson.put("ReminderDate", "");
				}
				subJson.put("NextFollowUpSteps", resultSet.getString("NextFollowUpSteps"));

			}

		} catch (Exception sqe) {
			sqe.printStackTrace();
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException sqle) {
			}
		}
		return subJson.toString();
	}

	public String customerProjectDetailsDashboard(NewAjaxHandlerAction ajaxHandlerAction,
			HttpServletRequest httpServletRequest) throws ServiceLocatorException {
		String totalStream = "";
		Statement statement = null;
		String query = "";
		Connection connection = null;
		ResultSet resultSet = null;
		int i = 0;
		ajaxHandlerAction.setLoginId(
				(String) httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID));
		String userName = httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_NAME)
				.toString();

		// query = "SELECT
		// tblProjects.ProjectName,tblPmoAuthors.AccountId,tblProjects.ProjectStartDate,tblProjects.STATUS,
		// tblCrmAccount.NAME,tblPmoAuthors.ProjectId FROM tblPmoAuthors JOIN
		// tblProjects ON (tblProjects.Id=tblPmoAuthors.ProjectId) JOIN
		// tblCrmAccount ON (tblCrmAccount.Id = tblPmoAuthors.AccountId) WHERE
		// tblPmoAuthors.AuthorId = '" + ajaxHandlerAction.getLoginId() + "' AND
		// tblPmoAuthors.STATUS = 'Active' ";
		query = "SELECT DISTINCT  tblCrmAccount.Id  AS AccountId, tblCrmAccount.NAME  FROM tblProjects JOIN tblCrmAccount ON (tblCrmAccount.Id = tblProjects.CustomerId) LEFT JOIN tblProjectContacts ON (tblProjects.Id = tblProjectContacts.ProjectId ) WHERE 1=1 ";

		Map rolesMap = (Map) httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_MY_ROLES);
		if (rolesMap.containsValue("Admin") || httpServletRequest.getSession(false)
				.getAttribute(ApplicationConstants.SESSION_PMO_ACTIVITY_ACCESS).toString().equals("1")) {

			if (ajaxHandlerAction.getPmoLoginId() != null && !"".equalsIgnoreCase(ajaxHandlerAction.getPmoLoginId())) {
				query = query + " AND tblProjects.PMO = '" + ajaxHandlerAction.getPmoLoginId() + "'";
			}
		} else {
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_IS_TEAM_LEAD).toString()
					.equals("1")
					|| httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_IS_USER_MANAGER)
							.toString().equals("1")) {
				// Map teamMap = (Map)
				// httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_MY_TEAM_MAP);
				// teamMap.put(ajaxHandlerAction.getLoginId(),
				// httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_NAME).toString());
				Map teamMap = DataSourceDataProvider.getInstance()
						.getTeamMapWithCurrentUser(ajaxHandlerAction.getLoginId(), userName, (Map) httpServletRequest
								.getSession(false).getAttribute(ApplicationConstants.SESSION_MY_TEAM_MAP));
				if (!"".equalsIgnoreCase(ajaxHandlerAction.getPmoLoginId())) {
					query = query + " AND tblProjects.PMO = '" + ajaxHandlerAction.getPmoLoginId() + "'";
				} else {
					query = query + " AND tblProjects.PMO IN ("
							+ DataSourceDataProvider.getInstance().getTeamLoginIdList(teamMap) + ") ";
				}

			} else {
				query = query + " AND tblProjects.PMO = '" + ajaxHandlerAction.getLoginId() + "'";
			}
		}

		if (!"".equalsIgnoreCase(ajaxHandlerAction.getCustomerName())
				&& !"-1".equals(ajaxHandlerAction.getCustomerName())) {
			query = query + " AND tblCrmAccount.Id=" + ajaxHandlerAction.getCustomerName() + " ";
		}

		if (ajaxHandlerAction.getProjectName() != null && !"".equals(ajaxHandlerAction.getProjectName())
				&& !"-1".equals(ajaxHandlerAction.getProjectName())) {
			query = query + "AND tblProjects.Id =" + ajaxHandlerAction.getProjectName() + " ";

		}
		if (!"".equalsIgnoreCase(ajaxHandlerAction.getStatus())) {
			query = query + " AND tblProjects.STATUS like '%" + ajaxHandlerAction.getStatus() + "%'";
		}

		if (!"".equalsIgnoreCase(ajaxHandlerAction.getProjectStartDate())) {
			ajaxHandlerAction.setProjectStartDate(
					DateUtility.getInstance().convertStringToMySQLDate(ajaxHandlerAction.getProjectStartDate()));
			query = query + " AND date(tblProjects.ProjectStartDate) like '%" + ajaxHandlerAction.getProjectStartDate()
					+ "%'";

		}
		if (((!"".equals(ajaxHandlerAction.getPreAssignEmpId()))) && (ajaxHandlerAction.getPreAssignEmpId() != 0)) {

			query = query + " AND tblProjectContacts.ObjectId= " + ajaxHandlerAction.getPreAssignEmpId();

		}
		if (ajaxHandlerAction.getPracticeId() != null && !"".equalsIgnoreCase(ajaxHandlerAction.getPracticeId())) {
			query = query + " AND tblProjects.Practice = '" + ajaxHandlerAction.getPracticeId() + "'";
		}
		if (ajaxHandlerAction.getProjectType() != null && !"".equalsIgnoreCase(ajaxHandlerAction.getProjectType())) {
			query = query + " AND tblProjects.ProjectType = \"" + ajaxHandlerAction.getProjectType() + "\" ";
		}

		query = query + " ORDER BY tblCrmAccount.NAME,tblProjects.ProjectName  LIMIT 100";
		try {

			// System.out.println("Query -->" + query);

			connection = ConnectionProvider.getInstance().getConnection();
			statement = connection.createStatement();
			resultSet = statement.executeQuery(query);

			while (resultSet.next()) {
				i = i + 1;
				String NAME = resultSet.getString("NAME");
				int AccountId = resultSet.getInt("AccountId");
				// String ProjectType = resultSet.getString("ProjectType");

				// if(ProjectType==null || "".equalsIgnoreCase(ProjectType))
				// {
				// ProjectType= "-";

				// }
				int resources = 0;
				// int resources =
				// DataSourceDataProvider.getInstance().getProjectActiveResourcesByCustomer(resultSet.getInt("AccountId"));
				if (ajaxHandlerAction.getProjectName() != null && !"".equals(ajaxHandlerAction.getProjectName())
						&& !"-1".equals(ajaxHandlerAction.getProjectName())) {
					resources = DataSourceDataProvider.getInstance()
							.getProjectActiveResources(Integer.parseInt(ajaxHandlerAction.getProjectName()));
				} else {
					resources = DataSourceDataProvider.getInstance().getProjectActiveResourcesByCustomer(
							resultSet.getInt("AccountId"), ajaxHandlerAction.getStatus());
				}
				// totalStream = totalStream + "|" + ProjectId + "|" +
				// ProjectName +"|" + NAME + "|" + ProjectStartDate + "|" +
				// resources + "|" + status + "|"+ resultSet.getString("PMO") +
				// "|" + AccountId + "^";
				totalStream = totalStream + i + "#^$" + NAME + "#^$" + resources + "#^$" + AccountId + "*@!";

			}
			// System.out.println("totalStream..."+totalStream);
		} catch (Exception sqe) {
			sqe.printStackTrace();
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (statement != null) {
					statement.close();
					statement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException sqle) {
			}
		}
		return totalStream;
	}

	public String getResourceTypeDetailsByCustomer(int accId, String projectId, HttpServletRequest httpServletRequest)
			throws ServiceLocatorException {
		// String qsTitle = "";
		Statement statement = null;
		ResultSet resultSet = null;
		String query = "";
		Connection connection = null;
		String response = "";
		try {
			connection = ConnectionProvider.getInstance().getConnection();
			query = " SELECT SUM(CASE WHEN tblProjectContacts.EmpProjStatus='Main' THEN 1 ELSE 0 END) AS Main ,"
					+ "SUM(CASE WHEN tblProjectContacts.EmpProjStatus='Shadow' THEN 1 ELSE 0 END) AS Shadow,"
					+ "SUM(CASE WHEN tblProjectContacts.EmpProjStatus='Training' THEN 1 ELSE 0 END) AS Training,"
					+ "SUM(CASE WHEN tblProjectContacts.EmpProjStatus='Overhead' THEN 1 ELSE 0 END) AS Overhead,"
					+ "SUM(CASE WHEN tblProjectContacts.EmpProjStatus='Main' AND tblEmployee.Country='USA' THEN 1 ELSE 0 END) AS MainOnSite,"
					+ "SUM(CASE WHEN tblProjectContacts.EmpProjStatus='Main' AND tblEmployee.Country='INDIA' THEN 1 ELSE 0 END) AS MainOffShore, "
					+ "SUM(CASE WHEN tblProjectContacts.EmpProjStatus='Shadow' AND tblEmployee.Country='USA' THEN 1 ELSE 0 END) AS ShadowOnSite,"
					+ "SUM(CASE WHEN tblProjectContacts.EmpProjStatus='Shadow' AND tblEmployee.Country='INDIA' THEN 1 ELSE 0 END) AS ShadowOffShore,"
					+ "SUM(CASE WHEN tblProjectContacts.EmpProjStatus='Training' AND tblEmployee.Country='USA' THEN 1 ELSE 0 END) AS TrainingOnSite,"
					+ "SUM(CASE WHEN tblProjectContacts.EmpProjStatus='Training' AND tblEmployee.Country='INDIA' THEN 1 ELSE 0 END) AS TrainingOffShore,"
					+ "SUM(CASE WHEN tblProjectContacts.EmpProjStatus='Overhead' AND tblEmployee.Country='USA' THEN 1 ELSE 0 END) AS OverheadOnSite,"
					+ "SUM(CASE WHEN tblProjectContacts.EmpProjStatus='Overhead' AND tblEmployee.Country='INDIA' THEN 1 ELSE 0 END) AS OverheadOffShore,"
					// + "COUNT(tblProjectContacts.Id) AS Total FROM
					// tblProjectContacts LEFT JOIN tblEmployee ON
					// tblEmployee.Id=tblProjectContacts.ObjectId WHERE
					// AccountId=" + accId + " AND STATUS='Active' AND
					// ObjectType='E' AND CurStatus='Active' ";
					+ "COUNT(tblProjectContacts.Id) AS Total FROM tblProjectContacts LEFT  JOIN tblEmployee ON tblEmployee.Id=tblProjectContacts.ObjectId WHERE  AccountId="
					+ accId + "  AND STATUS='Active' AND ObjectType='E'";
			if (projectId != null && !"".equals(projectId.trim()) && !"-1".equals(projectId)) {
				query = query + " AND ProjectId=" + projectId;
			}
			// System.out.println("query.."+query);
			statement = connection.createStatement();
			resultSet = statement.executeQuery(query);
			while (resultSet.next()) {
				response = response + resultSet.getInt("Main") + "#^$" + resultSet.getInt("Shadow") + "#^$"
						+ resultSet.getInt("Training") + "#^$" + resultSet.getInt("Overhead") + "#^$"
						+ resultSet.getInt("MainOnSite") + "#^$" + resultSet.getInt("MainOffShore") + "#^$"
						+ resultSet.getInt("ShadowOnSite") + "#^$" + resultSet.getInt("ShadowOffShore") + "#^$"
						+ resultSet.getInt("TrainingOnSite") + "#^$" + resultSet.getInt("TrainingOffShore") + "#^$"
						+ resultSet.getInt("OverheadOnSite") + "#^$" + resultSet.getInt("OverheadOffShore") + "#^$"
						+ resultSet.getInt("Total") + "*@!";
			}
			// System.out.println("query..."+query);
			// System.out.println("response.."+response);
		} catch (Exception sqe) {
			sqe.printStackTrace();
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (statement != null) {
					statement.close();
					statement = null;
				}

				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException sqle) {
				sqle.printStackTrace();
			}
		}

		return response;
	}

	public String getProjectListByCustomer(NewAjaxHandlerAction ajaxHandlerAction,
			HttpServletRequest httpServletRequest) throws ServiceLocatorException {
		// String qsTitle = "";
		Statement statement = null;
		ResultSet resultSet = null;
		String query = "";
		Connection connection = null;
		String response = "";
		int resources = 0;
		try {
			connection = ConnectionProvider.getInstance().getConnection();
			// query="SELECT DISTINCT
			// tblProjects.ProjectName,ProjectStartDate,STATUS,Practice FROM
			// tblProjects WHERE CustomerId="+ajaxHandlerAction.getAccId()+" ";

			// query = "SELECT DISTINCT
			// tblProjects.ProjectName,tblProjects.ProjectStartDate,tblProjects.STATUS,tblProjects.Practice,tblProjects.ProjectType
			// AS ProjectType FROM tblProjects LEFT JOIN tblProjectContacts ON
			// (tblProjects.Id = tblProjectContacts.ProjectId ) WHERE
			// CustomerId=" + ajaxHandlerAction.getAccId() + " ";
			query = "SELECT DISTINCT tblProjects.Id,tblProjects.ProjectName,tblProjects.ProjectStartDate,tblProjects.STATUS,tblProjects.Practice,tblProjects.ProjectType AS ProjectType FROM tblProjects LEFT JOIN tblProjectContacts ON (tblProjects.Id = tblProjectContacts.ProjectId ) WHERE  CustomerId="
					+ ajaxHandlerAction.getAccId() + " ";
			Map rolesMap = (Map) httpServletRequest.getSession(false)
					.getAttribute(ApplicationConstants.SESSION_MY_ROLES);
			/*
			 * if (!rolesMap.containsValue("Admin")) { if
			 * (httpServletRequest.getSession(false).getAttribute(
			 * ApplicationConstants.SESSION_IS_TEAM_LEAD).toString().equals("1")
			 * || httpServletRequest.getSession(false).getAttribute(
			 * ApplicationConstants.SESSION_IS_USER_MANAGER).toString().equals(
			 * "1")) { Map teamMap = (Map)
			 * httpServletRequest.getSession(false).getAttribute(
			 * ApplicationConstants.SESSION_MY_TEAM_MAP);
			 * teamMap.put(ajaxHandlerAction.getLoginId(),
			 * httpServletRequest.getSession(false).getAttribute(
			 * ApplicationConstants.SESSION_USER_NAME).toString());
			 * 
			 * if (!"".equalsIgnoreCase(ajaxHandlerAction.getPmoLoginId())) {
			 * query = query + " AND tblProjects.PMO = '" +
			 * ajaxHandlerAction.getPmoLoginId() + "'"; } else { query = query +
			 * " AND tblProjects.PMO IN (" +
			 * DataSourceDataProvider.getInstance().getTeamLoginIdList(teamMap)
			 * + ") "; }
			 * 
			 * 
			 * } else { query = query + " AND tblProjects.PMO = '" +
			 * ajaxHandlerAction.getLoginId() + "'"; } } else { if
			 * (ajaxHandlerAction.getPmoLoginId() != null &&
			 * !"".equalsIgnoreCase(ajaxHandlerAction.getPmoLoginId())) { query
			 * = query + " AND tblProjects.PMO = '" +
			 * ajaxHandlerAction.getPmoLoginId() + "'"; } }
			 */
			if (rolesMap.containsValue("Admin") || httpServletRequest.getSession(false)
					.getAttribute(ApplicationConstants.SESSION_PMO_ACTIVITY_ACCESS).toString().equals("1")) {

				if (ajaxHandlerAction.getPmoLoginId() != null
						&& !"".equalsIgnoreCase(ajaxHandlerAction.getPmoLoginId())) {
					query = query + " AND tblProjects.PMO = '" + ajaxHandlerAction.getPmoLoginId() + "'";
				}
			} else {
				if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_IS_TEAM_LEAD)
						.toString().equals("1")
						|| httpServletRequest.getSession(false)
								.getAttribute(ApplicationConstants.SESSION_IS_USER_MANAGER).toString().equals("1")) {
					Map teamMap = (Map) httpServletRequest.getSession(false)
							.getAttribute(ApplicationConstants.SESSION_MY_TEAM_MAP);
					teamMap.put(ajaxHandlerAction.getLoginId(), httpServletRequest.getSession(false)
							.getAttribute(ApplicationConstants.SESSION_USER_NAME).toString());

					if (!"".equalsIgnoreCase(ajaxHandlerAction.getPmoLoginId())) {
						query = query + " AND tblProjects.PMO = '" + ajaxHandlerAction.getPmoLoginId() + "'";
					} else {
						query = query + " AND tblProjects.PMO IN ("
								+ DataSourceDataProvider.getInstance().getTeamLoginIdList(teamMap) + ") ";
					}

				} else {
					query = query + " AND tblProjects.PMO = '" + ajaxHandlerAction.getLoginId() + "'";
				}
			}

			if (ajaxHandlerAction.getProjectName() != null && !"".equals(ajaxHandlerAction.getProjectName())
					&& !"-1".equals(ajaxHandlerAction.getProjectName())) {
				query = query + "AND tblProjects.Id =" + ajaxHandlerAction.getProjectName() + " ";

			}

			if (!"".equalsIgnoreCase(ajaxHandlerAction.getStatus())) {
				query = query + " AND tblProjects.STATUS like '%" + ajaxHandlerAction.getStatus() + "%'";
			}

			if (!"".equalsIgnoreCase(ajaxHandlerAction.getProjectStartDate())) {
				ajaxHandlerAction.setProjectStartDate(
						DateUtility.getInstance().convertStringToMySQLDate(ajaxHandlerAction.getProjectStartDate()));
				query = query + " AND date(tblProjects.ProjectStartDate) like '%"
						+ ajaxHandlerAction.getProjectStartDate() + "%'";

			}

			if (ajaxHandlerAction.getPracticeId() != null && !"".equalsIgnoreCase(ajaxHandlerAction.getPracticeId())) {
				query = query + " AND tblProjects.Practice = '" + ajaxHandlerAction.getPracticeId() + "'";
			}

			if (((!"".equals(ajaxHandlerAction.getPreAssignEmpId()))) && (ajaxHandlerAction.getPreAssignEmpId() != 0)) {

				query = query + " AND tblProjectContacts.ObjectId= " + ajaxHandlerAction.getPreAssignEmpId();

			}
			// System.out.println("query.."+query);
			statement = connection.createStatement();
			resultSet = statement.executeQuery(query);
			while (resultSet.next()) {

				String Name = resultSet.getString("ProjectName");
				String ProjectStartDate = resultSet.getString("ProjectStartDate");
				String Practice = resultSet.getString("Practice");
				String projectStatus = resultSet.getString("STATUS");
				String projectType = resultSet.getString("ProjectType");
				int projectId = resultSet.getInt("Id");
				resources = DataSourceDataProvider.getInstance().getProjectActiveResources(projectId);

				if ("".equalsIgnoreCase(Name) || Name == null) {
					Name = "-";
				}
				if ("".equalsIgnoreCase(Practice) || Practice == null) {
					Practice = "-";
				}
				if ("".equalsIgnoreCase(projectStatus) || projectStatus == null) {
					projectStatus = "-";
				}
				if ("".equalsIgnoreCase(projectType) || projectType == null) {
					projectType = "-";
				}
				// response = response + Name + "#^$" + ProjectStartDate + "#^$"
				// + projectStatus + "#^$" + Practice+ "#^$" + projectType +
				// "*@!";
				response = response + Name + "#^$" + ProjectStartDate + "#^$" + projectStatus + "#^$" + Practice + "#^$"
						+ projectType + "#^$" + resources + "#^$" + ajaxHandlerAction.getAccId() + "#^$" + projectId
						+ "*@!";

			}
			// System.out.println("query..."+query);
			// System.out.println("response.."+response);
		} catch (Exception sqe) {
			sqe.printStackTrace();
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (statement != null) {
					statement.close();
					statement = null;
				}

				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException sqle) {
				sqle.printStackTrace();
			}
		}

		return response;
	}

	/*
	 * Author : sarada tatisetti Date: 10-10-2017
	 */
	public String getResourceTypeDetailsByProject(int accId, String projectId, HttpServletRequest httpServletRequest)
			throws ServiceLocatorException {
		// String qsTitle = "";
		Statement statement = null;
		ResultSet resultSet = null;
		String query = "";
		Connection connection = null;
		String response = "";
		try {
			connection = ConnectionProvider.getInstance().getConnection();
			query = " SELECT SUM(CASE WHEN tblProjectContacts.EmpProjStatus='Main' THEN 1 ELSE 0 END) AS Main ,"
					+ "SUM(CASE WHEN tblProjectContacts.EmpProjStatus='Shadow' THEN 1 ELSE 0 END) AS Shadow,"
					+ "SUM(CASE WHEN tblProjectContacts.EmpProjStatus='Training' THEN 1 ELSE 0 END) AS Training,"
					+ "SUM(CASE WHEN tblProjectContacts.EmpProjStatus='Overhead' THEN 1 ELSE 0 END) AS Overhead,"
					+ "SUM(CASE WHEN tblProjectContacts.EmpProjStatus='Main' AND tblEmployee.Country='USA' THEN 1 ELSE 0 END) AS MainOnSite,"
					+ "SUM(CASE WHEN tblProjectContacts.EmpProjStatus='Main' AND tblEmployee.Country='INDIA' THEN 1 ELSE 0 END) AS MainOffShore, "
					+ "SUM(CASE WHEN tblProjectContacts.EmpProjStatus='Shadow' AND tblEmployee.Country='USA' THEN 1 ELSE 0 END) AS ShadowOnSite,"
					+ "SUM(CASE WHEN tblProjectContacts.EmpProjStatus='Shadow' AND tblEmployee.Country='INDIA' THEN 1 ELSE 0 END) AS ShadowOffShore,"
					+ "SUM(CASE WHEN tblProjectContacts.EmpProjStatus='Training' AND tblEmployee.Country='USA' THEN 1 ELSE 0 END) AS TrainingOnSite,"
					+ "SUM(CASE WHEN tblProjectContacts.EmpProjStatus='Training' AND tblEmployee.Country='INDIA' THEN 1 ELSE 0 END) AS TrainingOffShore,"
					+ "SUM(CASE WHEN tblProjectContacts.EmpProjStatus='Overhead' AND tblEmployee.Country='USA' THEN 1 ELSE 0 END) AS OverheadOnSite,"
					+ "SUM(CASE WHEN tblProjectContacts.EmpProjStatus='Overhead' AND tblEmployee.Country='INDIA' THEN 1 ELSE 0 END) AS OverheadOffShore,"
					// + "COUNT(tblProjectContacts.Id) AS Total FROM
					// tblProjectContacts LEFT JOIN tblEmployee ON
					// tblEmployee.Id=tblProjectContacts.ObjectId WHERE
					// AccountId=" + accId + " AND STATUS='Active' AND
					// ObjectType='E' AND CurStatus='Active' ";
					+ "COUNT(tblProjectContacts.Id) AS Total FROM tblProjectContacts LEFT  JOIN tblEmployee ON tblEmployee.Id=tblProjectContacts.ObjectId  LEFT JOIN tblProjects ON (tblProjects.Id = tblProjectContacts.ProjectId ) WHERE  ProjectId='"
					+ projectId + "' AND AccountId=" + accId
					+ " AND tblProjectContacts.STATUS='Active' AND ObjectType='E'";

			statement = connection.createStatement();
			resultSet = statement.executeQuery(query);
			while (resultSet.next()) {
				response = response + resultSet.getInt("Main") + "#^$" + resultSet.getInt("Shadow") + "#^$"
						+ resultSet.getInt("Training") + "#^$" + resultSet.getInt("Overhead") + "#^$"
						+ resultSet.getInt("MainOnSite") + "#^$" + resultSet.getInt("MainOffShore") + "#^$"
						+ resultSet.getInt("ShadowOnSite") + "#^$" + resultSet.getInt("ShadowOffShore") + "#^$"
						+ resultSet.getInt("TrainingOnSite") + "#^$" + resultSet.getInt("TrainingOffShore") + "#^$"
						+ resultSet.getInt("OverheadOnSite") + "#^$" + resultSet.getInt("OverheadOffShore") + "#^$"
						+ resultSet.getInt("Total") + "*@!";
			}
			// System.out.println("query..."+query);
			// System.out.println("response.."+response);
		} catch (Exception sqe) {
			sqe.printStackTrace();
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (statement != null) {
					statement.close();
					statement = null;
				}

				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException sqle) {
				sqle.printStackTrace();
			}
		}

		return response;
	}

	public String getTaskNotes(int notesId) throws ServiceLocatorException {

		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;
		String notes = "";
		try {
			connection = ConnectionProvider.getInstance().getConnection();
			statement = connection.createStatement();
			resultSet = statement.executeQuery("SELECT Notes FROM tblTaskNotes WHERE Id=" + notesId);
			if (resultSet.next()) {
				notes = resultSet.getString("Notes");
			}

		} catch (SQLException se) {
			se.printStackTrace();
			throw new ServiceLocatorException(se);
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (statement != null) {
					statement.close();
					statement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException sqle) {
				sqle.printStackTrace();
				throw new ServiceLocatorException(sqle);
			}
		}
		return notes;
	}

	public String getIssuesDashBoardByStatus(String taskStartDate, String taskEndDate, String reportsTo, int graphId,
			HttpServletRequest httpServletRequest) throws ServiceLocatorException {

		String response = "";
		// Connection connection = null;
		CallableStatement callableStatement = null;
		Connection connection = null;
		String teamMembersKeys = "";
		Map teamMembersMapByLoginid = null;
		Map teamMembersMapByReportsTo = null;

		try {
			connection = ConnectionProvider.getInstance().getConnection();
			teamMembersMapByLoginid = (Map) httpServletRequest.getSession(false)
					.getAttribute(ApplicationConstants.SESSION_MY_TEAM_MAP);
			// System.out.println("teamMembersMapByLoginid---"+teamMembersMapByLoginid);
			String loginId = (String) httpServletRequest.getSession(false)
					.getAttribute(ApplicationConstants.SESSION_USER_ID);
			// System.out.println("reportsTo----" + reportsTo);
			if ("-1".equals(reportsTo)) {

				teamMembersKeys = getKeys(teamMembersMapByLoginid, ",");
				teamMembersKeys = teamMembersKeys.replaceAll("'", "");
				// System.out.println("teamMembersKeys
				// main---"+teamMembersKeys);
				if (!"".equals(teamMembersKeys)) {
					teamMembersKeys = teamMembersKeys + ',' + loginId;
				} else {
					teamMembersKeys = loginId;
				}
				// System.out.println("reportsTo-teamMembersKeys --- in if---" +
				// teamMembersKeys);
			} else {
				String department = DataSourceDataProvider.getInstance().getDepartmentName(reportsTo);
				teamMembersMapByReportsTo = DataSourceDataProvider.getInstance().getMyTeamMembers(reportsTo,
						department);
				teamMembersKeys = getKeys(teamMembersMapByReportsTo, ",");
				teamMembersKeys = teamMembersKeys.replaceAll("'", "");
				if (!"".equals(teamMembersKeys)) {
					teamMembersKeys = teamMembersKeys + ',' + reportsTo;
				} else {
					teamMembersKeys = reportsTo;
				}
				// System.out.println("reportsTo-teamMembersKeys --- in else---"
				// + teamMembersKeys);
			}
			// System.out.println("sd--" +
			// DateUtility.getInstance().convertStringToMySQLDate(taskStartDate));
			// System.out.println("ed--" +
			// DateUtility.getInstance().convertStringToMySQLDate(taskEndDate));

			callableStatement = connection.prepareCall("{call spTaskDashBoard(?,?,?,?,?,?)}");
			callableStatement.setInt(1, graphId);
			callableStatement.setString(2, "-1");
			callableStatement.setString(3, teamMembersKeys);
			callableStatement.setString(4, DateUtility.getInstance().convertStringToMySQLDate(taskStartDate));
			callableStatement.setString(5, DateUtility.getInstance().convertStringToMySQLDate(taskEndDate));
			// System.out.println("graphId--" + graphId);
			callableStatement.registerOutParameter(6, Types.VARCHAR);
			callableStatement.execute();

			response = callableStatement.getString(6);
			// System.out.println("response--" + response);

		} catch (SQLException se) {
			se.printStackTrace();
		} finally {
			try {

				if (callableStatement != null) {
					callableStatement.close();
					callableStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		return response;
	}

	public String getTaskListByStatus(String taskStartDate, String taskEndDate, String reportsTo, String activityType,
			int graphId, HttpServletRequest httpServletRequest) throws ServiceLocatorException {

		String response = "";
		// Connection connection = null;
		CallableStatement callableStatement = null;
		Connection connection = null;
		String teamMembersKeys = "";

		Map teamMembersMapByLoginid = null;
		Map teamMembersMapByReportsTo = null;

		try {
			connection = ConnectionProvider.getInstance().getConnection();

			teamMembersMapByLoginid = (Map) httpServletRequest.getSession(false)
					.getAttribute(ApplicationConstants.SESSION_MY_TEAM_MAP);
			String loginId = (String) httpServletRequest.getSession(false)
					.getAttribute(ApplicationConstants.SESSION_USER_ID);
			// System.out.println("teamMembersMapByLoginid---"+teamMembersMapByLoginid);
			// System.out.println("reportsTo----" + reportsTo);
			if ("-1".equals(reportsTo)) {
				teamMembersKeys = getKeys(teamMembersMapByLoginid, ",");
				teamMembersKeys = teamMembersKeys.replaceAll("'", "");
				if (!"".equals(teamMembersKeys)) {
					teamMembersKeys = teamMembersKeys + ',' + loginId;
				} else {
					teamMembersKeys = loginId;
				}
				// System.out.println("reportsTo-teamMembersKeys --- in if---" +
				// teamMembersKeys);
			} else {
				String department = DataSourceDataProvider.getInstance().getDepartmentName(reportsTo);
				teamMembersMapByReportsTo = DataSourceDataProvider.getInstance().getMyTeamMembers(reportsTo,
						department);
				teamMembersKeys = getKeys(teamMembersMapByReportsTo, ",");
				teamMembersKeys = teamMembersKeys.replaceAll("'", "");
				if (!"".equals(teamMembersKeys)) {
					teamMembersKeys = teamMembersKeys + ',' + reportsTo;
				} else {
					teamMembersKeys = reportsTo;
				}
				// System.out.println("reportsTo-teamMembersKeys --- in else---"
				// + teamMembersKeys);
			}

			// System.out.println("teamMembersKeys-------1---" +
			// teamMembersKeys);

			callableStatement = connection.prepareCall("{call spTaskDashBoard(?,?,?,?,?,?)}");

			callableStatement.setInt(1, graphId);
			callableStatement.setString(2, activityType);
			callableStatement.setString(3, teamMembersKeys);
			// System.out.println("555---activityType---" + activityType);
			callableStatement.setString(4, DateUtility.getInstance().convertStringToMySQLDate(taskStartDate));
			callableStatement.setString(5, DateUtility.getInstance().convertStringToMySQLDate(taskEndDate));
			// System.out.println("sd--" +
			// DateUtility.getInstance().convertStringToMySQLDate(taskStartDate));
			// System.out.println("ed--" +
			// DateUtility.getInstance().convertStringToMySQLDate(taskEndDate));
			callableStatement.registerOutParameter(6, Types.VARCHAR);
			callableStatement.execute();

			response = callableStatement.getString(6);

		} catch (SQLException se) {
			se.printStackTrace();
		} finally {
			try {

				if (callableStatement != null) {
					callableStatement.close();
					callableStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		return response;
	}

	public String doPopulateTaskDetails(int taskId) throws ServiceLocatorException {
		StringBuffer reportsToBuffer = new StringBuffer("");
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		Connection connection = null;
		String response = "";

		try {
			connection = ConnectionProvider.getInstance().getConnection();

			String query = "SELECT Description FROM tblEmpTasks WHERE Id=" + taskId;

			preparedStatement = connection.prepareStatement(query);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {

				response = response + resultSet.getString("Description");

			}
			if (response.equals("")) {
				response = "NoData";
			}

		} catch (SQLException sle) {
			sle.printStackTrace();
		} catch (ServiceLocatorException sle) {
			sle.printStackTrace();
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
		}
		// System.out.println("Team: "+reportsToBuffer.toString());
		return response;
	}
	/*
	 * q-reviews
	 */

	public int doSetQReviewManagerComments(int Id, String comments) throws ServiceLocatorException {

		Connection connection = null;
		PreparedStatement preparedStatement = null;
		int count = 0;
		try {
			// System.out.println("Id==="+Id);
			// System.out.println("comments==="+comments);
			connection = ConnectionProvider.getInstance().getConnection();
			preparedStatement = connection
					.prepareStatement("UPDATE tblQuarterlyAppraisalsLines SET MgrCommnets=?  WHERE Id = ?");

			preparedStatement.setString(1, comments);
			preparedStatement.setInt(2, Id);
			count = preparedStatement.executeUpdate();
			// System.out.println("count==="+count);
		} catch (Exception e) {
			System.err.println("Exception is-->" + e.getMessage());
		} finally {
			try {
				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (Exception sqle) {
				System.err.println("SQL Exception is-->" + sqle.getMessage());
			}
		}
		return count;

	}

	public int doSetQReviewPersonalityComments(int Id, String strengthsComments, String improvementsComments,
			String strengths, String improvements) throws ServiceLocatorException {

		Connection connection = null;
		PreparedStatement preparedStatement = null;
		int count = 0;
		try {
			connection = ConnectionProvider.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(
					"UPDATE tblQuarterlyAppraisals SET StrengthsComments=?,ImprovementsComments=?,Strength=?,Improvements=?  WHERE Id = ?");

			preparedStatement.setString(1, strengthsComments);
			preparedStatement.setString(2, improvementsComments);
			preparedStatement.setString(3, strengths);
			preparedStatement.setString(4, improvements);
			preparedStatement.setInt(5, Id);
			count = preparedStatement.executeUpdate();
			// System.out.println("count==="+count);
		} catch (Exception e) {
			System.err.println("Exception is-->" + e.getMessage());
		} finally {
			try {
				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (Exception sqle) {
				System.err.println("SQL Exception is-->" + sqle.getMessage());
			}
		}
		return count;

	}

	public int doSetQReviewGoalsComments(int Id, String shortTermGoalComments, String longTermGoalComments,
			String shortTermGoal, String longTermGoal) throws ServiceLocatorException {

		Connection connection = null;
		PreparedStatement preparedStatement = null;
		int count = 0;
		try {
			connection = ConnectionProvider.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(
					"UPDATE tblQuarterlyAppraisals SET ShortTermGoalComments=?,LongTermGoalComments=?,ShortTermGoal=?,LongTermGoal=?  WHERE Id = ?");

			preparedStatement.setString(1, shortTermGoalComments);
			preparedStatement.setString(2, longTermGoalComments);
			preparedStatement.setString(3, shortTermGoal);
			preparedStatement.setString(4, longTermGoal);
			preparedStatement.setInt(5, Id);
			count = preparedStatement.executeUpdate();
			// System.out.println("count==="+count);
		} catch (Exception e) {
			System.err.println("Exception is-->" + e.getMessage());
		} finally {
			try {
				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (Exception sqle) {
				System.err.println("SQL Exception is-->" + sqle.getMessage());
			}
		}
		return count;

	}

	public String empQuarterlyAppraisalSave(NewAjaxHandlerAction newAjaxHandlerAction,
			HttpServletRequest httpServletRequest) throws ServiceLocatorException {

		Connection connection = null;
		PreparedStatement preparedStatement = null;
		CallableStatement callableStatement = null;
		ResultSet resultSet = null;
		String resutMessage = "";
		JSONObject jObject = null;
		String queryString = "{call spQuarterlyAppraisalSave(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";

		try {
			jObject = new JSONObject(newAjaxHandlerAction.getJsonData());
			String roleName = httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_ROLE_NAME)
					.toString();
			String loginId = httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID)
					.toString();
			// System.out.println("getRowCount==="+rowcount);
			// System.out.println("loginId==="+loginId);

			/*
			 * String id = httpServletRequest.getParameter("ids"); String
			 * measurementId = httpServletRequest.getParameter("measurementId");
			 * String keyFactorId =
			 * httpServletRequest.getParameter("keyFactorId"); String
			 * keyFactorWeightage =
			 * httpServletRequest.getParameter("keyFactorWeightage"); String
			 * empDescription =
			 * httpServletRequest.getParameter("empDescription"); String
			 * empRating = httpServletRequest.getParameter("empRating"); String
			 * shortTermGoal = httpServletRequest.getParameter("shortTermGoal");
			 * String longTermGoal =
			 * httpServletRequest.getParameter("longTermGoal"); String strength
			 * = httpServletRequest.getParameter("strength"); String
			 * improvements = httpServletRequest.getParameter("improvements");
			 * int rowcount = newAjaxHandlerAction.getRowCount();
			 */

			String id = jObject.getString("ids");
			int empId = jObject.getInt("empId");
			newAjaxHandlerAction.setEmpId(empId + "");
			String measurementId = jObject.getString("measurementId");
			String keyFactorId = jObject.getString("keyFactorId");
			String keyFactorWeightage = jObject.getString("keyFactorWeightage");
			String empDescription = jObject.getString("empDescription");
			String empRating = jObject.getString("empRating");
			String shortTermGoal = jObject.getString("shortTermGoal");
			String longTermGoal = jObject.getString("longTermGoal");
			String strength = jObject.getString("strength");
			String improvements = jObject.getString("improvements");
			String status = jObject.getString("status");
			String quarterly = jObject.getString("quarterly");
			int qyear = jObject.getInt("qyear");
			int rowcount = jObject.getInt("rowCount");
			String HrWeightage = jObject.getString("HrWeightage");
			String appraisalHrRating = "";
			String hrCalWeightage = "";
			double totalHrRating = 0.0;
			double totalHrCalWeightage = 0.0;

			// System.out.println("measurementId=="+measurementId);
			// System.out.println("keyFactorId=="+keyFactorId);
			// System.out.println("keyFactorWeightage=="+keyFactorWeightage);
			// System.out.println("empDescription=="+empDescription);
			// System.out.println("empRating=="+empRating);
			// System.out.println("shortTermGoal=="+shortTermGoal);
			// System.out.println("longTermGoal=="+longTermGoal);
			// System.out.println("strength=="+strength);
			// System.out.println("improvements=="+improvements);

			connection = ConnectionProvider.getInstance().getConnection();

			callableStatement = connection.prepareCall(queryString);
			callableStatement.setString(1, id);
			callableStatement.setInt(2, 0);
			callableStatement.setInt(3, empId);
			callableStatement.setString(4, measurementId);
			callableStatement.setString(5, keyFactorId);
			callableStatement.setString(6, keyFactorWeightage);
			callableStatement.setString(7, empDescription);
			callableStatement.setString(8, empRating);
			callableStatement.setString(9, loginId);
			callableStatement.setTimestamp(10, DateUtility.getInstance().getCurrentMySqlDateTime());
			callableStatement.setString(11, status);
			callableStatement.setDouble(12, 0);
			callableStatement.setDouble(13, 0);
			callableStatement.setInt(14, rowcount);
			callableStatement.setString(15, "");
			callableStatement.setString(16, "");
			callableStatement.setString(17, "");
			callableStatement.setDouble(18, 0);
			callableStatement.setDouble(19, 0);
			callableStatement.setInt(20, 0);
			callableStatement.setString(21, shortTermGoal);
			callableStatement.setString(22, "");
			callableStatement.setString(23, longTermGoal);
			callableStatement.setString(24, "");
			callableStatement.setString(25, strength);
			callableStatement.setString(26, "");
			callableStatement.setString(27, improvements);
			callableStatement.setString(28, "");
			callableStatement.setString(29, quarterly);
			callableStatement.setString(30, roleName);
			callableStatement.setString(31, "");
			callableStatement.setInt(32, qyear);
			callableStatement.setString(33, HrWeightage);
			callableStatement.setString(34, appraisalHrRating);
			callableStatement.setString(35, hrCalWeightage);
			callableStatement.setDouble(36, totalHrRating);
			callableStatement.setDouble(37, totalHrCalWeightage);
			callableStatement.registerOutParameter(38, Types.VARCHAR);
			callableStatement.executeUpdate();
			resutMessage = callableStatement.getString(38);

		} catch (SQLException se) {
			se.printStackTrace();
			// resutMessage = "<font color=red>" + se.getMessage() + "</font>";
			throw new ServiceLocatorException(se);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException se) {
				se.printStackTrace();
				throw new ServiceLocatorException(se);
			}
		}
		return resutMessage;
	}

	public String quarterlyAppraisalEdit(int empId, int appraisalId, int lineId) throws ServiceLocatorException {

		Connection connection = null;
		PreparedStatement preparedStatement = null;
		CallableStatement callableStatement = null;
		ResultSet resultSet = null;
		String resutMessage = "";
		boolean isInserted = false;

		// String queryString = "INSERT INTO `tblInvestments` ( `Inv_Name`,
		// `Country`, `StartDate`, `EndDate`,
		// `TotalExpenses`,`Currency`,`Location`,`Description`,`AttachmentFileName`,`AttachmentLocation`,`CreatedBy`,InvestmentType)"
		// + " VALUES(?,?,?,?,?,?,?,?,?,?,?,?)";
		String queryString = "{call spGetFactoryDetailsEdit(?,?,?,?)}";

		try {
			connection = ConnectionProvider.getInstance().getConnection();

			callableStatement = connection.prepareCall(queryString);
			callableStatement.setInt(1, empId);
			callableStatement.setInt(2, appraisalId);
			callableStatement.setInt(3, lineId);
			callableStatement.registerOutParameter(4, Types.VARCHAR);
			callableStatement.executeUpdate();
			resutMessage = callableStatement.getString(4);

		} catch (SQLException se) {
			se.printStackTrace();
			// resutMessage = "<font color=red>" + se.getMessage() + "</font>";
			throw new ServiceLocatorException(se);
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException se) {
				se.printStackTrace();
				throw new ServiceLocatorException(se);
			}
		}
		return resutMessage;
	}

	public int setQReviewEmpDescriptions(String jsonData) throws ServiceLocatorException {

		Connection connection = null;
		PreparedStatement preparedStatement = null;
		int count = 0;
		JSONObject jObject = null;
		try {
			// System.out.println("Id==="+Id);
			// System.out.println("comments==="+comments);
			jObject = new JSONObject(jsonData);
			int Id = jObject.getInt("id");
			String comments = jObject.getString("comments");
			connection = ConnectionProvider.getInstance().getConnection();
			preparedStatement = connection
					.prepareStatement("UPDATE tblQuarterlyAppraisalsLines SET EmpDescription=?  WHERE Id = ?");

			preparedStatement.setString(1, comments);
			preparedStatement.setInt(2, Id);
			count = preparedStatement.executeUpdate();
			// System.out.println("count==="+count);
		} catch (Exception e) {
			System.err.println("Exception is-->" + e.getMessage());
		} finally {
			try {
				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (Exception sqle) {
				System.err.println("SQL Exception is-->" + sqle.getMessage());
			}
		}
		return count;

	}

	public int doAddQReviewHrDescriptions(String jsonData) throws ServiceLocatorException {

		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		int count = 0;
		JSONObject jObject = null;
		try {
			// System.out.println("Id==="+Id);
			// System.out.println("comments==="+comments);
			jObject = new JSONObject(jsonData);
			int empId = jObject.getInt("empId");

			int appraisalId = jObject.getInt("appraisalId");
			int measurement = jObject.getInt("measurement");
			int keyFactor = jObject.getInt("keyFactor");
			double keyFactorWeightage = jObject.getDouble("keyFactorWeightage");
			String comments = jObject.getString("comments");
			String quarterly = jObject.getString("quarterly");
			double HrWeightage = jObject.getDouble("HrWeightage");
			connection = ConnectionProvider.getInstance().getConnection();
			String queryString = "INSERT INTO tblQuarterlyAppraisalsLines(EmpId,AppraisalId,MeasurementId,KeyFactorId,KFWeightage,HrComments,HrWeightage,CreatedBy,CreatedDate,Quarterly) values(?,?,?,?,?,?,?,?,?,?)";
			preparedStatement = connection.prepareStatement(queryString, Statement.RETURN_GENERATED_KEYS);

			preparedStatement.setInt(1, empId);
			preparedStatement.setInt(2, appraisalId);
			preparedStatement.setInt(3, measurement);
			preparedStatement.setInt(4, keyFactor);
			preparedStatement.setDouble(5, keyFactorWeightage);
			preparedStatement.setString(6, comments);
			preparedStatement.setDouble(7, HrWeightage);
			preparedStatement.setString(8, DataSourceDataProvider.getInstance().getLoginIdByEmpId(empId));
			preparedStatement.setTimestamp(9, DateUtility.getInstance().getCurrentMySqlDateTime());
			preparedStatement.setString(10, quarterly);
			count = preparedStatement.executeUpdate();
			resultSet = preparedStatement.getGeneratedKeys();
			// int generatedKey = 0;
			if (resultSet.next()) {
				count = resultSet.getInt(1);
			}
			// asSystem.out.println("count==="+count);
		} catch (Exception e) {
			System.err.println("Exception is-->" + e.getMessage());
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (Exception sqle) {
				System.err.println("SQL Exception is-->" + sqle.getMessage());
			}
		}
		return count;

	}

	public int doSetQReviewHrComments(int Id, String comments) throws ServiceLocatorException {

		Connection connection = null;
		PreparedStatement preparedStatement = null;
		int count = 0;
		try {
			// System.out.println("Id==="+Id);
			// System.out.println("comments==="+comments);
			connection = ConnectionProvider.getInstance().getConnection();
			preparedStatement = connection
					.prepareStatement("UPDATE tblQuarterlyAppraisalsLines SET HrComments=?  WHERE Id = ?");

			preparedStatement.setString(1, comments);
			preparedStatement.setInt(2, Id);
			count = preparedStatement.executeUpdate();
			// System.out.println("count==="+count);
		} catch (Exception e) {
			System.err.println("Exception is-->" + e.getMessage());
		} finally {
			try {
				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (Exception sqle) {
				System.err.println("SQL Exception is-->" + sqle.getMessage());
			}
		}
		return count;

	}

	public int doSetQReviewPersonalityHrComments(int Id, String strengthsComments, String improvementsComments)
			throws ServiceLocatorException {

		Connection connection = null;
		PreparedStatement preparedStatement = null;
		int count = 0;
		try {
			connection = ConnectionProvider.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(
					"UPDATE tblQuarterlyAppraisals SET StrengthsHrComments=?,ImprovementsHrComments=?  WHERE Id = ?");

			preparedStatement.setString(1, strengthsComments);
			preparedStatement.setString(2, improvementsComments);

			preparedStatement.setInt(3, Id);
			count = preparedStatement.executeUpdate();
			// System.out.println("count==="+count);
		} catch (Exception e) {
			System.err.println("Exception is-->" + e.getMessage());
		} finally {
			try {
				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (Exception sqle) {
				System.err.println("SQL Exception is-->" + sqle.getMessage());
			}
		}
		return count;

	}

	public int doSetQReviewGoalsHrComments(int Id, String shortTermGoalComments, String longTermGoalComments)
			throws ServiceLocatorException {

		Connection connection = null;
		PreparedStatement preparedStatement = null;
		int count = 0;
		try {
			connection = ConnectionProvider.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(
					"UPDATE tblQuarterlyAppraisals SET ShortTermGoalHrComments=?,LongTermGoalHrComments=?  WHERE Id = ?");

			preparedStatement.setString(1, shortTermGoalComments);
			preparedStatement.setString(2, longTermGoalComments);

			preparedStatement.setInt(3, Id);
			count = preparedStatement.executeUpdate();
			// System.out.println("count==="+count);
		} catch (Exception e) {
			System.err.println("Exception is-->" + e.getMessage());
		} finally {
			try {
				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (Exception sqle) {
				System.err.println("SQL Exception is-->" + sqle.getMessage());
			}
		}
		return count;

	}
	/*
	 * BDM Association start
	 */

	public String getBdmReport(NewAjaxHandlerAction ajaxHandlerAction, HttpServletRequest httpServletRequest)
			throws ServiceLocatorException {
		// String qsTitle = "";
		Statement statement = null;
		ResultSet resultSet = null;
		String query = "";
		Connection connection = null;
		String response = "";
		int count = 0;
		try {
			// System.out.println("in service impl ajax");
			connection = ConnectionProvider.getInstance().getConnection();
			// query="SELECT DISTINCT
			// tblProjects.ProjectName,ProjectStartDate,STATUS,Practice FROM
			// tblProjects WHERE CustomerId="+ajaxHandlerAction.getAccId()+" ";
			// String endDate =
			// DateUtility.getInstance().getDateByMonthYear(ajaxHandlerAction.getToMonth(),
			// ajaxHandlerAction.getToYear());
			// String startDate =
			// DateUtility.getInstance().getDateLastMonthsByMonthYear(ajaxHandlerAction.getFromMonth(),
			// ajaxHandlerAction.getFromYear(), 0);
			// query="SELECT DISTINCT Id,LoginId FROM tblLogUserAccess WHERE ";

			query = "SELECT t1.Id,CONCAT(TRIM(t1.FName),'.',TRIM(t1.LName)) AS EmployeeName,t1.Email1 AS EmailId,t1.WorkPhoneNo AS PhoneNo,CONCAT(TRIM(t2.FName),'.',TRIM(t2.LName)) AS ReportsTo"
					+ " FROM tblEmployee t1 LEFT JOIN tblEmployee t2 ON t1.ReportsTo = t2.LoginId WHERE t1.CurStatus='Active' AND t1.TitleTypeId='BDM' ";

			// query="SELECT Id,CONCAT(TRIM(FName),'.',TRIM(LName)) AS
			// EmployeeName,Email1 AS EmailId,WorkPhoneNo AS PhoneNo,ReportsTo
			// AS ReportsTo FROM"
			// + " tblEmployee WHERE CurStatus='Active' AND TitleTypeId='BDM' ";

			// SELECT COUNT(Id),DateAccessed FROM tblLogUserAccess WHERE
			// DateAccessed >= '2017-02-24'AND DateAccessed <= '2017-02-28'
			// GROUP BY DATE(DateAccessed);
			if (ajaxHandlerAction.getBdmId() != null && !"".equalsIgnoreCase(ajaxHandlerAction.getBdmId())) {

				query = query + " AND t1.Id  = '" + ajaxHandlerAction.getBdmId() + "'";

			}

			//
			// if (ajaxHandlerAction.getEndDate() != null &&
			// !"".equalsIgnoreCase(ajaxHandlerAction.getEndDate())) {
			// ajaxHandlerAction.setEndDate(DateUtility.getInstance().convertStringToMySQLDate(ajaxHandlerAction.getEndDate()));
			// query = query + "AND DateAccessed <= '" +
			// ajaxHandlerAction.getEndDate() + "'";
			//
			// }
			//
			query = query + "  ORDER BY EmployeeName";

			// System.out.println("query.."+query);
			statement = connection.createStatement();
			resultSet = statement.executeQuery(query);
			while (resultSet.next()) {
				count++;
				int Id = resultSet.getInt("Id");
				String employeeName = resultSet.getString("EmployeeName");
				String EmailId = resultSet.getString("EmailId");
				String PhoneNo = resultSet.getString("PhoneNo");
				String ReportsTo = resultSet.getString("ReportsTo");
				// String ReportsToName=
				// DataSourceDataProvider.getInstance().getemployeenamebyloginId(ReportsTo);
				// System.out.println("ReportsToName is----->"+ReportsToName);
				response = response + count + "#^$" + Id + "#^$" + employeeName + "#^$" + EmailId + "#^$" + PhoneNo
						+ "#^$" + ReportsTo + "*@!";

				// response=response+employeeName+"*@!";
				// System.out.println("count is----->"+count);
			}
			response = response + "addto";

			response = response + count;
			// System.out.println("query..."+query);
			// System.out.println("response.."+response);
		} catch (Exception sqe) {
			sqe.printStackTrace();
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (statement != null) {
					statement.close();
					statement = null;
				}

				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException sqle) {
				sqle.printStackTrace();
			}
		}

		return response;
	}

	public String getSalesTeamforBDMAssociate(String salesName) throws ServiceLocatorException {
		boolean isGetting = false;
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		StringBuffer sb = new StringBuffer();
		String query = null;
		try {

			query = "SELECT CONCAT(TRIM(FName),'.',TRIM(LName)) AS FullName,Id FROM tblEmployee "
					+ "WHERE (LName LIKE '" + salesName + "%' OR FName LIKE '" + salesName
					+ "%') AND DepartmentId='Sales'" + "AND CurStatus='Active'";

			// System.out.println("query-->"+query);
			connection = ConnectionProvider.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(query);
			resultSet = preparedStatement.executeQuery();

			int count = 0;
			sb.append("<xml version=\"1.0\">");
			sb.append("<EMPLOYEES>");
			while (resultSet.next()) {
				sb.append("<EMPLOYEE><VALID>true</VALID>");

				if (resultSet.getString(1) == null || resultSet.getString(1).equals("")) {
					sb.append("<NAME>NoRecord</NAME>");
				} else {
					String title = resultSet.getString(1);
					if (title.contains("&")) {
						title = title.replace("&", "&amp;");
					}
					sb.append("<NAME>" + title + "</NAME>");
				}
				// sb.append("<NAME>" +resultSet.getString(1) + "</NAME>");
				sb.append("<EMPID>" + resultSet.getInt(2) + "</EMPID>");
				sb.append("</EMPLOYEE>");
				isGetting = true;
				count++;
			}

			if (!isGetting) {
				// sb.append("<EMPLOYEES>" + sb.toString() + "</EMPLOYEES>");
				// } else {
				isGetting = false;
				// nothing to show
				// response.setStatus(HttpServletResponse.SC_NO_CONTENT);
				sb.append("<EMPLOYEE><VALID>false</VALID></EMPLOYEE>");
			}
			sb.append("</EMPLOYEES>");
			sb.append("</xml>");

			// System.out.println(sb.toString());
		} catch (SQLException sqle) {
			throw new ServiceLocatorException(sqle);
		} finally {
			try {
				if (resultSet != null) {

					resultSet.close();
					resultSet = null;
				}
				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}

				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException sql) {
				// System.err.print("Error :"+sql);
			}

		}
		// System.out.println("the string "+sb.toString());
		return sb.toString();
	}

	public String getSalesTeamDetails(String teamMemberId, String bdmId) throws ServiceLocatorException {
		StringBuffer stringBuffer = new StringBuffer();
		Connection connection = null;
		CallableStatement callableStatement = null;
		PreparedStatement preparedStatement = null;
		Statement statement = null;
		ResultSet resultSet = null;
		String activityDetails = "";
		String queryString;
		int i = 0;
		JSONObject subJson = null;
		;
		try {
			// queryString = "SELECT CONCAT(tblEmployee.FName,'
			// ',tblEmployee.MName,'.',tblEmployee.LName) AS
			// EmployeeName,ReviewType,EmpId,ReviewTypeId,tblEmpReview.Status,EmpComments,tblEmpReview.CreatdBy,tblEmpReview.CreatedDate,AttachmentName,AttachmentLocation,ReviewName,tblEmpReview.Id
			// AS
			// Id,HrComments,TLComments,ReviewDate,TLRating,HRRating,MaxPoints,ApprovedBy1,Approver1Date,Approver2Date,ApprovedBy2,Approver3Date,ApprovedBy3,OnSiteMgrRating,OnSiteMgrComments,OnSiteMgrStatus,HRStatus,BillingAmount,LogosCount,tblEmpReview.UserId
			// as UserId FROM tblEmpReview JOIN tblLkReviews ON (ReviewTypeId =
			// tblLkReviews.Id) JOIN tblEmployee ON (tblEmpReview.UserId =
			// tblEmployee.LoginId) WHERE tblEmpReview.Id=" + reviewId;

			queryString = "SELECT CONCAT(TRIM(FName),'.',TRIM(LName)) AS EmployeeName,tblBDMAssociates.Id AS Id,tblBDMAssociates.STATUS AS Status,tblBDMAssociates.TeamMemberId AS TeamMemberId"
					+ " FROM tblEmployee LEFT JOIN tblBDMAssociates ON(tblEmployee.Id=tblBDMAssociates.TeamMemberId) WHERE tblBDMAssociates.TeamMemberId='"
					+ teamMemberId + "' AND tblBDMAssociates.BdmId='" + bdmId + "'";

			// System.out.println("queryString-->"+queryString);
			connection = ConnectionProvider.getInstance().getConnection();
			statement = connection.createStatement();
			resultSet = statement.executeQuery(queryString);
			if (resultSet.next()) {
				subJson = new JSONObject();
				subJson.put("Id", resultSet.getInt("Id"));
				subJson.put("EmployeeName", resultSet.getString("EmployeeName"));
				subJson.put("Status", resultSet.getString("Status"));
				subJson.put("TeamMemberId", resultSet.getString("TeamMemberId"));

			}

		} catch (Exception sqe) {
			sqe.printStackTrace();
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (statement != null) {
					statement.close();
					statement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException sqle) {
			}
		}
		return subJson.toString();
	}

	public int addSalesToBdm(NewAjaxHandlerAction newAjaxHandlerAction) throws ServiceLocatorException {
		// System.out.println("in addSalesToBdm");
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		CallableStatement callableStatement = null;
		int isInserted = 0;

		// queryString = "insert into tblEmpReview
		// (ReviewTypeId,EmpComments,AttachmentName,AttachmentLocation,EmpId,CreatdBy,ReviewName,ReviewDate,Status,TLRating,HRRating,MaxPoints,UserId,TLComments,HrComments,ApprovedBy2,Approver2Date)
		// values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		try {
			String emailId = DataSourceDataProvider.getInstance()
					.getEmailIdForEmployee(Integer.parseInt(newAjaxHandlerAction.getPreAssignSalesId()));
			String workPhn = DataSourceDataProvider.getInstance()
					.getWorkPhNoById(newAjaxHandlerAction.getPreAssignSalesId());
			connection = ConnectionProvider.getInstance().getConnection();
			callableStatement = connection.prepareCall("{call spBDMAssociates(?,?,?,?,?,?,?)}");
			callableStatement.setString(1, newAjaxHandlerAction.getPreAssignSalesId());
			callableStatement.setString(2, newAjaxHandlerAction.getBdmId());
			callableStatement.setString(3, emailId);
			callableStatement.setString(4, "Active");
			callableStatement.setString(5, workPhn);
			callableStatement.setString(6, newAjaxHandlerAction.getCreatedBy());
			callableStatement.setString(7, newAjaxHandlerAction.getCreatedBy());
			isInserted = callableStatement.executeUpdate();

			// System.out.println("isInserted " + isInserted);
		} catch (SQLException se) {
			se.printStackTrace();
			throw new ServiceLocatorException(se);
		} finally {
			try {
				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException se) {
				throw new ServiceLocatorException(se);
			}
		}

		return isInserted;

	}

	public boolean checkSalesAgainstBdm(NewAjaxHandlerAction newAjaxHandlerAction) throws ServiceLocatorException {
		// System.out.println("in impl of resourcename");
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		String strConsId = "";
		String responseString = "";

		// String strCategory = "";
		boolean isInserted = true;
		String teamMemberId = null;
		String Status = null;
		String BdmId = null;
		String queryString = "SELECT TeamMemberId,Status,BdmId FROM tblBDMAssociates";
		// System.out.println("queryString----->"+queryString);
		try {
			connection = ConnectionProvider.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(queryString);
			resultSet = preparedStatement.executeQuery();
			// System.out.println("getResource Id
			// isss---->"+newAjaxHandlerAction.getBdmId());
			while (resultSet.next()) {
				teamMemberId = resultSet.getString("TeamMemberId");
				Status = resultSet.getString("Status");
				BdmId = resultSet.getString("BdmId");
				// System.out.println("empName in while loop
				// is---->"+teamMemberId);
				// System.out.println("Status in while loop is---->"+Status);
				// System.out.println("BdmId in while loop is---->"+BdmId);

				// System.out.println("final member is--->"+teamMemberId);
				// System.out.println("adminAction.getPreAssignSalesId()
				// is--->"+newAjaxHandlerAction.getPreAssignSalesId());
				// System.out.println("adminAction.getPreAssignSalesId()
				// is--->"+newAjaxHandlerAction.getPreAssignSalesId());
				// System.out.println("adminAction.getPreAssignSalesId()
				// is--->"+newAjaxHandlerAction.getBdmId());
				if (teamMemberId.equals(newAjaxHandlerAction.getPreAssignSalesId()) && Status.equals("Active")
						&& BdmId.equals(newAjaxHandlerAction.getBdmId())) {
					// if(Status.equals("Active")){
					// if(BdmId.equals(newAjaxHandlerAction.getBdmId())){
					// System.out.println("in equals case");
					responseString = "sorry! the Sales has been already associated";
					isInserted = false;
					break;
					// }
					// }
				}
			}

			// System.out.println("empName is---->"+teamMemberId);

			preparedStatement.execute();
			// System.out.println("isInserted for resourcename
			// is---->"+isInserted);
		} catch (SQLException se) {
			se.printStackTrace();
			throw new ServiceLocatorException(se);
		} finally {
			try {
				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException se) {
				throw new ServiceLocatorException(se);
			}
		}

		return isInserted;

	}

	public String checkBdmAddedName(NewAjaxHandlerAction newAjaxHandlerAction) throws ServiceLocatorException {
		// String qsTitle = "";
		Statement statement = null;
		ResultSet resultSet = null;
		String query = "";
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		String assignedBdmId = null;
		String response = "";
		int count = 0;
		try {

			// System.out.println("in else case");
			String queryString = "SELECT TeamMemberId,BdmId,Status FROM tblBDMAssociates";
			connection = ConnectionProvider.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(queryString);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				String teamMemberId = resultSet.getString("TeamMemberId");
				assignedBdmId = resultSet.getString("BdmId");
				String status = resultSet.getString("Status");

				// System.out.println("empName in while loop
				// is---->"+teamMemberId);

				// System.out.println("final member is--->"+teamMemberId);
				// System.out.println("adminAction.getPreAssignSalesId()
				// is--->"+newAjaxHandlerAction.getPreAssignSalesId());
				if (teamMemberId.equals(newAjaxHandlerAction.getPreAssignSalesId()) && status.equals("Active")) {
					// System.out.println("in equals case");

					break;
				}

			}
			// System.out.println("assignedBdmId final is---->"+assignedBdmId);
			String assignedBdmName = DataSourceDataProvider.getInstance()
					.getEmployeeNameByEmpNo(Integer.parseInt(assignedBdmId));
			response = "<font size='2' color='red'>Resource Name has been alerady assocaited with BDM '"
					+ assignedBdmName + "'</font>";
			// System.out.println("getResponseString is---->"+response);

			// System.out.println("in service impl ajax");

			// System.out.println("query..."+query);
			// System.out.println("response.."+response);
		} catch (Exception sqe) {
			sqe.printStackTrace();
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (statement != null) {
					statement.close();
					statement = null;
				}

				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException sqle) {
				sqle.printStackTrace();
			}
		}

		return response;
	}

	public boolean updateBdmTeam(NewAjaxHandlerAction newAjaxHandlerAction) throws ServiceLocatorException {
		// System.out.println("in updateBdmTeam------");
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		boolean isUpdated = false;
		String queryString = "";

		// System.out.println("newAjaxHandlerAction.getTeamMemberId()----->"+newAjaxHandlerAction.getTeamMemberId());
		// System.out.println("newAjaxHandlerAction.getTeamMemberStatus()----->"+newAjaxHandlerAction.getTeamMemberStatus());
		// System.out.println("newAjaxHandlerAction.getBdmId()----->"+newAjaxHandlerAction.getBdmId());
		// queryString = "update tblEmpReview set
		// ReviewTypeId=?,EmpComments=?,ModifiedBy=?,ModifiedDate=?,ReviewName=?,ReviewDate=?,Status=?,TLComments=?,TLRating=?,HRRating=?,HrComments=?
		// where Id=?";
		// if(roleName.equalsIgnoreCase("Employee"))
		String emailId = DataSourceDataProvider.getInstance()
				.getEmailIdForEmployee(Integer.parseInt(newAjaxHandlerAction.getTeamMemberId()));
		String workPhn = DataSourceDataProvider.getInstance().getWorkPhNoById(newAjaxHandlerAction.getTeamMemberId());
		// System.out.println("emailId----->"+emailId);
		// System.out.println("workPhn----->"+workPhn);
		queryString = "update tblBDMAssociates set TeamMemberId=?,BdmId=?,EmailId=?,STATUS=?,PhoneNumber=?,ModifiedBy=?,ModifiedDate=? where Id=?";
		// System.out.println("queryString---->"+queryString);
		try {
			connection = ConnectionProvider.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(queryString);
			// preparedStatement.setString(1,ajaxHandlerAction.getOverlayReviewType());
			preparedStatement.setString(1, newAjaxHandlerAction.getTeamMemberId());
			preparedStatement.setString(2, newAjaxHandlerAction.getBdmId());
			preparedStatement.setString(3, emailId);
			preparedStatement.setString(4, newAjaxHandlerAction.getTeamMemberStatus());
			preparedStatement.setString(5, workPhn);
			preparedStatement.setString(6, newAjaxHandlerAction.getCreatedBy());
			preparedStatement.setTimestamp(7, DateUtility.getInstance().getCurrentMySqlDateTime());
			preparedStatement.setInt(8, newAjaxHandlerAction.getId());

			isUpdated = preparedStatement.execute();
			// System.out.println("queryString---->"+queryString);
			// System.out.println("isUpdated");
		} catch (SQLException se) {
			se.printStackTrace();
			throw new ServiceLocatorException(se);
		} finally {
			try {
				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException se) {
				throw new ServiceLocatorException(se);
			}
		}

		return isUpdated;

	}

	public boolean checkBdmStatus(NewAjaxHandlerAction newAjaxHandlerAction) throws ServiceLocatorException {
		// System.out.println("in impl of checkBdmStatus");
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		String strConsId = "";
		String responseString = "";

		// String strCategory = "";
		boolean isInserted = true;
		String teamMemberId = null;
		String Status = null;
		String BdmId = null;
		String queryString = "SELECT TeamMemberId,Status,BdmId FROM tblBDMAssociates where BdmId='"
				+ newAjaxHandlerAction.getBdmId() + "'";
		// System.out.println("queryString----->"+queryString);
		try {
			connection = ConnectionProvider.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(queryString);
			resultSet = preparedStatement.executeQuery();
			// System.out.println("newAjaxHandlerAction.getPreAssignSalesId() Id
			// isss---->"+newAjaxHandlerAction.getPreAssignSalesId());
			// System.out.println("newAjaxHandlerAction.getBdmId() Id
			// isss---->"+newAjaxHandlerAction.getBdmId());
			while (resultSet.next()) {
				teamMemberId = resultSet.getString("TeamMemberId");
				Status = resultSet.getString("Status");
				BdmId = resultSet.getString("BdmId");
				if (teamMemberId.equals(newAjaxHandlerAction.getPreAssignSalesId())
						&& BdmId.equals(newAjaxHandlerAction.getBdmId()) && Status.equals("Inactive")) {
					// if(Status.equals("Active")){
					// if(BdmId.equals(newAjaxHandlerAction.getBdmId())){
					// System.out.println("in equals case of checkBdmStatus");
					responseString = "sorry! the Sales has been already associated";
					isInserted = false;
					break;
					// }
					// }
				}
			}

			// System.out.println("empName is---->"+teamMemberId);

			preparedStatement.execute();
			// System.out.println("isInserted for resourcename
			// is---->"+isInserted);
		} catch (SQLException se) {
			se.printStackTrace();
			throw new ServiceLocatorException(se);
		} finally {
			try {
				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException se) {
				throw new ServiceLocatorException(se);
			}
		}

		return isInserted;

	}

	/*
	 * BDM Association end
	 */

	/*
	 * Author :Teja Kadamanti Date : 04/20/2017 Description: Multiple Projects
	 * Employee Details start
	 * 
	 */

	public String getMultipleProjectsEmployeeList(NewAjaxHandlerAction newAjaxHandlerAction,
			HttpServletRequest httpServletRequest) throws ServiceLocatorException {
		// String qsTitle = "";
		Statement statement = null;
		ResultSet resultSet = null;
		String query = "";
		Connection connection = null;
		String response = "";
		String department = "";
		String Practice = "";
		String subPractice = "";
		String empNo = "";
		try {
			connection = ConnectionProvider.getInstance().getConnection();
			// query = "SELECT ObjectId,CONCAT(FName,'',MName,'.',LName) AS
			// NAME,DepartmentId,PracticeId,SubPractice,COUNT(tblProjectContacts.Id)
			// AS projectCount FROM tblProjectContacts LEFT JOIN tblEmployee ON
			// tblProjectContacts.ObjectId=tblEmployee.Id WHERE STATUS='Active'
			// AND CurStatus='Active' ";
			query = "SELECT ObjectId,CONCAT(FName,'',MName,'.',LName) AS NAME,DepartmentId,EmpNo,PracticeId,SubPractice,COUNT(tblProjectContacts.Id) AS projectCount FROM tblProjectContacts LEFT JOIN tblEmployee ON tblProjectContacts.ObjectId=tblEmployee.Id WHERE STATUS='Active' AND  CurStatus='Active'";

			if (((!"".equals(newAjaxHandlerAction.getPreAssignEmpId())))
					&& (newAjaxHandlerAction.getPreAssignEmpId() != 0)) {

				query = query + " AND tblProjectContacts.ObjectId= '" + newAjaxHandlerAction.getPreAssignEmpId() + "'";

			}
			if (((!"".equals(newAjaxHandlerAction.getDepartmentId())))
					&& (!"-1".equals(newAjaxHandlerAction.getDepartmentId()))) {

				query = query + " AND tblEmployee.DepartmentId = '" + newAjaxHandlerAction.getDepartmentId() + "'";

			}

			if (((!"".equals(newAjaxHandlerAction.getPracticeId())))
					&& (!"-1".equals(newAjaxHandlerAction.getPracticeId()))) {

				query = query + " AND tblEmployee.PracticeId = '" + newAjaxHandlerAction.getPracticeId() + "'";

			}
			if (((!"".equals(newAjaxHandlerAction.getSubPracticeId())))
					&& (!"-1".equals(newAjaxHandlerAction.getSubPracticeId()))) {

				query = query + " AND tblEmployee.SubPractice = '" + newAjaxHandlerAction.getSubPracticeId() + "'";

			}

			query = query + " GROUP BY ObjectId HAVING COUNT(tblProjectContacts.Id)>1 ORDER BY  FName";
			// System.out.println("query..."+query);
			statement = connection.createStatement();
			resultSet = statement.executeQuery(query);
			while (resultSet.next()) {
				if ("".equals(resultSet.getString("DepartmentId"))) {
					department = "-";
				} else {
					department = resultSet.getString("DepartmentId");
				}
				if ("".equals(resultSet.getString("PracticeId"))) {
					Practice = "-";
				} else {
					Practice = resultSet.getString("PracticeId");
				}
				if ("".equals(resultSet.getString("SubPractice"))) {
					subPractice = "-";
				} else {
					subPractice = resultSet.getString("SubPractice");
				}
				if ("".equals(resultSet.getString("EmpNo"))) {
					empNo = "-";
				} else {
					empNo = resultSet.getString("EmpNo");
				}
				// response = response + resultSet.getInt("ObjectId") + "#^$" +
				// resultSet.getString("NAME") + "#^$" + department + "#^$" +
				// Practice + "#^$" +subPractice +
				// "#^$"+resultSet.getInt("projectCount") + "*@!";
				response = response + resultSet.getInt("ObjectId") + "#^$" + resultSet.getString("NAME") + "#^$" + empNo
						+ "#^$" + department + "#^$" + Practice + "#^$" + subPractice + "#^$"
						+ resultSet.getInt("projectCount") + "*@!";
			}

		} catch (Exception sqe) {
			sqe.printStackTrace();
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (statement != null) {
					statement.close();
					statement = null;
				}

				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException sqle) {
				sqle.printStackTrace();
			}
		}

		return response;
	}

	public String multipleProjectsEmployeeListDetails(int preAssignedEmpId, HttpServletRequest httpServletRequest)
			throws ServiceLocatorException {
		// String qsTitle = "";
		Statement statement = null;
		ResultSet resultSet = null;
		String query = "";
		Connection connection = null;
		int totalUtilization = 0;
		String response = "";
		try {
			connection = ConnectionProvider.getInstance().getConnection();
			query = "SELECT tblCrmAccount.NAME,tblProjects.ProjectName,DATE(StartDate) AS StartDate,CASE WHEN EndDate IS NOT NULL THEN DATE(EndDate) ELSE '-' END AS EndDate,CASE WHEN EmpProjStatus!='-1' THEN EmpProjStatus ELSE '-' END AS EmpProjStatus,Billable,Utilization,tblProjects.ProjectType FROM tblProjectContacts JOIN  tblEmployee ON tblProjectContacts.ObjectId=tblEmployee.Id JOIN tblCrmAccount ON tblCrmAccount.Id=tblProjectContacts.AccountId JOIN tblProjects ON tblProjects.Id=tblProjectContacts.ProjectId  WHERE tblProjectContacts.STATUS='Active' AND  CurStatus='Active' AND ObjectId="
					+ preAssignedEmpId;

			statement = connection.createStatement();
			resultSet = statement.executeQuery(query);
			while (resultSet.next()) {
				totalUtilization = totalUtilization + Integer.parseInt(resultSet.getString("Utilization"));
				String ProjectType = resultSet.getString("ProjectType");

				if (ProjectType == null || "".equalsIgnoreCase(ProjectType)) {
					ProjectType = "-";

				}

				response = response + resultSet.getString("NAME") + "#^$" + resultSet.getString("ProjectName") + "#^$"
						+ resultSet.getString("StartDate") + "#^$" + resultSet.getString("EndDate") + "#^$"
						+ resultSet.getString("EmpProjStatus") + "#^$" + resultSet.getString("Billable") + "#^$"
						+ resultSet.getString("Utilization") + "#^$" + resultSet.getString("ProjectType") + "*@!";
			}
			response = response + "addTo" + totalUtilization;
		} catch (Exception sqe) {
			sqe.printStackTrace();
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (statement != null) {
					statement.close();
					statement = null;
				}

				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException sqle) {
				sqle.printStackTrace();
			}
		}

		return response;
	}

	/*
	 * Author :Teja Kadamanti Date : 04/20/2017 Description: Multiple Projects
	 * Employee Details end
	 * 
	 */

	/*
	 * Author :Triveni
	 */
	public String getConsultantNames(String query) throws ServiceLocatorException {
		boolean isGetting = false;
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		StringBuffer sb = new StringBuffer();
		try {
			connection = ConnectionProvider.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(query);
			resultSet = preparedStatement.executeQuery();

			int count = 0;
			sb.append("<xml version=\"1.0\">");
			sb.append("<CONSULTANTS>");
			while (resultSet.next()) {
				sb.append("<CONSULTANT><VALID>true</VALID>");

				if (resultSet.getString(1) == null || resultSet.getString(1).equals("")) {
					sb.append("<NAME>NoRecord</NAME>");
				} else {
					String title = resultSet.getString(1);
					if (title.contains("&")) {
						title = title.replace("&", "&amp;");
					}
					sb.append("<NAME>" + title + "</NAME>");
				}
				// sb.append("<NAME>" +resultSet.getString(1) + "</NAME>");
				// sb.append("<EMPLOGINID>" + resultSet.getString(2) +
				// "</EMPLOGINID>");
				sb.append("</CONSULTANT>");
				isGetting = true;
				count++;
			}

			if (!isGetting) {
				// sb.append("<EMPLOYEES>" + sb.toString() + "</EMPLOYEES>");
				// } else {
				isGetting = false;
				// nothing to show
				// response.setStatus(HttpServletResponse.SC_NO_CONTENT);
				sb.append("<CONSULTANT><VALID>false</VALID></CONSULTANT>");
			}
			sb.append("</CONSULTANTS>");
			sb.append("</xml>");
		} catch (SQLException sqle) {
			throw new ServiceLocatorException(sqle);
		} finally {
			try {
				if (resultSet != null) {

					resultSet.close();
					resultSet = null;
				}
				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}

				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException sql) {
				// System.err.print("Error :"+sql);
			}

		}
		// System.out.println(sb.toString());
		return sb.toString();
	}

	public String getRespectiveVideoTitleMap(String accesType) throws ServiceLocatorException {
		StringBuffer stringBuffer = new StringBuffer("");
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		Connection connection = null;
		String videiTitle = null;
		int videoId = 0;

		// queryString = "SELECT distinct tblProjectContacts.ProjectId AS
		// projectId,ProjectName FROM tblProjectContacts LEFT OUTER JOIN
		// tblProjects ON(tblProjectContacts.ProjectId=tblProjects.Id) WHERE
		// AccountId="+accountId;
		String queryString = "SELECT Id,VideoTitle FROM tblLKDigitalVideoManagement WHERE STATUS='Active' AND VideoAccessType =?";

		try {
			connection = ConnectionProvider.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(queryString);
			preparedStatement.setString(1, accesType);
			resultSet = preparedStatement.executeQuery();
			stringBuffer.append("<xml version=\"1.0\">");
			stringBuffer.append("<DIGITALVIDEOMANAGEMENT>");
			stringBuffer.append("<USER videoId=\"-1\">--Please Select--</USER>");
			while (resultSet.next()) {
				videoId = resultSet.getInt("Id");
				videiTitle = resultSet.getString("VideoTitle");

				// projects.append(projectName);

				stringBuffer.append("<USER videoId=\"" + videoId + "\">");
				if (videiTitle.contains("&")) {
					videiTitle = videiTitle.replace("&", "&amp;");
				}
				stringBuffer.append(videiTitle);
				stringBuffer.append("</USER>");

			}
			stringBuffer.append("</DIGITALVIDEOMANAGEMENT>");
			stringBuffer.append("</xml>");
		} catch (SQLException ex) {
			ex.printStackTrace();
		} catch (ServiceLocatorException sle) {
			sle.printStackTrace();
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
		}
		// System.out.println("Team List: "+projects.toString());
		return stringBuffer.toString();
	}

	public String getMobileAppDetails(NewAjaxHandlerAction newAjaxHandlerAction, HttpServletRequest httpServletRequest)
			throws ServiceLocatorException {
		// String qsTitle = "";
		Statement statement = null;
		ResultSet resultSet = null;
		String query = "";
		Connection connection = null;
		int count = 0;
		String response = "";
		try {

			connection = ConnectionProvider.getInstance().getConnection();
			query = "SELECT tblMiracleMeLogUserAccess.Id,tblMiracleMeLogUserAccess.LoginId,MAX(ActivityDateTime) AS ActivityDateTime,ActivityStatus,CONCAT(FName,'',MName,'.',LName) AS EmpName FROM tblMiracleMeLogUserAccess JOIN tblEmployee ON tblEmployee.LoginId=tblMiracleMeLogUserAccess.LoginId WHERE 1=1";
			if (!"-1".equals(newAjaxHandlerAction.getActivityType())
					&& newAjaxHandlerAction.getActivityType() != null) {
				query = query + " AND ActivityStatus='" + newAjaxHandlerAction.getActivityType() + "'";
			}
			if (!"-1".equals(newAjaxHandlerAction.getLocation()) && newAjaxHandlerAction.getLocation() != null) {
				query = query + " AND Location='" + newAjaxHandlerAction.getLocation() + "'";
			}
			if (!"-1".equals(newAjaxHandlerAction.getCountry()) && newAjaxHandlerAction.getCountry() != null) {
				query = query + " AND Country='" + newAjaxHandlerAction.getCountry() + "'";
			}
			if (!"".equals(newAjaxHandlerAction.getEmployeeName()) && newAjaxHandlerAction.getEmployeeName() != null) {
				query = query + " AND (FName LIKE '" + newAjaxHandlerAction.getEmployeeName() + "' or LName LIKE '"
						+ newAjaxHandlerAction.getEmployeeName() + "' or MName LIKE '"
						+ newAjaxHandlerAction.getEmployeeName() + "') ";
			}
			if (!"".equals(newAjaxHandlerAction.getEmpId()) && newAjaxHandlerAction.getEmpId() != null) {
				query = query + " AND EmpNo='" + newAjaxHandlerAction.getEmpId() + "'";
			}
			if (!"-1".equals(newAjaxHandlerAction.getDepartmentId())
					&& newAjaxHandlerAction.getDepartmentId() != null) {
				query = query + " AND DepartmentId='" + newAjaxHandlerAction.getDepartmentId() + "'";
			}

			query = query + " GROUP BY LoginId,ActivityStatus ORDER BY ActivityDateTime DESC";
			statement = connection.createStatement();
			resultSet = statement.executeQuery(query);
			while (resultSet.next()) {
				count = count + 1;
				response = response + count + "#^$" + resultSet.getString("EmpName") + "#^$"
						+ resultSet.getString("LoginId") + "#^$" + resultSet.getString("ActivityDateTime") + "#^$"
						+ resultSet.getString("ActivityStatus") + "#^$" + resultSet.getString("Id") + "*@!";
			}
			response = response + "addTo" + count;
		} catch (Exception sqe) {
			sqe.printStackTrace();
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (statement != null) {
					statement.close();
					statement = null;
				}

				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException sqle) {
				sqle.printStackTrace();
			}
		}

		return response;
	}

	/*
	 * Author : Phani Kanuri Date : 05/10/2017 Desc: Qreview Dashboard
	 */

	public String getQReviewDashBoardByStatus(String year, String quarter, HttpServletRequest httpServletRequest)
			throws ServiceLocatorException {
		String response = "";
		// Connection connection = null;
		CallableStatement callableStatement = null;
		Connection connection = null;
		String teamMembersKeys = "";

		Map teamMembersMapByLoginid = null;
		Map teamMembersMapByReportsTo = null;

		try {
			connection = ConnectionProvider.getInstance().getConnection();

			// teamMembersMapByLoginid = (Map)
			// httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_MY_TEAM_MAP);
			String loginId = (String) httpServletRequest.getSession(false)
					.getAttribute(ApplicationConstants.SESSION_USER_ID);
			callableStatement = connection.prepareCall("{call spQReviewResourcesVsStatus(?,?,?)}");

			callableStatement.setString(1, year);
			callableStatement.setString(2, quarter);
			callableStatement.registerOutParameter(3, Types.VARCHAR);
			callableStatement.execute();

			response = callableStatement.getString(3);

		} catch (SQLException se) {
			se.printStackTrace();
		} finally {
			try {

				if (callableStatement != null) {
					callableStatement.close();
					callableStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		return response;
	}

	public String getPracticeVsStatusStackChart(String year, String quarter, HttpServletRequest httpServletRequest)
			throws ServiceLocatorException {
		String response = "";
		// Connection connection = null;
		CallableStatement callableStatement = null;
		Connection connection = null;
		String teamMembersKeys = "";

		Map teamMembersMapByLoginid = null;
		Map teamMembersMapByReportsTo = null;

		try {
			connection = ConnectionProvider.getInstance().getConnection();

			// teamMembersMapByLoginid = (Map)
			// httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_MY_TEAM_MAP);
			String loginId = (String) httpServletRequest.getSession(false)
					.getAttribute(ApplicationConstants.SESSION_USER_ID);
			callableStatement = connection.prepareCall("{call spQReviewPracticeVsStatus(?,?,?)}");

			callableStatement.setString(1, year);
			callableStatement.setString(2, quarter);
			callableStatement.registerOutParameter(3, Types.VARCHAR);
			callableStatement.execute();

			response = callableStatement.getString(3);

		} catch (SQLException se) {
			se.printStackTrace();
		} finally {
			try {

				if (callableStatement != null) {
					callableStatement.close();
					callableStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		return response;
	}

	public String getClosedVsQuarterStackChart(String year, String quarter, HttpServletRequest httpServletRequest)
			throws ServiceLocatorException {
		String response = "";
		// Connection connection = null;
		CallableStatement callableStatement = null;
		Connection connection = null;
		String teamMembersKeys = "";

		Map teamMembersMapByLoginid = null;
		Map teamMembersMapByReportsTo = null;

		try {
			connection = ConnectionProvider.getInstance().getConnection();

			// teamMembersMapByLoginid = (Map)
			// httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_MY_TEAM_MAP);
			String loginId = (String) httpServletRequest.getSession(false)
					.getAttribute(ApplicationConstants.SESSION_USER_ID);
			callableStatement = connection.prepareCall("{call spQReviewClosedVsQuarter(?,?,?)}");

			callableStatement.setString(1, year);
			callableStatement.setString(2, quarter);
			callableStatement.registerOutParameter(3, Types.VARCHAR);
			callableStatement.execute();

			response = callableStatement.getString(3);

		} catch (SQLException se) {
			se.printStackTrace();
		} finally {
			try {

				if (callableStatement != null) {
					callableStatement.close();
					callableStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		return response;
	}

	public String getQReviewDashBoardByPractice(String qReviewStartDate, String qReviewEndDate,
			HttpServletRequest httpServletRequest) throws ServiceLocatorException {
		// String qsTitle = "";
		Statement statement = null;
		ResultSet resultSet = null;
		String query = "";
		Connection connection = null;
		String response = "";
		try {
			connection = ConnectionProvider.getInstance().getConnection();
			query = "SELECT Practice," + "SUM(CASE WHEN (STATUS = 'Approved') THEN 1 ELSE 0 END) AS Approved,"
					+ "SUM(CASE WHEN (STATUS = 'Submitted') THEN 1 ELSE 0 END) AS Submitted,"
					+ "SUM(CASE WHEN (STATUS = 'Entered') THEN 1 ELSE 0 END) AS Entered,"
					+ "SUM(CASE WHEN (OpperationTeamStatus = 'Rejected') THEN 1 ELSE 0 END) AS OpperationRejected,"
					+ "SUM(CASE WHEN (OpperationTeamStatus = 'Closed') THEN 1 ELSE 0 END) AS OpperationClosed FROM tblQuarterlyAppraisals "
					+ "WHERE (Practice!=NULL OR Practice!='')";
			if (qReviewStartDate != null && !"".equals(qReviewStartDate)) {
				query = query + " and  DATE(CreatedDate)>='"
						+ DateUtility.getInstance().convertStringToMySQLDate(qReviewStartDate) + "'";
			}
			if (qReviewEndDate != null && !"".equals(qReviewEndDate)) {
				query = query + " and  DATE(CreatedDate)<='"
						+ DateUtility.getInstance().convertStringToMySQLDate(qReviewEndDate) + "'";
			}

			query = query + " GROUP BY Practice";
			// System.out.println("query.."+query);
			statement = connection.createStatement();
			resultSet = statement.executeQuery(query);
			while (resultSet.next()) {
				response = response + "Approved#^$" + resultSet.getInt("Approved") + "*@!" + "Submitted#^$"
						+ resultSet.getInt("Submitted") + "*@!" + "Entered#^$" + resultSet.getInt("Entered") + "*@!"
						+ "OpperationRejected#^$" + resultSet.getInt("OpperationRejected") + "*@!"
						+ "OpperationClosed#^$" + resultSet.getInt("OpperationClosed") + "*@!";
			}
			// System.out.println("query..."+query);
			// System.out.println("response.."+response);
		} catch (Exception sqe) {
			sqe.printStackTrace();
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (statement != null) {
					statement.close();
					statement = null;
				}

				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException sqle) {
				sqle.printStackTrace();
			}
		}

		return response;
	}

	public String getQReviewListByStatus(String qReviewYear, String qReviewQuarter, String activityType,
			HttpServletRequest httpServletRequest) throws ServiceLocatorException {

		String response = "";
		// Connection connection = null;
		CallableStatement callableStatement = null;
		Connection connection = null;
		String teamMembersKeys = "";

		Map teamMembersMapByLoginid = null;
		Map teamMembersMapByReportsTo = null;
		int i = 0;
		String queryString = "";
		String queryString1 = "";

		Statement statement = null;
		ResultSet resultSet = null;
		String EmpName = "-";
		String STATUS = "-";
		String WorkPhoneNo = "-";
		String EmpCurrentStatus = "-";
		String Practice = "-";
		String Title = "-";
		String ReportsToName = "-";
		String OpperationTeamStatus = "-";
		String EmpId = "-";
		String ReportsTo = "-";
		String DepartmentId = "-";
		String Id = "-";
		String OpsContactId = "-";
		String Email1 = "-";
		String PracticeId = "-";
		String SubPractice = "-";
		String Location = "-";
		try {
			connection = ConnectionProvider.getInstance().getConnection();

			// teamMembersMapByLoginid = (Map)
			// httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_MY_TEAM_MAP);
			String loginId = (String) httpServletRequest.getSession(false)
					.getAttribute(ApplicationConstants.SESSION_USER_ID);

			if (!activityType.equals("NotEntered")) {
				queryString = "SELECT tblQuarterlyAppraisals.LoginId,EmpId,DATE(tblQuarterlyAppraisals.CreatedDate) AS CreatedDate,STATUS,EmpCurrentStatus,Practice,Title,tblQuarterlyAppraisals.ReportsTo,OpperationTeamStatus,"
						+ " CONCAT(t1.FName,'',t1.MName,'.',t1.LName) AS EmpName,CONCAT(t2.FName,'',t2.MName,'.',t2.LName) AS ReportsToName"
						+ " FROM tblQuarterlyAppraisals JOIN tblEmployee t1 ON t1.LoginId=tblQuarterlyAppraisals.LoginId JOIN tblEmployee t2 ON t2.LoginId=tblQuarterlyAppraisals.ReportsTo WHERE 1=1 "
						+ " ANd YEAR(tblQuarterlyAppraisals.CreatedDate)>='" + qReviewYear + "' AND Quarterly='"
						+ qReviewQuarter + "' AND t1.Country='India'";
				if (activityType.equals("Enetered")) {
					// System.out.println("Enetered case");
					activityType = "Entered";
					queryString = queryString + " AND STATUS = '" + activityType + "'  ";
				} else if (activityType.equals("Submitted")) {
					// System.out.println("Submitted case");
					activityType = "Submitted";
					queryString = queryString + " AND STATUS = '" + activityType + "' ";
				} else if (activityType.equals("ManagerApproved")) {
					// System.out.println("ManagerApproved case");
					activityType = "Approved";
					queryString = queryString + " AND STATUS = '" + activityType
							+ "' AND (ApprovedDate>=OpperationApprovedDate OR OpperationApprovedDate is null) ";
				} else if (activityType.equals("ManagerRejected")) {
					// System.out.println("ManagerRejected case");
					activityType = "Rejected";
					queryString = queryString + " AND STATUS = '" + activityType
							+ "'   AND (ApprovedDate>=OpperationApprovedDate OR OpperationApprovedDate is null) ";
				} else if (activityType.equals("Closed")) {
					// System.out.println("Closed case");
					activityType = "Closed";
					queryString = queryString + " AND OpperationTeamStatus = '" + activityType + "' ";
				} else if (activityType.equals("HrRejected")) {
					// System.out.println("HrRejected case");
					activityType = "Rejected";
					queryString = queryString + " AND  OpperationTeamStatus = '" + activityType
							+ "' AND OpperationApprovedDate>=ApprovedDate";
				}
				// System.out.println("queryString ====> is====>"+queryString);

				statement = connection.createStatement();
				resultSet = statement.executeQuery(queryString);
				while (resultSet.next()) {
					i++;
					if (resultSet.getString("EmpName") != null && !resultSet.getString("EmpName").equals("")) {
						EmpName = resultSet.getString("EmpName");
					}
					Date CreatedDate = resultSet.getDate("CreatedDate");
					if (resultSet.getString("STATUS") != null && !resultSet.getString("STATUS").equals("")) {
						STATUS = resultSet.getString("STATUS");
					}
					if (resultSet.getString("EmpCurrentStatus") != null
							&& !resultSet.getString("EmpCurrentStatus").equals("")) {
						EmpCurrentStatus = resultSet.getString("EmpCurrentStatus");
					}
					if (resultSet.getString("Practice") != null && !resultSet.getString("Practice").equals("")) {
						Practice = resultSet.getString("Practice");
					}
					if (resultSet.getString("Title") != null && !resultSet.getString("Title").equals("")) {
						Title = resultSet.getString("Title");
					}
					if (resultSet.getString("ReportsToName") != null
							&& !resultSet.getString("ReportsToName").equals("")) {
						ReportsToName = resultSet.getString("ReportsToName");
					}
					if (resultSet.getString("OpperationTeamStatus") != null
							&& !resultSet.getString("OpperationTeamStatus").equals("")) {
						OpperationTeamStatus = resultSet.getString("OpperationTeamStatus");
					}
					if (resultSet.getString("EmpId") != null && !resultSet.getString("EmpId").equals("")) {
						EmpId = resultSet.getString("EmpId");
					}
					response = response + i + "#^$" + EmpName + "#^$" + CreatedDate + "#^$" + STATUS + "#^$"
							+ EmpCurrentStatus + "#^$" + Practice + "#^$" + Title + "#^$" + ReportsToName + "#^$"
							+ OpperationTeamStatus + "#^$" + EmpId + "*@!";
				}
				// System.out.println("response====>"+response);

			} else if (activityType.equals("NotEntered")) {
				// System.out.println("NotEntered case");
				activityType = "NotEntered";
				queryString1 = "SELECT CONCAT(TRIM(tblEmployee.FName),'.',TRIM(tblEmployee.LName)) AS EmployeeName,Id,WorkPhoneNo,ReportsTo,DepartmentId,OpsContactId,Email1,PracticeId,SubPractice,Location FROM tblEmployee WHERE CurStatus='Active' AND departmentId IN('GDC') AND Country='India'"
						+ "AND tblEmployee.loginId NOT IN (SELECT loginId FROM tblQuarterlyAppraisals WHERE  YEAR(CreatedDate) ='"
						+ qReviewYear + "' AND Quarterly='" + qReviewQuarter + "')";
				// System.out.println("queryString1---->"+queryString1);
				statement = connection.createStatement();
				resultSet = statement.executeQuery(queryString1);
				while (resultSet.next()) {
					i++;
					if (resultSet.getString("EmployeeName") != null
							&& !resultSet.getString("EmployeeName").equals("")) {
						EmpName = resultSet.getString("EmployeeName");
					}

					if (resultSet.getString("WorkPhoneNo") != null && !resultSet.getString("WorkPhoneNo").equals("")) {
						WorkPhoneNo = resultSet.getString("WorkPhoneNo");
					}
					if (resultSet.getString("ReportsTo") != null && !resultSet.getString("ReportsTo").equals("")) {
						ReportsTo = resultSet.getString("ReportsTo");

					}
					if (resultSet.getString("DepartmentId") != null
							&& !resultSet.getString("DepartmentId").equals("")) {
						DepartmentId = resultSet.getString("DepartmentId");
					}
					if (resultSet.getString("Email1") != null && !resultSet.getString("Email1").equals("")) {
						Email1 = resultSet.getString("Email1");
					}
					if (resultSet.getString("PracticeId") != null && !resultSet.getString("PracticeId").equals("")) {
						PracticeId = resultSet.getString("PracticeId");
					}
					if (resultSet.getString("SubPractice") != null && !resultSet.getString("SubPractice").equals("")) {
						SubPractice = resultSet.getString("SubPractice");
					}

					if (resultSet.getString("Location") != null && !resultSet.getString("Location").equals("")) {
						Location = resultSet.getString("Location");
					}

					// if(resultSet.getString("OpsContactId") != null &&
					// !resultSet.getString("OpsContactId").equals(""))
					// {
					// OpsContactId = resultSet.getString("OpsContactId");
					// }
					if (resultSet.getString("Id") != null && !resultSet.getString("Id").equals("")) {
						Id = resultSet.getString("Id");
					}
					response = response + i + "#^$" + EmpName + "#^$" + Email1 + "#^$" + WorkPhoneNo + "#^$" + ReportsTo
							+ "#^$" + DepartmentId + "#^$" + PracticeId + "#^$" + SubPractice + "#^$" + Location + "#^$"
							+ Id + "*@!";
				}
				// System.out.println("response for not entered
				// is---->"+response);
			}

		} catch (Exception se) {
			se.printStackTrace();
		} finally {
			try {

				if (callableStatement != null) {
					callableStatement.close();
					callableStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		return response;
	}

	public String getQReviewListByStatusVsPractice(String qReviewYear, String qReviewQuarter, String activityType,
			String columnLabel, HttpServletRequest httpServletRequest) throws ServiceLocatorException {

		String response = "";
		// Connection connection = null;
		CallableStatement callableStatement = null;
		Connection connection = null;
		String teamMembersKeys = "";

		Map teamMembersMapByLoginid = null;
		Map teamMembersMapByReportsTo = null;
		int i = 0;
		String queryString = "";

		Statement statement = null;
		ResultSet resultSet = null;
		String EmpName = "-";
		String STATUS = "-";
		String WorkPhoneNo = "-";
		String EmpCurrentStatus = "-";
		String Practice = "-";
		String Title = "-";
		String ReportsToName = "-";
		String OpperationTeamStatus = "-";
		String EmpId = "-";
		String ReportsTo = "-";
		String DepartmentId = "-";
		String Id = "-";
		String OpsContactId = "-";
		String Email1 = "-";
		String PracticeId = "-";
		String SubPractice = "-";
		String Location = "-";
		try {
			connection = ConnectionProvider.getInstance().getConnection();

			// teamMembersMapByLoginid = (Map)
			// httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_MY_TEAM_MAP);
			String loginId = (String) httpServletRequest.getSession(false)
					.getAttribute(ApplicationConstants.SESSION_USER_ID);
			if (!columnLabel.equals("NotEntered")) {
				queryString = "SELECT tblQuarterlyAppraisals.LoginId,EmpId,DATE(tblQuarterlyAppraisals.CreatedDate) AS CreatedDate,STATUS,EmpCurrentStatus,Practice,Title,tblQuarterlyAppraisals.ReportsTo,OpperationTeamStatus,"
						+ " CONCAT(t1.FName,'',t1.MName,'.',t1.LName) AS EmpName,CONCAT(t2.FName,'',t2.MName,'.',t2.LName) AS ReportsToName"
						+ " FROM tblQuarterlyAppraisals JOIN tblEmployee t1 ON t1.LoginId=tblQuarterlyAppraisals.LoginId JOIN tblEmployee t2 ON t2.LoginId=tblQuarterlyAppraisals.ReportsTo WHERE 1=1 "
						+ " ANd YEAR(tblQuarterlyAppraisals.CreatedDate)>='" + qReviewYear + "' AND Quarterly='"
						+ qReviewQuarter + "' AND Practice LIKE '" + activityType + "' AND t1.Country='India'";
				if (columnLabel.equals("Enetered")) {
					// System.out.println("Enetered case");
					columnLabel = "Entered";
					queryString = queryString + " AND STATUS = '" + columnLabel + "'  ";
				} else if (columnLabel.equals("Submitted")) {
					// System.out.println("Submitted case");
					columnLabel = "Submitted";
					queryString = queryString + " AND STATUS = '" + columnLabel + "' ";
				} else if (columnLabel.equals("ManagerApproved")) {
					// System.out.println("ManagerApproved case");
					columnLabel = "Approved";
					queryString = queryString + " AND STATUS = '" + columnLabel
							+ "' AND (ApprovedDate>=OpperationApprovedDate OR OpperationApprovedDate is null) ";
				} else if (columnLabel.equals("ManagerRejected")) {
					// System.out.println("ManagerRejected case");
					columnLabel = "Rejected";
					queryString = queryString + " AND STATUS = '" + columnLabel
							+ "'   AND (ApprovedDate>=OpperationApprovedDate OR OpperationApprovedDate is null) ";
				} else if (columnLabel.equals("Closed")) {
					// System.out.println("Closed case");
					columnLabel = "Closed";
					queryString = queryString + " AND OpperationTeamStatus = '" + columnLabel + "' ";
				} else if (columnLabel.equals("HrRejected")) {
					// System.out.println("HrRejected case");
					columnLabel = "Rejected";
					queryString = queryString + " AND  OpperationTeamStatus = '" + columnLabel
							+ "' AND OpperationApprovedDate>=ApprovedDate";
				}
				// System.out.println("queryString ====> is====>"+queryString);

				statement = connection.createStatement();
				resultSet = statement.executeQuery(queryString);
				while (resultSet.next()) {
					i++;
					if (resultSet.getString("EmpName") != null && !resultSet.getString("EmpName").equals("")) {
						EmpName = resultSet.getString("EmpName");
					}
					Date CreatedDate = resultSet.getDate("CreatedDate");
					if (resultSet.getString("STATUS") != null && !resultSet.getString("STATUS").equals("")) {
						STATUS = resultSet.getString("STATUS");
					}
					if (resultSet.getString("EmpCurrentStatus") != null
							&& !resultSet.getString("EmpCurrentStatus").equals("")) {
						EmpCurrentStatus = resultSet.getString("EmpCurrentStatus");
					}
					if (resultSet.getString("Practice") != null && !resultSet.getString("Practice").equals("")) {
						Practice = resultSet.getString("Practice");
					}
					if (resultSet.getString("Title") != null && !resultSet.getString("Title").equals("")) {
						Title = resultSet.getString("Title");
					}
					if (resultSet.getString("ReportsToName") != null
							&& !resultSet.getString("ReportsToName").equals("")) {
						ReportsToName = resultSet.getString("ReportsToName");
					}
					if (resultSet.getString("OpperationTeamStatus") != null
							&& !resultSet.getString("OpperationTeamStatus").equals("")) {
						OpperationTeamStatus = resultSet.getString("OpperationTeamStatus");
					}
					if (resultSet.getString("EmpId") != null && !resultSet.getString("EmpId").equals("")) {
						EmpId = resultSet.getString("EmpId");
					}
					response = response + i + "#^$" + EmpName + "#^$" + CreatedDate + "#^$" + STATUS + "#^$"
							+ EmpCurrentStatus + "#^$" + Practice + "#^$" + Title + "#^$" + ReportsToName + "#^$"
							+ OpperationTeamStatus + "#^$" + EmpId + "*@!";
				}
			} else if (columnLabel.equals("NotEntered")) {
				queryString = "SELECT CONCAT(TRIM(tblEmployee.FName),'.',TRIM(tblEmployee.LName)) AS EmployeeName,Id,WorkPhoneNo,ReportsTo,DepartmentId,"
						+ "OpsContactId,Email1,PracticeId,SubPractice,Location FROM tblEmployee WHERE CurStatus='Active' AND departmentId IN('GDC') AND Country='India' AND PracticeId='"
						+ activityType + "'"
						+ " AND tblEmployee.loginId NOT IN (SELECT loginId FROM tblQuarterlyAppraisals WHERE  YEAR(CreatedDate) ='"
						+ qReviewYear + "' AND Quarterly='" + qReviewQuarter + "')";
				// System.out.println("queryString1---->" + queryString +
				// "---NotEntered");
				statement = connection.createStatement();
				resultSet = statement.executeQuery(queryString);
				while (resultSet.next()) {
					i++;
					if (resultSet.getString("EmployeeName") != null
							&& !resultSet.getString("EmployeeName").equals("")) {
						EmpName = resultSet.getString("EmployeeName");
					}

					if (resultSet.getString("Location") != null && !resultSet.getString("Location").equals("")) {
						Location = resultSet.getString("Location");
					}

					if (resultSet.getString("WorkPhoneNo") != null && !resultSet.getString("WorkPhoneNo").equals("")) {
						WorkPhoneNo = resultSet.getString("WorkPhoneNo");
					}
					if (resultSet.getString("ReportsTo") != null && !resultSet.getString("ReportsTo").equals("")) {
						ReportsTo = resultSet.getString("ReportsTo");

					}
					if (resultSet.getString("DepartmentId") != null
							&& !resultSet.getString("DepartmentId").equals("")) {
						DepartmentId = resultSet.getString("DepartmentId");
					}
					if (resultSet.getString("Email1") != null && !resultSet.getString("Email1").equals("")) {
						Email1 = resultSet.getString("Email1");
					}
					if (resultSet.getString("PracticeId") != null && !resultSet.getString("PracticeId").equals("")) {
						PracticeId = resultSet.getString("PracticeId");
					}
					if (resultSet.getString("SubPractice") != null && !resultSet.getString("SubPractice").equals("")) {
						SubPractice = resultSet.getString("SubPractice");
					}

					// if(resultSet.getString("OpsContactId") != null &&
					// !resultSet.getString("OpsContactId").equals(""))
					// {
					// OpsContactId = resultSet.getString("OpsContactId");
					// }
					if (resultSet.getString("Id") != null && !resultSet.getString("Id").equals("")) {
						Id = resultSet.getString("Id");
					}
					response = response + i + "#^$" + EmpName + "#^$" + Email1 + "#^$" + WorkPhoneNo + "#^$" + ReportsTo
							+ "#^$" + DepartmentId + "#^$" + PracticeId + "#^$" + SubPractice + "#^$" + Location + "#^$"
							+ Id + "*@!";
				}
			}
			// System.out.println("response====>"+response);
		} catch (Exception se) {
			se.printStackTrace();
		} finally {
			try {

				if (callableStatement != null) {
					callableStatement.close();
					callableStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		return response;
	}

	public String getQReviewListByStatusVsQuarter(String qReviewYear, String qReviewQuarter, String activityType,
			String columnLabel, HttpServletRequest httpServletRequest) throws ServiceLocatorException {

		String response = "";
		// Connection connection = null;
		CallableStatement callableStatement = null;
		Connection connection = null;
		String teamMembersKeys = "";

		Map teamMembersMapByLoginid = null;
		Map teamMembersMapByReportsTo = null;
		int i = 0;
		String queryString = "";

		Statement statement = null;
		ResultSet resultSet = null;
		String EmpName = "-";
		String STATUS = "-";
		String EmpCurrentStatus = "-";
		String Practice = "-";
		String Title = "-";
		String ReportsToName = "-";
		String OpperationTeamStatus = "-";
		String EmpId = "-";
		try {
			connection = ConnectionProvider.getInstance().getConnection();

			// teamMembersMapByLoginid = (Map)
			// httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_MY_TEAM_MAP);
			String loginId = (String) httpServletRequest.getSession(false)
					.getAttribute(ApplicationConstants.SESSION_USER_ID);
			// query="SELECT t1.Id,CONCAT(TRIM(t1.FName),'.',TRIM(t1.LName)) AS
			// EmployeeName,t1.Email1 AS EmailId,t1.WorkPhoneNo AS
			// PhoneNo,CONCAT(TRIM(t2.FName),'.',TRIM(t2.LName)) AS ReportsTo"
			// + " FROM tblEmployee t1, tblEmployee t2 WHERE t1.ReportsTo =
			// t2.LoginId AND t1.CurStatus='Active' AND t1.TitleTypeId='BDM' ";
			// callableStatement = connection.prepareCall("{call
			// spQReviewReportByStatus(?,?,?,?)}");
			// System.out.println("activityType---->"+activityType);
			// System.out.println("columnLabel---->"+columnLabel);
			queryString = "SELECT tblQuarterlyAppraisals.LoginId,EmpId,DATE(tblQuarterlyAppraisals.CreatedDate) AS CreatedDate,STATUS,EmpCurrentStatus,Practice,Title,tblQuarterlyAppraisals.ReportsTo,OpperationTeamStatus,"
					+ " CONCAT(t1.FName,'',t1.MName,'.',t1.LName) AS EmpName,CONCAT(t2.FName,'',t2.MName,'.',t2.LName) AS ReportsToName"
					+ " FROM tblQuarterlyAppraisals JOIN tblEmployee t1 ON t1.LoginId=tblQuarterlyAppraisals.LoginId JOIN tblEmployee t2 ON t2.LoginId=tblQuarterlyAppraisals.ReportsTo WHERE 1=1 "
					+ " ANd YEAR(tblQuarterlyAppraisals.CreatedDate)>='" + qReviewYear + "' AND Quarterly='"
					+ qReviewQuarter + "' AND Quarterly LIKE '" + activityType + "' AND t2.Country='India'";

			if (columnLabel.equals("HrClosed")) {
				// System.out.println("Closed case");
				columnLabel = "Closed";
				queryString = queryString + " AND OpperationTeamStatus = '" + columnLabel + "' ";
			} else if (columnLabel.equals("HrRejected")) {
				// System.out.println("HrRejected case");
				columnLabel = "Rejected";
				queryString = queryString + " AND  OpperationTeamStatus = '" + columnLabel
						+ "' AND OpperationApprovedDate>=ApprovedDate";
			}
			// System.out.println("queryString ====> is====>"+queryString);

			statement = connection.createStatement();
			resultSet = statement.executeQuery(queryString);
			while (resultSet.next()) {
				i++;
				if (resultSet.getString("EmpName") != null && !resultSet.getString("EmpName").equals("")) {
					EmpName = resultSet.getString("EmpName");
				}
				Date CreatedDate = resultSet.getDate("CreatedDate");
				if (resultSet.getString("STATUS") != null && !resultSet.getString("STATUS").equals("")) {
					STATUS = resultSet.getString("STATUS");
				}
				if (resultSet.getString("EmpCurrentStatus") != null
						&& !resultSet.getString("EmpCurrentStatus").equals("")) {
					EmpCurrentStatus = resultSet.getString("EmpCurrentStatus");
				}
				if (resultSet.getString("Practice") != null && !resultSet.getString("Practice").equals("")) {
					Practice = resultSet.getString("Practice");
				}
				if (resultSet.getString("Title") != null && !resultSet.getString("Title").equals("")) {
					Title = resultSet.getString("Title");
				}
				if (resultSet.getString("ReportsToName") != null && !resultSet.getString("ReportsToName").equals("")) {
					ReportsToName = resultSet.getString("ReportsToName");
				}
				if (resultSet.getString("OpperationTeamStatus") != null
						&& !resultSet.getString("OpperationTeamStatus").equals("")) {
					OpperationTeamStatus = resultSet.getString("OpperationTeamStatus");
				}
				if (resultSet.getString("EmpId") != null && !resultSet.getString("EmpId").equals("")) {
					EmpId = resultSet.getString("EmpId");
				}
				response = response + i + "#^$" + EmpName + "#^$" + CreatedDate + "#^$" + STATUS + "#^$"
						+ EmpCurrentStatus + "#^$" + Practice + "#^$" + Title + "#^$" + ReportsToName + "#^$"
						+ OpperationTeamStatus + "#^$" + EmpId + "*@!";
			}
			// System.out.println("response====>"+response);
		} catch (Exception se) {
			se.printStackTrace();
		} finally {
			try {

				if (callableStatement != null) {
					callableStatement.close();
					callableStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		return response;
	}

	public String getQReviewListByManagers(NewAjaxHandlerAction ajaxHandlerAction,
			HttpServletRequest httpServletRequest) throws ServiceLocatorException {
		String response = "";
		// Connection connection = null;
		CallableStatement callableStatement = null;
		Connection connection = null;
		String teamMembersKeys = "";

		Map teamMembersMapByLoginid = null;
		Map teamMembersMapByReportsTo = null;
		JSONObject jObject = null;

		try {
			connection = ConnectionProvider.getInstance().getConnection();

			// teamMembersMapByLoginid = (Map)
			// httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_MY_TEAM_MAP);
			String loginId = (String) httpServletRequest.getSession(false)
					.getAttribute(ApplicationConstants.SESSION_USER_ID);
			jObject = new JSONObject(ajaxHandlerAction.getqReviewMnagersDetailsJson());
			String yearForManagersReport = jObject.getString("yearForManagersReport");
			String quarterlyForManagersReport = jObject.getString("quarterlyForManagersReport");
			String quarterlyLivingCountry = jObject.getString("quarterlyLivingCountry");
			Boolean isIncludeTeam = jObject.getBoolean("isIncludeTeam");
			int opsContactId = jObject.getInt("opsContactId");
			String ReportsToField = jObject.getString("ReportsToField");
			String practiceName = jObject.getString("practiceName");

			callableStatement = connection.prepareCall("{call spQReviewResourcesVsManager(?,?,?,?,?,?,?,?)}");
			callableStatement.setString(1, yearForManagersReport);
			callableStatement.setString(2, quarterlyForManagersReport);
			callableStatement.setString(3, quarterlyLivingCountry);
			callableStatement.setBoolean(4, isIncludeTeam);
			callableStatement.setInt(5, opsContactId);
			callableStatement.setString(6, ReportsToField);
			callableStatement.setString(7, practiceName);
			callableStatement.registerOutParameter(8, Types.VARCHAR);
			callableStatement.execute();

			response = callableStatement.getString(8);

			// System.out.println("response====>"+response);

		} catch (SQLException se) {
			se.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {

				if (callableStatement != null) {
					callableStatement.close();
					callableStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		return response;
	}

	public String getQReviewPendingListByManagers(NewAjaxHandlerAction ajaxHandlerAction,
			HttpServletRequest httpServletRequest, String empId) throws ServiceLocatorException {
		// System.out.println("Heloooo"+empId);
		String response = "";
		// Connection connection = null;
		CallableStatement callableStatement = null;
		Connection connection = null;
		String teamMembersKeys = "";

		Map teamMembersMapByLoginid = null;
		Map teamMembersMapByReportsTo = null;
		JSONObject jObject = null;

		try {
			connection = ConnectionProvider.getInstance().getConnection();

			// teamMembersMapByLoginid = (Map)
			// httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_MY_TEAM_MAP);
			String loginId = (String) httpServletRequest.getSession(false)
					.getAttribute(ApplicationConstants.SESSION_USER_ID);
			jObject = new JSONObject(ajaxHandlerAction.getqReviewMnagersDetailsJson());
			String yearForManagersReport = jObject.getString("yearForManagersReport");
			String quarterlyForManagersReport = jObject.getString("quarterlyForManagersReport");
			String quarterlyLivingCountry = jObject.getString("quarterlyLivingCountry");
			Boolean isIncludeTeam = jObject.getBoolean("isIncludeTeam");

			String ReportsToField = jObject.getString("ReportsToField");
			String practiceName = jObject.getString("practiceName");

			callableStatement = connection.prepareCall("{call spQReviewResourcesVsManager(?,?,?,?,?,?,?,?)}");
			callableStatement.setString(1, yearForManagersReport);
			callableStatement.setString(2, quarterlyForManagersReport);
			callableStatement.setString(3, quarterlyLivingCountry);
			callableStatement.setBoolean(4, isIncludeTeam);
			callableStatement.setString(5, empId);
			callableStatement.setString(6, ReportsToField);
			callableStatement.setString(7, practiceName);
			callableStatement.registerOutParameter(8, Types.VARCHAR);
			callableStatement.execute();

			response = callableStatement.getString(8);

			// System.out.println("response====>"+response);

		} catch (SQLException se) {
			se.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {

				if (callableStatement != null) {
					callableStatement.close();
					callableStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		return response;
	}

	public String avaiableEmployeBySubPracticeList(String practiceName) throws ServiceLocatorException {
		Connection connection = null;
		CallableStatement callableStatement = null;
		String resutMessage = "";

		String queryString = "{call spResourceCountBySubPractice (?,?)}";

		try {
			connection = ConnectionProvider.getInstance().getConnection();

			callableStatement = connection.prepareCall(queryString);

			if (!"-1".equals(practiceName)) {
				// System.out.println("projectName"+projectName);
				callableStatement.setString(1, practiceName);
			} else {
				callableStatement.setString(1, "%");
			}
			callableStatement.registerOutParameter(2, Types.VARCHAR);
			callableStatement.executeUpdate();
			resutMessage = callableStatement.getString(2);

		} catch (SQLException se) {
			se.printStackTrace();

			throw new ServiceLocatorException(se);
		} finally {
			try {

				if (callableStatement != null) {
					callableStatement.close();
					callableStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException se) {
				se.printStackTrace();
				throw new ServiceLocatorException(se);
			}
		}
		return resutMessage;
	}

	public String getEmployeesForMngrByApprasailStatus(String year, String quarter, String loginId, String status,
			String country, String isIncludeTeam, HttpServletRequest httpServletRequest)
			throws ServiceLocatorException {
		String response = "";
		// Connection connection = null;
		CallableStatement callableStatement = null;
		Connection connection = null;

		try {
			connection = ConnectionProvider.getInstance().getConnection();

			// teamMembersMapByLoginid = (Map)
			// httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_MY_TEAM_MAP);
			callableStatement = connection.prepareCall("{call spQReviewManagerTeamDetails(?,?,?,?,?,?,?)}");

			callableStatement.setString(1, year);
			callableStatement.setString(2, quarter);
			callableStatement.setString(3, loginId);
			callableStatement.setString(4, status);
			callableStatement.setString(5, country);
			callableStatement.setString(6, isIncludeTeam);
			callableStatement.registerOutParameter(7, Types.VARCHAR);
			callableStatement.execute();

			response = callableStatement.getString(7);

		} catch (SQLException se) {
			se.printStackTrace();
		} finally {
			try {

				if (callableStatement != null) {
					callableStatement.close();
					callableStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		return response;
	}

	public String getOffshoreEmployeeDetails(String practice, String subPractice, String status)
			throws ServiceLocatorException {
		String response = "";
		CallableStatement callableStatement = null;
		Connection connection = null;
		try {
			connection = ConnectionProvider.getInstance().getConnection();

			callableStatement = connection.prepareCall("{call spOffshoreAvailableEmployeesBySubPractice(?,?,?,?)}");

			callableStatement.setString(1, practice);
			callableStatement.setString(2, subPractice);
			callableStatement.setString(3, status);
			callableStatement.registerOutParameter(4, Types.VARCHAR);
			callableStatement.execute();

			response = callableStatement.getString(4);

		} catch (SQLException se) {
			se.printStackTrace();
		} finally {
			try {

				if (callableStatement != null) {
					callableStatement.close();
					callableStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		return response;
	}

	/* Star Performers related methods start */

	public String starPerformers(NewAjaxHandlerAction ajaxHandlerAction) throws ServiceLocatorException {
		StringBuffer stringBuffer = new StringBuffer();
		Statement statement = null;
		ResultSet resultSet = null;
		Connection connection = null;
		String totalStream = "";
		int i = 0;
		try {
			String queryString = "SELECT DATE_FORMAT(DATE, '%b') AS Month,YEAR(DATE) AS Year,Id,Status,MONTHNAME(DATE) As MonthName,CampaignScheduledDate,ScheduledId,CampaignId,CreatedBy,SurveyFormId,StarPerformerCount,DATE_FORMAT(DATE,'%m') AS mnth FROM tblStarPerformers WHERE 1=1";

			if (ajaxHandlerAction.getMonth() != null && !"".equals(ajaxHandlerAction.getMonth())) {
				queryString = queryString + " AND MONTH(DATE) = '" + ajaxHandlerAction.getMonth() + "'";
			}
			if (ajaxHandlerAction.getYear() != null && !"".equals(ajaxHandlerAction.getYear())) {
				queryString = queryString + " AND  YEAR(DATE) = '" + ajaxHandlerAction.getYear() + "'";
			}
			queryString = queryString + " order by CreatedDate";
			// System.out.println(queryString);
			connection = ConnectionProvider.getInstance().getConnection();
			statement = connection.createStatement();
			resultSet = statement.executeQuery(queryString);

			while (resultSet.next()) {
				int Id = resultSet.getInt("Id");
				String Month = resultSet.getString("Month");
				String Year = resultSet.getString("Year");
				String Status = DataSourceDataProvider.getInstance().getStarPerformerStatus()
						.get(resultSet.getString("Status")).toString();
				String MonthName = resultSet.getString("MonthName");
				String CampaignId = resultSet.getString("CampaignId");
				String url = Properties.getProperty("Images.NewsLetters.URL");
				String scheduledDate = "";
				String CreatedBy = resultSet.getString("CreatedBy");
				String SurveyFormId = resultSet.getString("SurveyFormId");
				scheduledDate = "not scheduled";
				int countId = resultSet.getInt("StarPerformerCount");
				String mnth = resultSet.getString("mnth");
				int StatusRepresentation = resultSet.getInt("Status");
				i++;

				totalStream = totalStream + i + "#^$" + Month + "#^$" + Year + "#^$" + Status + "#^$" + Id + "#^$"
						+ MonthName + "#^$" + url.trim() + "#^$" + scheduledDate + "#^$" + CreatedBy + "#^$"
						+ SurveyFormId + "#^$" + countId + "#^$" + mnth + "#^$" + StatusRepresentation + "*@!";
				// System.out.println(totalStream);
			}
			stringBuffer.append(totalStream);
			if (i > 0) {
				stringBuffer.append("addto");
				stringBuffer.append(i);
			}

		} catch (Exception sqe) {
			sqe.printStackTrace();
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (statement != null) {
					statement.close();
					statement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException sqle) {
			}
		}
		return stringBuffer.toString();
	}

	public String doUpdateStarPerformer(String objectId, String Status) throws ServiceLocatorException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;

		String str = "";

		String queryString = "update tblStarPerformers set Status=? where Id=?";
		try {
			connection = ConnectionProvider.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(queryString);

			preparedStatement.setString(1, Status);
			preparedStatement.setString(2, objectId);
			boolean isUpdated = preparedStatement.execute();

			if (!isUpdated) {
				str = "<font size='2' color='green'>Successfully Updated</font>";
			} else {
				str = "<font size='2' color='red'>Please try again later</font>";
			}
		} catch (SQLException se) {
			se.printStackTrace();
			throw new ServiceLocatorException(se);
		} finally {
			try {
				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException se) {
				throw new ServiceLocatorException(se);
			}
		}

		return str;

	}

	public boolean checkStarDetails(NewAjaxHandlerAction ajaxHandlerAction) throws ServiceLocatorException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		String responseString = "";
		int year = 0;
		int month = 0;
		boolean isInserted = true;
		String queryString = "select YEAR(DATE) as Year,MONTH(DATE) as Month from tblStarPerformers";

		try {
			connection = ConnectionProvider.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(queryString);

			resultSet = preparedStatement.executeQuery();

			int count = 0;
			while (resultSet.next()) {

				year = resultSet.getInt("Year");
				// System.out.println("year in while loop is---->"+year);
				month = resultSet.getInt("Month");
				if (year == ajaxHandlerAction.getOverlayYear() && month == ajaxHandlerAction.getOverlayMonth()) {
					responseString = "sorry! the star performers for particular specified period has been recorded already";
					isInserted = false;
					break;
				}

			}
			preparedStatement.execute();
		} catch (SQLException se) {
			se.printStackTrace();
			throw new ServiceLocatorException(se);
		} finally {
			try {
				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException se) {
				throw new ServiceLocatorException(se);
			}
		}

		return isInserted;

	}

	public int addStarDetails(NewAjaxHandlerAction ajaxHandlerAction) throws ServiceLocatorException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;

		int flag = 0;
		int isInserted = 0;
		// INSERT INTO `mirage`.`tblStarPerformers`
		// (`Date`,`Status`,`CreatedDate`,`CreatedBy`, `ModifiedBy`,
		// `ModifiedDate`) VALUES(?,?,?,?,?,?)
		String queryString = "insert into tblStarPerformers(Date,Status,CreatedDate,CreatedBy,ModifiedBy,ModifiedDate) values(?,?,?,?,?,?)";
		try {
			connection = ConnectionProvider.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(queryString);
			preparedStatement = connection.prepareStatement(queryString, Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setString(1, ajaxHandlerAction.getOverlayDate());
			preparedStatement.setString(2, "1");
			preparedStatement.setTimestamp(3, ajaxHandlerAction.getCreatedDate());
			preparedStatement.setString(4, ajaxHandlerAction.getModifiedBy());
			preparedStatement.setString(5, ajaxHandlerAction.getModifiedBy());
			preparedStatement.setTimestamp(6, ajaxHandlerAction.getModifiedDate());

			preparedStatement.execute();

			ResultSet rs = preparedStatement.getGeneratedKeys();
			// int generatedKey = 0;
			if (rs.next()) {
				isInserted = rs.getInt(1);
			}

			// isInserted = preparedStatement.executeUpdate();

			if (isInserted > 0) {

				try {
					flag = ConstantContactMailManager.doSendStarPerformerNomination(isInserted,
							ajaxHandlerAction.getMonth());
				} catch (ConstantContactComponentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		} catch (SQLException se) {
			se.printStackTrace();
			throw new ServiceLocatorException(se);
		} finally {
			try {
				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException se) {
				throw new ServiceLocatorException(se);
			}
		}

		return isInserted;

	}

	public String dogetNomineesList(String ObjectId, String statusFlag, String accessFlag, String loginId,
			Map teamMap) {
		StringBuffer stringBuffer = new StringBuffer();

		Statement statement = null;
		ResultSet resultSet = null;
		Connection connection = null;
		Map teammembers = new TreeMap();

		String totalStream = "";
		int i = 0;
		try {
			String queryString = "";

			if ("true".equalsIgnoreCase(statusFlag.trim()) || "true".equalsIgnoreCase(accessFlag.trim())) {
				queryString = "SELECT MONTHNAME(DATE) As MonthName,YEAR(DATE) AS fullYear,DATE_FORMAT(DATE, '%b') AS shortMonth,tblStarPerformerLines.Id,tblStarPerformerLines.ObjectId,tblStarPerformerLines.EmpId,tblStarPerformerLines.EmpName AS EmployeeName,tblStarPerformerLines.Title AS Title,tblStarPerformerLines.CreatedBy,tblStarPerformerLines.FilePath As FilePath,tblStarPerformerLines.Department As Department,tblStarPerformerLines.EmpId,tblStarPerformerLines.status,tblStarPerformerLines.Comments AS Comments FROM tblStarPerformerLines LEFT OUTER JOIN tblStarPerformers ON(tblStarPerformers.Id=tblStarPerformerLines.ObjectId)  WHERE tblStarPerformerLines.ObjectId='"
						+ ObjectId + "'";

			} else {

				// teammembers =
				// DataSourceDataProvider.getInstance().getHrTeamList(loginId);
				String TeamLoginIdList = "";
				if (teamMap.size() > 0) {
					TeamLoginIdList = DataSourceDataProvider.getInstance().getTeamLoginIdList(teamMap);
					TeamLoginIdList = TeamLoginIdList + ",'" + loginId + "'";
				} else {
					TeamLoginIdList = "'" + loginId + "'";
				}

				queryString = "SELECT MONTHNAME(DATE) As MonthName,YEAR(DATE) AS fullYear,DATE_FORMAT(DATE, '%b') AS shortMonth,tblStarPerformerLines.Id,tblStarPerformerLines.ObjectId,tblStarPerformerLines.EmpId,tblStarPerformerLines.EmpName AS EmployeeName,tblStarPerformerLines.Title AS Title,tblStarPerformerLines.CreatedBy,tblStarPerformerLines.FilePath As FilePath,tblStarPerformerLines.Department As Department,tblStarPerformerLines.EmpId,tblStarPerformerLines.status,tblStarPerformerLines.Comments AS Comments FROM tblStarPerformerLines LEFT OUTER JOIN tblStarPerformers ON(tblStarPerformers.Id=tblStarPerformerLines.ObjectId)   WHERE tblStarPerformerLines.ObjectId='"
						+ ObjectId + "' And tblStarPerformerLines.CreatedBy In(" + TeamLoginIdList + ") ";
			}

			// queryString = "SELECT YEAR(DATE) AS fullYear,DATE_FORMAT(DATE,
			// '%b') AS
			// shortMonth,tblStarPerformerLines.Id,tblStarPerformerLines.ObjectId,tblStarPerformerLines.EmpId,tblStarPerformerLines.EmpName
			// AS EmployeeName,tblStarPerformerLines.Title AS
			// Title,tblStarPerformerLines.CreatedBy,tblStarPerformerLines.FilePath
			// As FilePath,tblStarPerformerLines.Department As
			// Department,tblStarPerformerLines.EmpId,tblStarPerformerLines.status
			// FROM tblStarPerformerLines LEFT OUTER JOIN tblStarPerformers
			// ON(tblStarPerformers.Id=tblStarPerformerLines.ObjectId) WHERE
			// tblStarPerformerLines.ObjectId='"+ObjectId+"'" ;
			// queryString = queryString+" order by CreatedDate";
			// System.out.println("--"+queryString);
			connection = ConnectionProvider.getInstance().getConnection();
			statement = connection.createStatement();
			resultSet = statement.executeQuery(queryString);

			while (resultSet.next()) {
				int Id = resultSet.getInt("Id");
				String Month = resultSet.getString("MonthName");
				String Year = resultSet.getString("fullYear");
				String EmployeeName = resultSet.getString("EmployeeName");
				String Title = resultSet.getString("Title");
				String Department = resultSet.getString("Department");
				String status = resultSet.getString("status");
				String CreatedBy = resultSet.getString("CreatedBy");
				String Comments = resultSet.getString("Comments");

				i++;

				totalStream = totalStream + i + "#^$" + Month + "#^$" + Year + "#^$" + EmployeeName + "#^$" + Id + "#^$"
						+ Title + "#^$" + Department + "#^$" + status + "#^$" + CreatedBy + "#^$" + Comments + "*@!";
				// System.out.println("--totalStream......................................................."+totalStream);
			}
			stringBuffer.append(totalStream);
			if (i > 0) {
				stringBuffer.append("addto");
				stringBuffer.append(i);
			}

		} catch (Exception sqe) {
			sqe.printStackTrace();
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (statement != null) {
					statement.close();
					statement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException sqle) {
			}
		}
		// System.err.println("response string is"+stringBuffer.toString());
		return stringBuffer.toString();
	}

	public String doSubmitForApproval(NewAjaxHandlerAction ajaxHandlerAction) throws ServiceLocatorException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		Statement statement = null;
		int Status = 0;
		String responce = "";
		String month = "";

		String sendMailId = null;
		String queryString = "SELECT Status,MONTHNAME(STR_TO_DATE(MONTH(DATE), '%m')) AS mnth From  tblStarPerformers where Id=?";
		// System.out.println("queryString----->"+queryString);
		try {
			connection = ConnectionProvider.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(queryString);
			preparedStatement.setInt(1, ajaxHandlerAction.getObjId());
			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {

				month = resultSet.getString("mnth");
				Status = resultSet.getInt("Status");

			}
			if (Status == 2) {

				sendMailId = Properties.getProperty("STARPERFORMER.EMAILS.STATUS");

				MailManager.sendStarPerformerListDetails(sendMailId, ajaxHandlerAction.getObjId(),
						ajaxHandlerAction.getMail(), month);

				String query1 = "Update tblStarPerformers Set Status='3' WHERE Id=" + ajaxHandlerAction.getObjId() + "";
				statement = connection.createStatement();
				statement.executeUpdate(query1);

				responce = "<font size='2' color='green'>Star Performer Nominees has been Submitted And Waiting for Approval </font>";

			} else if (Status == 3) {
				responce = "<font size='2' color='red'>Star Performer Nominees had already Submitted And Waiting for Approval </font>";

			} else if (Status > 3) {
				responce = "<font size='2' color='red'>Star Performer Nominees had already got  Approved </font>";

			} else {
				responce = "<font size='2' color='red'>Star Performer Nominees should be added </font>";

			}

		} catch (SQLException se) {
			se.printStackTrace();
			throw new ServiceLocatorException(se);
		} finally {
			try {
				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException se) {
				throw new ServiceLocatorException(se);
			}
		}

		return responce;

	}

	public String getEmployeeDetailStarList(String resourceName, String loginId, Map teamMap)
			throws ServiceLocatorException {
		boolean isGetting = false;
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		StringBuffer sb = new StringBuffer();
		String query = null;
		Map teammembers = new TreeMap();
		try {

			// teammembers =
			// DataSourceDataProvider.getInstance().getHrTeamMember(loginId);
			String TeamLoginIdList = "";
			if (teamMap.size() > 0) {
				TeamLoginIdList = DataSourceDataProvider.getInstance().getTeamLoginIdList(teamMap);
				TeamLoginIdList = TeamLoginIdList + ",'" + loginId + "'";
			} else {
				TeamLoginIdList = "'" + loginId + "'";
			}

			query = "SELECT CONCAT(TRIM(FName),'.',TRIM(LName)) AS FullName,Id FROM tblEmployee "
					+ "WHERE (LName LIKE '" + resourceName + "%' OR FName LIKE '" + resourceName + "%') "
					+ "AND CurStatus='Active' And OpsContactId IN( SELECT Id FROM tblEmployee WHERE LoginId IN("
					+ TeamLoginIdList + ")) ";

			connection = ConnectionProvider.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(query);
			resultSet = preparedStatement.executeQuery();

			int count = 0;
			sb.append("<xml version=\"1.0\">");
			sb.append("<EMPLOYEES>");
			while (resultSet.next()) {
				sb.append("<EMPLOYEE><VALID>true</VALID>");

				if (resultSet.getString(1) == null || resultSet.getString(1).equals("")) {
					sb.append("<NAME>NoRecord</NAME>");
				} else {
					String title = resultSet.getString(1);
					if (title.contains("&")) {
						title = title.replace("&", "&amp;");
					}
					sb.append("<NAME>" + title + "</NAME>");
				}
				// sb.append("<NAME>" +resultSet.getString(1) + "</NAME>");
				sb.append("<EMPID>" + resultSet.getInt(2) + "</EMPID>");
				sb.append("</EMPLOYEE>");
				isGetting = true;
				count++;
			}

			if (!isGetting) {
				// sb.append("<EMPLOYEES>" + sb.toString() + "</EMPLOYEES>");
				// } else {
				isGetting = false;
				// nothing to show
				// response.setStatus(HttpServletResponse.SC_NO_CONTENT);
				sb.append("<EMPLOYEE><VALID>false</VALID></EMPLOYEE>");
			}
			sb.append("</EMPLOYEES>");
			sb.append("</xml>");

			// System.out.println(sb.toString());
		} catch (SQLException sqle) {
			throw new ServiceLocatorException(sqle);
		} finally {
			try {
				if (resultSet != null) {

					resultSet.close();
					resultSet = null;
				}
				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}

				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException sql) {
				// System.err.print("Error :"+sql);
			}

		}
		// System.out.println("the string "+sb.toString());
		return sb.toString();
	}

	public int doSMCampaignIntegration(String objectId, String campaignStartDate, String month, String year)
			throws ServiceLocatorException {
		int rows = 0;
		PreparedStatement preparedStatement = null;
		Statement statement1 = null;
		Statement statement2 = null;
		ResultSet resultSet = null;
		Connection connection = null;
		try {

			String contactId = "";

			ContactListService cls = new ContactListService(Properties.getProperty("API_KEY"),
					Properties.getProperty("ACCESS_TOCKEN"));

			Set set = DataSourceDataProvider.getInstance().getEmployeeLocationsLst().keySet();
			List cclocation = new ArrayList(set);

			List<SentToContactList> scl = new ArrayList();

			List<ContactList> clst = cls.getLists(null);

			for (int j = 0; j < clst.size(); j++) {

				for (int i = 0; i < cclocation.size(); i++) {

					if (clst.get(j).getName().equals(cclocation.get(i).toString())) {

						SentToContactList stcl = new SentToContactList();

						stcl.setId(clst.get(j).getId());
						scl.add(stcl);

					}

				}

				if ("".equals(contactId)) {
					if (clst.get(j).getName().equals("SP_Campaign_List_For_" + month + "_" + year)) {
						contactId = clst.get(j).getId();
					}
				}
			}

			if (!"".equals(contactId.toString().trim())) {
				cls.deleteList(contactId);
				contactId = "";
			}
			if ("".equals(contactId.toString().trim())) {
				ContactList contList = new ContactList();
				contList.setName("SP_Campaign_List_For_" + month + "_" + year);
				contList.setStatus("ACTIVE");
				contList = cls.addList(contList);
				contactId = contList.getId();

				SentToContactList stcl = new SentToContactList();
				stcl.setId(contactId);
				scl.add(stcl);
			}

			///////////////////////////////////////

			///////////////////////////////////

			ConstantContactFactory cf = new ConstantContactFactory(Properties.getProperty("API_KEY"),
					Properties.getProperty("ACCESS_TOCKEN"));
			IBulkActivitiesService _activityService = cf.createBulkActivitiesService();

			AddContactsRequest contactsToAdd = new AddContactsRequest();
			List<String> contactLists = new ArrayList<String>();
			List<String> columns = new ArrayList<String>();
			List<ContactData> importData = new ArrayList<ContactData>();

			contactLists.add(contactId); // NOTE: Replace this with the IF for
											// the list you wish to use
			columns.add("EMAIL ADDRESS");
			columns.add("FIRST NAME");
			columns.add("LAST NAME");
			columns.add("Company Name");
			contactsToAdd.setColumnNames(columns);
			contactsToAdd.setLists(contactLists);

			connection = ConnectionProvider.getInstance().getConnection();

			String query = "SELECT (YEAR(DATE)-2000) AS yr,YEAR(DATE) AS fullYear,MONTHNAME(STR_TO_DATE(MONTH(DATE), '%m')) AS mnth,DATE_FORMAT(DATE, '%b') AS shortMonth,tblStarPerformerLines.Id,tblStarPerformers.Id As spId,tblStarPerformerLines.ObjectId,tblStarPerformerLines.EmpId,tblStarPerformerLines.EmpName AS EmployeeName,tblStarPerformerLines.Title AS Title,tblStarPerformerLines.CreatedBy,tblStarPerformerLines.Comments,DATE,tblStarPerformerLines.FilePath As FilePath,tblStarPerformerLines.Department As Department,tblStarPerformerLines.EmpId,tblEmployee.Email1,tblEmployee.FName,tblEmployee.LName,tblEmployee.OrgId FROM tblStarPerformerLines LEFT OUTER JOIN tblStarPerformers ON(tblStarPerformers.Id=tblStarPerformerLines.ObjectId) left join tblEmployee ON(tblStarPerformerLines.EmpId=tblEmployee.Id)  WHERE tblStarPerformerLines.Status='Approve' And tblStarPerformerLines.ObjectId="
					+ objectId + "";

			preparedStatement = connection.prepareStatement(query);

			resultSet = preparedStatement.executeQuery();

			String fullYear = "";
			String shortMonth = "";
			String htmlTxt = "";
			String Title = null;
			String FilePath = null;
			String Department = null;
			String EmployeeName = null;
			int i = 0;
			int td = 1;
			while (resultSet.next()) {

				if ("".equals(fullYear)) {
					fullYear = resultSet.getString("fullYear");
				}
				if ("".equals(shortMonth)) {
					// mnth
					shortMonth = resultSet.getString("mnth");
					// shortMonth=resultSet.getString("shortMonth");
				}

				ContactData newContact = new ContactData();
				List<String> emails = new ArrayList<String>();
				emails.add(resultSet.getString("Email1"));
				newContact.setEmailAddresses(emails);
				newContact.setFirstName(resultSet.getString("FName"));
				newContact.setLastName(resultSet.getString("LName"));
				newContact.setCompanyName(resultSet.getString("OrgId"));
				importData.add(newContact);

				FilePath = resultSet.getString("FilePath");
				EmployeeName = resultSet.getString("EmployeeName");
				Department = resultSet.getString("Department");
				Title = resultSet.getString("Title");

				if (td == 1) {
					// htmlText.append("<tr><td align='center'
					// valign='top'><table cellpadding='0' cellspacing='0'
					// border='0'><tbody><tr><td width='950' align='center'
					// valign='top'><table width='100%' cellpadding='0'
					// cellspacing='0' border='0'><tbody><tr><td align='center'
					// valign='top' style='padding:0 10px'><table width='100%'
					// cellpadding='0' cellspacing='0' border='0'><tbody><tr><td
					// style='padding-top:0;line-height:0'></td></tr></tbody></table><table
					// width='100%' align='center' cellpadding='0'
					// cellspacing='0' border='0'><tbody><tr><td align='center'
					// valign='top' style='font-size:0!important'>");
					// htmlText.append("<div
					// style='width:100%;display:inline-block;vertical-align:top;font-size:normal;max-width:230px'
					// class='block'><table width='100%' cellpadding='0'
					// cellspacing='0' border='0'><tbody><tr><td align='center'
					// valign='top' style='padding:0 10px'><table width='100%'
					// cellpadding='0' cellspacing='0' border='0'><tbody><tr><td
					// style='padding-top:10px;line-height:0'></td></tr></tbody></table><table
					// width='100%' cellpadding='0' cellspacing='0'
					// border='0'><tbody><tr><td align='center' valign='top'
					// style='line-height:0!important'><img src='"+FilePath+"'
					// alt='' width='100'></td></tr></tbody></table><table
					// width='100%' cellpadding='0' cellspacing='0'
					// border='0'><tbody><tr><td
					// style='padding-top:20px;line-height:0'></td></tr></tbody></table><table
					// width='100%' cellpadding='0' cellspacing='0'
					// border='0'><tbody><tr><td align='center' valign='top'
					// style='color:#fff;font-family:oPen
					// sans;font-size:14px;mso-line-height-rule:exactly;line-height:20px;font-weight:700;text-transform:normal'>"+EmployeeName+"</td></tr><tr><td
					// height='5'></td></tr><tr><td align='center' valign='top'
					// style='color:#fff;font-family:oPen
					// sans;font-size:13px;mso-line-height-rule:exactly;line-height:20px;font-weight:400;text-transform:normal'>"+Department+"
					// | "+Title+"</td></tr><tr><td
					// height='10px'></td></tr></tbody></table><table
					// width='100%' cellpadding='0' cellspacing='0'
					// border='0'><tbody><tr><td
					// style='padding-top:10px;line-height:0'></td></tr></tbody></table></td></tr></tbody></table></div>");
					htmlTxt = htmlTxt
							+ "<tr><td align='center' valign='top'><table cellpadding='0' cellspacing='0' border='0'><tbody><tr><td width='950' align='center' valign='top'><table width='100%' cellpadding='0' cellspacing='0' border='0'><tbody><tr><td align='center' valign='top' style='padding:0 10px'><table width='100%' cellpadding='0' cellspacing='0' border='0'><tbody><tr><td style='padding-top:0;line-height:0'></td></tr></tbody></table><table width='100%' align='center' cellpadding='0' cellspacing='0' border='0'><tbody><tr><td align='center' valign='top' style='font-size:0!important'>";
					htmlTxt = htmlTxt
							+ "<div style='width:100%;display:inline-block;vertical-align:top;font-size:normal;max-width:230px' class='block'><table width='100%' cellpadding='0' cellspacing='0' border='0'><tbody><tr><td align='center' valign='top' style='padding:0 10px'>\n"
							+ "<table width='100%' cellpadding='0' cellspacing='0' border='0'><tbody><tr><td style='padding-top:10px;line-height:0'></td>\n"
							+ "</tr></tbody></table><table width='100' cellpadding='0' cellspacing='0' border='0'><tbody><tr><td class='image-cropper' width='100' height='100' align='center' valign='top' style='border-radius: 180px; line-height:0!important;'><img src='"
							+ FilePath
							+ "' alt='' width='100' style='display: block;-ms-interpolation-mode: bicubic;width: 100px;height: auto;max-width: 100px;-moz-border-radius: 200px;border-radius: 200px;'></td></tr></tbody></table><table width='100%' cellpadding='0' cellspacing='0' border='0'><tbody>\n"
							+ "<tr><td style='padding-top:20px;line-height:0'></td></tr></tbody></table><table width='100%' cellpadding='0' cellspacing='0' border='0'><tbody><tr><td align='center' valign='top' style='color:#fff;font-family:oPen sans;font-size:14px;mso-line-height-rule:exactly;line-height:20px;font-weight:700;text-transform:normal'>\n"
							+ "" + EmployeeName + "\n"
							+ "</td></tr><tr><td height='5'></td></tr><tr><td align='center' valign='top' style='color:#fff;font-family:oPen sans;font-size:13px;mso-line-height-rule:exactly;line-height:20px;font-weight:400;text-transform:normal'>\n"
							+ "" + Department + "\n"
							+ "</td></tr><tr><td height='10px'></td></tr></tbody></table><table width='100%' cellpadding='0' cellspacing='0' border='0'>\n"
							+ "<tbody><tr><td style='padding-top:10px;line-height:0'></td></tr></tbody></table></td></tr></tbody></table></div>";

					td = td + 1;
				} else if (td == 4) {
					htmlTxt = htmlTxt
							+ "<div style='width:100%;display:inline-block;vertical-align:top;font-size:normal;max-width:230px' class='block'><table width='100%' cellpadding='0' cellspacing='0' border='0'><tbody><tr><td align='center' valign='top' style='padding:0 10px'>\n"
							+ "<table width='100%' cellpadding='0' cellspacing='0' border='0'><tbody><tr><td style='padding-top:10px;line-height:0'></td>\n"
							+ "</tr></tbody></table><table width='100' cellpadding='0' cellspacing='0' border='0'><tbody><tr><td class='image-cropper' width='100' height='100' align='center' valign='top' style='border-radius: 180px; line-height:0!important;'><img src='"
							+ FilePath
							+ "' alt='' width='100' style='display: block;-ms-interpolation-mode: bicubic;width: 100px;height: auto;max-width: 100px;-moz-border-radius: 200px;border-radius: 200px;'></td></tr></tbody></table><table width='100%' cellpadding='0' cellspacing='0' border='0'><tbody>\n"
							+ "<tr><td style='padding-top:20px;line-height:0'></td></tr></tbody></table><table width='100%' cellpadding='0' cellspacing='0' border='0'><tbody><tr><td align='center' valign='top' style='color:#fff;font-family:oPen sans;font-size:14px;mso-line-height-rule:exactly;line-height:20px;font-weight:700;text-transform:normal'>\n"
							+ "" + EmployeeName + "\n"
							+ "</td></tr><tr><td height='5'></td></tr><tr><td align='center' valign='top' style='color:#fff;font-family:oPen sans;font-size:13px;mso-line-height-rule:exactly;line-height:20px;font-weight:400;text-transform:normal'>\n"
							+ "" + Department + "\n"
							+ "</td></tr><tr><td height='10px'></td></tr></tbody></table><table width='100%' cellpadding='0' cellspacing='0' border='0'>\n"
							+ "<tbody><tr><td style='padding-top:10px;line-height:0'></td></tr></tbody></table></td></tr></tbody></table></div>";

					htmlTxt = htmlTxt
							+ "</td></tr></tbody></table></td></tr></tbody></table><table width='100%' cellpadding='0' cellspacing='0' border='0'><tbody><tr><td style='padding-top:0px;line-height:0'></td></tr></tbody></table></td></tr></tbody></table></td></tr>";
					// htmlText.append("<div
					// style='width:100%;display:inline-block;vertical-align:top;font-size:normal;max-width:230px'
					// class='block'><table width='100%' cellpadding='0'
					// cellspacing='0' border='0'><tbody><tr><td align='center'
					// valign='top' style='padding:0 10px'><table width='100%'
					// cellpadding='0' cellspacing='0' border='0'><tbody><tr><td
					// style='padding-top:10px;line-height:0'></td></tr></tbody></table><table
					// width='100%' cellpadding='0' cellspacing='0'
					// border='0'><tbody><tr><td align='center' valign='top'
					// style='line-height:0!important'><img src='"+FilePath+"'
					// alt='' width='100'></td></tr></tbody></table><table
					// width='100%' cellpadding='0' cellspacing='0'
					// border='0'><tbody><tr><td
					// style='padding-top:20px;line-height:0'></td></tr></tbody></table><table
					// width='100%' cellpadding='0' cellspacing='0'
					// border='0'><tbody><tr><td align='center' valign='top'
					// style='color:#fff;font-family:oPen
					// sans;font-size:14px;mso-line-height-rule:exactly;line-height:20px;font-weight:700;text-transform:normal'>"+EmployeeName+"</td></tr><tr><td
					// height='5'></td></tr><tr><td align='center' valign='top'
					// style='color:#fff;font-family:oPen
					// sans;font-size:13px;mso-line-height-rule:exactly;line-height:20px;font-weight:400;text-transform:normal'>"+Department+"
					// | "+Title+"</td></tr><tr><td
					// height='10px'></td></tr></tbody></table><table
					// width='100%' cellpadding='0' cellspacing='0'
					// border='0'><tbody><tr><td
					// style='padding-top:10px;line-height:0'></td></tr></tbody></table></td></tr></tbody></table></div>");
					// htmlText.append("</td></tr></tbody></table></td></tr></tbody></table><table
					// width='100%' cellpadding='0' cellspacing='0'
					// border='0'><tbody><tr><td
					// style='padding-top:0px;line-height:0'></td></tr></tbody></table></td></tr></tbody></table></td></tr>");
					td = 1;
				} else {

					htmlTxt = htmlTxt
							+ "<div style='width:100%;display:inline-block;vertical-align:top;font-size:normal;max-width:230px' class='block'><table width='100%' cellpadding='0' cellspacing='0' border='0'><tbody><tr><td align='center' valign='top' style='padding:0 10px'>\n"
							+ "<table width='100%' cellpadding='0' cellspacing='0' border='0'><tbody><tr><td style='padding-top:10px;line-height:0'></td>\n"
							+ "</tr></tbody></table><table width='100' cellpadding='0' cellspacing='0' border='0'><tbody><tr><td class='image-cropper' width='100' height='100' align='center' valign='top' style='border-radius: 180px; line-height:0!important;'><img src='"
							+ FilePath
							+ "' alt='' width='100' style='display: block;-ms-interpolation-mode: bicubic;width: 100px;height: auto;max-width: 100px;-moz-border-radius: 200px;border-radius: 200px;'></td></tr></tbody></table><table width='100%' cellpadding='0' cellspacing='0' border='0'><tbody>\n"
							+ "<tr><td style='padding-top:20px;line-height:0'></td></tr></tbody></table><table width='100%' cellpadding='0' cellspacing='0' border='0'><tbody><tr><td align='center' valign='top' style='color:#fff;font-family:oPen sans;font-size:14px;mso-line-height-rule:exactly;line-height:20px;font-weight:700;text-transform:normal'>\n"
							+ "" + EmployeeName + "\n"
							+ "</td></tr><tr><td height='5'></td></tr><tr><td align='center' valign='top' style='color:#fff;font-family:oPen sans;font-size:13px;mso-line-height-rule:exactly;line-height:20px;font-weight:400;text-transform:normal'>\n"
							+ "" + Department + "\n"
							+ "</td></tr><tr><td height='10px'></td></tr></tbody></table><table width='100%' cellpadding='0' cellspacing='0' border='0'>\n"
							+ "<tbody><tr><td style='padding-top:10px;line-height:0'></td></tr></tbody></table></td></tr></tbody></table></div>";

					// htmlText.append("<div
					// style='width:100%;display:inline-block;vertical-align:top;font-size:normal;max-width:230px'
					// class='block'><table width='100%' cellpadding='0'
					// cellspacing='0' border='0'><tbody><tr><td align='center'
					// valign='top' style='padding:0 10px'><table width='100%'
					// cellpadding='0' cellspacing='0' border='0'><tbody><tr><td
					// style='padding-top:10px;line-height:0'></td></tr></tbody></table><table
					// width='100%' cellpadding='0' cellspacing='0'
					// border='0'><tbody><tr><td align='center' valign='top'
					// style='line-height:0!important'><img src='"+FilePath+"'
					// alt='' width='100'></td></tr></tbody></table><table
					// width='100%' cellpadding='0' cellspacing='0'
					// border='0'><tbody><tr><td
					// style='padding-top:20px;line-height:0'></td></tr></tbody></table><table
					// width='100%' cellpadding='0' cellspacing='0'
					// border='0'><tbody><tr><td align='center' valign='top'
					// style='color:#fff;font-family:oPen
					// sans;font-size:14px;mso-line-height-rule:exactly;line-height:20px;font-weight:700;text-transform:normal'>"+EmployeeName+"</td></tr><tr><td
					// height='5'></td></tr><tr><td align='center' valign='top'
					// style='color:#fff;font-family:oPen
					// sans;font-size:13px;mso-line-height-rule:exactly;line-height:20px;font-weight:400;text-transform:normal'>"+Department+"
					// | "+Title+"</td></tr><tr><td
					// height='10px'></td></tr></tbody></table><table
					// width='100%' cellpadding='0' cellspacing='0'
					// border='0'><tbody><tr><td
					// style='padding-top:10px;line-height:0'></td></tr></tbody></table></td></tr></tbody></table></div>");
					td = td + 1;
				}

				i++;
			}

			if (td > 1 && td <= 4) {
				htmlTxt = htmlTxt
						+ "</td></tr></tbody></table></td></tr></tbody></table><table width='100%' cellpadding='0' cellspacing='0' border='0'><tbody><tr><td style='padding-top:0px;line-height:0'></td></tr></tbody></table></td></tr></tbody></table></td></tr>";
				// htmlText.append("</td></tr></tbody></table></td></tr></tbody></table><table
				// width='100%' cellpadding='0' cellspacing='0'
				// border='0'><tbody><tr><td
				// style='padding-top:0px;line-height:0'></td></tr></tbody></table></td></tr></tbody></table></td></tr>");
			}

			htmlTxt = htmlTxt + "<tr>\n" + "<td height='20'></td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n"
					+ "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td align='center' valign='top'>\n" + "<table cellpadding='0' cellspacing='0'>\n" + "<tbody>\n"
					+ "<tr>\n" + "<td width='1000' align='center' valign='top'>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td align='center' valign='top' style='padding:0 10px'></td>\n" + "</tr>\n" + "</tbody>\n"
					+ "</table>\n" + "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n" + "</td>\n" + "</tr>\n"
					+ "</tbody>\n" + "</table>\n"
					+ "<table width='100%' align='center' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td align='center' valign='top' style='background:#eceff3'>\n"
					+ "<table width='100%' align='center' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td style='background-color:#eceff3;background-repeat:repeat;background-attachment:scroll;background-position:center top;background-clip:border-box;background-origin:padding-box;background-size:cover' align='center' bgcolor='#eceff3' width='100%' class=''>\n"
					+ "<table width='100%' align='center' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td align='center' valign='top' style='padding:0'>\n"
					+ "<table align='center' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td width='200' align='center' valign='top'>\n"
					+ "<table width='100%' align='center' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td height='30' style='height:30px;line-height:30px'></td>\n" + "</tr>\n" + "<tr>\n"
					+ "<td align='center' valign='top' style='line-height:0!important'>\n"
					+ "<a style='text-decoration:none;display:block' href='https://www.miraclesoft.com/' target='blank'>\n"
					+ "<img src='https://www.miraclesoft.com/images/newsletters/miracle-logo-dark.png' alt='logo' width='100%' style='width:100%;max-width:100%'>\n"
					+ "</a>\n" + "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n" + "</td>\n" + "</tr>\n"
					+ "</tbody>\n" + "</table>\n"
					+ "<table width='100%' align='center' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td height='20' style='height:20px;line-height:20px'></td>\n" + "</tr>\n" + "<tr>\n"
					+ "<td align='center' valign='top'>\n" + "<table align='center' cellpadding='0' cellspacing='0'>\n"
					+ "<tbody>\n" + "<tr>\n"
					+ "<td width='600' align='center' valign='top' style='font-size:0!important'>\n"
					+ "<div style='width:100%;display:inline-block;vertical-align:top;font-size:normal;max-width:60px'>\n"
					+ "<table width='100%' align='center' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td align='center' valign='top' style='padding:0 10px'>\n"
					+ "<table width='100%' align='center' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td height='10' style='height:10px;line-height:10px'></td>\n" + "</tr>\n" + "<tr>\n"
					+ "<td align='center' valign='top' style='line-height:0!important'>\n"
					+ "<a style='text-decoration:none;display:inline-block' href='https://facebook.com/miracle45625' target='blank'>\n"
					+ "<img src='https://www.miraclesoft.com/images/newsletters/2016/September/facebook-1.png' alt='socials1' width='40'>\n"
					+ "</a>\n" + "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n" + "</td>\n" + "</tr>\n"
					+ "</tbody>\n" + "</table>\n" + "</div>\n"
					+ "<div style='width:100%;display:inline-block;vertical-align:top;font-size:normal;max-width:60px'>\n"
					+ "<table width='100%' align='center' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td align='center' valign='top' style='padding:0 10px'>\n"
					+ "<table width='100%' align='center' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td height='10' style='height:10px;line-height:10px'></td>\n" + "</tr>\n" + "<tr>\n"
					+ "<td align='center' valign='top' style='line-height:0!important'>\n"
					+ "<a style='text-decoration:none;display:inline-block' href='https://plus.google.com/+Team_MSS' target='blank'>\n"
					+ "<img src='https://www.miraclesoft.com/images/newsletters/2016/September/google-plus-2.png' alt='socials1' width='40'>\n"
					+ "</a>\n" + "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n" + "</td>\n" + "</tr>\n"
					+ "</tbody>\n" + "</table>\n" + "</div>\n"
					+ "<div style='width:100%;display:inline-block;vertical-align:top;font-size:normal;max-width:60px'>\n"
					+ "<table width='100%' align='center' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td align='center' valign='top' style='padding:0 10px'>\n"
					+ "<table width='100%' align='center' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td height='10' style='height:10px;line-height:10px'></td>\n" + "</tr>\n" + "<tr>\n"
					+ "<td align='center' valign='top' style='line-height:0!important'>\n"
					+ "<a style='text-decoration:none;display:inline-block' href='https://www.linkedin.com/company/miracle-software-systems-inc' target='blank'>\n"
					+ "<img src='https://www.miraclesoft.com/images/newsletters/2016/September/linkedin-2.png' alt='socials1' width='40'>\n"
					+ "</a>\n" + "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n" + "</td>\n" + "</tr>\n"
					+ "</tbody>\n" + "</table>\n" + "</div>\n"
					+ "<div style='width:100%;display:inline-block;vertical-align:top;font-size:normal;max-width:60px'>\n"
					+ "<table width='100%' align='center' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td align='center' valign='top' style='padding:0 10px'>\n"
					+ "<table width='100%' align='center' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td height='10' style='height:10px;line-height:10px'></td>\n" + "</tr>\n" + "<tr>\n"
					+ "<td align='center' valign='top' style='line-height:0!important'>\n"
					+ "<a style='text-decoration:none;display:inline-block' href='https://www.flickr.com/photos/team_miracle' target='blank'>\n"
					+ "<img src='https://www.miraclesoft.com/images/newsletters/2016/September/flickr-1.png' alt='socials1' width='40'>\n"
					+ "</a>\n" + "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n" + "</td>\n" + "</tr>\n"
					+ "</tbody>\n" + "</table>\n" + "</div>\n"
					+ "<div style='width:100%;display:inline-block;vertical-align:top;font-size:normal;max-width:60px'>\n"
					+ "<table width='100%' align='center' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td align='center' valign='top' style='padding:0 10px'>\n"
					+ "<table width='100%' align='center' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td height='10' style='height:10px;line-height:10px'></td>\n" + "</tr>\n" + "<tr>\n"
					+ "<td align='center' valign='top' style='line-height:0!important'>\n"
					+ "<a style='text-decoration:none;display:inline-block' href='https://twitter.com/Team_MSS' target='blank'>\n"
					+ "<img src='https://www.miraclesoft.com/images/newsletters/2016/September/twitter-2.png' alt='socials2' width='40'>\n"
					+ "</a>\n" + "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n" + "</td>\n" + "</tr>\n"
					+ "</tbody>\n" + "</table>\n" + "</div>\n"
					+ "<div style='width:100%;display:inline-block;vertical-align:top;font-size:normal;max-width:60px'>\n"
					+ "<table width='100%' align='center' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td align='center' valign='top' style='padding:0 10px'>\n"
					+ "<table width='100%' align='center' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td height='10' style='height:10px;line-height:10px'></td>\n" + "</tr>\n" + "<tr>\n"
					+ "<td align='center' valign='top' style='line-height:0!important'>\n"
					+ "<a style='text-decoration:none;display:inline-block' href='https://www.youtube.com/c/Team_MSS' target='blank'>\n"
					+ "<img src='https://www.miraclesoft.com/images/newsletters/2016/September/youtube-3.png' alt='socials3' width='40'>\n"
					+ "</a>\n" + "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n" + "</td>\n" + "</tr>\n"
					+ "</tbody>\n" + "</table>\n" + "</div>\n" + "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n"
					+ "</td>\n" + "</tr>\n" + "<tr>\n" + "<td height='15' style='height:15px;line-height:15px'></td>\n"
					+ "</tr>\n" + "</tbody>\n" + "</table>\n"
					+ "<table width='100%' align='center' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td height='10' style='height:10px;line-height:10px'></td>\n" + "</tr>\n" + "<tr>\n"
					+ "<td align='center' valign='top' style='color:#232527;font-family:Open sans;font-size:14px;mso-line-height-rule:exactly;line-height:23px;font-weight:400'>\n"
					+ "&#169; Copyrights " + Calendar.getInstance().get(Calendar.YEAR) + " | Miracle Software Systems\n"
					+ "</td>\n" + "</tr>\n" + "<tr>\n" + "<td height='30' style='height:30px;line-height:30px'></td>\n"
					+ "</tr>\n" + "</tbody>\n" + "</table>\n" + "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n"
					+ "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n" + "</td>\n" + "</tr>\n" + "</tbody>\n"
					+ "</table>\n" + "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n" + "</td>\n" + "</tr>\n"
					+ "</tbody>\n" + "</table>\n" + "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table></body></html>";

			//////////////////////////

			//////////////////
			String first = "<html xmlns='http://www.w3.org/1999/xhtml'><head>\n" + "\n"
					+ "<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0;' name='viewport'>\n"
					+ "<link rel='stylesheet' type='text/css' href='//fonts.googleapis.com/css?family=Open+Sans'>\n"
					+ "<title>Star Performers</title>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>#outlook a{padding:0}\n"
					+ ".ReadMsgBody,.ExternalClass{width:100%}.ExternalClass,.ExternalClass p,.ExternalClass span,.ExternalClass font,.ExternalClass td,.ExternalClass div{line-height:100%}body,table,td,span,a{-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;-webkit-font-smoothing:antialiased}table,td{mso-table-lspace:0;mso-table-rspace:0}img{-ms-interpolation-mode:bicubic}html,body,#wrappertable{width:100%!important;margin:0!important;padding:0!important;line-height:100%!important}img{display:block;border:0;height:auto;outline:none!important;line-height:100%!important;text-decoration:none!important}table{border-spacing:0!important;border-collapse:collapse!important}table td{border-collapse:collapse!important}a,a:link,a:visited,a:focus,a:hover{color:inherit!important;outline:none!important;text-decoration:none!important}</style>\n"
					+ "<style type='text/css'>@media only screen and (max-width:568px){div[class='block']{width:100%!important;max-width:none!important}table[class='full-width']{width:100%!important}table[class='center'],td[class='center']{text-align:center!important;float:none!important}td[class='smaller']{font-size:65px!important;line-height:65px!important}}@media only screen and (max-width:375px){div[class='block']{width:100%!important;max-width:none!important}td[class='smaller']{font-size:50px!important;line-height:50px!important}}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "</head>\n" + "<body id='body-layout'>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0' id='wrappertable' style='table-layout:fixed'>\n"
					+ "<tbody>\n" + "<tr>\n" + "<td align='center' valign='top'>\n"
					+ "<table cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td width='1600' align='center' valign='top'>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0' style='background:#eceff3'>\n" + "<tbody>\n"
					+ "<tr>\n" + "<td align='center' valign='top'>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0' style='background:#2e3538'>\n" + "<tbody>\n"
					+ "<tr>\n"
					+ "<td style='background-color:#fff;background-repeat:repeat;background-attachment:scroll;background-position:center top;background-clip:border-box;background-origin:padding-box;background-size:cover;background:url(https://www.miraclesoft.com/images/newsletters/2017/January/star_banner2.png)' align='center' bgcolor='e35500' width='100%' class=''>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td align='center' valign='top' style='padding:0 20px'>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td style='padding-top:30px;line-height:0'></td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n"
					+ "<table cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td width='250' align='center' valign='top' style='line-height:0!important'>\n"
					+ "<a style='text-decoration:none;display:block' href='https://www.miraclesoft.com/' target='_blank'>\n"
					+ "<img src='https://www.miraclesoft.com/images/newsletters/Q2/miracle-logo-light.png' alt='' width='100%' style='width:100%;max-width:100%'>\n"
					+ "</a>\n" + "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td style='padding-top:30px;line-height:20px'></td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n"
					+ "<table cellpadding='0' cellspacing='0' style='background:#fff;background:rgba(255,255,255,0.4)'>\n"
					+ "<tbody>\n" + "<tr>\n" + "<td width='900' align='center' valign='top'>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td align='center' valign='top' style='padding:0'>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td style='padding-top:50px;line-height:0'></td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td align='center' valign='top' style='color:#fff;font-family:oPen sans;font-size:47px;mso-line-height-rule:exactly;line-height:40px;font-weight:900;text-transform:normal'>\n"
					+ "Star Performers - " + month + "\n" + "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td style='padding-top:25px;line-height:0'></td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n"
					+ "<table cellpadding='0' cellspacing='0' style=''>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td width='220' align='center' valign='top'>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td style='padding-top:2px;line-height:0'>\n" + "<span>\n"
					+ "<img src='https://www.miraclesoft.com/images/newsletters/heading-divider-img.png' alt='heading-divider-img' width='200' height='auto' style='display:inline-block'>\n"
					+ "</span>\n" + "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n" + "</td>\n" + "</tr>\n"
					+ "</tbody>\n" + "</table>\n" + "<table width='100%' cellpadding='0' cellspacing='0'>\n"
					+ "<tbody>\n" + "<tr>\n" + "<td style='padding-top:20px;line-height:0'></td>\n" + "</tr>\n"
					+ "</tbody>\n" + "</table>\n" + "<table width='100%' cellpadding='0' cellspacing='0'>\n"
					+ "<tbody>\n" + "<tr>\n"
					+ "<td align='center' valign='top' style='color:#fff;font-family:oPen sans;font-size:20px;mso-line-height-rule:exactly;line-height:32px;font-weight:500'>\n"
					+ "Miraclites who have exceeded expectations and were outstanding within their teams!\n" + "</td>\n"
					+ "</tr>\n" + "</tbody>\n" + "</table>\n" + "<table width='100%' cellpadding='0' cellspacing='0'>\n"
					+ "<tbody>\n" + "<tr>\n" + "<td style='padding-top:40px;line-height:0'></td>\n" + "</tr>\n"
					+ "</tbody>\n" + "</table>\n" + "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n" + "</td>\n"
					+ "</tr>\n" + "</tbody>\n" + "</table>\n" + "<table width='100%' cellpadding='0' cellspacing='0'>\n"
					+ "<tbody>\n" + "<tr>\n" + "<td align='center' valign='top' style='line-height:0!important'>\n"
					+ "<img src='https://www.miraclesoft.com/images/newsletters/2016/September/corner.png' style='opacity:0.4' alt='' width='44'>\n"
					+ "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td style='padding-top:30px;line-height:0'></td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n"
					+ "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n" + "</td>\n" + "</tr>\n" + "</tbody>\n"
					+ "</table>\n" + "<table width='100%' cellpadding='0' cellspacing='0' border='0'>\n" + "<tbody>\n"
					+ "<tr>\n" + "<td align='center' valign='top'>\n"
					+ "<table cellpadding='0' cellspacing='0' border='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td width='900' align='center' valign='top'>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0' border='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td align='center' valign='top' style='padding:0 10px'>\n"
					+ "<table width='100%' align='center' cellpadding='0' cellspacing='0' border='0'>\n" + "<tbody>\n"
					+ "<tr>\n" + "<td align='center' valign='top' style='font-size:0!important'>\n"
					+ "<div style='width:100%;display:inline-block;vertical-align:top;font-size:normal;max-width:250px' class='block'>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0' border='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td align='center' valign='top' style='padding:0 10px'>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0' border='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td style='padding-top:30px;line-height:0'></td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0' border='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td align='center' valign='top' style='line-height:0!important'>\n"
					+ "<img src='https://www.miraclesoft.com/images/newsletters/2017/January/hardwork.png' alt='' width='120'>\n"
					+ "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0' border='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td style='padding-top:17px;line-height:0'></td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0' border='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td align='center' valign='top' style='color:#232527;font-family:oPen sans;font-size:16px;mso-line-height-rule:exactly;line-height:30px;font-weight:700;text-transform:normal'>\n"
					+ "Hard-work\n" + "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n" + "</td>\n" + "</tr>\n"
					+ "</tbody>\n" + "</table>\n" + "</div>\n"
					+ "<div style='width:100%;display:inline-block;vertical-align:top;font-size:normal;max-width:250px' class='block'>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0' border='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td align='center' valign='top' style='padding:0 10px'>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0' border='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td style='padding-top:30px;line-height:0'></td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0' border='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td align='center' valign='top' style='line-height:0!important'>\n"
					+ "<img src='https://www.miraclesoft.com/images/newsletters/2017/January/performance.png' alt='' width='120'>\n"
					+ "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0' border='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td style='padding-top:17px;line-height:0'></td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0' border='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td align='center' valign='top' style='color:#232527;font-family:oPen sans;font-size:16px;mso-line-height-rule:exactly;line-height:30px;font-weight:700;text-transform:normal'>\n"
					+ "Performance\n" + "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n" + "</td>\n" + "</tr>\n"
					+ "</tbody>\n" + "</table>\n" + "</div>\n"
					+ "<div style='width:100%;display:inline-block;vertical-align:top;font-size:normal;max-width:250px' class='block'>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0' border='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td align='center' valign='top' style='padding:0 10px'>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0' border='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td style='padding-top:30px;line-height:0'></td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0' border='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td align='center' valign='top' style='line-height:0!important'>\n"
					+ "<img src='https://www.miraclesoft.com/images/newsletters/2017/January/awards.png' alt='' width='120'>\n"
					+ "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0' border='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td style='padding-top:17px;line-height:0'></td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0' border='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td align='center' valign='top' style='color:#232527;font-family:oPen sans;font-size:16px;mso-line-height-rule:exactly;line-height:30px;font-weight:700;text-transform:normal'>\n"
					+ "Awards\n" + "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n" + "</td>\n" + "</tr>\n"
					+ "</tbody>\n" + "</table>\n" + "</div>\n" + "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n"
					+ "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n" + "</td>\n" + "</tr>\n" + "</tbody>\n"
					+ "</table>\n" + "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td height='30px'></td>\n" + "</tr>\n" + "<tr>\n" + "<td align='center' valign='top'>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td align='center' valign='top' style='padding:0 20px'>\n"
					+ "<table cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td width='900' align='center' valign='top'>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td align='center' valign='top'>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0' style='background:#fff'>\n" + "<tbody>\n"
					+ "<tr>\n" + "<td align='center' valign='top' style='padding:0 18px'>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td style='padding-top:20px;line-height:0'></td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td align='center' valign='top' style='padding:0 12px'>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td style='padding-top:10px;line-height:0'></td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td align='justify' valign='top' style='color:#8c8c8c;font-family:oPen sans;font-size:14px;mso-line-height-rule:exactly;line-height:30px;font-weight:400'>\n"
					+ "<b>An organization is defined by its employees!</b> At Miracle we are one great big family and we are thrilled to see some of our best being recognized. The following team members have been able to excel in high pressure environments and deliver when needed. For this month they are our Stars and will get the chance to go to a movie with their friends (or) family. We would like to thank them all for their contributions and the efforts that they have put in for the organization.\n"
					+ "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td style='padding-top:10px;line-height:0'></td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td align='justify' valign='top' style='color:#8c8c8c;font-family:oPen sans;font-size:14px;mso-line-height-rule:exactly;line-height:30px;font-weight:400'>\n"
					+ "<b>Keep working hard and we hope to see more Miracle Stars in the months to come!</b>\n"
					+ "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td style='padding-top:10px;line-height:0'></td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n"
					+ "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td style='padding-top:20px;line-height:0'></td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n"
					+ "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n" + "</td>\n" + "</tr>\n" + "</tbody>\n"
					+ "</table>\n" + "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n" + "</td>\n" + "</tr>\n"
					+ "</tbody>\n" + "</table>\n" + "</td>\n" + "</tr>\n" + "<tr>\n" + "<td height='30px'></td>\n"
					+ "</tr>\n" + "</tbody>\n" + "</table>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0' border='0' style='background:#2e3538'>\n"
					+ "<tbody>\n" + "<tr>\n"
					+ "<td data-bg='Top Banner' data-thumb='https://www.miraclesoft.com/images/newsletters/2016/november/banner_DS9.png' data-module='Top Banner' mc:repeatable='layout' mc:hideable='' mc:variant='Top Banner' style='background-color:#fff;background-repeat:repeat;background-attachment:scroll;background-position:center top;background-clip:border-box;background-origin:padding-box;background-size:cover' align='center' background='https://www.miraclesoft.com/images/newsletters/2016/november/banner_DS9.png' bgcolor='e35500' border='0' cellpadding='0' cellspacing='0' width='100%' class=''>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0' border='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td align='center' valign='top' style='padding:0 20px'>\n"
					+ "<table cellpadding='0' cellspacing='0' border='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td width='900' align='center' valign='top'>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0' border='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td style='padding-top:30px;line-height:0'></td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0' border='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td align='center' valign='top' style='color:#fff;font-family:oPen sans;font-size:28px;mso-line-height-rule:exactly;line-height:36px;font-weight:700;text-transform:normal'>\n"
					+ "Best of The Best!!\n" + "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n" + "</td>\n"
					+ "</tr>\n" + "</tbody>\n" + "</table>\n" + "</td>\n" + "</tr>\n" + "<tr>\n"
					+ "<td height='20'></td>\n" + "</tr>\n" + "<tr>\n" + "<td>\n"
					+ "<table data-bgcolor='Who We Are Underline' style='mso-table-lspace:0;mso-table-rspace:0;margin:0 auto;border-collaps:collaps' align='center' bgcolor='e4e4e4' border='0' cellpadding='0' cellspacing='0' width='40'>\n"
					+ "<tbody>\n" + "<tr>\n" + "<td height='2'></td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n"
					+ "</td>\n" + "</tr><tr>\n" + "<td height='20'></td>\n" + "</tr>";

			String finalText = first + htmlTxt;

			contactsToAdd.setImportData(importData);

			_activityService.addContacts(contactsToAdd);

			String campaignTemplate = finalText.toString().replaceAll("\"", "'");
			///////////////////////////////////////////////////////////////////////////////////////////////

			EmailCampaignService ecs = new EmailCampaignService(Properties.getProperty("API_KEY"),
					Properties.getProperty("ACCESS_TOCKEN"));

			EmailCampaignRequest emailReq = new EmailCampaignRequest();

			String datetime = new SimpleDateFormat("ddMMMyyyy_hh_mm_ss_SSSa").format(new java.util.Date());
			emailReq.setName("Star Performers List " + datetime);
			emailReq.setSubject("Star Performers " + shortMonth + " " + fullYear);

			emailReq.setFromEmail("newsletter@miraclesoft.com");
			emailReq.setFromName("Miracle Software Systems, Inc.");
			emailReq.setReplyToEmail("newsletter@miraclesoft.com ");

			emailReq.setViewAsWebpageEnabled(false);

			emailReq.setGreetingName("FIRST_NAME");
			emailReq.setGreetingSalutations("Hello");
			emailReq.setGreetingString("Dear");
			emailReq.setEmailContent(campaignTemplate);
			emailReq.setEmailContentFormat("HTML");
			// emailReq.setTemplateType("CUSTOM");
			emailReq.setTextContent("Star Performers");

			MessageFooter messageFooter = new MessageFooter();
			messageFooter.setAddressLine1("45625 Grand River Avenue");
			messageFooter.setOrganizationName("Miracle Software Systems, Inc.");
			messageFooter.setCity("Novi");
			messageFooter.setCountry("US");
			messageFooter.setState("MI");
			messageFooter.setPostalCode("48374");
			emailReq.setMessageFooter(messageFooter);

			emailReq.setSentToContactLists(scl);

			// System.out.println(emailReq.toJSON().toString());
			EmailCampaignResponse errs = ecs.addCampaign(emailReq);

			if (errs.getId() != null && !"".equalsIgnoreCase(errs.getId().trim())) {

				String query1 = "Update tblStarPerformers Set CampaignId='" + errs.getId() + "',Status='5' WHERE Id="
						+ objectId + "";
				statement1 = connection.createStatement();
				statement1.executeUpdate(query1);

				EmailCampaignScheduleService ecsh = new EmailCampaignScheduleService(Properties.getProperty("API_KEY"),
						Properties.getProperty("ACCESS_TOCKEN"));

				// EmailCampaignResponse ecrsp=ecs.getCampaign(errs.getId());

				EmailCampaignResponse ecrsp = ecs.getCampaign(errs.getId());

				EmailCampaignSchedule Ecs = new EmailCampaignSchedule();

				String campgnStartDate = com.mss.mirage.util.DateUtility.getInstance()
						.getDateforScheduledDateTime(campaignStartDate);
				Ecs.setScheduledDate(campgnStartDate);

				EmailCampaignSchedule finalEcs = null;
				int size = 0;
				while (size <= i) {
					com.constantcontact.components.generic.response.ResultSet<Contact> c = cls
							.getContactsFromList(contactId, 500, null);

					size = c.size();

					// System.out.println(size+"........"+i);

					if (size == i) {
						finalEcs = ecsh.addSchedule(ecrsp.getId(), Ecs);
						break;
					}

				}
				if (finalEcs != null) {
					String campgnDate = com.mss.mirage.util.DateUtility.getInstance()
							.convertFromGraphViewToElastic(finalEcs.getScheduledDate());

					String query2 = "Update tblStarPerformers Set CampaignScheduledDate='" + campgnDate
							+ "',ScheduledId='" + finalEcs.getId() + "' WHERE Id=" + objectId + "";
					statement2 = connection.createStatement();
					rows = statement2.executeUpdate(query2);

					String sendMailId = Properties.getProperty("STARPERFORMER_SUVERYFORM_ACCESS");
					MailManager sendMail = new MailManager();
					sendMail.sendStarPerformerDetailsForSurveyForm(sendMailId, Integer.parseInt(objectId),
							shortMonth + " " + fullYear);

				}

			}

		} catch (SQLException ex) {
			Logger.getLogger(NewAjaxHandlerServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
		} catch (ConstantContactServiceException e) {
			ConstantContactServiceException c = (ConstantContactServiceException) e.getCause();
			for (Iterator<RawApiRequestError> i1 = c.getErrorInfo().iterator(); i1.hasNext();) {
				RawApiRequestError item = i1.next();
				// System.out.println(item.getErrorKey() + " : " +
				// item.getErrorMessage());
			}
			e.printStackTrace();
			// System.out.println(e.getMessage());
		}

		finally {
			try {
				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException sqle) {
			}
		}
		// System.err.println("response string is"+stringBuffer.toString());
		return rows;
	}

	public String doEmpMovieTicketList(String objectId, int survyId, List emailList) throws ServiceLocatorException {

		PreparedStatement preparedStatement = null;

		ResultSet resultSet = null;
		Connection connection = null;
		String totalStream = "";
		try {

			String email = "";

			if (emailList.size() > 0) {
				for (int j = 0; j < emailList.size(); j++) {
					email = email + "'" + emailList.get(j) + "',";
				}
			}

			connection = ConnectionProvider.getInstance().getConnection();
			if (!"".equalsIgnoreCase(email)) {
				email = email.substring(0, email.length() - 1);
			} else {
				email = "''";
			}
			String query = "SELECT tblStarPerformerLines.Id,tblStarPerformerLines.ObjectId,tblStarPerformerLines.EmpId,tblStarPerformerLines.EmpName AS EmployeeName,tblEmployee.Email1,tblStarPerformerLines.ResumePath,tblStarPerformers.SurveyFormId,tblStarPerformerLines.IsMovieTicketSent FROM tblStarPerformerLines LEFT OUTER JOIN tblStarPerformers ON(tblStarPerformers.Id=tblStarPerformerLines.ObjectId) left join tblEmployee ON(tblStarPerformerLines.EmpId=tblEmployee.Id)  WHERE tblStarPerformerLines.ObjectId="
					+ objectId + " AND  tblEmployee.Email1 IN(" + email + ")";
			// System.out.println("query-----" + query);

			preparedStatement = connection.prepareStatement(query);

			resultSet = preparedStatement.executeQuery();

			int i = 0;
			while (resultSet.next()) {

				int Id = resultSet.getInt("Id");
				int ObjectId = resultSet.getInt("ObjectId");
				int EmpId = resultSet.getInt("EmpId");
				String EmployeeName = resultSet.getString("EmployeeName");
				String Email1 = resultSet.getString("Email1");
				String ResumePath = resultSet.getString("ResumePath");
				int SurveyFormId = resultSet.getInt("SurveyFormId");
				int IsMovieTicketSent = resultSet.getInt("IsMovieTicketSent");

				i++;

				totalStream = totalStream + i + "#^$" + Id + "#^$" + ObjectId + "#^$" + EmployeeName + "#^$" + EmpId
						+ "#^$" + Email1 + "#^$" + ResumePath + "#^$" + SurveyFormId + "#^$" + IsMovieTicketSent
						+ "*@!";
				// System.out.println("--totalStream......................................................."+totalStream);

			}

		}

		catch (SQLException ex) {
			Logger.getLogger(NewAjaxHandlerServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
		}

		finally {
			try {
				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException sqle) {
			}
		}
		// System.err.println("response string is"+stringBuffer.toString());
		return totalStream;
	}

	public int doFeedBackIntegration(String objectId, String year, String month, int suvyId, List emailList)
			throws ServiceLocatorException {
		int rows = 0;
		PreparedStatement preparedStatement = null;
		Statement statement = null;
		ResultSet resultSet = null;
		Connection connection = null;
		try {

			String contactId = "";

			ContactListService cls = new ContactListService(Properties.getProperty("API_KEY"),
					Properties.getProperty("ACCESS_TOCKEN"));

			String email = "";

			if (emailList.size() > 0) {
				for (int j = 0; j < emailList.size(); j++) {
					email = email + "'" + emailList.get(j) + "',";
				}
			}

			if (!"".equalsIgnoreCase(email)) {
				email = email.substring(0, email.length() - 1);
			} else {
				email = "''";
			}

			List<ContactList> clst = cls.getLists(null);

			for (int j = 0; j < clst.size(); j++) {

				if ("".equals(contactId)) {
					if (clst.get(j).getName().equals("SP_FeedBk_List" + month + year)) {
						contactId = clst.get(j).getId();
					}
				}
			}
			// System.out.println("contactId"+contactId);
			if (!"".equals(contactId.toString().trim())) {
				cls.deleteList(contactId);
				contactId = "";
			}

			if ("".equals(contactId.toString().trim())) {
				ContactList contList = new ContactList();
				contList.setName("SP_FeedBk_List" + month + year);
				contList.setStatus("ACTIVE");
				contList = cls.addList(contList);
				contactId = contList.getId();
				// System.out.println("contactId"+contactId);
			}
			// System.out.println("contactId"+contactId);

			ConstantContactFactory cf = new ConstantContactFactory(Properties.getProperty("API_KEY"),
					Properties.getProperty("ACCESS_TOCKEN"));
			IBulkActivitiesService _activityService = cf.createBulkActivitiesService();

			AddContactsRequest contactsToAdd = new AddContactsRequest();
			List<String> contactLists = new ArrayList<String>();
			List<String> columns = new ArrayList<String>();
			List<ContactData> importData = new ArrayList<ContactData>();

			contactLists.add(contactId); // NOTE: Replace this with the IF for
											// the list you wish to use
			columns.add("EMAIL ADDRESS");
			columns.add("FIRST NAME");
			columns.add("LAST NAME");
			columns.add("Company Name");
			contactsToAdd.setColumnNames(columns);
			contactsToAdd.setLists(contactLists);

			connection = ConnectionProvider.getInstance().getConnection();

			String query = "SELECT (YEAR(DATE)-2000) AS yr,YEAR(DATE) AS fullYear,MONTHNAME(STR_TO_DATE(MONTH(DATE), '%m')) AS mnth,DATE_FORMAT(DATE, '%b') AS shortMonth,tblStarPerformerLines.Id,tblStarPerformers.Id As spId,tblStarPerformerLines.ObjectId,tblStarPerformerLines.EmpId,tblStarPerformerLines.EmpName AS EmployeeName,tblStarPerformerLines.Title AS Title,tblStarPerformerLines.CreatedBy,tblStarPerformerLines.Comments,DATE,tblStarPerformerLines.FilePath As FilePath,tblStarPerformerLines.Department As Department,tblStarPerformerLines.EmpId,tblEmployee.Email1,tblEmployee.FName,tblEmployee.LName,tblEmployee.OrgId FROM tblStarPerformerLines LEFT OUTER JOIN tblStarPerformers ON(tblStarPerformers.Id=tblStarPerformerLines.ObjectId) left join tblEmployee ON(tblStarPerformerLines.EmpId=tblEmployee.Id)  WHERE tblStarPerformerLines.Status='Approve' AND tblStarPerformerLines.ObjectId="
					+ objectId + " AND tblEmployee.Email1 IN(" + email + ")";
			// System.out.println("query-----" + query);

			preparedStatement = connection.prepareStatement(query);

			resultSet = preparedStatement.executeQuery();

			String fullYear = "";
			String shortMonth = "";

			String Title = null;
			String FilePath = null;
			String Department = null;
			String EmployeeName = null;
			int i = 0;
			int td = 1;
			while (resultSet.next()) {

				if ("".equals(fullYear)) {
					fullYear = resultSet.getString("fullYear");
				}
				if ("".equals(shortMonth)) {
					// shortMonth=resultSet.getString("shortMonth");
					shortMonth = resultSet.getString("mnth");
				}

				ContactData newContact = new ContactData();
				List<String> emails = new ArrayList<String>();
				emails.add(resultSet.getString("Email1"));
				newContact.setEmailAddresses(emails);
				newContact.setFirstName(resultSet.getString("FName"));
				newContact.setLastName(resultSet.getString("LName"));
				newContact.setCompanyName(resultSet.getString("OrgId"));
				importData.add(newContact);

				FilePath = resultSet.getString("FilePath");
				EmployeeName = resultSet.getString("EmployeeName");
				Department = resultSet.getString("Department");
				Title = resultSet.getString("Title");
				i++;
			}

			contactsToAdd.setImportData(importData);

			_activityService.addContacts(contactsToAdd);

			String htmlText = "<html xmlns='http://www.w3.org/1999/xhtml'><head>\n"
					+ "<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>\n"
					+ "<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0;' name='viewport'>\n"
					+ "<link rel='stylesheet' type='text/css' href='//fonts.googleapis.com/css?family=Open+Sans'>\n"
					+ "<title>Star Performers - February'17</title>\n" + "\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>#outlook a{padding:0}.ReadMsgBody,.ExternalClass{width:100%}.ExternalClass,.ExternalClass p,.ExternalClass span,.ExternalClass font,.ExternalClass td,.ExternalClass div{line-height:100%}body,table,td,span,a{-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;-webkit-font-smoothing:antialiased}table,td{mso-table-lspace:0;mso-table-rspace:0}img{-ms-interpolation-mode:bicubic}html,body,#wrappertable{width:100%!important;margin:0!important;padding:0!important;line-height:100%!important}img{display:block;border:0;height:auto;outline:none!important;line-height:100%!important;text-decoration:none!important}table{border-spacing:0!important;border-collapse:collapse!important}table td{border-collapse:collapse!important}a,a:link,a:visited,a:focus,a:hover{color:inherit!important;outline:none!important;text-decoration:none!important}</style>\n"
					+ "<style type='text/css'>@media only screen and (max-width:568px){div[class='block']{width:100%!important;max-width:none!important}table[class='full-width']{width:100%!important}table[class='center'],td[class='center']{text-align:center!important;float:none!important}td[class='smaller']{font-size:65px!important;line-height:65px!important}}@media only screen and (max-width:375px){div[class='block']{width:100%!important;max-width:none!important}td[class='smaller']{font-size:50px!important;line-height:50px!important}}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "</head>\n" + "<body id='body-layout'>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0' id='wrappertable' style='table-layout: fixed;'>\n"
					+ "<tbody>\n" + "<tr>\n" + "<td align='center' valign='top'>\n"
					+ "<table cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td width='1600' align='center' valign='top'>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0' style='background: #eceff3;'>\n"
					+ "<tbody>\n" + "<tr>\n" + "<td align='center' valign='top'>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0' style='background: #2e3538;'>\n"
					+ "<tbody>\n" + "<tr>\n"
					+ "<td style='background-color: rgb(255, 255, 255); background-repeat: repeat; background-attachment: scroll; background-position: center top; background-clip: border-box; background-origin: padding-box; background-size: cover;background:url(https://www.miraclesoft.com/images/newsletters/2017/May/feedbackk.png);' align='center' bgcolor='e35500' width='100%' class=''>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td align='center' valign='top' style='padding: 0 20px;'>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td style='padding-top: 20px;line-height: 0px;'></td>\n" + "</tr>\n" + "</tbody>\n"
					+ "</table>\n" + "<table cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td width='250' align='center' valign='top' style='line-height: 0px !important;'>\n"
					+ "<a style='text-decoration:none;display:block' href='https://www.miraclesoft.com/' target='_blank'> \n"
					+ "<img src='https://www.miraclesoft.com/images/newsletters/Q2/miracle-logo-light.png' alt='' width='100%' style='width:100%;max-width: 100%;'>\n"
					+ "</a>\n" + "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td style='padding-top: 25px;line-height: 20px;'></td>\n" + "</tr>\n" + "</tbody>\n"
					+ "</table>\n"
					+ "<table cellpadding='0' cellspacing='0' style='background: #ffffff; background: rgba(255,255,255,0.4);'>\n"
					+ "<tbody>\n" + "<tr>\n" + "<td width='900' align='center' valign='top'>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td align='center' valign='top' style='padding: 0;'>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td style='padding-top: 35px; line-height: 0px;'></td>\n" + "</tr>\n" + "</tbody>\n"
					+ "</table>\n" + "<table width='100%' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td align='center' valign='top' style='color: #ffffff; font-family: oPen sans; font-size: 47px; mso-line-height-rule: exactly; line-height: 40px; font-weight: 900; text-transform: normal;'>\n"
					+ "Star Performers - " + shortMonth + "\n" + "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td style='padding-top: 20px; line-height: 0px;'></td>\n" + "</tr>\n" + "</tbody>\n"
					+ "</table>\n" + "<table cellpadding='0' cellspacing='0' style=''>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td width='220' align='center' valign='top'>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td style='padding-top: 2px; line-height: 0px;'>\n" + "<span>\n"
					+ "<img src='https://www.miraclesoft.com/images/newsletters/heading-divider-img.png' alt='heading-divider-img' width='200' height='auto' style='display: inline-block;'>\n"
					+ "</span>\n" + "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n" + "</td>\n" + "</tr>\n"
					+ "</tbody>\n" + "</table>\n" + "<table width='100%' cellpadding='0' cellspacing='0'>\n"
					+ "<tbody>\n" + "<tr>\n" + "<td style='padding-top: 10px; line-height: 0px;'></td>\n" + "</tr>\n"
					+ "</tbody>\n" + "</table>\n" + "<table width='100%' cellpadding='0' cellspacing='0'>\n"
					+ "<tbody>\n" + "<tr>\n"
					+ "<td align='center' valign='top' style='color: #ffffff; font-family: oPen sans; font-size: 20px; mso-line-height-rule: exactly; line-height: 32px; font-weight: 500;'>\n"
					+ "Share your experience\n" + "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td style='padding-top: 15px; line-height: 0px;'></td>\n" + "</tr>\n" + "</tbody>\n"
					+ "</table>\n" + "<table cellpadding='0' cellspacing='0' border='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td width='100%' height='40' align='center' valign='middle' style='border-radius:7px; color:#fff;font-family:oPen sans;font-size:15px;mso-line-height-rule:exactly;line-height:16px;font-weight:700;display:block;background:#232527;padding: 0 15px;'>\n"
					+ "<span style='color:#fff'>\n"
					+ "<a style='display:block;height:40px;width:100%;text-decoration:none;font-family:color:#fff;font-family:oPen sans;font-size:15px;mso-line-height-rule:exactly;line-height:16px;font-weight:700;color:#fff' href='"
					+ com.mss.mirage.util.Properties.getProperty("WEBSITE.URL") + "/survey/survey-form.php?surveyId="
					+ suvyId + "' target='_blank'>\n" + "<span style='line-height:40px'>\n" + "Submit Your Feedback\n"
					+ "</span>\n" + "</a>\n" + "</span>\n" + "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n" + "\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0' border='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td style='padding-top:20px;line-height:0'></td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n"
					+ "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n" + "</td>\n" + "</tr>\n" + "</tbody>\n"
					+ "</table>\n" + "<table width='100%' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td align='center' valign='top' style='line-height: 0px !important;'>\n"
					+ "<img src='https://www.miraclesoft.com/images/newsletters/2016/September/corner.png' style='opacity:0.4;' alt='' width='44'>\n"
					+ "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td style='padding-top: 30px; line-height: 0px;'></td>\n" + "</tr>\n" + "</tbody>\n"
					+ "</table>\n" + "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n" + "</td>\n" + "</tr>\n"
					+ "</tbody>\n" + "</table>\n" + "\n" + "<table width='100%' cellpadding='0' cellspacing='0'>\n"
					+ "<tbody>\n" + "<tr>\n" + "<td height='30px'></td>\n" + "</tr>\n" + "<tr>\n"
					+ "<td align='center' valign='top'>\n" + "<table width='100%' cellpadding='0' cellspacing='0'>\n"
					+ "<tbody>\n" + "<tr>\n" + "<td align='center' valign='top' style='padding: 0 20px;'>\n"
					+ "<table cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td width='900' align='center' valign='top'>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td align='center' valign='top'>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0' style='background: #ffffff;'>\n"
					+ "<tbody>\n" + "<tr>\n" + "<td align='center' valign='top' style='padding: 0 18px;'>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td style='padding-top: 20px; line-height: 0px;'></td>\n" + "</tr>\n" + "</tbody>\n"
					+ "</table>\n" + "<table width='100%' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td align='center' valign='top' style='padding: 0 12px;'>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td style='padding-top: 10px; line-height: 0px;'></td>\n" + "</tr>\n" + "</tbody>\n"
					+ "</table>\n" + "<table width='100%' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td align='justify' valign='top' style='color: #8c8c8c; font-family: oPen sans; font-size: 14px; mso-line-height-rule: exactly; line-height: 30px; font-weight: 400;'>\n"
					+ "<b>Watching your most loved movie is fun, right?</b> It's time to share your experience at watching your favorite movie last weekend. We hope you had a wonderful time at the show with your special people and loved ones. We expect you to submit your <b>feedback</b> within the next 48 hours and share your wonderful experience with us. Then why hold-up! Flip your phone, open gallery and <b>upload your memories</b> that you have shot at the movie time. We would love to add more lovable pictures to our memory collection.\n"
					+ "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td style='padding-top: 10px; line-height: 0px;'></td>\n" + "</tr>\n" + "</tbody>\n"
					+ "</table>\n" + "<table align='left' cellpadding='0' cellspacing='0' border='0'>\n" + "<tbody>\n"
					+ "<tr>\n"
					+ "<td width='100%' height='40' align='center' valign='middle' style='border-radius:7px; color:#fff;font-family:oPen sans;font-size:15px;mso-line-height-rule:exactly;line-height:16px;font-weight:700;display:block;background:#232527;padding: 0 15px;'>\n"
					+ "<span style='color:#fff'>\n"
					+ "<a style='display:block;height:40px;width:100%;text-decoration:none;font-family:color:#fff;font-family:oPen sans;font-size:15px;mso-line-height-rule:exactly;line-height:16px;font-weight:700;color:#fff' href='"
					+ com.mss.mirage.util.Properties.getProperty("WEBSITE.URL") + "/survey/survey-form.php?surveyId="
					+ suvyId + "' target='_blank'>\n" + "<span style='line-height:40px'>\n" + "Submit Your Feedback\n"
					+ "</span>\n" + "</a>\n" + "</span>\n" + "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0' border='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td style='padding-top:10px;line-height:0'></td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n"
					+ "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td style='padding-top: 20px; line-height: 0px;'></td>\n" + "</tr>\n" + "</tbody>\n"
					+ "</table>\n" + "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n" + "</td>\n" + "</tr>\n"
					+ "</tbody>\n" + "</table>\n" + "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n" + "</td>\n"
					+ "</tr>\n" + "</tbody>\n" + "</table>\n" + "</td>\n" + "</tr>\n" + "<tr>\n"
					+ "<td height='30px'></td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n" + "\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td align='center' valign='top'>\n" + "<table cellpadding='0' cellspacing='0'>\n" + "<tbody>\n"
					+ "<tr>\n" + "<td width='1000' align='center' valign='top'>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td align='center' valign='top' style='padding: 0 10px;'></td>\n" + "</tr>\n" + "</tbody>\n"
					+ "</table>\n" + "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n" + "</td>\n" + "</tr>\n"
					+ "</tbody>\n" + "</table>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0' border='0' style='background: #2e3538;'>\n"
					+ "<tbody>\n" + "<tr>\n"
					+ "<td data-bg='Top Banner' data-thumb='https://www.miraclesoft.com/images/newsletters/2016/december/social_banner.png' data-module='Top Banner' mc:repeatable='layout' mc:hideable='' mc:variant='Top Banner' style='background-color: rgb(255, 255, 255); background-repeat: repeat; background-attachment: scroll; background-position: center top; background-clip: border-box; background-origin: padding-box; background-size: cover;' align='center' background='https://www.miraclesoft.com/images/newsletters/2016/december/social_banner.png' bgcolor='e35500' border='0' cellpadding='0' cellspacing='0' width='100%' class=''>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0' border='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td align='center' valign='top' style='padding: 0 20px;'>\n"
					+ "<table cellpadding='0' cellspacing='0' border='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td width='900' align='center' valign='top'>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0' border='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td style='padding-top: 30px; line-height: 0px;'></td>\n" + "</tr>\n" + "</tbody>\n"
					+ "</table>\n" + "<table width='100%' cellpadding='0' cellspacing='0' border='0'>\n" + "<tbody>\n"
					+ "<tr>\n"
					+ "<td align='center' valign='top' style='color: #ffffff; font-family: oPen sans; font-size: 28px; mso-line-height-rule: exactly; line-height: 30px; font-weight: 700; text-transform: normal;'>\n"
					+ "Follow Us\n" + "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0' border='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td style='padding-top: 18px; line-height: 0px;'></td>\n" + "</tr>\n" + "</tbody>\n"
					+ "</table>\n" + "<table cellpadding='0' cellspacing='0' border='0' style='background: #ffffff;'>\n"
					+ "<tbody>\n" + "<tr>\n" + "<td width='50' align='center' valign='top'>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0' border='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td style='padding-top: 2px; line-height: 0px;'></td>\n" + "</tr>\n" + "</tbody>\n"
					+ "</table>\n" + "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n" + "</td>\n" + "</tr>\n"
					+ "</tbody>\n" + "</table>\n" + "</td>\n" + "</tr>\n" + "<tr>\n"
					+ "<td align='center' valign='top'>\n" + "<table cellpadding='0' cellspacing='0' border='0'>\n"
					+ "<tbody>\n" + "<tr>\n" + "<td width='900' align='center' valign='top'>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0' border='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td align='center' valign='top' style='padding: 0 10px;'>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0' border='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td style='padding-top: 10px; line-height: 0px;'></td>\n" + "</tr>\n" + "</tbody>\n"
					+ "</table>\n" + "<table width='100%' align='center' cellpadding='0' cellspacing='0' border='0'>\n"
					+ "<tbody>\n" + "<tr>\n" + "<td height='10' style='height:10px;line-height:10px'></td>\n"
					+ "</tr>\n" + "<tr>\n" + "<td align='center' valign='top'>\n"
					+ "<table align='center' cellpadding='0' cellspacing='0' border='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td width='600' align='center' valign='top' style='font-size:0!important'>\n"
					+ "<div style='width:100%;display:inline-block;vertical-align:top;font-size:normal;max-width:60px'>\n"
					+ "<table width='100%' align='center' cellpadding='0' cellspacing='0' border='0'>\n" + "<tbody>\n"
					+ "<tr>\n" + "<td align='center' valign='top' style='padding:0 10px'>\n"
					+ "<table width='100%' align='center' cellpadding='0' cellspacing='0' border='0'>\n" + "<tbody>\n"
					+ "<tr>\n" + "<td height='10' style='height:10px;line-height:10px'></td>\n" + "</tr>\n" + "<tr>\n"
					+ "<td align='center' valign='top' style='line-height:0!important'>\n"
					+ "<a style='text-decoration:none;display:inline-block' href='https://facebook.com/miracle45625' target='blank'>\n"
					+ "<img src='https://www.miraclesoft.com/images/newsletters/2016/September/facebook-1.png' alt='socials1' width='40'>\n"
					+ "</a>\n" + "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n" + "</td>\n" + "</tr>\n"
					+ "</tbody>\n" + "</table>\n" + "</div>\n"
					+ "<div style='width:100%;display:inline-block;vertical-align:top;font-size:normal;max-width:60px'>\n"
					+ "<table width='100%' align='center' cellpadding='0' cellspacing='0' border='0'>\n" + "<tbody>\n"
					+ "<tr>\n" + "<td align='center' valign='top' style='padding:0 10px'>\n"
					+ "<table width='100%' align='center' cellpadding='0' cellspacing='0' border='0'>\n" + "<tbody>\n"
					+ "<tr>\n" + "<td height='10' style='height:10px;line-height:10px'></td>\n" + "</tr>\n" + "<tr>\n"
					+ "<td align='center' valign='top' style='line-height:0!important'>\n"
					+ "<a style='text-decoration:none;display:inline-block' href='https://plus.google.com/+Team_MSS' target='blank'>\n"
					+ "<img src='https://www.miraclesoft.com/images/newsletters/2016/September/google-plus-2.png' alt='socials1' width='40'>\n"
					+ "</a>\n" + "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n" + "</td>\n" + "</tr>\n"
					+ "</tbody>\n" + "</table>\n" + "</div>\n"
					+ "<div style='width:100%;display:inline-block;vertical-align:top;font-size:normal;max-width:60px'>\n"
					+ "<table width='100%' align='center' cellpadding='0' cellspacing='0' border='0'>\n" + "<tbody>\n"
					+ "<tr>\n" + "<td align='center' valign='top' style='padding:0 10px'>\n"
					+ "<table width='100%' align='center' cellpadding='0' cellspacing='0' border='0'>\n" + "<tbody>\n"
					+ "<tr>\n" + "<td height='10' style='height:10px;line-height:10px'></td>\n" + "</tr>\n" + "<tr>\n"
					+ "<td align='center' valign='top' style='line-height:0!important'>\n"
					+ "<a style='text-decoration:none;display:inline-block' href='https://www.linkedin.com/company/miracle-software-systems-inc' target='blank'>\n"
					+ "<img src='https://www.miraclesoft.com/images/newsletters/2016/September/linkedin-2.png' alt='socials1' width='40'>\n"
					+ "</a>\n" + "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n" + "</td>\n" + "</tr>\n"
					+ "</tbody>\n" + "</table>\n" + "</div>\n"
					+ "<div style='width:100%;display:inline-block;vertical-align:top;font-size:normal;max-width:60px'>\n"
					+ "<table width='100%' align='center' cellpadding='0' cellspacing='0' border='0'>\n" + "<tbody>\n"
					+ "<tr>\n" + "<td align='center' valign='top' style='padding:0 10px'>\n"
					+ "<table width='100%' align='center' cellpadding='0' cellspacing='0' border='0'>\n" + "<tbody>\n"
					+ "<tr>\n" + "<td height='10' style='height:10px;line-height:10px'></td>\n" + "</tr>\n" + "<tr>\n"
					+ "<td align='center' valign='top' style='line-height:0!important'>\n"
					+ "<a style='text-decoration:none;display:inline-block' href='https://www.flickr.com/photos/team_miracle' target='blank'>\n"
					+ "<img src='https://www.miraclesoft.com/images/newsletters/2016/September/flickr-1.png' alt='socials1' width='40'>\n"
					+ "</a>\n" + "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n" + "</td>\n" + "</tr>\n"
					+ "</tbody>\n" + "</table>\n" + "</div>\n"
					+ "<div style='width:100%;display:inline-block;vertical-align:top;font-size:normal;max-width:60px'>\n"
					+ "<table width='100%' align='center' cellpadding='0' cellspacing='0' border='0'>\n" + "<tbody>\n"
					+ "<tr>\n" + "<td align='center' valign='top' style='padding:0 10px'>\n"
					+ "<table width='100%' align='center' cellpadding='0' cellspacing='0' border='0'>\n" + "<tbody>\n"
					+ "<tr>\n" + "<td height='10' style='height:10px;line-height:10px'></td>\n" + "</tr>\n" + "<tr>\n"
					+ "<td align='center' valign='top' style='line-height:0!important'>\n"
					+ "<a style='text-decoration:none;display:inline-block' href='https://twitter.com/Team_MSS' target='blank'>\n"
					+ "<img src='https://www.miraclesoft.com/images/newsletters/2016/September/twitter-2.png' alt='socials2' width='40'>\n"
					+ "</a>\n" + "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n" + "</td>\n" + "</tr>\n"
					+ "</tbody>\n" + "</table>\n" + "</div>\n"
					+ "<div style='width:100%;display:inline-block;vertical-align:top;font-size:normal;max-width:60px'>\n"
					+ "<table width='100%' align='center' cellpadding='0' cellspacing='0' border='0'>\n" + "<tbody>\n"
					+ "<tr>\n" + "<td align='center' valign='top' style='padding:0 10px'>\n"
					+ "<table width='100%' align='center' cellpadding='0' cellspacing='0' border='0'>\n" + "<tbody>\n"
					+ "<tr>\n" + "<td height='10' style='height:10px;line-height:10px'></td>\n" + "</tr>\n" + "<tr>\n"
					+ "<td align='center' valign='top' style='line-height:0!important'>\n"
					+ "<a style='text-decoration:none;display:inline-block' href='https://www.youtube.com/c/Team_MSS' target='blank'>\n"
					+ "<img src='https://www.miraclesoft.com/images/newsletters/2016/September/youtube-3.png' alt='socials3' width='40'>\n"
					+ "</a>\n" + "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n" + "</td>\n" + "</tr>\n"
					+ "</tbody>\n" + "</table>\n" + "</div>\n" + "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n"
					+ "</td>\n" + "</tr>\n" + "<tr>\n" + "<td height='10' style='height:10px;line-height:15px'></td>\n"
					+ "</tr>\n" + "</tbody>\n" + "</table>\n" + "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n"
					+ "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n" + "</td>\n" + "</tr>\n" + "<tr>\n"
					+ "<td align='center' valign='top'>\n" + "<table cellpadding='0' cellspacing='0' border='0'>\n"
					+ "<tbody>\n" + "<tr>\n" + "<td width='900' align='center' valign='top'>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0' border='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td align='center' valign='top' style='padding: 0 10px;'>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0' border='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td style='padding-top: 10px; line-height: 0px;'></td>\n" + "</tr>\n" + "</tbody>\n"
					+ "</table>\n" + "<table width='100%' align='center' cellpadding='0' cellspacing='0' border='0'>\n"
					+ "<tbody>\n" + "<tr>\n"
					+ "<td align='center' valign='top' style='color:#ffffff;font-family:Open sans;font-size:14px;mso-line-height-rule:exactly;line-height:23px;font-weight:400'>\n"
					+ "&#169; Copyrights " + Calendar.getInstance().get(Calendar.YEAR) + " | Miracle Software Systems\n"
					+ "</td>\n" + "</tr>\n" + "<tr>\n" + "<td height='25' style='height:25px;line-height:25px'></td>\n"
					+ "</tr>\n" + "\n" + "</tbody>\n" + "</table>\n" + "</td>\n" + "</tr>\n" + "</tbody>\n"
					+ "</table>\n" + "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n" + "</td>\n" + "</tr>\n" + "\n"
					+ "</tbody>\n" + "</table>\n" + "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n" + "</td>\n"
					+ "</tr>\n" + "</tbody>\n" + "</table>\n" + "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n"
					+ "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n" + "\n" + "</body></html>";
			// System.out.println("ajaxHandlerAction.getFileLocation()"+strContent.toString());

			// String campaignTemplate=strContent.toString().replaceAll("\"",
			// "'");

			String campaignTemplate = htmlText.toString();
			// System.out.println("campaignTemplate"+campaignTemplate);
			///////////////////////////////////////////////////////////////////////////////////////////////

			EmailCampaignService ecs = new EmailCampaignService(Properties.getProperty("API_KEY"),
					Properties.getProperty("ACCESS_TOCKEN"));

			EmailCampaignRequest emailReq = new EmailCampaignRequest();
			String datetime = new SimpleDateFormat("ddMMMyyyy_hh_mm_ss_SSSa").format(new java.util.Date());

			emailReq.setName("Star Performer FeedBack Form " + datetime);
			emailReq.setSubject("Star Performer FeedBack Form " + shortMonth + " " + fullYear);
			emailReq.setFromEmail("newsletter@miraclesoft.com ");
			emailReq.setFromName("Miracle Software Systems, Inc.");
			emailReq.setReplyToEmail("newsletter@miraclesoft.com ");

			// emailReq.setPermissionReminderEnabled(true);
			// emailReq.setPermissionReminderText("");
			emailReq.setViewAsWebpageEnabled(false);

			emailReq.setGreetingName("FIRST_NAME");
			emailReq.setGreetingSalutations("Hello");
			emailReq.setGreetingString("Dear");
			emailReq.setEmailContent(campaignTemplate);
			emailReq.setEmailContentFormat("HTML");
			// emailReq.setTemplateType("CUSTOM");
			emailReq.setTextContent("Star Performer FeedBack Form");

			MessageFooter messageFooter = new MessageFooter();
			messageFooter.setAddressLine1("45625 Grand River Avenue");
			messageFooter.setOrganizationName("Miracle Software Systems, Inc.");
			messageFooter.setCity("Novi");
			messageFooter.setCountry("US");
			messageFooter.setState("MI");
			messageFooter.setPostalCode("48374");
			emailReq.setMessageFooter(messageFooter);
			List<SentToContactList> scl = new ArrayList();

			SentToContactList stcl = new SentToContactList();

			stcl.setId(contactId);

			scl.add(stcl);
			emailReq.setSentToContactLists(scl);

			// System.out.println(emailReq.toJSON().toString());
			EmailCampaignResponse errs = ecs.addCampaign(emailReq);

			// System.out.println(errs.getSentToContactLists());

			if (errs.getId() != null && !"".equalsIgnoreCase(errs.getId().trim())) {

				EmailCampaignScheduleService ecsh = new EmailCampaignScheduleService(Properties.getProperty("API_KEY"),
						Properties.getProperty("ACCESS_TOCKEN"));

				EmailCampaignResponse ecrsp = ecs.getCampaign(errs.getId());

				EmailCampaignSchedule Ecs = new EmailCampaignSchedule();

				// String campgnStartDate=
				// com.mss.mirage.util.DateUtility.getInstance().convertFromElasticToGraphView(
				// com.mss.mirage.util.DateUtility.getInstance().getCurrentDateformate());
				// Ecs.setScheduledDate(campgnStartDate);

				EmailCampaignSchedule finalEcs = null;
				int size = 0;
				while (size <= i) {
					com.constantcontact.components.generic.response.ResultSet<Contact> c = cls
							.getContactsFromList(contactId, 500, null);

					size = c.size();

					if (size == i) {
						finalEcs = ecsh.addSchedule(ecrsp.getId(), Ecs);
						break;
					}

				}

				if (finalEcs != null) {

					String query1 = "Update tblStarPerformers Set Status='7' WHERE Id=" + objectId + "";
					statement = connection.createStatement();
					rows = statement.executeUpdate(query1);

					rows = 1;

				}

			}

		} catch (SQLException ex) {
			Logger.getLogger(NewAjaxHandlerServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
		} catch (ConstantContactServiceException e) {
			ConstantContactServiceException c = (ConstantContactServiceException) e.getCause();
			for (Iterator<RawApiRequestError> i1 = c.getErrorInfo().iterator(); i1.hasNext();) {
				RawApiRequestError item = i1.next();
				System.out.println(item.getErrorKey() + " : " + item.getErrorMessage());
			}
			e.printStackTrace();
			System.out.println(e.getMessage());
		}

		finally {
			try {
				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException sqle) {
			}
		}
		// System.err.println("response string is"+stringBuffer.toString());
		return rows;
	}

	/*
	 * project risk reports dashboard added by triveni on june 28th 2017
	 * 
	 */
	public String getProjectRiskDetails(NewAjaxHandlerAction ajaxHandlerAction) throws ServiceLocatorException {

		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		String queryString = "";
		String responseString = "";
		int sno = 0;
		String customerName = "";
		String projectName = "";
		String description = "";
		String assignedTo = "";
		String riskImpact = "";
		String dateClosed = "";
		String status = "";
		String resolution = "";
		// StringBuffer queryStringBuffer = new StringBuffer("");

		try {
			// System.out.println(ajaxHandlerAction.getStatus());
			// System.out.println(ajaxHandlerAction.getCustomerName());
			connection = ConnectionProvider.getInstance().getConnection();
			queryString = "SELECT tblCrmAccount.NAME AS CustomerName,tblProjects.ProjectName AS ProjectName,tblProjectRisks.Description AS Description,AssignedTo,RiskImpact,DateClosed,tblProjectRisks.STATUS AS Status,Resolution FROM tblProjectRisks LEFT JOIN tblProjects ON (tblProjects.Id = tblProjectRisks.ProjectId) LEFT JOIN tblCrmAccount ON (tblCrmAccount.Id = tblProjects.CustomerId) WHERE 1=1";
			// preparedStatement = connection.prepareStatement("select ID,
			// JobTitle,DatePosted,ClosedDate,TargetRate from tblRecRequirement
			// where STATUS='closed' AND ClosedDate LIKE'"+year+"-"+month+"%'
			// ORDER BY DatePosted DESC");
			if (!"".equalsIgnoreCase(ajaxHandlerAction.getCustomerName())
					&& !"-1".equalsIgnoreCase(ajaxHandlerAction.getCustomerName())) {
				queryString = queryString + " AND tblCrmAccount.Id = " + ajaxHandlerAction.getCustomerName() + " ";
			}

			if (!"".equalsIgnoreCase(ajaxHandlerAction.getStatus())) {
				queryString = queryString + " AND tblProjectRisks.STATUS = '" + ajaxHandlerAction.getStatus() + "' ";
				// query = query + " AND tblProjects.STATUS like '%" +
				// ajaxHandlerAction.getStatus() + "%'";
			}
			if (!"".equalsIgnoreCase(ajaxHandlerAction.getImpact())) {
				queryString = queryString + " AND tblProjectRisks.RiskImpact = '" + ajaxHandlerAction.getImpact()
						+ "' ";
				// query = query + " AND tblProjects.STATUS like '%" +
				// ajaxHandlerAction.getStatus() + "%'";
			}
			// System.out.print("queryString---->"+queryString);
			preparedStatement = connection.prepareStatement(queryString);

			// preparedStatement.setString(1, (String)sourcingList.get(i));
			// preparedStatement.setString(1, practice);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				sno++;
				if (!"".equalsIgnoreCase(resultSet.getString("CustomerName"))
						&& !"-1".equalsIgnoreCase(resultSet.getString("CustomerName"))) {
					customerName = resultSet.getString("CustomerName");
				} else {
					customerName = "-";
				}
				if (!"".equalsIgnoreCase(resultSet.getString("ProjectName"))
						&& resultSet.getString("ProjectName") != null) {
					projectName = resultSet.getString("ProjectName");
				} else {
					projectName = "-";
				}
				if (!"".equalsIgnoreCase(resultSet.getString("Description"))
						&& resultSet.getString("Description") != null) {
					description = resultSet.getString("Description");
				} else {
					description = "-";
				}
				if (!"".equalsIgnoreCase(resultSet.getString("AssignedTo"))
						&& !"-1".equalsIgnoreCase(resultSet.getString("AssignedTo"))
						&& resultSet.getString("AssignedTo") != null) {
					assignedTo = resultSet.getString("AssignedTo");
				} else {
					assignedTo = "-";
				}
				if (!"".equalsIgnoreCase(resultSet.getString("RiskImpact"))
						&& resultSet.getString("RiskImpact") != null) {
					riskImpact = resultSet.getString("RiskImpact");
				} else {
					riskImpact = "-";
				}
				if (!"".equalsIgnoreCase(resultSet.getString("DateClosed"))
						&& resultSet.getString("DateClosed") != null) {
					dateClosed = resultSet.getString("DateClosed");
				} else {
					dateClosed = "-";
				}
				if (!"".equalsIgnoreCase(resultSet.getString("Status")) && resultSet.getString("Status") != null) {
					status = resultSet.getString("Status");
				} else {
					status = "-";
				}
				if (!"".equalsIgnoreCase(resultSet.getString("Resolution"))
						&& resultSet.getString("Resolution") != null) {
					resolution = resultSet.getString("Resolution");
				} else {
					resolution = "-";
				}
				responseString = responseString + sno + "#^$" + customerName + "#^$" + projectName + "#^$" + description
						+ "#^$" + assignedTo + "#^$" + riskImpact + "#^$" + dateClosed + "#^$" + status + "#^$"
						+ resolution + "*@!";
			}
			// System.out.print("responseString --------->"+responseString);
		} catch (Exception exception) {
			exception.printStackTrace();
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException sqle) {
			}
		}
		// System.out.println(" responseString -->"+responseString);

		// System.out.println("String length-->"+responseString.length());
		return responseString;
	}

	public String getEmployeePerformance(NewAjaxHandlerAction ajaxHandlerAction) throws ServiceLocatorException {
		Connection connection = null;
		CallableStatement callableStatement = null;
		String responseString = null;
		try {
			int empid = Integer.parseInt(ajaxHandlerAction.getEmpId());
			// String
			// empLoginId=DataSourceDataProvider.getInstance().getLoginIdByEmpId(empid);
			connection = ConnectionProvider.getInstance().getConnection();
			callableStatement = connection.prepareCall("{call  spGetEmpCurperformance(?,?,?,?,?,?)}");
			callableStatement.setString(1, ajaxHandlerAction.getEmpId());
			callableStatement.setString(2, ajaxHandlerAction.getLoginId());
			callableStatement.setString(3, ajaxHandlerAction.getCurrId());
			callableStatement.setString(4, ajaxHandlerAction.getStateStartDate());
			callableStatement.setString(5, ajaxHandlerAction.getStateEndDate());
			callableStatement.executeQuery();
			callableStatement.registerOutParameter(6, java.sql.Types.VARCHAR);
			responseString = callableStatement.getString(6);
		} catch (SQLException sqle) {
			throw new ServiceLocatorException(sqle);
		} finally {

			try {

				if (callableStatement != null) {
					callableStatement.close();
					callableStatement = null;
				}

				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException sql) {
				// System.err.print("Error :"+sql);
			}

		}
		// System.out.println("---------->" + resultString1);
		return responseString;
	}

	public String getOnProjectEmployeeUtilizationDetails(String empId) throws ServiceLocatorException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		String responseString = "";

		String queryString = "SELECT PrjName,Utilization,IsBillable,StartDate,EndDate  FROM tblEmpStateHistory1 WHERE EmpID='"
				+ empId
				+ "' AND ProjectStatus='Active' AND State='OnProject' ORDER BY IsBillable DESC , Utilization DESC  ";

		try {
			connection = ConnectionProvider.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(queryString);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				if (resultSet.getString("PrjName") != null && !"".equals(resultSet.getString("PrjName"))) {
					responseString = responseString + resultSet.getString("PrjName") + "#^$";
				} else {
					responseString = responseString + "-" + "#^$";
				}
				if (resultSet.getString("StartDate") != null && !"".equals(resultSet.getString("StartDate"))) {
					responseString = responseString + resultSet.getString("StartDate") + "#^$";
				} else {
					responseString = responseString + "-" + "#^$";
				}
				if (resultSet.getString("EndDate") != null && !"".equals(resultSet.getString("EndDate"))) {
					responseString = responseString + resultSet.getString("EndDate") + "#^$";
				} else {
					responseString = responseString + "-" + "#^$";
				}

				if (resultSet.getString("Utilization") != null && !"".equals(resultSet.getString("Utilization"))) {
					responseString = responseString + resultSet.getString("Utilization") + "#^$";
				} else {
					responseString = responseString + "-" + "#^$";
				}
				if (resultSet.getString("IsBillable").equals("1")) {
					responseString = responseString + "Yes" + "*@!";
				} else {
					responseString = responseString + "No" + "*@!";
				}
				// responseString=responseString +
				// resultSet.getString("PrjName") + "#^$" +
				// resultSet.getString("StartDate") + "#^$" +
				// resultSet.getString("EndDate") + "#^$"+
				// resultSet.getString("Utilization") + "#^$" +
				// resultSet.getString("IsBillable") + "*@!" ;
			}

		} catch (SQLException se) {
			se.printStackTrace();
			throw new ServiceLocatorException(se);
		} finally {
			try {
				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
			} catch (SQLException se) {
				throw new ServiceLocatorException(se);
			}
		}

		return responseString;

	}

	public String getReasonForExit(String empId) throws ServiceLocatorException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		String responseString = "";

		String queryString = "SELECT ExitType,Termination FROM tblEmployee WHERE Id=?";

		try {
			connection = ConnectionProvider.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(queryString);
			preparedStatement.setInt(1, Integer.parseInt(empId));
			resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {

				if (resultSet.getString("ExitType") != null && !"".equals(resultSet.getString("ExitType"))) {
					responseString = responseString + resultSet.getString("ExitType") + "#^$";
				} else {
					responseString = responseString + "-" + "#^$";
				}

				if (resultSet.getString("Termination") != null && !"".equals(resultSet.getString("Termination"))) {
					responseString = responseString + resultSet.getString("Termination");
				} else {
					responseString = responseString + "-";
				}

			}

		} catch (SQLException se) {
			se.printStackTrace();
			throw new ServiceLocatorException(se);
		} finally {
			try {
				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
			} catch (SQLException se) {
				throw new ServiceLocatorException(se);
			}
		}

		return responseString;

	}

	/*
	 * Nagalakshmi Telluri 5/17/2017
	 */

	public String getTimeSheettotalhoursbyProjectList(NewAjaxHandlerAction ajaxhandleraction)
			throws ServiceLocatorException {
		Connection connection = null;
		CallableStatement callableStatement = null;
		String responseString = "";

		String queryString = "{call spGetTimeSheetWeeklyHoursByProject(?,?,?,?,?,?)}";

		try {
			connection = ConnectionProvider.getInstance().getConnection();
			callableStatement = connection.prepareCall(queryString);
			callableStatement.setString(1, ajaxhandleraction.getYear());

			// System.out.println(" the year is" +ajaxhandleraction.getYear());

			callableStatement.setString(2, ajaxhandleraction.getMonth());
			// System.out.println(" the month is"
			// +ajaxhandleraction.getMonth());

			callableStatement.setString(3, ajaxhandleraction.getCustomerName());

			callableStatement.setString(4, ajaxhandleraction.getPracticeId());
			// System.out.println(" practice name is"
			// +ajaxhandleraction.getPracticeId());

			callableStatement.setString(5, ajaxhandleraction.getCostModel());
			// System.out.println(" cost model is"
			// +ajaxhandleraction.getCostModel());

			callableStatement.registerOutParameter(6, java.sql.Types.VARCHAR);

			// System.out.println("response String is "+responseString);
			callableStatement.executeQuery();
			responseString = callableStatement.getString(6);

		} catch (SQLException se) {
			se.printStackTrace();

			throw new ServiceLocatorException(se);
		} finally {
			try {

				if (callableStatement != null) {
					callableStatement.close();
					callableStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException se) {
				se.printStackTrace();
				throw new ServiceLocatorException(se);
			}
		}
		return responseString;
	}

	public int doRSVPIntegration(String objectId, String year, String month, int suvyId, String movieDates,
			String expireDate) throws ServiceLocatorException {
		int rows = 0;
		PreparedStatement preparedStatement = null;
		Statement statement = null;
		ResultSet resultSet = null;
		Connection connection = null;
		try {

			String contactId = "";

			ContactListService cls = new ContactListService(Properties.getProperty("API_KEY"),
					Properties.getProperty("ACCESS_TOCKEN"));

			List<ContactList> clst = cls.getLists(null);

			for (int j = 0; j < clst.size(); j++) {

				if ("".equals(contactId)) {
					if (clst.get(j).getName().equals("SP_RSVP_List_" + month + year)) {
						contactId = clst.get(j).getId();
					}
				}
			}
			if (!"".equals(contactId.toString().trim())) {
				cls.deleteList(contactId);
				contactId = "";
			}
			if ("".equals(contactId.toString().trim())) {
				ContactList contList = new ContactList();
				contList.setName("SP_RSVP_List_" + month + year);
				contList.setStatus("ACTIVE");
				contList = cls.addList(contList);
				contactId = contList.getId();

			}

			ConstantContactFactory cf = new ConstantContactFactory(Properties.getProperty("API_KEY"),
					Properties.getProperty("ACCESS_TOCKEN"));
			IBulkActivitiesService _activityService = cf.createBulkActivitiesService();

			AddContactsRequest contactsToAdd = new AddContactsRequest();
			List<String> contactLists = new ArrayList<String>();
			List<String> columns = new ArrayList<String>();
			List<ContactData> importData = new ArrayList<ContactData>();

			contactLists.add(contactId); // NOTE: Replace this with the IF for
											// the list you wish to use
			columns.add("EMAIL ADDRESS");
			columns.add("FIRST NAME");
			columns.add("LAST NAME");
			columns.add("Company Name");
			contactsToAdd.setColumnNames(columns);
			contactsToAdd.setLists(contactLists);

			connection = ConnectionProvider.getInstance().getConnection();

			String query = "SELECT (YEAR(DATE)-2000) AS yr,YEAR(DATE) AS fullYear,MONTHNAME(STR_TO_DATE(MONTH(DATE), '%m')) AS mnth,DATE_FORMAT(DATE, '%b') AS shortMonth,tblStarPerformerLines.Id,tblStarPerformers.Id As spId,tblStarPerformerLines.ObjectId,tblStarPerformerLines.EmpId,tblStarPerformerLines.EmpName AS EmployeeName,tblStarPerformerLines.Title AS Title,tblStarPerformerLines.CreatedBy,tblStarPerformerLines.Comments,DATE,tblStarPerformerLines.FilePath As FilePath,tblStarPerformerLines.Department As Department,tblStarPerformerLines.EmpId,tblEmployee.Email1,tblEmployee.FName,tblEmployee.LName,tblEmployee.OrgId FROM tblStarPerformerLines LEFT OUTER JOIN tblStarPerformers ON(tblStarPerformers.Id=tblStarPerformerLines.ObjectId) left join tblEmployee ON(tblStarPerformerLines.EmpId=tblEmployee.Id)  WHERE  tblStarPerformerLines.Status='Approve' AND tblStarPerformerLines.ObjectId="
					+ objectId + "";

			preparedStatement = connection.prepareStatement(query);

			resultSet = preparedStatement.executeQuery();

			String fullYear = "";
			String shortMonth = "";

			String Title = null;
			String FilePath = null;
			String Department = null;
			String EmployeeName = null;

			int i = 0;
			while (resultSet.next()) {

				if ("".equals(fullYear)) {
					fullYear = resultSet.getString("fullYear");
				}
				if ("".equals(shortMonth)) {

					shortMonth = resultSet.getString("mnth");
					// shortMonth=resultSet.getString("shortMonth");
				}

				ContactData newContact = new ContactData();
				List<String> emails = new ArrayList<String>();
				emails.add(resultSet.getString("Email1"));
				newContact.setEmailAddresses(emails);
				newContact.setFirstName(resultSet.getString("FName"));
				newContact.setLastName(resultSet.getString("LName"));
				newContact.setCompanyName(resultSet.getString("OrgId"));
				importData.add(newContact);

				FilePath = resultSet.getString("FilePath");
				EmployeeName = resultSet.getString("EmployeeName");
				Department = resultSet.getString("Department");
				Title = resultSet.getString("Title");
				i++;
			}

			contactsToAdd.setImportData(importData);

			_activityService.addContacts(contactsToAdd);

			///////////////////////////////////////////////////////////////////////////////////////////////

			String htmlText = "<html xmlns='http://www.w3.org/1999/xhtml'><head>\n"
					+ "<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>\n"
					+ "<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0;' name='viewport'>\n"
					+ "<link rel='stylesheet' type='text/css' href='//fonts.googleapis.com/css?family=Open+Sans'>\n"
					+ "<title>Star Performers - January '17</title>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>#outlook a{padding:0}.ReadMsgBody,.ExternalClass{width:100%}.ExternalClass,.ExternalClass p,.ExternalClass span,.ExternalClass font,.ExternalClass td,.ExternalClass div{line-height:100%}body,table,td,span,a{-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;-webkit-font-smoothing:antialiased}table,td{mso-table-lspace:0;mso-table-rspace:0}img{-ms-interpolation-mode:bicubic}html,body,#wrappertable{width:100%!important;margin:0!important;padding:0!important;line-height:100%!important}img{display:block;border:0;height:auto;outline:none!important;line-height:100%!important;text-decoration:none!important}table{border-spacing:0!important;border-collapse:collapse!important}table td{border-collapse:collapse!important}a,a:link,a:visited,a:focus,a:hover{color:inherit!important;outline:none!important;text-decoration:none!important}</style>\n"
					+ "<style type='text/css'>@media only screen and (max-width:568px){div[class='block']{width:100%!important;max-width:none!important}table[class='full-width']{width:100%!important}table[class='center'],td[class='center']{text-align:center!important;float:none!important}td[class='smaller']{font-size:65px!important;line-height:65px!important}}@media only screen and (max-width:375px){div[class='block']{width:100%!important;max-width:none!important}td[class='smaller']{font-size:50px!important;line-height:50px!important}}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "<style type='text/css'>.cf-hidden{display:none}.cf-invisible{visibility:hidden}</style>\n"
					+ "</head>\n" + "<body id='body-layout'>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0' id='wrappertable' style='table-layout: fixed;'>\n"
					+ "<tbody>\n" + "<tr>\n" + "<td align='center' valign='top'>\n"
					+ "<table cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td width='1600' align='center' valign='top'>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0' style='background: #eceff3;'>\n"
					+ "<tbody>\n" + "<tr>\n" + "<td align='center' valign='top'>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0' style='background: #2e3538;'>\n"
					+ "<tbody>\n" + "<tr>\n"
					+ "<td style='background-color: rgb(255, 255, 255); background-repeat: repeat; background-attachment: scroll; background-position: center top; background-clip: border-box; background-origin: padding-box; background:url(https://www.miraclesoft.com/images/newsletters/2017/May/rvsp.png);' align='center' bgcolor='e35500' width='100%' class=''>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td align='center' valign='top' style='padding: 0 20px;'>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td style='padding-top: 20px;line-height: 0px;'></td>\n" + "</tr>\n" + "</tbody>\n"
					+ "</table>\n" + "<table cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td width='250' align='center' valign='top' style='line-height: 0px !important;'>\n"
					+ "<a style='text-decoration:none;display:block' href='https://www.miraclesoft.com/' target='_blank'> \n"
					+ "<img src='https://www.miraclesoft.com/images/newsletters/Q2/miracle-logo-light.png' alt='' width='100%' style='width:100%;max-width: 100%;'>\n"
					+ "</a>\n" + "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td style='padding-top: 25px;line-height: 20px;'></td>\n" + "</tr>\n" + "</tbody>\n"
					+ "</table>\n"
					+ "<table cellpadding='0' cellspacing='0' style='background: #ffffff; background: rgba(255,255,255,0.4);'>\n"
					+ "<tbody>\n" + "<tr>\n" + "<td width='900' align='center' valign='top'>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td align='center' valign='top' style='padding: 0;'>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td style='padding-top: 40px; line-height: 0px;'></td>\n" + "</tr>\n" + "</tbody>\n"
					+ "</table>\n" + "<table width='100%' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td align='center' valign='top' style='color: #ffffff; font-family: oPen sans; font-size: 47px; mso-line-height-rule: exactly; line-height: 40px; font-weight: 900; text-transform: normal;'>\n"
					+ "You're A Star!\n" + "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td style='padding-top: 20px; line-height: 0px;'></td>\n" + "</tr>\n" + "</tbody>\n"
					+ "</table>\n" + "<table cellpadding='0' cellspacing='0' style=''>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td width='220' align='center' valign='top'>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td style='padding-top: 2px; line-height: 0px;'>\n" + "<span>\n"
					+ "<img src='https://www.miraclesoft.com/images/newsletters/heading-divider-img.png' alt='heading-divider-img' width='200' height='auto' style='display: inline-block;'>\n"
					+ "</span>\n" + "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n" + "</td>\n" + "</tr>\n"
					+ "</tbody>\n" + "</table>\n" + "<table width='100%' cellpadding='0' cellspacing='0'>\n"
					+ "<tbody>\n" + "<tr>\n" + "<td style='padding-top: 10px; line-height: 0px;'></td>\n" + "</tr>\n"
					+ "</tbody>\n" + "</table>\n" + "<table width='100%' cellpadding='0' cellspacing='0'>\n"
					+ "<tbody>\n" + "<tr>\n"
					+ "<td align='center' valign='top' style='color: #ffffff; font-family: oPen sans; font-size: 20px; mso-line-height-rule: exactly; line-height: 32px; font-weight: 500;'>\n"
					+ "Keep twinkling and secure your shine to remain a star!\n" + "</td>\n" + "</tr>\n" + "</tbody>\n"
					+ "</table>\n" + "<table width='100%' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td style='padding-top: 15px; line-height: 0px;'></td>\n" + "</tr>\n" + "</tbody>\n"
					+ "</table>\n" + "<table cellpadding='0' cellspacing='0' border='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td width='100%' height='40' align='center' valign='middle' style='border-radius:7px; color:#fff;font-family:oPen sans;font-size:15px;mso-line-height-rule:exactly;line-height:18px;font-weight:700;display:block;background:#232527;padding: 0 15px;'>\n"
					+ "<span style='color:#fff'>\n"
					+ "<a style='display:block;height:40px;width:100%;text-decoration:none;font-family:color:#fff;font-family:oPen sans;font-size:15px;mso-line-height-rule:exactly;line-height:18px;font-weight:700;color:#fff' href='"
					+ com.mss.mirage.util.Properties.getProperty("WEBSITE.URL") + "/survey/survey-form.php?surveyId="
					+ suvyId + "' target='_blank'>\n" + "<span style='line-height:40px'>\n" + "RSVP Today\n"
					+ "</span>\n" + "</a>\n" + "</span>\n" + "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0' border='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td style='padding-top:25px;line-height:0'></td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n"
					+ "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n" + "</td>\n" + "</tr>\n" + "</tbody>\n"
					+ "</table>\n" + "<table width='100%' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td align='center' valign='top' style='line-height: 0px !important;'>\n"
					+ "<img src='https://www.miraclesoft.com/images/newsletters/2016/September/corner.png' style='opacity:0.4;' alt='' width='44'>\n"
					+ "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td style='padding-top: 30px; line-height: 0px;'></td>\n" + "</tr>\n" + "</tbody>\n"
					+ "</table>\n" + "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n" + "</td>\n" + "</tr>\n"
					+ "</tbody>\n" + "</table>\n" + "<table width='100%' cellpadding='0' cellspacing='0' border='0'>\n"
					+ "<tbody>\n" + "<tr>\n" + "<td align='center' valign='top'>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0' border='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td align='center' valign='top' style='padding:0 20px'>\n"
					+ "<table cellpadding='0' cellspacing='0' border='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td width='900' align='center' valign='top'>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0' border='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td style='padding-top:30px;line-height:0'></td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0' border='0' style='background:#fff'>\n"
					+ "<tbody>\n" + "<tr>\n" + "<td align='center' valign='top'>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0' border='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td style='padding-top:33px;line-height:0'></td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0' border='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td width='9' align='center' valign='top' style='background:#232527'></td>\n"
					+ "<td align='center' valign='top' style='padding:0 20px'>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0' border='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td style='padding-top:3px;line-height:0'></td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0' border='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td align='left' valign='top' style='color: #2e3538; font-family: oPen sans; font-size: 20px; mso-line-height-rule: exactly; line-height: 36px; font-weight: 700; text-transform: normal;'>\n"
					+ "Dear,\n" + "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0' border='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td style='padding-top:0;line-height:0'></td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n"
					+ "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0' border='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td style='padding-top:2px;line-height:0'></td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n"
					+ "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n" + "</td>\n" + "</tr>\n" + "</tbody>\n"
					+ "</table>\n" + "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n" + "</td>\n" + "</tr>\n"
					+ "</tbody>\n" + "</table>\n" + "<table width='100%' cellpadding='0' cellspacing='0'>\n"
					+ "<tbody>\n" + "<tr>\n" + "<td align='center' valign='top'>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td align='center' valign='top' style='padding: 0 20px;'>\n"
					+ "<table cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td width='900' align='center' valign='top'>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td align='center' valign='top'>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0' style='background: #ffffff;'>\n"
					+ "<tbody>\n" + "<tr>\n" + "<td align='center' valign='top' style='padding: 0 18px;'>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td align='center' valign='top' style='padding: 0 12px;'>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td style='padding-top: 10px; line-height: 0px;'></td>\n" + "</tr>\n" + "</tbody>\n"
					+ "</table>\n" + "<table width='100%' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td align='justify' valign='top' style='color: #8c8c8c; font-family: oPen sans; font-size: 14px; mso-line-height-rule: exactly; line-height: 30px; font-weight: 400;'>\n"
					+ "<b>Hard work pays off and performance always gets rewarded.</b> For the month of " + month
					+ " I would like to appreciate your efforts and make sure that you get the due recognition for your passion, excellence and determination. In the spirit of your efforts for the organization I would like to give you the chance to go to a movie and enjoy your weekend along with your parents, family (or) friends. <b>Miracle will sponsor a fully paid movie for 3 people including yourself!</b> Movie would be of your choice, at your desired place and time on the selected dates <b>";
			String date = "";
			String mvDate[] = movieDates.split("\\,");
			for (int n = 0; n < mvDate.length; n++) {
				if (n == 0)
					date = mvDate[n];
				else {
					if (n == mvDate.length - 1)
						date = date + " or " + mvDate[n];
					else
						date = date + "," + mvDate[n];
				}
			}

			htmlText = htmlText + date + ".</b>\n" + "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td style='padding-top: 10px; line-height: 0px;'></td>\n" + "</tr>\n" + "</tbody>\n"
					+ "</table>\n" + "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td style='padding-top: 20px; line-height: 0px;'></td>\n" + "</tr>\n" + "</tbody>\n"
					+ "</table>\n" + "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n" + "</td>\n" + "</tr>\n"
					+ "</tbody>\n" + "</table>\n" + "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n" + "</td>\n"
					+ "</tr>\n" + "</tbody>\n" + "</table>\n" + "</td>\n" + "</tr>\n" + "<tr>\n"
					+ "<td height='30px'></td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0' border='0' style='background:#2e3538'>\n"
					+ "<tbody>\n" + "<tr>\n"
					+ "<td style='background:url(https://www.miraclesoft.com/images/newsletters/2016/november/banner_DS9.png); bgcolor:e35500; border:0; cellpadding:0; cellspacing:0; background-color:#fff;background-repeat:repeat;background-attachment:scroll;background-position:center top;background-clip:border-box;background-origin:padding-box;background-size:cover' align='center' width='100%' class=''>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0' border='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td align='center' valign='top' style='padding:0 20px'>\n"
					+ "<table cellpadding='0' cellspacing='0' border='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td width='900' align='center' valign='top'>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0' border='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td style='padding-top:30px;line-height:0'></td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0' border='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td align='center' valign='top' style='color:#fff;font-family:oPen sans;font-size:28px;mso-line-height-rule:exactly;line-height:36px;font-weight:700;text-transform:normal'>\n"
					+ "Things To Do!!\n" + "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n" + "</td>\n" + "</tr>\n"
					+ "</tbody>\n" + "</table>\n" + "</td>\n" + "</tr>\n" + "<tr>\n" + "<td height='20'></td>\n"
					+ "</tr>\n" + "<tr>\n" + "<td>\n"
					+ "<table style='mso-table-lspace:0pt; mso-table-rspace:0pt; margin: 0 auto; border-collaps:collaps;' align='center' bgcolor='e4e4e4' border='0' cellpadding='0' cellspacing='0' width='40'>\n"
					+ "<tbody>\n" + "<tr>\n" + "<td height='2'></td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n"
					+ "</td>\n" + "</tr>\n" + "<tr>\n" + "<td height='20'></td>\n" + "</tr>\n" + "<tr>\n"
					+ "<td align='center' valign='top'>\n" + "<table cellpadding='0' cellspacing='0' border='0'>\n"
					+ "<tbody>\n" + "<tr>\n" + "<td width='950' align='center' valign='top'>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0' border='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td align='center' valign='top' style='padding:0 10px'>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0' border='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td style='padding-top:0;line-height:0'></td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n"
					+ "<table width='100%' align='center' cellpadding='0' cellspacing='0' border='0'>\n" + "<tbody>\n"
					+ "<tr>\n" + "<td align='center' valign='top' style='font-size:0!important'>\n"
					+ "<div style='width:100%;display:inline-block;vertical-align:top;font-size:normal;max-width:230px' class='block'>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0' border='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td align='center' valign='top' style='padding:0 10px'>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0' border='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td style='padding-top:10px;line-height:0'></td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0' border='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td align='center' valign='top' style='line-height:0!important'>\n"
					+ "<img src='https://www.miraclesoft.com/images/newsletters/2017/January/fill.png' alt='' width='130'>\n"
					+ "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0' border='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td style='padding-top:20px;line-height:0'></td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0' border='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td align='center' valign='top' style='color:#fff;font-family:oPen sans;font-size:16px;mso-line-height-rule:exactly;line-height:20px;font-weight:700;text-transform:normal'>\n"
					+ "Fill\n" + "</td>\n" + "</tr>\n" + "<tr>\n" + "<td height='10px'></td>\n" + "</tr>\n"
					+ "</tbody>\n" + "</table>\n" + "<table width='100%' cellpadding='0' cellspacing='0' border='0'>\n"
					+ "<tbody>\n" + "<tr>\n" + "<td style='padding-top:10px;line-height:0'></td>\n" + "</tr>\n"
					+ "</tbody>\n" + "</table>\n" + "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n" + "</div>\n"
					+ "<div style='width:100%;display:inline-block;vertical-align:top;font-size:normal;max-width:230px' class='block'>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0' border='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td align='center' valign='top' style='padding:0 10px'>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0' border='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td style='padding-top:10px;line-height:0'></td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0' border='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td align='center' valign='top' style='line-height:0!important'>\n"
					+ "<img src='https://www.miraclesoft.com/images/newsletters/2017/January/select.png' alt='' width='130'>\n"
					+ "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0' border='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td style='padding-top:20px;line-height:0'></td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0' border='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td align='center' valign='top' style='color:#fff;font-family:oPen sans;font-size:16px;mso-line-height-rule:exactly;line-height:20px;font-weight:700;text-transform:normal'>\n"
					+ "Select\n" + "</td>\n" + "</tr>\n" + "<tr>\n" + "<td height='10px'></td>\n" + "</tr>\n"
					+ "</tbody>\n" + "</table>\n" + "<table width='100%' cellpadding='0' cellspacing='0' border='0'>\n"
					+ "<tbody>\n" + "<tr>\n" + "<td style='padding-top:10px;line-height:0'></td>\n" + "</tr>\n"
					+ "</tbody>\n" + "</table>\n" + "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n" + "</div>\n"
					+ "<div style='width:100%;display:inline-block;vertical-align:top;font-size:normal;max-width:230px' class='block'>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0' border='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td align='center' valign='top' style='padding:0 10px'>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0' border='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td style='padding-top:10px;line-height:0'></td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0' border='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td align='center' valign='top' style='line-height:0!important'>\n"
					+ "<img src='https://www.miraclesoft.com/images/newsletters/2017/January/enjoy.png' alt='' width='130'>\n"
					+ "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0' border='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td style='padding-top:20px;line-height:0'></td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0' border='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td align='center' valign='top' style='color:#fff;font-family:oPen sans;font-size:16px;mso-line-height-rule:exactly;line-height:20px;font-weight:700;text-transform:normal'>\n"
					+ "Enjoy\n" + "</td>\n" + "</tr>\n" + "<tr>\n" + "<td height='10px'></td>\n" + "</tr>\n"
					+ "</tbody>\n" + "</table>\n" + "<table width='100%' cellpadding='0' cellspacing='0' border='0'>\n"
					+ "<tbody>\n" + "<tr>\n" + "<td style='padding-top:10px;line-height:0'></td>\n" + "</tr>\n"
					+ "</tbody>\n" + "</table>\n" + "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n" + "</div>\n"
					+ "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n" + "</td>\n" + "</tr>\n" + "</tbody>\n"
					+ "</table>\n" + "<table width='100%' cellpadding='0' cellspacing='0' border='0'>\n" + "<tbody>\n"
					+ "<tr>\n" + "<td style='padding-top:0px;line-height:0'></td>\n" + "</tr>\n" + "</tbody>\n"
					+ "</table>\n" + "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n" + "</td>\n" + "</tr>\n"
					+ "<tr>\n" + "<td height='20'></td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n" + "</td>\n"
					+ "</tr>\n" + "</tbody>\n" + "</table>\n" + "<table width='100%' cellpadding='0' cellspacing='0'>\n"
					+ "<tbody>\n" + "<tr>\n" + "<td height='30px'></td>\n" + "</tr>\n" + "<tr>\n"
					+ "<td align='center' valign='top'>\n" + "<table width='100%' cellpadding='0' cellspacing='0'>\n"
					+ "<tbody>\n" + "<tr>\n" + "<td align='center' valign='top' style='padding: 0 20px;'>\n"
					+ "<table cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td width='900' align='center' valign='top'>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td align='center' valign='top'>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0' style='background: #ffffff;'>\n"
					+ "<tbody>\n" + "<tr>\n" + "<td align='center' valign='top' style='padding: 0 18px;'>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td style='padding-top: 20px; line-height: 0px;'></td>\n" + "</tr>\n" + "</tbody>\n"
					+ "</table>\n" + "<table width='100%' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td align='center' valign='top' style='padding: 0 12px;'>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td style='padding-top: 10px; line-height: 0px;'></td>\n" + "</tr>\n" + "</tbody>\n"
					+ "</table>\n" + "<table width='100%' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td align='justify' valign='top' style='color: #8c8c8c; font-family: oPen sans; font-size: 14px; mso-line-height-rule: exactly; line-height: 30px; font-weight: 400;'>\n"
					+ "Please go ahead and RSVP your presence and let us know who will be coming to the movie, when you want to go and which movie you would like to see. You are our STAR, and we will take the best care possible of you, your family and friends.\n"
					+ "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td style='padding-top: 10px; line-height: 0px;'></td>\n" + "</tr>\n" + "</tbody>\n"
					+ "</table>\n" + "<table width='100%' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td align='justify' valign='top' style='color: #8c8c8c; font-family: oPen sans; font-size: 14px; mso-line-height-rule: exactly; line-height: 30px; font-weight: 400;'>\n"
					+ "<b>Keep up the good work and I wish you all the best for the future!</b>\n" + "</td>\n"
					+ "</tr>\n" + "</tbody>\n" + "</table>\n" + "<table width='100%' cellpadding='0' cellspacing='0'>\n"
					+ "<tbody>\n" + "<tr>\n" + "<td style='padding-top: 10px; line-height: 0px;'></td>\n" + "</tr>\n"
					+ "</tbody>\n" + "</table>\n" + "<table width='100%' cellpadding='0' cellspacing='0'>\n"
					+ "<tbody>\n" + "<tr>\n" + "<td align='left' valign='top' style='line-height:0!important'>\n"
					+ "<img src='https://www.miraclesoft.com/images/newsletters/2016/October/Ravi%20Ijju.png' alt='' width='100'>\n"
					+ "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td style='padding-top: 10px; line-height: 0px;'></td>\n" + "</tr>\n" + "</tbody>\n"
					+ "</table>\n" + "<table width='100%' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td align='justify' valign='top' style='color: #8c8c8c; font-family: oPen sans; font-size: 14px; mso-line-height-rule: exactly; line-height: 30px; font-weight: 400;'>\n"
					+ "<b>Thanks and Regards,</b>\n" + "</td>\n" + "</tr>\n" + "<tr>\n"
					+ "<td align='justify' valign='top' style='color: #8c8c8c; font-family: oPen sans; font-size: 14px; mso-line-height-rule: exactly; line-height: 30px; font-weight: 400;'>\n"
					+ "<b>Ravi Ijju</b>\n" + "</td>\n" + "</tr>\n" + "<tr>\n"
					+ "<td align='justify' valign='top' style='color: #8c8c8c; font-family: oPen sans; font-size: 14px; mso-line-height-rule: exactly; line-height: 30px; font-weight: 400;'>\n"
					+ "Director HR and MILE\n" + "</td>\n" + "</tr>\n" + "<tr>\n"
					+ "<td align='justify' valign='top' style='color: #8c8c8c; font-family: oPen sans; font-size: 14px; mso-line-height-rule: exactly; line-height: 30px; font-weight: 400;'>\n"
					+ "Miracle Software Systems, Inc.\n" + "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td style='padding-top: 10px; line-height: 0px;'></td>\n" + "</tr>\n" + "</tbody>\n"
					+ "</table>\n" + "<table align='left' cellpadding='0' cellspacing='0' border='0'>\n" + "<tbody>\n"
					+ "<tr>\n"
					+ "<td width='100%' height='40' align='center' valign='middle' style='border-radius:7px; color:#fff;font-family:oPen sans;font-size:15px;mso-line-height-rule:exactly;line-height:18px;font-weight:700;display:block;background:#232527;padding: 0 15px;'>\n"
					+ "<span style='color:#fff'>\n"
					+ "<a style='display:block;height:40px;width:100%;text-decoration:none;font-family:color:#fff;font-family:oPen sans;font-size:15px;mso-line-height-rule:exactly;line-height:18px;font-weight:700;color:#fff' href='"
					+ com.mss.mirage.util.Properties.getProperty("WEBSITE.URL") + "/survey/survey-form.php?surveyId="
					+ suvyId + "' target='_blank'>\n" + "<span style='line-height:40px'>\n" + "RSVP Now\n" + "</span>\n"
					+ "</a>\n" + "</span>\n" + "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td style='padding-top: 10px; line-height: 0px;'></td>\n" + "</tr>\n" + "</tbody>\n"
					+ "</table>\n" + "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td style='padding-top: 20px; line-height: 0px;'></td>\n" + "</tr>\n" + "</tbody>\n"
					+ "</table>\n" + "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n" + "</td>\n" + "</tr>\n"
					+ "</tbody>\n" + "</table>\n" + "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n" + "</td>\n"
					+ "</tr>\n" + "</tbody>\n" + "</table>\n" + "</td>\n" + "</tr>\n" + "<tr>\n"
					+ "<td height='30px'></td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0' border='0' style='background:#2e3538'>\n"
					+ "<tbody>\n" + "<tr>\n"
					+ "<td style='background:url(https://www.miraclesoft.com/images/newsletters/2016/november/banner_DS9.png); bgcolor:#e35500; border:0; cellpadding:0; cellspacing:0;background-color:#fff;background-repeat:repeat;background-attachment:scroll;background-position:center top;background-clip:border-box;background-origin:padding-box;background-size:cover' align='center' width='100%' class=''>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0' border='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td height='20'></td>\n" + "</tr>\n" + "<tr>\n" + "<td align='center' valign='top'>\n"
					+ "<table cellpadding='0' cellspacing='0' border='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td width='900' align='center' valign='top'>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0' border='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td align='center' valign='top' style='padding:0 10px'>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0' border='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td style='padding-top:0;line-height:0'></td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td align='justify' valign='top' style='color: #ffffff; font-family: oPen sans; font-size: 14px; mso-line-height-rule: exactly; line-height: 30px; font-weight: 400;'>\n"
					+ "<i><b>Note</b> : If you do not RSVP before <b>"
					+ com.mss.mirage.util.DateUtility.getInstance().getDateWithMonthNameFromUsFormat(expireDate)
					+ "</b>, then I will consider that you are not interested in this programme.</i>\n" + "</td>\n"
					+ "</tr>\n" + "</tbody>\n" + "</table>\n" + "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0' border='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td style='padding-top:0px;line-height:0'></td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n"
					+ "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n" + "</td>\n" + "</tr>\n" + "<tr>\n"
					+ "<td height='20'></td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n" + "</td>\n" + "</tr>\n"
					+ "</tbody>\n" + "</table>\n" + "<table width='100%' cellpadding='0' cellspacing='0'>\n"
					+ "<tbody>\n" + "<tr>\n" + "<td align='center' valign='top'>\n"
					+ "<table cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td width='1000' align='center' valign='top'>\n"
					+ "<table width='100%' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td align='center' valign='top' style='padding: 0 10px;'></td>\n" + "</tr>\n" + "</tbody>\n"
					+ "</table>\n" + "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n" + "</td>\n" + "</tr>\n"
					+ "</tbody>\n" + "</table>\n"
					+ "<table width='100%' align='center' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td align='center' valign='top' style='background: #eceff3;'>\n"
					+ "<table width='100%' align='center' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td style='background-color: #eceff3; background-repeat: repeat; background-attachment: scroll; background-position: center top; background-clip: border-box; background-origin: padding-box; background-size: cover;' align='center' bgcolor='#eceff3' width='100%' class=''>\n"
					+ "<table width='100%' align='center' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td align='center' valign='top' style='padding: 0 0px;'>\n"
					+ "<table align='center' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td width='200' align='center' valign='top'>\n"
					+ "<table width='100%' align='center' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td height='30' style='height: 30px; line-height:30px;'></td>\n" + "</tr>\n" + "<tr>\n"
					+ "<td align='center' valign='top' style='line-height: 0px !important;'>\n"
					+ "<a style='text-decoration: none; display: block;' href='https://www.miraclesoft.com/' target='blank'>\n"
					+ "<img src='https://www.miraclesoft.com/images/newsletters/miracle-logo-dark.png' alt='logo' width='100%' style='width: 100%; max-width: 100%;'>\n"
					+ "</a>\n" + "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n" + "</td>\n" + "</tr>\n"
					+ "</tbody>\n" + "</table>\n"
					+ "<table width='100%' align='center' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td height='20' style='height: 20px; line-height:20px;'></td>\n" + "</tr>\n" + "<tr>\n"
					+ "<td align='center' valign='top'>\n" + "<table align='center' cellpadding='0' cellspacing='0'>\n"
					+ "<tbody>\n" + "<tr>\n"
					+ "<td width='600' align='center' valign='top' style='font-size: 0px !important;'>\n"
					+ "<div style='width: 100%; display: inline-block; vertical-align: top; font-size: normal; max-width: 60px;'>\n"
					+ "<table width='100%' align='center' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td align='center' valign='top' style='padding: 0 10px;'>\n"
					+ "<table width='100%' align='center' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td height='10' style='height: 10px; line-height:10px;'></td>\n" + "</tr>\n" + "<tr>\n"
					+ "<td align='center' valign='top' style='line-height: 0px !important;'>\n"
					+ "<a style='text-decoration: none; display: inline-block;' href='https://facebook.com/miracle45625' target='blank'>\n"
					+ "<img src='https://www.miraclesoft.com/images/newsletters/2016/September/facebook-1.png' alt='socials1' width='40'>\n"
					+ "</a>\n" + "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n" + "</td>\n" + "</tr>\n"
					+ "</tbody>\n" + "</table>\n" + "</div>\n"
					+ "<div style='width: 100%; display: inline-block; vertical-align: top; font-size: normal; max-width: 60px;'>\n"
					+ "<table width='100%' align='center' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td align='center' valign='top' style='padding: 0 10px;'>\n"
					+ "<table width='100%' align='center' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td height='10' style='height: 10px; line-height:10px;'></td>\n" + "</tr>\n" + "<tr>\n"
					+ "<td align='center' valign='top' style='line-height: 0px !important;'>\n"
					+ "<a style='text-decoration: none; display: inline-block;' href='https://plus.google.com/+Team_MSS' target='blank'>\n"
					+ "<img src='https://www.miraclesoft.com/images/newsletters/2016/September/google-plus-2.png' alt='socials1' width='40'>\n"
					+ "</a>\n" + "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n" + "</td>\n" + "</tr>\n"
					+ "</tbody>\n" + "</table>\n" + "</div>\n"
					+ "<div style='width: 100%; display: inline-block; vertical-align: top; font-size: normal; max-width: 60px;'>\n"
					+ "<table width='100%' align='center' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td align='center' valign='top' style='padding: 0 10px;'>\n"
					+ "<table width='100%' align='center' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td height='10' style='height: 10px; line-height:10px;'></td>\n" + "</tr>\n" + "<tr>\n"
					+ "<td align='center' valign='top' style='line-height: 0px !important;'>\n"
					+ "<a style='text-decoration: none; display: inline-block;' href='https://www.linkedin.com/company/miracle-software-systems-inc' target='blank'>\n"
					+ "<img src='https://www.miraclesoft.com/images/newsletters/2016/September/linkedin-2.png' alt='socials1' width='40'>\n"
					+ "</a>\n" + "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n" + "</td>\n" + "</tr>\n"
					+ "</tbody>\n" + "</table>\n" + "</div>\n"
					+ "<div style='width: 100%; display: inline-block; vertical-align: top; font-size: normal; max-width: 60px;'>\n"
					+ "<table width='100%' align='center' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td align='center' valign='top' style='padding: 0 10px;'>\n"
					+ "<table width='100%' align='center' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td height='10' style='height: 10px; line-height:10px;'></td>\n" + "</tr>\n" + "<tr>\n"
					+ "<td align='center' valign='top' style='line-height: 0px !important;'>\n"
					+ "<a style='text-decoration: none; display: inline-block;' href='https://www.flickr.com/photos/team_miracle' target='blank'>\n"
					+ "<img src='https://www.miraclesoft.com/images/newsletters/2016/September/flickr-1.png' alt='socials1' width='40'>\n"
					+ "</a>\n" + "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n" + "</td>\n" + "</tr>\n"
					+ "</tbody>\n" + "</table>\n" + "</div>\n"
					+ "<div style='width: 100%; display: inline-block; vertical-align: top; font-size: normal; max-width: 60px;'>\n"
					+ "<table width='100%' align='center' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td align='center' valign='top' style='padding: 0 10px;'>\n"
					+ "<table width='100%' align='center' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td height='10' style='height: 10px; line-height:10px;'></td>\n" + "</tr>\n" + "<tr>\n"
					+ "<td align='center' valign='top' style='line-height: 0px !important;'>\n"
					+ "<a style='text-decoration: none; display: inline-block;' href='https://twitter.com/Team_MSS' target='blank'>\n"
					+ "<img src='https://www.miraclesoft.com/images/newsletters/2016/September/twitter-2.png' alt='socials2' width='40'>\n"
					+ "</a>\n" + "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n" + "</td>\n" + "</tr>\n"
					+ "</tbody>\n" + "</table>\n" + "</div>\n"
					+ "<div style='width: 100%; display: inline-block; vertical-align: top; font-size: normal; max-width: 60px;'>\n"
					+ "<table width='100%' align='center' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td align='center' valign='top' style='padding: 0 10px;'>\n"
					+ "<table width='100%' align='center' cellpadding='0' cellspacing='0'>\n" + "<tbody>\n" + "<tr>\n"
					+ "<td height='10' style='height: 10px; line-height:10px;'></td>\n" + "</tr>\n" + "<tr>\n"
					+ "<td align='center' valign='top' style='line-height: 0px !important;'>\n"
					+ "<a style='text-decoration: none; display: inline-block;' href='https://www.youtube.com/c/Team_MSS' target='blank'>\n"
					+ "<img src='https://www.miraclesoft.com/images/newsletters/2016/September/youtube-3.png' alt='socials3' width='40'>\n"
					+ "</a>\n" + "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n" + "</td>\n" + "</tr>\n"
					+ "</tbody>\n" + "</table>\n" + "</div>\n" + "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n"
					+ "</td>\n" + "</tr>\n" + "<tr>\n"
					+ "<td height='15' style='height: 15px; line-height:15px;'></td>\n" + "</tr>\n" + "</tbody>\n"
					+ "</table>\n" + "<table width='100%' align='center' cellpadding='0' cellspacing='0'>\n"
					+ "<tbody>\n" + "<tr>\n" + "<td height='10' style='height: 10px; line-height:10px;'></td>\n"
					+ "</tr>\n" + "<tr>\n"
					+ "<td align='center' valign='top' style='color: #232527; font-family: Open sans; font-size: 14px; mso-line-height-rule: exactly; line-height: 23px; font-weight: 400;'>\n"
					+ "&#169; Copyrights " + Calendar.getInstance().get(Calendar.YEAR) + " | Miracle Software Systems\n"
					+ "</td>\n" + "</tr>\n" + "<tr>\n"
					+ "<td height='30' style='height: 30px; line-height:30px;'></td>\n" + "</tr>\n" + "</tbody>\n"
					+ "</table>\n" + "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n" + "</td>\n" + "</tr>\n"
					+ "</tbody>\n" + "</table>\n" + "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n" + "</td>\n"
					+ "</tr>\n" + "</tbody>\n" + "</table>\n" + "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n"
					+ "</td>\n" + "</tr>\n" + "</tbody>\n" + "</table>\n" + "\n" + "</body></html>";

			// String campaignTemplate=htmlText.toString().replaceAll("\"",
			// "'");
			String campaignTemplate = htmlText.toString();
			// System.out.println("campaignTemplate"+campaignTemplate);
			///////////////////////////////////////////////////////////////////////////////////////////////

			EmailCampaignService ecs = new EmailCampaignService(Properties.getProperty("API_KEY"),
					Properties.getProperty("ACCESS_TOCKEN"));

			EmailCampaignRequest emailReq = new EmailCampaignRequest();

			String datetime = new SimpleDateFormat("ddMMMyyyy_hh_mm_ss_SSSa").format(new java.util.Date());
			emailReq.setName("Star Performer RSVP " + datetime);
			emailReq.setSubject("Star Performer RSVP " + shortMonth + " " + fullYear);
			emailReq.setFromEmail("newsletter@miraclesoft.com ");
			emailReq.setFromName("Miracle Software Systems, Inc.");
			emailReq.setReplyToEmail("newsletter@miraclesoft.com ");

			// emailReq.setPermissionReminderEnabled(true);
			// emailReq.setPermissionReminderText("");
			emailReq.setViewAsWebpageEnabled(false);

			emailReq.setGreetingName("FIRST_NAME");
			emailReq.setGreetingSalutations("Hello");
			/// emailReq.setGreetingString("Dear");
			emailReq.setEmailContent(campaignTemplate);
			emailReq.setEmailContentFormat("HTML");
			// emailReq.setTemplateType("CUSTOM");
			emailReq.setTextContent("Star Performer RSVP");

			MessageFooter messageFooter = new MessageFooter();
			messageFooter.setAddressLine1("45625 Grand River Avenue");
			messageFooter.setOrganizationName("Miracle Software Systems, Inc.");
			messageFooter.setCity("Novi");
			messageFooter.setCountry("US");
			messageFooter.setState("MI");
			messageFooter.setPostalCode("48374");
			emailReq.setMessageFooter(messageFooter);

			List<SentToContactList> scl = new ArrayList();

			SentToContactList stcl = new SentToContactList();

			stcl.setId(contactId);

			scl.add(stcl);
			emailReq.setSentToContactLists(scl);

			EmailCampaignResponse errs = ecs.addCampaign(emailReq);

			if (errs.getId() != null && !"".equalsIgnoreCase(errs.getId().trim())) {

				EmailCampaignScheduleService ecsh = new EmailCampaignScheduleService(Properties.getProperty("API_KEY"),
						Properties.getProperty("ACCESS_TOCKEN"));

				EmailCampaignResponse ecrsp = ecs.getCampaign(errs.getId());

				// System.out.println("ecrsp.getSentToContactLists()"+ecrsp.getSentToContactLists());
				EmailCampaignSchedule Ecs = new EmailCampaignSchedule();

				// String campgnStartDate=
				// com.mss.mirage.util.DateUtility.getInstance().convertFromElasticToGraphView(
				// com.mss.mirage.util.DateUtility.getInstance().getCurrentDateformate());
				// Ecs.setScheduledDate(campgnStartDate);
				EmailCampaignSchedule finalEcs = null;
				int size = 0;
				while (size <= i) {
					com.constantcontact.components.generic.response.ResultSet<Contact> c = cls
							.getContactsFromList(contactId, 500, null);

					size = c.size();

					if (size >= i) {

						finalEcs = ecsh.addSchedule(ecrsp.getId(), Ecs);
						break;
					}

				}

				// System.out.println(finalEcs.toJSON().toString());

				if (finalEcs != null) {

					String query1 = "Update tblStarPerformers Set SurveyFormId='" + suvyId + "',Status='6' WHERE Id="
							+ objectId + "";
					// System.out.println("query.."+query1);
					statement = connection.createStatement();
					rows = statement.executeUpdate(query1);

				}

			}

		} catch (SQLException ex) {
			Logger.getLogger(NewAjaxHandlerServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
		} catch (ConstantContactServiceException e) {
			ConstantContactServiceException c = (ConstantContactServiceException) e.getCause();
			for (Iterator<RawApiRequestError> i1 = c.getErrorInfo().iterator(); i1.hasNext();) {
				RawApiRequestError item = i1.next();
				System.out.println(item.getErrorKey() + " : " + item.getErrorMessage());
			}
			e.printStackTrace();
			// System.out.println(e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}

		finally {
			try {
				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException sqle) {
			}
		}
		// System.err.println("response string is"+stringBuffer.toString());
		return rows;
	}

	/* Nagalakshmi Telluri */
	/* 5/17/2017 */

	public String gettotalweekcountbyprojectDetails(NewAjaxHandlerAction ajaxhandleraction)
			throws ServiceLocatorException {
		Connection connection = null;
		CallableStatement callableStatement = null;
		String responseString = "";

		String queryString = "{call spGetTimeSheetWeeklyHoursByEmployee(?,?,?,?,?)}";

		try {
			connection = ConnectionProvider.getInstance().getConnection();
			callableStatement = connection.prepareCall(queryString);
			callableStatement.setString(1, ajaxhandleraction.getYear());

			// System.out.println(" the year is" +ajaxhandleraction.getYear());

			callableStatement.setString(2, ajaxhandleraction.getMonth());
			// System.out.println(" the month is"
			// +ajaxhandleraction.getMonth());

			callableStatement.setString(3, ajaxhandleraction.getProjectId());
			callableStatement.setString(4, ajaxhandleraction.getWeek());

			callableStatement.registerOutParameter(5, java.sql.Types.VARCHAR);

			callableStatement.executeQuery();
			responseString = callableStatement.getString(5);
			// System.out.println("response String is "+responseString);
		} catch (SQLException se) {
			se.printStackTrace();

			throw new ServiceLocatorException(se);
		} finally {
			try {

				if (callableStatement != null) {
					callableStatement.close();
					callableStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException se) {
				se.printStackTrace();
				throw new ServiceLocatorException(se);
			}
		}
		return responseString;
	}

	public String getDateAndTotalHoursByProject(NewAjaxHandlerAction ajaxhandleraction) throws ServiceLocatorException {

		// System.out.println("enter into action method implol");
		Connection connection = null;
		CallableStatement callableStatement = null;
		String responseString = null;

		String queryString = "{call spGetDateAndTotalTimeSheetHoursByProject(?,?,?,?,?,?)}";

		try {
			connection = ConnectionProvider.getInstance().getConnection();
			callableStatement = connection.prepareCall(queryString);
			callableStatement.setString(1, ajaxhandleraction.getYear());
			callableStatement.setString(2, ajaxhandleraction.getMonth());
			callableStatement.setString(3, ajaxhandleraction.getProjectId());
			callableStatement.setString(4, ajaxhandleraction.getWeek());
			callableStatement.setString(5, ajaxhandleraction.getEmpId());
			callableStatement.registerOutParameter(6, java.sql.Types.VARCHAR);
			callableStatement.executeQuery();
			responseString = callableStatement.getString(6);
			// System.out.println("response String is "+responseString);
		} catch (SQLException sqle) {
			throw new ServiceLocatorException(sqle);
		} finally {

			try {

				if (callableStatement != null) {
					callableStatement.close();
					callableStatement = null;
				}

				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException sql) {
				// System.err.print("Error :"+sql);
			}

		}
		// System.out.println("---------->" + resultString1);
		return responseString;
	}

	/* LookUp Management Changes */

	public String getlookUpReport(NewAjaxHandlerAction ajaxHandlerAction, HttpServletRequest httpServletRequest)
			throws ServiceLocatorException {
		System.out.println(" getlookUpReport service impl -->" + ajaxHandlerAction.getTableName());
		// String qsTitle = "";
		Statement statement = null;
		ResultSet resultSet = null;
		String query = "";
		Connection connection = null;
		String response = "";
		String columnNames = "";
		int count = 0;
		int insertedRows = 0;
		String str = "";
		String secondColumnName = "";
		try {
			connection = ConnectionProvider.getInstance().getConnection();
			System.out.println("ajaxHandlerAction.getColumnData() is====>" + ajaxHandlerAction.getColumnData());
			System.out.println("ajaxHandlerAction.getColumnName() is====>" + ajaxHandlerAction.getColumnName());
			query = "select * from  " + ajaxHandlerAction.getTableName() + " where 1=1";
			if (ajaxHandlerAction.getColumnData() != null || !"".equals(ajaxHandlerAction.getColumnData())) {
				query = query + " AND " + ajaxHandlerAction.getColumnName() + " LIKE '%"
						+ ajaxHandlerAction.getColumnData() + "%' ";
			}
			System.out.println("query is====>" + query);
			statement = connection.createStatement();
			resultSet = statement.executeQuery(query);
			ResultSetMetaData rsMetaData = resultSet.getMetaData();

			int numberOfColumns = rsMetaData.getColumnCount();

			// System.out.println("resultSet MetaData column Count=" +
			// numberOfColumns);
			for (int i = 1; i <= numberOfColumns; i++) {
				columnNames = columnNames + "#^$" + resultSet.getMetaData().getColumnName(i) + "@"
						+ resultSet.getMetaData().getColumnDisplaySize(i);
				// System.out.println("columnNames...."+columnNames);
				secondColumnName = resultSet.getMetaData().getColumnName(2);
				System.out.println("columnNames out...." + resultSet.getMetaData().getColumnDisplaySize(i));
			}
			System.out.println("columnNames out...." + columnNames);
			System.out.println("secondColumnName out...." + secondColumnName);
			while (resultSet.next()) {
				count++;
				response = response + count;
				for (int i = 1; i < resultSet.getMetaData().getColumnCount() + 1; i++) {
					// System.out.print(" " +
					// resultSet.getMetaData().getColumnName(i) + "=" +
					// resultSet.getObject(i));

					if ("".equals(resultSet.getObject(i)) || resultSet.getObject(i) == null) {
						// System.out.println("in null case....");
						str = "-";

					}
					if (!"".equals(resultSet.getObject(i)) && resultSet.getObject(i) != null) {
						// System.out.println("in not null case....");
						str = resultSet.getObject(i).toString();
						// str =
						// resultSet.getObject(i).toString()+","+resultSet.getMetaData().getColumnDisplaySize(i);
					}
					// if(!"".equals(resultSet.getObject(i)) &&
					// resultSet.getObject(i) != null){

					response = response + "#^$" + str;
					// }

				}
				response = response + "*@!";
			}
			// System.out.println("count is----->"+count);
			// System.out.println("response final is----->"+response);
			response = response + "addto" + columnNames;
			// System.out.println("response.."+response);
			// httpServletRequest.setAttribute(ApplicationConstants.LOOKUP_COLUMN_NAME,secondColumnName);
		} catch (Exception sqe) {
			sqe.printStackTrace();
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (statement != null) {
					statement.close();
					statement = null;
				}

				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException sqle) {
				sqle.printStackTrace();
			}
		}

		return response;
	}

	public int addColumnNames(NewAjaxHandlerAction ajaxHandlerAction, HttpServletRequest httpServletRequest)
			throws ServiceLocatorException {
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		String query = "";
		String queryString = "";
		Connection connection = null;
		String response = "";
		String columnNames = "";
		int insertedRows = 0;
		int count = 0;
		String insertValues = "";
		int isUpdated = 0;
		int flagVariable;
		Statement statement = null;
		int idValue = 0;
		try {
			connection = ConnectionProvider.getInstance().getConnection();
			System.out.println("insertValues out side for loop====>" + ajaxHandlerAction.getStringData());
			String[] stringArray = ajaxHandlerAction.getStringData().split(",");
			System.out.println("stringArray====>" + stringArray);
			for (int i = 0; i < stringArray.length; i++) {
				insertValues += "\"" + stringArray[i] + "\"";
				if (i != stringArray.length - 1) {
					insertValues += ", ";
				}
				System.out.println("insertValues====>" + insertValues);
			}
			System.out.println("insertValues out side for again loop====>" + insertValues);
			System.out.println("ajaxHandlerAction.getHeaderFields()====>" + ajaxHandlerAction.getHeaderFields());
			System.out.println("ajaxHandlerAction.getTableName()====>" + ajaxHandlerAction.getTableName());
			flagVariable = DataSourceDataProvider.getInstance()
					.checkAutoIncrementValue(ajaxHandlerAction.getTableName());
			System.out.println("flagVariable====>" + flagVariable);
			if (flagVariable == 1) {
				System.out.println("in flagVariable 1====>");
				query = "insert into   " + ajaxHandlerAction.getTableName() + " (" + ajaxHandlerAction.getHeaderFields()
						+ ") values (" + insertValues + ")";
			}
			if (flagVariable == 0) {
				System.out.println("in flagVariable 0====>");
				queryString = "SELECT MAX(Id)+1 AS idValue FROM " + ajaxHandlerAction.getTableName() + " ";
				statement = connection.createStatement();
				resultSet = statement.executeQuery(queryString);
				while (resultSet.next()) {
					// count = resultSet.getInt("count");
					idValue = resultSet.getInt("idValue");
				}
				System.out.println("idValue====>" + idValue);

				query = "insert into   " + ajaxHandlerAction.getTableName() + " (Id,"
						+ ajaxHandlerAction.getHeaderFields() + ") values (" + idValue + "," + insertValues + ")";
			}
			System.out.println("query====>" + query);

			preparedStatement = connection.prepareStatement(query);
			isUpdated = preparedStatement.executeUpdate();
			System.out.println("query====>" + query);
			System.out.println("isUpdated====>" + isUpdated);
		} catch (Exception sqe) {
			sqe.printStackTrace();
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}

				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException sqle) {
				sqle.printStackTrace();
			}
		}

		return isUpdated;
	}

	public String getLookUpDetails(String lookUpId, String tableName) throws ServiceLocatorException {
		StringBuffer stringBuffer = new StringBuffer();
		Connection connection = null;
		CallableStatement callableStatement = null;
		PreparedStatement preparedStatement = null;
		Statement statement = null;
		ResultSet resultSet = null;
		String activityDetails = "";
		// int i = 0;
		JSONObject subJson = null;
		String queryString = "";
		String response = "";
		String columnNames = "";
		int count = 0;
		try {
			queryString = "SELECT  * FROM " + tableName + " WHERE Id=" + lookUpId;

			System.out.println("queryString-->" + queryString);
			connection = ConnectionProvider.getInstance().getConnection();
			statement = connection.createStatement();
			resultSet = statement.executeQuery(queryString);
			ResultSetMetaData rsMetaData = resultSet.getMetaData();
			if (resultSet.next()) {
				subJson = new JSONObject();

				for (int i = 1; i < resultSet.getMetaData().getColumnCount() + 1; i++) {
					subJson.put("" + resultSet.getMetaData().getColumnName(i) + "", resultSet.getObject(i));
					System.out.println("subJson...." + subJson.toString());
					System.out.print(" " + resultSet.getMetaData().getColumnName(i) + "=" + resultSet.getObject(i));
				}
				subJson.put("Id", resultSet.getInt("Id"));
			}
			System.out.println("subJson...." + subJson.toString());
		} catch (Exception sqe) {
			sqe.printStackTrace();
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (statement != null) {
					statement.close();
					statement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException sqle) {
			}
		}
		return subJson.toString();
	}

	public boolean updateColumnNames(NewAjaxHandlerAction ajaxHandlerAction, HttpServletRequest httpServletRequest)
			throws ServiceLocatorException {
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		String query = "";
		Connection connection = null;
		String response = "";
		String columnNames = "";
		int insertedRows = 0;
		int count = 0;
		String headerInsertData = "";
		boolean isUpdated = false;
		String queryString = "";
		try {
			System.out.println("ajaxHandlerAction.getStringData()====>" + ajaxHandlerAction.getStringData());
			System.out.println("ajaxHandlerAction.getHeaderFields()====>" + ajaxHandlerAction.getHeaderFields());

			String[] headerArray = ajaxHandlerAction.getHeaderFields().split(",");
			for (int i = 0; i < headerArray.length; i++) {
				// insertValues += "\"" + stringArray[i] + "\"";
				// if (i != stringArray.length - 1) {
				// insertValues += ", ";
				// }
				// System.out.println("insertValues====>"+insertValues);
				headerInsertData += "" + headerArray[i] + " =?";
				if (i != headerArray.length - 1) {
					headerInsertData += ", ";
				}
				System.out.println("headerInsertData====>" + headerInsertData);
				// queryString = "update "+ajaxHandlerAction.getTableName()+"
				// set EmpComments=? where Id=?";
			}
			queryString = "update " + ajaxHandlerAction.getTableName() + " set " + headerInsertData + " where Id="
					+ ajaxHandlerAction.getLookUpId() + " ";
			System.out.println("queryString====>" + queryString);

			connection = ConnectionProvider.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(queryString);
			String[] stringArray = ajaxHandlerAction.getStringData().split(",");
			for (int i = 0; i < stringArray.length; i++) {
				preparedStatement.setString(i + 1, stringArray[i]);
				System.out.println("headerInsertData====>" + i + "value" + stringArray[i]);
			}
			isUpdated = preparedStatement.execute();
			//

			//
			// query="update "+ajaxHandlerAction.getTableName()+"
			// ("+ajaxHandlerAction.getHeaderFields()+") values
			// ("+insertValues+")";
			// connection = ConnectionProvider.getInstance().getConnection();
			// preparedStatement = connection.prepareStatement(query);
			// isUpdated = preparedStatement.execute();
			// System.out.println("query====>"+query);
			//
			// queryString = "update tblEmpReview set
			// EmpComments=?,ReviewName=?,ReviewDate=?,Status=?,BillingAmount=?,LogosCount=?
			// where Id=?";
			//
			// try {
			// connection = ConnectionProvider.getInstance().getConnection();
			// preparedStatement = connection.prepareStatement(queryString);
			// //
			// preparedStatement.setString(1,ajaxHandlerAction.getOverlayReviewType());
			// preparedStatement.setString(1,
			// ajaxHandlerAction.getOverlayDescription());
			//
			//
			//
			// preparedStatement.setString(2,
			// ajaxHandlerAction.getOverlayReviewName());
			// preparedStatement.setDate(3,
			// DateUtility.getInstance().getMysqlDate(ajaxHandlerAction.getOverlayReviewDate()));
			// preparedStatement.setString(4,
			// ajaxHandlerAction.getReviewStatusOverlay());
			// preparedStatement.setDouble(5,
			// ajaxHandlerAction.getOverlayReviewBilling());
			// preparedStatement.setInt(6,
			// ajaxHandlerAction.getOverlayReviewLogAdded());
			// preparedStatement.setString(7, ajaxHandlerAction.getReviewId());
			//
			// isUpdated = preparedStatement.execute();
			//

		} catch (Exception sqe) {
			sqe.printStackTrace();
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}

				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException sqle) {
				sqle.printStackTrace();
			}
		}

		return isUpdated;
	}

	// public String getLookUpTableDetails(String query) throws
	// ServiceLocatorException {
	// System.out.println("in getLookUpTableDetails====>");
	// boolean isGetting = false;
	// Connection connection = null;
	// PreparedStatement preparedStatement = null;
	// ResultSet resultSet = null;
	// StringBuffer sb = new StringBuffer();
	// try {
	// System.out.println("query====>"+query);
	// connection = ConnectionProvider.getInstance().getConnection();
	// preparedStatement = connection.prepareStatement(query);
	// resultSet = preparedStatement.executeQuery();
	//
	// int count = 0;
	// sb.append("<xml version=\"1.0\">");
	// sb.append("<EMPLOYEES>");
	// while (resultSet.next()) {
	// sb.append("<EMPLOYEE><VALID>true</VALID>");
	//
	// if (resultSet.getString(1) == null || resultSet.getString(1).equals(""))
	// {
	// sb.append("<NAME>NoRecord</NAME>");
	// } else {
	// String title = resultSet.getString(1);
	// if (title.contains("&")) {
	// title = title.replace("&", "&amp;");
	// }
	// sb.append("<NAME>" + title + "</NAME>");
	// }
	// //sb.append("<NAME>" +resultSet.getString(1) + "</NAME>");
	// sb.append("<EMPID>" + String.valueOf(resultSet.getInt(2)) + "</EMPID>");
	// sb.append("</EMPLOYEE>");
	// isGetting = true;
	// count++;
	// System.out.println("sb.toString()====>"+sb.toString());
	// }
	//
	// if (!isGetting) {
	// //sb.append("<EMPLOYEES>" + sb.toString() + "</EMPLOYEES>");
	// //} else {
	// isGetting = false;
	// //nothing to show
	// // response.setStatus(HttpServletResponse.SC_NO_CONTENT);
	// sb.append("<EMPLOYEE><VALID>false</VALID></EMPLOYEE>");
	// }
	// sb.append("</EMPLOYEES>");
	// sb.append("</xml>");
	//
	// } catch (SQLException sqle) {
	// throw new ServiceLocatorException(sqle);
	//
	// } finally {
	// try {
	// if (resultSet != null) {
	//
	// resultSet.close();
	// resultSet = null;
	// }
	// if (preparedStatement != null) {
	// preparedStatement.close();
	// preparedStatement = null;
	// }
	//
	// if (connection != null) {
	// connection.close();
	// connection = null;
	// }
	// } catch (SQLException sql) {
	// //System.err.print("Error :"+sql);
	// }
	//
	// }
	// return sb.toString();
	// }

	public String getLookUpTableDetails(String query) throws ServiceLocatorException {
		boolean isGetting = false;
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		StringBuffer sb = new StringBuffer();
		try {
			connection = ConnectionProvider.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(query);
			resultSet = preparedStatement.executeQuery();

			int count = 0;
			sb.append("<xml version=\"1.0\">");
			sb.append("<EMPLOYEES>");
			while (resultSet.next()) {

				sb.append("<EMPLOYEE><VALID>true</VALID>");

				if (resultSet.getString(1) == null || resultSet.getString(1).equals("")) {
					sb.append("<NAME>NoRecord</NAME>");
				} else {
					String title = resultSet.getString(1);
					if (title.contains("&")) {
						title = title.replace("&", "&amp;");
					}
					sb.append("<NAME>" + title + "</NAME>");
				}
				// sb.append("<NAME>" +resultSet.getString(1) + "</NAME>");
				sb.append("<EMPID>" + String.valueOf(resultSet.getInt(2)) + "</EMPID>");
				sb.append("</EMPLOYEE>");
				isGetting = true;
				count++;
			}

			if (!isGetting) {
				// sb.append("<EMPLOYEES>" + sb.toString() + "</EMPLOYEES>");
				// } else {
				isGetting = false;
				// nothing to show
				// response.setStatus(HttpServletResponse.SC_NO_CONTENT);
				sb.append("<EMPLOYEE><VALID>false</VALID></EMPLOYEE>");
			}
			sb.append("</EMPLOYEES>");
			sb.append("</xml>");
		} catch (SQLException sqle) {
			throw new ServiceLocatorException(sqle);
		} finally {
			try {
				if (resultSet != null) {

					resultSet.close();
					resultSet = null;
				}
				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}

				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException sql) {
				// System.err.print("Error :"+sql);
			}

		}
		return sb.toString();
	}

	public String doGetExperienceReport(NewAjaxHandlerAction ajaxHandlerAction) throws ServiceLocatorException {
		Connection connection = null;
		CallableStatement callableStatement = null;
		String responseString = null;
		try {

			connection = ConnectionProvider.getInstance().getConnection();
			callableStatement = connection.prepareCall("{call  spGetEpmExperienceReports(?,?,?,?,?,?,?,?,?,?)}");

			if (ajaxHandlerAction.getCountry() == null || "".equals(ajaxHandlerAction.getCountry())) {
				// System.out.println("..."+ajaxHandlerAction.getCountry());
				callableStatement.setString(1, "%");
			} else {
				// System.out.println(ajaxHandlerAction.getCountry());
				callableStatement.setString(1, "" + ajaxHandlerAction.getCountry() + "");
			}

			if (ajaxHandlerAction.getLocation() == null || "".equals(ajaxHandlerAction.getLocation())) {
				callableStatement.setString(2, "%");
				// System.out.println("..."+ajaxHandlerAction.getLocation());
			} else {
				// System.out.println(ajaxHandlerAction.getLocation());
				callableStatement.setString(2, "" + ajaxHandlerAction.getLocation() + "");
			}

			if (ajaxHandlerAction.getDepartmentId() == null || "".equals(ajaxHandlerAction.getDepartmentId())) {
				// System.out.println("..."+ajaxHandlerAction.getDepartmentId());
				callableStatement.setString(3, "%");
			} else {
				// System.out.println(ajaxHandlerAction.getDepartmentId());
				callableStatement.setString(3, "" + ajaxHandlerAction.getDepartmentId() + "");
			}

			callableStatement.setInt(4, ajaxHandlerAction.getExpYear());
			callableStatement.setInt(5, ajaxHandlerAction.getExpMonth());
			callableStatement.setInt(6, ajaxHandlerAction.getObjId());

			if (ajaxHandlerAction.getEmpId() == null || "".equals(ajaxHandlerAction.getEmpId())) {
				// System.out.println("..."+ajaxHandlerAction.getEmpId());
				callableStatement.setString(7, "%");
			} else {
				// System.out.println(ajaxHandlerAction.getEmpId());
				callableStatement.setString(7, "" + ajaxHandlerAction.getEmpId() + "");
			}

			if (ajaxHandlerAction.getOpsContactId() == null || "-1".equals(ajaxHandlerAction.getOpsContactId())
					|| "".equals(ajaxHandlerAction.getOpsContactId())) {
				// System.out.println("..."+ajaxHandlerAction.getOpsContactId());
				callableStatement.setString(8, "%");
			} else {
				// System.out.println(ajaxHandlerAction.getOpsContactId());
				callableStatement.setString(8, "" + ajaxHandlerAction.getOpsContactId() + "");
			}

			if (ajaxHandlerAction.getPracticeId() != null && !"-1".equals(ajaxHandlerAction.getPracticeId())
					&& !"".equals(ajaxHandlerAction.getPracticeId())) {
				// System.out.println("..."+ajaxHandlerAction.getPracticeId());
				callableStatement.setString(9, ajaxHandlerAction.getPracticeId());
			} else {
				System.out.println(ajaxHandlerAction.getPracticeId());
				callableStatement.setString(9, "%");
			}

			callableStatement.registerOutParameter(10, java.sql.Types.VARCHAR);
			callableStatement.executeQuery();
			responseString = callableStatement.getString(10);

			// System.out.println(responseString);
		} catch (SQLException sqle) {
			throw new ServiceLocatorException(sqle);
		} finally {

			try {

				if (callableStatement != null) {
					callableStatement.close();
					callableStatement = null;
				}

				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException sql) {
				// System.err.print("Error :"+sql);
			}

		}
		// System.out.println("---------->" + resultString1);
		return responseString;
	}

	public String popupTenurStatus(NewAjaxHandlerAction ajaxHandlerAction) throws ServiceLocatorException {
		Connection connection = null;
		CallableStatement callableStatement = null;
		String responseString = null;
		try {

			connection = ConnectionProvider.getInstance().getConnection();
			callableStatement = connection.prepareCall("{call  spGetEpmTenurReports(?,?)}");
			if (ajaxHandlerAction.getLoginId() == null || "".equals(ajaxHandlerAction.getLoginId())) {
				callableStatement.setString(1, "%");
			} else {
				callableStatement.setString(1, ajaxHandlerAction.getLoginId());
			}

			callableStatement.executeQuery();
			callableStatement.registerOutParameter(2, java.sql.Types.VARCHAR);
			responseString = callableStatement.getString(2);
		} catch (SQLException sqle) {
			throw new ServiceLocatorException(sqle);
		} finally {

			try {

				if (callableStatement != null) {
					callableStatement.close();
					callableStatement = null;
				}

				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException sql) {
				// System.err.print("Error :"+sql);
			}

		}
		// System.out.println("---------->" + resultString1);
		return responseString;
	}

	public String getCountVsStatusReport(int graphId, String country, String departmentId, String practiceId,
			String subpractice, String availableUtl, HttpServletRequest httpServletRequest)
			throws ServiceLocatorException {

		String response = "";
		// Connection connection = null;
		CallableStatement callableStatement = null;
		Connection connection = null;
		String teamMembersKeys = "";
		Map teamMembersMapByLoginid = null;
		Map teamMembersMapByReportsTo = null;
		try {
			connection = ConnectionProvider.getInstance().getConnection();
			teamMembersMapByLoginid = (Map) httpServletRequest.getSession(false)
					.getAttribute(ApplicationConstants.SESSION_MY_TEAM_MAP);
			String loginId = (String) httpServletRequest.getSession(false)
					.getAttribute(ApplicationConstants.SESSION_USER_ID);

			String department = DataSourceDataProvider.getInstance().getDepartmentName(loginId);
			teamMembersMapByReportsTo = DataSourceDataProvider.getInstance().getMyTeamMembers(loginId, department);
			teamMembersKeys = getKeys(teamMembersMapByReportsTo, ",");
			teamMembersKeys = teamMembersKeys.replaceAll("'", "");
			if (!"".equals(teamMembersKeys)) {
				teamMembersKeys = teamMembersKeys;
			} else {
				teamMembersKeys = loginId;
			}

			int autthFlag = DataSourceDataProvider.getInstance().isAuthorizedForViewResource_Utilization(loginId);

			callableStatement = connection.prepareCall("{call spResourceUtilization22(?,?,?,?,?,?,?,?,?)}");
			callableStatement.setInt(1, graphId);
			callableStatement.setInt(2, autthFlag);
			callableStatement.setString(3, teamMembersKeys);
			if (country != null && !"-1".equals(country)) {
				callableStatement.setString(4, country);
			} else {
				callableStatement.setString(4, "India,USA");
			}
			if (departmentId != null && !"-1".equals(departmentId)) {
				callableStatement.setString(5, departmentId);
			} else {
				callableStatement.setString(5, "SSG,GDC");
			}
			if (practiceId != null && !"-1".equals(practiceId) && !practiceId.contains("--Please Select--")
					&& !"All".equalsIgnoreCase(practiceId)) {
				callableStatement.setString(6, practiceId);
			} else {
				callableStatement.setString(6, "%");
			}

			;
			if (subpractice != null && !"-1".equals(subpractice) && !subpractice.contains("--Please Select--")
					&& !"All".equalsIgnoreCase(subpractice)) {
				callableStatement.setString(7, subpractice);
			} else {
				callableStatement.setString(7, "%");
			}
			if (availableUtl != null && !"-1".equals(availableUtl) && !availableUtl.contains("--Please Select--")
					&& !"All".equalsIgnoreCase(availableUtl)) {
				callableStatement.setString(8, availableUtl);
			} else {
				callableStatement.setString(8, "%");
			}

			callableStatement.registerOutParameter(9, Types.VARCHAR);
			callableStatement.execute();

			response = callableStatement.getString(9);

		} catch (SQLException se) {
			se.printStackTrace();
		} finally {
			try {

				if (callableStatement != null) {
					callableStatement.close();
					callableStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		return response;
	}

	public String getResourcesByStatusList(String activityType, int graphId, String country, String departmentId,
			String practiceId, String subpractice, String availableUtl, HttpServletRequest httpServletRequest)
			throws ServiceLocatorException {

		String response = "";
		// Connection connection = null;
		CallableStatement callableStatement = null;
		Connection connection = null;
		String teamMembersKeys = "";

		Map teamMembersMapByReportsTo = null;

		try {
			connection = ConnectionProvider.getInstance().getConnection();

			String loginId = (String) httpServletRequest.getSession(false)
					.getAttribute(ApplicationConstants.SESSION_USER_ID);

			String department = DataSourceDataProvider.getInstance().getDepartmentName(loginId);
			teamMembersMapByReportsTo = DataSourceDataProvider.getInstance().getMyTeamMembers(loginId, department);
			teamMembersKeys = getKeys(teamMembersMapByReportsTo, ",");
			teamMembersKeys = teamMembersKeys.replaceAll("'", "");
			if (!"".equals(teamMembersKeys)) {
				teamMembersKeys = teamMembersKeys;
			} else {
				teamMembersKeys = loginId;
			}

			int autthFlag = DataSourceDataProvider.getInstance().isAuthorizedForViewResource_Utilization(loginId);

			callableStatement = connection.prepareCall("{call spResourceUtilizationListByStatus(?,?,?,?,?,?,?,?,?,?)}");
			callableStatement.setInt(1, graphId);
			callableStatement.setInt(2, autthFlag);
			callableStatement.setString(3, teamMembersKeys);
			callableStatement.setString(4, activityType);
			if (country != null && !"-1".equals(country)) {
				callableStatement.setString(5, country);
			} else {
				callableStatement.setString(5, "India,USA");
			}
			if (departmentId != null && !"-1".equals(departmentId)) {
				callableStatement.setString(6, departmentId);
			} else {
				callableStatement.setString(6, "SSG,GDC");
			}
			if (practiceId != null && !"-1".equals(practiceId) && !practiceId.contains("--Please Select--")
					&& !"All".equalsIgnoreCase(practiceId)) {
				callableStatement.setString(7, practiceId);
			} else {
				callableStatement.setString(7, "%");
			}

			if (subpractice != null && !"-1".equals(subpractice) && !subpractice.contains("--Please Select--")
					&& !"All".equalsIgnoreCase(subpractice)) {
				callableStatement.setString(8, subpractice);
			} else {
				callableStatement.setString(8, "%");
			}
			if (availableUtl != null && !"-1".equals(availableUtl) && !availableUtl.contains("--Please Select--")
					&& !"All".equalsIgnoreCase(availableUtl)) {
				callableStatement.setString(9, availableUtl);
			} else {
				callableStatement.setString(9, "%");
			}

			// System.out.println("graphId--" + graphId);
			callableStatement.registerOutParameter(10, Types.VARCHAR);
			callableStatement.execute();

			response = callableStatement.getString(10);

		} catch (SQLException se) {
			se.printStackTrace();
		} finally {
			try {

				if (callableStatement != null) {
					callableStatement.close();
					callableStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		return response;
	}

	public String getUtilizationDetails(String empId, HttpServletRequest httpServletRequest)
			throws ServiceLocatorException {
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		Connection connection = null;
		String response = "";

		try {
			connection = ConnectionProvider.getInstance().getConnection();

			String query = "  SELECT PrjName,Utilization FROM tblEmpStateHistory1 WHERE ProjectStatus='Active' AND EmpId='"
					+ empId + "';";

			preparedStatement = connection.prepareStatement(query);
			resultSet = preparedStatement.executeQuery();
			String prjName = "";
			String util = "";
			while (resultSet.next()) {

				if (!"".equalsIgnoreCase(resultSet.getString("PrjName")) && resultSet.getString("PrjName") != null) {

					prjName = resultSet.getString("PrjName");
				} else {
					prjName = "-";
				}
				if (!"0".equalsIgnoreCase(resultSet.getString("Utilization"))
						&& resultSet.getString("Utilization") != null) {

					util = resultSet.getString("Utilization");
				} else {
					util = "-";
				}
				response = response + prjName + "#^$" + util + "*@!";

			}
			if (response.equals("")) {
				response = "NoData";
			}

		} catch (SQLException sle) {
			sle.printStackTrace();
		} catch (ServiceLocatorException sle) {
			sle.printStackTrace();
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
		}
		// System.out.println("Team: "+reportsToBuffer.toString());
		return response;
	}

	public String getEmployeeVerificationDetails(HttpServletRequest httpServletRequest, int infoId) throws Exception {
		String message = "";
		boolean isUpdated = false;
		JSONObject jb = new JSONObject();
		try {
			List qmeetAttendeeList = (List) httpServletRequest.getSession(false).getAttribute("websiteInfoList");

			Map attendeeDetails;
			// System.out.println("qmeetAttendeeList-->"+qmeetAttendeeList.size());
			for (int i = 0; i < qmeetAttendeeList.size(); i++) {
				attendeeDetails = (Map) qmeetAttendeeList.get(i);

				if (attendeeDetails.get("ID").toString().equals(String.valueOf(infoId))) {
					// System.out.println("info-->"+infoId);
					Iterator entries = attendeeDetails.entrySet().iterator();
					while (entries.hasNext()) {
						Entry thisEntry = (Entry) entries.next();
						jb.put((String) thisEntry.getKey(), (String) thisEntry.getValue());

					}
				}
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return jb.toString();
	}

	/* Release Notes Start */

	public String popupSubTitleDescWindow(int releaseId) throws ServiceLocatorException {
		String subTitleDesc = "";
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		// queryString = "SELECT Description FROM tblCrmActivity WHERE
		// Id="+accId;

		try {
			connection = ConnectionProvider.getInstance().getConnection();
			preparedStatement = connection
					.prepareStatement("SELECT SubTitle FROM tblReleaseNotesLines WHERE RelId=" + releaseId);

			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {

				subTitleDesc = subTitleDesc + resultSet.getString("SubTitle") + "*@!";
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		} catch (ServiceLocatorException sle) {
			sle.printStackTrace();
		} finally {
			try {

				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
		}

		return subTitleDesc;
	}

	public String popupPurposeDescWindow(int releaseId) throws ServiceLocatorException {
		String purposeDesc = "";
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		try {
			connection = ConnectionProvider.getInstance().getConnection();
			preparedStatement = connection
					.prepareStatement("SELECT Purpose FROM tblReleaseNotesLines WHERE RelId=" + releaseId);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {

				purposeDesc = purposeDesc + resultSet.getString("Purpose") + "*@!";
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		} catch (ServiceLocatorException sle) {
			sle.printStackTrace();
		} finally {
			try {

				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
		}

		return purposeDesc;
	}

	public String getReleaseNotesData(NewAjaxHandlerAction ajaxHandlerAction, HttpServletRequest httpServletRequest)
			throws ServiceLocatorException {
		// String qsTitle = "";
		Statement statement = null;
		ResultSet resultSet = null;
		String query = "";
		Connection connection = null;
		String response = "";

		try {
			connection = ConnectionProvider.getInstance().getConnection();
			query = "SELECT DISTINCT YEAR(DATE) AS ReleaseNoteYear FROM tblReleaseNotes ORDER BY YEAR(DATE) DESC";
			statement = connection.createStatement();
			resultSet = statement.executeQuery(query);
			while (resultSet.next()) {

				response = response + resultSet.getString("ReleaseNoteYear") + "*@!";
			}
		} catch (Exception sqe) {
			sqe.printStackTrace();
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (statement != null) {
					statement.close();
					statement = null;
				}

				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException sqle) {
				sqle.printStackTrace();
			}
		}

		return response;
	}

	public String getReleaseNotesMonthData(NewAjaxHandlerAction ajaxHandlerAction,
			HttpServletRequest httpServletRequest) throws ServiceLocatorException {
		// String qsTitle = "";
		Statement statement = null;
		ResultSet resultSet = null;
		String query = "";
		Connection connection = null;
		String response = "";

		try {
			connection = ConnectionProvider.getInstance().getConnection();
			query = "SELECT DISTINCT MONTHNAME(DATE) AS ReleaseNoteMonth FROM tblReleaseNotes WHERE YEAR(DATE) = '"
					+ ajaxHandlerAction.getYear() + "' ";
			statement = connection.createStatement();
			resultSet = statement.executeQuery(query);
			while (resultSet.next()) {

				response = response + resultSet.getString("ReleaseNoteMonth") + "*@!";
			}
		} catch (Exception sqe) {
			sqe.printStackTrace();
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (statement != null) {
					statement.close();
					statement = null;
				}

				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException sqle) {
				sqle.printStackTrace();
			}
		}

		return response;
	}

	public String getReleaseNotesTitleInfo(NewAjaxHandlerAction ajaxHandlerAction,
			HttpServletRequest httpServletRequest) throws ServiceLocatorException {
		// String qsTitle = "";
		Statement statement = null;
		ResultSet resultSet = null;
		String query = "";
		Connection connection = null;
		String response = "";
		String RolesAccess = "";
		Map AssignedRoles;
		try {
			connection = ConnectionProvider.getInstance().getConnection();
			// query="SELECT DISTINCT MONTHNAME(DATE) AS ReleaseNoteMonth FROM
			// tblReleaseNotes WHERE YEAR(DATE) =
			// '"+ajaxHandlerAction.getYear()+"' ";
			String empId = httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_EMP_ID)
					.toString();
			// System.out.println("empId====>"+empId);
			AssignedRoles = DataSourceDataProvider.getInstance().getUserRolesDesc(Integer.parseInt(empId));
			RolesAccess = getKeys(AssignedRoles, ",");
			RolesAccess = RolesAccess.replaceAll("\\'", "");
			// System.out.println("RolesAccess====>"+RolesAccess);
			query = "SELECT Id,tblReleaseNotes.Title As ReleaseTitle,tblReleaseNotes.RolesAccess As ReleaseRolesAccess FROM tblReleaseNotes WHERE YEAR(DATE) = '"
					+ ajaxHandlerAction.getYear() + "' AND MONTHNAME(DATE)='" + ajaxHandlerAction.getMonth() + "' ";
			statement = connection.createStatement();
			resultSet = statement.executeQuery(query);
			while (resultSet.next()) {

				String roles[] = resultSet.getString("ReleaseRolesAccess").split(",");
				String accessibleRoles[] = RolesAccess.split(",");
				for (int j = 0; j < roles.length; j++) {
					for (int k = 0; k < accessibleRoles.length; k++) {

						if (accessibleRoles[k].equals(roles[j])) {

							response = response + resultSet.getString("Id") + "#^$"
									+ resultSet.getString("ReleaseTitle") + "*@!";
						}
					}
				}
			}
		} catch (Exception sqe) {
			sqe.printStackTrace();
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (statement != null) {
					statement.close();
					statement = null;
				}

				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException sqle) {
				sqle.printStackTrace();
			}
		}

		return response;
	}

	public String getReleaseNotesInfoData(NewAjaxHandlerAction ajaxHandlerAction, HttpServletRequest httpServletRequest)
			throws ServiceLocatorException {
		// String qsTitle = "";
		Statement statement = null;
		ResultSet resultSet = null;
		String query = "";
		Connection connection = null;
		String response = "";

		try {
			connection = ConnectionProvider.getInstance().getConnection();
			query = "SELECT tblReleaseNotesLines.Id AS SubId,tblReleaseNotes.DATE,tblReleaseNotesLines.SubTitle,tblReleaseNotesLines.Purpose,tblReleaseNotesLines.FilePath,";
			query = query + "tblReleaseNotesLines.FileName FROM tblReleaseNotes LEFT OUTER JOIN tblReleaseNotesLines ";
			query = query + "ON(tblReleaseNotes.Id=tblReleaseNotesLines.RelId) WHERE RelId='"
					+ ajaxHandlerAction.getReleaseId() + "'";
			statement = connection.createStatement();
			resultSet = statement.executeQuery(query);

			while (resultSet.next()) {

				response = response + resultSet.getString("SubId") + "#^$" + resultSet.getString("SubTitle") + "#^$"
						+ resultSet.getString("Purpose") + "#^$" + resultSet.getString("FilePath") + "#^$"
						+ resultSet.getString("FileName") + "*@!";
			}
		} catch (Exception sqe) {
			sqe.printStackTrace();
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (statement != null) {
					statement.close();
					statement = null;
				}

				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException sqle) {
				sqle.printStackTrace();
			}
		}

		return response;
	}
	/* Release Notes End */

	/* Time sheet audit start */

	/*
	 * Author : AnandPatnaik Date : 10-31-2017
	 */
	public String doGetCustomerDetailsByCostModel(String costModel, String pmo) throws ServiceLocatorException {

		String totalStream = "";
		String query = "";
		String pmoList = "";

		String responseString = "";
		int updatedRows = 0;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		JSONObject jObject = null;
		JSONObject jsonResult = null;
		JSONArray jsonArray = new JSONArray();
		JSONArray jsonHeader = new JSONArray();
		JSONObject mainJson = null;
		try {

			connection = ConnectionProvider.getInstance().getConnection();

			query = "SELECT DISTINCT tblCrmAccount.Id AS Id,tblCrmAccount.NAME AS CustomerName FROM tblCrmAccount INNER JOIN tblProjects ON(tblCrmAccount.Id=tblProjects.CustomerId) WHERE 1=1 ";
			if (!"".equals(costModel) && !"-1".equals(costModel) && !"0".equals(costModel)) {

				query += " AND CostModel='" + costModel + "' ";
			}
			if (!"".equals(pmo)) {
				query += " AND FIND_IN_SET(PMO,'" + pmo + "') ";
			}
			query += " ORDER BY tblCrmAccount.NAME";

			preparedStatement = connection.prepareStatement(query);
			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				jsonResult = new JSONObject();
				jsonResult.put("Id", resultSet.getInt("Id"));
				jsonResult.put("CustomerName", resultSet.getString("CustomerName"));
				jsonArray.put(jsonResult);
			}

			mainJson = new JSONObject();

			mainJson.put("data", jsonArray);

		} catch (Exception sqe) {
			sqe.printStackTrace();
		} finally {
			try {

				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException sqle) {
			}
		}
		return mainJson.toString();
	}

	/**************************************************
	 * *Employee list for TImesheet audit author: anand patnaik Date :
	 * 09-27-2017
	 ************************************************/
	public String doGetEmployeeForTimeSheetHrs(String empName) throws ServiceLocatorException {
		boolean isGetting = false;
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		StringBuffer sb = new StringBuffer();
		String query = null;
		try {

			query = "SELECT CONCAT(TRIM(FName),'.',TRIM(LName)) AS FullName,Id FROM tblEmployee "
					+ "WHERE (LName LIKE '" + empName + "%' OR FName LIKE '" + empName + "%') "
					+ "AND CurStatus='Active' ORDER BY FullName";

			// System.out.println("query-->"+query);
			connection = ConnectionProvider.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(query);
			resultSet = preparedStatement.executeQuery();

			int count = 0;
			sb.append("<xml version=\"1.0\">");
			sb.append("<EMPLOYEES>");
			while (resultSet.next()) {
				sb.append("<EMPLOYEE><VALID>true</VALID>");

				if (resultSet.getString(1) == null || resultSet.getString(1).equals("")) {
					sb.append("<NAME>NoRecord</NAME>");
				} else {
					String title = resultSet.getString(1);
					if (title.contains("&")) {
						title = title.replace("&", "&amp;");
					}
					sb.append("<NAME>" + title + "</NAME>");
				}
				// sb.append("<NAME>" +resultSet.getString(1) + "</NAME>");
				sb.append("<EMPID>" + resultSet.getInt(2) + "</EMPID>");
				sb.append("</EMPLOYEE>");
				isGetting = true;
				count++;
			}

			if (!isGetting) {
				// sb.append("<EMPLOYEES>" + sb.toString() + "</EMPLOYEES>");
				// } else {
				isGetting = false;
				// nothing to show
				// response.setStatus(HttpServletResponse.SC_NO_CONTENT);
				sb.append("<EMPLOYEE><VALID>false</VALID></EMPLOYEE>");
			}
			sb.append("</EMPLOYEES>");
			sb.append("</xml>");

			// System.out.println(sb.toString());
		} catch (SQLException sqle) {
			throw new ServiceLocatorException(sqle);
		} finally {
			try {
				if (resultSet != null) {

					resultSet.close();
					resultSet = null;
				}
				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}

				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException sql) {
			}

		}
		return sb.toString();
	}

	public String doGetLoadForAudit(String jsonData) throws ServiceLocatorException {

		String totalStream = "";
		String query = "";
		String pmoList = "";

		String responseString = "";
		int updatedRows = 0;
		CallableStatement callableStatement = null;

		JSONObject jObject = null;
		try {
			jObject = new JSONObject(jsonData);

			String customerId = jObject.getString("customerId");
			int year = jObject.getInt("year");
			int month = jObject.getInt("month");
			String ProjectId = jObject.getString("projectId");
			String costModel = jObject.getString("costModel");
			String empId = jObject.getString("empId");
			connection = ConnectionProvider.getInstance().getConnection();

			if (ProjectId.equals("") || ProjectId.equals("-1") || ProjectId.equals("0")) {
				ProjectId = "";
			}
			if (costModel.equals("") || costModel.equals("-1") || costModel.equals("0")) {
				costModel = "";
			}

			callableStatement = connection.prepareCall("{call spEmpTimeSheetInvoiceHrsDetails(?,?,?,?,?,?,?)}");

			callableStatement.setString(1, customerId);
			callableStatement.setInt(2, year);
			callableStatement.setInt(3, month);

			callableStatement.setString(4, ProjectId);
			callableStatement.setString(5, costModel);
			callableStatement.setString(6, empId);
			callableStatement.registerOutParameter(7, Types.VARCHAR);
			callableStatement.executeQuery();
			responseString = callableStatement.getString(7);
		} catch (Exception sqe) {
			sqe.printStackTrace();
		} finally {
			try {

				if (callableStatement != null) {
					callableStatement.close();
					callableStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException sqle) {
			}
		}
		return responseString;
	}
	/*
	 * Author Anand Patnaik Date : 30-10-2017
	 */

	public String doSaveInvoiceDetails(String invoiceDetails, String createdBy) throws ServiceLocatorException {

		String totalStream = "";
		PreparedStatement preparedStatement = null;
		String query = "";
		String pmoList = "";

		String responseString = "";
		int updatedRows = 0;
		JSONObject jObject = null;
		JSONObject jObjectchild = null;
		try {

			connection = ConnectionProvider.getInstance().getConnection();
			jObject = new JSONObject(invoiceDetails);
			Iterator<?> keys = jObject.keys();
			query = "INSERT INTO tblTimeSheetsAuditDetails(CustomerId, ProjectId, EmpId, CostModel, MONTH, YEAR, ActualHrsPerUtilization,ProjectHrs,InvoiceHrs, IsInvoiced, AuditedBy, AuditedDate,Comments	,WorkingHrs,EmpStatus) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			preparedStatement = connection.prepareStatement(query);
			while (keys.hasNext()) {
				String key = (String) keys.next();
				if (jObject.get(key) instanceof JSONObject) {
					jObjectchild = (JSONObject) jObject.get(key);
					int CustomerId = jObjectchild.getInt("newCustomerId");
					int ProjectId = jObjectchild.getInt("newProjectId");
					int empId = jObjectchild.getInt("newEmpId");
					String ProjectType = jObjectchild.getString("newCostModel");
					int month = jObjectchild.getInt("newMonth");
					int year = jObjectchild.getInt("newYear");
					double ProjectHrs = jObjectchild.getDouble("projectHrs");
					double InvoicedHours = jObjectchild.getDouble("invoiceHrs");

					double hrsPerUtilization = jObjectchild.getDouble("hrsPerUtilization");
					String comments = jObjectchild.getString("comments");
					String EmpStatus = jObjectchild.getString("EmpStatus");
					preparedStatement.setInt(1, CustomerId);
					preparedStatement.setInt(2, ProjectId);
					preparedStatement.setInt(3, empId);
					preparedStatement.setString(4, ProjectType);
					preparedStatement.setInt(5, month);
					preparedStatement.setInt(6, year);
					preparedStatement.setDouble(7, hrsPerUtilization);
					preparedStatement.setDouble(8, ProjectHrs);
					preparedStatement.setDouble(9, InvoicedHours);
					preparedStatement.setInt(10, 1);
					preparedStatement.setString(11, createdBy);
					preparedStatement.setTimestamp(12, DateUtility.getInstance().getCurrentMySqlDateTime());
					preparedStatement.setString(13, comments);
					String hrs = DataSourceDataProvider.getInstance().getonthlyWorkinghrsAndHrsPerUtilization(year,
							month, ProjectId + "", empId + "");

					preparedStatement.setDouble(14, Double.parseDouble(hrs.split(Pattern.quote("#^$"))[0]));
					preparedStatement.setString(15, EmpStatus);
					preparedStatement.executeUpdate();
				}
			}

			responseString = "success";
		} catch (Exception sqe) {
			sqe.printStackTrace();
		} finally {
			try {
				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException sqle) {
			}
		}
		return responseString;
	}

	/*
	 * Author Anand Patnaik Date 10-1-2017
	 */
	public String doGetLoadTimeSheetDataForAudit(String jsonData) throws ServiceLocatorException {

		String totalStream = "";
		String query = "";
		String pmoList = "";

		String responseString = "";
		int updatedRows = 0;
		CallableStatement callableStatement = null;

		JSONObject jObject = null;
		JSONObject jsonResult = null;
		JSONArray jsonArray = new JSONArray();
		JSONArray jsonHeader = new JSONArray();
		JSONObject mainJson = null;
		try {
			jObject = new JSONObject(jsonData);

			String customerId = jObject.getString("customerId");
			int year = jObject.getInt("year");
			int month = jObject.getInt("month");
			String ProjectId = jObject.getString("projectId");

			int empId = jObject.getInt("empId");
			connection = ConnectionProvider.getInstance().getConnection();

			if (ProjectId.equals("") || ProjectId.equals("-1") || ProjectId.equals("0")) {
				ProjectId = "0";
			}
			Set set = DataSourceDataProvider.getInstance().getProjectListForTimeSheetAudit(empId, year, month);

			String projectList = "";
			Iterator<String> itr = set.iterator();
			while (itr.hasNext()) {
				projectList = projectList + itr.next() + ",";
			}
			if (projectList.length() > 0) {
				projectList = projectList.substring(0, projectList.length() - 1);
			}
			callableStatement = connection.prepareCall("{call spEmpTimeSheetDaybyDayDetails(?,?,?,?,?,?,?,?,?)}");

			callableStatement.setString(1, customerId);
			callableStatement.setInt(2, year);
			callableStatement.setInt(3, month);

			callableStatement.setString(4, ProjectId);
			callableStatement.setInt(5, empId);
			callableStatement.setInt(6, set.size());
			callableStatement.setString(7, projectList);
			callableStatement.registerOutParameter(8, Types.VARCHAR);
			callableStatement.registerOutParameter(9, Types.VARCHAR);
			callableStatement.executeQuery();
			responseString = callableStatement.getString(8);
			String headreString = callableStatement.getString(9);

			if (!headreString.equals("")) {
				String result[] = headreString.split(Pattern.quote("*@!"));
				for (int i = 0; i < result.length; i++) {
					String rowData[] = result[i].split(Pattern.quote("#^$"));
					jsonResult = new JSONObject();
					for (int j = 0; j < rowData.length; j++) {
						jsonHeader.put(rowData[j]);
					}

				}
			}
			mainJson = new JSONObject();
			mainJson.put("header", jsonHeader);
			mainJson.put("data", jsonArray);
			mainJson.put("dataNew", responseString);
		} catch (Exception sqe) {
			sqe.printStackTrace();
		} finally {
			try {

				if (callableStatement != null) {
					callableStatement.close();
					callableStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException sqle) {
			}
		}
		return mainJson.toString();
	}

	public String doGetTimeSheetAuditSearch(String jsonData, String pmo) throws ServiceLocatorException {

		String totalStream = "";
		String query = "";
		String pmoList = "";

		String responseString = "";
		int updatedRows = 0;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		JSONObject jObject = null;
		JSONObject jsonResult = null;
		JSONArray jsonArray = new JSONArray();
		try {
			jObject = new JSONObject(jsonData);

			String customerId = jObject.getString("customerId");
			int year = jObject.getInt("year");
			int month = jObject.getInt("month");
			String ProjectId = jObject.getString("projectId");
			String costModel = jObject.getString("costModel");
			String empId = jObject.getString("empId");
			connection = ConnectionProvider.getInstance().getConnection();

			query = "SELECT tblTimeSheetsAuditDetails.Id,tblCrmAccount.NAME,ProjectName,tblProjects.CostModel,CONCAT(tblEmployee.Fname,' ',tblEmployee.Mname,' ',tblEmployee.Lname) AS empName,tblEmployee.WorkingCountry,tblTimeSheetsAuditDetails.YEAR,MONTHNAME(STR_TO_DATE(tblTimeSheetsAuditDetails.MONTH, '%m')) AS MonthName,.tblTimeSheetsAuditDetails.Month,"
					+ "tblTimeSheetsAuditDetails.EmpId,tblTimeSheetsAuditDetails.CustomerId,tblTimeSheetsAuditDetails.ProjectId,tblTimeSheetsAuditDetails.ActualHrsPerUtilization,tblTimeSheetsAuditDetails.ProjectHrs,tblTimeSheetsAuditDetails.InvoiceHrs,tblTimeSheetsAuditDetails.Comments,CONCAT(tblEmployee1.Fname,' ',tblEmployee1.Mname,' ',tblEmployee1.Lname) AS AuditedBy,tblTimeSheetsAuditDetails.EmpStatus FROM tblTimeSheetsAuditDetails "
					+ " LEFT JOIN tblEmployee ON (tblTimeSheetsAuditDetails.EmpId=tblEmployee.Id) "
					+ " LEFT JOIN tblEmployee tblEmployee1 ON (tblTimeSheetsAuditDetails.AuditedBy=tblEmployee1.LoginId) "
					+ " LEFT JOIN tblCrmAccount ON (tblTimeSheetsAuditDetails.CustomerId=tblCrmAccount.Id) "
					+ " LEFT JOIN tblProjects ON (tblTimeSheetsAuditDetails.ProjectId=tblProjects.Id) where year="
					+ year + " And Month=" + month;

			if (customerId != null && !"".equals(customerId) && !"-1".equals(customerId) && !"0".equals(customerId)) {
				query += " AND tblTimeSheetsAuditDetails.CustomerId=" + customerId;
			}
			if (ProjectId != null && !"".equals(ProjectId) && !"-1".equals(ProjectId) && !"0".equals(ProjectId)) {
				query += " AND tblTimeSheetsAuditDetails.ProjectId=" + ProjectId;
			}
			if (costModel != null && !"".equals(costModel) && !"-1".equals(costModel) && !"0".equals(costModel)) {
				query += " AND tblProjects.costModel='" + costModel + "'";
			}
			if (empId != null && !"".equals(empId)) {
				query += " AND tblTimeSheetsAuditDetails.EmpId=" + empId;
			}
			if (!"".equals(pmo)) {
				query += " AND FIND_IN_SET(PMO,'" + pmo + "') ";

			}

			query += " ORDER BY empName";
			preparedStatement = connection.prepareStatement(query);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				jsonResult = new JSONObject();
				jsonResult.put("Id", resultSet.getString("Id"));
				jsonResult.put("customerName", resultSet.getString("Name"));
				jsonResult.put("ProjectName", resultSet.getString("ProjectName"));
				jsonResult.put("CostModel", resultSet.getString("CostModel"));
				jsonResult.put("empName", resultSet.getString("empName"));
				jsonResult.put("Year", resultSet.getString("Year"));
				jsonResult.put("MonthName", resultSet.getString("MonthName"));
				jsonResult.put("Month", resultSet.getString("Month"));
				jsonResult.put("MonthName", resultSet.getString("MonthName"));
				jsonResult.put("EmpId", resultSet.getString("EmpId"));
				jsonResult.put("CustomerId", resultSet.getString("CustomerId"));
				jsonResult.put("ProjectId", resultSet.getString("ProjectId"));
				jsonResult.put("ProjectHrs", resultSet.getString("ProjectHrs"));
				jsonResult.put("InvoiceHrs", resultSet.getString("InvoiceHrs"));
				jsonResult.put("ActualHrsPerUtilization", resultSet.getString("ActualHrsPerUtilization"));
				jsonResult.put("Comments", resultSet.getString("Comments"));
				jsonResult.put("WorkingCountry", resultSet.getString("WorkingCountry"));
				jsonResult.put("AuditedBy", resultSet.getString("AuditedBy"));
				jsonResult.put("EmpStatus", resultSet.getString("EmpStatus"));

				jsonArray.put(jsonResult);
			}
			responseString = jsonArray.toString();
		} catch (Exception sqe) {
			sqe.printStackTrace();
		} finally {
			try {

				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException sqle) {
			}
		}
		return responseString;
	}

	public String doUpdateAuditComment(String jsonData, String createdBy) throws ServiceLocatorException {

		String totalStream = "";
		PreparedStatement preparedStatement = null;
		String query = "";
		String pmoList = "";

		String responseString = "";
		int updatedRows = 0;
		JSONObject jObject = null;
		JSONObject jObjectchild = null;
		try {

			connection = ConnectionProvider.getInstance().getConnection();
			jObject = new JSONObject(jsonData);
			String comments = jObject.getString("comment");
			int auditId = jObject.getInt("auditId");
			query = "Update  tblTimeSheetsAuditDetails set Comments=?,ModifiedBy=?,ModifiedDate=? where Id=?";
			preparedStatement = connection.prepareStatement(query);

			preparedStatement.setString(1, comments);
			preparedStatement.setString(2, createdBy);
			preparedStatement.setTimestamp(3, DateUtility.getInstance().getCurrentMySqlDateTime());
			preparedStatement.setInt(4, auditId);
			preparedStatement.executeUpdate();
			responseString = "success";
		} catch (Exception sqe) {
			sqe.printStackTrace();
		} finally {
			try {
				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException sqle) {
			}
		}
		return responseString;
	}
	/* Time sheet audit end */

	public String doGetEmployeeNames(String query, int empId) throws ServiceLocatorException {
		boolean isGetting = false;
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		StringBuffer sb = new StringBuffer();
		List empExpencesDetails = new ArrayList();
		try {
			connection = ConnectionProvider.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(query);
			resultSet = preparedStatement.executeQuery();

			int count = 0;
			sb.append("<xml version=\"1.0\">");
			sb.append("<EMPLOYEES>");
			while (resultSet.next()) {
				sb.append("<EMPLOYEE><VALID>true</VALID>");

				if (resultSet.getString(1) == null || resultSet.getString(1).equals("")) {
					sb.append("<NAME>NoRecord</NAME>");
				} else {
					String title = resultSet.getString(1);
					if (title.contains("&")) {
						title = title.replace("&", "&amp;");
					}
					sb.append("<NAME>" + title + "</NAME>");
				}
				// sb.append("<NAME>" +resultSet.getString(1) + "</NAME>");
				sb.append("<EMPLOGINID>" + resultSet.getString(2) + "</EMPLOGINID>");
				sb.append("<EMPID>" + resultSet.getString(3) + "</EMPID>");
				sb.append("<EMPNO>" + resultSet.getString(4) + "</EMPNO>");
				if (!resultSet.getString(5).equals("") && resultSet.getString(5) != null) {
					sb.append("<DEPTID>" + resultSet.getString(5) + "</DEPTID>");
				} else {
					sb.append("<DEPTID> </DEPTID>");
				}
				if (!resultSet.getString(6).equals("") && resultSet.getString(6) != null) {
					sb.append("<TITLEID>" + resultSet.getString(6) + "</TITLEID>");
				} else {
					sb.append("<TITLEID> </TITLEID>");
				}

				sb.append("</EMPLOYEE>");
				isGetting = true;
				count++;
			}

			if (!isGetting) {
				// sb.append("<EMPLOYEES>" + sb.toString() + "</EMPLOYEES>");
				// } else {
				isGetting = false;
				// nothing to show
				// response.setStatus(HttpServletResponse.SC_NO_CONTENT);
				sb.append("<EMPLOYEE><VALID>false</VALID></EMPLOYEE>");
			}
			sb.append("</EMPLOYEES>");
			sb.append("</xml>");
		} catch (SQLException sqle) {
			throw new ServiceLocatorException(sqle);
		} finally {
			try {
				if (resultSet != null) {

					resultSet.close();
					resultSet = null;
				}
				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}

				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException sql) {
				// System.err.print("Error :"+sql);
			}

		}
		// System.out.println(sb.toString());
		return sb.toString();
	}

	/**
	 * This method is to show Stacked Chart for Employee Count based on
	 * countries Created By: Triveni Niddara Created Date : 11/5/2017
	 * 
	 * @return
	 */
	@Override
	public String getEmpCountryCount() throws ServiceLocatorException {
		String response = "";
		// Connection connection = null;
		CallableStatement callableStatement = null;
		Connection connection = null;
		try {
			connection = ConnectionProvider.getInstance().getConnection();
			callableStatement = connection.prepareCall("{call spEmpCountryCountStackedChart(?)}");

			callableStatement.registerOutParameter(1, Types.VARCHAR);
			callableStatement.execute();

			response = callableStatement.getString(1);
			// System.out.println("response------>"+response);

		} catch (SQLException se) {
			se.printStackTrace();
		} finally {
			try {

				if (callableStatement != null) {
					callableStatement.close();
					callableStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		return response;
	}

	/**
	 * This method is to show Pie Chart for Employee Count based on Practices
	 * Created By: Triveni Niddara Created Date : 11/5/2017
	 * 
	 * @return
	 */
	@Override
	public String getEmpPracticeCount(String country, String category) throws ServiceLocatorException {
		String response = "";
		// Connection connection = null;
		CallableStatement callableStatement = null;
		Connection connection = null;

		// System.out.println("country------>"+country);

		// System.out.println("category------>"+category);
		try {
			connection = ConnectionProvider.getInstance().getConnection();
			callableStatement = connection.prepareCall("{call spEmpPracticeCountPieChart(?,?,?)}");
			callableStatement.setString(1, country);
			callableStatement.setString(2, category);
			callableStatement.registerOutParameter(3, Types.VARCHAR);
			callableStatement.execute();

			response = callableStatement.getString(3);
			// System.out.println("response------>"+response);

		} catch (SQLException se) {
			se.printStackTrace();
		} finally {
			try {

				if (callableStatement != null) {
					callableStatement.close();
					callableStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		return response;
	}

	/**
	 * This method is to show Pie Chart for Employee Count based on SubPractices
	 * Created By: Triveni Niddara Created Date : 11/5/2017
	 * 
	 * @return
	 */
	@Override
	public String getEmpSubPracticeCount(String country, String category, String practiceId)
			throws ServiceLocatorException {
		String response = "";
		// Connection connection = null;
		CallableStatement callableStatement = null;
		Connection connection = null;

		// System.out.println("country------>"+country);

		// System.out.println("category------>"+category);
		try {
			connection = ConnectionProvider.getInstance().getConnection();
			callableStatement = connection.prepareCall("{call spEmpSubPracticeCountPieChart(?,?,?,?)}");
			if (practiceId != null && !"".equals(practiceId)) {
				// System.out.println("practiceId"+practiceId);
				callableStatement.setString(1, practiceId);
			} else {
				callableStatement.setString(1, "%");
			}
			callableStatement.setString(2, country);
			callableStatement.setString(3, category);

			callableStatement.registerOutParameter(4, Types.VARCHAR);
			callableStatement.execute();

			response = callableStatement.getString(4);
			// System.out.println("response------>"+response);

		} catch (SQLException se) {
			se.printStackTrace();
		} finally {
			try {

				if (callableStatement != null) {
					callableStatement.close();
					callableStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		return response;
	}

	/*
	 * @Override public String getEmpStatePieChart(String country, String
	 * category,String practiceId, String subPractice) throws
	 * ServiceLocatorException { String response = ""; // Connection connection
	 * = null; CallableStatement callableStatement = null; Connection connection
	 * = null; try { connection =
	 * ConnectionProvider.getInstance().getConnection(); callableStatement =
	 * connection.prepareCall("{call spEmpStateCountPieChart(?,?,?,?,?)}");
	 * callableStatement.setString(1,practiceId);
	 * callableStatement.setString(2,country);
	 * callableStatement.setString(3,category);
	 * callableStatement.setString(4,subPractice);
	 * callableStatement.registerOutParameter(5, Types.VARCHAR);
	 * callableStatement.execute();
	 * 
	 * 
	 * response = callableStatement.getString(5);
	 * 
	 * System.out.println("response------>"+response);
	 * 
	 * } catch (SQLException se) { se.printStackTrace(); } finally { try {
	 * 
	 * if (callableStatement != null) { callableStatement.close();
	 * callableStatement = null; } if (connection != null) { connection.close();
	 * connection = null; } } catch (SQLException se) { se.printStackTrace(); }
	 * } return response; }
	 * 
	 */
	/**
	 * This method is to Created By: Triveni Niddara Created Date : 11/5/2017
	 * 
	 * @return
	 */
	public String getEmployeeDetailsThroughState(String state, int graphId, String country, String departmentId,
			String practiceId, String subpractice) throws ServiceLocatorException {
		String response = "";
		// Connection connection = null;
		CallableStatement callableStatement = null;
		Connection connection = null;

		try {
			connection = ConnectionProvider.getInstance().getConnection();

			callableStatement = connection.prepareCall("{call spEmployeeDetailsThroughState(?,?,?,?,?,?,?)}");
			callableStatement.setInt(1, graphId);

			callableStatement.setString(2, state);

			callableStatement.setString(3, country);

			callableStatement.setString(4, departmentId);

			if (practiceId != null && !"".equals(practiceId)) {

				callableStatement.setString(5, practiceId);
			} else {
				callableStatement.setString(5, "%");
			}

			if (subpractice != null && !"".equals(subpractice)) {

				callableStatement.setString(6, subpractice);
			} else {
				callableStatement.setString(6, "%");
			}

			// System.out.println("graphId--" + graphId);
			callableStatement.registerOutParameter(7, Types.VARCHAR);
			callableStatement.execute();

			response = callableStatement.getString(7);
			// System.out.println("graphId---"+graphId+"country----->"+country+"departmentId---->"+departmentId+"state---"+state+"practiceId----->"+practiceId+"--->subpractice"+subpractice);
			// System.out.println("response--" + response);

		} catch (SQLException se) {
			se.printStackTrace();
		} finally {
			try {

				if (callableStatement != null) {
					callableStatement.close();
					callableStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		return response;
	}

	/**
	 * This method is to show Pie Chart for Employee Count based on CurStatus
	 * Created By: Triveni Niddara Created Date : 11/5/2017
	 * 
	 * @return
	 */

	@Override
	public String getEmpCurStatePieChart(int graphId, String country, String departmentId, String practiceId,
			String subpractice) throws ServiceLocatorException {

		String response = "";
		// Connection connection = null;
		CallableStatement callableStatement = null;
		Connection connection = null;

		try {
			connection = ConnectionProvider.getInstance().getConnection();
			callableStatement = connection.prepareCall("{call spEmpCurStateCountPieChart(?,?,?,?,?,?)}");
			callableStatement.setInt(1, graphId);
			callableStatement.setString(2, country);
			callableStatement.setString(3, departmentId);

			if (practiceId != null && !"".equals(practiceId)) {
				// System.out.println("practiceId"+practiceId);
				callableStatement.setString(4, practiceId);
			} else {
				callableStatement.setString(4, "%");
			}

			if (subpractice != null && !"".equals(subpractice)) {
				// System.out.println("subpractice"+subpractice);
				callableStatement.setString(5, subpractice);
			} else {
				callableStatement.setString(5, "%");
			}

			// System.out.println("graphId--" + graphId);
			callableStatement.registerOutParameter(6, Types.VARCHAR);
			callableStatement.execute();

			response = callableStatement.getString(6);
			// System.out.println("graphId---"+graphId+"country----->"+country+"departmentId---->"+departmentId+"practiceId----->"+practiceId+"--->subpractice"+subpractice);
			// System.out.println("response--" + response);

		} catch (SQLException se) {
			se.printStackTrace();
		} finally {
			try {

				if (callableStatement != null) {
					callableStatement.close();
					callableStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		return response;
	}

	/**
	 * This method is to Created By: Triveni Niddara Created Date : 11/5/2017
	 * 
	 * @return
	 */
	public String getUtilizationDetails(String empId) throws ServiceLocatorException {
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		Connection connection = null;
		String response = "";

		try {
			connection = ConnectionProvider.getInstance().getConnection();

			String query = "  SELECT PrjName,Utilization FROM tblEmpStateHistory1 WHERE ProjectStatus='Active' AND EmpId='"
					+ empId + "';";

			preparedStatement = connection.prepareStatement(query);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {

				response = response + resultSet.getString("PrjName") + "#^$" + resultSet.getString("Utilization")
						+ "*@!";

			}
			if (response.equals("")) {
				response = "NoData";
			}

			// System.out.println("response..."+response);

		} catch (SQLException sle) {
			sle.printStackTrace();
		} catch (ServiceLocatorException sle) {
			sle.printStackTrace();
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
		}
		// System.out.println("Team: "+reportsToBuffer.toString());
		return response;
	}

	public String getBiometricLoad(String empId, int month, int year, String reportType)
			throws ServiceLocatorException {
		Connection connection = null;

		CallableStatement callableStatement = null;

		String result = "";
		String queryString1 = "";
		String queryString2 = "";
		int insertedRows = 0;
		int updatedrows = 0;
		int count = 0;
		JSONObject jObject = new JSONObject();

		try {

			connection = ConnectionProvider.getInstance().getConnection();
			callableStatement = connection.prepareCall("{call spLoadBiometric(?,?,?,?,?,?) }");

			callableStatement.setInt(1, month);
			callableStatement.setInt(2, year);
			callableStatement.setString(3, empId);
			callableStatement.setString(4, reportType);
			callableStatement.registerOutParameter(5, Types.VARCHAR);
			callableStatement.execute();
			result = callableStatement.getString(5);
			System.out.println("result" + result);

			String empno = DataSourceDataProvider.getInstance().getEmpNoByEmpId(Integer.parseInt(empId));
			boolean isExist = DataSourceDataProvider.getInstance().getIfAtendanceLoadExists(empno,
					String.valueOf(month), String.valueOf(year));
			jObject.put("response", result);

			jObject.put("isExist", isExist);

			// resultMessage = "<font style='color:red;'>Can Proceed</font>";

		} catch (SQLException ex) {
			ex.printStackTrace();

		} catch (Exception sle) {
			sle.printStackTrace();
		} finally {
			try {
				if (callableStatement != null) {
					callableStatement.close();
					callableStatement = null;
				}

				if (connection != null) {
					connection.close();
					connection = null;
				}

			} catch (SQLException ex) {
				ex.printStackTrace();
			}
		}
		return jObject.toString();

	}

	public String getBioAttendanceDetails(String empId, String biometricDate, String status)
			throws ServiceLocatorException {
		Connection connection = null;

		CallableStatement callableStatement = null;

		String result = "";
		String resultString = "";
		String queryString2 = "";
		int insertedRows = 0;
		int updatedrows = 0;
		int count = 0;

		try {

			connection = ConnectionProvider.getInstance().getConnection();
			callableStatement = connection.prepareCall("{call spAttendanceDetails(?,?,?,?,?) }");

			callableStatement.setString(1, biometricDate);
			callableStatement.setString(2, empId);
			callableStatement.setString(3, status);
			callableStatement.registerOutParameter(4, Types.VARCHAR);
			callableStatement.execute();
			result = callableStatement.getString(4);

		} catch (SQLException ex) {
			ex.printStackTrace();

		} catch (ServiceLocatorException sle) {
			sle.printStackTrace();
		} finally {
			try {

				if (callableStatement != null) {
					callableStatement.close();
					callableStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}

			} catch (SQLException ex) {
				ex.printStackTrace();
			}
		}
		// System.out.println("for leave===>"+result);
		return result;

	}

	public String calculateAttendance(String empId, NewAjaxHandlerAction newAjaxHandlerAction) {

		Connection connection = null;
		PreparedStatement preparedStatement = null;
		PreparedStatement preparedStatement1 = null;
		boolean isUpdated = false;
		String queryString = "";
		String queryString1 = "";
		String resultMessage = "";
		try {

			String response = DataSourceDataProvider.getInstance().getEmpNoFNameLNameOrgId(empId);

			JSONObject result = new JSONObject(response);

			// JSONParser parser = new JSONParser();
			JSONObject data = (JSONObject) result;

			boolean isExists = DataSourceDataProvider.getInstance().getIfAtendanceLoadExists(data.getString("EmpNo"),
					newAjaxHandlerAction.getMonth(), newAjaxHandlerAction.getYear());

			connection = ConnectionProvider.getInstance().getConnection();

			if (isExists == true) {
				if (newAjaxHandlerAction.getReportType().equals("PayrollData")) {
					queryString = "UPDATE tblEmpAttendanceLaod SET DaysInMonth = ?,DaysWorked = ?, DaysVacation = ?, DaysHolidays = ?, DaysWeekends = ?, UpdatedBy = ?, UpdatedDate = ? WHERE PayRollId = ? AND MONTH(PayrollDate) = ? AND YEAR(PayrollDate) = ? ";
				}
			} else {
				queryString = "INSERT INTO tblEmpAttendanceLaod(DaysInMonth,DaysWorked,DaysVacation,DaysHolidays,DaysWeekends,UplaodedBy,UplaodedDate,PayRollId,PayrollDate,OrgId,FirstName,LastName) values(?,?,?,?,?,?,?,?,?,?,?,?)";
			}

			/*
			 * int month = Integer.parseInt(newAjaxHandlerAction.getMonth())+1;
			 * String payrollDate = newAjaxHandlerAction.getYear() + "-" + month
			 * + "-" + 10 ;
			 * 
			 * preparedStatement.setString(1,data.getString("EmpNo"));
			 * preparedStatement.setInt(2,
			 * DataSourceDataProvider.getInstance().getOrgIdByName(data.
			 * getString("OrgId"))) ;
			 * preparedStatement.setString(3,data.getString("FName"));
			 * preparedStatement.setString(4,data.getString("LName"));
			 * 
			 * preparedStatement.setString(5,payrollDate);
			 */
			if (!"".equals(queryString)) {
				preparedStatement = connection.prepareStatement(queryString);
				preparedStatement.setString(1, newAjaxHandlerAction.getDaysInMonth());
				preparedStatement.setString(2, newAjaxHandlerAction.getDaysWorked());
				preparedStatement.setString(3, newAjaxHandlerAction.getDaysVacation());
				preparedStatement.setString(4, newAjaxHandlerAction.getDaysHolidays());
				preparedStatement.setString(5, newAjaxHandlerAction.getDaysWeekends());
				// preparedStatement.setString(6,newAjaxHandlerAction.getDaysProject());
				preparedStatement.setString(6, newAjaxHandlerAction.getCreatedBy());
				preparedStatement.setString(7, DateUtility.getInstance().getCurrentSQLDate());
				preparedStatement.setString(8, data.getString("EmpNo"));
				if (isExists == true) {
					if (newAjaxHandlerAction.getReportType().equals("PayrollData")) {
						preparedStatement.setInt(9, Integer.parseInt(newAjaxHandlerAction.getMonth()));
						preparedStatement.setString(10, newAjaxHandlerAction.getYear());
					}
				} else {
					String payrollDate = newAjaxHandlerAction.getYear() + "-" + newAjaxHandlerAction.getMonth() + "-10";
					preparedStatement.setString(9, payrollDate);

					preparedStatement.setInt(10,
							DataSourceDataProvider.getInstance().getOrgIdByName(data.getString("OrgId")));
					preparedStatement.setString(11, data.getString("FName"));
					preparedStatement.setString(12, data.getString("LName"));
				}

				isUpdated = preparedStatement.execute();

				// preparedStatement.setString(1,releasedFor);
				// preparedStatement.setString(2,
				// DateUtility.getInstance().convertStringToMySQLDate(ajaxHandlerAction.getReleasedDate()));
				// preparedStatement.setString(3,
				// ajaxHandlerAction.getReleasedBy());
				// preparedStatement.setString(4,
				// ajaxHandlerAction.getStatus());
				// isUpdated = preparedStatement.execute();

				queryString1 = "update tblBioPayrollAttendanceLogs set ReviewFlag = ? where EmpNo= ? and MONTH(AttendanceDate) = ? and YEAR(AttendanceDate)= ?";

				preparedStatement1 = connection.prepareStatement(queryString1);
				preparedStatement1.setInt(1, 1);
				preparedStatement1.setString(2, data.getString("EmpNo"));
				preparedStatement1.setInt(3, Integer.parseInt(newAjaxHandlerAction.getMonth()));
				preparedStatement1.setString(4, newAjaxHandlerAction.getYear());
				preparedStatement1.execute();

				if (!isUpdated) {
					resultMessage = "success";
				} else {
					resultMessage = "error";
				}
			} else {
				resultMessage = "success";
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {

				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}
				if (preparedStatement1 != null) {
					preparedStatement1.close();
					preparedStatement1 = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
		}

		return resultMessage;

	}

	@Override
	public String updateAttendanceLoad(String empId, NewAjaxHandlerAction newAjaxHandlerAction)
			throws ServiceLocatorException {

		Connection connection = null;
		PreparedStatement preparedStatement = null;

		String result = "";
		boolean isUpdated = false;
		String queryString = "";
		String query = "";
		String maxDate = "";
		try {
			connection = ConnectionProvider.getInstance().getConnection();

			String empNo = DataSourceDataProvider.getInstance().getEmpNoByEmpId(Integer.parseInt(empId));

			queryString = "UPDATE tblBioPayrollAttendanceLogs SET STATUS = ? ,StatusCode = ?,Absent = ?,Present = ?,WeeklyOff = ?,Holiday = ?,Remarks = ?,ReviewedBy = ?,ReviewedDate = ? WHERE EmpNo = ? AND AttendanceDate = ? ";

			preparedStatement = connection.prepareStatement(queryString);
			preparedStatement.setString(1, newAjaxHandlerAction.getStatus());
			preparedStatement.setString(2, newAjaxHandlerAction.getStatusCode());
			if ("A".equals(newAjaxHandlerAction.getStatusCode())) {
				preparedStatement.setInt(3, '1');
			} else {
				preparedStatement.setInt(3, '0');
			}
			if ("P".equals(newAjaxHandlerAction.getStatusCode())) {
				preparedStatement.setInt(4, '1');
			} else {
				preparedStatement.setInt(4, '0');
			}
			if ("WO".equals(newAjaxHandlerAction.getStatusCode())) {
				preparedStatement.setInt(5, '1');
			} else {
				preparedStatement.setInt(5, '0');
			}
			if ("H".equals(newAjaxHandlerAction.getStatusCode())) {
				preparedStatement.setInt(6, '1');
			} else {
				preparedStatement.setInt(6, '0');
			}

			if (newAjaxHandlerAction.getComments() != null && !"".equals(newAjaxHandlerAction.getComments())) {
				preparedStatement.setString(7, newAjaxHandlerAction.getComments());

			} else {
				preparedStatement.setString(7, "");

			}

			preparedStatement.setString(8, newAjaxHandlerAction.getCreatedBy());
			preparedStatement.setTimestamp(9, newAjaxHandlerAction.getCreatedDate());

			preparedStatement.setString(10, empNo);
			// System.out.println("date1"+DateUtility.getInstance().convertStringToMySQLDate(newAjaxHandlerAction.getBiometricDate()));
			preparedStatement.setString(11, newAjaxHandlerAction.getBiometricDate());
			isUpdated = preparedStatement.execute();
			// System.out.println("isUpdated---updateAttendanceLoad--->"+isUpdated);
			if (isUpdated) {
				result = "<font style='color:green;font-size:15px;'>Updated Successfully</font>";
			} else {
				result = "<font style='color:green;font-size:15px;'>Please try again</font>";
			}

		} catch (SQLException se) {
			se.printStackTrace();
			throw new ServiceLocatorException(se);
		} finally {
			try {
				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException se) {
				throw new ServiceLocatorException(se);
			}
		}

		return result;

	}

	public String addSynAttendance(NewAjaxHandlerAction newAjaxHandlerAction, int empId, int isManager, int isAdmin) {

		// System.out.println("addSynAttendance");
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		boolean isUpdated = false;
		String queryString = "";
		String resultMessage = "";

		try {
			System.out.println(empId + "--" + isManager + "--" + isAdmin + "--"
					+ newAjaxHandlerAction.getSyncMonthOverlay() + "--" + newAjaxHandlerAction.getSyncYearOverlay());
			String responseString = DataSourceDataProvider.getInstance().getEmpDetailstoAddSyncAttendance(empId,
					isManager, isAdmin, newAjaxHandlerAction.getSyncMonthOverlay(),
					newAjaxHandlerAction.getSyncYearOverlay());

			/*
			 * String payrollId = response.split(Pattern.quote("#^$"))[0];
			 * 
			 * int count =
			 * Integer.parseInt(response.split(Pattern.quote("#^$"))[1].toString
			 * ());
			 * 
			 * String responseString =
			 * DataSourceDataProvider.getInstance().getEmpIdsFromAttendanceLoad(
			 * payrollId,
			 * newAjaxHandlerAction.getSyncMonthOverlay(),newAjaxHandlerAction.
			 * getSyncYearOverlay(),count);
			 * 
			 */
			if ("NoRecords".equals(responseString)) {
				String syncDate = newAjaxHandlerAction.getSyncYearOverlay() + "-"
						+ newAjaxHandlerAction.getSyncMonthOverlay() + "-" + 01;
				connection = ConnectionProvider.getInstance().getConnection();

				queryString = "insert into tblSyncAttendance(Date,Comments,SyncBy,SyncDate) values(?,?,?,?)";

				preparedStatement = connection.prepareStatement(queryString);

				preparedStatement.setString(1, syncDate);
				preparedStatement.setString(2, newAjaxHandlerAction.getComments());
				preparedStatement.setString(3, newAjaxHandlerAction.getCreatedBy());
				preparedStatement.setString(4, DateUtility.getInstance().getCurrentSQLDate());
				isUpdated = preparedStatement.execute();

				// preparedStatement.setString(1,releasedFor);
				// preparedStatement.setString(2,
				// DateUtility.getInstance().convertStringToMySQLDate(ajaxHandlerAction.getReleasedDate()));
				// preparedStatement.setString(3,
				// ajaxHandlerAction.getReleasedBy());
				// preparedStatement.setString(4,
				// ajaxHandlerAction.getStatus());
				// isUpdated = preparedStatement.execute();

				if (!isUpdated) {
					resultMessage = "success";
				} else {
					resultMessage = "error";
				}
			} else if ("NoData".equals(responseString)) {
				resultMessage = "NoData";
			} else {
				resultMessage = responseString;
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {

				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
		}

		return resultMessage;

	}

	@Override
	public String getEmpNameByLocation(String location) throws ServiceLocatorException {
		StringBuffer reportsToBuffer = new StringBuffer();
		String loginId = null;
		String topManagementPeople[] = null;
		boolean isTopManager = false;
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		String queryString = "";

		try {
			if (location == null || "".equals(location)) {
				location = "%";
			}
			queryString = "SELECT LoginId,CONCAT(TRIM(FName),'.',TRIM(LName)) AS EmployeeName FROM tblEmployee WHERE Location LIKE '"
					+ location + "' AND CurStatus='Active' AND Country='India' ORDER BY EmployeeName";

			connection = ConnectionProvider.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(queryString);
			resultSet = preparedStatement.executeQuery();
			reportsToBuffer.append("<xml version=\"1.0\">");
			reportsToBuffer.append("<TEAM Description=\"" + location + "\">");
			reportsToBuffer.append("<USER userId=\"\">All</USER>");

			while (resultSet.next()) {
				loginId = resultSet.getString("LoginId");

				reportsToBuffer.append("<USER userId=\"" + loginId + "\">");
				reportsToBuffer.append(resultSet.getString("EmployeeName"));
				reportsToBuffer.append("</USER>");

			}
			reportsToBuffer.append("</TEAM>");
			reportsToBuffer.append("</xml>");
		} catch (SQLException ex) {
			ex.printStackTrace();
		} catch (ServiceLocatorException sle) {
			sle.printStackTrace();
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
		}

		return reportsToBuffer.toString();
	}

	public String syncAttendance(String locationId, String empnameById, int month, int year, int syncBy)
			throws ServiceLocatorException {
		Connection connection = null;
		PreparedStatement prestatement = null;
		ResultSet resultSet = null;

		CallableStatement callableStatement = null;

		String result = "";
		String resultString = "";
		String queryString2 = "";
		int insertedRows = 0;
		int updatedrows = 0;
		int count = 0;

		try {

			connection = ConnectionProvider.getInstance().getConnection();
			callableStatement = connection.prepareCall("{call spSyncAttendance(?,?,?,?,?,?) }");

			callableStatement.setInt(1, month);
			callableStatement.setInt(2, year);
			callableStatement.setInt(3, syncBy);
			callableStatement.setString(4, locationId);
			// callableStatement.setString(5,locationId);
			callableStatement.registerOutParameter(5, Types.VARCHAR);
			callableStatement.execute();
			result = callableStatement.getString(5);

		} catch (SQLException ex) {
			ex.printStackTrace();

		} catch (ServiceLocatorException sle) {
			sle.printStackTrace();
		} finally {
			try {

				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (prestatement != null) {
					prestatement.close();
					prestatement = null;
				}
				if (callableStatement != null) {
					callableStatement.close();
					callableStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}

			} catch (SQLException ex) {
				ex.printStackTrace();
			}
		}
		return result;

	}

	public String getEmpFullNames(String empName, int empId, int isManager, int isAdmin)
			throws ServiceLocatorException {
		boolean isGetting = false;
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		StringBuffer sb = new StringBuffer();
		String query = null;
		try {

			// "SELECT LoginId,CONCAT(TRIM(FName),'.',TRIM(LName)) AS
			// EmployeeName FROM"
			// + " tblEmployee WHERE CurStatus='Active' AND DeletedFlag != 1
			// ORDER BY EmployeeName";
			//
			query = "SELECT CONCAT(TRIM(FName),'.',TRIM(LName)) AS EmployeeName,Id,EmpNo,LoginId FROM "
					+ " tblEmployee WHERE (LName LIKE '" + empName + "%' OR FName LIKE '" + empName + "%') "
					+ "AND CurStatus='Active' AND DeletedFlag != 1";
			if (isManager == 0 && isAdmin == 0) {
				query = query + " AND OpsContactId = " + empId + " AND CurStatus='Active' AND DeletedFlag != 1 ";
			} else {
				query = query + " AND CurStatus='Active' AND DeletedFlag != 1";
			}

			connection = ConnectionProvider.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(query);
			resultSet = preparedStatement.executeQuery();

			int count = 0;
			sb.append("<xml version=\"1.0\">");
			sb.append("<EMPLOYEES>");
			while (resultSet.next()) {
				sb.append("<EMPLOYEE><VALID>true</VALID>");

				if (resultSet.getString(1) == null || resultSet.getString(1).equals("")) {
					sb.append("<NAME>NoRecord</NAME>");
				} else {
					String title = resultSet.getString(1);
					if (title.contains("&")) {
						title = title.replace("&", "&amp;");
					}
					sb.append("<NAME>" + title + "</NAME>");
				}
				// sb.append("<NAME>" +resultSet.getString(1) + "</NAME>");
				sb.append("<EMPID>" + resultSet.getString(2) + "</EMPID>");
				sb.append("<EMPNO>" + resultSet.getString(3) + "</EMPNO>");
				sb.append("<LOGINID>" + resultSet.getString(4) + "</LOGINID>");
				sb.append("</EMPLOYEE>");
				isGetting = true;
				count++;
			}

			if (!isGetting) {
				// sb.append("<EMPLOYEES>" + sb.toString() + "</EMPLOYEES>");
				// } else {
				isGetting = false;
				// nothing to show
				// response.setStatus(HttpServletResponse.SC_NO_CONTENT);
				sb.append("<EMPLOYEE><VALID>false</VALID></EMPLOYEE>");
			}
			sb.append("</EMPLOYEES>");
			sb.append("</xml>");

			// System.out.println(sb.toString());
		} catch (SQLException sqle) {
			throw new ServiceLocatorException(sqle);
		} finally {
			try {
				if (resultSet != null) {

					resultSet.close();
					resultSet = null;
				}
				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}

				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException sql) {
				// System.err.print("Error :"+sql);
			}

		}

		return sb.toString();
	}

	public String getEmpNameandId(String empNo) throws ServiceLocatorException {
		StringBuffer reportsToBuffer = new StringBuffer("");
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		Connection connection = null;

		try {
			connection = ConnectionProvider.getInstance().getConnection();

			String query = "Select CONCAT(FName,'.',LName) AS EmpName,Id,LoginId from tblEmployee where EmpNo='" + empNo
					+ "'";

			preparedStatement = connection.prepareStatement(query);
			resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {

				reportsToBuffer.append(resultSet.getString("EmpName") + "@#" + resultSet.getString("Id") + "@#"
						+ resultSet.getString("LoginId"));

			}

		} catch (SQLException sle) {
			sle.printStackTrace();
		} catch (ServiceLocatorException sle) {
			sle.printStackTrace();
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
		}

		return reportsToBuffer.toString();
	}

	public int addHolidays(NewAjaxHandlerAction ajaxHandlerAction) throws ServiceLocatorException {
		PreparedStatement preparedStatement = null;
		String query = "";
		Connection connection = null;

		int isUpdated = 0;

		try {
			connection = ConnectionProvider.getInstance().getConnection();

			query = "insert into tblEmpHolidays(Date,Description,Country) values (?,?,?)";

			preparedStatement = connection.prepareStatement(query);

			preparedStatement.setString(1,
					DateUtility.getInstance().convertStringToMySQLDate(ajaxHandlerAction.getHolidayDate()));
			preparedStatement.setString(2, ajaxHandlerAction.getDescription());
			preparedStatement.setString(3, ajaxHandlerAction.getCountry());

			isUpdated = preparedStatement.executeUpdate();

		} catch (Exception sqe) {
			sqe.printStackTrace();
		} finally {
			try {

				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}

				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException sqle) {
				sqle.printStackTrace();
			}
		}

		return isUpdated;
	}

	public String getEmpHolidaysList(NewAjaxHandlerAction ajaxHandlerAction) throws ServiceLocatorException {
		String response = "";
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		Connection connection = null;
		int flag = 0;
		int sno = 0;
		try {
			connection = ConnectionProvider.getInstance().getConnection();

			String query = "Select DATE,Country,Description FROM tblEmpHolidays";
			if (ajaxHandlerAction.getYear() != null && !"".equals(ajaxHandlerAction.getYear())) {
				flag = 1;
				query = query + " WHERE Year(Date) = " + ajaxHandlerAction.getYear();
			}
			if (ajaxHandlerAction.getCountry() != null && !"0".equals(ajaxHandlerAction.getCountry())) {
				if (flag == 1) {
					query = query + " AND Country = '" + ajaxHandlerAction.getCountry() + "'";
				} else {
					query = query + " WHERE Country = '" + ajaxHandlerAction.getCountry() + "'";
				}
			}
			// System.out.println("query--->"+query);
			preparedStatement = connection.prepareStatement(query);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				sno = sno + 1;
				String date = (DateUtility.getInstance().convertToviewFormat(resultSet.getString("DATE")));
				String country = resultSet.getString("Country");
				String description = resultSet.getString("Description");
				// System.out.println("reportsToBuffer====>"+date);
				// System.out.println("reportsToBuffer====>"+country);
				// System.out.println("reportsToBuffer====>"+description);
				response = response + sno + "#^$" + date + "#^$" + country + "#^$" + description + "*@!";
				// System.out.println("reportsToBuffer====>"+response);
			}

		} catch (Exception sqe) {
			sqe.printStackTrace();
		} finally {
			try {

				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}

				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException sqle) {
				sqle.printStackTrace();
			}
		}

		return response;
	}

	/*
	 * public String getElasticBiometricLoad(String empId,int month,int
	 * year,String reportType) throws ServiceLocatorException { stringBuffer =
	 * new StringBuffer(); // CallableStatement callableStatement = null; //
	 * PreparedStatement preparedStatement = null; // Statement statement =
	 * null; // ResultSet resultSet = null; String createdBy = ""; String
	 * totalStream = ""; String response=""; int empNo = 0; String location =
	 * ""; String country = ""; String biometricDate = ""; boolean hasRecord =
	 * false; String blankDaysRes = ""; String status = ""; float duration = 0;
	 * String statusCode = ""; int attendanceLoad = 0; String comments = "";
	 * //List<EmployeeVTO> eventsList = new ArrayList<EmployeeVTO>();
	 * 
	 * 
	 * try {
	 * 
	 * // For Elastic Search implementation: Refer -
	 * http://www.ibm.com/developerworks/java/library/j-javadev2-24/index.html
	 * // Jest Client Configuration Settings
	 * 
	 * JestClientFactory factory = new JestClientFactory();
	 * 
	 * factory.setHttpClientConfig(new
	 * HttpClientConfig.Builder("http://localhost:9200").multiThreaded(true).
	 * readTimeout(20000).build());
	 * 
	 * JestClient client = factory.getObject();
	 * 
	 * String startDate = year + "-" + month + "-01";
	 * 
	 * System.out.println("startDate: ---->" + startDate);
	 * 
	 * DateFormat format = new SimpleDateFormat("yyyy-MM-dd"); Date date =
	 * format.parse(startDate);
	 * 
	 * 
	 * Calendar calendar = Calendar.getInstance(); calendar.setTime(date);
	 * 
	 * Calendar cal = Calendar.getInstance(); cal.setTime(mar.getEventDate());
	 * int day = cal.get(Calendar.DAY_OF_MONTH);
	 * 
	 * calendar.add(Calendar.MONTH, 1); calendar.set(Calendar.DAY_OF_MONTH, 1);
	 * calendar.add(Calendar.DATE, -1); int noOfDays =
	 * calendar.get(Calendar.DAY_OF_MONTH);
	 * 
	 * // Date lastDayOfMonth = calendar.getTime();
	 * 
	 * // System.out.println("lastDayOfMonth: ---->" + lastDayOfMonth);
	 * 
	 * // System.out.println("Last Day of Month: " +
	 * sdf.format(lastDayOfMonth));
	 * 
	 * 
	 * Calendar startDayCal = Calendar.getInstance(); startDayCal.setTime(date);
	 * 
	 * 
	 * int dayOfWeek = startDayCal.get(Calendar.DAY_OF_WEEK);
	 * 
	 * 
	 * 
	 * 
	 * String endDate = year + "-" + month + "-" + noOfDays;
	 * 
	 * System.out.println(endDate+"startDate: ---->" + startDate);
	 * 
	 * 
	 * 
	 * 
	 * System.out.println("noOfDays: ---->" + noOfDays);
	 * System.out.println("Day of Week: ---->" + dayOfWeek);
	 * 
	 * Date date1 = format.parse(endDate); Calendar lastDaycalendar =
	 * Calendar.getInstance(); lastDaycalendar.setTime(date1);
	 * System.out.println(date1+"<lastDaycalendar====>"+lastDaycalendar); int
	 * lastDayOfWeek = lastDaycalendar.get(Calendar.DAY_OF_WEEK);
	 * System.out.println("lastDayOfWeek====>"+lastDayOfWeek);
	 * 
	 * 
	 * List<com.mss.mirage.employee.general.EmployeeElasticVTO> empList =
	 * DataSourceDataProvider.getInstance().getEmpDetailsById(empId); //
	 * Iterating over the list and set those values into Event Class for
	 * (com.mss.mirage.employee.general.EmployeeElasticVTO eventObj : empList) {
	 * // System.out.println("search result is " + empList); empNo =
	 * eventObj.getEmpNo(); location = eventObj.getLocation(); country =
	 * eventObj.getCountry(); } // String lastDay = ((year+"-" + month + "-" +
	 * noOfDays)); // String firstDay = ((year + "-" + month + "-" + 01));
	 * System.out.println("--->"+startDate+"--->"+endDate);
	 * if("LiveData".equals(reportType)){ hasRecord =
	 * DataSourceDataProvider.getInstance().gethasRecInBioAttendanceLogs(empNo,
	 * startDate,endDate); }else{ hasRecord =
	 * DataSourceDataProvider.getInstance().gethasRecInBioPayrollAttendanceLogs(
	 * empNo,startDate,endDate); }
	 * 
	 * if(!hasRecord){ response = "NoRecord"; }else{ if(dayOfWeek > 1) { int
	 * blankDays = 1; while(dayOfWeek > blankDays){ blankDaysRes = blankDaysRes
	 * + "Blank" + "#^$"; blankDaysRes = blankDaysRes + "Blank" + "#^$";
	 * blankDaysRes = blankDaysRes + "Blank" + "#^$"; blankDaysRes =
	 * blankDaysRes + "Blank" + "#^$"; blankDaysRes = blankDaysRes + noOfDays +
	 * "*@!"; // response = blankDaysRes; blankDays = blankDays + 1; } }
	 * 
	 * 
	 * String endBlankDaysRes = "";
	 * 
	 * if(lastDayOfWeek < 7) { int blankDays = 7; while(lastDayOfWeek <
	 * blankDays){ endBlankDaysRes = endBlankDaysRes + "Blank" + "#^$";
	 * endBlankDaysRes = endBlankDaysRes + "Blank" + "#^$"; endBlankDaysRes =
	 * endBlankDaysRes + "Blank" + "#^$"; endBlankDaysRes = endBlankDaysRes +
	 * "Blank" + "#^$"; endBlankDaysRes = endBlankDaysRes + noOfDays + "*@!"; //
	 * response = blankDaysRes; blankDays = blankDays - 1; }
	 * 
	 * System.out.println(endBlankDaysRes+"<+++++response====>"+response); }
	 * 
	 * response = blankDaysRes;
	 * 
	 * System.out.println("<+++++response====>"+blankDaysRes);
	 * 
	 * for(int day = 1;day <= noOfDays;day++){ biometricDate = year + "-" +
	 * month + "-" + day;
	 * 
	 * if("Work From Home".equals(location)){ int leavesCount =
	 * DataSourceDataProvider.getInstance().getLeaveStatusOfWorkFromHome(
	 * biometricDate,empId); int holidayCount =
	 * DataSourceDataProvider.getInstance().getHolidayWithDateandCountry(
	 * biometricDate,country);
	 * 
	 * if(leavesCount > 0){ status = "Absent"; duration = 9; statusCode = "A"; }
	 * else{ status = "Present"; duration = 9; statusCode = "P"; } Calendar cal1
	 * = Calendar.getInstance(); cal1.setTime(date); dayOfWeek =
	 * cal1.get(cal1.DAY_OF_WEEK);
	 * 
	 * if(dayOfWeek == 7 || dayOfWeek == 1){ status = "WeeklyOff"; duration = 0;
	 * statusCode = "WO"; }
	 * 
	 * if(holidayCount > 0){ status = "Holiday"; duration = 0; statusCode = "H";
	 * }
	 * 
	 * }
	 * 
	 * 
	 * 
	 * if("LiveData".equals(reportType)){ hasRecord =
	 * DataSourceDataProvider.getInstance().getIfNoBioPunchAndNoStatus(empNo,
	 * biometricDate); if(hasRecord){
	 * List<com.mss.mirage.employee.timesheets.BiometricElasticVTO> bioList =
	 * DataSourceDataProvider.getInstance().getBioAttendanceLogsData(empNo,
	 * biometricDate); // Iterating over the list and set those values into
	 * Event Class // System.out.println("search result is " + bioList); for
	 * (com.mss.mirage.employee.timesheets.BiometricElasticVTO bioeventObj :
	 * bioList) {
	 * 
	 * // System.out.println(bioeventObj.getStatusCode()
	 * +"search result for Status " + bioeventObj.getStatus());
	 * 
	 * status = (bioeventObj.getStatus()).trim(); duration = (float)
	 * Math.ceil((bioeventObj.getDuration())/60); statusCode =
	 * (bioeventObj.getStatusCode()).trim(); attendanceLoad =
	 * bioeventObj.getAttendanceLogId(); } }else{ status = "noRecord"; duration
	 * = 0; statusCode = "N"; attendanceLoad = 0; } }else{ hasRecord =
	 * DataSourceDataProvider.getInstance().getIfNoBioPayrollPunchAndNoStatus(
	 * empNo,biometricDate); if(hasRecord){
	 * List<com.mss.mirage.employee.timesheets.BiometricPayrollElasticVTO>
	 * biopayrollList =
	 * DataSourceDataProvider.getInstance().getBioPayrollAttendanceLogsData(
	 * empNo,biometricDate); // Iterating over the list and set those values
	 * into Event Class for
	 * (com.mss.mirage.employee.timesheets.BiometricPayrollElasticVTO
	 * biopayrolleventObj : biopayrollList) { //
	 * System.out.println("search result is " + eventList); status =
	 * (biopayrolleventObj.getStatus()).trim();
	 * 
	 * duration = (float) Math.ceil((biopayrolleventObj.getDuration())/60);
	 * statusCode = (biopayrolleventObj.getStatusCode()).trim(); attendanceLoad
	 * = biopayrolleventObj.getAttendanceLogId(); comments =
	 * biopayrolleventObj.getRemarks(); } }else{ status = "noRecord"; duration =
	 * 0; statusCode = "N"; attendanceLoad = 0; } } response = response +
	 * biometricDate + "#^$" + status + "#^$" + duration + "#^$" + statusCode +
	 * "#^$" + noOfDays + "#^$" + attendanceLoad + "#^$" + comments + "*@!" ;
	 * 
	 * }
	 * 
	 * response = response + endBlankDaysRes;
	 * System.out.println("<+++++response====>"+response); } int maxRecords=10;
	 * int startValue = (pageNo - 1) * maxRecords; i=startValue;
	 * 
	 * 
	 * 
	 * } catch (Exception ex) { ex.printStackTrace();
	 * 
	 * } finally { try {
	 * 
	 * 
	 * } catch (Exception ex) { ex.printStackTrace(); } } return response; }
	 * 
	 * 
	 * @Override public String updateElasticAttendanceLoad(String empId,
	 * NewAjaxHandlerAction newAjaxHandlerAction) throws ServiceLocatorException
	 * { String result = ""; String empNo =
	 * DataSourceDataProvider.getInstance().getEmpNoByEmpId(Integer.parseInt(
	 * empId)); BiometricPayrollElasticVTO event = null; JestClientFactory
	 * factory1 = new JestClientFactory();
	 * 
	 * String eventsUrl1 = "http://localhost:9200"; String attId = "";
	 * 
	 * factory1.setHttpClientConfig( new
	 * HttpClientConfig.Builder(eventsUrl1).multiThreaded(true).readTimeout(
	 * 20000).build());
	 * 
	 * JestClient client1 = factory1.getObject();
	 * 
	 * String query1 = "{\n" // + " \"size\": " + maxRecords + ",\n" // +
	 * " \"from\": " + startValue + ",\n" + "   \"query\": {\n" +
	 * "   \"bool\": {\n" + "   \"must\": [\n";
	 * 
	 * //query = query + "  { \"match\": { \"empNo\": \"" + empNo + "\" } } ,";
	 * query1 = query1 + "  { \"match\": { \"attendanceLogId\": \"" +
	 * newAjaxHandlerAction.getAttendanceLogId() + "\" } } "; query1 = query1+
	 * "" + "   ]" + "   }}}";
	 * 
	 * String eventIndex1 = "tbltestbiopayrollindexsample"; String eventType1 =
	 * "tbltestbiopayrolltypesample"; Search search = new
	 * Search.Builder(query1).addIndex(eventIndex1).addType(eventType1).
	 * setParameter(Parameters.SCROLL, "10m").build(); try { // Execution of
	 * query with index and type. SearchResult result1 =
	 * client1.execute(search);
	 * 
	 * List<BiometricPayrollElasticVTO> biometricPayrollElasticVTOEventObj =
	 * result1.getSourceAsObjectList(BiometricPayrollElasticVTO.class);
	 * ObjectMapper objectMapper = new ObjectMapper();
	 * System.out.println(objectMapper.writeValueAsString(
	 * biometricPayrollElasticVTOEventObj.get(0))); JSONObject jsonObject = new
	 * JSONObject(objectMapper.writeValueAsString(
	 * biometricPayrollElasticVTOEventObj.get(0)));
	 * 
	 * String statusCode = newAjaxHandlerAction.getStatusCode(); String status =
	 * newAjaxHandlerAction.getStatus();
	 * 
	 * jsonObject.put("remarks", newAjaxHandlerAction.getComments());
	 * jsonObject.put("p2Status", ""); jsonObject.put("status", status);
	 * jsonObject.put("statusCode", statusCode); if("A".equals(statusCode)){
	 * jsonObject.put("absent", "1"); }else{ jsonObject.put("absent", "0");
	 * 
	 * } if("P".equals(statusCode)){ jsonObject.put("present", "1"); } else{
	 * jsonObject.put("present", "0"); } if("WO".equals(statusCode)){
	 * jsonObject.put("weeklyOff", "1"); }
	 * 
	 * else{ jsonObject.put("weeklyOff", "0"); } if("H".equals(statusCode)){
	 * jsonObject.put("holiday", "1"); } else{ jsonObject.put("holiday", "0"); }
	 * 
	 * result =
	 * DataSourceDataProvider.getInstance().doUpdateElasticPayrollBiometric(
	 * jsonObject.getInt("attendanceLogId"),jsonObject.getString(
	 * "attendanceDate"), jsonObject.getInt("empNo"),
	 * jsonObject.getString("inTime"), jsonObject.getString("inDeviceId"),
	 * jsonObject.getString("outTime"), jsonObject.getString("outDeviceId"),
	 * Float.parseFloat(String.valueOf(jsonObject.getDouble("duration"))),
	 * jsonObject.getInt("lateBy"), jsonObject.getInt("earlyBy"),
	 * jsonObject.getInt("isOnLeave"), jsonObject.getString("leaveType"),
	 * Float.parseFloat(String.valueOf(jsonObject.getDouble("leaveDuration"))),
	 * jsonObject.getInt("weeklyOff"), jsonObject.getInt("holiday"),
	 * jsonObject.getString("leaveRemarks"),
	 * jsonObject.getString("punchRecords"), jsonObject.getInt("shiftId"),
	 * Float.parseFloat(String.valueOf(jsonObject.getDouble("present"))),
	 * Float.parseFloat(String.valueOf(jsonObject.getDouble("absent"))),
	 * jsonObject.getString("status"), jsonObject.getString("statusCode"),
	 * jsonObject.getString("p1Status"),jsonObject.getString("p2Status"),
	 * jsonObject.getString("p3Status"), jsonObject.getInt("isOnSpecialOff"),
	 * jsonObject.getString("specialOffType"),
	 * jsonObject.getString("specialOffRemark"),
	 * jsonObject.getInt("specialOffDuration"), jsonObject.getInt("overTime"),
	 * jsonObject.getInt("overTimeE"), jsonObject.getInt("missedOutPunch"),
	 * jsonObject.getString("remarks"), jsonObject.getString("missedInPunch"),
	 * jsonObject.getString("c1"), jsonObject.getString("c2"),
	 * jsonObject.getString("c3"), jsonObject.getString("c4"),
	 * jsonObject.getString("c5"), jsonObject.getString("c6"),
	 * jsonObject.getString("c7"), jsonObject.getInt("leaveTypeId"));
	 * 
	 * // String result1 =
	 * updateElasticAttendanceLoad1(jsonObject.getInt("attendanceLogId"),
	 * jsonObject.getString("attendanceDate"), jsonObject.getInt("empNo"),
	 * jsonObject.getString("inTime"), jsonObject.getString("inDeviceId"),
	 * jsonObject.getString("outTime"), jsonObject.getString("outDeviceId"),
	 * Float.parseFloat(String.valueOf(jsonObject.getDouble("duration"))),
	 * jsonObject.getInt("lateBy"), jsonObject.getInt("earlyBy"),
	 * jsonObject.getInt("isOnLeave"), jsonObject.getString("leaveType"),
	 * Float.parseFloat(String.valueOf(jsonObject.getDouble("leaveDuration"))),
	 * jsonObject.getInt("weeklyOff"), jsonObject.getInt("holiday"),
	 * jsonObject.getString("leaveRemarks"),
	 * jsonObject.getString("punchRecords"), jsonObject.getInt("shiftId"),
	 * Float.parseFloat(String.valueOf(jsonObject.getDouble("present"))),
	 * Float.parseFloat(String.valueOf(jsonObject.getDouble("absent"))),
	 * jsonObject.getString("status"), jsonObject.getString("statusCode"),
	 * jsonObject.getString("p1Status"), jsonObject.getString("p3Status"),
	 * jsonObject.getInt("isOnSpecialOff"),
	 * jsonObject.getString("specialOffType"),
	 * jsonObject.getString("specialOffRemark"),
	 * jsonObject.getInt("specialOffDuration"), jsonObject.getInt("overTime"),
	 * jsonObject.getInt("overTimeE"), jsonObject.getInt("missedOutPunch"),
	 * jsonObject.getString("remarks"), jsonObject.getString("missedInPunch"),
	 * jsonObject.getString("c1"), jsonObject.getString("c2"),
	 * jsonObject.getString("c3"), jsonObject.getString("c4"),
	 * jsonObject.getString("c5"), jsonObject.getString("c6"),
	 * jsonObject.getString("c7"), jsonObject.getInt("leaveTypeId"));
	 * System.out.println("jsonObject"+jsonObject);
	 * 
	 * 
	 * 
	 * 
	 * } catch (Exception ex) { // ex.getMessage();
	 * System.err.println("Exception in getEvents-->" + ex.getMessage()); //
	 * Logger.getLogger(DataSourceDataProvider.class.getName()).log(Level.
	 * SEVERE, // null, ex); } finally { // System.out.println("in finally-->");
	 * if (client1 != null) { client1.shutdownClient(); } }
	 * 
	 * return result; }
	 */

	@Override
	public String getEmployeeDetailsStackChart() throws ServiceLocatorException {
		Statement statement = null;
		String response = "";
		String queryString = "";
		ResultSet resultSet = null;
		Connection connection = ConnectionProvider.getInstance().getConnection();
		try {

			queryString = "SELECT country ,COUNT(id) AS total,"
					+ "SUM(CASE WHEN DepartmentId='Operations' THEN 1 ELSE 0 END) AS Operations,"
					+ "SUM(CASE WHEN DepartmentId='Recruiting' THEN 1 ELSE 0 END) AS Recruiting,"
					+ "SUM(CASE WHEN DepartmentId='Sales' THEN 1 ELSE 0 END) AS Sales,"
					+ "SUM(CASE WHEN DepartmentId='Marketing' THEN 1 ELSE 0 END) AS Marketing,"
					+ "SUM(CASE WHEN DepartmentId='SSG' THEN 1 ELSE 0 END) AS SSG, "
					+ "SUM(CASE WHEN DepartmentId='GDC' THEN 1 ELSE 0 END) AS GDC, "
					+ "SUM(CASE WHEN DepartmentId='Executive Board' THEN 1 ELSE 0 END) AS ExecutiveBoard "
					+ "FROM tblEmployee WHERE (Country IS NOT NULL AND Country!='') AND CurStatus='Active' GROUP BY country ORDER BY Country DESC";

			// System.out.println("queryString---"+queryString);
			statement = connection.createStatement();
			resultSet = statement.executeQuery(queryString);
			int i = 0;
			while (resultSet.next()) {
				response = response + resultSet.getString("country") + "#^$";
				response = response + resultSet.getString("total") + "#^$";
				response = response + resultSet.getString("Operations") + "#^$";
				response = response + resultSet.getString("Recruiting") + "#^$";
				response = response + resultSet.getString("Sales") + "#^$";
				response = response + resultSet.getString("Marketing") + "#^$";
				response = response + resultSet.getString("SSG") + "#^$";
				response = response + resultSet.getString("GDC") + "#^$";
				response = response + resultSet.getString("ExecutiveBoard") + "#^$";
				int billing = DataSourceDataProvider.getInstance()
						.getBillableResourcesCount(resultSet.getString("country"));
				response = response + billing + "*@!";

				i++;

			}

			// System.out.println("response---->"+response);
		} catch (SQLException sqle) {
			throw new ServiceLocatorException(sqle);
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (statement != null) {
					statement.close();
					statement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException se) {
				se.printStackTrace();
				throw new ServiceLocatorException(se);
			}
		}
		return response;
	}

	@Override
	public String getEmployeeDetailsThroughCount(String country, String category) throws ServiceLocatorException {
		Statement statement = null;
		String response = "";
		String queryString = "";
		ResultSet resultSet = null;
		Connection connection = ConnectionProvider.getInstance().getConnection();
		try {

			queryString = "SELECT CONCAT(FName,'.',LName) AS EmpName,Email1,WorkPhoneNo,CellPhoneNo FROM tblEmployee WHERE  (Country IS NOT NULL AND Country!='') AND CurStatus='Active' AND Country = '"
					+ country + "' AND DepartmentId = '" + category + "'";

			/*
			 * queryString = "SELECT country ," +
			 * "SUM(CASE WHEN DepartmentId='Operations' THEN 1 ELSE 0 END) AS Operations,"
			 * +
			 * "SUM(CASE WHEN DepartmentId='Recruiting' THEN 1 ELSE 0 END) AS Recruiting,"
			 * +
			 * "SUM(CASE WHEN DepartmentId='Sales' THEN 1 ELSE 0 END) AS Sales,"
			 * +
			 * "SUM(CASE WHEN DepartmentId='Marketing' THEN 1 ELSE 0 END) AS Marketing,"
			 * + "SUM(CASE WHEN DepartmentId='SSG' THEN 1 ELSE 0 END) AS SSG, "
			 * + "SUM(CASE WHEN DepartmentId='GDC' THEN 1 ELSE 0 END) AS GDC, "
			 * +
			 * "SUM(CASE WHEN DepartmentId='Executive Board' THEN 1 ELSE 0 END) AS ExecutiveBoard "
			 * +
			 * "FROM tblEmployee WHERE (Country IS NOT NULL AND Country!='') AND CurStatus='Active' GROUP BY country ORDER BY Country DESC"
			 * ;
			 */

			// System.out.println("queryString---"+queryString);
			statement = connection.createStatement();
			resultSet = statement.executeQuery(queryString);
			int i = 0;
			while (resultSet.next()) {
				int billing = DataSourceDataProvider.getInstance().getBillableResourcesCount(country);
				i++;
				// response = response + resultSet.getInt("EmpNo") + "#^$";
				// response = response + resultSet.getString("total") + "#^$";

				String empName = resultSet.getString("EmpName");
				if (empName == null || "".equals(empName)) {
					empName = "-";
				}
				String email = resultSet.getString("Email1");
				if (email == null || "".equals(email)) {
					email = "-";
				}
				String phoneNo = resultSet.getString("WorkPhoneNo");
				if (phoneNo == null || "".equals(phoneNo)) {
					phoneNo = "-";
				}
				response = response + billing + "#^$";
				response = response + (i) + "#^$";
				response = response + empName + "#^$";
				response = response + email + "#^$";
				response = response + phoneNo + "*@!";
				// response = response + resultSet.getString("CellPhoneNo") +
				// "*@!";
				// response = response + resultSet.getString("SSG") + "#^$";
				// response = response + resultSet.getString("GDC") + "#^$";
				// response = response + resultSet.getString("ExecutiveBoard") +
				// "*@!";
				// response = response + billing + "*@!";

			}

			// System.out.println("response---->"+response);
		} catch (SQLException sqle) {
			throw new ServiceLocatorException(sqle);
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (statement != null) {
					statement.close();
					statement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException se) {
				se.printStackTrace();
				throw new ServiceLocatorException(se);
			}
		}
		return response;
	}

	@Override
	public String getRequirementDetailsStackChart(int pastMonths) throws ServiceLocatorException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		String response = "";
		// String query = "SELECT HrComments FROM tblEmpReview WHERE Id =" +
		// reviewId;
		// String query = "SELECT SkillSet FROM tblEmpStateHistory WHERE Id=
		// "+currId+" AND EmpId="+empId;
		try {
			String startDate = "";
			String endDate;

			endDate = DateUtility.getInstance().CurrentMonthLastDate();

			startDate = DateUtility.getInstance().getPastMonthsFirstDate(pastMonths - 1);
			StringBuffer querStringBuffer = new StringBuffer();
			querStringBuffer.append(
					"SELECT Country,COUNT(id) AS total,SUM(CASE WHEN STATUS='Open' THEN 1 ELSE 0 END) AS OPENED,");
			querStringBuffer.append(" SUM(CASE WHEN STATUS='InProgress' THEN 1 ELSE 0 END) AS InProgress,");
			querStringBuffer.append(" SUM(CASE WHEN STATUS='Forecast' THEN 1 ELSE 0 END) AS Forecast,");
			// querStringBuffer.append("SUM(CASE WHEN STATUS='Forecast' THEN 1
			// ELSE 0 END) AS Forecast,");
			querStringBuffer.append(" SUM(CASE WHEN STATUS='lost' THEN 1 ELSE 0 END) AS lost,");
			querStringBuffer.append(" SUM(CASE WHEN STATUS='Withdrawn' THEN 1 ELSE 0 END) AS Withdrawn,");
			querStringBuffer.append(" SUM(CASE WHEN STATUS='Hold' THEN 1 ELSE 0 END) AS Hold,");
			querStringBuffer.append(" SUM(CASE WHEN STATUS='won' THEN 1 ELSE 0 END) AS won ");
			querStringBuffer.append(" FROM tblRecRequirement ");

			querStringBuffer.append(" WHERE (Country IS NOT NULL AND Country!='') AND (DATE(DatePosted) >= '"
					+ DateUtility.getInstance().convertStringToMySQLDate(startDate) + "') ");
			querStringBuffer
					.append(" AND (DATE(DatePosted) <= '" + DateUtility.getInstance().convertStringToMySQLDate(endDate)
							+ "') GROUP BY Country ORDER BY Country DESC");

			// System.out.println("isManagerInclude-->" + isManagerInclude);

			// System.out.println("Rec Query-->" + querStringBuffer.toString());
			connection = ConnectionProvider.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(querStringBuffer.toString());
			resultSet = preparedStatement.executeQuery();
			int i = 0;
			ArrayList alist = new ArrayList();
			while (resultSet.next()) {
				i = 1;
				response = response + resultSet.getString("Country") + "#^$" + resultSet.getString("total") + "#^$"
						+ resultSet.getString("OPENED") + "#^$" + resultSet.getString("InProgress") + "#^$"
						+ resultSet.getString("Forecast") + "#^$" + resultSet.getString("lost") + "#^$"
						+ resultSet.getString("Withdrawn") + "#^$" + resultSet.getString("Hold") + "#^$"
						+ resultSet.getString("won") + "*@!";
			}

			if (i == 0) {
				response = "no data";
			}
			// System.out.println("response-->" + response);
			// System.err.println("Result----"+sb);
		} catch (ParseException ex) {
			throw new ServiceLocatorException(ex);
		} catch (SQLException sqle) {
			throw new ServiceLocatorException(sqle);
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}

				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException sql) {
				// System.err.print("Error :"+sql);
			}

		}
		// System.out.println("---------->" + response);
		return response;
	}

	@Override
	public String getRequirementDetailsFromStack(int pastMonths, String country, String status)
			throws ServiceLocatorException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		int count = 0;
		String response = "";
		// String query = "SELECT HrComments FROM tblEmpReview WHERE Id =" +
		// reviewId;
		// String query = "SELECT SkillSet FROM tblEmpStateHistory WHERE Id=
		// "+currId+" AND EmpId="+empId;
		try {
			String startDate = "";
			String endDate;

			endDate = DateUtility.getInstance().CurrentMonthLastDate();

			startDate = DateUtility.getInstance().getPastMonthsFirstDate(pastMonths - 1);
			StringBuffer querStringBuffer = new StringBuffer();
			querStringBuffer.append("SELECT JobTitle,Practice,AssignedTo,AssignToTechLead,State");
			/*
			 * querStringBuffer.
			 * append("SUM(CASE WHEN STATUS='InProgress' THEN 1 ELSE 0 END) AS InProgress,"
			 * ); querStringBuffer.
			 * append("SUM(CASE WHEN STATUS='Forecast' THEN 1 ELSE 0 END) AS Forecast,"
			 * ); querStringBuffer.
			 * append("SUM(CASE WHEN STATUS='Forecast' THEN 1 ELSE 0 END) AS Forecast,"
			 * ); querStringBuffer.
			 * append("SUM(CASE WHEN STATUS='lost' THEN 1 ELSE 0 END) AS lost,"
			 * ); querStringBuffer.
			 * append("SUM(CASE WHEN STATUS='Withdrawn' THEN 1 ELSE 0 END) AS Withdrawn,"
			 * ); querStringBuffer.
			 * append("SUM(CASE WHEN STATUS='Hold' THEN 1 ELSE 0 END) AS Hold,"
			 * ); querStringBuffer.
			 * append("SUM(CASE WHEN STATUS='won' THEN 1 ELSE 0 END) AS won ");
			 */
			querStringBuffer.append(" FROM tblRecRequirement ");

			querStringBuffer.append(" WHERE (Country IS NOT NULL AND Country!='') AND Country = '" + country
					+ "' AND STATUS = '" + status + "' AND (DATE(DatePosted) >= '"
					+ DateUtility.getInstance().convertStringToMySQLDate(startDate) + "') ");
			querStringBuffer.append(" AND (DATE(DatePosted) <= '"
					+ DateUtility.getInstance().convertStringToMySQLDate(endDate) + "') ");

			/*
			 * queryString = "SELECT country ," +
			 * "SUM(CASE WHEN DepartmentId='Operations' THEN 1 ELSE 0 END) AS Operations,"
			 * +
			 * "SUM(CASE WHEN DepartmentId='Recruiting' THEN 1 ELSE 0 END) AS Recruiting,"
			 * +
			 * "SUM(CASE WHEN DepartmentId='Sales' THEN 1 ELSE 0 END) AS Sales,"
			 * +
			 * "SUM(CASE WHEN DepartmentId='Marketing' THEN 1 ELSE 0 END) AS Marketing,"
			 * + "SUM(CASE WHEN DepartmentId='SSG' THEN 1 ELSE 0 END) AS SSG, "
			 * + "SUM(CASE WHEN DepartmentId='GDC' THEN 1 ELSE 0 END) AS GDC, "
			 * +
			 * "SUM(CASE WHEN DepartmentId='Executive Board' THEN 1 ELSE 0 END) AS ExecutiveBoard "
			 * +
			 * "FROM tblEmployee WHERE (Country IS NOT NULL AND Country!='') AND CurStatus='Active' GROUP BY country ORDER BY Country DESC"
			 * ;
			 */

			// System.out.println("Rec Query-->" + querStringBuffer.toString());
			connection = ConnectionProvider.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(querStringBuffer.toString());
			resultSet = preparedStatement.executeQuery();
			int i = 0;
			ArrayList alist = new ArrayList();
			while (resultSet.next()) {
				i = 1;
				count++;
				String jobTitle = resultSet.getString("JobTitle");
				if (jobTitle == null || "".equals(jobTitle)) {
					jobTitle = "-";
				}
				String practice = resultSet.getString("Practice");
				if (practice == null || "".equals(practice)) {
					practice = "-";
				}
				String assignedTo = resultSet.getString("AssignedTo");
				if (assignedTo == null || "".equals(assignedTo)) {
					assignedTo = "-";
				}
				String assaignedToLead = resultSet.getString("AssignToTechLead");
				if (assaignedToLead == null || "".equals(assaignedToLead)) {
					assaignedToLead = "-";
				}
				String state = resultSet.getString("State");
				if (state == null || "".equals(state)) {
					state = "-";
				}
				response = response + count + "#^$" + jobTitle + "#^$" + practice + "#^$" + assignedTo + "#^$"
						+ assaignedToLead + "#^$" + state + "*@!";
			}

			if (i == 0) {
				response = "nodata";
			}
			// System.out.println("Rec response-->" + response);
			// System.err.println("Result----"+sb);
		} catch (ParseException ex) {
			throw new ServiceLocatorException(ex);
		} catch (SQLException sqle) {
			throw new ServiceLocatorException(sqle);
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}

				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException sql) {
				// System.err.print("Error :"+sql);
			}

		}
		// System.out.println("---------->" + response);
		return response;
	}

	public String getGreenSheetCountStackChart(int pastMonths, boolean external) throws ServiceLocatorException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		String response = "";
		// String query = "SELECT HrComments FROM tblEmpReview WHERE Id =" +
		// reviewId;
		// String query = "SELECT SkillSet FROM tblEmpStateHistory WHERE Id=
		// "+currId+" AND EmpId="+empId;
		try {
			String startDate = "";
			String endDate = DateUtility.getInstance().CurrentMonthLastDate();
			startDate = DateUtility.getInstance().getPastMonthsFirstDate(pastMonths - 1);
			StringBuffer querStringBuffer = new StringBuffer();
			// System.out.println("external---"+external);
			querStringBuffer.append("SELECT Country,COUNT(DISTINCT Id) AS placements,");
			querStringBuffer.append(
					"SUM(CASE WHEN UnitsRate IS NOT NULL THEN UnitsRate ELSE 0 END)*" + pastMonths + " AS UnitsRates,");
			querStringBuffer.append("SUM(CASE WHEN VenUnitRate IS NOT NULL THEN VenUnitRate ELSE 0 END)*" + pastMonths
					+ " AS VenUnitRates,");
			querStringBuffer
					.append("SUM((CASE WHEN UnitsRate IS NOT NULL THEN UnitsRate ELSE 0 END)-(CASE WHEN VenUnitRate IS NOT NULL THEN VenUnitRate ELSE 0 END))*"
							+ pastMonths + " AS Margin FROM tblGreensheets");
			querStringBuffer
					.append(" WHERE (STATUS='Created' OR STATUS='Renewal'  OR STATUS='Approved' OR STATUS='NotKnown')");
			querStringBuffer.append(" AND (Country='USA' OR Country='India') ");
			if (external) {
				querStringBuffer.append("AND VenUnitRate>0 ");
			} else {
				querStringBuffer.append("AND VenUnitRate=0 ");
			}
			querStringBuffer.append("AND (DATE(DateCreated) >= '"
					+ DateUtility.getInstance().convertStringToMySQLDate(startDate) + "') ");
			querStringBuffer
					.append("AND (DATE(DateCreated) <= '" + DateUtility.getInstance().convertStringToMySQLDate(endDate)
							+ "') GROUP BY Country ORDER BY Country DESC");

			// System.out.println("isManagerInclude-->" + isManagerInclude);

			// System.out.println("greensheet Query-->" +
			// querStringBuffer.toString());
			connection = ConnectionProvider.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(querStringBuffer.toString());
			resultSet = preparedStatement.executeQuery();
			int i = 0;
			ArrayList alist = new ArrayList();
			double totalCost = 0.00;
			DecimalFormat decimalFormat = new DecimalFormat("0.00");
			while (resultSet.next()) {
				i = 1;
				if (external) {
					response = response + resultSet.getString("Country") + "#^$" + resultSet.getString("placements")
							+ "#^$" + decimalFormat.format(resultSet.getDouble("UnitsRates")) + " $#^$"
							+ decimalFormat.format(resultSet.getDouble("VenUnitRates")) + " $#^$"
							+ decimalFormat.format(resultSet.getDouble("Margin")) + " $*@!";
				} else {
					response = response + resultSet.getString("Country") + "#^$" + resultSet.getString("placements")
							+ "#^$" + decimalFormat.format(resultSet.getDouble("UnitsRates")) + " $*@!";
				}
				totalCost += resultSet.getDouble("Margin");
			}
			String tCost = decimalFormat.format(totalCost);

			if (i == 0) {
				response = "no data";
			} else {
				response += "addto" + tCost;
			}
			System.out.println("greensheet response-->" + response);
			// System.err.println("Result----"+sb);
		} catch (ParseException ex) {
			throw new ServiceLocatorException(ex);
		} catch (SQLException sqle) {
			throw new ServiceLocatorException(sqle);
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}

				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException sql) {
				// System.err.print("Error :"+sql);
			}

		}
		// System.out.println("---------->" + response);
		return response;
	}

	public String getOpportunitiesCountsStackChart(int pastMonths) throws ServiceLocatorException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		String response = "";
		// System.out.println("getOpportunitiesCounts impl");
		// String query = "SELECT HrComments FROM tblEmpReview WHERE Id =" +
		// reviewId;
		// String query = "SELECT SkillSet FROM tblEmpStateHistory WHERE Id=
		// "+currId+" AND EmpId="+empId;
		try {
			String startDate = "";
			String endDate = DateUtility.getInstance().CurrentMonthLastDate();
			startDate = DateUtility.getInstance().getPastMonthsFirstDate(pastMonths - 1);
			// startDate="10/01/2011";
			StringBuffer querStringBuffer = new StringBuffer();
			querStringBuffer.append("SELECT tblCrmAccount.Region,COUNT(tblCrmOpportunity.Id) AS total,");
			querStringBuffer.append(
					" SUM(CASE WHEN tblCrmOpportunity.VALUE IS NOT NULL THEN tblCrmOpportunity.VALUE ELSE 0 END) AS val ");
			querStringBuffer.append(
					"FROM tblCrmOpportunity JOIN tblCrmAccount ON tblCrmAccount.Id = tblCrmOpportunity.AccountId ");
			querStringBuffer
					.append("where (Region='Central' OR Region='West' OR Region='East' OR Region='Enterprise') AND ");
			querStringBuffer.append(
					"  (DATE(DueDate) >= '" + DateUtility.getInstance().convertStringToMySQLDate(startDate) + "') ");
			querStringBuffer.append("AND (DATE(DueDate) <= '"
					+ DateUtility.getInstance().convertStringToMySQLDate(endDate) + "') GROUP BY Region");

			// System.out.println("isManagerInclude-->" + isManagerInclude);

			// System.out.println(" oppertunities Query-->" +
			// querStringBuffer.toString());
			connection = ConnectionProvider.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(querStringBuffer.toString());
			resultSet = preparedStatement.executeQuery();
			int i = 0;
			ArrayList alist = new ArrayList();
			double totalCost = 0.00;
			DecimalFormat decimalFormat = new DecimalFormat("0.00");
			while (resultSet.next()) {
				i = 1;
				response = response + resultSet.getString("Region") + "#^$" + resultSet.getString("total") + "#^$"
						+ decimalFormat.format(resultSet.getDouble("val")) + "*@!";
				totalCost += resultSet.getDouble("val");
			}
			String tCost = decimalFormat.format(totalCost);

			if (i == 0) {
				response = "no data";
			} else {
				response += "addto" + tCost;
			}
			// System.out.println(" oppertunities response-->" + response);
			// System.err.println("Result----"+sb);
		} catch (ParseException ex) {
			throw new ServiceLocatorException(ex);
		} catch (SQLException sqle) {
			throw new ServiceLocatorException(sqle);
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}

				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException sql) {
				// System.err.print("Error :"+sql);
			}

		}
		// System.out.println("---------->" + response);
		return response;
	}

	/**
	 * This method is to show Pie Chart for Employee Count based on CurStatus
	 * Created By: Triveni Niddara Created Date : 11/5/2017
	 * 
	 * @return
	 */

	@Override
	public String getEmpSalesOppertunitiesPieChart(String country, String departmentId, String practiceId,
			String subpractice) throws ServiceLocatorException {

		String response = "";
		// Connection connection = null;
		CallableStatement callableStatement = null;
		Connection connection = null;

		try {
			connection = ConnectionProvider.getInstance().getConnection();
			callableStatement = connection.prepareCall("{call spEmpSalesOppertuniesCount(?,?,?,?,?)}");

			callableStatement.setString(1, country);
			callableStatement.setString(2, departmentId);

			if (practiceId != null && !"".equals(practiceId)) {
				// System.out.println("practiceId"+practiceId);
				callableStatement.setString(3, practiceId);
			} else {
				callableStatement.setString(3, "%");
			}

			if (subpractice != null && !"".equals(subpractice)) {
				// System.out.println("subpractice"+subpractice);
				callableStatement.setString(4, subpractice);
			} else {
				callableStatement.setString(4, "%");
			}

			// System.out.println("graphId--" + graphId);
			callableStatement.registerOutParameter(5, Types.VARCHAR);
			callableStatement.execute();

			response = callableStatement.getString(5);
			// System.out.println("country----->"+country+"departmentId---->"+departmentId+"practiceId----->"+practiceId+"--->subpractice"+subpractice);
			// System.out.println("response--" + response);

		} catch (SQLException se) {
			se.printStackTrace();
		} finally {
			try {

				if (callableStatement != null) {
					callableStatement.close();
					callableStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		return response;
	}

	/**
	 * This method is to show Pie Chart for Employee Count of requirements based
	 * on Status Created By: Triveni Niddara Created Date : 11/5/2017
	 * 
	 * @return
	 */

	@Override
	public String getEmpSalesRequirementPieChart(String country, String departmentId, String practiceId,
			String subpractice) throws ServiceLocatorException {

		String response = "";
		// Connection connection = null;
		CallableStatement callableStatement = null;
		Connection connection = null;

		try {
			connection = ConnectionProvider.getInstance().getConnection();
			callableStatement = connection.prepareCall("{call spEmpSalesRequirementCount(?,?,?,?,?)}");

			callableStatement.setString(1, country);
			callableStatement.setString(2, departmentId);

			if (practiceId != null && !"".equals(practiceId)) {
				// System.out.println("practiceId"+practiceId);
				callableStatement.setString(3, practiceId);
			} else {
				callableStatement.setString(3, "%");
			}

			if (subpractice != null && !"".equals(subpractice)) {
				// System.out.println("subpractice"+subpractice);
				callableStatement.setString(4, subpractice);
			} else {
				callableStatement.setString(4, "%");
			}

			// System.out.println("graphId--" + graphId);
			callableStatement.registerOutParameter(5, Types.VARCHAR);
			callableStatement.execute();

			response = callableStatement.getString(5);
			// System.out.println("country----->"+country+"departmentId---->"+departmentId+"practiceId----->"+practiceId+"--->subpractice"+subpractice);
			// System.out.println("response--" + response);

		} catch (SQLException se) {
			se.printStackTrace();
		} finally {
			try {

				if (callableStatement != null) {
					callableStatement.close();
					callableStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		return response;
	}

	/**
	 * This method is to show Pie Chart for Employee Count of requirements based
	 * on Status Created By: Triveni Niddara Created Date : 11/5/2017
	 * 
	 * @return
	 */

	@Override
	public String getSalesAccStatusPieChart(int graphId) throws ServiceLocatorException {

		String response = "";
		// Connection connection = null;
		CallableStatement callableStatement = null;
		Connection connection = null;

		try {
			connection = ConnectionProvider.getInstance().getConnection();
			callableStatement = connection.prepareCall("{call spSalesAccPieChartCounts(?,?)}");

			callableStatement.setInt(1, graphId);

			// System.out.println("graphId--" + graphId);
			callableStatement.registerOutParameter(2, Types.VARCHAR);
			callableStatement.execute();

			response = callableStatement.getString(2);
			// System.out.println("graphId-- 0----->"+graphId);
			// System.out.println("response-- 0--" + response);

		} catch (SQLException se) {
			se.printStackTrace();
		} finally {
			try {

				if (callableStatement != null) {
					callableStatement.close();
					callableStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		return response;
	}

	/**
	 * This method is to show Pie Chart for Employee Count of requirements based
	 * on Status Created By: Triveni Niddara Created Date : 11/5/2017
	 * 
	 * @return
	 */

	@Override
	public String getSalesAccRegionPieChart(int graphId) throws ServiceLocatorException {

		String response = "";
		// Connection connection = null;
		CallableStatement callableStatement = null;
		Connection connection = null;

		try {
			connection = ConnectionProvider.getInstance().getConnection();
			callableStatement = connection.prepareCall("{call spSalesAccPieChartCounts(?,?)}");

			callableStatement.setInt(1, graphId);

			// System.out.println("graphId--" + graphId);
			callableStatement.registerOutParameter(2, Types.VARCHAR);
			callableStatement.execute();

			response = callableStatement.getString(2);
			// System.out.println("graphId--- 1---->"+graphId);
			// System.out.println("response-- 1--" + response);

		} catch (SQLException se) {
			se.printStackTrace();
		} finally {
			try {

				if (callableStatement != null) {
					callableStatement.close();
					callableStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		return response;
	}

	/**
	 * This method is to show Pie Chart for Employee Count of requirements based
	 * on Status Created By: Triveni Niddara Created Date : 11/5/2017
	 * 
	 * @return
	 */

	@Override
	public String getSalesAccTerritoryPieChart(int graphId) throws ServiceLocatorException {

		String response = "";
		// Connection connection = null;
		CallableStatement callableStatement = null;
		Connection connection = null;

		try {
			connection = ConnectionProvider.getInstance().getConnection();
			callableStatement = connection.prepareCall("{call spSalesAccPieChartCounts(?,?)}");

			callableStatement.setInt(1, graphId);

			// System.out.println("graphId--" + graphId);
			callableStatement.registerOutParameter(2, Types.VARCHAR);
			callableStatement.execute();

			response = callableStatement.getString(2);
			// System.out.println("graphId-- 2--->"+graphId);
			// System.out.println("response-- 2--" + response);

		} catch (SQLException se) {
			se.printStackTrace();
		} finally {
			try {

				if (callableStatement != null) {
					callableStatement.close();
					callableStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		return response;
	}

	/**
	 * This method is to show Pie Chart for Employee Count of requirements based
	 * on Status Created By: Triveni Niddara Created Date : 11/5/2017
	 * 
	 * @return
	 */

	@Override
	public String getSalesAccStatePieChart(int graphId) throws ServiceLocatorException {

		String response = "";
		// Connection connection = null;
		CallableStatement callableStatement = null;
		Connection connection = null;

		try {
			connection = ConnectionProvider.getInstance().getConnection();
			callableStatement = connection.prepareCall("{call spSalesAccPieChartCounts(?,?)}");

			callableStatement.setInt(1, graphId);

			// System.out.println("graphId--" + graphId);
			callableStatement.registerOutParameter(2, Types.VARCHAR);
			callableStatement.execute();

			response = callableStatement.getString(2);
			// System.out.println("graphId-- 3--->"+graphId);
			// System.out.println("response-- 3--" + response);

		} catch (SQLException se) {
			se.printStackTrace();
		} finally {
			try {

				if (callableStatement != null) {
					callableStatement.close();
					callableStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		return response;
	}

	public String getProjectsByAccountIdPmo(int accountId, String userId, String department)
			throws ServiceLocatorException {
		// System.out.println("getProjectsByAccountIdPmo
		// accountId.."+accountId);
		StringBuffer projects = new StringBuffer();
		String projectName = null;
		int projectId = 0;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		Connection connection = null;
		// queryString = "SELECT distinct tblProjectContacts.ProjectId AS
		// projectId,ProjectName FROM tblProjectContacts LEFT OUTER JOIN
		// tblProjects ON(tblProjectContacts.ProjectId=tblProjects.Id) WHERE
		// AccountId="+accountId;
		String myTeamMembers = "";

		Map teamMap = DataSourceDataProvider.getInstance().getMyTeamMembers(userId, department);

		myTeamMembers = DataSourceDataProvider.getInstance().getTeamLoginIdList(teamMap);
		myTeamMembers = myTeamMembers.replaceAll("'", "");
		if (myTeamMembers.equals("")) {
			myTeamMembers = userId;
		} else {
			myTeamMembers = myTeamMembers + "," + userId;
		}
		// System.out.println("myTeamMembers-->"+myTeamMembers);
		if (accountId == -1 || accountId == 0) {
			// queryString = "SELECT tblProjectContacts.ObjectId as
			// EmpId,tblProjectContacts.ResourceName as EmpName FROM
			// tblProjectContacts LEFT OUTER JOIN tblProjects
			// ON(tblProjectContacts.ProjectId=tblProjects.Id) WHERE
			// ProjectId="+projectId+" and tblProjects.Status='Active' and
			// ObjectType = 'E'";
			// Map projectsMap = new HashMap();
			// projectsMap =
			// DataSourceDataProvider.getInstance().getProjectsMapPmo(userId);
			// String myTeamMembers = getKeys(projectsMap, ",");
			// myTeamMembers = myTeamMembers.replaceAll("'", "");
			// queryString = "SELECT DISTINCT tblProjectContacts.ObjectId AS
			// EmpId,tblProjectContacts.Resourcename AS EmpName FROM
			// tblProjectContacts"
			// + " WHERE ProjectId IN (" + myTeamMembers + " ) AND
			// STATUS='Active'";

			queryString = "SELECT DISTINCT tblProjects.Id as Id,tblProjects.ProjectName as ProjectName FROM tblProjects WHERE tblProjects.Status = 'Active' and FIND_IN_SET(tblProjects.PMO,'"
					+ myTeamMembers + "')  ORDER BY Id";
		} else {
			queryString = "select DISTINCT Id,ProjectName from tblProjects where CustomerId = " + accountId
					+ " AND FIND_IN_SET(tblProjects.PMO,'" + myTeamMembers
					+ "') AND STATUS='Active' ORDER BY ProjectName";
		}

		//// System.out.println("getProjectsByAccountIdPmo
		//// queryString.."+queryString);
		try {
			connection = ConnectionProvider.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(queryString);
			resultSet = preparedStatement.executeQuery();
			projects.append("<xml version=\"1.0\">");
			projects.append("<PROJECTS>");
			// projects.append("<USER projectId=\"-1\">--Please
			// Select--</USER>");
			projects.append("<USER projectId=\"-1\">All</USER>");
			while (resultSet.next()) {
				projectId = resultSet.getInt("Id");
				projectName = resultSet.getString("ProjectName");

				// projects.append(projectName);

				projects.append("<USER projectId=\"" + projectId + "\">");
				if (projectName.contains("&")) {
					projectName = projectName.replace("&", "&amp;");
				}
				projects.append(projectName);
				projects.append("</USER>");

			}
			projects.append("</PROJECTS>");
			projects.append("</xml>");
		} catch (SQLException ex) {
			ex.printStackTrace();
		} catch (ServiceLocatorException sle) {
			sle.printStackTrace();
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
		}
		// System.out.println("Team List: "+projects.toString());
		return projects.toString();
	}

	public String getEmployeesByProjectIdPmo(String projectId, String userId, String accountId)
			throws ServiceLocatorException {
		// StringBuffer projects = new StringBuffer();
		StringBuffer employees = new StringBuffer();
		// String projectName = null;
		String empName = null;
		// int projectId=0;
		int empId = 0;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		Connection connection = null;

		// queryString = "SELECT distinct tblProjectContacts.ProjectId AS
		// projectId,ProjectName FROM tblProjectContacts LEFT OUTER JOIN
		// tblProjects ON(tblProjectContacts.ProjectId=tblProjects.Id) WHERE
		// AccountId="+accountId;
		if (Integer.parseInt(projectId) == -1 || Integer.parseInt(projectId) == 0) {
			// queryString = "SELECT tblProjectContacts.ObjectId as
			// EmpId,tblProjectContacts.ResourceName as EmpName FROM
			// tblProjectContacts LEFT OUTER JOIN tblProjects
			// ON(tblProjectContacts.ProjectId=tblProjects.Id) WHERE
			// ProjectId="+projectId+" and tblProjects.Status='Active' and
			// ObjectType = 'E'";
			Map projectsMap = new HashMap();
			projectsMap = DataSourceDataProvider.getInstance().getProjectsMapPmo(userId);
			String myTeamMembers = getKeys(projectsMap, ",");
			myTeamMembers = myTeamMembers.replaceAll("'", "");
			// queryString = "SELECT DISTINCT tblProjectContacts.ObjectId AS
			// EmpId,tblProjectContacts.Resourcename AS EmpName FROM
			// tblProjectContacts"
			// + " WHERE ProjectId IN (" + myTeamMembers + " ) AND
			// STATUS='Active'";

			queryString = "SELECT DISTINCT ObjectId AS EmpId,Resourcename AS EmpName FROM tblProjectContacts WHERE AccountId = "
					+ accountId + " AND STATUS='Active'  ORDER BY EmpName";
		} else {
			// queryString = "SELECT tblProjectContacts.ObjectId as
			// EmpId,tblProjectContacts.ResourceName as EmpName FROM
			// tblProjectContacts LEFT OUTER JOIN tblProjects
			// ON(tblProjectContacts.ProjectId=tblProjects.Id) WHERE
			// ProjectId="+projectId+" and tblProjects.Status='Active' and
			// ObjectType = 'E' ORDER BY EmpName";
			queryString = "SELECT DISTINCT ObjectId as EmpId,Resourcename as EmpName FROM tblProjectContacts WHERE ProjectId="
					+ projectId + " and Status='Active'  ORDER BY EmpName";
		}

		// System.out.println("queryString-->"+queryString);
		try {
			connection = ConnectionProvider.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(queryString);
			resultSet = preparedStatement.executeQuery();
			employees.append("<xml version=\"1.0\">");
			// projects.append("<PROJECTS>");
			employees.append("<EMPLOYEES>");
			// projects.append("<USER projectId=\"-1\">--Please
			// Select--</USER>");
			// employees.append("<USER empId=\"-1\">--Please Select--</USER>");
			employees.append("<USER empId=\"-1\">All</USER>");
			while (resultSet.next()) {
				// projectId=resultSet.getInt("Id");
				empId = resultSet.getInt("EmpId");
				empName = resultSet.getString("EmpName");

				// projects.append(projectName);

				// projects.append("<USER projectId=\""+projectId+"\">");
				employees.append("<USER empId=\"" + empId + "\">");
				if (empName.contains("&")) {
					empName = empName.replace("&", "&amp;");
				}
				employees.append(empName);
				employees.append("</USER>");

			}
			employees.append("</EMPLOYEES>");
			employees.append("</xml>");
		} catch (SQLException ex) {
			ex.printStackTrace();
		} catch (ServiceLocatorException sle) {
			sle.printStackTrace();
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
		}
		// System.out.println("Team List: "+projects.toString());
		return employees.toString();
	}

	public String getEmployeesByCustomerIdPmo(int accountId, String userId) throws ServiceLocatorException {
		// StringBuffer projects = new StringBuffer();
		StringBuffer employees = new StringBuffer();
		// String projectName = null;
		String empName = null;
		// int projectId=0;
		int empId = 0;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		Connection connection = null;
		Map projectsMap = new HashMap();
		projectsMap = DataSourceDataProvider.getInstance().getProjectsMapPmo(userId);
		String myTeamMembers = getKeys(projectsMap, ",");
		myTeamMembers = myTeamMembers.replaceAll("'", "");
		System.out.println("queryString getEmployeesByCustomerIdPmo() accountId-->" + accountId);
		if (accountId == -1 || accountId == 0) {
			queryString = "SELECT DISTINCT tblProjectContacts.ObjectId AS EmpId,tblProjectContacts.Resourcename AS EmpName FROM tblProjectContacts"
					+ " WHERE ProjectId IN (" + myTeamMembers + " ) AND STATUS='Active'";
		}
		// queryString = "SELECT distinct tblProjectContacts.ProjectId AS
		// projectId,ProjectName FROM tblProjectContacts LEFT OUTER JOIN
		// tblProjects ON(tblProjectContacts.ProjectId=tblProjects.Id) WHERE
		// AccountId="+accountId;
		// queryString = "select Id,ProjectName from tblProjects where
		// CustomerId = " + accountId+" AND tblProjects.PMO='"+userId+"' AND
		// STATUS='Active' ORDER BY ProjectName";
		else {
			queryString = "SELECT DISTINCT ObjectId AS EmpId,Resourcename AS EmpName FROM tblProjectContacts WHERE AccountId = "
					+ accountId + " AND STATUS='Active'  ORDER BY EmpName";
		}

		System.out.println("queryString getEmployeesByCustomerIdPmo()-->" + queryString);
		try {
			connection = ConnectionProvider.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(queryString);
			resultSet = preparedStatement.executeQuery();
			employees.append("<xml version=\"1.0\">");
			// projects.append("<PROJECTS>");
			employees.append("<EMPLOYEES>");
			// projects.append("<USER projectId=\"-1\">--Please
			// Select--</USER>");
			// employees.append("<USER empId=\"-1\">--Please Select--</USER>");
			employees.append("<USER empId=\"-1\">All</USER>");
			while (resultSet.next()) {
				// projectId=resultSet.getInt("Id");
				empId = resultSet.getInt("EmpId");
				empName = resultSet.getString("EmpName");

				// projects.append(projectName);

				// projects.append("<USER projectId=\""+projectId+"\">");
				employees.append("<USER empId=\"" + empId + "\">");
				if (empName.contains("&")) {
					empName = empName.replace("&", "&amp;");
				}
				employees.append(empName);
				employees.append("</USER>");

			}
			employees.append("</EMPLOYEES>");
			employees.append("</xml>");
		} catch (SQLException ex) {
			ex.printStackTrace();
		} catch (ServiceLocatorException sle) {
			sle.printStackTrace();
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
		}
		// System.out.println("Team List: "+projects.toString());
		return employees.toString();
	}

	public String getTimeSheetStatusReportDetailsList(NewAjaxHandlerAction ajaxhandleraction, String userId)
			throws ServiceLocatorException {
		Connection connection = null;
		CallableStatement callableStatement = null;
		String responseString = "";

		String queryString = "{call spGetTimeSheetWeeklyStatusDetails(?,?,?,?,?,?,?,?)}";

		try {
			connection = ConnectionProvider.getInstance().getConnection();
			callableStatement = connection.prepareCall(queryString);
			callableStatement.setString(1, ajaxhandleraction.getYear());

			// System.out.println(" the year is" +ajaxhandleraction.getYear());

			callableStatement.setString(2, ajaxhandleraction.getMonth());
			// System.out.println(" the month is"
			// +ajaxhandleraction.getMonth());

			callableStatement.setString(3, ajaxhandleraction.getEmployeeName());
			// System.out.println(" the employee Id is"
			// +ajaxhandleraction.getEmployeeName());

			callableStatement.setString(4, ajaxhandleraction.getStatus());
			// System.out.println(" Status is" +ajaxhandleraction.getStatus());

			callableStatement.setString(5, ajaxhandleraction.getWeek());
			// System.out.println(" Status is" +ajaxhandleraction.getWeek());
			callableStatement.setString(6, userId);
			// System.out.println(" userId is" +userId);
			callableStatement.setInt(7, ajaxhandleraction.getPgNo());
			// System.out.println(" userId is" + ajaxhandleraction.getPgNo());

			callableStatement.registerOutParameter(8, java.sql.Types.VARCHAR);

			// System.out.println("response String is "+responseString);
			callableStatement.executeQuery();
			responseString = callableStatement.getString(8);
			// System.out.println(" responseString" +responseString);
		} catch (SQLException se) {
			se.printStackTrace();

			throw new ServiceLocatorException(se);
		} finally {
			try {

				if (callableStatement != null) {
					callableStatement.close();
					callableStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException se) {
				se.printStackTrace();
				throw new ServiceLocatorException(se);
			}
		}
		return responseString;
	}

	public String getEmpProjectExcelReport(NewAjaxHandlerAction ajaxHandlerAction) throws ServiceLocatorException {

		String response = "";
		// Connection connection = null;
		CallableStatement callableStatement = null;
		Connection connection = null;

		try {

			connection = ConnectionProvider.getInstance().getConnection();

			int j = 1;

			callableStatement = connection.prepareCall("{call spGetEmpProjectExcelRpt(?,?,?,?,?,?,?,?,?)}");

			callableStatement.setString(1, DateUtility.getInstance().convertDateToMySql1(
					DateUtility.getInstance().convertStringToMySql(ajaxHandlerAction.getStartDate())));

			callableStatement.setString(2, DateUtility.getInstance().convertDateToMySql1(
					DateUtility.getInstance().convertStringToMySql(ajaxHandlerAction.getEndDate())));

			if (ajaxHandlerAction.getCountry() != null && !"-1".equals(ajaxHandlerAction.getCountry())
					&& !"".equals(ajaxHandlerAction.getCountry())) {

				callableStatement.setString(3, ajaxHandlerAction.getCountry());
			} else {
				callableStatement.setString(3, "%");
			}

			if (ajaxHandlerAction.getDepartmentId() != null && !"-1".equals(ajaxHandlerAction.getDepartmentId())
					&& !"".equals(ajaxHandlerAction.getDepartmentId().trim())) {
				callableStatement.setString(4, ajaxHandlerAction.getDepartmentId());
			} else {
				callableStatement.setString(4, "GDC,SSG");
			}

			if (ajaxHandlerAction.getPracticeId() != null && !"".equals(ajaxHandlerAction.getPracticeId())
					&& !"-1".equals(ajaxHandlerAction.getPracticeId())) {
				callableStatement.setString(5, ajaxHandlerAction.getPracticeId());
			} else {
				callableStatement.setString(5, "%");
			}
			if (ajaxHandlerAction.getEmpId() == null || "".equals(ajaxHandlerAction.getEmpId())
					|| "-1".equals(ajaxHandlerAction.getEmpId())) {
				callableStatement.setString(6, "%");
			} else {
				callableStatement.setString(6, "" + ajaxHandlerAction.getEmpId() + "");
			}
			if (ajaxHandlerAction.getOpsContactId() == null || "-1".equals(ajaxHandlerAction.getOpsContactId())
					|| "".equals(ajaxHandlerAction.getOpsContactId())) {
				callableStatement.setString(7, "%");
			} else {
				callableStatement.setString(7, "" + ajaxHandlerAction.getOpsContactId() + "");
			}

			if (ajaxHandlerAction.getLocation() == null || "".equals(ajaxHandlerAction.getLocation())
					|| "-1".equals(ajaxHandlerAction.getLocation())) {
				callableStatement.setString(8, "%");
			} else {
				callableStatement.setString(8, "" + ajaxHandlerAction.getLocation() + "");
			}
			callableStatement.registerOutParameter(9, Types.VARCHAR);

			callableStatement.execute();
			response = callableStatement.getString(9);
			// System.out.println("resultMessage " + response);

		} catch (SQLException se) {
			se.printStackTrace();
		} finally {
			try {

				if (callableStatement != null) {
					callableStatement.close();
					callableStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		return response;
	}

	public String getRequirementAjaxListVP(HttpServletRequest httpServletRequest) throws ServiceLocatorException {
		stringBuffer = new StringBuffer();
		CallableStatement callableStatement = null;
		PreparedStatement preparedStatement = null;
		Statement statement = null;
		ResultSet resultSet = null;
		String createdBy = "";
		String totalStream = "";
		StringBuffer queryStringBuffer;
		int i = 0;
		String temp = "";
		String userWorkCountry = httpServletRequest.getSession(false).getAttribute(ApplicationConstants.WORKING_COUNTRY)
				.toString();

		try {

			queryStringBuffer = new StringBuffer();
			String territory = "";
			// httpServletRequest.getSession(false).setAttribute("dummyStatus",
			// "Open");
			queryStringBuffer.append(
					"SELECT COUNT(tblRecRequirement.Id) AS resumecount,TRIM(tblRecRequirement .Id) AS RequirementId,TRIM(tblRecConsultant.Id) AS ConsultantId,TRIM(JobTitle) AS JobTitle, ");
			queryStringBuffer.append("CONCAT(tblRecConsultant.FName,'.',tblRecConsultant.LName) AS ConsultantNAME ,");
			queryStringBuffer.append(
					"CASE WHEN (`tblRecRequirement`.`State` !='' AND `tblRecRequirement`.`State` IS NOT NULL) THEN CONCAT(`tblRecRequirement`.`State`,',',`tblRecRequirement`.`Country`) ELSE `tblRecRequirement`.`Country` END AS Location,");
			queryStringBuffer.append("tblRecRequirement .STATUS AS status,tblRec.CreatedDate as SubmittedDate,");
			queryStringBuffer.append(
					"tblRecRequirement.AssignedDate as AssignedDate,tblRecRequirement.NoResumes AS noofresumes,");
			queryStringBuffer.append(
					"AssignedTo AS Recruiter,SecondaryRecruiter AS SecondaryRecruiter,AssignToTechLead AS PreSales,");
			queryStringBuffer.append(
					"Skills,tblCrmAccount.NAME AS AccountName FROM tblRecRequirement LEFT JOIN(tblRecConsultant, tblRec)");
			queryStringBuffer.append(
					"ON (tblRecRequirement.Id=tblRec.RequirementId AND tblRecConsultant.Id=tblRec.ConsultantId) LEFT OUTER JOIN tblCrmAccount ON (tblRecRequirement.CustomerId=tblCrmAccount.Id) ");
			queryStringBuffer.append("WHERE 1=1 ");
			// queryStringBuffer.append("WHERE tblRecRequirement.Country LIKE
			// '%' " );
			String userLoginId = (String) httpServletRequest.getSession(false)
					.getAttribute(ApplicationConstants.SESSION_USER_ID);

			String[] offShoreAccess = SecurityProperties.getProperty("Requirements_Add_Access")
					.split(Pattern.quote(","));

			for (int j = 0; j < offShoreAccess.length; j++) {
				String name = "";
				if (j == 0) {
					name = offShoreAccess[j] + "'" + ",";
				}

				else if (j == offShoreAccess.length - 1) {
					name = "'" + offShoreAccess[j];
				} else {
					name = "'" + offShoreAccess[j] + "'" + ",";
				}

				temp = temp.concat(name);

			}

			queryStringBuffer.append(" AND tblRecRequirement.CreatedBy IN ('" + temp + "' )");

			queryStringBuffer
					.append(" GROUP BY tblRecRequirement.Id ORDER BY tblRecRequirement.DatePosted DESC Limit 200");

			// System.out.println("Default query ----" +
			// queryStringBuffer.toString());

			connection = ConnectionProvider.getInstance().getConnection();
			statement = connection.createStatement();
			resultSet = statement.executeQuery(queryStringBuffer.toString());
			// System.err.println("Account Activities:"+queryString);
			while (resultSet.next()) {

				int resumeCount = resultSet.getInt("resumecount");
				int RequirementId = resultSet.getInt("RequirementId");

				int ConsultantId = resultSet.getInt("ConsultantId");
				String JobTitle = resultSet.getString("JobTitle");

				String ConsultantNAME = resultSet.getString("ConsultantNAME");

				String Location = resultSet.getString("Location");

				String status = resultSet.getString("status");

				String SubmittedDate = resultSet.getString("SubmittedDate");
				String AssignedDate = resultSet.getString("AssignedDate");
				String AccountName = resultSet.getString("AccountName");

				int noofresumes = resultSet.getInt("noofresumes");

				String Recruiter = "-";
				if (resultSet.getString("Recruiter") != null || resultSet.getString("Recruiter") != "") {
					Recruiter = resultSet.getString("Recruiter");
				}

				String SecondaryRecruiter = "-";
				if (resultSet.getString("SecondaryRecruiter") != null
						|| resultSet.getString("SecondaryRecruiter") != "") {
					SecondaryRecruiter = resultSet.getString("SecondaryRecruiter");
				}

				String PreSales = "-";
				if (resultSet.getString("PreSales") != null || resultSet.getString("PreSales") != "") {
					PreSales = resultSet.getString("PreSales");
				}

				String Skills = resultSet.getString("Skills");

				i++;
				/*
				 * createdBy=resultSet.getString("CreatedById"); count
				 * =resultSet.getInt("total");
				 */
				// totalStream=totalStream+i+"|"+createdDate+"|"+actType+"|"+description+"|"+comments+"|"+assignedToId+"|"+status+"|"+datedue+"|"+contactId+"|"+accountId+"|"+"^";
				// totalStream=totalStream+i+"|"+createdDate+"|"+actType+"|"+description+"|"+comments+"|"+assignedToId+"|"+status+"|"+datedue+"|"+"^";
				totalStream = totalStream + i + "|" + resumeCount + "|" + RequirementId + "|" + ConsultantId + "|"
						+ JobTitle + "|" + ConsultantNAME + "|" + Location + "|" + status + "|" + SubmittedDate + "|"
						+ AssignedDate + "|" + noofresumes + "|" + Recruiter + "|" + SecondaryRecruiter + "|" + PreSales
						+ "|" + PreSales + "|" + Skills + "|" + AccountName + "^";
				// totalActivities=totalActivities+count;
			}
			stringBuffer.append(totalStream);
			stringBuffer.append("addto");

			stringBuffer.append(i);

		} catch (Exception sqe) {
			sqe.printStackTrace();
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (statement != null) {
					statement.close();
					statement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException sqle) {
			}
		}
		// System.err.println("response string is"+stringBuffer.toString());
		return stringBuffer.toString();
	}

	public String getSearchRequirementAjaxListVP(HttpServletRequest httpServletRequest,
			NewAjaxHandlerAction ajaxHandlerAction) throws ServiceLocatorException {

		stringBuffer = new StringBuffer();
		CallableStatement callableStatement = null;
		PreparedStatement preparedStatement = null;
		Statement statement = null;
		ResultSet resultSet = null;
		String createdBy = "";
		String totalStream = "";
		String queryString = "";
		int i = 0;

		String temp = "";

		String userWorkCountry = httpServletRequest.getSession(false).getAttribute(ApplicationConstants.WORKING_COUNTRY)
				.toString();

		int userRoleId = Integer.parseInt(
				httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_ROLE_ID).toString());

		DateUtility dateUtility = new DateUtility();

		try {

			httpServletRequest.getSession(false).setAttribute("dummyTitle", ajaxHandlerAction.getTitle());
			httpServletRequest.getSession(false).setAttribute("dummyStatus", ajaxHandlerAction.getStatus());
			httpServletRequest.getSession(false).setAttribute("dummyAssignedTo", ajaxHandlerAction.getAssignedTo());
			httpServletRequest.getSession(false).setAttribute("dummyStartDate", ajaxHandlerAction.getPostedDate1());
			httpServletRequest.getSession(false).setAttribute("dummyEndDate", ajaxHandlerAction.getPostedDate2());
			httpServletRequest.getSession(false).setAttribute("dummyCreatedBy", ajaxHandlerAction.getCreatedBy());
			httpServletRequest.getSession(false).setAttribute("dummyAssignedBy", ajaxHandlerAction.getAssignedBy());
			httpServletRequest.getSession(false).setAttribute("dummyClientId", ajaxHandlerAction.getClientId());

			int columnCounter = 0;
			StringBuffer queryStringBuffer = new StringBuffer();
			String territory = "";
			queryStringBuffer.append(
					"SELECT COUNT(tblRecRequirement.Id) AS resumecount,TRIM(tblRecRequirement .Id) AS RequirementId,TRIM(tblRecConsultant.Id) AS ConsultantId,TRIM(JobTitle) AS JobTitle, ");
			queryStringBuffer.append("CONCAT(tblRecConsultant.FName,'.',tblRecConsultant.LName) AS ConsultantNAME ,");
			queryStringBuffer.append(
					"CASE WHEN (`tblRecRequirement`.`State` !='' AND `tblRecRequirement`.`State` IS NOT NULL) THEN CONCAT(`tblRecRequirement`.`State`,',',`tblRecRequirement`.`Country`) ELSE `tblRecRequirement`.`Country` END AS Location,");
			queryStringBuffer.append("tblRecRequirement .STATUS AS status,tblRec.CreatedDate as SubmittedDate,");
			queryStringBuffer.append(
					"tblRecRequirement.AssignedDate as AssignedDate,tblRecRequirement.NoResumes AS noofresumes,");
			queryStringBuffer.append(
					"AssignedTo AS Recruiter,SecondaryRecruiter AS SecondaryRecruiter,AssignToTechLead AS PreSales,");
			queryStringBuffer.append(
					"Skills,tblCrmAccount.NAME AS AccountName FROM tblRecRequirement LEFT JOIN(tblRecConsultant, tblRec)");
			queryStringBuffer.append(
					"ON (tblRecRequirement.Id=tblRec.RequirementId AND tblRecConsultant.Id=tblRec.ConsultantId) LEFT OUTER JOIN tblCrmAccount ON (tblRecRequirement.CustomerId=tblCrmAccount.Id) ");
			// queryStringBuffer.append("WHERE tblRecRequirement.Country LIKE
			// '"+userWorkCountry+"' " );
			queryStringBuffer.append("WHERE 1=1  ");
			// queryStringBuffer.append("WHERE tblRecRequirement.Country LIKE
			// '%' " );

			String userLoginId = (String) httpServletRequest.getSession(false)
					.getAttribute(ApplicationConstants.SESSION_USER_ID);

			String[] offShoreAccess = SecurityProperties.getProperty("Requirements_Add_Access")
					.split(Pattern.quote(","));

			for (int j = 0; j < offShoreAccess.length; j++) {
				String name = "";
				if (j == 0) {
					name = offShoreAccess[j] + "'" + ",";
				}

				else if (j == offShoreAccess.length - 1) {
					name = "'" + offShoreAccess[j];
				} else {
					name = "'" + offShoreAccess[j] + "'" + ",";
				}

				temp = temp.concat(name);

			}

			columnCounter++;

			queryStringBuffer.append("AND tblRecRequirement.CreatedBy IN ('" + temp + "') ");

			/**
			 *
			 * where Start
			 */
			if (null == ajaxHandlerAction.getCreatedBy()) {
				ajaxHandlerAction.setCreatedBy("");
			}
			if ("All".equalsIgnoreCase(ajaxHandlerAction.getCreatedBy())) {
				ajaxHandlerAction.setCreatedBy("");
			}
			if (null == ajaxHandlerAction.getAssignedTo()) {
				ajaxHandlerAction.setAssignedTo("");
			}
			if ("All".equalsIgnoreCase(ajaxHandlerAction.getAssignedTo())) {
				ajaxHandlerAction.setAssignedTo("");
			}
			if (null == ajaxHandlerAction.getStatus()) {
				ajaxHandlerAction.setStatus("");
			}
			if ("All".equalsIgnoreCase(ajaxHandlerAction.getStatus())) {
				ajaxHandlerAction.setStatus("");
			}
			if (null == ajaxHandlerAction.getTitle()) {
				ajaxHandlerAction.setTitle("");
			}
			if (null == ajaxHandlerAction.getPostedDate1()) {
				ajaxHandlerAction.setPostedDate1("");
			}
			if (null == ajaxHandlerAction.getPostedDate2()) {
				ajaxHandlerAction.setPostedDate2("");
			}
			// commented by prasad
			if (!"".equalsIgnoreCase(ajaxHandlerAction.getPostedDate1())) {
				ajaxHandlerAction.setPostedDate1(
						DateUtility.getInstance().convertStringToMySQLDate(ajaxHandlerAction.getPostedDate1()));
			}

			if (!"".equalsIgnoreCase(ajaxHandlerAction.getPostedDate2())) {
				ajaxHandlerAction.setPostedDate2(
						DateUtility.getInstance().convertStringToMySQLDate(ajaxHandlerAction.getPostedDate2()));
			}
			if ("All".equalsIgnoreCase(ajaxHandlerAction.getAssignedBy())) {
				ajaxHandlerAction.setAssignedBy("");
			}

			/*
			 * if ((!"".equalsIgnoreCase(ajaxHandlerAction.getCreatedBy())) ||
			 * (!"".equalsIgnoreCase(ajaxHandlerAction.getAssignedTo())) ||
			 * (!"".equalsIgnoreCase(ajaxHandlerAction.getStatus())) ||
			 * (!"".equalsIgnoreCase(ajaxHandlerAction.getTitle())) ||
			 * (!"".equalsIgnoreCase(ajaxHandlerAction.getPostedDate1())) ||
			 * (!"".equalsIgnoreCase(ajaxHandlerAction.getPostedDate2()))) {
			 * queryStringBuffer.append(" WHERE "); }
			 */

			if (!"".equalsIgnoreCase(ajaxHandlerAction.getAssignedTo()) && columnCounter == 0) {
				if ((ajaxHandlerAction.getAssignedTo().indexOf("*") == -1)
						&& (ajaxHandlerAction.getAssignedTo().indexOf("%") == -1)) {
					ajaxHandlerAction.setAssignedTo(ajaxHandlerAction.getAssignedTo() + "*");
				}
				ajaxHandlerAction.setAssignedTo(ajaxHandlerAction.getAssignedTo().replace("*", "%"));
				queryStringBuffer.append(" (`tblRecRequirement`.`AssignedTo` LIKE '" + ajaxHandlerAction.getAssignedTo()
						+ "' OR tblRecRequirement.SecondaryRecruiter  LIKE'" + ajaxHandlerAction.getAssignedTo()
						+ "') ");
				columnCounter++;

				ajaxHandlerAction.setAssignedTo(ajaxHandlerAction.getAssignedTo().replace("%", ""));
			} else if (!"".equalsIgnoreCase(ajaxHandlerAction.getAssignedTo()) && columnCounter != 0) {
				if ((ajaxHandlerAction.getAssignedTo().indexOf("*") == -1)
						&& (ajaxHandlerAction.getAssignedTo().indexOf("%") == -1)) {
					ajaxHandlerAction.setAssignedTo(ajaxHandlerAction.getAssignedTo() + "*");
				}
				ajaxHandlerAction.setAssignedTo(ajaxHandlerAction.getAssignedTo().replace("*", "%"));
				queryStringBuffer.append("AND (`tblRecRequirement`.`AssignedTo` LIKE '"
						+ ajaxHandlerAction.getAssignedTo() + "' OR tblRecRequirement.SecondaryRecruiter  LIKE'"
						+ ajaxHandlerAction.getAssignedTo() + "') ");
				columnCounter++;

				ajaxHandlerAction.setAssignedTo(ajaxHandlerAction.getAssignedTo().replace("%", ""));
			}
			if (!"".equalsIgnoreCase(ajaxHandlerAction.getAssignedBy()) && columnCounter == 0) {
				if ((ajaxHandlerAction.getAssignedBy().indexOf("*") == -1)
						&& (ajaxHandlerAction.getAssignedBy().indexOf("%") == -1)) {
					ajaxHandlerAction.setAssignedBy(ajaxHandlerAction.getAssignedBy() + "*");
				}
				ajaxHandlerAction.setAssignedBy(ajaxHandlerAction.getAssignedBy().replace("*", "%"));
				queryStringBuffer
						.append("`tblRecRequirement`.`AssignedBy` LIKE '" + ajaxHandlerAction.getAssignedBy() + "'");
				columnCounter++;

				ajaxHandlerAction.setAssignedBy(ajaxHandlerAction.getAssignedBy().replace("%", ""));
			} else if (!"".equalsIgnoreCase(ajaxHandlerAction.getAssignedBy()) && columnCounter != 0) {
				if ((ajaxHandlerAction.getAssignedBy().indexOf("*") == -1)
						&& (ajaxHandlerAction.getAssignedBy().indexOf("%") == -1)) {
					ajaxHandlerAction.setAssignedBy(ajaxHandlerAction.getAssignedBy() + "*");
				}
				ajaxHandlerAction.setAssignedBy(ajaxHandlerAction.getAssignedBy().replace("*", "%"));
				queryStringBuffer.append(
						"AND `tblRecRequirement`.`AssignedBy` LIKE '" + ajaxHandlerAction.getAssignedBy() + "'");
				columnCounter++;

				ajaxHandlerAction.setAssignedBy(ajaxHandlerAction.getAssignedBy().replace("%", ""));
			}
			if (!"".equalsIgnoreCase(ajaxHandlerAction.getStatus()) && columnCounter == 0) {
				if ((ajaxHandlerAction.getStatus().indexOf("*") == -1)
						&& (ajaxHandlerAction.getStatus().indexOf("%") == -1)) {
					ajaxHandlerAction.setStatus(ajaxHandlerAction.getStatus() + "*");
				}
				ajaxHandlerAction.setStatus(ajaxHandlerAction.getStatus().replace("*", "%"));
				queryStringBuffer.append("`tblRecRequirement`.`Status` LIKE '" + ajaxHandlerAction.getStatus() + "'");
				columnCounter++;

				ajaxHandlerAction.setStatus(ajaxHandlerAction.getStatus().replace("%", ""));
			} else if (!"".equalsIgnoreCase(ajaxHandlerAction.getStatus()) && columnCounter != 0) {
				if ((ajaxHandlerAction.getStatus().indexOf("*") == -1)
						&& (ajaxHandlerAction.getStatus().indexOf("%") == -1)) {
					ajaxHandlerAction.setStatus(ajaxHandlerAction.getStatus() + "*");
				}
				ajaxHandlerAction.setStatus(ajaxHandlerAction.getStatus().replace("*", "%"));
				queryStringBuffer
						.append("AND `tblRecRequirement`.`Status` LIKE '" + ajaxHandlerAction.getStatus() + "'");
				columnCounter++;

				ajaxHandlerAction.setStatus(ajaxHandlerAction.getStatus().replace("%", ""));
			}

			if (!"".equalsIgnoreCase(ajaxHandlerAction.getTitle()) && columnCounter == 0) {
				if ((ajaxHandlerAction.getTitle().indexOf("*") == -1)
						&& (ajaxHandlerAction.getTitle().indexOf("%") == -1)) {
					ajaxHandlerAction.setTitle(ajaxHandlerAction.getTitle() + "*");
				}
				ajaxHandlerAction.setTitle(ajaxHandlerAction.getTitle().replace("*", "%"));
				queryStringBuffer.append("`tblRecRequirement`.`JobTitle` LIKE '" + ajaxHandlerAction.getTitle() + "'");
				columnCounter++;

			} else if (!"".equalsIgnoreCase(ajaxHandlerAction.getTitle()) && columnCounter != 0) {
				if ((ajaxHandlerAction.getTitle().indexOf("*") == -1)
						&& (ajaxHandlerAction.getTitle().indexOf("%") == -1)) {
					ajaxHandlerAction.setTitle(ajaxHandlerAction.getTitle() + "*");
				}
				ajaxHandlerAction.setTitle(ajaxHandlerAction.getTitle().replace("*", "%"));
				queryStringBuffer
						.append("AND `tblRecRequirement`.`JobTitle` LIKE '" + ajaxHandlerAction.getTitle() + "'");
				columnCounter++;
			}

			if (!"".equalsIgnoreCase(ajaxHandlerAction.getPostedDate1())
					&& "".equalsIgnoreCase(ajaxHandlerAction.getPostedDate2()) && columnCounter == 0) {
				if ((ajaxHandlerAction.getPostedDate1().indexOf("*") == -1)
						&& (ajaxHandlerAction.getPostedDate1().indexOf("%") == -1)) {
					ajaxHandlerAction.setPostedDate1(ajaxHandlerAction.getPostedDate1() + "*");
				}
				ajaxHandlerAction.setPostedDate1(ajaxHandlerAction.getPostedDate1().replace("*", "%"));
				queryStringBuffer.append(
						"date(`tblRecRequirement`.`DatePosted`) LIKE '" + ajaxHandlerAction.getPostedDate1() + "'");
				columnCounter++;

				ajaxHandlerAction.setPostedDate1(
						dateUtility.convertToviewFormat(ajaxHandlerAction.getPostedDate1().replace("%", "")));
			} else if (!"".equalsIgnoreCase(ajaxHandlerAction.getPostedDate1())
					&& "".equalsIgnoreCase(ajaxHandlerAction.getPostedDate2()) && columnCounter != 0) {
				if ((ajaxHandlerAction.getPostedDate1().indexOf("*") == -1)
						&& (ajaxHandlerAction.getPostedDate1().indexOf("%") == -1)) {
					ajaxHandlerAction.setPostedDate1(ajaxHandlerAction.getPostedDate1() + "*");
				}
				ajaxHandlerAction.setPostedDate1(ajaxHandlerAction.getPostedDate1().replace("*", "%"));
				queryStringBuffer.append(
						"AND date(`tblRecRequirement`.`DatePosted`) LIKE '" + ajaxHandlerAction.getPostedDate1() + "'");
				columnCounter++;

				ajaxHandlerAction.setPostedDate1(
						dateUtility.convertToviewFormat(ajaxHandlerAction.getPostedDate1().replace("%", "")));
			}

			if (!"".equalsIgnoreCase(ajaxHandlerAction.getPostedDate2())
					&& "".equalsIgnoreCase(ajaxHandlerAction.getPostedDate1()) && columnCounter == 0) {
				if ((ajaxHandlerAction.getPostedDate2().indexOf("*") == -1)
						&& (ajaxHandlerAction.getPostedDate2().indexOf("%") == -1)) {
					ajaxHandlerAction.setPostedDate2(ajaxHandlerAction.getPostedDate2() + "*");
				}
				ajaxHandlerAction.setPostedDate2(ajaxHandlerAction.getPostedDate2().replace("*", "%"));
				queryStringBuffer.append(
						"date(`tblRecRequirement`.`DatePosted`) LIKE '" + ajaxHandlerAction.getPostedDate2() + "'");
				columnCounter++;

				ajaxHandlerAction.setPostedDate2(
						dateUtility.convertToviewFormat(ajaxHandlerAction.getPostedDate2().replace("%", "")));
			} else if (!"".equalsIgnoreCase(ajaxHandlerAction.getPostedDate2())
					&& "".equalsIgnoreCase(ajaxHandlerAction.getPostedDate1()) && columnCounter != 0) {
				if ((ajaxHandlerAction.getPostedDate2().indexOf("*") == -1)
						&& (ajaxHandlerAction.getPostedDate2().indexOf("%") == -1)) {
					ajaxHandlerAction.setPostedDate2(ajaxHandlerAction.getPostedDate2() + "*");
				}
				ajaxHandlerAction.setPostedDate2(ajaxHandlerAction.getPostedDate2().replace("*", "%"));
				queryStringBuffer.append(
						"AND date(`tblRecRequirement`.`DatePosted`) LIKE '" + ajaxHandlerAction.getPostedDate2() + "'");
				columnCounter++;

				ajaxHandlerAction.setPostedDate2(
						dateUtility.convertToviewFormat(ajaxHandlerAction.getPostedDate2().replace("%", "")));
			}

			if (!"".equalsIgnoreCase(ajaxHandlerAction.getPostedDate1())
					&& !"".equalsIgnoreCase(ajaxHandlerAction.getPostedDate2()) && columnCounter == 0) {
				if ((ajaxHandlerAction.getPostedDate1().indexOf("*") == -1)
						&& (ajaxHandlerAction.getPostedDate1().indexOf("%") == -1)) {
					ajaxHandlerAction.setPostedDate1(ajaxHandlerAction.getPostedDate1() + "*");
				}
				ajaxHandlerAction.setPostedDate1(ajaxHandlerAction.getPostedDate1().replace("*", "%"));

				if ((ajaxHandlerAction.getPostedDate2().indexOf("*") == -1)
						&& (ajaxHandlerAction.getPostedDate2().indexOf("%") == -1)) {
					ajaxHandlerAction.setPostedDate2(ajaxHandlerAction.getPostedDate2() + "*");
				}
				ajaxHandlerAction.setPostedDate2(ajaxHandlerAction.getPostedDate2().replace("*", "%"));

				queryStringBuffer.append("date(`tblRecRequirement`.`DatePosted`) BETWEEN '"
						+ ajaxHandlerAction.getPostedDate1() + "' AND '" + ajaxHandlerAction.getPostedDate2() + "'");
				columnCounter++;

				ajaxHandlerAction.setPostedDate1(
						dateUtility.convertToviewFormat(ajaxHandlerAction.getPostedDate1().replace("%", "")));
				ajaxHandlerAction.setPostedDate2(
						dateUtility.convertToviewFormat(ajaxHandlerAction.getPostedDate2().replace("%", "")));

			} else if (!"".equalsIgnoreCase(ajaxHandlerAction.getPostedDate1())
					&& !"".equalsIgnoreCase(ajaxHandlerAction.getPostedDate2()) && columnCounter != 0) {
				if ((ajaxHandlerAction.getPostedDate1().indexOf("*") == -1)
						&& (ajaxHandlerAction.getPostedDate1().indexOf("%") == -1)) {
					ajaxHandlerAction.setPostedDate1(ajaxHandlerAction.getPostedDate1() + "*");
				}
				ajaxHandlerAction.setPostedDate1(ajaxHandlerAction.getPostedDate1().replace("*", "%"));

				if ((ajaxHandlerAction.getPostedDate2().indexOf("*") == -1)
						&& (ajaxHandlerAction.getPostedDate2().indexOf("%") == -1)) {
					ajaxHandlerAction.setPostedDate2(ajaxHandlerAction.getPostedDate2() + "*");
				}
				ajaxHandlerAction.setPostedDate2(ajaxHandlerAction.getPostedDate2().replace("*", "%"));

				queryStringBuffer.append("AND date(`tblRecRequirement`.`DatePosted`) BETWEEN '"
						+ ajaxHandlerAction.getPostedDate1() + "' AND '" + ajaxHandlerAction.getPostedDate2() + "'");
				columnCounter++;

				ajaxHandlerAction.setPostedDate1(
						dateUtility.convertToviewFormat(ajaxHandlerAction.getPostedDate1().replace("%", "")));
				ajaxHandlerAction.setPostedDate2(
						dateUtility.convertToviewFormat(ajaxHandlerAction.getPostedDate2().replace("%", "")));
			}

			// working country removed
			/*
			 * if (columnCounter == 0) {//queryStringBuffer.
			 * append(" Where `tblRecRequirement`.`Country` like '"
			 * +userWorkCountry+"' ORDER BY date(`tblRecRequirement`.`DatePosted`),Country DESC"
			 * ); ,tblRecRequirement.Country queryStringBuffer.
			 * append(" Where `tblRecRequirement`.`Country` like '" +
			 * userWorkCountry + "' "); } else { //queryStringBuffer.
			 * append(" AND `tblRecRequirement`.`Country` like '"
			 * +userWorkCountry+"' ORDER BY date(`tblRecRequirement`.`DatePosted`),Country DESC"
			 * );,tblRecRequirement.Country queryStringBuffer.
			 * append(" AND `tblRecRequirement`.`Country` like '" +
			 * userWorkCountry + "' "); //
			 * httpServletRequest.getSession(false).setAttribute(
			 * ApplicationConstants.QUERY_STRING,queryStringBuffer.toString());
			 * }
			 */

			if (!"-1".equals(ajaxHandlerAction.getPracticeid()) && ajaxHandlerAction.getPracticeid() != null) {
				// System.out.println("ajaxHandlerAction.getPracticeid()---"+ajaxHandlerAction.getPracticeid());
				queryStringBuffer
						.append("  AND tblRecRequirement.Practice like '" + ajaxHandlerAction.getPracticeid() + "' ");
			}
			if (ajaxHandlerAction.getRequirementId() != 0) {
				queryStringBuffer.append("  AND tblRecRequirement .Id =" + ajaxHandlerAction.getRequirementId() + " ");
			}

			if (!"-1".equals(ajaxHandlerAction.getPracticeid()) && ajaxHandlerAction.getPracticeid() != null) {
				// System.out.println("ajaxHandlerAction.getPracticeid()---"+ajaxHandlerAction.getPracticeid());
			}
			if (!"-1".equals(ajaxHandlerAction.getCountry()) && ajaxHandlerAction.getCountry() != null) {
				// System.out.println("ajaxHandlerAction.getCountry()----"+ajaxHandlerAction.getCountry());
				queryStringBuffer
						.append(" AND `tblRecRequirement`.`Country` like '" + ajaxHandlerAction.getCountry() + "' ");
				if (!"".equals(ajaxHandlerAction.getState()) && ajaxHandlerAction.getState() != null) {
					queryStringBuffer
							.append(" AND `tblRecRequirement`.`state` like '" + ajaxHandlerAction.getState() + "' ");
				}
			}
			if (ajaxHandlerAction.getPreSalesPerson() != null && !"".equals(ajaxHandlerAction.getPreSalesPerson())) {
				queryStringBuffer.append("AND (tblRecRequirement.AssignToTechLead ='"
						+ ajaxHandlerAction.getPreSalesPerson() + "' || tblRecRequirement.SecondaryTechLead ='"
						+ ajaxHandlerAction.getPreSalesPerson() + "') ");
			}
			if (ajaxHandlerAction.getClientId() != null && !"".equals(ajaxHandlerAction.getClientId())) {
				queryStringBuffer.append("  AND tblRecRequirement .CustomerId =" + ajaxHandlerAction.getClientId());
			}

			queryStringBuffer
					.append(" GROUP BY tblRecRequirement.Id ORDER BY `tblRecRequirement`.`DatePosted` DESC Limit 200");

			// System.out.println("ajaxHandlerAction.getStatus()===="+ajaxHandlerAction.getStatus());
			// System.out.println("ajaxHandlerAction.getTitle()===" +
			// ajaxHandlerAction.getTitle());
			// System.out.println("queryStringBuffer.toString()-->" +
			// queryStringBuffer.toString());
			// System.out.println("query---"+queryStringBuffer.toString());
			httpServletRequest.getSession(false).setAttribute("REQ_SEARCH_QUERY", queryStringBuffer.toString());
			// System.out.println("REQ_SEARCH_QUERY --->"+REQ_SEARCH_QUERY);
			/*
			 *
			 * End of where
			 */

			// System.out.println("Search Query
			// ---"+queryStringBuffer.toString());

			connection = ConnectionProvider.getInstance().getConnection();
			statement = connection.createStatement();
			resultSet = statement.executeQuery(queryStringBuffer.toString());

			// System.err.println("Account Activities:"+queryString);
			while (resultSet.next()) {

				int resumeCount = resultSet.getInt("resumecount");
				int RequirementId = resultSet.getInt("RequirementId");

				int ConsultantId = resultSet.getInt("ConsultantId");

				String JobTitle = resultSet.getString("JobTitle");

				String ConsultantNAME = resultSet.getString("ConsultantNAME");

				String Location = resultSet.getString("Location");

				String status = resultSet.getString("status");

				String SubmittedDate = resultSet.getString("SubmittedDate");

				String AssignedDate = resultSet.getString("AssignedDate");
				String AccountName = resultSet.getString("AccountName");

				int noofresumes = resultSet.getInt("noofresumes");

				String Recruiter = "-";
				if (resultSet.getString("Recruiter") != null || resultSet.getString("Recruiter") != "") {
					Recruiter = resultSet.getString("Recruiter");
				}

				String SecondaryRecruiter = "-";
				if (resultSet.getString("SecondaryRecruiter") != null
						|| resultSet.getString("SecondaryRecruiter") != "") {
					SecondaryRecruiter = resultSet.getString("SecondaryRecruiter");
				}

				String PreSales = "-";
				if (resultSet.getString("PreSales") != null || resultSet.getString("PreSales") != "") {
					PreSales = resultSet.getString("PreSales");
				}

				String Skills = resultSet.getString("Skills");

				i++;
				/*
				 * createdBy=resultSet.getString("CreatedById"); count
				 * =resultSet.getInt("total");
				 */
				// totalStream=totalStream+i+"|"+createdDate+"|"+actType+"|"+description+"|"+comments+"|"+assignedToId+"|"+status+"|"+datedue+"|"+contactId+"|"+accountId+"|"+"^";
				// totalStream=totalStream+i+"|"+createdDate+"|"+actType+"|"+description+"|"+comments+"|"+assignedToId+"|"+status+"|"+datedue+"|"+"^";
				totalStream = totalStream + i + "|" + resumeCount + "|" + RequirementId + "|" + ConsultantId + "|"
						+ JobTitle + "|" + ConsultantNAME + "|" + Location + "|" + status + "|" + SubmittedDate + "|"
						+ AssignedDate + "|" + noofresumes + "|" + Recruiter + "|" + SecondaryRecruiter + "|" + PreSales
						+ "|" + PreSales + "|" + Skills + "|" + AccountName + "^";
				;
				// totalActivities=totalActivities+count;
			}
			stringBuffer.append(totalStream);
			stringBuffer.append("addto");

			stringBuffer.append(i);

		} catch (Exception sqe) {
			sqe.printStackTrace();
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (statement != null) {
					statement.close();
					statement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException sqle) {
			}
		}
		// System.err.println("response string is"+stringBuffer.toString());
		return stringBuffer.toString();
	}

	public String getStarResourceComments(int starId) throws ServiceLocatorException {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String Comments = "";
		queryString = "SELECT Comments FROM tblStarPerformerLines WHERE Id=" + starId;
		try {
			connection = ConnectionProvider.getInstance().getConnection();
			statement = connection.prepareStatement(queryString);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				if (resultSet.getString("Comments") != null && !"".equals(resultSet.getString("Comments"))) {
					Comments = resultSet.getString("Comments");
				} else {
					Comments = "-";
				}
			}

		} catch (SQLException ex) {
			ex.printStackTrace();
		} catch (ServiceLocatorException sle) {
			sle.printStackTrace();
		} finally {
			try {

				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (statement != null) {
					statement.close();
					statement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
		}
		return Comments;
	}

	public String getEmployeeCertificationDetails(String loginId, String startDate, String endDate)
			throws ServiceLocatorException {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String ReviewName = "";
		String ReviewDate = "";
		String AttachmentName = "";
		String response = "";
		int i = 0;
		queryString = "SELECT tblEmpReview.Id AS Id,ReviewName,ReviewDate,AttachmentName FROM tblEmpReview LEFT JOIN tblLkReviews ON tblEmpReview.ReviewTypeId=tblLkReviews.Id  WHERE tblLkReviews.Id=4 AND UserId='"
				+ loginId + "'";

		if (startDate != null && !"".equals(startDate)) {
			queryString = queryString + " AND DATE(tblEmpReview.ReviewDate)>=DATE('"
					+ DateUtility.getInstance().convertStringToMySQLDate(startDate) + "') ";
		}

		if (endDate != null && !"".equals(endDate)) {
			queryString = queryString + " AND DATE(tblEmpReview.ReviewDate)<=DATE('"
					+ DateUtility.getInstance().convertStringToMySQLDate(endDate) + "') ";
		}

		// System.out.println("queryString.."+queryString);
		try {
			connection = ConnectionProvider.getInstance().getConnection();
			statement = connection.prepareStatement(queryString);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				i++;
				if (resultSet.getString("ReviewName") != null && !"".equals(resultSet.getString("ReviewName"))) {
					ReviewName = resultSet.getString("ReviewName");
				} else {
					ReviewName = "-";
				}
				if (resultSet.getString("ReviewDate") != null && !"".equals(resultSet.getString("ReviewDate"))) {
					ReviewDate = resultSet.getString("ReviewDate");
				} else {
					ReviewDate = "-";
				}
				if (resultSet.getString("AttachmentName") != null
						&& !"".equals(resultSet.getString("AttachmentName"))) {
					AttachmentName = resultSet.getString("AttachmentName");
				} else {
					AttachmentName = "-";
				}

				response = response + resultSet.getString("Id") + "#^$" + i + "#^$" + ReviewName + "#^$"
						+ DateUtility.getInstance().convertToviewFormat(ReviewDate) + "#^$" + AttachmentName + "*@!";

			}

		} catch (SQLException ex) {
			ex.printStackTrace();
		} catch (ServiceLocatorException sle) {
			sle.printStackTrace();
		} finally {
			try {

				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (statement != null) {
					statement.close();
					statement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
		}
		return response;
	}

	public String getReviewDescription(int reviewId) throws ServiceLocatorException {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String Comments = "";
		queryString = " SELECT EmpComments FROM tblEmpReview WHERE Id=" + reviewId;
		try {
			connection = ConnectionProvider.getInstance().getConnection();
			statement = connection.prepareStatement(queryString);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				if (resultSet.getString("EmpComments") != null && !"".equals(resultSet.getString("EmpComments"))) {
					Comments = resultSet.getString("EmpComments");
				} else {
					Comments = "-";
				}
			}

		} catch (SQLException ex) {
			ex.printStackTrace();
		} catch (ServiceLocatorException sle) {
			sle.printStackTrace();
		} finally {
			try {

				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (statement != null) {
					statement.close();
					statement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
		}
		return Comments;
	}

	public String getPracticeStatusReport(String practiceId, int shadowFlag, String department, String orderBy)
			throws ServiceLocatorException {
		Connection connection = null;
		CallableStatement callableStatement = null;
		String responseString = "";
		String queryString = "";

		if ("All".equals(practiceId)) {
			queryString = "{call spGetAllPracticeStatusReports(?,?,?,?)}";
		} else {
			queryString = "{call spGetPracticeStatusReports(?,?,?,?)}";
		}
		try {
			connection = ConnectionProvider.getInstance().getConnection();
			callableStatement = connection.prepareCall(queryString);
			// callableStatement.setString(1,
			// DateUtility.getInstance().convertStringToMySQLDate(startDate));
			// callableStatement.setString(2,
			// DateUtility.getInstance().convertStringToMySQLDate(endDate));

			if ("All".equals(practiceId)) {
				callableStatement.setString(1, "%");
			} else {
				callableStatement.setString(1, practiceId);
			}
			callableStatement.setInt(2, shadowFlag);

			callableStatement.setString(3, department);
			// callableStatement.setString(4, orderBy);

			callableStatement.registerOutParameter(4, java.sql.Types.VARCHAR);

			// System.out.println(practiceId+"r--->
			// "+DateUtility.getInstance().convertStringToMySQLDate(startDate)+"--->"+startDate);
			callableStatement.executeQuery();
			responseString = callableStatement.getString(4);
			if (!"-1".equals(orderBy)) {
				responseString = DataSourceDataProvider.getInstance().getSortedPracticeReportResponse(responseString,
						orderBy);
			}

		} catch (SQLException se) {
			se.printStackTrace();

			throw new ServiceLocatorException(se);
		} finally {
			try {

				if (callableStatement != null) {
					callableStatement.close();
					callableStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException se) {
				se.printStackTrace();
				throw new ServiceLocatorException(se);
			}
		}
		return responseString;
	}

	public String getShadowsUtilizationReport(String accId) throws ServiceLocatorException {

		String response = "";
		// Connection connection = null;
		CallableStatement callableStatement = null;
		Connection connection = null;

		try {
			connection = ConnectionProvider.getInstance().getConnection();
			callableStatement = connection.prepareCall("{call spGetShadowsUtilizationReport(?,?)}");

			callableStatement.setString(1, accId);
			callableStatement.registerOutParameter(2, Types.VARCHAR);

			callableStatement.execute();

			response = callableStatement.getString(2);

		} catch (SQLException se) {
			se.printStackTrace();
		} finally {
			try {

				if (callableStatement != null) {
					callableStatement.close();
					callableStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		return response;
	}

	public String getShadowsUtilizationReportByProject(String accId, String projectId, String status, String type,
			String costModel) throws ServiceLocatorException {

		String response = "";
		// Connection connection = null;
		CallableStatement callableStatement = null;
		Connection connection = null;
		try {
			connection = ConnectionProvider.getInstance().getConnection();
			callableStatement = connection
					.prepareCall("{call spGetShadowsUtilizationReportBasedOnProject(?,?,?,?,?,?)}");

			callableStatement.setString(1, accId);
			callableStatement.setString(2, projectId);
			callableStatement.setString(3, status);
			callableStatement.setString(4, type);
			callableStatement.setString(5, costModel);
			callableStatement.registerOutParameter(6, Types.VARCHAR);

			callableStatement.execute();

			response = callableStatement.getString(6);
		} catch (SQLException se) {
			se.printStackTrace();
		} finally {
			try {

				if (callableStatement != null) {
					callableStatement.close();
					callableStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		return response;
	}

	public String getResourceTotal(String practiceId, String department, String state) throws ServiceLocatorException {
		Connection connection = null;
		CallableStatement callableStatement = null;
		String responseString = "";
		String queryString = "";

		queryString = "{call spResourceTotal(?,?,?,?)}";

		try {
			connection = ConnectionProvider.getInstance().getConnection();
			callableStatement = connection.prepareCall(queryString);

			if ("All".equals(practiceId)) {
				callableStatement.setString(1, "%");
			} else {
				callableStatement.setString(1, practiceId);
			}

			callableStatement.setString(2, department);

			if ("All".equals(state)) {
				callableStatement.setString(3, "%");
			} else {
				callableStatement.setString(3, state);
			}

			callableStatement.registerOutParameter(4, Types.VARCHAR);

			callableStatement.executeQuery();
			responseString = callableStatement.getString(4);
			// System.out.println("responseString---> "+responseString);
		} catch (SQLException se) {
			se.printStackTrace();

			throw new ServiceLocatorException(se);
		} finally {
			try {

				if (callableStatement != null) {
					callableStatement.close();
					callableStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException se) {
				se.printStackTrace();
				throw new ServiceLocatorException(se);
			}
		}
		return responseString;
	}

	public String getReviewListBasedOnActivityType(String activityType) throws ServiceLocatorException {
		StringBuffer reviewList = new StringBuffer();
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		int reviewId;
		String reviewName;
		if ("ConfCall Setup".equalsIgnoreCase(activityType)) {
			queryString = "SELECT Id,ReviewType FROM tblLkReviews WHERE  ReviewType IN('Conference Call Schedule','On-site Visit Schedule','No.Of Conference Meets Scheduled')";
		} else if ("Conference Call".equalsIgnoreCase(activityType)) {
			queryString = "SELECT Id,ReviewType FROM tblLkReviews WHERE  ReviewType IN('Conference Call Executed')";
		} else if ("RFQ".equalsIgnoreCase(activityType)) {
			queryString = "SELECT Id,ReviewType FROM tblLkReviews WHERE  ReviewType IN('Quote Procured','Quote Submitted','Quote Won')";
		} else if ("F2F Setup".equalsIgnoreCase(activityType)) {
			queryString = "SELECT Id,ReviewType FROM tblLkReviews WHERE  ReviewType IN('Customer Demo-F2F Scheduled','Customer Demo-Virtual Scheduled','Customer meet -Break Fast Scheduled','Customer meet-F2F Scheduled',"
					+ "'Customer meet-Lunch Scheduled','Customer meet-Dinner Scheduled','Customer visit Miracle Facility Scheduled')";
		} else if ("Customer Meet-Conference".equalsIgnoreCase(activityType)) {
			queryString = "SELECT Id,ReviewType FROM tblLkReviews WHERE  ReviewType IN('Customer Meet-Conference Scheduled','Customer Meet-Conference Executed')";
		}

		else {

			queryString = "SELECT Id,ReviewType FROM tblLkReviews WHERE  ReviewType IN('On-site Visit Executed')";

		}
		try {
			connection = ConnectionProvider.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(queryString);
			resultSet = preparedStatement.executeQuery();
			reviewList.append("<xml version=\"1.0\">");
			reviewList.append("<REVIEWS>");
			reviewList.append("<NAME reviewId=\"-1\">---Please Select---</NAME>");
			while (resultSet.next()) {
				reviewId = resultSet.getInt("Id");
				reviewName = resultSet.getString("ReviewType");

				reviewList.append("<NAME reviewId=\"" + reviewId + "\">");
				if (reviewName.contains("&")) {
					reviewName = reviewName.replace("&", "&amp;");
				}
				reviewList.append(reviewName);
				reviewList.append("</NAME>");

			}
			reviewList.append("</REVIEWS>");
			reviewList.append("</xml>");
			// System.out.println(" reviewList.toString()..."+
			// reviewList.toString());
		} catch (SQLException ex) {
			ex.printStackTrace();
		} catch (ServiceLocatorException sle) {
			sle.printStackTrace();
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
		}
		// System.out.println("Team List: "+projects.toString());
		return reviewList.toString();
	}

	public String getReviewSalesStatusReport(NewAjaxHandlerAction ajaxHandlerAction,
			HttpServletRequest httpServletRequest) throws ServiceLocatorException {

		Connection connection = null;
		ResultSet resultSet = null;
		String responseString = "";
		int updatedRows = 0;
		CallableStatement callableStatement = null;
		String myTeamMembers = "";
		Map teamMembers = new TreeMap();
		DataSourceDataProvider dataSourceDataProvider = null;

		try {

			if ("-1".equals(ajaxHandlerAction.getTeamMemberId())) {
				teamMembers = DataSourceDataProvider.getInstance()
						.getAllSalesTeamByCountry(ajaxHandlerAction.getCountry());
				if (teamMembers.size() > 0) {
					myTeamMembers = getKeys(teamMembers, ",");
				}
			} else {
				String teamMemberId = ajaxHandlerAction.getTeamMemberId();
				String empTitle = DataSourceDataProvider.getInstance().getEmpTitleByLoginId(teamMemberId);
				if ("BDM".equalsIgnoreCase(empTitle)) {
					int empId = DataSourceDataProvider.getInstance().getEmpIdByLoginId(teamMemberId);
					teamMembers = DataSourceDataProvider.getInstance().getBdmAssociateList(Integer.toString(empId));
					myTeamMembers = getKeys(teamMembers, ",");
					myTeamMembers = myTeamMembers + ",'" + ajaxHandlerAction.getTeamMemberId() + "'";
				} else {
					myTeamMembers = "'" + ajaxHandlerAction.getTeamMemberId() + "'";
				}
			}
			myTeamMembers = myTeamMembers.replaceAll("'", "");

			connection = ConnectionProvider.getInstance().getConnection();
			callableStatement = connection.prepareCall("{call spSalesReviewsStatusReport(?,?,?,?,?)}");

			callableStatement.setString(1, myTeamMembers);
			if (ajaxHandlerAction.getStartDate() != null && !"".equals(ajaxHandlerAction.getStartDate())) {
				callableStatement.setString(2,
						DateUtility.getInstance().convertStringToMySQLDate(ajaxHandlerAction.getStartDate()));
			} else {
				callableStatement.setString(2, "");
			}
			if (ajaxHandlerAction.getEndDate() != null && !"".equals(ajaxHandlerAction.getEndDate())) {
				callableStatement.setString(3,
						DateUtility.getInstance().convertStringToMySQLDate(ajaxHandlerAction.getEndDate()));
			} else {
				callableStatement.setString(3, "");
			}

			if (ajaxHandlerAction.getCountry() != null && !"".equals(ajaxHandlerAction.getCountry())) {
				callableStatement.setString(4, ajaxHandlerAction.getCountry());
			}

			callableStatement.registerOutParameter(5, Types.VARCHAR);
			updatedRows = callableStatement.executeUpdate();
			responseString = callableStatement.getString(5);

		} catch (Exception sqe) {
			sqe.printStackTrace();
		} finally {
			try {
				if (callableStatement != null) {
					callableStatement.close();
					callableStatement = null;
				}

				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException sqle) {
			}
		}
		// System.out.println("responseString-->"+responseString);
		return responseString;
	}

	public String getActivitySummeryByAccount(NewAjaxHandlerAction ajaxHandlerAction,
			HttpServletRequest httpServletRequest) throws ServiceLocatorException {

		Connection connection = null;
		ResultSet resultSet = null;
		String responseString = "";
		int updatedRows = 0;
		CallableStatement callableStatement = null;
		String myTeamMembers = "";
		Map teamMembers = new TreeMap();
		DataSourceDataProvider dataSourceDataProvider = null;

		try {

			String teamMemberId = ajaxHandlerAction.getTeamMemberId();

			if ("false".equalsIgnoreCase(ajaxHandlerAction.getIncludeTeamFlag())) {
				myTeamMembers = "'" + ajaxHandlerAction.getTeamMemberId() + "'";
			} else {
				String empTitle = DataSourceDataProvider.getInstance().getEmpTitleByLoginId(teamMemberId);
				if ("BDM".equalsIgnoreCase(empTitle)) {
					int empId = DataSourceDataProvider.getInstance().getEmpIdByLoginId(teamMemberId);
					teamMembers = DataSourceDataProvider.getInstance().getBdmAssociateList(Integer.toString(empId));
					myTeamMembers = getKeys(teamMembers, ",");
				} else {
					String isTeamLead = DataSourceDataProvider.getInstance().checkIsTeamLead(teamMemberId);
					if ("YES".equals(isTeamLead)) {
						teamMembers = DataSourceDataProvider.getInstance().getMyTeamList(teamMemberId);
						myTeamMembers = getKeys(teamMembers, ",");
					}
				}

				// if(empNamesList.equalsIgnoreCase(""))
				if (!"".equals(myTeamMembers)) {
					myTeamMembers = myTeamMembers + ",'" + ajaxHandlerAction.getTeamMemberId() + "'";
				} else {
					myTeamMembers = "'" + ajaxHandlerAction.getTeamMemberId() + "'";
				}

			}
			myTeamMembers = myTeamMembers.replaceAll("'", "");
			// System.out.println("myTeamMembers..."+myTeamMembers+"ajaxHandlerAction.getStartDate().."+ajaxHandlerAction.getStartDate()+"ajaxHandlerAction.getEndDate().."+ajaxHandlerAction.getEndDate());

			connection = ConnectionProvider.getInstance().getConnection();
			callableStatement = connection.prepareCall("{call spActivitySummeryByAccount(?,?,?,?)}");

			callableStatement.setString(1, myTeamMembers);
			if (ajaxHandlerAction.getStartDate() != null && !"".equals(ajaxHandlerAction.getStartDate())) {
				callableStatement.setString(2,
						DateUtility.getInstance().convertStringToMySQLDate(ajaxHandlerAction.getStartDate()));
			} else {
				callableStatement.setString(2, "");
			}
			if (ajaxHandlerAction.getEndDate() != null && !"".equals(ajaxHandlerAction.getEndDate())) {
				callableStatement.setString(3,
						DateUtility.getInstance().convertStringToMySQLDate(ajaxHandlerAction.getEndDate()));
			} else {
				callableStatement.setString(3, "");
			}
			callableStatement.registerOutParameter(4, Types.VARCHAR);
			updatedRows = callableStatement.executeUpdate();
			responseString = callableStatement.getString(4);
			// System.out.println("responseString..."+responseString);

		} catch (Exception sqe) {
			sqe.printStackTrace();
		} finally {
			try {
				if (callableStatement != null) {
					callableStatement.close();
					callableStatement = null;
				}

				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException sqle) {
			}
		}
		// System.out.println("responseString-->"+responseString);
		return responseString;
	}

	// public String accountSummaryOfBdmReport(NewAjaxHandlerAction
	// ajaxHandlerAction,
	// HttpServletRequest httpServletRequest) throws ServiceLocatorException {
	//
	// Connection connection = null;
	// ResultSet resultSet = null;
	// String responseString = "";
	// int updatedRows = 0;
	// CallableStatement callableStatement = null;
	// String myTeamMembers = "";
	// Map teamMembers = new TreeMap();
	// DataSourceDataProvider dataSourceDataProvider = null;
	//
	// try {
	//
	// String teamMemberId = ajaxHandlerAction.getTeamMemberId();
	// int includeTeamFlag = ajaxHandlerAction.getIncludeTeam();
	// String loginId =
	// DataSourceDataProvider.getInstance().getLoginIdByEmpId(Integer.parseInt(teamMemberId));
	// teamMembers =
	// DataSourceDataProvider.getInstance().getBdmAssociateList(teamMemberId);
	// if (includeTeamFlag == 1) {
	// // System.out.println("if case ==== "+includeTeamFlag);
	// teamMembers.put(loginId, httpServletRequest.getSession(false)
	// .getAttribute(ApplicationConstants.SESSION_USER_NAME).toString());
	// }
	// myTeamMembers = getKeys(teamMembers, ",");
	//
	// myTeamMembers = myTeamMembers.replaceAll("'", "");
	//
	// connection = ConnectionProvider.getInstance().getConnection();
	// callableStatement = connection.prepareCall("{call
	// spAccountSummaryOfBdmReport(?,?,?,?,?,?)}");
	//
	// callableStatement.setString(1, myTeamMembers);
	// callableStatement.setString(2,
	// DateUtility.getInstance().convertStringToMySQLDate(ajaxHandlerAction.getStartDate()));
	// callableStatement.setString(3,
	// DateUtility.getInstance().convertStringToMySQLDate(ajaxHandlerAction.getEndDate()));
	// callableStatement.setString(4, ajaxHandlerAction.getReportBy());
	// callableStatement.setInt(5, includeTeamFlag);
	// callableStatement.registerOutParameter(6, Types.VARCHAR);
	// updatedRows = callableStatement.executeUpdate();
	// responseString = callableStatement.getString(6);
	// } catch (Exception sqe) {
	// sqe.printStackTrace();
	// } finally {
	// try {
	// if (callableStatement != null) {
	// callableStatement.close();
	// callableStatement = null;
	// }
	//
	// if (connection != null) {
	// connection.close();
	// connection = null;
	// }
	// } catch (SQLException sqle) {
	// }
	// }
	// // System.out.println("responseString-->"+responseString);
	// return responseString;
	// }

	public String accountSummaryOfBdmReport(NewAjaxHandlerAction ajaxHandlerAction,
			HttpServletRequest httpServletRequest) throws ServiceLocatorException {

		Connection connection = null;
		ResultSet resultSet = null;
		String responseString = "";
		int updatedRows = 0;
		CallableStatement callableStatement = null;
		String myTeamMembers = "";
		Map teamMembers = new TreeMap();
		DataSourceDataProvider dataSourceDataProvider = null;
		Map teamMembersInfo = new HashMap();
		try {
			String loginId = httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID)
					.toString();
			String dbUserName = (String) httpServletRequest.getSession(false)
					.getAttribute(ApplicationConstants.SESSION_USER_NAME);
			String empId = httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_EMP_ID)
					.toString();
			String teamMemberId = ajaxHandlerAction.getTeamMemberId();
			int includeTeamFlag = ajaxHandlerAction.getIncludeTeam();
			if ("mlokam".equals(loginId)) {
				if ("-1".equals(teamMemberId)) {
					teamMembersInfo = DataSourceDataProvider.getInstance().getAllBDMsLoginId();
					myTeamMembers = getKeys(teamMembersInfo, ",");
					myTeamMembers = myTeamMembers.replaceAll("'", "");
					if (includeTeamFlag == 1) {
						teamMembersInfo.put(loginId, dbUserName);
						myTeamMembers = getKeys(teamMembersInfo, ",");
						myTeamMembers = myTeamMembers.replaceAll("'", "");
					}
				} else {
					int empIdByLoginId = DataSourceDataProvider.getInstance().getEmpIdByLoginId(teamMemberId);
					teamMembersInfo = DataSourceDataProvider.getInstance()
							.getBdmAssociateList(Integer.toString(empIdByLoginId));
					myTeamMembers = getKeys(teamMembersInfo, ",");
					myTeamMembers = myTeamMembers.replaceAll("'", "");
					if (includeTeamFlag == 1) {
						teamMembersInfo.put(teamMemberId, teamMemberId);
						myTeamMembers = getKeys(teamMembersInfo, ",");
						myTeamMembers = myTeamMembers.replaceAll("'", "");
					}
				}
			} else {
				teamMembers = DataSourceDataProvider.getInstance().getBdmAssociateList(teamMemberId);
				teamMembersInfo = DataSourceDataProvider.getInstance().getBdmAssociateList(empId);
				myTeamMembers = getKeys(teamMembersInfo, ",");
				myTeamMembers = myTeamMembers.replaceAll("'", "");
				if (includeTeamFlag == 1) {
					teamMembersInfo.put(loginId, dbUserName);
					myTeamMembers = getKeys(teamMembersInfo, ",");
					myTeamMembers = myTeamMembers.replaceAll("'", "");
				}
			}
			connection = ConnectionProvider.getInstance().getConnection();
			callableStatement = connection.prepareCall("{call spAccountSummaryOfBdmReport(?,?,?,?,?,?)}");

			callableStatement.setString(1, myTeamMembers);

			callableStatement.setString(2,
					DateUtility.getInstance().convertStringToMySQLDate(ajaxHandlerAction.getStartDate()));
			callableStatement.setString(3,
					DateUtility.getInstance().convertStringToMySQLDate(ajaxHandlerAction.getEndDate()));
			callableStatement.setString(4, ajaxHandlerAction.getReportBy());
			callableStatement.setInt(5, includeTeamFlag);
			callableStatement.registerOutParameter(6, Types.VARCHAR);
			updatedRows = callableStatement.executeUpdate();
			responseString = callableStatement.getString(6);
		} catch (Exception sqe) {
			sqe.printStackTrace();
		} finally {
			try {
				if (callableStatement != null) {
					callableStatement.close();
					callableStatement = null;
				}

				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException sqle) {
			}
		}
		// System.out.println("responseString-->"+responseString);
		return responseString;
	}

	public String requirementSummaryOfBdmReport(NewAjaxHandlerAction ajaxHandlerAction,
			HttpServletRequest httpServletRequest) throws ServiceLocatorException {

		Connection connection = null;
		ResultSet resultSet = null;
		String responseString = "";
		int updatedRows = 0;
		CallableStatement callableStatement = null;
		String myTeamMembers = "";
		Map teamMembers = new TreeMap();
		DataSourceDataProvider dataSourceDataProvider = null;

		try {

			String teamMemberId = ajaxHandlerAction.getTeamMemberId();
			int includeTeamFlag = ajaxHandlerAction.getIncludeTeam();
			String loginId = DataSourceDataProvider.getInstance().getLoginIdByEmpId(Integer.parseInt(teamMemberId));
			teamMembers = DataSourceDataProvider.getInstance().getBdmAssociateList(teamMemberId);
			if (includeTeamFlag == 1) {

				teamMembers.put(loginId, httpServletRequest.getSession(false)
						.getAttribute(ApplicationConstants.SESSION_USER_NAME).toString());
			}
			myTeamMembers = getKeys(teamMembers, ",");

			myTeamMembers = myTeamMembers.replaceAll("'", "");

			connection = ConnectionProvider.getInstance().getConnection();
			callableStatement = connection.prepareCall("{call spRequirementSummaryByBDM(?,?,?,?,?,?)}");

			callableStatement.setString(1, myTeamMembers);
			callableStatement.setString(2,
					DateUtility.getInstance().convertStringToMySQLDate(ajaxHandlerAction.getStartDate()));
			callableStatement.setString(3,
					DateUtility.getInstance().convertStringToMySQLDate(ajaxHandlerAction.getEndDate()));
			callableStatement.setString(4, ajaxHandlerAction.getReportBy());
			callableStatement.setInt(5, includeTeamFlag);
			callableStatement.registerOutParameter(6, Types.VARCHAR);
			updatedRows = callableStatement.executeUpdate();
			responseString = callableStatement.getString(6);
		} catch (Exception sqe) {
			sqe.printStackTrace();
		} finally {
			try {
				if (callableStatement != null) {
					callableStatement.close();
					callableStatement = null;
				}

				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException sqle) {
			}
		}
		// System.out.println("responseString-->"+responseString);
		return responseString;
	}

	public String getOffshoreStrengthDetails(NewAjaxHandlerAction ajaxHandlerAction,
			HttpServletRequest httpServletRequest) throws ServiceLocatorException {

		Connection connection = null;
		ResultSet resultSet = null;
		String responseString = "";
		int updatedRows = 0;
		CallableStatement callableStatement = null;
		String myTeamMembers = "";
		Map teamMembers = new TreeMap();
		DataSourceDataProvider dataSourceDataProvider = null;

		try {

			connection = ConnectionProvider.getInstance().getConnection();
			callableStatement = connection.prepareCall("{call spGetOffshoreStrength(?,?,?)}");

			callableStatement.setString(1, ajaxHandlerAction.getYear());
			callableStatement.setString(2, ajaxHandlerAction.getMonth());

			callableStatement.registerOutParameter(3, Types.VARCHAR);
			updatedRows = callableStatement.executeUpdate();
			responseString = callableStatement.getString(3);
		} catch (Exception sqe) {
			sqe.printStackTrace();
		} finally {
			try {
				if (callableStatement != null) {
					callableStatement.close();
					callableStatement = null;
				}

				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException sqle) {
			}
		}
		// System.out.println("responseString-->"+responseString);
		return responseString;
	}

	public String getCustomerProjectRollInRollOffDetails(NewAjaxHandlerAction ajaxHandlerAction,
			HttpServletRequest httpServletRequest) throws ServiceLocatorException {

		Connection connection = null;
		ResultSet resultSet = null;
		String responseString = "";
		int updatedRows = 0;
		CallableStatement callableStatement = null;
		String myTeamMembers = "";
		Map teamMembers = new TreeMap();
		DataSourceDataProvider dataSourceDataProvider = null;

		try {

			connection = ConnectionProvider.getInstance().getConnection();
			callableStatement = connection.prepareCall("{call spGetCustomerProjectRollInRollOffDetails(?,?,?,?)}");

			callableStatement.setString(1,
					DateUtility.getInstance().convertStringToMySQLDate(ajaxHandlerAction.getStartDate()));
			callableStatement.setString(2,
					DateUtility.getInstance().convertStringToMySQLDate(ajaxHandlerAction.getEndDate()));
			callableStatement.setString(3, ajaxHandlerAction.getType());

			callableStatement.registerOutParameter(4, Types.VARCHAR);
			updatedRows = callableStatement.executeUpdate();
			responseString = callableStatement.getString(4);
		} catch (Exception sqe) {
			sqe.printStackTrace();
		} finally {
			try {
				if (callableStatement != null) {
					callableStatement.close();
					callableStatement = null;
				}

				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException sqle) {
			}
		}
		// System.out.println("responseString-->"+responseString);
		return responseString;
	}

	public String activitySummeryDetailsByAccount(NewAjaxHandlerAction ajaxHandlerAction,
			HttpServletRequest httpServletRequest) throws ServiceLocatorException {

		Connection connection = null;
		ResultSet resultSet = null;
		String responseString = "";
		int updatedRows = 0;
		CallableStatement callableStatement = null;
		String myTeamMembers = "";
		Map teamMembers = new TreeMap();
		DataSourceDataProvider dataSourceDataProvider = null;

		try {

			String teamMemberId = ajaxHandlerAction.getTeamMemberId();

			if ("false".equalsIgnoreCase(ajaxHandlerAction.getIncludeTeamFlag())) {
				myTeamMembers = "'" + ajaxHandlerAction.getTeamMemberId() + "'";
			} else {
				String empTitle = DataSourceDataProvider.getInstance().getEmpTitleByLoginId(teamMemberId);
				if ("BDM".equalsIgnoreCase(empTitle)) {
					int empId = DataSourceDataProvider.getInstance().getEmpIdByLoginId(teamMemberId);
					teamMembers = DataSourceDataProvider.getInstance().getBdmAssociateList(Integer.toString(empId));
					myTeamMembers = getKeys(teamMembers, ",");
				} else {
					String isTeamLead = DataSourceDataProvider.getInstance().checkIsTeamLead(teamMemberId);
					if ("YES".equals(isTeamLead)) {
						teamMembers = DataSourceDataProvider.getInstance().getMyTeamList(teamMemberId);
						myTeamMembers = getKeys(teamMembers, ",");
					}
				}

				// if(empNamesList.equalsIgnoreCase(""))
				if (!"".equals(myTeamMembers)) {
					myTeamMembers = myTeamMembers + ",'" + ajaxHandlerAction.getTeamMemberId() + "'";
				} else {
					myTeamMembers = "'" + ajaxHandlerAction.getTeamMemberId() + "'";
				}

			}
			myTeamMembers = myTeamMembers.replaceAll("'", "");
			// System.out.println("myTeamMembers..."+myTeamMembers+"ajaxHandlerAction.getStartDate().."+ajaxHandlerAction.getStartDate()+"ajaxHandlerAction.getEndDate().."+ajaxHandlerAction.getEndDate());

			connection = ConnectionProvider.getInstance().getConnection();
			callableStatement = connection.prepareCall("{call spActivitySummeryDetailsByAccount(?,?,?,?,?,?)}");

			callableStatement.setString(1, myTeamMembers);
			if (ajaxHandlerAction.getStartDate() != null && !"".equals(ajaxHandlerAction.getStartDate())) {
				callableStatement.setString(2,
						DateUtility.getInstance().convertStringToMySQLDate(ajaxHandlerAction.getStartDate()));
			} else {
				callableStatement.setString(2, "");
			}
			if (ajaxHandlerAction.getEndDate() != null && !"".equals(ajaxHandlerAction.getEndDate())) {
				callableStatement.setString(3,
						DateUtility.getInstance().convertStringToMySQLDate(ajaxHandlerAction.getEndDate()));
			} else {
				callableStatement.setString(3, "");
			}
			callableStatement.setInt(4, ajaxHandlerAction.getAccountId());
			callableStatement.setString(5, ajaxHandlerAction.getFlag());
			callableStatement.registerOutParameter(6, Types.VARCHAR);
			updatedRows = callableStatement.executeUpdate();
			responseString = callableStatement.getString(6);
			// System.out.println("responseString..."+responseString);

		} catch (Exception sqe) {
			sqe.printStackTrace();
		} finally {
			try {
				if (callableStatement != null) {
					callableStatement.close();
					callableStatement = null;
				}

				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException sqle) {
			}
		}
		// System.out.println("responseString-->"+responseString);
		return responseString;
	}

	public String offshoreDeliveryStrength(NewAjaxHandlerAction ajaxHandlerAction,
			HttpServletRequest httpServletRequest) throws ServiceLocatorException {

		Connection connection = null;
		ResultSet resultSet = null;
		String responseString = "";
		int updatedRows = 0;
		CallableStatement callableStatement = null;

		try {

			connection = ConnectionProvider.getInstance().getConnection();
			callableStatement = connection.prepareCall("{call spOffshoreDeliveryStrenght(?,?,?)}");

			callableStatement.setString(1, ajaxHandlerAction.getPracticeId());
			callableStatement.setString(2, ajaxHandlerAction.getSubPractice());

			callableStatement.registerOutParameter(3, Types.VARCHAR);
			updatedRows = callableStatement.executeUpdate();
			responseString = callableStatement.getString(3);
		} catch (Exception sqe) {
			sqe.printStackTrace();
		} finally {
			try {
				if (callableStatement != null) {
					callableStatement.close();
					callableStatement = null;
				}

				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException sqle) {
			}
		}
		// System.out.println("responseString-->"+responseString);
		return responseString;
	}

	public String getEmployeeDetailsByLoginId(String loginId) throws ServiceLocatorException {

		String response = "";
		// Connection connection = null;

		try {
			response = DataSourceDataProvider.getInstance().getemployeeDetailsByLoginId(loginId);
			System.out.println("response--" + response);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}

	public String doGetEmployeeNamesForMcon(String query) throws ServiceLocatorException {
		boolean isGetting = false;
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		StringBuffer sb = new StringBuffer();
		try {
			connection = ConnectionProvider.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(query);
			resultSet = preparedStatement.executeQuery();

			int count = 0;
			sb.append("<xml version=\"1.0\">");
			sb.append("<EMPLOYEES>");
			while (resultSet.next()) {
				sb.append("<EMPLOYEE><VALID>true</VALID>");

				if (resultSet.getString(1) == null || resultSet.getString(1).equals("")) {
					sb.append("<NAME>NoRecord</NAME>");
				} else {
					String title = resultSet.getString(1);
					if (title.contains("&")) {
						title = title.replace("&", "&amp;");
					}
					sb.append("<NAME>" + title + "</NAME>");
				}
				// sb.append("<NAME>" +resultSet.getString(1) + "</NAME>");
				sb.append("<EMPLOGINID>" + resultSet.getString(2) + "</EMPLOGINID>");
				sb.append("</EMPLOYEE>");
				isGetting = true;
				count++;
			}

			if (!isGetting) {
				// sb.append("<EMPLOYEES>" + sb.toString() + "</EMPLOYEES>");
				// } else {
				isGetting = false;
				// nothing to show
				// response.setStatus(HttpServletResponse.SC_NO_CONTENT);
				sb.append("<EMPLOYEE><VALID>false</VALID></EMPLOYEE>");
			}
			sb.append("</EMPLOYEES>");
			sb.append("</xml>");
		} catch (SQLException sqle) {
			throw new ServiceLocatorException(sqle);
		} finally {
			try {
				if (resultSet != null) {

					resultSet.close();
					resultSet = null;
				}
				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}

				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException sql) {
				// System.err.print("Error :"+sql);
			}

		}
		// System.out.println(sb.toString());
		return sb.toString();
	}

	public String getLinkedInProfile(int contactId) throws ServiceLocatorException {

		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		String queryString = null;

		String response = "";
		try {
			connection = ConnectionProvider.getInstance().getConnection();
			queryString = "SELECT ProfileLink FROM tblCrmContact WHERE Id=?";
			preparedStatement = connection.prepareStatement(queryString);
			preparedStatement.setInt(1, contactId);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {

				response = resultSet.getString("ProfileLink");
			}
		} catch (SQLException sql) {
			throw new ServiceLocatorException(sql);
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}

				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException ex) {
				throw new ServiceLocatorException(ex);
			}
		}
		return response;
	}

	public String getActivityComments(int id, String flag) throws ServiceLocatorException {

		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		String queryString = null;

		String response = "";
		try {
			connection = ConnectionProvider.getInstance().getConnection();

			if (flag.equals("Leads")) {
				queryString = "SELECT tblCrmLeads.Description AS Comments FROM tblCrmLeads WHERE Id=?";
			} else if (flag.equals("Requirements")) {
				queryString = "SELECT Skills  AS Comments  FROM tblRecRequirement WHERE Id=?";
			} else {
				queryString = "SELECT tblCrmActivity.Comments  AS Comments FROM tblCrmActivity WHERE Id=?";
			}
			preparedStatement = connection.prepareStatement(queryString);
			preparedStatement.setInt(1, id);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {

				response = resultSet.getString("Comments");
			}
		} catch (SQLException sql) {
			throw new ServiceLocatorException(sql);
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}

				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException ex) {
				throw new ServiceLocatorException(ex);
			}
		}
		return response;
	}
	/* E certification changes */

	public String setFlagStatus(NewAjaxHandlerAction newAjaxHandlerAction, String key,
			HttpServletRequest httpServletRequest) throws ServiceLocatorException {
		// String qsTitle = "";
		/*
		 * System.out.println("in impl"+loginId);
		 * System.out.println("in impl"+month);
		 * System.out.println("in impl"+year);
		 */
		PreparedStatement prepareStatement = null;
		ResultSet resultSet = null;
		String query = "";
		Connection connection = null;
		String responseString = "";
		JSONObject jObject = null;
		JSONObject jsonResult = null;
		int ExamKeyId = 0;
		int qId = 0;
		QuestionsVTO questionVTO = null;
		Map questionMap = new HashMap();
		JSONArray jsonArray = new JSONArray();
		try {

			// System.out.println("in try impl");
			connection = ConnectionProvider.getInstance().getConnection();
			query = "SELECT ExamKeyId FROM tblEcertValidatorKeys WHERE VKey=?";
			prepareStatement = connection.prepareStatement(query);
			prepareStatement.setString(1, key);
			resultSet = prepareStatement.executeQuery();
			while (resultSet.next()) {
				ExamKeyId = resultSet.getInt(1);
			}

			Map questionVtoMap = (Map) httpServletRequest.getSession(false)
					.getAttribute(ApplicationConstants.ECERT_QUESTIONS_MAP);
			jsonResult = new JSONObject();
			for (int i = 1; i <= questionVtoMap.size(); i++) {

				questionVTO = (QuestionsVTO) questionVtoMap.get(i);
				qId = questionVTO.getId();
				questionMap.put(i, qId);
				query = "select EmpAns from tblEcertSummary where QuestionId=? AND ExamKeyId=?";
				prepareStatement = connection.prepareStatement(query);
				prepareStatement.setInt(1, qId);
				prepareStatement.setInt(2, ExamKeyId);
				resultSet = prepareStatement.executeQuery();
				while (resultSet.next()) {
					// System.out.println("in
					// while"+resultSet.getInt("EmpAns"));
					jsonResult.put(i + "", resultSet.getInt("EmpAns"));
					// jsonArray.put(jsonResult);
				}
			}

			responseString = jsonResult.toString() + "@$" + questionVtoMap.size();
			// System.out.println("responseString"+responseString);

			// response = response + resultSet.getString("NAME") + "#^$" +
			// resultSet.getString("ProjectName") + "#^$" +
			// resultSet.getString("StartDate") + "#^$" +
			// resultSet.getString("EndDate") + "#^$" +
			// resultSet.getString("EmpProjStatus") + "#^$" +
			// resultSet.getString("Billable") + "#^$" +
			// resultSet.getString("Utilization") + "#^$" +
			// resultSet.getString("ProjectType") + "*@!";

			// }
			// response=response+"addTo"+totalUtilization;
		} catch (Exception sqe) {
			sqe.printStackTrace();
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (prepareStatement != null) {
					prepareStatement.close();
					prepareStatement = null;
				}

				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException sqle) {
				sqle.printStackTrace();
			}
		}

		return responseString;
	}

	public String getRemainingTime(NewAjaxHandlerAction newAjaxHandlerAction, String key)
			throws ServiceLocatorException {
		// String qsTitle = "";
		/*
		 * System.out.println("in impl"+loginId);
		 * System.out.println("in impl"+month);
		 * System.out.println("in impl"+year);
		 */
		PreparedStatement prepareStatement = null;
		ResultSet resultSet = null;
		String query = "";
		Connection connection = null;
		int totalUtilization = 0;
		String responseString = "";
		JSONObject jObject = null;
		JSONObject jsonResult = null;
		double resultSum = 0.0;

		double breakFastPrice;
		double lunchPrice;
		double dinnerPrice;
		try {

			// System.out.println("in try impl");
			connection = ConnectionProvider.getInstance().getConnection();
			query = "SELECT Duration FROM tblEcertValidatorKeys WHERE VKey=?";
			prepareStatement = connection.prepareStatement(query);
			prepareStatement.setString(1, key);

			resultSet = prepareStatement.executeQuery();
			while (resultSet.next()) {

				jsonResult = new JSONObject();
				jsonResult.put("remainingTime", resultSet.getInt("Duration"));

			}

			responseString = jsonResult.toString();
			// System.out.println("response from impl"+responseString);

			// response = response + resultSet.getString("NAME") + "#^$" +
			// resultSet.getString("ProjectName") + "#^$" +
			// resultSet.getString("StartDate") + "#^$" +
			// resultSet.getString("EndDate") + "#^$" +
			// resultSet.getString("EmpProjStatus") + "#^$" +
			// resultSet.getString("Billable") + "#^$" +
			// resultSet.getString("Utilization") + "#^$" +
			// resultSet.getString("ProjectType") + "*@!";

			// }
			// response=response+"addTo"+totalUtilization;
		} catch (Exception sqe) {
			sqe.printStackTrace();
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (prepareStatement != null) {
					prepareStatement.close();
					prepareStatement = null;
				}

				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException sqle) {
				sqle.printStackTrace();
			}
		}

		return responseString;
	}

	public String getEcertCount(NewAjaxHandlerAction newAjaxHandlerAction, String userId)
			throws ServiceLocatorException {
		PreparedStatement prepareStatement = null;
		ResultSet resultSet = null;
		String query = "";
		Connection connection = null;
		int totalUtilization = 0;
		String responseString = "";
		JSONObject jObject = null;
		JSONObject jsonResult = null;
		int ExamKeyId = 0;
		try {
			// System.out.println("userId in remaining time"+userId);
			// System.out.println("in try impl");
			connection = ConnectionProvider.getInstance().getConnection();
			query = "SELECT ExamKeyId FROM tblEcertValidatorKeys WHERE VKey=?";
			prepareStatement = connection.prepareStatement(query);
			prepareStatement.setString(1, userId);
			resultSet = prepareStatement.executeQuery();
			while (resultSet.next()) {
				ExamKeyId = resultSet.getInt(1);
			}

			query = "SELECT count(*) FROM tblEcertSummary WHERE ExamKeyId=?";
			prepareStatement = connection.prepareStatement(query);

			prepareStatement.setInt(1, ExamKeyId);

			resultSet = prepareStatement.executeQuery();
			while (resultSet.next()) {

				jsonResult = new JSONObject();
				jsonResult.put("count", resultSet.getInt(1));

			}

			responseString = jsonResult.toString();
			// System.out.println("response from impl"+responseString);

		} catch (Exception sqe) {
			sqe.printStackTrace();
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (prepareStatement != null) {
					prepareStatement.close();
					prepareStatement = null;
				}

				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException sqle) {
				sqle.printStackTrace();
			}
		}

		return responseString;
	}

	public String getCreRemainingTime(NewAjaxHandlerAction newAjaxHandlerAction, String userId)
			throws ServiceLocatorException {
		PreparedStatement prepareStatement = null;
		ResultSet resultSet = null;
		String query = "";
		Connection connection = null;
		int totalUtilization = 0;
		String responseString = "";
		JSONObject jObject = null;
		JSONObject jsonResult = null;
		try {

			// System.out.println("userId in remaining time"+userId);
			// System.out.println("in try impl");
			connection = ConnectionProvider.getInstance().getConnection();
			query = "SELECT Duration FROM tblCreConsultentDetails WHERE ConsultentId=?";
			prepareStatement = connection.prepareStatement(query);

			prepareStatement.setString(1, userId);

			resultSet = prepareStatement.executeQuery();
			while (resultSet.next()) {

				jsonResult = new JSONObject();
				jsonResult.put("remainingTime", resultSet.getInt("Duration"));

			}

			responseString = jsonResult.toString();
			// System.out.println("response from impl"+responseString);

		} catch (Exception sqe) {
			sqe.printStackTrace();
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (prepareStatement != null) {
					prepareStatement.close();
					prepareStatement = null;
				}

				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException sqle) {
				sqle.printStackTrace();
			}
		}

		return responseString;
	}

	public String setCreFlagStatus(NewAjaxHandlerAction newAjaxHandlerAction, String userId,
			HttpServletRequest httpServletRequest) throws ServiceLocatorException {
		PreparedStatement prepareStatement = null;
		ResultSet resultSet = null;
		String query = "";
		Connection connection = null;
		String responseString = "";
		JSONObject jObject = null;
		JSONObject jsonResult = null;
		int ExamKeyId = 0;
		int qId = 0;
		QuestionsVTO questionVTO = null;
		Map questionMap = new HashMap();
		JSONArray jsonArray = new JSONArray();
		try {

			// System.out.println("in try impl");
			connection = ConnectionProvider.getInstance().getConnection();
			query = "SELECT ExamKeyId FROM tblCreConsultentDetails WHERE ConsultentId=?";
			prepareStatement = connection.prepareStatement(query);
			prepareStatement.setString(1, userId);
			resultSet = prepareStatement.executeQuery();
			while (resultSet.next()) {
				ExamKeyId = resultSet.getInt(1);
			}

			Map questionVtoMap = (Map) httpServletRequest.getSession(false)
					.getAttribute(ApplicationConstants.ECERT_QUESTIONS_MAP);
			jsonResult = new JSONObject();
			for (int i = 1; i <= questionVtoMap.size(); i++) {

				questionVTO = (QuestionsVTO) questionVtoMap.get(i);
				qId = questionVTO.getId();
				questionMap.put(i, qId);
				query = "select EmpAns from tblEcertSummary where QuestionId=? AND ExamKeyId=?";
				prepareStatement = connection.prepareStatement(query);
				prepareStatement.setInt(1, qId);
				prepareStatement.setInt(2, ExamKeyId);
				resultSet = prepareStatement.executeQuery();
				while (resultSet.next()) {
					// System.out.println("in
					// while"+resultSet.getInt("EmpAns"));
					jsonResult.put(i + "", resultSet.getInt("EmpAns"));
					// jsonArray.put(jsonResult);
				}
			}

			int size = questionVtoMap.size();

			responseString = jsonResult.toString() + "@$" + size;
			// System.out.println("responseString"+responseString);
			// System.out.println("responseString"+responseString);

		} catch (Exception sqe) {
			sqe.printStackTrace();
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (prepareStatement != null) {
					prepareStatement.close();
					prepareStatement = null;
				}

				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException sqle) {
				sqle.printStackTrace();
			}
		}

		return responseString;
	}

	public String getCreCount(NewAjaxHandlerAction newAjaxHandlerAction, String userId) throws ServiceLocatorException {
		PreparedStatement prepareStatement = null;
		ResultSet resultSet = null;
		String query = "";
		Connection connection = null;
		int totalUtilization = 0;
		String responseString = "";
		JSONObject jObject = null;
		JSONObject jsonResult = null;
		int ExamKeyId = 0;
		try {

			// System.out.println("userId in remaining time"+userId);
			// System.out.println("in try impl");
			connection = ConnectionProvider.getInstance().getConnection();
			query = "SELECT ExamKeyId FROM tblCreConsultentDetails WHERE ConsultentId=?";
			prepareStatement = connection.prepareStatement(query);
			prepareStatement.setString(1, userId);
			resultSet = prepareStatement.executeQuery();
			while (resultSet.next()) {
				ExamKeyId = resultSet.getInt(1);
			}

			query = "SELECT count(*) FROM tblEcertSummary WHERE ExamKeyId=?";
			prepareStatement = connection.prepareStatement(query);

			prepareStatement.setInt(1, ExamKeyId);

			resultSet = prepareStatement.executeQuery();
			while (resultSet.next()) {

				jsonResult = new JSONObject();
				jsonResult.put("count", resultSet.getInt(1));

			}

			responseString = jsonResult.toString();
			// System.out.println("response from impl"+responseString);

		} catch (Exception sqe) {
			sqe.printStackTrace();
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (prepareStatement != null) {
					prepareStatement.close();
					prepareStatement = null;
				}

				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException sqle) {
				sqle.printStackTrace();
			}
		}

		return responseString;
	}

	/* E certification changes end */

	public String doGetLoadForPMOReview(String jsonData, String userId, int isAdmin) throws ServiceLocatorException {

		String responseString = "";

		CallableStatement callableStatement = null;

		JSONObject jObject = null;
		try {
			jObject = new JSONObject(jsonData);

			String customerId = jObject.getString("customerId");
			int year = jObject.getInt("year");
			int month = jObject.getInt("month");
			String ProjectId = jObject.getString("projectId");
			String costModel = jObject.getString("costModel");
			String week = jObject.getString("week");
			String weeklyReport = jObject.getString("weeklyReport");
			// String weeksCount = jObject.getString("weeksCount");
			// String flag = jObject.getString("flag");
			connection = ConnectionProvider.getInstance().getConnection();
			// System.out.println("weeksCount"+weeksCount);

			if (ProjectId.equals("") || ProjectId.equals("-1") || ProjectId.equals("0")) {
				ProjectId = "";
			}
			if (costModel.equals("") || costModel.equals("-1") || costModel.equals("0")) {
				costModel = "";
			}

			if (isAdmin == 1 || userId.equals("ljampana") || userId.equals("ddean")) {
				userId = "%";
			}
			// System.out.println("userId===>"+userId);
			callableStatement = connection.prepareCall("{call spLoadPMOWeeklyReviewData(?,?,?,?,?,?,?,?,?)}");

			callableStatement.setString(1, customerId);
			callableStatement.setInt(2, year);
			callableStatement.setInt(3, month);

			callableStatement.setString(4, ProjectId);
			callableStatement.setString(5, costModel);
			callableStatement.setString(6, week);
			callableStatement.setString(7, weeklyReport);
			callableStatement.setString(8, userId);

			callableStatement.registerOutParameter(9, Types.VARCHAR);
			callableStatement.executeQuery();
			responseString = callableStatement.getString(9);

			// System.out.println(responseString);

		} catch (Exception sqe) {
			sqe.printStackTrace();
		} finally {
			try {

				if (callableStatement != null) {
					callableStatement.close();
					callableStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException sqle) {
			}
		}
		return responseString;
	}

	public String saveWeeklyPMOReviews(String jsonData, String loginId) throws ServiceLocatorException {

		String totalStream = "";
		String query = "";
		String pmoList = "";

		String responseString = "";
		int updatedRows = 0;

		JSONObject jObject = null;
		PreparedStatement preparedStatement = null;

		Connection connection = null;

		int isUpdated = 0;

		try {
			connection = ConnectionProvider.getInstance().getConnection();
			// System.out.println("insertValues out side for
			// loop====>"+ajaxHandlerAction.getHolidayDate());

			jObject = new JSONObject(jsonData);

			int flag = 1;

			int customerId = jObject.getInt("customerId");
			int projectId = jObject.getInt("projectId");
			String weekStartDate = (jObject.getString("weekStartDate"));
			String weekEndDate = (jObject.getString("weekEndDate"));
			String health = jObject.getString("health");
			String scope = jObject.getString("scope");
			String schedule = jObject.getString("schedule");
			String invoice = jObject.getString("invoice");
			String risk = jObject.getString("risk");
			String resources = jObject.getString("resources");
			String comments = jObject.getString("comments");
			String costModel = jObject.getString("costModel");
			// System.out.println(weekStartDate+"weekEndDate"+weekEndDate);

			// query="insert into
			// tblPMOWeeklyReportDetails(CustomerId,ProjectId,WeekStartDate,WeekEndDate,Health,Scope,SCHEDULE,Invoice,Risk,Resources,Comments,SubmittedBy,SubmittedDate,CostModel)
			// values (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

			query = "UPDATE tblPMOWeeklyReportDetails SET Health = ? ,Scope = ?,SCHEDULE = ?,Invoice = ?,Risk = ?,Resources = ?,Comments = ?,SubmittedBy = ? , SubmittedDate = ? , Flag = ?  WHERE CustomerId = ? and ProjectId = ?  and WeekStartDate = ? and WeekEndDate = ?";

			preparedStatement = connection.prepareStatement(query);

			preparedStatement.setString(1, health);
			preparedStatement.setString(2, scope);
			preparedStatement.setString(3, schedule);
			preparedStatement.setString(4, invoice);
			preparedStatement.setString(5, risk);
			preparedStatement.setString(6, resources);
			preparedStatement.setString(7, comments);
			preparedStatement.setString(8, loginId);
			preparedStatement.setString(9, DateUtility.getInstance().getCurrentSQLDate());
			preparedStatement.setInt(10, flag);

			preparedStatement.setInt(11, customerId);
			preparedStatement.setInt(12, projectId);
			preparedStatement.setString(13, weekStartDate);
			preparedStatement.setString(14, weekEndDate);

			isUpdated = preparedStatement.executeUpdate();
			if (isUpdated == 1) {
				responseString = "success";
			} else {
				responseString = "tryagain";
			}
			// System.out.println(responseString);

		} catch (Exception sqe) {
			sqe.printStackTrace();
		} finally {
			try {

				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}

				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException sqle) {
				sqle.printStackTrace();
			}
		}
		return responseString;
	}

	public String updateWeeklyPMOReviews(String jsonData, String loginId) throws ServiceLocatorException {

		String totalStream = "";
		String query = "";
		String pmoList = "";

		String responseString = "";
		int updatedRows = 0;

		JSONObject jObject = null;
		PreparedStatement preparedStatement = null;

		Connection connection = null;

		int isUpdated = 0;

		try {
			connection = ConnectionProvider.getInstance().getConnection();
			// System.out.println("insertValues out side for
			// loop====>"+ajaxHandlerAction.getHolidayDate());

			jObject = new JSONObject(jsonData);

			int customerId = jObject.getInt("customerId");
			int projectId = jObject.getInt("projectId");
			String weekStartDate = (jObject.getString("weekStartDate"));
			String weekEndDate = (jObject.getString("weekEndDate"));
			String health = jObject.getString("health");
			String scope = jObject.getString("scope");
			String schedule = jObject.getString("schedule");
			String invoice = jObject.getString("invoice");
			String risk = jObject.getString("risk");
			String resources = jObject.getString("resources");
			String comments = jObject.getString("comments");
			String costModel = jObject.getString("costModel");
			// System.out.println(weekStartDate+"weekEndDate"+weekEndDate);

			query = "UPDATE tblPMOWeeklyReportDetails SET Health = ? ,Scope = ?,SCHEDULE = ?,Invoice = ?,Risk = ?,Resources = ?,Comments = ?,ModifiedBy = ? , ModifiedDate = ?  WHERE CustomerId = ? and ProjectId = ?  and WeekStartDate = ? and WeekEndDate = ?";

			preparedStatement = connection.prepareStatement(query);

			preparedStatement.setString(1, health);
			preparedStatement.setString(2, scope);
			preparedStatement.setString(3, schedule);
			preparedStatement.setString(4, invoice);
			preparedStatement.setString(5, risk);
			preparedStatement.setString(6, resources);
			preparedStatement.setString(7, comments);
			preparedStatement.setString(8, loginId);
			preparedStatement.setString(9, DateUtility.getInstance().getCurrentSQLDate());

			preparedStatement.setInt(10, customerId);
			preparedStatement.setInt(11, projectId);
			preparedStatement.setString(12, weekStartDate);
			preparedStatement.setString(13, weekEndDate);

			isUpdated = preparedStatement.executeUpdate();
			if (isUpdated == 1) {
				responseString = "success";
			} else {
				responseString = "tryagain";
			}
			// System.out.println(responseString);

		} catch (Exception sqe) {
			sqe.printStackTrace();
		} finally {
			try {

				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}

				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException sqle) {
				sqle.printStackTrace();
			}
		}
		return responseString;
	}

	public String getPMOWeeklyReportSearch(String jsonData, String userId, int isAdmin) throws ServiceLocatorException {
		// String qsTitle = "";
		String responseString = "";

		CallableStatement callableStatement = null;

		JSONObject jObject = null;
		try {
			jObject = new JSONObject(jsonData);

			String customerId = jObject.getString("customerId");
			int year = jObject.getInt("year");
			int month = jObject.getInt("month");
			String ProjectId = jObject.getString("projectId");
			String costModel = jObject.getString("costModel");
			String week = jObject.getString("week");
			String weeklyReport = jObject.getString("weeklyReport");
			// int weeksCount = jObject.getInt("weeksCount");
			// String flag = jObject.getString("flag");
			connection = ConnectionProvider.getInstance().getConnection();
			// System.out.println("weeksCount"+weeksCount);

			if (ProjectId.equals("") || ProjectId.equals("-1") || ProjectId.equals("0")) {
				ProjectId = "";
			}
			if (costModel.equals("") || costModel.equals("-1") || costModel.equals("0")) {
				costModel = "";
			}
			if (isAdmin == 1 || userId.equals("ljampana") || userId.equals("ddean") || userId.equals("rkalaga")) {
				userId = "%";
			}
			callableStatement = connection.prepareCall("{call spLoadPMOWeeklyReviewForSearch(?,?,?,?,?,?,?,?,?)}");

			callableStatement.setString(1, customerId);
			callableStatement.setInt(2, year);
			callableStatement.setInt(3, month);

			callableStatement.setString(4, ProjectId);
			callableStatement.setString(5, costModel);
			callableStatement.setString(6, week);
			callableStatement.setString(7, weeklyReport);
			callableStatement.setString(8, userId);

			callableStatement.registerOutParameter(9, Types.VARCHAR);
			callableStatement.executeQuery();
			responseString = callableStatement.getString(9);

			// System.out.println(responseString);

		} catch (Exception sqe) {
			sqe.printStackTrace();
		} finally {
			try {

				if (callableStatement != null) {
					callableStatement.close();
					callableStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException sqle) {
			}
		}
		return responseString;
	}

	public String updateWeeklyReviewComment(String jsonData, String createdBy) throws ServiceLocatorException {
		String totalStream = "";
		PreparedStatement preparedStatement = null;
		String query = "";
		String pmoList = "";

		String responseString = "";
		int updatedRows = 0;
		JSONObject jObject = null;
		JSONObject jObjectchild = null;
		try {

			connection = ConnectionProvider.getInstance().getConnection();
			jObject = new JSONObject(jsonData);
			String comments = jObject.getString("comment");
			int Id = jObject.getInt("id");
			query = "Update  tblPMOWeeklyReportDetails set Comments=?,ModifiedBy=?,ModifiedDate=? where Id=?";
			preparedStatement = connection.prepareStatement(query);

			preparedStatement.setString(1, comments);
			preparedStatement.setString(2, createdBy);
			preparedStatement.setTimestamp(3, DateUtility.getInstance().getCurrentMySqlDateTime());
			preparedStatement.setInt(4, Id);
			preparedStatement.executeUpdate();
			responseString = "success";
		} catch (Exception sqe) {
			sqe.printStackTrace();
		} finally {
			try {
				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException sqle) {
			}
		}
		return responseString;
	}

	public String getWeeksForGivenMonth(String jsonData) throws ServiceLocatorException {
		// String qsTitle = "";
		String responseString = "";

		CallableStatement callableStatement = null;

		JSONObject jObject = null;
		try {
			jObject = new JSONObject(jsonData);

			int year = jObject.getInt("year");
			int month = jObject.getInt("month");
			int weeksCount = jObject.getInt("weeksCount");
			// String flag = jObject.getString("flag");
			connection = ConnectionProvider.getInstance().getConnection();
			System.out.println("weeksCount" + weeksCount);

			callableStatement = connection.prepareCall("{call spGetWeeksForGivenMonth(?,?,?,?)}");

			callableStatement.setInt(1, year);
			callableStatement.setInt(2, month);
			callableStatement.setInt(3, weeksCount);

			callableStatement.registerOutParameter(4, Types.VARCHAR);
			callableStatement.executeQuery();
			responseString = callableStatement.getString(4);

			// System.out.println(responseString);

		} catch (Exception sqe) {
			sqe.printStackTrace();
		} finally {
			try {

				if (callableStatement != null) {
					callableStatement.close();
					callableStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException sqle) {
			}
		}
		return responseString;
	}

	/* sales performance review report start */
	public String getReviewSalesStatusReview(NewAjaxHandlerAction ajaxHandlerAction,
			HttpServletRequest httpServletRequest) throws ServiceLocatorException {

		Connection connection = null;
		ResultSet resultSet = null;
		String responseString = "";
		int updatedRows = 0;
		CallableStatement callableStatement = null;
		String myTeamMembers = "";
		Map teamMembers = new TreeMap();
		DataSourceDataProvider dataSourceDataProvider = null;

		try {
			// System.out.println("in service
			// impl"+ajaxHandlerAction.getSubPracticeId());
			if ("-1".equals(ajaxHandlerAction.getTeamMemberId())) {
				teamMembers = DataSourceDataProvider.getInstance()
						.getAllSalesTeamByCountry(ajaxHandlerAction.getCountry());
				if (teamMembers.size() > 0) {
					myTeamMembers = getKeys(teamMembers, ",");
					// System.out.println("myTeamMembersall"+myTeamMembers);
				}
			} /*
				 * else { String teamMemberId =
				 * ajaxHandlerAction.getTeamMemberId(); String empTitle =
				 * DataSourceDataProvider.getInstance().getEmpTitleByLoginId(
				 * teamMemberId); if ("BDM".equalsIgnoreCase(empTitle)) { int
				 * empId =
				 * DataSourceDataProvider.getInstance().getEmpIdByLoginId(
				 * teamMemberId); teamMembers =
				 * DataSourceDataProvider.getInstance().getBdmAssociateList(
				 * Integer.toString( empId)); myTeamMembers =
				 * getKeys(teamMembers, ","); myTeamMembers = myTeamMembers +
				 * ",'" + ajaxHandlerAction.getTeamMemberId() + "'";
				 * System.out.println("myTeamMemberselseif"+myTeamMembers); }
				 */ else {
				myTeamMembers = "'" + ajaxHandlerAction.getTeamMemberId() + "'";
			}
			// }
			myTeamMembers = myTeamMembers.replaceAll("'", "");

			connection = ConnectionProvider.getInstance().getConnection();
			callableStatement = connection.prepareCall("{call spSalesPerformanceStatusReview(?,?,?,?,?,?)}");

			callableStatement.setString(1, myTeamMembers);
			if (ajaxHandlerAction.getStartDate() != null && !"".equals(ajaxHandlerAction.getStartDate())) {
				callableStatement.setString(2,
						DateUtility.getInstance().convertStringToMySQLDate(ajaxHandlerAction.getStartDate()));
			} else {
				callableStatement.setString(2, "");
			}
			if (ajaxHandlerAction.getEndDate() != null && !"".equals(ajaxHandlerAction.getEndDate())) {
				callableStatement.setString(3,
						DateUtility.getInstance().convertStringToMySQLDate(ajaxHandlerAction.getEndDate()));
			} else {
				callableStatement.setString(3, "");
			}
			if (ajaxHandlerAction.getSubPracticeId().equalsIgnoreCase("-1")) {
				callableStatement.setString(4, "%");
			} else {
				callableStatement.setString(4, ajaxHandlerAction.getSubPracticeId());
			}

			if (ajaxHandlerAction.getCountry() != null && !"".equals(ajaxHandlerAction.getCountry())) {
				callableStatement.setString(5, ajaxHandlerAction.getCountry());
			}
			callableStatement.registerOutParameter(6, Types.VARCHAR);
			updatedRows = callableStatement.executeUpdate();
			responseString = callableStatement.getString(6);

		} catch (Exception sqe) {
			sqe.printStackTrace();
		} finally {
			try {
				if (callableStatement != null) {
					callableStatement.close();
					callableStatement = null;
				}

				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException sqle) {
			}
		}
		// System.out.println("responseString-->"+responseString);
		return responseString;
	}

	/* sales performance review report end */

	public String getEmpDetailsForBioMetric(String jsonData, HttpServletRequest httpServletRequest)
			throws ServiceLocatorException {

		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		Connection connection = null;
		String response = "-";
		JSONObject jObj = null;
		JSONObject subJson = null;
		JSONArray jArray = new JSONArray();

		try {
			int empId = Integer.parseInt(
					httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_EMP_ID).toString());
			int isManager = Integer.parseInt(httpServletRequest.getSession(false)
					.getAttribute(ApplicationConstants.SESSION_IS_USER_MANAGER).toString());
			int isAdmin = Integer.parseInt(httpServletRequest.getSession(false)
					.getAttribute(ApplicationConstants.SESSION_IS_ADMIN_ACCESS).toString());

			jObj = new JSONObject(jsonData);
			queryString = "SELECT DISTINCT(tblEmployee.EmpNo),tblEmployee.Id, DepartmentId, LName, FName, MName, Email1, WorkPhoneNo, AlterPhoneNo, CellPhoneNo,"
					+ " CurStatus ,LoginId, tblBioPayrollAttendanceLogs.ReviewFlag FROM tblEmployee JOIN tblBioPayrollAttendanceLogs"
					+ " ON(tblEmployee.EmpNo=tblBioPayrollAttendanceLogs.EmpNo) WHERE 1=1 ";

			if (!"".equals(jObj.getString("empNo")) && jObj.getString("empNo") != null) {
				queryString = queryString + "AND tblEmployee.EmpNo = " + jObj.getString("empNo");
			}
			if (!"".equals(jObj.getString("loginId")) && jObj.getString("loginId") != null) {
				queryString = queryString + " AND LoginId  LIKE '" + jObj.getString("loginId") + "' ";
			}
			if (!"".equals(jObj.getString("departmentId")) && jObj.getString("departmentId") != null) {
				queryString = queryString + " AND DepartmentId LIKE '" + jObj.getString("departmentId") + "' ";
			}

			if (!"".equals(jObj.getString("location")) && jObj.getString("location") != null
					&& !"All".equals(jObj.getString("location"))) {
				queryString = queryString + " AND Location LIKE '" + jObj.getString("location") + "' ";
			}
			if (!"".equals(jObj.getString("practiceId")) && jObj.getString("practiceId") != null) {
				queryString = queryString + " AND PracticeId LIKE '" + jObj.getString("practiceId") + "' ";
			}
			if (jObj.getInt("opsContactId") != 0 && jObj.getInt("opsContactId") != -1
					&& jObj.getInt("opsContactId") != 1) {
				queryString = queryString + " AND OpsContactId =" + jObj.getInt("opsContactId");

			}
			if (isManager == 0 && isAdmin == 0) {
				queryString = queryString + " AND OpsContactId = " + empId + "";
			}
			if (jObj.getString("reviewType").equals("notReviewed")) {
				queryString = queryString + " AND tblBioPayrollAttendanceLogs.ReviewFlag=0";
			} else if (jObj.getString("reviewType").equals("reviewed")) {
				queryString = queryString + " AND tblBioPayrollAttendanceLogs.ReviewFlag=1";
			}

			/*
			 * if (!"".equals(getReportingpersonId()) && getReportingpersonId()
			 * != null && !"All".equals(getReportingpersonId())) { queryString =
			 * queryString +" AND ReportsTo LIKE '"+
			 * getReportingpersonId()+"' ";
			 * 
			 * }
			 */

			if (jObj.getBoolean("isTeamLead")) {
				// setTeamLead(isTeamLead());
				// System.out.println("isIsTeamLead()-->" + isTeamLead());
				queryString = queryString + " AND IsTeamLead =1 ";
			}
			if (jObj.getBoolean("isManager")) {
				// setManager(isManager());
				// System.out.println("getIsManager()-->" + isManager());
				queryString = queryString + " AND IsManager =1 ";
			}

			/*
			 * queryString+
			 * =" AND tblEmployee.EmpNo IN(SELECT DISTINCT EmpNo FROM tblBioPayrollAttendanceLogs WHERE MONTH(AttendanceDate)='"
			 * +jObj.getString("month")+"' AND YEAR(AttendanceDate)='"+jObj.
			 * getString("year" )+"')"; queryString = queryString +
			 * " AND CurStatus='Active'  and  DeletedFlag != 1 GROUP BY tblEmployee.EmpNo ORDER BY trim(FName) "
			 * ;
			 */

			queryString += " AND MONTH(AttendanceDate)='" + jObj.getString("month") + "' AND YEAR(AttendanceDate)='"
					+ jObj.getString("year") + "' "
					+ " AND tblEmployee.EmpNo IS NOT NULL AND tblEmployee.EmpNo != 0 and  DeletedFlag != 1  ORDER BY trim(FName)";
			// queryString = queryString + " AND CurStatus='Active' and
			// DeletedFlag != 1
			// GROUP BY tblEmployee.EmpNo ORDER BY trim(FName) ";

			// System.out.println("queryString=="+queryString);
			connection = ConnectionProvider.getInstance().getConnection();
			// SELECT Comments FROM tblEmpIssues WHERE Id=810
			preparedStatement = connection.prepareStatement(queryString);
			resultSet = preparedStatement.executeQuery();
			int count = 0;

			String monthString = new DateFormatSymbols().getMonths()[Integer.parseInt(jObj.getString("month")) - 1];
			String RewiewType;

			while (resultSet.next()) {

				if (resultSet.getString("ReviewFlag").equals("0")) {
					RewiewType = "Not Reviewed";
				} else {
					RewiewType = "Reviewed";
				}
				subJson = new JSONObject();
				subJson.put("fName", resultSet.getString("FName"));
				subJson.put("lName", resultSet.getString("LName"));
				subJson.put("empNo", resultSet.getString("EmpNo"));
				subJson.put("workPhoneNo", resultSet.getString("WorkPhoneNo"));
				subJson.put("cellPhoneNo", resultSet.getString("CellPhoneNo"));
				subJson.put("email", resultSet.getString("Email1"));
				subJson.put("status", resultSet.getString("CurStatus"));
				subJson.put("empId", resultSet.getString("Id"));
				subJson.put("reviewType", RewiewType);
				subJson.put("month", monthString);
				subJson.put("year", jObj.getString("year"));
				subJson.put("monthNo", jObj.getString("month"));

				jArray.put(subJson);
				count++;
			}

			// System.out.println("coount"+count);
			// System.out.println("jArray"+jArray.toString());
		} catch (SQLException ex) {
			ex.printStackTrace();
		} catch (Exception sle) {
			sle.printStackTrace();
		} finally {
			try {

				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
		}

		return jArray.toString();
	}

	@Override
	public String getLunchBoxData(String jsonData) throws ServiceLocatorException {
		String response = "";
		try {
			// JSONObject jb = new JSONObject();
			// System.out.println("in impl ajax");
			String serviceUrl = RestRepository.getInstance().getSrviceUrl("getLunchBoxData");
			// System.out.println("serviceUrl===>"+serviceUrl);
			URL url = new URL(serviceUrl);
			URLConnection connection = url.openConnection();
			connection.setDoOutput(true);
			connection.setRequestProperty("Content-Type", "application/json");
			connection.setConnectTimeout(360 * 5000);
			connection.setReadTimeout(360 * 5000);
			OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
			out.write(jsonData);
			out.close();

			BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			String s = null;
			String data = "";
			while ((s = in.readLine()) != null) {

				data = data + s;
			}

			JSONObject jObject = new JSONObject(data);

			response = jObject.getString("lunchBoxData");
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		// System.out.println("output==>"+response);
		return response;
	}

	@Override
	public String updateLunchBoxStatus(String jsonData) throws ServiceLocatorException {
		String response = "";
		try {
			JSONObject jb = new JSONObject();
			// System.out.println("in impl ajax");
			String serviceUrl = RestRepository.getInstance().getSrviceUrl("updateLunchBoxData");
			// System.out.println("serviceUrl===>"+serviceUrl);
			URL url = new URL(serviceUrl);
			URLConnection connection = url.openConnection();
			connection.setDoOutput(true);
			connection.setRequestProperty("Content-Type", "application/json");
			connection.setConnectTimeout(360 * 5000);
			connection.setReadTimeout(360 * 5000);
			OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
			out.write(jsonData);
			out.close();

			BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			String s = null;
			String data = "";
			while ((s = in.readLine()) != null) {

				data = data + s;
			}

			JSONObject jObject = new JSONObject(data);

			response = jObject.getString("ResultString");
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		// System.out.println("output==>"+response);
		return response;
	}

	// Roll in Roll Off Overall analysis Changes start

	public String getRollInRollOffAnalysisDetails(NewAjaxHandlerAction ajaxHandlerAction,
			HttpServletRequest httpServletRequest) throws ServiceLocatorException {

		Connection connection = null;
		ResultSet resultSet = null;
		String responseString = "";
		int updatedRows = 0;
		CallableStatement callableStatement = null;
		String myTeamMembers = "";
		Map teamMembers = new TreeMap();
		DataSourceDataProvider dataSourceDataProvider = null;

		try {

			connection = ConnectionProvider.getInstance().getConnection();

			if (!"".equals(ajaxHandlerAction.getReportBasedOn())
					&& ajaxHandlerAction.getReportBasedOn().equalsIgnoreCase("Quarterly")) {

				callableStatement = connection.prepareCall("{call spGetRollInRollOffAnalysisDetailsfinalNew(?,?,?)}");
			} else {

				callableStatement = connection.prepareCall("{call spGetRollInRollOffAnalysisDetailsMonthly(?,?,?)}");
			}

			callableStatement.setString(1, ajaxHandlerAction.getYear());

			callableStatement.setString(2, ajaxHandlerAction.getCountry());

			callableStatement.registerOutParameter(3, Types.VARCHAR);
			updatedRows = callableStatement.executeUpdate();
			responseString = callableStatement.getString(3);
		} catch (Exception sqe) {
			sqe.printStackTrace();
		} finally {
			try {
				if (callableStatement != null) {
					callableStatement.close();
					callableStatement = null;
				}

				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException sqle) {
			}
		}
		// System.out.println("responseString-->"+responseString);
		return responseString;
	}

	public String getRollInRollOffAnalysisDetailsPractice(NewAjaxHandlerAction ajaxHandlerAction,
			HttpServletRequest httpServletRequest) throws ServiceLocatorException {

		Connection connection = null;
		ResultSet resultSet = null;
		String responseString = "";
		int updatedRows = 0;
		CallableStatement callableStatement = null;
		String myTeamMembers = "";
		Map teamMembers = new TreeMap();
		DataSourceDataProvider dataSourceDataProvider = null;

		try {

			connection = ConnectionProvider.getInstance().getConnection();

			if (!"".equals(ajaxHandlerAction.getReportBasedOn())
					&& ajaxHandlerAction.getReportBasedOn().equalsIgnoreCase("Quarterly")) {

				callableStatement = connection
						.prepareCall("{call spGetRollInRollOffAnalysispPracticeDetails(?,?,?,?)}");
			} else {

				callableStatement = connection
						.prepareCall("{call spGetRollInRollOffAnalysisPracticeDetailsMonthly(?,?,?,?)}");
			}

			callableStatement.setString(1, ajaxHandlerAction.getYear());
			// callableStatement.setString(2,
			// ajaxHandlerAction.getReportBasedOn());
			callableStatement.setString(2, ajaxHandlerAction.getCountry());

			callableStatement.setString(3, ajaxHandlerAction.getDepartmentId());

			callableStatement.registerOutParameter(4, Types.VARCHAR);
			updatedRows = callableStatement.executeUpdate();
			responseString = callableStatement.getString(4);
		} catch (Exception sqe) {
			sqe.printStackTrace();
		} finally {
			try {
				if (callableStatement != null) {
					callableStatement.close();
					callableStatement = null;
				}

				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException sqle) {
			}
		}
		// System.out.println("responseString-->"+responseString);
		return responseString;
	}

	// Roll in Roll Off Overall analysis Changes end

	public String getProjectResourcesRollInRollOffReportDetails(NewAjaxHandlerAction ajaxHandlerAction,
			HttpServletRequest httpServletRequest) throws ServiceLocatorException {

		Connection connection = null;
		ResultSet resultSet = null;
		String responseString = "";
		int updatedRows = 0;
		CallableStatement callableStatement = null;
		String myTeamMembers = "";
		Map teamMembers = new TreeMap();
		DataSourceDataProvider dataSourceDataProvider = null;

		try {

			connection = ConnectionProvider.getInstance().getConnection();
			callableStatement = connection
					.prepareCall("{call spGetProjectResourcesRollInRollOffReportDetails(?,?,?,?,?,?,?,?)}");

			callableStatement.setString(1,
					DateUtility.getInstance().convertStringToMySQLDate(ajaxHandlerAction.getProjstartDate()));
			callableStatement.setString(2,
					DateUtility.getInstance().convertStringToMySQLDate(ajaxHandlerAction.getEndDate()));
			callableStatement.setString(3, ajaxHandlerAction.getType());

			callableStatement.setString(4, ajaxHandlerAction.getDepartmentId());

			callableStatement.setString(5, ajaxHandlerAction.getPracticeId());

			callableStatement.setString(6, ajaxHandlerAction.getCostModel());

			callableStatement.setString(7, ajaxHandlerAction.getCustomerName());

			callableStatement.registerOutParameter(8, Types.VARCHAR);
			updatedRows = callableStatement.executeUpdate();
			responseString = callableStatement.getString(8);
			// System.out.println("responseString"+responseString);
		} catch (Exception sqe) {
			sqe.printStackTrace();
		} finally {
			try {
				if (callableStatement != null) {
					callableStatement.close();
					callableStatement = null;
				}

				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException sqle) {
			}
		}
		// System.out.println("responseString-->"+responseString);
		return responseString;
	}

	/*
	 * sarada tatisetti 9/17/2019
	 */

	public String getTeamMemberByTitleType(String titleType) throws ServiceLocatorException {
		stringBuffer = new StringBuffer();

		if ("-1".equals(titleType)) {
			titleType = "%";
			// System.out.println("titleType" + titleType);
		}

		queryString = " SELECT LoginId,CONCAT(fName,' ',mName,'.',lName) AS EmployeeName FROM tblEmployee WHERE TitleTypeId LIKE '"
				+ titleType + "' AND DepartmentId='sales' AND CurStatus='Active' ORDER BY EmployeeName";

		try {
			connection = ConnectionProvider.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(queryString);
			resultSet = preparedStatement.executeQuery();
			// stringBuffer.append("<?xml version=\"1.0\"
			// encoding=\"UTF-8\"?>");
			stringBuffer.append("<xml version=\"1.0\">");
			stringBuffer.append("<TITLETYPEID Description=\"" + titleType + "\">");

			// stringBuffer.append("<EMPLOYEENAME userId=\"\">--Please
			// Select--</EMPLOYEENAME>");
			// stringBuffer.append("<EMPLOYEENAME>All</EMPLOYEENAME>");
			stringBuffer.append("<EMPLOYEENAME userId=\"" + -1 + "\">");
			stringBuffer.append("--Please Select--");
			stringBuffer.append("</EMPLOYEENAME>");
			while (resultSet.next()) {
				// stringBuffer.append("<EMPLOYEENAME att=\"\">" +
				// resultSet.getString("EmployeeName") + "</EMPLOYEENAME>");
				stringBuffer.append("<EMPLOYEENAME userId=\"" + resultSet.getString("LoginId") + "\">");
				stringBuffer.append(resultSet.getString("EmployeeName"));
				stringBuffer.append("</EMPLOYEENAME>");

			}
			stringBuffer.append("</TITLETYPEID>");
			stringBuffer.append("</xml>");

		} catch (SQLException ex) {
			ex.printStackTrace();
		} catch (ServiceLocatorException sle) {
			sle.printStackTrace();
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException ex) {
				ex.printStackTrace();
			}

		}
		// System.out.println("stringBuffer.toString();" +
		// stringBuffer.toString());

		return stringBuffer.toString();
	}

	public String popupConferenceCallsWindow(NewAjaxHandlerAction ajaxHandlerAction,
			HttpServletRequest httpServletRequest) throws ServiceLocatorException {
		// String qsTitle = "";
		Statement statement = null;
		ResultSet resultSet = null;
		String query = "";
		Connection connection = null;
		String response = "";
		try {
			connection = ConnectionProvider.getInstance().getConnection();
			query = "SELECT SUM(CASE WHEN tblCrmActivity.Activitytype ='Confcall Setup' THEN 1 ELSE 0 END) AS ConfcallSetup ,"
					+ "SUM(CASE WHEN tblCrmActivity.Activitytype ='Conference Call' THEN 1 ELSE 0 END) AS ConferenceCall,CONCAT(FName,' ',MName,'.',LName) AS EmpName FROM tblCrmActivity "
					+ "LEFT JOIN tblEmployee ON tblEmployee.LoginId=tblCrmActivity.CreatedById WHERE tblCrmActivity.CreatedById='"
					+ ajaxHandlerAction.getLoginId() + " '";
			if (ajaxHandlerAction.getStartDate() != null && !"".equals(ajaxHandlerAction.getStartDate())) {

				query = query + " AND DATE(tblCrmActivity.CreatedDate) >='"
						+ DateUtility.getInstance().convertStringToMySQLDate(ajaxHandlerAction.getStartDate()) + "'";
			}

			if (ajaxHandlerAction.getEndDate() != null && !"".equals(ajaxHandlerAction.getEndDate())) {
				query = query + " AND DATE(tblCrmActivity.CreatedDate) <='"
						+ DateUtility.getInstance().convertStringToMySQLDate(ajaxHandlerAction.getEndDate()) + "'";

			}
			// System.out.println("query.." + query);
			statement = connection.createStatement();
			resultSet = statement.executeQuery(query);
			while (resultSet.next()) {
				response = response + resultSet.getString("EmpName") + "^" + resultSet.getInt("ConfcallSetup") + "^"
						+ resultSet.getInt("ConferenceCall");

			}
			// System.out.println("query..."+query);
			// System.out.println("response.." + response);
		} catch (Exception sqe) {
			sqe.printStackTrace();
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (statement != null) {
					statement.close();
					statement = null;
				}

				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException sqle) {
				sqle.printStackTrace();
			}
		}

		return response;

	}

	public String popupF2FvisitsWindow(NewAjaxHandlerAction ajaxHandlerAction, HttpServletRequest httpServletRequest)
			throws ServiceLocatorException {
		// String qsTitle = "";
		Statement statement = null;
		ResultSet resultSet = null;
		String query = "";
		Connection connection = null;
		String response = "";
		try {
			connection = ConnectionProvider.getInstance().getConnection();
			query = "SELECT SUM(CASE WHEN tblCrmActivity.Activitytype ='Customer Demo-F2F' THEN 1 ELSE 0 END) AS CustomerDemoF2FCount ,"
					+ "SUM(CASE WHEN tblCrmActivity.Activitytype='Customer Demo Virtual' THEN 1 ELSE 0 END) AS CustomerDemoVirtualCount ,"
					+ " SUM(CASE WHEN tblCrmActivity.Activitytype ='Customer Meet- Break Fast' THEN 1 ELSE 0 END) AS CustomerMeetBreakFast ,"
					+ "SUM(CASE WHEN tblCrmActivity.Activitytype ='Customer Meet-F2F' THEN 1 ELSE 0 END) AS CustomerMeetF2F ,"
					+ " SUM(CASE WHEN tblCrmActivity.Activitytype ='Customer Meet-Lunch' THEN 1 ELSE 0 END) AS CustomerMeetLunch ,"
					+ "SUM(CASE WHEN tblCrmActivity.Activitytype ='Customer Meet-Dinner' THEN 1 ELSE 0 END) AS CustomerMeetDinner ,"
					+ " SUM(CASE WHEN tblCrmActivity.Activitytype ='Customer Visit Miracle Facility' THEN 1 ELSE 0 END) AS CustomerVisitMiracleFacility ,"
					+ "CONCAT(FName,' ',MName,'.',LName) AS EmpName FROM tblCrmActivity "
					+ "LEFT JOIN tblEmployee ON tblEmployee.LoginId=tblCrmActivity.CreatedById WHERE tblCrmActivity.CreatedById='"
					+ ajaxHandlerAction.getLoginId() + " '";

			if (ajaxHandlerAction.getStartDate() != null && !"".equals(ajaxHandlerAction.getStartDate())) {

				query = query + " AND DATE(tblCrmActivity.CreatedDate) >='"
						+ DateUtility.getInstance().convertStringToMySQLDate(ajaxHandlerAction.getStartDate()) + "'";
			}

			if (ajaxHandlerAction.getEndDate() != null && !"".equals(ajaxHandlerAction.getEndDate())) {
				query = query + " AND DATE(tblCrmActivity.CreatedDate) <='"
						+ DateUtility.getInstance().convertStringToMySQLDate(ajaxHandlerAction.getEndDate()) + "'";

			}

			// System.out.println("query.." + query);
			statement = connection.createStatement();
			resultSet = statement.executeQuery(query);
			while (resultSet.next()) {
				response = response + resultSet.getString("EmpName") + "^" + resultSet.getInt("CustomerDemoF2FCount")
						+ "^" + resultSet.getInt("CustomerDemoVirtualCount") + "^"
						+ resultSet.getInt("CustomerMeetBreakFast") + "^" + resultSet.getInt("CustomerMeetF2F") + "^"
						+ resultSet.getInt("CustomerMeetLunch") + "^" + resultSet.getInt("CustomerMeetDinner") + "^"
						+ resultSet.getInt("CustomerVisitMiracleFacility");

			}
			// System.out.println("query..."+query);
			// System.out.println("response.." + response);
		} catch (Exception sqe) {
			sqe.printStackTrace();
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (statement != null) {
					statement.close();
					statement = null;
				}

				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException sqle) {
				sqle.printStackTrace();
			}
		}

		return response;

	}

	public String getConferenceCallsAndF2FVisits(NewAjaxHandlerAction ajaxHandlerAction,
			HttpServletRequest httpServletRequest) throws ServiceLocatorException {

		Connection connection = null;
		ResultSet resultSet = null;
		String responseString = "";
		int updatedRows = 0;
		CallableStatement callableStatement = null;
		String myTeamMembers = "";
		String newmyTeamMembers = "";

		String title = "";
		Map teamMembers = new TreeMap();
		Map salesTitles = new TreeMap();
		String myTeamMembersTeamByTitle = "";
		Map teamMembersTeamByTitle = new TreeMap();
		List list = new ArrayList();
		String teamMemberId = "";
		DataSourceDataProvider dataSourceDataProvider = null;

		title = ajaxHandlerAction.getTitle();
		teamMemberId = ajaxHandlerAction.getTeamMemberId();
		try {
			if ("-1".equals(ajaxHandlerAction.getTitle()) && "-1".equals(ajaxHandlerAction.getTeamMemberId())) {

				salesTitles = DataSourceDataProvider.getInstance().getTitleMap();
				if (salesTitles.size() > 0) {
					title = getKeys(salesTitles, ",");
				}
				title = title.replaceAll("'", "");
				teamMembers = DataSourceDataProvider.getInstance().getAllSalesTeam();
				if (teamMembers.size() > 0) {
					myTeamMembers = getKeys(teamMembers, ",");
				}
				list.add(myTeamMembers);

			}

			else if ("-1".equals(ajaxHandlerAction.getTeamMemberId())) {

				teamMembersTeamByTitle = DataSourceDataProvider.getInstance().getAllSalesTeamByTitle(title);

				if (teamMembers.size() > 0) {
					myTeamMembersTeamByTitle = getKeys(teamMembers, ",");
				}

				String[] teamMember = myTeamMembersTeamByTitle.split(",");
				for (int i = 0; i < teamMember.length; i++) {

					Map teamMap = DataSourceDataProvider.getInstance().getMyTeamMembers(teamMember[i], "sales");
					// myTeamMembers = getKeys(teamMap, ",");

					myTeamMembers = DataSourceDataProvider.getInstance().getTeamLoginIdList(teamMap);
					myTeamMembers = myTeamMembers.replaceAll("'", "");
					if (myTeamMembers.equals("")) {
						myTeamMembers = teamMember[i];
					} else {
						myTeamMembers = myTeamMembers + "," + teamMember[i];
					}
					list.add(myTeamMembers);

				}

				salesTitles = DataSourceDataProvider.getInstance().getTitleMap();
				if (salesTitles.size() > 0) {
					title = getKeys(salesTitles, ",");
				}
				title = title.replaceAll("'", "");

			} else {

				String empTitle = DataSourceDataProvider.getInstance().getEmpTitleByLoginId(teamMemberId);
				if ("BDM".equalsIgnoreCase(empTitle)) {

					salesTitles = DataSourceDataProvider.getInstance().getTitleMap();
					if (salesTitles.size() > 0) {
						title = getKeys(salesTitles, ",");
					}
					title = title.replaceAll("'", "");
					int empId = DataSourceDataProvider.getInstance().getEmpIdByLoginId(teamMemberId);
					teamMembers = DataSourceDataProvider.getInstance().getBdmAssociateList(Integer.toString(empId));

					myTeamMembers = getKeys(teamMembers, ",");

					String[] teamMember = myTeamMembers.split(",");
					for (int i = 0; i < teamMember.length; i++) {

						Map teamMap = DataSourceDataProvider.getInstance().getMyTeamMembers(teamMember[i], "sales");

						myTeamMembers = DataSourceDataProvider.getInstance().getTeamLoginIdList(teamMap);
						myTeamMembers = myTeamMembers.replaceAll("'", "");
						if (myTeamMembers.equals("")) {
							myTeamMembers = teamMember[i];
						} else {
							myTeamMembers = myTeamMembers + "," + teamMember[i];
						}

						list.add(myTeamMembers);
						list.add(teamMemberId);
					}

				} else {

					salesTitles = DataSourceDataProvider.getInstance().getTitleMap();
					if (salesTitles.size() > 0) {
						title = getKeys(salesTitles, ",");
					}
					title = title.replaceAll("'", "");

					Map teamMap = DataSourceDataProvider.getInstance().getMyTeamMembers(teamMemberId, "sales");

					myTeamMembers = DataSourceDataProvider.getInstance().getTeamLoginIdList(teamMap);
					myTeamMembers = myTeamMembers.replaceAll("'", "");
					if (myTeamMembers.equals("")) {
						myTeamMembers = teamMemberId;
					} else {
						myTeamMembers = myTeamMembers + "," + teamMemberId;
					}
					list.add(myTeamMembers);

				}
			}

			for (int j = 0; j < list.size(); j++) {

				newmyTeamMembers = newmyTeamMembers + "," + list.get(j).toString();
			}

			newmyTeamMembers = newmyTeamMembers.replaceAll("'", "");

			connection = ConnectionProvider.getInstance().getConnection();
			callableStatement = connection.prepareCall("{call spConferenceCallsAndF2FVisits(?,?,?,?,?)}");

			callableStatement.setString(1, newmyTeamMembers);
			if (ajaxHandlerAction.getStartDate() != null && !"".equals(ajaxHandlerAction.getStartDate())) {
				callableStatement.setString(2,
						DateUtility.getInstance().convertStringToMySQLDate(ajaxHandlerAction.getStartDate()));
			} else {
				callableStatement.setString(2, "");
			}
			if (ajaxHandlerAction.getEndDate() != null && !"".equals(ajaxHandlerAction.getEndDate())) {
				callableStatement.setString(3,
						DateUtility.getInstance().convertStringToMySQLDate(ajaxHandlerAction.getEndDate()));
			} else {
				callableStatement.setString(3, "");
			}
			callableStatement.setString(4, title);

			callableStatement.registerOutParameter(5, Types.VARCHAR);

			updatedRows = callableStatement.executeUpdate();
			responseString = callableStatement.getString(5);
			// System.out.println("responseString" + responseString);

		} catch (Exception sqe) {
			sqe.printStackTrace();
		} finally {
			try {
				if (callableStatement != null) {
					callableStatement.close();
					callableStatement = null;
				}

				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException sqle) {
			}
		}
		// System.out.println("responseString-->"+responseString);
		return responseString;
	}

	public String getSalesTeamMemberByCountry(String country) throws ServiceLocatorException {
		stringBuffer = new StringBuffer();

		if ("-1".equals(country)) {
			country = "%";
			// System.out.println("titleType" + titleType);
		}

		queryString = " SELECT LoginId,CONCAT(fName,' ',mName,'.',lName) AS EmployeeName FROM tblEmployee WHERE Id IN (SELECT EmpId FROM tblEmpRoles LEFT OUTER JOIN tblLKRoles ON (tblEmpRoles.RoleId=tblLKRoles.Id) WHERE Description='Sales') AND CurStatus = 'Active' AND country like '"
				+ country + "' ORDER BY EmployeeName";

		try {
			connection = ConnectionProvider.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(queryString);
			resultSet = preparedStatement.executeQuery();
			// stringBuffer.append("<?xml version=\"1.0\"
			// encoding=\"UTF-8\"?>");
			stringBuffer.append("<xml version=\"1.0\">");
			stringBuffer.append("<COUNTRY Description=\"" + country + "\">");

			// stringBuffer.append("<EMPLOYEENAME userId=\"\">--Please
			// Select--</EMPLOYEENAME>");
			// stringBuffer.append("<EMPLOYEENAME>All</EMPLOYEENAME>");
			stringBuffer.append("<EMPLOYEENAME userId=\"" + -1 + "\">");
			stringBuffer.append("--Please Select--");
			stringBuffer.append("</EMPLOYEENAME>");
			while (resultSet.next()) {
				// stringBuffer.append("<EMPLOYEENAME att=\"\">" +
				// resultSet.getString("EmployeeName") + "</EMPLOYEENAME>");
				stringBuffer.append("<EMPLOYEENAME userId=\"" + resultSet.getString("LoginId") + "\">");
				stringBuffer.append(resultSet.getString("EmployeeName"));
				stringBuffer.append("</EMPLOYEENAME>");

			}
			stringBuffer.append("</COUNTRY>");
			stringBuffer.append("</xml>");

		} catch (SQLException ex) {
			ex.printStackTrace();
		} catch (ServiceLocatorException sle) {
			sle.printStackTrace();
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException ex) {
				ex.printStackTrace();
			}

		}
		// System.out.println("stringBuffer.toString();" +
		// stringBuffer.toString());

		return stringBuffer.toString();
	}

	/*
	 * NagaLakshmi Telluri 10/15/2019
	 * 
	 * @see
	 * com.mss.mirage.ajaxnew.NewAjaxHandlerService#getImmigrationDetails(java.
	 * lang.String)
	 */

	public String getImmigrationDetails(String status) throws ServiceLocatorException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		String responseString = "";
		int i = 0;

		if ("-1".equals(status)) {
			status = "%";
		}

		try {
			connection = ConnectionProvider.getInstance().getConnection();

			String queryString = "SELECT  EmpId,CONCAT(fname,' ',lname) AS EmpName,ImmigrationStatus, StartDate,DateExpire, tblEmpImmigrationV2.CreatedBy as CreatedBy,SevisNumber,EadNumber,ApplicationNumber FROM tblEmpImmigrationV2 LEFT JOIN tblEmployee ON (tblEmployee.Id=tblEmpImmigrationV2.EmpId)  WHERE ImmigrationStatus like '"
					+ status + "'";

			preparedStatement = connection.prepareStatement(queryString);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {

				i++;

				responseString = responseString + i + "#^$";

				if (resultSet.getString("EmpName") != null && !"".equals(resultSet.getString("EmpName"))) {
					responseString = responseString + resultSet.getString("EmpName") + "#^$";
				} else {
					responseString = responseString + "-" + "#^$";
				}
				if (resultSet.getString("ImmigrationStatus") != null
						&& !"".equals(resultSet.getString("ImmigrationStatus"))) {
					responseString = responseString + resultSet.getString("ImmigrationStatus") + "#^$";
				} else {
					responseString = responseString + "-" + "#^$";
				}

				if (resultSet.getDate("StartDate") != null && !"".equals(resultSet.getDate("StartDate"))) {

					responseString = responseString
							+ DateUtility.getInstance().convertDateToView(resultSet.getDate("StartDate")) + "#^$";
				} else {
					responseString = responseString + "-" + "#^$";
				}

				if (resultSet.getString("DateExpire") != null && !"".equals(resultSet.getString("DateExpire"))) {
					responseString = responseString
							+ DateUtility.getInstance().convertDateToView(resultSet.getDate("DateExpire")) + "#^$";
				} else {
					responseString = responseString + "-" + "#^$";
				}

				if (resultSet.getString("SevisNumber") != null && !"".equals(resultSet.getString("SevisNumber"))) {
					responseString = responseString + resultSet.getString("SevisNumber") + "#^$";
				} else {
					responseString = responseString + "NA" + "#^$";
				}

				if (resultSet.getString("EadNumber") != null && !"".equals(resultSet.getString("EadNumber"))) {
					responseString = responseString + resultSet.getString("EadNumber") + "#^$";
				} else {
					responseString = responseString + "NA" + "#^$";
				}

				if (resultSet.getString("ApplicationNumber") != null
						&& !"".equals(resultSet.getString("ApplicationNumber"))) {
					responseString = responseString + resultSet.getString("ApplicationNumber") + "#^$";
				} else {
					responseString = responseString + "NA" + "#^$";
				}

				int id = resultSet.getInt("EmpId");

				String currentDate = DateUtility.getInstance().getCurrentSQLDate1();

				responseString = responseString + currentDate + "#^$";

				responseString = responseString + id + "*@!";

				// System.out.println("responseString"+responseString);

				/*
				 * if (resultSet.getString("CreatedBy") != null &&
				 * !"".equals(resultSet.getString("CreatedBy"))) {
				 * responseString = responseString +
				 * resultSet.getString("CreatedBy") + "*@!"; } else {
				 * responseString = responseString + "-" + "*@!"; }
				 */

				// System.out.println("responseString"+responseString);

			}

		} catch (SQLException se) {
			se.printStackTrace();
			throw new ServiceLocatorException(se);
		} finally {
			try {
				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
			} catch (SQLException se) {
				throw new ServiceLocatorException(se);
			}
		}

		return responseString;

	}

	/**
	 * New service for displaying the Leave details of the employee in popup
	 * window
	 */
	public String popuppassportDetailsWindow(NewAjaxHandlerAction ajaxHandlerAction,
			HttpServletRequest httpServletRequest) throws ServiceLocatorException {
		String passportDetails = null;

		String StartDate = null;
		String endDate = null;
		String pnumber = null;
		String i94no = null;
		String universityName = null;
		String universityContactName=null;
		String phoneNumber=null;
		String  email=null;
		String dateNotifiedTermination=null;
		String terminationDate=null;
		String sDate=null;
		String curImgStatus=null;
		DateUtility dateUtil;
		dateUtil = DateUtility.getInstance();

		try {
			connection = ConnectionProvider.getInstance().getConnection();
			preparedStatement = connection
					.prepareStatement("SELECT * FROM tblEmpImmigrationV2 WHERE EmpId=" + ajaxHandlerAction.getId());
			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				
				
				
				if (resultSet.getDate("PassportStartDate") != null
						&& !"".equals(resultSet.getDate("PassportStartDate"))) {

					StartDate = dateUtil.convertDateToView(resultSet.getDate("PassportStartDate"));
				} else {
					StartDate = "";
				}

				if (resultSet.getDate("PassportEndDate") != null && !"".equals(resultSet.getDate("PassportEndDate"))) {
					endDate = dateUtil.convertDateToView(resultSet.getDate("PassportEndDate"));
				} else {
					endDate = "";
				}

				if (resultSet.getString("PassportNumber") != null
						&& !"".equals(resultSet.getString("PassportNumber"))) {

					pnumber = resultSet.getString("PassportNumber");
				} else {
					pnumber = "";
				}

				if (resultSet.getString("I94Number") != null && !"".equals(resultSet.getString("I94Number"))) {

					i94no = resultSet.getString("I94Number");
				} else {

					i94no = "";
				}

				if (resultSet.getString("UniversityName") != null
						&& !"".equals(resultSet.getString("UniversityName"))) {

					universityName = resultSet.getString("UniversityName");
				} else {

					universityName = "";
				}

				

				if (resultSet.getString("PhoneNumber") != null
						&& !"".equals(resultSet.getString("PhoneNumber"))) {

					phoneNumber = resultSet.getString("PhoneNumber");
				} else {

					phoneNumber = "";
				}
				
				
				if (resultSet.getString("Email") != null
						&& !"".equals(resultSet.getString("Email"))) {

					email = resultSet.getString("Email");
				} else {

					email = "";
				}
				
				
				
				if (resultSet.getDate("DateNotifiedTermination") != null && !"".equals(resultSet.getDate("DateNotifiedTermination"))) {
					dateNotifiedTermination = dateUtil.convertDateToView(resultSet.getDate("DateNotifiedTermination"));
				} else {
					dateNotifiedTermination = "";
				}
				

				if (resultSet.getDate("TerminationDate") != null && !"".equals(resultSet.getDate("TerminationDate"))) {
					terminationDate = dateUtil.convertDateToView(resultSet.getDate("TerminationDate"));
				} else {
					terminationDate = "";
				}
				
				
				if (resultSet.getDate("StartDateEmployee") != null && !"".equals(resultSet.getDate("StartDateEmployee"))) {
					sDate = dateUtil.convertDateToView(resultSet.getDate("StartDateEmployee"));
				} else {
					sDate = "";
				}
				
				
				curImgStatus= resultSet.getString("ImmigrationStatus");
			}
			
			

			passportDetails = StartDate + "^" + endDate + "^" + pnumber + "^" + i94no + "^" + universityName + "^" + phoneNumber + "^" + email + "^" + dateNotifiedTermination + "^" + sDate + "^" + terminationDate+ "^" + curImgStatus;

		} catch (SQLException ex) {
			ex.printStackTrace();
		} catch (ServiceLocatorException sle) {
			sle.printStackTrace();
		} finally {
			try {

				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
		}

		// leavDetails=reportsTo+"^"+leaveType+"^"+reason+"^"+status;
		return passportDetails;
	}


	/*NagaLakshmi Telluri
	 * 10/24/2019
	 */
	
	public String addMSADatesToCustomer(NewAjaxHandlerAction ajaxHandlerAction,
			HttpServletRequest httpServletRequest)
			throws ServiceLocatorException {
		Connection connection = null;
		String responseText="";
		PreparedStatement preparedStatement = null;
		int i = 0;
		try {
			connection = ConnectionProvider.getInstance().getConnection();

			preparedStatement = connection.prepareStatement(
					"UPDATE tblCrmAccount SET MSASignedDate = ?,MSAExpiryDate=?,MSAModifiedDate=NOW(),MSAModifiedBy =?  WHERE Id = ?");
			
			  if(ajaxHandlerAction.getMsaStartDate()!=null && !"".equals(ajaxHandlerAction.getMsaStartDate())){
				
				
				preparedStatement.setDate(1,DateUtility.getInstance().getMysqlDate(ajaxHandlerAction.getMsaStartDate()));
				}else{
					preparedStatement.setDate(1,null);
				}
			  
			  
			
			  if(ajaxHandlerAction.getMsaExpiryDate()!=null && !"".equals(ajaxHandlerAction.getMsaExpiryDate())){
					
					
					preparedStatement.setDate(2,DateUtility.getInstance().getMysqlDate(ajaxHandlerAction.getMsaExpiryDate()));
					}else{
						preparedStatement.setDate(2,null);
					}
			
			
			
		
			preparedStatement.setString(3, ajaxHandlerAction.getModifiedBy());
			
			preparedStatement.setInt(4, ajaxHandlerAction.getAccountId());
			
			
			i = preparedStatement.executeUpdate();
			
			if(i>0)
			{
				
				responseText ="1";
				
			}else
			{
				
				responseText ="0";
			}
			
			System.out.println("responseText"+responseText);
			
		} catch (Exception e) {
			System.err.println("Exception is-->" + e.getMessage());
		} finally {
			try {
				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (Exception sqle) {
				System.err.println("SQL Exception is-->" + sqle.getMessage());
			}
		}
		return responseText;

	}	
	
	
	public String searchMSACustomerProjectsAjaxList(NewAjaxHandlerAction ajaxHandlerAction,
			HttpServletRequest httpServletRequest)
			throws ServiceLocatorException {
		
		Connection connection = null;
		String totalStream = "";
		Statement statement = null;
		ResultSet resultSet = null;
		// String query = "";
		String MSASignedDate="";
		String MSAExpiryDate="";
         int i=1;
 
		queryString = "SELECT tblCrmAccount.Id AS Id, tblCrmAccount.NAME,MSASignedDate,MSAExpiryDate FROM  tblCrmAccount WHERE  tblCrmAccount.NAME != '.' AND tblCrmAccount.NAME != '-'  AND tblCrmAccount.NAME != ''  "; 
								

		/*
		* if (ajaxHandlerAction.getCustomerName() != null &&
		* !"".equalsIgnoreCase(ajaxHandlerAction.getCustomerName())) { queryString =
		* queryString + " where tblCrmAccount.NAME like '%" +
		* ajaxHandlerAction.getCustomerName() + "%'"; }
		*/

		if (ajaxHandlerAction.getAccId() != 0) {
		queryString = queryString + " AND tblCrmAccount.Id = '" + ajaxHandlerAction.getAccId() + "'";
		}

		
		
		
		if (ajaxHandlerAction.getMsafrmStartDate() != null && !"".equals(ajaxHandlerAction.getMsafrmStartDate())) {

			queryString = queryString + " AND DATE(tblCrmAccount.MSASignedDate) >='"
			+ DateUtility.getInstance().convertStringToMySQLDate(ajaxHandlerAction.getMsafrmStartDate()) + "'";
			}

		
		
		if (ajaxHandlerAction.getMsatoStartDate() != null && !"".equals(ajaxHandlerAction.getMsatoStartDate())) {

			queryString = queryString + " AND DATE(tblCrmAccount.MSASignedDate) <='"
			+ DateUtility.getInstance().convertStringToMySQLDate(ajaxHandlerAction.getMsatoStartDate()) + "'";
			}
		
		
			if (ajaxHandlerAction.getMsafrmExpiryDate() != null && !"".equals(ajaxHandlerAction.getMsafrmExpiryDate())) {
				queryString = queryString + " AND DATE(tblCrmAccount.MSAExpiryDate) >='"
			+ DateUtility.getInstance().convertStringToMySQLDate(ajaxHandlerAction.getMsafrmExpiryDate()) + "'";

			} 
			
			
			if (ajaxHandlerAction.getMsatoExpiryDate() != null && !"".equals(ajaxHandlerAction.getMsatoExpiryDate())) {
				queryString = queryString + " AND DATE(tblCrmAccount.MSAExpiryDate) <='"
			+ DateUtility.getInstance().convertStringToMySQLDate(ajaxHandlerAction.getMsatoExpiryDate()) + "'";

			} 

		queryString = queryString + " GROUP BY Id  ORDER BY tblCrmAccount.NAME LIMIT 100";
		// System.out.println("query..."+queryString);
		try {

		connection = ConnectionProvider.getInstance().getConnection();
		statement = connection.createStatement();
		resultSet = statement.executeQuery(queryString);
		while (resultSet.next()) {
			
		String Name = resultSet.getString("NAME");
		
		int  Id = resultSet.getInt("Id");
		
		
		
		if(resultSet.getString("MSASignedDate") !=null && !"".equals(resultSet.getString("MSASignedDate")))
		{
		
	 MSASignedDate =DateUtility.getInstance().convertToviewFormat(resultSet.getString("MSASignedDate"));
		}
		else
		{
			MSASignedDate="-";
		}
		
		if(resultSet.getString("MSAExpiryDate") !=null && !"".equals(resultSet.getString("MSAExpiryDate")))
		{
			
	 MSAExpiryDate = DateUtility.getInstance().convertToviewFormat(resultSet.getString("MSAExpiryDate"));
		}else
		{
			MSAExpiryDate="-";
		}

		/*String ProjectCount = resultSet.getString("COUNT(ProjectName)");
		int AccountId = resultSet.getInt("Id");*/

		totalStream = totalStream + "#^$" + i + "#^$" + Name + "#^$" + MSASignedDate + "#^$" + MSAExpiryDate +  "#^$"+ Id + "*@!";

		i++;
		}

		} catch (Exception sqe) {
		sqe.printStackTrace();
		} finally {
		try {
		if (resultSet != null) {
		resultSet.close();
		resultSet = null;
		}
		if (statement != null) {
		statement.close();
		statement = null;
		}
		if (connection != null) {
		connection.close();
		connection = null;
		}
		} catch (SQLException sqle) {
		}
		}
		return totalStream;
		
		
		
		
				
	}
	
	
}
