/*******************************************************************************
 *
 * Project : Mirage V2
 *
 * Package :
 *
 * Date    :  October 9, 2007, 3:11 PM
 *
 * Author  : MrutyumjayaRao Chennu<mchennu@miraclesoft.com>
 *
 * File    : AttachmentServiceImpl.java
 *
 * Copyright 2007 Miracle Software Systems, Inc. All rights reserved.
 * MIRACLE SOFTWARE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * *****************************************************************************
 */

package com.mss.mirage.crm.attachments;

import com.mss.mirage.util.ApplicationConstants;
import com.mss.mirage.util.ConnectionProvider;
import com.mss.mirage.util.HibernateServiceLocator;
import com.mss.mirage.util.Properties;
import com.mss.mirage.util.ServiceLocatorException;
import java.util.Date;
import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Iterator;
import java.util.List;

import org.apache.jasper.tagplugins.jstl.core.ForEach;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author MrutyumjayaRao Chennu<mchennu@miraclesoft.com>
 *
 * This Class.... ENTER THE DESCRIPTION HERE
 */
public class AttachmentServiceImpl implements AttachmentService {
    
    
    /** Creates a new instance of AttachmentServiceImpl */
    public AttachmentServiceImpl() {
    }
    
    public boolean insertAttachment(AttachmentAction attachmentPojo) throws ServiceLocatorException {
        boolean isInserted = false;
        Connection connection = null;
       String query="";
        PreparedStatement preparedStatement = null;
        try {
            
           /* Session session = HibernateServiceLocator.getInstance().getSession();
            Transaction transaction = session.beginTransaction();
            transaction.begin();
            Query qry = session.createQuery("insert into AttachmentAction(objectId,objectType,attachmentName,attachmentLocation,attachmentFileName,dateUploaded)  values(?,?,?,?,?,?)");
            qry.setParameter(0,attachmentPojo.getObjectId());
            qry.setParameter(1,attachmentPojo.getObjectType());
            qry.setParameter(2,attachmentPojo.getAttachmentName());
            qry.setParameter(3,attachmentPojo.getAttachmentLocation());
            qry.setParameter(4,attachmentPojo.getAttachmentFileName());
            qry.setParameter(5,attachmentPojo.getDateUploaded());
            int res = qry.executeUpdate();
          //  session.save(attachmentPojo);
            session.flush();
            transaction.commit();
            isInserted = true;
            
            session.close(); */
        	query="insert into tblCrmAttachments(objectId,objectType,attachmentName,attachmentLocation,attachmentFileName,dateUploaded)  values(?,?,?,?,?,?)";
        	 connection = ConnectionProvider.getInstance().getConnection();
        	 preparedStatement=connection.prepareStatement(query);
        	 preparedStatement.setInt(1,attachmentPojo.getObjectId());
        	 preparedStatement.setString(2,attachmentPojo.getObjectType());
             preparedStatement.setString(3,attachmentPojo.getAttachmentName());
             preparedStatement.setString(4,attachmentPojo.getAttachmentLocation());
             preparedStatement.setString(5,attachmentPojo.getAttachmentFileName());
             preparedStatement.setTimestamp(6,attachmentPojo.getDateUploaded());
             preparedStatement.executeUpdate();
             isInserted = true;
        } catch (Exception ex) {
            
            throw new ServiceLocatorException(ex);
        }finally {
            try{
               
                
                if(preparedStatement!=null){
                    preparedStatement.close();
                    preparedStatement = null;
                }
                if(connection != null){
                    connection.close();
                    connection = null;
                }
            }catch (Exception ex) {
                throw new ServiceLocatorException(ex);
            }
            
        }
        return isInserted;
        
    }
    
    public String generatePath(String contextPath, String objectType) throws ServiceLocatorException {
        
        Date date = new Date();
        String monthName = null;
        String weekName = null;
        
        /*The path which is created in the drive and used as a home directroy*/
        String generatedPath = contextPath;
        
        
        if(date.getMonth()==0) monthName="January";
        else if(date.getMonth()==1) monthName="February";
        else if(date.getMonth()==2) monthName="March";
        else if(date.getMonth()==3) monthName="April";
        else if(date.getMonth()==4) monthName="May";
        else if(date.getMonth()==5) monthName="June";
        else if(date.getMonth()==6) monthName="July";
        else if(date.getMonth()==7) monthName="August";
        else if(date.getMonth()==8) monthName="September";
        else if(date.getMonth()==9) monthName="October";
        else if(date.getMonth()==10) monthName="November";
        else if(date.getMonth()==11) monthName="December";
        short week = (short)(Math.round(date.getDate()/7));
        
        if(week == 0) weekName="WeekFirst";
        else if(week == 1) weekName="WeekSecond";
        else if(week == 2) weekName="WeekThird";
        else if(week == 3) weekName="WeekFourth";
        else if(week == 4) weekName="WeekFifth";
        
        /*getrequestType is used to create a directory of the object type specified in the jsp page*/
//        createPath=new File(createPath.getAbsolutePath()+"//MirageV2//"+getRequestType()+"//"+String.valueOf(date.getYear()+1900)+"//"+monthName+"//"+weekName);
        generatedPath = generatedPath+"/MirageV2/"+objectType+"/"
                +String.valueOf(date.getYear()+1900)
                +"/"+monthName+"/"+weekName;
        return generatedPath;
    }
    
    public boolean deleteAttachment(AttachmentAction attachmentPojo) throws ServiceLocatorException {
        boolean isDeleted = false;
        try {
            
            Session session = HibernateServiceLocator.getInstance().getSession();
            Transaction transaction = session.beginTransaction();
            transaction.begin();
            session.delete(attachmentPojo);
            session.flush();
            transaction.commit();
            isDeleted = true;
            session.close();
            
        } catch (Exception ex) {
            
            throw new ServiceLocatorException(ex);
        }
        return isDeleted;
    }
    
 public String getAttachmentLocation(int attachmentId) throws ServiceLocatorException {
        
        Session session = HibernateServiceLocator.getInstance().getSession();
        Transaction transaction = session.beginTransaction();
        String attachmentLocation = null;
        String SQL_QUERY ="Select h.attachmentLocation from AttachmentAction as h where h.id=:attachmentId";
        
        Query query = session.createQuery(SQL_QUERY).setInteger("attachmentId",attachmentId);
        for(Iterator it=query.iterate();it.hasNext();){
            attachmentLocation = (String) it.next();
        }//end of the for loop
        
        if(session!=null){
                    try{
                        session.close();
                        session = null;
                    }catch(HibernateException he){
                        he.printStackTrace();
                    }
                }
        
        return attachmentLocation;
    }
    
}
