/*******************************************************************************
 *
 * Project : Mirage V2
 *
 * Package : com.mss.mirage.immigration
 *
 * Date    :  October 9, 2007, 3:52 PM
 *
 * Author  :Jyothi Nimmagadda<jnimmagadda@miraclesoft.com>
 *
 * File    :ImmigrationServiceImpl.java
 *
 * Copyright 2007 Miracle Software Systems, Inc. All rights reserved.
 * MIRACLE SOFTWARE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * *****************************************************************************
 */

package com.mss.mirage.employee.immigration;

import com.mss.mirage.util.HibernateServiceLocator;
import com.mss.mirage.util.ServiceLocatorException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import com.mss.mirage.util.ConnectionProvider;
import com.mss.mirage.util.DateUtility;

/**
 * The <code>ImmigrationServiceImpl</code> class is used for getting new Note
 * details and overridding the methods from the
 * <i>EmployeeTravelService.class</i>
 * <p>
 * Then it overrides methods in <code>ImmigrationService</code> class and
 * performs doImmigrationEdit() method and doImmigrationAdd() in
 * <code>immigrationAction</code> for inserting employee details in
 * TBLEMPIMMIGRATION table.
 *
 * @author Jyothi Nimmagadda <a href=
 *         "mailto:jnimmagadda@miraclesoft.com">jnimmagadda@miraclesoft.com</a>
 *
 * @version 1.0 November 01, 2007
 *
 * @see com.mss.mirage.immigration.ImmigrationAction
 * @see com.mss.mirage.immigration.ImmigrationService
 * @see com.mss.mirage.immigration.ImmigrationServiceImpl
 * @see com.mss.mirage.immigration.ImmigrationVTO
 */
public class ImmigrationServiceImpl implements ImmigrationService {

	/** Creates a new instance of ImmigrationServiceImpl */
	public ImmigrationServiceImpl() {
	}

	/**
	 * The addImmigration() is used for insert the data from the Immigration.jsp
	 * page into the databasetable.
	 * 
	 * @return The name of the string or resultType by this abstract pathname,
	 *         or the empty string if this pathname's name sequence is empty
	 */
	public boolean addImmigration(ImmigrationAction immigrationAction) throws ServiceLocatorException {
		boolean isInserted = false;
		// Generating query to inserting activity details into tblCrmActivity
		String queryString = "";
		Session hibernateSession = null;
		Transaction transaction = null;
		// travelVTO = new TravelVTO();
		try {
			hibernateSession = HibernateServiceLocator.getInstance().getSession();

			transaction = hibernateSession.beginTransaction();

			/* Save Pojo object into Session Object */
			hibernateSession.save(immigrationAction);

			/* Pushing Pojo Object into DataBase */
			hibernateSession.flush();

			/* Commit a Tranasaction */
			transaction.commit();

			isInserted = true;
		} catch (Exception e) {
			isInserted = false;
			throw new ServiceLocatorException(e);
		} finally {
			hibernateSession.close();
		}
		return isInserted;
	}

	/**
	 * The editImmigration() is used for update the data from the
	 * Immigration.jsp page and save update data into the databasetable.
	 * 
	 * @return The name of the string or resultType by this abstract pathname,
	 *         or the empty string if this pathname's name sequence is empty
	 */
	public boolean editImmigration(ImmigrationAction immigrationPojo) throws ServiceLocatorException {
		Session hibernateSession = null;
		Transaction transaction = null;
		boolean isUpdated = false;
		try {

			hibernateSession = HibernateServiceLocator.getInstance().getSession();

			transaction = hibernateSession.beginTransaction();

			/* updating pojo Object */
			hibernateSession.update(immigrationPojo);

			/* Pushing Pojo Object into DataBase */

			hibernateSession.flush();

			/* Commit a Tranasaction */
			transaction.commit();
			isUpdated = true;
		} catch (Exception e) {
			isUpdated = false;
			throw new ServiceLocatorException(e);
		} finally {
			/* Closing Session */
			hibernateSession.close();
		}
		return isUpdated;
	}

	/**
	 * The getImmigrationVTO() is used for display the values from the database
	 * in corresponding jsp as Immigration.jsp page.
	 * 
	 * @return The name of the reference value of ImmigrationVTO by this
	 *         abstract pathname, or the empty string if this pathname's name
	 *         sequence is empty
	 */
	public ImmigrationVTO getImmigrationVTO(ImmigrationAction immigrationAction) throws ServiceLocatorException {

		ImmigrationVTO immigrationVTO = new ImmigrationVTO();
		immigrationVTO.setEmpId(immigrationAction.getEmpId());
		immigrationVTO.setCurrentImmigStatus(immigrationAction.getCurrentImmigStatus());
		immigrationVTO.setPassportNo(immigrationAction.getPassportNo());
		immigrationVTO.setPassportIssuedAt(immigrationAction.getPassportIssuedAt());
		immigrationVTO.setPassportIssue(immigrationAction.getPassportIssue());
		immigrationVTO.setPassportExp(immigrationAction.getPassportExp());
		immigrationVTO.setPassportCountry(immigrationAction.getPassportCountry());

		immigrationVTO.setDateOfArrival(immigrationAction.getDateOfArrival());
		immigrationVTO.setPortOfEntry(immigrationAction.getPortOfEntry());
		immigrationVTO.setI94No(immigrationAction.getI94No());
		immigrationVTO.setPtUniversity(immigrationAction.getPtUniversity());
		immigrationVTO.setPtStart(immigrationAction.getPtStart());
		immigrationVTO.setPtEnd(immigrationAction.getPtEnd());

		immigrationVTO.setH1Filed(immigrationAction.getH1Filed());
		immigrationVTO.setH1LinReceive(immigrationAction.getH1LinReceive());
		immigrationVTO.setH1Lin(immigrationAction.getH1Lin());
		immigrationVTO.setH1LcaEtaNo(immigrationAction.getH1LcaEtaNo());
		immigrationVTO.setH1Title(immigrationAction.getH1Title());
		immigrationVTO.setH1Start(immigrationAction.getH1Start());
		immigrationVTO.setH1Termination(immigrationAction.getH1Termination());
		immigrationVTO.setH1End(immigrationAction.getH1End());
		immigrationVTO.setH1Status(immigrationAction.getH1Status());
		immigrationVTO.setQueryReceived(immigrationAction.getQueryReceived());
		immigrationVTO.setQueryDue(immigrationAction.getQueryDue());
		immigrationVTO.setQueryResponded(immigrationAction.getQueryResponded());
		immigrationVTO.setQueryComments(immigrationAction.getQueryComments());

		immigrationVTO.setB1Filed(immigrationAction.getB1Filed());
		immigrationVTO.setB1Interview(immigrationAction.getB1Interview());
		immigrationVTO.setB1Start(immigrationAction.getB1Start());
		immigrationVTO.setB1End(immigrationAction.getB1End());
		immigrationVTO.setB1Status(immigrationAction.getB1Status());

		immigrationVTO.setL1Filed(immigrationAction.getL1Filed());
		immigrationVTO.setL1Interview(immigrationAction.getL1Interview());
		immigrationVTO.setL1Start(immigrationAction.getL1Start());
		immigrationVTO.setL1End(immigrationAction.getL1End());
		immigrationVTO.setL1Status(immigrationAction.getL1Status());

		immigrationVTO.setGcApplied(immigrationAction.getGcApplied());
		immigrationVTO.setGcApproved(immigrationAction.getGcApproved());
		immigrationVTO.setGcRef(immigrationAction.getGcRef());
		immigrationVTO.setGcTitle(immigrationAction.getGcTitle());
		immigrationVTO.setGcStatus(immigrationAction.getGcStatus());
		immigrationVTO.setOtherVisas(immigrationAction.getOtherVisas());
		immigrationVTO.setComments(immigrationAction.getComments());
		/*
		 * immigrationVTO.setModifiedBy(immigrationAction.getModifiedBy());
		 * immigrationVTO.setModifiedDate(immigrationAction.getModifiedDate());
		 */

		return immigrationVTO;
	}

	/**
	 * The getImmigration() is used for getting th value of the Id from the
	 * Immigration.jsp page.
	 * 
	 * @return The name of the reference value of ImmigrationVTO by this
	 *         abstract pathname, or the empty string if this pathname's name
	 *         sequence is empty
	 * @throws SQLException
	 *             If a required system property value cannot be accessed.
	 */
	public ImmigrationVTO getImmigration(int empId) throws ServiceLocatorException {

		ImmigrationVTO immigrationVTO = new ImmigrationVTO();
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;
		String queryString = "select * from tblEmpImmigration where EmpId =" + empId;

		try {

			connection = com.mss.mirage.util.ConnectionProvider.getInstance().getConnection();
			statement = connection.createStatement();
			resultSet = statement.executeQuery(queryString);

			while (resultSet.next()) {
				immigrationVTO.setEmpId(resultSet.getInt("EmpId"));
				immigrationVTO.setCurrentImmigStatus(resultSet.getString("CurrentImmigStatus"));
				immigrationVTO.setPassportNo(resultSet.getString("PassportNo"));
				immigrationVTO.setPassportIssuedAt(resultSet.getString("PassportIssuedAt"));
				immigrationVTO.setPassportIssue(resultSet.getDate("PassportIssueDate"));
				immigrationVTO.setPassportExp(resultSet.getDate("PassportExpiryDate"));
				immigrationVTO.setPassportCountry(resultSet.getString("PassportCountry"));

				immigrationVTO.setDateOfArrival(resultSet.getDate("DateOfArrival"));
				immigrationVTO.setPortOfEntry(resultSet.getString("PortOfEntry"));
				immigrationVTO.setI94No(resultSet.getString("I94Number"));
				immigrationVTO.setPtUniversity(resultSet.getString("PTUniversity"));
				immigrationVTO.setPtStart(resultSet.getDate("PtStartDate"));
				immigrationVTO.setPtEnd(resultSet.getDate("PtEndDate"));

				immigrationVTO.setH1Filed(resultSet.getDate("H1FiledDate"));
				immigrationVTO.setH1LinReceive(resultSet.getDate("H1LinRcvDate"));
				immigrationVTO.setH1Lin(resultSet.getString("H1Lin"));
				immigrationVTO.setH1LcaEtaNo(resultSet.getString("H1LcaEtaNo"));
				immigrationVTO.setH1Title(resultSet.getString("H1Title"));
				immigrationVTO.setH1Start(resultSet.getDate("H1StartDate"));
				immigrationVTO.setH1Termination(resultSet.getDate("H1TerminationDate"));
				immigrationVTO.setH1End(resultSet.getDate("H1EndDate"));
				immigrationVTO.setH1Status(resultSet.getString("H1Status"));
				immigrationVTO.setQueryReceived(resultSet.getDate("QueryReceivedDate"));
				immigrationVTO.setQueryDue(resultSet.getDate("QueryDueDate"));
				immigrationVTO.setQueryResponded(resultSet.getDate("QueryResponded"));
				immigrationVTO.setQueryComments(resultSet.getString("QueryComments"));

				immigrationVTO.setB1Filed(resultSet.getDate("B1FiledDate"));
				immigrationVTO.setB1Interview(resultSet.getDate("B1InterviewDate"));
				immigrationVTO.setB1Start(resultSet.getDate("B1StartDate"));
				immigrationVTO.setB1End(resultSet.getDate("B1EndDate"));
				immigrationVTO.setB1Status(resultSet.getString("B1Status"));

				immigrationVTO.setL1Filed(resultSet.getDate("L1FiledDate"));
				immigrationVTO.setL1Interview(resultSet.getDate("L1InterviewDate"));
				immigrationVTO.setL1Start(resultSet.getDate("L1StartDate"));
				immigrationVTO.setL1End(resultSet.getDate("L1EndDate"));
				immigrationVTO.setL1Status(resultSet.getString("L1Status"));

				immigrationVTO.setGcApplied(resultSet.getDate("GcAppliedDate"));
				immigrationVTO.setGcApproved(resultSet.getDate("GcApprovedDate"));
				immigrationVTO.setGcRef(resultSet.getString("GcRef"));
				immigrationVTO.setGcTitle(resultSet.getString("GcTitle"));
				immigrationVTO.setGcStatus(resultSet.getString("GcStatus"));
				immigrationVTO.setOtherVisas(resultSet.getString("OtherVISAS"));
				immigrationVTO.setComments(resultSet.getString("Comments"));
				/*
				 * immigrationVTO.setModifiedBy(resultSet.getString("ModifiedBy"
				 * )); immigrationVTO.setModifiedDate(resultSet.getTimestamp(
				 * "ModifiedDate"));
				 */

			}

		} catch (SQLException se) {
			throw new ServiceLocatorException(se);
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (statement != null) {
					statement.close();
					statement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException se) {
				throw new ServiceLocatorException(se);
			}
		}

		return immigrationVTO;
	}

	public int checkAction(int empId) throws ServiceLocatorException {
		int count = 0;
		Connection connection = null;
		ResultSet resultSet = null;
		PreparedStatement preparedStatement = null;
		connection = ConnectionProvider.getInstance().getConnection();
		String query = "select * from tblEmpImmigration where EmpId=" + empId;
		try {
			preparedStatement = connection.prepareStatement(query);
			resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				count++;
			}

		} catch (Exception e) {
			throw new ServiceLocatorException(e);
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException ex) {
				throw new ServiceLocatorException(ex);
			}
		}

		return count;
	}

	/*
	 * NagaLakshmi Telluri 10/11/2019
	 */

	public int addImmigrationNew(ImmigrationAction immigrationPojo) throws ServiceLocatorException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		String queryString = "";
		int updatedRows = 0;

		try {
			connection = ConnectionProvider.getInstance().getConnection();
			queryString = "insert into tblEmpImmigrationV2(EmpId,ImmigrationStatus,StartDate,DateExpire,SevisNumber,EadNumber,ApplicationNumber,CreatedDate,CreatedBy,PassportStartDate,PassportEndDate,PassportNumber,I94Number,UniversityName,PhoneNumber,Email,DateNotifiedTermination,TerminationDate,StartDateEmployee) values(?,?,?,?,?,?,?,NOW(),?,?,?,?,?,?,?,?,?,?,?)";

			preparedStatement = connection.prepareStatement(queryString);
			preparedStatement.setInt(1, immigrationPojo.getEmpId());

			preparedStatement.setString(2, immigrationPojo.getCurImgStatus());

			if ("F1-OPT".equalsIgnoreCase(immigrationPojo.getCurImgStatus())
					|| "F1-CPT".equalsIgnoreCase(immigrationPojo.getCurImgStatus())) {

				if (immigrationPojo.getStartDate() != null && !"".equals(immigrationPojo.getStartDate())) {
					preparedStatement.setString(3,
							DateUtility.getInstance().convertStringToMySQLDate(immigrationPojo.getStartDate()));
				} else {
					preparedStatement.setString(3, null);
				}

				if (immigrationPojo.getStatusExpire() != null && !"".equals(immigrationPojo.getStatusExpire())) {
					preparedStatement.setString(4,
							DateUtility.getInstance().convertStringToMySQLDate(immigrationPojo.getStatusExpire()));
				} else {
					preparedStatement.setString(4, null);
				}

				preparedStatement.setString(5, immigrationPojo.getSevisNumber());
				preparedStatement.setString(6, immigrationPojo.getEadNumber());
				preparedStatement.setString(7, "");

			}

			else if ("USCitizen".equalsIgnoreCase(immigrationPojo.getCurImgStatus())) {
				preparedStatement.setString(3, null);
				preparedStatement.setString(4, null);
				preparedStatement.setString(5, "");
				preparedStatement.setString(6, "");
				preparedStatement.setString(7, "");
			}

			else {
				if (immigrationPojo.getStartDate() != null && !"".equals(immigrationPojo.getStartDate())) {
					preparedStatement.setString(3,
							DateUtility.getInstance().convertStringToMySQLDate(immigrationPojo.getStartDate()));
				} else {
					preparedStatement.setString(3, null);
				}

				if (immigrationPojo.getStatusExpire() != null && !"".equals(immigrationPojo.getStatusExpire())) {
					preparedStatement.setString(4,
							DateUtility.getInstance().convertStringToMySQLDate(immigrationPojo.getStatusExpire()));
				} else {
					preparedStatement.setString(4, null);
				}

				preparedStatement.setString(5, "");
				preparedStatement.setString(6, "");
				preparedStatement.setString(7, immigrationPojo.getReceiptNumber());
			}

			preparedStatement.setString(8, immigrationPojo.getCreatedBy());

			if (immigrationPojo.getPtstartDate() != null && !"".equals(immigrationPojo.getPtstartDate())) {
				preparedStatement.setString(9,
						DateUtility.getInstance().convertStringToMySQLDate(immigrationPojo.getPtstartDate()));
			} else {
				preparedStatement.setString(9, null);
			}

			if (immigrationPojo.getPstatusExpire() != null && !"".equals(immigrationPojo.getPstatusExpire())) {
				preparedStatement.setString(10,
						DateUtility.getInstance().convertStringToMySQLDate(immigrationPojo.getPstatusExpire()));
			} else {
				preparedStatement.setString(10, null);
			}

			preparedStatement.setString(11, immigrationPojo.getPassportNumber());

			if ("USCitizen".equalsIgnoreCase(immigrationPojo.getCurImgStatus())
					&& "GreenCard".equalsIgnoreCase(immigrationPojo.getCurImgStatus())) {
				preparedStatement.setString(12, "");
			}
			else
			{
				preparedStatement.setString(12, immigrationPojo.getI94Number());
			}

			
			
			preparedStatement.setString(13, immigrationPojo.getUniName());
			
		
			
			preparedStatement.setString(14, immigrationPojo.getPhnoNumberImg());
			
			
			preparedStatement.setString(15, immigrationPojo.getEmailImg());
			
			
			

			if (immigrationPojo.getDateNotified() != null && !"".equals(immigrationPojo.getDateNotified())) {
				preparedStatement.setString(16,
						DateUtility.getInstance().convertStringToMySQLDate(immigrationPojo.getDateNotified()));
			} else {
				preparedStatement.setString(16, null);
			}

			if (immigrationPojo.getTerminationDateimg() != null && !"".equals(immigrationPojo.getTerminationDateimg())) {
				preparedStatement.setString(17,
						DateUtility.getInstance().convertStringToMySQLDate(immigrationPojo.getTerminationDateimg()));
			} else {
				preparedStatement.setString(17, null);
			}
			
			
			if (immigrationPojo.getsDateimg() != null && !"".equals(immigrationPojo.getsDateimg())) {
				preparedStatement.setString(18,
						DateUtility.getInstance().convertStringToMySQLDate(immigrationPojo.getsDateimg()));
			} else {
				preparedStatement.setString(18, null);
			}
			
			
			
			
			updatedRows = preparedStatement.executeUpdate();
		} catch (Exception e) {
			System.err.println("Exception is-->" + e.getMessage());
		} finally {
			try {
				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (Exception sqle) {
				System.err.println("SQL Exception is-->" + sqle.getMessage());
			}
		}
		return updatedRows;
	}

	public int editImmigrationNew(ImmigrationAction immigrationPojo) throws ServiceLocatorException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		int i = 0;
		try {
			connection = ConnectionProvider.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(
					"UPDATE tblEmpImmigrationV2 SET ImmigrationStatus=?,StartDate=?,DateExpire=?,SevisNumber=?,EadNumber=?,ApplicationNumber=?,ModifiedDate=NOW(),ModifiedBy=?,PassportStartDate=?,PassportEndDate=?,PassportNumber=?,I94Number=?,UniversityName=?,PhoneNumber=?,Email=?,DateNotifiedTermination=?,TerminationDate=?,StartDateEmployee=?  WHERE EmpId="
							+ immigrationPojo.getEmpId());
			preparedStatement.setString(1, immigrationPojo.getCurImgStatus());

			if ("F1-OPT".equalsIgnoreCase(immigrationPojo.getCurImgStatus())
					|| "F1-CPT".equalsIgnoreCase(immigrationPojo.getCurImgStatus())) {

				if (immigrationPojo.getStartDate() != null && !"".equals(immigrationPojo.getStartDate())) {
					preparedStatement.setString(2,
							DateUtility.getInstance().convertStringToMySQLDate(immigrationPojo.getStartDate()));
				} else {
					preparedStatement.setString(2, null);
				}

				if (immigrationPojo.getStatusExpire() != null && !"".equals(immigrationPojo.getStatusExpire())) {
					preparedStatement.setString(3,
							DateUtility.getInstance().convertStringToMySQLDate(immigrationPojo.getStatusExpire()));
				} else {
					preparedStatement.setString(3, null);
				}

				preparedStatement.setString(4, immigrationPojo.getSevisNumber());
				preparedStatement.setString(5, immigrationPojo.getEadNumber());
				preparedStatement.setString(6, "");

			}

			else if ("USCitizen".equalsIgnoreCase(immigrationPojo.getCurImgStatus())) {
				preparedStatement.setString(2, null);
				preparedStatement.setString(3, null);
				preparedStatement.setString(4, "");
				preparedStatement.setString(5, "");
				preparedStatement.setString(6, "");
			}

			else {
				if (immigrationPojo.getStartDate() != null && !"".equals(immigrationPojo.getStartDate())) {
					preparedStatement.setString(2,
							DateUtility.getInstance().convertStringToMySQLDate(immigrationPojo.getStartDate()));
				} else {
					preparedStatement.setString(2, null);
				}

				if (immigrationPojo.getStatusExpire() != null && !"".equals(immigrationPojo.getStatusExpire())) {
					preparedStatement.setString(3,
							DateUtility.getInstance().convertStringToMySQLDate(immigrationPojo.getStatusExpire()));
				} else {
					preparedStatement.setString(3, null);
				}

				preparedStatement.setString(4, "");
				preparedStatement.setString(5, "");
				preparedStatement.setString(6, immigrationPojo.getReceiptNumber());
			}

			preparedStatement.setString(7, immigrationPojo.getModifiedBy());

			if (immigrationPojo.getPtstartDate() != null && !"".equals(immigrationPojo.getPtstartDate())) {
				preparedStatement.setString(8,
						DateUtility.getInstance().convertStringToMySQLDate(immigrationPojo.getPtstartDate()));
			} else {
				preparedStatement.setString(8, null);
			}

			if (immigrationPojo.getPstatusExpire() != null && !"".equals(immigrationPojo.getPstatusExpire())) {
				preparedStatement.setString(9,
						DateUtility.getInstance().convertStringToMySQLDate(immigrationPojo.getPstatusExpire()));
			} else {
				preparedStatement.setString(9, null);
			}

			preparedStatement.setString(10, immigrationPojo.getPassportNumber());

			if ("USCitizen".equalsIgnoreCase(immigrationPojo.getCurImgStatus())
					&& "GreenCard".equalsIgnoreCase(immigrationPojo.getCurImgStatus())) {
				preparedStatement.setString(11, "");
			}
			else
			{
				preparedStatement.setString(11, immigrationPojo.getI94Number());
			}
			
			
            preparedStatement.setString(12, immigrationPojo.getUniName());
			
		
			
			preparedStatement.setString(13, immigrationPojo.getPhnoNumberImg());
			
			
			preparedStatement.setString(14, immigrationPojo.getEmailImg());
			
			
			

			if (immigrationPojo.getDateNotified() != null && !"".equals(immigrationPojo.getDateNotified())) {
				preparedStatement.setString(15,
						DateUtility.getInstance().convertStringToMySQLDate(immigrationPojo.getDateNotified()));
			} else {
				preparedStatement.setString(15, null);
			}

			if (immigrationPojo.getTerminationDateimg() != null && !"".equals(immigrationPojo.getTerminationDateimg())) {
				preparedStatement.setString(16,
						DateUtility.getInstance().convertStringToMySQLDate(immigrationPojo.getTerminationDateimg()));
			} else {
				preparedStatement.setString(16, null);
			}
			
			
			if (immigrationPojo.getsDateimg() != null && !"".equals(immigrationPojo.getsDateimg())) {
				preparedStatement.setString(17,
						DateUtility.getInstance().convertStringToMySQLDate(immigrationPojo.getsDateimg()));
			} else {
				preparedStatement.setString(17, null);
			}
			

			i = preparedStatement.executeUpdate();
		} catch (Exception e) {
			System.err.println("Exception is-->" + e.getMessage());
		} finally {
			try {
				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (Exception sqle) {
				System.err.println("SQL Exception is-->" + sqle.getMessage());
			}

		}
		return i;
	}

	public int checkActionNew(int empId) throws ServiceLocatorException {
		int count = 0;
		Connection connection = null;
		ResultSet resultSet = null;
		PreparedStatement preparedStatement = null;
		connection = ConnectionProvider.getInstance().getConnection();
		String query = "select * from tblEmpImmigrationV2 where EmpId=" + empId;
		try {
			preparedStatement = connection.prepareStatement(query);
			resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				count++;
			}

		} catch (Exception e) {
			throw new ServiceLocatorException(e);
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException ex) {
				throw new ServiceLocatorException(ex);
			}
		}

		return count;
	}

	public ImmigrationVTO getImmigrationNew(int empId) throws ServiceLocatorException {

		ImmigrationVTO immigrationVTO = new ImmigrationVTO();
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;
		String queryString = "select * from tblEmpImmigrationV2 where EmpId =" + empId;

		try {

			connection = com.mss.mirage.util.ConnectionProvider.getInstance().getConnection();
			statement = connection.createStatement();
			resultSet = statement.executeQuery(queryString);

			while (resultSet.next()) {
				immigrationVTO.setEmpId(resultSet.getInt("EmpId"));
				immigrationVTO.setCurImgStatus(resultSet.getString("ImmigrationStatus"));

				if (resultSet.getDate("StartDate") != null && !"".equals(resultSet.getDate("StartDate"))) {

					immigrationVTO
							.setStartDate(DateUtility.getInstance().convertDateToView(resultSet.getDate("StartDate")));
				} else {
					immigrationVTO.setStartDate("");
				}

				if (resultSet.getDate("DateExpire") != null && !"".equals(resultSet.getDate("DateExpire"))) {
					immigrationVTO.setStatusExpire(
							DateUtility.getInstance().convertDateToView(resultSet.getDate("DateExpire")));
				} else {
					immigrationVTO.setStatusExpire("");
				}
				immigrationVTO.setSevisNumber(resultSet.getString("SevisNumber"));
				immigrationVTO.setEadNumber(resultSet.getString("EadNumber"));
				immigrationVTO.setReceiptNumber(resultSet.getString("ApplicationNumber"));
				
				if (resultSet.getDate("PassportStartDate") != null && !"".equals(resultSet.getDate("PassportStartDate"))) {

					immigrationVTO
							.setPtstartDate(DateUtility.getInstance().convertDateToView(resultSet.getDate("PassportStartDate")));
				} else {
					immigrationVTO.setPtstartDate("");
				}
				if (resultSet.getDate("PassportEndDate") != null && !"".equals(resultSet.getDate("PassportEndDate"))) {

					immigrationVTO.setPstatusExpire((DateUtility.getInstance().convertDateToView(resultSet.getDate("PassportEndDate"))));
							
				} else {
					immigrationVTO.setPstatusExpire("");
				}
			
				immigrationVTO.setPassportNumber(resultSet.getString("PassportNumber"));
				
				if("USCitizen".equalsIgnoreCase(resultSet.getString("ImmigrationStatus")) && "GreenCard".equalsIgnoreCase(resultSet.getString("ImmigrationStatus")))
				{
				immigrationVTO.setI94Number("");
				}
				else
				{
					immigrationVTO.setI94Number(resultSet.getString("I94Number"));
				}
				
				immigrationVTO.setUniName(resultSet.getString("UniversityName"));
				
				immigrationVTO.setPhnoNumberImg(resultSet.getString("PhoneNumber"));
				immigrationVTO.setEmailImg(resultSet.getString("Email"));
				
				if (resultSet.getDate("DateNotifiedTermination") != null && !"".equals(resultSet.getDate("DateNotifiedTermination"))) {

					immigrationVTO
							.setDateNotified(DateUtility.getInstance().convertDateToView(resultSet.getDate("DateNotifiedTermination")));
				} else {
					immigrationVTO.setDateNotified("");
				}
				if (resultSet.getDate("TerminationDate") != null && !"".equals(resultSet.getDate("TerminationDate"))) {

					immigrationVTO.setTerminationDateimg(DateUtility.getInstance().convertDateToView(resultSet.getDate("TerminationDate")));
							
				} else {
					immigrationVTO.setTerminationDateimg("");
				}
				
				if (resultSet.getDate("StartDateEmployee") != null && !"".equals(resultSet.getDate("StartDateEmployee"))) {

					immigrationVTO.setsDateimg(DateUtility.getInstance().convertDateToView(resultSet.getDate("StartDateEmployee")));
							
				} else {
					immigrationVTO.setsDateimg("");
				}
				
			}

		} catch (SQLException se) {
			throw new ServiceLocatorException(se);
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (statement != null) {
					statement.close();
					statement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException se) {
				throw new ServiceLocatorException(se);
			}
		}

		return immigrationVTO;
	}

}
