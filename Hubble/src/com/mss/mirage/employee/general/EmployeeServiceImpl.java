/*
 *******************************************************************************
 *
 * Project : Mirage V2
 *
 * Package : com.mss.mirage.employee.genaral
 *
 * Date    :  September 24, 2007, 10:18 PM
 *
 * Author  : Praveen Kumar Ralla<pralla@miraclesoft.com>
 *
 * File    : EmployeeServiceImpl.java
 *
 * Copyright 2007 Miracle Software Systems, Inc. All rights reserved.
 * MIRACLE SOFTWARE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * *****************************************************************************
 */

package com.mss.mirage.employee.general;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.constantcontact.ConstantContactFactory;
import com.constantcontact.components.activities.contacts.request.AddContactsRequest;
import com.constantcontact.components.activities.contacts.request.ContactData;
import com.constantcontact.components.contacts.Contact;
import com.constantcontact.components.contacts.ContactList;
import com.constantcontact.components.emailcampaigns.EmailCampaignRequest;
import com.constantcontact.components.emailcampaigns.EmailCampaignResponse;
import com.constantcontact.components.emailcampaigns.MessageFooter;
import com.constantcontact.components.emailcampaigns.SentToContactList;
import com.constantcontact.components.emailcampaigns.schedules.EmailCampaignSchedule;
import com.constantcontact.exceptions.service.ConstantContactServiceException;
import com.constantcontact.services.activities.IBulkActivitiesService;
import com.constantcontact.services.contactlists.ContactListService;
import com.constantcontact.services.emailcampaigns.EmailCampaignService;
import com.constantcontact.services.emailcampaigns.schedule.EmailCampaignScheduleService;
import com.mss.mirage.util.ApplicationConstants;
import com.mss.mirage.util.ConnectionProvider;
import com.mss.mirage.util.DataSourceDataProvider;
import com.mss.mirage.util.DateUtility;
import com.mss.mirage.util.HibernateServiceLocator;
import com.mss.mirage.util.MailManager;
import com.mss.mirage.util.PasswordUtility;
import com.mss.mirage.util.Properties;
import com.mss.mirage.util.RestRepository;
import com.mss.mirage.util.ServiceLocatorException;
/*
 * @(#)EmployeeService.java 1.0 November 01, 2007
 *
 * Copyright 2006 Miracle Software Systems(INDIA) Pvt Ltd. All rights reserved.
 *
 * @see com.mss.mirage.util.MailManager
 * @see com.mss.mirage.util.PasswordUtility
 * @see com.mss.mirage.util.ServiceLocatorException
 * @see com.mss.mirage.util.HibernateServiceLocator
 * @see com.mss.mirage.employee.general.EmployeeService
 *
 * @since 1.0
 */
public class EmployeeServiceImpl implements EmployeeService {

	/** Creates a new instance of EmployeeSeviceImpl */
	public EmployeeServiceImpl() {

	}

	/**
	 * the upadateEmployee(EmployeeAction employeeAction) is used for updating
	 * employee information.
	 * 
	 * @throws ServiceLocatorException
	 * @see com.mss.mirage.util.ServiceLocatorException
	 * @see com.mss.mirage.employee.general.EmployeeAction
	 *
	 * @return boolean variable
	 */

	public boolean updateEmployee(EmployeeAction employeeAction) throws ServiceLocatorException {

		boolean isUpdateEmployee = false;
		Session session = HibernateServiceLocator.getInstance().getSession();
		Transaction tran = session.beginTransaction();
		session.update(getEmployeeVTO(employeeAction));
		session.flush();
		tran.commit();
		isUpdateEmployee = true;
		 if(isUpdateEmployee && (!employeeAction.getPrevLiveInCountry().equals(employeeAction.getCountry()))){
			 
			 int count=DataSourceDataProvider.getInstance().doAddEmpLocationTrackingHistory(employeeAction.getId(),employeeAction.getCountry(),employeeAction.getLocation(),employeeAction.getModifiedBy(),employeeAction.getModifiedDate());
		 } 
		// System.out.println("isUpdateEmployee-------->"+isUpdateEmployee);
		session.close();
		return isUpdateEmployee;
	}

	public boolean updateEmployeeState(StateVTO stateVTO) throws ServiceLocatorException {
		boolean isUpdated = false;
		Session session = HibernateServiceLocator.getInstance().getSession();
		Transaction tran = session.beginTransaction();
		session.update(stateVTO);
		session.flush();
		tran.commit();
		isUpdated = true;
		session.close();
		return isUpdated;
	}

	/**
	 * The EmployeeVTO getEmployee(int employeeId) is used for retrieving
	 * employee Details.
	 * 
	 * @throws ServiceLocatorException
	 * @see com.mss.mirage.util.ServiceLocatorException
	 * @see com.mss.mirage.employee.general.EmployeeVTO
	 * @return EmployeeVTO variable
	 */
	public EmployeeVTO getEmployeeVTO(EmployeeAction employeePojo) throws ServiceLocatorException {

		EmployeeVTO employeeVTO = new EmployeeVTO();

		employeeVTO.setId(employeePojo.getId());
		employeeVTO.setLoginId(employeePojo.getLoginId());
		employeeVTO.setFirstName(employeePojo.getFirstName());
		employeeVTO.setLastName(employeePojo.getLastName());
		employeeVTO.setMiddleName(employeePojo.getMiddleName());
		employeeVTO.setGender(employeePojo.getGender());
		employeeVTO.setAliasName(employeePojo.getAliasName());
		employeeVTO.setMaritalStatus(employeePojo.getMaritalStatus());
		employeeVTO.setCountry(employeePojo.getCountry());
		employeeVTO.setSsn(employeePojo.getSsn());
		// if(employeePojo.getEmpno()!=null &&
		// !"0".equals(employeePojo.getEmpno()))
		if (employeePojo.getEmpno() != null && !"".equals(employeePojo.getEmpno()))
			employeeVTO.setEmpno(employeePojo.getEmpno());
		else
			employeeVTO.setEmpno("0");
		employeeVTO.setNsrno(employeePojo.getNsrno());
		employeeVTO.setCurrStatus(employeePojo.getCurrStatus());
		employeeVTO.setEmpTypeId(employeePojo.getEmpTypeId());
		employeeVTO.setOrgId(employeePojo.getOrgId());
		employeeVTO.setOpsContactId(employeePojo.getOpsContactId());
		employeeVTO.setTeamId(employeePojo.getTeamId());
		employeeVTO.setPracticeId(employeePojo.getPracticeId());
		employeeVTO.setSubPractice(employeePojo.getSubPractice());
		employeeVTO.setIsManager(employeePojo.getIsManager());
		employeeVTO.setIsTeamLead(employeePojo.isIsTeamLead());
		employeeVTO.setReportsTo(employeePojo.getReportsTo());
		// employeeVTO.setReportsTo(employeePojo.getReportsTo());
		employeeVTO.setTitleId(employeePojo.getTitleId());
		employeeVTO.setIndustryId(employeePojo.getIndustryId());
		employeeVTO.setDepartmentId(employeePojo.getDepartmentId());
		employeeVTO.setBirthDate(employeePojo.getBirthDate());
		employeeVTO.setOffBirthDate(employeePojo.getOffBirthDate());
		employeeVTO.setHireDate(employeePojo.getHireDate());
		employeeVTO.setAnniversaryDate(employeePojo.getAnniversaryDate());
		employeeVTO.setWorkPhoneNo(employeePojo.getWorkPhoneNo());
		employeeVTO.setAltPhoneNo(employeePojo.getAltPhoneNo());
		employeeVTO.setHomePhoneNo(employeePojo.getHomePhoneNo());
		employeeVTO.setCellPhoneNo(employeePojo.getCellPhoneNo());
		employeeVTO.setOfficeEmail(employeePojo.getOfficeEmail());
		employeeVTO.setHotelPhoneNo(employeePojo.getHotelPhoneNo());
		employeeVTO.setPersonalEmail(employeePojo.getPersonalEmail());
		employeeVTO.setIndiaPhoneNo(employeePojo.getIndiaPhoneNo());
		employeeVTO.setOtherEmail(employeePojo.getOtherEmail());
		employeeVTO.setFaxNo(employeePojo.getFaxNo());
		employeeVTO.setLastContactBy(employeePojo.getLastContactBy());
		employeeVTO.setModifiedBy(employeePojo.getModifiedBy());
		employeeVTO.setModifiedDate(employeePojo.getModifiedDate());
		employeeVTO.setCreatedBy(employeePojo.getCreatedBy());
		employeeVTO.setCreatedDate(employeePojo.getCreatedDate());

		employeeVTO.setEmpState(employeePojo.getEmpState());
		employeeVTO.setStateStartDate(employeePojo.getStateStartDate());
		employeeVTO.setStateEndDate(employeePojo.getStateEndDate());
		// employeeVTO.setIntRatePerHour(employeePojo.getIntRatePerHour());
		// employeeVTO.setInvRatePerHour(employeePojo.getInvRatePerHour());
		employeeVTO.setPrjName(employeePojo.getPrjName());
		// employeeVTO.setCtcPerYear(employeePojo.getCtcPerYear());
		employeeVTO.setSkillSet(employeePojo.getSkillSet());
		employeeVTO.setItgBatch(employeePojo.getItgBatch());
		employeeVTO.setIsOnsite(employeePojo.getIsOnsite());
		employeeVTO.setPreviousEmpState(employeePojo.getEmpState());
		employeeVTO.setWorkingCountry(employeePojo.getWorkingCountry());
		employeeVTO.setIsCreTeam(employeePojo.getIsCreTeam());
		employeeVTO.setLocation(employeePojo.getLocation());
		employeeVTO.setIsOperationTeam(employeePojo.getIsOperationTeam());
		employeeVTO.setIsPMO(employeePojo.getIsPMO());
		// New For ReportsTo
		employeeVTO.setReportsToFlag(employeePojo.getReportsToFlag());

		employeeVTO.setBankName(employeePojo.getBankName());
		employeeVTO.setAccNum(employeePojo.getAccNum());
		employeeVTO.setNameAsPerAcc(employeePojo.getNameAsPerAcc());
		employeeVTO.setIfscCode(employeePojo.getIfscCode());
		employeeVTO.setPhyChallenged(employeePojo.getPhyChallenged());
		employeeVTO.setPhyCategory(employeePojo.getPhyCategory());
		employeeVTO.setAadharNum(employeePojo.getAadharNum());
		employeeVTO.setAadharName(employeePojo.getAadharName());
		employeeVTO.setNameAsPerPan(employeePojo.getNameAsPerPan());

		employeeVTO.setUanNo(employeePojo.getUanNo());
		employeeVTO.setPfno(employeePojo.getPfno());
		employeeVTO.setRevisedDate(employeePojo.getRevisedDate());
		employeeVTO.setLastRevisedDate(employeePojo.getLastRevisedDate());
		employeeVTO.setIsInternationalWorker(employeePojo.getIsInternationalWorker());
		employeeVTO.setDateOfTermination(employeePojo.getDateOfTermination());
		employeeVTO.setReasonsForTerminate(employeePojo.getReasonsForTerminate());

		employeeVTO.setPrvexpMnths(employeePojo.getPrvexpMnths().replace("m", "").trim());
		employeeVTO.setPrevExp(employeePojo.getPrvexpYears() + " " + employeePojo.getPrvexpMnths());
		employeeVTO.setPrvexpYears(employeePojo.getPrvexpYears().replace("yr", "").trim());

		employeeVTO.setProjectStatus(employeePojo.getEmpStatus());

		employeeVTO.setLateralFlag(employeePojo.getLateralFlag());
		employeeVTO.setExitType(employeePojo.getExitType());

		employeeVTO.setSystemType(employeePojo.getSystemType());
		employeeVTO.setShiftTimings(employeePojo.getShiftTimings());
		employeeVTO.setWeekendsSupportFlag(employeePojo.getWeekendsSupportFlag());
		employeeVTO.setOutSideAccess(employeePojo.getOutSideAccess());
		employeeVTO.setPayRollBatchUs(employeePojo.getPayRollBatchUs());
		return employeeVTO;
	}

	public EmployeeVTO getEmployee(int employeeId, int currId) throws ServiceLocatorException {

		 //System.out.println("employeeId"+employeeId+"currId"+currId);
		EmployeeVTO employeeVTO = new EmployeeVTO();
		String queryString = "SELECT * FROM tblEmployee WHERE Id=" + employeeId;
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;
		String TABLE_EMP_STATE_HISTORY = Properties.getProperty("TABLE_EMP_STATE_HISTORY");

		try {
			connection = ConnectionProvider.getInstance().getConnection();
			statement = connection.createStatement();
			resultSet = statement.executeQuery(queryString);
			employeeVTO.setId(employeeId);
			while (resultSet.next()) {
				employeeVTO.setLoginId(resultSet.getString("LoginId"));
				employeeVTO.setFirstName(resultSet.getString("FName"));
				employeeVTO.setLastName(resultSet.getString("LName"));
				employeeVTO.setMiddleName(resultSet.getString("MName"));
				employeeVTO.setGender(resultSet.getString("Gender"));
				employeeVTO.setAliasName(resultSet.getString("AliasName"));
				employeeVTO.setMaritalStatus(resultSet.getString("MaritalStatus"));
				employeeVTO.setCountry(resultSet.getString("Country"));
				employeeVTO.setSsn(resultSet.getString("SSN"));
				if (resultSet.getString("EmpNo") != null && !"".equals(resultSet.getString("EmpNo")))
					employeeVTO.setEmpno(resultSet.getString("EmpNo"));
				else
					employeeVTO.setEmpno("0");

				employeeVTO.setNsrno(resultSet.getString("NSR"));
				employeeVTO.setCurrStatus(resultSet.getString("CurStatus"));
				employeeVTO.setEmpTypeId(resultSet.getString("EmployeeTypeId"));
				employeeVTO.setOrgId(resultSet.getString("OrgId"));
				employeeVTO.setOpsContactId(resultSet.getString("OpsContactId"));
				employeeVTO.setTeamId(resultSet.getString("TeamId"));
				employeeVTO.setPracticeId(resultSet.getString("PracticeId"));
				employeeVTO.setSubPractice(resultSet.getString("SubPractice"));
				employeeVTO.setIsManager(resultSet.getBoolean("IsManager"));
				employeeVTO.setIsTeamLead(resultSet.getBoolean("IsTeamLead"));
				employeeVTO.setReportsTo(resultSet.getString("ReportsTo"));
				// employeeVTO.setReportsToId(resultSet.getInt("ReportsToId"));
				employeeVTO.setTitleId(resultSet.getString("TitleTypeId"));
				employeeVTO.setIndustryId(resultSet.getString("IndustryId"));
				employeeVTO.setDepartmentId(resultSet.getString("DepartmentId"));
				employeeVTO.setBirthDate(resultSet.getDate("BirthDate"));
				employeeVTO.setOffBirthDate(resultSet.getDate("OffBirthDate"));
				employeeVTO.setHireDate(resultSet.getDate("HireDate"));
				employeeVTO.setAnniversaryDate(resultSet.getDate("AnniversaryDate"));
				employeeVTO.setWorkPhoneNo(resultSet.getString("WorkPhoneNo"));
				employeeVTO.setAltPhoneNo(resultSet.getString("AlterPhoneNo"));
				employeeVTO.setHomePhoneNo(resultSet.getString("HomePhoneNo"));
				employeeVTO.setCellPhoneNo(resultSet.getString("CellPhoneNo"));
				employeeVTO.setOfficeEmail(resultSet.getString("Email1"));
				employeeVTO.setHotelPhoneNo(resultSet.getString("HotelPhoneNo"));
				employeeVTO.setPersonalEmail(resultSet.getString("Email2"));
				employeeVTO.setIndiaPhoneNo(resultSet.getString("IndiaPhoneNo"));
				employeeVTO.setOtherEmail(resultSet.getString("Email3"));
				employeeVTO.setFaxNo(resultSet.getString("FaxPhoneNo"));
				employeeVTO.setLastContactBy(resultSet.getString("LastContactBy"));
				employeeVTO.setModifiedBy(resultSet.getString("ModifiedBy"));
				employeeVTO.setModifiedDate(resultSet.getTimestamp("ModifiedDate"));
				employeeVTO.setCreatedBy(resultSet.getString("CreatedBy"));
				employeeVTO.setCreatedDate(resultSet.getTimestamp("CreatedDate"));
				employeeVTO.setEmpState(resultSet.getString("State"));
				employeeVTO.setStateStartDate(resultSet.getDate("StateStartDate"));
				employeeVTO.setStateEndDate(resultSet.getDate("StateEndDate"));
				// employeeVTO.setIntRatePerHour(resultSet.getFloat("IntRatePerHour"));
				// employeeVTO.setInvRatePerHour(resultSet.getFloat("InvRatePerHour"));
				employeeVTO.setSkillSet(resultSet.getString("SkillSet"));
				employeeVTO.setPreviousEmpState(resultSet.getString("State"));
				employeeVTO.setPrjName(resultSet.getString("PrjName"));
				// employeeVTO.setCtcPerYear(resultSet.getDouble("CTC"));
				employeeVTO.setItgBatch(resultSet.getString("Itgbatch"));
				employeeVTO.setIsOnsite(resultSet.getString("WorkingOnsite"));
				employeeVTO.setWorkingCountry(resultSet.getString("WorkingCountry"));

				// System.out.println("iscreteam---->"+resultSet.getBoolean("IsCreTeam"));
				employeeVTO.setIsCreTeam(resultSet.getBoolean("IsCreTeam"));
				// System.out.println("Location---->"+resultSet.getString("Location"));
				employeeVTO.setLocation(resultSet.getString("Location"));
				employeeVTO.setIsOperationTeam(resultSet.getBoolean("IsOperationContactTeam"));
				employeeVTO.setIsPMO(resultSet.getBoolean("IsPMO"));
				// New For ReportsToFlag
				employeeVTO.setReportsToFlag(resultSet.getBoolean("reportsToFlag"));
				employeeVTO.setComments(resultSet.getString("Comments"));
				employeeVTO.setBankName(resultSet.getString("BankName"));
				employeeVTO.setAccNum(resultSet.getString("AccNum"));
				employeeVTO.setNameAsPerAcc(resultSet.getString("NameAsPerAcc"));
				employeeVTO.setIfscCode(resultSet.getString("IfscCode"));
				employeeVTO.setPhyChallenged(resultSet.getString("PhyChallenged"));
				employeeVTO.setPhyCategory(resultSet.getString("PhyCategory"));
				employeeVTO.setAadharNum(resultSet.getString("AadharNum"));
				employeeVTO.setAadharName(resultSet.getString("AadharName"));
				employeeVTO.setNameAsPerPan(resultSet.getString("NameAsPerPan"));

				employeeVTO.setUanNo(resultSet.getString("UANNo"));
				employeeVTO.setPfno(resultSet.getString("PFNo"));
				employeeVTO.setLastRevisedDate(resultSet.getDate("RevisedDate"));
				employeeVTO.setRevisedDate(resultSet.getDate("NextRevisedDate"));
				employeeVTO.setStateStartDate1(DateUtility.getInstance().getCurrentMySqlDate());
				employeeVTO.setIsInternationalWorker(resultSet.getBoolean("IsInternationalWorker"));

				employeeVTO.setDateOfTermination(resultSet.getDate("TerminationDate"));
				employeeVTO.setReasonsForTerminate(resultSet.getString("Termination"));
				employeeVTO.setExitType(resultSet.getString("ExitType"));

				// String []
				// expArray=resultSet.getString("PrevExp").toString().split("
				// ");

				if (resultSet.getString("PrevExp") != null
						&& resultSet.getString("PrevExp").toString().split(" ").length >= 1) {
					employeeVTO.setPrvexpYears(resultSet.getString("PrevExp").split(" ")[0].replace("yr", "").trim());
				}
				if (resultSet.getString("PrevExp") != null && resultSet.getString("PrevExp") != null
						&& resultSet.getString("PrevExp").toString().split(" ").length >= 2) {
					employeeVTO.setPrvexpMnths(resultSet.getString("PrevExp").split(" ")[1].replace("m", "").trim());
				}

				employeeVTO.setLateralFlag(resultSet.getBoolean("IsLateral"));

				employeeVTO.setSystemType(resultSet.getString("SystemType"));
				employeeVTO.setShiftTimings(resultSet.getString("ShiftTimings"));
				employeeVTO.setWeekendsSupportFlag(resultSet.getBoolean("IsWeekendsSupport"));
				employeeVTO.setOutSideAccess(resultSet.getBoolean("OutsideAccess"));
				employeeVTO.setPayRollBatchUs(resultSet.getString("PayrollBatchUs"));
			}

			if (currId != 0) {
				resultSet.close();
				resultSet = null;

				resultSet = statement.executeQuery("select * from " + TABLE_EMP_STATE_HISTORY + " where Id=" + currId);
				while (resultSet.next()) {
					employeeVTO.setEmpState(resultSet.getString("State"));
					employeeVTO.setStateStartDate(resultSet.getDate("StartDate"));
					employeeVTO.setStateEndDate(resultSet.getDate("EndDate"));
					// employeeVTO.setIntRatePerHour(resultSet.getFloat("IntRatePerHour"));
					// employeeVTO.setInvRatePerHour(resultSet.getFloat("InvRatePerHour"));
					employeeVTO.setSkillSet(resultSet.getString("SkillSet"));
					employeeVTO.setPrjName(resultSet.getString("PrjName"));
					// employeeVTO.setCtcPerYear(resultSet.getDouble("CTC"));
					employeeVTO.setUtilization(resultSet.getInt("Utilization"));
					employeeVTO.setComments(resultSet.getString("Comments"));
					employeeVTO.setTrainingRating(resultSet.getString("TrainingRating"));
					if (resultSet.getString("StartDate") != null && !"".equals(resultSet.getString("StartDate"))) {
						employeeVTO.setStateStartDate1(
								DateUtility.getInstance().convertToviewFormat(resultSet.getString("StartDate")));
					}
					if (resultSet.getString("EndDate") != null && !"".equals(resultSet.getString("EndDate"))) {
						employeeVTO.setStateEndDate1(
								DateUtility.getInstance().convertToviewFormat(resultSet.getString("EndDate")));
					}
					employeeVTO.setResumeName(resultSet.getString("ResumeName"));
					employeeVTO.setResumePath(resultSet.getString("ResumePath"));

					employeeVTO.setProjectContactId(resultSet.getInt("ProjectContactsId"));
					employeeVTO.setProjectStatus(resultSet.getString("ProjectStatus"));
					employeeVTO.setTrainingType(resultSet.getString("TrainingType"));
					
					
					
					
				}

			}
			employeeVTO.setProjectStartDate(DateUtility.getInstance().getDateOfLastYear());
			employeeVTO.setProjectEndDate(DateUtility.getInstance().getCurrentMySqlDate());

		} catch (ParseException ex) {
			ex.printStackTrace();
		} catch (SQLException se) {

			se.printStackTrace();
			// throw new ServiceLocatorException(se);
		} catch (Exception se) {

			se.printStackTrace();
			// throw new ServiceLocatorException(se);
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (statement != null) {
					statement.close();
					statement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}

			} catch (SQLException se) {
				throw new ServiceLocatorException(se);
			}
		}

		return employeeVTO;
	}

	/**
	 * The deleteEmployee(int employeeId) is used for deleting employee Details.
	 * 
	 * @throws ServiceLocatorException
	 * @see com.mss.mirage.util.ServiceLocatorException
	 *
	 * @return int variable
	 */
	public int deleteEmployee(int employeeId) throws ServiceLocatorException {
		int deletedRows = 0;
		Connection connection = null;
		Statement statement = null;
		String queryString = "UPDATE tblEmployee set DeletedFlag = 1 WHERE Id=" + employeeId;
		try {
			connection = ConnectionProvider.getInstance().getConnection();
			statement = connection.createStatement();
			deletedRows = statement.executeUpdate(queryString);

		} catch (SQLException se) {
			throw new ServiceLocatorException(se);
		} finally {
			try {
				if (statement != null) {
					statement.close();
					statement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException se) {
				throw new ServiceLocatorException(se);
			}
		}
		return deletedRows;
	}

	/**
	 * The sendMail(int employeeId) is used for sending e-mail to specified
	 * employee.
	 * 
	 * @throws ServiceLocatorException
	 * @see com.mss.mirage.util.ServiceLocatorException
	 *
	 * @return boolean variable
	 */
	public boolean sendMail(int employeeId) throws ServiceLocatorException {
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;
		PasswordUtility passwordUtility = new PasswordUtility();
		MailManager sendMail = new MailManager();
		boolean isMailSend = false;
		String userId = null;
		String userName = null;
		String password = null;
		String queryString = "SELECT FName,LName,MName,LoginId,Password FROM tblEmployee WHERE Id=" + employeeId;
		try {
			connection = ConnectionProvider.getInstance().getConnection();
			statement = connection.createStatement();
			resultSet = statement.executeQuery(queryString);
			while (resultSet.next()) {
				userName = resultSet.getString("FName") + " " + resultSet.getString("MName") + " "
						+ resultSet.getString("LName");
				userId = resultSet.getString("LoginId");
				password = resultSet.getString("Password");
			}

			if (password != null) {
				password = passwordUtility.decryptPwd(password);

			}
			if (Properties.getProperty("Mail.Flag").equals("1")) {
				sendMail.sendUserIdPwd(userId, userName, password);
			}
		} catch (SQLException se) {
			throw new ServiceLocatorException(se);
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (statement != null) {
					statement.close();
					statement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException se) {
				throw new ServiceLocatorException(se);
			}
		}
		isMailSend = true;
		return isMailSend;
	}

	/**
	 * The insetDefaultRoles(int employeeId) is used for inserting default roles
	 * to the employee depending up on his department in tblEmpRoles table.
	 * 
	 * @throws ServiceLocatorException
	 * @see com.mss.mirage.util.ServiceLocatorException
	 *
	 * @return int inserted rows
	 */
	public int insertDefaultRoles(int employeeId, String departmentName) throws ServiceLocatorException {
		int insertedRows = 0;
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;
		try {
			connection = ConnectionProvider.getInstance().getConnection();
			statement = connection.createStatement();
			/*
			 * ======================================== Assigning Default Role
			 * to the Employee ========================================
			 */
			if ("Sales".equalsIgnoreCase(departmentName)) {
				statement
						.addBatch("INSERT INTO tblEmpRoles(EmpId, RoleId, PrimaryFlag) values(" + employeeId + ",2,0)");
				statement
						.addBatch("INSERT INTO tblEmpRoles(EmpId, RoleId, PrimaryFlag) values(" + employeeId + ",4,1)");
			} else {
				statement
						.addBatch("INSERT INTO tblEmpRoles(EmpId, RoleId, PrimaryFlag) values(" + employeeId + ",2,1)");
			}
			int insertedRowsArray[] = statement.executeBatch();
			insertedRows = insertedRowsArray.length;

		} catch (SQLException se) {
			throw new ServiceLocatorException(se);
		} finally {
			try {
				if (statement != null) {
					statement.close();
					statement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException se) {
				throw new ServiceLocatorException(se);
			}
		}

		return insertedRows;
	}

	public boolean insertStateHistory(EmployeeAction employeePojo) throws ServiceLocatorException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		boolean isInserted = false;
		String practice="";
		String subPractice="";
		String empNo="";
		String country="";
		
		 String TABLE_EMP_STATE_HISTORY = Properties.getProperty("TABLE_EMP_STATE_HISTORY");
		 String empDetails=DataSourceDataProvider.getInstance().getEmployeeDetailsById(Integer.toString(employeePojo.getId()));
		 String[] data=empDetails.split(Pattern.quote("#^$"));
		 if(data[0]!=null){
		 practice=data[0];
		 }
		 if(data[1]!=null){
		 subPractice=data[1];
		 }
		 if(data[2]!=null){
		 empNo=data[2];
		 }
		 if(data[3]!=null){
		 country=data[3]; 
		 }
		// String queryString = "INSERT INTO " + TABLE_EMP_STATE_HISTORY +
		// "(EmpId,LoginId,State,StartDate,EndDate,SkillSet,CreatedDate,PrjName,Utilization,Comments,CreatedBy,ProjectStatus,ResumePath,ResumeName)
		// VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		String queryString = "INSERT INTO " + TABLE_EMP_STATE_HISTORY
				+ "(EmpId,LoginId,State,StartDate,EndDate,SkillSet,CreatedDate,PrjName,Utilization,Comments,CreatedBy,ProjectStatus,ResumePath,ResumeName,TrainingRating,TrainingType,Practice,SubPractice,EmpNo,Country) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		try {
			connection = ConnectionProvider.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(queryString);
			preparedStatement.setInt(1, employeePojo.getId());
			preparedStatement.setString(2, employeePojo.getLoginId());
			preparedStatement.setString(3, employeePojo.getEmpState());
			preparedStatement.setDate(4, employeePojo.getStateStartDate());
			preparedStatement.setDate(5, employeePojo.getStateEndDate());
			// preparedStatement.setFloat(6, employeePojo.getIntRatePerHour());
			// preparedStatement.setFloat(7, employeePojo.getInvRatePerHour());
			preparedStatement.setString(6, employeePojo.getSkillSet());
			preparedStatement.setTimestamp(7, employeePojo.getCreatedDate());
			preparedStatement.setString(8, employeePojo.getPrjName());
			// preparedStatement.setDouble(11, employeePojo.getCtcPerYear());
			preparedStatement.setInt(9, employeePojo.getUtilization());
			preparedStatement.setString(10, employeePojo.getComments());
			preparedStatement.setString(11, employeePojo.getCreatedBy());
			preparedStatement.setString(12, employeePojo.getEmpStatus());
			preparedStatement.setString(13, employeePojo.getFilepath());
			preparedStatement.setString(14, employeePojo.getUploadFileName());
			if (employeePojo.getTrainingRating() != null && !"".equals(employeePojo.getTrainingRating()))
				preparedStatement.setInt(15, Integer.parseInt(employeePojo.getTrainingRating()));
			else
				preparedStatement.setInt(15, 0);
			preparedStatement.setString(16, employeePojo.getTrainingType());
			preparedStatement.setString(17, practice);
			preparedStatement.setString(18,subPractice);
			preparedStatement.setString(19, empNo);
			preparedStatement.setString(20,country);
			isInserted = preparedStatement.execute();

		} catch (SQLException se) {
			se.printStackTrace();
			throw new ServiceLocatorException(se);
		} finally {
			try {
				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException se) {
				throw new ServiceLocatorException(se);
			}
		}

		return isInserted;

	}

	public StateVTO getStateVTO(EmployeeAction employeePojo) throws ServiceLocatorException {
		StateVTO stateVTO = new StateVTO();
		stateVTO.setEmpId(employeePojo.getId());
		stateVTO.setEmpState(employeePojo.getEmpState());
		// stateVTO.setIntRatePerHour(employeePojo.getIntRatePerHour());
		// stateVTO.setInvRatePerHour(employeePojo.getInvRatePerHour());
		stateVTO.setLoginId(employeePojo.getLoginId());
		stateVTO.setSkillSet(employeePojo.getSkillSet());
		stateVTO.setStateEndDate(employeePojo.getStateEndDate());
		stateVTO.setStateStartDate(employeePojo.getStateStartDate());
		stateVTO.setCreatedDate(employeePojo.getCreatedDate());
		stateVTO.setPrjName(employeePojo.getPrjName());
		// stateVTO.setCtcPerYear(employeePojo.getCtcPerYear());
		stateVTO.setFilepath(employeePojo.getFilepath());
		stateVTO.setUploadFileName(employeePojo.getUploadFileName());
		return stateVTO;
	}

	public int updateStateHistory(EmployeeAction employeePojo, int recordId) throws ServiceLocatorException {
		int updatedRows = 0;
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		String TABLE_EMP_STATE_HISTORY = Properties.getProperty("TABLE_EMP_STATE_HISTORY");

		String queryString = "UPDATE " + TABLE_EMP_STATE_HISTORY
				+ " SET State=?,StartDate=?,EndDate=?,SkillSet=?,PrjName=?, Utilization=?, Comments=?,ModifiedBy=?,ModifiedDate=?,ProjectStatus=?,ResumePath=?,ResumeName=?,TrainingRating=?,TrainingType=? WHERE Id=?";
		try {
			connection = ConnectionProvider.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(queryString);
			preparedStatement.setString(1, employeePojo.getEmpState());
			preparedStatement.setDate(2, employeePojo.getStateStartDate());
			preparedStatement.setDate(3, employeePojo.getStateEndDate());
			// preparedStatement.setFloat(4, employeePojo.getIntRatePerHour());
			// preparedStatement.setFloat(5, employeePojo.getInvRatePerHour());
			preparedStatement.setString(4, employeePojo.getSkillSet());
			// preparedStatement.setTimestamp(7,employeePojo.getCreatedDate());
			preparedStatement.setString(5, employeePojo.getPrjName());
			// preparedStatement.setDouble(8, employeePojo.getCtcPerYear());
			preparedStatement.setInt(6, employeePojo.getUtilization());
			preparedStatement.setString(7, employeePojo.getComments());
			preparedStatement.setString(8, employeePojo.getModifiedBy());
			preparedStatement.setTimestamp(9, employeePojo.getModifiedDate());
			preparedStatement.setString(10, employeePojo.getEmpStatus());
			preparedStatement.setString(11, employeePojo.getFilepath());
			preparedStatement.setString(12, employeePojo.getUploadFileName());
			if (employeePojo.getTrainingRating() != null && !"".equals(employeePojo.getTrainingRating()))
				preparedStatement.setInt(13, Integer.parseInt(employeePojo.getTrainingRating()));
			else
				preparedStatement.setInt(13, 0);
			preparedStatement.setString(14, employeePojo.getTrainingType());

			preparedStatement.setInt(15, recordId);
			updatedRows = preparedStatement.executeUpdate();

		} catch (SQLException se) {
			se.printStackTrace();
			throw new ServiceLocatorException(se);
		} finally {
			try {
				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException se) {
				throw new ServiceLocatorException(se);
			}
		}
		return updatedRows;
	}

	public int getRecentStateHistoryId(String loginId) throws ServiceLocatorException {
		int recordId = 0;
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;
		String TABLE_EMP_STATE_HISTORY = Properties.getProperty("TABLE_EMP_STATE_HISTORY");

		try {
			String queryString = "SELECT Id FROM " + TABLE_EMP_STATE_HISTORY + " WHERE LoginId='" + loginId
					+ "' ORDER BY Id ASC";
			connection = ConnectionProvider.getInstance().getConnection();
			statement = connection.createStatement();
			resultSet = statement.executeQuery(queryString);
			int counter = 0;
			while (resultSet.next()) {
				if (counter == 0) {
					recordId = resultSet.getInt("Id");
				}
				counter++;
			}

		} catch (SQLException se) {
			throw new ServiceLocatorException(se);
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (statement != null) {
					statement.close();
					statement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException se) {
				throw new ServiceLocatorException(se);
			}
		}
		return recordId;
	}

	public Collection getStateHistoryCollection(String loginId, int noOfRecords) throws ServiceLocatorException {
		Map stateHistoryMap = new LinkedHashMap();
		int counter = 1;
		String tempCreatedDate = "";
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;
		String TABLE_EMP_STATE_HISTORY = Properties.getProperty("TABLE_EMP_STATE_HISTORY");

		try {
			String queryString = "SELECT * FROM " + TABLE_EMP_STATE_HISTORY + " WHERE LoginId='" + loginId
					+ "' ORDER BY StartDate DESC,CreatedDate DESC";
			connection = ConnectionProvider.getInstance().getConnection();
			statement = connection.createStatement();
			resultSet = statement.executeQuery(queryString);
			while (resultSet.next()) {
				if (counter <= noOfRecords) {
					StateVTO stateVTO = new StateVTO();
					stateVTO.setId(resultSet.getInt("Id"));
					stateVTO.setEmpId(resultSet.getInt("EmpId"));
					stateVTO.setEmpState(resultSet.getString("State"));
					// stateVTO.setIntRatePerHour(resultSet.getFloat("IntRatePerHour"));
					// stateVTO.setInvRatePerHour(resultSet.getFloat("InvRatePerHour"));
					stateVTO.setLoginId(resultSet.getString("LoginId"));
					stateVTO.setSkillSet(resultSet.getString("SkillSet"));
					if (resultSet.getDate("StartDate") != null) {
						stateVTO.setStateStartDate(resultSet.getDate("StartDate"));
						stateVTO.setStartDate(resultSet.getString("StartDate"));
					}
					if (resultSet.getDate("EndDate") != null) {
						stateVTO.setStateEndDate(resultSet.getDate("EndDate"));
						stateVTO.setEndDate(resultSet.getString("EndDate"));
					}
					stateVTO.setCreatedDate(resultSet.getTimestamp("CreatedDate"));
					tempCreatedDate = resultSet.getTimestamp("CreatedDate").toString();
					tempCreatedDate = tempCreatedDate.substring(0, 10);
					stateVTO.setCreatedDate1(DateUtility.getInstance().convertToviewFormat(tempCreatedDate));
					stateVTO.setUtilization(resultSet.getInt("Utilization"));
					stateVTO.setPrjName(resultSet.getString("PrjName"));
					stateVTO.setProjectContactId(resultSet.getInt("ProjectContactsId"));
					stateVTO.setProjectStatus(resultSet.getString("ProjectStatus"));
					stateVTO.setTrainingRating(resultSet.getString("TrainingRating"));
					stateVTO.setTrainingType(resultSet.getString("TrainingType"));
				
					if (resultSet.getString("EmpProjStatus") != null && !"".equals(resultSet.getString("EmpProjStatus"))) {
						
						if(resultSet.getInt("IsBillable") == 1){
							stateVTO.setEmpPrjStatus(resultSet.getString("EmpProjStatus") + " - Billable");
						}
						else{
							stateVTO.setEmpPrjStatus(resultSet.getString("EmpProjStatus"));
						}
					}
					
					else{
						stateVTO.setEmpPrjStatus(" - ");
					}
					
					stateHistoryMap.put("stateVTO" + counter, stateVTO);
					counter++;
				}
			}

		} catch (SQLException se) {
			throw new ServiceLocatorException(se);
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (statement != null) {
					statement.close();
					statement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException se) {
				throw new ServiceLocatorException(se);
			}
		}
		return stateHistoryMap.values();
	}

	public int deleteEmpStatus(int empId, int currId) throws ServiceLocatorException {
		int deletedRows = 0;
		Connection connection = null;
		Statement statement = null;
		String TABLE_EMP_STATE_HISTORY = Properties.getProperty("TABLE_EMP_STATE_HISTORY");

		String queryString = "delete from " + TABLE_EMP_STATE_HISTORY + " where EmpId = " + empId + " AND Id = "
				+ currId;
		try {
			connection = ConnectionProvider.getInstance().getConnection();
			statement = connection.createStatement();
			deletedRows = statement.executeUpdate(queryString);

		} catch (SQLException se) {
			throw new ServiceLocatorException(se);
		} finally {
			try {
				if (statement != null) {
					statement.close();
					statement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException se) {
				throw new ServiceLocatorException(se);
			}
		}
		return deletedRows;
	}

	public String generateEmployeeList(String loginId) {
		DateUtility dateutility = new DateUtility();
		String filePath = "";
		StringBuffer sb = null;
		Connection connection = null;

		/** callableStatement is a reference variable for CallableStatement . */
		CallableStatement callableStatement = null;

		/**
		 * preStmt,preStmtTemp are reference variable for PreparedStatement .
		 */
		PreparedStatement preStmt = null, preStmtTemp = null;

		/**
		 * The queryString is useful to get queryString result to the particular
		 * jsp page
		 */
		String queryString = "";
		Statement statement = null;

		/** The statement is useful to execute the above queryString */
		ResultSet resultSet = null;
		String timeSheetStatus = "";
		HashMap map = null;
		HashMap map1 = null;
		List finalList = new ArrayList();
		try {

			String TABLE_EMP_STATE_HISTORY = Properties.getProperty("TABLE_EMP_STATE_HISTORY");

			File file = new File(Properties.getProperty("Emp.StateHistory.Path"));

			if (!file.exists()) {
				file.mkdirs();
			}

			FileOutputStream fileOut = new FileOutputStream(
					file.getAbsolutePath() + File.separator + loginId + "_StateHistory.xls");

			connection = ConnectionProvider.getInstance().getConnection();
			String query = null;

			// query = "SELECT * FROM tblEmpStateHistory WHERE
			// LoginId='"+loginId+"' ORDER BY StartDate DESC";
			query = "SELECT * FROM " + TABLE_EMP_STATE_HISTORY + " WHERE LoginId='" + loginId
					+ "' ORDER BY StartDate DESC";

			// System.out.println("query123-->"+query);
			String reportToName = "";
			List teamList = null;

			int j = 1;
			preStmt = connection.prepareStatement(query);

			resultSet = preStmt.executeQuery();

			while (resultSet.next()) {
				String state = "";
				if (!"".equals(resultSet.getString("State")) && resultSet.getString("State") != null) {
					state = resultSet.getString("State");
				}

				double IntRatePerHour = resultSet.getFloat("IntRatePerHour");

				double InvRatePerHour = resultSet.getFloat("InvRatePerHour");

				String LoginId = resultSet.getString("LoginId");

				String SkillSet = "";
				if (!"".equals(resultSet.getString("SkillSet")) && resultSet.getString("SkillSet") != null) {
					SkillSet = resultSet.getString("SkillSet");
				}

				String EndDate = "";
				if (!"".equals(resultSet.getString("EndDate")) && resultSet.getString("EndDate") != null) {
					EndDate = resultSet.getString("EndDate");
				}

				String StartDate = "";
				if (!"".equals(resultSet.getString("StartDate")) && resultSet.getString("StartDate") != null) {
					StartDate = resultSet.getString("StartDate");
				}

				String CreatedDate = "";
				if (!"".equals(resultSet.getString("CreatedDate")) && resultSet.getString("CreatedDate") != null) {
					CreatedDate = resultSet.getString("CreatedDate");
				}

				String PrjName = "";

				if (!"".equals(resultSet.getString("PrjName")) && resultSet.getString("PrjName") != null) {
					PrjName = resultSet.getString("PrjName");
				}

				String reportsTo = "";

				map = new HashMap();
				map.put("SNO", String.valueOf(j));
				map.put("state", state);
				map.put("IntRatePerHour", IntRatePerHour);
				map.put("InvRatePerHour", InvRatePerHour);
				map.put("LoginId", LoginId);
				map.put("SkillSet", SkillSet);
				map.put("EndDate", EndDate);
				map.put("StartDate", StartDate);
				map.put("CreatedDate", CreatedDate);
				map.put("PrjName", PrjName);
				// System.out.println("map=="+map);

				finalList.add(map);
				j++;

			}

			if (finalList.size() > 0) {
				filePath = file.getAbsolutePath() + File.separator + loginId + "_StateHistory.xls";
				HSSFWorkbook hssfworkbook = new HSSFWorkbook();
				HSSFSheet sheet = hssfworkbook.createSheet("State History");

				HSSFCellStyle cs = hssfworkbook.createCellStyle();
				HSSFCellStyle headercs = hssfworkbook.createCellStyle();
				headercs.setFillForegroundColor(HSSFColor.BLACK.index);
				headercs.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
				headercs.setBorderTop((short) 1); // single line border
				headercs.setBorderBottom((short) 1); // single line border

				HSSFFont timesBoldFont = hssfworkbook.createFont();
				timesBoldFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
				timesBoldFont.setColor(HSSFColor.WHITE.index);
				timesBoldFont.setFontName("Arial");
				headercs.setFont(timesBoldFont);

				HSSFFont footerFont = hssfworkbook.createFont();
				footerFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
				footerFont.setFontName("Arial");

				HSSFCellStyle footercs = hssfworkbook.createCellStyle();
				footercs.setFont(footerFont);

				HSSFDataFormat df = hssfworkbook.createDataFormat();
				HSSFRow row = sheet.createRow((short) 0);
				HSSFCell cell = row.createCell((short) 0);

				HSSFCell cell1 = row.createCell((short) 1);

				HSSFCell cell2 = row.createCell((short) 2);
				HSSFCell cell3 = row.createCell((short) 3);

				HSSFCell cell4 = row.createCell((short) 4);
				HSSFCell cell5 = row.createCell((short) 5);
				HSSFCell cell6 = row.createCell((short) 6);
				HSSFCell cell7 = row.createCell((short) 7);
				HSSFCell cell8 = row.createCell((short) 8);
				cell.setCellValue("SNO");
				cell1.setCellValue("State");
				cell2.setCellValue("StartDate");
				cell3.setCellValue("EndDate");
				cell4.setCellValue("IntRatePerHour");
				cell5.setCellValue("InvRatePerHour");
				cell6.setCellValue("SkillSet");
				cell7.setCellValue("ProjectName");
				cell8.setCellValue("CreatedDate");

				cell.setCellStyle(headercs);
				cell1.setCellStyle(headercs);
				cell2.setCellStyle(headercs);
				cell3.setCellStyle(headercs);
				cell4.setCellStyle(headercs);
				cell5.setCellStyle(headercs);
				cell6.setCellStyle(headercs);
				cell7.setCellStyle(headercs);
				cell8.setCellStyle(headercs);
				int count = 1;

				if (finalList.size() > 0) {
					Map stateHistorylMap = null;
					for (int i = 0; i < finalList.size(); i++) {
						stateHistorylMap = (Map) finalList.get(i);
						row = sheet.createRow((short) count++);
						cell = row.createCell((short) 0);

						cell1 = row.createCell((short) 1);
						cell2 = row.createCell((short) 2);
						cell3 = row.createCell((short) 3);
						cell4 = row.createCell((short) 4);
						cell5 = row.createCell((short) 5);
						cell6 = row.createCell((short) 6);
						cell7 = row.createCell((short) 7);
						cell8 = row.createCell((short) 8);

						cell.setCellValue((String) stateHistorylMap.get("SNO"));
						cell1.setCellValue((String) stateHistorylMap.get("state"));
						cell2.setCellValue((String) stateHistorylMap.get("StartDate"));
						cell3.setCellValue((String) stateHistorylMap.get("EndDate"));
						cell4.setCellValue((Double) stateHistorylMap.get("IntRatePerHour"));
						cell5.setCellValue((Double) stateHistorylMap.get("InvRatePerHour"));
						cell6.setCellValue((String) stateHistorylMap.get("SkillSet"));
						cell7.setCellValue((String) stateHistorylMap.get("PrjName"));
						cell8.setCellValue((String) stateHistorylMap.get("CreatedDate"));

						cell.setCellStyle(cs);
						cell1.setCellStyle(cs);
						cell2.setCellStyle(cs);
						cell3.setCellStyle(cs);
						cell4.setCellStyle(cs);
						cell5.setCellStyle(cs);
						cell6.setCellStyle(cs);
						cell7.setCellStyle(cs);
						cell8.setCellStyle(cs);
					}
					row = sheet.createRow((short) count++);
					cell = row.createCell((short) 0);

					cell1 = row.createCell((short) 1);
					cell2 = row.createCell((short) 2);
					cell3 = row.createCell((short) 3);
					cell4 = row.createCell((short) 4);
					cell.setCellValue("");

					cell4.setCellValue("");

					cell.setCellStyle(footercs);
					cell1.setCellStyle(footercs);
					cell2.setCellStyle(footercs);
					cell3.setCellStyle(footercs);

					cell4.setCellStyle(footercs);
				}
				sheet.autoSizeColumn((int) 0);
				sheet.autoSizeColumn((int) 1);
				sheet.autoSizeColumn((int) 2);
				sheet.autoSizeColumn((int) 3);
				sheet.autoSizeColumn((int) 4);

				hssfworkbook.write(fileOut);
				fileOut.flush();
				fileOut.close();

			}

		} catch (FileNotFoundException fne) {

			fne.printStackTrace();
		} catch (IOException ioe) {

			ioe.printStackTrace();
		} catch (Exception ex) {
			ex.printStackTrace();

		} finally {
			// System.out.println("finally");
			try {
				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (preStmt != null) {
					preStmt.close();
					preStmt = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (Exception se) {
				se.printStackTrace();
			}
		}

		return filePath;

	}

	public String getEmployeeResumeLocation(int fileId) throws ServiceLocatorException {
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;
		String fileLocation = "";
		try {
			connection = ConnectionProvider.getInstance().getConnection();
			statement = connection.createStatement();
			String TABLE_EMP_STATE_HISTORY = Properties.getProperty("TABLE_EMP_STATE_HISTORY");
			resultSet = statement
					.executeQuery("SELECT ResumePath FROM " + TABLE_EMP_STATE_HISTORY + " WHERE Id=" + fileId);
			if (resultSet.next()) {
				fileLocation = resultSet.getString("ResumePath");
			}

		} catch (SQLException se) {
			throw new ServiceLocatorException(se);
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (statement != null) {
					statement.close();
					statement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException sqle) {
				throw new ServiceLocatorException(sqle);
			}
		}
		return fileLocation;
	}

	public String getQuarterAppraisalDetails(int managerFlag, int empId) throws ServiceLocatorException {

		Connection connection = null;
		PreparedStatement preparedStatement = null;
		CallableStatement callableStatement = null;
		ResultSet resultSet = null;
		String resutMessage = "";
		boolean isInserted = false;

		// String queryString = "INSERT INTO `tblInvestments` ( `Inv_Name`,
		// `Country`, `StartDate`, `EndDate`,
		// `TotalExpenses`,`Currency`,`Location`,`Description`,`AttachmentFileName`,`AttachmentLocation`,`CreatedBy`,InvestmentType)"
		// + " VALUES(?,?,?,?,?,?,?,?,?,?,?,?)";
		String queryString = "{call spGetFactoryDetails(?,?,?)}";

		try {
			connection = ConnectionProvider.getInstance().getConnection();

			callableStatement = connection.prepareCall(queryString);
			// System.out.println("managerFlag---"+managerFlag);
			callableStatement.setInt(1, managerFlag);
			callableStatement.setInt(2, empId);
			callableStatement.registerOutParameter(3, Types.VARCHAR);
			callableStatement.executeUpdate();
			resutMessage = callableStatement.getString(3);

		} catch (SQLException se) {
			se.printStackTrace();
			// resutMessage = "<font color=red>" + se.getMessage() + "</font>";
			throw new ServiceLocatorException(se);
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException se) {
				se.printStackTrace();
				throw new ServiceLocatorException(se);
			}
		}
		return resutMessage;
	}

	@Override
	public String empQuarterlyAppraisalSave(HttpServletRequest httpServletRequest, int rowcount, String curretRole,
			int appraisalId, String status, int empId, int lineId, String shortTeamGoal, String shortTeamGoalComments,
			String longTeamGoal, String longTeamGoalComments, String strength, String strengthsComments,
			String improvements, String improvementsComments, String quarterly, String rejectedComments, int year)
			throws ServiceLocatorException {

		Connection connection = null;
		PreparedStatement preparedStatement = null;
		CallableStatement callableStatement = null;
		ResultSet resultSet = null;
		String resutMessage = "";

		String queryString = "{call spQuarterlyAppraisalSave(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";

		try {
			String roleName = httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_ROLE_NAME)
					.toString();
			String loginId = httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID)
					.toString();
			// System.out.println("getRowCount==="+rowcount);
			String measurementId = "";
			String keyFactorId = "";
			String keyFactorWeightage = "";
			String empDescription = "";
			String empRating = "";
			String mgrComments = "";
			String appraisalManagerRating = "";
			String calWeightage = "";
			String HrWeightage = "";
			String appraisalHrRating="";
			String hrCalWeightage="";
			String id = "";
			double totalEmpRating = Double.parseDouble(httpServletRequest.getParameter("totalEmpRating"));
			double totalWeightage = Double.parseDouble(httpServletRequest.getParameter("totalKeyFactor"));
			double totalMgrRating = 0.0;
			double totalCalWeightage = 0.0;
			double totalHrRating=0.0;
			double totalHrCalWeightage=0.0;

			
			if (rowcount > 0) {

				for (int i = 1; i <= rowcount; i++) {
					measurementId = measurementId + httpServletRequest.getParameter("measurementId" + i) + "#^$";
					keyFactorId = keyFactorId + httpServletRequest.getParameter("keyFactorId" + i) + "#^$";
					keyFactorWeightage = keyFactorWeightage + httpServletRequest.getParameter("keyFactorWeightage" + i)
							+ "#^$";
					HrWeightage = HrWeightage + httpServletRequest.getParameter("HrWeightage" + i) + "#^$";
					
					empDescription = empDescription + httpServletRequest.getParameter("empDescription" + i) + "#^$";
					if (httpServletRequest.getParameter("empRating" + i).equals("")) {
						empRating = empRating + "0" + "#^$";
					} else {
						empRating = empRating + httpServletRequest.getParameter("empRating" + i) + "#^$";
					}
					if (httpServletRequest.getParameter("id" + i).equals("")) {
						id = id + "0" + "#^$";
					} else {
						id = id + httpServletRequest.getParameter("id" + i) + "#^$";
					}
					if (curretRole.equals("team")) {
						mgrComments = mgrComments + httpServletRequest.getParameter("mgrComments" + i) + "#^$";
						if (httpServletRequest.getParameter("appraisalManagerRating" + i).equals("")) {
							appraisalManagerRating = appraisalManagerRating + "0" + "#^$";
						} else {
							appraisalManagerRating = appraisalManagerRating
									+ httpServletRequest.getParameter("appraisalManagerRating" + i) + "#^$";
						}
						// System.out.println("httpServletRequest.getParameter(\"calWeightage\"+i)==="+httpServletRequest.getParameter("calWeightage"+i));
						if (httpServletRequest.getParameter("calWeightage" + i).equals("")) {
							calWeightage = calWeightage + "0" + "#^$";
						} else {
							calWeightage = calWeightage + httpServletRequest.getParameter("calWeightage" + i) + "#^$";
						}
					}
					
					if( roleName.equals("Operations")){
						if (httpServletRequest.getParameter("appraisalHrRating" + i).equals("")) {
							appraisalHrRating = appraisalHrRating + "0" + "#^$";
						} else {
							appraisalHrRating = appraisalHrRating
									+ httpServletRequest.getParameter("appraisalHrRating" + i) + "#^$";
						}
						
						if (httpServletRequest.getParameter("hrCalWeightage" + i).equals("")) {
							hrCalWeightage = hrCalWeightage + "0" + "#^$";
						} else {
							hrCalWeightage = hrCalWeightage + httpServletRequest.getParameter("hrCalWeightage" + i) + "#^$";
						}
						
					}

				}
				if (curretRole.equals("team")) {
					totalMgrRating = Double.parseDouble(httpServletRequest.getParameter("totalMgrRating"));
					totalCalWeightage = Double.parseDouble(httpServletRequest.getParameter("totalCalWeightage"));
				}
				if( roleName.equals("Operations")){
					
					totalHrRating = Double.parseDouble(httpServletRequest.getParameter("totalHrRating"));
					totalHrCalWeightage = Double.parseDouble(httpServletRequest.getParameter("totalHrCalWeightage"));
				}

				// System.out.println("measurementId=="+measurementId);
				// System.out.println("keyFactorId=="+keyFactorId);
				// System.out.println("keyFactorWeightage=="+keyFactorWeightage);
				// System.out.println("empDescription=="+empDescription);
				// System.out.println("empRating=="+empRating);
				// System.out.println("appraisalManagerRating=="+appraisalManagerRating);
				// System.out.println("calWeightage=="+calWeightage);
			}
			// System.out.println("shortTeamGoal=="+shortTeamGoal);
			// System.out.println("shortTeamGoalComments=="+shortTeamGoalComments);
			// System.out.println("longTeamGoal=="+longTeamGoal);
			// System.out.println("longTeamGoalComments=="+longTeamGoalComments);
			// System.out.println("strength=="+strength);
			// System.out.println("strengthsComments=="+strengthsComments);
			// System.out.println("improvements=="+improvements);
			// System.out.println("improvementsComments=="+improvementsComments);

			connection = ConnectionProvider.getInstance().getConnection();

			callableStatement = connection.prepareCall(queryString);
			callableStatement.setString(1, id);
			callableStatement.setInt(2, appraisalId);
			callableStatement.setInt(3, empId);
			callableStatement.setString(4, measurementId);
			callableStatement.setString(5, keyFactorId);
			callableStatement.setString(6, keyFactorWeightage);
			callableStatement.setString(7, empDescription);
			callableStatement.setString(8, empRating);
			callableStatement.setString(9, loginId);
			callableStatement.setTimestamp(10, DateUtility.getInstance().getCurrentMySqlDateTime());
			callableStatement.setString(11, status);
			callableStatement.setDouble(12, totalWeightage);
			callableStatement.setDouble(13, totalEmpRating);
			callableStatement.setInt(14, rowcount);
			callableStatement.setString(15, mgrComments);
			callableStatement.setString(16, appraisalManagerRating);
			callableStatement.setString(17, calWeightage);
			callableStatement.setDouble(18, totalMgrRating);
			callableStatement.setDouble(19, totalCalWeightage);
			callableStatement.setInt(20, lineId);
			callableStatement.setString(21, shortTeamGoal);
			callableStatement.setString(22, shortTeamGoalComments);
			callableStatement.setString(23, longTeamGoal);
			callableStatement.setString(24, longTeamGoalComments);
			callableStatement.setString(25, strength);
			callableStatement.setString(26, strengthsComments);
			callableStatement.setString(27, improvements);
			callableStatement.setString(28, improvementsComments);
			callableStatement.setString(29, quarterly);
			callableStatement.setString(30, roleName);
			callableStatement.setString(31, rejectedComments);
			callableStatement.setInt(32, year);
			 callableStatement.setString(33, HrWeightage);
				callableStatement.setString(34, appraisalHrRating);
				callableStatement.setString(35, hrCalWeightage);
				callableStatement.setDouble(36, totalHrRating);
				callableStatement.setDouble(37, totalHrCalWeightage);
				callableStatement.registerOutParameter(38, Types.VARCHAR);
				callableStatement.executeUpdate();
				resutMessage = callableStatement.getString(38);
			// System.out.println("resutMessage==="+resutMessage);

		} catch (SQLException se) {
			se.printStackTrace();
			// resutMessage = "<font color=red>" + se.getMessage() + "</font>";
			throw new ServiceLocatorException(se);
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException se) {
				se.printStackTrace();
				throw new ServiceLocatorException(se);
			}
		}
		return resutMessage;
	}

	public String quarterlyAppraisalEdit(int empId, int appraisalId, int lineId) throws ServiceLocatorException {

		Connection connection = null;
		PreparedStatement preparedStatement = null;
		CallableStatement callableStatement = null;
		ResultSet resultSet = null;
		String resutMessage = "";
		boolean isInserted = false;

		// String queryString = "INSERT INTO `tblInvestments` ( `Inv_Name`,
		// `Country`, `StartDate`, `EndDate`,
		// `TotalExpenses`,`Currency`,`Location`,`Description`,`AttachmentFileName`,`AttachmentLocation`,`CreatedBy`,InvestmentType)"
		// + " VALUES(?,?,?,?,?,?,?,?,?,?,?,?)";
		String queryString = "{call spGetFactoryDetailsEdit(?,?,?,?)}";

		try {
			connection = ConnectionProvider.getInstance().getConnection();

			callableStatement = connection.prepareCall(queryString);
			callableStatement.setInt(1, empId);
			callableStatement.setInt(2, appraisalId);
			callableStatement.setInt(3, lineId);
			callableStatement.registerOutParameter(4, Types.VARCHAR);
			callableStatement.executeUpdate();
			resutMessage = callableStatement.getString(4);

		} catch (SQLException se) {
			se.printStackTrace();
			// resutMessage = "<font color=red>" + se.getMessage() + "</font>";
			throw new ServiceLocatorException(se);
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException se) {
				se.printStackTrace();
				throw new ServiceLocatorException(se);
			}
		}
		return resutMessage;
	}

	public int isExistedAppraisal(int empId, int year, String Quarterly) throws ServiceLocatorException {
		int recordId = 0;
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;

		try {
			/*
			 * GregorianCalendar cal = new GregorianCalendar(); int
			 * year=cal.get(Calendar.YEAR); int month=cal.get(Calendar.MONTH)+1;
			 * double count=Math.ceil((double)month/3); int totalCount=(int)
			 * count; String Quarterly="Q"+totalCount;
			 */
			String queryString = "SELECT id FROM tblQuarterlyAppraisals WHERE EmpId=" + empId
					+ " AND QYear=" + year + " AND Quarterly='" + Quarterly + "'";
			// // System.out.println(queryString);
			connection = ConnectionProvider.getInstance().getConnection();
			statement = connection.createStatement();
			resultSet = statement.executeQuery(queryString);
			if (resultSet.next()) {
				recordId = resultSet.getInt("Id");

				recordId = 1;
			}

		} catch (SQLException se) {
			throw new ServiceLocatorException(se);
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (statement != null) {
					statement.close();
					statement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException se) {
				throw new ServiceLocatorException(se);
			}
		}
		return recordId;

	}

	public int notClosedAppraisal(int empId) throws ServiceLocatorException {
		int recordId = 0;
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;

		try {
			/*
			 * GregorianCalendar cal = new GregorianCalendar(); int
			 * year=cal.get(Calendar.YEAR); int month=cal.get(Calendar.MONTH)+1;
			 * double count=Math.ceil((double)month/3); int totalCount=(int)
			 * count; String Quarterly="Q"+totalCount;
			 */
			/*String queryString = "SELECT id FROM tblQuarterlyAppraisals WHERE EmpId=" + empId
					+ " AND OpperationTeamStatus!='Closed'"; */
			String queryString = "SELECT id FROM tblQuarterlyAppraisals WHERE EmpId=" + empId
					+ " AND Status='Entered'";
			// // System.out.println(queryString);
			connection = ConnectionProvider.getInstance().getConnection();
			statement = connection.createStatement();
			resultSet = statement.executeQuery(queryString);
			if (resultSet.next()) {
				// recordId = resultSet.getInt("Id");

				recordId = 1;
			}

		} catch (SQLException se) {
			throw new ServiceLocatorException(se);
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (statement != null) {
					statement.close();
					statement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException se) {
				throw new ServiceLocatorException(se);
			}
		}
		return recordId;

	}

	//////// star performer

	public int doNomineeApproval(int Id, String nomineeIds, String createdBy, String starMonth, String mail)
			throws ServiceLocatorException {

		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;
		String query = "";
		int row = 0;
		try {
			// System.out.println("in service Impl---->");
			connection = ConnectionProvider.getInstance().getConnection();
			statement = connection.createStatement();

			query = "Update tblStarPerformerLines Set Status='Approve',ModifiedDate=NOW(),ModifiedBy='" + createdBy
					+ "'" + " WHERE FIND_IN_SET(tblStarPerformerLines.Id,'" + nomineeIds + "') AND ObjectId=" + Id;

			row = statement.executeUpdate(query);
			if (row > 0) {
				String sendMailId = Properties.getProperty("STARPERFORMER_SUBMITTED_ACCESS");

				MailManager.sendStarPerformerFinalListDetails(sendMailId, Id, starMonth, mail);

				String query1 = "Update tblStarPerformers Set Status='4' WHERE Id=" + Id + "";

				statement = connection.createStatement();
				statement.executeUpdate(query1);

			}

		} catch (SQLException se) {
			throw new ServiceLocatorException(se);
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (statement != null) {
					statement.close();
					statement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException sqle) {
				throw new ServiceLocatorException(sqle);
			}
		}

		return row;

	}

	public int starPerformerDelete(int starId) throws ServiceLocatorException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;

		int isdeleted = 0;

		// System.out.println("starPerformerDelete");
		String queryString = "Delete from tblStarPerformerLines where Id=" + starId;
		try {
			connection = ConnectionProvider.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(queryString);
			isdeleted = preparedStatement.executeUpdate();
			// System.out.println("isdeleted"+isdeleted);
		} catch (SQLException se) {
			se.printStackTrace();
			throw new ServiceLocatorException(se);
		} finally {
			try {
				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}

				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException se) {
				throw new ServiceLocatorException(se);
			}
		}

		return isdeleted;

	}

	public int doStarPerformersAdd(String resourceName, int EmployeeId, int ObjectId, String loginId,
			String resourceComments) throws ServiceLocatorException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		Statement statement = null;
		ResultSet resultSet = null;
		DataSourceDataProvider dataSourceDataProvider = null;
		int isInserted = 0;
		String image = "";
		// System.out.println("doStarPerformersAdd");
		String queryString = "insert into tblStarPerformerLines(ObjectId,EmpId,EmpName,Department,Title,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,FilePath,FileName,Status,Comments) values(?,?,?,?,?,?,?,?,?,?,?,?,?)";
		try {

			String departmentDetails[] = dataSourceDataProvider.getInstance()
					.getemployeenameAndDepbyEmpId(String.valueOf(EmployeeId)).toString().split("\\#\\$\\#");
			String title = dataSourceDataProvider.getInstance().getEmpTitleByEmpId(EmployeeId);
			// System.out.println(departmentDetails[0]);
			connection = ConnectionProvider.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(queryString);
			preparedStatement.setInt(1, ObjectId);
			preparedStatement.setInt(2, EmployeeId);
			preparedStatement.setString(3, resourceName);
			preparedStatement.setString(4, departmentDetails[1]);
			preparedStatement.setString(5, title);
			preparedStatement.setString(6, loginId);
			preparedStatement.setTimestamp(7, DateUtility.getInstance().getCurrentMySqlDateTime());
			preparedStatement.setString(8, loginId);
			preparedStatement.setTimestamp(9, DateUtility.getInstance().getCurrentMySqlDateTime());
			String empLoginId = dataSourceDataProvider.getInstance().getEmpLoginIdById(String.valueOf(EmployeeId));
			image = Properties.getProperty("EmpImages.path").trim() + "/" + empLoginId + ".png";

			File f = new File(image);
			if (f.exists()) {
				image = Properties.getProperty("EmpImages.URL").trim() + "/" + empLoginId + ".png";

				preparedStatement.setString(10, image);
				preparedStatement.setString(11, empLoginId + ".png");

			} else {

				if (departmentDetails[2].equalsIgnoreCase("Female")) {
					image = Properties.getProperty("EmpImages.URL").trim() + "/" + "NoImage_Female.png";
					preparedStatement.setString(10, image);
					preparedStatement.setString(11, "NoImage_Female.png");

				} else {
					image = Properties.getProperty("EmpImages.URL").trim() + "/" + "NoImage_male.png";
					preparedStatement.setString(10, image);
					preparedStatement.setString(11, "NoImage_male.png");

				}

			}

			// preparedStatement.setString(10, image);
			preparedStatement.setString(12, "Submit ");
			preparedStatement.setString(13, resourceComments);

			isInserted = preparedStatement.executeUpdate();
			if (isInserted > 0) {
				String query1 = "Update tblStarPerformers Set Status='2' WHERE Id=" + ObjectId + "";

				// System.out.println("query.."+query1);
				statement = connection.createStatement();
				statement.executeUpdate(query1);

			}
		} catch (SQLException se) {
			se.printStackTrace();
			throw new ServiceLocatorException(se);
		} finally {
			try {
				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}
				if (statement != null) {
					statement.close();
					statement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException se) {
				throw new ServiceLocatorException(se);
			}
		}

		return isInserted;

	}

	public int starPerformerMovieTkt(int starId, String resumePath, String loginId) throws ServiceLocatorException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;

		int isupdated = 0;

		// System.out.println("doStarPerformersAdd");
		String queryString = "Update  tblStarPerformerLines  Set ModifiedBy=?,ModifiedDate=?,ResumePath=?  where Id=?";

		try {
			connection = ConnectionProvider.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(queryString);
			preparedStatement.setString(1, loginId);
			preparedStatement.setTimestamp(2, DateUtility.getInstance().getCurrentMySqlDateTime());
			preparedStatement.setString(3, resumePath);
			preparedStatement.setInt(4, starId);

			isupdated = preparedStatement.executeUpdate();
			// System.out.println("isdeleted"+isupdated);
		} catch (SQLException se) {
			se.printStackTrace();
			throw new ServiceLocatorException(se);
		} finally {
			try {
				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}

				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException se) {
				throw new ServiceLocatorException(se);
			}
		}

		return isupdated;

	}

	public List getAllTickets(int starId) throws ServiceLocatorException {

		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;
		String fileLocation = "";
		List<String> files = new ArrayList<String>();
		try {
			// System.out.println("in service Impl---->");
			connection = ConnectionProvider.getInstance().getConnection();
			statement = connection.createStatement();
			resultSet = statement
					.executeQuery("SELECT ResumePath  FROM tblStarPerformerLines WHERE ObjectId=" + starId);
			while (resultSet.next()) {
				fileLocation = resultSet.getString("ResumePath");
				if (!"".equals(fileLocation.trim())) {
					// System.out.println("fileLocation.trim()"+fileLocation.trim());
					files.add(fileLocation);
					// fileName = resultSet.getString("FileName");}
				}
			}

		} catch (SQLException se) {
			throw new ServiceLocatorException(se);
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (statement != null) {
					statement.close();
					statement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException sqle) {
				throw new ServiceLocatorException(sqle);
			}
		}
		// System.out.println("fileLocation is---->"+fileLocation);
		return files;

	}

	public String getStarMovieTicketLocation(int starId) throws ServiceLocatorException {

		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;
		String fileLocation = "";
		String fileName = "";
		try {
			// System.out.println("in service Impl---->");
			connection = ConnectionProvider.getInstance().getConnection();
			statement = connection.createStatement();
			resultSet = statement.executeQuery("SELECT ResumePath  FROM tblStarPerformerLines WHERE Id=" + starId);
			if (resultSet.next()) {
				fileLocation = resultSet.getString("ResumePath");
				// fileName = resultSet.getString("FileName");
			}

		} catch (SQLException se) {
			throw new ServiceLocatorException(se);
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (statement != null) {
					statement.close();
					statement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException sqle) {
				throw new ServiceLocatorException(sqle);
			}
		}
		// System.out.println("fileLocation is---->"+fileLocation);
		return fileLocation;

	}

	public String getStarPerformerLocation1(int starId, int empId) throws ServiceLocatorException {

		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;
		String fileLocation = "";
		String fileName = "";
		try {
			// System.out.println("in service Impl---->");
			connection = ConnectionProvider.getInstance().getConnection();
			statement = connection.createStatement();
			resultSet = statement.executeQuery("SELECT ResumePath  FROM tblStarPerformerLines WHERE Id=" + starId
					+ " And tblStarPerformerLines.EmpId=" + empId);
			if (resultSet.next()) {
				fileLocation = resultSet.getString("ResumePath");
				// fileName = resultSet.getString("FileName");
			}

		} catch (SQLException se) {
			throw new ServiceLocatorException(se);
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (statement != null) {
					statement.close();
					statement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException sqle) {
				throw new ServiceLocatorException(sqle);
			}
		}
		// System.out.println("fileLocation is---->"+fileLocation);
		return fileLocation;

	}

	public int isEligibleForAppraisal(int empId, int year, String quarterly) throws ServiceLocatorException {
		int recordId = 0;
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;

		try {
			/*
			 * GregorianCalendar cal = new GregorianCalendar(); int
			 * year=cal.get(Calendar.YEAR); int month=cal.get(Calendar.MONTH)+1;
			 * double count=Math.ceil((double)month/3); int totalCount=(int)
			 * count; String Quarterly="Q"+totalCount;
			 */
			int number = Integer.parseInt(quarterly.charAt(1) + "");

			String quarterDate = "";
			if (number == 1 || number == 4)
				quarterDate = year + "-" + (number * 3) + "-31";
			else
				quarterDate = year + "-" + (number * 3) + "-30";
			String queryString = "SELECT Id FROM tblEmployee WHERE curStatus='Active' and Id=" + empId
					+ " AND  HireDate IS NOT NULL AND HireDate!='1950-01-31' AND DATE(DATE_ADD(HireDate,INTERVAL 60 DAY ))<=DATE('"
					+ quarterDate + "')";
			// System.out.println(queryString);
			connection = ConnectionProvider.getInstance().getConnection();
			statement = connection.createStatement();
			resultSet = statement.executeQuery(queryString);
			if (resultSet.next()) {

				recordId = 1;
			}

		} catch (SQLException se) {
			throw new ServiceLocatorException(se);
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (statement != null) {
					statement.close();
					statement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException se) {
				throw new ServiceLocatorException(se);
			}
		}
		return recordId;

	}

	public int isPreviousQReviewAdded(int empId, int year, String quarterly) throws ServiceLocatorException {
		int recordId = 0;
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;

		try {

			char ch = quarterly.charAt(1);
			String Quarterly = "";
			switch (ch) {
			case '1':
				Quarterly = "Q4";
				year = year - 1;
				break;
			case '2':
				Quarterly = "Q1";
				break;
			case '3':
				Quarterly = "Q2";
				break;
			case '4':
				Quarterly = "Q3";
			}
			String queryString = "SELECT id FROM tblQuarterlyAppraisals WHERE EmpId=" + empId
					+ " AND QYear=" + year + " AND Quarterly='" + Quarterly + "'";
			// System.out.println(queryString);
			connection = ConnectionProvider.getInstance().getConnection();
			statement = connection.createStatement();
			resultSet = statement.executeQuery(queryString);
			if (resultSet.next()) {

				recordId = 1;
			} else {
				recordId = isEligibleForAppraisal(empId, year, Quarterly);
				if (recordId == 1) {
					recordId = 0;
				} else {
					recordId = 1;
				}
			}

		} catch (SQLException se) {
			throw new ServiceLocatorException(se);
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (statement != null) {
					statement.close();
					statement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException se) {
				throw new ServiceLocatorException(se);
			}
		}
		return recordId;

	}

	/*
	 * Author: Anand Patnaik Date : 18-08-2017
	 * 
	 * @see com.mss.mirage.employee.general.EmployeeService#
	 * employeeCurrentStatusDetails(int,int,String)
	 */
	public String employeeCurrentStatusDetailsForQreview(int empId, int year, String quarterly)
			throws ServiceLocatorException {

		Connection connection = null;
		PreparedStatement preparedStatement = null;
		CallableStatement callableStatement = null;
		ResultSet resultSet = null;
		String resutMessage = "";
		boolean isInserted = false;

		try {

			String startDate = "01/04/1950";
			String endDate = "";
			char ch = quarterly.charAt(1);
			String Quarterly = "";
			switch (ch) {
			case '1':
				startDate = "01/01/" + year;
				endDate = "03/31/" + year;
				break;
			case '2':
				startDate = "04/01/" + year;
				endDate = "06/30/" + year;
				break;
			case '3':
				startDate = "07/01/" + year;
				endDate = "09/30/" + year;
				break;
			case '4':
				startDate = "10/01/" + year;
				endDate = "12/31/" + year;

			}
			String queryString = "{call spGetCurStatusForQReview(?,?,?,?)}";
			connection = ConnectionProvider.getInstance().getConnection();

			callableStatement = connection.prepareCall(queryString);
			// System.out.println("empId---"+empId);
			// System.out.println(startDate+"
			// startDate---"+DateUtility.getInstance().convertStringToMySQLDate(startDate));
			// System.out.println(endDate+"
			// endDate---"+DateUtility.getInstance().convertStringToMySQLDate(endDate));
			callableStatement.setInt(1, empId);
			callableStatement.setString(2, DateUtility.getInstance().convertStringToMySQLDate(startDate));
			callableStatement.setString(3, DateUtility.getInstance().convertStringToMySQLDate(endDate));
			callableStatement.registerOutParameter(4, Types.VARCHAR);
			callableStatement.executeUpdate();
			resutMessage = callableStatement.getString(4);

		} catch (SQLException se) {
			se.printStackTrace();
			// resutMessage = "<font color=red>" + se.getMessage() + "</font>";
			throw new ServiceLocatorException(se);
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (preparedStatement != null) {
					preparedStatement.close();
					preparedStatement = null;
				}
				if (connection != null) {
					connection.close();
					connection = null;
				}
			} catch (SQLException se) {
				se.printStackTrace();
				throw new ServiceLocatorException(se);
			}
		}
		return resutMessage;
	}

	@Override
	public String addEmpTransfers(EmployeeAction employeeAction,int empId,String userId,String transferType,String empno) throws ServiceLocatorException {
		
		
		 Connection connection = null;
	     PreparedStatement preparedStatement = null;
	     ResultSet resultSet = null;
	     int isInserted = 0;
	     String resultString = "";
	     
	     Statement statement=null;
	    
	     
	   //  String queryString = "INSERT INTO " + TABLE_EMP_STATE_HISTORY + "(EmpId,LoginId,State,StartDate,EndDate,SkillSet,CreatedDate,PrjName,Utilization,Comments,CreatedBy,ProjectStatus,ResumePath,ResumeName) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	     String queryString = "INSERT INTO tblEmpLocationTransferDetails(EmpId,EmpName,DepartmentId,Title,STATUS,InitiatedOn,FromLocation,ToLocation,ReasonForTransfer,HostelDues,IsKeysHandover,CafteriaDues,TransportationDues,DuesRemarks,ITTeamStatus,ITTeamComments,ReportedTo,TentativeReportedDate,IsReported,NextLocationHRComments,ActualReportedDate,CreatedBy,CreatedDate,Accommodation,RoomNo,RoomFee,Cafeteria,CafeteriaFee,Transportation,TransLocation,TransFee,OccupancyType,DateOfOccupancy,ElecrricalCharges,NeedTransport,TransferType,EmpNo) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	     try {
	         connection = ConnectionProvider.getInstance().getConnection();
	         preparedStatement = connection.prepareStatement(queryString);
	         preparedStatement.setInt(1, employeeAction.getEmpId());
	         preparedStatement.setString(2, employeeAction.getEmpName());
	         if(employeeAction.getDepartmentId() == null || employeeAction.getDepartmentId().equals("")){
	         preparedStatement.setString(3, "");
	         }else{
	        	 preparedStatement.setString(3, employeeAction.getDepartmentId());	 
	         }
	         if(employeeAction.getTitleId() == null || employeeAction.getTitleId().equals("")){
	         preparedStatement.setString(4, "");
	         }else{
	        	 preparedStatement.setString(4, employeeAction.getTitleId()); 
	         }
	         preparedStatement.setString(5, employeeAction.getStatus());
	         preparedStatement.setString(6, DateUtility.getInstance().convertStringToMySQLDate(employeeAction.getInitiatedOn()));
	         
	         if(transferType.equals("CountryTransfer"))
	         {
	         preparedStatement.setString(7, employeeAction.getFromLocationnew());
	         }else
	         {
	        	  preparedStatement.setString(7, employeeAction.getFromLocation());
	         }
	         if(transferType.equals("CountryTransfer"))
	         {
	         preparedStatement.setString(8, employeeAction.getToLocationnew());
	         }
	         else
	         {
	        	  preparedStatement.setString(8, employeeAction.getToLocation()); 
	         }
	         if(employeeAction.getReasonsForTransfer() == null || employeeAction.getReasonsForTransfer().equals("") ){
	             preparedStatement.setString(9, "");
	             }else{
	         preparedStatement.setString(9, employeeAction.getReasonsForTransfer());
	             }
	         
	         if(employeeAction.getHostelDues() == null || employeeAction.getHostelDues().equals("")){
	             preparedStatement.setString(10, "");
	             }else{
	         preparedStatement.setString(10, employeeAction.getHostelDues());
	             }
	         if(employeeAction.getIsKeysHandover() == null || employeeAction.getIsKeysHandover().equals("")){
	             preparedStatement.setInt(11, 0);
	             }else{
	         if(employeeAction.getIsKeysHandover())
	         preparedStatement.setInt(11, 1);
	         else
	        	 preparedStatement.setInt(11, 0);
	             }
	         if(employeeAction.getCafteriaDues() == null || employeeAction.getCafteriaDues().equals("")){
	             preparedStatement.setString(12, "");
	             }else{
	         preparedStatement.setString(12, employeeAction.getCafteriaDues());
	             }
	      

	         if(employeeAction.getTransportationDues() == null || employeeAction.getTransportationDues().equals("")){
	             preparedStatement.setString(13, "");
	             }else{
	         preparedStatement.setString(13, employeeAction.getTransportationDues());
	             }
	         if(employeeAction.getDuesRemarks() == null || employeeAction.getDuesRemarks().equals("")){
	             preparedStatement.setString(14, "");
	             }else{
	         preparedStatement.setString(14, employeeAction.getDuesRemarks());
	             }
	         if(employeeAction.getItTeamStatus() == null || employeeAction.getItTeamStatus().equals("")){
	             preparedStatement.setString(15, "");
	             }else{
	         preparedStatement.setString(15, employeeAction.getItTeamStatus());
	             }
	         if(employeeAction.getItTeamComments() == null || employeeAction.getItTeamComments().equals("")){
	             preparedStatement.setString(16, "");
	             }else{
	         preparedStatement.setString(16, employeeAction.getItTeamComments());
	             }
	         if(transferType.equals("CountryTransfer") && employeeAction.getToLocationnew().equals("USA"))
	         {
	        preparedStatement.setString(17, employeeAction.getReportedToNew());
	         }
	         else
	         {
	        	 preparedStatement.setString(17, employeeAction.getReportedTo());
	         }
	        
	         
	         if(transferType.equals("CountryTransfer") && employeeAction.getToLocationnew().equals("USA"))
	         {
	        	 if(employeeAction.getTentativeReportedDateNew() == null || employeeAction.getTentativeReportedDateNew().equals("")){
		             preparedStatement.setDate(18, null);
		             }else{
		         preparedStatement.setString(18, DateUtility.getInstance().convertStringToMySQLDate(employeeAction.getTentativeReportedDateNew()));
		         }
	         }else
	         {
	         if(employeeAction.getTentativeReportedDate() == null || employeeAction.getTentativeReportedDate().equals("")){
	             preparedStatement.setDate(18, null);
	             }else{
	         preparedStatement.setString(18, DateUtility.getInstance().convertStringToMySQLDate(employeeAction.getTentativeReportedDate()));
	         }
	         }
	         
	         if(employeeAction.getIsReported() == null || employeeAction.getIsReported().equals("")){
	             preparedStatement.setInt(19, 0);
	             }else{
	         if(employeeAction.getIsReported())
	         preparedStatement.setInt(19, 1);
	         else
	        	 preparedStatement.setInt(19, 0);
	             }
	         if(employeeAction.getNextLocationHRComments() == null || employeeAction.getNextLocationHRComments().equals("")){
	             preparedStatement.setString(20, "");
	             }else{
	         preparedStatement.setString(20, employeeAction.getNextLocationHRComments());
	             }
	         if(employeeAction.getActualReportedDate() != null && !employeeAction.getActualReportedDate().equals("")){
	             preparedStatement.setString(21, DateUtility.getInstance().convertStringToMySQLDate(employeeAction.getActualReportedDate()));
	             
	             }else{
	            	 preparedStatement.setDate(21, null);
	                 
	             }
	         preparedStatement.setString(22, userId);
	         preparedStatement.setString(23, DateUtility.getInstance().getCurrentSQLDate());
	         
	         if(employeeAction.getAccommodation() != null && !employeeAction.getAccommodation().equals("")){
	             preparedStatement.setString(24, employeeAction.getAccommodation());
	             
	             }else{
	            	 preparedStatement.setString(24, null);
	                 
	             }
	         if(employeeAction.getRoomNo() != 0 ){
	             preparedStatement.setInt(25, employeeAction.getRoomNo());
	             
	             }else{
	            	 preparedStatement.setInt(25, 0);
	                 
	             }
	         if(employeeAction.getRoomFee() != 0.0 ){
	             preparedStatement.setDouble(26, employeeAction.getRoomFee());
	             
	             }else{
	            	 preparedStatement.setDouble(26, 0.0);
	                 
	             }
	         if(employeeAction.getCafeteria() != null && !employeeAction.getCafeteria().equals("")){
	             preparedStatement.setString(27, employeeAction.getCafeteria());
	             
	             }else{
	            	 preparedStatement.setString(27, null);
	                 
	             }
	         if(employeeAction.getCafeteriaFee() != 0.0 ){
	             preparedStatement.setDouble(28, employeeAction.getCafeteriaFee());
	             
	             }else{
	            	 preparedStatement.setDouble(28, 0.0);
	                 
	             }
	        
	         if(employeeAction.getTransportation() != null && !employeeAction.getTransportation().equals("")){
	             preparedStatement.setString(29, employeeAction.getTransportation());
	             
	             }else{
	            	 preparedStatement.setString(29, null);
	                 
	             }
	         if(employeeAction.getTransportLocation() != null && !employeeAction.getTransportLocation().equals("")){
	             preparedStatement.setString(30, employeeAction.getTransportLocation());
	             
	             }else{
	            	 preparedStatement.setString(30, null);
	                 
	             }
	         
	         if(employeeAction.getTransportFee() != 0.0 ){
	             preparedStatement.setDouble(31, employeeAction.getTransportFee());
	             
	             }else{
	            	 preparedStatement.setDouble(31, 0.0);
	                 
	             }
	         if(employeeAction.getOccupancyType() != null && !employeeAction.getOccupancyType().equals("")){
	             preparedStatement.setString(32, employeeAction.getOccupancyType());
	             
	             }else{
	            	 preparedStatement.setString(32, null);
	                 
	             }
	         
	         if(employeeAction.getDateOfOccupancy() != null && !employeeAction.getDateOfOccupancy().equals("") && !"".equals(employeeAction.getDateOfOccupancy().trim())){
	             preparedStatement.setString(33, DateUtility.getInstance().convertStringToMySQLDate(employeeAction.getDateOfOccupancy()));
	             
	             }else{
	            	 preparedStatement.setDate(33, null);
	                 
	             }
	         if(employeeAction.getElectricalCharges() != 0.0){
	             preparedStatement.setDouble(34, employeeAction.getElectricalCharges());
	             
	             }else{
	            	 preparedStatement.setDouble(34, 0.00);
	                 
	             }
	        
	         if(employeeAction.isNeedTransport())
	         preparedStatement.setInt(35, 1);
	         else
	         preparedStatement.setInt(35, 0);
	         
	             if(transferType != null && !"".equals(transferType))
	             {
	            	 preparedStatement.setString(36,transferType);
	             }
	         
	           if(empno != null && !"".equals(empno))
	             {
	            	 preparedStatement.setString(37,empno);
	             }
	     
	          String createdBy = DataSourceDataProvider.getInstance().getemployeenamebyloginId(userId);
	          String empNo = DataSourceDataProvider.getInstance().getEmpNoByEmpId(employeeAction.getEmpId());
	          String opsContact = DataSourceDataProvider.getInstance().getEmailIdForLoginId(userId);
	          String reportedTo =  DataSourceDataProvider.getInstance().getEmailIdForLoginId(employeeAction.getReportedTo());
	          String empEmail = DataSourceDataProvider.getInstance().getEmailIdForEmployee(employeeAction.getEmpId());
	          
	         isInserted = preparedStatement.executeUpdate();
	         
	       
	   
	         
	         if(isInserted == 1){
	        	
	               if (transferType.equalsIgnoreCase("CountryTransfer")) {
	            	
	            	   if(employeeAction.getIsReported())
	            	   {
	            	
		        	 String newLocation =employeeAction.getToLocationnew();
		        	 
		        
		        	  
						String query1 = "Update tblEmployee SET Country='"+newLocation+"',WorkingCountry='"+newLocation+"' WHERE Id=" + empId + "";

					
						statement = connection.createStatement();
						statement.executeUpdate(query1);
				   }
	         }
		        
		          
	        	 
	        	 resultString = "Added";
	        	 
	        	 MailManager.sendTransferLocationEmails(empEmail,createdBy,opsContact,reportedTo,employeeAction.getReportedTo(),employeeAction.getEmpName(),empNo,employeeAction.getDepartmentId(),employeeAction.getFromLocation(),employeeAction.getToLocation(),employeeAction.getItTeamComments(),"Add",employeeAction.getItTeamStatus(),0,employeeAction.getStatus());
	         }
	         
	         
	         
	         
	        
	      

	     } catch (SQLException se) {
	         se.printStackTrace();
	         throw new ServiceLocatorException(se);
	     } finally {
	         try {
	             if (preparedStatement != null) {
	                 preparedStatement.close();
	                 preparedStatement = null;
	             }
	             if (connection != null) {
	                 connection.close();
	                 connection = null;
	             }
	         } catch (SQLException se) {
	             throw new ServiceLocatorException(se);
	         }
	     }

	     return resultString;

	}


	@Override
	public String getEmpTransferDetails(int empId, EmployeeAction employeeAction) throws ServiceLocatorException {
		 Connection connection = null;
	     PreparedStatement preparedStatement = null;
	     ResultSet resultSet = null;
	     boolean isInserted = false;
	     String resultString = "";
	    

	   //  String queryString = "INSERT INTO " + TABLE_EMP_STATE_HISTORY + "(EmpId,LoginId,State,StartDate,EndDate,SkillSet,CreatedDate,PrjName,Utilization,Comments,CreatedBy,ProjectStatus,ResumePath,ResumeName) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	     String queryString = "SELECT EmpName,DepartmentId,Title,STATUS,InitiatedOn,FromLocation,ToLocation,ReasonForTransfer,HostelDues,IsKeysHandover,CafteriaDues,TransportationDues,DuesRemarks,ITTeamStatus,ITTeamComments,ReportedTo,TentativeReportedDate,IsReported,NextLocationHRComments,ActualReportedDate,CreatedBy,Accommodation,RoomNo,RoomFee,Cafeteria,CafeteriaFee,Transportation,TransLocation,TransFee,OccupancyType,DateOfOccupancy,ElecrricalCharges,NeedTransport,TransferType FROM tblEmpLocationTransferDetails WHERE EmpId ="+empId+" AND Id="+employeeAction.getId();
	     try {
	         connection = ConnectionProvider.getInstance().getConnection();
	         preparedStatement = connection.prepareStatement(queryString);
	         resultSet = preparedStatement.executeQuery();
	         
	         if(resultSet.next()){
	         employeeAction.setEmpName(resultSet.getString("EmpName"));
	         if( resultSet.getString("DepartmentId") == null || resultSet.getString("DepartmentId").equals("")){
	           employeeAction.setDepartmentId("");
	         }else{
	        	 employeeAction.setDepartmentId(resultSet.getString("DepartmentId")); 
	          }
	         if( resultSet.getString("Title") == null || resultSet.getString("Title").equals("")){
	          employeeAction.setTitleId("");
	         }else{
	        	 employeeAction.setTitleId(resultSet.getString("Title")); 
	         }
	         if( resultSet.getString("STATUS") == null || resultSet.getString("STATUS").equals("")){
	             employeeAction.setStatus("");
	            }else{
	         employeeAction.setStatus(resultSet.getString("STATUS"));
	            }
	         if( resultSet.getString("InitiatedOn") == null || resultSet.getString("InitiatedOn").equals("")){
	             employeeAction.setInitiatedOn("");
	            }else{
	         employeeAction.setInitiatedOn(DateUtility.getInstance().convertToviewFormat(resultSet.getString("InitiatedOn")));
	            }
	         
	         
	         String transferType=resultSet.getString("TransferType");
	       
	         if(resultSet.getString("TransferType").equals("CountryTransfer"))
	         {
	        	 
	        	
	        	  if( resultSet.getString("FromLocation") == null || resultSet.getString("FromLocation").equals("")){
	 	           
	 	             employeeAction.setFromLocationnew("");
	 	            }else{
	 	        
	 	         employeeAction.setFromLocationnew(resultSet.getString("FromLocation"));

	 	            } 
	         }else
	         {
	        	 if( resultSet.getString("FromLocation") == null || resultSet.getString("FromLocation").equals("")){
		 	           
	 	             employeeAction.setFromLocation("");
	 	            }else{
	 	        
	 	         employeeAction.setFromLocation(resultSet.getString("FromLocation"));

	 	            } 
	         }
	         if(resultSet.getString("TransferType").equals("CountryTransfer"))
	         {
	        
	         if(resultSet.getString("ToLocation") == null || resultSet.getString("ToLocation").equals("")){
	           
	             employeeAction.setToLocationnew("");
	            }else{
	       
	         employeeAction.setToLocationnew(resultSet.getString("ToLocation"));
	            }
	         }else
	         {

		         if( resultSet.getString("ToLocation") == null || resultSet.getString("ToLocation").equals("")){
		             employeeAction.setToLocation("");
		            
		            }else{
		         employeeAction.setToLocation(resultSet.getString("ToLocation"));
		        
		            }
	         }
	         if( resultSet.getString("ReasonForTransfer") == null || resultSet.getString("ReasonForTransfer").equals("")){
	             employeeAction.setReasonsForTransfer("");
	            }else{
	         employeeAction.setReasonsForTransfer(resultSet.getString("ReasonForTransfer"));
	            }
	         if( resultSet.getString("HostelDues") == null || resultSet.getString("HostelDues").equals("")){
	             employeeAction.setHostelDues("");
	            }else{
	         employeeAction.setHostelDues(resultSet.getString("HostelDues"));
	            }
	         if(resultSet.getInt("IsKeysHandover") == 1)
	         employeeAction.setIsKeysHandover(true);
	         else
	         employeeAction.setIsKeysHandover(false);	 
	         if( resultSet.getString("CafteriaDues") == null || resultSet.getString("CafteriaDues").equals("")){
	             employeeAction.setCafteriaDues("");
	            }else{
	         employeeAction.setCafteriaDues(resultSet.getString("CafteriaDues"));
	            }
	         if( resultSet.getString("TransportationDues") == null || resultSet.getString("TransportationDues").equals("")){
	             employeeAction.setTransportationDues("");
	            }else{
	         employeeAction.setTransportationDues(resultSet.getString("TransportationDues"));
	            }
	         if( resultSet.getString("DuesRemarks") == null || resultSet.getString("DuesRemarks").equals("")){
	             employeeAction.setDuesRemarks("");
	            }else{
	         employeeAction.setDuesRemarks(resultSet.getString("DuesRemarks"));
	            }
	         if( resultSet.getString("ITTeamStatus") == null || resultSet.getString("ITTeamStatus").equals("")){
	             employeeAction.setItTeamStatus("");
	            }else{
	         employeeAction.setItTeamStatus(resultSet.getString("ITTeamStatus"));
	            }
	         if(resultSet.getString("ITTeamComments") == null || resultSet.getString("ITTeamComments").equals("")){
	             employeeAction.setItTeamComments("");
	            }else{
	         employeeAction.setItTeamComments(resultSet.getString("ITTeamComments"));
	            }
	        String ReportedTo=resultSet.getString("ReportedTo") ;
	      //  System.out.println("ReportedTo"+ReportedTo);
	        
	        
	        if(resultSet.getString("TransferType").equals("CountryTransfer") && resultSet.getString("ToLocation").equals("USA"))
	         {
	        
	         if( resultSet.getString("ReportedTo") == null || resultSet.getString("ReportedTo").equals("")){
	             employeeAction.setReportedToNew("");
	            }else{
	         employeeAction.setReportedToNew(resultSet.getString("ReportedTo"));
	            }
	         }else
	         {
	        	 
		         if( resultSet.getString("ReportedTo") == null || resultSet.getString("ReportedTo").equals("")){
		             employeeAction.setReportedTo("");
		            }else{
		         employeeAction.setReportedTo(resultSet.getString("ReportedTo"));
		            }
	         }
	        
	        
	         if(resultSet.getString("TransferType").equals("CountryTransfer") && resultSet.getString("ToLocation").equals("USA"))
	         {
	        
	         if( resultSet.getString("TentativeReportedDate") == null || resultSet.getString("TentativeReportedDate").equals("")){
	             employeeAction.setTentativeReportedDateNew("");
	            }else{
	         employeeAction.setTentativeReportedDateNew(DateUtility.getInstance().convertToviewFormat(resultSet.getString("TentativeReportedDate")));
	            }
	         }else
	         {
	        	 
		         if( resultSet.getString("TentativeReportedDate") == null || resultSet.getString("TentativeReportedDate").equals("")){
		             employeeAction.setTentativeReportedDate("");
		            }else{
		         employeeAction.setTentativeReportedDate(DateUtility.getInstance().convertToviewFormat(resultSet.getString("TentativeReportedDate")));
		            }
	         }
	         
	         if(resultSet.getInt("IsReported") == 1)
	         employeeAction.setIsReported(true);
	         else
	        	 employeeAction.setIsReported(false);	 
	         if( resultSet.getString("NextLocationHRComments") == null || resultSet.getString("NextLocationHRComments").equals("")){
	             employeeAction.setNextLocationHRComments("");
	            }else{
	         employeeAction.setNextLocationHRComments(resultSet.getString("NextLocationHRComments"));
	            }
	         if( resultSet.getString("ActualReportedDate") == null || resultSet.getString("ActualReportedDate").equals("")){
	             employeeAction.setActualReportedDate("");
	            }else{
	         employeeAction.setActualReportedDate(DateUtility.getInstance().convertToviewFormat(resultSet.getString("ActualReportedDate")));
	            }
	         if(resultSet.getString("CreatedBy") == null || resultSet.getString("CreatedBy").equals("")){
	             employeeAction.setCreatedBy("");
	            }else{
	         employeeAction.setCreatedBy(resultSet.getString("CreatedBy"));
	            }
	         if( resultSet.getString("Accommodation") == null || resultSet.getString("Accommodation").equals("")){
	             employeeAction.setAccommodation("");
	            }else{
	         employeeAction.setAccommodation(resultSet.getString("Accommodation"));
	            }
	         
	         if( resultSet.getInt("RoomNo") == 0 ){
	             employeeAction.setRoomNo(0);
	            }else{
	         employeeAction.setRoomNo(resultSet.getInt("RoomNo"));
	            }
	         if( resultSet.getDouble("RoomFee") == 0.0){
	             employeeAction.setRoomFee(0.0);
	            }else{
	         employeeAction.setRoomFee(resultSet.getDouble("RoomFee"));
	            }
	         
	         if( resultSet.getString("Cafeteria") == null || resultSet.getString("Cafeteria").equals("")){
	             employeeAction.setCafeteria("");
	            }else{
	         employeeAction.setCafeteria(resultSet.getString("Cafeteria"));
	            }
	         if( resultSet.getDouble("CafeteriaFee") == 0 ){
	             employeeAction.setCafeteriaFee(0.0);
	            }else{
	         employeeAction.setCafeteriaFee(resultSet.getDouble("CafeteriaFee"));
	            }
	         if( resultSet.getString("Transportation") == null || resultSet.getString("Transportation").equals("")){
	             employeeAction.setTransportation("");
	            }else{
	         employeeAction.setTransportation(resultSet.getString("Transportation"));
	            }
	         if( resultSet.getString("TransLocation") == null || resultSet.getString("TransLocation").equals("")){
	             employeeAction.setTransportLocation("");
	            }else{
	         employeeAction.setTransportLocation(resultSet.getString("TransLocation"));
	            }
	         if( resultSet.getDouble("TransFee") == 0 ){
	             employeeAction.setTransportFee(0);
	            }else{
	         employeeAction.setTransportFee(resultSet.getDouble("TransFee"));
	            }
	         if( resultSet.getString("OccupancyType") == null || resultSet.getString("OccupancyType").equals("")){
	             employeeAction.setOccupancyType("");
	            }else{
	         employeeAction.setOccupancyType(resultSet.getString("OccupancyType"));
	            }
	         
	         if( resultSet.getString("DateOfOccupancy") == null || resultSet.getString("DateOfOccupancy").equals("")){
	             employeeAction.setDateOfOccupancy("");
	            }else{
	         employeeAction.setDateOfOccupancy(DateUtility.getInstance().convertToviewFormat(resultSet.getString("DateOfOccupancy")));
	            }
	         if( resultSet.getDouble("ElecrricalCharges") == 0 ){
	             employeeAction.setElectricalCharges(0);
	            }else{
	         employeeAction.setElectricalCharges(resultSet.getDouble("ElecrricalCharges"));
	            }
	         if(resultSet.getInt("NeedTransport") == 1)
	             employeeAction.setNeedTransport(true);
	             else
	             employeeAction.setNeedTransport(false);	 
	         if( resultSet.getString("TransferType") == null || resultSet.getString("TransferType").equals("")){
	             employeeAction.setTransferType("");
	            }else{
	         employeeAction.setTransferType(resultSet.getString("TransferType"));
	            }
	         
	         
	         }
	         	         
	         else{
	        	 resultString = "NoData";
//	        	 employeeAction.setEmpName("");
//	        	 employeeAction.setDepartmentId("");
//	             employeeAction.setTitleId("");
//	             employeeAction.setStatus("");
//	            // employeeAction.setInitiatedOn("");
//	             employeeAction.setFromLocation("");
//	             employeeAction.setToLocation("");
//	             employeeAction.setReasonsForTransfer("");
//	             employeeAction.setHostelDues("");
//	             employeeAction.setIsKeysHandover(0);
//	             employeeAction.setCafteriaDues("");
//	             employeeAction.setTransportationDues("");
//	             employeeAction.setDuesRemarks("");
//	             employeeAction.setItTeamStatus("");
//	             employeeAction.setItTeamComments("");
//	             employeeAction.setReportedTo("");
//	          //   employeeAction.setReportedDate("");
//	             employeeAction.setIsReported(0);
//	             employeeAction.setNextLocationHRComments("");
	         }
	         
	     } catch (SQLException se) {
	         se.printStackTrace();
	         throw new ServiceLocatorException(se);
	     } finally {
	         try {
	        	 if (resultSet != null) {
	                 resultSet.close();
	                 resultSet = null;
	             }
	             if (preparedStatement != null) {
	                 preparedStatement.close();
	                 preparedStatement = null;
	             }
	             if (connection != null) {
	                 connection.close();
	                 connection = null;
	             }
	         } catch (SQLException se) {
	             throw new ServiceLocatorException(se);
	         }
	     }

	     return resultString;

	}


	public String updateEmpTransfers(EmployeeAction employeeAction,int empId,String userId,String mailFlag,String transferType) throws ServiceLocatorException{
		 Connection connection = null;
	     PreparedStatement preparedStatement = null;
	     Statement statement = null;
	     int isupdated = 0;
	     String result = "";
	    
	     // System.out.println("doStarPerformersAdd");
	   
	      String queryString = "UPDATE tblEmpLocationTransferDetails SET STATUS=?,InitiatedOn=?,FromLocation=?,ToLocation=?,ReasonForTransfer=?,HostelDues=?,IsKeysHandover=?,CafteriaDues=?,TransportationDues=?,DuesRemarks=?,ITTeamStatus=?,ITTeamComments=?,ReportedTo=?,TentativeReportedDate=?,IsReported=?,NextLocationHRComments=?,ModifiedBy=?,ModifiedDate=?,ActualReportedDate=?,Accommodation=?,RoomNo=?,RoomFee=?,Cafeteria=?,CafeteriaFee=?,Transportation=?,TransLocation=?,TransFee=?,OccupancyType=?,DateOfOccupancy=?,ElecrricalCharges=?,NeedTransport=?  where EmpId ="+empId+" AND Id="+employeeAction.getId();
	    try {
	            connection = ConnectionProvider.getInstance().getConnection();
	         preparedStatement = connection.prepareStatement(queryString); 
	      
	         
	         
	       //  System.out.println(employeeAction.getItTeamStatus()+"employeeAction.getItTeamStatus()"+employeeAction.getStatus());
	         preparedStatement.setString(1, employeeAction.getStatus());
	         preparedStatement.setString(2, DateUtility.getInstance().convertStringToMySQLDate(employeeAction.getInitiatedOn()));
	         if(transferType.equals("CountryTransfer")){
	        	 preparedStatement.setString(3, employeeAction.getFromLocationnew());
		         preparedStatement.setString(4, employeeAction.getToLocationnew());
	         }else
	         {
	         preparedStatement.setString(3, employeeAction.getFromLocation());
	         preparedStatement.setString(4, employeeAction.getToLocation());
	         }
	         if( employeeAction.getReasonsForTransfer() == null || employeeAction.getReasonsForTransfer().equals("")){
	             preparedStatement.setString(5, "");
	             }else{
	         preparedStatement.setString(5, employeeAction.getReasonsForTransfer());
	             }
	         if( employeeAction.getHostelDues() == null || employeeAction.getHostelDues().equals("")){
	             preparedStatement.setString(6, "");
	             }else{
	         preparedStatement.setString(6, employeeAction.getHostelDues());
	             }
	         if( employeeAction.getIsKeysHandover() == null || employeeAction.getIsKeysHandover().equals("")){
	        	 preparedStatement.setInt(7, 0);	
	         }else{
	         if(employeeAction.getIsKeysHandover())
	         preparedStatement.setInt(7, 1);
	         else
	        	 preparedStatement.setInt(7, 0);	
	         }
	         if(employeeAction.getCafteriaDues() == null || employeeAction.getCafteriaDues().equals("")){
	             preparedStatement.setString(8, "");
	             }else{
	         preparedStatement.setString(8, employeeAction.getCafteriaDues());
	             }
	         if(employeeAction.getTransportationDues() == null || employeeAction.getTransportationDues().equals("")){
	             preparedStatement.setString(9, "");
	             }else{
	         preparedStatement.setString(9, employeeAction.getTransportationDues());
	             }
	         if(employeeAction.getDuesRemarks() == null || employeeAction.getDuesRemarks().equals("")){
	             preparedStatement.setString(10, "");
	             }else{
	         preparedStatement.setString(10, employeeAction.getDuesRemarks());
	             }
	         if(employeeAction.getItTeamStatus() == null || employeeAction.getItTeamStatus().equals("") ){
	             preparedStatement.setString(11, "");
	             }else{
	         preparedStatement.setString(11, employeeAction.getItTeamStatus());
	             }
	         if(employeeAction.getItTeamComments() == null || employeeAction.getItTeamComments().equals("")  ){
	             preparedStatement.setString(12, "");
	             }else{
	         preparedStatement.setString(12, employeeAction.getItTeamComments());
	             }
	         
	         if(transferType.equals("CountryTransfer") && employeeAction.getToLocationnew().equals("USA"))
	         {
	         if( employeeAction.getReportedToNew() == null || employeeAction.getReportedToNew().equals("")){
	        	 preparedStatement.setString(13, "");
	         }else{
	        	 preparedStatement.setString(13, employeeAction.getReportedToNew()); 
	         }
	         }
	         else
	         {
	        	   if( employeeAction.getReportedTo() == null || employeeAction.getReportedTo().equals("")){
	  	        	 preparedStatement.setString(13, "");
	  	         }else{
	  	        	 preparedStatement.setString(13, employeeAction.getReportedTo()); 
	  	         } 
	         }
	         if(transferType.equals("CountryTransfer")  && employeeAction.getToLocationnew().equals("USA")){
	        	 if(employeeAction.getTentativeReportedDateNew() == null || employeeAction.getTentativeReportedDateNew().equals("")  ){
		        	 preparedStatement.setDate(14, null);
		             }else{
		         preparedStatement.setString(14, DateUtility.getInstance().convertStringToMySQLDate(employeeAction.getTentativeReportedDateNew()));
		             }
	         }
	         else
	         {
	        	 if(employeeAction.getTentativeReportedDate() == null || employeeAction.getTentativeReportedDate().equals("")  ){
		        	 preparedStatement.setDate(14, null);
		             }else{
		         preparedStatement.setString(14, DateUtility.getInstance().convertStringToMySQLDate(employeeAction.getTentativeReportedDate()));
		             }
	         }
	        
	         if(employeeAction.getIsReported() == null || employeeAction.getIsReported().equals("")  ){
	        	 preparedStatement.setInt(15, 0);
	             }else{
	         if(employeeAction.getIsReported())
	         preparedStatement.setInt(15, 1);
	         else
	        	 preparedStatement.setInt(15, 0);
	             }
	         if(employeeAction.getNextLocationHRComments() == null || employeeAction.getNextLocationHRComments().equals("")){
	             preparedStatement.setString(16, "");
	             }else{
	         preparedStatement.setString(16, employeeAction.getNextLocationHRComments());
	             }
	         preparedStatement.setString(17, userId);
	         preparedStatement.setString(18, DateUtility.getInstance().getCurrentSQLDate());
	         if(employeeAction.getActualReportedDate() == null || employeeAction.getActualReportedDate().equals("")  ){
	        	 preparedStatement.setDate(19, null);
	             }else{
	         preparedStatement.setString(19, DateUtility.getInstance().convertStringToMySQLDate(employeeAction.getActualReportedDate()));
	             }
	         
	         if(employeeAction.getAccommodation() != null && !employeeAction.getAccommodation().equals("")){
	             preparedStatement.setString(20, employeeAction.getAccommodation());
	             
	             }else{
	            	 preparedStatement.setString(20, null);
	                 
	             }
	         if(employeeAction.getRoomNo() != 0 ){
	             preparedStatement.setInt(21, employeeAction.getRoomNo());
	             
	             }else{
	            	 preparedStatement.setInt(21, 0);
	                 
	             }
	         if(employeeAction.getRoomFee() != 0.0 ){
	             preparedStatement.setDouble(22, employeeAction.getRoomFee());
	             
	             }else{
	            	 preparedStatement.setDouble(22, 0.0);
	                 
	             }
	         if(employeeAction.getCafeteria() != null && !employeeAction.getCafeteria().equals("")){
	             preparedStatement.setString(23, employeeAction.getCafeteria());
	             
	             }else{
	            	 preparedStatement.setString(23, null);
	                 
	             }
	         if(employeeAction.getCafeteriaFee() != 0.0 ){
	             preparedStatement.setDouble(24, employeeAction.getCafeteriaFee());
	             
	             }else{
	            	 preparedStatement.setDouble(24, 0.0);
	                 
	             }
	        
	         if(employeeAction.getTransportation() != null && !employeeAction.getTransportation().equals("")){
	             preparedStatement.setString(25, employeeAction.getTransportation());
	             
	             }else{
	            	 preparedStatement.setString(25, null);
	                 
	             }
	         if(employeeAction.getTransportLocation() != null && !employeeAction.getTransportLocation().equals("")){
	             preparedStatement.setString(26, employeeAction.getTransportLocation());
	             
	             }else{
	            	 preparedStatement.setString(26, null);
	                 
	             }
	         
	         if(employeeAction.getTransportFee() != 0.0 ){
	             preparedStatement.setDouble(27, employeeAction.getTransportFee());
	             
	             }else{
	            	 preparedStatement.setDouble(27, 0.0);
	                 
	             }
	         if(employeeAction.getOccupancyType() != null && !employeeAction.getOccupancyType().equals("")){
	             preparedStatement.setString(28, employeeAction.getOccupancyType());
	             
	             }else{
	            	 preparedStatement.setString(28, null);
	                 
	             }
	         
	         if(employeeAction.getDateOfOccupancy() != null && !employeeAction.getDateOfOccupancy().equals("")){
	             preparedStatement.setString(29, DateUtility.getInstance().convertStringToMySQLDate(employeeAction.getDateOfOccupancy()));
	             
	             }else{
	            	 preparedStatement.setDate(29, null);
	                 
	             }
	         if(employeeAction.getElectricalCharges() != 0.0){
	             preparedStatement.setDouble(30, employeeAction.getElectricalCharges());
	             
	             }else{
	            	 preparedStatement.setDouble(30, 0.00);
	                 
	             }
	         
	         
	         if(employeeAction.isNeedTransport())
	         preparedStatement.setInt(31, 1);
	         else
	        	 preparedStatement.setInt(31, 0);	
	         
	         
	         
	         
	         isupdated = preparedStatement.executeUpdate();
	         	         
	         String createdBy = DataSourceDataProvider.getInstance().getemployeenamebyloginId(userId);
	         String empNo = DataSourceDataProvider.getInstance().getEmpNoByEmpId(employeeAction.getEmpId());
	         String empEmail = DataSourceDataProvider.getInstance().getEmailIdForEmployee(employeeAction.getEmpId());
	         String opsContact = DataSourceDataProvider.getInstance().getEmailIdForLoginId(employeeAction.getCreatedBy());
	         String reportedTo =  DataSourceDataProvider.getInstance().getEmailIdForLoginId(employeeAction.getReportedTo());
	    	
	         if(isupdated == 1){
	        	 
	        	   if(transferType.equals("CountryTransfer"))
	  	         {
	  	         
	  	         if (employeeAction.getIsReported()) {
	  	        	 
	  	        	 String newLocation =employeeAction.getToLocationnew();
	  					String query1 = "Update tblEmployee Set Country='"+newLocation+"', WorkingCountry='"+newLocation+"' WHERE Id=" + empId + "";

	  					// System.out.println("query.."+query1);
	  					statement = connection.createStatement();
	  					statement.executeUpdate(query1);
	  			   }
	  	         }
	        	 
	        	 result = "Updated";
	        	 if(mailFlag.equals("ITTeam") || (mailFlag.equals("NextLocHR") && employeeAction.getStatus().equals("Completed"))){
	        	 MailManager.sendTransferLocationEmails(empEmail,createdBy,opsContact,reportedTo,employeeAction.getReportedTo(),employeeAction.getEmpName(),empNo,employeeAction.getDepartmentId(),employeeAction.getFromLocation(),employeeAction.getToLocation(),employeeAction.getItTeamComments(),mailFlag,employeeAction.getItTeamStatus(),employeeAction.getId(),employeeAction.getStatus());
	        	 }
	         }
	         
	      
	         
	       //   System.out.println("isupdated"+isupdated);
	     } catch (SQLException se) {
	         se.printStackTrace();
	         System.err.println("Message: " + se.getMessage());
	         System.err.println("SQLState: " +se.getSQLState());
	         System.err.println("Error Code: " +
	                 ((SQLException)se).getErrorCode());
	         throw new ServiceLocatorException(se);
	      //   

	           

	             
	     } finally {
	         try {
	             if (preparedStatement != null) {
	                 preparedStatement.close();
	                 preparedStatement = null;
	             }
	            
	             if (connection != null) {
	                 connection.close();
	                 connection = null;
	             }
	         } catch (SQLException se) {
	             throw new ServiceLocatorException(se);
	         }
	     }

	     return result;

	}
	
	public String updateEmpITTransfers(EmployeeAction employeeAction,int empId,String userId,String mailFlag) throws ServiceLocatorException{
		 Connection connection = null;
	     PreparedStatement preparedStatement = null;
	    
	     int isupdated = 0;
	     String result = "";
	    
	     // System.out.println("doStarPerformersAdd");
	   
	      String queryString = "UPDATE tblEmpLocationTransferDetails SET ITTeamStatus=?,ITTeamComments=?,ModifiedBy=?,ModifiedDate=?  where EmpId ="+empId+" AND Id="+employeeAction.getId();
	    try {
	            connection = ConnectionProvider.getInstance().getConnection();
	         preparedStatement = connection.prepareStatement(queryString); 
	      
	         
	         
	       //  System.out.println(employeeAction.getItTeamStatus()+"employeeAction.getItTeamStatus()"+employeeAction.getStatus());
	         
	         if(employeeAction.getItTeamStatus() == null || employeeAction.getItTeamStatus().equals("") ){
	             preparedStatement.setString(1, "");
	             }else{
	         preparedStatement.setString(1, employeeAction.getItTeamStatus());
	             }
	         if(employeeAction.getItTeamComments() == null || employeeAction.getItTeamComments().equals("")  ){
	             preparedStatement.setString(2, "");
	             }else{
	         preparedStatement.setString(2, employeeAction.getItTeamComments());
	             }
	        
	         preparedStatement.setString(3, userId);
	         preparedStatement.setString(4, DateUtility.getInstance().getCurrentSQLDate());
	         
	         
	         isupdated = preparedStatement.executeUpdate();
	         String createdBy = DataSourceDataProvider.getInstance().getemployeenamebyloginId(userId);
	         String empNo = DataSourceDataProvider.getInstance().getEmpNoByEmpId(employeeAction.getEmpId());
	         String empEmail = DataSourceDataProvider.getInstance().getEmailIdForEmployee(employeeAction.getEmpId());
	         String opsContact = DataSourceDataProvider.getInstance().getEmailIdForLoginId(employeeAction.getCreatedBy());
	         String reportedTo =  DataSourceDataProvider.getInstance().getEmailIdForLoginId(employeeAction.getReportedTo());
	    	
	         if(isupdated == 1){
	        	 result = "Updated";
	        	 if(mailFlag.equals("ITTeam")){
	        	 MailManager.sendTransferLocationEmails(empEmail,createdBy,opsContact,reportedTo,employeeAction.getReportedTo(),employeeAction.getEmpName(),empNo,employeeAction.getDepartmentId(),employeeAction.getFromLocation(),employeeAction.getToLocation(),employeeAction.getItTeamComments(),mailFlag,employeeAction.getItTeamStatus(),employeeAction.getId(),employeeAction.getStatus());
	        	 }
	         }
	       //   System.out.println("isupdated"+isupdated);
	     } catch (SQLException se) {
	         se.printStackTrace();
	         System.err.println("Message: " + se.getMessage());
	         System.err.println("SQLState: " +se.getSQLState());
	         System.err.println("Error Code: " +
	                 ((SQLException)se).getErrorCode());
	         throw new ServiceLocatorException(se);
	      //   

	           

	             
	     } finally {
	         try {
	             if (preparedStatement != null) {
	                 preparedStatement.close();
	                 preparedStatement = null;
	             }
	            
	             if (connection != null) {
	                 connection.close();
	                 connection = null;
	             }
	         } catch (SQLException se) {
	             throw new ServiceLocatorException(se);
	         }
	     }

	     return result;

	}
	
	public int doSMCampaignIntegration(int objectId,String month,int year) throws ServiceLocatorException {
 int rows=0;
   PreparedStatement preparedStatement = null;
Statement statement1=null;
Statement statement2=null;
   ResultSet resultSet = null;
   Connection connection=null;
    try {

      
     
       String contactId="";
       
        ContactListService cls=new ContactListService(Properties.getProperty("API_KEY"), Properties.getProperty("ACCESS_TOCKEN"));
        
      Set set=DataSourceDataProvider.getInstance().getEmployeeLocationsLst().keySet();  
      List cclocation=new ArrayList(set);
 
      
        List<SentToContactList> scl=new ArrayList();
                
     
      
        
  List<ContactList>  clst= cls.getLists(null);
    
    for(int j=0;j<clst.size();j++){
   
          for(int i=0;i<cclocation.size();i++){
        
               if(clst.get(j).getName().equals(cclocation.get(i).toString())){
                   
                      SentToContactList stcl=new SentToContactList();
          
               
                stcl.setId(clst.get(j).getId());
                      scl.add(stcl);
          
            }
              
        }
        
          if("".equals(contactId)){
       	   if(clst.get(j).getName().equals("SP_Campaign_List_For_"+month+"_"+year)){
            contactId=clst.get(j).getId();
            }}
    }
    
     if(!"".equals(contactId.toString().trim())){
  cls.deleteList(contactId);
  contactId="";
  }
    if("".equals(contactId.toString().trim())){
       ContactList contList = new ContactList();
           contList.setName("SP_Campaign_List_For_"+month+"_"+year);
           contList.setStatus("ACTIVE");
           contList = cls.addList(contList);
           contactId = contList.getId();
           
            SentToContactList stcl=new SentToContactList();
                        stcl.setId(contactId);
                           scl.add(stcl);
    }

///////////////////////////////////////
    
    
    ///////////////////////////////////

   ConstantContactFactory cf = new ConstantContactFactory(Properties.getProperty("API_KEY"), Properties.getProperty("ACCESS_TOCKEN"));
 IBulkActivitiesService _activityService = cf.createBulkActivitiesService();
   
 AddContactsRequest contactsToAdd = new AddContactsRequest();
List<String> contactLists = new ArrayList<String>();
List<String> columns = new ArrayList<String>();
List<ContactData> importData = new ArrayList<ContactData>();

contactLists.add(contactId); // NOTE: Replace this with the IF for the list you wish to use
columns.add("EMAIL ADDRESS");
columns.add("FIRST NAME");
columns.add("LAST NAME");
columns.add("Company Name");
contactsToAdd.setColumnNames(columns);
contactsToAdd.setLists(contactLists);


connection = ConnectionProvider.getInstance().getConnection();

String query = "SELECT (YEAR(DATE)-2000) AS yr,tblEmployee.Loginid AS loginId,tblEmployee.gender AS gender,YEAR(DATE) AS fullYear,MONTHNAME(STR_TO_DATE(MONTH(DATE), '%m')) AS mnth,DATE_FORMAT(DATE, '%b') AS shortMonth,tblStarPerformerLines.Id,tblStarPerformers.Id As spId,tblStarPerformerLines.ObjectId,tblStarPerformerLines.EmpId,tblStarPerformerLines.EmpName AS EmployeeName,tblStarPerformerLines.Title AS Title,tblStarPerformerLines.CreatedBy,tblStarPerformerLines.Comments,DATE,tblStarPerformerLines.FilePath As FilePath,tblStarPerformerLines.Department As Department,tblStarPerformerLines.EmpId,tblEmployee.Email1,tblEmployee.FName,tblEmployee.LName,tblEmployee.OrgId FROM tblStarPerformerLines LEFT OUTER JOIN tblStarPerformers ON(tblStarPerformers.Id=tblStarPerformerLines.ObjectId) left join tblEmployee ON(tblStarPerformerLines.EmpId=tblEmployee.Id)  WHERE tblStarPerformerLines.Status='Approve' And tblStarPerformerLines.ObjectId="+objectId+"";

preparedStatement = connection.prepareStatement(query);

resultSet = preparedStatement.executeQuery();

String fullYear="";
String shortMonth="";
String htmlTxt="";
String Title= null;
String FilePath= null;
String Department= null;
String EmployeeName= null;
String Comments="";
String errorPath="";
int i=0;
int td=1;
while (resultSet.next()) {

if("".equals(fullYear)){
fullYear =resultSet.getString("fullYear");}
if("".equals(shortMonth)){
  //mnth
  shortMonth=resultSet.getString("mnth");
// shortMonth=resultSet.getString("shortMonth");
}


ContactData newContact = new ContactData();
List<String> emails = new ArrayList<String>();
emails.add(resultSet.getString("Email1"));
newContact.setEmailAddresses(emails);
newContact.setFirstName(resultSet.getString("FName"));
newContact.setLastName(resultSet.getString("LName"));
newContact.setCompanyName(resultSet.getString("OrgId"));
importData.add(newContact);

String isEmployeeImageExist = RestRepository.getInstance().isEmployeeImageExistOrNot(resultSet.getString("loginId"));
if("1".equals(isEmployeeImageExist)){
	FilePath = "https://www.miraclesoft.com/images/employee-profile-pics/"+resultSet.getString("loginId")+".png";
}else {
	
	if("Male".equalsIgnoreCase(resultSet.getString("gender").toString())){
		FilePath ="https://www.miraclesoft.com/images/employee-profile-pics/NoImage_male.png";
	}
	else if("Female".equalsIgnoreCase(resultSet.getString("gender").toString())){
		FilePath ="https://www.miraclesoft.com/images/employee-profile-pics/NoImage_Female.png";
	}
	
}


//String errSrc ="this.src=\'https://www.miraclesoft.com/images/employee-profile-pics/NoImage_male.png\'";


//String errSrc = "this.src='"+errorPath+"'";
//String onErrorTag=" onerror=\""+errSrc+"\"";


//System.out.println("onErrorTag........."+onErrorTag);

//FilePath = resultSet.getString("FilePath");

EmployeeName = resultSet.getString("EmployeeName");
Department = resultSet.getString("Department");
Title = resultSet.getString("Title");
if(!"".equalsIgnoreCase(resultSet.getString("Comments")) && resultSet.getString("Comments")!=null){
Comments=resultSet.getString("Comments");
}

/*sarada tatisetti
 * 7/16/2019
 * New Template
 */

//htmlTxt

String div ="<div style=\"width:100%;display:inline-block;vertical-align:top;max-width: 230px;padding: 5px;\"><table style=\"max-width: 230px;background-color: #ffffff;      \" width=\"100%\" align=\"center\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\"><tbody style=\"\">" +
"<tr><td height=\"10px\"></td></tr><tr><td align=\"center\"><table style=\"max-width: 230px;\" width=\"100%\" align=\"center\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\"><tbody>" +
"<tr><td><table align=\"center\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"230px\"><tbody><tr><td align=\"center\">" +
"<img alt=\"Miracle\" style=\"border-radius: 50%;border: 2px solid #f1f1f1;\" src='"+FilePath+"' width=\"55px\" height=\"55px\" align=\"middle\">" +
"</td></tr></tbody></table></td></tr><tr><td height=\"5px\"></td></tr><tr style=\"background-color: #00aae7;\"><td style=\"padding: 0 10px;\" align=\"center\" height=\"31px\">" +
"<div width=\"100%\" align=\"center\"><table width=\"100%\" align=\"center\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\"><tbody><tr><td height=\"5px\">" +
"</td></tr><tr><td><table style=\"/* border-left: 2px solid #ffffff; */\" align=\"center\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\">" +
"<tbody><tr><td style=\"text-align: center;font-size: 12px;line-height: 20px;font-family:Open Sans;font-weight:400;color: #fff;/* padding-left: 8px; */\">" +
"<b>"+EmployeeName+"</b></td></tr><tr><td height=\"3px\"></td></tr><tr><td style=\"text-align: center;font-size: 12px;line-height: 20px;font-family:'Open Sans';font-weight:400;color: #fff;/* padding-left: 8px; */\">" +
"<b>"+Department+"</b></td></tr></tbody></table></td></tr><tr><td height=\"5px\"></td></tr></tbody></table></div></td></tr></tbody></table></td>" +
"</tr><tr><td height=\"5px\"></td></tr><tr><td align=\"center\" height=\"120px !important\"><table style=\"max-width: 230px;\" width=\"100%\" align=\"center\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" height=\"120px !important\">" +
"<tbody><tr><td style=\"text-align: justify;font-size: 11px;line-height: 20px;font-family:Open Sans;font-weight: 500;color: #2325272;padding: 0 10px;\">"+Comments+"</td></tr></tbody></table></td></tr><tr><td height=\"10px\"></td></tr></tbody></table></div>";

if(td==1){


htmlTxt=htmlTxt+"<tr><td style=\"font-size:0!important\" valign=\"top\" align=\"center\">"+div;
td=td+1;}

//second HtmlTxt

else if(td==3){
htmlTxt=htmlTxt+div+"</td></tr>";


td=1;
}


else{

htmlTxt=htmlTxt+div;


td=td+1;
}

i++; 
}

//this text not changed

if(td==2 || td==3 ){
htmlTxt=htmlTxt+"</td></tr>";


}



//htmltextfinalend


htmlTxt=htmlTxt+"<tr><td height=\"30px\"></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table><table width=\"100%\" align=\"center\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\"><tbody><tr><td valign=\"top\" align=\"center\"><table style=\"max-width:800px;\" width=\"100%\" align=\"center\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\"><tbody><tr><td valign=\"top\" align=\"center\" bgcolor=\"#ffffff\"><table width=\"100%\" align=\"center\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\"><tbody><tr><td valign=\"top\" align=\"center\" bgcolor=\"#ffffff\"><table style=\"max-width:700px;\" width=\"100%\" align=\"center\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\"><tbody><tr><td height=\"10px\" align=\"center\"></td></tr><tr><td style=\"font-size:0;padding:0\" valign=\"top\" align=\"center\"><div style=\"display:inline-block;max-width: 500px;vertical-align:top;width:100%;\"><table align=\"center\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\"><tbody><tr><td height=\"5px\"></td></tr></tbody></table><table style=\"max-width: 500px;\" width=\"100%\" align=\"center\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\"><tbody><tr><td style=\"font-family:Open Sans;text-align:left;color:#8c8c8c;font-size: 12px;line-height:22px;font-weight:400;/* padding-top: 5px; */\">" +
		"&#169; Copyrights "+ Calendar.getInstance().get(Calendar.YEAR) +" | Miracle Software Systems, Inc. </td></tr></tbody></table></div><div style=\"display:inline-block;max-width:200px;vertical-align:top;width:100%;\"><table style=\"padding-top: 0px;max-width: 200px;\" width=\"100%\" align=\"center\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\"><tbody><tr><td width=\"30\" valign=\"top\" align=\"center\"><a href=\"https://www.facebook.com/miracle45625\" target=\"blank\"><img src=\"https://www.miraclesoft.com/images/newsletters/2016/november/facebook2.png\" alt=\"Facebook\" style=\"border:0;width:30px;\" width=\"30\"></a></td><td width=\"30\" valign=\"top\" align=\"center\"><a href=\"https://www.instagram.com/team_mss/\" target=\"blank\"><img src=\"https://d2b8lqy494c4mo.cloudfront.net/mss/images/newsletters/2019/April/insta.png\" alt=\"Instagram\" style=\"border:0;width:30px;\" width=\"30\"></a></td><td width=\"30\" valign=\"top\" align=\"center\"><a href=\"https://www.linkedin.com/company/miracle-software-systems-inc/\" target=\"blank\">" +
		"<img src=\"https://www.miraclesoft.com/images/newsletters/2016/november/linkedin2.png\" alt=\"Linkedin\" style=\"border:0;width:30px;\" width=\"30\"></a></td><td width=\"30\" valign=\"top\" align=\"center\"><a href=\"https://www.youtube.com/c/Team_MSS\" target=\"blank\">" +
		"<img src=\"https://www.miraclesoft.com/images/newsletters/2016/november/youtube2.png\" alt=\"YouTube\" style=\"border:0;width:30px;\" width=\"30\"></a></td></tr></tbody></table></div></td></tr><tr><td height=\"5px\"></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table><table style=\"max-width:800px;\" width=\"100%\" align=\"center\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\"><tbody><tr><td valign=\"top\" align=\"center\" bgcolor=\"#00aae7\"><table padding=\"0\" style=\"max-width:700px;\" width=\"100%\" align=\"center\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\"><tbody><tr><td height=\"05\" align=\"center\"></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table></body></html>";

String first = "<html><head><link rel=\"stylesheet\" "
		+ "type=\"text/css\" href=\"//fonts.googleapis.com/css?family=Open+Sans\"><title>Star Performers</title></head><body align=\"center\" style=\"margin-top: 0; margin-bottom: 0; padding-top: 0; padding-bottom: 0; width: 100%; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%;\" offset=\"0\" margin=\"0\" data-gr-c-s-loaded=\"true\" topmargin=\"0\" marginheight=\"0\" leftmargin=\"0\" bgcolor=\"#f1f1f1\"><span style=\"display:none;font-size:12px;font-family:Open Sans\">Star performers of the month!</span><table width=\"100%\" align=\"center\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\"><tbody><tr><td><table style=\"max-width:800px;\" width=\"100%\" align=\"center\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\"><tbody><tr><td valign=\"top\" align=\"center\" bgcolor=\"#00aae7\"><table padding=\"0\" style=\"max-width:700px;\" width=\"100%\" align=\"center\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\"><tbody><tr><td height=\"05\" align=\"center\"></td></tr></tbody></table></td></tr></tbody></table><table width=\"100%\" align=\"center\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\"><tbody><tr><td valign=\"top\" align=\"center\"><table style=\"max-width:800px;\" width=\"100%\" align=\"center\" cellspacing=\"0\" cellpadding=\"0\" "
		+ "border=\"0\"><tbody><tr><td valign=\"top\" align=\"center\"><table width=\"100%\" align=\"center\" "
		+ "cellspacing=\"0\" cellpadding=\"0\" border=\"0\"><tbody><tr><td valign=\"top\" "
		+ "align=\"center\"><table alt=\"Miracle\" style=\"background-size:cover;background-position:center;background-repeat:no-repeat;\" width=\"100%\""
		+ " align=\"center\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" "
		+ "background=\"https://d2b8lqy494c4mo.cloudfront.net/mss/images/newsletters/2019/June/StarPerf_banner_june19_2.png\"><tbody><tr><td valign=\"top\" align=\"center\"><table style=\"max-width:700px;\""
		+ "width=\"100%\" align=\"center\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\"><tbody><tr><td height=\"40px\" align=\"center\"></td></tr><tr><td><table style=\"\" width=\"100%\" align=\"center\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\"><tbody><tr><td align=\"center\"><table style=\"max-width: 700px;\""
		+ " width=\"100%\" align=\"center\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\"><tbody><tr><td align=\"center\"><a target=\"blank\" href=\"https://www.miraclesoft.com/\"><img style=\"width:230px\" alt=\"Miracle\" src=\"https://www.miraclesoft.com/images/newsletters/Q2/miracle-logo-light.png\" "
		+ "align=\"middle\"></a></td></tr><tr><td height=\"20px\" align=\"center\"></td></tr><tr><td style=\"font-family:Open Sans;font-size: 35px;line-height: 50px;font-weight: 800;color: #ffffff;padding: 0 10px;\" align=\"center\"><b>Star Performers | "+shortMonth+" -" +fullYear+" </b></td></tr><tr><td height=\"25px\"></td></tr><tr><td style=\"padding: 0 10px;\" align=\"center\"><b style=\"font-size: 16px;font-family: open sans;font-weight: 500;color:#ffffff;\">Miraclites who have shined with their phenomenal efforts</b> </td></tr></tbody></table></td></tr></tbody></table></td></tr><tr>"
		+ "<td height=\"40px\" align=\"center\"></td></tr></tbody></table></td></tr><tr><td><table width=\"100%\" align=\"center\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" background=\"https://d2b8lqy494c4mo.cloudfront.net/mss/images/newsletters/2018/October/white1.png\"><tbody><tr background=\"https://d2b8lqy494c4mo.cloudfront.net/mss/images/newsletters/2018/October/white1.png\"><td valign=\"top\" align=\"center\"><table style=\"max-width: 700px;\" width=\"100%\" align=\"center\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\"><tbody><tr><td height=\"10px\"></td></tr><tr><td style=\"\" align=\"center\"><div style=\"display: inline-block;max-width: 230px;\" width=\"100%\" align=\"center\"><table style=\"max-width: 230px;border-right: 2px solid #ffffff;padding: 0 15px;\" width=\"100%\" align=\"center\"><tbody><tr><td style=\"max-width: 230px;\" width=\"100%\"><div width=\"100%\" style=\"max-width: 60px;display: inline-block;\" align=\"center\"><table style=\"max-width: 60px;\" width=\"100%\" align=\"center\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\"><tbody><tr><td><table width=\"60px\" align=\"center\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\"><tbody><tr><td align=\"center\"><img alt=\"Commitment\" src=\"https://d2b8lqy494c4mo.cloudfront.net/mss/images/newsletters/2019/June/Commitment.png\" width=\"55px\" align=\"middle\"></td></tr></tbody></table></td></tr></tbody></table></div><div width=\"100%\" style=\"max-width: 120px;display: inline-block;\" align=\"center\"><table style=\"max-width: 120px;\" width=\"100%\" align=\"center\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\"><tbody><tr><td height=\"15px\"></td></tr><tr><td><table style=\"\" width=\"120px\" align=\"center\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\"><tbody><tr><td style=\"text-align: center;font-size: "
		+ "16px;line-height: 20px;font-family:Open Sans;font-weight:400;color: #fff;\"><b>Commitment</b></td></tr></tbody></table></td></tr><tr><td height=\"15px\"></td></tr></tbody></table></div></td></tr></tbody></table></div><div style=\"display: inline-block;max-width: 230px;\" width=\"100%\" align=\"center\"><table style=\"max-width: 230px;border-right: 2px solid #ffffff;padding: 0 15px;\" width=\"100%\" align=\"center\"><tbody><tr><td><div width=\"100%\" style=\"max-width: 60px;display: inline-block;\" align=\"center\"><table style=\"max-width: 60px;\" width=\"100%\" align=\"center\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\"><tbody><tr><td><table width=\"60px\" align=\"center\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\"><tbody><tr><td align=\"center\"><img alt=\"Efforts\" src=\"https://d2b8lqy494c4mo.cloudfront.net/mss/images/newsletters/2019/June/Efforts.png\" width=\"55px\" align=\"middle\"></td></tr></tbody></table></td></tr></tbody></table></div><div width=\"100%\" style=\"max-width: 120px;display: inline-block;\" align=\"center\"><table style=\"max-width: 120px;\" width=\"100%\" align=\"center\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\"><tbody><tr><td height=\"15px\"></td></tr><tr><td><table style=\"\" width=\"120px\" align=\"center\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\"><tbody><tr><td style=\"text-align: center;font-size: 16px;line-height: 20px;font-family:Open Sans;font-weight:400;color: #fff;/* padding-left: 8px; */\"><b>Hard-work</b></td></tr></tbody></table></td></tr><tr><td height=\"15px\"></td></tr></tbody></table></div></td></tr></tbody></table></div><div style=\"display: inline-block;max-width: 230px;\" width=\"100%\" align=\"center\"><table style=\"max-width: 230px;padding: 0 15px;\" width=\"100%\" align=\"center\"><tbody><tr><td><div width=\"100%\" style=\"max-width: 60px;display: inline-block;\" align=\"center\">"
		+ "<table style=\"max-width: 60px;\" width=\"100%\" align=\"center\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\"><tbody><tr><td><table width=\"60px\" align=\"center\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\"><tbody><tr><td align=\"center\"><img alt=\"Performance\" src=\"https://d2b8lqy494c4mo.cloudfront.net/mss/images/newsletters/2019/June/Performance3.png\" width=\"55px\" align=\"middle\"></td></tr></tbody></table></td></tr></tbody></table></div><div width=\"100%\" style=\"max-width: 125px;display: inline-block;\" align=\"center\"><table style=\"max-width: 125px;\" width=\"100%\" align=\"center\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\"><tbody><tr><td height=\"15px\"></td></tr><tr><td>"
		+ "<table style=\"\" width=\"125px\" align=\"center\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\"><tbody><tr><td style=\"text-align: center;font-size: 16px;line-height: 20px;font-family:Open Sans;font-weight:400;color: #fff;\"><b>Performance</b></td></tr></tbody></table></td></tr><tr><td height=\"15px\"></td></tr></tbody></table></div></td></tr></tbody></table></div></td></tr><tr><td height=\"10px\"></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table><table width=\"100%\" align=\"center\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\"><tbody><tr><td align=\"center\"><table style=\"max-width:800px;background-color: #fff;\" width=\"100%\" align=\"center\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\"><tbody><tr><td valign=\"top\" align=\"center\">"
		+ "<table style=\"max-width:700px;padding: 0 5px;\" width=\"100%\" align=\"center\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\"><tbody><tr><td height=\"30px\"></td></tr><tr><td style=\"font-size: 15px; line-height: 30px; font-family: Open Sans; color: #8c8c8c;\" align=\"justify\"><b><greeting></greeting></b></td></tr><tr><td height=\"10px\"></td></tr><tr><td valign=\"middle\" align=\"center\"><table width=\"100%\" align=\"center\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\"><tbody><tr><td style=\"line-height: 30px;font-family: open sans;color: #8c8c8c;font-size: 14px;\" align=\"justify\"><b>An organization is defined by its employees!</b> At <b>Miracle</b> we are one great big family and we are thrilled to see some of our best being recognized. The following team members have been able to excel in high pressure environments and deliver when needed. For this month they are our Stars and will get the chance to go to a movie with their friends (or) family. We would like to thank them all for their contributions and the efforts that they have put in for the organization. </td></tr></tbody></table></td></tr><tr><td height=\"10\"></td></tr><tr><td valign=\"middle\" align=\"center\"><table width=\"100%\" align=\"center\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\"><tbody><tr><td style=\"line-height: 30px;font-family: open sans;color: #8c8c8c;font-size: 14px;\" align=\"justify\">Keep working hard and we hope to see more of our Miraclites being honored!</td></tr></tbody></table></td></tr><tr><td height=\"30\"></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table><table width=\"100%\" align=\"center\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\"><tbody><tr><td align=\"center\"><table style=\"max-width:800px;\" width=\"100%\" align=\"center\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\"><tbody><tr><td style=\"background-size: cover;background-color: #232527;\" valign=\"top\" align=\"center\"><table style=\"max-width:720px;\" width=\"100%\" align=\"center\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\"><tbody style=\"\"><tr><td height=\"30px\"></td></tr><tr><td style=\"font-family:Open Sans;font-size: 30px;line-height: 50px;font-weight: 800;color: #ffffff;padding: 0 10px;\" align=\"center\"><b>Best of the Best!</b></td></tr><tr><td width=\"50px\"><hr style=\"border: 1px dashed #ffffff;\" width=\"50px\"></td></tr><tr><td height=\"15px\"></td></tr>";


String finalText=first+htmlTxt;


contactsToAdd.setImportData(importData);
     
_activityService.addContacts(contactsToAdd);


//String campaignTemplate=finalText.toString().replaceAll("\"", "'");

String campaignTemplate=finalText.toString(); 
///////////////////////////////////////////////////////////////////////////////////////////////
      
 EmailCampaignService ecs=new EmailCampaignService(Properties.getProperty("API_KEY"), Properties.getProperty("ACCESS_TOCKEN"));

       
   EmailCampaignRequest  emailReq=new EmailCampaignRequest();
       
   String datetime = new SimpleDateFormat("ddMMMyyyy_hh_mm_ss_SSSa").format(new java.util.Date());
   emailReq.setName("Star Performers List "+datetime);
   emailReq.setSubject("Star Performers "+shortMonth+" "+fullYear);
     
       emailReq.setFromEmail("newsletter@miraclesoft.com");
       emailReq.setFromName("Miracle Software Systems, Inc.");
       emailReq.setReplyToEmail("newsletter@miraclesoft.com");
    
       emailReq.setViewAsWebpageEnabled(false);
      
                emailReq.setGreetingName("FIRST_NAME");
                emailReq.setGreetingSalutations("Hello");
                emailReq.setGreetingString("Dear");
               emailReq.setEmailContent(campaignTemplate);
                emailReq.setEmailContentFormat("HTML");
             //   emailReq.setTemplateType("CUSTOM");
                emailReq.setTextContent("Star Performers");
            
                MessageFooter messageFooter =new MessageFooter();
                  messageFooter.setAddressLine1("45625 Grand River Avenue");
           messageFooter.setOrganizationName("Miracle Software Systems, Inc.");
           messageFooter.setCity("Novi");
           messageFooter.setCountry("US");
           messageFooter.setState("MI");
           messageFooter.setPostalCode("48374");
                emailReq.setMessageFooter(messageFooter);
                
              
                emailReq.setSentToContactLists(scl);
                
                
               // System.out.println(emailReq.toJSON().toString());
         EmailCampaignResponse errs= ecs.addCampaign(emailReq);
    


 
      
       if(errs.getId()!=null && !"".equalsIgnoreCase(errs.getId().trim())){
           
          

           
           
 EmailCampaignScheduleService ecsh=new EmailCampaignScheduleService(Properties.getProperty("API_KEY"), Properties.getProperty("ACCESS_TOCKEN"));
   
 
                 // EmailCampaignResponse ecrsp=ecs.getCampaign(errs.getId());
                
          
     EmailCampaignResponse ecrsp=ecs.getCampaign(errs.getId());
 
  EmailCampaignSchedule  Ecs=new EmailCampaignSchedule();
  
//  String campgnStartDate= com.mss.mirage.util.DateUtility.getInstance().getDateforScheduledDateTime(campaignStartDate);
//  Ecs.setScheduledDate(campgnStartDate);
   

  
    
EmailCampaignSchedule finalEcs=null; 
    int size=0;
     while(size<=i){
   com.constantcontact.components.generic.response.ResultSet<Contact> c = cls.getContactsFromList(contactId, 500, null);
    
     size=c.size();
     
         //  System.out.println(size+"........"+i);
     
            if(size==i){
  finalEcs=ecsh.addSchedule(ecrsp.getId(), Ecs);
         break;   }
            
            
        }
     if (finalEcs != null) {
   	  
   	  
   	  String query1="Update tblStarPerformers Set CampaignId='"+errs.getId()+"',Status='5' WHERE Id="+objectId+"";
         statement1 = connection.createStatement();
         rows =  statement1.executeUpdate(query1);

//         String campgnDate = com.mss.mirage.util.DateUtility.getInstance().convertFromGraphViewToElastic(finalEcs.getScheduledDate());
//
//         String query2 = "Update tblStarPerformers Set CampaignScheduledDate='" + campgnDate + "',ScheduledId='" + finalEcs.getId() + "' WHERE Id=" + objectId + "";
//         statement2 = connection.createStatement();
//         rows = statement2.executeUpdate(query2);


         String sendMailId = Properties.getProperty("STARPERFORMER_SUVERYFORM_ACCESS");
         MailManager sendMail = new MailManager();
         sendMail.sendStarPerformerDetailsForSurveyForm(sendMailId,objectId, shortMonth + " " + fullYear);

     }

   
            
       }

   } catch (SQLException ex) {
   	ex.printStackTrace();
       Logger.getLogger(EmployeeServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
   }
   catch (ConstantContactServiceException e) {
      
       e.printStackTrace();
      // System.out.println(e.getMessage());
   }
   
   finally {
       try {
           if (resultSet != null) {
               resultSet.close();
               resultSet = null;
           }
           if (preparedStatement != null) {
               preparedStatement.close();
               preparedStatement = null;
           }
           if (connection != null) {
               connection.close();
               connection = null;
           }
       } catch (SQLException sqle) {
       	sqle.printStackTrace();
       }
   }
   // System.err.println("response string is"+stringBuffer.toString());
   return   rows;
}
	
	
}
