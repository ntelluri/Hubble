/*
 * @(#)EmployeeAction.java 1.0 November 01, 2007
 *
 * Copyright 2006 Miracle Software Systems(INDIA) Pvt Ltd. All rights reserved.
 *
 *
 *******************************************************************************
 *
 * Project : Mirage V2
 *
 * Package : com.mss.mirage.employee.genaralgetEmployee
 *
 * Date    :  September 24, 2007, 10:18 PM
 *
 * Author  : Praveen Kumar Ralla<pralla@miraclesoft.com>
 *
 * File    : EmployeeAction.java
 *
 * Copyright 2007 Miracle Software Systems, Inc. All rights reserved.
 * MIRACLE SOFTWARE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * *****************************************************************************
 */
package com.mss.mirage.employee.general;

import com.itextpdf.text.pdf.codec.Base64;
import com.mss.mirage.employee.general.StateVTO;
import com.mss.mirage.employee.profile.EmpProfileVTO;
import com.mss.mirage.util.ApplicationConstants;
import com.mss.mirage.util.ConnectionProvider;
import com.mss.mirage.util.DataSourceDataProvider;
import com.mss.mirage.util.DataUtility;
import com.mss.mirage.util.DefaultDataProvider;
import com.mss.mirage.util.HibernateDataProvider;
import com.mss.mirage.util.LdapServiceProvider;
import com.mss.mirage.util.AuthorizationManager;
import com.mss.mirage.util.DateUtility;
import com.mss.mirage.util.MailManager;
import com.mss.mirage.util.MergeImages;
import com.mss.mirage.util.PasswordUtility;
import com.mss.mirage.util.ServiceLocator;
import com.opensymphony.xwork2.ActionSupport;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Date;
import java.util.Collection;
import java.util.List;
import java.sql.Timestamp;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.struts2.interceptor.ServletRequestAware;

//new
import com.mss.mirage.util.DateUtility;
import com.mss.mirage.util.FileUploadUtility;
import com.mss.mirage.util.Properties;
import com.mss.mirage.util.RestRepository;
import com.mss.mirage.util.ServiceLocator;
import com.mss.mirage.util.ServiceLocatorException;
import com.opensymphony.xwork2.ActionSupport;
import java.io.*;
import java.sql.*;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;
import org.json.JSONObject;

import java.sql.Blob;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import java.util.zip.ZipOutputStream;
import org.apache.commons.io.FileUtils;
import java.util.zip.ZipEntry;

import javax.imageio.ImageIO;

/**
 * The <code>EmployeeAction</code> class is used for getting adding new Activity
 * from <i>ActivityAdd.jsp</i> page.
 * <p>
 * Then it invokes setter methods in <code>EmployeeAction</code> class and
 * invokes execute() method in <code>EmployeeAction</code> for inserting
 * activity details in MIRAGE.tblCrmActivity table.
 *
 * @author Praveen Kumar Ralla<a href=
 *         "mailto:pralla@miraclesoft.com">pralla@miraclesoft.com</a>
 *
 * @version 1.0 November 01, 2007
 *
 * @see com.mss.mirage.util.ServiceLocator
 * @see com.mss.mirage.employee.general.EmployeeVTO
 * @see com.mss.mirage.util.ApplicationDataProvider
 *
 * @since 1.0
 */
public class EmployeeAction extends ActionSupport implements ServletRequestAware, ServletResponseAware {

	/**
	 * The empId is used for storing employee id.
	 */
	private int empId;
	/**
	 * The resultType is used for storing type of result.
	 */
	private String resultType;
	/**
	 * The queryString is used for storing sqlQuery String.
	 */
	private StringBuilder queryStringBuffer;
	private String queryString;
	private String deleteAction;
	private String loginId;
	private String previousEmpState;
	private transient Collection currentStateHistoryCollection;
	/**
	 * The resultMessage is used for storing resultMessage.
	 */
	private String resultMessage;
	/**
	 * The httpServletRequest is used for storing httpServletRequest Object.
	 */
	private transient HttpServletRequest httpServletRequest;
	/**
	 * The hibernateDataProvider is used for storing reference of
	 * ApplicationDataProvider methods.
	 */
	private HibernateDataProvider hibernateDataProvider;
	private DefaultDataProvider defaultDataProvider;
	private DataSourceDataProvider dataSourceDataProvider;
	/**
	 * The currentEmployee is used for storing currentEmployee Details.
	 */
	private EmployeeVTO currentEmployee;
	/**
	 * The employeeService is used for accessing methods of EmployeeService.
	 */
	private EmployeeService employeeService;
	/**
	 * The id is used for storing id of employee.
	 */
	private int id;
	/**
	 * The firstName is used for storing firstName of employee.
	 */
	private String firstName;
	/**
	 * The lastName is used for storing lastName of employee.
	 */
	private String lastName;
	/**
	 * The middleName is used for storing middleName of employee.
	 */
	private String middleName;
	/**
	 * The aliasName is used for storing aliasName of employee.
	 */
	private String aliasName;
	/**
	 * The gender is used for storing gender of employee.
	 */
	private String gender;
	/**
	 * The maritalStatus is used for storing maritalStatus of employee.
	 */
	private String maritalStatus;
	/**
	 * The country is used for storing contry of employee.
	 */
	private String country;
	/**
	 * The ssn is used for storing ssn of employee.
	 */
	private String ssn;
	private String empno;
	private String nsrno;
	/**
	 * The currStatus is used for storing currentStatus of employee.
	 */
	private String currStatus = ApplicationConstants.DEFAULT_EMP_STATUS;
	/**
	 * The preCurrStatus is used for storing previous current Status of
	 * employee.
	 */
	private String preCurrStatus;
	/**
	 * The empTypeId is used for storing employeeTypeId of employee.
	 */
	private String empTypeId;
	/**
	 * The orgId is used for storing OraganizationId of employee.
	 */
	private String orgId = ApplicationConstants.DEFAULT_ORG;
	/**
	 * The opsContactId is used for storing ContactId
	 */
	private String opsContactId;
	/**
	 * The teamId is used for storing teamId.
	 */
	private String teamId;
	/**
	 * The practiceId is used for storing PracticeId.
	 */
	private String practiceId;
	private String subPractice;
	/**
	 * The titleId is used for storing titleId.
	 */
	private String titleId;
	/**
	 * The industryId is used for storing industryId
	 */
	private String industryId;
	/**
	 * The departmentId is used for storing DepartmentId.
	 */
	private String departmentId;
	/**
	 * The birthDate is used for storing dateOfBirth of employee.
	 */
	private Date birthDate;
	/**
	 * The offBirthDate is used for storing official date of birth of employee.
	 */
	private Date offBirthDate;
	/**
	 * The hireDate is used for storing hiredate of employee.
	 */
	private Date hireDate;
	/**
	 * The anniversaryDate is used for storing anniversaryDate of employee.
	 */
	private Date anniversaryDate;
	/**
	 * The workPhoneNo is used for storing wormPhoneNo of employee.
	 */
	private String workPhoneNo;
	/**
	 * The altPhoneNo is used for storing alternatePhoneNo of employee.
	 */
	private String altPhoneNo;
	/**
	 * The homePhoneNo is used for storing homePhoneNo of employee.
	 */
	private String homePhoneNo;
	/**
	 * The cellPhoneNo is used for storing cellPhoneNo of employee.
	 */
	private String cellPhoneNo;
	/**
	 * The officeEmail is used for storing officeEmail of employee.
	 */
	private String officeEmail;
	/**
	 * The hotelPhoneNo is used for storing hotelPhoneNo of employee.
	 */
	private String hotelPhoneNo;
	/**
	 * The personalEmail is used for storing personal Email Of employee.
	 */
	private String personalEmail;
	/**
	 * The indiaPhoneNo is used for storing indiaPhoneNo of employee.
	 */
	private String indiaPhoneNo;
	/**
	 * The otherEmail is used for storing other Email of employee.
	 */
	private String otherEmail;
	/**
	 * The faxNo is used for storing Fax of employee.
	 */
	private String faxNo;
	/**
	 * The lastContactBy is used for storing lastContact Peroson User Id.
	 */
	private String lastContactBy;
	/**
	 * The modifiedBy is used for storing modified Person Id.
	 */
	private String modifiedBy;
	/**
	 * The modifiedDate is used for storing modifiedDate.
	 */
	private Timestamp modifiedDate;
	/**
	 * The createdBy is used for storing created Person id.
	 */
	private String createdBy;
	/**
	 * The createdDate is used for storing created Date.
	 */
	private Timestamp createdDate;
	/**
	 * The genderList is used for storing genderOptions.
	 */
	private List genderList;
	/**
	 * The maritalStatusList is used for storing maritalStatusOptions.
	 */
	private List maritalStatusList;
	/**
	 * The countryList is used for storing countryOptions.
	 */
	private List countryList;
	/**
	 * The currStatusList is used for storing currStatusListOptions.
	 */
	private List currStatusList;
	/**
	 * The empTypeIdList is used for storing employeeTypeId Options.
	 */
	private List empTypeIdList;
	/**
	 * The orgIdList is used for storing Organizationid Options.
	 */
	private List orgIdList;
	/**
	 * The opsContactIdMap is used for storing ContactId Options.
	 */
	private Map opsContactIdMap;
	/**
	 * The teamIdList is used for storing TeamId Options.
	 */
	private List teamIdList;
	/**
	 * The practiceIdList is used for storing practiceId Options.
	 */
	private List practiceIdList;
	private List subPracticeList;
	private List empCurrentStatus;
	/**
	 * The reportsToIdMap is used for storing reportsToId Options.
	 */
	private Map reportsToIdMap;
	/**
	 * titleIdList is used for storing titleId Options.
	 */
	private List titleIdList;
	/**
	 * The industryIdList is used for storing industryList Options.
	 */
	private List industryIdList;
	/**
	 * The departmentIdList is used for storing departmentId Options.
	 */
	private List departmentIdList;
	/**
	 * isManager is used to store the flag to identify user has manager role
	 */
	private boolean isManager;
	/**
	 * isTeamLead is used to store the flag to identify user has teamLead role
	 */
	private boolean isTeamLead;
	/**
	 * reportsTo is used to store the User Id of reporting person
	 */
	private String reportsTo;
	/**
	 * The pointOfContactMap is used for storing pointOfContact Options.
	 */
	private Map pointOfContactMap;
	/**
	 * The submitFrom is used to store the value of request from wich form on
	 * the page
	 */
	private String submitFrom;
	private String submitFromReportsTo;
	// private Map regionMap;
	private List regionList;
	private List territoryList;
	private String workingCountry;
	private String empState;
	private Date stateStartDate;
	private Date stateEndDate;
	// private float intRatePerHour;
	// private float invRatePerHour;
	private String skillSet;
	private int userRoleId;
	private String prjName;
	// private double ctcPerYear;
	private String itgBatch;
	private String isOnsite;
	private boolean isCreTeam;
	private StateVTO currentEmployeeState;
	private String tempName = null;
	private String tempPh = null;
	private String tempCurStatus = null;
	private String tempDeptId = null;
	private String tempOrgId = null;
	private String tempEmail = null;
	private int currId;
	private int tempVar;
	private Map reportingPersons;
	private String reportingpersonId;
	// new
	private File imagePath = null;
	// new varibble to allow either insert or update in Employee current statsus
	// screen
	private int navId = 0;
	private String location;
	// For oprations team
	private boolean isOperationTeam;
	private String projectName;
	private String status;
	private String startDate;
	private String projectId;
	private String accountId;
	private Map myAccounts;
	private Map myProjects;
	private boolean isPMO;
	// New Fileds For Confedential Info Date : 08/19/2014
	private String bankName;
	private String accNum;
	private String nameAsPerAcc;
	private String ifscCode;
	private String phyChallenged;
	private String phyCategory;
	private String aadharNum;
	private String aadharName;
	private String nameAsPerPan;
	private String uanNo;
	private String pfno;
	private String techReviews;
	private String endDate;

	// New ReportstoFlag
	private boolean reportsToFlag;
	private Date lastRevisedDate;
	private Date revisedDate;
	private HttpServletResponse httpServletResponse;
	private Map myTeamMembers;
	private List stateList;
	private Map managerTeamMembersList;
	private String managerTeamMember;
	private String currentAction;
	private String customerName;
	private int utilization;
	private String comments;
	private Map projectTeamReportsTo;
	private String isBillable;
	private boolean isInternationalWorker;
	// private List locationsList;
	private Map locationsMap;
	private List practiceList = new ArrayList();
	private String empStatus;
	private Map myPMOTeamMembers;
	private Date dateOfTermination;
	private String reasonsForTerminate;
	private String prvexpMnths;
	private String prvexpYears;
	private File upload;
	private String uploadContentType;
	private String uploadFileName;
	private String filepath;
	private InputStream inputStream;
	private OutputStream outputStream;

	public String getTempEmail() {
		return tempEmail;
	}

	public void setTempEmail(String tempEmail) {
		this.tempEmail = tempEmail;
	}

	private int projectContactId;
	private boolean lateralFlag;
	private Map clientMap = new LinkedHashMap();
	private String empName;
	private String empDateOFBirth;
	private String isLateral;
	private String quarterAppraisalDetails;
	private int rowCount;
	private String curretRole;
	private int appraisalId;
	private int lineId;
	private int year;
	private String quarterly;
	private String shortTermGoal;
	private String shortTermGoalComments;
	private String longTermGoal;
	private String longTermGoalComments;
	private String strength;
	private String strengthsComments;
	private String improvements;
	private String improvementsComments;
	private String rejectedComments;
	private String operationTeamStatus;
	private String managerRejectedComments;
	private String operationRejectedComments;
	private int dayCount;
	private int accessCount;
	private String empProjectStatus = "Active";
	private List empPracticeList;
	private int backToFlag = 0;
	private String quarterlyForManagersReport;
	private int yearForManagersReport;

	/* Star Performer Variables */
	private Map starPerformerStatus = new LinkedHashMap();
	private int month;
	private int overlayMonth;
	private int overlayYear;
	private int statusId;
	private String nomineeIds;
	private String starMonth;
	private int objectId;
	private int starId;
	private int employeeId;
	private String resourceName;
	private int suvyId;

	private String currentDate;
	private int weightageFrom = -1;
	private int weightageTo = -1;

	// search field sustain
	private int searchYear;
	private String searchQuarterly;
	private String searchStatus;
	private String searchLoginId;
	private String searchDepartmentId;
	private String searchPracticeId;
	private String searchSubPractice;
	private String searchOpsContactId;
	private String searchLocation;
	private String searchTitleId;
	private String txtCurr;
	private String searchOperationStatus;
	private List projectTypesList;
	private String isIncludeTeam;
	private String trainingRating;
	private int qyear;
	private Map<String, String> monthsMap;

	private String costModel;
	private String week;
	private List exitTypeList;
	private String exitType;

	private int customerId;

	private String systemType;
	private List shiftTimingsList;
	private String shiftTimings;
	private boolean weekendsSupportFlag;
	private String monthName;
	private String email1;

	private String initiatedOn;
	private String fromLocation;
	private String toLocation;
	private String reasonsForTransfer;
	private String hostelDues;
	private Boolean isKeysHandover;
	private String cafteriaDues;
	private String transportationDues;
	private String duesRemarks;
	private String itTeamStatus;
	private String itTeamComments;
	private String reportedTo;
	private String tentativeReportedDate;
	private Boolean isReported;
	private String nextLocationHRComments;
	private Map empLocationMap;
	private String actualReportedDate;
	private int isNextLocationHR;

	private List transportLocationList;
	private List accommodationList;

	private int roomNo;
	private String accommodation;
	private String transportLocation;
	private double transportFee;
	private double cafeteriaFee;
	private double roomFee;
	private String cafeteria;
	private String transportation;
	private String occupancyType;
	private String dateOfOccupancy;
	private double electricalCharges;
	private int flag;
	private String resourceComments;

	private String trainingType;

	private boolean needTransport;

	private Map practiceMap;

	private String shortTermGoalHrComments;
	private String longTermGoalHrComments;
	private String strengthsHrComments;
	private String improvementsHrComments;

	private Map practiceMapGDC;
	private boolean outSideAccess;

	private String prevLiveInCountry;
	
	private String transferType;
	private String fromLocationnew;
	private String toLocationnew;
	private String reportedToNew;
	private String tentativeReportedDateNew;
	private Map opsContactIdMapNew;
	
	private List usPayRollBatchList;

	private String payRollBatchUs;
	
	private String empname;
	private String position;
	private String url;
	private String Email;
	private String sMonth;
	private String sstarId;
	private String titleTypeId;
	
	private String reportBasedOn;
	
	private String isAdminFlag;
	private String ProjstartDate;
	private String startDateFrom;
	private String startDateTo;
	private String endDateFrom;
	private String endDateTo;
	
	public String getIsAdminFlag() {
		return isAdminFlag;
	}

	public void setIsAdminFlag(String isAdminFlag) {
		this.isAdminFlag = isAdminFlag;
	}

	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	public String getWeek() {
		return week;
	}

	public void setWeek(String week) {
		this.week = week;
	}

	public String getCostModel() {
		return costModel;
	}

	public void setCostModel(String costModel) {
		this.costModel = costModel;
	}

	public Map<String, String> getMonthsMap() {
		return monthsMap;
	}

	public void setMonthsMap(Map<String, String> monthsMap) {
		this.monthsMap = monthsMap;
	}

	public String getTrainingRating() {
		return trainingRating;
	}

	public void setTrainingRating(String trainingRating) {
		this.trainingRating = trainingRating;
	}

	public boolean getReportsToFlag() {
		return reportsToFlag;
	}

	public void setReportsToFlag(boolean reportsToFlag) {
		this.reportsToFlag = reportsToFlag;
	}

	/**
	 * Creates a new instance of EmployeeAction
	 */
	public EmployeeAction() {
	}

	/**
	 * The prepare() is used for executing block of code which is required
	 * before execute method get's executed.
	 *
	 * @throws Exception
	 * @return string variable.
	 */
	public String prepare() throws Exception {
		resultType = LOGIN;
		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			String roleName = httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_ROLE_NAME)
					.toString();
			// System.out.println("roleName---->"+roleName);
			hibernateDataProvider = HibernateDataProvider.getInstance();
			defaultDataProvider = DefaultDataProvider.getInstance();
			employeeService = ServiceLocator.getEmployeeService();
			dataSourceDataProvider = dataSourceDataProvider.getInstance();

			setGenderList(defaultDataProvider.getGender(ApplicationConstants.GENDER_OPTIONS));

			setMaritalStatusList(defaultDataProvider.getMaritalStatus(ApplicationConstants.MARITAL_STATUS_OPTIONS));

			setCountryList(hibernateDataProvider.getContries(ApplicationConstants.COUNTRY_OPTIONS));

			// setTerritoryList(hibernateDataProvider.getTerritoryList(ApplicationConstants.QS_COUNT_DASHBOARDTERRITORY_LIST));

			setCurrStatusList(defaultDataProvider.getCurrentStatus(ApplicationConstants.CURRENT_STATUS_OPTIONS));

			setEmpTypeIdList(hibernateDataProvider.getEmployeeType(ApplicationConstants.EMP_TYPE_OPTIONS));

			setOrgIdList(hibernateDataProvider.getLkOrganization(ApplicationConstants.LKORGANIZATION_OPTION));

			// setOpsContactIdMap(hibernateDataProvider.getEmployeeNames());
			// setRegionList(hibernateDataProvider.getRegion(ApplicationConstants.REGION_OPTIONS));

			setPointOfContactMap(dataSourceDataProvider.getPointOfContact());

			// setPracticeIdList(hibernateDataProvider.getPractice(ApplicationConstants.PRACTICE_OPTION));
			setPracticeIdList(dataSourceDataProvider.getPracticeByDepartment(getDepartmentId()));

			setSubPracticeList(dataSourceDataProvider.getSubPracticeByPractice(getPracticeId()));

			setTeamIdList(dataSourceDataProvider.getTeamBySubPractice(getSubPractice()));

			setEmpCurrentStatus(hibernateDataProvider.getEmpCurrentState(ApplicationConstants.EMP_CURRENT_STATUS));

			setReportsToIdMap(dataSourceDataProvider.getReportsToByDepartment(getDepartmentId()));
			setReportingPersons(dataSourceDataProvider.getEmployeeNamesByReportingPerson());

			// setReportsToIdMap(hibernateDataProvider.getReportsToDefault());

			// if("Registered".equalsIgnoreCase(getCurrentEmployee().getCurrStatus())){
			// reportsToIdMap = new TreeMap();
			// reportsToIdMap.put("plokam","Prasad Venkata.Lokam");
			// reportsToIdMap.put("mlokam","Madhavi Naga.Loakam");
			// }

			// setTitleIdList(hibernateDataProvider.getTitleType(ApplicationConstants.TITLE_TYPE_OPTIONS));
			setTitleIdList(dataSourceDataProvider.getTitleTypeByDepartment(getDepartmentId()));

			setIndustryIdList(hibernateDataProvider.getIndustry(ApplicationConstants.INDUSTRY_OPTION));

			setDepartmentIdList(hibernateDataProvider.getDepartment(ApplicationConstants.DEPARTMENT_OPTION));

			setCurrentStateHistoryCollection(employeeService.getStateHistoryCollection(getLoginId(), 100));

			// setRegionMap(hibernateDataProvider.getRegion(ApplicationConstants.REGION_OPTIONS));

			String Country = (String) httpServletRequest.getSession(false)
					.getAttribute(ApplicationConstants.Living_COUNTRY);
			setOpsContactIdMap(dataSourceDataProvider.getOpsContactId(Country, roleName));
			setLocationsMap(dataSourceDataProvider.getEmployeeLocationsList(getCurrentEmployee().getCountry()));
			setExitTypeList(dataSourceDataProvider.getExitType());
			setShiftTimingsList(dataSourceDataProvider.getShiftTimingDetails());
			setUsPayRollBatchList(dataSourceDataProvider.getUsPayRollBatchList());
			resultType = SUCCESS;
		}
		return resultType;
	}

	/**
	 * The searchPrepare() is used for displaying selectBox options from
	 * database server.
	 *
	 * @throws Exception
	 * @return String variable for navigation.
	 */
	public String searchPrepare() throws Exception {
		resultType = LOGIN;
		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			hibernateDataProvider = HibernateDataProvider.getInstance();
			defaultDataProvider = DefaultDataProvider.getInstance();
			setDepartmentIdList(hibernateDataProvider.getDepartment(ApplicationConstants.DEPARTMENT_OPTION));
			setPracticeIdList(DataSourceDataProvider.getInstance().getPracticeByDepartment(getDepartmentId()));
			setCurrStatusList(defaultDataProvider.getCurrentStatus(ApplicationConstants.CURRENT_STATUS_OPTIONS));
			setOrgIdList(hibernateDataProvider.getLkOrganization(ApplicationConstants.LKORGANIZATION_OPTION));
			setReportingPersons(DataSourceDataProvider.getInstance().getEmployeeNamesByReportingPerson());
			setCountryList(hibernateDataProvider.getContries(ApplicationConstants.COUNTRY_OPTIONS));
			String Country = (String) httpServletRequest.getSession(false)
					.getAttribute(ApplicationConstants.WORKING_COUNTRY);
			String roleName = httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_ROLE_NAME)
					.toString();

			setOpsContactIdMap(DataSourceDataProvider.getInstance().getOpsContactId(Country, roleName));
			dataSourceDataProvider = DataSourceDataProvider.getInstance();
			if ("Admin".equals(roleName)) {
				setLocationsMap(dataSourceDataProvider.getEmployeeLocationsList("%"));
			} else {
				setLocationsMap(dataSourceDataProvider.getEmployeeLocationsList(Country));
			}
			setExitTypeList(dataSourceDataProvider.getExitType());
			setUsPayRollBatchList(dataSourceDataProvider.getUsPayRollBatchList());
			setEmpTypeIdList(hibernateDataProvider.getEmployeeType(ApplicationConstants.EMP_TYPE_OPTIONS));
			
			resultType = SUCCESS;
		}
		return resultType;
	}

	public String getEmpBackToList() {
		resultType = LOGIN;
		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			try {
				searchPrepare();
				resultType = SUCCESS;
			} catch (Exception ex) {
				// List errorMsgList = ExceptionToListUtility.errorMessages(ex);
				httpServletRequest.getSession(false).setAttribute("errorMessage", ex.toString());
				resultType = ERROR;
			}
		}
		return resultType;
	}

	public String getEmpSearchAll() {
		resultType = LOGIN;

		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			userRoleId = Integer.parseInt(
					httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_ROLE_ID).toString());
			// String workingCountry =
			// httpServletRequest.getSession(false).getAttribute(ApplicationConstants.WORKING_COUNTRY).toString();
			String livingCountry = httpServletRequest.getSession(false)
					.getAttribute(ApplicationConstants.Living_COUNTRY).toString();
			
			
			String workingCountry =httpServletRequest.getSession(false) 
					.getAttribute(ApplicationConstants.WORKING_COUNTRY).toString();
			
			String userRoleName = httpServletRequest.getSession(false)
					.getAttribute(ApplicationConstants.SESSION_ROLE_NAME).toString();

			// System.out.println("workingCountry----->"+workingCountry);
			resultType = "accessFailed";
			if (AuthorizationManager.getInstance().isAuthorizedUser("GET_EMP_SEARCH_ALL", userRoleId)) {
				try {
					if (getSubmitFrom() == null) {
						if ("Admin".equals(userRoleName)) {

							queryString = "SELECT Id, DepartmentId, LName, FName, MName,EmpNo, Email1, WorkPhoneNo, AlterPhoneNo, CellPhoneNo, CurStatus ,LoginId ";
							queryString = queryString
									+ " FROM tblEmployee WHERE CurStatus='Active' and DeletedFlag != 1 ORDER BY trim(FName)";

						} /*
							 * else { queryString =
							 * "SELECT Id, DepartmentId, LName, FName, MName,EmpNo, Email1, WorkPhoneNo, AlterPhoneNo, CellPhoneNo, CurStatus ,LoginId "
							 * ; queryString = queryString +
							 * " FROM tblEmployee WHERE CurStatus='Active' and Country like '"
							 * + livingCountry +
							 * "' and DeletedFlag != 1 ORDER BY trim(FName)"; }
							 */ else {
							queryString = "SELECT Id, DepartmentId, LName, FName, MName,EmpNo, Email1, WorkPhoneNo, AlterPhoneNo, CellPhoneNo, CurStatus ,LoginId ";
							queryString = queryString + " FROM tblEmployee WHERE CurStatus='Active' and ";
							if (livingCountry.equalsIgnoreCase("USA")) {
								queryString = queryString + "Country != 'India' and ";
							} else {
								queryString = queryString + "Country like '"+ workingCountry +"' and ";
							}

							queryString = queryString + " DeletedFlag != 1 ORDER BY trim(FName)";
						}
						dataSourceDataProvider = DataSourceDataProvider.getInstance();
						setSubmitFrom("searchFormAll");
						setCurrStatus("Active");
						// System.err.println("Before");
						setReportingPersons(dataSourceDataProvider.getEmployeeNamesByReportingPerson());
						httpServletRequest.getSession(false).setAttribute(ApplicationConstants.SESSION_DELETE_ACTION,
								"empSearchAll");
						httpServletRequest.getSession(false).setAttribute(ApplicationConstants.QS_EMP_LIST,
								queryString);
					}
					searchPrepare();
					// prepare();
					resultType = SUCCESS;
				} catch (Exception ex) {
					// List errorMsgList =
					// ExceptionToListUtility.errorMessages(ex);
					httpServletRequest.getSession(false).setAttribute("errorMessage", ex.toString());
					resultType = ERROR;
				}
			} // END-Authorization Checking
		} // Close Session Checking
		return resultType;
	}

	public String getEmpSearchMyTeam() {
		resultType = LOGIN;

		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			userRoleId = Integer.parseInt(
					httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_ROLE_ID).toString());
			resultType = "accessFailed";
			if (AuthorizationManager.getInstance().isAuthorizedUser("GET_EMP_SEARCH_MY_TEAM", userRoleId)) {
				try {
					if (getSubmitFrom() == null) {
						queryString = "SELECT Id, DepartmentId, LName, FName, MName, Email1, WorkPhoneNo, AlterPhoneNo, CellPhoneNo, CurStatus ";
						queryString = queryString + " FROM tblEmployee WHERE ReportsTo='"
								+ httpServletRequest.getSession(false)
										.getAttribute(ApplicationConstants.SESSION_USER_ID)
								+ "' AND DeletedFlag != 1 ORDER BY CurStatus,LName";
						setSubmitFrom("searchFormMyTeam");
						httpServletRequest.getSession(false).setAttribute(ApplicationConstants.SESSION_DELETE_ACTION,
								"empSearchMyTeam");
						httpServletRequest.getSession(false).setAttribute(ApplicationConstants.QS_EMP_LIST,
								queryString);
					}
					searchPrepare();
					resultType = SUCCESS;
				} catch (Exception ex) {
					// List errorMsgList =
					// ExceptionToListUtility.errorMessages(ex);
					httpServletRequest.getSession(false).setAttribute("errorMessage", ex.toString());
					resultType = ERROR;
				}
			} // END-Authorization Checking
		} // Close Session Checking
		return resultType;
	}

	/**
	 * The getSearchQuery() is used for retrieving SQLquery .
	 *
	 * @throws Exception
	 * @return String variable for navigation.
	 */
	/*
	 * Modifed By : Aditya MAlla Modified Date : 08/04/2014 Reason : Added
	 * IsTeamLead and Is MAnager search fields
	 */
	/*
	 * 
	 * 
	 * public String getSearchQuery() {
	 * 
	 * // System.out.println("I am in getSearchQuery() +getOrgId()); resultType
	 * = LOGIN;
	 * 
	 * if(httpServletRequest.getSession(false).getAttribute(ApplicationConstants
	 * .SESSION_USER_ID) != null){
	 * 
	 * userRoleId =
	 * Integer.parseInt(httpServletRequest.getSession(false).getAttribute(
	 * ApplicationConstants.SESSION_ROLE_ID).toString()); String workingCountry
	 * = httpServletRequest.getSession(false).getAttribute(ApplicationConstants.
	 * WORKING_COUNTRY).toString(); resultType = "accessFailed"; String
	 * userRoleName =
	 * httpServletRequest.getSession(false).getAttribute(ApplicationConstants.
	 * SESSION_ROLE_NAME).toString();
	 * if(AuthorizationManager.getInstance().isAuthorizedUser(
	 * "GET_SEARCH_QUERY_EMP",userRoleId)){ try{ //
	 * setPracticeIdList(dataSourceDataProvider.getPracticeByDepartment(
	 * getDepartmentId())); //
	 * setPracticeId(getCurrentEmployee().getPracticeId()); //
	 * setSubPractice(getCurrentEmployee().getSubPractice());
	 * 
	 * //System.err.println(getSubmitFrom());
	 * 
	 * if("searchFormAll".equalsIgnoreCase(getSubmitFrom()) ||
	 * "searchFormMyTeam".equalsIgnoreCase(getSubmitFrom())){ queryStringBuffer
	 * = new StringBuffer();
	 * 
	 * queryStringBuffer.
	 * append("SELECT Id, DepartmentId, LName, FName, MName, Email1, WorkPhoneNo, AlterPhoneNo, CellPhoneNo, CurStatus,SubPractice,TeamId"
	 * ); queryStringBuffer.append(" FROM tblEmployee ");
	 * 
	 * // queryStringBuffer.append(" tblLKSubPractice");
	 * 
	 * if(null == getFirstName()) setFirstName(""); if(null == getLastName())
	 * setLastName(""); if(null == getWorkPhoneNo()) setWorkPhoneNo(""); if(null
	 * == getCurrStatus()) setCurrStatus("");
	 * if("All".equalsIgnoreCase(getCurrStatus())) setCurrStatus(""); if(null ==
	 * getOrgId()) setOrgId(""); if("All".equalsIgnoreCase(getOrgId()))
	 * setOrgId(""); if(null == getReportingpersonId())
	 * setReportingpersonId("");
	 * if("All".equalsIgnoreCase(getReportingpersonId()))
	 * setReportingpersonId(""); if(null == getOrgId()) setOrgId("");
	 * if("All".equalsIgnoreCase(getOrgId())) setOrgId(""); if(null==
	 * getDepartmentId()) setDepartmentId(""); if(null==getSubPractice())
	 * setSubPractice(""); if("All".equalsIgnoreCase(getDepartmentId()))
	 * setDepartmentId(""); if(null == getTeamId()) setTeamId("");
	 * if("All".equalsIgnoreCase(getTeamId())) setTeamId("");
	 * 
	 * 
	 * 
	 * 
	 * // if((!"".equals(getFirstName())) // || (!"".equals(getLastName())) //
	 * || (!"".equals(getWorkPhoneNo())) // || (!"".equals(getCurrStatus())) //
	 * ||(!"".equals(getOrgId()))){ // queryStringBuffer.append(" WHERE "); // }
	 * if("Admin".equals(userRoleName)) { queryStringBuffer.append("where ");
	 * }else {
	 * queryStringBuffer.append(" WHERE  Country like '"+workingCountry+"' AND "
	 * ); } int columnCounter = 0;
	 * 
	 * 
	 * if(!"".equals(getFirstName()) && columnCounter==0){
	 * setTempName(getFirstName()); if((getFirstName().indexOf("*") ==
	 * -1)&&(getFirstName().indexOf("%") == -1))
	 * setFirstName(getFirstName()+"*");
	 * setFirstName(getFirstName().replace("*","%"));
	 * queryStringBuffer.append("(FName LIKE '" + getFirstName() +
	 * "%' OR LName LIKE '" + getFirstName() + "') "); columnCounter ++; }else
	 * if(!"".equals(getFirstName()) && columnCounter!=0){
	 * setTempName(getFirstName()); if((getFirstName().indexOf("*") ==
	 * -1)&&(getFirstName().indexOf("%") == -1))
	 * setFirstName(getFirstName()+"*");
	 * setFirstName(getFirstName().replace("*","%"));
	 * queryStringBuffer.append("AND (FName LIKE '" + getFirstName() +
	 * "' OR LName LIKE '" + getFirstName() + "') "); } //
	 * if(!"".equals(getLastName()) && columnCounter==0){ //
	 * queryStringBuffer.append("LName LIKE '" + getLastName() + "%' "); //
	 * columnCounter ++; // }else if(!"".equals(getLastName()) &&
	 * columnCounter!=0){ // queryStringBuffer.append("AND LName LIKE '" +
	 * getLastName() + "%' "); // }
	 * 
	 * if(!"".equals(getWorkPhoneNo()) && columnCounter==0){
	 * setTempPh(getWorkPhoneNo()); if((getWorkPhoneNo().indexOf("*") ==
	 * -1)&&(getWorkPhoneNo().indexOf("%") == -1))
	 * setWorkPhoneNo(getWorkPhoneNo().replace("*","%"));
	 * setWorkPhoneNo(getWorkPhoneNo().replace("*","%"));
	 * queryStringBuffer.append("WorkPhoneNo LIKE '" + getWorkPhoneNo() + "' ");
	 * columnCounter ++; }else if(!"".equals(getWorkPhoneNo()) &&
	 * columnCounter!=0){ setTempPh(getWorkPhoneNo());
	 * if((getWorkPhoneNo().indexOf("*") == -1)&&(getWorkPhoneNo().indexOf("%")
	 * == -1)) setWorkPhoneNo(getWorkPhoneNo().replace("*","%"));
	 * queryStringBuffer.append("AND WorkPhoneNo LIKE '" + getWorkPhoneNo() +
	 * "' "); }
	 * 
	 * 
	 * if(!"".equals(getCurrStatus()) && columnCounter==0){
	 * queryStringBuffer.append("CurStatus ='" + getCurrStatus() + "' ");
	 * columnCounter ++; setTempCurStatus(getCurrStatus()); }else
	 * if(!"".equals(getCurrStatus()) && columnCounter!=0){
	 * queryStringBuffer.append("AND CurStatus ='" + getCurrStatus() + "' ");
	 * setTempCurStatus(getCurrStatus()); } else { setTempCurStatus("All"); }
	 * if(!"".equals(getOrgId()) && columnCounter==0){
	 * queryStringBuffer.append("OrgId='" + getOrgId() + "' "); columnCounter
	 * ++; setTempOrgId(getOrgId()); }else if(!"".equals(getOrgId()) &&
	 * columnCounter!=0){ queryStringBuffer.append("AND OrgId='" + getOrgId() +
	 * "' "); setTempOrgId(getOrgId()); }
	 * 
	 * 
	 * if(!"".equals(getReportingpersonId()) && columnCounter==0){
	 * queryStringBuffer.append("reportsTo='" + getReportingpersonId() + "' ");
	 * columnCounter ++;
	 * 
	 * }else if(!"".equals(getReportingpersonId()) && columnCounter!=0){
	 * queryStringBuffer.append("AND reportsTo='" + getReportingpersonId() +
	 * "' ");
	 * 
	 * }
	 * 
	 * if(!"".equals(getDepartmentId()) && columnCounter==0){
	 * queryStringBuffer.append("DepartmentId='" + getDepartmentId() + "' ");
	 * columnCounter ++; }else if(!"".equals(getDepartmentId()) &&
	 * columnCounter!=0){ queryStringBuffer.append("AND DepartmentId='" +
	 * getDepartmentId() + "' "); setTempDeptId(getDepartmentId()); }
	 * 
	 * if(!"".equals(getSubPractice()) && columnCounter==0){
	 * queryStringBuffer.append("SubPractice='" + getSubPractice() + "' ");
	 * columnCounter ++; }else if(!"".equals(getSubPractice()) &&
	 * columnCounter!=0){ queryStringBuffer.append("AND SubPractice='" +
	 * getSubPractice() + "' "); } if(!"".equals(getTeamId()) &&
	 * columnCounter==0){ queryStringBuffer.append("TeamId='" + getTeamId() +
	 * "' "); columnCounter ++; }else if(!"".equals(getTeamId()) &&
	 * columnCounter!=0){ queryStringBuffer.append("AND TeamId='" + getTeamId()
	 * + "' "); }
	 * 
	 * 
	 * if("searchFormMyTeam".equalsIgnoreCase(getSubmitFrom()) &&
	 * columnCounter==0){
	 * queryStringBuffer.append(" ReportsTo='"+httpServletRequest.getSession(
	 * false).getAttribute(ApplicationConstants.SESSION_USER_ID)+"' ");
	 * columnCounter ++;; }else
	 * if("searchFormMyTeam".equalsIgnoreCase(getSubmitFrom()) &&
	 * columnCounter!=0){
	 * queryStringBuffer.append(" AND ReportsTo='"+httpServletRequest.getSession
	 * (false).getAttribute(ApplicationConstants.SESSION_USER_ID)+"' ");
	 * columnCounter ++; }
	 * 
	 * if(columnCounter == 0){
	 * queryStringBuffer.append(" DeletedFlag != 1 ORDER BY trim(FName)"); }else
	 * if(columnCounter != 0){
	 * queryStringBuffer.append(" AND DeletedFlag != 1 ORDER BY trim(FName)"); }
	 * 
	 * 
	 * httpServletRequest.getSession(false).setAttribute(ApplicationConstants.
	 * QS_EMP_LIST,queryStringBuffer.toString());
	 * //System.err.println(httpServletRequest.getSession(false).getAttribute(
	 * ApplicationConstants.QS_EMP_LIST));
	 * queryStringBuffer.delete(0,queryStringBuffer.capacity()); }
	 * 
	 * if("dbGrid".equalsIgnoreCase(getSubmitFrom())){ queryStringBuffer = new
	 * StringBuffer();
	 * 
	 * queryStringBuffer.
	 * append("SELECT Id, DepartmentId, LName, FName, MName, Email1, WorkPhoneNo, AlterPhoneNo, CellPhoneNo, CurStatus,SubPractice,TeamId"
	 * ); queryStringBuffer.append(" FROM tblEmployee  "); //
	 * queryStringBuffer.append(" tblLKSubPractice");
	 * 
	 * if(null == getFirstName()) setFirstName(""); if(null == getLastName())
	 * setLastName(""); if(null == getWorkPhoneNo()) setWorkPhoneNo(""); if(null
	 * == getCurrStatus()) setCurrStatus("");
	 * if("All".equalsIgnoreCase(getCurrStatus())) setCurrStatus(""); if(null ==
	 * getOrgId()) setOrgId(""); if("All".equalsIgnoreCase(getOrgId()))
	 * setOrgId(""); if(null == getReportingpersonId())
	 * setReportingpersonId("");
	 * if("All".equalsIgnoreCase(getReportingpersonId()))
	 * setReportingpersonId(""); if(null== getDepartmentId())
	 * setDepartmentId(""); if(null==getSubPractice()) setSubPractice("");
	 * if("All".equalsIgnoreCase(getDepartmentId())) setDepartmentId("");
	 * if(null == getTeamId()) setTeamId("");
	 * if("All".equalsIgnoreCase(getTeamId())) setTeamId("");
	 * 
	 * 
	 * 
	 * 
	 * // if((!"".equals(getFirstName())) // || (!"".equals(getLastName())) //
	 * || (!"".equals(getWorkPhoneNo())) // || (!"".equals(getCurrStatus())) //
	 * ||(!"".equals(getOrgId()))){ // queryStringBuffer.append(" WHERE "); // }
	 * 
	 * // queryStringBuffer.append(" WHERE ");
	 * 
	 * if("Admin".equals(userRoleName)) { queryStringBuffer.append("where ");
	 * }else {
	 * queryStringBuffer.append(" WHERE  Country like '"+workingCountry+"' AND "
	 * ); } int columnCounter = 0;
	 * 
	 * 
	 * if(!"".equals(getFirstName()) && columnCounter==0){
	 * setTempName(getFirstName()); if((getFirstName().indexOf("*") ==
	 * -1)&&(getFirstName().indexOf("%") == -1))
	 * setFirstName(getFirstName()+"*");
	 * setFirstName(getFirstName().replace("*","%"));
	 * queryStringBuffer.append("(FName LIKE '" + getFirstName() +
	 * "%' OR LName LIKE '" + getFirstName() + "') "); columnCounter ++; }else
	 * if(!"".equals(getFirstName()) && columnCounter!=0){
	 * setTempName(getFirstName()); if((getFirstName().indexOf("*") ==
	 * -1)&&(getFirstName().indexOf("%") == -1))
	 * setFirstName(getFirstName()+"*");
	 * setFirstName(getFirstName().replace("*","%"));
	 * queryStringBuffer.append("AND (FName LIKE '" + getFirstName() +
	 * "' OR LName LIKE '" + getFirstName() + "') "); } //
	 * if(!"".equals(getLastName()) && columnCounter==0){ //
	 * queryStringBuffer.append("LName LIKE '" + getLastName() + "%' "); //
	 * columnCounter ++; // }else if(!"".equals(getLastName()) &&
	 * columnCounter!=0){ // queryStringBuffer.append("AND LName LIKE '" +
	 * getLastName() + "%' "); // }
	 * 
	 * if(!"".equals(getWorkPhoneNo()) && columnCounter==0){
	 * setTempPh(getWorkPhoneNo());
	 * 
	 * 
	 * if((getWorkPhoneNo().indexOf("*") == -1)&&(getWorkPhoneNo().indexOf("%")
	 * == -1)) setWorkPhoneNo(getWorkPhoneNo().replace("*","%"));
	 * setWorkPhoneNo(getWorkPhoneNo().replace("*","%"));
	 * queryStringBuffer.append("WorkPhoneNo LIKE '" + getWorkPhoneNo() + "' ");
	 * 
	 * columnCounter ++; }else if(!"".equals(getWorkPhoneNo()) &&
	 * columnCounter!=0){ setTempPh(getWorkPhoneNo());
	 * if((getWorkPhoneNo().indexOf("*") == -1)&&(getWorkPhoneNo().indexOf("%")
	 * == -1)) setWorkPhoneNo(getWorkPhoneNo().replace("*","%"));
	 * queryStringBuffer.append("AND WorkPhoneNo LIKE '" + getWorkPhoneNo() +
	 * "' "); }
	 * 
	 * 
	 * if(!"".equals(getCurrStatus()) && columnCounter==0){
	 * queryStringBuffer.append("CurStatus ='" + getCurrStatus() + "' ");
	 * columnCounter ++; setTempCurStatus(getCurrStatus()); }else
	 * if(!"".equals(getCurrStatus()) && columnCounter!=0){
	 * queryStringBuffer.append("AND CurStatus ='" + getCurrStatus() + "' ");
	 * setTempCurStatus(getCurrStatus()); } else { setTempCurStatus("All"); }
	 * if(!"".equals(getOrgId()) && columnCounter==0){
	 * queryStringBuffer.append("OrgId='" + getOrgId() + "' "); columnCounter
	 * ++; setTempOrgId(getOrgId()); }else if(!"".equals(getOrgId()) &&
	 * columnCounter!=0){ queryStringBuffer.append("AND OrgId='" + getOrgId() +
	 * "' "); setTempOrgId(getOrgId()); }
	 * 
	 * 
	 * if(!"".equals(getReportingpersonId()) && columnCounter==0){
	 * queryStringBuffer.append("reportsTo='" + getReportingpersonId() + "' ");
	 * columnCounter ++;
	 * 
	 * }else if(!"".equals(getReportingpersonId()) && columnCounter!=0){
	 * queryStringBuffer.append("AND reportsTo='" + getReportingpersonId() +
	 * "' ");
	 * 
	 * }
	 * 
	 * if(!"".equals(getDepartmentId()) && columnCounter==0){
	 * queryStringBuffer.append("DepartmentId='" + getDepartmentId() + "' ");
	 * columnCounter ++; }else if(!"".equals(getDepartmentId()) &&
	 * columnCounter!=0){ queryStringBuffer.append("AND DepartmentId='" +
	 * getDepartmentId() + "' "); setTempDeptId(getDepartmentId()); }
	 * 
	 * if(!"".equals(getSubPractice()) && columnCounter==0){
	 * queryStringBuffer.append("SubPractice='" + getSubPractice() + "' ");
	 * columnCounter ++; }else if(!"".equals(getSubPractice()) &&
	 * columnCounter!=0){ queryStringBuffer.append("AND SubPractice='" +
	 * getSubPractice() + "' "); } if(!"".equals(getTeamId()) &&
	 * columnCounter==0){ queryStringBuffer.append("TeamId='" + getTeamId() +
	 * "' "); columnCounter ++; }else if(!"".equals(getTeamId()) &&
	 * columnCounter!=0){ queryStringBuffer.append("AND TeamId='" + getTeamId()
	 * + "' "); }
	 * 
	 * 
	 * if("searchFormMyTeam".equalsIgnoreCase(getSubmitFrom()) &&
	 * columnCounter==0){
	 * queryStringBuffer.append(" ReportsTo='"+httpServletRequest.getSession(
	 * false).getAttribute(ApplicationConstants.SESSION_USER_ID)+"' ");
	 * columnCounter ++; }else
	 * if("searchFormMyTeam".equalsIgnoreCase(getSubmitFrom()) &&
	 * columnCounter!=0){
	 * queryStringBuffer.append(" AND ReportsTo='"+httpServletRequest.getSession
	 * (false).getAttribute(ApplicationConstants.SESSION_USER_ID)+"' ");
	 * columnCounter ++; } }
	 * 
	 * if(columnCounter == 0){
	 * queryStringBuffer.append(" DeletedFlag != 1 ORDER BY trim(FName)"); }else
	 * if(columnCounter != 0){
	 * queryStringBuffer.append(" AND DeletedFlag != 1 ORDER BY trim(FName)"); }
	 * 
	 * //System.out.println("queryStringBuffer------>"+queryStringBuffer);
	 * httpServletRequest.getSession(false).setAttribute(ApplicationConstants.
	 * QS_EMP_LIST,queryStringBuffer.toString());
	 * 
	 * queryStringBuffer.delete(0,queryStringBuffer.capacity());
	 * 
	 * } //Calling searchPrepare() method to populate select components
	 * searchPrepare(); prepare(); setFirstName(getTempName());
	 * setWorkPhoneNo(getTempPh()); setCurrStatus(getTempCurStatus());
	 * setDepartmentId(getTempDeptId()); setOrgId(getTempOrgId());
	 * setReportingpersonId(getReportingpersonId()); //
	 * System.err.println(getReportingpersonId());
	 * 
	 * resultType = SUCCESS; }catch (Exception ex){ //List errorMsgList =
	 * ExceptionToListUtility.errorMessages(ex);
	 * httpServletRequest.getSession(false).setAttribute("errorMessage",ex.
	 * toString()); resultType = ERROR; } }//END-Authorization Checking }//Close
	 * Session Checking //System.err.println("resultType"+resultType); return
	 * resultType; }
	 * 
	 */
	public String getSearchQuery() {

		// System.out.println("I am in getSearchQuery() +getOrgId());
		resultType = LOGIN;

		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {

			userRoleId = Integer.parseInt(
					httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_ROLE_ID).toString());
			String workingCountry = httpServletRequest.getSession(false)
					.getAttribute(ApplicationConstants.WORKING_COUNTRY).toString();
			String livingCountry = httpServletRequest.getSession(false)
					.getAttribute(ApplicationConstants.Living_COUNTRY).toString();
			resultType = "accessFailed";
			String userRoleName = httpServletRequest.getSession(false)
					.getAttribute(ApplicationConstants.SESSION_ROLE_NAME).toString();
			if (AuthorizationManager.getInstance().isAuthorizedUser("GET_SEARCH_QUERY_EMP", userRoleId)) {
				try {
				//	System.out.println("getOutSideAccess()====>"+getOutSideAccess());
					// setPracticeIdList(dataSourceDataProvider.getPracticeByDepartment(getDepartmentId()));
					// setPracticeId(getCurrentEmployee().getPracticeId());
					// setSubPractice(getCurrentEmployee().getSubPractice());

					// System.err.println(getSubmitFrom());
				//	System.out.println("getPayRollBatchUs()====>"+getPayRollBatchUs());

					if ("searchFormAll".equalsIgnoreCase(getSubmitFrom())
							|| "searchFormMyTeam".equalsIgnoreCase(getSubmitFrom())) {
						queryStringBuffer = new StringBuilder();

						queryStringBuffer.append(
								"SELECT Id, DepartmentId, LName, FName, MName,EmpNo, Email1, WorkPhoneNo, AlterPhoneNo, CellPhoneNo, CurStatus,SubPractice,TeamId ,LoginId ");
						queryStringBuffer.append(" FROM tblEmployee ");

						// queryStringBuffer.append(" tblLKSubPractice");

						if (null == getFirstName()) {
							setFirstName("");
						}
						if (null == getLastName()) {
							setLastName("");
						}
						if (null == getWorkPhoneNo()) {
							setWorkPhoneNo("");
						}
						if (null == getCurrStatus()) {
							setCurrStatus("");
						}
						if (null == getEmpno()) {
							setEmpno("");
						}
						if ("All".equalsIgnoreCase(getCurrStatus())) {
							setCurrStatus("");
						}
						if (null == getOrgId()) {
							setOrgId("");
						}
						if ("All".equalsIgnoreCase(getOrgId())) {
							setOrgId("");
						}
						if (null == getReportingpersonId()) {
							setReportingpersonId("");
						}
						if ("All".equalsIgnoreCase(getReportingpersonId())) {
							setReportingpersonId("");
						}
						if (null == getPayRollBatchUs()) {
							setPayRollBatchUs("");
						}
						if ("All".equalsIgnoreCase(getPayRollBatchUs())) {
							setPayRollBatchUs("");
						}
						if (null == getOrgId()) {
							setOrgId("");
						}
						if ("All".equalsIgnoreCase(getOrgId())) {
							setOrgId("");
						}
						if (null == getDepartmentId()) {
							setDepartmentId("");
						}
						if (null == getSubPractice()) {
							setSubPractice("");
						}
						if ("All".equalsIgnoreCase(getDepartmentId())) {
							setDepartmentId("");
						}
						if (null == getTeamId()) {
							setTeamId("");
						}
						if ("All".equalsIgnoreCase(getTeamId())) {
							setTeamId("");
						}
						if (null == getCountry()) {
							setCountry("");
						}
						if (null == getPracticeId()) {
							setPracticeId("");
						}
						if ("All".equalsIgnoreCase(getLocation())) {
							setLocation("");
						}
						if (null == getItgBatch()) {
							setItgBatch("");
						}

						// if((!"".equals(getFirstName()))
						// || (!"".equals(getLastName()))
						// || (!"".equals(getWorkPhoneNo()))
						// || (!"".equals(getCurrStatus()))
						// ||(!"".equals(getOrgId()))){
						// queryStringBuffer.append(" WHERE ");
						// }
						if ("Admin".equals(userRoleName)) {
							queryStringBuffer.append("where ");
						} // else {
							// queryStringBuffer.append(" WHERE Country like '"
							// + livingCountry + "' AND ");
							// }
						else {
							if (livingCountry.equalsIgnoreCase("USA")) {
								queryStringBuffer.append(" WHERE  Country != 'India' AND ");
							} else {
								queryStringBuffer.append(" WHERE Country like '"+workingCountry+"' AND ");

							}

						}
						int columnCounter = 0;
						if (getIsManager() && columnCounter == 0) {
							setIsManager(getIsManager());

							queryStringBuffer.append("IsManager =1 ");
							columnCounter++;
						} else if (getIsManager() && columnCounter != 0) {
							setIsManager(getIsManager());
							queryStringBuffer.append("AND IsManager =1 ");
						}
						if (isIsTeamLead() && columnCounter == 0) {
							setIsTeamLead(isIsTeamLead());

							queryStringBuffer.append("IsTeamLead =1 ");
							columnCounter++;
						} else if (isIsTeamLead() && columnCounter != 0) {
							setIsTeamLead(isIsTeamLead());
							queryStringBuffer.append("AND IsTeamLead =1 ");
						}
					//	System.out.println("getOutSideAccess()====>"+getOutSideAccess());
						if (getOutSideAccess() && columnCounter == 0) {
							setOutSideAccess(getOutSideAccess());

							queryStringBuffer.append("OutSideAccess =1 ");
							columnCounter++;
						} else if (getOutSideAccess() && columnCounter != 0) {
							setOutSideAccess(getOutSideAccess());
							queryStringBuffer.append("AND OutSideAccess =1 ");
						}
						
						
						
						if (getEmpTypeId() != null && !"".equals(getEmpTypeId()) && columnCounter == 0) {
							queryStringBuffer.append(" EmployeeTypeId LIKE  '" + getEmpTypeId() +"'");
							columnCounter++;
						} else if (getEmpTypeId() != null && !"1".equals(getEmpTypeId()) && columnCounter != 0) {
							queryStringBuffer.append(" AND EmployeeTypeId =  '" + getEmpTypeId() +"'");
						}
						
						
						if (!"".equals(getFirstName()) && columnCounter == 0) {
							setTempName(getFirstName());
							if ((getFirstName().indexOf("*") == -1) && (getFirstName().indexOf("%") == -1)) {
								setFirstName(getFirstName() + "*");
							}
							setFirstName(getFirstName().replace("*", "%"));
							queryStringBuffer.append(
									"(FName LIKE '" + getFirstName() + "%' OR LName LIKE '" + getFirstName() + "') ");
							columnCounter++;
						} else if (!"".equals(getFirstName()) && columnCounter != 0) {
							setTempName(getFirstName());
							if ((getFirstName().indexOf("*") == -1) && (getFirstName().indexOf("%") == -1)) {
								setFirstName(getFirstName() + "*");
							}
							setFirstName(getFirstName().replace("*", "%"));
							queryStringBuffer.append("AND (FName LIKE '" + getFirstName() + "' OR LName LIKE '"
									+ getFirstName() + "') ");
						}

						if (!"".equals(getEmail1()) && columnCounter == 0) {
							setTempEmail(getEmail1());
							if ((getEmail1().indexOf("*") == -1) && (getEmail1().indexOf("%") == -1)) {
								setEmail1(getEmail1() + "*");
							}
							setEmail1(getEmail1().replace("*", "%"));
							queryStringBuffer.append("Email1 LIKE '" + getEmail1() + "' ");
							columnCounter++;
						} else if (!"".equals(getEmail1()) && columnCounter != 0) {
							setTempEmail(getEmail1());
							if ((getEmail1().indexOf("*") == -1) && (getEmail1().indexOf("%") == -1)) {
								setEmail1(getEmail1() + "*");
							}
							setEmail1(getEmail1().replace("*", "%"));
							queryStringBuffer.append("AND Email1 LIKE '" + getEmail1() + "' ");
						}

						// if(!"".equals(getLastName()) && columnCounter==0){
						// queryStringBuffer.append("LName LIKE '" +
						// getLastName() + "%' ");
						// columnCounter ++;
						// }else if(!"".equals(getLastName()) &&
						// columnCounter!=0){
						// queryStringBuffer.append("AND LName LIKE '" +
						// getLastName() + "%' ");
						// }

						if (!"".equals(getWorkPhoneNo()) && columnCounter == 0) {
							setTempPh(getWorkPhoneNo());
							if ((getWorkPhoneNo().indexOf("*") == -1) && (getWorkPhoneNo().indexOf("%") == -1)) {
								setWorkPhoneNo(getWorkPhoneNo().replace("*", "%"));
							}
							setWorkPhoneNo(getWorkPhoneNo().replace("*", "%"));
							queryStringBuffer
									.append("REPLACE(REPLACE(REPLACE(WorkPhoneNo,'(',''),')',''),'-','') LIKE '%"
											+ getWorkPhoneNo() + "%' ");
							columnCounter++;
						} else if (!"".equals(getWorkPhoneNo()) && columnCounter != 0) {
							setTempPh(getWorkPhoneNo());
							if ((getWorkPhoneNo().indexOf("*") == -1) && (getWorkPhoneNo().indexOf("%") == -1)) {
								setWorkPhoneNo(getWorkPhoneNo().replace("*", "%"));
							}
							queryStringBuffer
									.append("AND REPLACE(REPLACE(REPLACE(WorkPhoneNo,'(',''),')',''),'-','') LIKE '%"
											+ getWorkPhoneNo() + "%' ");
						}

						if (!"".equals(getCurrStatus()) && columnCounter == 0) {
							queryStringBuffer.append("CurStatus ='" + getCurrStatus() + "' ");
							columnCounter++;
							setTempCurStatus(getCurrStatus());
						} else if (!"".equals(getCurrStatus()) && columnCounter != 0) {
							queryStringBuffer.append("AND CurStatus ='" + getCurrStatus() + "' ");
							setTempCurStatus(getCurrStatus());
						} else {
							setTempCurStatus("All");
						}
						if (!"".equals(getOrgId()) && columnCounter == 0) {
							queryStringBuffer.append("OrgId='" + getOrgId() + "' ");
							columnCounter++;
							setTempOrgId(getOrgId());
						} else if (!"".equals(getOrgId()) && columnCounter != 0) {
							queryStringBuffer.append("AND OrgId='" + getOrgId() + "' ");
							setTempOrgId(getOrgId());
						}

						if (!"".equals(getReportingpersonId()) && columnCounter == 0) {
							queryStringBuffer.append("reportsTo='" + getReportingpersonId() + "' ");
							columnCounter++;

						} else if (!"".equals(getReportingpersonId()) && columnCounter != 0) {
							queryStringBuffer.append("AND reportsTo='" + getReportingpersonId() + "' ");

						}
						
						//System.out.println("getPayRollBatchUs()====>"+getPayRollBatchUs());
						if (!"".equals(getPayRollBatchUs()) && columnCounter == 0) {
							queryStringBuffer.append("PayrollBatchUs='" + getPayRollBatchUs() + "' ");
							columnCounter++;

						} else if (!"".equals(getPayRollBatchUs()) && columnCounter != 0) {
							queryStringBuffer.append("AND PayrollBatchUs='" + getPayRollBatchUs() + "' ");

						}
						

						if (!"".equals(getDepartmentId()) && columnCounter == 0) {
							queryStringBuffer.append("DepartmentId='" + getDepartmentId() + "' ");
							columnCounter++;
						} else if (!"".equals(getDepartmentId()) && columnCounter != 0) {
							queryStringBuffer.append("AND DepartmentId='" + getDepartmentId() + "' ");
							setTempDeptId(getDepartmentId());
						}

						if (!"".equals(getSubPractice()) && columnCounter == 0) {
							queryStringBuffer.append("SubPractice='" + getSubPractice() + "' ");
							columnCounter++;
						} else if (!"".equals(getSubPractice()) && columnCounter != 0) {
							queryStringBuffer.append("AND SubPractice='" + getSubPractice() + "' ");
						}
						if (!"".equals(getTeamId()) && columnCounter == 0) {
							queryStringBuffer.append("TeamId='" + getTeamId() + "' ");
							columnCounter++;
						} else if (!"".equals(getTeamId()) && columnCounter != 0) {
							queryStringBuffer.append("AND TeamId='" + getTeamId() + "' ");
						}
						if (!"".equals(getEmpno()) && columnCounter == 0) {
							queryStringBuffer.append("EmpNo=" + getEmpno() + " ");
							columnCounter++;

						} else if (!"".equals(getEmpno()) && columnCounter != 0) {
							queryStringBuffer.append("AND EmpNo=" + getEmpno() + " ");

						}

						if ("searchFormMyTeam".equalsIgnoreCase(getSubmitFrom()) && columnCounter == 0) {
							queryStringBuffer.append(" ReportsTo='" + httpServletRequest.getSession(false)
									.getAttribute(ApplicationConstants.SESSION_USER_ID) + "' ");
							columnCounter++;
							;
						} else if ("searchFormMyTeam".equalsIgnoreCase(getSubmitFrom()) && columnCounter != 0) {
							queryStringBuffer.append(" AND ReportsTo='" + httpServletRequest.getSession(false)
									.getAttribute(ApplicationConstants.SESSION_USER_ID) + "' ");
							columnCounter++;
						}

						if (columnCounter == 0) {
							queryStringBuffer.append(" DeletedFlag != 1 ");
							columnCounter++;
						} else if (columnCounter != 0) {
							queryStringBuffer.append(" AND DeletedFlag != 1 ");
						}

						if (getSkillSet() != null && !"".equals(getSkillSet())) {
							// System.out.println("getSkillSet---" +
							// getSkillSet());
							String skillsREsult = "";
							String skills = getSkillSet();
							if (!skills.contains("-")) {
								skills += "-";
							}
							String add = skills.substring(0, skills.indexOf("-"));
							String remove = skills.substring(skills.indexOf("-") + 1, skills.length());
							String orCaseSkills[] = add.split(Pattern.quote("|"));
							queryStringBuffer.append("AND ((");
							for (int i = 0; i < orCaseSkills.length; i++) {
								String string = orCaseSkills[i];
								String skill[] = orCaseSkills[i].split(Pattern.quote("&"));
								// System.out.println(string);
								queryStringBuffer.append("(");
								for (int j = 0; j < skill.length; j++) {
									// String string1 = skill[j];
									skillsREsult = DataSourceDataProvider.getInstance().getEmpIDsBySkill(skill[j]);
									if (skillsREsult.length() > 0) {
										queryStringBuffer.append("tblEmployee.Id IN (" + skillsREsult + ")");
										if (j < skill.length - 1) {
											queryStringBuffer.append(" AND ");
										}
									}
									if (j == skill.length - 1) {
										queryStringBuffer.append(")");
									}

								}
								if (i < orCaseSkills.length - 1) {
									queryStringBuffer.append(" OR ");
								}
								if (i == orCaseSkills.length - 1) {
									queryStringBuffer.append(")");
								}
							}

							String removedSkills[] = remove.split(Pattern.quote("-"));

							if (removedSkills.length > 0 && !removedSkills[0].equals("")) {
								queryStringBuffer.append(" AND (");
								for (int i = 0; i < removedSkills.length; i++) {
									String string = removedSkills[i];
									// System.out.print(string);
									skillsREsult = DataSourceDataProvider.getInstance()
											.getEmpIDsBySkill(removedSkills[i]);
									if (skillsREsult.length() > 0) {
										queryStringBuffer.append("tblEmployee.Id NOT IN (" + skillsREsult + ")");
										if (i < removedSkills.length - 1) {
											queryStringBuffer.append(" AND ");
										}
									}
									if (i == removedSkills.length - 1) {
										queryStringBuffer.append(")");
									}
								}
							}
							queryStringBuffer.append(")");

							// queryStringBuffer.append(" " + skillsREsult);
						}
						if (getOpsContactId() != null && !"".equals(getOpsContactId()) && columnCounter == 0) {
							queryStringBuffer.append(" OpsContactId= " + getOpsContactId());
							columnCounter++;
						} else if (getOpsContactId() != null && !"1".equals(getOpsContactId()) && columnCounter != 0) {
							queryStringBuffer.append(" AND OpsContactId= " + getOpsContactId());
						}
						if (getCountry() != null && !"".equals(getCountry()) && columnCounter == 0) {
							queryStringBuffer.append(" Country='" + getCountry() + "' ");
							columnCounter++;
						} else if (getCountry() != null && !"".equals(getCountry()) && columnCounter != 0) {
							queryStringBuffer.append(" AND Country='" + getCountry() + "' ");
							// setTempDeptId(getCountry());
						}
						if (getPracticeId() != null && !"".equals(getPracticeId()) && columnCounter == 0) {
							queryStringBuffer.append(" PracticeId='" + getPracticeId() + "' ");
							columnCounter++;
						} else if (getPracticeId() != null && !"".equals(getPracticeId()) && columnCounter != 0) {
							queryStringBuffer.append(" AND PracticeId='" + getPracticeId() + "' ");
							// setPracticeId(getPracticeId());
						}
						if (getLocation() != null && !"".equals(getLocation()) && columnCounter == 0) {
							queryStringBuffer.append(" Location='" + getLocation() + "' ");
							columnCounter++;
						} else if (getLocation() != null && !"".equals(getLocation()) && columnCounter != 0) {
							queryStringBuffer.append(" AND Location='" + getLocation() + "' ");

						}

						if (!"".equals(getItgBatch()) && columnCounter == 0) {
							queryStringBuffer.append(" Itgbatch='" + getItgBatch() + "' ");
							columnCounter++;

						} else if (!"".equals(getItgBatch()) && columnCounter != 0) {
							queryStringBuffer.append(" AND Itgbatch='" + getItgBatch() + "' ");

						}
						queryStringBuffer.append("  ORDER BY trim(FName)");

					//	 System.out.println("queryStringBuffer.toString()---"+queryStringBuffer.toString());
						httpServletRequest.getSession(false).setAttribute(ApplicationConstants.QS_EMP_LIST,
								queryStringBuffer.toString());
						// System.err.println(httpServletRequest.getSession(false).getAttribute(ApplicationConstants.QS_EMP_LIST));
						queryStringBuffer.delete(0, queryStringBuffer.capacity());
					//	System.out.println("queryStringBuffer====>"+queryStringBuffer.toString());
					}

					if ("dbGrid".equalsIgnoreCase(getSubmitFrom())) {
						queryStringBuffer = new StringBuilder();

						queryStringBuffer.append(
								"SELECT Id, DepartmentId, LName, FName, MName,EmpNo, Email1, WorkPhoneNo, AlterPhoneNo, CellPhoneNo, CurStatus,SubPractice,TeamId ,LoginId ");
						queryStringBuffer.append(" FROM tblEmployee  ");
						// queryStringBuffer.append(" tblLKSubPractice");

						if (null == getFirstName()) {
							setFirstName("");
						}
						if (null == getLastName()) {
							setLastName("");
						}
						if (null == getWorkPhoneNo()) {
							setWorkPhoneNo("");
						}
						if (null == getCurrStatus()) {
							setCurrStatus("");
						}
						if (null == getEmpno()) {
							setEmpno("");
						}

						if ("All".equalsIgnoreCase(getCurrStatus())) {
							setCurrStatus("");
						}
						if (null == getOrgId()) {
							setOrgId("");
						}
						if ("All".equalsIgnoreCase(getOrgId())) {
							setOrgId("");
						}
						if (null == getReportingpersonId()) {
							setReportingpersonId("");
						}
						if ("All".equalsIgnoreCase(getReportingpersonId())) {
							setReportingpersonId("");
						}
						
						if (null == getPayRollBatchUs()) {
							setPayRollBatchUs("");
						}
						if ("All".equalsIgnoreCase(getPayRollBatchUs())) {
							setPayRollBatchUs("");
						}
						
						if (null == getDepartmentId()) {
							setDepartmentId("");
						}
						if (null == getSubPractice()) {
							setSubPractice("");
						}
						if ("All".equalsIgnoreCase(getDepartmentId())) {
							setDepartmentId("");
						}
						if (null == getTeamId()) {
							setTeamId("");
						}
						if ("All".equalsIgnoreCase(getTeamId())) {
							setTeamId("");
						}
						if (null == getCountry()) {
							setCountry("");
						}
						if (null == getPracticeId()) {
							setPracticeId("");
						}
						if ("All".equalsIgnoreCase(getLocation())) {
							setLocation("");
						}

						if (null == getItgBatch()) {
							setItgBatch("");
						}

						// if((!"".equals(getFirstName()))
						// || (!"".equals(getLastName()))
						// || (!"".equals(getWorkPhoneNo()))
						// || (!"".equals(getCurrStatus()))
						// ||(!"".equals(getOrgId()))){
						// queryStringBuffer.append(" WHERE ");
						// }

						// queryStringBuffer.append(" WHERE ");

						if ("Admin".equals(userRoleName)) {
							queryStringBuffer.append("where ");
						} // else {
							// queryStringBuffer.append(" WHERE Country like '"
							// + livingCountry + "' AND ");
							// }
						else {
							if (livingCountry.equalsIgnoreCase("USA")) {
								queryStringBuffer.append(" WHERE  Country != 'India' AND ");
							} else {
								queryStringBuffer.append(" WHERE Country like '"+workingCountry+"' AND ");
							}

						}
						int columnCounter = 0;
						if (getIsManager() && columnCounter == 0) {
							setIsManager(getIsManager());

							queryStringBuffer.append("IsManager =1 ");
							columnCounter++;
						} else if (getIsManager() && columnCounter != 0) {
							setIsManager(getIsManager());
							queryStringBuffer.append("AND IsManager =1 ");
						}
						if (isIsTeamLead() && columnCounter == 0) {
							setIsTeamLead(isIsTeamLead());

							queryStringBuffer.append("IsTeamLead =1 ");
							columnCounter++;
						} else if (isIsTeamLead() && columnCounter != 0) {
							setIsTeamLead(isIsTeamLead());
							queryStringBuffer.append("AND IsTeamLead =1 ");
						}
						if (getOutSideAccess() && columnCounter == 0) {
							setOutSideAccess(getOutSideAccess());

							queryStringBuffer.append("OutSideAccess =1 ");
							columnCounter++;
						} else if (getOutSideAccess() && columnCounter != 0) {
							setOutSideAccess(getOutSideAccess());
							queryStringBuffer.append("AND OutSideAccess =1 ");
						}
						
						
						if (getEmpTypeId() != null && !"".equals(getEmpTypeId()) && columnCounter == 0) {
							queryStringBuffer.append(" EmployeeTypeId LIKE  '" + getEmpTypeId() +"'");
							columnCounter++;
						} else if (getEmpTypeId() != null && !"1".equals(getEmpTypeId()) && columnCounter != 0) {
							queryStringBuffer.append(" AND EmployeeTypeId =  '" + getEmpTypeId() +"'");
						}
						
						if (!"".equals(getFirstName()) && columnCounter == 0) {
							setTempName(getFirstName());
							if ((getFirstName().indexOf("*") == -1) && (getFirstName().indexOf("%") == -1)) {
								setFirstName(getFirstName() + "*");
							}
							setFirstName(getFirstName().replace("*", "%"));
							queryStringBuffer.append(
									"(FName LIKE '" + getFirstName() + "%' OR LName LIKE '" + getFirstName() + "') ");
							columnCounter++;
						} else if (!"".equals(getFirstName()) && columnCounter != 0) {
							setTempName(getFirstName());
							if ((getFirstName().indexOf("*") == -1) && (getFirstName().indexOf("%") == -1)) {
								setFirstName(getFirstName() + "*");
							}
							setFirstName(getFirstName().replace("*", "%"));
							queryStringBuffer.append("AND (FName LIKE '" + getFirstName() + "' OR LName LIKE '"
									+ getFirstName() + "') ");
						}

						if (!"".equals(getEmail1()) && columnCounter == 0) {
							setTempEmail(getEmail1());
							if ((getEmail1().indexOf("*") == -1) && (getEmail1().indexOf("%") == -1)) {
								setEmail1(getEmail1() + "*");
							}
							setEmail1(getEmail1().replace("*", "%"));
							queryStringBuffer.append("Email1 LIKE '" + getEmail1() + "' ");
							columnCounter++;
						} else if (!"".equals(getEmail1()) && columnCounter != 0) {
							setTempEmail(getEmail1());
							if ((getEmail1().indexOf("*") == -1) && (getEmail1().indexOf("%") == -1)) {
								setEmail1(getEmail1() + "*");
							}
							setEmail1(getEmail1().replace("*", "%"));
							queryStringBuffer.append("AND Email1 LIKE '" + getEmail1() + "' ");
						}
						// if(!"".equals(getLastName()) && columnCounter==0){
						// queryStringBuffer.append("LName LIKE '" +
						// getLastName() + "%' ");
						// columnCounter ++;
						// }else if(!"".equals(getLastName()) &&
						// columnCounter!=0){
						// queryStringBuffer.append("AND LName LIKE '" +
						// getLastName() + "%' ");
						// }

						if (!"".equals(getWorkPhoneNo()) && columnCounter == 0) {
							setTempPh(getWorkPhoneNo());

							if ((getWorkPhoneNo().indexOf("*") == -1) && (getWorkPhoneNo().indexOf("%") == -1)) {
								setWorkPhoneNo(getWorkPhoneNo().replace("*", "%"));
							}
							setWorkPhoneNo(getWorkPhoneNo().replace("*", "%"));
							queryStringBuffer
									.append("REPLACE(REPLACE(REPLACE(WorkPhoneNo,'(',''),')',''),'-','') LIKE '%"
											+ getWorkPhoneNo() + "%' ");

							columnCounter++;
						} else if (!"".equals(getWorkPhoneNo()) && columnCounter != 0) {
							setTempPh(getWorkPhoneNo());
							if ((getWorkPhoneNo().indexOf("*") == -1) && (getWorkPhoneNo().indexOf("%") == -1)) {
								setWorkPhoneNo(getWorkPhoneNo().replace("*", "%"));
							}
							queryStringBuffer
									.append("AND REPLACE(REPLACE(REPLACE(WorkPhoneNo,'(',''),')',''),'-','') LIKE '%"
											+ getWorkPhoneNo() + "%' ");
						}

						if (!"".equals(getCurrStatus()) && columnCounter == 0) {
							queryStringBuffer.append("CurStatus ='" + getCurrStatus() + "' ");
							columnCounter++;
							setTempCurStatus(getCurrStatus());
						} else if (!"".equals(getCurrStatus()) && columnCounter != 0) {
							queryStringBuffer.append("AND CurStatus ='" + getCurrStatus() + "' ");
							setTempCurStatus(getCurrStatus());
						} else {
							setTempCurStatus("All");
						}
						if (!"".equals(getOrgId()) && columnCounter == 0) {
							queryStringBuffer.append("OrgId='" + getOrgId() + "' ");
							columnCounter++;
							setTempOrgId(getOrgId());
						} else if (!"".equals(getOrgId()) && columnCounter != 0) {
							queryStringBuffer.append("AND OrgId='" + getOrgId() + "' ");
							setTempOrgId(getOrgId());
						}

						if (!"".equals(getReportingpersonId()) && columnCounter == 0) {
							queryStringBuffer.append("reportsTo='" + getReportingpersonId() + "' ");
							columnCounter++;

						} else if (!"".equals(getReportingpersonId()) && columnCounter != 0) {
							queryStringBuffer.append("AND reportsTo='" + getReportingpersonId() + "' ");

						}

						if (!"".equals(getDepartmentId()) && columnCounter == 0) {
							queryStringBuffer.append("DepartmentId='" + getDepartmentId() + "' ");
							columnCounter++;
						} else if (!"".equals(getDepartmentId()) && columnCounter != 0) {
							queryStringBuffer.append("AND DepartmentId='" + getDepartmentId() + "' ");
							setTempDeptId(getDepartmentId());
						}

						if (!"".equals(getSubPractice()) && columnCounter == 0) {
							queryStringBuffer.append("SubPractice='" + getSubPractice() + "' ");
							columnCounter++;
						} else if (!"".equals(getSubPractice()) && columnCounter != 0) {
							queryStringBuffer.append("AND SubPractice='" + getSubPractice() + "' ");
						}
						if (!"".equals(getTeamId()) && columnCounter == 0) {
							queryStringBuffer.append("TeamId='" + getTeamId() + "' ");
							columnCounter++;
						} else if (!"".equals(getTeamId()) && columnCounter != 0) {
							queryStringBuffer.append("AND TeamId='" + getTeamId() + "' ");
						}

						if (!"".equals(getEmpno()) && columnCounter == 0) {
							queryStringBuffer.append("EmpNo=" + getEmpno() + " ");
							columnCounter++;

						} else if (!"".equals(getEmpno()) && columnCounter != 0) {
							queryStringBuffer.append("AND EmpNo=" + getEmpno() + " ");

						}
						
						if (!"".equals(getPayRollBatchUs()) && columnCounter == 0) {
							queryStringBuffer.append("PayrollBatchUs='" + getPayRollBatchUs() + "' ");
							columnCounter++;

						} else if (!"".equals(getPayRollBatchUs()) && columnCounter != 0) {
							queryStringBuffer.append("AND PayrollBatchUs='" + getPayRollBatchUs() + "' ");

						}
						
						if ("searchFormMyTeam".equalsIgnoreCase(getSubmitFrom()) && columnCounter == 0) {
							queryStringBuffer.append(" ReportsTo='" + httpServletRequest.getSession(false)
									.getAttribute(ApplicationConstants.SESSION_USER_ID) + "' ");
							columnCounter++;
						} else if ("searchFormMyTeam".equalsIgnoreCase(getSubmitFrom()) && columnCounter != 0) {
							queryStringBuffer.append(" AND ReportsTo='" + httpServletRequest.getSession(false)
									.getAttribute(ApplicationConstants.SESSION_USER_ID) + "' ");
							columnCounter++;
						}
						/*
						 * else
						 * if("dbGrid".equalsIgnoreCase(getSubmitFromReportsTo()
						 * ) && columnCounter==0){
						 * queryStringBuffer.append(" ReportsTo='"
						 * +httpServletRequest.getSession(false).getAttribute(
						 * ApplicationConstants.SESSION_USER_ID)+"' ");
						 * columnCounter ++; } else
						 * if("dbGrid".equalsIgnoreCase(getSubmitFromReportsTo()
						 * ) && columnCounter!=0){
						 * queryStringBuffer.append(" AND ReportsTo='"
						 * +httpServletRequest.getSession(false).getAttribute(
						 * ApplicationConstants.SESSION_USER_ID)+"' ");
						 * columnCounter ++; }
						 */
						if (columnCounter == 0) {
							queryStringBuffer.append(" DeletedFlag != 1 ");
							columnCounter++;
						} else if (columnCounter != 0) {
							queryStringBuffer.append(" AND DeletedFlag != 1 ");
						}

						if (getSkillSet() != null && !"".equals(getSkillSet())) {
							// System.out.println("getSkillSet---" +
							// getSkillSet());
							String skillsREsult = "";
							String skills = getSkillSet();
							if (!skills.contains("-")) {
								skills += "-";
							}
							String add = skills.substring(0, skills.indexOf("-"));
							String remove = skills.substring(skills.indexOf("-") + 1, skills.length());
							String orCaseSkills[] = add.split(Pattern.quote("|"));
							queryStringBuffer.append("AND ((");
							for (int i = 0; i < orCaseSkills.length; i++) {
								String string = orCaseSkills[i];
								String skill[] = orCaseSkills[i].split(Pattern.quote("&"));
								// System.out.println(string);
								queryStringBuffer.append("(");
								for (int j = 0; j < skill.length; j++) {
									// String string1 = skill[j];
									skillsREsult = DataSourceDataProvider.getInstance().getEmpIDsBySkill(skill[j]);
									if (skillsREsult.length() > 0) {
										queryStringBuffer.append("tblEmployee.Id IN (" + skillsREsult + ")");
										if (j < skill.length - 1) {
											queryStringBuffer.append(" AND ");
										}
									}
									if (j == skill.length - 1) {
										queryStringBuffer.append(")");
									}

								}
								if (i < orCaseSkills.length - 1) {
									queryStringBuffer.append(" OR ");
								}
								if (i == orCaseSkills.length - 1) {
									queryStringBuffer.append(")");
								}
							}

							String removedSkills[] = remove.split(Pattern.quote("-"));

							if (removedSkills.length > 0 && !removedSkills[0].equals("")) {
								queryStringBuffer.append(" AND (");
								for (int i = 0; i < removedSkills.length; i++) {
									String string = removedSkills[i];
									// System.out.print(string);
									skillsREsult = DataSourceDataProvider.getInstance()
											.getEmpIDsBySkill(removedSkills[i]);
									if (skillsREsult.length() > 0) {
										queryStringBuffer.append("tblEmployee.Id NOT IN (" + skillsREsult + ")");
										if (i < removedSkills.length - 1) {
											queryStringBuffer.append(" AND ");
										}
									}
									if (i == removedSkills.length - 1) {
										queryStringBuffer.append(")");
									}
								}
							}
							queryStringBuffer.append(")");

							// queryStringBuffer.append(" " + skillsREsult);
						}
						if (getOpsContactId() != null && !"".equals(getOpsContactId()) && columnCounter == 0) {
							queryStringBuffer.append(" OpsContactId= " + getOpsContactId());
							columnCounter++;
						} else if (getOpsContactId() != null && !"1".equals(getOpsContactId()) && columnCounter != 0) {
							queryStringBuffer.append(" AND OpsContactId= " + getOpsContactId());
						}
						if (getCountry() != null && !"".equals(getCountry()) && columnCounter == 0) {
							queryStringBuffer.append(" Country='" + getCountry() + "' ");
							columnCounter++;
						} else if (getCountry() != null && !"".equals(getCountry()) && columnCounter != 0) {
							queryStringBuffer.append(" AND Country='" + getCountry() + "' ");
							// setTempDeptId(getCountry());
						}
						if (getPracticeId() != null && !"".equals(getPracticeId()) && columnCounter == 0) {
							queryStringBuffer.append(" PracticeId='" + getPracticeId() + "' ");
							columnCounter++;
						} else if (getPracticeId() != null && !"".equals(getPracticeId()) && columnCounter != 0) {
							queryStringBuffer.append(" AND PracticeId='" + getPracticeId() + "' ");
							// setPracticeId(getPracticeId());
						}
						if (getLocation() != null && !"".equals(getLocation()) && columnCounter == 0) {
							queryStringBuffer.append(" Location='" + getLocation() + "' ");
							columnCounter++;
						} else if (getLocation() != null && !"".equals(getLocation()) && columnCounter != 0) {
							queryStringBuffer.append(" AND Location='" + getLocation() + "' ");

						}
						if (!"".equals(getItgBatch()) && columnCounter == 0) {
							queryStringBuffer.append(" Itgbatch='" + getItgBatch() + "' ");
							columnCounter++;

						} else if (!"".equals(getItgBatch()) && columnCounter != 0) {
							queryStringBuffer.append(" AND Itgbatch='" + getItgBatch() + "' ");

						}

						queryStringBuffer.append("  ORDER BY trim(FName)");

					//	 System.out.println("queryStringBuffer------>"+queryStringBuffer.toString());
						httpServletRequest.getSession(false).setAttribute(ApplicationConstants.QS_EMP_LIST,
								queryStringBuffer.toString());

						queryStringBuffer.delete(0, queryStringBuffer.capacity());

					}
					// Calling searchPrepare() method to populate select
					// components

					// prepare();

					setFirstName(getTempName());
					setWorkPhoneNo(getTempPh());
					setCurrStatus(getTempCurStatus());
					setDepartmentId(getTempDeptId());
					setOrgId(getTempOrgId());
					setReportingpersonId(getReportingpersonId());
					setPayRollBatchUs(getPayRollBatchUs());
					setIsManager(getIsManager());
					setIsTeamLead(isIsTeamLead());
					setOutSideAccess(getOutSideAccess());
					setEmpno(getEmpno());
					setCountry(getCountry());
					setPracticeId(getPracticeId());
					setLocation(getLocation());
					setItgBatch(getItgBatch());
					setEmail1(getEmail1());
					setEmpTypeId(getEmpTypeId());
					searchPrepare();
					 //System.out.println(getPayRollBatchUs());
					/*
					 * httpServletRequest.getSession(false).setAttribute(
					 * "currStatus",getTempCurStatus());
					 * //httpServletRequest.getSession(false).setAttribute(
					 * "FName",getTempName());
					 * httpServletRequest.getSession(false).setAttribute(
					 * "DeptId",getTempDeptId());
					 * httpServletRequest.getSession(false).setAttribute("orgId"
					 * ,getTempOrgId());
					 */
					resultType = SUCCESS;
				} catch (Exception ex) {
					// List errorMsgList =
					// ExceptionToListUtility.errorMessages(ex);
					httpServletRequest.getSession(false).setAttribute("errorMessage", ex.toString());
					resultType = ERROR;
				}
			} // END-Authorization Checking
		} // Close Session Checking
			// System.err.println("resultType"+resultType);
		return resultType;
	}

	public String getTeamSearchQuery() {

		// System.out.println("I am in getSearchQuery() +getOrgId());
		resultType = LOGIN;

		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {

			userRoleId = Integer.parseInt(
					httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_ROLE_ID).toString());
			String workingCountry = httpServletRequest.getSession(false)
					.getAttribute(ApplicationConstants.WORKING_COUNTRY).toString();
			resultType = "accessFailed";
			String userRoleName = httpServletRequest.getSession(false)
					.getAttribute(ApplicationConstants.SESSION_ROLE_NAME).toString();
			if (AuthorizationManager.getInstance().isAuthorizedUser("GET_SEARCH_QUERY_EMP", userRoleId)) {
				try {
					// setPracticeIdList(dataSourceDataProvider.getPracticeByDepartment(getDepartmentId()));
					// setPracticeId(getCurrentEmployee().getPracticeId());
					// setSubPractice(getCurrentEmployee().getSubPractice());

					if ("searchFormAll".equalsIgnoreCase(getSubmitFrom())
							|| "searchFormMyTeam".equalsIgnoreCase(getSubmitFrom())) {
						queryStringBuffer = new StringBuilder();

						queryStringBuffer.append(
								"SELECT Id, DepartmentId, LName, FName, MName, Email1, WorkPhoneNo, AlterPhoneNo, CellPhoneNo, CurStatus,SubPractice,TeamId");
						queryStringBuffer.append(" FROM tblEmployee ");

						// queryStringBuffer.append(" tblLKSubPractice");

						if (null == getFirstName()) {
							setFirstName("");
						}
						if (null == getLastName()) {
							setLastName("");
						}
						if (null == getWorkPhoneNo()) {
							setWorkPhoneNo("");
						}
						if (null == getCurrStatus()) {
							setCurrStatus("");
						}
						if ("All".equalsIgnoreCase(getCurrStatus())) {
							setCurrStatus("");
						}
						if (null == getOrgId()) {
							setOrgId("");
						}
						if ("All".equalsIgnoreCase(getOrgId())) {
							setOrgId("");
						}
						if (null == getDepartmentId()) {
							setDepartmentId("");
						}
						if (null == getSubPractice()) {
							setSubPractice("");
						}
						if ("All".equalsIgnoreCase(getDepartmentId())) {
							setDepartmentId("");
						}
						if (null == getTeamId()) {
							setTeamId("");
						}
						if ("All".equalsIgnoreCase(getTeamId())) {
							setTeamId("");
						}

						// if((!"".equals(getFirstName()))
						// || (!"".equals(getLastName()))
						// || (!"".equals(getWorkPhoneNo()))
						// || (!"".equals(getCurrStatus()))
						// ||(!"".equals(getOrgId()))){
						// queryStringBuffer.append(" WHERE ");
						// }
						if ("Admin".equals(userRoleName)) {
							queryStringBuffer.append("where ");
						} else {
							queryStringBuffer.append(" WHERE  Country like '" + workingCountry + "' AND ");
						}
						int columnCounter = 0;

						if (!"".equals(getFirstName()) && columnCounter == 0) {
							setTempName(getFirstName());
							if ((getFirstName().indexOf("*") == -1) && (getFirstName().indexOf("%") == -1)) {
								setFirstName(getFirstName() + "*");
							}
							setFirstName(getFirstName().replace("*", "%"));
							queryStringBuffer.append(
									"(FName LIKE '" + getFirstName() + "%' OR LName LIKE '" + getFirstName() + "') ");
							columnCounter++;
						} else if (!"".equals(getFirstName()) && columnCounter != 0) {
							setTempName(getFirstName());
							if ((getFirstName().indexOf("*") == -1) && (getFirstName().indexOf("%") == -1)) {
								setFirstName(getFirstName() + "*");
							}
							setFirstName(getFirstName().replace("*", "%"));
							queryStringBuffer.append("AND (FName LIKE '" + getFirstName() + "' OR LName LIKE '"
									+ getFirstName() + "') ");
						}
						// if(!"".equals(getLastName()) && columnCounter==0){
						// queryStringBuffer.append("LName LIKE '" +
						// getLastName() + "%' ");
						// columnCounter ++;
						// }else if(!"".equals(getLastName()) &&
						// columnCounter!=0){
						// queryStringBuffer.append("AND LName LIKE '" +
						// getLastName() + "%' ");
						// }

						if (!"".equals(getWorkPhoneNo()) && columnCounter == 0) {
							setTempPh(getWorkPhoneNo());
							if ((getWorkPhoneNo().indexOf("*") == -1) && (getWorkPhoneNo().indexOf("%") == -1)) {
								setWorkPhoneNo(getWorkPhoneNo().replace("*", "%"));
							}
							setWorkPhoneNo(getWorkPhoneNo().replace("*", "%"));
							queryStringBuffer.append("WorkPhoneNo LIKE '" + getWorkPhoneNo() + "' ");
							columnCounter++;
						} else if (!"".equals(getWorkPhoneNo()) && columnCounter != 0) {
							setTempPh(getWorkPhoneNo());
							if ((getWorkPhoneNo().indexOf("*") == -1) && (getWorkPhoneNo().indexOf("%") == -1)) {
								setWorkPhoneNo(getWorkPhoneNo().replace("*", "%"));
							}
							queryStringBuffer.append("AND WorkPhoneNo LIKE '" + getWorkPhoneNo() + "' ");
						}

						if (!"".equals(getCurrStatus()) && columnCounter == 0) {
							queryStringBuffer.append("CurStatus ='" + getCurrStatus() + "' ");
							columnCounter++;
							setTempCurStatus(getCurrStatus());
						} else if (!"".equals(getCurrStatus()) && columnCounter != 0) {
							queryStringBuffer.append("AND CurStatus ='" + getCurrStatus() + "' ");
							setTempCurStatus(getCurrStatus());
						} else {
							setTempCurStatus("All");
						}
						if (!"".equals(getOrgId()) && columnCounter == 0) {
							queryStringBuffer.append("OrgId='" + getOrgId() + "' ");
							columnCounter++;
							setTempOrgId(getOrgId());
						} else if (!"".equals(getOrgId()) && columnCounter != 0) {
							queryStringBuffer.append("AND OrgId='" + getOrgId() + "' ");
							setTempOrgId(getOrgId());
						}

						if (!"".equals(getDepartmentId()) && columnCounter == 0) {
							queryStringBuffer.append("DepartmentId='" + getDepartmentId() + "' ");
							columnCounter++;
						} else if (!"".equals(getDepartmentId()) && columnCounter != 0) {
							queryStringBuffer.append("AND DepartmentId='" + getDepartmentId() + "' ");
							setTempDeptId(getDepartmentId());
						}

						if (!"".equals(getSubPractice()) && columnCounter == 0) {
							queryStringBuffer.append("SubPractice='" + getSubPractice() + "' ");
							columnCounter++;
						} else if (!"".equals(getSubPractice()) && columnCounter != 0) {
							queryStringBuffer.append("AND SubPractice='" + getSubPractice() + "' ");
						}
						if (!"".equals(getTeamId()) && columnCounter == 0) {
							queryStringBuffer.append("TeamId='" + getTeamId() + "' ");
							columnCounter++;
						} else if (!"".equals(getTeamId()) && columnCounter != 0) {
							queryStringBuffer.append("AND TeamId='" + getTeamId() + "' ");
						}

						if ("searchFormMyTeam".equalsIgnoreCase(getSubmitFrom()) && columnCounter == 0) {
							queryStringBuffer.append(" ReportsTo='" + httpServletRequest.getSession(false)
									.getAttribute(ApplicationConstants.SESSION_USER_ID) + "' ");
							columnCounter++;
							;
						} else if ("searchFormMyTeam".equalsIgnoreCase(getSubmitFrom()) && columnCounter != 0) {
							queryStringBuffer.append(" AND ReportsTo='" + httpServletRequest.getSession(false)
									.getAttribute(ApplicationConstants.SESSION_USER_ID) + "' ");
							columnCounter++;
						}

						if (columnCounter == 0) {
							queryStringBuffer.append(" DeletedFlag != 1 ORDER BY trim(FName)");
						} else if (columnCounter != 0) {
							queryStringBuffer.append(" AND DeletedFlag != 1 ORDER BY trim(FName)");
						}

						httpServletRequest.getSession(false).setAttribute(ApplicationConstants.QS_EMP_LIST,
								queryStringBuffer.toString());

						queryStringBuffer.delete(0, queryStringBuffer.capacity());
					}

					if ("dbGrid".equalsIgnoreCase(getSubmitFrom())) {
						queryStringBuffer = new StringBuilder();

						queryStringBuffer.append(
								"SELECT Id, DepartmentId, LName, FName, MName, Email1, WorkPhoneNo, AlterPhoneNo, CellPhoneNo, CurStatus,SubPractice,TeamId");
						queryStringBuffer.append(" FROM tblEmployee  ");
						// queryStringBuffer.append(" tblLKSubPractice");

						if (null == getFirstName()) {
							setFirstName("");
						}
						if (null == getLastName()) {
							setLastName("");
						}
						if (null == getWorkPhoneNo()) {
							setWorkPhoneNo("");
						}
						if (null == getCurrStatus()) {
							setCurrStatus("");
						}
						if ("All".equalsIgnoreCase(getCurrStatus())) {
							setCurrStatus("");
						}
						if (null == getOrgId()) {
							setOrgId("");
						}
						if ("All".equalsIgnoreCase(getOrgId())) {
							setOrgId("");
						}
						if (null == getDepartmentId()) {
							setDepartmentId("");
						}
						if (null == getSubPractice()) {
							setSubPractice("");
						}
						if ("All".equalsIgnoreCase(getDepartmentId())) {
							setDepartmentId("");
						}
						if (null == getTeamId()) {
							setTeamId("");
						}
						if ("All".equalsIgnoreCase(getTeamId())) {
							setTeamId("");
						}

						// if((!"".equals(getFirstName()))
						// || (!"".equals(getLastName()))
						// || (!"".equals(getWorkPhoneNo()))
						// || (!"".equals(getCurrStatus()))
						// ||(!"".equals(getOrgId()))){
						// queryStringBuffer.append(" WHERE ");
						// }

						// queryStringBuffer.append(" WHERE ");

						if ("Admin".equals(userRoleName)) {
							queryStringBuffer.append("where ReportsTo='" + httpServletRequest.getSession(false)
									.getAttribute(ApplicationConstants.SESSION_USER_ID) + "' AND ");
						} else {
							queryStringBuffer.append(" WHERE ReportsTo='"
									+ httpServletRequest.getSession(false)
											.getAttribute(ApplicationConstants.SESSION_USER_ID)
									+ "' AND Country like '" + workingCountry + "' AND ");
						}
						int columnCounter = 0;

						if (!"".equals(getFirstName()) && columnCounter == 0) {
							setTempName(getFirstName());
							if ((getFirstName().indexOf("*") == -1) && (getFirstName().indexOf("%") == -1)) {
								setFirstName(getFirstName() + "*");
							}
							setFirstName(getFirstName().replace("*", "%"));
							queryStringBuffer.append(
									"(FName LIKE '" + getFirstName() + "%' OR LName LIKE '" + getFirstName() + "') ");
							columnCounter++;
						} else if (!"".equals(getFirstName()) && columnCounter != 0) {
							setTempName(getFirstName());
							if ((getFirstName().indexOf("*") == -1) && (getFirstName().indexOf("%") == -1)) {
								setFirstName(getFirstName() + "*");
							}
							setFirstName(getFirstName().replace("*", "%"));
							queryStringBuffer.append("AND (FName LIKE '" + getFirstName() + "' OR LName LIKE '"
									+ getFirstName() + "') ");
						}
						// if(!"".equals(getLastName()) && columnCounter==0){
						// queryStringBuffer.append("LName LIKE '" +
						// getLastName() + "%' ");
						// columnCounter ++;
						// }else if(!"".equals(getLastName()) &&
						// columnCounter!=0){
						// queryStringBuffer.append("AND LName LIKE '" +
						// getLastName() + "%' ");
						// }

						if (!"".equals(getWorkPhoneNo()) && columnCounter == 0) {
							setTempPh(getWorkPhoneNo());

							if ((getWorkPhoneNo().indexOf("*") == -1) && (getWorkPhoneNo().indexOf("%") == -1)) {
								setWorkPhoneNo(getWorkPhoneNo().replace("*", "%"));
							}
							setWorkPhoneNo(getWorkPhoneNo().replace("*", "%"));
							queryStringBuffer.append("WorkPhoneNo LIKE '" + getWorkPhoneNo() + "' ");

							columnCounter++;
						} else if (!"".equals(getWorkPhoneNo()) && columnCounter != 0) {
							setTempPh(getWorkPhoneNo());
							if ((getWorkPhoneNo().indexOf("*") == -1) && (getWorkPhoneNo().indexOf("%") == -1)) {
								setWorkPhoneNo(getWorkPhoneNo().replace("*", "%"));
							}
							queryStringBuffer.append("AND WorkPhoneNo LIKE '" + getWorkPhoneNo() + "' ");
						}

						if (!"".equals(getCurrStatus()) && columnCounter == 0) {
							queryStringBuffer.append("CurStatus ='" + getCurrStatus() + "' ");
							columnCounter++;
							setTempCurStatus(getCurrStatus());
						} else if (!"".equals(getCurrStatus()) && columnCounter != 0) {
							queryStringBuffer.append("AND CurStatus ='" + getCurrStatus() + "' ");
							setTempCurStatus(getCurrStatus());
						} else {
							setTempCurStatus("All");
						}
						if (!"".equals(getOrgId()) && columnCounter == 0) {
							queryStringBuffer.append("OrgId='" + getOrgId() + "' ");
							columnCounter++;
							setTempOrgId(getOrgId());
						} else if (!"".equals(getOrgId()) && columnCounter != 0) {
							queryStringBuffer.append("AND OrgId='" + getOrgId() + "' ");
							setTempOrgId(getOrgId());
						}

						if (!"".equals(getDepartmentId()) && columnCounter == 0) {
							queryStringBuffer.append("DepartmentId='" + getDepartmentId() + "' ");
							columnCounter++;
						} else if (!"".equals(getDepartmentId()) && columnCounter != 0) {
							queryStringBuffer.append("AND DepartmentId='" + getDepartmentId() + "' ");
							setTempDeptId(getDepartmentId());
						}

						if (!"".equals(getSubPractice()) && columnCounter == 0) {
							queryStringBuffer.append("SubPractice='" + getSubPractice() + "' ");
							columnCounter++;
						} else if (!"".equals(getSubPractice()) && columnCounter != 0) {
							queryStringBuffer.append("AND SubPractice='" + getSubPractice() + "' ");
						}
						if (!"".equals(getTeamId()) && columnCounter == 0) {
							queryStringBuffer.append("TeamId='" + getTeamId() + "' ");
							columnCounter++;
						} else if (!"".equals(getTeamId()) && columnCounter != 0) {
							queryStringBuffer.append("AND TeamId='" + getTeamId() + "' ");
						}
						if (columnCounter == 0) {
							queryStringBuffer.append(" DeletedFlag != 1 ORDER BY trim(FName)");
						} else if (columnCounter != 0) {
							queryStringBuffer.append(" AND DeletedFlag != 1 ORDER BY trim(FName)");
						}

						// System.out.println("queryStringBuffer------>"+queryStringBuffer);
						httpServletRequest.getSession(false).setAttribute(ApplicationConstants.QS_EMP_LIST,
								queryStringBuffer.toString());

						queryStringBuffer.delete(0, queryStringBuffer.capacity());

					}
					// Calling searchPrepare() method to populate select
					// components
					searchPrepare();
					prepare();
					setFirstName(getTempName());
					setWorkPhoneNo(getTempPh());
					setCurrStatus(getTempCurStatus());
					setDepartmentId(getTempDeptId());
					setOrgId(getTempOrgId());
					/*
					 * httpServletRequest.getSession(false).setAttribute(
					 * "currStatus",getTempCurStatus());
					 * //httpServletRequest.getSession(false).setAttribute(
					 * "FName",getTempName());
					 * httpServletRequest.getSession(false).setAttribute(
					 * "DeptId",getTempDeptId());
					 * httpServletRequest.getSession(false).setAttribute("orgId"
					 * ,getTempOrgId());
					 */
					resultType = SUCCESS;
				} catch (Exception ex) {
					// List errorMsgList =
					// ExceptionToListUtility.errorMessages(ex);
					httpServletRequest.getSession(false).setAttribute("errorMessage", ex.toString());
					resultType = ERROR;
				}
			} // END-Authorization Checking
		} // Close Session Checking
		return resultType;
	}

	/**
	 * The getEmployee() is used for retrieving the employee details .
	 *
	 * @throws Exception
	 * @return String variable for navigation.
	 */
	public String getEmployee() {
		resultType = LOGIN;

		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			userRoleId = Integer.parseInt(
					httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_ROLE_ID).toString());
			resultType = "accessFailed";
			if (AuthorizationManager.getInstance().isAuthorizedUser("GET_EMPLOYEE", userRoleId)) {
				try {
					setCurrentEmployee(ServiceLocator.getEmployeeService().getEmployee(getEmpId(), getCurrId()));
					setId(getCurrentEmployee().getId());
					setLoginId(getCurrentEmployee().getLoginId());
					// Calling prepare() method to populate select components
					setDepartmentId(getCurrentEmployee().getDepartmentId());
					setPracticeId(getCurrentEmployee().getPracticeId());
					setSubPractice(getCurrentEmployee().getSubPractice());
					prepare();
					resultType = SUCCESS;
				} catch (Exception ex) {
					// List errorMsgList =
					// ExceptionToListUtility.errorMessages(ex);
					ex.printStackTrace();
					httpServletRequest.getSession(false).setAttribute("errorMessage:", ex.toString());

					resultType = ERROR;
				}
			} // END-Authorization Checking
		} // Close Session Checking
		return resultType;
	}

	/**
	 * The deleteEmployee() is used for deleting purticular the employee details
	 * .
	 *
	 * @throws Exception
	 * @return String variable for navigation.
	 */
	public String deleteEmployee() {
		resultType = LOGIN;

		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			userRoleId = Integer.parseInt(
					httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_ROLE_ID).toString());
			resultType = "accessFailed";
			if (AuthorizationManager.getInstance().isAuthorizedUser("DELETE_EMPLOYEE", userRoleId)) {
				try {
					int deletedRows = ServiceLocator.getEmployeeService().deleteEmployee(this.getEmpId());

					if (deletedRows == 1) {
						setDeleteAction(httpServletRequest.getSession(false)
								.getAttribute(ApplicationConstants.SESSION_DELETE_ACTION).toString());
						resultType = SUCCESS;
						resultMessage = "<font color=\"green\" size=\"1.5\">Employee has been successfully Deleted!</font>";
					} else {
						resultType = INPUT;
						resultMessage = "<font color=\"red\" size=\"1.5\">Sorry! Please Try again!</font>";
					}
					httpServletRequest.setAttribute(ApplicationConstants.RESULT_MSG, resultMessage);
				} catch (Exception ex) {
					// List errorMsgList =
					// ExceptionToListUtility.errorMessages(ex);
					httpServletRequest.getSession(false).setAttribute("errorMessage", ex.toString());
					resultType = ERROR;
				}
			} // END-Authorization Checking
		} // Close Session Checking
		return resultType;
	}

	/**
	 * The doEdit() is used for editing purticular the employee details .
	 *
	 * @throws Exception
	 * @return String variable for navigation.
	 */
	public String doEdit() {
		resultType = LOGIN;
		String password = "";

		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {

			userRoleId = Integer.parseInt(
					httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_ROLE_ID).toString());
			resultType = "accessFailed";
			if (AuthorizationManager.getInstance().isAuthorizedUser("DO_EDIT_EMPLOYEE", userRoleId)) {
				try {
					employeeService = ServiceLocator.getEmployeeService();

					String userId = httpServletRequest.getSession(false)
							.getAttribute(ApplicationConstants.SESSION_USER_ID).toString();
					setModifiedBy(userId);

					setModifiedDate(DateUtility.getInstance().getCurrentMySqlDateTime());
					// System.out.println("Working
					// Onsite------>"+getIsOnsite().toString());
					if ("Registered".equalsIgnoreCase(getPreCurrStatus())) {
						setCreatedBy(userId);
						setCreatedDate(DateUtility.getInstance().getCurrentMySqlDateTime());
					}
					if (!"".equals(getPrvexpYears().trim())) {
						setPrvexpYears(getPrvexpYears() + "yr");
					} else {
						setPrvexpYears("0yr");

					}
					if (!"".equals(getPrvexpMnths().trim())) {
						setPrvexpMnths(getPrvexpMnths() + "m");
					} else {
						setPrvexpMnths("0m");

					}
					// System.out.println("Location---->"+getLocation());
					boolean isUpdateUser = employeeService.updateEmployee(this);

					if (isUpdateUser) {
						boolean isSendMail = false;
						if ("Registered".equalsIgnoreCase(getPreCurrStatus())
								&& !("Registered".equalsIgnoreCase(getCurrStatus()))) {

							JSONObject jObject = new JSONObject();

							jObject.put("userId", this.getLoginId());
							jObject.put("firstName", this.getFirstName());
							jObject.put("middleName", this.getMiddleName());
							jObject.put("emailId", this.getOfficeEmail());
							jObject.put("mobile", this.getCellPhoneNo());
							jObject.put("lastName", this.getLastName());
							// jObject.put("plainPassword", this.getLoginId());
							jObject.put("Country", this.getCountry());
							jObject.put("DepartmentId", this.getDepartmentId());
							jObject.put("PracticeId", this.getPracticeId());
							jObject.put("Location", this.getLocation());
							password = PasswordUtility
									.decryptPwd(DataSourceDataProvider.getPassword(this.getLoginId()));
							jObject.put("plainPassword", password);
							System.out.println(jObject);
							LdapServiceProvider.addUserEntry(jObject);

							isSendMail = employeeService.sendMail(this.getId());
							int isertedRows = employeeService.insertDefaultRoles(this.getId(), this.getDepartmentId());
						}
						setEmpId(getId());
						resultMessage = "<font color=\"green\" size=\"1.5\">Employee has been successfully updated!</font>";
						resultType = SUCCESS;
					} else {
						resultType = INPUT;
						resultMessage = "<font color=\"red\" size=\"1.5\">Sorry! Please Try again!</font>";
					}

					httpServletRequest.setAttribute("resultMessage", resultMessage);
					setCurrentEmployee(employeeService.getEmployeeVTO(this));
					prepare();
				} catch (Exception ex) {
					// List errorMsgList =
					// ExceptionToListUtility.errorMessages(ex);
					httpServletRequest.getSession(false).setAttribute("errorMessage", ex.toString());
					resultType = ERROR;
				}
			} // END-Authorization Checking
		} // Close Session Checking
		return resultType;
	}

	/**
	 * Updated by vkandregula on 08132013
	 * 
	 * Updated By Teja Kadamanti on 09/23/2016
	 */
	public String doUpdateState() {
		resultType = LOGIN;
		int avaiable = 0;
		int stateCount = 0;
		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {

			userRoleId = Integer.parseInt(
					httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_ROLE_ID).toString());
			resultType = "accessFailed";

			if (AuthorizationManager.getInstance().isAuthorizedUser("DO_UPDATE_STATE_EMP", userRoleId)) {

				try {

					int result = DataSourceDataProvider.getInstance().getStatusDateCheck(getId(), getStateStartDate());

					String flagVar = "";
					java.sql.Date hireDate = null;
					hireDate = dataSourceDataProvider.getInstance()
							.getHireDateOfEmployeeBeforeAddingCurrentStatus(getId());
					if (hireDate == null
							|| ("1950-01-31".equals(DateUtility.getInstance().convertDateToStringPayroll(hireDate)))) {
						setNavId(0);
						resultType = SUCCESS;
						resultMessage = "<font size='2' color='red'> Employee HireDate is not updated. Please update it !</font>";
						httpServletRequest.setAttribute(ApplicationConstants.RESULT_MSG, resultMessage);
						flagVar = "add";

					}
					if (hireDate != null && "".equals(flagVar)) {
						if (getStateStartDate().compareTo(hireDate) < 0) {
							setNavId(0);
							resultType = SUCCESS;
							resultMessage = "<font size='2' color='red'>  Start Date should be greater than or equal to Employee HireDate'"
									+ com.mss.mirage.util.DateUtility.getInstance()
											.convertToviewFormat(hireDate.toString())
									+ "' !</font>";
							httpServletRequest.setAttribute(ApplicationConstants.RESULT_MSG, resultMessage);
							flagVar = "add";
						}
					}

					if ("Available".equals(getEmpState())) {
						stateCount = DataSourceDataProvider.getInstance().getAvailableState(getId());
						// System.out.println("availableState"+availableState);
					}
					employeeService = ServiceLocator.getEmployeeService();
					boolean isHistoryInserted = false;
					int updatedRows = 0;
					setCurrentEmployeeState(employeeService.getStateVTO(this));
					if ("".equals(flagVar)) {
						if (getTempVar() == 2) {
							setCreatedDate(DateUtility.getInstance().getCurrentMySqlDateTime());
							if (stateCount > 0) {
								// resultMessage = "<font color=\"red\"
								// size=\"2\">Sorry! Available record is already
								// in Active state for this employee you can add
								// new record by completing the prevoius one.
								// </font>";
								resultMessage = "<font color=\"red\" size=\"2\">Sorry! Available record is already in Active state for this employee you can add only one Active Available record . </font>";
								httpServletRequest.setAttribute(ApplicationConstants.RESULT_MSG, resultMessage);
							} else {
								if (result > 0 && "Available".equals(getEmpState())) {
									resultMessage = "<font color=\"red\" size=\"2\">Start Date must be greater than previous status dates</font>";
									httpServletRequest.setAttribute(ApplicationConstants.RESULT_MSG, resultMessage);
								} else {
									setCreatedBy(httpServletRequest.getSession(false)
											.getAttribute(ApplicationConstants.SESSION_USER_ID).toString());
									if ("Available".equals(getEmpState())) {
										List projectList = DataSourceDataProvider.getInstance()
												.getProjectsListByContactId(getId());
										avaiable = projectList.size();
										String loginIdEmp = DataSourceDataProvider.getInstance()
												.getLoginIdByEmpId(getId());

										if (getUpload() != null) {
											if (getUploadFileName() != null && !getUploadFileName().equals("")) {

												String basePath = Properties.getProperty("EmpResume.Attachments");
												// File createPath = new
												// File(basePath);
												String theFilePath = FileUploadUtility.getInstance()
														.filePathGeneration(basePath);
												String theFileName = FileUploadUtility.getInstance()
														.fileNameGeneration(getUploadFileName());
												File theFile = new File(
														theFilePath + "//" + loginIdEmp + "//" + theFileName);
												setFilepath(theFile.toString());
												/*
												 * copies the file to the
												 * destination
												 */
												// System.err.println("Here..."+getUpload());
												FileUtils.copyFile(getUpload(), theFile);
												// System.err.println("Here...1");
												// boolean
												// isInsert=ServiceLocator.getConsultantService().attachResume1(this);

											}
										}

										if (avaiable == 0) {
											isHistoryInserted = employeeService.insertStateHistory(this);
										}
									} else {
										isHistoryInserted = employeeService.insertStateHistory(this);
									}

									resultMessage = "<font color=\"green\" size=\"2\">Employee State has been successfully updated!</font>";
									setProjectName("");
									setSkillSet("");
									setComments("");
									if (avaiable > 0) {
										resultMessage = "<font color=\"red\" size=\"2\">this employee already allocated to project please contact to PMO!</font>";
									}
									httpServletRequest.setAttribute(ApplicationConstants.RESULT_MSG, resultMessage);
								}
							}
							resultType = SUCCESS;
						} else if (getTempVar() == 1) {

							setModifiedBy(httpServletRequest.getSession(false)
									.getAttribute(ApplicationConstants.SESSION_USER_ID).toString());
							setModifiedDate(DateUtility.getInstance().getCurrentMySqlDateTime());
							getCurrentEmployeeState().setModifiedBy(httpServletRequest.getSession(false)
									.getAttribute(ApplicationConstants.SESSION_USER_ID).toString());
							getCurrentEmployeeState()
									.setModifiedDate(DateUtility.getInstance().getCurrentMySqlDateTime());
							if (getCurrId() == 0) {
								boolean isUpdateState = employeeService.updateEmployeeState(getCurrentEmployeeState());

								if (isUpdateState) {
									resultMessage = "<font color=\"green\" size=\"2\">Employee State has been successfully updated!</font>";
									setNavId(0);
									httpServletRequest.setAttribute(ApplicationConstants.RESULT_MSG, resultMessage);
									resultType = SUCCESS;

									setCreatedDate(DateUtility.getInstance().getCurrentMySqlDateTime());
									// int recordId =
									// employeeService.getRecentStateHistoryId(getLoginId());
									// if(recordId == 0) {
									isHistoryInserted = employeeService.insertStateHistory(this);
									// }else {
									// updatedRows =
									// employeeService.updateStateHistory(this,recordId);
									// }
								} else {
									setNavId(0);
									resultType = INPUT;
									resultMessage = "<font color=\"red\" size=\"2\">Sorry! Please Try again!</font>";
									httpServletRequest.setAttribute(ApplicationConstants.RESULT_MSG, resultMessage);
								}
							} else {
								setCreatedDate(DateUtility.getInstance().getCurrentMySqlDateTime());

								if ("Available".equals(getEmpState())) {
									String loginIdEmp = DataSourceDataProvider.getInstance().getLoginIdByEmpId(getId());
									if (getUpload() != null) {
										if (getUploadFileName() != null && !getUploadFileName().equals("")) {

											String basePath = Properties.getProperty("EmpResume.Attachments");
											// File createPath = new
											// File(basePath);
											String theFilePath = FileUploadUtility.getInstance()
													.filePathGeneration(basePath);
											String theFileName = FileUploadUtility.getInstance()
													.fileNameGeneration(getUploadFileName());
											File theFile = new File(
													theFilePath + "//" + loginIdEmp + "//" + theFileName);
											setFilepath(theFile.toString());
											/*
											 * copies the file to the
											 * destination
											 */
											// System.err.println("Here..."+getUpload());
											FileUtils.copyFile(getUpload(), theFile);
											// System.err.println("Here...1");
											// boolean
											// isInsert=ServiceLocator.getConsultantService().attachResume1(this);

										}
									}

								}

								updatedRows = employeeService.updateStateHistory(this, getCurrId());
								if (updatedRows == 1) {
									setNavId(0);
									resultMessage = "<font color=\"green\" size=\"2\">Employee State has been successfully updated!</font>";
									httpServletRequest.setAttribute(ApplicationConstants.RESULT_MSG, resultMessage);
									resultType = SUCCESS;
								} else {
									setNavId(0);
									resultType = INPUT;
									resultMessage = "<font color=\"red\" size=\"2\">Sorry! Please Try again!</font>";
									httpServletRequest.setAttribute(ApplicationConstants.RESULT_MSG, resultMessage);
								}
							}
						}
					}
					setCurrId(0);
					setCurrentEmployee(employeeService.getEmployee(getCurrentEmployeeState().getEmpId(), getCurrId()));
					setDepartmentId(getCurrentEmployee().getDepartmentId());
					setEmpId(getId());

					setResultMessage(resultMessage);
					prepare();

				} catch (Exception ex) {
					// List errorMsgList =
					// ExceptionToListUtility.errorMessages(ex);
					ex.printStackTrace();
					httpServletRequest.getSession(false).setAttribute("errorMessage", ex.toString());
					resultType = ERROR;
				}
			} // END-Authorization Checking
		} // Close Session Checking
		return resultType;
	}

	public String deleteEmpStatus() {
		resultType = LOGIN;

		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {

			userRoleId = Integer.parseInt(
					httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_ROLE_ID).toString());
			resultType = "accessFailed";
			if (AuthorizationManager.getInstance().isAuthorizedUser("DO_UPDATE_STATE_EMP", userRoleId)) {
				try {
					employeeService = ServiceLocator.getEmployeeService();
					setCurrentEmployeeState(employeeService.getStateVTO(this));
					int isDelete = employeeService.deleteEmpStatus(getEmpId(), getCurrId());
					if (isDelete == 1) {

						resultMessage = "<font color=\"green\" size=\"1.5\">Employee State has been successfully deleted!</font>";
						httpServletRequest.setAttribute(ApplicationConstants.RESULT_MSG, resultMessage);
						resultType = SUCCESS;
					} else {
						resultType = INPUT;
						resultMessage = "<font color=\"red\" size=\"1.5\">Sorry! Please Try again!</font>";
						httpServletRequest.setAttribute(ApplicationConstants.RESULT_MSG, resultMessage);
					}
					/*
					 * setCurrId(0);
					 * setCurrentEmployee(employeeService.getEmployee(getEmpId()
					 * ,getCurrId()));
					 * setDepartmentId(getCurrentEmployee().getDepartmentId());
					 * prepare();
					 */
				} catch (Exception ex) {
					// List errorMsgList =
					// ExceptionToListUtility.errorMessages(ex);
					httpServletRequest.getSession(false).setAttribute("errorMessage", ex.toString());
					resultType = ERROR;
				}

			}
		}
		return resultType;
	}

	/**
	 * The execute() is used for executing business logic.
	 *
	 * @throws Exception
	 * @return Success string
	 */
	// modification
	/*
	 * public String execute()throws FileNotFoundException,ServletException {
	 * InputStream imageFile = new FileInputStream(getImagePath()); Connection
	 * connection = null; PreparedStatement statement = null; try {
	 * 
	 * connection = ConnectionProvider.getInstance().getConnection(); statement
	 * = connection.
	 * prepareStatement("update tblEmployee set Image=? where LoginId='vkandreula'"
	 * ); System.err.print("Updating iamge");
	 * statement.setBinaryStream(1,imageFile, (int) imagePath.length());
	 * //statement.setString(2, (String)
	 * httpServletRequest.getSession(false).getAttribute(ApplicationConstants.
	 * SESSION_USER_ID));
	 * 
	 * //statement.setString(2,getCurrentEmployee()); int update =
	 * statement.executeUpdate(); //getSkills(); getEmployee(); if(update==1){
	 * resultMessage = "Employee Image Updated Successfully!"; }else{
	 * resultMessage = "Please Try again!"; }
	 * httpServletRequest.setAttribute(ApplicationConstants.RESULT_MSG,
	 * resultMessage); resultType = SUCCESS; } catch (Exception ex) {
	 * httpServletRequest.getSession(false).setAttribute("errorMessage",ex);
	 * resultType=ERROR; }finally { try{ if(statement!=null){ statement.close();
	 * statement=null; } if(connection!=null){ connection.close();
	 * connection=null; } }catch(SQLException sqle){
	 * httpServletRequest.getSession(false).setAttribute("errorMessage",sqle); }
	 * } return resultType; //return SUCCESS; }
	 */
	// Original
	/**
	 * The execute() is used for executing business logic.
	 *
	 * @throws Exception
	 * @return Success string
	 */
	// new
	public String doImageUpdate() throws FileNotFoundException, ServletException {
		// String basePath = Properties.getProperty("EmpImages.path");
		// File createPath = new File(basePath);
		// System.out.println(getImagePath()+"........"+createPath);
		String ImageDetails = "";
		int update = 0;
		try {
			employeeService = ServiceLocator.getEmployeeService();

			JSONObject jb = new JSONObject();
			String loginId = getLoginId();

			if (getImagePath() != null && !"".equals(getImagePath())) {
				jb.put("ImageName", getImagePath().getName());
				jb.put("LoginId", loginId);

				String encodedBase64 = null;
				try {
					FileInputStream fileInputStreamReader = new FileInputStream(getImagePath());

					byte[] bytes = new byte[(int) (getImagePath()).length()];
					// System.out.println("fileInputStreamReader.."+fileInputStreamReader+"bytes.."+bytes);

					fileInputStreamReader.read(bytes);
					encodedBase64 = new String(Base64.encodeBytes(bytes));
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
				encodedBase64 = "data:application/pdf;base64," + encodedBase64;
				jb.put("Base64String", encodedBase64);

			}

			String serviceUrl = RestRepository.getInstance().getSrviceUrl("employeeProfilePicUpdate");
			URL url = new URL(serviceUrl);
			URLConnection connection = url.openConnection();
			connection.setDoOutput(true);
			connection.setRequestProperty("Content-Type", "application/json");
			connection.setConnectTimeout(360 * 5000);
			connection.setReadTimeout(360 * 5000);
			OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
			out.write(jb.toString());
			out.close();

			BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			String s = null;
			String data = "";
			while ((s = in.readLine()) != null) {

				data = data + s;
			}

			JSONObject jObject = new JSONObject(data);

			ImageDetails = jObject.getString("ImageDetails");
			// System.out.println("ImageDetails...."+ImageDetails);

			in.close();/*
						 * statement = connection
						 * .prepareStatement("update tblEmployee SET FileName = ? , FilePath = ? where LoginId=?"
						 * ); statement.setString(1, fileName);
						 * statement.setString(2, ImageDetails); String lid =
						 * getLoginId(); statement.setString(3, lid); int update
						 * = statement.executeUpdate();
						 */
			update = 1;

			// int blobLength = (int) test.length();
			// byte[] blobAsBytes = test.getBytes(1, blobLength);

			setEmpId(getId());
			setCurrId(0);
			setCurrentEmployee(employeeService.getEmployee(getEmpId(), getCurrId()));
			setDepartmentId(getCurrentEmployee().getDepartmentId());

			prepare();
			if (update == 1) {
				JSONObject mainJson = new JSONObject();
				mainJson.put("loginId", getLoginId());
				mainJson.put("ImageDetails", ImageDetails);

				LdapServiceProvider.empProfilePicUpdate(mainJson);
				resultMessage = "<font color=\"green\" size=\"1.5\">Employee Image Updated Successfully! </font>";
			} else {
				resultMessage = "<font color=\"green\" size=\"1.5\">Please Try again! </font>";
			}
			// httpServletRequest.setAttribute(ApplicationConstants.RESULT_MSG,resultMessage);
			httpServletRequest.setAttribute("resultMessage", resultMessage);

			resultType = SUCCESS;

		} catch (Exception ex) {
			httpServletRequest.getSession(false).setAttribute("errorMessage", ex);
			resultType = ERROR;
		}
		return resultType;
	}

	public String execute() {

		return SUCCESS;
	}
	/*
	 * public String getPMOActivity() { resultType = LOGIN;
	 * 
	 * if(httpServletRequest.getSession(false).getAttribute(ApplicationConstants
	 * .SESSION_USER_ID) != null){ userRoleId =
	 * Integer.parseInt(httpServletRequest.getSession(false).getAttribute(
	 * ApplicationConstants.SESSION_ROLE_ID).toString()); resultType =
	 * "accessFailed"; System.out.println("role"+userRoleId); String objectId =
	 * httpServletRequest.getSession(false).getAttribute(ApplicationConstants.
	 * SESSION_EMP_ID).toString();
	 * if(AuthorizationManager.getInstance().isAuthorizedUser("GET_PMO_ACTIVITY"
	 * ,userRoleId)){ try{ setMyProjects(new HashMap()); //
	 * setMyProjects(dataSourceDataProvider.getInstance().getMyProjectList(
	 * Integer.parseInt(objectId)));
	 * setMyAccounts(dataSourceDataProvider.getInstance().getMyCrmAccountList(
	 * Integer.parseInt(objectId))); String empId =
	 * httpServletRequest.getSession(false).getAttribute(ApplicationConstants.
	 * SESSION_EMP_ID).toString();
	 * 
	 * // String empId =
	 * httpServletRequest.getSession(false).getAttribute(ApplicationConstants.
	 * SESSION_EMP_ID).toString(); if(getSubmitFrom()==null){ queryString =
	 * "SELECT ProjectName,AccountId,StartDate,tblProjectContacts.STATUS,TotalResources,NAME,ProjectId "
	 * + "FROM tblProjectContacts LEFT OUTER JOIN tblProjects ON " +
	 * "(tblProjectContacts.ProjectId = tblProjects.Id) " +
	 * "LEFT OUTER JOIN tblCrmAccount " +
	 * "ON (tblCrmAccount.Id = tblProjectContacts.AccountId) " +
	 * "WHERE ObjectId = "+empId+" AND IsPMO =1 AND ResourceTitle!=8";
	 * 
	 * // queryString =
	 * queryString+" FROM tblEmployee WHERE ReportsTo='"+httpServletRequest.
	 * getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID)
	 * +"' AND DeletedFlag != 1 ORDER BY CurStatus,LName";
	 * //setSubmitFrom("searchFormMyTeam"); //
	 * httpServletRequest.getSession(false).setAttribute(ApplicationConstants.
	 * SESSION_PMO_ACTIVITY,"empSearchMyTeam");
	 * httpServletRequest.getSession(false).setAttribute(ApplicationConstants.
	 * SESSION_PMO_ACTIVITY,queryString); } // searchPrepare(); resultType =
	 * SUCCESS; }catch (Exception ex){ //List errorMsgList =
	 * ExceptionToListUtility.errorMessages(ex);
	 * httpServletRequest.getSession(false).setAttribute("errorMessage",ex.
	 * toString()); resultType = ERROR; } }//END-Authorization Checking }//Close
	 * Session Checking return resultType; }
	 * 
	 * 
	 * 
	 * public String pmoActivitySearch() { resultType = LOGIN;
	 * 
	 * if(httpServletRequest.getSession(false).getAttribute(ApplicationConstants
	 * .SESSION_USER_ID) != null){ userRoleId =
	 * Integer.parseInt(httpServletRequest.getSession(false).getAttribute(
	 * ApplicationConstants.SESSION_ROLE_ID).toString()); resultType =
	 * "accessFailed"; // System.out.println("role"+userRoleId);
	 * if(AuthorizationManager.getInstance().isAuthorizedUser("GET_PMO_ACTIVITY"
	 * ,userRoleId)){ try{ String empId =
	 * httpServletRequest.getSession(false).getAttribute(ApplicationConstants.
	 * SESSION_EMP_ID).toString(); if(getSubmitFrom()==null){ queryString =
	 * "SELECT ProjectName,StartDate,AccountId,tblProjectContacts.STATUS,TotalResources,NAME,ProjectId FROM tblProjectContacts LEFT OUTER JOIN tblProjects ON (tblProjectContacts.ProjectId = tblProjects.Id) LEFT OUTER JOIN tblCrmAccount ON (tblCrmAccount.Id = tblProjectContacts.AccountId)  "
	 * ;
	 * 
	 * // queryString =
	 * queryString+" FROM tblEmployee WHERE ReportsTo='"+httpServletRequest.
	 * getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID)
	 * +"' AND DeletedFlag != 1 ORDER BY CurStatus,LName";
	 * //setSubmitFrom("searchFormMyTeam"); //
	 * httpServletRequest.getSession(false).setAttribute(ApplicationConstants.
	 * SESSION_PMO_ACTIVITY,"empSearchMyTeam");
	 * 
	 * //if((getProjectName()!=null && !"".equals(getProjectName())) ||
	 * (getStatus()!=null && !"".equals(getStatus())) || (getStartDate()!=null
	 * && !"".equals(getStartDate())))
	 * 
	 * queryString = queryString +
	 * "  WHERE ObjectId = "+empId+" AND IsPMO =1 AND ResourceTitle!=8 ";
	 * 
	 * // int count = 0;
	 * 
	 * if(getProjectName()!=null && !"".equals(getProjectName())) { queryString
	 * = queryString + "AND ProjectName LIKE '"+getProjectName()+"%' "; //
	 * count++; } if(getStatus()!=null && !"".equals(getStatus())) { //
	 * if(count==0) // queryString = queryString +
	 * "Status LIKE '"+getStatus()+"%' "; // else queryString = queryString +
	 * "AND tblProjectContacts.Status LIKE '"+getStatus()+"%' "; // count++; }
	 * 
	 * if(getStartDate()!=null && !"".equals(getStartDate())) { // if(count==0)
	 * // queryString = queryString +
	 * "date(StartDate) = '"+DateUtility.getInstance().convertStringToMySQLDate(
	 * getStartDate())+"' "; // else queryString = queryString +
	 * "AND date(StartDate) = '"+DateUtility.getInstance().
	 * convertStringToMySQLDate(getStartDate())+"' "; }
	 * httpServletRequest.getSession(false).setAttribute(ApplicationConstants.
	 * SESSION_PMO_ACTIVITY,queryString); } // searchPrepare(); resultType =
	 * SUCCESS; }catch (Exception ex){ //List errorMsgList =
	 * ExceptionToListUtility.errorMessages(ex);
	 * httpServletRequest.getSession(false).setAttribute("errorMessage",ex.
	 * toString()); resultType = ERROR; } }//END-Authorization Checking }//Close
	 * Session Checking return resultType; }
	 */

	public String getPMOActivity() {
		
		resultType = LOGIN;

		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			String loginId = httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID)
					.toString();
			userRoleId = Integer.parseInt(
					httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_ROLE_ID).toString());
			resultType = "accessFailed";
			// System.out.println("role" + userRoleId);

			String userName = httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_NAME)
					.toString();
			String objectId = httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_EMP_ID)
					.toString();
			if (AuthorizationManager.getInstance().isAuthorizedUser("GET_PMO_ACTIVITY", userRoleId)) {
				try {
					setMyProjects(new HashMap());
					// setMyProjects(dataSourceDataProvider.getInstance().getMyProjectList(Integer.parseInt(objectId)));
					setMyAccounts(dataSourceDataProvider.getInstance().getMyCrmAccountList(Integer.parseInt(objectId)));
					String empId = httpServletRequest.getSession(false)
							.getAttribute(ApplicationConstants.SESSION_EMP_ID).toString();
					setPracticeList(DataSourceDataProvider.getInstance().getPracticeByDepartment("GDC"));
					setStatus("Active");
					setEmpStatus("Active");
					setClientMap(DataSourceDataProvider.getInstance().getCustomerMap());
					setProjectTypesList(DataSourceDataProvider.getInstance().getProjectTypesList());
				/*	if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_PMO_FORD_ACCESS)
							.toString().equals("1")) {
						setCustomerName("100463");// Ford Motor Company Id for
													// US hr Team(Specified
													// users)
					}
					if ((httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_IS_ADMIN_ACCESS)
							.toString().equals("1"))
							|| (httpServletRequest.getSession(false)
									.getAttribute(ApplicationConstants.SESSION_PMO_ACTIVITY_ACCESS).toString()
									.equals("1"))
							|| (httpServletRequest.getSession(false)
									.getAttribute(ApplicationConstants.SESSION_PMO_FORD_ACCESS).toString()
									.equals("1"))) {*/
						
						if ((httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_IS_ADMIN_ACCESS)
								.toString().equals("1"))
								|| (httpServletRequest.getSession(false)
										.getAttribute(ApplicationConstants.SESSION_PMO_ACTIVITY_ACCESS).toString()
										.equals("1"))
								) {
						
						
						setMyTeamMembers(DataSourceDataProvider.getInstance().getPmoMap());
					} else if (httpServletRequest.getSession(false)
							.getAttribute(ApplicationConstants.SESSION_IS_TEAM_LEAD).toString().equals("1")
							|| httpServletRequest.getSession(false)
									.getAttribute(ApplicationConstants.SESSION_IS_USER_MANAGER).toString()
									.equals("1")) {
						// setMyTeamMembers((Map)
						// httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_MY_TEAM_MAP));
						// getMyTeamMembers().put(loginId,
						// httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_NAME).toString());
						setMyTeamMembers(DataSourceDataProvider.getInstance().getTeamMapWithCurrentUser(loginId,
								userName, (Map) httpServletRequest.getSession(false)
										.getAttribute(ApplicationConstants.SESSION_MY_TEAM_MAP)));

					}
					// searchPrepare();
					resultType = SUCCESS;
				} catch (Exception ex) {
					// List errorMsgList =
					// ExceptionToListUtility.errorMessages(ex);
					httpServletRequest.getSession(false).setAttribute("errorMessage", ex.toString());
					resultType = ERROR;
				}
			} // END-Authorization Checking
		} // Close Session Checking
		return resultType;
	
	}

	public String pmoActivitySearch() {
		resultType = LOGIN;

		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			userRoleId = Integer.parseInt(
					httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_ROLE_ID).toString());
			resultType = "accessFailed";
			String loginId = httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID)
					.toString();
			// System.out.println("role"+userRoleId);
			if (AuthorizationManager.getInstance().isAuthorizedUser("GET_PMO_ACTIVITY", userRoleId)) {
				try {

					String empId = httpServletRequest.getSession(false)
							.getAttribute(ApplicationConstants.SESSION_EMP_ID).toString();
					if (getSubmitFrom() == null) {
						// change queryString = "SELECT
						// ProjectName,StartDate,AccountId,tblProjectContacts.STATUS,TotalResources,NAME,ProjectId
						// FROM tblProjectContacts LEFT OUTER JOIN tblProjects
						// ON (tblProjectContacts.ProjectId = tblProjects.Id)
						// LEFT OUTER JOIN tblCrmAccount ON (tblCrmAccount.Id =
						// tblProjectContacts.AccountId) ";

						// queryString = queryString+" FROM tblEmployee WHERE
						// ReportsTo='"+httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID)+"'
						// AND DeletedFlag != 1 ORDER BY CurStatus,LName";
						// setSubmitFrom("searchFormMyTeam");
						// httpServletRequest.getSession(false).setAttribute(ApplicationConstants.SESSION_PMO_ACTIVITY,"empSearchMyTeam");

						// if((getProjectName()!=null &&
						// !"".equals(getProjectName())) || (getStatus()!=null
						// && !"".equals(getStatus())) || (getStartDate()!=null
						// && !"".equals(getStartDate())))

						// chaneg queryString = queryString + " WHERE ObjectId =
						// "+empId+" AND IsPMO =1 AND ResourceTitle!=8 ";
						queryString = "SELECT ProjectName,AccountId,tblProjects.ProjectStartDate,tblPmoAuthors.Status,TotalResources,NAME,ProjectId "
								+ "FROM tblPmoAuthors LEFT OUTER JOIN tblProjects ON (tblPmoAuthors.ProjectId = tblProjects.Id) "
								+ "LEFT OUTER JOIN tblCrmAccount ON (tblCrmAccount.Id = tblPmoAuthors.AccountId) WHERE AuthorId = '"
								+ loginId + "'";

						// int count = 0;

						if (getProjectName() != null && !"".equals(getProjectName())) {
							queryString = queryString + "AND ProjectName LIKE '%" + getProjectName() + "%' ";
							// count++;
						}
						if (getStatus() != null && !"".equals(getStatus())) {
							// if(count==0)
							// queryString = queryString + "Status LIKE
							// '"+getStatus()+"%' ";
							// else
							queryString = queryString + "AND tblProjects.Status LIKE '" + getStatus() + "%' ";
							// count++;
						}

						if (getStartDate() != null && !"".equals(getStartDate())) {
							// if(count==0)
							// queryString = queryString + "date(StartDate) =
							// '"+DateUtility.getInstance().convertStringToMySQLDate(getStartDate())+"'
							// ";
							// else
							queryString = queryString + "AND date(ProjectStartDate) = '"
									+ DateUtility.getInstance().convertStringToMySQLDate(getStartDate()) + "' ";
						}
						httpServletRequest.getSession(false).setAttribute(ApplicationConstants.SESSION_PMO_ACTIVITY,
								queryString);
					}
					// searchPrepare();
					resultType = SUCCESS;
				} catch (Exception ex) {
					// List errorMsgList =
					// ExceptionToListUtility.errorMessages(ex);
					httpServletRequest.getSession(false).setAttribute("errorMessage", ex.toString());
					resultType = ERROR;
				}
			} // END-Authorization Checking
		} // Close Session Checking
		return resultType;
	}

	public Map getOpsContactIdMapNew() {
		return opsContactIdMapNew;
	}

	public void setOpsContactIdMapNew(Map opsContactIdMapNew) {
		this.opsContactIdMapNew = opsContactIdMapNew;
	}

	public String getFromLocationnew() {
		return fromLocationnew;
	}

	public void setFromLocationnew(String fromLocationnew) {
		this.fromLocationnew = fromLocationnew;
	}

	public String getToLocationnew() {
		return toLocationnew;
	}

	public void setToLocationnew(String toLocationnew) {
		this.toLocationnew = toLocationnew;
	}

	public String getReportedToNew() {
		return reportedToNew;
	}

	public void setReportedToNew(String reportedToNew) {
		this.reportedToNew = reportedToNew;
	}

	public String getTentativeReportedDateNew() {
		return tentativeReportedDateNew;
	}

	public void setTentativeReportedDateNew(String tentativeReportedDateNew) {
		this.tentativeReportedDateNew = tentativeReportedDateNew;
	}

	public String getTransferType() {
		return transferType;
	}

	public void setTransferType(String transferType) {
		this.transferType = transferType;
	}

	public String getEmail1() {
		return email1;
	}

	public void setEmail1(String email1) {
		this.email1 = email1;
	}

	public String getMonthName() {
		return monthName;
	}

	public void setMonthName(String monthName) {
		this.monthName = monthName;
	}

	public String viewProjectTeam() {
		resultType = LOGIN;

		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			userRoleId = Integer.parseInt(
					httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_ROLE_ID).toString());
			String workingCountry = httpServletRequest.getSession(false)
					.getAttribute(ApplicationConstants.WORKING_COUNTRY).toString();
			String userRoleName = httpServletRequest.getSession(false)
					.getAttribute(ApplicationConstants.SESSION_ROLE_NAME).toString();
			// System.out.println("workingCountry----->"+workingCountry);
			resultType = "accessFailed";
			if (AuthorizationManager.getInstance().isAuthorizedUser("GET_EMP_SEARCH_ALL", userRoleId)) {
				try {
					if (getSubmitFrom() == null) {

						queryString = "SELECT tblProjects.ProjectName,tblProjectContacts.Id AS Id,AccountId,ProjectId,ObjectId,ResourceName AS EmpName ,Email,ResourceTitle,ObjectType,CASE WHEN (Billable=1) THEN  'Yes' ELSE 'No' END AS Billable,tblProjectContacts.Status ,"
								+ "tblProjectContacts.StartDate,tblProjectContacts.Utilization FROM tblProjectContacts LEFT OUTER JOIN tblProjects ON (tblProjectContacts.ProjectId=tblProjects.Id)";
						queryString = queryString + " WHERE tblProjectContacts.ProjectId =" + getProjectId()
								+ " AND tblProjectContacts.STATUS IN('Active')";

						// System.err.println("Before");
						// setReportingPersons(dataSourceDataProvider.getEmployeeNamesByReportingPerson());
						// httpServletRequest.getSession(false).setAttribute(ApplicationConstants.SESSION_DELETE_ACTION,"empSearchAll");
						httpServletRequest.getSession(false).setAttribute(ApplicationConstants.QS_PROJECT_TEAM_LIST,
								queryString);
					}
					setProjectTeamReportsTo(DataSourceDataProvider.getInstance()
							.getProjectReportsTo(Integer.parseInt(getAccountId()), Integer.parseInt(getProjectId())));
					// dataSourceDataProvider =
					// DataSourceDataProvider.getInstance();
					// setSubmitFrom("searchFormAll");
					setCurrStatus("Active");
					setCustomerName(customerName);
					// searchPrepare();
					// prepare();
					resultType = SUCCESS;
				} catch (Exception ex) {
					// List errorMsgList =
					// ExceptionToListUtility.errorMessages(ex);
					httpServletRequest.getSession(false).setAttribute("errorMessage", ex.toString());
					resultType = ERROR;
				}
			} // END-Authorization Checking
		} // Close Session Checking
		return resultType;
	}

	public String getProjectTeamQuery() {

		// System.out.println("I am in getSearchQuery() +getOrgId());
		resultType = LOGIN;

		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {

			userRoleId = Integer.parseInt(
					httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_ROLE_ID).toString());
			String workingCountry = httpServletRequest.getSession(false)
					.getAttribute(ApplicationConstants.WORKING_COUNTRY).toString();
			resultType = "accessFailed";
			String userRoleName = httpServletRequest.getSession(false)
					.getAttribute(ApplicationConstants.SESSION_ROLE_NAME).toString();
			if (AuthorizationManager.getInstance().isAuthorizedUser("GET_SEARCH_QUERY_EMP", userRoleId)) {
				try {
					// setPracticeIdList(dataSourceDataProvider.getPracticeByDepartment(getDepartmentId()));
					// setPracticeId(getCurrentEmployee().getPracticeId());
					// setSubPractice(getCurrentEmployee().getSubPractice());

					// System.err.println(getSubmitFrom());
					setCustomerName(customerName);
					setProjectName(
							DataSourceDataProvider.getInstance().getProjectName(Integer.parseInt(getProjectId())));
					// if("searchFormAll".equalsIgnoreCase(getSubmitFrom()) ||
					// "searchFormMyTeam".equalsIgnoreCase(getSubmitFrom())){
					queryStringBuffer = new StringBuilder();
					setProjectTeamReportsTo(DataSourceDataProvider.getInstance()
							.getProjectReportsTo(Integer.parseInt(getAccountId()), Integer.parseInt(getProjectId())));
					// queryStringBuffer.append("SELECT
					// tblProjects.ProjectName,tblProjectContacts.Id AS
					// Id,AccountId,ProjectId,ObjectId,ResourceName AS EmpName
					// ,Email,ResourceTitle,ObjectType,CASE WHEN (Billable=1)
					// THEN 'Yes' ELSE 'No' END AS
					// Billable,tblProjectContacts.Status FROM
					// tblProjectContacts LEFT OUTER JOIN tblProjects ON
					// (tblProjectContacts.ProjectId=tblProjects.Id)");
					queryStringBuffer.append(
							"SELECT tblProjects.ProjectName,tblProjectContacts.Id AS Id,AccountId,ProjectId,ObjectId,ResourceName AS EmpName ,Email,ResourceTitle,ObjectType,CASE WHEN (Billable=1) THEN  CONCAT(EmpProjStatus,'(','Billable',')') ELSE CONCAT(EmpProjStatus,' ') END AS ResourceType,tblProjectContacts.Status,tblProjectContacts.StartDate,tblProjectContacts.Utilization,tblEmployee.Country AS ResourceCountry,tblProjectContacts.EndDate  FROM tblProjectContacts LEFT OUTER JOIN tblProjects ON (tblProjectContacts.ProjectId=tblProjects.Id) LEFT OUTER JOIN tblEmployee ON (tblEmployee.Id=tblProjectContacts.ObjectId) ");
					if (!"".equals(getProjectId()) && getProjectId() != null) {
						queryStringBuffer.append(" where tblProjectContacts.ProjectId=" + getProjectId() + " ");

					}
					if (!"".equals(getFirstName()) && getFirstName() != null) {
						queryStringBuffer
								.append(" and tblProjectContacts.ResourceName like '%" + getFirstName() + "%' ");

					}
					if (!"".equals(getEmpProjectStatus()) && getEmpProjectStatus() != null
							&& !("All".equals(getEmpProjectStatus()))) {
						queryStringBuffer.append(" and tblProjectContacts.Status = '" + getEmpProjectStatus() + "' ");

					}

					// if (!"".equals(getCurrStatus()) && getCurrStatus() !=
					// null && !("All".equals(getCurrStatus()))) {
					// queryStringBuffer.append(" and tblProjectContacts.Status
					// = '" + getCurrStatus() + "' ");
					//
					// }
					if (getReportsTo() != null && !"-1".equals(getReportsTo()) && !"".equals(getReportsTo())) {
						queryStringBuffer.append(" and tblProjectContacts.ReportsTo = '" + getReportsTo() + "' ");

					}
					// System.out.println("getIsBillable"+getIsBillable());
					if ("true".equals(getIsBillable())) {
						queryStringBuffer.append(" and tblProjectContacts.Billable = 1 ");

					}

					// setReportsTo(getReportsTo());
					// setIsBillable(getIsBillable());
					// System.out.println("queryStringBuffer--->" +
					// queryStringBuffer.toString());

					httpServletRequest.getSession(false).setAttribute(ApplicationConstants.QS_PROJECT_TEAM_LIST,
							queryStringBuffer.toString());

					queryStringBuffer.delete(0, queryStringBuffer.capacity());

					// }
					resultType = SUCCESS;
				} catch (Exception ex) {
					// List errorMsgList =
					// ExceptionToListUtility.errorMessages(ex);
					httpServletRequest.getSession(false).setAttribute("errorMessage", ex.toString());
					resultType = ERROR;
				}
			} // END-Authorization Checking
		} // Close Session Checking
			// System.err.println("resultType"+resultType);
		return resultType;
	}
	// new

	public String consultantTechReviews() {
		resultType = LOGIN;

		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			userRoleId = Integer.parseInt(
					httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_ROLE_ID).toString());
			String workingCountry = httpServletRequest.getSession(false)
					.getAttribute(ApplicationConstants.WORKING_COUNTRY).toString();
			String userRoleName = httpServletRequest.getSession(false)
					.getAttribute(ApplicationConstants.SESSION_ROLE_NAME).toString();
			// System.out.println("workingCountry----->"+workingCountry);
			resultType = "accessFailed";
			if (AuthorizationManager.getInstance().isAuthorizedUser("GET_EMP_SEARCH_ALL", userRoleId)) {
				try {
					setTechReviews("1");
					setStatus("Assigned");
					resultType = SUCCESS;
				} catch (Exception ex) {
					// List errorMsgList =
					// ExceptionToListUtility.errorMessages(ex);
					httpServletRequest.getSession(false).setAttribute("errorMessage", ex.toString());
					resultType = ERROR;
				}
			} // END-Authorization Checking
		} // Close Session Checking
		return resultType;
	}

	public String searchTechReviews() {
		resultType = LOGIN;

		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			userRoleId = Integer.parseInt(
					httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_ROLE_ID).toString());
			String workingCountry = httpServletRequest.getSession(false)
					.getAttribute(ApplicationConstants.WORKING_COUNTRY).toString();
			String userRoleName = httpServletRequest.getSession(false)
					.getAttribute(ApplicationConstants.SESSION_ROLE_NAME).toString();
			String empId = httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_EMP_ID)
					.toString();
			// System.out.println("workingCountry----->"+workingCountry);
			resultType = "accessFailed";
			if (AuthorizationManager.getInstance().isAuthorizedUser("GET_EMP_SEARCH_ALL", userRoleId)) {
				try {
					// System.out.println("status-->"+getStatus());
					setStatus(getStatus());
					String startDate;
					String endDate;
					queryStringBuffer = new StringBuilder();
					String email = DataSourceDataProvider.getInstance().getEmailIdForEmployee(Integer.parseInt(empId));

					queryStringBuffer
							.append("SELECT tblRecConsultant.Id as conId,tblRecConsultantActivity.Id AS id,CONCAT(FName,' ',MName,' ',LName) AS NAME,tblRecConsultantActivity.Rateing,"
									+ "tblRecConsultantActivity.ForwardedDate,tblRecConsultantActivity.LastModifiedDate,tblRecConsultantActivity.Status "
									+ "FROM tblRecConsultantActivity LEFT OUTER JOIN tblRecConsultant ON "
									+ "(tblRecConsultantActivity.ConsultantId=tblRecConsultant.Id) WHERE ForwardedTo='"
									+ email + "' ");
					if (getStartDate() != null && getEndDate() != null) {
						startDate = DateUtility.getInstance().convertStringToMySQLDate(getStartDate());
						endDate = DateUtility.getInstance().convertStringToMySQLDate(getEndDate());

						queryStringBuffer.append(
								"AND ForwardedDate >='" + startDate + "' AND ForwardedDate<='" + endDate + "' ");
					}
					if (getSkillSet() != null) {
						queryStringBuffer.append("AND SkillSet LIKE '%" + getSkillSet() + "%'");
					}
					if (getStatus().equalsIgnoreCase("Assigned") || getStatus().equalsIgnoreCase("A")) {
						queryStringBuffer.append("AND tblRecConsultantActivity.Status LIKE 'A'");
					} else if (getStatus().equalsIgnoreCase("Forward") || getStatus().equalsIgnoreCase("F")) {
						queryStringBuffer.append("AND tblRecConsultantActivity.Status LIKE 'F'");
					} else if (getStatus().equalsIgnoreCase("Reviewed") || getStatus().equalsIgnoreCase("R")) {
						queryStringBuffer.append("AND tblRecConsultantActivity.Status LIKE 'R'");
					} else if (getStatus().equalsIgnoreCase("Reviewed&Forwarded")
							|| getStatus().equalsIgnoreCase("RF")) {
						queryStringBuffer.append("AND tblRecConsultantActivity.Status LIKE 'RF'");
					}
					// System.out.println(queryStringBuffer.toString());
					httpServletRequest.setAttribute("resultMessage", getResultMessage());
					httpServletRequest.getSession(false).setAttribute(ApplicationConstants.EMP_TECH_REVIEWS,
							queryStringBuffer.toString());
					resultType = SUCCESS;
				} catch (Exception ex) {
					// List errorMsgList =
					// ExceptionToListUtility.errorMessages(ex);
					ex.printStackTrace();
					httpServletRequest.getSession(false).setAttribute("errorMessage", ex.toString());
					resultType = ERROR;
				}
			} // END-Authorization Checking
		} // Close Session Checking
		return resultType;
	}

	public String getCurrentEmployeeReport() {
		// System.out.println("getCurrentEmployeeReport method");
		resultType = INPUT;
		OutputStream outputStream = null;
		InputStream inputStream = null;
		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			userRoleId = Integer.parseInt(
					httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_ROLE_ID).toString());
			// resultType = "accessFailed";
			// System.out.println("if block");
			if (AuthorizationManager.getInstance().isAuthorizedUser("GET_EMPLOYEE", userRoleId)) {

				String responseString = "";
				try {
					String fileLocation = "";

					// fileLocation = generateEmployeeList(getLoginId());

					fileLocation = ServiceLocator.getEmployeeService().generateEmployeeList(getLoginId());
					getHttpServletResponse().setContentType("application/force-download");

					if (!"".equals(fileLocation)) {
						File file = new File(fileLocation);

						String fileName = "";

						fileName = file.getName();
						if (file.exists()) {
							inputStream = new FileInputStream(file);
							outputStream = getHttpServletResponse().getOutputStream();
							getHttpServletResponse().setHeader("Content-Disposition",
									"attachment;filename=\"" + fileName + "\"");
							int noOfBytesRead = 0;
							byte[] byteArray = null;
							while (true) {
								byteArray = new byte[1024];
								noOfBytesRead = inputStream.read(byteArray);
								if (noOfBytesRead == -1) {
									break;
								}
								outputStream.write(byteArray, 0, noOfBytesRead);
							}

						} else {
							throw new FileNotFoundException("File not found");
						}
					} else {

						resultMessage = "<font color=\"red\" size=\"1.5\">Sorry! No records for this Employee!</font>";
						httpServletRequest.setAttribute(ApplicationConstants.RESULT_MSG, resultMessage);

					}

				} catch (FileNotFoundException ex) {
					try {
						getHttpServletResponse()
								.sendRedirect("../general/exception.action?exceptionMessage='No File found'");
					} catch (IOException ex1) {
						Logger.getLogger(EmployeeAction.class.getName()).log(Level.SEVERE, null, ex1);
					}
				} catch (IOException ex) {
					ex.printStackTrace();
				} finally {
					try {
						// System.out.println("finally resultType "+resultType);
						if (inputStream != null) {
							inputStream.close();
						}
						if (outputStream != null) {
							outputStream.close();
						}

					} catch (IOException ex) {
						ex.printStackTrace();
					}
				}

			} // END-Authorization Checking
		} // Close Session Checking
		resultType = SUCCESS;

		setEmpId(getEmpId());
		return resultType;
	}

	public String getPMODashBoard() {

		resultType = LOGIN;
		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			try {

				String roleName = httpServletRequest.getSession(false)
						.getAttribute(ApplicationConstants.SESSION_ROLE_NAME).toString();
				String userName = httpServletRequest.getSession(false)
						.getAttribute(ApplicationConstants.SESSION_USER_NAME).toString();
				hibernateDataProvider = HibernateDataProvider.getInstance();
				defaultDataProvider = DefaultDataProvider.getInstance();
				dataSourceDataProvider = dataSourceDataProvider.getInstance();
				setPracticeIdList(dataSourceDataProvider.getPracticeByDepartment(getDepartmentId()));
				setCountryList(hibernateDataProvider.getContries(ApplicationConstants.COUNTRY_OPTIONS));
				String Country = (String) httpServletRequest.getSession(false)
						.getAttribute(ApplicationConstants.Living_COUNTRY);
				setStateList(hibernateDataProvider.getEmpCurrentState(ApplicationConstants.EMP_CURRENT_STATUS));
				loginId = httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID)
						.toString();
				setStatus("Active");
				setEmpStatus("Active");
				setSubPracticeList(dataSourceDataProvider.getSubPracticeByPractice(getPracticeId()));
				setEmpPracticeList(DataSourceDataProvider.getInstance().getEmpPracticeByDepartment());
				setClientMap(DataSourceDataProvider.getInstance().getCustomerMap());
				setProjectTypesList(DataSourceDataProvider.getInstance().getProjectTypesList());

				if ((httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_IS_ADMIN_ACCESS)
						.toString().equals("1"))
						|| (httpServletRequest.getSession(false)
								.getAttribute(ApplicationConstants.SESSION_PMO_ACTIVITY_ACCESS).toString()
								.equals("1"))) {
					setMyPMOTeamMembers(DataSourceDataProvider.getInstance().getPmoMap());
				} else if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_IS_TEAM_LEAD)
						.toString().equals("1")
						|| httpServletRequest.getSession(false)
								.getAttribute(ApplicationConstants.SESSION_IS_USER_MANAGER).toString().equals("1")) {
					// map = (Map)
					// httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_MY_TEAM_MAP);
					// map.put(loginId,
					// DataSourceDataProvider.getInstance().getemployeenamebyloginId(loginId));
					// setMyPMOTeamMembers(map);

					setMyPMOTeamMembers(DataSourceDataProvider.getInstance().getTeamMapWithCurrentUser(loginId,
							userName, (Map) httpServletRequest.getSession(false)
									.getAttribute(ApplicationConstants.SESSION_MY_TEAM_MAP)));
				}
				setPracticeList(DataSourceDataProvider.getInstance().getPracticeByDepartment("GDC"));

				// Code added by Nagalakshmi start

				GregorianCalendar cal = new GregorianCalendar();
				int year = cal.get(Calendar.YEAR);
				int month = cal.get(Calendar.MONTH);

				if (getBackToFlag() == 1) {
					setBackToFlag(getBackToFlag());
					setCustomerName(getCustomerName());
					setCustomerId(getCustomerId());
					// System.out.println("get getCustomerName is"
					// +getCustomerName());
					setPracticeId(getPracticeId());
					// System.out.println("get getPracticeId is"
					// +getPracticeId());
					setCostModel(getCostModel());
					// System.out.println("get costmodel is" +getCostModel());
					if (getYear() == 0) {
						setYear(year);
					}
					if (getMonth() == 0) {
						setMonth(month + 1);
					}
				} else {

					if (getYear() == 0) {
						setYear(year);
					}

					if (getMonth() == 0) {
						setMonth(month + 1);
					}
					setCostModel("-1");
					setCustomerName("");
					setCustomerId(getCustomerId());
					setPracticeId("");

				}

				// Code added by Nagalakshmi end

				resultType = SUCCESS;
			} catch (Exception ex) {
				// List errorMsgList = ExceptionToListUtility.errorMessages(ex);
				httpServletRequest.getSession(false).setAttribute("errorMessage", ex.toString());
				resultType = ERROR;
			}
		}
		return resultType;

	}

	public String getManagerDashBoard() {
		resultType = LOGIN;
		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			userRoleId = Integer.parseInt(
					httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_ROLE_ID).toString());
			String loginId = httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID)
					.toString();
			String objectId = httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_EMP_ID)
					.toString();
			// System.out.println("objectId-->"+objectId);
			resultType = "accessFailed";
			// if
			// (AuthorizationManager.getInstance().isAuthorizedUser("GET_PROJECT_DASH_BOARD",
			// userRoleId)) {
			try {
				dataSourceDataProvider = DataSourceDataProvider.getInstance();
				hibernateDataProvider = HibernateDataProvider.getInstance();
				// setMyProjects(new HashMap());
				// String departmentId =
				// DataSourceDataProvider.getInstance().getDepartmentName(loginId);
				// setManagerTeamMembersList(dataSourceDataProvider.getInstance().getMyTeamMembers(loginId,
				// departmentId));

				setManagerTeamMembersList((Map) httpServletRequest.getSession(false)
						.getAttribute(ApplicationConstants.SESSION_MY_TEAM_MAP));
				
				
				// Customer Roll in roll off start
				GregorianCalendar cal = new GregorianCalendar();
				int year = cal.get(Calendar.YEAR);
				int month = cal.get(Calendar.MONTH);

				if (getYear() == 0) {
					setYear(year);
				}
				if (getMonth() == 0) {
					setMonth(month + 1);
				}
				setStartDate(DateUtility.getInstance().YearBackFromCurrentDate());
				setEndDate(DateUtility.getInstance().getCurrentMySqlDate());
				//Customer Roll in rollof end
				//Offfshore Deliver Strength start
				
				setPracticeMap(DataSourceDataProvider.getInstance().getPracticeIdList());
				//Offfshore Deliver Strength end
				//Employee report by Current Status start
				//setClientMap(DataSourceDataProvider.getInstance().getCustomerMap());
				 setPracticeIdList(dataSourceDataProvider.getPracticeByDepartment(getDepartmentId()));
				 setCountryList(hibernateDataProvider.getContries(ApplicationConstants.COUNTRY_OPTIONS));
                 setLocationsMap(dataSourceDataProvider.getEmployeeLocationsList(getCountry()));
                 setSubPracticeList(dataSourceDataProvider.getSubPracticeByPractice(getPracticeId()));
                 setStateList(hibernateDataProvider.getEmpCurrentState(ApplicationConstants.EMP_CURRENT_STATUS));
                 Map rolesMap=(Map)httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_MY_ROLES);
                 if(rolesMap.containsValue("Admin"))
                     setIsAdminFlag("YES");
                             else
                     setIsAdminFlag("NO");
                 
                 
				//Employee report by Current Status end

                 
                 //Project Details by Customer start
                 setMyProjects(new HashMap());
                 //Project Details by Customer end
				resultType = SUCCESS;
			} catch (Exception ex) {
				httpServletRequest.getSession(false).setAttribute("errorMessage", ex.toString());
				resultType = ERROR;
			}

			// }//END-Authorization Checking
		} // Close Session Checking
		resultType = SUCCESS;
		return resultType;
	}

	public String getCustomerProjectsList() {

		resultType = LOGIN;

		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			String loginId = httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID)
					.toString();
			userRoleId = Integer.parseInt(
					httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_ROLE_ID).toString());
			resultType = "accessFailed";
			// String objectId =
			// httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_EMP_ID).toString();

			if (AuthorizationManager.getInstance().isAuthorizedUser("GET_PMO_ACTIVITY", userRoleId)) {

				try {
					setCurrentAction("getCustomerProjectsList");
					;
					// String empId =
					// httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_EMP_ID).toString();

					resultType = SUCCESS;
				} catch (Exception ex) {
					ex.printStackTrace();
					httpServletRequest.getSession(false).setAttribute("errorMessage", ex.toString());
					resultType = ERROR;
				}

			}
		}
		return resultType;
	}

	public String getCustomerProjectsDetailsList() {

		resultType = LOGIN;
		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			// String loginId =
			// httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID).toString();
			userRoleId = Integer.parseInt(
					httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_ROLE_ID).toString());
			resultType = "accessFailed";
			System.out.println("role" + userRoleId);
			// String objectId =
			// httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_EMP_ID).toString();
			setCustomerName(getCustomerName());
			setAccountId(accountId);
			if (AuthorizationManager.getInstance().isAuthorizedUser("GET_PMO_ACTIVITY", userRoleId)) {

				try {
					// System.out.println("in getCustomerProjectsList try"+getStartDateFrom()+"To"+getStartDateTo()+"from"+getEndDateFrom()+"efrom"+getEndDateTo());
					setCurrentAction("getCustomerProjectsList");
					if (getSubmitFrom() == null) {

						queryString = "SELECT tblCrmAccount.NAME,tblProjects.CustomerId,tblProjects.Id,tblProjects.ProjectName,tblProjects.STATUS,"
								+ "tblProjects.TotalResources,tblProjects.ProjectStartDate,tblProjects.ProjectEndDate,tblProjects.Comments,CASE WHEN (tblProjects.PMO= -1)"
								+ " THEN  '' ELSE tblProjects.PMO END AS PMO,CASE WHEN (tblProjects.PreSalesMgrId= -1) "
								+ "THEN  '' ELSE tblProjects.PreSalesMgrId END AS preSalesMgr FROM tblCrmAccount JOIN tblProjects  "
								+ "ON tblCrmAccount.Id = tblProjects.CustomerId WHERE tblCrmAccount.Id ="
								+ getAccountId();

						if (getPrjName() != null && !"".equalsIgnoreCase(getPrjName())) {
							queryString = queryString + " and  tblProjects.ProjectName like '%" + getPrjName() + "%'";
						}
						
						
						if (!"".equals(getStatus()) && getStatus() != null) {
							queryString = queryString + " and tblProjects.STATUS='" + getStatus() + "' ";
						}
						
						if ((getStartDateFrom() != null && !"".equals(getStartDateFrom()))) {
							queryString = queryString + " AND DATE(tblProjects.ProjectStartDate) >= '"
								+ DateUtility.getInstance().convertStringToMySQLDate(getStartDateFrom()) + "'";
								
								
						}
						
						
						if ((getStartDateTo() != null && !"".equals(getStartDateTo()))) {
							
							queryString = queryString + " AND DATE(tblProjects.ProjectStartDate) <='"
									+ DateUtility.getInstance().convertStringToMySQLDate(getStartDateTo()) + "'";
							
							
						}
						
						
						if ((getEndDateFrom() != null && !"".equals(getEndDateFrom()))) {
							queryString = queryString + " AND DATE(tblProjects.ProjectEndDate) >= '"
								+ DateUtility.getInstance().convertStringToMySQLDate(getEndDateFrom()) + "'";
								
								
						}
						
						
						if ((getEndDateTo() != null && !"".equals(getEndDateTo()))) {
							queryString = queryString + " AND DATE(tblProjects.ProjectEndDate) <= '"
							
								
								+ DateUtility.getInstance().convertStringToMySQLDate(getEndDateTo()) + "'"; 
						}
						
						
						queryString=queryString+"  ORDER BY tblProjects.ProjectStartDate DESC";
						
						// System.out.println("queryString..getCustomerProjectsDetailsList----"+ queryString);
						httpServletRequest.getSession(false)
								.setAttribute(ApplicationConstants.QS_CUSTOMER_PROJECTS_LIST, queryString);

					}
					resultType = SUCCESS;
				} catch (Exception ex) {
					ex.printStackTrace();
					httpServletRequest.getSession(false).setAttribute("errorMessage", ex.toString());
					resultType = ERROR;
				}

			}
		}
		return resultType;
	}

	/*
	 * Employee Resume Download Start
	 * 
	 */
	public String downloadEmployeeResume() {
		try {
			resultType = INPUT;
			// this.setId(Integer.parseInt(httpServletRequest.getParameter("Id").toString()));

			// this.setAttachmentLocation(ServiceLocator.getProjIssuesService().getAttachmentLocation(this.getId()));
			// setResultMessage();
			String location = ServiceLocator.getEmployeeService().getEmployeeResumeLocation(getId());

			getHttpServletResponse().setContentType("application/force-download");

			File file = new File(location);
			if (file.exists()) {

				String fileName = file.getName();
				inputStream = new FileInputStream(file);
				outputStream = getHttpServletResponse().getOutputStream();
				getHttpServletResponse().setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
				int noOfBytesRead = 0;
				byte[] byteArray = null;
				while (true) {
					byteArray = new byte[1024];
					noOfBytesRead = getInputStream().read(byteArray);
					if (noOfBytesRead == -1) {
						break;
					}
					getOutputStream().write(byteArray, 0, noOfBytesRead);
				}

			} else {
				resultMessage = "<font color=\"red\" size=\"1.5\">Sorry! File Does not Exist!</font>";
				httpServletRequest.getSession().setAttribute(ApplicationConstants.RESULT_MSG, resultMessage);
			}
		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} finally {
			try {
				if (getInputStream() != null) {
					getInputStream().close();
					getOutputStream().close();
				}
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return resultType;
	}

	public String projectPortfolioReport() {

		resultType = LOGIN;
		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			userRoleId = Integer.parseInt(
					httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_ROLE_ID).toString());
			String loginId = httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID)
					.toString();
			String objectId = httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_EMP_ID)
					.toString();
			// System.out.println("objectId-->"+objectId);
			resultType = "accessFailed";
			// if
			// (AuthorizationManager.getInstance().isAuthorizedUser("GET_PROJECT_DASH_BOARD",
			// userRoleId)) {
			try {
				dataSourceDataProvider = dataSourceDataProvider.getInstance();
				// setMyProjects(new HashMap());
				// String departmentId =
				// DataSourceDataProvider.getInstance().getDepartmentName(loginId);
				// setManagerTeamMembersList(dataSourceDataProvider.getInstance().getMyTeamMembers(loginId,
				// departmentId));
				// setManagerTeamMembersList((Map)
				// httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_MY_TEAM_MAP));
				setStatus("Active");
				setClientMap(dataSourceDataProvider.getClientMap());
				setProjectTypesList(DataSourceDataProvider.getInstance().getProjectTypesList());

				resultType = SUCCESS;
			} catch (Exception ex) {
				httpServletRequest.getSession(false).setAttribute("errorMessage", ex.toString());
				resultType = ERROR;
			}

			// }//END-Authorization Checking
		} // Close Session Checking
		resultType = SUCCESS;
		return resultType;
	}

	/*
	 * Employee Resume Download end
	 * 
	 */

	/*
	 * Quarterly Appraisal Start
	 */
	public String getMyQuarterlyAppraisalSearch() {
		resultType = LOGIN;

		if (httpServletRequest.getSession(false) != null
				&& httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			userRoleId = Integer.parseInt(
					httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_ROLE_ID).toString());
			// String workingCountry =
			// httpServletRequest.getSession(false).getAttribute(ApplicationConstants.WORKING_COUNTRY).toString();
			String empId = httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_EMP_ID)
					.toString();

			if (AuthorizationManager.getInstance().isAuthorizedUser("QUARTERLY_APPRAISAL", userRoleId)) {
				try {
					GregorianCalendar cal = new GregorianCalendar();
					int year = cal.get(Calendar.YEAR);

					if (getYear() == 0) {
						setYear(year);
					}

					queryString = "SELECT * FROM vwQuarterlyAppraisalsList  where EmpId=" + empId + " and QYear="
							+ getYear();

					if (getQuarterly() != null && !"".equals(quarterly)) {
						queryString = queryString + " AND Quarterly='" + getQuarterly() + "' ";
					}
					// queryString=queryString+" ORDER BY yer,Quarterly";

					httpServletRequest.getSession(false).setAttribute(ApplicationConstants.QS_EMP_APPRAISAL_LIST,
							queryString);

					resultType = SUCCESS;
				} catch (Exception ex) {
					// List errorMsgList =
					// ExceptionToListUtility.errorMessages(ex);
					httpServletRequest.getSession(false).setAttribute("errorMessage", ex.toString());
					resultType = ERROR;
				}
			} // END-Authorization Checking
		} // Close Session Checking
		return resultType;
	}

	public String teamQuaterAppraisalSearch() {
		resultType = LOGIN;

		if (httpServletRequest.getSession(false) != null
				&& httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			userRoleId = Integer.parseInt(
					httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_ROLE_ID).toString());
			// String workingCountry =
			// httpServletRequest.getSession(false).getAttribute(ApplicationConstants.WORKING_COUNTRY).toString();
			String empId = httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_EMP_ID)
					.toString();
			String loginId1 = httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID)
					.toString();
			if (AuthorizationManager.getInstance().isAuthorizedUser("QUARTERLY_APPRAISAL", userRoleId)) {
				try {

					GregorianCalendar cal = new GregorianCalendar();
					int year = cal.get(Calendar.YEAR);

					if (getYear() == 0) {
						setYear(year);
					}
					Map rolesMap = (Map) httpServletRequest.getSession(false)
							.getAttribute(ApplicationConstants.SESSION_MY_ROLES);
					// queryString = "SELECT
					// tblQuarterlyAppraisals.Id,EmpId,AppraisalId,CONCAT(fname,'
					// ',mName,'.',lName) AS
					// empName,DATE_FORMAT(tblQuarterlyAppraisals.CreatedDate,'%M')
					// AS mnth, YEAR(tblQuarterlyAppraisals.CreatedDate) AS
					// yer,tblQuarterlyAppraisals.CreatedDate,Quarterly,STATUS,SubmittedDate,ApprovedDate,OpperationTeamStatus,CONCAT(emp2.fname,'.',emp2.lName)
					// AS approvedBy FROM tblQuarterlyAppraisals LEFT JOIN
					// tblEmployee emp1 ON(tblQuarterlyAppraisals.EmpId=emp1.Id)
					// LEFT JOIN tblEmployee emp2
					// ON(tblQuarterlyAppraisals.ApprovedBy=emp2.LoginId) where
					// year(tblQuarterlyAppraisals.CreatedDate)=" + getYear();
					queryString = "SELECT * FROM vwQuarterlyAppraisalsList where QYear=" + getYear();
					Map myTeamMemebrs = new HashMap();
					// System.out.println("httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_ROLE_NAME).toString()==="
					// +
					// httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_ROLE_NAME).toString());
					String leadFlag = "false";
					if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_ROLE_NAME)
							.toString().equalsIgnoreCase("Employee")) {
						if (loginId1.equals("rkalaga") && backToFlag == 1) {
							// myTeamMemebrs =
							// DataSourceDataProvider.getInstance().getAllEmployees();
							myTeamMemebrs = DataSourceDataProvider.getInstance().getAllEmployeesByCountry("India");
						} else {

							leadFlag = "true";
							Map myLeadChnge = new HashMap();
							// myTeamMemebrs = (Map)
							// httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_MY_TEAM_MAP);
							String LoginIdKey = "";
							String LoginIdValue = "";
							int i = 0;

							myLeadChnge = DataSourceDataProvider.getInstance().getmyLeadChange(loginId1, getYear());
							Set entrySet = myLeadChnge.entrySet();
							Iterator it = entrySet.iterator();
							if (entrySet.size() > 0) {
								while (it.hasNext()) {
									Map.Entry map = (Map.Entry) it.next();
									LoginIdKey = (String) map.getKey();
									LoginIdValue = (String) map.getValue();
									i++;
									if (i == 1) {
										myTeamMemebrs = DataSourceDataProvider.getInstance().getTeamMapWithCurrentUser(
												LoginIdKey, LoginIdValue, (Map) httpServletRequest.getSession(false)
														.getAttribute(ApplicationConstants.SESSION_MY_TEAM_MAP));
									} else {
										myTeamMemebrs = DataSourceDataProvider.getInstance()
												.getTeamMapWithCurrentUser(LoginIdKey, LoginIdValue, myTeamMemebrs);
									}

									/*
									 * myTeamMemebrs = (Map)
									 * httpServletRequest.getSession(false)
									 * .getAttribute(ApplicationConstants.
									 * SESSION_MY_TEAM_MAP); myLeadChnge =
									 * DataSourceDataProvider.getInstance().
									 * getmyLeadChnge(loginId1);
									 * myTeamMemebrs.putAll(myLeadChnge);
									 * myTeamMemebrs=DataUtility.getInstance().
									 * getMapSortByValue(myTeamMemebrs);
									 */

								}
							} else {
								myTeamMemebrs = (Map) httpServletRequest.getSession(false)
										.getAttribute(ApplicationConstants.SESSION_MY_TEAM_MAP);
							}
							myTeamMemebrs = DataUtility.getInstance().getMapSortByValue(myTeamMemebrs);

						}
					} else if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_ROLE_NAME)
							.toString().equalsIgnoreCase("Operations")) {

						String department = httpServletRequest.getSession(false)
								.getAttribute(ApplicationConstants.SESSION_MY_DEPT_ID).toString();
						int isManager = Integer.parseInt(httpServletRequest.getSession(false)
								.getAttribute(ApplicationConstants.SESSION_IS_USER_MANAGER).toString());
						String access[] = Properties.getProperty("QuarterlyAppraisal.Access").split(Pattern.quote(","));

						List accessList = Arrays.asList(access);
						if (accessList.contains(loginId1) || (department.equals("Operations") && isManager == 1)
								|| rolesMap.containsValue("Admin")) {
							setAccessCount(1);
						}
						if (getAccessCount() == 1) {
							// myTeamMemebrs =
							// DataSourceDataProvider.getInstance().getAllEmployees();
							myTeamMemebrs = DataSourceDataProvider.getInstance().getAllEmployeesByCountry("India");

						} else {
							myTeamMemebrs = DataSourceDataProvider.getInstance().getInstance()
									.getEmployeeNamesByOperationsContactId((httpServletRequest.getSession(false)
											.getAttribute(ApplicationConstants.SESSION_EMP_ID).toString()));

						}
						// myTeamMemebrs = (Map)
						// httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_MY_TEAM_MAP);

					}
					String teamList = DataSourceDataProvider.getInstance().getTeamLoginIdList(myTeamMemebrs);
					setMyTeamMembers(myTeamMemebrs);
					if (loginId != null && !"".equals(loginId)) {
						queryString = queryString + " AND loginId ='" + loginId + "'";
					} else {
						if (!"".equals(teamList)) {
							queryString = queryString + " AND loginId IN(" + teamList + ")";
						} else {
							queryString = queryString + " AND loginId IN('')";
						}
					}
					if (getQuarterly() != null && !"".equals(quarterly)) {
						queryString = queryString + " AND Quarterly='" + getQuarterly() + "' ";
					}

					if (status != null && !"".equals(status)) {
						queryString = queryString + " AND status='" + status + "' ";
					}

					if (operationTeamStatus != null && !"".equals(operationTeamStatus)) {
						queryString = queryString + " AND OpperationTeamStatus='" + operationTeamStatus + "' ";
					}

					if (departmentId != null && !"".equals(departmentId)) {
						queryString = queryString + " AND DepartmentId='" + departmentId + "' ";
					}
					if (practiceId != null && !"".equals(practiceId) && !"All".equals(practiceId)) {
						queryString = queryString + " AND Practice='" + practiceId + "' ";
					}
					if (subPractice != null && !"".equals(subPractice) && !"All".equals(subPractice)) {
						queryString = queryString + " AND SubPractice='" + subPractice + "' ";
					}
					if (opsContactId != null && !"".equals(opsContactId)) {
						queryString = queryString + " AND OpsContactId=" + opsContactId + " ";
					}
					if (location != null && !"".equals(location)) {
						queryString = queryString + " AND Location='" + location + "' ";
					}
					if (titleId != null && !"".equals(titleId)) {

						String ManagerList = Properties.getProperty("Management.Quarter");
						if (titleId.equals("Management")) {
							queryString = queryString + " AND FIND_IN_SET(Title,'" + ManagerList + "') ";
						} else {
							queryString = queryString + " AND !FIND_IN_SET(Title,'" + ManagerList + "') ";
						}
					}
					if (getWeightageFrom() != -1) {
						queryString = queryString + " AND TotalCalWeightage >=" + getWeightageFrom();
					}
					if (getWeightageTo() != -1) {
						queryString = queryString + " AND TotalCalWeightage <=" + getWeightageTo();
					}

					/*if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_ROLE_NAME)
							.toString().equalsIgnoreCase("Employee")) {
					queryString = queryString + " AND ReportsTo='" +loginId1 + "'"; 
					}*/
					/*
					 * if ("true".equalsIgnoreCase(leadFlag)) { queryString =
					 * queryString + " AND CASE WHEN EmpReportsTo='"
					 * +loginId1+"' THEN 1=1 ELSE ReportsTo = '"
					 * +loginId1+"' END "; }
					 */
					// queryString = queryString + " AND CASE WHEN
					// EmpReportsTo='"+loginId1+"' THEN 1=1 ELSE ReportsTo =
					// '"+loginId1+"' END ";
					/*
					 * if (httpServletRequest.getSession(false).getAttribute(
					 * ApplicationConstants.SESSION_ROLE_NAME).toString().
					 * equalsIgnoreCase("Operations") ||
					 * loginId1.equals("rkalaga")) { queryString = queryString +
					 * " AND STATUS NOT IN ('Submitted','Entered') ";
					 * 
					 * 
					 * }else{ queryString = queryString +
					 * " AND STATUS NOT IN ('Entered') "; }
					 */

					// queryString=queryString+" ORDER BY yer,Quarterly";
					String Country = (String) httpServletRequest.getSession(false)
							.getAttribute(ApplicationConstants.Living_COUNTRY);
					if (rolesMap.containsValue("Admin")) {
						setOpsContactIdMap(DataSourceDataProvider.getInstance().getOpsContactId(Country, "Yes"));
					} else {
						setOpsContactIdMap(DataSourceDataProvider.getInstance().getOpsContactId(Country, "No"));
					}

					if (rolesMap.containsValue("Admin")) {
						setLocationsMap(DataSourceDataProvider.getInstance().getEmployeeLocationsList("%"));
					} else {
						setLocationsMap(DataSourceDataProvider.getInstance().getEmployeeLocationsList(Country));
					}
					httpServletRequest.getSession(false).setAttribute(ApplicationConstants.QS_EMP_APPRAISAL_LIST,
							queryString);

					resultType = SUCCESS;
				} catch (Exception ex) {
					ex.printStackTrace();
					// List errorMsgList =
					// ExceptionToListUtility.errorMessages(ex);
					httpServletRequest.getSession(false).setAttribute("errorMessage", ex.toString());
					resultType = ERROR;
				}
			} // END-Authorization Checking
		} // Close Session Checking
		return resultType;
	}

	public String myQuarterlyAppraisalAdd() {
		resultType = LOGIN;

		if (httpServletRequest.getSession(false) != null
				&& httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			userRoleId = Integer.parseInt(
					httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_ROLE_ID).toString());
			// int isManager =
			// Integer.parseInt(httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_IS_USER_MANAGER).toString());
			// String workingCountry =
			// httpServletRequest.getSession(false).getAttribute(ApplicationConstants.WORKING_COUNTRY).toString();

			if (AuthorizationManager.getInstance().isAuthorizedUser("QUARTERLY_APPRAISAL", userRoleId)) {
				try {
					int isManager = 0;
					String title = httpServletRequest.getSession(false)
							.getAttribute(ApplicationConstants.SESSION_EMP_TITLE).toString();

					String Factortype[] = Properties.getProperty("Management.Quarter").split(Pattern.quote(","));

					List ManagementQuarter = Arrays.asList(Factortype);
					// System.out.println("ManagementQuarter===" +
					// ManagementQuarter);
					// System.out.println("title===" + title);
					if (ManagementQuarter.contains(title)) {
						isManager = 1;
					}
					String empId = httpServletRequest.getSession(false)
							.getAttribute(ApplicationConstants.SESSION_EMP_ID).toString();
					setEmpId(Integer.parseInt(empId));
					if (getYear() >= 2017) {

						int count = ServiceLocator.getEmployeeService().isExistedAppraisal(Integer.parseInt(empId),
								getYear(), getQuarterly());

						if (count == 0) {
							int notClosed = ServiceLocator.getEmployeeService()
									.notClosedAppraisal(Integer.parseInt(empId));
							if (notClosed == 0) {
								int isEligible = ServiceLocator.getEmployeeService()
										.isEligibleForAppraisal(Integer.parseInt(empId), getYear(), getQuarterly());

								if (isEligible == 1) {

									int previousQReview = 0;

									//
									if (getYear() == 2017 && getQuarterly().endsWith("Q1")) {
										previousQReview = 1;
									} else {
										previousQReview = ServiceLocator.getEmployeeService().isPreviousQReviewAdded(
												Integer.parseInt(empId), getYear(), getQuarterly());

									}
									if (previousQReview == 1) {

										// String quarterAppraisalDetails =
										// ServiceLocator.getEmployeeService().getEmployeeResumeLocation(getId());
										String result = (ServiceLocator.getEmployeeService()
												.getQuarterAppraisalDetails(isManager, Integer.parseInt(empId)));
										setQuarterAppraisalDetails(result.split(Pattern.quote("@^$"))[0]);
										String currentStatus = result.split(Pattern.quote("@^$"))[1];
										if (currentStatus != null && !"".equals(currentStatus)
												&& !"_".equals(currentStatus)) {
											setCurrStatus(currentStatus);
										} else {
											setCurrStatus("");
										}

										// setDeliveryKeyFactorsList(DataSourceDataProvider.getInstance().getDeliveryFactors());
										// setTrainingKeyFactorsList(DataSourceDataProvider.getInstance().getTrainingFactors());
										List empdetails = DataSourceDataProvider.getInstance()
												.getEmployeeInfoById(empId);
										if (empdetails.size() > 0) {
											setEmpName((String) empdetails.get(0));
											setItgBatch((String) empdetails.get(1));
											setEmpDateOFBirth((String) empdetails.get(2));
											setPracticeId((String) empdetails.get(4));
											setTitleId((String) empdetails.get(5));
											// System.out.println("setPracticeId==="
											// +
											// getPracticeId());
											isManager = Integer.parseInt(httpServletRequest.getSession(false)
													.getAttribute(ApplicationConstants.SESSION_IS_USER_MANAGER)
													.toString());
											if (isManager == 1) {
												setIsManager(true);
											} else {
												setIsManager(false);
											}
										}

										// System.out.println("getCurretRole==="
										// +
										// getCurretRole());

										setCurretRole("my");
										setQyear(getYear());
										httpServletRequest.setAttribute("currentStatusData",
												ServiceLocator.getEmployeeService()
														.employeeCurrentStatusDetailsForQreview(Integer.parseInt(empId),
																getYear(), getQuarterly()));
										// System.out.println("getCurretRole1==="
										// +
										// getCurretRole());
										// searchPrepare();
										// prepare();
										resultType = SUCCESS;
									} else {
										resultMessage = "<font color=\"red\" size=\"2.5\">Sorry !! You are not allowed to enter "
												+ getQuarterly()
												+ " review as your previous review was not completed. Please contact operation team.</font>";
										httpServletRequest.getSession().setAttribute(ApplicationConstants.RESULT_MSG,
												resultMessage);
										resultType = INPUT;
									}

								} else {
									resultMessage = "<font color=\"red\" size=\"2.5\">Sorry !! You have joined after this Quarter so you are not allowed to enter "
											+ getQuarterly() + " review.</font>";
									httpServletRequest.getSession().setAttribute(ApplicationConstants.RESULT_MSG,
											resultMessage);
									resultType = INPUT;
								}
							} else {
								// resultMessage = "<font color=\"red\"
								// size=\"2.5\">Sorry !! You are not allowed to
								// add next Q-review until your previous
								// Q-review is aprroved by your manager and
								// operation team.</font>";
								resultMessage = "<font color=\"red\" size=\"2.5\">Sorry !! You are not allowed to add next Q-review until your previous Q-review submit.</font>";
								httpServletRequest.getSession().setAttribute(ApplicationConstants.RESULT_MSG,
										resultMessage);
								resultType = INPUT;
							}
						} else {
							resultMessage = "<font color=\"red\" size=\"2.5\">Sorry! Quarterly  Review  already added for this quarter</font>";
							httpServletRequest.getSession().setAttribute(ApplicationConstants.RESULT_MSG,
									resultMessage);
							resultType = INPUT;
						}
					} else {
						resultMessage = "<font color=\"red\" size=\"2.5\">Sorry! we are not allowed before 2017</font>";
						httpServletRequest.getSession().setAttribute(ApplicationConstants.RESULT_MSG, resultMessage);
						resultType = INPUT;
					}
				} catch (Exception ex) {
					// List errorMsgList =
					// ExceptionToListUtility.errorMessages(ex);
					httpServletRequest.getSession(false).setAttribute("errorMessage", ex.toString());
					resultType = ERROR;
				}
			} // END-Authorization Checking
		} // Close Session Checking
		return resultType;
	}

	public String empQuarterlyAppraisalSave() {
		resultType = LOGIN;

		if (httpServletRequest.getSession(false) != null
				&& httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			userRoleId = Integer.parseInt(
					httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_ROLE_ID).toString());
			int isManager = Integer.parseInt(httpServletRequest.getSession(false)
					.getAttribute(ApplicationConstants.SESSION_IS_USER_MANAGER).toString());
			// String workingCountry =
			// httpServletRequest.getSession(false).getAttribute(ApplicationConstants.WORKING_COUNTRY).toString();
			String livingCountry = httpServletRequest.getSession(false)
					.getAttribute(ApplicationConstants.Living_COUNTRY).toString();
			String userRoleName = httpServletRequest.getSession(false)
					.getAttribute(ApplicationConstants.SESSION_ROLE_NAME).toString();

			if (AuthorizationManager.getInstance().isAuthorizedUser("QUARTERLY_APPRAISAL", userRoleId)) {
				try {
					// System.out.println("shortTermGoalComments===" +
					// getShortTermGoalComments());

					String result = ServiceLocator.getEmployeeService().empQuarterlyAppraisalSave(httpServletRequest,
							getRowCount(), getCurretRole(), getAppraisalId(), getStatus(), getEmpId(), getLineId(),
							getShortTermGoal(), getShortTermGoalComments(), getLongTermGoal(),
							getLongTermGoalComments(), getStrength(), getStrengthsComments(), getImprovements(),
							getImprovementsComments(), getQuarterly(), getRejectedComments(), getQyear());
					// System.out.println("result===" + result);
					if (result.contains("added")) {

						setAppraisalId(Integer.parseInt(result.split(Pattern.quote("#^$"))[1]));
						setLineId(Integer.parseInt(result.split(Pattern.quote("#^$"))[2]));
						result = result.split(Pattern.quote("#^$"))[0];
					}
					if (result.equals("added") && getStatus().equals("Entered")) {
						resultType = "added";
						resultMessage = "<font color='green' size='2.5'>Quarterly  Review " + getStatus().toLowerCase()
								+ " successfully.</font>";

						httpServletRequest.getSession().setAttribute(ApplicationConstants.RESULT_MSG, resultMessage);
					} else if (result.equals("updated") && getStatus().equals("Entered")) {
						resultType = "added";
						resultMessage = "<font color='green' size='2.5'>Quarterly  Review " + getStatus().toLowerCase()
								+ " successfully.</font>";

						httpServletRequest.getSession().setAttribute(ApplicationConstants.RESULT_MSG, resultMessage);
					} else if (result.equals("updated") || result.equals("added")) {
						resultMessage = "<font color='green' size='2.5'>Quarterly  Review " + getStatus().toLowerCase()
								+ " successfully</font>";

						httpServletRequest.getSession().setAttribute(ApplicationConstants.RESULT_MSG, resultMessage);
						resultType = SUCCESS;
					} else {
						resultMessage = "<font color='red' size='2.5'>please try again later</font>";

						httpServletRequest.getSession().setAttribute(ApplicationConstants.RESULT_MSG, resultMessage);
						resultType = SUCCESS;
					}

					if (getCurretRole().equals("team")) {
						resultType = "team";
					}
				} catch (Exception ex) {
					// List errorMsgList =
					// ExceptionToListUtility.errorMessages(ex);
					// httpServletRequest.getSession(false).setAttribute("errorMessage",
					// ex.toString());
					// ex.printStackTrace();
					// resultType = ERROR;
					resultMessage = "<font color='red' size='2.5'>Oops..please try again once.</font>";
					httpServletRequest.getSession().setAttribute(ApplicationConstants.RESULT_MSG, resultMessage);
					resultType = SUCCESS;
				}
			} // END-Authorization Checking
		} // Close Session Checking
		return resultType;
	}

	public String myQuarterlyAppraisalEdit() {
		resultType = LOGIN;

		if (httpServletRequest.getSession(false) != null
				&& httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			userRoleId = Integer.parseInt(
					httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_ROLE_ID).toString());
			loginId = httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID)
					.toString();
			int isManager = Integer.parseInt(httpServletRequest.getSession(false)
					.getAttribute(ApplicationConstants.SESSION_IS_USER_MANAGER).toString());
			int isLead = Integer.parseInt(httpServletRequest.getSession(false)
					.getAttribute(ApplicationConstants.SESSION_IS_TEAM_LEAD).toString());
			// String workingCountry =
			// httpServletRequest.getSession(false).getAttribute(ApplicationConstants.WORKING_COUNTRY).toString();
			String livingCountry = httpServletRequest.getSession(false)
					.getAttribute(ApplicationConstants.Living_COUNTRY).toString();
			String userRoleName = httpServletRequest.getSession(false)
					.getAttribute(ApplicationConstants.SESSION_ROLE_NAME).toString();
			resultType = "accessFailed";
			if (AuthorizationManager.getInstance().isAuthorizedUser("QUARTERLY_APPRAISAL", userRoleId)) {
				try {
					GregorianCalendar cal = new GregorianCalendar();
					int year = cal.get(Calendar.YEAR);

					if (getYear() == 0) {
						setYear(year);
					}
					boolean accessAllow = false;
					if (getEmpId() != 0 && getAppraisalId() != 0 && getLineId() != 0) {
						String emploginId = DataSourceDataProvider.getInstance().getLoginIdByEmpId(getEmpId());

						Map myTeamMemebrs = new HashMap();
						Map rolesMap = (Map) httpServletRequest.getSession(false)
								.getAttribute(ApplicationConstants.SESSION_MY_ROLES);
						String department = httpServletRequest.getSession(false)
								.getAttribute(ApplicationConstants.SESSION_MY_DEPT_ID).toString();
						String access[] = Properties.getProperty("QuarterlyAppraisal.Access").split(Pattern.quote(","));
						List accessList = Arrays.asList(access);

						int sessionEmpId = Integer.parseInt(httpServletRequest.getSession(false)
								.getAttribute(ApplicationConstants.SESSION_EMP_ID).toString());
						if (getCurretRole() != null && getCurretRole().equals("my")) {

							if (sessionEmpId == getEmpId()) {
								accessAllow = true;
							}
						} else if (getCurretRole() != null && getCurretRole().equals("team")) {
							String roleName = httpServletRequest.getSession(false)
									.getAttribute(ApplicationConstants.SESSION_ROLE_NAME).toString();
							if (loginId.equals("rkalaga") && backToFlag == 1) {
								accessAllow = true;
							} else if ((roleName.equals("Operations") && accessList.contains(loginId))
									|| (department.equals("Operations") && isManager == 1)
									|| rolesMap.containsValue("Admin")) {
								accessAllow = true;
							} else {
								if (roleName.equalsIgnoreCase("Employee") && (isManager == 1 || isLead == 1)) {
									// myTeamMemebrs = (Map)
									// httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_MY_TEAM_MAP);

									String LoginIdKey = "";
									String LoginIdValue = "";
									int i = 0;
									Map myLeadChnge = new HashMap();
									myLeadChnge = DataSourceDataProvider.getInstance().getmyLeadChange(loginId,
											getYear());
									Set entrySet = myLeadChnge.entrySet();
									Iterator it = entrySet.iterator();
									if (entrySet.size() > 0) {
										while (it.hasNext()) {
											Map.Entry map = (Map.Entry) it.next();
											LoginIdKey = (String) map.getKey();
											LoginIdValue = (String) map.getValue();
											i++;
											if (i == 1) {
												myTeamMemebrs = DataSourceDataProvider.getInstance()
														.getTeamMapWithCurrentUser(LoginIdKey, LoginIdValue,
																(Map) httpServletRequest.getSession(false).getAttribute(
																		ApplicationConstants.SESSION_MY_TEAM_MAP));
											} else {
												myTeamMemebrs = DataSourceDataProvider.getInstance()
														.getTeamMapWithCurrentUser(LoginIdKey, LoginIdValue,
																myTeamMemebrs);
											}

											/*
											 * myTeamMemebrs = (Map)
											 * httpServletRequest.getSession(
											 * false) .getAttribute(
											 * ApplicationConstants.
											 * SESSION_MY_TEAM_MAP); myLeadChnge
											 * = DataSourceDataProvider.
											 * getInstance().
											 * getmyLeadChnge(loginId1);
											 * myTeamMemebrs.putAll(myLeadChnge)
											 * ; myTeamMemebrs=DataUtility.
											 * getInstance().
											 * getMapSortByValue(myTeamMemebrs);
											 */

										}
									} else {
										myTeamMemebrs = (Map) httpServletRequest.getSession(false)
												.getAttribute(ApplicationConstants.SESSION_MY_TEAM_MAP);
									}

									myTeamMemebrs = DataUtility.getInstance().getMapSortByValue(myTeamMemebrs);

									/*
									 * myTeamMemebrs = (Map)
									 * httpServletRequest.getSession(false)
									 * .getAttribute(ApplicationConstants.
									 * SESSION_MY_TEAM_MAP);
									 */

									if (myTeamMemebrs.containsKey(emploginId)) {
										accessAllow = true;
									}

								}
								if (roleName.equalsIgnoreCase("Operations")) {
									myTeamMemebrs = DataSourceDataProvider.getInstance().getInstance()
											.getEmployeeNamesByOperationsContactId((httpServletRequest.getSession(false)
													.getAttribute(ApplicationConstants.SESSION_EMP_ID).toString()));

									if (myTeamMemebrs.containsKey(emploginId)) {
										accessAllow = true;
									}
								}
								if (roleName.equalsIgnoreCase("Employee")) {

									String ReportSto = DataSourceDataProvider.getInstance().getInstance()
											.getReportsToForQReview(getEmpId(), getAppraisalId());

									if (loginId.equals(ReportSto)) {
										accessAllow = true;
									}
								}

							}
						}
						if (accessAllow) {
							// String quarterAppraisalDetails =
							// ServiceLocator.getEmployeeService().getEmployeeResumeLocation(getId());
							String result = ServiceLocator.getEmployeeService().quarterlyAppraisalEdit(getEmpId(),
									getAppraisalId(), getLineId());
							setQuarterAppraisalDetails(result.split(Pattern.quote("@^$"))[0]);
							String remainingData[] = (result.split(Pattern.quote("@^$"))[1])
									.split(Pattern.quote("#^$"));
							if (remainingData[0] != null && !"".equals(remainingData[0])
									&& !"_".equals(remainingData[0])) {
								setShortTermGoal(remainingData[0]);
							}
							if (remainingData[1] != null && !"".equals(remainingData[1])
									&& !"_".equals(remainingData[1])) {
								setShortTermGoalComments(remainingData[1]);
							}
							if (remainingData[2] != null && !"".equals(remainingData[2])
									&& !"_".equals(remainingData[2])) {
								setLongTermGoal(remainingData[2]);
							}
							if (remainingData[3] != null && !"".equals(remainingData[3])
									&& !"_".equals(remainingData[3])) {
								setLongTermGoalComments(remainingData[3]);
							}
							if (remainingData[4] != null && !"".equals(remainingData[4])
									&& !"_".equals(remainingData[4])) {
								setStrength(remainingData[4]);
							}
							if (remainingData[5] != null && !"".equals(remainingData[5])
									&& !"_".equals(remainingData[5])) {
								setStrengthsComments(remainingData[5]);
							}
							if (remainingData[6] != null && !"".equals(remainingData[6])
									&& !"_".equals(remainingData[6])) {
								setImprovements(remainingData[6]);
							}
							if (remainingData[7] != null && !"".equals(remainingData[7])
									&& !"_".equals(remainingData[7])) {
								setImprovementsComments(remainingData[7]);
							}
							if (remainingData[8] != null && !"".equals(remainingData[8])
									&& !"_".equals(remainingData[8])) {
								setOperationTeamStatus(remainingData[8]);
							}

							if (remainingData[9] != null && !"".equals(remainingData[9])
									&& !"_".equals(remainingData[9])) {
								setManagerRejectedComments(remainingData[9]);
							}

							if (remainingData[10] != null && !"".equals(remainingData[10])
									&& !"_".equals(remainingData[10])) {
								setOperationRejectedComments(remainingData[10]);
							}

							java.util.Date approvedDate = null;
							java.util.Date opeartionApprovedDate = null;

							if (remainingData[11] != null && !"".equals(remainingData[11])
									&& !"_".equals(remainingData[11])) {
								approvedDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(remainingData[11]);
							}
							if (remainingData[12] != null && !"".equals(remainingData[12])
									&& !"_".equals(remainingData[12])) {
								opeartionApprovedDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
										.parse(remainingData[12]);
							}

							if (remainingData[13] != null && !"".equals(remainingData[13])
									&& !"_".equals(remainingData[13])) {
								setQuarterly(remainingData[13]);
							}

							if (remainingData[14] != null && !"".equals(remainingData[14])
									&& !"_".equals(remainingData[14])) {
								setStatus(remainingData[14]);
							}
							if (remainingData[15] != null && !"".equals(remainingData[15])
									&& !"_".equals(remainingData[15])) {
								setCurrStatus(remainingData[15]);
							} else {
								setCurrStatus("");
							}
							if (remainingData[16] != null && !"".equals(remainingData[16])
									&& !"_".equals(remainingData[16])) {
								setQyear(Integer.parseInt(remainingData[16]));
							} else {
								setQyear(0);
							}

							if (remainingData[17] != null && !"".equals(remainingData[17])
									&& !"_".equals(remainingData[17])) {

								setShortTermGoalHrComments(remainingData[17]);
							}

							if (remainingData[18] != null && !"".equals(remainingData[18])
									&& !"_".equals(remainingData[18])) {
								setLongTermGoalHrComments(remainingData[18]);
							}
							if (remainingData[19] != null && !"".equals(remainingData[19])
									&& !"_".equals(remainingData[19])) {
								setStrengthsHrComments(remainingData[19]);
							}
							if (remainingData[20] != null && !"".equals(remainingData[20])
									&& !"_".equals(remainingData[20])) {
								setImprovementsHrComments(remainingData[20]);
							}

							if (approvedDate == null && opeartionApprovedDate == null) {
								setDayCount(0);
							} else {
								if (opeartionApprovedDate == null) {
									setDayCount(1);
								} else {
									if (approvedDate.compareTo(opeartionApprovedDate) > 0) {
										setDayCount(1);
									} else {
										setDayCount(2);
									}
								}
							}

							// setDeliveryKeyFactorsList(DataSourceDataProvider.getInstance().getDeliveryFactors());
							// setTrainingKeyFactorsList(DataSourceDataProvider.getInstance().getTrainingFactors());
							List empdetails = DataSourceDataProvider.getInstance()
									.getEmployeeInfoById(String.valueOf(empId));
							if (empdetails.size() > 0) {
								setEmpName((String) empdetails.get(0));
								setItgBatch((String) empdetails.get(1));
								setEmpDateOFBirth((String) empdetails.get(2));
								setPracticeId((String) empdetails.get(4));
								setTitleId((String) empdetails.get(5));
								if (isManager == 1) {
									setIsManager(true);
								} else {
									setIsManager(false);
								}
							}

							if (getCurretRole() == null || !getCurretRole().equals("team")) {
								setCurretRole("my");
							}

							if (accessList.contains(loginId) || (department.equals("Operations"))
									|| rolesMap.containsValue("Admin")) {
								setAccessCount(1);
							}

							httpServletRequest.setAttribute("currentStatusData", ServiceLocator.getEmployeeService()
									.employeeCurrentStatusDetailsForQreview(getEmpId(), getQyear(), getQuarterly()));
							// searchPrepare();
							// prepare();
							resultType = SUCCESS;
						}
					}

				} catch (Exception ex) {
					// List errorMsgList =
					// ExceptionToListUtility.errorMessages(ex);
					ex.printStackTrace();
					httpServletRequest.getSession(false).setAttribute("errorMessage", ex.toString());
					resultType = ERROR;
				}
			} // END-Authorization Checking
		} // Close Session Checking

		/*
		 * if (httpServletRequest.getParameter("quarterly") != null) { String
		 * empId = httpServletRequest.getParameter("empId"); String lineId =
		 * httpServletRequest.getParameter("lineId"); String appraisalId =
		 * httpServletRequest.getParameter("appraisalId"); String quarterly =
		 * httpServletRequest.getParameter("quarterly");
		 * setEmpId(Integer.parseInt(empId));
		 * setLineId(Integer.parseInt(lineId));
		 * setAppraisalId(Integer.parseInt(appraisalId));
		 * setQuarterly(quarterly); }
		 */

		return resultType;
	}
	/*
	 * Quarterly Appraisal End
	 */

	/*
	 * Author : Phani Kanuri Date : 05/10/2017 Description : Qreview Dashboard
	 */
	public String getQReviewDashBoard() throws Exception {
		// System.out.println("getQReviewDashBoard----");
		resultType = LOGIN;
		// if(httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID)
		// != null){
		if ((httpServletRequest.getSession(false) != null
				&& httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null)
				|| (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_CUST_ID) != null)) {

			int userRoleId = Integer.parseInt(
					httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_ROLE_ID).toString());

			String userId = httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID)
					.toString();

			String roleName = httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_ROLE_NAME)
					.toString();
			String Country = (String) httpServletRequest.getSession(false)
					.getAttribute(ApplicationConstants.Living_COUNTRY);

			if (AuthorizationManager.getInstance().isAuthorizedUser("QUARTERLY_APPRAISAL", userRoleId)) {

				if ((httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_IS_ADMIN_ACCESS)
						.toString().equals("1"))
						|| AuthorizationManager.getInstance()
								.isAuthorizedForSurveyForm("QuarterlyAppraisalDashBoards.Access", userId)) {

					try {
						setOpsContactIdMap(DataSourceDataProvider.getInstance().getOpsContactId(Country, roleName));
						setEmpPracticeList(DataSourceDataProvider.getInstance().getEmpPracticeByDepartment());
						// setReportsToHierarchyMap(DataSourceDataProvider.getInstance().getMyTeamList(userId));
						GregorianCalendar cal = new GregorianCalendar();
						int year = cal.get(Calendar.YEAR);
						if (getBackToFlag() == 1) {
							setBackToFlag(getBackToFlag());
							setYearForManagersReport(getYearForManagersReport());
							setQuarterlyForManagersReport(getQuarterlyForManagersReport());
							if (getYear() == 0) {
								setYear(year);
							}
							setCountry(getCountry());
							setIsIncludeTeam(getIsIncludeTeam());
						} else {

							if (getYear() == 0) {
								setYear(year);
							}

							if (getYearForManagersReport() == 0) {
								setYearForManagersReport(year);
							}
							setCountry("-1");
							setIsIncludeTeam("false");

						}

						resultType = SUCCESS;
					} catch (Exception ex) {
						// List errorMsgList =
						// ExceptionToListUtility.errorMessages(ex);
						httpServletRequest.getSession(false).setAttribute("errorMessage", ex.toString());
						resultType = ERROR;
					}
				}
			}
		}
		return resultType;
	}

	/*
	 * Author : NagaLakshmi Telluri Date : 06/04/2018 Description : Qreview
	 * Pending Report Dashboard
	 */
	public String getQReviewPendingReportDashBoard() throws Exception {
		// System.out.println("getQReviewDashBoard----");
		resultType = LOGIN;
		// if(httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID)
		// != null){
		if ((httpServletRequest.getSession(false) != null
				&& httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null)
				|| (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_CUST_ID) != null)) {

			int userRoleId = Integer.parseInt(
					httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_ROLE_ID).toString());

			String userId = httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID)
					.toString();

			String roleName = httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_ROLE_NAME)
					.toString();
			String Country = (String) httpServletRequest.getSession(false)
					.getAttribute(ApplicationConstants.Living_COUNTRY);

			if (AuthorizationManager.getInstance().isAuthorizedUser("QUARTERLY_APPRAISAL", userRoleId)) {

				/*
				 * if ((httpServletRequest.getSession(false).getAttribute(
				 * ApplicationConstants.SESSION_IS_ADMIN_ACCESS)
				 * .toString().equals("1")) ||
				 * AuthorizationManager.getInstance()
				 * .isAuthorizedForSurveyForm(
				 * "QuarterlyAppraisalDashBoards.Access", userId)) {
				 */

				try {
					setOpsContactIdMap(DataSourceDataProvider.getInstance().getOpsContactId(Country, roleName));
					setEmpPracticeList(DataSourceDataProvider.getInstance().getEmpPracticeByDepartment());
					// setReportsToHierarchyMap(DataSourceDataProvider.getInstance().getMyTeamList(userId));
					GregorianCalendar cal = new GregorianCalendar();
					int year = cal.get(Calendar.YEAR);
					if (getBackToFlag() == 1) {
						setBackToFlag(getBackToFlag());
						setYearForManagersReport(getYearForManagersReport());
						setQuarterlyForManagersReport(getQuarterlyForManagersReport());
						setOpsContactId(getOpsContactId());

						if (getYear() == 0) {
							setYear(year);
						}
						setCountry(getCountry());
						setIsIncludeTeam(getIsIncludeTeam());
					} else {

						if (getYear() == 0) {
							setYear(year);
						}

						if (getYearForManagersReport() == 0) {
							setYearForManagersReport(year);
						}
						setQuarterlyForManagersReport("Q1");
						setCountry("-1");
						setIsIncludeTeam("false");

					}

					resultType = SUCCESS;
				} catch (Exception ex) {
					// List errorMsgList =
					// ExceptionToListUtility.errorMessages(ex);
					httpServletRequest.getSession(false).setAttribute("errorMessage", ex.toString());
					resultType = ERROR;
				}

			}
		}
		return resultType;
	}

	public String getEmployeePendingDetailsByAppraisalStatus() throws Exception {
		resultType = LOGIN;
		// if(httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID)
		// != null){
		if ((httpServletRequest.getSession(false) != null
				&& httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null)
				|| (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_CUST_ID) != null)) {
			int userRoleId = Integer.parseInt(
					httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_ROLE_ID).toString());

			String userId = httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID)
					.toString();
			if (AuthorizationManager.getInstance().isAuthorizedUser("QUARTERLY_APPRAISAL", userRoleId)) {

				try {
					setLoginId(getLoginId());
					setYear(getYear());
					setQuarterly(getQuarterly());
					setStatus(getStatus());
					setEmpName(getEmpName());
					setCountry(getCountry());
					setIsIncludeTeam(getIsIncludeTeam());
					resultType = SUCCESS;
				} catch (Exception ex) {
					// List errorMsgList =
					// ExceptionToListUtility.errorMessages(ex);
					httpServletRequest.getSession(false).setAttribute("errorMessage", ex.toString());
					resultType = ERROR;
				}

			}
		}
		return resultType;
	}

	public String getEmployeeDetailsByAppraisalStatus() throws Exception {
		resultType = LOGIN;
		// if(httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID)
		// != null){
		if ((httpServletRequest.getSession(false) != null
				&& httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null)
				|| (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_CUST_ID) != null)) {
			int userRoleId = Integer.parseInt(
					httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_ROLE_ID).toString());

			String userId = httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID)
					.toString();
			if (AuthorizationManager.getInstance().isAuthorizedUser("QUARTERLY_APPRAISAL", userRoleId)) {

				if ((httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_IS_ADMIN_ACCESS)
						.toString().equals("1"))
						|| AuthorizationManager.getInstance()
								.isAuthorizedForSurveyForm("QuarterlyAppraisalDashBoards.Access", userId)) {

					try {
						setLoginId(getLoginId());
						setYear(getYear());
						setQuarterly(getQuarterly());
						setStatus(getStatus());
						setEmpName(getEmpName());
						setCountry(getCountry());
						setIsIncludeTeam(getIsIncludeTeam());
						resultType = SUCCESS;
					} catch (Exception ex) {
						// List errorMsgList =
						// ExceptionToListUtility.errorMessages(ex);
						httpServletRequest.getSession(false).setAttribute("errorMessage", ex.toString());
						resultType = ERROR;
					}
				}
			}
		}
		return resultType;
	}

	/* Star Performer Associated Methods Start */
	public String starPerformance() {
		resultType = LOGIN;
		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			userRoleId = Integer.parseInt(
					httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_ROLE_ID).toString());
			int sessionIsOperationContactTeamFlag = Integer.parseInt(httpServletRequest.getSession(false)
					.getAttribute(ApplicationConstants.SESSION_ISOPERATIONCONTACTTEAM_FLAG).toString());
			int sessionStarPerformerMarketingAccess = Integer.parseInt(httpServletRequest.getSession(false)
					.getAttribute(ApplicationConstants.SESSION_STARPERFORMER_MARKETING_ACCESS).toString());
			boolean starPerformerAccess = (Boolean) httpServletRequest.getSession(false)
					.getAttribute(ApplicationConstants.STAR_PERFORMER_ACCESS);
			resultType = "accessFailed";
			if (sessionIsOperationContactTeamFlag == 1 || sessionStarPerformerMarketingAccess == 1
					|| starPerformerAccess) {

				try {

					setYear(Calendar.getInstance().get(Calendar.YEAR));
					setOverlayYear(Calendar.getInstance().get(Calendar.YEAR));
					setMonth(Calendar.getInstance().get(Calendar.MONTH) + 1);
					setOverlayMonth(Calendar.getInstance().get(Calendar.MONTH) + 1);
					setStarPerformerStatus(DataSourceDataProvider.getInstance().getStarPerformerStatus());
					setCurrentDate(DateUtility.getInstance().getCurrentMySqlDateTime1());
					setMonthsMap(DateUtility.getInstance().getMonthsMap());
					resultType = SUCCESS;
				} catch (Exception ex) {
					ex.printStackTrace();
					resultType = ERROR;
				}
			} // Close Authorization Checking
		} // Close Session Checking
		return resultType;
	}

	public String starPerformersList() {
		resultType = LOGIN;

		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			userRoleId = Integer.parseInt(
					httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_ROLE_ID).toString());
			int sessionIsOperationContactTeamFlag = Integer.parseInt(httpServletRequest.getSession(false)
					.getAttribute(ApplicationConstants.SESSION_ISOPERATIONCONTACTTEAM_FLAG).toString());
			boolean starPerformerAccess = (Boolean) httpServletRequest.getSession(false)
					.getAttribute(ApplicationConstants.STAR_PERFORMER_ACCESS);

			resultType = "accessFailed";
			if (sessionIsOperationContactTeamFlag == 1 || starPerformerAccess) {
				try {

					setId(getId());
					// setStatusId(getStatusId());
					setStatusId(dataSourceDataProvider.getInstance().getStarPerformerStatus(getId()));
					setResultMessage(getResultMessage());
					httpServletRequest.setAttribute(ApplicationConstants.RESULT_MSG, getResultMessage());
					httpServletRequest.setAttribute(ApplicationConstants.QUERY_STRING, queryString);
					// setResultMessage("<font color=\"green\" size=\"1.5\">Star
					// Performers has been successfully Added!</font>");
					resultType = SUCCESS;
				} catch (Exception ex) {
					ex.printStackTrace();
					// List errorMsgList =
					// ExceptionToListUtility.errorMessages(ex);
					httpServletRequest.getSession(false).setAttribute("errorMessage", ex.toString());
					resultType = ERROR;
				}
			}
			// END-Authorization Checking
		}
		return resultType;
	}

	public String doNomineeApproval() {
		// System.out.println("in doNomineeApproval ");
		resultType = LOGIN;

		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			userRoleId = Integer.parseInt(
					httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_ROLE_ID).toString());

			String loginId = httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID)
					.toString();
			String mail = httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_EMAIL)
					.toString();
			try {

				// System.out.println("id is---->"+getId());
				setId(getId());
				int status = dataSourceDataProvider.getInstance().getStarPerformerStatus(getId());

				setStatusId(status);
				if (status == 3) {
					int row = ServiceLocator.getEmployeeService().doNomineeApproval(getId(), getNomineeIds(), loginId,
							getStarMonth(), mail);

					if (row > 0) {
						// httpServletRequest.setAttribute(ApplicationConstants.QUERY_STRING,
						// queryString);
						// prepare();
						setStatusId(dataSourceDataProvider.getInstance().getStarPerformerStatus(getId()));
						setResultMessage("<font color=\"green\" size=\"2\">Nominees Approved successfully!</font>");
						httpServletRequest.setAttribute(ApplicationConstants.RESULT_MSG, getResultMessage());
						// System.out.println("resultMessage
						// is----->"+getResultMessage());
					} else {
						// httpServletRequest.setAttribute(ApplicationConstants.QUERY_STRING,
						// queryString);
						// prepare();

						setResultMessage("<font color=\"red\" size=\"2\">Plese try again</font>");
						httpServletRequest.setAttribute(ApplicationConstants.RESULT_MSG, getResultMessage());
						// System.out.println("resultMessage
						// is----->"+getResultMessage());
					}
				} else if (status > 3) {
					setResultMessage("<font color=\"red\" size=\"2\">Nominees Already Approved </font>");
				} else {
					setResultMessage("<font color=\"red\" size=\"2\">Nominees should be submited for Approval</font>");
				}
				resultType = SUCCESS;
			} catch (Exception ex) {
				ex.printStackTrace();
				// List errorMsgList = ExceptionToListUtility.errorMessages(ex);
				httpServletRequest.getSession(false).setAttribute("errorMessage", ex.toString());
				resultType = ERROR;
			}
			// END-Authorization Checking
		} // Close Session Checking
		return resultType;
	}

	public String starPerformerDelete() {
		// System.out.println("in doStarPerformersAdd ");
		resultType = LOGIN;

		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			userRoleId = Integer.parseInt(
					httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_ROLE_ID).toString());

			String loginId = httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID)
					.toString();

			try {

				setId(getObjectId());
				int row = 0;

				row = ServiceLocator.getEmployeeService().starPerformerDelete(getStarId());

				int status = dataSourceDataProvider.getInstance().getStarPerformerStatus(getObjectId());
				setStatusId(status);

				if (row > 0) {

					setResultMessage("<font color=\"green\" size=\"2\">Nomineee Deleted successfully!</font>");
					httpServletRequest.setAttribute(ApplicationConstants.RESULT_MSG, getResultMessage());
					// System.out.println("resultMessage
					// is----->"+getResultMessage());
				} else {
					setResultMessage("<font color=\"red\" size=\"2\">Plese try again</font>");
					httpServletRequest.setAttribute(ApplicationConstants.RESULT_MSG, getResultMessage());

				}
				resultType = SUCCESS;
			} catch (Exception ex) {
				ex.printStackTrace();
				// List errorMsgList = ExceptionToListUtility.errorMessages(ex);
				httpServletRequest.getSession(false).setAttribute("errorMessage", ex.toString());
				resultType = ERROR;
			}
			// END-Authorization Checking
		} // Close Session Checking
		return resultType;
	}

	public String doStarPerformersAdd() {
		// System.out.println("in doStarPerformersAdd ");
		resultType = LOGIN;

		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			userRoleId = Integer.parseInt(
					httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_ROLE_ID).toString());

			String loginId = httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID)
					.toString();

			try {

				int status = dataSourceDataProvider.getInstance().getStarPerformerStatus(getObjectId());
				setStatusId(status);
				// System.out.println("id is---->"+getId());
				setId(getObjectId());
				int row = 0;
				int count1 = dataSourceDataProvider.getInstance().checkResourceCountPerHr(loginId, getObjectId());
				int count = dataSourceDataProvider.getInstance().checkResourceName(getEmployeeId(), getObjectId());
				if (count1 < 3) {
					if (count == 0) {

						if (status <= 3) {
							// row =
							// ServiceLocator.getEmployeeService().doStarPerformersAdd(getResourceName(),getEmployeeId(),
							// getObjectId(), loginId);
							row = ServiceLocator.getEmployeeService().doStarPerformersAdd(getResourceName(),
									getEmployeeId(), getObjectId(), loginId, getResourceComments());
							if (row > 0) {
								httpServletRequest.setAttribute(ApplicationConstants.QUERY_STRING, queryString);
								// prepare();
								setStatusId(dataSourceDataProvider.getInstance().getStarPerformerStatus(getId()));
								setResultMessage(
										"<font color=\"green\" size=\"2\">Nominees Added successfully!</font>");
								httpServletRequest.setAttribute(ApplicationConstants.RESULT_MSG, getResultMessage());
								// System.out.println("resultMessage
								// is----->"+getResultMessage());
							} else {
								httpServletRequest.setAttribute(ApplicationConstants.QUERY_STRING, queryString);
								// prepare();

								setResultMessage("<font color=\"red\" size=\"2\">Plese try again</font>");
								httpServletRequest.setAttribute(ApplicationConstants.RESULT_MSG, getResultMessage());
								// System.out.println("resultMessage
								// is----->"+getResultMessage());
							}
						} else {

							httpServletRequest.setAttribute(ApplicationConstants.QUERY_STRING, queryString);
							// prepare();

							setResultMessage(
									"<font color=\"red\" size=\"2\">No nominees should be added in this state</font>");
							httpServletRequest.setAttribute(ApplicationConstants.RESULT_MSG, getResultMessage());
							// System.out.println("resultMessage
							// is----->"+getResultMessage());
						}

					} else {
						setResultMessage(
								"<font color=\"red\" size=\"2\">Already this nominee has been selected</font>");

					}
				} else {
					setResultMessage(
							"<font color=\"red\" size=\"2\">You are eligible to nominate only three persons per month.</font>");
				}
				resultType = SUCCESS;
			} catch (Exception ex) {
				ex.printStackTrace();
				// List errorMsgList = ExceptionToListUtility.errorMessages(ex);
				httpServletRequest.getSession(false).setAttribute("errorMessage", ex.toString());
				resultType = ERROR;
			}
			// END-Authorization Checking
		} // Close Session Checking
		return resultType;
	}

	public String doStarPerformerMvTickets() {
		resultType = LOGIN;

		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			userRoleId = Integer.parseInt(
					httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_ROLE_ID).toString());
			resultType = "accessFailed";
			try {
				// setStarPerformerCountId(Integer.parseInt(Properties.getProperty("STARPERFORMER.COUNTId")));
				// System.out.println("id is---->"+getId());
				setId(getId());
				int status = dataSourceDataProvider.getInstance().getStarPerformerStatus(getId());
				setStatusId(status);
				setSuvyId(getSuvyId());

				setResultMessage(getResultMessage());
				httpServletRequest.setAttribute(ApplicationConstants.RESULT_MSG, getResultMessage());
				httpServletRequest.setAttribute(ApplicationConstants.QUERY_STRING, queryString);
				// setResultMessage("<font color=\"green\" size=\"1.5\">Star
				// Performers has been successfully Added!</font>");
				resultType = SUCCESS;
			} catch (Exception ex) {
				ex.printStackTrace();
				// List errorMsgList = ExceptionToListUtility.errorMessages(ex);
				httpServletRequest.getSession(false).setAttribute("errorMessage", ex.toString());
				resultType = ERROR;
			}
			// END-Authorization Checking
		}
		return resultType;
	}

	public String starPerformerMovieTickets() {
		// System.out.println("in starPerformerMovieTickets ");
		resultType = LOGIN;

		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			userRoleId = Integer.parseInt(
					httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_ROLE_ID).toString());

			String loginId = httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID)
					.toString();

			try {

				setId(getObjectId());
				int row = 0;

				// System.out.println(getId());
				int status = dataSourceDataProvider.getInstance().getStarPerformerStatus(getObjectId());
				setStatusId(status);
				setSuvyId(getSuvyId());

				//
				// getUpload();
				// getUploadContentType();
				// getUploadFileName();
				//
				if (getUploadFileName() != null) {
					String path = com.mss.mirage.util.Properties.getProperty("StarPerformer.Attachments") + "/"
							+ "StarPerformerMovieTickets" + "/" + loginId;
					java.util.Date date = new java.util.Date();

					String monthName = null;

					if (date.getMonth() == 0)
						monthName = "January";
					else if (date.getMonth() == 1)
						monthName = "February";
					else if (date.getMonth() == 2)
						monthName = "March";
					else if (date.getMonth() == 3)
						monthName = "April";
					else if (date.getMonth() == 4)
						monthName = "May";
					else if (date.getMonth() == 5)
						monthName = "June";
					else if (date.getMonth() == 6)
						monthName = "July";
					else if (date.getMonth() == 7)
						monthName = "August";
					else if (date.getMonth() == 8)
						monthName = "September";
					else if (date.getMonth() == 9)
						monthName = "October";
					else if (date.getMonth() == 10)
						monthName = "November";
					else if (date.getMonth() == 11)
						monthName = "December";
					String datetime = new SimpleDateFormat("ddMMMyyyy_hh_mm_ssa").format(new java.util.Date());
					// setFileFileName(empId+"_"+datetime+"_"+
					// getFileFileName());
					String generatedPath = path + "/" + monthName;
					File geneartedDir = new File(generatedPath);
					if (!geneartedDir.exists())
						geneartedDir.mkdirs();

					File targetDirectory = new File(generatedPath + "/" + datetime + "_" + getUploadFileName());
					// System.out.println("targetDirectory"+targetDirectory);
					// setAttachmentLocation(targetDirectory.toString());
					FileUtils.copyFile(getUpload(), targetDirectory);

					row = ServiceLocator.getEmployeeService().starPerformerMovieTkt(getStarId(),
							targetDirectory.toString(), loginId);
					if (row > 0) {
						// String
						// res=MailManager.sendStarPerformerTickets(targetDirectory.toString(),getUploadFileName());

						setResultMessage("<font color=\"green\" size=\"2\">Ticket successfully uploded!</font>");
					} else {
						setResultMessage("<font color=\"red\" size=\"2\">Plese try again</font>");
						httpServletRequest.setAttribute(ApplicationConstants.RESULT_MSG, getResultMessage());

					}
				} else {
					setResultMessage("<font color=\"red\" size=\"2\">Plese try again</font>");
					httpServletRequest.setAttribute(ApplicationConstants.RESULT_MSG, getResultMessage());

				}

				resultType = SUCCESS;
			} catch (Exception ex) {
				ex.printStackTrace();
				// List errorMsgList = ExceptionToListUtility.errorMessages(ex);
				httpServletRequest.getSession(false).setAttribute("errorMessage", ex.toString());
				resultType = ERROR;
			}
			// END-Authorization Checking
		} // Close Session Checking
		return resultType;
	}

	public String getAllTickets() {
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;
		try {
			resultType = INPUT;
			// System.out.println("getstarid0()---->"+getStarId());
			List files = ServiceLocator.getEmployeeService().getAllTickets(getStarId());
			// System.out.println("location is---->"+location);

			// getHttpServletResponse().setContentType("application/force-download");

			getHttpServletResponse().setContentType("application/zip");
			getHttpServletResponse().setHeader("Content-Disposition", "attachment; filename=tickets.zip");

			outputStream = getHttpServletResponse().getOutputStream();

			ZipOutputStream zos = new ZipOutputStream(new BufferedOutputStream(getOutputStream()));
			// System.out.println("files.size()"+files.size());
			for (int i = 0; i < files.size(); i++) {
				// System.out.println("vvvvvvvvvvvvvvvvvvvvvvvvvvvvvv");
				File filee = new File((String) files.get(i));
				// System.out.println("Adding " + filee.getName());
				zos.putNextEntry(new ZipEntry(filee.getName()));

				// Get the file
				FileInputStream fis = null;
				try {
					fis = new FileInputStream(filee);

				} catch (FileNotFoundException fnfe) {
					// If the file does not exists, write an error entry instead
					// of
					// file
					// contents
					zos.write(("ERRORld not find file " + filee.getName()).getBytes());
					zos.closeEntry();

					continue;
				}

				BufferedInputStream fif = new BufferedInputStream(fis);

				// Write the contents of the file
				int data = 0;
				while ((data = fif.read()) != -1) {
					zos.write(data);
				}
				fif.close();

				zos.closeEntry();
				// System.out.println("Finishedng file " + filee.getName());
			}

			zos.close();

		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} finally {
			try {
				if (getInputStream() != null) {
					getInputStream().close();
					getOutputStream().close();
				}
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		// System.out.println("resultType---->"+resultType);
		return resultType;
	}

	public String reSendStarperformerTicket() {
		// System.out.println("in starPerformerMovieTickets ");
		resultType = LOGIN;
		Map spdetailsMap=new HashMap();

		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			userRoleId = Integer.parseInt(
					httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_ROLE_ID).toString());

			String loginId = httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID)
					.toString();

			try {
				String location = ServiceLocator.getEmployeeService().getStarMovieTicketLocation(getStarId());

				setId(getObjectId());
				int row = 0;

				// System.out.println(getId());
				int status = dataSourceDataProvider.getInstance().getStarPerformerStatus(getObjectId());
				setStatusId(status);
				setSuvyId(getSuvyId());

				// System.out.println("getSuvyId()"+getSuvyId());
				//
				// getUpload();
				// getUploadContentType();
				// getUploadFileName();
				//
				spdetailsMap=DataSourceDataProvider.getInstance().getStarPerformersDetails(getObjectId(),getEmpId());
				
				String empname = (String)spdetailsMap.get("empname");
				String name=empname.replace('.', ' ');
				String email1 = (String)spdetailsMap.get("email1");
	           
				File file = new File(location);
				if (file.exists()) {

				//	String res = MailManager.sendStarPerformerTickets(location.toString(), file.getName(), getStarId());
					String res = MailManager.sendStarPerformerTickets(location.toString(), file.getName(), getStarId(),getEmpId(),name,email1,getObjectId());
					if (res.equals("success")) {

						setResultMessage("<font color=\"green\" size=\"2\">mail successfully sent!</font>");
						httpServletRequest.setAttribute(ApplicationConstants.RESULT_MSG, getResultMessage());
						int count = dataSourceDataProvider.getInstance().doUpdateIsMovieTicketSent(getStarId());
						// System.out.println("resultMessage
						// is----->"+getResultMessage());
					}
					// setObjectType("Emp Reviews");
					else {
						setResultMessage("<font color=\"red\" size=\"2\">Plese try again</font>");
						httpServletRequest.setAttribute(ApplicationConstants.RESULT_MSG, getResultMessage());

					}
				}

				resultType = SUCCESS;
			} catch (Exception ex) {
				ex.printStackTrace();
				// List errorMsgList = ExceptionToListUtility.errorMessages(ex);
				httpServletRequest.getSession(false).setAttribute("errorMessage", ex.toString());
				resultType = ERROR;
			}
			// END-Authorization Checking
		} // Close Session Checking
		return resultType;
	}

	public String downloadStarPerformerTkts() {

		try {
			resultType = "accessFailed";
			// System.out.println("getstarid0()---->"+getStarId());
			String location = ServiceLocator.getEmployeeService().getStarPerformerLocation1(getStarId(), getId());
			// System.out.println("location is---->"+location);

			getHttpServletResponse().setContentType("application/force-download");

			if (location != null && !"".equals(location)) {
				File file = new File(location);
				if (file.exists()) {
					inputStream = new FileInputStream(file);
					outputStream = getHttpServletResponse().getOutputStream();
					getHttpServletResponse().setHeader("Content-Disposition",
							"attachment; filename=\"" + file.getName() + "\"");
					int noOfBytesRead = 0;
					byte[] byteArray = null;
					while (true) {
						byteArray = new byte[1024];
						noOfBytesRead = getInputStream().read(byteArray);
						if (noOfBytesRead == -1)
							break;
						getOutputStream().write(byteArray, 0, noOfBytesRead);
					}
					resultType = null;
				} else {
					resultType = "accessFailed";
				}
			} else {
				resultType = "accessFailed";
			}

		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} finally {
			try {
				if (getInputStream() != null) {
					getInputStream().close();
					getOutputStream().close();
				}
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		// System.out.println("resultType---->"+resultType);

		return resultType;
	}

	public String downloadStarPerformerTicket() {
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;
		try {
			resultType = INPUT;
			// System.out.println("getstarid0()---->" + getStarId());
			String location = ServiceLocator.getEmployeeService().getStarMovieTicketLocation(getStarId());
			// System.out.println("location is---->" + location);

			getHttpServletResponse().setContentType("application/force-download");

			File file = new File(location);
			if (file.exists()) {
				inputStream = new FileInputStream(file);
				outputStream = getHttpServletResponse().getOutputStream();
				getHttpServletResponse().setHeader("Content-Disposition",
						"attachment; filename=\"" + file.getName() + "\"");
				int noOfBytesRead = 0;
				byte[] byteArray = null;
				while (true) {
					byteArray = new byte[1024];
					noOfBytesRead = getInputStream().read(byteArray);
					if (noOfBytesRead == -1)
						break;
					getOutputStream().write(byteArray, 0, noOfBytesRead);
				}

			} else {
				resultType = INPUT;
			}

		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} finally {
			try {
				if (getInputStream() != null) {
					getInputStream().close();
					getOutputStream().close();
				}
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		// System.out.println("resultType---->" + resultType);
		return resultType;
	}
	public String downloadStarPerformerSocialTile() throws URISyntaxException {
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;
		String filePath="";
		int randomNumber=0;
		String outputpath="";
		ResultSet rs=null;
		Map spdetailsMap = new HashMap();
		try {
			resultType = INPUT;
			// System.out.println("getTitleTypeId()---->" +getTitleTypeId());
			String location = ServiceLocator.getEmployeeService().getStarMovieTicketLocation(getStarId());
			// System.out.println("location is---->" + location);
			
			
				int tempStarId= getStarId();
				
				while(tempStarId>44) {
					tempStarId = tempStarId -43;
					
					
				}
				spdetailsMap=DataSourceDataProvider.getInstance().getStarPerformersDetails(getObjectId(),getEmpId());
			
				String empname = (String)spdetailsMap.get("empname");
				//System.out.println("empname"+empname);
				String loginId =(String)spdetailsMap.get("loginId");
				String email1 = (String)spdetailsMap.get("email1");
	            String month =(String)spdetailsMap.get("month");
	             String year = (String)spdetailsMap.get("year");
				String departmentId =(String)spdetailsMap.get("departmentId");
				
				String gender =(String)spdetailsMap.get("gender");
				String titleTypeId = (String)spdetailsMap.get("titleTypeId");
				
				String name=empname.replace('.', ' ');
				String tempQ="Q"+tempStarId;
				
				String quote=com.mss.mirage.util.Properties.getProperty(tempQ);
			
			
			MergeImages mergeImages1=new MergeImages();
			
			 File file1 = new File(Properties.getProperty("Emp.OutputImage.Path") +"/" +year+ "/" +month+"/");
				
			 if(!file1.exists())
			 {
				 file1.mkdirs();
			 }
			 
			 File output=new File(file1.getAbsolutePath()+"/"+name+".jpg");
					 
			 
			
			File input = new File(Properties.getProperty("Emp.InputImage.Path") +"/" +month+".jpg");
			outputpath=Properties.getProperty("Emp.OutputImage.Path")+"/" +year+ "/" +month+"/"+name+".jpg";
			mergeImages1.mergeImages(name,month,departmentId,loginId,email1,input,output,quote,gender,titleTypeId);
			getHttpServletResponse().setContentType("application/force-download");

			File file = new File(outputpath);
			//System.out.println("file.getName()"+file.getName());
			if (file.exists()) {
				inputStream = new FileInputStream(file);
				outputStream = getHttpServletResponse().getOutputStream();
				getHttpServletResponse().setHeader("Content-Disposition",
						"attachment; filename=\"" + file.getName() + "\"");
				int noOfBytesRead = 0;
				byte[] byteArray = null;
				while (true) {
					byteArray = new byte[1024];
					noOfBytesRead = getInputStream().read(byteArray);
					if (noOfBytesRead == -1)
						break;
					getOutputStream().write(byteArray, 0, noOfBytesRead);
				}

			} else {
				resultType = INPUT;
			}

		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		} catch (ServiceLocatorException ex) {
			ex.printStackTrace();
		} finally {
			try {
				if (getInputStream() != null) {
					getInputStream().close();
					getOutputStream().close();
				}
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		// System.out.println("resultType---->" + resultType);
		return resultType;
	}

	/* Star Performer Associated Methods End */
	/* Nagalakshmi Telluri */
	/* 5/17/2017 */

	public String gettotalweekcountbyproject() {

		resultType = LOGIN;
		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			userRoleId = Integer.parseInt(
					httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_ROLE_ID).toString());
			// System.out.println("objectId-->"+objectId);
			resultType = "accessFailed";
			// if
			// (AuthorizationManager.getInstance().isAuthorizedUser("GET_PROJECT_DASH_BOARD",
			// userRoleId)) {
			try {
				setCustomerId(customerId);

				setCustomerName(customerName);
				setProjectName(projectName);
				setYear(year);
				setMonth(month);
				setMonthName(DateUtility.getInstance().getMonthNameByMonthNumber(month));
				setWeek(week);
				setProjectId(projectId);
				setPracticeId(practiceId);
				setCostModel(getCostModel());
				resultType = SUCCESS;
			} catch (Exception ex) {
				httpServletRequest.getSession(false).setAttribute("errorMessage", ex.toString());
				resultType = ERROR;
			}

			// }//END-Authorization Checking
		} // Close Session Checking
		resultType = SUCCESS;
		return resultType;
	}

	public String getAuthorization() {
		resultType = LOGIN;
		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			int isManager = Integer.parseInt(httpServletRequest.getSession(false)
					.getAttribute(ApplicationConstants.SESSION_IS_USER_MANAGER).toString());
			int isTeamLead = Integer.parseInt(httpServletRequest.getSession(false)
					.getAttribute(ApplicationConstants.SESSION_IS_TEAM_LEAD).toString());
			int rUtil = Integer.parseInt(httpServletRequest.getSession(false)
					.getAttribute(ApplicationConstants.RESOURCE_UTILIZATION).toString());
			System.out.println("ism,ist,isutil" + isManager + isTeamLead + rUtil);
			resultType = "accessFailed";
			if (isManager == 1 || isTeamLead == 1 || rUtil == 1) {

				resultType = SUCCESS;

				// }//END-Authorization Checking
			} // Close Session Checking

		}
		return resultType;
	}

	public String mDrive() {
		resultType = LOGIN;
		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			String isFlag = httpServletRequest.getSession(false).getAttribute(ApplicationConstants.ISPRE_SALES)
					.toString();

			if (isFlag != null && "1".equals(isFlag)) {

				resultType = SUCCESS;

				// }//END-Authorization Checking
			} else {

				resultType = "accessFailed";
			} // Close Session Checking

		}
		return resultType;
	}

	public String getEmpTransfers() {

		// System.out.println("I am in getSearchQuery() +getOrgId()");

		// System.out.println("getEmpTransfers.."+getTransferType());
		resultType = LOGIN;

		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			setYear(Calendar.getInstance().get(Calendar.YEAR));
			setMonth(Calendar.getInstance().get(Calendar.MONTH) + 1);
			String userId = httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID)
					.toString();
			int isManager = Integer.parseInt(httpServletRequest.getSession(false)
					.getAttribute(ApplicationConstants.SESSION_IS_USER_MANAGER).toString());

			int isAdmin = Integer.parseInt(httpServletRequest.getSession(false)
					.getAttribute(ApplicationConstants.SESSION_IS_ADMIN_ACCESS).toString());

			String practice = (httpServletRequest.getSession(false)
					.getAttribute(ApplicationConstants.SESSION_EMP_PRACTICE).toString());
			String dept = (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_MY_DEPT_ID)
					.toString());
			String empId = httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_EMP_ID)
					.toString();
			String Country = (String) httpServletRequest.getSession(false)
					.getAttribute(ApplicationConstants.Living_COUNTRY);
			// System.out.println("role----.." +
			// (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_ROLE_NAME)
			// .toString().equalsIgnoreCase("Operations")));
			try {
				setDepartmentIdList(
						HibernateDataProvider.getInstance().getDepartment(ApplicationConstants.DEPARTMENT_OPTION));

				hibernateDataProvider = HibernateDataProvider.getInstance();
				setCountryList(hibernateDataProvider.getContries(ApplicationConstants.COUNTRY_OPTIONS));

				if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_ROLE_NAME).toString()
						.equalsIgnoreCase("Operations")) {
					queryStringBuffer = new StringBuilder();

					if (Country.equals("USA")) {
						

						queryStringBuffer
								.append("SELECT tblEmpLocationTransferDetails.Id,EmpId,tblEmployee.EmpNo,EmpName,STATUS,FromLocation,ToLocation,InitiatedOn,TentativeReportedDate,ActualReportedDate FROM tblEmpLocationTransferDetails LEFT JOIN tblEmployee ON (tblEmpLocationTransferDetails.EmpId = tblEmployee.Id) WHERE MONTH(tblEmpLocationTransferDetails.InitiatedOn) = "
										+ getMonth() + " AND YEAR(tblEmpLocationTransferDetails.InitiatedOn) = "
										+ getYear() + "  ");
					} else {
						
						queryStringBuffer
								.append("SELECT tblEmpLocationTransferDetails.Id,EmpId,tblEmployee.EmpNo,EmpName,TransferType,STATUS,FromLocation,ToLocation,InitiatedOn,TentativeReportedDate,ActualReportedDate FROM tblEmpLocationTransferDetails LEFT JOIN tblEmployee ON (tblEmpLocationTransferDetails.EmpId = tblEmployee.Id) WHERE MONTH(tblEmpLocationTransferDetails.InitiatedOn) = "
										+ getMonth() + " AND YEAR(tblEmpLocationTransferDetails.InitiatedOn) = "
										+ getYear() + "  ");
					}

					if (isManager != 1 && isAdmin != 1 && "Operations".equals(dept) && !"Systems".equals(practice)) {
						queryStringBuffer.append(
								"AND ((ReportsTo OR ReportedTo = '" + userId + "') OR OpsContactId = '" + empId + "')");
					}
				} else if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_ROLE_NAME)
						.toString().equalsIgnoreCase("Employee")) {
					queryStringBuffer = new StringBuilder();

					queryStringBuffer
							.append("SELECT EmpId,Id,STATUS,FromLocation,ToLocation,InitiatedOn,TentativeReportedDate,ActualReportedDate FROM tblEmpLocationTransferDetails where EmpId = "
									+ empId);

					if (getMonth() != 0) {

						queryStringBuffer
								.append(" AND MONTH(tblEmpLocationTransferDetails.InitiatedOn) = " + getMonth() + " ");

					}
					if (getYear() != 0) {
						queryStringBuffer
								.append("  AND YEAR(tblEmpLocationTransferDetails.InitiatedOn) = " + getYear() + "  ");

					}

				}
				httpServletRequest.setAttribute(ApplicationConstants.EMP_REVIEWS_LIST, queryStringBuffer.toString());
			//	System.out.println("queryStringBuffer.toString() transfers.." + queryStringBuffer.toString());

				// queryStringBuffer.delete(0, queryStringBuffer.capacity());

				// }
				resultType = SUCCESS;
			} catch (Exception ex) {
				// List errorMsgList =
				// ExceptionToListUtility.errorMessages(ex);
				httpServletRequest.getSession(false).setAttribute("errorMessage", ex.toString());
				resultType = ERROR;
			}
			// END-Authorization Checking
		} // Close Session Checking
			// System.err.println("resultType"+resultType);
		return resultType;
	}

	public String getEmpTransferSearch() {

		
		resultType = LOGIN;

		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			// System.out.println("role---------.." +
			// (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_ROLE_NAME)
			// .toString().equalsIgnoreCase("Operations")));
			String userId = httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID)
					.toString();
			int isManager = Integer.parseInt(httpServletRequest.getSession(false)
					.getAttribute(ApplicationConstants.SESSION_IS_USER_MANAGER).toString());

			int isAdmin = Integer.parseInt(httpServletRequest.getSession(false)
					.getAttribute(ApplicationConstants.SESSION_IS_ADMIN_ACCESS).toString());

			String practice = (httpServletRequest.getSession(false)
					.getAttribute(ApplicationConstants.SESSION_EMP_PRACTICE).toString());
			String dept = (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_MY_DEPT_ID)
					.toString());
			String empId = httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_EMP_ID)
					.toString();
			String Country = (String) httpServletRequest.getSession(false)
					.getAttribute(ApplicationConstants.Living_COUNTRY);

			int temp = 0;
			try {
				hibernateDataProvider = HibernateDataProvider.getInstance();
		
				setDepartmentIdList(
						HibernateDataProvider.getInstance().getDepartment(ApplicationConstants.DEPARTMENT_OPTION));

				queryStringBuffer = new StringBuilder();
				if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_ROLE_NAME).toString()
						.equalsIgnoreCase("Operations")) {

					if (Country.equals("USA")) {

						
						queryStringBuffer.append(
								"SELECT tblEmpLocationTransferDetails.Id,EmpId,tblEmployee.EmpNo,EmpName,STATUS,FromLocation,ToLocation,InitiatedOn,TentativeReportedDate,ActualReportedDate FROM tblEmpLocationTransferDetails LEFT JOIN tblEmployee ON (tblEmpLocationTransferDetails.EmpId = tblEmployee.Id) ");

					} else {

						//System.out.println("usa in if12222");
						queryStringBuffer.append(
								"SELECT tblEmpLocationTransferDetails.Id,EmpId,tblEmployee.EmpNo,EmpName,TransferType,STATUS,FromLocation,ToLocation,InitiatedOn,TentativeReportedDate,ActualReportedDate FROM tblEmpLocationTransferDetails LEFT JOIN tblEmployee ON (tblEmpLocationTransferDetails.EmpId = tblEmployee.Id) ");

					}

					if (isManager != 1 && isAdmin != 1 && "Operations".equals(dept) && !"Systems".equals(practice)) {
						queryStringBuffer.append(" WHERE ((ReportsTo OR ReportedTo = '" + userId
								+ "') OR OpsContactId = '" + empId + "')");
						temp = 1;
					}

					if (getMonth() != 0 && temp != 1) {
						queryStringBuffer.append(
								" WHERE MONTH(tblEmpLocationTransferDetails.InitiatedOn) = " + getMonth() + " ");
						temp = 1;
					} else if (getMonth() != 0 && temp == 1) {
						queryStringBuffer
								.append(" AND MONTH(tblEmpLocationTransferDetails.InitiatedOn) = " + getMonth() + " ");

					}
					if (getYear() != 0 && temp == 1) {
						queryStringBuffer
								.append("  AND YEAR(tblEmpLocationTransferDetails.InitiatedOn) = " + getYear() + "  ");

					} else if (getYear() != 0 && temp != 1) {
						queryStringBuffer.append(
								"  WHERE YEAR(tblEmpLocationTransferDetails.InitiatedOn) = " + getYear() + "  ");
						temp = 1;
					}

					if (!getDepartmentId().equals("") && getDepartmentId() != null) {
						queryStringBuffer.append(
								" AND tblEmpLocationTransferDetails.DepartmentId = '" + getDepartmentId() + "'");
					}
					if (!getStatus().equals("") && getStatus() != null) {
						queryStringBuffer.append(" AND tblEmpLocationTransferDetails.Status = '" + getStatus() + "'");
					}

					if (!Country.equals("USA")) {

						if (!getTransferType().equals("") && getTransferType() != null) {
							queryStringBuffer.append(
									"  AND tblEmpLocationTransferDetails.TransferType = '" + getTransferType() + "'");
						}
					}

				} else if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_ROLE_NAME)
						.toString().equalsIgnoreCase("Employee")) {

					queryStringBuffer
							.append("SELECT EmpId,Id,STATUS,FromLocation,ToLocation,InitiatedOn,TentativeReportedDate,ActualReportedDate FROM tblEmpLocationTransferDetails where EmpId = "
									+ empId);

					if (getMonth() != 0) {

						queryStringBuffer
								.append(" AND MONTH(tblEmpLocationTransferDetails.InitiatedOn) = " + getMonth() + " ");

					}
					if (getYear() != 0) {
						queryStringBuffer
								.append("  AND YEAR(tblEmpLocationTransferDetails.InitiatedOn) = " + getYear() + "  ");

					}
					
				}

				httpServletRequest.setAttribute(ApplicationConstants.EMP_REVIEWS_LIST, queryStringBuffer.toString());
				

				// queryStringBuffer.delete(0, queryStringBuffer.capacity());

				// }
				resultType = SUCCESS;
			} catch (Exception ex) {
				// List errorMsgList =
				// ExceptionToListUtility.errorMessages(ex);
				httpServletRequest.getSession(false).setAttribute("errorMessage", ex.toString());
				resultType = ERROR;
			}
			// END-Authorization Checking
		} // Close Session Checking
			// System.err.println("resultType"+resultType);
		return resultType;
	}

	public String getEmpITTransfers() {

		// System.out.println("I am in getSearchQuery() +getOrgId()");
		resultType = LOGIN;

		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_EMP_PRACTICE).toString()
					.equalsIgnoreCase("Systems")
					|| (Integer.parseInt(httpServletRequest.getSession(false)
							.getAttribute(ApplicationConstants.SESSION_IS_ADMIN_ACCESS).toString()) == 1)) {
				setYear(Calendar.getInstance().get(Calendar.YEAR));
				setMonth(Calendar.getInstance().get(Calendar.MONTH) + 1);
				String userId = httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID)
						.toString();
				int isManager = Integer.parseInt(httpServletRequest.getSession(false)
						.getAttribute(ApplicationConstants.SESSION_IS_USER_MANAGER).toString());

				int isAdmin = Integer.parseInt(httpServletRequest.getSession(false)
						.getAttribute(ApplicationConstants.SESSION_IS_ADMIN_ACCESS).toString());

				String practice = (httpServletRequest.getSession(false)
						.getAttribute(ApplicationConstants.SESSION_EMP_PRACTICE).toString());
				String dept = (httpServletRequest.getSession(false)
						.getAttribute(ApplicationConstants.SESSION_MY_DEPT_ID).toString());
				String empId = httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_EMP_ID)
						.toString();
				// System.out.println("role----.." +
				// (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_ROLE_NAME)
				// .toString().equalsIgnoreCase("Operations")));
				try {
					setDepartmentIdList(
							HibernateDataProvider.getInstance().getDepartment(ApplicationConstants.DEPARTMENT_OPTION));

					queryStringBuffer = new StringBuilder();

					queryStringBuffer.append(
							"SELECT tblEmpLocationTransferDetails.Id,EmpId,tblEmployee.EmpNo,EmpName,STATUS,FromLocation,ToLocation,InitiatedOn,TentativeReportedDate,ActualReportedDate FROM tblEmpLocationTransferDetails LEFT JOIN tblEmployee ON (tblEmpLocationTransferDetails.EmpId = tblEmployee.Id) WHERE 1=1");

					if (getMonth() != 0) {

						queryStringBuffer
								.append(" AND MONTH(tblEmpLocationTransferDetails.InitiatedOn) = " + getMonth() + " ");

					}
					if (getYear() != 0) {
						queryStringBuffer
								.append("  AND YEAR(tblEmpLocationTransferDetails.InitiatedOn) = " + getYear() + "  ");

					}

					httpServletRequest.setAttribute(ApplicationConstants.EMP_REVIEWS_LIST,
							queryStringBuffer.toString());
					// System.out.println("queryStringBuffer.toString().." +
					// queryStringBuffer.toString());

					// queryStringBuffer.delete(0,
					// queryStringBuffer.capacity());

					// }
					resultType = SUCCESS;
				} catch (Exception ex) {
					// List errorMsgList =
					// ExceptionToListUtility.errorMessages(ex);
					httpServletRequest.getSession(false).setAttribute("errorMessage", ex.toString());
					resultType = ERROR;
				}
				// END-Authorization Checking
			}
		} // Close Session Checking
			// System.err.println("resultType"+resultType);
		return resultType;
	}

	public String getEmpITTransferSearch() {

		// System.out.println("I am in getSearchQuery() "+getOrgId());
		resultType = LOGIN;

		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_EMP_PRACTICE).toString()
					.equalsIgnoreCase("Systems")
					|| (Integer.parseInt(httpServletRequest.getSession(false)
							.getAttribute(ApplicationConstants.SESSION_IS_ADMIN_ACCESS).toString()) == 1)) {

				// System.out.println("role---------.." +
				// (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_ROLE_NAME)
				// .toString().equalsIgnoreCase("Operations")));
				String userId = httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID)
						.toString();
				int isManager = Integer.parseInt(httpServletRequest.getSession(false)
						.getAttribute(ApplicationConstants.SESSION_IS_USER_MANAGER).toString());

				int isAdmin = Integer.parseInt(httpServletRequest.getSession(false)
						.getAttribute(ApplicationConstants.SESSION_IS_ADMIN_ACCESS).toString());

				String practice = (httpServletRequest.getSession(false)
						.getAttribute(ApplicationConstants.SESSION_EMP_PRACTICE).toString());
				String dept = (httpServletRequest.getSession(false)
						.getAttribute(ApplicationConstants.SESSION_MY_DEPT_ID).toString());
				String empId = httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_EMP_ID)
						.toString();
				int temp = 0;
				try {

					setDepartmentIdList(
							HibernateDataProvider.getInstance().getDepartment(ApplicationConstants.DEPARTMENT_OPTION));

					queryStringBuffer = new StringBuilder();

					queryStringBuffer.append(
							"SELECT tblEmpLocationTransferDetails.Id,EmpId,tblEmployee.EmpNo,EmpName,STATUS,FromLocation,ToLocation,InitiatedOn,TentativeReportedDate,ActualReportedDate FROM tblEmpLocationTransferDetails LEFT JOIN tblEmployee ON (tblEmpLocationTransferDetails.EmpId = tblEmployee.Id) WHERE 1=1");

					if (getMonth() != 0) {

						queryStringBuffer
								.append(" AND MONTH(tblEmpLocationTransferDetails.InitiatedOn) = " + getMonth() + " ");

					}
					if (getYear() != 0) {
						queryStringBuffer
								.append("  AND YEAR(tblEmpLocationTransferDetails.InitiatedOn) = " + getYear() + "  ");

					}
					if (!getDepartmentId().equals("") && getDepartmentId() != null) {
						queryStringBuffer.append(
								" AND tblEmpLocationTransferDetails.DepartmentId = '" + getDepartmentId() + "'");
					}
					if (!getItTeamStatus().equals("") && getItTeamStatus() != null) {
						queryStringBuffer.append(
								" AND tblEmpLocationTransferDetails.ITTeamStatus = '" + getItTeamStatus() + "'");
					}

					httpServletRequest.setAttribute(ApplicationConstants.EMP_REVIEWS_LIST,
							queryStringBuffer.toString());
					// System.out.println("queryStringBuffer.toString().." +
					// queryStringBuffer.toString());

					// queryStringBuffer.delete(0,
					// queryStringBuffer.capacity());

					// }
					resultType = SUCCESS;
				} catch (Exception ex) {
					// List errorMsgList =
					// ExceptionToListUtility.errorMessages(ex);
					httpServletRequest.getSession(false).setAttribute("errorMessage", ex.toString());
					resultType = ERROR;
				}
				// END-Authorization Checking
			} // Close Session Checking
		} // System.err.println("resultType"+resultType);
		return resultType;
	}

	public String getEmpITTransferDetails() throws ServiceLocatorException {
		// System.out.println("getEmpTransferDetails----");
		resultType = LOGIN;
		String result = "";
		List empExpencesDetails = new ArrayList();

		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_EMP_PRACTICE).toString()
					.equalsIgnoreCase("Systems")
					|| (Integer.parseInt(httpServletRequest.getSession(false)
							.getAttribute(ApplicationConstants.SESSION_IS_ADMIN_ACCESS).toString()) == 1)) {
				resultType = "accessFailed";
				String empId = httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_EMP_ID)
						.toString();
				String Country = (String) httpServletRequest.getSession(false)
						.getAttribute(ApplicationConstants.Living_COUNTRY);
				String roleName = httpServletRequest.getSession(false)
						.getAttribute(ApplicationConstants.SESSION_ROLE_NAME).toString();
				setTransportLocationList(DataSourceDataProvider.getInstance().getTransportLocationList());
				setAccommodationList(DataSourceDataProvider.getInstance().getAccommodationList());

				setOpsContactIdMap(DataSourceDataProvider.getInstance().getTaskEmpDetailsBasedOnIssueRel("0"));
				setEmpLocationMap(DataSourceDataProvider.getInstance().getEmployeeLocationsList(Country));
				// setFlag(getFlag());
				// setTitleIdList(DataSourceDataProvider.getInstance().getTitleTypeByDepartment(getDepartmentId()));

				// if
				// (AuthorizationManager.getInstance().isAuthorizedUser("GET_EMPLOYEE",
				// userRoleId)) {
				try {
					// resultType = INPUT;
					result = ServiceLocator.getEmployeeService().getEmpTransferDetails(getEmpId(), this);

					// System.out.println("status--->"+getStatus());
					setStatus(getStatus());
					setFromLocation(getFromLocation());
					setToLocation(getToLocation());
					if (getItTeamStatus() != null && !getItTeamStatus().equals("")) {
						setItTeamStatus(getItTeamStatus());
					}
					setIsNextLocationHR(
							DataSourceDataProvider.getInstance()
									.getIfIsNextLocationHR(getEmpId(),
											(httpServletRequest.getSession(false)
													.getAttribute(ApplicationConstants.SESSION_USER_ID).toString()),
											getId()));
					setCurrentAction("updateEmpITTransfers");

					resultType = SUCCESS;
				} catch (Exception ex) {
					// List errorMsgList =
					// ExceptionToListUtility.errorMessages(ex);
					ex.printStackTrace();
					httpServletRequest.getSession(false).setAttribute("errorMessage:", ex.toString());

					resultType = ERROR;
				}
			}
		} // END-Authorization Checking
			// } // Close Session Checking
		return resultType;
	}

	public String updateEmpITTransfers() {
		// System.out.println("updateEmpTransfers");
		resultType = LOGIN;

		if (httpServletRequest.getSession(false) != null
				&& httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {

			String userId = httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID)
					.toString();
			String practice = (httpServletRequest.getSession(false)
					.getAttribute(ApplicationConstants.SESSION_EMP_PRACTICE).toString());
			String dept = (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_MY_DEPT_ID)
					.toString());
			String mailFlag = "";
			try {
				// System.out.println("shortTermGoalComments===" +
				// getShortTermGoalComments());
				// resultType = INPUT;
				// System.out.println("status--in update->"+getStatus());

				mailFlag = "ITTeam";

				String result = ServiceLocator.getEmployeeService().updateEmpITTransfers(this, getEmpId(), userId,
						mailFlag);

				// System.out.println("result===" + result +
				// "status====="+getStatus());
				if (result.equals("Updated")) {

					resultMessage = "<font color=\"green\" size=\"2.5\">Employee Tranfer Form " + result
							+ " successfully!</font>";
					httpServletRequest.getSession().setAttribute(ApplicationConstants.RESULT_MSG, resultMessage);
				} else {
					resultMessage = "<font color=\"red\" size=\"2.5\">please try again later!</font>";
					httpServletRequest.getSession().setAttribute(ApplicationConstants.RESULT_MSG, resultMessage);

				}
				resultType = SUCCESS;
				/*
				 * if (getCurretRole().equals("team")) { resultType = "team"; }
				 */
			} catch (Exception ex) {
				// List errorMsgList =
				// ExceptionToListUtility.errorMessages(ex);
				// httpServletRequest.getSession(false).setAttribute("errorMessage",
				// ex.toString());
				// ex.printStackTrace();
				// resultType = ERROR;
				ex.printStackTrace();
				httpServletRequest.getSession(false).setAttribute("errorMessage:", ex.toString());

				resultType = ERROR;
			}
			// } // END-Authorization Checking
		} // Close Session Checking
		return resultType;
	}

	public String getEmpTransferDetails() throws ServiceLocatorException {

		resultType = LOGIN;
		String result = "";
		List empExpencesDetails = new ArrayList();

		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			resultType = "accessFailed";
			String empId = httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_EMP_ID)
					.toString();
			String Country = (String) httpServletRequest.getSession(false)
					.getAttribute(ApplicationConstants.Living_COUNTRY);

		

			String roleName = httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_ROLE_NAME)
					.toString();
			setTransportLocationList(DataSourceDataProvider.getInstance().getTransportLocationList());
			setAccommodationList(DataSourceDataProvider.getInstance().getAccommodationList());

			setOpsContactIdMap(DataSourceDataProvider.getInstance().getTaskEmpDetailsBasedOnIssueRel("0"));
			setOpsContactIdMapNew(DataSourceDataProvider.getInstance().getTaskEmpDetailsBasedOnIssueRelNew("0"));

			setEmpLocationMap(DataSourceDataProvider.getInstance().getEmployeeLocationsList(Country));

			if (Country.equals("USA")) {
				setTransferType("CountryTransfer");
			}
			// setFlag(getFlag());
			// setTitleIdList(DataSourceDataProvider.getInstance().getTitleTypeByDepartment(getDepartmentId()));

			// if
			// (AuthorizationManager.getInstance().isAuthorizedUser("GET_EMPLOYEE",
			// userRoleId)) {
			try {
				// resultType = INPUT;

				result = ServiceLocator.getEmployeeService().getEmpTransferDetails(getEmpId(), this);
				// empExpencesDetails =
				// DataSourceDataProvider.getInstance().getEmpExpencesThroughEmpId();
				// System.out.println("--->empExpencesDetails--->"+empExpencesDetails);
				// if(empExpencesDetails.get(0).equals("NoData")){
				// setAccommodation("");
				// setRoomNo(0);
				// setRoomFee(0.00);
				// setCafeteria("");
				// setCafeteriaFee(0.00);
				// setTransportation("");
				// setTransportLocation("");
				// setTransportFee(0.00);
				// setOccupancyType("");
				// setDateOfOccupancy("");
				// setElectricalCharges(0.00);
				// setFromLocation("");
				// }
				// else{
				// setAccommodation((String) empExpencesDetails.get(0));
				// setRoomNo((Integer) empExpencesDetails.get(1));
				// setRoomFee((Double) empExpencesDetails.get(2));
				// setCafeteria((String) empExpencesDetails.get(3));
				// setCafeteriaFee((Double) empExpencesDetails.get(4));
				// setTransportation((String) empExpencesDetails.get(5));
				// setTransportLocation((String) empExpencesDetails.get(6));
				// setTransportFee((Double) empExpencesDetails.get(7));
				// setOccupancyType((String) empExpencesDetails.get(8));
				// setDateOfOccupancy((String) empExpencesDetails.get(9));
				// setElectricalCharges((Double) empExpencesDetails.get(10));
				// setFromLocation((String) empExpencesDetails.get(11));
				// }
				//
				if (result.equals("NoData")) {
					setCurrentAction("addEmpTransfers");
					String initiated = (DateUtility.getInstance().getCurrentSQLDate());
					setStatus("Initiated");
					setInitiatedOn(DateUtility.getInstance().convertToviewFormat(initiated));
					setIsNextLocationHR(0);
				} else {
					// System.out.println("status--->"+getStatus());
					setStatus(getStatus());
					setFromLocation(getFromLocation());
					setToLocation(getToLocation());
					if (getItTeamStatus() != null && !getItTeamStatus().equals("")) {
						setItTeamStatus(getItTeamStatus());
					}
					setIsNextLocationHR(
							DataSourceDataProvider.getInstance()
									.getIfIsNextLocationHR(getEmpId(),
											(httpServletRequest.getSession(false)
													.getAttribute(ApplicationConstants.SESSION_USER_ID).toString()),
											getId()));
					setCurrentAction("updateEmpTransfers");
				}

				resultType = SUCCESS;
			} catch (Exception ex) {
				// List errorMsgList =
				// ExceptionToListUtility.errorMessages(ex);
				ex.printStackTrace();
				httpServletRequest.getSession(false).setAttribute("errorMessage:", ex.toString());

				resultType = ERROR;
			}
		} // END-Authorization Checking
			// } // Close Session Checking
		return resultType;
	}

	public String addEmpTransfers() {
		
		resultType = LOGIN;

		if (httpServletRequest.getSession(false) != null
				&& httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {

			String userId = httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID)
					.toString();

			try {
				setStatus("InProgress");
				// System.out.println("shortTermGoalComments===" +
				// getShortTermGoalComments());
				// resultType = INPUT;

				// String empNo =
				// DataSourceDataProvider.getInstance().getEmpNoByEmpId(getEmpId());

				// System.out.println("empNo"+empNo+"helloo"+getEmpId());

				String result = ServiceLocator.getEmployeeService().addEmpTransfers(this, getEmpId(), userId,
						getTransferType(), getEmpno());

				// System.out.println("result===" + result +
				// "status====="+getStatus());
				if (result.equals("Added")) {

					resultMessage = "<font color=\"green\" size=\"2.5\">Employee Tranfer Form " + result
							+ " successfully!</font>";
					httpServletRequest.getSession().setAttribute(ApplicationConstants.RESULT_MSG, resultMessage);
				} else {
					resultMessage = "<font color=\"red\" size=\"2.5\">please try again later!</font>";
					httpServletRequest.getSession().setAttribute(ApplicationConstants.RESULT_MSG, resultMessage);

				}
				setResultMessage(resultMessage);
				resultType = SUCCESS;
				/*
				 * if (getCurretRole().equals("team")) { resultType = "team"; }
				 */
			} catch (Exception ex) {
				// List errorMsgList =
				// ExceptionToListUtility.errorMessages(ex);
				// httpServletRequest.getSession(false).setAttribute("errorMessage",
				// ex.toString());
				// ex.printStackTrace();
				// resultType = ERROR;
				ex.printStackTrace();
				httpServletRequest.getSession(false).setAttribute("errorMessage:", ex.toString());

				resultType = ERROR;
			}
			// } // END-Authorization Checking
		} // Close Session Checking
		return resultType;
	}

	public String updateEmpTransfers() {
		// System.out.println("updateEmpTransfers");
		resultType = LOGIN;

		if (httpServletRequest.getSession(false) != null
				&& httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {

			String userId = httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID)
					.toString();
			String practice = (httpServletRequest.getSession(false)
					.getAttribute(ApplicationConstants.SESSION_EMP_PRACTICE).toString());
			String dept = (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_MY_DEPT_ID)
					.toString());
			String mailFlag = "";
			try {
				// System.out.println("shortTermGoalComments===" +
				// getShortTermGoalComments());
				// resultType = INPUT;
				// System.out.println("status--in update->"+getStatus());

				if (DataSourceDataProvider.getInstance().getIfIsNextLocationHR(getEmpId(), userId, getId()) == 1) {
					mailFlag = "NextLocHR";
				}
				String result = ServiceLocator.getEmployeeService().updateEmpTransfers(this, getEmpId(), userId,
						mailFlag, getTransferType());

				// System.out.println("result===" + result +
				// "status====="+getStatus());
				if (result.equals("Updated")) {

					resultMessage = "<font color=\"green\" size=\"2.5\">Employee Tranfer Form " + result
							+ " successfully!</font>";
					httpServletRequest.getSession().setAttribute(ApplicationConstants.RESULT_MSG, resultMessage);
				} else {
					resultMessage = "<font color=\"red\" size=\"2.5\">please try again later!</font>";
					httpServletRequest.getSession().setAttribute(ApplicationConstants.RESULT_MSG, resultMessage);

				}
				resultType = SUCCESS;
				/*
				 * if (getCurretRole().equals("team")) { resultType = "team"; }
				 */
			} catch (Exception ex) {
				// List errorMsgList =
				// ExceptionToListUtility.errorMessages(ex);
				// httpServletRequest.getSession(false).setAttribute("errorMessage",
				// ex.toString());
				// ex.printStackTrace();
				// resultType = ERROR;
				ex.printStackTrace();
				httpServletRequest.getSession(false).setAttribute("errorMessage:", ex.toString());

				resultType = ERROR;
			}
			// } // END-Authorization Checking
		} // Close Session Checking
		return resultType;
	}

	public String getEmpHolidays() {

		resultType = LOGIN;

		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			setYear(Calendar.getInstance().get(Calendar.YEAR));

			try {
				queryStringBuffer = new StringBuilder();
				queryStringBuffer.append("SELECT Date,Description,Country FROM tblEmpHolidays ");
				if (getYear() != 0) {

					queryStringBuffer.append(" WHERE Year(Date) = " + getYear() + "");
				}

				httpServletRequest.setAttribute(ApplicationConstants.EMP_REVIEWS_LIST, queryStringBuffer.toString());
				// System.out.println("queryStringBuffer.toString().." +
				// queryStringBuffer.toString());
				resultType = SUCCESS;
			} catch (Exception ex) {

				httpServletRequest.getSession(false).setAttribute("errorMessage", ex.toString());
				resultType = ERROR;
			}

		}
		return resultType;

	}

	public String getEmpHolidaysSearch() {

		resultType = LOGIN;

		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			// setYear(Calendar.getInstance().get(Calendar.YEAR));
			// setMonth(Calendar.getInstance().get(Calendar.MONTH)+1);
			int flag = 0;

			try {
				queryStringBuffer = new StringBuilder();
				queryStringBuffer.append("SELECT Date,Description,Country FROM tblEmpHolidays ");
				if (getYear() != 0) {
					flag = 1;
					queryStringBuffer.append(" WHERE Year(Date) = " + getYear() + "");
				}
				if (!getCountry().equals("0") && getCountry() != null) {
					if (flag == 1) {
						queryStringBuffer.append(" AND Country = '" + getCountry() + "'");
					} else {
						queryStringBuffer.append(" WHERE Country = '" + getCountry() + "'");
					}

				}

				httpServletRequest.setAttribute(ApplicationConstants.EMP_REVIEWS_LIST, queryStringBuffer.toString());
				// System.out.println("queryStringBuffer.toString().." +
				// queryStringBuffer.toString());
				resultType = SUCCESS;
			} catch (Exception ex) {

				httpServletRequest.getSession(false).setAttribute("errorMessage", ex.toString());
				resultType = ERROR;
			}

		}
		return resultType;

	}

	public String getEmpHolidaysList() {

		resultType = LOGIN;

		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			setYear(Calendar.getInstance().get(Calendar.YEAR));
			setCountry("USA");

			try {
				resultType = SUCCESS;
			} catch (Exception ex) {

				httpServletRequest.getSession(false).setAttribute("errorMessage", ex.toString());
				resultType = ERROR;
			}

		}
		return resultType;

	}

	public String doSMCampaignIntegration() {
		resultType = LOGIN;
		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			try {

				int rows = 0;
				rows = ServiceLocator.getEmployeeService().doSMCampaignIntegration(getObjectId(), getMonthName(),
						getYear());
				System.out.println("rows..." + rows);

				if (rows > 0) {

					setResultMessage("<font color=\"green\" size=\"2\">Published successfully !</font>");

				} else {
					setResultMessage("<font color=\"red\" size=\"2\">Plese try again</font>");

				}
				System.out.println(getResultMessage());
				httpServletRequest.setAttribute(ApplicationConstants.RESULT_MSG, getResultMessage());

				resultType = SUCCESS;

			} catch (Exception ex) {
				httpServletRequest.getSession(false).setAttribute("errorMessage", ex.toString());
				resultType = ERROR;
			}
		}
		return resultType;
	}

	public String doGetPracticeStatusReport() throws ServiceLocatorException, ParseException {

		resultType = LOGIN;

		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			hibernateDataProvider = HibernateDataProvider.getInstance();
			setPracticeMap(DataSourceDataProvider.getInstance().getPracticeIdList());
			setPracticeMapGDC(DataSourceDataProvider.getInstance().getPracticeIdListForGDC());

			setClientMap(DataSourceDataProvider.getInstance().getCustomerMap());
			setProjectTypesList(DataSourceDataProvider.getInstance().getProjectTypesList());
			GregorianCalendar cal = new GregorianCalendar();
			int year = cal.get(Calendar.YEAR);
			int month = cal.get(Calendar.MONTH);

			if (getYear() == 0) {
				setYear(year);
			}
			if (getMonth() == 0) {
				setMonth(month + 1);
			}
			setStartDate(DateUtility.getInstance().YearBackFromCurrentDate());
			setEndDate(DateUtility.getInstance().getCurrentMySqlDate());
			cal.add(cal.MONTH, -1);
			String monthBackfromCurrentdate =  DateUtility.getInstance().convertDateToView(cal.getTime());
			
			
			setProjstartDate(monthBackfromCurrentdate);
			setCountryList(hibernateDataProvider.getContries(ApplicationConstants.COUNTRY_OPTIONS));
			
			setDepartmentIdList(hibernateDataProvider.getDepartment(ApplicationConstants.DEPARTMENT_OPTION));
			setPracticeIdList(DataSourceDataProvider.getInstance().getPracticeByDepartment(getDepartmentId()));
			setCountry("India");
			setReportBasedOn("Quarterly");
			
			setDepartmentId("All");

			try {
				resultType = SUCCESS;
			} catch (Exception ex) {

				httpServletRequest.getSession(false).setAttribute("errorMessage", ex.toString());
				resultType = ERROR;
			}

		}
		return resultType;

	}
	
	
	public String getLunchDetails() throws ServiceLocatorException, ParseException {
		resultType = LOGIN;

		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			userRoleId = Integer.parseInt(
					httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_ROLE_ID).toString());

			String livingCountry = httpServletRequest.getSession(false)
					.getAttribute(ApplicationConstants.Living_COUNTRY).toString();
			String userRoleName = httpServletRequest.getSession(false)
					.getAttribute(ApplicationConstants.SESSION_ROLE_NAME).toString();

			resultType = "accessFailed";

			try {

				resultType = SUCCESS;
			} catch (Exception ex) {

				httpServletRequest.getSession(false).setAttribute("errorMessage", ex.toString());
				resultType = ERROR;
			}
		} 
		return resultType;
	}
	
	
	/*NagaLakhsmi Telluri
	 * 10/25/2019
	 */
	
	public String getCustomerMSAList() {

		resultType = LOGIN;

		if (httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
			String loginId = httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID)
					.toString();
			userRoleId = Integer.parseInt(
					httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_ROLE_ID).toString());
			resultType = "accessFailed";
			// String objectId =
			// httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_EMP_ID).toString();

			if (AuthorizationManager.getInstance().isAuthorizedUser("GET_PMO_ACTIVITY", userRoleId)) {

				try {
					

					resultType = SUCCESS;
				} catch (Exception ex) {
					ex.printStackTrace();
					httpServletRequest.getSession(false).setAttribute("errorMessage", ex.toString());
					resultType = ERROR;
				}

			}
		}
		return resultType;
	}

	/**
	 * The getFirstName() is used for firstName of employee.
	 *
	 * @ return String variable
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * The setFirstName(String firstName) is used for setting firstName of
	 * employee.
	 *
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * The getLastName() is used for lastName of employee.
	 *
	 * @ return String variable
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * The setLastName(String lastName) is used for setting lastName of
	 * employee.
	 *
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * The getMiddleName() is used for middleName of employee.
	 *
	 * @ return String variable
	 */
	public String getMiddleName() {
		return middleName;
	}

	/**
	 * The setMiddleName(String middleName) is used for setting middleName of
	 * employee.
	 *
	 */
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	/**
	 * The getAliasName() is used for aliasName of employee.
	 *
	 * @ return String variable
	 */
	public String getAliasName() {
		return aliasName;
	}

	/**
	 * The setAliasName(String aliasName) is used for setting middleName of
	 * employee.
	 *
	 */
	public void setAliasName(String aliasName) {
		this.aliasName = aliasName;
	}

	/**
	 * The getGender() is used for gender of employee.
	 *
	 * @ return String variable
	 */
	public String getGender() {
		return gender;
	}

	/**
	 * The setGender(String gender) is used for setting gender of employee.
	 *
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}

	/**
	 * The getMaritalStatus() is used for maritalStatus of employee.
	 *
	 * @ return String variable
	 */
	public String getMaritalStatus() {
		return maritalStatus;
	}

	/**
	 * The setMaritalStatus(String maritalStatus) is used for setting
	 * maritalStatus of employee.
	 *
	 */
	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	/**
	 * The getCountry() is used for country of employee.
	 *
	 * @ return String variable
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * The setCountry(String country) is used for setting country of employee.
	 *
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 * The getSsn() is used for ssn of employee.
	 *
	 * @ return String variable
	 */
	public String getSsn() {
		return ssn;
	}

	/**
	 * The setSsn(String ssn) is used for setting ssn of employee.
	 *
	 */
	public void setSsn(String ssn) {
		this.ssn = ssn;
	}

	/**
	 * The getCurrStatus() is used for currStatus of employee.
	 *
	 * @ return String variable
	 */
	public String getCurrStatus() {
		return currStatus;
	}

	/**
	 * The setCurrStatus(String currStatus) is used for setting currStatus of
	 * employee.
	 *
	 */
	public void setCurrStatus(String currStatus) {
		this.currStatus = currStatus;
	}

	/**
	 * The getEmpTypeId() is used for empTypeId of employee.
	 *
	 * @ return String variable
	 */
	public String getEmpTypeId() {
		return empTypeId;
	}

	/**
	 * The setEmpTypeId(String empTypeId) is used for setting empTypeId of
	 * employee.
	 *
	 */
	public void setEmpTypeId(String empTypeId) {
		this.empTypeId = empTypeId;
	}

	/**
	 * The getOrgId() is used for orgId of employee.
	 *
	 * @ return String variable
	 */
	public String getOrgId() {
		return orgId;
	}

	/**
	 * The setOrgId(String orgId) is used for setting orgId of employee.
	 *
	 */
	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}

	/**
	 * The getOpsContactId() is used for opsContactId of employee.
	 *
	 * @ return int variable
	 */
	public String getOpsContactId() {
		return opsContactId;
	}

	/**
	 * The setOpsContactId(int opsContactId) is used for setting opsContactId of
	 * employee.
	 *
	 */
	public void setOpsContactId(String opsContactId) {
		this.opsContactId = opsContactId;
	}

	/**
	 * The getTeamId() is used for teamId of employee.
	 *
	 * @ return String variable
	 */
	public String getTeamId() {
		return teamId;
	}

	/**
	 * The setTeamId(String teamId) is used for setting teamId of employee.
	 *
	 */
	public void setTeamId(String teamId) {
		this.teamId = teamId;
	}

	/**
	 * The getPracticeId() is used for practiceId of employee.
	 *
	 * @ return String variable
	 */
	public String getPracticeId() {
		return practiceId;
	}

	/**
	 * The setPracticeId(String practiceId) is used for setting practiceId of
	 * employee.
	 *
	 */
	public void setPracticeId(String practiceId) {
		this.practiceId = practiceId;
	}

	/*
	 * public int getReportsToId() { return reportsToId; }
	 * 
	 * public void setReportsToId(int reportsToId) { this.reportsToId =
	 * reportsToId; }
	 */
	/**
	 * The getTitleId() is used for titleId of employee.
	 *
	 * @ return String variable
	 */
	public String getTitleId() {
		return titleId;
	}

	/**
	 * The setTitleId(String titleId) is used for setting titleId of employee.
	 *
	 */
	public void setTitleId(String titleId) {
		this.titleId = titleId;
	}

	/**
	 * The getIndustryId() is used for industryId of employee.
	 *
	 * @ return String variable
	 */
	public String getIndustryId() {
		return industryId;
	}

	/**
	 * The setIndustryId(String industryId) is used for setting industryId of
	 * employee.
	 *
	 */
	public void setIndustryId(String industryId) {
		this.industryId = industryId;
	}

	/**
	 * The getDepartmentId() is used for departmentId of employee.
	 *
	 * @ return String variable
	 */
	public String getDepartmentId() {
		return departmentId;
	}

	/**
	 * The setDepartmentId(String departmentId) is used for setting departmentId
	 * of employee.
	 *
	 */
	public void setDepartmentId(String departmentId) {
		this.departmentId = departmentId;
	}

	/**
	 * The getBirthDate() is used for birthDate of employee.
	 *
	 * @ return Date variable
	 */
	public Date getBirthDate() {
		return birthDate;
	}

	/**
	 * The setBirthDate(Date birthDate) is used for setting birthDate of
	 * employee.
	 *
	 */
	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	/**
	 * The getOffBirthDate() is used for offBirthDate of employee.
	 *
	 * @ return Date variable
	 */
	public Date getOffBirthDate() {
		return offBirthDate;
	}

	/**
	 * The setOffBirthDate(Date offBirthDate) is used for setting offBirthDate
	 * of employee.
	 *
	 */
	public void setOffBirthDate(Date offBirthDate) {
		this.offBirthDate = offBirthDate;
	}

	/**
	 * The getHireDate() is used for hireDate of employee.
	 *
	 * @ return Date variable
	 */
	public Date getHireDate() {
		return hireDate;
	}

	/**
	 * The setHireDate(Date hireDate) is used for setting hireDate of employee.
	 *
	 */
	public void setHireDate(Date hireDate) {
		this.hireDate = hireDate;
	}

	/**
	 * The getAnniversaryDate() is used for anniversaryDate of employee.
	 *
	 * @ return Date variable
	 */
	public Date getAnniversaryDate() {
		return anniversaryDate;
	}

	/**
	 * The setAnniversaryDate(Date anniversaryDate) is used for setting
	 * anniversaryDate of employee.
	 *
	 */
	public void setAnniversaryDate(Date anniversaryDate) {
		this.anniversaryDate = anniversaryDate;
	}

	/**
	 * The getWorkPhoneNo() is used for workPhoneNo of employee.
	 *
	 * @ return String variable
	 */
	public String getWorkPhoneNo() {
		return workPhoneNo;
	}

	/**
	 * The setWorkPhoneNo(String workPhoneNo) is used for setting workPhoneNo of
	 * employee.
	 *
	 */
	public void setWorkPhoneNo(String workPhoneNo) {
		this.workPhoneNo = workPhoneNo;
	}

	/**
	 * The getAltPhoneNo() is used for altPhoneNo of employee.
	 *
	 * @ return String variable
	 */
	public String getAltPhoneNo() {
		return altPhoneNo;
	}

	/**
	 * The setAltPhoneNo(String altPhoneNo) is used for setting altPhoneNo of
	 * employee.
	 *
	 */
	public void setAltPhoneNo(String altPhoneNo) {
		this.altPhoneNo = altPhoneNo;
	}

	/**
	 * The getHomePhoneNo() is used for homePhoneNo of employee.
	 *
	 * @ return String variable
	 */
	public String getHomePhoneNo() {
		return homePhoneNo;
	}

	/**
	 * The setHomePhoneNo(String homePhoneNo) is used for setting homePhoneNo of
	 * employee.
	 *
	 */
	public void setHomePhoneNo(String homePhoneNo) {
		this.homePhoneNo = homePhoneNo;
	}

	/**
	 * The getCellPhoneNo() is used for cellPhoneNo of employee.
	 *
	 * @ return String variable
	 */
	public String getCellPhoneNo() {
		return cellPhoneNo;
	}

	/**
	 * The setCellPhoneNo(String cellPhoneNo) is used for setting cellPhoneNo of
	 * employee.
	 *
	 */
	public void setCellPhoneNo(String cellPhoneNo) {
		this.cellPhoneNo = cellPhoneNo;
	}

	/**
	 * The getOfficeEmail() is used for officeEmail of employee.
	 *
	 * @ return String variable
	 */
	public String getOfficeEmail() {
		return officeEmail;
	}

	/**
	 * The setOfficeEmail(String officeEmail) is used for setting officeEmail of
	 * employee.
	 *
	 */
	public void setOfficeEmail(String officeEmail) {
		this.officeEmail = officeEmail;
	}

	/**
	 * The getHotelPhoneNo() is used for hotelPhoneNo of employee.
	 *
	 * @ return String variable
	 */
	public String getHotelPhoneNo() {
		return hotelPhoneNo;
	}

	/**
	 * The setHotelPhoneNo(String hotelPhoneNo) is used for setting hotelPhoneNo
	 * of employee.
	 *
	 */
	public void setHotelPhoneNo(String hotelPhoneNo) {
		this.hotelPhoneNo = hotelPhoneNo;
	}

	/**
	 * The getPersonalEmail() is used for personalEmail of employee.
	 *
	 * @ return String variable
	 */
	public String getPersonalEmail() {
		return personalEmail;
	}

	/**
	 * The setPersonalEmail(String personalEmail) is used for setting
	 * personalEmail of employee.
	 *
	 */
	public void setPersonalEmail(String personalEmail) {
		this.personalEmail = personalEmail;
	}

	/**
	 * The getIndiaPhoneNo() is used for indiaPhoneNo of employee.
	 *
	 * @ return String variable
	 */
	public String getIndiaPhoneNo() {
		return indiaPhoneNo;
	}

	/**
	 * The setIndiaPhoneNo(String indiaPhoneNo) is used for setting indiaPhoneNo
	 * of employee.
	 *
	 */
	public void setIndiaPhoneNo(String indiaPhoneNo) {
		this.indiaPhoneNo = indiaPhoneNo;
	}

	/**
	 * The getOtherEmail() is used for otherEmail of employee.
	 *
	 * @ return String variable
	 */
	public String getOtherEmail() {
		return otherEmail;
	}

	/**
	 * The setOtherEmail(String otherEmail) is used for setting otherEmail of
	 * employee.
	 *
	 */
	public void setOtherEmail(String otherEmail) {
		this.otherEmail = otherEmail;
	}

	/**
	 * The setFaxNo(String faxNo) is used for setting faxNo of employee.
	 *
	 */
	public void setFaxNo(String faxNo) {
		this.faxNo = faxNo;
	}

	/**
	 * The getFaxNo() is used for otherEmail of employee.
	 *
	 * @ return String variable
	 */
	public String getFaxNo() {
		return faxNo;
	}

	/**
	 * The getMaritalStatusList() is used for maritalStatusList of employee.
	 *
	 * @ return List variable
	 */
	public List getMaritalStatusList() {
		return maritalStatusList;
	}

	/**
	 * The setMaritalStatusList(List maritalStatusList) is used for setting
	 * maritalStatusList of employee.
	 *
	 */
	public void setMaritalStatusList(List maritalStatusList) {
		this.maritalStatusList = maritalStatusList;
	}

	/**
	 * The getCountryList() is used for maritalStatusList of employee.
	 *
	 * @ return List variable
	 */
	public List getCountryList() {
		return countryList;
	}

	/**
	 * The setCountryList(List coutryList) is used for coutryList of employee.
	 *
	 * @ return List variable
	 */
	public void setCountryList(List coutryList) {
		this.countryList = coutryList;
	}

	/**
	 * The getCurrStatusList() is used for currStatusList of employee.
	 *
	 * @ return List variable
	 */
	public List getCurrStatusList() {
		return currStatusList;
	}

	/**
	 * The setCurrStatusList(List currStatusList) is used for currStatusList of
	 * employee.
	 *
	 * @ return List variable
	 */
	public void setCurrStatusList(List currStatusList) {
		this.currStatusList = currStatusList;
	}

	/**
	 * The getEmpTypeIdList() is used for empTypeIdList of employee.
	 *
	 * @ return List variable
	 */
	public List getEmpTypeIdList() {
		return empTypeIdList;
	}

	/**
	 * The setEmpTypeIdList(List empTypeIdList) is used for empTypeIdList of
	 * employee.
	 *
	 */
	public void setEmpTypeIdList(List empTypeIdList) {
		this.empTypeIdList = empTypeIdList;
	}

	/**
	 * The getOrgIdList() is used for orgIdList of employee.
	 *
	 * @ return List variable
	 */
	public List getOrgIdList() {
		return orgIdList;
	}

	/**
	 * The setOrgIdList(List orgIdList) is used for orgIdList of employee.
	 *
	 */
	public void setOrgIdList(List orgIdList) {
		this.orgIdList = orgIdList;
	}

	/**
	 * The getOpsContactIdMap() is used for opsContactIdMap of employee.
	 *
	 * @ return Map variable
	 */
	public Map getOpsContactIdMap() {
		return opsContactIdMap;
	}

	/**
	 * The setOpsContactIdMap(Map opsContactIdMap) is used for opsContactIdMap
	 * of employee.
	 *
	 */
	public void setOpsContactIdMap(Map opsContactIdMap) {
		this.opsContactIdMap = opsContactIdMap;
	}

	/**
	 * The getTeamIdList() is used for teamIdList of employee.
	 *
	 * @ return List variable
	 */
	public List getTeamIdList() {
		return teamIdList;
	}

	/**
	 * The setTeamIdList(List teamIdList) is used for teamIdList of employee.
	 *
	 */
	public void setTeamIdList(List teamIdList) {
		this.teamIdList = teamIdList;
	}

	/**
	 * The getPracticeIdList() is used for practiceIdList of employee.
	 *
	 * @ return List variable
	 */
	public List getPracticeIdList() {
		return practiceIdList;
	}

	/**
	 * The setPracticeIdList(List practiceIdList) is used for practiceIdList of
	 * employee.
	 *
	 */
	public void setPracticeIdList(List practiceIdList) {
		this.practiceIdList = practiceIdList;
	}

	/**
	 * The getReportsToIdMap() is used for reportsToIdMap of employee.
	 *
	 * @ return Map variable
	 */
	public Map getReportsToIdMap() {
		return reportsToIdMap;
	}

	/**
	 * The setReportsToIdMap(Map reportsToIdMap) is used for reportsToIdMap of
	 * employee.
	 *
	 */
	public void setReportsToIdMap(Map reportsToIdMap) {
		this.reportsToIdMap = reportsToIdMap;
	}

	/**
	 * The getTitleIdList() is used for titleIdList of employee.
	 *
	 * @ return List variable
	 */
	public List getTitleIdList() {
		return titleIdList;
	}

	/**
	 * The setTitleIdList(List titleIdList) is used for titleIdList of employee.
	 *
	 */
	public void setTitleIdList(List titleIdList) {
		this.titleIdList = titleIdList;
	}

	/**
	 * The getIndustryIdList() is used for industryIdList of employee.
	 *
	 * @ return List variable
	 */
	public List getIndustryIdList() {
		return industryIdList;
	}

	/**
	 * The setIndustryIdList(List industryIdList) is used for industryIdList of
	 * employee.
	 *
	 */
	public void setIndustryIdList(List industryIdList) {
		this.industryIdList = industryIdList;
	}

	/**
	 * The getDepartmentIdList() is used for departmentIdList of employee.
	 *
	 * @ return List variable
	 */
	public List getDepartmentIdList() {
		return departmentIdList;
	}

	/**
	 * The setDepartmentIdList(List departmentIdList) is used for
	 * departmentIdList of employee.
	 *
	 */
	public void setDepartmentIdList(List departmentIdList) {
		this.departmentIdList = departmentIdList;
	}

	/**
	 * The getGenderList() is used for genderList of employee.
	 *
	 * @ return List variable
	 */
	public List getGenderList() {
		return genderList;
	}

	/**
	 * The setGenderList(List genderList) is used for genderList of employee.
	 *
	 */
	public void setGenderList(List genderList) {
		this.genderList = genderList;
	}

	/**
	 * The getId() is used for id of employee.
	 *
	 * @ return int variable
	 */
	public int getId() {
		return id;
	}

	/**
	 * The setId(int id) is used for id of employee.
	 *
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * The getLastContactBy() is used for lastContactBy of employee.
	 *
	 * @ return int variable
	 */
	public String getLastContactBy() {
		return lastContactBy;
	}

	/**
	 * The setLastContactBy(int lastContactBy) is used for lastContactBy of
	 * employee.
	 *
	 */
	public void setLastContactBy(String lastContactBy) {
		this.lastContactBy = lastContactBy;
	}

	/**
	 * The getQueryString() is used for queryString of employee.
	 *
	 * @ return StringBuffer variable
	 */
	public StringBuilder getQueryStringBuffer() {
		return queryStringBuffer;
	}

	/**
	 * The setQueryString(StringBuffer queryString) is used for queryString of
	 * employee.
	 *
	 */
	public void setQueryStringBuffer(StringBuilder queryStringBuf) {
		this.queryStringBuffer = queryStringBuf;
	}

	/**
	 * The setServletRequest(HttpServletRequest httpServletRequest) is used for
	 * httpServletRequest of employee.
	 *
	 */
	public void setServletRequest(HttpServletRequest httpServletRequest) {
		this.httpServletRequest = httpServletRequest;
	}

	/**
	 * The getModifiedBy() is used for modifiedBy of employee.
	 *
	 * @ return String variable
	 */
	public String getModifiedBy() {
		return modifiedBy;
	}

	/**
	 * The setModifiedBy(String modifiedBy) is used for modifiedBy of employee.
	 *
	 */
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	/**
	 * The getModifiedDate() is used for modifiedDate of employee.
	 *
	 * @ return Timestamp variable
	 */
	public Timestamp getModifiedDate() {
		return modifiedDate;
	}

	/**
	 * The setModifiedDate(Timestamp modifiedDate) is used for modifiedDate of
	 * employee.
	 *
	 */
	public void setModifiedDate(Timestamp modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	/**
	 * The getCreatedBy() is used for createdBy of employee.
	 *
	 * @ return String variable
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * The setCreatedBy(String createdBy) is used for createdBy of employee.
	 *
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * The getCreatedDate() is used for createdDate of employee.
	 *
	 * @ return Timestamp variable
	 */
	public Timestamp getCreatedDate() {
		return createdDate;
	}

	/**
	 * The setCreatedDate(Timestamp createdDate) is used for createdDate of
	 * employee.
	 *
	 */
	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	/**
	 * The setEmpId(int empId) is used for empId of employee.
	 *
	 */
	public void setEmpId(int empId) {
		this.empId = empId;
	}

	/**
	 * The getEmpId() is used for empId of employee.
	 *
	 * @ return int variable
	 */
	public int getEmpId() {
		return empId;
	}

	/**
	 * EmployeeVTO getCurrentEmployee() is used for currentEmployee of employee.
	 *
	 * @see com.mss.miracle.employee.general.EmployeeVTO
	 *
	 */
	public EmployeeVTO getCurrentEmployee() {
		return currentEmployee;
	}

	/**
	 * setCurrentEmployee(EmployeeVTO currentEmployee) is used for
	 * currentEmployee of employee.
	 *
	 * @see com.mss.miracle.employee.general.EmployeeVTO
	 *
	 */
	public void setCurrentEmployee(EmployeeVTO currentEmployee) {
		this.currentEmployee = currentEmployee;
	}

	/**
	 * The getPreCurrStatus() is used for preCurrStatus of employee.
	 *
	 * @ return String variable
	 */
	public String getPreCurrStatus() {
		return preCurrStatus;
	}

	/**
	 * setPreCurrStatus(String preCurrStatus) is used for preCurrStatus of
	 * employee.
	 *
	 */
	public void setPreCurrStatus(String preCurrStatus) {
		this.preCurrStatus = preCurrStatus;
	}

	public boolean getIsManager() {
		return isManager;
	}

	public void setIsManager(boolean isManager) {
		this.isManager = isManager;
	}

	public String getReportsTo() {
		return reportsTo;
	}

	public void setReportsTo(String reportsTo) {
		this.reportsTo = reportsTo;
	}

	public Map getPointOfContactMap() {
		return pointOfContactMap;
	}

	public void setPointOfContactMap(Map pointOfContactMap) {
		this.pointOfContactMap = pointOfContactMap;
	}

	public String getSubmitFrom() {
		return submitFrom;
	}

	public void setSubmitFrom(String submitFrom) {
		this.submitFrom = submitFrom;
	}

	public String getEmpState() {
		return empState;
	}

	public void setEmpState(String empState) {
		this.empState = empState;
	}

	public Date getStateStartDate() {
		return stateStartDate;
	}

	public void setStateStartDate(Date stateStartDate) {
		this.stateStartDate = stateStartDate;
	}

	public Date getStateEndDate() {
		return stateEndDate;
	}

	public void setStateEndDate(Date stateEndDate) {
		this.stateEndDate = stateEndDate;
	}

	// public float getIntRatePerHour() {
	// return intRatePerHour;
	// }
	//
	// public void setIntRatePerHour(float intRatePerHour) {
	// this.intRatePerHour = intRatePerHour;
	// }
	//
	// public float getInvRatePerHour() {
	// return invRatePerHour;
	// }
	//
	// public void setInvRatePerHour(float invRatePerHour) {
	// this.invRatePerHour = invRatePerHour;
	// }
	public String getSkillSet() {
		return skillSet;
	}

	public void setSkillSet(String skillSet) {
		this.skillSet = skillSet;
	}

	public List getEmpCurrentStatus() {
		return empCurrentStatus;
	}

	public void setEmpCurrentStatus(List empCurrentStatus) {
		this.empCurrentStatus = empCurrentStatus;
	}

	public String getDeleteAction() {
		return deleteAction;
	}

	public void setDeleteAction(String deleteAction) {
		this.deleteAction = deleteAction;
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getPreviousEmpState() {
		return previousEmpState;
	}

	public void setPreviousEmpState(String previousEmpState) {
		if (previousEmpState == null) {
			previousEmpState = "";
		}
		this.previousEmpState = previousEmpState;
	}

	public StateVTO getCurrentEmployeeState() {
		return currentEmployeeState;
	}

	public void setCurrentEmployeeState(StateVTO currentEmployeeState) {
		this.currentEmployeeState = currentEmployeeState;
	}

	public Collection getCurrentStateHistoryCollection() {
		return currentStateHistoryCollection;
	}

	public void setCurrentStateHistoryCollection(Collection currentStateHistoryCollection) {
		this.currentStateHistoryCollection = currentStateHistoryCollection;
	}

	public List getSubPracticeList() {
		return subPracticeList;
	}

	public void setSubPracticeList(List subPracticeList) {
		this.subPracticeList = subPracticeList;
	}

	public String getSubPractice() {
		return subPractice;
	}

	public void setSubPractice(String subPractice) {
		this.subPractice = subPractice;
	}

	public boolean isIsTeamLead() {
		return isTeamLead;
	}

	public void setIsTeamLead(boolean isTeamLead) {
		this.isTeamLead = isTeamLead;
	}

	public String getEmpno() {
		return empno;
	}

	public void setEmpno(String empno) {
		this.empno = empno;
	}

	public String getNsrno() {
		return nsrno;
	}

	public void setNsrno(String nsrno) {
		this.nsrno = nsrno;
	}

	public String getPrjName() {
		return prjName;
	}

	public void setPrjName(String prjName) {
		this.prjName = prjName;
	}

	// public double getCtcPerYear() {
	// return ctcPerYear;
	// }
	//
	// public void setCtcPerYear(double ctcPerYear) {
	// this.ctcPerYear = ctcPerYear;
	// }
	public String getItgBatch() {
		return itgBatch;
	}

	public void setItgBatch(String itgBatch) {
		this.itgBatch = itgBatch;
	}

	public List getTerritoryList() {
		return territoryList;
	}

	public void setTerritoryList(List territoryList) {
		this.territoryList = territoryList;
	}

	public List getRegionList() {
		return regionList;
	}

	public void setRegionList(List regionList) {
		this.regionList = regionList;
	}

	public String getWorkingCountry() {
		return workingCountry;
	}

	public void setWorkingCountry(String workingCountry) {
		this.workingCountry = workingCountry;
	}

	public String getIsOnsite() {
		return isOnsite;
	}

	public void setIsOnsite(String isOnsite) {
		this.isOnsite = isOnsite;
	}

	public String getTempName() {
		return tempName;
	}

	public void setTempName(String tempName) {
		this.tempName = tempName;
	}

	public String getTempPh() {
		return tempPh;
	}

	public void setTempPh(String tempPh) {
		this.tempPh = tempPh;
	}

	public String getTempCurStatus() {
		return tempCurStatus;
	}

	public void setTempCurStatus(String tempCurStatus) {
		this.tempCurStatus = tempCurStatus;
	}

	public String getTempDeptId() {
		return tempDeptId;
	}

	public void setTempDeptId(String tempDeptId) {
		this.tempDeptId = tempDeptId;
	}

	public String getTempOrgId() {
		return tempOrgId;
	}

	public void setTempOrgId(String tempOrgId) {
		this.tempOrgId = tempOrgId;
	}

	public int getCurrId() {
		return currId;
	}

	public void setCurrId(int currId) {
		this.currId = currId;
	}

	public int getTempVar() {
		return tempVar;
	}

	public void setTempVar(int tempVar) {
		this.tempVar = tempVar;
	}

	public String getSubmitFromReportsTo() {
		return submitFromReportsTo;
	}

	public void setSubmitFromReportsTo(String submitFromReportsTo) {
		this.submitFromReportsTo = submitFromReportsTo;
	}

	public Map getReportingPersons() {
		return reportingPersons;
	}

	public void setReportingPersons(Map reportingPersons) {
		this.reportingPersons = reportingPersons;
	}

	public String getReportingpersonId() {
		return reportingpersonId;
	}

	public void setReportingpersonId(String reportingpersonId) {
		this.reportingpersonId = reportingpersonId;
	}

	// new
	public File getImagePath() {
		return imagePath;
	}

	public void setImagePath(File imagePath) {
		this.imagePath = imagePath;
	}

	public int getNavId() {
		return navId;
	}

	public void setNavId(int navId) {
		this.navId = navId;
	}

	public boolean getIsCreTeam() {
		return isCreTeam;
	}

	public void setIsCreTeam(boolean isCreTeam) {
		this.isCreTeam = isCreTeam;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	/**
	 * @return the isOperationTeam
	 */
	public boolean getIsOperationTeam() {
		return isOperationTeam;
	}

	/**
	 * @param isOperationTeam
	 *            the isOperationTeam to set
	 */
	public void setIsOperationTeam(boolean isOperationTeam) {
		this.isOperationTeam = isOperationTeam;
	}

	/**
	 * @return the projectName
	 */
	public String getProjectName() {
		return projectName;
	}

	/**
	 * @param projectName
	 *            the projectName to set
	 */
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the startDate
	 */
	public String getStartDate() {
		return startDate;
	}

	/**
	 * @param startDate
	 *            the startDate to set
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	/**
	 * @return the projectId
	 */
	public String getProjectId() {
		return projectId;
	}

	/**
	 * @param projectId
	 *            the projectId to set
	 */
	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	/**
	 * @return the accountId
	 */
	public String getAccountId() {
		return accountId;
	}

	/**
	 * @param accountId
	 *            the accountId to set
	 */
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	/**
	 * @return the myAccounts
	 */
	public Map getMyAccounts() {
		return myAccounts;
	}

	/**
	 * @param myAccounts
	 *            the myAccounts to set
	 */
	public void setMyAccounts(Map myAccounts) {
		this.myAccounts = myAccounts;
	}

	/**
	 * @return the myProjects
	 */
	public Map getMyProjects() {
		return myProjects;
	}

	/**
	 * @param myProjects
	 *            the myProjects to set
	 */
	public void setMyProjects(Map myProjects) {
		this.myProjects = myProjects;
	}

	public boolean getIsPMO() {
		return isPMO;
	}

	public void setIsPMO(boolean isPMO) {
		this.isPMO = isPMO;
	}

	/**
	 * @return the bankName
	 */
	public String getBankName() {
		return bankName;
	}

	/**
	 * @param bankName
	 *            the bankName to set
	 */
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	/**
	 * @return the accNum
	 */
	public String getAccNum() {
		return accNum;
	}

	/**
	 * @param accNum
	 *            the accNum to set
	 */
	public void setAccNum(String accNum) {
		this.accNum = accNum;
	}

	/**
	 * @return the nameAsPerAcc
	 */
	public String getNameAsPerAcc() {
		return nameAsPerAcc;
	}

	/**
	 * @param nameAsPerAcc
	 *            the nameAsPerAcc to set
	 */
	public void setNameAsPerAcc(String nameAsPerAcc) {
		this.nameAsPerAcc = nameAsPerAcc;
	}

	/**
	 * @return the ifscCode
	 */
	public String getIfscCode() {
		return ifscCode;
	}

	/**
	 * @param ifscCode
	 *            the ifscCode to set
	 */
	public void setIfscCode(String ifscCode) {
		this.ifscCode = ifscCode;
	}

	/**
	 * @return the phyChallenged
	 */
	public String getPhyChallenged() {
		return phyChallenged;
	}

	/**
	 * @param phyChallenged
	 *            the phyChallenged to set
	 */
	public void setPhyChallenged(String phyChallenged) {
		this.phyChallenged = phyChallenged;
	}

	/**
	 * @return the phyCategory
	 */
	public String getPhyCategory() {
		return phyCategory;
	}

	/**
	 * @param phyCategory
	 *            the phyCategory to set
	 */
	public void setPhyCategory(String phyCategory) {
		this.phyCategory = phyCategory;
	}

	/**
	 * @return the aadharNum
	 */
	public String getAadharNum() {
		return aadharNum;
	}

	/**
	 * @param aadharNum
	 *            the aadharNum to set
	 */
	public void setAadharNum(String aadharNum) {
		this.aadharNum = aadharNum;
	}

	/**
	 * @return the aadharName
	 */
	public String getAadharName() {
		return aadharName;
	}

	/**
	 * @param aadharName
	 *            the aadharName to set
	 */
	public void setAadharName(String aadharName) {
		this.aadharName = aadharName;
	}

	/**
	 * @return the nameAsPerPan
	 */
	public String getNameAsPerPan() {
		return nameAsPerPan;
	}

	/**
	 * @param nameAsPerPan
	 *            the nameAsPerPan to set
	 */
	public void setNameAsPerPan(String nameAsPerPan) {
		this.nameAsPerPan = nameAsPerPan;
	}

	/**
	 * @return the uanNo
	 */
	public String getUanNo() {
		return uanNo;
	}

	/**
	 * @param uanNo
	 *            the uanNo to set
	 */
	public void setUanNo(String uanNo) {
		this.uanNo = uanNo;
	}

	/**
	 * @return the pfno
	 */
	public String getPfno() {
		return pfno;
	}

	/**
	 * @param pfno
	 *            the pfno to set
	 */
	public void setPfno(String pfno) {
		this.pfno = pfno;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getResultMessage() {
		return resultMessage;
	}

	public void setResultMessage(String resultMessage) {
		this.resultMessage = resultMessage;
	}

	public String getTechReviews() {
		return techReviews;
	}

	public void setTechReviews(String techReviews) {
		this.techReviews = techReviews;
	}

	/**
	 * @return the lastRevisedDate
	 */
	public Date getLastRevisedDate() {
		return lastRevisedDate;
	}

	/**
	 * @param lastRevisedDate
	 *            the lastRevisedDate to set
	 */
	public void setLastRevisedDate(Date lastRevisedDate) {
		this.lastRevisedDate = lastRevisedDate;
	}

	/**
	 * @return the revisedDate
	 */
	public Date getRevisedDate() {
		return revisedDate;
	}

	/**
	 * @param revisedDate
	 *            the revisedDate to set
	 */
	public void setRevisedDate(Date revisedDate) {
		this.revisedDate = revisedDate;
	}

	/**
	 * @return the myTeamMembers
	 */
	public Map getMyTeamMembers() {
		return myTeamMembers;
	}

	/**
	 * @param myTeamMembers
	 *            the myTeamMembers to set
	 */
	public void setMyTeamMembers(Map myTeamMembers) {
		this.myTeamMembers = myTeamMembers;
	}

	/**
	 * @return the stateList
	 */
	public List getStateList() {
		return stateList;
	}

	/**
	 * @param stateList
	 *            the stateList to set
	 */
	public void setStateList(List stateList) {
		this.stateList = stateList;
	}

	/**
	 * @return the managerTeamMembersList
	 */
	public Map getManagerTeamMembersList() {
		return managerTeamMembersList;
	}

	/**
	 * @param managerTeamMembersList
	 *            the managerTeamMembersList to set
	 */
	public void setManagerTeamMembersList(Map managerTeamMembersList) {
		this.managerTeamMembersList = managerTeamMembersList;
	}

	/**
	 * @return the managerTeamMember
	 */
	public String getManagerTeamMember() {
		return managerTeamMember;
	}

	/**
	 * @param managerTeamMember
	 *            the managerTeamMember to set
	 */
	public void setManagerTeamMember(String managerTeamMember) {
		this.managerTeamMember = managerTeamMember;
	}

	/**
	 * @return the currentAction
	 */
	public String getCurrentAction() {
		return currentAction;
	}

	/**
	 * @param currentAction
	 *            the currentAction to set
	 */
	public void setCurrentAction(String currentAction) {
		this.currentAction = currentAction;
	}

	/**
	 * @return the customerName
	 */
	public String getCustomerName() {
		return customerName;
	}

	/**
	 * @param customerName
	 *            the customerName to set
	 */
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	/**
	 * @return the utilization
	 */
	public int getUtilization() {
		return utilization;
	}

	/**
	 * @param utilization
	 *            the utilization to set
	 */
	public void setUtilization(int utilization) {
		this.utilization = utilization;
	}

	/**
	 * @return the comments
	 */
	public String getComments() {
		return comments;
	}

	/**
	 * @param comments
	 *            the comments to set
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}

	/**
	 * @return the projectTeamReportsTo
	 */
	public Map getProjectTeamReportsTo() {
		return projectTeamReportsTo;
	}

	/**
	 * @param projectTeamReportsTo
	 *            the projectTeamReportsTo to set
	 */
	public void setProjectTeamReportsTo(Map projectTeamReportsTo) {
		this.projectTeamReportsTo = projectTeamReportsTo;
	}

	/**
	 * @return the isBillable
	 */
	public String getIsBillable() {
		return isBillable;
	}

	/**
	 * @param isBillable
	 *            the isBillable to set
	 */
	public void setIsBillable(String isBillable) {
		this.isBillable = isBillable;
	}

	/**
	 * @return the isInternationalWorker
	 */
	public boolean getIsInternationalWorker() {
		return isInternationalWorker;
	}

	/**
	 * @param isInternationalWorker
	 *            the isInternationalWorker to set
	 */
	public void setIsInternationalWorker(boolean isInternationalWorker) {
		this.isInternationalWorker = isInternationalWorker;
	}

	/**
	 * @return the locationsMap
	 */
	public Map getLocationsMap() {
		return locationsMap;
	}

	/**
	 * @param locationsMap
	 *            the locationsMap to set
	 */
	public void setLocationsMap(Map locationsMap) {
		this.locationsMap = locationsMap;
	}

	/**
	 * @return the practiceList
	 */
	public List getPracticeList() {
		return practiceList;
	}

	/**
	 * @param practiceList
	 *            the practiceList to set
	 */
	public void setPracticeList(List practiceList) {
		this.practiceList = practiceList;
	}

	/**
	 * @return the empStatus
	 */
	public String getEmpStatus() {
		return empStatus;
	}

	/**
	 * @param empStatus
	 *            the empStatus to set
	 */
	public void setEmpStatus(String empStatus) {
		this.empStatus = empStatus;
	}

	/**
	 * @return the myPMOTeamMembers
	 */
	public Map getMyPMOTeamMembers() {
		return myPMOTeamMembers;
	}

	/**
	 * @param myPMOTeamMembers
	 *            the myPMOTeamMembers to set
	 */
	public void setMyPMOTeamMembers(Map myPMOTeamMembers) {
		this.myPMOTeamMembers = myPMOTeamMembers;
	}

	/**
	 * @return the dateOfTermination
	 */
	public Date getDateOfTermination() {
		return dateOfTermination;
	}

	/**
	 * @param dateOfTermination
	 *            the dateOfTermination to set
	 */
	public void setDateOfTermination(Date dateOfTermination) {
		this.dateOfTermination = dateOfTermination;
	}

	/**
	 * @return the reasonsForTerminate
	 */
	public String getReasonsForTerminate() {
		return reasonsForTerminate;
	}

	/**
	 * @param reasonsForTerminate
	 *            the reasonsForTerminate to set
	 */
	public void setReasonsForTerminate(String reasonsForTerminate) {
		this.reasonsForTerminate = reasonsForTerminate;
	}

	/**
	 * @return the prvexpMnths
	 */
	public String getPrvexpMnths() {
		return prvexpMnths;
	}

	/**
	 * @param prvexpMnths
	 *            the prvexpMnths to set
	 */
	public void setPrvexpMnths(String prvexpMnths) {
		this.prvexpMnths = prvexpMnths;
	}

	/**
	 * @return the prvexpYears
	 */
	public String getPrvexpYears() {
		return prvexpYears;
	}

	/**
	 * @param prvexpYears
	 *            the prvexpYears to set
	 */
	public void setPrvexpYears(String prvexpYears) {
		this.prvexpYears = prvexpYears;
	}

	/**
	 * @return the upload
	 */
	public File getUpload() {
		return upload;
	}

	/**
	 * @param upload
	 *            the upload to set
	 */
	public void setUpload(File upload) {
		this.upload = upload;
	}

	/**
	 * @return the uploadContentType
	 */
	public String getUploadContentType() {
		return uploadContentType;
	}

	/**
	 * @param uploadContentType
	 *            the uploadContentType to set
	 */
	public void setUploadContentType(String uploadContentType) {
		this.uploadContentType = uploadContentType;
	}

	/**
	 * @return the uploadFileName
	 */
	public String getUploadFileName() {
		return uploadFileName;
	}

	/**
	 * @param uploadFileName
	 *            the uploadFileName to set
	 */
	public void setUploadFileName(String uploadFileName) {
		this.uploadFileName = uploadFileName;
	}

	/**
	 * @return the filepath
	 */
	public String getFilepath() {
		return filepath;
	}

	/**
	 * @param filepath
	 *            the filepath to set
	 */
	public void setFilepath(String filepath) {
		this.filepath = filepath;
	}

	/**
	 * @return the projectContactId
	 */
	public int getProjectContactId() {
		return projectContactId;
	}

	/**
	 * @param projectContactId
	 *            the projectContactId to set
	 */
	public void setProjectContactId(int projectContactId) {
		this.projectContactId = projectContactId;
	}

	/**
	 * @return the lateralFlag
	 */
	public boolean getLateralFlag() {
		return lateralFlag;
	}

	/**
	 * @param lateralFlag
	 *            the lateralFlag to set
	 */
	public void setLateralFlag(boolean lateralFlag) {
		this.lateralFlag = lateralFlag;
	}

	/**
	 * @return the clientMap
	 */
	public Map getClientMap() {
		return clientMap;
	}

	/**
	 * @param clientMap
	 *            the clientMap to set
	 */
	public void setClientMap(Map clientMap) {
		this.clientMap = clientMap;
	}

	/**
	 * @return the empName
	 */
	public String getEmpName() {
		return empName;
	}

	/**
	 * @param empName
	 *            the empName to set
	 */
	public void setEmpName(String empName) {
		this.empName = empName;
	}

	/**
	 * @return the empDateOFBirth
	 */
	public String getEmpDateOFBirth() {
		return empDateOFBirth;
	}

	/**
	 * @param empDateOFBirth
	 *            the empDateOFBirth to set
	 */
	public void setEmpDateOFBirth(String empDateOFBirth) {
		this.empDateOFBirth = empDateOFBirth;
	}

	/**
	 * @return the isLateral
	 */
	public String getIsLateral() {
		return isLateral;
	}

	/**
	 * @param isLateral
	 *            the isLateral to set
	 */
	public void setIsLateral(String isLateral) {
		this.isLateral = isLateral;
	}

	/**
	 * @return the quarterAppraisalDetails
	 */
	public String getQuarterAppraisalDetails() {
		return quarterAppraisalDetails;
	}

	/**
	 * @param quarterAppraisalDetails
	 *            the quarterAppraisalDetails to set
	 */
	public void setQuarterAppraisalDetails(String quarterAppraisalDetails) {
		this.quarterAppraisalDetails = quarterAppraisalDetails;
	}

	/**
	 * @return the rowCount
	 */
	public int getRowCount() {
		return rowCount;
	}

	/**
	 * @param rowCount
	 *            the rowCount to set
	 */
	public void setRowCount(int rowCount) {
		this.rowCount = rowCount;
	}

	/**
	 * @return the curretRole
	 */
	public String getCurretRole() {
		return curretRole;
	}

	/**
	 * @param curretRole
	 *            the curretRole to set
	 */
	public void setCurretRole(String curretRole) {
		this.curretRole = curretRole;
	}

	/**
	 * @return the appraisalId
	 */
	public int getAppraisalId() {
		return appraisalId;
	}

	/**
	 * @param appraisalId
	 *            the appraisalId to set
	 */
	public void setAppraisalId(int appraisalId) {
		this.appraisalId = appraisalId;
	}

	/**
	 * @return the lineId
	 */
	public int getLineId() {
		return lineId;
	}

	/**
	 * @param lineId
	 *            the lineId to set
	 */
	public void setLineId(int lineId) {
		this.lineId = lineId;
	}

	/**
	 * @return the year
	 */
	public int getYear() {
		return year;
	}

	/**
	 * @param year
	 *            the year to set
	 */
	public void setYear(int year) {
		this.year = year;
	}

	/**
	 * @return the quarterly
	 */
	public String getQuarterly() {
		return quarterly;
	}

	/**
	 * @param quarterly
	 *            the quarterly to set
	 */
	public void setQuarterly(String quarterly) {
		this.quarterly = quarterly;
	}

	/**
	 * @return the shortTermGoal
	 */
	public String getShortTermGoal() {
		return shortTermGoal;
	}

	/**
	 * @param shortTermGoal
	 *            the shortTermGoal to set
	 */
	public void setShortTermGoal(String shortTermGoal) {
		this.shortTermGoal = shortTermGoal;
	}

	/**
	 * @return the shortTermGoalComments
	 */
	public String getShortTermGoalComments() {
		return shortTermGoalComments;
	}

	/**
	 * @param shortTermGoalComments
	 *            the shortTermGoalComments to set
	 */
	public void setShortTermGoalComments(String shortTermGoalComments) {
		this.shortTermGoalComments = shortTermGoalComments;
	}

	/**
	 * @return the longTermGoal
	 */
	public String getLongTermGoal() {
		return longTermGoal;
	}

	/**
	 * @param longTermGoal
	 *            the longTermGoal to set
	 */
	public void setLongTermGoal(String longTermGoal) {
		this.longTermGoal = longTermGoal;
	}

	/**
	 * @return the longTermGoalComments
	 */
	public String getLongTermGoalComments() {
		return longTermGoalComments;
	}

	/**
	 * @param longTermGoalComments
	 *            the longTermGoalComments to set
	 */
	public void setLongTermGoalComments(String longTermGoalComments) {
		this.longTermGoalComments = longTermGoalComments;
	}

	/**
	 * @return the strength
	 */
	public String getStrength() {
		return strength;
	}

	/**
	 * @param strength
	 *            the strength to set
	 */
	public void setStrength(String strength) {
		this.strength = strength;
	}

	/**
	 * @return the strengthsComments
	 */
	public String getStrengthsComments() {
		return strengthsComments;
	}

	/**
	 * @param strengthsComments
	 *            the strengthsComments to set
	 */
	public void setStrengthsComments(String strengthsComments) {
		this.strengthsComments = strengthsComments;
	}

	/**
	 * @return the improvements
	 */
	public String getImprovements() {
		return improvements;
	}

	/**
	 * @param improvements
	 *            the improvements to set
	 */
	public void setImprovements(String improvements) {
		this.improvements = improvements;
	}

	/**
	 * @return the improvementsComments
	 */
	public String getImprovementsComments() {
		return improvementsComments;
	}

	/**
	 * @param improvementsComments
	 *            the improvementsComments to set
	 */
	public void setImprovementsComments(String improvementsComments) {
		this.improvementsComments = improvementsComments;
	}

	/**
	 * @return the rejectedComments
	 */
	public String getRejectedComments() {
		return rejectedComments;
	}

	/**
	 * @param rejectedComments
	 *            the rejectedComments to set
	 */
	public void setRejectedComments(String rejectedComments) {
		this.rejectedComments = rejectedComments;
	}

	/**
	 * @return the operationTeamStatus
	 */
	public String getOperationTeamStatus() {
		return operationTeamStatus;
	}

	/**
	 * @param operationTeamStatus
	 *            the operationTeamStatus to set
	 */
	public void setOperationTeamStatus(String operationTeamStatus) {
		this.operationTeamStatus = operationTeamStatus;
	}

	/**
	 * @return the managerRejectedComments
	 */
	public String getManagerRejectedComments() {
		return managerRejectedComments;
	}

	/**
	 * @param managerRejectedComments
	 *            the managerRejectedComments to set
	 */
	public void setManagerRejectedComments(String managerRejectedComments) {
		this.managerRejectedComments = managerRejectedComments;
	}

	/**
	 * @return the operationRejectedComments
	 */
	public String getOperationRejectedComments() {
		return operationRejectedComments;
	}

	/**
	 * @param operationRejectedComments
	 *            the operationRejectedComments to set
	 */
	public void setOperationRejectedComments(String operationRejectedComments) {
		this.operationRejectedComments = operationRejectedComments;
	}

	/**
	 * @return the dayCount
	 */
	public int getDayCount() {
		return dayCount;
	}

	/**
	 * @param dayCount
	 *            the dayCount to set
	 */
	public void setDayCount(int dayCount) {
		this.dayCount = dayCount;
	}

	/**
	 * @return the accessCount
	 */
	public int getAccessCount() {
		return accessCount;
	}

	/**
	 * @param accessCount
	 *            the accessCount to set
	 */
	public void setAccessCount(int accessCount) {
		this.accessCount = accessCount;
	}

	/**
	 * @return the empProjectStatus
	 */
	public String getEmpProjectStatus() {
		return empProjectStatus;
	}

	/**
	 * @param empProjectStatus
	 *            the empProjectStatus to set
	 */
	public void setEmpProjectStatus(String empProjectStatus) {
		this.empProjectStatus = empProjectStatus;
	}

	/**
	 * @return the empPracticeList
	 */
	public List getEmpPracticeList() {
		return empPracticeList;
	}

	/**
	 * @param empPracticeList
	 *            the empPracticeList to set
	 */
	public void setEmpPracticeList(List empPracticeList) {
		this.empPracticeList = empPracticeList;
	}

	/**
	 * @return the backToFlag
	 */
	public int getBackToFlag() {
		return backToFlag;
	}

	/**
	 * @param backToFlag
	 *            the backToFlag to set
	 */
	public void setBackToFlag(int backToFlag) {
		this.backToFlag = backToFlag;
	}

	/**
	 * @return the quarterlyForManagersReport
	 */
	public String getQuarterlyForManagersReport() {
		return quarterlyForManagersReport;
	}

	/**
	 * @param quarterlyForManagersReport
	 *            the quarterlyForManagersReport to set
	 */
	public void setQuarterlyForManagersReport(String quarterlyForManagersReport) {
		this.quarterlyForManagersReport = quarterlyForManagersReport;
	}

	/**
	 * @return the yearForManagersReport
	 */
	public int getYearForManagersReport() {
		return yearForManagersReport;
	}

	/**
	 * @param yearForManagersReport
	 *            the yearForManagersReport to set
	 */
	public void setYearForManagersReport(int yearForManagersReport) {
		this.yearForManagersReport = yearForManagersReport;
	}

	/**
	 * @return the month
	 */
	public int getMonth() {
		return month;
	}

	/**
	 * @param month
	 *            the month to set
	 */
	public void setMonth(int month) {
		this.month = month;
	}

	/**
	 * @return the overlayMonth
	 */
	public int getOverlayMonth() {
		return overlayMonth;
	}

	/**
	 * @param overlayMonth
	 *            the overlayMonth to set
	 */
	public void setOverlayMonth(int overlayMonth) {
		this.overlayMonth = overlayMonth;
	}

	/**
	 * @return the overlayYear
	 */
	public int getOverlayYear() {
		return overlayYear;
	}

	/**
	 * @param overlayYear
	 *            the overlayYear to set
	 */
	public void setOverlayYear(int overlayYear) {
		this.overlayYear = overlayYear;
	}

	/**
	 * @return the starPerformerStatus
	 */
	public Map getStarPerformerStatus() {
		return starPerformerStatus;
	}

	/**
	 * @param starPerformerStatus
	 *            the starPerformerStatus to set
	 */
	public void setStarPerformerStatus(Map starPerformerStatus) {
		this.starPerformerStatus = starPerformerStatus;
	}

	/**
	 * @return the statusId
	 */
	public int getStatusId() {
		return statusId;
	}

	/**
	 * @param statusId
	 *            the statusId to set
	 */
	public void setStatusId(int statusId) {
		this.statusId = statusId;
	}

	/**
	 * @return the nomineeIds
	 */
	public String getNomineeIds() {
		return nomineeIds;
	}

	/**
	 * @param nomineeIds
	 *            the nomineeIds to set
	 */
	public void setNomineeIds(String nomineeIds) {
		this.nomineeIds = nomineeIds;
	}

	/**
	 * @return the starMonth
	 */
	public String getStarMonth() {
		return starMonth;
	}

	/**
	 * @param starMonth
	 *            the starMonth to set
	 */
	public void setStarMonth(String starMonth) {
		this.starMonth = starMonth;
	}

	/**
	 * @return the objectId
	 */
	public int getObjectId() {
		return objectId;
	}

	/**
	 * @param objectId
	 *            the objectId to set
	 */
	public void setObjectId(int objectId) {
		this.objectId = objectId;
	}

	/**
	 * @return the starId
	 */
	public int getStarId() {
		return starId;
	}

	/**
	 * @param starId
	 *            the starId to set
	 */
	public void setStarId(int starId) {
		this.starId = starId;
	}

	/**
	 * @return the employeeId
	 */
	public int getEmployeeId() {
		return employeeId;
	}

	/**
	 * @param employeeId
	 *            the employeeId to set
	 */
	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}

	/**
	 * @return the resourceName
	 */
	public String getResourceName() {
		return resourceName;
	}

	/**
	 * @param resourceName
	 *            the resourceName to set
	 */
	public void setResourceName(String resourceName) {
		this.resourceName = resourceName;
	}

	/**
	 * @return the suvyId
	 */
	public int getSuvyId() {
		return suvyId;
	}

	/**
	 * @param suvyId
	 *            the suvyId to set
	 */
	public void setSuvyId(int suvyId) {
		this.suvyId = suvyId;
	}

	/**
	 * @return the httpServletResponse
	 */
	public HttpServletResponse getHttpServletResponse() {
		return httpServletResponse;
	}

	/**
	 * @param httpServletResponse
	 *            the httpServletResponse to set
	 */

	/**
	 * @return the inputStream
	 */
	public InputStream getInputStream() {
		return inputStream;
	}

	/**
	 * @return the outputStream
	 */
	public OutputStream getOutputStream() {
		return outputStream;
	}

	@Override
	public void setServletResponse(HttpServletResponse httpServletResponse) {
		this.httpServletResponse = httpServletResponse;
	}

	public void setHttpServletResponse(HttpServletResponse httpServletResponse) {
		this.httpServletResponse = httpServletResponse;
	}

	/**
	 * @return the currentDate
	 */
	public String getCurrentDate() {
		return currentDate;
	}

	/**
	 * @param currentDate
	 *            the currentDate to set
	 */
	public void setCurrentDate(String currentDate) {
		this.currentDate = currentDate;
	}

	/**
	 * @return the weightageFrom
	 */
	public int getWeightageFrom() {
		return weightageFrom;
	}

	/**
	 * @param weightageFrom
	 *            the weightageFrom to set
	 */
	public void setWeightageFrom(int weightageFrom) {
		this.weightageFrom = weightageFrom;
	}

	/**
	 * @return the weightageTo
	 */
	public int getWeightageTo() {
		return weightageTo;
	}

	/**
	 * @param weightageTo
	 *            the weightageTo to set
	 */
	public void setWeightageTo(int weightageTo) {
		this.weightageTo = weightageTo;
	}

	public int getSearchYear() {
		return searchYear;
	}

	public void setSearchYear(int searchYear) {
		this.searchYear = searchYear;
	}

	public String getSearchQuarterly() {
		return searchQuarterly;
	}

	public void setSearchQuarterly(String searchQuarterly) {
		this.searchQuarterly = searchQuarterly;
	}

	public String getSearchStatus() {
		return searchStatus;
	}

	public void setSearchStatus(String searchStatus) {
		this.searchStatus = searchStatus;
	}

	public String getSearchLoginId() {
		return searchLoginId;
	}

	public void setSearchLoginId(String searchLoginId) {
		this.searchLoginId = searchLoginId;
	}

	public String getSearchDepartmentId() {
		return searchDepartmentId;
	}

	public void setSearchDepartmentId(String searchDepartmentId) {
		this.searchDepartmentId = searchDepartmentId;
	}

	public String getSearchPracticeId() {
		return searchPracticeId;
	}

	public void setSearchPracticeId(String searchPracticeId) {
		this.searchPracticeId = searchPracticeId;
	}

	public String getSearchSubPractice() {
		return searchSubPractice;
	}

	public void setSearchSubPractice(String searchSubPractice) {
		this.searchSubPractice = searchSubPractice;
	}

	public String getSearchOpsContactId() {
		return searchOpsContactId;
	}

	public void setSearchOpsContactId(String searchOpsContactId) {
		this.searchOpsContactId = searchOpsContactId;
	}

	public String getSearchLocation() {
		return searchLocation;
	}

	public void setSearchLocation(String searchLocation) {
		this.searchLocation = searchLocation;
	}

	public String getSearchTitleId() {
		return searchTitleId;
	}

	public void setSearchTitleId(String searchTitleId) {
		this.searchTitleId = searchTitleId;
	}

	public String getTxtCurr() {
		return txtCurr;
	}

	public void setTxtCurr(String txtCurr) {
		this.txtCurr = txtCurr;
	}

	public List getProjectTypesList() {
		return projectTypesList;
	}

	public void setProjectTypesList(List projectTypesList) {
		this.projectTypesList = projectTypesList;
	}

	public String getIsIncludeTeam() {
		return isIncludeTeam;
	}

	public void setIsIncludeTeam(String isIncludeTeam) {
		this.isIncludeTeam = isIncludeTeam;
	}

	public int getQyear() {
		return qyear;
	}

	public void setQyear(int qyear) {
		this.qyear = qyear;
	}

	public List getExitTypeList() {
		return exitTypeList;
	}

	public void setExitTypeList(List exitTypeList) {
		this.exitTypeList = exitTypeList;
	}

	public String getExitType() {
		return exitType;
	}

	public void setExitType(String exitType) {
		this.exitType = exitType;
	}

	public String getSystemType() {
		return systemType;
	}

	public void setSystemType(String systemType) {
		this.systemType = systemType;
	}

	public List getShiftTimingsList() {
		return shiftTimingsList;
	}

	public void setShiftTimingsList(List shiftTimingsList) {
		this.shiftTimingsList = shiftTimingsList;
	}

	public String getShiftTimings() {
		return shiftTimings;
	}

	public void setShiftTimings(String shiftTimings) {
		this.shiftTimings = shiftTimings;
	}

	public boolean getWeekendsSupportFlag() {
		return weekendsSupportFlag;
	}

	public void setWeekendsSupportFlag(boolean weekendsSupportFlag) {
		this.weekendsSupportFlag = weekendsSupportFlag;
	}

	public EmployeeService getEmployeeService() {
		return employeeService;
	}

	public void setEmployeeService(EmployeeService employeeService) {
		this.employeeService = employeeService;
	}

	public boolean isTeamLead() {
		return isTeamLead;
	}

	public void setTeamLead(boolean isTeamLead) {
		this.isTeamLead = isTeamLead;
	}

	public String getInitiatedOn() {
		return initiatedOn;
	}

	public void setInitiatedOn(String initiatedOn) {
		this.initiatedOn = initiatedOn;
	}

	public String getFromLocation() {
		return fromLocation;
	}

	public void setFromLocation(String fromLocation) {
		this.fromLocation = fromLocation;
	}

	public String getToLocation() {
		return toLocation;
	}

	public void setToLocation(String toLocation) {
		this.toLocation = toLocation;
	}

	public String getReasonsForTransfer() {
		return reasonsForTransfer;
	}

	public void setReasonsForTransfer(String reasonsForTransfer) {
		this.reasonsForTransfer = reasonsForTransfer;
	}

	public String getHostelDues() {
		return hostelDues;
	}

	public void setHostelDues(String hostelDues) {
		this.hostelDues = hostelDues;
	}

	public Boolean getIsKeysHandover() {
		return isKeysHandover;
	}

	public void setIsKeysHandover(Boolean isKeysHandover) {
		this.isKeysHandover = isKeysHandover;
	}

	public String getCafteriaDues() {
		return cafteriaDues;
	}

	public void setCafteriaDues(String cafteriaDues) {
		this.cafteriaDues = cafteriaDues;
	}

	public String getTransportationDues() {
		return transportationDues;
	}

	public void setTransportationDues(String transportationDues) {
		this.transportationDues = transportationDues;
	}

	public String getDuesRemarks() {
		return duesRemarks;
	}

	public void setDuesRemarks(String duesRemarks) {
		this.duesRemarks = duesRemarks;
	}

	public String getItTeamStatus() {
		return itTeamStatus;
	}

	public void setItTeamStatus(String itTeamStatus) {
		this.itTeamStatus = itTeamStatus;
	}

	public String getItTeamComments() {
		return itTeamComments;
	}

	public void setItTeamComments(String itTeamComments) {
		this.itTeamComments = itTeamComments;
	}

	public String getReportedTo() {
		return reportedTo;
	}

	public void setReportedTo(String reportedTo) {
		this.reportedTo = reportedTo;
	}

	public String getTentativeReportedDate() {
		return tentativeReportedDate;
	}

	public void setTentativeReportedDate(String tentativeReportedDate) {
		this.tentativeReportedDate = tentativeReportedDate;
	}

	public Boolean getIsReported() {
		return isReported;
	}

	public void setIsReported(Boolean isReported) {
		this.isReported = isReported;
	}

	public String getNextLocationHRComments() {
		return nextLocationHRComments;
	}

	public void setNextLocationHRComments(String nextLocationHRComments) {
		this.nextLocationHRComments = nextLocationHRComments;
	}

	public Map getEmpLocationMap() {
		return empLocationMap;
	}

	public void setEmpLocationMap(Map empLocationMap) {
		this.empLocationMap = empLocationMap;
	}

	public String getActualReportedDate() {
		return actualReportedDate;
	}

	public void setActualReportedDate(String actualReportedDate) {
		this.actualReportedDate = actualReportedDate;
	}

	public int getIsNextLocationHR() {
		return isNextLocationHR;
	}

	public void setIsNextLocationHR(int isNextLocationHR) {
		this.isNextLocationHR = isNextLocationHR;
	}

	public List getTransportLocationList() {
		return transportLocationList;
	}

	public void setTransportLocationList(List transportLocationList) {
		this.transportLocationList = transportLocationList;
	}

	public List getAccommodationList() {
		return accommodationList;
	}

	public void setAccommodationList(List accommodationList) {
		this.accommodationList = accommodationList;
	}

	public int getRoomNo() {
		return roomNo;
	}

	public void setRoomNo(int roomNo) {
		this.roomNo = roomNo;
	}

	public String getAccommodation() {
		return accommodation;
	}

	public void setAccommodation(String accommodation) {
		this.accommodation = accommodation;
	}

	public String getTransportLocation() {
		return transportLocation;
	}

	public void setTransportLocation(String transportLocation) {
		this.transportLocation = transportLocation;
	}

	public double getTransportFee() {
		return transportFee;
	}

	public void setTransportFee(double transportFee) {
		this.transportFee = transportFee;
	}

	public double getCafeteriaFee() {
		return cafeteriaFee;
	}

	public void setCafeteriaFee(double cafeteriaFee) {
		this.cafeteriaFee = cafeteriaFee;
	}

	public double getRoomFee() {
		return roomFee;
	}

	public void setRoomFee(double roomFee) {
		this.roomFee = roomFee;
	}

	public String getCafeteria() {
		return cafeteria;
	}

	public void setCafeteria(String cafeteria) {
		this.cafeteria = cafeteria;
	}

	public String getTransportation() {
		return transportation;
	}

	public void setTransportation(String transportation) {
		this.transportation = transportation;
	}

	public String getOccupancyType() {
		return occupancyType;
	}

	public void setOccupancyType(String occupancyType) {
		this.occupancyType = occupancyType;
	}

	public String getDateOfOccupancy() {
		return dateOfOccupancy;
	}

	public void setDateOfOccupancy(String dateOfOccupancy) {
		this.dateOfOccupancy = dateOfOccupancy;
	}

	public double getElectricalCharges() {
		return electricalCharges;
	}

	public void setElectricalCharges(double electricalCharges) {
		this.electricalCharges = electricalCharges;
	}

	public int getFlag() {
		return flag;
	}

	public void setFlag(int flag) {
		this.flag = flag;
	}

	public void setManager(boolean isManager) {
		this.isManager = isManager;
	}

	public void setCreTeam(boolean isCreTeam) {
		this.isCreTeam = isCreTeam;
	}

	public void setOperationTeam(boolean isOperationTeam) {
		this.isOperationTeam = isOperationTeam;
	}

	public void setPMO(boolean isPMO) {
		this.isPMO = isPMO;
	}

	public void setInternationalWorker(boolean isInternationalWorker) {
		this.isInternationalWorker = isInternationalWorker;
	}

	public String getTrainingType() {
		return trainingType;
	}

	public void setTrainingType(String trainingType) {
		this.trainingType = trainingType;
	}

	public String getResourceComments() {
		return resourceComments;
	}

	public void setResourceComments(String resourceComments) {
		this.resourceComments = resourceComments;
	}

	public boolean isNeedTransport() {
		return needTransport;
	}

	public void setNeedTransport(boolean needTransport) {
		this.needTransport = needTransport;
	}

	public Map getPracticeMap() {
		return practiceMap;
	}

	public void setPracticeMap(Map practiceMap) {
		this.practiceMap = practiceMap;
	}

	public String getShortTermGoalHrComments() {
		return shortTermGoalHrComments;
	}

	public void setShortTermGoalHrComments(String shortTermGoalHrComments) {
		this.shortTermGoalHrComments = shortTermGoalHrComments;
	}

	public String getLongTermGoalHrComments() {
		return longTermGoalHrComments;
	}

	public void setLongTermGoalHrComments(String longTermGoalHrComments) {
		this.longTermGoalHrComments = longTermGoalHrComments;
	}

	public String getStrengthsHrComments() {
		return strengthsHrComments;
	}

	public void setStrengthsHrComments(String strengthsHrComments) {
		this.strengthsHrComments = strengthsHrComments;
	}

	public String getImprovementsHrComments() {
		return improvementsHrComments;
	}

	public void setImprovementsHrComments(String improvementsHrComments) {
		this.improvementsHrComments = improvementsHrComments;
	}

	public String getSearchOperationStatus() {
		return searchOperationStatus;
	}

	public void setSearchOperationStatus(String searchOperationStatus) {
		this.searchOperationStatus = searchOperationStatus;
	}

	public Map getPracticeMapGDC() {
		return practiceMapGDC;
	}

	public void setPracticeMapGDC(Map practiceMapGDC) {
		this.practiceMapGDC = practiceMapGDC;
	}

	public boolean getOutSideAccess() {
		return outSideAccess;
	}

	public void setOutSideAccess(boolean outSideAccess) {
		this.outSideAccess = outSideAccess;
	}

	public String getPrevLiveInCountry() {
		return prevLiveInCountry;
	}

	public void setPrevLiveInCountry(String prevLiveInCountry) {
		this.prevLiveInCountry = prevLiveInCountry;
	}

	public List getUsPayRollBatchList() {
		return usPayRollBatchList;
	}

	public void setUsPayRollBatchList(List usPayRollBatchList) {
		this.usPayRollBatchList = usPayRollBatchList;
	}

	public String getPayRollBatchUs() {
		return payRollBatchUs;
	}

	public void setPayRollBatchUs(String payRollBatchUs) {
		this.payRollBatchUs = payRollBatchUs;
	}

	public String getEmpname() {
		return empname;
	}

	public void setEmpname(String empname) {
		this.empname = empname;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getEmail() {
		return Email;
	}

	public void setEmail(String email) {
		Email = email;
	}

	public String getsMonth() {
		return sMonth;
	}

	public void setsMonth(String sMonth) {
		this.sMonth = sMonth;
	}

	public String getSstarId() {
		return sstarId;
	}

	public void setSstarId(String sstarId) {
		this.sstarId = sstarId;
	}

	public String getTitleTypeId() {
		return titleTypeId;
	}

	public void setTitleTypeId(String titleTypeId) {
		this.titleTypeId = titleTypeId;
	}

	public String getReportBasedOn() {
		return reportBasedOn;
	}

	public void setReportBasedOn(String reportBasedOn) {
		this.reportBasedOn = reportBasedOn;
	}

	public String getProjstartDate() {
		return ProjstartDate;
	}

	public void setProjstartDate(String projstartDate) {
		ProjstartDate = projstartDate;
	}

	public String getStartDateFrom() {
		return startDateFrom;
	}

	public void setStartDateFrom(String startDateFrom) {
		this.startDateFrom = startDateFrom;
	}

	public String getStartDateTo() {
		return startDateTo;
	}

	public void setStartDateTo(String startDateTo) {
		this.startDateTo = startDateTo;
	}

	public String getEndDateFrom() {
		return endDateFrom;
	}

	public void setEndDateFrom(String endDateFrom) {
		this.endDateFrom = endDateFrom;
	}

	public String getEndDateTo() {
		return endDateTo;
	}

	public void setEndDateTo(String endDateTo) {
		this.endDateTo = endDateTo;
	}

}
