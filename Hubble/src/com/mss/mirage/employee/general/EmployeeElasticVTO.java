package com.mss.mirage.employee.general;



/*import io.searchbox.annotations.JestId;
import net.sf.json.JSONObject;*/

public class EmployeeElasticVTO {

    private int empNo;
    private String location;
    private String country;
  
	   

	  
    /*@JestId*/
    private int id;

 

/*	public static EmployeeElasticVTO fromJSON(JSONObject json) {
        return new EmployeeElasticVTO(
        		json.getInt("id"),
                json.getInt("empNo"),
                json.getString("location"),
                json.getString("country")
                
        );
   } */

	
	  public EmployeeElasticVTO(int id, int empNo, String location, String country) {
	    	    this.id=id;
	    	    this.empNo = empNo;
		        this.location = location;
		        this.country = country;
		       
			
		}


	public int getEmpNo() {
		return empNo;
	}


	public void setEmpNo(int empNo) {
		this.empNo = empNo;
	}


	public String getLocation() {
		return location;
	}


	public void setLocation(String location) {
		this.location = location;
	}


	public String getCountry() {
		return country;
	}


	public void setCountry(String country) {
		this.country = country;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	@Override
    public String toString() {
        return "EmployeeEvent{" + "empNo=" + empNo + ", location=" + location + ", country=" + country + '}';
    }


}
