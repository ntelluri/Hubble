package com.mss.mirage.employee.timesheets;


/*import java.sql.Timestamp;



import io.searchbox.annotations.JestId;

import org.json.JSONException;
import org.json.JSONObject;*/

public class BiometricElasticVTO {

/*
    private String attendanceDate;
    private int empNo;
    private String inTime;
    private String inDeviceId;
    private String outTime;
    private String outDeviceId;
    private float duration;
    private int lateBy;
    private int earlyBy;
    private int isOnLeave;
    private String leaveType;
    private float leaveDuration;
   private int weeklyOff;
    private int holiday;
    private String leaveRemarks;
    private String punchRecords;
    private int shiftId;
    private float present;
    private float absent;
    private String status;
    private String statusCode;
   private String p1Status;
    private String p2Status;
    private String p3Status;
    private int isOnSpecialOff;
    private String specialOffType;
    private String specialOffRemark;
    private int specialOffDuration;
    private int overTime;
    private int overTimeE;
    private String remarks;
    private int missedOutPunch;
    private String missedInPunch;
    private String c1;
    private String c2;
    private String c3;
    private String c4;
    private String c5;
    private String c6;
    private String c7;
    private int leaveTypeId;
 //   private int flag;
 //   private int count;
   

  


	@JestId
	private int attendanceLogId;

 

	public static BiometricElasticVTO fromJSON(JSONObject json) throws JSONException {
        return new BiometricElasticVTO(
        		 json.getInt("attendanceLogId"),
                json.getString("attendanceDate"),
                json.getInt("empNo"),
             json.getString("inTime"),
                json.getString("inDeviceId"),
                json.getString("outTime"),
                json.getString("outDeviceId"),
                Float.parseFloat(json.get("duration").toString()),
                json.getInt("lateBy"),
                json.getInt("earlyBy"),
                json.getInt("isOnLeave"),
               json.getString("leaveType"),
               Float.parseFloat(json.get("leaveDuration").toString()),
                json.getInt("weeklyOff"),
                json.getInt("holiday"),
                json.getString("leaveRemarks"),
               json.getString("punchRecords"),
                json.getInt("shiftId"),
                Float.parseFloat(json.get("present").toString()),
                Float.parseFloat(json.get("absent").toString()),
                json.getString("status"),
                json.getString("statusCode"),
                json.getString("p1Status"),
                json.getString("p2Status"),
                json.getString("p3Status"),
                json.getInt("isOnSpecialOff"),
                json.getString("specialOffType"),
                json.getString("specialOffRemark"),
                json.getInt("specialOffDuration"),
                json.getInt("overTime"),
                json.getInt("overTimeE"),
                 json.getInt("missedOutPunch"),
                json.getString("remarks"),
                json.getString("missedInPunch"),
                json.getString("c1"),
                json.getString("c2"),
                json.getString("c3"),
                json.getString("c4"),
                json.getString("c5"),
                json.getString("c6"),
                json.getString("c7"),
        	    json.getInt("leaveTypeId")
             //   json.getInt("flag")
        );
    }
	
	

 



	public BiometricElasticVTO(int attendanceLogId, String attendanceDate, int empNo, String inTime,
			String inDeviceId, String outTime, String outDeviceId, float duration, int lateBy, int earlyBy,
			int isOnLeave, String leaveType, float leaveDuration, int weeklyOff, int holiday,
			String leaveRemarks, String punchRecords, int shiftId, float present, float absent, String status,
			String statusCode, String p1Status, String pStatus, String p3Status, int isOnSpecialOff,
			String specialOffType, String specialOffRemark, int specialOffDuration, int overTime,
			int overTimeE, int missedOutPunch,String remarks , String missedInPunch, String c1, String c2,
			String c3, String c4, String c5, String c6, String c7, int leaveTypeId) {

		// TODO Auto-generated constructor stub
		
		  this.attendanceLogId = attendanceLogId;
	        this.attendanceDate = attendanceDate;
	        this.empNo = empNo;
	        this.inTime = inTime;
	        this.inDeviceId = inDeviceId;
	        this.outTime = outTime;
	        this.outDeviceId = outDeviceId;
	        this.duration = duration;
	       this.lateBy=lateBy;
	        this.earlyBy=earlyBy;
	        this.isOnLeave=isOnLeave;
	        this.leaveType = leaveType;
	        this.leaveDuration = leaveDuration;
	        this.weeklyOff = weeklyOff;
	        this.holiday = holiday;
	        this.leaveRemarks = leaveRemarks;
	        this.punchRecords = punchRecords;
	        this.shiftId = shiftId;
	        this.present = present;
	        this.absent = absent;
	        this.status = status;
	        this.statusCode = statusCode;
	       this.p1Status = p1Status;
	        this.p2Status = p2Status;
	        this.p3Status = p3Status;
	        this.isOnSpecialOff = isOnSpecialOff;
	        this.specialOffType = specialOffType;
	        this.specialOffRemark = specialOffRemark;
	        this.specialOffDuration = specialOffDuration;
	        this.overTime = overTime;
	        this.overTimeE = overTimeE;
	        this.remarks = remarks;
	        this.missedOutPunch = missedOutPunch;
            this.missedInPunch = missedInPunch;
	        this.c1 = c1;
	        this.c2 = c2;
	        this.c3 = c3;
	        this.c4 = c4;
	        this.c5 = c5;
            this.c6 = c6;
	        this.c7 = c7;
            this.leaveTypeId = leaveTypeId;
           
	}


	













	public int getAttendanceLogId() {
		 System.out.println("   get attendanceLogId===>" + attendanceLogId);
		return attendanceLogId;
	}


	public void setAttendanceLogId(int attendanceLogId) {
		 System.out.println("attendanceLogId =======set==========>" + attendanceLogId);
		this.attendanceLogId = attendanceLogId;
	}


	public String getAttendanceDate() {
		return attendanceDate;
	}


	public void setAttendanceDate(String attendanceDate) {
		this.attendanceDate = attendanceDate;
	}


	public int getEmpNo() {
		return empNo;
	}


	public void setEmpNo(int empNo) {
		this.empNo = empNo;
	}


	public String getInTime() {
		return inTime;
	}


	public void setInTime(String inTime) {
		this.inTime = inTime;
	}


	public String getInDeviceId() {
		return inDeviceId;
	}


	public void setInDeviceId(String inDeviceId) {
		this.inDeviceId = inDeviceId;
	}


	public String getOutTime() {
		return outTime;
	}


	public void setOutTime(String outTime) {
		this.outTime = outTime;
	}


	public String getOutDeviceId() {
		return outDeviceId;
	}


	public void setOutDeviceId(String outDeviceId) {
		this.outDeviceId = outDeviceId;
	}


	


	public String getLeaveType() {
		return leaveType;
	}




	public int getWeeklyOff() {
		return weeklyOff;
	}


	public void setWeeklyOff(int weeklyOff) {
		this.weeklyOff = weeklyOff;
	}


	public int getHoliday() {
		return holiday;
	}


	public void setHoliday(int holiday) {
		this.holiday = holiday;
	}


	public String getPunchRecords() {
		return punchRecords;
	}


	public void setPunchRecords(String punchRecords) {
		this.punchRecords = punchRecords;
	}


	public int getShiftId() {
		return shiftId;
	}


	public void setShiftId(int shiftId) {
		this.shiftId = shiftId;
	}


	


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}


	public String getStatusCode() {
		return statusCode;
	}


	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}


	public String getP1Status() {
		return p1Status;
	}


	public void setP1Status(String p1Status) {
		this.p1Status = p1Status;
	}


	

	


	public int getLateBy() {
		return lateBy;
	}


	public void setLateBy(int lateBy) {
		this.lateBy = lateBy;
	}


	public int getEarlyBy() {
		return earlyBy;
	}


	public void setEarlyBy(int earlyBy) {
		this.earlyBy = earlyBy;
	}


	public int getIsOnLeave() {
		return isOnLeave;
	}


	public void setIsOnLeave(int isOnLeave) {
		this.isOnLeave = isOnLeave;
	}


	public String getLeaveRemarks() {
		return leaveRemarks;
	}


	public void setLeaveRemarks(String leaveRemarks) {
		this.leaveRemarks = leaveRemarks;
	}


	public String getP2Status() {
		return p2Status;
	}


	public void setP2Status(String p2Status) {
		this.p2Status = p2Status;
	}


	public String getP3Status() {
		return p3Status;
	}


	public void setP3Status(String p3Status) {
		this.p3Status = p3Status;
	}


	public int getIsOnSpecialOff() {
		return isOnSpecialOff;
	}


	public void setIsOnSpecialOff(int isOnSpecialOff) {
		this.isOnSpecialOff = isOnSpecialOff;
	}


	public String getSpecialOffType() {
		return specialOffType;
	}


	public void setSpecialOffType(String specialOffType) {
		this.specialOffType = specialOffType;
	}


	public String getSpecialOffRemark() {
		return specialOffRemark;
	}


	public void setSpecialOffRemark(String specialOffRemark) {
		this.specialOffRemark = specialOffRemark;
	}


	public int getSpecialOffDuration() {
		return specialOffDuration;
	}


	public void setSpecialOffDuration(int specialOffDuration) {
		this.specialOffDuration = specialOffDuration;
	}


	public int getOverTime() {
		return overTime;
	}


	public void setOverTime(int overTime) {
		this.overTime = overTime;
	}


	public int getOverTimeE() {
		return overTimeE;
	}


	public void setOverTimeE(int overTimeE) {
		this.overTimeE = overTimeE;
	}


	public String getRemarks() {
		return remarks;
	}


	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}


	public int getMissedOutPunch() {
		return missedOutPunch;
	}


	public void setMissedOutPunch(int missedOutPunch) {
		this.missedOutPunch = missedOutPunch;
	}


	public String getMissedInPunch() {
		return missedInPunch;
	}


	public void setMissedInPunch(String missedInPunch) {
		this.missedInPunch = missedInPunch;
	}


	public String getC1() {
		return c1;
	}


	public void setC1(String c1) {
		this.c1 = c1;
	}


	public String getC2() {
		return c2;
	}


	public void setC2(String c2) {
		this.c2 = c2;
	}


	public String getC3() {
		return c3;
	}


	public void setC3(String c3) {
		this.c3 = c3;
	}


	public String getC4() {
		return c4;
	}


	public void setC4(String c4) {
		this.c4 = c4;
	}


	public String getC5() {
		return c5;
	}


	public void setC5(String c5) {
		this.c5 = c5;
	}


	public String getC6() {
		return c6;
	}


	public void setC6(String c6) {
		this.c6 = c6;
	}


	public String getC7() {
		return c7;
	}


	public void setC7(String c7) {
		this.c7 = c7;
	}


	public int getLeaveTypeId() {
		return leaveTypeId;
	}


	public void setLeaveTypeId(int leaveTypeId) {
		this.leaveTypeId = leaveTypeId;
	}


	public float getDuration() {
		return duration;
	}


	public void setDuration(float duration) {
		this.duration = duration;
	}


	public float getLeaveDuration() {
		return leaveDuration;
	}


	public void setLeaveDuration(float leaveDuration) {
		this.leaveDuration = leaveDuration;
	}


	public float getPresent() {
		return present;
	}


	public void setPresent(float present) {
		this.present = present;
	}


	public float getAbsent() {
		return absent;
	}


	public void setAbsent(float absent) {
		this.absent = absent;
	}


	public void setLeaveType(String leaveType) {
		this.leaveType = leaveType;
	}


	@Override
    public String toString() {
		 return "BiometricElasticVTO{" + "attendanceLogId=" + attendanceLogId + ", attendanceDate=" + attendanceDate + ", empNo=" + empNo + ", inTime=" + inTime + ", inDeviceId=" + inDeviceId + ", outTime=" + outTime + ", outDeviceId=" + outDeviceId + ", duration=" + duration + ","
		 		+ "	lateBy=" + lateBy + ", earlyBy=" + earlyBy + ",  isOnLeave=" + isOnLeave + ","
				+ "leaveType=" + leaveType + ", leaveDuration=" + leaveDuration + ",  weeklyOff=" + weeklyOff + ", holiday=" + holiday + ","
				+ "punchRecords=" + punchRecords + ", shiftId=" + shiftId + ", present=" + present + ", absent=" + absent + ", status=" + status + ","
				+ "statusCode=" + statusCode + ", p1Status=" + p1Status + "," 
				+ " p2Status=" + p2Status + ", p3Status=" + p3Status + "," 
				+  "isOnSpecialOff=" + isOnSpecialOff + ", specialOffRemark=" + specialOffRemark + "," 
				+  "specialOffDuration=" + specialOffDuration + ", overTime=" + overTime + "," 
				+	"overTimeE=" + overTimeE + ", remarks=" + remarks + "," 
				+	  "missedOutPunch=" + missedOutPunch + ", missedInPunch=" + missedInPunch + "," 
				 +"c1=" + c1 + ", c2=" + c2 + "," 
				 +"c3=" + c3 + ", c4=" + c4 + "," 
				+"c5=" + c5 + ", c6=" + c6 + "," 
				 +"c7=" + c7 + ", leaveTypeId=" + leaveTypeId + '}';
				// +"flag=" + flag + '}';
				   }

	
//	@Override
   // public String toString() {
	//	 return "BiometricElasticVTO{" + "attendanceLogId=" + attendanceLogId + ", attendanceDate=" + attendanceDate + ", empNo=" + empNo + ", duration=" + duration + ","
	//	 		+ " weeklyOff=" + weeklyOff + ", holiday=" + holiday + ","
	//			+ "punchRecords=" + punchRecords + ",  present=" + present + ", absent=" + absent + ", status=" + status + ","
	//			+ "statusCode=" + statusCode  + '}';
				// +"flag=" + flag + '}';
			//	   }
	    */
	
}
