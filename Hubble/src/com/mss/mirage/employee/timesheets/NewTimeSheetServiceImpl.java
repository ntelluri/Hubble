/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mss.mirage.employee.timesheets;

import com.mss.mirage.util.ConnectionProvider;
import com.mss.mirage.util.DataSourceDataProvider;
import com.mss.mirage.util.DateUtility;
import com.mss.mirage.util.Properties;
import com.mss.mirage.util.ReportProperties;
//import com.mss.mirage.util.LoggerManager;
import com.mss.mirage.util.ServiceLocatorException;
import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.view.JasperViewer;




import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.util.CellRangeAddress;



/**
 *
 * @author miracle
 */
public class NewTimeSheetServiceImpl implements NewTimeSheetService{
    
    
     public List getweekStartAndEndDays(Calendar cal) {
        
        /* used to store the totla weekdays. */
        List daysList = new ArrayList();
        
        /* Used for Storing the Week Start Date */
        String wstDate = null;
        
        /* Used for Storing the Week End Date */
        String wenDate = null;
        
        /* used for the current date */
        String curntDate=null;
        
        /* for stroring the weekdays */
        String [] weekDays = new String[7];
        
        /* to get the day of week */
        int w = cal.get(Calendar.DAY_OF_WEEK);
        
        /* if its sunday then the index is 0 other then sunday then minus the index */
        if(w==1) {
            cal.add(Calendar.DATE,0);
            
        } else if (w==2) {
            cal.add(Calendar.DATE,-1);
            
        } else if (w==3) {
            cal.add(Calendar.DATE,-2);
            
        } else if(w==4) {
            cal.add(Calendar.DATE,-3);
            
        } else if(w==5) {
            cal.add(Calendar.DATE,-4);
            
        } else if(w==6) {
            cal.add(Calendar.DATE,-5);
            
        } else if(w==7) {
            cal.add(Calendar.DATE,-6);
            
        }
        
        /* for generating the month/day sequence of the week */
        int zeroForMon=0; // if month is single digit then Zero is append to left of that digit
        int zeroForDay=0; // if day is single digit then Zero is append to left of that digit
        for(int index=0;index<7;index++) {
            
            /* for the purpose of concatinating 0 before the day and month */
            zeroForMon=(cal.get(Calendar.MONTH)+1);
            zeroForDay=cal.get(Calendar.DAY_OF_MONTH);
            
            if(zeroForMon<10 && zeroForDay <10) {
                weekDays[index]="0"+(cal.get(Calendar.MONTH)+1) +"/0" + cal.get(Calendar.DAY_OF_MONTH)+"/"+(cal.get(Calendar.YEAR));
                
            } else
                if(zeroForMon >9 && zeroForDay <10) {
                weekDays[index]=(cal.get(Calendar.MONTH)+1) +"/0" + cal.get(Calendar.DAY_OF_MONTH)+"/"+(cal.get(Calendar.YEAR));
                
                } else
                    if(zeroForMon <10 && zeroForDay >9) {
                weekDays[index]="0"+(cal.get(Calendar.MONTH)+1) +"/" + cal.get(Calendar.DAY_OF_MONTH)+"/"+(cal.get(Calendar.YEAR));
                
                
                    } else {
                weekDays[index]=(cal.get(Calendar.MONTH)+1) +"/" + cal.get(Calendar.DAY_OF_MONTH)+"/"+(cal.get(Calendar.YEAR));
                
                    }
            cal.add(Calendar.DAY_OF_MONTH,1);
        }// End for loop
        
        wstDate =weekDays[0];  // storing the week startday
        wenDate =  weekDays[6]; // storing the week endday
        
        /* current date */
        Calendar currDay = Calendar.getInstance();
        zeroForMon = (cal.get(Calendar.MONTH) + 1);
        zeroForDay = cal.get(Calendar.DAY_OF_MONTH);
        if (zeroForMon < 10 && zeroForDay < 10) {
            curntDate = "0" +(currDay.get(Calendar.MONTH) + 1) + "/0" + currDay.get(Calendar.DAY_OF_MONTH) + "/" + currDay.get(Calendar.YEAR);

        } else if (zeroForMon > 9 && zeroForDay < 10) {
             curntDate = (currDay.get(Calendar.MONTH) + 1) + "/0" + currDay.get(Calendar.DAY_OF_MONTH) + "/" + currDay.get(Calendar.YEAR);
        } else if (zeroForMon < 10 && zeroForDay > 9) {
             curntDate =  "0" + (currDay.get(Calendar.MONTH) + 1) + "/" + currDay.get(Calendar.DAY_OF_MONTH) + "/" + currDay.get(Calendar.YEAR);
        } else {
             curntDate = (currDay.get(Calendar.MONTH) + 1) + "/" + currDay.get(Calendar.DAY_OF_MONTH) + "/" + currDay.get(Calendar.YEAR);

        }
      //  curntDate = (currDay.get(Calendar.MONTH)+1) + "/" + currDay.get(Calendar.DAY_OF_MONTH)+"/"+currDay.get(Calendar.YEAR) ;
        daysList.add(wstDate);
        daysList.add(wenDate);
        daysList.add(curntDate);
        daysList.add(weekDays);
        
        return daysList;
    } // @ getweekStartAndEndDays(Calendar cal)
    
     
      public String checkTimeSheetExists(List li, String empID,String empType) {
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        String isTimeSheetExist = "";
        try {
            // convert the date to string
            String wstDate = DateUtility.getInstance().convertDateToMySql(DateUtility.getInstance().convertStringToMySql((String)li.get(0)));
            String wenDate = DateUtility.getInstance().convertDateToMySql(DateUtility.getInstance().convertStringToMySql((String)li.get(1)));
            
            Date weekStarDate = DateUtility.getInstance().convertStringToMySql((String)li.get(0));
            Date currDate = DateUtility.getInstance().convertStringToMySql((String)li.get(2));
            
            connection = ConnectionProvider.getInstance().getConnection();
            
            if(connection != null) {
                statement = connection.createStatement();
                
                // select the data from vwTimeSheetList of DataBase.
               /* if("e".equalsIgnoreCase(empType))
                resultSet =statement.executeQuery("SELECT *  FROM vwNewTimeSheetList WHERE EmpId="+Integer.parseInt(empID)+" AND DateStart='"+wstDate+"' AND DateEnd='"+wenDate+"'");
                else
                  resultSet =statement.executeQuery("SELECT *  FROM vwNewCustTimeSheetList WHERE EmpId="+Integer.parseInt(empID)+" AND DateStart='"+wstDate+"' AND DateEnd='"+wenDate+"'");
               */
                if("e".equalsIgnoreCase(empType))
                resultSet =statement.executeQuery("SELECT Id FROM tblTimeSheets WHERE ResourceType='E' AND EmpId="+Integer.parseInt(empID)+" AND DATE(DateStart)=DATE('"+wstDate+"') AND DATE(DateEnd)=DATE('"+wenDate+"')");
                else
                	resultSet =statement.executeQuery("SELECT Id FROM tblTimeSheets WHERE ResourceType='c' AND EmpId="+Integer.parseInt(empID)+" AND DATE(DateStart)=DATE('"+wstDate+"') AND DATE(DateEnd)=DATE('"+wenDate+"')");	
                
                
                if(resultSet.next()) {
                    isTimeSheetExist = "exist";
                }else if(currDate.before(weekStarDate)){
                    isTimeSheetExist = "notallow";
                }else{
                    isTimeSheetExist = "allow";
                }
                
            } //if
            
            
        } // try
        catch(Exception ex) {
            ex.printStackTrace();
            //log.setLevel((Level)Level.ERROR);
            //log.error("The Error @ checkTimeSheetExists()",ex);
        } // catch
        finally{
            try {
              if(resultSet != null) {
                 resultSet.close();
                 resultSet = null;
                }
              if(statement != null) {
                 statement.close();
                 statement = null;
                }
            if(connection != null){
               connection.close();
               connection = null;
                } 
                
            }catch (SQLException ex) {
                    ex.printStackTrace();
                }
        }
        
        return isTimeSheetExist;
    } // @checkTimeSheetExists
      
      
      
        public int deleteTimeSheet(int id, int empId, int timeSheetId) throws ServiceLocatorException{
        
       // System.out.println("in service impl");
        int deletedRows = 0;
        Connection connection = null;
        Statement statement = null;
        String queryString = "Delete from tblTimeSheets  WHERE id="+id;
        String queryString1 = "Delete from tblTimeSheetLines WHERE EmpId='"+empId+"' AND TimeSheetId='"+timeSheetId+"'";
        try{
            connection = ConnectionProvider.getInstance().getConnection();
            statement = connection.createStatement();
            deletedRows = statement.executeUpdate(queryString);
            deletedRows = statement.executeUpdate(queryString1);
            
        }catch (SQLException se){
            throw new ServiceLocatorException(se);
        }finally{
            try{
                if(statement != null){
                    statement.close();
                    statement = null;
                }
                if(connection != null){
                    connection.close();
                    connection = null;
                }
            }catch (SQLException se){
                throw new ServiceLocatorException(se);
            }
        }
        return deletedRows;
    }
    
      
       public NewTimeSheetVTO getWeekDaysBean(List li) {
        NewTimeSheetVTO timeSheetVTO = new  NewTimeSheetVTO();
        
        /* Used for Storing the Week Start Date */
        timeSheetVTO.setWstDate((String)li.get(0));
        
        /* Used for Storing the Week End Date */
        timeSheetVTO.setWenDate((String)li.get(1));
        
        /* Used for Storing the Week Submitted Date */
        timeSheetVTO.setSubmittedDate((String)li.get(2));
        
        /* for storing the seven weekdays */
        String [] weekDaysSequence=(String [])li.get(3);
        
        timeSheetVTO.setWeekDate1(weekDaysSequence[0]);
        timeSheetVTO.setWeekDate2(weekDaysSequence[1]);
        timeSheetVTO.setWeekDate3(weekDaysSequence[2]);
        timeSheetVTO.setWeekDate4(weekDaysSequence[3]);
        timeSheetVTO.setWeekDate5(weekDaysSequence[4]);
        timeSheetVTO.setWeekDate6(weekDaysSequence[5]);
        timeSheetVTO.setWeekDate7(weekDaysSequence[6]);
        
        /* to set the action Type */
        //timeSheetVTO.setModeType("AddTimeSheet");
        timeSheetVTO.setModeType("newAddTimeSheet");
        
        return timeSheetVTO; //  return the bean reference
    } // @ getWeekDaysBean(List)
      
       
       
public int newAddTimeSheet(NewTimeSheetAction timeSheetAction) throws ServiceLocatorException {
        
        /* for stroing the approvied date */
        int isTimesheetAddOrEdit = 0;
        java.sql.Date approDate = null;
        Connection connection = null;
        CallableStatement callableStatement = null;
        ResultSet resultSet = null;
        
        if(timeSheetAction.getApproveDate()!=null && !("".equalsIgnoreCase(timeSheetAction.getApproveDate()))) {
            
            /* if employee not enter the approved date then we fill default value to table */
            approDate = DateUtility.getInstance().getMysqlDate(timeSheetAction.getApproveDate());
        } else {
            
            approDate = null;
        } //if
        
        try {
            
            /* getting the connection from the ConnectionProvider */
            connection = ConnectionProvider.getInstance().getConnection();
            if(connection != null) {
                callableStatement = connection.prepareCall("{call spNewTimeSheetAdd(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
                callableStatement.setInt(1,Integer.parseInt(timeSheetAction.getEmpID()));
                callableStatement.setInt(2,1);
                callableStatement.setInt(3,0);
                
                /* converting the string object to date object*/
                callableStatement.setDate(4,DateUtility.getInstance().getMysqlDate(timeSheetAction.getWstDate()));
                callableStatement.setDate(5,DateUtility.getInstance().getMysqlDate(timeSheetAction.getWenDate()));
                callableStatement.setDate(6,approDate);
                callableStatement.setDate(7,DateUtility.getInstance().getMysqlDate(timeSheetAction.getSubmittedDate()));
                callableStatement.setDouble(8,timeSheetAction.getTotalBillHrs());
                callableStatement.setDouble(9,timeSheetAction.getProj1Sun());
                callableStatement.setDouble(10,timeSheetAction.getProj1Mon());
                callableStatement.setDouble(11,timeSheetAction.getProj1Tus());
                callableStatement.setDouble(12,timeSheetAction.getProj1Wed());
                callableStatement.setDouble(13,timeSheetAction.getProj1Thur());
                callableStatement.setDouble(14,timeSheetAction.getProj1Fri());
                callableStatement.setDouble(15,timeSheetAction.getProj1Sat());
                callableStatement.setDouble(16,timeSheetAction.getProj2Sun());
                callableStatement.setDouble(17,timeSheetAction.getProj2Mon());
                callableStatement.setDouble(18,timeSheetAction.getProj2Tus());
                callableStatement.setDouble(19,timeSheetAction.getProj2Wed());
                callableStatement.setDouble(20,timeSheetAction.getProj2Thur());
                callableStatement.setDouble(21,timeSheetAction.getProj2Fri());
                callableStatement.setDouble(22,timeSheetAction.getProj2Sat());
                
                  callableStatement.setDouble(23,timeSheetAction.getProj3Sun());
                callableStatement.setDouble(24,timeSheetAction.getProj3Mon());
                callableStatement.setDouble(25,timeSheetAction.getProj3Tus());
                callableStatement.setDouble(26,timeSheetAction.getProj3Wed());
                callableStatement.setDouble(27,timeSheetAction.getProj3Thur());
                callableStatement.setDouble(28,timeSheetAction.getProj3Fri());
                callableStatement.setDouble(29,timeSheetAction.getProj3Sat());
                
                 callableStatement.setDouble(30,timeSheetAction.getProj4Sun());
                callableStatement.setDouble(31,timeSheetAction.getProj4Mon());
                callableStatement.setDouble(32,timeSheetAction.getProj4Tus());
                callableStatement.setDouble(33,timeSheetAction.getProj4Wed());
                callableStatement.setDouble(34,timeSheetAction.getProj4Thur());
                callableStatement.setDouble(35,timeSheetAction.getProj4Fri());
                callableStatement.setDouble(36,timeSheetAction.getProj4Sat());
                
                callableStatement.setDouble(37,timeSheetAction.getProj5Sun());
                callableStatement.setDouble(38,timeSheetAction.getProj5Mon());
                callableStatement.setDouble(39,timeSheetAction.getProj5Tus());
                callableStatement.setDouble(40,timeSheetAction.getProj5Wed());
                callableStatement.setDouble(41,timeSheetAction.getProj5Thur());
                callableStatement.setDouble(42,timeSheetAction.getProj5Fri());
                callableStatement.setDouble(43,timeSheetAction.getProj5Sat());
                
                
                
                callableStatement.setDouble(44,timeSheetAction.getInternalSun());
                callableStatement.setDouble(45,timeSheetAction.getInternalMon());
                callableStatement.setDouble(46,timeSheetAction.getInternalTus());
                callableStatement.setDouble(47,timeSheetAction.getInternalWed());
                callableStatement.setDouble(48,timeSheetAction.getInternalThur());
                callableStatement.setDouble(49,timeSheetAction.getInternalFri());
                callableStatement.setDouble(50,timeSheetAction.getInternalSat());
                callableStatement.setDouble(51,timeSheetAction.getVacationSun());
                callableStatement.setDouble(52,timeSheetAction.getVacationMon());
                callableStatement.setDouble(53,timeSheetAction.getVacationTus());
                callableStatement.setDouble(54,timeSheetAction.getVacationWed());
                callableStatement.setDouble(55,timeSheetAction.getVacationThur());
                callableStatement.setDouble(56,timeSheetAction.getVacationFri());
                callableStatement.setDouble(57,timeSheetAction.getVacationSat());
                callableStatement.setDouble(58,timeSheetAction.getHoliSun());
                callableStatement.setDouble(59,timeSheetAction.getHoliMon());
                callableStatement.setDouble(60,timeSheetAction.getHoliTus());
                callableStatement.setDouble(61,timeSheetAction.getHoliWed());
                callableStatement.setDouble(62,timeSheetAction.getHoliThur());
                callableStatement.setDouble(63,timeSheetAction.getHoliFri());
                callableStatement.setDouble(64,timeSheetAction.getHoliSat());
                
                 callableStatement.setDouble(65,timeSheetAction.getCompSun());
                callableStatement.setDouble(66,timeSheetAction.getCompMon());
                callableStatement.setDouble(67,timeSheetAction.getCompTus());
                callableStatement.setDouble(68,timeSheetAction.getCompWed());
                callableStatement.setDouble(69,timeSheetAction.getCompThur());
                callableStatement.setDouble(70,timeSheetAction.getCompFri());
                callableStatement.setDouble(71,timeSheetAction.getCompSat());
                
                // converting the String to sql Date object.
                callableStatement.setDate(72,DateUtility.getInstance().getMysqlDate(timeSheetAction.getWeekDate1()));
                callableStatement.setDate(73,DateUtility.getInstance().getMysqlDate(timeSheetAction.getWeekDate2()));
                callableStatement.setDate(74,DateUtility.getInstance().getMysqlDate(timeSheetAction.getWeekDate3()));
                callableStatement.setDate(75,DateUtility.getInstance().getMysqlDate(timeSheetAction.getWeekDate4()));
                callableStatement.setDate(76,DateUtility.getInstance().getMysqlDate(timeSheetAction.getWeekDate5()));
                callableStatement.setDate(77,DateUtility.getInstance().getMysqlDate(timeSheetAction.getWeekDate6()));
                callableStatement.setDate(78,DateUtility.getInstance().getMysqlDate(timeSheetAction.getWeekDate7()));
                callableStatement.setString(79,timeSheetAction.getTxtNotes());
                callableStatement.setString(80,"Ins");
                callableStatement.setInt(81,timeSheetAction.getPriProjId());//inserting project id as 0
                callableStatement.setInt(82,timeSheetAction.getProj1Id());
                callableStatement.setInt(83,timeSheetAction.getProj2Id());
                callableStatement.setInt(84,timeSheetAction.getProj3Id());
                callableStatement.setInt(85,timeSheetAction.getProj4Id());
                callableStatement.setInt(86,timeSheetAction.getProj5Id());
                
                
                callableStatement.setDouble(87,timeSheetAction.getBiometricSun());
                callableStatement.setDouble(88,timeSheetAction.getBiometricMon());
                callableStatement.setDouble(89,timeSheetAction.getBiometricTus());
                callableStatement.setDouble(90,timeSheetAction.getBiometricWed());
                callableStatement.setDouble(91,timeSheetAction.getBiometricThur());
                callableStatement.setDouble(92,timeSheetAction.getBiometricFri());
                callableStatement.setDouble(93,timeSheetAction.getBiometricSat());
                callableStatement.setDouble(94,timeSheetAction.getBiometricTotalHrs());
                
                //gopalakrishna
                callableStatement.setDouble(95,timeSheetAction.getTotalHoliHrs());
                callableStatement.setDouble(96,timeSheetAction.getTotalVacationHrs());
                callableStatement.setDouble(97,timeSheetAction.getTotalComptimeHrs());
                callableStatement.setString(98,timeSheetAction.getEmpType().toUpperCase());
                callableStatement.setInt(99,timeSheetAction.getTempVar());
                callableStatement.setString(100,timeSheetAction.getAttachmentLocation());
                callableStatement.registerOutParameter(101,java.sql.Types.INTEGER);
                callableStatement.execute() ;
                int timeSheetId = callableStatement.getInt(101); 
                if(timeSheetId<1) // for retriving the timesheetID
                { isTimesheetAddOrEdit = timeSheetId;
                  //log.debug("The Error occur while inserting the TimeSheet for EmpId"+timeSheetAction.getEmpID());
                } else {
                    isTimesheetAddOrEdit = timeSheetId;
                    // log.debug("The TimeSheet Successfully Added For EmpId:"+timeSheetAction.getEmpID()+"with TimeSheetId:"+callableStatement.getInt(53));
                }// if
                
            } //if
            
            
        }//try
        catch(Exception ex) {
           // ex.printStackTrace();
            throw new ServiceLocatorException(ex);
            //log.debug("The Error @addTimeSheet()",ex);
        } finally {
            
            try {
                if(callableStatement!=null){
                    callableStatement.close();
                    callableStatement = null;
                }
                if(connection != null){
                    connection.close();
                    connection = null;
                }
            } catch (SQLException ex) {
                throw new ServiceLocatorException(ex);
                //log.debug("The Error @addTimeSheet()",ex);
            } //try
        } //finally
        return isTimesheetAddOrEdit;
    }


public NewTimeSheetVTO newGetTimeSheet(int empId, int timeSheetID,String empType,String resourceType) {
        NewTimeSheetVTO timeSheetVTO = new NewTimeSheetVTO();
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        String queryString = "";
        String endDateString = "";
        int rowCount=1;
        try {
            
            /* getting the connection from the ConnectionProvider */
            connection = ConnectionProvider.getInstance().getConnection();
            
            if(connection!=null) {
                
                statement = connection.createStatement();
                
                /* taking the info from the tblTimeSheets based on the empId and timeSheet Id */
            //   resultSet = statement.executeQuery("SELECT EmpId,DateStart,DateEnd,SubmittedDate,ApprovedDate,TotalHrs,Notes,ProjectId  FROM tblTimeSheets WHERE EmpId="+empId+" AND TimeSheetId="+timeSheetID+"");
                 resultSet = statement.executeQuery("SELECT EmpId,DateStart,DateEnd,SubmittedDate,ApprovedDate,TotalHrs,Notes,ProjectId,TimeSheetStatusTypeId,SecondReportsToStatusTypeId,FileFlag,BmFlag FROM tblTimeSheets WHERE EmpId="+empId+" AND TimeSheetId="+timeSheetID+"");
               //log=LoggerManager.getLog(TimeSheetServiceImpl.class.getName());
                while(resultSet.next()) {
                    timeSheetVTO.setWstDate(DateUtility.getInstance().convertDateToView(resultSet.getDate(2)));
                    timeSheetVTO.setWenDate(DateUtility.getInstance().convertDateToView(resultSet.getDate(3)));
                    timeSheetVTO.setSubmittedDate(DateUtility.getInstance().convertDateToView(resultSet.getDate(4)));
                  //  System.out.println("setApproveDate--->"+resultSet.getDate(5));
                   
                   // if("")
                 //   timeSheetVTO.setApproveDate(DateUtility.getInstance().convertDateToView(resultSet.getDate(5)));
                    if(resultSet.getInt(9)==2){
                 	      timeSheetVTO.setApproveDate(com.mss.mirage.util.DateUtility.getInstance().getToDayDateToView());
                        }  else {
                    	  timeSheetVTO.setApproveDate(DateUtility.getInstance().convertDateToView(resultSet.getDate(5)));
                        } 
                    
                    timeSheetVTO.setTotalBillHrs(resultSet.getDouble(6));
                    timeSheetVTO.setNotes(resultSet.getString(7));
                    //timeSheetVTO.setProjectId(resultSet.getInt(8));
                    timeSheetVTO.setPriProjId(resultSet.getInt(8));
                      timeSheetVTO.setTimeSheetStatusTypeId(resultSet.getInt(9));
                    timeSheetVTO.setSecondReportsToStatusTypeId(resultSet.getInt(10));
                     timeSheetVTO.setFileFlagValue(resultSet.getInt(11));
                     timeSheetVTO.setBiometricFlag(resultSet.getInt(12));

                } // while
                resultSet.close(); // clsoing connection
                resultSet = null;
                /* from tblTimeSheetLines getting full week of the TimeSheet */
               // resultSet = statement.executeQuery("SELECT WorkDate,Prj1Hrs,Prj2Hrs,InternalHrs,VacationHrs,HolidayHrs  FROM tblTimeSheetLines WHERE EmpId="+empId+" AND TimeSheetId="+timeSheetID+"");
                // resultSet = statement.executeQuery("SELECT WorkDate,Prj1Hrs,Prj2Hrs,InternalHrs,VacationHrs,HolidayHrs,CmtHrs,Prj3Hrs,Prj4Hrs,Prj5Hrs,PrjId1,PrjId2,PrjId3,PrjId4,PrjId5 FROM tblTimeSheetLines WHERE EmpId="+empId+" AND TimeSheetId="+timeSheetID+"");
                 resultSet = statement.executeQuery("SELECT WorkDate,Prj1Hrs,Prj2Hrs,InternalHrs,VacationHrs,HolidayHrs,CmtHrs,Prj3Hrs,Prj4Hrs,Prj5Hrs,PrjId1,PrjId2,PrjId3,PrjId4,PrjId5,BmHrs,BmStatus FROM tblTimeSheetLines WHERE EmpId="+empId+" AND TimeSheetId="+timeSheetID+"");
                 DecimalFormat df = new DecimalFormat("0.00");
                while(resultSet.next()) {
                    
                    /* Work days of sunday */
                    if(rowCount==1) {
                        timeSheetVTO.setWeekDate1(DateUtility.getInstance().convertDateToView(resultSet.getDate(1)));
                        timeSheetVTO.setProj1Sun(Double.parseDouble(df.format(resultSet.getDouble(2))));
                        timeSheetVTO.setProj2Sun(Double.parseDouble(df.format(resultSet.getDouble(3))));
                        timeSheetVTO.setInternalSun(Double.parseDouble(df.format(resultSet.getDouble(4))));
                        timeSheetVTO.setVacationSun(Double.parseDouble(df.format(resultSet.getDouble(5))));
                        timeSheetVTO.setHoliSun(Double.parseDouble(df.format(resultSet.getDouble(6))));
                        timeSheetVTO.setCompSun(Double.parseDouble(df.format(resultSet.getDouble(7))));
                        timeSheetVTO.setProj3Sun(Double.parseDouble(df.format(resultSet.getDouble(8))));
                        timeSheetVTO.setProj4Sun(Double.parseDouble(df.format(resultSet.getDouble(9))));
                        timeSheetVTO.setProj5Sun(Double.parseDouble(df.format(resultSet.getDouble(10))));
                        timeSheetVTO.setProj1Id(resultSet.getInt(11));
                        timeSheetVTO.setProj2Id(resultSet.getInt(12));
                        timeSheetVTO.setProj3Id(resultSet.getInt(13));
                        timeSheetVTO.setProj4Id(resultSet.getInt(14));
                        timeSheetVTO.setProj5Id(resultSet.getInt(15));
                        timeSheetVTO.setBiometricSun(Double.parseDouble(df.format(resultSet.getDouble(16))));
                        timeSheetVTO.setBmSunStatus(resultSet.getString(17));
                    } else if(rowCount==2) {
                        
                        /* Work days of monday */
                        timeSheetVTO.setWeekDate2(DateUtility.getInstance().convertDateToView(resultSet.getDate(1)));
                        timeSheetVTO.setProj1Mon(Double.parseDouble(df.format(resultSet.getDouble(2))));
                        timeSheetVTO.setProj2Mon(Double.parseDouble(df.format(resultSet.getDouble(3))));
                        timeSheetVTO.setInternalMon(Double.parseDouble(df.format(resultSet.getDouble(4))));
                        timeSheetVTO.setVacationMon(Double.parseDouble(df.format(resultSet.getDouble(5))));
                        timeSheetVTO.setHoliMon(Double.parseDouble(df.format(resultSet.getDouble(6))));
                        timeSheetVTO.setCompMon(Double.parseDouble(df.format(resultSet.getDouble(7))));
                         timeSheetVTO.setProj3Mon(Double.parseDouble(df.format(resultSet.getDouble(8))));
                        timeSheetVTO.setProj4Mon(Double.parseDouble(df.format(resultSet.getDouble(9))));
                        timeSheetVTO.setProj5Mon(Double.parseDouble(df.format(resultSet.getDouble(10))));
                        timeSheetVTO.setProj1Id(resultSet.getInt(11));
                        timeSheetVTO.setProj2Id(resultSet.getInt(12));
                        timeSheetVTO.setProj3Id(resultSet.getInt(13));
                        timeSheetVTO.setProj4Id(resultSet.getInt(14));
                        timeSheetVTO.setProj5Id(resultSet.getInt(15));
                        timeSheetVTO.setBiometricMon(Double.parseDouble(df.format(resultSet.getDouble(16))));
                        timeSheetVTO.setBmMonStatus(resultSet.getString(17));
                        
                    } else if(rowCount==3) {
                        
                        /* Work days of Tuesday */
                        timeSheetVTO.setWeekDate3(DateUtility.getInstance().convertDateToView(resultSet.getDate(1)));
                        timeSheetVTO.setProj1Tus(Double.parseDouble(df.format(resultSet.getDouble(2))));
                        timeSheetVTO.setProj2Tus(Double.parseDouble(df.format(resultSet.getDouble(3))));
                        timeSheetVTO.setInternalTus(Double.parseDouble(df.format(resultSet.getDouble(4))));
                        timeSheetVTO.setVacationTus(Double.parseDouble(df.format(resultSet.getDouble(5))));
                        timeSheetVTO.setHoliTus(Double.parseDouble(df.format(resultSet.getDouble(6))));
                        timeSheetVTO.setCompTus(Double.parseDouble(df.format(resultSet.getDouble(7))));
                        
                        timeSheetVTO.setProj3Tus(Double.parseDouble(df.format(resultSet.getDouble(8))));
                        timeSheetVTO.setProj4Tus(Double.parseDouble(df.format(resultSet.getDouble(9))));
                        timeSheetVTO.setProj5Tus(Double.parseDouble(df.format(resultSet.getDouble(10))));
                        timeSheetVTO.setProj1Id(resultSet.getInt(11));
                        timeSheetVTO.setProj2Id(resultSet.getInt(12));
                        timeSheetVTO.setProj3Id(resultSet.getInt(13));
                        timeSheetVTO.setProj4Id(resultSet.getInt(14));
                        timeSheetVTO.setProj5Id(resultSet.getInt(15));
                        timeSheetVTO.setBiometricTus(Double.parseDouble(df.format(resultSet.getDouble(16))));
                        timeSheetVTO.setBmTusStatus(resultSet.getString(17));
                    } else if(rowCount==4) {
                        
                        /* Work days of wednessDay */
                        timeSheetVTO.setWeekDate4(DateUtility.getInstance().convertDateToView(resultSet.getDate(1)));
                        timeSheetVTO.setProj1Wed(Double.parseDouble(df.format(resultSet.getDouble(2))));
                        timeSheetVTO.setProj2Wed(Double.parseDouble(df.format(resultSet.getDouble(3))));
                        timeSheetVTO.setInternalWed(Double.parseDouble(df.format(resultSet.getDouble(4))));
                        timeSheetVTO.setVacationWed(Double.parseDouble(df.format(resultSet.getDouble(5))));
                        timeSheetVTO.setHoliWed(Double.parseDouble(df.format(resultSet.getDouble(6))));
                        timeSheetVTO.setCompWed(Double.parseDouble(df.format(resultSet.getDouble(7))));
                        
                        timeSheetVTO.setProj3Wed(Double.parseDouble(df.format(resultSet.getDouble(8))));
                        timeSheetVTO.setProj4Wed(Double.parseDouble(df.format(resultSet.getDouble(9))));
                        timeSheetVTO.setProj5Wed(Double.parseDouble(df.format(resultSet.getDouble(10))));
                        timeSheetVTO.setProj1Id(resultSet.getInt(11));
                        timeSheetVTO.setProj2Id(resultSet.getInt(12));
                        timeSheetVTO.setProj3Id(resultSet.getInt(13));
                        timeSheetVTO.setProj4Id(resultSet.getInt(14));
                        timeSheetVTO.setProj5Id(resultSet.getInt(15));
                        timeSheetVTO.setBiometricWed(Double.parseDouble(df.format(resultSet.getDouble(16))));
                        timeSheetVTO.setBmWedStatus(resultSet.getString(17));
                        
                    } else if(rowCount==5) {
                        
                        /* Work days of ThursDay */
                        timeSheetVTO.setWeekDate5(DateUtility.getInstance().convertDateToView(resultSet.getDate(1)));
                        timeSheetVTO.setProj1Thur(Double.parseDouble(df.format(resultSet.getDouble(2))));
                        timeSheetVTO.setProj2Thur(Double.parseDouble(df.format(resultSet.getDouble(3))));
                        timeSheetVTO.setInternalThur(Double.parseDouble(df.format(resultSet.getDouble(4))));
                        timeSheetVTO.setVacationThur(Double.parseDouble(df.format(resultSet.getDouble(5))));
                        timeSheetVTO.setHoliThur(Double.parseDouble(df.format(resultSet.getDouble(6))));
                        timeSheetVTO.setCompThur(Double.parseDouble(df.format(resultSet.getDouble(7))));
                        
                         timeSheetVTO.setProj3Thur(Double.parseDouble(df.format(resultSet.getDouble(8))));
                        timeSheetVTO.setProj4Thur(Double.parseDouble(df.format(resultSet.getDouble(9))));
                        timeSheetVTO.setProj5Thur(Double.parseDouble(df.format(resultSet.getDouble(10))));
                        timeSheetVTO.setProj1Id(resultSet.getInt(11));
                        timeSheetVTO.setProj2Id(resultSet.getInt(12));
                        timeSheetVTO.setProj3Id(resultSet.getInt(13));
                        timeSheetVTO.setProj4Id(resultSet.getInt(14));
                        timeSheetVTO.setProj5Id(resultSet.getInt(15));
                        timeSheetVTO.setBiometricThur(Double.parseDouble(df.format(resultSet.getDouble(16))));
                        timeSheetVTO.setBmThurStatus(resultSet.getString(17));
                    } else if(rowCount==6) {
                        
                        /* Work days of Friday */
                        timeSheetVTO.setWeekDate6(DateUtility.getInstance().convertDateToView(resultSet.getDate(1)));
                        timeSheetVTO.setProj1Fri(Double.parseDouble(df.format(resultSet.getDouble(2))));
                        timeSheetVTO.setProj2Fri(Double.parseDouble(df.format(resultSet.getDouble(3))));
                        timeSheetVTO.setInternalFri(Double.parseDouble(df.format(resultSet.getDouble(4))));
                        timeSheetVTO.setVacationFri(Double.parseDouble(df.format(resultSet.getDouble(5))));
                        timeSheetVTO.setHoliFri(Double.parseDouble(df.format(resultSet.getDouble(6))));
                        timeSheetVTO.setCompFri(Double.parseDouble(df.format(resultSet.getDouble(7))));
                        
                         timeSheetVTO.setProj3Fri(Double.parseDouble(df.format(resultSet.getDouble(8))));
                        timeSheetVTO.setProj4Fri(Double.parseDouble(df.format(resultSet.getDouble(9))));
                        timeSheetVTO.setProj5Fri(Double.parseDouble(df.format(resultSet.getDouble(10))));
                        timeSheetVTO.setProj1Id(resultSet.getInt(11));
                        timeSheetVTO.setProj2Id(resultSet.getInt(12));
                        timeSheetVTO.setProj3Id(resultSet.getInt(13));
                        timeSheetVTO.setProj4Id(resultSet.getInt(14));
                        timeSheetVTO.setProj5Id(resultSet.getInt(15));
                        timeSheetVTO.setBiometricFri(Double.parseDouble(df.format(resultSet.getDouble(16))));
                        timeSheetVTO.setBmFriStatus(resultSet.getString(17));
                    } else if(rowCount==7) {
                        
                        /* Work days of Saturday */
                        timeSheetVTO.setWeekDate7(DateUtility.getInstance().convertDateToView(resultSet.getDate(1)));
                        timeSheetVTO.setProj1Sat(Double.parseDouble(df.format(resultSet.getDouble(2))));
                        timeSheetVTO.setProj2Sat(Double.parseDouble(df.format(resultSet.getDouble(3))));
                        timeSheetVTO.setInternalSat(Double.parseDouble(df.format(resultSet.getDouble(4))));
                        timeSheetVTO.setVacationSat(Double.parseDouble(df.format(resultSet.getDouble(5))));
                        timeSheetVTO.setHoliSat(Double.parseDouble(df.format(resultSet.getDouble(6))));
                        timeSheetVTO.setCompSat(Double.parseDouble(df.format(resultSet.getDouble(7))));
                        
                        timeSheetVTO.setProj3Sat(Double.parseDouble(df.format(resultSet.getDouble(8))));
                        timeSheetVTO.setProj4Sat(Double.parseDouble(df.format(resultSet.getDouble(9))));
                        timeSheetVTO.setProj5Sat(Double.parseDouble(df.format(resultSet.getDouble(10))));
                        timeSheetVTO.setProj1Id(resultSet.getInt(11));
                        timeSheetVTO.setProj2Id(resultSet.getInt(12));
                        timeSheetVTO.setProj3Id(resultSet.getInt(13));
                        timeSheetVTO.setProj4Id(resultSet.getInt(14));
                        timeSheetVTO.setProj5Id(resultSet.getInt(15));
                        timeSheetVTO.setBiometricSat(Double.parseDouble(df.format(resultSet.getDouble(16))));
                        timeSheetVTO.setBmSatStatus(resultSet.getString(17));
                    }
                    rowCount++;
                } // while
                
                
                timeSheetVTO.setProj1Name(DataSourceDataProvider.getInstance().getProjectName(timeSheetVTO.getProj1Id()));
                timeSheetVTO.setProj2Name(DataSourceDataProvider.getInstance().getProjectName(timeSheetVTO.getProj2Id()));
                timeSheetVTO.setProj3Name(DataSourceDataProvider.getInstance().getProjectName(timeSheetVTO.getProj3Id()));
                timeSheetVTO.setProj4Name(DataSourceDataProvider.getInstance().getProjectName(timeSheetVTO.getProj4Id()));
                timeSheetVTO.setProj5Name(DataSourceDataProvider.getInstance().getProjectName(timeSheetVTO.getProj5Id()));
                
                resultSet.close(); // clsoing connection
                resultSet = null;
                // for setting the Action Type name
                
                resultSet = statement.executeQuery("SELECT ProjectId,Date(EndDate) AS EndDate FROM tblProjectContacts LEFT OUTER JOIN tblProjects ON(tblProjectContacts.ProjectId=tblProjects.Id) WHERE objectId="
						+ empId
						+ " AND tblProjects.STATUS='Active' AND (tblProjectContacts.EmpProjStatus<>'OverHead' OR  tblProjects.ProjectType  IN ('Innovation','POC','POT')) ORDER BY ResourceType DESC");
               
                 while(resultSet.next()) {
                	 
                 int ProjectId   =	 resultSet.getInt("ProjectId");
                 
                 Date EndDate =  resultSet.getDate("EndDate");
                 String EndDateNew =  resultSet.getString("EndDate");
                 
                 Date weekStartDate=DateUtility.getInstance().getMySqlDate2(timeSheetVTO.getWstDate());
                  
                 if(EndDateNew!=null)
                 {
                if((EndDate.compareTo(weekStartDate)>=0) ){
                endDateString =endDateString +ProjectId+ "," + EndDate + "|"  ;
                }
                 }else if(EndDateNew==null){
                    endDateString =endDateString +ProjectId+ "," + EndDate + "|"  ;
                    }
                		   
                   // System.out.println("Enddate is"+endDateString);
                
                 }
                 
                 timeSheetVTO.setProjectEndDate(endDateString);
                 
                 
                 resultSet.close(); // clsoing connection
                 resultSet = null;
                timeSheetVTO.setModeType("NewEditTimesheet");
                
                /* calculation of the individual day of total working , billing , vaction and holidays sums*/
                
                
                timeSheetVTO.setTotalSun(Double.parseDouble(df.format((timeSheetVTO.getProj1Sun()+timeSheetVTO.getProj2Sun()+timeSheetVTO.getProj3Sun()+timeSheetVTO.getProj4Sun()+timeSheetVTO.getProj5Sun()+timeSheetVTO.getInternalSun()+timeSheetVTO.getVacationSun()+timeSheetVTO.getHoliSun()+timeSheetVTO.getCompSun()))));
                timeSheetVTO.setTotalMon(Double.parseDouble(df.format((timeSheetVTO.getProj1Mon()+timeSheetVTO.getProj2Mon()+timeSheetVTO.getProj3Mon()+timeSheetVTO.getProj4Mon()+timeSheetVTO.getProj5Mon()+timeSheetVTO.getInternalMon()+timeSheetVTO.getVacationMon()+timeSheetVTO.getHoliMon()+timeSheetVTO.getCompMon()))));
                timeSheetVTO.setTotalTus(Double.parseDouble(df.format((timeSheetVTO.getProj1Tus()+timeSheetVTO.getProj2Tus()+timeSheetVTO.getProj3Tus()+timeSheetVTO.getProj4Tus()+timeSheetVTO.getProj5Tus()+timeSheetVTO.getInternalTus()+timeSheetVTO.getVacationTus()+timeSheetVTO.getHoliTus()+timeSheetVTO.getCompTus()))));
                timeSheetVTO.setTotalWed(Double.parseDouble(df.format((timeSheetVTO.getProj1Wed()+timeSheetVTO.getProj2Wed()+timeSheetVTO.getProj3Wed()+timeSheetVTO.getProj4Wed()+timeSheetVTO.getProj5Wed()+timeSheetVTO.getInternalWed()+timeSheetVTO.getVacationWed()+timeSheetVTO.getHoliWed()+timeSheetVTO.getCompWed()))));
                timeSheetVTO.setTotalThur(Double.parseDouble(df.format((timeSheetVTO.getProj1Thur()+timeSheetVTO.getProj2Thur()+timeSheetVTO.getProj3Thur()+timeSheetVTO.getProj4Thur()+timeSheetVTO.getProj5Thur()+timeSheetVTO.getInternalThur()+timeSheetVTO.getVacationThur()+timeSheetVTO.getHoliThur()+timeSheetVTO.getCompThur()))));
                timeSheetVTO.setTotalFri(Double.parseDouble(df.format((timeSheetVTO.getProj1Fri()+timeSheetVTO.getProj2Fri()+timeSheetVTO.getProj3Fri()+timeSheetVTO.getProj4Fri()+timeSheetVTO.getProj5Fri()+timeSheetVTO.getInternalFri()+timeSheetVTO.getVacationFri()+timeSheetVTO.getHoliFri()+timeSheetVTO.getCompFri()))));
                timeSheetVTO.setTotalSat(Double.parseDouble(df.format((timeSheetVTO.getProj1Sat()+timeSheetVTO.getProj2Sat()+timeSheetVTO.getProj3Sat()+timeSheetVTO.getProj4Sat()+timeSheetVTO.getProj5Sat()+timeSheetVTO.getInternalSat()+timeSheetVTO.getVacationSat()+timeSheetVTO.getHoliSat()+timeSheetVTO.getCompSat()))));
                timeSheetVTO.setProj1TotalHrs(Double.parseDouble(df.format((timeSheetVTO.getProj1Tus()+timeSheetVTO.getProj1Sun()+timeSheetVTO.getProj1Mon()+timeSheetVTO.getProj1Wed()+timeSheetVTO.getProj1Thur()+timeSheetVTO.getProj1Fri()+timeSheetVTO.getProj1Sat()))));
                timeSheetVTO.setProj2TotalHrs(Double.parseDouble(df.format((timeSheetVTO.getProj2Tus()+timeSheetVTO.getProj2Sun()+timeSheetVTO.getProj2Mon()+timeSheetVTO.getProj2Wed()+timeSheetVTO.getProj2Thur()+timeSheetVTO.getProj2Fri()+timeSheetVTO.getProj2Sat()))));
                
                 timeSheetVTO.setProj3TotalHrs(Double.parseDouble(df.format((timeSheetVTO.getProj3Tus()+timeSheetVTO.getProj3Sun()+timeSheetVTO.getProj3Mon()+timeSheetVTO.getProj3Wed()+timeSheetVTO.getProj3Thur()+timeSheetVTO.getProj3Fri()+timeSheetVTO.getProj3Sat()))));
                  timeSheetVTO.setProj4TotalHrs(Double.parseDouble(df.format((timeSheetVTO.getProj4Tus()+timeSheetVTO.getProj4Sun()+timeSheetVTO.getProj4Mon()+timeSheetVTO.getProj4Wed()+timeSheetVTO.getProj4Thur()+timeSheetVTO.getProj4Fri()+timeSheetVTO.getProj4Sat()))));
                   timeSheetVTO.setProj5TotalHrs(Double.parseDouble(df.format((timeSheetVTO.getProj5Tus()+timeSheetVTO.getProj5Sun()+timeSheetVTO.getProj5Mon()+timeSheetVTO.getProj5Wed()+timeSheetVTO.getProj5Thur()+timeSheetVTO.getProj5Fri()+timeSheetVTO.getProj5Sat()))));
                
                
                timeSheetVTO.setInternalTotalHrs(Double.parseDouble(df.format((timeSheetVTO.getInternalSun()+timeSheetVTO.getInternalMon()+timeSheetVTO.getInternalTus()+timeSheetVTO.getInternalWed()+timeSheetVTO.getInternalThur()+timeSheetVTO.getInternalFri()+timeSheetVTO.getInternalSat()))));
                timeSheetVTO.setVacationTotalHrs(Double.parseDouble(df.format((timeSheetVTO.getVacationSun()+timeSheetVTO.getVacationMon()+timeSheetVTO.getVacationTus()+timeSheetVTO.getVacationWed()+timeSheetVTO.getVacationThur()+timeSheetVTO.getVacationFri()+timeSheetVTO.getVacationSat()))));
                timeSheetVTO.setHoliTotalHrs(Double.parseDouble(df.format((timeSheetVTO.getHoliSun()+timeSheetVTO.getHoliMon()+timeSheetVTO.getHoliTus()+timeSheetVTO.getHoliWed()+timeSheetVTO.getHoliThur()+timeSheetVTO.getHoliFri()+timeSheetVTO.getHoliSat()))));
              
                
                timeSheetVTO.setCompTotalHrs(Double.parseDouble(df.format((timeSheetVTO.getCompSun()+timeSheetVTO.getCompMon()+timeSheetVTO.getCompTus()+timeSheetVTO.getCompWed()+timeSheetVTO.getCompThur()+timeSheetVTO.getCompFri()+timeSheetVTO.getCompSat()))));
                
                timeSheetVTO.setAllWeekendTotalHors(Double.parseDouble(df.format((timeSheetVTO.getProj1TotalHrs()+timeSheetVTO.getProj2TotalHrs()+timeSheetVTO.getProj3TotalHrs()+timeSheetVTO.getProj4TotalHrs()+timeSheetVTO.getProj5TotalHrs()+timeSheetVTO.getInternalTotalHrs()+timeSheetVTO.getVacationTotalHrs()+timeSheetVTO.getHoliTotalHrs()+timeSheetVTO.getCompTotalHrs()))));
                timeSheetVTO.setTimeSheetID(String.valueOf(timeSheetID));
                timeSheetVTO.setBiometricTotalHrs(Double.parseDouble(df.format(timeSheetVTO.getBiometricSun()+timeSheetVTO.getBiometricMon()+timeSheetVTO.getBiometricTus()+timeSheetVTO.getBiometricWed()+timeSheetVTO.getBiometricThur()+timeSheetVTO.getBiometricFri()+timeSheetVTO.getBiometricSat())));
               
                
                // System.out.println("empType-->"+empType);
                // System.out.println("resourceType-->"+resourceType);
                if(empType.equalsIgnoreCase("e")){
                if(!(resourceType!=null)){
                     //queryString = "SELECT EmpName from vwTimeSheetList where EmpId='"+empId+"'";
                    queryString = "SELECT EmpName from vwNewTimeSheetList where EmpId='"+empId+"'";
                }
                else if(resourceType.equalsIgnoreCase("e"))
                    //queryString = "SELECT EmpName from vwTimeSheetList where EmpId='"+empId+"'";
                    queryString = "SELECT EmpName from vwNewTimeSheetList where EmpId='"+empId+"'";
                    else 
                        //queryString = "SELECT EmpName from vwCustTimeSheetList where EmpId='"+empId+"'";
                    queryString = "SELECT EmpName from vwNewCustTimeSheetList where EmpId='"+empId+"'";
                
                }else{
                //queryString = "SELECT EmpName from vwCustTimeSheetList where EmpId='"+empId+"'";
                    queryString = "SELECT EmpName from vwNewCustTimeSheetList where EmpId='"+empId+"'";
                }
                //System.out.println(queryString);
                resultSet = statement.executeQuery(queryString);
                resultSet.next();
               
                timeSheetVTO.setEmpName(resultSet.getString("EmpName"));
                resultSet.close(); // clsoing connection
                resultSet = null;
                
                resultSet = statement.executeQuery("SELECT Comments from tblTimeSheets where EmpId='"+empId+"' AND TimeSheetId="+timeSheetID+"");
                resultSet.next();
                timeSheetVTO.setComment(resultSet.getString("Comments"));
                resultSet.close(); // clsoing connection
                resultSet = null;
                // Dual change
                 boolean isDualReq = false;
               if(timeSheetVTO.getPriProjId()!=0){
                     resultSet = statement.executeQuery("SELECT Dualreporting FROM tblProjects WHERE Id ="+timeSheetVTO.getPriProjId());
                resultSet.next();
                isDualReq = resultSet.getBoolean("Dualreporting");
                //timeSheetVTO.setIsDualReportingRequired(resultSet.getBoolean("Dualreporting"));
                 resultSet.close(); // clsoing connection
                resultSet = null;
               }
               timeSheetVTO.setIsDualReportingRequired(isDualReq);
                
                
            }
            
        } catch (Exception ex) {
            ex.printStackTrace();
            // log.setLevel((Level)Level.ERROR);
            // log.error("The Error @getTimeSheet()",ex);
        } finally {
            if(connection!=null)
                try {
                     if(resultSet != null){
                        resultSet.close();
                        resultSet = null;
                    }
                    if(statement != null){
                        statement.close();
                        statement = null;
                    }
                    if(connection != null){
                        connection.close();
                        connection = null;
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                    // log.setLevel((Level)Level.ERROR);
                    // log.error("The Error @getTimeSheet()",ex);
                }
        }
        return timeSheetVTO;
    } // @ getTimeSheet(int empId, int timeSheetID)
     public boolean newEditTimeSheet(NewTimeSheetAction timeSheetAction) throws ServiceLocatorException {
        Connection connection = null;
        CallableStatement callableStatement = null;
        boolean isTimesheetAddOrEdit = false;
        try {
            
            connection = ConnectionProvider.getInstance().getConnection();
            if(connection != null) {
                callableStatement = connection.prepareCall("{call spNewTimeSheetEdit(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
                callableStatement.setInt(1,Integer.parseInt(timeSheetAction.getEmpID().trim()));
                callableStatement.setInt(2,Integer.parseInt(timeSheetAction.getTimeSheetID().trim()));
                
                // converting the String to sql Date object.
                callableStatement.setDate(3,DateUtility.getInstance().getMysqlDate(timeSheetAction.getApproveDate().trim()));
                callableStatement.setDouble(4,timeSheetAction.getTotalBillHrs());
                callableStatement.setDouble(5,timeSheetAction.getProj1Sun());
                callableStatement.setDouble(6,timeSheetAction.getProj1Mon());
                callableStatement.setDouble(7,timeSheetAction.getProj1Tus());
                callableStatement.setDouble(8,timeSheetAction.getProj1Wed());
                callableStatement.setDouble(9,timeSheetAction.getProj1Thur());
                callableStatement.setDouble(10,timeSheetAction.getProj1Fri());
                callableStatement.setDouble(11,timeSheetAction.getProj1Sat());
                callableStatement.setDouble(12,timeSheetAction.getProj2Sun());
                callableStatement.setDouble(13,timeSheetAction.getProj2Mon());
                callableStatement.setDouble(14,timeSheetAction.getProj2Tus());
                callableStatement.setDouble(15,timeSheetAction.getProj2Wed());
                callableStatement.setDouble(16,timeSheetAction.getProj2Thur());
                callableStatement.setDouble(17,timeSheetAction.getProj2Fri());
                callableStatement.setDouble(18,timeSheetAction.getProj2Sat());
                
                  callableStatement.setDouble(19,timeSheetAction.getProj3Sun());
                callableStatement.setDouble(20,timeSheetAction.getProj3Mon());
                callableStatement.setDouble(21,timeSheetAction.getProj3Tus());
                callableStatement.setDouble(22,timeSheetAction.getProj3Wed());
                callableStatement.setDouble(23,timeSheetAction.getProj3Thur());
                callableStatement.setDouble(24,timeSheetAction.getProj3Fri());
                callableStatement.setDouble(25,timeSheetAction.getProj3Sat());
                
                  callableStatement.setDouble(26,timeSheetAction.getProj4Sun());
                callableStatement.setDouble(27,timeSheetAction.getProj4Mon());
                callableStatement.setDouble(28,timeSheetAction.getProj4Tus());
                callableStatement.setDouble(29,timeSheetAction.getProj4Wed());
                callableStatement.setDouble(30,timeSheetAction.getProj4Thur());
                callableStatement.setDouble(31,timeSheetAction.getProj4Fri());
                callableStatement.setDouble(32,timeSheetAction.getProj4Sat());
                
                  callableStatement.setDouble(33,timeSheetAction.getProj5Sun());
                callableStatement.setDouble(34,timeSheetAction.getProj5Mon());
                callableStatement.setDouble(35,timeSheetAction.getProj5Tus());
                callableStatement.setDouble(36,timeSheetAction.getProj5Wed());
                callableStatement.setDouble(37,timeSheetAction.getProj5Thur());
                callableStatement.setDouble(38,timeSheetAction.getProj5Fri());
                callableStatement.setDouble(39,timeSheetAction.getProj5Sat());
                
                
                callableStatement.setDouble(40,timeSheetAction.getInternalSun());
                callableStatement.setDouble(41,timeSheetAction.getInternalMon());
                callableStatement.setDouble(42,timeSheetAction.getInternalTus());
                callableStatement.setDouble(43,timeSheetAction.getInternalWed());
                callableStatement.setDouble(44,timeSheetAction.getInternalThur());
                callableStatement.setDouble(45,timeSheetAction.getInternalFri());
                callableStatement.setDouble(46,timeSheetAction.getInternalSat());
                callableStatement.setDouble(47,timeSheetAction.getVacationSun());
                callableStatement.setDouble(48,timeSheetAction.getVacationMon());
                callableStatement.setDouble(49,timeSheetAction.getVacationTus());
                callableStatement.setDouble(50,timeSheetAction.getVacationWed());
                callableStatement.setDouble(51,timeSheetAction.getVacationThur());
                callableStatement.setDouble(52,timeSheetAction.getVacationFri());
                callableStatement.setDouble(53,timeSheetAction.getVacationSat());
                callableStatement.setDouble(54,timeSheetAction.getHoliSun());
                callableStatement.setDouble(55,timeSheetAction.getHoliMon());
                callableStatement.setDouble(56,timeSheetAction.getHoliTus());
                callableStatement.setDouble(57,timeSheetAction.getHoliWed());
                callableStatement.setDouble(58,timeSheetAction.getHoliThur());
                callableStatement.setDouble(59,timeSheetAction.getHoliFri());
                callableStatement.setDouble(60,timeSheetAction.getHoliSat());
                
                 callableStatement.setDouble(61,timeSheetAction.getCompSun());
                callableStatement.setDouble(62,timeSheetAction.getCompMon());
                callableStatement.setDouble(63,timeSheetAction.getCompTus());
                callableStatement.setDouble(64,timeSheetAction.getCompWed());
                callableStatement.setDouble(65,timeSheetAction.getCompThur());
                callableStatement.setDouble(66,timeSheetAction.getCompFri());
                callableStatement.setDouble(67,timeSheetAction.getCompSat());
                
                // converting the String to sql Date object.
                callableStatement.setString(68,DateUtility.getInstance().convertDateToMySql(DateUtility.getInstance().convertStringToMySql(timeSheetAction.getWeekDate1().trim())));
                callableStatement.setString(69,DateUtility.getInstance().convertDateToMySql(DateUtility.getInstance().convertStringToMySql(timeSheetAction.getWeekDate2().trim())));
                callableStatement.setString(70,DateUtility.getInstance().convertDateToMySql(DateUtility.getInstance().convertStringToMySql(timeSheetAction.getWeekDate3().trim())));
                callableStatement.setString(71,DateUtility.getInstance().convertDateToMySql(DateUtility.getInstance().convertStringToMySql(timeSheetAction.getWeekDate4().trim())));
                callableStatement.setString(72,DateUtility.getInstance().convertDateToMySql(DateUtility.getInstance().convertStringToMySql(timeSheetAction.getWeekDate5().trim())));
                callableStatement.setString(73,DateUtility.getInstance().convertDateToMySql(DateUtility.getInstance().convertStringToMySql(timeSheetAction.getWeekDate6().trim())));
                callableStatement.setString(74,DateUtility.getInstance().convertDateToMySql(DateUtility.getInstance().convertStringToMySql(timeSheetAction.getWeekDate7().trim())));
                callableStatement.setString(75,timeSheetAction.getTxtNotes().trim());
                callableStatement.setInt(76,timeSheetAction.getTempVar());
                callableStatement.setString(77,timeSheetAction.getEmpType());
                
                callableStatement.setInt(78,timeSheetAction.getProj1Id());
                callableStatement.setInt(79,timeSheetAction.getProj2Id());
                callableStatement.setInt(80,timeSheetAction.getProj3Id());
                callableStatement.setInt(81,timeSheetAction.getProj4Id());
                callableStatement.setInt(82,timeSheetAction.getProj5Id());
                
                callableStatement.setDouble(83,timeSheetAction.getBiometricSun());
                callableStatement.setDouble(84,timeSheetAction.getBiometricMon());
                callableStatement.setDouble(85,timeSheetAction.getBiometricTus());
                callableStatement.setDouble(86,timeSheetAction.getBiometricWed());
                callableStatement.setDouble(87,timeSheetAction.getBiometricThur());
                callableStatement.setDouble(88,timeSheetAction.getBiometricFri());
                callableStatement.setDouble(89,timeSheetAction.getBiometricSat());
                callableStatement.setDouble(90,timeSheetAction.getBiometricTotalHrs());
                
                callableStatement.setDouble(91,timeSheetAction.getTotalHoliHrs());
                callableStatement.setDouble(92,timeSheetAction.getTotalVacationHrs());
                callableStatement.setDouble(93,timeSheetAction.getTotalComptimeHrs());
                callableStatement.setString(94,timeSheetAction.getAttachmentLocation());
                callableStatement.registerOutParameter(95,java.sql.Types.INTEGER);
                callableStatement.execute() ;
                
                /* get the registerout param to know the successfull insertion. */
                if(callableStatement.getInt(95)<1) {
                    isTimesheetAddOrEdit = false;
                    //log.debug("The Error occur while inserting the TimeSheet for EmpId"+timeSheetAction.getEmpID());
                } else {
                    isTimesheetAddOrEdit = true;
                    //log.debug("The TimeSheet Successfully Updatd For EmpId:"+timeSheetAction.getEmpID()+"with TimeSheetId:"+callableStatement.getInt(52));
                }// if
                
                
            } //if
            
        } //try
        catch(Exception ex) {
            throw new ServiceLocatorException(ex);
            //log.setLevel((Level)Level.ERROR);
            // log.error("The Error @ editTimeSheet()",ex);
            
        } finally {
            try {
                if(callableStatement!=null){
                    callableStatement.close();
                    callableStatement = null;
                }
                if(connection != null){
                    connection.close();
                    connection = null;
                }
            } catch (SQLException ex) {
                throw new ServiceLocatorException(ex);
                // log.setLevel((Level)Level.ERROR);
                // log.error("The Error @ editTimeSheet()",ex);
            } //catch
        } // finally
        return isTimesheetAddOrEdit;
    } // @ editTimeSheet()
    
   /*   public String getSubmitTimeSheetEmailLogBody(String empName,String wstDate,String wenDate,String empId,String reportsToType,int timeSheetID,String resourceType,String description) throws ServiceLocatorException{
        
       // System.out.println("in service impl");
        int deletedRows = 0;
      //  Connection connection = null;
      //  Statement statement = null;
       // String queryString = "Delete from tblTimeSheets  WHERE id="+id;
      StringBuffer htmlText = new StringBuffer();
       
        try{
            
            
            
             htmlText.append("<html><head><title>Mail From Hubble Portal</title>");
                htmlText.append("</head><body><font color='blue' size='2' face='Arial'>");
                htmlText.append("<p>A TimeSheet has been submitted by "+empName+" <br>");
                htmlText.append("<p><u>Time Sheet Details:</u><br>");
                htmlText.append("Week Start Date: "+wstDate+"<br>");
                htmlText.append("Week End Date: "+wenDate+"<br>");
                // linkUrl="newgetEmpTimeSheet.action?empID={EmpID}&timeSheetID={TimeSheetId}&resourceType={ResourceType}&statusValue={Description}" imageBorder="0" 
                if(!(reportsToType!=null)){
                htmlText.append(" <br><br>\n\n\n <font color=\"red\">  <a href='http://w3.miraclesoft.com/Hubble/employee/timesheets/newGetTeamTimeSheet.action?" +
                        "employeeID="+empId+"&emptimeSheetID="+timeSheetID+"&type=e&resourceType="+resourceType+"&statusValue="+description+"'> " +
                        " Click here </a> </font>  to Approve/Reject the TimeSheet.");
                }else if(reportsToType.equalsIgnoreCase("e"))
                {
                    htmlText.append(" <br><br>\n\n\n <font color=\"red\">  <a href='http://w3.miraclesoft.com/Hubble/employee/timesheets/newGetTeamTimeSheet.action?" +
                        "employeeID="+empId+"&emptimeSheetID="+timeSheetID+"&type=e&resourceType="+resourceType+"&statusValue="+description+"'> " +
                        " Click here </a> </font>  to Approve/Reject the TimeSheet.");
                }else if(reportsToType.equalsIgnoreCase("v"))
                {
                    htmlText.append(" <br><br>\n\n\n <font color=\"red\">  <a href='http://w3.miraclesoft.com/Hubble/employee/timesheets/newGetTeamTimeSheet.action?" +
                        "employeeID="+empId+"&emptimeSheetID="+timeSheetID+"&type=v&resourceType="+resourceType+"&statusValue="+description+"'> " +
                        " Click here </a> </font>  to Approve/Reject the TimeSheet.");
                }else if(reportsToType.equalsIgnoreCase("c"))
                {
                    htmlText.append(" <br><br>\n\n\n <font color=\"red\">  <a href='http://w3.miraclesoft.com/Hubble/employee/timesheets/newGetTeamTimeSheet.action?" +
                        "employeeID="+empId+"&emptimeSheetID="+timeSheetID+"&type=c&resourceType="+resourceType+"&statusValue="+description+"'> " +
                        " Click here </a> </font>  to Approve/Reject the TimeSheet.");
                }
                htmlText.append("<br><br> Thank you.</p></font>");
                htmlText.append("<font color='red', size='2' face='Arial'>*Note:Please do not reply to this e-mail. It was generated by our System.</font>");
                htmlText.append("</body></html>");
                
                
            
        }catch (Exception se){
            throw new ServiceLocatorException(se);
        }finally{
            
        }
        return htmlText.toString();
    }
    

*/


 public String getApproveTimeSheetEmailLogBody(String empName,String wstDate,String wenDate) throws ServiceLocatorException{
        
       // System.out.println("in service impl");
        int deletedRows = 0;
      //  Connection connection = null;
      //  Statement statement = null;
       // String queryString = "Delete from tblTimeSheets  WHERE id="+id;
      StringBuffer htmlText = new StringBuffer();
       
        try{
            
            
            
//              htmlText.append("<html><head><title>Mail From Hubble Portal</title>");
//                htmlText.append("</head><body><font color='blue' size='2' face='Arial'>");
//                htmlText.append("<p>Hello "+empName+"</p>");
//                htmlText.append("<p>Your Time Sheet has been Approved.</p>");
//                htmlText.append("<p><u><b>Time Sheet Details:</b></u><br>");
//                htmlText.append("Week Start Date: "+wstDate+"<br>");
//                htmlText.append("Week End Date: "+wenDate+"<br>");
//                htmlText.append("Thank you.</p></font>");
//                htmlText.append("<font color='red', size='2' face='Arial'>*Note:Please do not reply to this e-mail.  It was generated by our System.</font>");
//                htmlText.append("</body></html>");
            
            htmlText.append("<!DOCTYPE html><html><head><meta charset='utf-8'><meta name='viewport' content='width=device-width, initial-scale=1'><meta http-equiv='X-UA-Compatible' content='IE=edge' />");
            htmlText.append("<style type='text/css'>body, table, td, a{-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%;}");
            htmlText.append("table, td{mso-table-lspace: 0pt; mso-table-rspace: 0pt;}");
            htmlText.append("img{-ms-interpolation-mode: bicubic;}");
            htmlText.append("img{border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none;}");
            htmlText.append("table{border-collapse: collapse !important;}");
            htmlText.append("body{height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important;}");
            htmlText.append("a[x-apple-data-detectors] {color: inherit !important;text-decoration: none !important;font-size: inherit !important;font-family: inherit !important;font-weight: inherit !important;line-height: inherit !important;}");
            htmlText.append("@media screen and (max-width: 525px) { .wrapper {width: 100% !important;max-width: 100% !important;}");
            htmlText.append(".logo img {margin: 0 auto !important;} .mobile-hide {display: none !important;} .img-max {max-width: 100% !important;width: 100% !important; height: auto !important; }");
            htmlText.append(".responsive-table {width: 100% !important;}");
            htmlText.append(".padding {padding: 10px 5% 15px 5% !important;}");
            htmlText.append(".padding-meta {padding: 30px 5% 0px 5% !important;text-align: center;}");
            htmlText.append(".padding-copy {padding: 10px 5% 10px 5% !important;text-align: center;}");
            htmlText.append(".no-padding {padding: 0 !important;}");
            htmlText.append(".section-padding {padding: 50px 15px 50px 15px !important;}");
            htmlText.append(".mobile-button-container {margin: 0 auto;width: 100% !important;}");
            htmlText.append(".mobile-button {padding: 15px !important;border: 0 !important;font-size: 16px !important;display: block !important;} }");
            htmlText.append("</style></head><body style='margin: 0 !important; padding: 0 !important;'>");
            htmlText.append("<div style='display: none; font-size: 1px; color: #fefefe; line-height: 1px; font-family: Helvetica, Arial, sans-serif; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;'>Hello <b>"+empName+",</b><br>Your Time Sheet has been Approved.</div>");
            htmlText.append("<table border='0' cellpadding='0' cellspacing='0' width='100%'><tr><td bgcolor='#ffffff' align='center'>");
            htmlText.append("<table border='0' cellpadding='0' cellspacing='0' width='100%' style='max-width: 500px;' class='wrapper'><tr>");
            htmlText.append("<td align='center' valign='top' style='padding: 15px 0;' class='logo'>");
            htmlText.append("<a href='https://www.miraclesoft.com/' target='_blank'><img alt='Logo' src='https://www.miraclesoft.com/images/newsletters/miracle-logo-dark.png' width='165' height='auto' style='display: block; font-family: Helvetica, Arial, sans-serif; color: #ffffff; font-size: 16px;' border='0'></a>");
            htmlText.append("</td></tr></table></td></tr>");
            htmlText.append("<tr><td bgcolor='#ffffff' align='center' style='padding: 5px;'>");
            htmlText.append("<table border='0' cellpadding='0' cellspacing='0' width='100%' style='max-width: 500px;' class='responsive-table'><tr><td>");
            htmlText.append("<table width='100%' border='0' cellspacing='0' cellpadding='0'>");
            htmlText.append("<tr><td align='center' style='font-size: 26px; font-family: calibri; color: #2368a0; padding-top: 10px;' class='padding-copy'><b>Time Sheet Approval</b></td></tr>");
            htmlText.append("</table></td></tr></table></td></tr>");
            htmlText.append("<tr><td bgcolor='#ffffff' align='center' style='padding: 15px;' class='padding'>");
            htmlText.append("<table border='0' cellpadding='0' cellspacing='0' width='100%' style='max-width: 500px;' class='responsive-table'><tr><td>");
            htmlText.append("<table width='100%' border='0' cellspacing='0' cellpadding='0'><tr>");
            htmlText.append("<td align='left' style='padding: 5px 0 5px 0; font-size: 14px; line-height: 25px; font-family: calibri; color: #232527;' class='padding-copy'>Hello <b>"+empName+",</b><br>Your Time Sheet has been Approved.     </td></tr>");
            htmlText.append("<tr><td align='justify' style='padding: 5px 0 5px 0; border-top: 1px dashed #2368a0; border-bottom: 1px dashed #2368a0; font-size: 14px; line-height: 25px; font-family: calibri; color: #232527;' class='padding-copy'><b style='font-size: 14px; color: #ef4048;'>From Date: </b> "+wstDate+"<br><b style='font-size: 14px; color: #ef4048;'>To Date: </b> "+wenDate+"<br>");
            htmlText.append("</td></tr></tr></table></td></tr>");
            htmlText.append("<tr><td><table width='100%' border='0' cellspacing='0' cellpadding='0'><tr>");
            htmlText.append("<td align='left' style='padding: 5px 0 5px 0; font-size: 14px; line-height: 22px; font-family: calibri; color: #8c8c8c; font-style: normal;' class='padding-copy'>");
            htmlText.append("Thanks & Regards,<br>Corporate Application Support Team, <br>Miracle Software Systems, Inc. <br>Email: hubble@miraclesoft.com <br>Phone: (+1)248-233-1814");
            htmlText.append("</td></tr></table></td></tr>");
            htmlText.append("<tr><td> <table width='100%' border='0' cellspacing='0' cellpadding='0'><tr>");
            htmlText.append("<td align='left' style='padding: 5px 0 5px 0; font-size: 14px; line-height: 22px; font-family: calibri; color: #ef4048; font-style: italic;' class='padding-copy'>*Note: Please do not reply to this email as this is an automated notification</td>");
            htmlText.append("</tr></table></td></tr></table></td></tr>");
            htmlText.append("<tr><td bgcolor='#ffffff' align='center' style='padding: 15px 0px;'>");
            htmlText.append("<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' style='max-width: 500px;' class='responsive-table'>");
            htmlText.append("<tr><td width='200' align='center' style='text-align: center;'><table width='200' cellpadding='0' cellspacing='0' align='center'>");
            htmlText.append("<tr><td width='10'><a href='https://www.facebook.com/miracle45625' target='_blank'><img src='https://www.miraclesoft.com/images/newsletters/facebook.png' alt='facebook' width='26' height='auto' data-max-width='40' data-customIcon='true' ></a></td>");
            htmlText.append("<td width='10'><a href='https://plus.google.com/+Team_MSS/videos' target='_blank'><img src='https://www.miraclesoft.com/images/newsletters/googleplus.png' alt='facebook' width='26' height='auto' data-max-width='40' data-customIcon='true' ></a></td>");
            htmlText.append("<td width='10'><a href='https://www.linkedin.com/company/miracle-software-systems-inc' target='_blank'><img src='https://www.miraclesoft.com/images/newsletters/linkedin.png' alt='facebook' width='26' height='auto' data-max-width='40' data-customIcon='true' ></a></td>");
            htmlText.append("<td width='10'><a href='https://www.youtube.com/c/Team_MSS' target='_blank'><img src='https://www.miraclesoft.com/images/newsletters/youtube.png' alt='facebook' width='26' height='auto' data-max-width='40' data-customIcon='true' ></a></td>");
            htmlText.append("<td width='10'><a href='https://twitter.com/Team_MSSs' target='_blank'><img src='https://www.miraclesoft.com/images/newsletters/twitter.png' alt='facebook' width='26' height='auto' data-max-width='40' data-customIcon='true' ></a></td>");
            htmlText.append("</tr></table></td></tr><tr><td height='10'></td></tr>");
            htmlText.append("<tr><td align='center' style='font-size: 14px; line-height: 20px; font-family: calibri; color:#666666;'>&copy; "+Calendar.getInstance().get(Calendar.YEAR)+" Miracle Software Systems<br></td></tr>");
            htmlText.append("</table></td></tr></table></body></html>");

            
        }catch (Exception se){
            throw new ServiceLocatorException(se);
        }finally{
            
        }
        return htmlText.toString();
    }
    

public String getRejectTimeSheetEmailLogBody(String empName,String wstDate,String wenDate) throws ServiceLocatorException{
        
       // System.out.println("in service impl");
        int deletedRows = 0;
      //  Connection connection = null;
      //  Statement statement = null;
       // String queryString = "Delete from tblTimeSheets  WHERE id="+id;
      StringBuffer htmlText = new StringBuffer();
       
        try{
                
//                 htmlText.append("<html><head><title>Mail From Hubble Portal</title>");
//                htmlText.append("</head><body><font color='blue' size='2' face='Arial'>");
//                htmlText.append("<p>Hello "+empName+"</p>");
//                htmlText.append("<p>Your Time Sheet has been Rejected.</p>");
//                htmlText.append("<p><u><b>Time Sheet Details:</b></u><br>");
//                htmlText.append("Week Start Date: "+wstDate+"<br>");
//                htmlText.append("Week End Date: "+wenDate+"<br>");
//                htmlText.append("Thank you.</p></font>");
//                htmlText.append("<font color='red', size='2' face='Arial'>*Note:Please do not reply to this e-mail.  It was generated by our System.</font>");
//                htmlText.append("</body></html>");
            
            
            htmlText.append("<!DOCTYPE html><html><head><meta charset='utf-8'><meta name='viewport' content='width=device-width, initial-scale=1'><meta http-equiv='X-UA-Compatible' content='IE=edge' />");
            htmlText.append("<style type='text/css'>body, table, td, a{-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%;}");
            htmlText.append("table, td{mso-table-lspace: 0pt; mso-table-rspace: 0pt;}");
            htmlText.append("img{-ms-interpolation-mode: bicubic;}");
            htmlText.append("img{border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none;}");
            htmlText.append("table{border-collapse: collapse !important;}");
            htmlText.append("body{height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important;}");
            htmlText.append("a[x-apple-data-detectors] {color: inherit !important;text-decoration: none !important;font-size: inherit !important;font-family: inherit !important;font-weight: inherit !important;line-height: inherit !important;}");
            htmlText.append("@media screen and (max-width: 525px) { .wrapper {width: 100% !important;max-width: 100% !important;}");
            htmlText.append(".logo img {margin: 0 auto !important;} .mobile-hide {display: none !important;} .img-max {max-width: 100% !important;width: 100% !important; height: auto !important; }");
            htmlText.append(".responsive-table {width: 100% !important;}");
            htmlText.append(".padding {padding: 10px 5% 15px 5% !important;}");
            htmlText.append(".padding-meta {padding: 30px 5% 0px 5% !important;text-align: center;}");
            htmlText.append(".padding-copy {padding: 10px 5% 10px 5% !important;text-align: center;}");
            htmlText.append(".no-padding {padding: 0 !important;}");
            htmlText.append(".section-padding {padding: 50px 15px 50px 15px !important;}");
            htmlText.append(".mobile-button-container {margin: 0 auto;width: 100% !important;}");
            htmlText.append(".mobile-button {padding: 15px !important;border: 0 !important;font-size: 16px !important;display: block !important;} }");
            htmlText.append("</style></head><body style='margin: 0 !important; padding: 0 !important;'>");
            htmlText.append("<div style='display: none; font-size: 1px; color: #fefefe; line-height: 1px; font-family: Helvetica, Arial, sans-serif; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;'>Hello <b>"+empName+",</b><br>Your Time Sheet has been Rejected.</div>");
            htmlText.append("<table border='0' cellpadding='0' cellspacing='0' width='100%'><tr><td bgcolor='#ffffff' align='center'>");
            htmlText.append("<table border='0' cellpadding='0' cellspacing='0' width='100%' style='max-width: 500px;' class='wrapper'><tr>");
            htmlText.append("<td align='center' valign='top' style='padding: 15px 0;' class='logo'>");
            htmlText.append("<a href='https://www.miraclesoft.com/' target='_blank'><img alt='Logo' src='https://www.miraclesoft.com/images/newsletters/miracle-logo-dark.png' width='165' height='auto' style='display: block; font-family: Helvetica, Arial, sans-serif; color: #ffffff; font-size: 16px;' border='0'></a>");
            htmlText.append("</td></tr></table></td></tr>");
            htmlText.append("<tr><td bgcolor='#ffffff' align='center' style='padding: 5px;'>");
            htmlText.append("<table border='0' cellpadding='0' cellspacing='0' width='100%' style='max-width: 500px;' class='responsive-table'><tr><td>");
            htmlText.append("<table width='100%' border='0' cellspacing='0' cellpadding='0'>");
            htmlText.append("<tr><td align='center' style='font-size: 26px; font-family: calibri; color: #2368a0; padding-top: 10px;' class='padding-copy'><b>Time Sheet Rejection</b></td></tr>");
            htmlText.append("</table></td></tr></table></td></tr>");
            htmlText.append("<tr><td bgcolor='#ffffff' align='center' style='padding: 15px;' class='padding'>");
            htmlText.append("<table border='0' cellpadding='0' cellspacing='0' width='100%' style='max-width: 500px;' class='responsive-table'><tr><td>");
            htmlText.append("<table width='100%' border='0' cellspacing='0' cellpadding='0'><tr>");
            htmlText.append("<td align='left' style='padding: 5px 0 5px 0; font-size: 14px; line-height: 25px; font-family: calibri; color: #232527;' class='padding-copy'>Hello <b>"+empName+",</b><br>Your Time Sheet has been Rejected.     </td></tr>");
            htmlText.append("<tr><td align='justify' style='padding: 5px 0 5px 0; border-top: 1px dashed #2368a0; border-bottom: 1px dashed #2368a0; font-size: 14px; line-height: 25px; font-family: calibri; color: #232527;' class='padding-copy'><b style='font-size: 14px; color: #ef4048;'>From Date: </b> "+wstDate+"<br><b style='font-size: 14px; color: #ef4048;'>To Date: </b> "+wenDate+"<br>");
            htmlText.append("</td></tr></tr></table></td></tr>");
            htmlText.append("<tr><td><table width='100%' border='0' cellspacing='0' cellpadding='0'><tr>");
            htmlText.append("<td align='left' style='padding: 5px 0 5px 0; font-size: 14px; line-height: 22px; font-family: calibri; color: #8c8c8c; font-style: normal;' class='padding-copy'>");
            htmlText.append("Thanks & Regards,<br>Corporate Application Support Team, <br>Miracle Software Systems, Inc. <br>Email: hubble@miraclesoft.com <br>Phone: (+1)248-233-1814");
            htmlText.append("</td></tr></table></td></tr>");
            htmlText.append("<tr><td> <table width='100%' border='0' cellspacing='0' cellpadding='0'><tr>");
            htmlText.append("<td align='left' style='padding: 5px 0 5px 0; font-size: 14px; line-height: 22px; font-family: calibri; color: #ef4048; font-style: italic;' class='padding-copy'>*Note: Please do not reply to this email as this is an automated notification</td>");
            htmlText.append("</tr></table></td></tr></table></td></tr>");
            htmlText.append("<tr><td bgcolor='#ffffff' align='center' style='padding: 15px 0px;'>");
            htmlText.append("<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' style='max-width: 500px;' class='responsive-table'>");
            htmlText.append("<tr><td width='200' align='center' style='text-align: center;'><table width='200' cellpadding='0' cellspacing='0' align='center'>");
            htmlText.append("<tr><td width='10'><a href='https://www.facebook.com/miracle45625' target='_blank'><img src='https://www.miraclesoft.com/images/newsletters/facebook.png' alt='facebook' width='26' height='auto' data-max-width='40' data-customIcon='true' ></a></td>");
            htmlText.append("<td width='10'><a href='https://plus.google.com/+Team_MSS/videos' target='_blank'><img src='https://www.miraclesoft.com/images/newsletters/googleplus.png' alt='facebook' width='26' height='auto' data-max-width='40' data-customIcon='true' ></a></td>");
            htmlText.append("<td width='10'><a href='https://www.linkedin.com/company/miracle-software-systems-inc' target='_blank'><img src='https://www.miraclesoft.com/images/newsletters/linkedin.png' alt='facebook' width='26' height='auto' data-max-width='40' data-customIcon='true' ></a></td>");
            htmlText.append("<td width='10'><a href='https://www.youtube.com/c/Team_MSS' target='_blank'><img src='https://www.miraclesoft.com/images/newsletters/youtube.png' alt='facebook' width='26' height='auto' data-max-width='40' data-customIcon='true' ></a></td>");
            htmlText.append("<td width='10'><a href='https://twitter.com/Team_MSSs' target='_blank'><img src='https://www.miraclesoft.com/images/newsletters/twitter.png' alt='facebook' width='26' height='auto' data-max-width='40' data-customIcon='true' ></a></td>");
            htmlText.append("</tr></table></td></tr><tr><td height='10'></td></tr>");
            htmlText.append("<tr><td align='center' style='font-size: 14px; line-height: 20px; font-family: calibri; color:#666666;'>&copy; "+Calendar.getInstance().get(Calendar.YEAR)+" Miracle Software Systems<br></td></tr>");
            htmlText.append("</table></td></tr></table></body></html>");
            
        }catch (Exception se){
            throw new ServiceLocatorException(se);
        }finally{
            
        }
        return htmlText.toString();
    }
    
public String getEnteredTimeSheetEmailLogBody(String empName,String wstDate,String wenDate) throws ServiceLocatorException{
        
       // System.out.println("in service impl");
        int deletedRows = 0;
      //  Connection connection = null;
      //  Statement statement = null;
       // String queryString = "Delete from tblTimeSheets  WHERE id="+id;
      StringBuffer htmlText = new StringBuffer();
       
        try{
                
                 htmlText.append("<html><head><title>Mail From Hubble Portal</title>");
                htmlText.append("</head><body><font color='blue' size='2' face='Arial'>");
                htmlText.append("<p>Hello "+empName+"</p>");
                htmlText.append("<p>Please submit your enterd timesheet in below dates.</p>");
                htmlText.append("<p><u><b>Time Sheet Details:</b></u><br>");
                htmlText.append("Week Start Date: "+wstDate+"<br>");
                htmlText.append("Week End Date: "+wenDate+"<br>");
                htmlText.append("Thank you.</p></font>");
                htmlText.append("<font color='red', size='2' face='Arial'>*Note:Please do not reply to this e-mail.  It was generated by our System.</font>");
                htmlText.append("</body></html>");
            
        }catch (Exception se){
            throw new ServiceLocatorException(se);
        }finally{
            
        }
        return htmlText.toString();
    }
     public String getTeamMembersList(String LoginId) throws ServiceLocatorException {
        Connection connection = null;
        CallableStatement callableStatement = null;
        String teamMembersList="'"+LoginId+"',";
        try {
            
            connection = ConnectionProvider.getInstance().getConnection();
            if(connection != null) {
                callableStatement = connection.prepareCall("{call spTeamTimeSheets(?,?)}");
                callableStatement.setString(1,LoginId);
                callableStatement.registerOutParameter(2,java.sql.Types.VARCHAR);
                
                callableStatement.execute() ;
                String str = callableStatement.getString(2);
                String[] temp;
                
                /* delimiter */
                String delimiter = ",";
                /* given string will be split by the argument delimiter provided. */
                temp = str.split(delimiter);
                /* print substrings */
                for(int i =0; i < temp.length ; i++) {
                    if(!"".equals(temp[i]) ) {
                        teamMembersList=teamMembersList+"'"+(temp[i]);
                        teamMembersList=teamMembersList+"'";
                        if(i!=temp.length-1) teamMembersList=teamMembersList+",";
                    }
                }
                // System.out.println("Team Members List IS"+teamMembersList.toString());
            } //if
            
        } //try
        catch(Exception ex) {
            throw new ServiceLocatorException(ex);
            
            
        } finally {
            try {
                if(callableStatement!=null){
                    callableStatement.close();
                    callableStatement = null;
                }
                if(connection != null){
                    connection.close();
                    connection = null;
                }
            } catch (SQLException ex) {
                throw new ServiceLocatorException(ex);
                
            } //catch
        } // finally
        return teamMembersList;
    }
    
     
     // Dual changes
/*     
public String getSubmitTimeSheetEmailLogBody(String empName,String wstDate,String wenDate,String empId,String reportsToType,int timeSheetID,String resourceType,String description,String secDescription) throws ServiceLocatorException{
        
       // System.out.println("in service impl");
        int deletedRows = 0;
      //  Connection connection = null;
      //  Statement statement = null;
       // String queryString = "Delete from tblTimeSheets  WHERE id="+id;
      StringBuffer htmlText = new StringBuffer();
       
        try{
            
            
            
             htmlText.append("<html><head><title>Mail From Hubble Portal</title>");
                htmlText.append("</head><body><font color='blue' size='2' face='Arial'>");
                htmlText.append("<p>A TimeSheet has been submitted by "+empName+" <br>");
                htmlText.append("<p><u>Time Sheet Details:</u><br>");
                htmlText.append("Week Start Date: "+wstDate+"<br>");
                htmlText.append("Week End Date: "+wenDate+"<br>");
                // linkUrl="newgetEmpTimeSheet.action?empID={EmpID}&timeSheetID={TimeSheetId}&resourceType={ResourceType}&statusValue={Description}" imageBorder="0" 
                if(!(reportsToType!=null)){
                htmlText.append(" <br><br>\n\n\n <font color=\"red\">  <a href='http://w3.miraclesoft.com/Hubble/employee/timesheets/newGetTeamTimeSheet.action?" +
                       // "employeeID="+empId+"&emptimeSheetID="+timeSheetID+"&type=e&resourceType="+resourceType+"&statusValue="+description+"&secStatusValue="+secDescription+"&teamType="+teamType+"'> " +
                         "employeeID="+empId+"&emptimeSheetID="+timeSheetID+"&type=e&resourceType="+resourceType+"&statusValue="+description+"&secStatusValue="+secDescription+"'> " +
                        " Click here </a> </font>  to Approve/Reject the TimeSheet.");
                }else if(reportsToType.equalsIgnoreCase("e"))
                {
                    htmlText.append(" <br><br>\n\n\n <font color=\"red\">  <a href='http://w3.miraclesoft.com/Hubble/employee/timesheets/newGetTeamTimeSheet.action?" +
                        "employeeID="+empId+"&emptimeSheetID="+timeSheetID+"&type=e&resourceType="+resourceType+"&statusValue="+description+"&secStatusValue="+secDescription+"'> " +
                        " Click here </a> </font>  to Approve/Reject the TimeSheet.");
                }else if(reportsToType.equalsIgnoreCase("v"))
                {
                    htmlText.append(" <br><br>\n\n\n <font color=\"red\">  <a href='http://w3.miraclesoft.com/Hubble/employee/timesheets/newGetTeamTimeSheet.action?" +
                        "employeeID="+empId+"&emptimeSheetID="+timeSheetID+"&type=v&resourceType="+resourceType+"&statusValue="+description+"&secStatusValue="+secDescription+"'> " +
                        " Click here </a> </font>  to Approve/Reject the TimeSheet.");
                }else if(reportsToType.equalsIgnoreCase("c"))
                {
                    htmlText.append(" <br><br>\n\n\n <font color=\"red\">  <a href='http://w3.miraclesoft.com/Hubble/employee/timesheets/newGetTeamTimeSheet.action?" +
                        "employeeID="+empId+"&emptimeSheetID="+timeSheetID+"&type=c&resourceType="+resourceType+"&statusValue="+description+"&secStatusValue="+secDescription+"'> " +
                        " Click here </a> </font>  to Approve/Reject the TimeSheet.");
                }
                htmlText.append("<br><br> Thank you.</p></font>");
                htmlText.append("<font color='red', size='2' face='Arial'>*Note:Please do not reply to this e-mail. It was generated by our System.</font>");
                htmlText.append("</body></html>");
                
                
            
        }catch (Exception se){
            throw new ServiceLocatorException(se);
        }finally{
            
        }
        return htmlText.toString();
    }
    */
      public String getSubmitTimeSheetEmailLogBody(String empName,String wstDate,String wenDate,String empId,String reportsToType,int timeSheetID,String resourceType) throws ServiceLocatorException{
        
      
        int deletedRows = 0;
    
      StringBuffer htmlText = new StringBuffer();
       
        try{
            
            String url = Properties.getProperty("PROD.URL");
            
//             htmlText.append("<html><head><title>Mail From Hubble Portal</title>");
//                htmlText.append("</head><body><font color='blue' size='2' face='Arial'>");
//                htmlText.append("<p>A TimeSheet has been submitted by "+empName+" <br>");
//                htmlText.append("<p><u>Time Sheet Details:</u><br>");
//                htmlText.append("Week Start Date: "+wstDate+"<br>");
//                htmlText.append("Week End Date: "+wenDate+"<br>");
//               
//                if(!(reportsToType!=null)){
//              
//                     htmlText.append(" <br><br>\n\n\n <font color=\"red\">  <a href='"+url+"employee/timesheets/newGetTeamTimeSheet.action?" +
//                     
//                         "employeeID="+empId+"&emptimeSheetID="+timeSheetID+"&type=e&resourceType="+resourceType+"'> " +
//                        " Click here </a> </font>  to Approve/Reject the TimeSheet.");
//                }else if(reportsToType.equalsIgnoreCase("e"))
//                {
//                   htmlText.append(" <br><br>\n\n\n <font color=\"red\">  <a href='"+url+"employee/timesheets/newGetTeamTimeSheet.action?" +
//                
//                    "employeeID="+empId+"&emptimeSheetID="+timeSheetID+"&type=e&resourceType="+resourceType+"'> " +
//                        " Click here </a> </font>  to Approve/Reject the TimeSheet.");
//                }else if(reportsToType.equalsIgnoreCase("v"))
//                {
//                    htmlText.append(" <br><br>\n\n\n <font color=\"red\">  <a href='"+url+"employee/timesheets/newGetTeamTimeSheet.action?" +
//                        "employeeID="+empId+"&emptimeSheetID="+timeSheetID+"&type=v&resourceType="+resourceType+"'> " +
//                        " Click here </a> </font>  to Approve/Reject the TimeSheet.");
//                }else if(reportsToType.equalsIgnoreCase("c"))
//                {
//                    htmlText.append(" <br><br>\n\n\n <font color=\"red\">  <a href='"+url+"employee/timesheets/newGetTeamTimeSheet.action?" +
//                        "employeeID="+empId+"&emptimeSheetID="+timeSheetID+"&type=c&resourceType="+resourceType+"'> " +
//                        " Click here </a> </font>  to Approve/Reject the TimeSheet.");
//                }
//                htmlText.append("<br><br> Thank you.</p></font>");
//                htmlText.append("<font color='red', size='2' face='Arial'>*Note:Please do not reply to this e-mail. It was generated by our System.</font>");
//                htmlText.append("</body></html>");
                
                htmlText.append("<!DOCTYPE html><html><head><title>Mail From Hubble Portal</title>");
             htmlText.append("<meta charset='utf-8'><meta name='viewport' content='width=device-width, initial-scale=1'><meta http-equiv='X-UA-Compatible' content='IE=edge' />");
             htmlText.append("<style type='text/css'>body, table, td, a{-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%;}");
             htmlText.append("table, td{mso-table-lspace: 0pt; mso-table-rspace: 0pt;}");
             htmlText.append("img{-ms-interpolation-mode: bicubic;}");
             htmlText.append("img{border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none;}");
             htmlText.append("table{border-collapse: collapse !important;} body{height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important;}");
             htmlText.append("a[x-apple-data-detectors] {color: inherit !important; text-decoration: none !important;font-size: inherit !important;font-family: inherit !important; font-weight: inherit !important; line-height: inherit !important;}");
             htmlText.append("@media screen and (max-width: 525px) {");
             htmlText.append(".wrapper {width: 100% !important; max-width: 100% !important;}");
             htmlText.append(".logo img {margin: 0 auto !important;}");
             htmlText.append(".mobile-hide {display: none !important;}");
             htmlText.append(".img-max {max-width: 100% !important; width: 100% !important; height: auto !important;}");
             htmlText.append(".responsive-table {width: 100% !important;}");
             htmlText.append(".padding {padding: 10px 5% 15px 5% !important;}");
             htmlText.append(".padding-meta {padding: 30px 5% 0px 5% !important;text-align: center;}");
             htmlText.append(".padding-copy {padding: 10px 5% 10px 5% !important;text-align: center;}");
             htmlText.append(".no-padding {padding: 0 !important;}");
             htmlText.append(".section-padding {padding: 50px 15px 50px 15px !important;}");
             htmlText.append(".mobile-button-container {margin: 0 auto;width: 100% !important;}");
             htmlText.append(".mobile-button {padding: 15px !important;border: 0 !important;font-size: 16px !important;display: block !important;}");
             htmlText.append("} div[style*='margin: 16px 0;'] { margin: 0 !important; }");
                htmlText.append("</style></head><body style='margin: 0 !important; padding: 0 !important;'>");
                htmlText.append("<div style='display: none; font-size: 1px; color: #fefefe; line-height: 1px; font-family: Helvetica, Arial, sans-serif; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;'>Timesheet has been submitted by <b>"+empName+"</b></div>");
                htmlText.append("<table border='0' cellpadding='0' cellspacing='0' width='100%'><tr><td bgcolor='#ffffff' align='center'>");
                htmlText.append("<table border='0' cellpadding='0' cellspacing='0' width='100%' style='max-width: 500px;' class='wrapper'><tr><td align='center' valign='top' style='padding: 15px 0;' class='logo'>");
                htmlText.append("<a href='https://www.miraclesoft.com/' target='_blank'><img alt='Logo' src='https://www.miraclesoft.com/images/newsletters/miracle-logo-dark.png' width='165' height='auto' style='display: block; font-family: Helvetica, Arial, sans-serif; color: #ffffff; font-size: 16px;' border='0'></a>");
                htmlText.append("</td></tr></table></td></tr>");
                htmlText.append("<tr><td bgcolor='#ffffff' align='center' style='padding: 5px;'><table border='0' cellpadding='0' cellspacing='0' width='100%' style='max-width: 500px;' class='responsive-table'><tr><td>");
                htmlText.append("<table width='100%' border='0' cellspacing='0' cellpadding='0'><tr><td align='center' style='font-size: 26px; font-family: calibri; color: #2368a0; padding-top: 10px;' class='padding-copy'><b>Time Sheet</b></td></tr></table>");
                htmlText.append("</td></tr></table></td></tr>");
                htmlText.append("<tr><td bgcolor='#ffffff' align='center' style='padding: 15px;' class='padding'> <table border='0' cellpadding='0' cellspacing='0' width='100%' style='max-width: 500px;' class='responsive-table'><tr><td>");
                htmlText.append("<table width='100%' border='0' cellspacing='0' cellpadding='0'><tr><td align='left' style='padding: 5px 0 5px 0; font-size: 14px; line-height: 25px; font-family: calibri; color: #232527;' class='padding-copy'>Hello <b>Team,</b><br>Timesheet has been submitted by <b>"+empName+"</b></td></tr>");
                htmlText.append("<tr><td align='justify' style='padding: 5px 0 5px 0; border-top: 1px dashed #2368a0; border-bottom: 1px dashed #2368a0; font-size: 14px; line-height: 25px; font-family: calibri; color: #232527;' class='padding-copy'><b style='font-size: 14px; color: #ef4048;'>From Date: </b> "+wstDate+"<br><b style='font-size: 14px; color: #ef4048;'>To Date: </b> "+wenDate+"<br></td></tr>");
//                htmlText.append("<td align='left' style='padding: 5px 0 5px 0; font-size: 14px; line-height: 25px; font-family: calibri; color: #232527;' class='padding-copy'><b> Click <a href='https://www.miraclesoft.com/Hubble/employee/timesheets/newGetTeamTimeSheet.action?employeeID=3202&emptimeSheetID=4&type=e&resourceType=e' target='_blank' style='font-size: 14px; color: #2368a0;'>here</a> to Approve/Reject the TimeSheet.</b></td>");
//                htmlText.append("<p>A TimeSheet has been submitted by "+empName+" <br>");
//                htmlText.append("<p><u>Time Sheet Details:</u><br>");
//                htmlText.append("Week Start Date: "+wstDate+"<br>");
//                htmlText.append("Week End Date: "+wenDate+"<br>");
                // linkUrl="newgetEmpTimeSheet.action?empID={EmpID}&timeSheetID={TimeSheetId}&resourceType={ResourceType}&statusValue={Description}" imageBorder="0" 
                if(!(reportsToType!=null)){
               // htmlText.append(" <br><br>\n\n\n <font color=\"red\">  <a href='https://www.miraclesoft.com/Hubble/employee/timesheets/newGetTeamTimeSheet.action?" +
                    htmlText.append("<td align='left' style='padding: 5px 0 5px 0; font-size: 14px; line-height: 25px; font-family: calibri; color: #232527;' class='padding-copy'><b> Click <a href='"+url+"employee/timesheets/newGetTeamTimeSheet.action?" +
                            "employeeID="+empId+"&emptimeSheetID="+timeSheetID+"&type=e&resourceType="+resourceType+"'> " +
                            "<target='_blank' style='font-size: 14px; color: #2368a0;'>here</a> to Approve/Reject the TimeSheet.</b></td>");
//                            href='https://www.miraclesoft.com/Hubble/employee/timesheets/newGetTeamTimeSheet.action?employeeID=3202&emptimeSheetID=4&type=e&resourceType=e'");
//                     htmlText.append(" <br><br>\n\n\n <font color=\"red\">  <a href='"+url+"employee/timesheets/newGetTeamTimeSheet.action?" +
//                       // "employeeID="+empId+"&emptimeSheetID="+timeSheetID+"&type=e&resourceType="+resourceType+"&statusValue="+description+"&secStatusValue="+secDescription+"&teamType="+teamType+"'> " +
//                        // "employeeID="+empId+"&emptimeSheetID="+timeSheetID+"&type=e&resourceType="+resourceType+"&statusValue="+description+"&secStatusValue="+secDescription+"'> " +
//                         "employeeID="+empId+"&emptimeSheetID="+timeSheetID+"&type=e&resourceType="+resourceType+"'> " +
//                        " Click here </a> </font>  to Approve/Reject the TimeSheet.");
                }else if(reportsToType.equalsIgnoreCase("e"))
                {
                   htmlText.append("<td align='left' style='padding: 5px 0 5px 0; font-size: 14px; line-height: 25px; font-family: calibri; color: #232527;' class='padding-copy'><b> Click <a href='"+url+"employee/timesheets/newGetTeamTimeSheet.action?" +
                  //   htmlText.append(" <br><br>\n\n\n <font color=\"red\">  <a href='http://172.17.11.251:8084/Hubble/employee/timesheets/newGetTeamTimeSheet.action?" + 
                    "employeeID="+empId+"&emptimeSheetID="+timeSheetID+"&type=e&resourceType="+resourceType+"'> " +
                        "<target='_blank' style='font-size: 14px; color: #2368a0;'>here</a> to Approve/Reject the TimeSheet.</b></td>");
                }else if(reportsToType.equalsIgnoreCase("v"))
                {
                    htmlText.append("<td align='left' style='padding: 5px 0 5px 0; font-size: 14px; line-height: 25px; font-family: calibri; color: #232527;' class='padding-copy'><b> Click <a href='"+url+"employee/timesheets/newGetTeamTimeSheet.action?" +
                        "employeeID="+empId+"&emptimeSheetID="+timeSheetID+"&type=v&resourceType="+resourceType+"'> " +
                        "<target='_blank' style='font-size: 14px; color: #2368a0;'>here</a> to Approve/Reject the TimeSheet.</b></td>");
                }else if(reportsToType.equalsIgnoreCase("c"))
                {
                    htmlText.append("<td align='left' style='padding: 5px 0 5px 0; font-size: 14px; line-height: 25px; font-family: calibri; color: #232527;' class='padding-copy'><b> Click <a href='"+url+"employee/timesheets/newGetTeamTimeSheet.action?" +
                        "employeeID="+empId+"&emptimeSheetID="+timeSheetID+"&type=c&resourceType="+resourceType+"'> " +
                        "<target='_blank' style='font-size: 14px; color: #2368a0;'>here</a> to Approve/Reject the TimeSheet.</b></td>");
                }
                htmlText.append("</tr></table></td></tr>");
                htmlText.append("<tr><td> <table width='100%' border='0' cellspacing='0' cellpadding='0'><tr><td align='left' style='padding: 5px 0 5px 0; font-size: 14px; line-height: 22px; font-family: calibri; color: #8c8c8c; font-style: normal;' class='padding-copy'>Thanks & Regards,<br>Corporate Application Support Team, <br>Miracle Software Systems, Inc. <br>Email: hubble@miraclesoft.com <br>Phone: (+1)248-233-1814</td>");
                htmlText.append("</tr></table></td></tr>");
                htmlText.append("<tr><td><table width='100%' border='0' cellspacing='0' cellpadding='0'><tr><td align='left' style='padding: 5px 0 5px 0; font-size: 14px; line-height: 22px; font-family: calibri; color: #ef4048; font-style: italic;' class='padding-copy'>*Note: Please do not reply to this email as this is an automated notification</td>");
                htmlText.append("</tr></table></td></tr></table> </td></tr>");
                htmlText.append("<tr><td bgcolor='#ffffff' align='center' style='padding: 15px 0px;'> <table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' style='max-width: 500px;' class='responsive-table'><tr><td width='200' align='center' style='text-align: center;'><table width='200' cellpadding='0' cellspacing='0' align='center'><tr>");
                htmlText.append("<td width='10'><a href='https://www.facebook.com/miracle45625' target='_blank'><img src='https://www.miraclesoft.com/images/newsletters/facebook.png' alt='facebook' width='26' height='auto' data-max-width='40' data-customIcon='true' ></a></td>");
                htmlText.append("<td width='10'><a href='https://plus.google.com/+Team_MSS/videos' target='_blank'><img src='https://www.miraclesoft.com/images/newsletters/googleplus.png' alt='facebook' width='26' height='auto' data-max-width='40' data-customIcon='true' ></a></td>");
                htmlText.append("<td width='10'><a href='https://www.linkedin.com/company/miracle-software-systems-inc' target='_blank'><img src='https://www.miraclesoft.com/images/newsletters/linkedin.png' alt='facebook' width='26' height='auto' data-max-width='40' data-customIcon='true' ></a></td>");
                htmlText.append("<td width='10'><a href='https://www.youtube.com/c/Team_MSS' target='_blank'><img src='https://www.miraclesoft.com/images/newsletters/youtube.png' alt='facebook' width='26' height='auto' data-max-width='40' data-customIcon='true' ></a></td>");
                htmlText.append("<td width='10'><a href='https://twitter.com/Team_MSSs' target='_blank'><img src='https://www.miraclesoft.com/images/newsletters/twitter.png' alt='facebook' width='26' height='auto' data-max-width='40' data-customIcon='true' ></a></td>");
                htmlText.append("</tr></table></td></tr><tr><td height='10'></td></tr>");
                htmlText.append("<tr><td align='center' style='font-size: 14px; line-height: 20px; font-family: calibri; color:#666666;'>&copy; "+Calendar.getInstance().get(Calendar.YEAR)+" Miracle Software Systems<br></td></tr>");
                htmlText.append("</table></td></tr></table></body></html>");
            
        }catch (Exception se){
            throw new ServiceLocatorException(se);
        }finally{
            
        }
        return htmlText.toString();
    }
    

	public void getBiometricHours(NewTimeSheetAction newTimeSheetAction) throws ServiceLocatorException{
        
       // System.out.println("in service impl");
        int deletedRows = 0;
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        String weekDate = null;
        String queryString = "";
      
        try{
            connection = ConnectionProvider.getInstance().getConnection();
            statement = connection.createStatement();
           
            
             weekDate = DateUtility.getInstance().convertStringToMySQLDate(newTimeSheetAction.getTimeSheetVTO().getWeekDate1());
            queryString = "select  EmpNo,AttendanceDate,TIMESTAMPDIFF(hour,InTime,OutTime)hrs,statusCode from tblBIOAttendanceLogs where EmpNo="+newTimeSheetAction.getEmpNo()+" AND AttendanceDate = '"+weekDate+"'";
           // System.out.println("queryString1-->"+queryString) ;
            resultSet = statement.executeQuery(queryString);
             if(resultSet.next()){
                 newTimeSheetAction.getTimeSheetVTO().setBiometricSun(resultSet.getDouble("hrs"));
                 newTimeSheetAction.getTimeSheetVTO().setBmSunStatus(resultSet.getString("statusCode"));
             }
             
             weekDate = DateUtility.getInstance().convertStringToMySQLDate(newTimeSheetAction.getTimeSheetVTO().getWeekDate2());
            queryString = "select  EmpNo,AttendanceDate,TIMESTAMPDIFF(hour,InTime,OutTime)hrs,statusCode from tblBIOAttendanceLogs where EmpNo="+newTimeSheetAction.getEmpNo()+" AND AttendanceDate = '"+weekDate+"'";
         //   System.out.println("queryString2-->"+queryString) ;
            resultSet = statement.executeQuery(queryString);
             if(resultSet.next()){
                 newTimeSheetAction.getTimeSheetVTO().setBiometricMon(resultSet.getDouble("hrs"));
                 newTimeSheetAction.getTimeSheetVTO().setBmMonStatus(resultSet.getString("statusCode"));
             }
             
              weekDate = DateUtility.getInstance().convertStringToMySQLDate(newTimeSheetAction.getTimeSheetVTO().getWeekDate3());
            queryString = "select  EmpNo,AttendanceDate,TIMESTAMPDIFF(hour,InTime,OutTime)hrs,statusCode from tblBIOAttendanceLogs where EmpNo="+newTimeSheetAction.getEmpNo()+" AND AttendanceDate = '"+weekDate+"'";
           // System.out.println("queryString3-->"+queryString) ;
            resultSet = statement.executeQuery(queryString);
             if(resultSet.next()){
                 newTimeSheetAction.getTimeSheetVTO().setBiometricTus(resultSet.getDouble("hrs"));
                 newTimeSheetAction.getTimeSheetVTO().setBmTusStatus(resultSet.getString("statusCode"));
             }
             
             
              weekDate = DateUtility.getInstance().convertStringToMySQLDate(newTimeSheetAction.getTimeSheetVTO().getWeekDate4());
            queryString = "select  EmpNo,AttendanceDate,TIMESTAMPDIFF(hour,InTime,OutTime)hrs,statusCode from tblBIOAttendanceLogs where EmpNo="+newTimeSheetAction.getEmpNo()+" AND AttendanceDate = '"+weekDate+"'";
           // System.out.println("queryString4-->"+queryString) ;
            resultSet = statement.executeQuery(queryString);
             if(resultSet.next()){
                 newTimeSheetAction.getTimeSheetVTO().setBiometricWed(resultSet.getDouble("hrs"));
                 newTimeSheetAction.getTimeSheetVTO().setBmWedStatus(resultSet.getString("statusCode"));
             }
             
             
              weekDate = DateUtility.getInstance().convertStringToMySQLDate(newTimeSheetAction.getTimeSheetVTO().getWeekDate5());
            queryString = "select  EmpNo,AttendanceDate,TIMESTAMPDIFF(hour,InTime,OutTime)hrs,statusCode from tblBIOAttendanceLogs where EmpNo="+newTimeSheetAction.getEmpNo()+" AND AttendanceDate = '"+weekDate+"'";
           // System.out.println("queryString5-->"+queryString) ; 
            resultSet = statement.executeQuery(queryString);
             if(resultSet.next()){
                 newTimeSheetAction.getTimeSheetVTO().setBiometricThur(resultSet.getDouble("hrs"));
                 newTimeSheetAction.getTimeSheetVTO().setBmThurStatus(resultSet.getString("statusCode"));
             }
             
              weekDate = DateUtility.getInstance().convertStringToMySQLDate(newTimeSheetAction.getTimeSheetVTO().getWeekDate6());
            queryString = "select  EmpNo,AttendanceDate,TIMESTAMPDIFF(hour,InTime,OutTime)hrs,statusCode from tblBIOAttendanceLogs where EmpNo="+newTimeSheetAction.getEmpNo()+" AND AttendanceDate = '"+weekDate+"'";
           // System.out.println("queryString6-->"+queryString) ;
            resultSet = statement.executeQuery(queryString);
             if(resultSet.next()){
                 newTimeSheetAction.getTimeSheetVTO().setBiometricFri(resultSet.getDouble("hrs"));
                 newTimeSheetAction.getTimeSheetVTO().setBmFriStatus(resultSet.getString("statusCode"));
             }
             
              weekDate = DateUtility.getInstance().convertStringToMySQLDate(newTimeSheetAction.getTimeSheetVTO().getWeekDate7());
            queryString = "select  EmpNo,AttendanceDate,TIMESTAMPDIFF(hour,InTime,OutTime)hrs,statusCode from tblBIOAttendanceLogs where EmpNo="+newTimeSheetAction.getEmpNo()+" AND AttendanceDate = '"+weekDate+"'";
           // System.out.println("queryString7-->"+queryString) ; 
            resultSet = statement.executeQuery(queryString);
             if(resultSet.next()){
                 newTimeSheetAction.getTimeSheetVTO().setBiometricSat(resultSet.getDouble("hrs"));
                 newTimeSheetAction.getTimeSheetVTO().setBmSatStatus(resultSet.getString("statusCode"));
             }
            newTimeSheetAction.getTimeSheetVTO().setBiometricTotalHrs(newTimeSheetAction.getTimeSheetVTO().getBiometricSun()+newTimeSheetAction.getTimeSheetVTO().getBiometricMon()+newTimeSheetAction.getTimeSheetVTO().getBiometricTus()+newTimeSheetAction.getTimeSheetVTO().getBiometricWed()+newTimeSheetAction.getTimeSheetVTO().getBiometricThur()+newTimeSheetAction.getTimeSheetVTO().getBiometricFri()+newTimeSheetAction.getTimeSheetVTO().getBiometricSat());
        }catch (SQLException se){
            throw new ServiceLocatorException(se);
        }finally{
            try{
                 if(resultSet != null){
                    resultSet.close();
                    resultSet = null;
                }
                if(statement != null){
                    statement.close();
                    statement = null;
                }
                if(connection != null){
                    connection.close();
                    connection = null;
                }
            }catch (SQLException se){
                throw new ServiceLocatorException(se);
            }
        }
       
    }
	
	
	 public String generateExcelTimesheets(String year,int month) throws ServiceLocatorException {       
	        String filePath = "";
	       // StringBuffer sb = null;

	        Connection connection = null;
	        /**
	         * preStmt,preStmtTemp are reference variable for PreparedStatement .
	         */
	        PreparedStatement preStmt = null;

	        /**
	         * The queryString is useful to get queryString result to the particular
	         * jsp page
	         */
	      
	        /**
	         * The statement is useful to execute the above queryString
	         */
	        ResultSet resultSet = null;
	        HashMap map = null;
	        String generatedPath = "";
	        List finalList = new ArrayList();
	      
	        // System.out.println("the opscontactId"+contactId);
	        try {      
	        	
	        	// String PaySlipDestination = ReportProperties.getProperty("PayslipDownloadLocation");
         //  File file = new File(PaySlipDestination + "/" + DetailsMap.get("EmpName") + "_" + DetailsMap.get("EmpId") + ".pdf");
	            generatedPath = Properties.getProperty("AttendanceLoad.Operations.Path");          
	         //   System.out.println("the generatedPath"+generatedPath);
	         /*   File file = new File(generatedPath);      
	            if (!file.exists()) {
	                file.mkdirs();
	            }
	            
	            //FileOutputStream fileOut = new FileOutputStream(file.getAbsolutePath()+"/PFPortal.xls");
	            FileOutputStream fileOut = new FileOutputStream(file.getAbsolutePath()+Properties.getProperty("OS.Compatabliliy.Download")+"AttLoad.xls");
	            filePath = file.getAbsolutePath()+Properties.getProperty("OS.Compatabliliy.Download")+"AttLoad.xls";
	          */ 
	            File file = new File(Properties.getProperty("Payroll.Report.Path"));

	            if (!file.exists()) {
	                file.mkdirs();
	            }
             filePath = file.getAbsolutePath() + Properties.getProperty("OS.Compatabliliy.Download") + "AttendanceLoad.xls";
	            FileOutputStream fileOut = new FileOutputStream(filePath);

	            
	            
	         //   System.out.println(fileOut+"----->fileOut--------"+filePath);
	            
	            connection = ConnectionProvider.getInstance().getConnection();
	            String query = null;
	        
	          //  String empNo = DataSourceDataProvider.getInstance().getEmpNoByEmpId(empId);
	            
	            query = "SELECT PayRollId,OrgId,FirstName,LastName,PayrollDate,DaysInMonth,DaysWorked,DaysVacation,DaysHolidays,DaysWeekends,DaysProject,Ded_Others,Bonus,Earned_others FROM tblEmpAttendanceLaod WHERE year(PayrollDate) ='"+year+"' AND Month(PayrollDate) ='"+month+"'";            
	           
	            
	            
	            
	         //   System.out.println("the queryyyyyy"+query);
	            int j = 1;
	            preStmt = connection.prepareStatement(query);
	            resultSet = preStmt.executeQuery();
	            
	            
	            String HeaderNameAsPerDoc ="Attendance Load";
	                  
	                      
	                  
	            
	            while (resultSet.next()) {
	            	
	                String PayRollId = resultSet.getString("PayRollId");                
	                String OrgId = resultSet.getString("OrgId");
	                String FirstName = resultSet.getString("FirstName");               
	                String LastName = resultSet.getString("LastName");            
	                String PayrollDate = resultSet.getString("PayrollDate");                           
	                String DaysInMonth = resultSet.getString("DaysInMonth");                           
	                String DaysWorked = resultSet.getString("DaysWorked");   
	                String DaysVacation = resultSet.getString("DaysVacation");   
	                String DaysHolidays = resultSet.getString("DaysHolidays");                           
	                String DaysWeekends = resultSet.getString("DaysWeekends");                           
	                String DaysProject = resultSet.getString("DaysProject");                           
	                String Ded_Others = resultSet.getString("Ded_Others");                           
	                String Bonus = resultSet.getString("Bonus");  
	                String Earned_others = resultSet.getString("Earned_others"); 
	                 
	                  
	                  String NameAsPerDoc ="";
	                 
	                  
	                 
	                map = new HashMap();
	                map.put("SNO", String.valueOf(j));
	                map.put("PayRollId", PayRollId);
	                map.put("OrgId", OrgId);
	                map.put("FirstName", FirstName);
	                map.put("LastName", LastName);
	                map.put("PayrollDate", PayrollDate);
	                map.put("DaysInMonth", DaysInMonth);
	                map.put("DaysWorked", DaysWorked);
	                map.put("DaysVacation", DaysVacation);
	                map.put("DaysHolidays", DaysHolidays);
	                map.put("DaysWeekends", DaysWeekends);
	                map.put("DaysProject", DaysProject);
	                map.put("Ded_Others", Ded_Others);
	                map.put("Bonus", Bonus);
	                map.put("Earned_others", Earned_others);
	                finalList.add(map);
	                j++;
	              

	            }
	         

	            if (finalList.size() > 0) {
	            	// filePath = file.getAbsolutePath() + Properties.getProperty("OS.Compatabliliy.Download") + "PayrollReport.xls";
	               // filePath = file.getAbsolutePath()+"/PFPortal.xls";
	              //  System.out.println("the file path"+filePath);
//	                HSSFWorkbook hssfworkbook = new HSSFWorkbook();
//	                HSSFSheet sheet = hssfworkbook.createSheet("PF Portal sheet");
//	                
	                 HSSFWorkbook hssfworkbook = new HSSFWorkbook();
	      HSSFSheet sheet = hssfworkbook.createSheet("Employee AttendanceLoad sheet");
//	     
//	      HSSFCellStyle cs = hssfworkbook.createCellStyle();
//	      HSSFDataFormat df = hssfworkbook.createDataFormat();
//	                
	                
	                

	                HSSFFont timesBoldFont1 = hssfworkbook.createFont();               
	                timesBoldFont1.setFontHeightInPoints((short) 13);
	                timesBoldFont1.setColor(HSSFColor.BLACK.index);
	                timesBoldFont1.setFontName("Arial");
	               
	                HSSFCellStyle cellColor = hssfworkbook.createCellStyle();
	                cellColor.setFillForegroundColor(HSSFColor.GREY_40_PERCENT.index);
	                cellColor.setAlignment(HSSFCellStyle.ALIGN_LEFT);
	                cellColor.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
	                cellColor.setBorderTop((short) 1); // single line border
	                cellColor.setBorderBottom((short) 1); // single line border
	                cellColor.setFont(timesBoldFont1);

	                HSSFCellStyle cellColor1 = hssfworkbook.createCellStyle();

	                cellColor1.setFillForegroundColor(HSSFColor.WHITE.index);
	                cellColor1.setAlignment(HSSFCellStyle.ALIGN_LEFT);
	                cellColor1.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
	                cellColor1.setBorderTop((short) 1); // single line border
	                cellColor1.setBorderBottom((short) 1); // single line border
	                cellColor1.setFont(timesBoldFont1);


	                HSSFCellStyle cs = hssfworkbook.createCellStyle();

	                HSSFCellStyle headercs = hssfworkbook.createCellStyle();
	                headercs.setFillForegroundColor(HSSFColor.BLUE.index);
	                headercs.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
	                headercs.setBorderTop((short) 1); // single line border
	                headercs.setBorderBottom((short) 1); // single line border
	                // cs.setFont(timesBoldFont1);

	                HSSFFont timesBoldFont = hssfworkbook.createFont();
	                timesBoldFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
	                timesBoldFont.setFontHeightInPoints((short) 13);
	                timesBoldFont.setColor(HSSFColor.WHITE.index);
	                timesBoldFont.setFontName("Calibri");
	                headercs.setFont(timesBoldFont);
	                HSSFFont footerFont = hssfworkbook.createFont();
	                footerFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
	                timesBoldFont.setFontHeightInPoints((short) 13);
	                footerFont.setFontName("Calibri");

	                HSSFCellStyle footercs = hssfworkbook.createCellStyle();
	                footercs.setFont(footerFont);


	                HSSFDataFormat df = hssfworkbook.createDataFormat();
	                HSSFRow row = sheet.createRow((short) 0);
	                HSSFCell cell = row.createCell((short) 0);

	                HSSFCell cell1 = row.createCell((short) 1);

	                HSSFCell cell2 = row.createCell((short) 2);
	                HSSFCell cell3 = row.createCell((short) 3);

	                HSSFCell cell4 = row.createCell((short) 4);
	                HSSFCell cell5 = row.createCell((short) 5);
	                HSSFCell cell6 = row.createCell((short) 6);
	                HSSFCell cell7 = row.createCell((short) 7);
	                HSSFCell cell8 = row.createCell((short) 8);
	                HSSFCell cell9 = row.createCell((short) 9);
	                HSSFCell cell10 = row.createCell((short) 10);
	                HSSFCell cell11 = row.createCell((short) 11);
	                HSSFCell cell12 = row.createCell((short) 12);
	                HSSFCell cell13 = row.createCell((short) 13);
	                HSSFCell cell14 = row.createCell((short) 14);
	               
	             
	            	cell.setCellValue("SNO");
	                cell1.setCellValue("PayRollId");
	                cell2.setCellValue("OrgId");
	                cell3.setCellValue("FirstName");
	                cell4.setCellValue("LastName");
	                cell5.setCellValue("PayrollDate");
	                cell6.setCellValue("DaysInMonth");
	                cell7.setCellValue("DaysWorked");
	                cell8.setCellValue("DaysVacation");
	                cell9.setCellValue("DaysHolidays");
	                cell10.setCellValue("DaysWeekends");
	                cell11.setCellValue("DaysProject");
	                cell12.setCellValue("Ded_Others");
	                cell13.setCellValue("Bonus");
	                cell14.setCellValue("Earned_others");


	//HeaderNameAsPerDoc


	                cell.setCellStyle(headercs);
	                cell1.setCellStyle(headercs);
	                cell2.setCellStyle(headercs);
	                cell3.setCellStyle(headercs);
	                cell4.setCellStyle(headercs);
	                cell5.setCellStyle(headercs);
	                cell6.setCellStyle(headercs);
	                cell7.setCellStyle(headercs);
	                cell8.setCellStyle(headercs);
	                cell9.setCellStyle(headercs);
	                cell10.setCellStyle(headercs);
	                cell11.setCellStyle(headercs);
	                cell12.setCellStyle(headercs);
	                cell13.setCellStyle(headercs);
	                cell14.setCellStyle(headercs);

	                int count = 1;

	                if (finalList.size() > 0) {
	                    Map stateHistorylMap = null;
	                    for (int i = 0; i < finalList.size(); i++) {
	                        stateHistorylMap = (Map) finalList.get(i);
	                        row = sheet.createRow((short) count++);
	                        cell = row.createCell((short) 0);

	                        cell1 = row.createCell((short) 1);
	                        cell2 = row.createCell((short) 2);
	                        cell3 = row.createCell((short) 3);
	                        cell4 = row.createCell((short) 4);
	                        cell5 = row.createCell((short) 5);
	                        cell6 = row.createCell((short) 6);
	                        cell7 = row.createCell((short) 7);
	                        cell8 = row.createCell((short) 8);
	                        cell9 = row.createCell((short) 9);
	                        cell10 = row.createCell((short) 10);
	                        cell11= row.createCell((short) 11);
	                        cell12= row.createCell((short) 12);
	                        cell13= row.createCell((short) 13);
	                        cell14= row.createCell((short) 14);

	                       


	                        cell.setCellValue((String) stateHistorylMap.get("SNO"));
	                        cell1.setCellValue((String) stateHistorylMap.get("PayRollId"));
	                        cell2.setCellValue((String) stateHistorylMap.get("OrgId"));                   
	                        cell3.setCellValue((String) stateHistorylMap.get("FirstName"));
	                        cell4.setCellValue((String) stateHistorylMap.get("LastName"));
	                        cell5.setCellValue((String) stateHistorylMap.get("PayrollDate"));
	                        cell6.setCellValue((String) stateHistorylMap.get("DaysInMonth"));
	                        cell7.setCellValue((String) stateHistorylMap.get("DaysWorked"));
	                        cell8.setCellValue((String) stateHistorylMap.get("DaysVacation"));
	                        cell9.setCellValue((String) stateHistorylMap.get("DaysHolidays"));
	                        cell10.setCellValue((String) stateHistorylMap.get("DaysWeekends"));
	                        cell11.setCellValue((String) stateHistorylMap.get("DaysProject"));
	                        cell12.setCellValue((String) stateHistorylMap.get("Ded_Others"));
	                        cell13.setCellValue((String) stateHistorylMap.get("Bonus"));
	                        cell14.setCellValue((String) stateHistorylMap.get("Earned_others"));

	         cell.setCellStyle(cs);
	         cell1.setCellStyle(cs);
	         cell2.setCellStyle(cs);
	         cell3.setCellStyle(cs);
	         cell4.setCellStyle(cs);
	         cell5.setCellStyle(cs);
	         cell6.setCellStyle(cs);
	         cell7.setCellStyle(cs);
	         cell8.setCellStyle(cs);
	         cell9.setCellStyle(cs);
	         cell10.setCellStyle(cs);
	         cell11.setCellStyle(cs);
	         cell12.setCellStyle(cs);
	         cell13.setCellStyle(cs);
	         cell14.setCellStyle(cs);

	                        if (count % 2 == 0) {
	                            cell.setCellStyle(cellColor1);
	                            cell1.setCellStyle(cellColor1);
	                            cell2.setCellStyle(cellColor1);
	                            cell3.setCellStyle(cellColor1);
	                            cell4.setCellStyle(cellColor1);
	                            cell5.setCellStyle(cellColor1);
	                            cell6.setCellStyle(cellColor1);
	                            cell7.setCellStyle(cellColor1);
	                            cell8.setCellStyle(cellColor1);
	                            cell9.setCellStyle(cellColor1);
	                            cell10.setCellStyle(cellColor1);
	                            cell11.setCellStyle(cellColor1);
	                            cell12.setCellStyle(cellColor1);
	                            cell13.setCellStyle(cellColor1);
	                            cell14.setCellStyle(cellColor1);

	                        } else {
	                            cell.setCellStyle(cellColor);
	                            cell1.setCellStyle(cellColor);
	                            cell2.setCellStyle(cellColor);
	                            cell3.setCellStyle(cellColor);
	                            cell4.setCellStyle(cellColor);
	                            cell5.setCellStyle(cellColor);
	                            cell6.setCellStyle(cellColor);
	                            cell7.setCellStyle(cellColor);
	                            cell8.setCellStyle(cellColor);
	                            cell9.setCellStyle(cellColor);
	                            cell10.setCellStyle(cellColor);
	                            cell11.setCellStyle(cellColor);
	                            cell12.setCellStyle(cellColor);
	                            cell13.setCellStyle(cellColor);
	                            cell14.setCellStyle(cellColor);
	                        }
	                    }
	                    row = sheet.createRow((short) count++);
	                    cell = row.createCell((short) 0);

	                    cell1 = row.createCell((short) 1);
	                    cell2 = row.createCell((short) 2);
	                    cell3 = row.createCell((short) 3);
	                    cell4 = row.createCell((short) 4);
	                    cell5 = row.createCell((short) 5);
	                    cell6 = row.createCell((short) 6);
	                    cell7 = row.createCell((short) 7);
	                    cell8 = row.createCell((short) 8);
	                    cell9 = row.createCell((short) 9);
	                    cell10 = row.createCell((short) 10);
	                    cell11 = row.createCell((short) 11);
	                    cell12 = row.createCell((short) 12);
	                    cell13 = row.createCell((short) 13);
	                    cell14 = row.createCell((short) 14);
	                    
	                    cell.setCellStyle(footercs);
	                    cell1.setCellStyle(footercs);
	                    cell2.setCellStyle(footercs);
	                    cell3.setCellStyle(footercs);

	                    cell4.setCellStyle(footercs);
	                    cell5.setCellStyle(footercs);
	                    cell6.setCellStyle(footercs);
	                    cell7.setCellStyle(footercs);
	                    cell8.setCellStyle(footercs);
	                    cell9.setCellStyle(footercs);
	                    cell10.setCellStyle(footercs);
	                    cell11.setCellStyle(footercs);
	                    cell12.setCellStyle(footercs);
	                    cell13.setCellStyle(footercs);
	                    cell14.setCellStyle(footercs);
	                }
	                HSSFCellStyle totalSum = hssfworkbook.createCellStyle();
	                totalSum.setFillForegroundColor(HSSFColor.LIGHT_GREEN.index);
	                totalSum.setAlignment(HSSFCellStyle.ALIGN_RIGHT);
	                totalSum.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
	                totalSum.setBorderTop((short) 1); // single line border
	                totalSum.setBorderBottom((short) 1); // single line border
	                totalSum.setFont(timesBoldFont1);

	                   
	                sheet.autoSizeColumn((int) 0);               
	                sheet.autoSizeColumn((int) 3);
	                sheet.autoSizeColumn((int) 4);
	                sheet.autoSizeColumn((int) 6);
	                sheet.autoSizeColumn((int) 7);
	                sheet.autoSizeColumn((int) 8);
	                sheet.autoSizeColumn((int) 9);
	                sheet.autoSizeColumn((int) 10);
	                sheet.autoSizeColumn((int) 13);
	               // sheet.setColumnWidth(1, 50 * 256);
	                sheet.setColumnWidth(2, 35 * 256);
	                sheet.setColumnWidth(5, 25 * 256);
	                  hssfworkbook.write(fileOut);
	                fileOut.flush();
	                fileOut.close();


	            }


	        } catch (FileNotFoundException fne) {

	            fne.printStackTrace();
	        } catch (IOException ioe) {

	            ioe.printStackTrace();
	        } catch (Exception ex) {
	            ex.printStackTrace();

	        } finally {         
	            try {
	                if (resultSet != null) {
	                    resultSet.close();
	                    resultSet = null;
	                }
	                if (preStmt != null) {
	                    preStmt.close();
	                    preStmt = null;
	                }
	                if (connection != null) {
	                    connection.close();
	                    connection = null;
	                }
	            } catch (Exception se) {
	                se.printStackTrace();
	            }
	        }
	       //  System.out.println("the file path"+filePath);
	        return filePath;


	    }
		   
	 public String getRejectTimeSheetEmailLogBodyNewPmo(String empName,String wstDate,String wenDate,String loggedUsername,String rejReason) throws ServiceLocatorException{
	        
	       // System.out.println("in service impl");
	        int deletedRows = 0;
	      //  Connection connection = null;
	      //  Statement statement = null;
	       // String queryString = "Delete from tblTimeSheets  WHERE id="+id;
	      StringBuffer htmlText = new StringBuffer();
	       
	        try{
	                
//	                 htmlText.append("<html><head><title>Mail From Hubble Portal</title>");
//	                htmlText.append("</head><body><font color='blue' size='2' face='Arial'>");
//	                htmlText.append("<p>Hello "+empName+"</p>");
//	                htmlText.append("<p>Your Time Sheet has been Rejected.</p>");
//	                htmlText.append("<p><u><b>Time Sheet Details:</b></u><br>");
//	                htmlText.append("Week Start Date: "+wstDate+"<br>");
//	                htmlText.append("Week End Date: "+wenDate+"<br>");
//	                htmlText.append("Thank you.</p></font>");
//	                htmlText.append("<font color='red', size='2' face='Arial'>*Note:Please do not reply to this e-mail.  It was generated by our System.</font>");
//	                htmlText.append("</body></html>");
	            
	            
	            htmlText.append("<!DOCTYPE html><html><head><meta charset='utf-8'><meta name='viewport' content='width=device-width, initial-scale=1'><meta http-equiv='X-UA-Compatible' content='IE=edge' />");
	            htmlText.append("<style type='text/css'>body, table, td, a{-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%;}");
	            htmlText.append("table, td{mso-table-lspace: 0pt; mso-table-rspace: 0pt;}");
	            htmlText.append("img{-ms-interpolation-mode: bicubic;}");
	            htmlText.append("img{border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none;}");
	            htmlText.append("table{border-collapse: collapse !important;}");
	            htmlText.append("body{height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important;}");
	            htmlText.append("a[x-apple-data-detectors] {color: inherit !important;text-decoration: none !important;font-size: inherit !important;font-family: inherit !important;font-weight: inherit !important;line-height: inherit !important;}");
	            htmlText.append("@media screen and (max-width: 525px) { .wrapper {width: 100% !important;max-width: 100% !important;}");
	            htmlText.append(".logo img {margin: 0 auto !important;} .mobile-hide {display: none !important;} .img-max {max-width: 100% !important;width: 100% !important; height: auto !important; }");
	            htmlText.append(".responsive-table {width: 100% !important;}");
	            htmlText.append(".padding {padding: 10px 5% 15px 5% !important;}");
	            htmlText.append(".padding-meta {padding: 30px 5% 0px 5% !important;text-align: center;}");
	            htmlText.append(".padding-copy {padding: 10px 5% 10px 5% !important;text-align: center;}");
	            htmlText.append(".no-padding {padding: 0 !important;}");
	            htmlText.append(".section-padding {padding: 50px 15px 50px 15px !important;}");
	            htmlText.append(".mobile-button-container {margin: 0 auto;width: 100% !important;}");
	            htmlText.append(".mobile-button {padding: 15px !important;border: 0 !important;font-size: 16px !important;display: block !important;} }");
	            htmlText.append("</style></head><body style='margin: 0 !important; padding: 0 !important;'>");
	            htmlText.append("<div style='display: none; font-size: 1px; color: #fefefe; line-height: 1px; font-family: Helvetica, Arial, sans-serif; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;'>Hello <b>"+empName+",</b><br>Your Time Sheet has been Rejected By <b>"+loggedUsername+"..</div>");
	            htmlText.append("<table border='0' cellpadding='0' cellspacing='0' width='100%'><tr><td bgcolor='#ffffff' align='center'>");
	            htmlText.append("<table border='0' cellpadding='0' cellspacing='0' width='100%' style='max-width: 500px;' class='wrapper'><tr>");
	            htmlText.append("<td align='center' valign='top' style='padding: 15px 0;' class='logo'>");
	            htmlText.append("<a href='https://www.miraclesoft.com/' target='_blank'><img alt='Logo' src='https://www.miraclesoft.com/images/newsletters/miracle-logo-dark.png' width='165' height='auto' style='display: block; font-family: Helvetica, Arial, sans-serif; color: #ffffff; font-size: 16px;' border='0'></a>");
	            htmlText.append("</td></tr></table></td></tr>");
	            htmlText.append("<tr><td bgcolor='#ffffff' align='center' style='padding: 5px;'>");
	            htmlText.append("<table border='0' cellpadding='0' cellspacing='0' width='100%' style='max-width: 500px;' class='responsive-table'><tr><td>");
	            htmlText.append("<table width='100%' border='0' cellspacing='0' cellpadding='0'>");
	            htmlText.append("<tr><td align='center' style='font-size: 26px; font-family: calibri; color: #2368a0; padding-top: 10px;' class='padding-copy'><b>Time Sheet Rejection</b></td></tr>");
	            htmlText.append("</table></td></tr></table></td></tr>");
	            htmlText.append("<tr><td bgcolor='#ffffff' align='center' style='padding: 15px;' class='padding'>");
	            htmlText.append("<table border='0' cellpadding='0' cellspacing='0' width='100%' style='max-width: 500px;' class='responsive-table'><tr><td>");
	            htmlText.append("<table width='100%' border='0' cellspacing='0' cellpadding='0'><tr>");
	            htmlText.append("<td align='left' style='padding: 5px 0 5px 0; font-size: 14px; line-height: 25px; font-family: calibri; color: #232527;' class='padding-copy'>Hello <b>"+empName+",</b><br>Your Time Sheet has been Rejected By <b>"+loggedUsername+".     </td></tr>");
	            htmlText.append("<tr><td align='justify' style='padding: 5px 0 5px 0; border-top: 1px dashed #2368a0; border-bottom: 1px dashed #2368a0; font-size: 14px; line-height: 25px; font-family: calibri; color: #232527;' class='padding-copy'><b style='font-size: 14px; color: #ef4048;'>From Date: </b> "+wstDate+"<br><b style='font-size: 14px; color: #ef4048;'>To Date: </b> "+wenDate+"<br><b style='font-size: 14px; color: #ef4048;'>Reject Reason: </b> "+rejReason+"<br>");
	            htmlText.append("</td></tr></tr></table></td></tr>");
	            htmlText.append("<tr><td><table width='100%' border='0' cellspacing='0' cellpadding='0'><tr>");
	            htmlText.append("<td align='left' style='padding: 5px 0 5px 0; font-size: 14px; line-height: 22px; font-family: calibri; color: #8c8c8c; font-style: normal;' class='padding-copy'>");
	            htmlText.append("Thanks & Regards,<br>Corporate Application Support Team, <br>Miracle Software Systems, Inc. <br>Email: hubble@miraclesoft.com <br>Phone: (+1)248-233-1814");
	            htmlText.append("</td></tr></table></td></tr>");
	            htmlText.append("<tr><td> <table width='100%' border='0' cellspacing='0' cellpadding='0'><tr>");
	            htmlText.append("<td align='left' style='padding: 5px 0 5px 0; font-size: 14px; line-height: 22px; font-family: calibri; color: #ef4048; font-style: italic;' class='padding-copy'>*Note: Please do not reply to this email as this is an automated notification</td>");
	            htmlText.append("</tr></table></td></tr></table></td></tr>");
	            htmlText.append("<tr><td bgcolor='#ffffff' align='center' style='padding: 15px 0px;'>");
	            htmlText.append("<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' style='max-width: 500px;' class='responsive-table'>");
	            htmlText.append("<tr><td width='200' align='center' style='text-align: center;'><table width='200' cellpadding='0' cellspacing='0' align='center'>");
	            htmlText.append("<tr><td width='10'><a href='https://www.facebook.com/miracle45625' target='_blank'><img src='https://www.miraclesoft.com/images/newsletters/facebook.png' alt='facebook' width='26' height='auto' data-max-width='40' data-customIcon='true' ></a></td>");
	            htmlText.append("<td width='10'><a href='https://plus.google.com/+Team_MSS/videos' target='_blank'><img src='https://www.miraclesoft.com/images/newsletters/googleplus.png' alt='facebook' width='26' height='auto' data-max-width='40' data-customIcon='true' ></a></td>");
	            htmlText.append("<td width='10'><a href='https://www.linkedin.com/company/miracle-software-systems-inc' target='_blank'><img src='https://www.miraclesoft.com/images/newsletters/linkedin.png' alt='facebook' width='26' height='auto' data-max-width='40' data-customIcon='true' ></a></td>");
	            htmlText.append("<td width='10'><a href='https://www.youtube.com/c/Team_MSS' target='_blank'><img src='https://www.miraclesoft.com/images/newsletters/youtube.png' alt='facebook' width='26' height='auto' data-max-width='40' data-customIcon='true' ></a></td>");
	            htmlText.append("<td width='10'><a href='https://twitter.com/Team_MSSs' target='_blank'><img src='https://www.miraclesoft.com/images/newsletters/twitter.png' alt='facebook' width='26' height='auto' data-max-width='40' data-customIcon='true' ></a></td>");
	            htmlText.append("</tr></table></td></tr><tr><td height='10'></td></tr>");
	            htmlText.append("<tr><td align='center' style='font-size: 14px; line-height: 20px; font-family: calibri; color:#666666;'>&copy; "+Calendar.getInstance().get(Calendar.YEAR)+" Miracle Software Systems<br></td></tr>");
	            htmlText.append("</table></td></tr></table></body></html>");
	        // System.out.println("htmlText.toString()==>"+htmlText.toString());
	        }catch (Exception se){
	            throw new ServiceLocatorException(se);
	        }finally{
	            
	        }
	        return htmlText.toString();
	    }
    
	 
	 /*nagalakshmi telluri
	  * 11/9/2018
	  */
	 
	 public  String generateOperationsTimeSheetReport(String startDate, String endDate, String opsConatctId,String deptId, String practice, String reportsTo) {
			// System.out.println("generateOperationsTimeSheetReport ");

			// System.out.println("startDate"+startDate+"enddate"+endDate+"ops"+opsConatctId);
			String filePath = "";
			Connection connection = null;

			CallableStatement callableStatement = null;
			HashMap map = null;
			ResultSet resultSet = null;
			PreparedStatement preStmt = null, preStmtTemp = null;
			List finalList = new ArrayList();
			try {
				File file = new File(ReportProperties.getProperty("OpeartionsTimeSheetReport.Path"));

				if (!file.exists()) {
					file.mkdirs();
				}
				String start = DateUtility.getInstance().convertStringToMySQLDate(startDate);
				String end = DateUtility.getInstance().convertStringToMySQLDate(endDate);

				String header = "OpeartionsTimeSheetReport-" + start + "-" + end;

				SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss");
				Date today = new Date();
				String date = DATE_FORMAT.format(today);

				String fileName1 = "OpeartionsTimeSheetReport-" + date;

				FileOutputStream fileOut = new FileOutputStream(
						ReportProperties.getProperty("OpeartionsTimeSheetReport.Path") + "/" + fileName1 + ".xls");

				

				Map startandenddate = DateUtility.getInstance().getweekstartenddates(startDate, endDate);

				// System.out.println("startandenddate"+startandenddate.toString());

				// if (finalList.size() > 0) {
				filePath =ReportProperties.getProperty("OpeartionsTimeSheetReport.Path") + "/" + fileName1
						+ ".xls";
				HSSFRow row1;
				HSSFWorkbook workbook = new HSSFWorkbook();
				HSSFSheet worksheet = workbook.createSheet(fileName1);
				for (int i = 0; i < (9 + startandenddate.size()); i++) {
					worksheet.setColumnWidth(i, 40 * 256);
				}
				// System.out.println("size"+startandenddate.size());
				int count = 1;
				/*
				 * while(true) { worksheet.setColumnWidth(count+5, 40 * 256);
				 * if(count==startandenddate.size()) { break; } count++;
				 * 
				 * }
				 */
				/*
				 * for (int i = 6; i <=startandenddate.size(); i++) {
				 * worksheet.setColumnWidth(i, 40 * 256); }
				 */
				HSSFFont timesBoldFont = workbook.createFont();
				timesBoldFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
				timesBoldFont.setColor(HSSFColor.WHITE.index);

				timesBoldFont.setFontName("Calibri");
				HSSFCellStyle headercs = workbook.createCellStyle();
				headercs.setFillForegroundColor(HSSFColor.PALE_BLUE.index);
				headercs.setAlignment(HSSFCellStyle.ALIGN_LEFT);
				headercs.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
				headercs.setBorderTop((short) 1); // single line border
				headercs.setBorderBottom((short) 1); // single line border
				headercs.setFont(timesBoldFont);

				HSSFFont footerFont = workbook.createFont();
				footerFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
				footerFont.setFontName("Calibri");

				HSSFFont font4 = workbook.createFont();
				font4.setColor(HSSFColor.WHITE.index);
				font4.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
				font4.setFontHeightInPoints((short) 14);
				font4.setFontName("Calibri");

				HSSFCellStyle cellStyle = workbook.createCellStyle();
				cellStyle.setFillForegroundColor(HSSFColor.SKY_BLUE.index);
				cellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
				cellStyle.setFont(font4);

				HSSFFont font1 = workbook.createFont();
				font1.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
				font1.setFontHeightInPoints((short) 14);
				font1.setFontName("Calibri");

				HSSFCellStyle cs = workbook.createCellStyle();
				cs.setFillForegroundColor(HSSFColor.GREY_25_PERCENT.index);
				cs.setAlignment(HSSFCellStyle.ALIGN_LEFT);
				cs.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
				cs.setBorderTop((short) 1); // single line border
				cs.setBorderBottom((short) 1); // single line border
				cs.setFont(font1);

				HSSFCellStyle cs1 = workbook.createCellStyle();
				cs1.setFillForegroundColor(HSSFColor.ROYAL_BLUE.index);
				cs1.setAlignment(HSSFCellStyle.ALIGN_LEFT);
				cs1.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
				cs1.setFont(font4);
				cs1.setBorderTop((short) 1); // single line border
				cs1.setBorderBottom((short) 1); // single line border

				HSSFCellStyle cs2 = workbook.createCellStyle();
				cs2.setFillForegroundColor(HSSFColor.WHITE.index);
				cs2.setAlignment(HSSFCellStyle.ALIGN_LEFT);
				cs2.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
				cs2.setBorderTop((short) 1); // single line border
				cs2.setBorderBottom((short) 1); // single line border
				cs2.setFont(font1);

				HSSFFont font3 = workbook.createFont();
				font3.setFontHeightInPoints((short) 14);
				font3.setFontName("Calibri");

				HSSFCellStyle cs3 = workbook.createCellStyle();
				cs3.setFillForegroundColor(HSSFColor.GREY_25_PERCENT.index);
				cs3.setAlignment(HSSFCellStyle.ALIGN_LEFT);
				cs3.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
				cs3.setFont(font3);
				cs3.setBorderTop((short) 1); // single line border
				cs3.setBorderBottom((short) 1); // single line border

				HSSFCellStyle cellStyle1 = workbook.createCellStyle();
				cellStyle1.setFillForegroundColor(HSSFColor.ROYAL_BLUE.index);
				cellStyle1.setAlignment(HSSFCellStyle.ALIGN_LEFT);
				cellStyle1.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
				cellStyle1.setBorderTop((short) 1); // single line border
				cellStyle1.setBorderBottom((short) 1); // single line border
				cellStyle1.setFont(font4);

				HSSFCellStyle cs4 = workbook.createCellStyle();
				cs4.setFillForegroundColor(HSSFColor.WHITE.index);
				cs4.setAlignment(HSSFCellStyle.ALIGN_LEFT);
				cs4.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
				cs4.setBorderTop((short) 1); // single line border
				cs4.setBorderBottom((short) 1); // single line border
				cs4.setFont(font3);

				/*
				 * //start SimpleDateFormat DATE_FORMAT = new
				 * SimpleDateFormat("dd-MM-yyyy"); Date today = new Date(); String
				 * date = DATE_FORMAT.format(today);
				 */
				row1 = worksheet.createRow((short) 0);

				HSSFCell cellpo0 = row1.createCell((short) 0);
				HSSFCell cellpo1 = row1.createCell((short) 1);
				HSSFCell cellpo2 = row1.createCell((short) 2);
				HSSFCell cellpo3 = row1.createCell((short) 3);
				HSSFCell cellpo4 = row1.createCell((short) 4);
				HSSFCell cellpo5 = row1.createCell((short) 5);
				HSSFCell cellpo6 = row1.createCell((short) 6);
				HSSFCell cellpo7 = row1.createCell((short) 7);
				HSSFCell cellpo8 = row1.createCell((short) 8);
				HSSFCell cellpo9 = row1.createCell((short) 9);
				HSSFCell cellpo10 = row1.createCell((short) 10);
				HSSFCell cellpo11 = row1.createCell((short) 11);

				row1 = worksheet.createRow((short) 0);
				Cell cell[] = new Cell[32];
				for (int i = 0; i < (8 + startandenddate.size()); i++) {
					cell[i] = row1.createCell((short) i);
				}
				/*
				 * for (int i = 6; i <startandenddate.size() ; i++) { cell[i] =
				 * row1.createCell((short) i); }
				 */

				cell[0].setCellValue(header);
				HSSFCellStyle cellStyleHead = workbook.createCellStyle();
				cellStyleHead.setFont(timesBoldFont);
				cellStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
				cellStyle.setFillBackgroundColor(HSSFColor.PALE_BLUE.index);
				cellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
				cell[0].setCellStyle(cellStyle);
				worksheet.addMergedRegion(CellRangeAddress.valueOf("A1:AF1"));

				worksheet.addMergedRegion(CellRangeAddress.valueOf("A1:AA1"));
				row1 = worksheet.createRow((short) 1);

				cell[0] = row1.createCell((short) 0);
				cell[0].setCellValue("Sno");
				cellStyleHead.setFont(timesBoldFont);
				cellStyleHead.setAlignment(HSSFCellStyle.ALIGN_LEFT);
				cell[0].setCellStyle(cs1);
				worksheet.addMergedRegion(CellRangeAddress.valueOf("A2:A3"));

				cell[0] = row1.createCell((short) 1);
				cell[0].setCellValue("EmpNo");
				cellStyleHead.setFont(timesBoldFont);
				cellStyleHead.setAlignment(HSSFCellStyle.ALIGN_LEFT);
				cell[0].setCellStyle(cs1);
				worksheet.addMergedRegion(CellRangeAddress.valueOf("B2:B3"));

				cell[0] = row1.createCell((short) 2);
				cell[0].setCellValue("EmpName");
				cellStyleHead.setFont(timesBoldFont);
				cellStyleHead.setAlignment(HSSFCellStyle.ALIGN_LEFT);
				cell[0].setCellStyle(cs1);
				worksheet.addMergedRegion(CellRangeAddress.valueOf("C2:C3"));

				cell[0] = row1.createCell((short) 3);
				cell[0].setCellValue("Email");
				cellStyleHead.setFont(timesBoldFont);
				cellStyleHead.setAlignment(HSSFCellStyle.ALIGN_LEFT);
				cell[0].setCellStyle(cs1);
				worksheet.addMergedRegion(CellRangeAddress.valueOf("D2:D3"));
				
				
				cell[0] = row1.createCell((short) 4);
				cell[0].setCellValue("DepartmentId");
				cellStyleHead.setFont(timesBoldFont);
				cellStyleHead.setAlignment(HSSFCellStyle.ALIGN_LEFT);
				cell[0].setCellStyle(cs1);
				worksheet.addMergedRegion(CellRangeAddress.valueOf("E2:E3"));

				cell[0] = row1.createCell((short) 5);
				cell[0].setCellValue("PracticeId");
				cellStyleHead.setFont(timesBoldFont);
				cellStyleHead.setAlignment(HSSFCellStyle.ALIGN_LEFT);
				cell[0].setCellStyle(cs1);
				worksheet.addMergedRegion(CellRangeAddress.valueOf("F2:F3"));

				cell[0] = row1.createCell((short) 6);
				cell[0].setCellValue("SubPractice");
				cellStyleHead.setFont(timesBoldFont);
				cellStyleHead.setAlignment(HSSFCellStyle.ALIGN_LEFT);
				cell[0].setCellStyle(cs1);
				worksheet.addMergedRegion(CellRangeAddress.valueOf("G2:G3"));

				cell[0] = row1.createCell((short) 7);
				cell[0].setCellValue("ReportsTo");
				cellStyleHead.setFont(timesBoldFont);
				cellStyleHead.setAlignment(HSSFCellStyle.ALIGN_LEFT);
				cell[0].setCellStyle(cs1);
				worksheet.addMergedRegion(CellRangeAddress.valueOf("H2:H3"));

				cell[0] = row1.createCell((short) 8);
				cell[0].setCellValue("HrContact");
				cellStyleHead.setFont(timesBoldFont);
				cellStyleHead.setAlignment(HSSFCellStyle.ALIGN_LEFT);
				cell[0].setCellStyle(cs1);
				worksheet.addMergedRegion(CellRangeAddress.valueOf("I2:I3"));

				String[] alpha = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R",
						"S", "T", "U", "V", "W", "X", "Y", "Z" };
				/*
				 * for(int i=6;i<=startandenddate.size();i++){
				 * 
				 * CreationHelper createHelper = workbook.getCreationHelper();
				 * cellStyle1.setDataFormat(
				 * createHelper.createDataFormat().getFormat("m/d/yy")); cell[0] =
				 * row1.createCell((short) i); String startandEndDates=(String)
				 * startandenddate.get(""+i); String
				 * startandEndDatesArray[]=startandEndDates.split(",");
				 * 
				 * cell[0].setCellValue((startandEndDatesArray[0]+"_"+
				 * startandEndDatesArray[1])); cell[0].setCellStyle(cellStyle1);
				 * cellStyleHead.setFont(timesBoldFont);
				 * cellStyleHead.setAlignment(HSSFCellStyle.ALIGN_LEFT); //
				 * cell[0].setCellStyle(cs1);
				 * worksheet.addMergedRegion(CellRangeAddress.valueOf("'"+alpha[i+1]
				 * +"'2:'"+alpha[i]+"'3")); }
				 */

				count = 1;
				while (true) {
					// System.out.println("map size"+startandenddate.size());
					cell[0] = row1.createCell((short) count + 8);
					// System.out.println((String)
					// startandenddate.get(""+(count-1)));
					String startandEndDates = (String) startandenddate.get("" + (count - 1));
					String startandEndDatesArray[] = startandEndDates.split(",");

					String sd = DateUtility.getInstance().convertToviewFormatNew(startandEndDatesArray[0]);
					String ed = DateUtility.getInstance().convertToviewFormatNew(startandEndDatesArray[1]);

					cell[0].setCellValue((sd + "-" + ed));
					cell[0].setCellStyle(cellStyle1);
					cellStyleHead.setFont(timesBoldFont);
					cellStyleHead.setAlignment(HSSFCellStyle.ALIGN_LEFT);
					// cell[0].setCellStyle(cs1);

					worksheet.addMergedRegion(
							CellRangeAddress.valueOf("" + alpha[count + 8] + "2:" + alpha[count + 8] + "3"));
					if (count == startandenddate.size()) {
						// System.out.println("count"+count);
						break;
					}
					count++;

				}

				int rowCount = 3;
				// System.out.println("rowCount"+rowCount);
				connection = ConnectionProvider.getInstance().getConnection();

				String query = null;

				if ("-1".equals(practice) || "All".equals(practice) || "--Please Select--".equalsIgnoreCase(practice)) {
					practice = "%";
				}
				if ("-1".equals(opsConatctId)) {
					opsConatctId = "%";
				}

				if ("-1".equals(reportsTo)) {
					reportsTo = "%";
				}

				/*if ("-1".equals(deptId)) {
					deptId = "GDC";
				}
	*/
				query = "SELECT t1.Id AS EmpId,t1.EmpNo,CONCAT(t1.fname,'',t1.mname,'',t1.lname) AS EmpName,t1.Email1 AS Email,t1.DepartmentId,t1.PracticeId,t1.SubPractice,t1.ReportsTo, CONCAT(t2.fname,'',t2.mname,'',t2.lname) AS HrName FROM tblEmployee t1 LEFT OUTER JOIN tblEmployee t2 ON t1.OpsContactId = t2.Id WHERE t1.OpsContactId LIKE '"
						+ opsConatctId + "' AND t1.DepartmentId = '" + deptId + "' AND t1.ReportsTo LIKE '" + reportsTo
						+ "' AND t1.PracticeId LIKE '" + practice + "' AND t1.CurStatus='Active'";

				// System.out.println("query="+query);
				int j = 1;
				preStmt = connection.prepareStatement(query);
				resultSet = preStmt.executeQuery();
				Map mainMap = new HashMap();
				int mapCount = 0;
				Map subMap = null;

				Map employeeMap = new HashMap();
				List employeesList = new ArrayList();
				while (resultSet.next()) {
					// System.out.println("in while");
					subMap = new HashMap();
					employeeMap = new HashMap();
					int EmpId = resultSet.getInt("EmpId");
					String EmpNo = resultSet.getString("EmpNo");
					
					String EmpName = resultSet.getString("EmpName");
					String Email = resultSet.getString("Email");
					String DepartmentId = resultSet.getString("DepartmentId");
					String PracticeId = resultSet.getString("PracticeId");
					String SubPractice = resultSet.getString("SubPractice");
					String ReportsTo = resultSet.getString("ReportsTo");
					String HrName = resultSet.getString("HrName");

					employeeMap.put("EmpId", EmpId);
					employeeMap.put("EmpNo", EmpNo);
					employeeMap.put("EmpName", EmpName);
					employeeMap.put("Email", Email);
					employeeMap.put("DepartmentId", DepartmentId);
					employeeMap.put("PracticeId", PracticeId);
					employeeMap.put("SubPractice", SubPractice);
					employeeMap.put("ReportsTo", ReportsTo);
					employeeMap.put("HrName", HrName);
					employeeMap.put("HrName", HrName);
					employeesList.add(employeeMap);
				}
				resultSet.close();
				preStmt.close();

				String timeSheetQuery = "SELECT Description FROM tblTimeSheets LEFT JOIN tblLKTimeSheetStatusType ON (tblTimeSheets.TimeSheetStatusTypeId = tblLKTimeSheetStatusType.Id) WHERE EmpId=? AND DATE(DateStart) =DATE(?)";
				preStmt = connection.prepareStatement(timeSheetQuery);

				for (int l = 0; l < employeesList.size(); l++) {

					Map tempEmpMap = (Map) employeesList.get(l);
					int EmpId = (Integer) tempEmpMap.get("EmpId");
					String EmpNo =(String) tempEmpMap.get("EmpNo");
					String EmpName = (String) tempEmpMap.get("EmpName");
					String Email = (String) tempEmpMap.get("Email");
					String DepartmentId = (String) tempEmpMap.get("DepartmentId");
					String PracticeId = (String) tempEmpMap.get("PracticeId");
					String SubPractice = (String) tempEmpMap.get("SubPractice");
					String ReportsTo = (String) tempEmpMap.get("ReportsTo");
					String HrName = (String) tempEmpMap.get("HrName");

					// subMap.put(""+mapCount, status);
					row1 = worksheet.createRow((short) rowCount++);
					for (int c = 0; c < (9 + startandenddate.size()); c++) {
						cell[c] = row1.createCell((short) c);
					}
					/*
					 * count=1; while(true) { cell[count+5] =
					 * row1.createCell((short) count+5);
					 * if(count==startandenddate.size()) { break; } count++;
					 * 
					 * }
					 */
					int indexVal = 0;
					cell[indexVal++].setCellValue((int) (rowCount - 3));
					cell[indexVal++].setCellValue((String) EmpNo);
					cell[indexVal++].setCellValue((String) EmpName);
					cell[indexVal++].setCellValue((String) Email);
					cell[indexVal++].setCellValue((String) DepartmentId);
					cell[indexVal++].setCellValue((String) PracticeId);
					cell[indexVal++].setCellValue((String) SubPractice);

					cell[indexVal++].setCellValue((String) ReportsTo);

					cell[indexVal++].setCellValue((String) HrName);
					for (int i = 0; i < startandenddate.size(); i++) {
						String dates = (String) startandenddate.get("" + i);
						String datesArray[] = dates.split(",");
						// System.out.println("datesArray[0]"+datesArray[0]);

						// String
						// status=dataSourceDataProvider.getTimeSheetStatus(datesArray[0],Id);

						String status = "";
						// System.out.println("status"+status);
						//System.out.println(Id+"--"+datesArray[0]);
						preStmt.setInt(1, EmpId);
						preStmt.setString(2, datesArray[0]);
						resultSet = preStmt.executeQuery();
						if (resultSet.next()) {
							if (resultSet.getString("Description") != null) {
								status = resultSet.getString("Description");
							}
						} else {
							status = "Not Entered";
						}

						cell[indexVal++].setCellValue(status);

						// subMap.put(""+i, status);
					}

					/*
					 * if (rowCount % 2 == 0) { cell[h].setCellStyle(cs3); } else {
					 * cell[h].setCellStyle(cs4); }
					 */

					for (int h = 0; h < (9 + startandenddate.size()); h++) {
						if (rowCount % 2 == 0) {
							cell[h].setCellStyle(cs3);
						} else {
							cell[h].setCellStyle(cs4);
						}
					}

				}

				/*
				 * row1 = worksheet.createRow((short) rowCount++); for (int y = 0; y
				 * < 32; y++) { cell[y] = row1.createCell((short) y); }
				 * 
				 * for (int f = 0; f < 32; f++) { cell[f].setCellStyle(cs1);
				 * cs.setWrapText(true); }
				 */

				worksheet.autoSizeColumn((short) 0);
				worksheet.autoSizeColumn((short) 1);
				worksheet.autoSizeColumn((short) 2);

				// worksheet.autoSizeColumn((short) 5);
				/*
				 * worksheet.setColumnWidth(0, 15 * 256);
				 * worksheet.setColumnWidth(1, 45 * 256);
				 * worksheet.setColumnWidth(2, 20 * 256);
				 * worksheet.setColumnWidth(3, 25 * 256);
				 * worksheet.setColumnWidth(4, 25 * 256);
				 * worksheet.setColumnWidth(5, 25 * 256); for(int
				 * i=6;i<=startandenddate.size();i++) {
				 * System.out.println("in for"+i); worksheet.setColumnWidth(i, 40 *
				 * 256);
				 * 
				 * }
				 */ workbook.write(fileOut);
				fileOut.flush();
				fileOut.close();
				// }
			} catch (FileNotFoundException fne) {
				//fne.printStackTrace();
				System.err.println(fne.getMessage());
			} catch (IOException ioe) {
				//ioe.printStackTrace();
				System.err.println(ioe.getMessage());
			} catch (Exception ex) {
				//ex.printStackTrace();
				System.err.println(ex.getMessage());
			} finally {

				try {

					if (callableStatement != null) {
						callableStatement.close();
						callableStatement = null;
					}
					if (connection != null) {
						connection.close();
						connection = null;
					}
				} catch (Exception se) {
					
					System.err.println(se.getMessage());
					//se.printStackTrace();
				}
			}

			return filePath;
		}

	 
}
