/*******************************************************************************
 *
 * Project : Mirage V2
 *
 * Package :
 *
 * Date    :   April 1, 2008, 4:39 PM
 *
 * Author  :  Hari Krishna Kondala <hkondala@miraclesoft.com>
 *
 * File    : EmpImageAction .java
 *
 * Copyright 2008 Miracle Software Systems, Inc. All rights reserved.
 * MIRACLE SOFTWARE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * *****************************************************************************
 */

package com.mss.mirage.employee.profile;


import com.itextpdf.text.pdf.codec.Base64;
import com.mss.mirage.util.ApplicationConstants;
import com.mss.mirage.util.AuthorizationManager;
import com.mss.mirage.util.LdapServiceProvider;
import com.mss.mirage.util.Properties;
import com.mss.mirage.util.RestRepository;
import com.mss.mirage.util.ServiceLocator;
import com.opensymphony.xwork2.ActionSupport;

import sun.misc.BASE64Decoder;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.sql.*;
import java.util.Hashtable;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;
import org.json.JSONObject;
import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.directory.Attribute;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.ModificationItem;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;

/**
 *
 * @author miracle
 */
public class EmpProfileAction extends ActionSupport implements ServletRequestAware,ServletResponseAware{
    
    private HttpServletRequest httpServletRequest;
    private HttpServletResponse httpServletResponse;
    private EmpProfileVTO currentEmployee;
    private File imagePath = null;
    private String skillSet = null;
    private String resultType = null;
    private int userRoleId;
    private int userId;
    private String resultMessage="";
    
    /** The firstName is used for storing firstName of employee. */
    //private String firstName;
    
    /** The lastName is used for storing lastName of employee. */
    //private String lastName;
    
    /** The middleName is used for storing middleName of employee. */
    //private String middleName;
    
    /** The aliasName is used for storing aliasName of employee. */
    private String aliasName;
       
     /** The gender is used for storing aliasName of employee. */
    private String gender;
    
    /** The maritalStatus is used for storing maritalStatus of employee. */
    private String maritalStatus;
    
    /** The country is used for storing maritalStatus of employee. */
    private String country;
           
    /** The birthDate is used for storing dateOfBirth of employee. */
    private Date birthDate;
    
    /** The offBirthDate is used for storing official date of birth of employee. */
    private Date offBirthDate;
        
    /** The anniversaryDate is used for storing anniversaryDate of employee.*/
    private Date anniversaryDate;
    
    /** The workPhoneNo is used for storing wormPhoneNo of employee. */
    private String workPhoneNo;
    
    /** The altPhoneNo is used for storing alternatePhoneNo of employee. */
    private String altPhoneNo;
    
    /** The homePhoneNo is used for storing homePhoneNo of employee. */
    private String homePhoneNo;
    
    /** The cellPhoneNo is used for storing cellPhoneNo of employee. */
    private String cellPhoneNo;   
    
    /** The hotelPhoneNo is used for storing hotelPhoneNo of employee. */
    private String hotelPhoneNo;
    
    /** The personalEmail is used for storing personal Email Of employee. */
    private String personalEmail;
    
    /** The indiaPhoneNo is used for storing indiaPhoneNo of employee. */
    private String indiaPhoneNo;
    
    /** The otherEmail is used for storing other Email of employee. */
    private String otherEmail;
    
    /** The faxNo is used for storing Fax of employee. */
    private String faxNo;
    
    private String empId; 
    
      //New Fileds For Confedential Info Date : 08/19/2014
     private String bankName;
private String accNum;
private String nameAsPerAcc;
private String ifscCode;
private String phyChallenged;
private String phyCategory;
private String aadharNum;
private String aadharName;
private String nameAsPerPan;
private String empno;
private String ssn;

private String nsrno;
private String itgBatch;
//new

private Date passportExp;
private String passportNo;
private String uanNo;
private String pfno;
private String reportsTo;
private String operationContact;
private String aboutMe;
private String loginId;
    public String getLoginId() {
	return loginId;
}

public void setLoginId(String loginId) {
	this.loginId = loginId;
}

	/**
     * Creates a new instance of EmpProfileAction
     */
    public EmpProfileAction() {
    }
    

    public String execute() throws FileNotFoundException,ServletException{
      	 String ImageDetails = "";
      	 int update = 0;
           try {
          	 JSONObject jb = new JSONObject();
          	 String loginId = httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID).toString();  
               
          	if (getImagePath() != null && !"".equals(getImagePath())) {
      			jb.put("ImageName", getImagePath().getName());
      			jb.put("LoginId", loginId);
      			

      			 String encodedBase64 = null;
      		        try {
      		            FileInputStream fileInputStreamReader = new FileInputStream(getImagePath());
      		          	
      		            byte[] bytes = new byte[(int)(getImagePath()).length()];
      		   //         System.out.println("fileInputStreamReader.."+fileInputStreamReader+"bytes.."+bytes);
      					
      		            fileInputStreamReader.read(bytes);
      		            encodedBase64 = new String(Base64.encodeBytes(bytes));
      		        } catch (FileNotFoundException e) {
      		            e.printStackTrace();
      		        } catch (IOException e) {
      		            e.printStackTrace();
      		        }
      		        encodedBase64="data:application/pdf;base64,"+encodedBase64;
      			jb.put("Base64String",encodedBase64);
      		//	System.out.println("filename.."+getImagePath().getName()+"encodedBase64..."+encodedBase64);
      			
      			}
          	
          	  String serviceUrl = RestRepository.getInstance().getSrviceUrl("employeeProfilePicUpdate");
                URL url = new URL(serviceUrl);
                URLConnection connection = url.openConnection();
                connection.setDoOutput(true);
                connection.setRequestProperty("Content-Type", "application/json");
                connection.setConnectTimeout(360 * 5000);
                connection.setReadTimeout(360 * 5000);
                OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
                out.write(jb.toString());
                out.close();

                BufferedReader in = new BufferedReader(new InputStreamReader(
                        connection.getInputStream()));
                String s = null;
                String data = "";
                while ((s = in.readLine()) != null) {

                    data = data + s;
                }

                JSONObject jObject = new JSONObject(data);

                ImageDetails = jObject.getString("ImageDetails");
  //System.out.println("ImageDetails...."+ImageDetails);


                in.close();
                update=1;
                getSkills();
                if(update==1){
                	
                	JSONObject mainJson = new JSONObject();
                	mainJson.put("loginId", loginId);
                	mainJson.put("ImageDetails", ImageDetails);
                	
                	LdapServiceProvider.empProfilePicUpdate(jb);

                    resultMessage = "Employee Image Updated Successfully!";
                }else{
                    resultMessage = "Please Try again!";
                }
                httpServletRequest.setAttribute(ApplicationConstants.RESULT_MSG,resultMessage);
              resultType = SUCCESS;
              
          } catch (Exception ex) {
              httpServletRequest.getSession(false).setAttribute("errorMessage",ex);
              resultType=ERROR;
          }
          return resultType;
      }
  	
//    public static JSONObject empProfilePicUpdate(JSONObject jObject) {
//
//		String base = "ou=employee,dc=miraclesoft,dc=com";
//		
//		JSONObject mainJson = new JSONObject();
//		
//		Hashtable srchEnv = new Hashtable(11);
//		srchEnv.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
//		srchEnv.put(Context.PROVIDER_URL, "ldap://139.59.27.199:389"); // ldap://localhost:389
//		srchEnv.put(Context.SECURITY_AUTHENTICATION, "simple");
//		srchEnv.put(Context.SECURITY_PRINCIPAL, "cn=admin,dc=miraclesoft,dc=com");
//		srchEnv.put(Context.SECURITY_CREDENTIALS, "6WFH5J?GT4nusw^%");
//
//		try {
//			DirContext srchContext = new InitialDirContext(srchEnv);
//			//System.out.println("Boolean value-->" + jObject);
//			boolean  resflag= empProfilePictureUpdate(srchContext, base, jObject);
//			//System.out.println("Boolean value-->" + resflag);
//			if(resflag){
//				mainJson.put("isAdded","Yes");
//				mainJson.put("result","updated Successfully");
//			}else{
//				mainJson.put("isAdded","No");
//				mainJson.put("result","Please try again");
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//
//		}
//
//		return mainJson;
//
//	}
//
//
//public static boolean empProfilePictureUpdate(DirContext dirContext, String base, JSONObject jObject) {
//	boolean flag = false;
//
//	ModificationItem[] modItemsOne=new ModificationItem[1];
//	
//
//	try {
//		
//		SearchControls srchControls = new SearchControls();
//		
//		srchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);
//		
//		String searchFilter = "(&(objectClass=*)(|(cn=" + jObject.getString("loginId") + ")))";
//	
//		NamingEnumeration srchResponse = dirContext.search(base, searchFilter, srchControls);
//         //  System.out.println(srchResponse);
//		while (srchResponse.hasMore()) {
//			SearchResult result = (SearchResult) srchResponse.next();
//			String distinguishedName = result.getNameInNamespace();
//		
//		
//		File file=null;
//			 file = new File(jObject.getString("ImageDetails"));
////			 if(!file.exists())
////				 file = new File("/opt/lampp/htdocs/msws/images/employee-profile-pics/noImage.jpg");
//			FileInputStream fis = null;
//			 byte[] binaryData =new byte[(int)file.length()];
//			
//			try{
//			fis = new FileInputStream(file);
//			
//			    fis.read(binaryData);
//			}catch (Exception ex){
//				ex.printStackTrace();
//			}
//							 
//
//			
//			
//		
//		
//	//	System.out.println("login--cn value-->" +jObject.getString("loginId"));
//		
//		modItemsOne[0] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE,
//				
//				new BasicAttribute("jpegPhoto",binaryData));
//	    
//		 String entryDN = distinguishedName;
//
//	
//     	 // System.out.println("entryDN :" + entryDN);
//
//		dirContext.modifyAttributes(distinguishedName, modItemsOne);
//		flag = true;}
//	} catch (Exception e) {
//	e.printStackTrace();
//	return flag;
//	}
//	return flag;
//}
//


    
    
    
    public String getSkills(){
        resultType = LOGIN;
        
        if(httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
            userId = Integer.parseInt(httpServletRequest.getSession().getAttribute(ApplicationConstants.SESSION_EMP_ID).toString());
            userRoleId = Integer.parseInt(httpServletRequest.getSession().getAttribute(ApplicationConstants.SESSION_ROLE_ID).toString());
            resultType = "accessFailed";
            if(AuthorizationManager.getInstance().isAuthorizedUser("GET_SKILLSET",userRoleId)) {
                try{
                    setCurrentEmployee(ServiceLocator.getEmpProfileService().getSkills(userId));
                    resultType = SUCCESS;
                } catch(Exception ex) {
                    httpServletRequest.getSession(false).setAttribute("errorMessage",ex.toString());
                    resultType =  ERROR;
                }
            }
        }
        return resultType;
    }
    
    public String doEdit() {
        resultType = LOGIN;
        int updatedRows;
        if(httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
            userId = Integer.parseInt(httpServletRequest.getSession().getAttribute(ApplicationConstants.SESSION_EMP_ID).toString());
            userRoleId = Integer.parseInt(httpServletRequest.getSession().getAttribute(ApplicationConstants.SESSION_ROLE_ID).toString());
            resultType = "accessFailed";
            if(AuthorizationManager.getInstance().isAuthorizedUser("EDIT_SKILLSET",userRoleId)) {
                try{
                    updatedRows = ServiceLocator.getEmpProfileService().updateSkills(this,userId);
                    if(updatedRows==1){
                        resultMessage = "Employee SkillSet Updated Successfully!";
                    }else{
                        resultMessage = "Please Try again!";
                    }
                    httpServletRequest.setAttribute(ApplicationConstants.RESULT_MSG,resultMessage);
                    getSkills();
                    resultType = SUCCESS;
                } catch(Exception ex) {
                    httpServletRequest.getSession(false).setAttribute("errorMessage",ex.toString());
                    resultType =  ERROR;
                }
            }
        }
        return resultType;
    }
    
    public void renderImage() {
    	System.out.println("renderImage...");
    	 byte[] image = null;
       OutputStream outputStream = null;
       InputStream inputStream = null;
         File imageFile = null;
        try {
             

           String loginId = httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID).toString();  
			JSONObject jb = new JSONObject();
			jb.put("LoginId", loginId);
           String serviceUrl = RestRepository.getInstance().getSrviceUrl("employeeRenderImage");
           URL url = new URL(serviceUrl);
           URLConnection connection = url.openConnection();
           connection.setDoOutput(true);
           connection.setRequestProperty("Content-Type", "application/json");
           connection.setConnectTimeout(360 * 5000);
           connection.setReadTimeout(360 * 5000);
           OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
           out.write(jb.toString());
           out.close();

           BufferedReader in = new BufferedReader(new InputStreamReader(
                   connection.getInputStream()));
           String s = null;
           String data = "";
           while ((s = in.readLine()) != null) {

               data = data + s;
           }
           
           JSONObject jObject = new JSONObject(data);

         String  Base64String = jObject.getString("Base64String");
            if ("NotExists".equals(Base64String)) {
            //	System.out.println("NotExists...");

                       imageFile = new File(Properties.getProperty("No.Image"));
                       InputStream is = new FileInputStream(imageFile);

                       long length = imageFile.length();

                       image = new byte[(int)length];

                       int offset = 0;
                       int numRead = 0;
                       while (offset < image.length && (numRead=is.read(image, offset, image.length-offset)) >= 0) {
                         offset += numRead;
                       }

                       if (offset < image.length) {
                         throw new IOException("Could not completely read file ");
                       }
        httpServletResponse.getOutputStream().write(image);

                       is.close();
              
            
            }else{
            //	System.out.println("exists...");

            String[] parts = Base64String.split(",");
     		String imageString = parts[1];
     		String[] imageType = parts[0].split(";");
     		String[] imageType1 = imageType[0].split("/");
     		
     		byte[] imageByte=Base64String.getBytes();

     		BASE64Decoder decoder = new BASE64Decoder();
     		imageByte = decoder.decodeBuffer(imageString);
            inputStream  = new ByteArrayInputStream(imageByte);
     		
            outputStream = httpServletResponse.getOutputStream();
            int noOfBytesRead = 0;
           
            while (true) {
            	image = new byte[1024];
                noOfBytesRead = inputStream.read(image);
                if (noOfBytesRead == -1) {
                    break;
                }
                outputStream.write(image, 0, noOfBytesRead);
            } 
            }
                  httpServletResponse.getOutputStream().close();
        //    }

        }catch (Exception ex) {
        	ex.printStackTrace();
            httpServletRequest.getSession(false).setAttribute("errorMessage",ex);
            resultType=ERROR;
        }
      //  return SUCCESS;
    }
	

    public String updateProfile() {
       
        resultType = LOGIN;
        int updatedRows;
        if(httpServletRequest.getSession(false).getAttribute(ApplicationConstants.SESSION_USER_ID) != null) {
            userId = Integer.parseInt(httpServletRequest.getSession().getAttribute(ApplicationConstants.SESSION_EMP_ID).toString());
            userRoleId = Integer.parseInt(httpServletRequest.getSession().getAttribute(ApplicationConstants.SESSION_ROLE_ID).toString());
            resultType = "accessFailed";
            if(AuthorizationManager.getInstance().isAuthorizedUser("EDIT_PROFILE",userRoleId)) {
                try {
                    updatedRows = ServiceLocator.getEmpProfileService().updateEmpProfile(this,userId);
                    if(updatedRows==1){
                        resultMessage = "Employee Profile Updated Successfully!";
                    }else{
                        resultMessage = "Please Try again!";
                    }
                    httpServletRequest.setAttribute(ApplicationConstants.RESULT_MSG,resultMessage);
                    getSkills();
                    resultType = SUCCESS;
                } catch(Exception ex) {
                    httpServletRequest.getSession(false).setAttribute("errorMessage",ex.toString());
                    resultType =  ERROR;
                }
            }
        }
        return resultType;
    }
    
    public void getEmployeeRenderImage() {
    	//System.out.println("renderImage...");
    	 byte[] image = null;
       OutputStream outputStream = null;
       InputStream inputStream = null;
         File imageFile = null;
        try {
             

           String loginId = getLoginId();  
			JSONObject jb = new JSONObject();
			jb.put("LoginId", loginId);
           String serviceUrl = RestRepository.getInstance().getSrviceUrl("employeeRenderImage");
           URL url = new URL(serviceUrl);
           URLConnection connection = url.openConnection();
           connection.setDoOutput(true);
           connection.setRequestProperty("Content-Type", "application/json");
           connection.setConnectTimeout(360 * 5000);
           connection.setReadTimeout(360 * 5000);
           OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
           out.write(jb.toString());
           out.close();

           BufferedReader in = new BufferedReader(new InputStreamReader(
                   connection.getInputStream()));
           String s = null;
           String data = "";
           while ((s = in.readLine()) != null) {

               data = data + s;
           }
           
           JSONObject jObject = new JSONObject(data);

         String  Base64String = jObject.getString("Base64String");
            if ("NotExists".equals(Base64String)) {
            	//System.out.println("NotExists...");

                       imageFile = new File(Properties.getProperty("No.Image"));
                       InputStream is = new FileInputStream(imageFile);

                       long length = imageFile.length();

                       image = new byte[(int)length];

                       int offset = 0;
                       int numRead = 0;
                       while (offset < image.length && (numRead=is.read(image, offset, image.length-offset)) >= 0) {
                         offset += numRead;
                       }

                       if (offset < image.length) {
                         throw new IOException("Could not completely read file ");
                       }
        httpServletResponse.getOutputStream().write(image);

                       is.close();
              
            
            }else{
            	//System.out.println("exists...");

            String[] parts = Base64String.split(",");
     		String imageString = parts[1];
     		String[] imageType = parts[0].split(";");
     		String[] imageType1 = imageType[0].split("/");
     		
     		byte[] imageByte=Base64String.getBytes();

     		BASE64Decoder decoder = new BASE64Decoder();
     		imageByte = decoder.decodeBuffer(imageString);
            inputStream  = new ByteArrayInputStream(imageByte);
     		
            outputStream = httpServletResponse.getOutputStream();
            int noOfBytesRead = 0;
           
            while (true) {
            	image = new byte[1024];
                noOfBytesRead = inputStream.read(image);
                if (noOfBytesRead == -1) {
                    break;
                }
                outputStream.write(image, 0, noOfBytesRead);
            } 
            }
                  httpServletResponse.getOutputStream().close();
        //    }

        }catch (Exception ex) {
        	ex.printStackTrace();
            httpServletRequest.getSession(false).setAttribute("errorMessage",ex);
            resultType=ERROR;
        }
      //  return SUCCESS;
    }
    
    
    
    
    
    
    
    
    public File getImagePath() {
        return imagePath;
    }
    
    public void setImagePath(File imagePath) {
        this.imagePath = imagePath;
    }
    
    
    public String getSkillSet() {
        return skillSet;
    }
    
    public void setSkillSet(String skillSet) {
        this.skillSet = skillSet;
    }
    
    public EmpProfileVTO getCurrentEmployee() {
        return currentEmployee;
    }
    
    public void setCurrentEmployee(EmpProfileVTO currentEmployee) {
        this.currentEmployee = currentEmployee;
    }
    
    
    public void setServletRequest(HttpServletRequest httpServletRequest) {
        this.httpServletRequest = httpServletRequest;
    }

    /*public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }*/

    public String getAliasName() {
        return aliasName;
    }

    public void setAliasName(String aliasName) {
        this.aliasName = aliasName;
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public Date getOffBirthDate() {
        return offBirthDate;
    }

    public void setOffBirthDate(Date offBirthDate) {
        this.offBirthDate = offBirthDate;
    }

    public Date getAnniversaryDate() {
        return anniversaryDate;
    }

    public void setAnniversaryDate(Date anniversaryDate) {
        this.anniversaryDate = anniversaryDate;
    }

    public String getWorkPhoneNo() {
        return workPhoneNo;
    }

    public void setWorkPhoneNo(String workPhoneNo) {
        this.workPhoneNo = workPhoneNo;
    }

    public String getAltPhoneNo() {
        return altPhoneNo;
    }

    public void setAltPhoneNo(String altPhoneNo) {
        this.altPhoneNo = altPhoneNo;
    }

    public String getHomePhoneNo() {
        return homePhoneNo;
    }

    public void setHomePhoneNo(String homePhoneNo) {
        this.homePhoneNo = homePhoneNo;
    }

    public String getCellPhoneNo() {
        return cellPhoneNo;
    }

    public void setCellPhoneNo(String cellPhoneNo) {
        this.cellPhoneNo = cellPhoneNo;
    }

    public String getHotelPhoneNo() {
        return hotelPhoneNo;
    }

    public void setHotelPhoneNo(String hotelPhoneNo) {
        this.hotelPhoneNo = hotelPhoneNo;
    }

    public String getPersonalEmail() {
        return personalEmail;
    }

    public void setPersonalEmail(String personalEmail) {
        this.personalEmail = personalEmail;
    }

    public String getIndiaPhoneNo() {
        return indiaPhoneNo;
    }

    public void setIndiaPhoneNo(String indiaPhoneNo) {
        this.indiaPhoneNo = indiaPhoneNo;
    }

    public String getOtherEmail() {
        return otherEmail;
    }

    public void setOtherEmail(String otherEmail) {
        this.otherEmail = otherEmail;
    }

    public String getFaxNo() {
        return faxNo;
    }

    public void setFaxNo(String faxNo) {
        this.faxNo = faxNo;
    }

    public void setServletResponse(HttpServletResponse httpServletResponse) {
        this.httpServletResponse = httpServletResponse;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getEmpId() {
        return empId;
    }

    public void setEmpId(String empId) {
        this.empId = empId;
    }

    /**
     * @return the bankName
     */
    public String getBankName() {
        return bankName;
    }

    /**
     * @param bankName the bankName to set
     */
    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    /**
     * @return the accNum
     */
    public String getAccNum() {
        return accNum;
    }

    /**
     * @param accNum the accNum to set
     */
    public void setAccNum(String accNum) {
        this.accNum = accNum;
    }

    /**
     * @return the nameAsPerAcc
     */
    public String getNameAsPerAcc() {
        return nameAsPerAcc;
    }

    /**
     * @param nameAsPerAcc the nameAsPerAcc to set
     */
    public void setNameAsPerAcc(String nameAsPerAcc) {
        this.nameAsPerAcc = nameAsPerAcc;
    }

    /**
     * @return the ifscCode
     */
    public String getIfscCode() {
        return ifscCode;
    }

    /**
     * @param ifscCode the ifscCode to set
     */
    public void setIfscCode(String ifscCode) {
        this.ifscCode = ifscCode;
    }

    /**
     * @return the phyChallenged
     */
    public String getPhyChallenged() {
        return phyChallenged;
    }

    /**
     * @param phyChallenged the phyChallenged to set
     */
    public void setPhyChallenged(String phyChallenged) {
        this.phyChallenged = phyChallenged;
    }

    /**
     * @return the phyCategory
     */
    public String getPhyCategory() {
        return phyCategory;
    }

    /**
     * @param phyCategory the phyCategory to set
     */
    public void setPhyCategory(String phyCategory) {
        this.phyCategory = phyCategory;
    }

    /**
     * @return the aadharNum
     */
    public String getAadharNum() {
        return aadharNum;
    }

    /**
     * @param aadharNum the aadharNum to set
     */
    public void setAadharNum(String aadharNum) {
        this.aadharNum = aadharNum;
    }

    /**
     * @return the aadharName
     */
    public String getAadharName() {
        return aadharName;
    }

    /**
     * @param aadharName the aadharName to set
     */
    public void setAadharName(String aadharName) {
        this.aadharName = aadharName;
    }

    /**
     * @return the nameAsPerPan
     */
    public String getNameAsPerPan() {
        return nameAsPerPan;
    }

    /**
     * @param nameAsPerPan the nameAsPerPan to set
     */
    public void setNameAsPerPan(String nameAsPerPan) {
        this.nameAsPerPan = nameAsPerPan;
    }

    /**
     * @return the empno
     */
    public String getEmpno() {
        return empno;
    }

    /**
     * @param empno the empno to set
     */
    public void setEmpno(String empno) {
        this.empno = empno;
    }

    /**
     * @return the ssn
     */
    public String getSsn() {
        return ssn;
    }

    /**
     * @param ssn the ssn to set
     */
    public void setSsn(String ssn) {
        this.ssn = ssn;
    }

    /**
     * @return the nsrno
     */
    public String getNsrno() {
        return nsrno;
    }

    /**
     * @param nsrno the nsrno to set
     */
    public void setNsrno(String nsrno) {
        this.nsrno = nsrno;
    }

    /**
     * @return the itgBatch
     */
    public String getItgBatch() {
        return itgBatch;
    }

    /**
     * @param itgBatch the itgBatch to set
     */
    public void setItgBatch(String itgBatch) {
        this.itgBatch = itgBatch;
    }

    public Date getPassportExp() {
        return passportExp;
    }

    public void setPassportExp(Date passportExp) {
        this.passportExp = passportExp;
    }

    public String getPassportNo() {
        return passportNo;
    }

    public void setPassportNo(String passportNo) {
        this.passportNo = passportNo;
    }

    /**
     * @return the uanNo
     */
    public String getUanNo() {
        return uanNo;
    }

    /**
     * @param uanNo the uanNo to set
     */
    public void setUanNo(String uanNo) {
        this.uanNo = uanNo;
    }

    /**
     * @return the pfno
     */
    public String getPfno() {
        return pfno;
    }

    /**
     * @param pfno the pfno to set
     */
    public void setPfno(String pfno) {
        this.pfno = pfno;
    }

    /**
     * @return the reportsTo
     */
    public String getReportsTo() {
        return reportsTo;
    }

    /**
     * @param reportsTo the reportsTo to set
     */
    public void setReportsTo(String reportsTo) {
        this.reportsTo = reportsTo;
    }

    /**
     * @return the operationContact
     */
    public String getOperationContact() {
        return operationContact;
    }

    /**
     * @param operationContact the operationContact to set
     */
    public void setOperationContact(String operationContact) {
        this.operationContact = operationContact;
    }

    /**
     * @return the aboutMe
     */
    public String getAboutMe() {
        return aboutMe;
    }

    /**
     * @param aboutMe the aboutMe to set
     */
    public void setAboutMe(String aboutMe) {
        this.aboutMe = aboutMe;
    }
    
    
}
