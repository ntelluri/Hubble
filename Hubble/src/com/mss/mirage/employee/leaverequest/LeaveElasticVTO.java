package com.mss.mirage.employee.leaverequest;

/*
import io.searchbox.annotations.JestId;
import net.sf.json.JSONObject;*/

public class LeaveElasticVTO {


    private int empId;
    private String startDate;
    private String endDate;
    
   /* private String orgId;
    private String departmentId;
    
    private String reason;
    private String status;
    private String leaveType;
    private String reportsTo;
    private String appliedDate;
    private String modifiedDate;
    private String lFlag; */
   
   

  
   // @JestId
    private int id;

   
/*
	public static LeaveElasticVTO fromJSON(JSONObject json) {
        return new LeaveElasticVTO(
        		json.getInt("id"),
                json.getInt("empId"),
                json.getString("startDate"),
                json.getString("endDate")
                
               /* json.getString("orgId"),
                json.getString("departmentId"),
               
                json.getString("reason"),
                json.getString("status"),
                json.getString("leaveType"),
                json.getString("reportsTo"),
                json.getString("appliedDate"),
                json.getString("modifiedDate"),
                json.getString("lFlag") */
               
 /*       );
    }
*/


	/* public LeaveElasticVTO(int id, int empId, String orgId, String departmentId, String startDate, String endDate,
			String reason, String status, String leaveType, String reportsTo, String appliedDate,
			String modifiedDate, String lFlag) { */
	public LeaveElasticVTO(int id, int empId, String startDate, String endDate){
		 this.id=id;
    	    this.empId = empId;
    	    this.startDate = startDate;
	        this.endDate = endDate;
	        /*
	        this.orgId = orgId;
	        this.departmentId = departmentId;
	      
	        this.reason = reason;
	        this.status = status;
	        this.leaveType = leaveType;
	        this.reportsTo = reportsTo;
	        this.appliedDate = appliedDate;
	        this.modifiedDate = modifiedDate;
	        this.lFlag = lFlag; */
	        
	}


	public int getEmpId() {
		return empId;
	}



	public void setEmpId(int empId) {
		this.empId = empId;
	}




	public String getStartDate() {
		return startDate;
	}



	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}



	public String getEndDate() {
		return endDate;
	}



	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}


	

	public int getId() {
		return id;
	}



	public void setId(int id) {
		this.id = id;
	}



	@Override
    public String toString() {
        // return "LeaveElasticVTO{" + "empId=" + empId + ", orgId=" + orgId + ", departmentId=" + departmentId + ", startDate=" + startDate + ", endDate=" + endDate + ", reason=" + reason + ", status=" + status + ", leaveType=" + leaveType + ", leaveType=" + leaveType + ", reportsTo=" + reportsTo + ",  appliedDate=" + appliedDate + ", modifiedDate=" + modifiedDate + ", lFlag=" + lFlag + '}';
		return "LeaveElasticVTO{" + "empId=" + empId + ", startDate=" + startDate + ", endDate=" + endDate + '}';
    }

  
	
}
