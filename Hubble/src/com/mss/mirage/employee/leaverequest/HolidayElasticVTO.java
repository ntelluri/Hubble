package com.mss.mirage.employee.leaverequest;

/*
import io.searchbox.annotations.JestId;
import net.sf.json.JSONObject;*/

public class HolidayElasticVTO {
    private String date;
    private String country;
    private String description;
   
   

  
   // @JestId
    private int id;

 
/*
	public static HolidayElasticVTO fromJSON(JSONObject json) {
        return new HolidayElasticVTO(
        		json.getInt("id"),
                json.getString("date"),
                json.getString("country"),
                json.getString("description")
                
        );
    }
*/
 
	   public HolidayElasticVTO(int id,  String date, String country,String description) {
	    	    this.id=id;
	    	    this.date = date;
		        this.country = country;
		        this.description = description;
		        
			
		}

  


	public String getDate() {
		return date;
	}


	public void setDate(String date) {
		this.date = date;
	}


	public String getCountry() {
		return country;
	}


	public void setCountry(String country) {
		this.country = country;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	@Override
    public String toString() {
        return "HolidayElasticVTO{" + "date=" + date + ", country=" + country + ", description=" + description + '}';
    }

    
}
