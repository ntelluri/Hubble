/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mss.mirage.ajax;

import com.itextpdf.text.pdf.codec.Base64;
import com.mss.mirage.ajaxnew.NewAjaxHandlerAction;
import com.mss.mirage.crm.attachments.AttachmentService;
import com.mss.mirage.util.ConnectionProvider;
import com.mss.mirage.util.DataSourceDataProvider;
import com.mss.mirage.util.DateUtility;
import com.mss.mirage.util.FileUploadUtility;
import com.mss.mirage.util.Properties;
import com.mss.mirage.util.RestRepository;
import com.mss.mirage.util.ServiceLocator;
import com.mss.mirage.util.ServiceLocatorException;
import java.sql.CallableStatement;

import sun.misc.BASE64Decoder;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.io.FileUtils;
import org.json.JSONObject;

/**
 *
 * @author miracle
 */
public class AjaxHandlerWebServiceImpl implements AjaxHandlerWebService{
    
	public String jobPosting(AjaxHandlerAction ajaxHandlerAction) throws Exception  {
        // Connection connection = null;
        //PreparedStatement preparedStatement = null;
        // ResultSet resultSet = null;
 String message ="";
        boolean isUpdated = false;
        //  String queryString ="";
          //    System.out.println("first call");

        //queryString = "update tblEmpReview set ReviewTypeId=?,EmpComments=?,ModifiedBy=?,ModifiedDate=?,ReviewName=?,ReviewDate=?,Status=?,TLComments=?,TLRating=?,HRRating=?,HrComments=? where Id=?";
        //if(roleName.equalsIgnoreCase("Employee"))
        // queryString = "update tblEmpReview set ReviewTypeId=?,EmpComments=?,ReviewName=?,ReviewDate=?,Status=? where Id=?";
        try {
        	
        	
 JSONObject jb = new JSONObject();
             
             jb.put("CreatedBy", ajaxHandlerAction.getCreatedBy());
           
             jb.put("Data", ajaxHandlerAction.getAddpostDetails());
         //    System.out.println(ajaxHandlerAction.getAddpostDetails());
            
      
         String serviceUrl = RestRepository.getInstance().getSrviceUrl("JobPost");
              URL url = new URL(serviceUrl);
            URLConnection connection = url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setConnectTimeout(360*5000);
            connection.setReadTimeout(360*5000);
            OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
           out.write(jb.toString());
            out.close();

            BufferedReader in = new BufferedReader(new InputStreamReader(
                    connection.getInputStream()));
            String s = null;
            String data = "";
            while ((s = in.readLine()) != null) {

                data = data + s;
            }

        JSONObject jObject = new JSONObject(data);

         message = jObject.getString("message");
       // System.out.println("displaymessage-->" +message);

if(!"".equals(message)){
             if(!message.contains("Oops"))
             sendJobPostingDetails(jb,"add");
         }
            in.close();
            /* connection = ConnectionProvider.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(queryString);
            preparedStatement.setString(1,ajaxHandlerAction.getOverlayReviewType());
            preparedStatement.setString(2,ajaxHandlerAction.getOverlayDescription());
            
            
            
            preparedStatement.setString(3,ajaxHandlerAction.getOverlayReviewName());
            preparedStatement.setDate(4, DateUtility.getInstance().getMysqlDate(ajaxHandlerAction.getOverlayReviewDate()));
            preparedStatement.setString(5,ajaxHandlerAction.getReviewStatusOverlay());
            
            preparedStatement.setString(6,ajaxHandlerAction.getReviewId());
            isUpdated = preparedStatement.execute();*/
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return message;
    }
    
	
   
      
      public String editPosting(String jobId) throws Exception{
          String message ="";
        boolean isUpdated = false;
        //  String queryString ="";
             // System.out.println("first call");
            String data = "";
        //queryString = "update tblEmpReview set ReviewTypeId=?,EmpComments=?,ModifiedBy=?,ModifiedDate=?,ReviewName=?,ReviewDate=?,Status=?,TLComments=?,TLRating=?,HRRating=?,HrComments=? where Id=?";
        //if(roleName.equalsIgnoreCase("Employee"))
        // queryString = "update tblEmpReview set ReviewTypeId=?,EmpComments=?,ReviewName=?,ReviewDate=?,Status=? where Id=?";
        try {
            
              
                JSONObject jb = new JSONObject();
      
        jb.put("jobId",jobId);        
          //  URL url = new URL("http://172.17.11.251:8080/WebRoot/resources/doJobPost/edit");
        //  URL url = new URL("http://172.17.14.226:8080/WebRoot/resources/doJobPost/edit");
        // URL url = new URL(Properties.getProperty("WEB.REST.URL") +"doJobPost/edit");
        String serviceUrl = RestRepository.getInstance().getSrviceUrl("jobedit");
              URL url = new URL(serviceUrl);
            URLConnection connection = url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setConnectTimeout(360*5000);
            connection.setReadTimeout(360*5000);
            OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
           out.write(jb.toString());
            out.close();

            BufferedReader in = new BufferedReader(new InputStreamReader(
                    connection.getInputStream()));
            String s = null;

            while ((s = in.readLine()) != null) {

                data = data + s;
            }

      //  JSONObject jObject = new JSONObject(data);

        // message = jObject.getString("message");
      //  System.out.println("displaymessage-->" +message);


            in.close();
            /* connection = ConnectionProvider.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(queryString);
            preparedStatement.setString(1,ajaxHandlerAction.getOverlayReviewType());
            preparedStatement.setString(2,ajaxHandlerAction.getOverlayDescription());
            
            
            
            preparedStatement.setString(3,ajaxHandlerAction.getOverlayReviewName());
            preparedStatement.setDate(4, DateUtility.getInstance().getMysqlDate(ajaxHandlerAction.getOverlayReviewDate()));
            preparedStatement.setString(5,ajaxHandlerAction.getReviewStatusOverlay());
            
            preparedStatement.setString(6,ajaxHandlerAction.getReviewId());
            isUpdated = preparedStatement.execute();*/
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return data;
      }
      
public String upadteJobPosting(AjaxHandlerAction ajaxHandlerAction) throws Exception{
    	  
    	  //System.out.println("upadteJobPosting   2");
           // Connection connection = null;
        //PreparedStatement preparedStatement = null;
        // ResultSet resultSet = null;
 String message ="";
        boolean isUpdated = false;
        //  String queryString ="";
           //   System.out.println("first call");

        //queryString = "update tblEmpReview set ReviewTypeId=?,EmpComments=?,ModifiedBy=?,ModifiedDate=?,ReviewName=?,ReviewDate=?,Status=?,TLComments=?,TLRating=?,HRRating=?,HrComments=? where Id=?";
        //if(roleName.equalsIgnoreCase("Employee"))
        // queryString = "update tblEmpReview set ReviewTypeId=?,EmpComments=?,ReviewName=?,ReviewDate=?,Status=? where Id=?";
        try {
        	
        	 JSONObject jb = new JSONObject();
             
             jb.put("CreatedBy", ajaxHandlerAction.getCreatedBy());
           
             jb.put("Data", ajaxHandlerAction.getPositionDetails());
             //System.out.println(ajaxHandlerAction.getPositionDetails());
        
  String serviceUrl = RestRepository.getInstance().getSrviceUrl("jobupdate");
              URL url = new URL(serviceUrl);
            URLConnection connection = url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setConnectTimeout(360*5000);
            connection.setReadTimeout(360*5000);
            OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
           out.write(jb.toString());
            out.close();

            BufferedReader in = new BufferedReader(new InputStreamReader(
                    connection.getInputStream()));
            String s = null;
            String data = "";
            while ((s = in.readLine()) != null) {

                data = data + s;
            }

        JSONObject jObject = new JSONObject(data);

         message = jObject.getString("message");
      // System.out.println("displaymessage-->" +message);

 /*if(!"".equals(message)){
             if(!message.contains("Oops"))
             sendJobPostingDetails(jb,"update");
         }
          */ 
            in.close();
            
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return message;
      }
     
            
       public String getApplicantDetails(int applicantId) throws Exception{
            String message ="";
        boolean isUpdated = false;
        //  String queryString ="";
           //   System.out.println("first call");
            String data = "";
            Connection dbConnection = null;
            Statement statement = null;
            ResultSet resultSet = null;
        //queryString = "update tblEmpReview set ReviewTypeId=?,EmpComments=?,ModifiedBy=?,ModifiedDate=?,ReviewName=?,ReviewDate=?,Status=?,TLComments=?,TLRating=?,HRRating=?,HrComments=? where Id=?";
        //if(roleName.equalsIgnoreCase("Employee"))
        // queryString = "update tblEmpReview set ReviewTypeId=?,EmpComments=?,ReviewName=?,ReviewDate=?,Status=? where Id=?";
        try {
            
              
                JSONObject jb = new JSONObject();
      
        jb.put("applicantId",applicantId);        
          //  URL url = new URL("http://172.17.11.251:8080/WebRoot/resources/doJobPost/edit");
         // URL url = new URL("http://172.17.14.226:8080/WebRoot/resources/doJobPost/applicantInfo");
       // URL url = new URL(Properties.getProperty("WEB.REST.URL") +"doJobPost/applicantInfo");
        String serviceUrl = RestRepository.getInstance().getSrviceUrl("applicantInfo");
              URL url = new URL(serviceUrl);
            URLConnection connection = url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setConnectTimeout(360*5000);
            connection.setReadTimeout(360*5000);
            OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
           out.write(jb.toString());
            out.close();

            BufferedReader in = new BufferedReader(new InputStreamReader(
                    connection.getInputStream()));
            String s = null;

            while ((s = in.readLine()) != null) {

                data = data + s;
            }

      //  JSONObject jObject = new JSONObject(data);

        // message = jObject.getString("message");
      //  System.out.println("displaymessage-->" +message);


            in.close();
          
            /* connection = ConnectionProvider.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(queryString);
            preparedStatement.setString(1,ajaxHandlerAction.getOverlayReviewType());
            preparedStatement.setString(2,ajaxHandlerAction.getOverlayDescription());
            
            
            
            preparedStatement.setString(3,ajaxHandlerAction.getOverlayReviewName());
            preparedStatement.setDate(4, DateUtility.getInstance().getMysqlDate(ajaxHandlerAction.getOverlayReviewDate()));
            preparedStatement.setString(5,ajaxHandlerAction.getReviewStatusOverlay());
            
            preparedStatement.setString(6,ajaxHandlerAction.getReviewId());
            isUpdated = preparedStatement.execute();*/
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return data;
       }
       
       
       
       
       
      
 public String addWebsiteConsultant(int applicantId,String loginId) throws Exception{
    	   
    	   
    	   System.out.println("addWebsiteConsultant method calling ");
            String message ="";
        boolean isExisted = false;
        //  String queryString ="";
             // System.out.println("first call");
            String data = "";
            Connection dbConnection = null;
            CallableStatement callableStatement = null;
    		
            Statement statement = null;
            ResultSet resultSet = null;
            PreparedStatement preparedStatement = null;
            int existedConsultantId = 0;
             JSONObject resultJObject  = new JSONObject();
        try {
            
              
                JSONObject jb = new JSONObject();
      
        jb.put("applicantId",String.valueOf(applicantId));        
          /*
        URL url = new URL(Properties.getProperty("WEB.REST.URL") +"doJobPost/applicantInfo");
            URLConnection connection = url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setConnectTimeout(5000);
            connection.setReadTimeout(5000);
            OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
           out.write(jb.toString());
            out.close();

            BufferedReader in = new BufferedReader(new InputStreamReader(
                    connection.getInputStream()));
            String s = null;

            while ((s = in.readLine()) != null) {

                data = data + s;
            }

            in.close();*/
        
         data = getApplicantDetails(applicantId);
         
         System.out.println("Data-->"+data);
             JSONObject jObject  = new JSONObject(data);
             
             
            String Id = (String)jObject.get("Id"); 
            String JobId = (String)jObject.get("JobId"); 
            String FirstName = (String)jObject.get("FirstName"); 
            String LastName = (String)jObject.get("LastName"); 
            String MiddleName = (String)jObject.get("MiddleName"); 
            String PhoneNumber = (String)jObject.get("PhoneNumber"); 
            String EmailId = (String)jObject.get("EmailId"); 
            String Source = (String)jObject.get("Source"); 
            String ReferedBy="";

             ReferedBy = (String)jObject.get("ReferedBy"); 
            
         //   System.out.println("  EmailId-->"+EmailId   +"   Source-->"+Source  +"   ReferedBy-->"+ReferedBy);
            String JobTitle=(String)jObject.get("JobTitle");
          //  String NativePlace = (String)jObject.get("NativePlace"); 
           // String CurrentLocation = (String)jObject.get("CurrentLocation"); 
           // String NoticePeriod = (String)jObject.get("NoticePeriod"); 
            String ResumePath = (String)jObject.get("ResumePath"); 
            String ResumeName = (String)jObject.get("ResumeName"); 
            String AppliedDate = (String)jObject.get("AppliedDate"); 
           /* String NativeDistrict = (String)jObject.get("NativeDistrict"); 
            String NativeState = (String)jObject.get("NativeState"); 
            String NativeZipcode = (String)jObject.get("NativeZipcode");
             * *
             */  
            String CurrentOrg = (String)jObject.get("CurrentOrg"); 
            String Experience = (String)jObject.get("Experience"); 
            String EduQualification = (String)jObject.get("EduQualification"); 
            
            String CurentDistrict = (String)jObject.get("CurentDistrict"); 
            String CurrentState = (String)jObject.get("CurrentState"); 
            String CurrentZipcode = (String)jObject.get("CurrentZipcode"); 
            
          dbConnection = ConnectionProvider.getInstance().getConnection();
          statement = dbConnection.createStatement();
          resultSet = statement.executeQuery("SELECT Id FROM tblRecConsultant WHERE Email2 = '"+EmailId+"'");
          if(resultSet.next()){
            //  resultJObject.put("IsExisted", "true");
              isExisted = true;
              existedConsultantId = resultSet.getInt("Id");
          }
          resultSet.close();
          statement.close();
          String copiedPath = "";
         //  System.out.println("ResumePath..."+ResumePath);
          if(isExisted){
              resultJObject.put("IsExisted", "true");
              resultJObject.put("ExistedId", String.valueOf(existedConsultantId));
              
               File sourceFile = new File(ResumePath);
               
               String Base64String = jObject.getString("Base64String");
              
               
               if(!"".equals("Base64String")){
                 AttachmentService attachmentService = ServiceLocator.getAttachmentService();
                // String generatedPath = attachmentService.generatePath(Properties.getProperty("Attachments.Path"), "Emp Tasks");
                copiedPath = attachResumeBasedOnBase64(Base64String,sourceFile.getName());
          //      System.out.println("copiedPath..existed..."+copiedPath);
                
                int inseretdRows= addConsultantAttachment(existedConsultantId,copiedPath,sourceFile.getName());
                         // addConsultantAttachment(int consultantId,String filePath,String fileName)
                if(inseretdRows>0){
                    resultJObject.put("FileAdded", "true");
                    updateMergeFlag(jb);
                }
                else{
                     resultJObject.put("FileAdded", "false"); 
                }
                   
             }
              
          }else {
              resultJObject.put("IsExisted", "false");
             
              String Base64String = jObject.getString("Base64String");
            File sourceFile = new File(ResumePath);
              String fileName = "";
              if(!"".equals("Base64String")){
                 AttachmentService attachmentService = ServiceLocator.getAttachmentService();
                // String generatedPath = attachmentService.generatePath(Properties.getProperty("Attachments.Path"), "Emp Tasks");
                 copiedPath = attachResumeBasedOnBase64(Base64String,sourceFile.getName());
           //      System.out.println("copiedPath..existed else..."+copiedPath);
                 
                fileName = sourceFile.getName();
               
              }
              
               
          String query = "INSERT INTO tblConsultantMigration(FromLocation,Sourcepath,TargetPath,WebTblConsultantId,WebTblConsultantEmailId,Createdby,FileName) VALUES(?,?,?,?,?,?,?)";    
              preparedStatement = dbConnection.prepareStatement(query);
              preparedStatement.setString(1, ResumePath);
              preparedStatement.setString(2, ResumePath);
              preparedStatement.setString(3, copiedPath);
              preparedStatement.setInt(4, Integer.parseInt(Id));
              preparedStatement.setString(5, EmailId);
               preparedStatement.setString(6, loginId);
                preparedStatement.setString(7, fileName);
             int insertRows=  preparedStatement.executeUpdate();
               
               resultJObject.put("IsAdded", "true");
               
               if(insertRows>0){
            		callableStatement =  dbConnection.prepareCall("{call spConsultantMigration(?,?,?,?,?,?,?,?,?,?,?,?,?)}");
        			callableStatement.setInt(1, applicantId);
        			callableStatement.setString(2, FirstName);
        			callableStatement.setString(3, LastName);
        			callableStatement.setString(4, MiddleName);
        			callableStatement.setString(5, PhoneNumber);
        			callableStatement.setString(6, EmailId);
        			callableStatement.setString(7, JobId);
        			callableStatement.setString(8, ReferedBy);
        			callableStatement.setString(9, Source);
        			callableStatement.setString(10, JobTitle);
        			callableStatement.setString(11, loginId);
        			callableStatement.setString(12, fileName);
        			callableStatement.setString(13, copiedPath);
        			
        			
        			callableStatement.execute();
                    /*URL suburl = new URL(Properties.getProperty("WEB.REST.URL") +"doJobPost/updateMergeFlag");
            URLConnection subconnection = suburl.openConnection();
            subconnection.setDoOutput(true);
            subconnection.setRequestProperty("Content-Type", "application/json");
            subconnection.setConnectTimeout(5000);
            subconnection.setReadTimeout(5000);
            OutputStreamWriter subout = new OutputStreamWriter(subconnection.getOutputStream());
           subout.write(jb.toString());
            subout.close();

            BufferedReader subin = new BufferedReader(new InputStreamReader(
                    subconnection.getInputStream()));
            String subs = null;
String subdata = "";
            while ((subs = subin.readLine()) != null) {

                subdata = subdata + subs;
            }
System.out.println("subdata-->"+subdata);
            subin.close();
                   */
                   
                   String subdata = updateMergeFlag(jb);
                   //System.out.println("subdata-->"+subdata);
                   
                   
               }
               
               
               
               
               
               
               
               
               
               
               
          }
        } catch (IOException ex) {
            ex.printStackTrace();
        }catch(Exception exception){
          exception.printStackTrace();  
        }finally{
             try{
                 if(resultSet!=null){
                    resultSet.close();
                    resultSet = null;
                }
                if(callableStatement!=null){
                	callableStatement.close();
                    callableStatement = null;
                } 
                if(preparedStatement!=null){
                    preparedStatement.close();
                    preparedStatement = null;
                } 
                if(statement!=null){
                    statement.close();
                    statement = null;
                }
                if(dbConnection!=null){
                    dbConnection.close();
                    dbConnection = null;
                }
                
            }catch(SQLException sqle){
                throw new ServiceLocatorException(sqle);
            }
        }
        return resultJObject.toString();
       }
 
 public String attachResumeBasedOnBase64(String Base64String,String fileName) throws ServiceLocatorException {
	   
	   String basePath = "";
	   String generatedPath = "";

	   String conferenceUrl="";
	   String addLibraryPath="";

	   try {

	   // tokenize the data
	   String[] parts = Base64String.split(",");
	   String imageString = parts[1];
	   String[] imageType = parts[0].split(";");
	   String[] imageType1 = imageType[0].split("/");

	   BufferedImage image = null;
	   byte[] imageByte=Base64String.getBytes();

	   BASE64Decoder decoder = new BASE64Decoder();
	   imageByte = decoder.decodeBuffer(imageString);

	    basePath = Properties.getProperty("Resume.Attachments") + "//MirageV2//A";
	    generatedPath = FileUploadUtility.getInstance().filePathGeneration(basePath);
    
	   //System.out.println("generatedPath-->"+generatedPath);

	   File fileDir = new File(generatedPath);
	   if (!fileDir.exists()) {

	   fileDir.mkdirs();
	   }


	   File outputfile = new File(generatedPath + "/" + fileName );

	   FileOutputStream fos = new FileOutputStream(outputfile);
	   fos.write(imageByte);
	   fos.close();
	   basePath = outputfile.getPath();
	   //System.out.println("basePath---" + basePath);

	   } catch (Exception e) {
	   e.printStackTrace();
	   throw new ServiceLocatorException(e);
	   } 
	  
	   
	   //System.out.println("dbStoredPath-->"+dbStoredPath);

	   
return basePath;

	   }

       
      public String attachResume(File file) {
        String targetPath = null;
        try {

            /*     String basePath = Properties.getProperty("Resume.Attachments");
             File createPath = new File(basePath);
             java.util.Date dt = new java.util.Date();
             String month = "";
             if (dt.getMonth() == 0) {
             month = "Jan";
             } else if (dt.getMonth() == 1) {
             month = "Feb";
             } else if (dt.getMonth() == 2) {
             month = "Mar";
             } else if (dt.getMonth() == 3) {
             month = "Apr";
             } else if (dt.getMonth() == 4) {
             month = "May";
             } else if (dt.getMonth() == 5) {
             month = "Jun";
             } else if (dt.getMonth() == 6) {
             month = "Jul";
             } else if (dt.getMonth() == 7) {
             month = "Aug";
             } else if (dt.getMonth() == 8) {
             month = "Sep";
             } else if (dt.getMonth() == 9) {
             month = "Oct";
             } else if (dt.getMonth() == 10) {
             month = "Nov";
             } else if (dt.getMonth() == 11) {
             month = "Dec";
             }
             short week = (short) (Math.round(dt.getDate() / 7));
             createPath = new File(createPath.getAbsolutePath() + "//MirageV2//A//" + String.valueOf(dt.getYear() + 1900) + "//" + month + "//" + String.valueOf(week));
             createPath.mkdir();
             File theFile = new File(createPath.getAbsolutePath() + "//" + file.getName());
             targetPath = theFile.getAbsolutePath();
             FileUtils.copyFile(file, theFile);   */

            String basePath = Properties.getProperty("Resume.Attachments") + "//MirageV2//A";
            String theFilePath = FileUploadUtility.getInstance().filePathGeneration(basePath);
            String fileName = FileUploadUtility.getInstance().fileNameGeneration(file.getName());
            File theFile = new File(theFilePath);
            theFile.mkdir();
            theFile = new File(theFile.getAbsoluteFile() + "//" + fileName);
            targetPath = theFile.getAbsolutePath();
            FileUtils.copyFile(file, theFile);


        } catch (Exception ex) {
            //List errorMsgList = ExceptionToListUtility.errorMessages(ex);
            ex.printStackTrace();
            //System.err.println("In Action Class Catch"+ex.getMessage());
            //httpServletRequest.getSession(false).setAttribute("errorMessage",ex.toString());

        }


        return targetPath;
    }
       
       
       
           public int addConsultantAttachment(int consultantId,String filePath,String fileName) throws ServiceLocatorException{
        int isSuccess = 0;
        Connection connection = null;
        PreparedStatement preStmt=null;
        
        try {
            connection = ConnectionProvider.getInstance().getConnection();
            
             
           
            /* int Id = DataSourceDataProvider.getInstance().getMaxRecAttachMentId();
             Id = Id + 1;*/
             //System.out.print("Id-->"+Id);
            
             preStmt = connection.prepareStatement("INSERT INTO tblRecAttachments(ObjectId,ObjectType,AttachmentName,AttachmentFileName,AttachmentLocation,DateUploaded) values(?,?,?,?,?,?)");
             
             preStmt.setInt(1,consultantId);
             preStmt.setString(2,"A");
             preStmt.setString(3,fileName);
             preStmt.setString(4,fileName);
             preStmt.setString(5,filePath);
             preStmt.setTimestamp(6,DateUtility.getInstance().getCurrentMySqlDateTime());
             
             isSuccess = preStmt.executeUpdate();
             
        }catch (Exception ex){
            throw new ServiceLocatorException(ex);
        }finally{
            try{
                if(preStmt!=null){
                    preStmt.close();
                    preStmt = null;
                }
                if(connection!=null){
                    connection.close();
                    connection = null;
                }
                
            }catch(SQLException sqle){
                throw new ServiceLocatorException(sqle);
            }
        }
        
        return isSuccess;
    }
       
           
           
           
public String updateMergeFlag(JSONObject jb) {
    String subdata = "";
       try {
     // URL suburl = new URL(Properties.getProperty("WEB.REST.URL") +"doJobPost/updateMergeFlag");
            String serviceUrl = RestRepository.getInstance().getSrviceUrl("consultantMerge");
              URL suburl = new URL(serviceUrl);
            URLConnection subconnection = suburl.openConnection();
            subconnection.setDoOutput(true);
            subconnection.setRequestProperty("Content-Type", "application/json");
            subconnection.setConnectTimeout(360*5000);
            subconnection.setReadTimeout(360*5000);
            OutputStreamWriter subout = new OutputStreamWriter(subconnection.getOutputStream());
           subout.write(jb.toString());
            subout.close();

            BufferedReader subin = new BufferedReader(new InputStreamReader(
                    subconnection.getInputStream()));
            String subs = null;

            while ((subs = subin.readLine()) != null) {

                subdata = subdata + subs;
            }
//System.out.println("subdata-->"+subdata);
            subin.close();
       }catch(Exception exception)   {
           exception.printStackTrace();
       }
       return subdata;
}
           
           
 //public static void sendConsultantUpdatedDetailsForRequirement(String title,String requirementId,String consultantId,String rate,String startDate,String status,String comments,String modifiedBy,String modifiedDate) throws ServiceLocatorException {
   
public static void sendJobPostingDetails(JSONObject jb,String activityType) throws ServiceLocatorException {
    // SUBSTITUTE YOUR EMAIL ADDRESSES HERE!!!
    
JSONObject jObject=null;

    try {
    	String jobApplicationdata = jb.getString("Data");
   	 jObject  = new JSONObject(jobApplicationdata);
    	
	    String jobTitle = jObject.get("JobTitle").toString();
        String jobExperience = jObject.get("JobQualification").toString()+" Years's";
        String JobStatus = jObject.get("JobStatus").toString();
		//String JobDescription = jObject.get("JobDescription").toString();
		String Location = jObject.get("Location").toString();	
		String JobDepartment = jObject.get("JobDepartment").toString();	
		String CreatedBy = jb.getString("CreatedBy");	
        
       
     // String to = "sjanakala@miraclesoft.com,bmedasetti@miraclesoft.com,vkusampudi@miraclesoft.com,clokam@miraclesoft.com";
            //WEB.JOBPOST.EMAILS
            String to = Properties.getProperty("WEB.JOBPOST.EMAILS");
      String from = Properties.getProperty("Mail.From");
      String subject = "";
      if(activityType.equals("add"))
       subject = "A Position has been created!";
        else
          subject = "A Position has been updated!";
       
      StringBuilder sb = new StringBuilder("");
   sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>");
    
    sb.append("<html xmlns='http://www.w3.org/1999/xhtml'>");
sb.append("<head>");
sb.append("<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />");
sb.append("<title>");
sb.append("Position Status Change");
sb.append("</title>");
sb.append("<style type='text/css'>");
sb.append("body {");
sb.append("padding-top: 0 !important;");
sb.append("padding-bottom: 0 !important;");
sb.append("padding-top: 0 !important;");
sb.append("padding-bottom: 0 !important;");
sb.append("margin:0 !important;");
sb.append("width: 100% !important;");
sb.append("-webkit-text-size-adjust: 100% !important;");
sb.append("-ms-text-size-adjust: 100% !important;");
sb.append("-webkit-font-smoothing: antialiased !important;");
sb.append("}");
sb.append(".tableContent img {");
sb.append("border: 0 !important;");
sb.append("display: block !important;");
sb.append("outline: none !important;");
sb.append("}");
sb.append("a{");
sb.append("color:#382F2E;");
sb.append("}");
sb.append("p, h1,h2,ul,ol,li,div{");
sb.append("margin:0;");
sb.append("padding:0;");
sb.append("}");
sb.append("h1,h2{");
sb.append("font-weight: normal;");
sb.append("background:transparent !important;");
sb.append("border:none !important;");
sb.append("}");
sb.append(".contentEditable h2.big,.contentEditable h1.big{");
sb.append("font-size: 26px !important;");
sb.append("}");
sb.append(".contentEditable h2.bigger,.contentEditable h1.bigger{");
sb.append("font-size: 37px !important;");
sb.append("}");
sb.append("td,table{");
sb.append("vertical-align: top;");
sb.append("}");
sb.append("td.middle{");
sb.append("vertical-align: middle;");
sb.append("}");
sb.append("a.link1{");
sb.append("font-size:13px;");
sb.append("color:#27A1E5;");
sb.append("line-height: 24px;");
sb.append("text-decoration:none;");
sb.append("}");
sb.append("a{");
sb.append("text-decoration: none;");
sb.append("}");
sb.append(".link2{");
sb.append("color:#ef4048;");
sb.append("border-top:6px solid #ef4048;");
sb.append("border-bottom:8px solid #ef4048;");
sb.append("border-left:8px solid #ef4048;");
sb.append("border-right:8px solid #ef4048;");
sb.append("border-radius:0px;");
sb.append("-moz-border-radius:3px;");
sb.append("-webkit-border-radius:3px;");
sb.append("background:#ef4048;");
sb.append("}");
sb.append(".link3{");
sb.append("color:#555555;");
sb.append("border:1px solid #cccccc;");
sb.append("padding:10px 18px;");
sb.append("border-radius:3px;");
sb.append("-moz-border-radius:3px;");
sb.append("-webkit-border-radius:3px;");
sb.append("background:#ffffff;");
sb.append("}");
sb.append(".link4{");
sb.append("color:#27A1E5;");
sb.append("line-height: 24px;");
sb.append("}");
sb.append("h2,h1{");
sb.append("line-height: 20px;");
sb.append("}");
sb.append("p{");
sb.append("font-size: 14px;");
sb.append("line-height: 21px;");
sb.append("color:#AAAAAA;");
sb.append("}");
sb.append(".contentEditable li{");
sb.append("}");
sb.append(".appart p{");
sb.append("}");
sb.append(".bgItem{");
sb.append("background:#ffffff;");
sb.append("}");
sb.append(".bgBody{");
sb.append("background: rgb(13,65,107);");
sb.append("}");
sb.append("img {");
sb.append("outline:none;");
sb.append("text-decoration:none;");
sb.append("-ms-interpolation-mode: bicubic;");
sb.append("width: auto;");
sb.append("max-width: 100%;");
sb.append("clear: both;");
sb.append("display: block;");
sb.append("float: none;");
sb.append("}");
sb.append("</style>");
sb.append("<script type='colorScheme' class='swatch active'>");
sb.append("{");
sb.append("'name':'Default',");
sb.append("'bgBody':'ffffff',");
sb.append("'link':'27A1E5',");
sb.append("'color':'AAAAAA',");
sb.append("'bgItem':'ffffff',");
sb.append("'title':'444444'");
sb.append("}");
sb.append("</script>");
sb.append("</head>");
sb.append("<body paddingwidth='0' paddingheight='0' bgcolor='#d1d3d4'  style='padding-top: 0; padding-bottom: 0; padding-top: 0; padding-bottom: 0; background-repeat: repeat; width: 100% !important; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; -webkit-font-smoothing: antialiased;' offset='0' toppadding='0' leftpadding='0'>");
sb.append("<table width='100%' border='0' cellspacing='0' cellpadding='0' class='tableContent bgBody' align='center'  style='font-family:Helvetica, sans-serif;'>");
sb.append("<tr>");
sb.append("<td align='center'>");
sb.append("<table width='600' border='0' cellspacing='0' cellpadding='0' align='center' >");
sb.append("<tr>");
sb.append("<td class='bgItem' align='center'>");
sb.append("<table width='600' border='0' cellspacing='0' cellpadding='0' align='center'>");
sb.append("<tr>");
sb.append("<td class='movableContentContainer' align='center'>");
sb.append("<div class='movableContent'>");
sb.append("<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center'>");
  sb.append("<tr>");
	sb.append("<td style='background: rgb(13,65,107); border-radius:0px;-moz-border-radius:0px;-webkit-border-radius:0px' height='30'>");
	sb.append("</td>");
  sb.append("</tr>");
  sb.append("<tr>");
	sb.append("<td style='background:rgb(13,65,107); border-radius:0px;-moz-border-radius:0px;-webkit-border-radius:0px'>");
	  sb.append("<table width='650' border='0' cellspacing='0' cellpadding='0' align='center'>");
		sb.append("<tr>");
		  sb.append("<td width='130'>");
			sb.append("<div class='contentEditableContainer contentImageEditable'>");
			  sb.append("<div class='contentEditable'>");
				sb.append("<a href='https://www.miraclesoft.com/index.php' target='_blank'>");
				  sb.append("<img src='https://www.miraclesoft.com/images/logo.png' alt='Logo' width='70' height='45' data-default='placeholder' data-max-width='200'>");
				sb.append("</a>");
			  sb.append("</div>");
			sb.append("</div>");
		  sb.append("</td>");
		  sb.append("<td valign='middle' style='vertical-align: middle;'>");
		  sb.append("</td>");
		  sb.append("<td valign='middle' style='vertical-align: middle;' width='150'>");
		  sb.append("</br>");
		sb.append("<table width='300' border='0' cellpadding='0' cellspacing='0' align='right' style='text-align: right; font-size: 15px; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;' class='fullCenter'>");
		  sb.append("<tr>");
			sb.append("<td height='55' valign='middle' width='100%' style='font-family: calibri; color:#000000;'>");
			  sb.append("<span style='font-family: calibri; font-weight: normal;'>");
				sb.append("<a href='https://www.miraclesoft.com' target='_blank' style='text-decoration: none; color:#ffffff;'class='underline' >");
				  sb.append("Company");
				sb.append("</a>");
			  sb.append("</span>");
			  sb.append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
			  sb.append("<span style='font-family:calibri; font-weight: normal;'>");
				sb.append("<a href='https://www.miraclesoft.com/careers/careers.php' target='_blank' style='text-decoration: none; color:#ffffff;'class='underline' >");
				  sb.append("Services");
				sb.append("</a>");
			  sb.append("</span>");
			  sb.append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
			  sb.append("<span style='font-family:calibri; font-weight: normal;'>");
				sb.append("<a href='https://www.miraclesoft.com/careers/careers.php' target='_blank' style='text-decoration: none; color:#ffffff;'class='underline' >");
				  sb.append("Contact");
				sb.append("</a>");
			  sb.append("</span>");
			sb.append("</td>");
		  sb.append("</tr>");
		sb.append("</table>");
	  sb.append("</td>");
	sb.append("</tr>");
sb.append("</table>");
sb.append("</td>");
sb.append("</tr>");
sb.append("</table>");
sb.append("</div>");
sb.append("<div class='movableContent'>");
sb.append("<table width='580' border='0' cellspacing='0' cellpadding='0' align='center'>");
sb.append("<tr>");
sb.append("<td style='border: 5px solid #000000; border-radius:0px;-moz-border-radius:0px;-webkit-border-radius:0px'>");
sb.append("<div class='movableContent'>");
sb.append("<table width='650' border='0' cellspacing='0' cellpadding='0' align='center'>");
sb.append("<tr>");
  sb.append("<td style='background:#ef4048; border-radius:0px;-moz-border-radius:0px;-webkit-border-radius:0px'>");
	sb.append("<table width='600' border='0' cellspacing='0' cellpadding='0' align='center'>");
	  sb.append("<tr>");
		sb.append("<td height='15'>");
		sb.append("</td>");
	  sb.append("</tr>");
	  sb.append("<tr>");
		sb.append("<td>");
		  sb.append("<div class='contentEditableContainer contentTextEditable'>");
			sb.append("<div class='contentEditable' style='text-align: left;'>");
			  sb.append("<h1 style='font-size: 24px;'>");
				sb.append("<font color='#FFffff' face='calibri'>");
				  sb.append("<b>");
                                      if(activityType.equals("add")) {
                                          sb.append("A Position Has Been Created!");
                                      }else {
                                          //sb.append("A Position Status Change has Occurred!");
                                          sb.append("A Position Has Been Updated!");
                                      }
				  sb.append("</b>");
				sb.append("</font>");
			  sb.append("</h1>");
			  sb.append("<br>");
			sb.append("</div>");
		  sb.append("</div>");
		sb.append("</td>");
	  sb.append("</tr>");
	sb.append("</table>");
  sb.append("</td>");
sb.append("</tr>");
sb.append("</table>");
sb.append("</div>");
sb.append("<table width='600' border='0' cellspacing='0' cellpadding='0' align='center'>");
sb.append("<tr>");
sb.append("<td>");
  sb.append("<div class='contentEditableContainer contentTextEditable'>");
	sb.append("<div class='contentEditable' style='text-align: center;'>");
	  sb.append("<br>");
	  sb.append("<p style='text-align: justify; font-size: 15px;'>");
		sb.append("<font color='#232527' face='calibri'>");
		  sb.append("<b>");
		  sb.append("Social Media Team,");
		  sb.append("</b>");
		sb.append("</font>");
	  sb.append("</p>");
	sb.append("</br>");
	sb.append("<p style='text-align: justify; font-size: 15px;'>");
	  sb.append("<font color='#8c8c8c' face='calibri'>");
		//sb.append("A positon has been <b>[status: created/activated/edited]</b> on our website. Please do post the same with the following details to our Social Media Pages. ");
              sb.append("A positon has been <b>"+JobStatus+"</b> on our website. Please do post the same with the following details to our Social Media Pages. ");
	  sb.append("</font>");
	sb.append("</p>");
  sb.append("</br>");
sb.append("</div>");
sb.append("</div>");

sb.append("<div class='movableContent'>");
sb.append("  <table width='600' border='0' cellspacing='0' cellpadding='0' align='center'>");

sb.append("	<tr>");
sb.append("	  <td width='600' style='line-height: 10px;'>");
sb.append("		<table width='600' border='0' cellpadding='0' cellspacing='0' align='center'>");
sb.append("		  <tr>");
sb.append("			<td>");
sb.append("			  <div class='contentEditableContainer contentTextEditable'>");
sb.append("				<div class='contentEditable' style='text-align: justify;'>");
sb.append("				  <p style='text-align: justify; font-size: 15px;'>");
sb.append("					<font color= #232527 face='calibri'>");
sb.append("					  <b>");
sb.append("						Job Title :");
sb.append("					  </b>");
sb.append("					</font>");
sb.append("					<font color= #8c8c8c face='calibri'>");
sb.append(					  jobTitle);
sb.append("					</font>");
sb.append("				  </p>");
sb.append("				  <br>");
sb.append("				  <p style='text-align: justify; font-size: 15px;'>");
sb.append("					<font color= #232527 face='calibri'>");
sb.append("					  <b>");
sb.append("						Location :");
sb.append("					  </b>");
sb.append("					</font>");
sb.append("					<font color= #8c8c8c face='calibri'>");
sb.append(					  Location);
sb.append("					</font>");
sb.append("				  </p>");
sb.append("				  <br>");
sb.append("				  <p style='text-align: justify; font-size: 15px;'>");
sb.append("					<font color= #232527 face='calibri'>");
sb.append("					  <b>");
sb.append("						Department :");
sb.append("					  </b>");
sb.append("					</font>");
sb.append("					<font color= #8c8c8c face='calibri'>");
sb.append(					  JobDepartment);
sb.append("					</font>");
sb.append("				  </p>");
sb.append("				  <br>");
sb.append("				  <p style='text-align: justify; font-size: 15px;'>");
sb.append("					<font color= #232527 face='calibri'>");
sb.append("					  <b>");
sb.append("						Experience: ");
sb.append("					  </b>");
sb.append("					</font>");
sb.append("					<font color= #8c8c8c face='calibri'>");
sb.append(					  jobExperience);
sb.append("					</font>");
sb.append("				  </p>");
sb.append("				  <br>");


if(activityType.equals("add")){
sb.append("				  <p style='text-align: justify; font-size: 15px;'>");
sb.append("					<font color= #232527 face='calibri'>");
sb.append("					  <b>");
sb.append("						CreatedBy: ");
sb.append("					  </b>");
sb.append("					</font>");
sb.append("					<font color= #8c8c8c face='calibri'>");
sb.append(					  CreatedBy);
sb.append("					</font>");
sb.append("				  </p>");
sb.append("				  <br>");
}else {
sb.append("				  <p style='text-align: justify; font-size: 15px;'>");
sb.append("					<font color= #232527 face='calibri'>");
sb.append("					  <b>");
sb.append("						ModifiedBy: ");
sb.append("					  </b>");
sb.append("					</font>");
sb.append("					<font color= #8c8c8c face='calibri'>");
sb.append(					  CreatedBy);
sb.append("					</font>");
sb.append("				  </p>");
sb.append("				  <br>");
}

sb.append("				  <br>");
sb.append("				  <br>");
sb.append("				  <a target='_blank' href='https://www.miraclesoft.com/careers/open-positions.php' class='link2' style='color:#ffffff'>");
sb.append("					<font face='calibri'>");
sb.append("					  <b>");
sb.append("						Read more");
sb.append("					  </b>");
sb.append("					</font>");
sb.append("				  </a>");
sb.append("				  <br>");
sb.append("				  <br>");
sb.append("				  <br>");
sb.append("				  <br>");
sb.append("				  <br>");
sb.append("				  <br>");
sb.append("				</div>");
sb.append("			  </div>");
sb.append("			</td>");
sb.append("		  </tr>");
sb.append("		</table>");
sb.append("	  </td>");

  
sb.append("	  <td height='40' colspan='3'>");
sb.append("	  </td>");
sb.append("	</tr>");
sb.append("  </table>");
sb.append("</div>");
sb.append("<div class='contentEditableContainer contentTextEditable'>");
sb.append("  <div class='contentEditable' style='text-align: center;'>");
sb.append("	<p style='text-align: justify; font-size: 14px;'>");
sb.append("	  <font color= #8c8c8c face='calibri'>");
sb.append("		<b>");
sb.append("		  Thanks & Regards,");
sb.append("		</b>");
sb.append("	  </br>");
sb.append("	  <b>");
sb.append("		Corporate Application Support Team,");
sb.append("	  </b>");
sb.append("	</br>");
sb.append("  Miracle Software Systems, Inc.");
sb.append("</br>");
sb.append("45625 Grand River Avenue, Novi, MI(USA)");
sb.append("</br>");
sb.append("<b>");
sb.append("Email :");
sb.append("</b>");
sb.append("hubbleapp@miraclesoft.com");
sb.append("</br>");
sb.append("<b>");
sb.append("Phone :");
sb.append("</b>");
sb.append("(+1)248-233-1814");
sb.append("</font>");
sb.append("</p>");
sb.append("</br>");
sb.append("<p style='text-align: justify; font-size: 15px;'>");
sb.append("<font color= #ef4048 face='calibri'>");
sb.append("<i>");
sb.append("<b>");
sb.append("*Note:");
sb.append("</b>");
sb.append("Please do not reply to this email as this is an automated notification.");
sb.append("</i>");
sb.append("</font>");
sb.append("</p>");
sb.append("<br>");
sb.append("</div>");
sb.append("</div>");
sb.append("</td>");
sb.append("</tr>");
sb.append("<tr>");
sb.append("<td height='10' colspan='3'>");
sb.append("</td>");
sb.append("</tr>");
sb.append("</table>");
sb.append("</td>");
sb.append("</tr>");
sb.append("</table>");
sb.append("</div>");
sb.append("<div class='movableContent'>");
sb.append("<table width='660' border='0' cellspacing='0' cellpadding='0' align='center'>");
sb.append("<tr>");
sb.append("<td style='background:rgb(13,65,107); border-radius:0px;-moz-border-radius:0px;-webkit-border-radius:0px'>");
sb.append("<table width='655' border='0' cellspacing='0' cellpadding='0' align='center'>");
sb.append("<tr>");
sb.append("<td colspan='3' height='20'>");
sb.append("</td>");
sb.append("</tr>");
sb.append("<tr>");
sb.append("<td width='90'>");
sb.append("</td>");
sb.append("<td width='660'align='center' style='text-align: center;'>");
sb.append("<table width='660' cellpadding='0' cellspacing='0' align='center'>");
sb.append("<tr>");
sb.append("<td>");
sb.append("<div class='contentEditableContainer contentTextEditable'>");
sb.append("  <div class='contentEditable' style='text-align: center;color:#AAAAAA;'>");
sb.append("	<p style='text-align: center; font-size: 14px;'>");
sb.append("	  <font color='#ffffff' face='calibri'>");
sb.append("		© Copyright 2015 Miracle Software Systems, Inc.");
sb.append("	  </br>");
sb.append("	  45625 Grand River Avenue");
sb.append("	</br>");
sb.append("  Novi, MI - USA");
sb.append("</p>");
sb.append("</div>");
sb.append("</div>");
sb.append("</td>");
sb.append("</tr>");
sb.append("</table>");
sb.append("</td>");
sb.append("<td width='90'>");
sb.append("</td>");
sb.append("</tr>");
sb.append("</table>");
sb.append("<table width='660' border='0' cellspacing='0' cellpadding='0' align='center'>");
sb.append("<tr>");
sb.append("<td colspan='3' height='10'>");
sb.append("</td>");
sb.append("</tr>");
sb.append("<tr>");
sb.append("<td width='230' align='center' style='text-align: center;'>");
sb.append("<table width='230' cellpadding='0' cellspacing='0' align='center'>");
sb.append("<tr>");
sb.append("<td width='40'>");
sb.append("<div class='contentEditableContainer contentFacebookEditable'>");
sb.append("<div class='contentEditable' style='text-align: center;color:#AAAAAA;'>");
sb.append("<a href='https://www.facebook.com/miracle45625' target='_blank'>");
sb.append("<img src='https://www.miraclesoft.com/newsletters/others/2015/social/fb.png' alt='facebook' width='30' height='30' data-max-width='40' data-customIcon='true' >");
sb.append("</a>");
sb.append("</div>");
sb.append("</div>");
sb.append("</td>");
sb.append("<td width='10'>");
sb.append("</td>");
sb.append("<td width='40'>");
sb.append("<div class='contentEditableContainer contentTwitterEditable'>");
sb.append("<div class='contentEditable' style='text-align: center;color:#AAAAAA;'>");
sb.append("<a href='https://twitter.com/team_mss' target='_blank'>");
sb.append("<img src='https://www.miraclesoft.com/newsletters/others/2015/social/twitter.png' alt='twitter' width='30' height='30' data-max-width='40' data-customIcon='true'>");
sb.append("</a>");
sb.append("</div>");
sb.append("</div>");
sb.append("</td>");
sb.append("<td width='10'>");
sb.append("</td>");
sb.append("<td width='40'>");
sb.append("<div class='contentEditableContainer contentTwitterEditable'>");
sb.append("<div class='contentEditable' style='text-align: center;color:#AAAAAA;'>");
sb.append("<a href='https://www.linkedin.com/company/miracle-software-systems-inc&quot;' target='_blank'>");
sb.append("<img src='https://www.miraclesoft.com/newsletters/others/2015/social/linkedin.png' alt='linkedin' width='30' height='30' data-max-width='40' data-customIcon='true'>");
sb.append("</a>");
sb.append("</div>");
sb.append("</div>");
sb.append("</td>");
sb.append("<td width='10'>");
sb.append("</td>");
sb.append("<td width='40'>");
sb.append("<div class='contentEditableContainer contentFacebookEditable'>");
sb.append("<div class='contentEditable' style='text-align: center;color:#AAAAAA;'>");
sb.append("<a href='https://plus.google.com/+Team_MSS' target='_blank'>");
sb.append("<img src='https://www.miraclesoft.com/newsletters/others/2015/social/googleplus.png' alt='googleplus' width='30' height='30' data-max-width='40' data-customIcon='true' >");
sb.append("</a>");
sb.append("</div>");
sb.append("</div>");
sb.append("</td>");
sb.append("<td width='10'>");
sb.append("</td>");
sb.append("<td width='40'>");
sb.append("<div class='contentEditableContainer contentTwitterEditable'>");
sb.append("<div class='contentEditable' style='text-align: center;color:#AAAAAA;'>");
sb.append("<a href='https://www.flickr.com/photos/team_miracle' target='_blank'>");
sb.append("<img src='https://www.miraclesoft.com/newsletters/others/2015/social/flicker.png' alt='flicker' width='30' height='30' data-max-width='40' data-customIcon='true'>");
sb.append("</a>");
sb.append("</div>");
sb.append("</div>");
sb.append("</td>");
sb.append("<td width='10'>");
sb.append("</td>");
sb.append("<td width='40'>");
sb.append("<div class='contentEditableContainer contentTwitterEditable'>");
sb.append("<div class='contentEditable' style='text-align: center;color:#AAAAAA;'>");
sb.append("<a href='https://www.youtube.com/c/Team_MSS' target='_blank'>");
sb.append("<img src='https://www.miraclesoft.com/newsletters/others/2015/social/youtube.png' alt='youtube' width='30' height='30' data-max-width='40' data-customIcon='true'>");
sb.append("</a>");
sb.append("</div>");
sb.append("</div>");
sb.append("</td>");
sb.append("<td width='10'>");
sb.append("</td>");
sb.append("</tr>");
sb.append("</table>");
sb.append("</td>");
sb.append("</tr>");
sb.append("<tr>");
sb.append("<td colspan='3' height='40'>");
sb.append("</td>");
sb.append("</tr>");
sb.append("</table>");
sb.append("</td>");
sb.append("</tr>");
sb.append("</table>");
sb.append("</div>");
sb.append("<div class='movableContent'>");
sb.append("<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center'>");
sb.append("<tr>");
sb.append("<td style='background:rgb(13,65,107); border-radius:0px;-moz-border-radius:0px;-webkit-border-radius:0px' height='0'>");
sb.append("</td>");
sb.append("</tr>");
sb.append("<tr>");
sb.append("</table>");
sb.append("</div>");
sb.append("</td>");
sb.append("</tr>");
sb.append("</table>");
sb.append("</td>");
sb.append("</tr>");
sb.append("</table>");
sb.append("</td>");
sb.append("</tr>");
sb.append("</table>");
sb.append("</body>");
sb.append("</html>");

        
      ServiceLocator.getMailServices().doAddEmailLog(to, "", subject, sb.toString(), "", "");
      //  doAddEmailLog(String toAddress, String cCAddress, String subject,String bodyContent,String createdDate,String bCCAddress)
    } catch(Exception ex){
        ex.printStackTrace();
    }
}

      
public String addEventposting(AjaxHandlerAction ajaxHandlerAction) throws Exception{
       
 String message ="";
        boolean isUpdated = false;
      
        try {
            
              
                JSONObject jb = new JSONObject();
              
        jb.put("event_title", ajaxHandlerAction.getEventTitle());
  
        jb.put("event_description", ajaxHandlerAction.getEventDescription());
    
        jb.put("event_startdate", ajaxHandlerAction.getStartDate());
      
        jb.put("evetnt_enddate",ajaxHandlerAction.getEndDate());
        jb.put("event_time_from",ajaxHandlerAction.getStartTime());
        jb.put("event_time_to",ajaxHandlerAction.getEndTime());  
        jb.put("midday_from",ajaxHandlerAction.getMidDayFrom()); 
        jb.put("midday_to",ajaxHandlerAction.getMidDayTo()); 
        jb.put("timezone",ajaxHandlerAction.getTimeZone()); 
        jb.put("location",ajaxHandlerAction.getEventLocation()); 
        jb.put("transport",ajaxHandlerAction.getTransportation()); 
        jb.put("createdby",ajaxHandlerAction.getCreatedBy()); 
       jb.put("status",ajaxHandlerAction.getEventStatus()); 
        jb.put("WebinarType",ajaxHandlerAction.getEventType()); 
        
        if(ajaxHandlerAction.getEventType().equalsIgnoreCase("I")|| ajaxHandlerAction.getEventType().equalsIgnoreCase("E")){
        jb.put("event_bold_Title",ajaxHandlerAction.getEventBoldtitle()); 
        jb.put("event_tagline",ajaxHandlerAction.getEventRegluarTitle()); 
        jb.put("OrganizerEmail",ajaxHandlerAction.getContactUsEmail()); 
        jb.put("RegistrationLink",ajaxHandlerAction.getEventRegistrationLink()); 
        
       // jb.put("PrimaryTrack",ajaxHandlerAction.getPrimaryTrack());
       // jb.put("SecondaryTrack",ajaxHandlerAction.getSecondaryTrack());
         if (!"".equals(ajaxHandlerAction.getPrimaryTrack()) && ajaxHandlerAction.getPrimaryTrack() != null) {
                jb.put("PrimaryTrack", ajaxHandlerAction.getPrimaryTrack());
            } else {
                jb.put("PrimaryTrack", "0");
            }
            if (!"".equals(ajaxHandlerAction.getSecondaryTrack()) && ajaxHandlerAction.getSecondaryTrack() != null) {
                jb.put("SecondaryTrack", ajaxHandlerAction.getSecondaryTrack());
            } else {
                jb.put("SecondaryTrack", "0");
            }
        jb.put("Department",ajaxHandlerAction.getEventDepartment());
        }
         if(ajaxHandlerAction.getEventType().equalsIgnoreCase("C")){
             jb.put("event_redirect",ajaxHandlerAction.getConferenceUrl()); 
         }
         jb.put("VideoLink",ajaxHandlerAction.getEventAfterVideoUrl());
     //    System.out.println("getConferenceUrl-->"+ajaxHandlerAction.getConferenceUrl());
         //   URL url = new URL(Properties.getProperty("WEB.REST.URL") +"doEventPost/addEvent");
         String serviceUrl = RestRepository.getInstance().getSrviceUrl("addEvent");
              URL url = new URL(serviceUrl);
            URLConnection connection = url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setConnectTimeout(360*5000);
            connection.setReadTimeout(360*5000);
            OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
           out.write(jb.toString());
            out.close();

            BufferedReader in = new BufferedReader(new InputStreamReader(
                    connection.getInputStream()));
            String s = null;
            String data = "";
            while ((s = in.readLine()) != null) {

                data = data + s;
            }

        JSONObject jObject = new JSONObject(data);

         message = jObject.getString("message");
     


            in.close();
           
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return message;   
      }     
      
      
      
       public String editEventposting(String eventId) throws Exception{
          String message ="";
        boolean isUpdated = false;
        //  String queryString ="";
             // System.out.println("first call");
            String data = "";
        //queryString = "update tblEmpReview set ReviewTypeId=?,EmpComments=?,ModifiedBy=?,ModifiedDate=?,ReviewName=?,ReviewDate=?,Status=?,TLComments=?,TLRating=?,HRRating=?,HrComments=? where Id=?";
        //if(roleName.equalsIgnoreCase("Employee"))
        // queryString = "update tblEmpReview set ReviewTypeId=?,EmpComments=?,ReviewName=?,ReviewDate=?,Status=? where Id=?";
        try {
            
              
                JSONObject jb = new JSONObject();
      
        jb.put("event_id",eventId);        
          //  URL url = new URL("http://172.17.11.251:8080/WebRoot/resources/doJobPost/edit");
        //  URL url = new URL("http://172.17.14.226:8080/WebRoot/resources/doJobPost/edit");
       //  URL url = new URL(Properties.getProperty("WEB.REST.URL") +"doJobPost/edit");
        String serviceUrl = RestRepository.getInstance().getSrviceUrl("eventEdit");
              URL url = new URL(serviceUrl);
            URLConnection connection = url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setConnectTimeout(360*5000);
            connection.setReadTimeout(360*5000);
            OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
           out.write(jb.toString());
            out.close();

            BufferedReader in = new BufferedReader(new InputStreamReader(
                    connection.getInputStream()));
            String s = null;

            while ((s = in.readLine()) != null) {

                data = data + s;
            }

      //  JSONObject jObject = new JSONObject(data);

        // message = jObject.getString("message");
      //  System.out.println("displaymessage-->" +message);


            in.close();
            /* connection = ConnectionProvider.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(queryString);
            preparedStatement.setString(1,ajaxHandlerAction.getOverlayReviewType());
            preparedStatement.setString(2,ajaxHandlerAction.getOverlayDescription());
            
            
            
            preparedStatement.setString(3,ajaxHandlerAction.getOverlayReviewName());
            preparedStatement.setDate(4, DateUtility.getInstance().getMysqlDate(ajaxHandlerAction.getOverlayReviewDate()));
            preparedStatement.setString(5,ajaxHandlerAction.getReviewStatusOverlay());
            
            preparedStatement.setString(6,ajaxHandlerAction.getReviewId());
            isUpdated = preparedStatement.execute();*/
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return data;
      }
     public String updateEventposting(AjaxHandlerAction ajaxHandlerAction) throws Exception{
         
 String message ="";
        boolean isUpdated = false;
      
        try {
            
              
                JSONObject jb = new JSONObject();
              jb.put("event_id", String.valueOf(ajaxHandlerAction.getEventId()));
        jb.put("event_title", ajaxHandlerAction.getEventTitle());
  
        jb.put("event_description", ajaxHandlerAction.getEventDescription());
    
        jb.put("event_startdate", ajaxHandlerAction.getStartDate());
      
        jb.put("evetnt_enddate",ajaxHandlerAction.getEndDate());
        jb.put("event_time_from",ajaxHandlerAction.getStartTime());
        jb.put("event_time_to",ajaxHandlerAction.getEndTime());  
        jb.put("midday_from",ajaxHandlerAction.getMidDayFrom()); 
        
       // System.out.println("getMidDayTo-->"+ajaxHandlerAction.getMidDayTo());
        jb.put("midday_to",ajaxHandlerAction.getMidDayTo()); 
        jb.put("timezone",ajaxHandlerAction.getTimeZone()); 
        jb.put("location",ajaxHandlerAction.getEventLocation()); 
        jb.put("transport",ajaxHandlerAction.getTransportation()); 
        jb.put("createdby",ajaxHandlerAction.getCreatedBy()); 
       jb.put("status",ajaxHandlerAction.getEventStatus()); 
        jb.put("WebinarType",ajaxHandlerAction.getEventType()); 
       if(ajaxHandlerAction.getEventType().equalsIgnoreCase("I")|| ajaxHandlerAction.getEventType().equalsIgnoreCase("E")){
        jb.put("event_bold_Title",ajaxHandlerAction.getEventBoldtitle()); 
        jb.put("event_tagline",ajaxHandlerAction.getEventRegluarTitle()); 
        jb.put("OrganizerEmail",ajaxHandlerAction.getContactUsEmail()); 
        jb.put("RegistrationLink",ajaxHandlerAction.getEventRegistrationLink()); 
        
       if (!"".equals(ajaxHandlerAction.getPrimaryTrack()) && ajaxHandlerAction.getPrimaryTrack() != null) {
                jb.put("PrimaryTrack", ajaxHandlerAction.getPrimaryTrack());
            } else {
                jb.put("PrimaryTrack", "0");
            }
            if (!"".equals(ajaxHandlerAction.getSecondaryTrack()) && ajaxHandlerAction.getSecondaryTrack() != null) {
                jb.put("SecondaryTrack", ajaxHandlerAction.getSecondaryTrack());
            } else {
                jb.put("SecondaryTrack", "0");
            }
        jb.put("Department",ajaxHandlerAction.getEventDepartment());
        
        jb.put("IsAssociated",ajaxHandlerAction.getIsAssociated());
        jb.put("AsociatedEventId",ajaxHandlerAction.getAssociatedEventId());
        
        }if(ajaxHandlerAction.getEventType().equalsIgnoreCase("C")){
             jb.put("event_redirect",ajaxHandlerAction.getConferenceUrl()); 
         }
         jb.put("VideoLink",ajaxHandlerAction.getEventAfterVideoUrl());
         jb.put("SeriesId",ajaxHandlerAction.getSeriesId());
         
        // System.out.println("VideoLink-->"+ajaxHandlerAction.getEventAfterVideoUrl());
       //  System.out.println("get VideoLink-->"+jb.getString("VideoLink"));
      //  System.out.println("getConferenceUrl-->"+ajaxHandlerAction.getConferenceUrl());
         //   URL url = new URL(Properties.getProperty("WEB.REST.URL") +"doEventPost/addEvent");
         String serviceUrl = RestRepository.getInstance().getSrviceUrl("updateEvent");
              URL url = new URL(serviceUrl);
            URLConnection connection = url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setConnectTimeout(360*5000);
            connection.setReadTimeout(360*5000);
            OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
           out.write(jb.toString());
            out.close();

            BufferedReader in = new BufferedReader(new InputStreamReader(
                    connection.getInputStream()));
            String s = null;
            String data = "";
            while ((s = in.readLine()) != null) {

                data = data + s;
            }

        JSONObject jObject = new JSONObject(data);

         message = jObject.getString("message");
     


            in.close();
           
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return message;   
     } 
     
     
     
      public String addEventSpeaker(AjaxHandlerAction ajaxHandlerAction) throws Exception{
       
 String message ="";
        boolean isUpdated = false;
      
        try {
            
              
                JSONObject jb = new JSONObject();
              
        jb.put("event_id", String.valueOf(ajaxHandlerAction.getEventId()));
  
        jb.put("event_speaker", ajaxHandlerAction.getSpeakerName());
    
        jb.put("event_designation", ajaxHandlerAction.getDesignation());
      
        jb.put("primary_speaker",ajaxHandlerAction.getSpeakerType());
        jb.put("Company",ajaxHandlerAction.getCompany());
        jb.put("Status",ajaxHandlerAction.getStatus());
       
         //   URL url = new URL(Properties.getProperty("WEB.REST.URL") +"doEventPost/addEvent");
         String serviceUrl = RestRepository.getInstance().getSrviceUrl("addEventSpeaker");
              URL url = new URL(serviceUrl);
            URLConnection connection = url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setConnectTimeout(360*5000);
            connection.setReadTimeout(360*5000);
            OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
           out.write(jb.toString());
            out.close();

            BufferedReader in = new BufferedReader(new InputStreamReader(
                    connection.getInputStream()));
            String s = null;
            String data = "";
            while ((s = in.readLine()) != null) {

                data = data + s;
            }

        JSONObject jObject = new JSONObject(data);

         message = jObject.getString("message");
     


            in.close();
           
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return message;   
      }     
       public String editEventSpeaker(String speakerId) throws Exception{
            String message ="";
        boolean isUpdated = false;
        //  String queryString ="";
             // System.out.println("first call");
            String data = "";
        //queryString = "update tblEmpReview set ReviewTypeId=?,EmpComments=?,ModifiedBy=?,ModifiedDate=?,ReviewName=?,ReviewDate=?,Status=?,TLComments=?,TLRating=?,HRRating=?,HrComments=? where Id=?";
        //if(roleName.equalsIgnoreCase("Employee"))
        // queryString = "update tblEmpReview set ReviewTypeId=?,EmpComments=?,ReviewName=?,ReviewDate=?,Status=? where Id=?";
        try {
            
              
                JSONObject jb = new JSONObject();
      
        jb.put("Id",speakerId);        
          //  URL url = new URL("http://172.17.11.251:8080/WebRoot/resources/doJobPost/edit");
        //  URL url = new URL("http://172.17.14.226:8080/WebRoot/resources/doJobPost/edit");
       //  URL url = new URL(Properties.getProperty("WEB.REST.URL") +"doJobPost/edit");
        String serviceUrl = RestRepository.getInstance().getSrviceUrl("editEventSpeaker");
              URL url = new URL(serviceUrl);
            URLConnection connection = url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setConnectTimeout(360*5000);
            connection.setReadTimeout(360*5000);
            OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
           out.write(jb.toString());
            out.close();

            BufferedReader in = new BufferedReader(new InputStreamReader(
                    connection.getInputStream()));
            String s = null;

            while ((s = in.readLine()) != null) {

                data = data + s;
            }

      //  JSONObject jObject = new JSONObject(data);

        // message = jObject.getString("message");
      //  System.out.println("displaymessage-->" +message);


            in.close();
            /* connection = ConnectionProvider.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(queryString);
            preparedStatement.setString(1,ajaxHandlerAction.getOverlayReviewType());
            preparedStatement.setString(2,ajaxHandlerAction.getOverlayDescription());
            
            
            
            preparedStatement.setString(3,ajaxHandlerAction.getOverlayReviewName());
            preparedStatement.setDate(4, DateUtility.getInstance().getMysqlDate(ajaxHandlerAction.getOverlayReviewDate()));
            preparedStatement.setString(5,ajaxHandlerAction.getReviewStatusOverlay());
            
            preparedStatement.setString(6,ajaxHandlerAction.getReviewId());
            isUpdated = preparedStatement.execute();*/
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return data;
       }
       
       
        public String updateEventSpeaker(AjaxHandlerAction ajaxHandlerAction) throws Exception{
            
 String message ="";
        boolean isUpdated = false;
      
        try {
            
              
                JSONObject jb = new JSONObject();
              
        jb.put("event_id", String.valueOf(ajaxHandlerAction.getEventId()));
  
        jb.put("event_speaker", ajaxHandlerAction.getSpeakerName());
    
        jb.put("event_designation", ajaxHandlerAction.getDesignation());
      
        jb.put("primary_speaker",ajaxHandlerAction.getSpeakerType());
        jb.put("Company",ajaxHandlerAction.getCompany());
        jb.put("Status",ajaxHandlerAction.getStatus());
        jb.put("Id",ajaxHandlerAction.getSpeakerId());
       
         //   URL url = new URL(Properties.getProperty("WEB.REST.URL") +"doEventPost/addEvent");
         String serviceUrl = RestRepository.getInstance().getSrviceUrl("updateEventSpeaker");
              URL url = new URL(serviceUrl);
            URLConnection connection = url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setConnectTimeout(360*5000);
            connection.setReadTimeout(360*5000);
            OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
           out.write(jb.toString());
            out.close();

            BufferedReader in = new BufferedReader(new InputStreamReader(
                    connection.getInputStream()));
            String s = null;
            String data = "";
            while ((s = in.readLine()) != null) {

                data = data + s;
            }

        JSONObject jObject = new JSONObject(data);

         message = jObject.getString("message");
     


            in.close();
           
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return message;   
        }
       public String getInfoDetails(String infoId,String tableName) throws Exception {
               String message ="";
        boolean isUpdated = false;
        //  String queryString ="";
             // System.out.println("first call");
            String data = "";
        //queryString = "update tblEmpReview set ReviewTypeId=?,EmpComments=?,ModifiedBy=?,ModifiedDate=?,ReviewName=?,ReviewDate=?,Status=?,TLComments=?,TLRating=?,HRRating=?,HrComments=? where Id=?";
        //if(roleName.equalsIgnoreCase("Employee"))
        // queryString = "update tblEmpReview set ReviewTypeId=?,EmpComments=?,ReviewName=?,ReviewDate=?,Status=? where Id=?";
        try {
            
              
                JSONObject jb = new JSONObject();
      
        jb.put("Id",infoId);   
        jb.put("tableName",tableName);   
          //  URL url = new URL("http://172.17.11.251:8080/WebRoot/resources/doJobPost/edit");
        //  URL url = new URL("http://172.17.14.226:8080/WebRoot/resources/doJobPost/edit");
       //  URL url = new URL(Properties.getProperty("WEB.REST.URL") +"doJobPost/edit");
        String serviceUrl = RestRepository.getInstance().getSrviceUrl("getInfoDetails");
              URL url = new URL(serviceUrl);
            URLConnection connection = url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setConnectTimeout(360*5000);
            connection.setReadTimeout(360*5000);
            OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
           out.write(jb.toString());
            out.close();

            BufferedReader in = new BufferedReader(new InputStreamReader(
                    connection.getInputStream()));
            String s = null;

            while ((s = in.readLine()) != null) {

                data = data + s;
            }

      //  JSONObject jObject = new JSONObject(data);

        // message = jObject.getString("message");
      //  System.out.println("displaymessage-->" +message);


            in.close();
            /* connection = ConnectionProvider.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(queryString);
            preparedStatement.setString(1,ajaxHandlerAction.getOverlayReviewType());
            preparedStatement.setString(2,ajaxHandlerAction.getOverlayDescription());
            
            
            
            preparedStatement.setString(3,ajaxHandlerAction.getOverlayReviewName());
            preparedStatement.setDate(4, DateUtility.getInstance().getMysqlDate(ajaxHandlerAction.getOverlayReviewDate()));
            preparedStatement.setString(5,ajaxHandlerAction.getReviewStatusOverlay());
            
            preparedStatement.setString(6,ajaxHandlerAction.getReviewId());
            isUpdated = preparedStatement.execute();*/
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return data;
       } 
       
       public String updateAfterEvent(AjaxHandlerAction ajaxHandlerAction) throws Exception{
            
        String message ="";
        boolean isUpdated = false;
      
        try {
            
              
                JSONObject jb = new JSONObject();
              
        jb.put("eventId", String.valueOf(ajaxHandlerAction.getEventId()));
  
        jb.put("eventAfterDescription", ajaxHandlerAction.getEventAfterDescription());
    
        jb.put("eventAfterVideoUrl", ajaxHandlerAction.getEventAfterVideoUrl());
      
        jb.put("eventStatus",ajaxHandlerAction.getEventStatus());
        jb.put("primaryTrack",ajaxHandlerAction.getPrimaryTrack());
        jb.put("secondaryTrack",ajaxHandlerAction.getSecondaryTrack());
        jb.put("eventType",ajaxHandlerAction.getEventType());
       //eventType
       
         //   URL url = new URL(Properties.getProperty("WEB.REST.URL") +"doEventPost/addEvent");
         String serviceUrl = RestRepository.getInstance().getSrviceUrl("updateAfterEvent");
              URL url = new URL(serviceUrl);
            URLConnection connection = url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setConnectTimeout(360*5000);
            connection.setReadTimeout(360*5000);
            OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
           out.write(jb.toString());
            out.close();

            BufferedReader in = new BufferedReader(new InputStreamReader(
                    connection.getInputStream()));
            String s = null;
            String data = "";
            while ((s = in.readLine()) != null) {

                data = data + s;
            }

        JSONObject jObject = new JSONObject(data);

         message = jObject.getString("message");
     


            in.close();
           
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return message;   
        }  
       
       
       public String getQmeetMap(String year) throws Exception {
            Map qmeetMap = new HashMap();
            StringBuffer qmeetXml = new StringBuffer();
           try {
               
                JSONObject jb = new JSONObject();
                 
              // System.out.println("year-->"+marketingAction.getQmeetYear());
                  jb.put("year", year);
               
              String serviceUrl = RestRepository.getInstance().getSrviceUrl("eventQmeetMap");
              URL url = new URL(serviceUrl);
                URLConnection connection = url.openConnection();
                connection.setDoOutput(true);
                connection.setRequestProperty("Content-Type", "application/json");
                connection.setConnectTimeout(360*5000);
                connection.setReadTimeout(360*5000);
                OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
                out.write(jb.toString());
                out.close();
 
                BufferedReader in = new BufferedReader(new InputStreamReader(
                        connection.getInputStream()));
String s=null;
String data = "";
                while ((s=in.readLine()) != null) {
               //     System.out.println(s);
                    data =data+s;
                }
                
                
                
            //   System.out.println("Data-->"+data);
               JSONObject jObject  = new JSONObject(data);
            
                 
                 /***********************/
                qmeetXml.append("<xml version=\"1.0\">");
            qmeetXml.append("<QMEETS>");
            qmeetXml.append("<QMEET qmeetId=\"\">--Please Select--</QMEET>");
                 Iterator<String> keysItr = jObject.keys();
    while(keysItr.hasNext()) {
         String key = keysItr.next();
        String value = (String)jObject.get(key);
        qmeetMap.put(key, value);
         //  System.out.println("Id-->"+key+"-->"+value);
           qmeetXml.append("<QMEET qmeetId=\"" + key + "\">");
           if (value.contains("&")) {
                    value = value.replace("&", "&amp;");
                }
                qmeetXml.append(value);
                qmeetXml.append("</QMEET>");
           
    }
             qmeetXml.append("</QMEETS>");
            qmeetXml.append("</xml>");    
                 
                 /*************************/
                 
             
                in.close();
            } catch (Exception e) {
                System.out.println("\nError while calling REST Service");
                System.out.println(e);
            }
         //  System.out.println("qmeetXml-->"+qmeetXml.toString());
  return qmeetXml.toString();
       }
       
       
    /*
       public String getEventSeries(String eventId,String eventType) throws Exception {
            Map qmeetMap = new HashMap();
            StringBuffer qmeetXml = new StringBuffer();
           try {
               
                JSONObject jb = new JSONObject();
                 
              // System.out.println("year-->"+marketingAction.getQmeetYear());
                  jb.put("eventId", eventId);
                  jb.put("eventType", eventType);
               
              String serviceUrl = RestRepository.getInstance().getSrviceUrl("eventSeriesMap");
              URL url = new URL(serviceUrl);
                URLConnection connection = url.openConnection();
                connection.setDoOutput(true);
                connection.setRequestProperty("Content-Type", "application/json");
                connection.setConnectTimeout(5000);
                connection.setReadTimeout(5000);
                OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
                out.write(jb.toString());
                out.close();
 
                BufferedReader in = new BufferedReader(new InputStreamReader(
                        connection.getInputStream()));
String s=null;
String data = "";
                while ((s=in.readLine()) != null) {
               //     System.out.println(s);
                    data =data+s;
                }
                
                
                
               System.out.println("Data-->"+data);
               JSONObject jObject  = new JSONObject(data);
            
                 
                 
                qmeetXml.append("<xml version=\"1.0\">");
            qmeetXml.append("<EVENTS>");
            qmeetXml.append("<EVENT eventId=\"\">--Please Select--</EVENT>");
                 Iterator<String> keysItr = jObject.keys();
    while(keysItr.hasNext()) {
         String key = keysItr.next();
        String value = (String)jObject.get(key);
        qmeetMap.put(key, value);
           System.out.println("Id-->"+key+"-->"+value);
           qmeetXml.append("<EVENT eventId=\"" + key + "\">");
           if (value.contains("&")) {
                    value = value.replace("&", "&amp;");
                }if (value.contains("<")) {
                    value = value.replace("<", "&lt;");
                }if (value.contains(">")) {
                    value = value.replace(">", "&gt;");
                }
                qmeetXml.append(value);
                qmeetXml.append("</EVENT>");
           
    }
             qmeetXml.append("</EVENTS>");
            qmeetXml.append("</xml>");    
                 
               
                 
             
                in.close();
            } catch (Exception e) {
                System.out.println("\nError while calling REST Service");
                System.out.println(e);
            }
           System.out.println("qmeetXml-->"+qmeetXml.toString());
  return qmeetXml.toString();
       }
       */
       
public String doCreateWebinarSeries(AjaxHandlerAction ajaxHandlerAction) throws Exception {
       
 String message ="";
        boolean isUpdated = false;
      
        try {
            
              
                JSONObject jb = new JSONObject();
              
        jb.put("SeriesTitle", ajaxHandlerAction.getSeriesTitle());
  
        jb.put("SeriesType", ajaxHandlerAction.getSeriesType());
    
      //  jb.put("SeriesTrack", ajaxHandlerAction.getSeriesTrack());
        if (!"".equals(ajaxHandlerAction.getSeriesTrack()) && ajaxHandlerAction.getSeriesTrack() != null) {
                jb.put("SeriesTrack", ajaxHandlerAction.getSeriesTrack());
            } else {
                jb.put("SeriesTrack", "0");
            }
      
        jb.put("SeriesStatus",ajaxHandlerAction.getSeriesStatus());
       
       jb.put("CreatedBy",ajaxHandlerAction.getCreatedBy());
         //   URL url = new URL(Properties.getProperty("WEB.REST.URL") +"doEventPost/addEvent");
         String serviceUrl = RestRepository.getInstance().getSrviceUrl("createWebinarSeries");
              URL url = new URL(serviceUrl);
            URLConnection connection = url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setConnectTimeout(360*5000);
            connection.setReadTimeout(360*5000);
            OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
           out.write(jb.toString());
            out.close();

            BufferedReader in = new BufferedReader(new InputStreamReader(
                    connection.getInputStream()));
            String s = null;
            String data = "";
            while ((s = in.readLine()) != null) {

                data = data + s;
            }

        JSONObject jObject = new JSONObject(data);

         message = jObject.getString("message");
     


            in.close();
           
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return message;   
}


public String getSeriesDetails(String seriesId) throws Exception{
            String message ="";
        boolean isUpdated = false;
        //  String queryString ="";
             // System.out.println("first call");
            String data = "";
        //queryString = "update tblEmpReview set ReviewTypeId=?,EmpComments=?,ModifiedBy=?,ModifiedDate=?,ReviewName=?,ReviewDate=?,Status=?,TLComments=?,TLRating=?,HRRating=?,HrComments=? where Id=?";
        //if(roleName.equalsIgnoreCase("Employee"))
        // queryString = "update tblEmpReview set ReviewTypeId=?,EmpComments=?,ReviewName=?,ReviewDate=?,Status=? where Id=?";
        try {
            
              
                JSONObject jb = new JSONObject();
      
        jb.put("Id",seriesId);        
          //  URL url = new URL("http://172.17.11.251:8080/WebRoot/resources/doJobPost/edit");
        //  URL url = new URL("http://172.17.14.226:8080/WebRoot/resources/doJobPost/edit");
       //  URL url = new URL(Properties.getProperty("WEB.REST.URL") +"doJobPost/edit");
        String serviceUrl = RestRepository.getInstance().getSrviceUrl("getSeriesDetails");
              URL url = new URL(serviceUrl);
            URLConnection connection = url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setConnectTimeout(360*5000);
            connection.setReadTimeout(360*5000);
            OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
           out.write(jb.toString());
            out.close();

            BufferedReader in = new BufferedReader(new InputStreamReader(
                    connection.getInputStream()));
            String s = null;

            while ((s = in.readLine()) != null) {

                data = data + s;
            }

      //  JSONObject jObject = new JSONObject(data);

        // message = jObject.getString("message");
      //  System.out.println("displaymessage-->" +message);


            in.close();
            /* connection = ConnectionProvider.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(queryString);
            preparedStatement.setString(1,ajaxHandlerAction.getOverlayReviewType());
            preparedStatement.setString(2,ajaxHandlerAction.getOverlayDescription());
            
            
            
            preparedStatement.setString(3,ajaxHandlerAction.getOverlayReviewName());
            preparedStatement.setDate(4, DateUtility.getInstance().getMysqlDate(ajaxHandlerAction.getOverlayReviewDate()));
            preparedStatement.setString(5,ajaxHandlerAction.getReviewStatusOverlay());
            
            preparedStatement.setString(6,ajaxHandlerAction.getReviewId());
            isUpdated = preparedStatement.execute();*/
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return data;
       }


public String doUpdateWebinarSeries(AjaxHandlerAction ajaxHandlerAction) throws Exception{
    String message ="";
        boolean isUpdated = false;
      
        try {
            
              
                JSONObject jb = new JSONObject();
              jb.put("Id", ajaxHandlerAction.getSeriesId());
        jb.put("SeriesTitle", ajaxHandlerAction.getSeriesTitle());
  
        jb.put("SeriesType", ajaxHandlerAction.getSeriesType());
    
   //     jb.put("SeriesTrack", ajaxHandlerAction.getSeriesTrack());
         if (!"".equals(ajaxHandlerAction.getSeriesTrack()) && ajaxHandlerAction.getSeriesTrack() != null) {
                jb.put("SeriesTrack", ajaxHandlerAction.getSeriesTrack());
            } else {
                jb.put("SeriesTrack", "0");
            }
      
        jb.put("SeriesStatus",ajaxHandlerAction.getSeriesStatus());
       
       jb.put("CreatedBy",ajaxHandlerAction.getCreatedBy());
         //   URL url = new URL(Properties.getProperty("WEB.REST.URL") +"doEventPost/addEvent");
         String serviceUrl = RestRepository.getInstance().getSrviceUrl("updateWebinarSeries");
              URL url = new URL(serviceUrl);
            URLConnection connection = url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setConnectTimeout(360*5000);
            connection.setReadTimeout(360*5000);
            OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
           out.write(jb.toString());
            out.close();

            BufferedReader in = new BufferedReader(new InputStreamReader(
                    connection.getInputStream()));
            String s = null;
            String data = "";
            while ((s = in.readLine()) != null) {

                data = data + s;
            }

        JSONObject jObject = new JSONObject(data);

         message = jObject.getString("message");
     


            in.close();
           
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return message;   
}     


public String addTrackName(AjaxHandlerAction ajaxHandlerAction) throws Exception{
       
 String message ="";
        boolean isUpdated = false;
      
        try {
            
              
                JSONObject jb = new JSONObject();
              
        jb.put("trackName", String.valueOf(ajaxHandlerAction.getTrackName()));
      //  System.out.println("addTrackNameAHWSI"+ajaxHandlerAction.getTrackName());
  
        
       
         //   URL url = new URL(Properties.getProperty("WEB.REST.URL") +"doEventPost/addEvent");
         String serviceUrl = RestRepository.getInstance().getSrviceUrl("addTrackName");
              URL url = new URL(serviceUrl);
            URLConnection connection = url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setConnectTimeout(360*5000);
            connection.setReadTimeout(360*5000);
            OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
           out.write(jb.toString());
            out.close();

            BufferedReader in = new BufferedReader(new InputStreamReader(
                    connection.getInputStream()));
            String s = null;
            String data = "";
            while ((s = in.readLine()) != null) {

                data = data + s;
            }

        JSONObject jObject = new JSONObject(data);

         message = jObject.getString("message");
     


            in.close();
           
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return message;   
      }

public String editTrackName(AjaxHandlerAction ajaxHandlerAction) throws Exception{
       
 String message ="";
        boolean isUpdated = false;
      
        try {
            
              
                JSONObject jb = new JSONObject();
              
        jb.put("trackName", ajaxHandlerAction.getTrackName());
        jb.put("trackId", String.valueOf(ajaxHandlerAction.getTrackId()));
        jb.put("createdBy", ajaxHandlerAction.getCreatedBy());
     //   System.out.println("addTrackNameAHWSI"+ajaxHandlerAction.getTrackId());
  
        
       
         //   URL url = new URL(Properties.getProperty("WEB.REST.URL") +"doEventPost/addEvent");
         String serviceUrl = RestRepository.getInstance().getSrviceUrl("updateTrackName");
              URL url = new URL(serviceUrl);
            URLConnection connection = url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setConnectTimeout(360*5000);
            connection.setReadTimeout(360*5000);
            OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
           out.write(jb.toString());
            out.close();

            BufferedReader in = new BufferedReader(new InputStreamReader(
                    connection.getInputStream()));
            String s = null;
            String data = "";
            while ((s = in.readLine()) != null) {

                data = data + s;
            }

        JSONObject jObject = new JSONObject(data);

         message = jObject.getString("message");
     


            in.close();
           
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return message;   
      }
	
/*Method : People Add
 * Author : Santosh Kola
 * Date : 08/05/2015 
 */
 /*public String doAddPeople(AjaxHandlerAction ajaxHandlerAction) throws Exception{
       
 String message ="";
        boolean isUpdated = false;
      
        try {
            
              
                JSONObject jb = new JSONObject();
              
        jb.put("Name", String.valueOf(ajaxHandlerAction.getPeopleName()));
  
        jb.put("Designation", ajaxHandlerAction.getDesignation());
    
        jb.put("Organization", ajaxHandlerAction.getOrganization());
      
        jb.put("EmailId",ajaxHandlerAction.getEmail());
        jb.put("Status",ajaxHandlerAction.getStatus());
        jb.put("CreatedBy",ajaxHandlerAction.getCreatedBy());
       
         //   URL url = new URL(Properties.getProperty("WEB.REST.URL") +"doEventPost/addEvent");
         String serviceUrl = RestRepository.getInstance().getSrviceUrl("addPeople");
              URL url = new URL(serviceUrl);
            URLConnection connection = url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setConnectTimeout(360*5000);
            connection.setReadTimeout(360*5000);
            OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
           out.write(jb.toString());
            out.close();

            BufferedReader in = new BufferedReader(new InputStreamReader(
                    connection.getInputStream()));
            String s = null;
            String data = "";
            while ((s = in.readLine()) != null) {

                data = data + s;
            }

        JSONObject jObject = new JSONObject(data);

         message = jObject.getString("message");
     


            in.close();
           
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return message;   
      }     */

public String doAddPeople(AjaxHandlerAction ajaxHandlerAction) throws Exception{
    
	 String message ="";
	        boolean isUpdated = false;
	        //System.out.println("doAddPeople impl");
	        try {
	        	// System.out.println("doAddPeople trtyyy");
	        	JSONObject jb1 = new JSONObject(ajaxHandlerAction.getDoAddPeopleJson());
	              
	        	  JSONObject jb = new JSONObject();
	              
	              jb.put("Name", jb1.getString("peopleName"));
	        
	              jb.put("Designation", jb1.getString("designation"));
	             // System.out.println("Designation"+jb1.getString("designation"));
	              jb.put("Organization", jb1.getString("organization"));
	            
	              jb.put("EmailId",jb1.getString("emailId"));
	              jb.put("Status",jb1.getString("status"));
	              jb.put("CreatedBy",ajaxHandlerAction.getCreatedBy());
	              
	              
	              
	              
	       
	         //   URL url = new URL(Properties.getProperty("WEB.REST.URL") +"doEventPost/addEvent");
	         String serviceUrl = RestRepository.getInstance().getSrviceUrl("addPeople");
	              URL url = new URL(serviceUrl);
	            URLConnection connection = url.openConnection();
	            connection.setDoOutput(true);
	            connection.setRequestProperty("Content-Type", "application/json");
	            connection.setConnectTimeout(360*5000);
	            connection.setReadTimeout(360*5000);
	            OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
	           out.write(jb.toString());
	            out.close();

	            BufferedReader in = new BufferedReader(new InputStreamReader(
	                    connection.getInputStream()));
	            String s = null;
	            String data = "";
	            while ((s = in.readLine()) != null) {

	                data = data + s;
	            }

	        JSONObject jObject = new JSONObject(data);

	         message = jObject.getString("message");
	     


	            in.close();
	           
	        } catch (IOException ex) {
	            ex.printStackTrace();
	        }
	        return message;   
	      }  
		  



public String getPeopleDetails(String peopleId) throws Exception{
     String message ="";
        boolean isUpdated = false;
        //  String queryString ="";
             // System.out.println("first call");
            String data = "";
        //queryString = "update tblEmpReview set ReviewTypeId=?,EmpComments=?,ModifiedBy=?,ModifiedDate=?,ReviewName=?,ReviewDate=?,Status=?,TLComments=?,TLRating=?,HRRating=?,HrComments=? where Id=?";
        //if(roleName.equalsIgnoreCase("Employee"))
        // queryString = "update tblEmpReview set ReviewTypeId=?,EmpComments=?,ReviewName=?,ReviewDate=?,Status=? where Id=?";
        try {
            
              
                JSONObject jb = new JSONObject();
      
        jb.put("Id",peopleId);        
          //  URL url = new URL("http://172.17.11.251:8080/WebRoot/resources/doJobPost/edit");
        //  URL url = new URL("http://172.17.14.226:8080/WebRoot/resources/doJobPost/edit");
       //  URL url = new URL(Properties.getProperty("WEB.REST.URL") +"doJobPost/edit");
        String serviceUrl = RestRepository.getInstance().getSrviceUrl("getPeopleDetails");
              URL url = new URL(serviceUrl);
            URLConnection connection = url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setConnectTimeout(360*5000);
            connection.setReadTimeout(360*5000);
            OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
           out.write(jb.toString());
            out.close();

            BufferedReader in = new BufferedReader(new InputStreamReader(
                    connection.getInputStream()));
            String s = null;

            while ((s = in.readLine()) != null) {

                data = data + s;
            }

      //  JSONObject jObject = new JSONObject(data);

        // message = jObject.getString("message");
      //  System.out.println("displaymessage-->" +message);


            in.close();
            /* connection = ConnectionProvider.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(queryString);
            preparedStatement.setString(1,ajaxHandlerAction.getOverlayReviewType());
            preparedStatement.setString(2,ajaxHandlerAction.getOverlayDescription());
            
            
            
            preparedStatement.setString(3,ajaxHandlerAction.getOverlayReviewName());
            preparedStatement.setDate(4, DateUtility.getInstance().getMysqlDate(ajaxHandlerAction.getOverlayReviewDate()));
            preparedStatement.setString(5,ajaxHandlerAction.getReviewStatusOverlay());
            
            preparedStatement.setString(6,ajaxHandlerAction.getReviewId());
            isUpdated = preparedStatement.execute();*/
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return data;
}




/*public String doUpdatePeopleDetails(AjaxHandlerAction ajaxHandlerAction) throws Exception{
    String message ="";
        boolean isUpdated = false;
      
        try {
            
              
                JSONObject jb = new JSONObject();
              jb.put("Id", String.valueOf(ajaxHandlerAction.getId()));
        jb.put("Name", String.valueOf(ajaxHandlerAction.getPeopleName()));
  
        jb.put("Designation", ajaxHandlerAction.getDesignation());
    
        jb.put("Organization", ajaxHandlerAction.getOrganization());
      
        jb.put("EmailId",ajaxHandlerAction.getEmail());
        jb.put("Status",ajaxHandlerAction.getStatus());
        jb.put("CreatedBy",ajaxHandlerAction.getCreatedBy());
         //   URL url = new URL(Properties.getProperty("WEB.REST.URL") +"doEventPost/addEvent");
         String serviceUrl = RestRepository.getInstance().getSrviceUrl("updatePeopleDetails");
              URL url = new URL(serviceUrl);
            URLConnection connection = url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setConnectTimeout(360*5000);
            connection.setReadTimeout(360*5000);
            OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
           out.write(jb.toString());
            out.close();

            BufferedReader in = new BufferedReader(new InputStreamReader(
                    connection.getInputStream()));
            String s = null;
            String data = "";
            while ((s = in.readLine()) != null) {

                data = data + s;
            }

        JSONObject jObject = new JSONObject(data);

         message = jObject.getString("message");
     


            in.close();
           
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return message;   
}
  	*/



public String doUpdatePeopleDetails(AjaxHandlerAction ajaxHandlerAction) throws Exception{
    String message ="";
        boolean isUpdated = false;
      
        try {
            
        	
        	
        	
        	JSONObject jb1 = new JSONObject(ajaxHandlerAction.getDoUpdatePeopleJson());
            
      	  JSONObject jb = new JSONObject();
            
      	  
      	   jb.put("Id", jb1.getString("Id"));
           jb.put("Name", jb1.getString("peopleName"));
      
            jb.put("Designation", jb1.getString("designation"));
            //System.out.println("Designation"+jb1.getString("designation"));
            jb.put("Organization", jb1.getString("organization"));
          
            jb.put("EmailId",jb1.getString("emailId"));
            jb.put("Status",jb1.getString("status"));
            jb.put("CreatedBy",ajaxHandlerAction.getCreatedBy());
            
        	
        	
              
         
         //   URL url = new URL(Properties.getProperty("WEB.REST.URL") +"doEventPost/addEvent");
         String serviceUrl = RestRepository.getInstance().getSrviceUrl("updatePeopleDetails");
              URL url = new URL(serviceUrl);
            URLConnection connection = url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setConnectTimeout(360*5000);
            connection.setReadTimeout(360*5000);
            OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
           out.write(jb.toString());
            out.close();

            BufferedReader in = new BufferedReader(new InputStreamReader(
                    connection.getInputStream()));
            String s = null;
            String data = "";
            while ((s = in.readLine()) != null) {

                data = data + s;
            }

        JSONObject jObject = new JSONObject(data);

         message = jObject.getString("message");
     


            in.close();
           
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return message;   
}

public String doLibraryTitleCheck(AjaxHandlerAction ajaxHandlerAction) throws Exception{
       
 String message ="";
        boolean isUpdated = false;
      
        try {
            
              
                JSONObject jb = new JSONObject();
              
        jb.put("ResourceTitle", String.valueOf(ajaxHandlerAction.getResourceTitle()));
        //System.out.println("getResourceTitle "+ajaxHandlerAction.getResourceTitle());
  
        
       
         //   URL url = new URL(Properties.getProperty("WEB.REST.URL") +"doEventPost/addEvent");
         String serviceUrl = RestRepository.getInstance().getSrviceUrl("libraryTitleCheck");
              URL url = new URL(serviceUrl);
            URLConnection connection = url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setConnectTimeout(360*5000);
            connection.setReadTimeout(360*5000);
            OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
           out.write(jb.toString());
            out.close();

            BufferedReader in = new BufferedReader(new InputStreamReader(
                    connection.getInputStream()));
            String s = null;
            String data = "";
            while ((s = in.readLine()) != null) {

                data = data + s;
            }

        JSONObject jObject = new JSONObject(data);

         message = jObject.getString("message");
     


            in.close();
           
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return message;   
      }

  

  public String doAddQuestionnaire(AjaxHandlerAction ajaxHandlerAction) throws ServiceLocatorException{
           
 String message ="";
    
      
        try {
                JSONObject jb = new JSONObject();
       
        jb.put("Query", ajaxHandlerAction.getQuestionTitle());
        jb.put("OptionType", ajaxHandlerAction.getOptionType());
        jb.put("SurveyId", ajaxHandlerAction.getSurveyId());
        jb.put("Sequence", ajaxHandlerAction.getQuerySequence());
         jb.put("PlaceHolder", ajaxHandlerAction.getPlaceHolder());
jb.put("questionStatus",ajaxHandlerAction.getQuestionStatus());
        if(ajaxHandlerAction.isIsRequired()){
            jb.put("IsRequired", 1);
        }else {
            jb.put("IsRequired", 0);
        }
        //CreatedBy
        jb.put("CreatedBy", ajaxHandlerAction.getCreatedBy());
        jb.put("Sequence", ajaxHandlerAction.getQuerySequence());
        if("Checkbox".equals(ajaxHandlerAction.getOptionType())||"RadioButton".equals(ajaxHandlerAction.getOptionType())){
            
            jb.put("OptionLabel1", ajaxHandlerAction.getOptionLabel1());
            jb.put("OptionSequence1", ajaxHandlerAction.getOptionSequence1());
             jb.put("OptionCount", ajaxHandlerAction.getOptionCount());
            
            if(ajaxHandlerAction.getOptionCount()>1){
                 jb.put("OptionLabel2", ajaxHandlerAction.getOptionLabel2());
            jb.put("OptionSequence2", ajaxHandlerAction.getOptionSequence2());
            } if(ajaxHandlerAction.getOptionCount()>2){
                 jb.put("OptionLabel3", ajaxHandlerAction.getOptionLabel3());
            jb.put("OptionSequence3", ajaxHandlerAction.getOptionSequence3());
            }if(ajaxHandlerAction.getOptionCount()>3){
                 jb.put("OptionLabel4", ajaxHandlerAction.getOptionLabel4());
            jb.put("OptionSequence4", ajaxHandlerAction.getOptionSequence4());
            }if(ajaxHandlerAction.getOptionCount()>4){
                 jb.put("OptionLabel5", ajaxHandlerAction.getOptionLabel5());
            jb.put("OptionSequence5", ajaxHandlerAction.getOptionSequence5());
            }if(ajaxHandlerAction.getOptionCount()>5){
                 jb.put("OptionLabel6", ajaxHandlerAction.getOptionLabel6());
            jb.put("OptionSequence6", ajaxHandlerAction.getOptionSequence6());
            }
        }
        else if("DropDown".equals(ajaxHandlerAction.getOptionType()))
        {
            jb.put("dropdownOptions", ajaxHandlerAction.getDropdownOptions());
        }
         if(ajaxHandlerAction.getAllowDocuments())
         jb.put("AllowDocuments", 1);
        else
            jb.put("AllowDocuments", 0);
        if(ajaxHandlerAction.getAllowPictures())
            jb.put("AllowPictures", 1);
        else
            jb.put("AllowPictures", 0);
     
         String serviceUrl = RestRepository.getInstance().getSrviceUrl("doAddQuestionnaire");
              URL url = new URL(serviceUrl);
            URLConnection connection = url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setConnectTimeout(360*5000);
            connection.setReadTimeout(360*5000);
            OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
           out.write(jb.toString());
            out.close();

            BufferedReader in = new BufferedReader(new InputStreamReader(
                    connection.getInputStream()));
            String s = null;
            String data = "";
            while ((s = in.readLine()) != null) {

                data = data + s;
            }

        JSONObject jObject = new JSONObject(data);

         message = jObject.getString("message");
     


            in.close();
           
        } catch (IOException ex) {
            ex.printStackTrace();
        }catch(Exception e){
             throw new ServiceLocatorException(e);
        }
        return message;   
     }
  
  public String editQuestionnaireDetails(AjaxHandlerAction ajaxHandlerAction) throws ServiceLocatorException{
     String message ="";
        boolean isUpdated = false;
    
            String data = "";
       
        try {
            
              
                JSONObject jb = new JSONObject();
      
        jb.put("SurveyId", ajaxHandlerAction.getSurveyId());
        jb.put("QuestionId", ajaxHandlerAction.getQuestionId());  
        
        String serviceUrl = RestRepository.getInstance().getSrviceUrl("editQuestionnaireDetails");
              URL url = new URL(serviceUrl);
            URLConnection connection = url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setConnectTimeout(360*5000);
            connection.setReadTimeout(360*5000);
            OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
           out.write(jb.toString());
            out.close();

            BufferedReader in = new BufferedReader(new InputStreamReader(
                    connection.getInputStream()));
            String s = null;

            while ((s = in.readLine()) != null) {

                data = data + s;
            }

      //  JSONObject jObject = new JSONObject(data);

        // message = jObject.getString("message");
      //  System.out.println("displaymessage-->" +message);


            in.close();
            /* connection = ConnectionProvider.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(queryString);
            preparedStatement.setString(1,ajaxHandlerAction.getOverlayReviewType());
            preparedStatement.setString(2,ajaxHandlerAction.getOverlayDescription());
            
            
            
            preparedStatement.setString(3,ajaxHandlerAction.getOverlayReviewName());
            preparedStatement.setDate(4, DateUtility.getInstance().getMysqlDate(ajaxHandlerAction.getOverlayReviewDate()));
            preparedStatement.setString(5,ajaxHandlerAction.getReviewStatusOverlay());
            
            preparedStatement.setString(6,ajaxHandlerAction.getReviewId());
            isUpdated = preparedStatement.execute();*/
        } catch (IOException ex) {
            ex.printStackTrace();
        }catch(Exception e){
             throw new ServiceLocatorException(e);
        }
        return data;
}


 public String doUpdateQuestionnaire(AjaxHandlerAction ajaxHandlerAction) throws ServiceLocatorException{
           
 String message ="";
    
      
        try {
                JSONObject jb = new JSONObject();
        jb.put("QuestionId", ajaxHandlerAction.getQuestionId());
        jb.put("Query", ajaxHandlerAction.getQuestionTitle());
        jb.put("OptionType", ajaxHandlerAction.getOptionType());
        jb.put("SurveyId", ajaxHandlerAction.getSurveyId());
        jb.put("Sequence", ajaxHandlerAction.getQuerySequence());
        jb.put("QuestionStatus", ajaxHandlerAction.getQuestionStatus());
        jb.put("PlaceHolder", ajaxHandlerAction.getPlaceHolder());
         if(ajaxHandlerAction.isIsRequired()){
            jb.put("IsRequired", 1);
        }else {
            jb.put("IsRequired", 0);
        }
         jb.put("CreatedBy", ajaxHandlerAction.getCreatedBy());
        if("Checkbox".equals(ajaxHandlerAction.getOptionType())||"RadioButton".equals(ajaxHandlerAction.getOptionType())){
            jb.put("OptionLabel1", ajaxHandlerAction.getOptionLabel1());
            jb.put("OptionSequence1", ajaxHandlerAction.getOptionSequence1());
             jb.put("OptionCount", ajaxHandlerAction.getOptionCount());
            
            if(ajaxHandlerAction.getOptionCount()>1){
                 jb.put("OptionLabel2", ajaxHandlerAction.getOptionLabel2());
            jb.put("OptionSequence2", ajaxHandlerAction.getOptionSequence2());
            } if(ajaxHandlerAction.getOptionCount()>2){
                 jb.put("OptionLabel3", ajaxHandlerAction.getOptionLabel3());
            jb.put("OptionSequence3", ajaxHandlerAction.getOptionSequence3());
            }if(ajaxHandlerAction.getOptionCount()>3){
                 jb.put("OptionLabel4", ajaxHandlerAction.getOptionLabel4());
            jb.put("OptionSequence4", ajaxHandlerAction.getOptionSequence4());
            }if(ajaxHandlerAction.getOptionCount()>4){
                 jb.put("OptionLabel5", ajaxHandlerAction.getOptionLabel5());
            jb.put("OptionSequence5", ajaxHandlerAction.getOptionSequence5());
            }if(ajaxHandlerAction.getOptionCount()>5){
                 jb.put("OptionLabel6", ajaxHandlerAction.getOptionLabel6());
            jb.put("OptionSequence6", ajaxHandlerAction.getOptionSequence6());
            }
        }
         else if("DropDown".equals(ajaxHandlerAction.getOptionType()))
        {
            jb.put("dropdownOptions", ajaxHandlerAction.getDropdownOptions());
        }
      if(ajaxHandlerAction.getAllowDocuments())
         jb.put("AllowDocuments", 1);
        else
            jb.put("AllowDocuments", 0);
        if(ajaxHandlerAction.getAllowPictures())
            jb.put("AllowPictures", 1);
        else
            jb.put("AllowPictures", 0);
         String serviceUrl = RestRepository.getInstance().getSrviceUrl("doUpdateQuestionnaire");
              URL url = new URL(serviceUrl);
            URLConnection connection = url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setConnectTimeout(360*5000);
            connection.setReadTimeout(360*5000);
            OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
           out.write(jb.toString());
            out.close();

            BufferedReader in = new BufferedReader(new InputStreamReader(
                    connection.getInputStream()));
            String s = null;
            String data = "";
            while ((s = in.readLine()) != null) {

                data = data + s;
            }

        JSONObject jObject = new JSONObject(data);

         message = jObject.getString("message");
     


            in.close();
           
        } catch (IOException ex) {
            ex.printStackTrace();
        }catch(Exception e){
             throw new ServiceLocatorException(e);
        }
        return message;   
     }

  public String showReviewDetails(int surveyInfoId) throws Exception{
          String message ="";
        boolean isUpdated = false;
        //  String queryString ="";
             // System.out.println("first call");
            String data = "";
        //queryString = "update tblEmpReview set ReviewTypeId=?,EmpComments=?,ModifiedBy=?,ModifiedDate=?,ReviewName=?,ReviewDate=?,Status=?,TLComments=?,TLRating=?,HRRating=?,HrComments=? where Id=?";
        //if(roleName.equalsIgnoreCase("Employee"))
        // queryString = "update tblEmpReview set ReviewTypeId=?,EmpComments=?,ReviewName=?,ReviewDate=?,Status=? where Id=?";
        try {
            
             JSONObject jb = new JSONObject();
    
        
          jb.put("surveyInfoId", surveyInfoId);
      String serviceUrl = RestRepository.getInstance().getSrviceUrl("getSurveyInfoDetails");
              URL url = new URL(serviceUrl);
            URLConnection connection = url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setConnectTimeout(360*5000);
            connection.setReadTimeout(360*5000);
            OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
           out.write(jb.toString());
            out.close();

            BufferedReader in = new BufferedReader(new InputStreamReader(
                    connection.getInputStream()));
            String s = null;
            while ((s = in.readLine()) != null) {

                data = data + s;
            }
            
            
          //  System.out.println("data->"+data);
          

      //  JSONObject jObject = new JSONObject(data);

        // message = jObject.getString("message");
      //  System.out.println("displaymessage-->" +message);


            in.close();
            /* connection = ConnectionProvider.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(queryString);
            preparedStatement.setString(1,ajaxHandlerAction.getOverlayReviewType());
            preparedStatement.setString(2,ajaxHandlerAction.getOverlayDescription());
            
            
            
            preparedStatement.setString(3,ajaxHandlerAction.getOverlayReviewName());
            preparedStatement.setDate(4, DateUtility.getInstance().getMysqlDate(ajaxHandlerAction.getOverlayReviewDate()));
            preparedStatement.setString(5,ajaxHandlerAction.getReviewStatusOverlay());
            
            preparedStatement.setString(6,ajaxHandlerAction.getReviewId());
            isUpdated = preparedStatement.execute();*/
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return data;
      }
 public String getSearchQuestionInfo(int questionId,List questionList ) throws ServiceLocatorException{
          String message ="";
        boolean isUpdated = false;
         JSONObject subJson = new JSONObject();
        //  String queryString ="";
             // System.out.println("first call");
            String data = "";
        //queryString = "update tblEmpReview set ReviewTypeId=?,EmpComments=?,ModifiedBy=?,ModifiedDate=?,ReviewName=?,ReviewDate=?,Status=?,TLComments=?,TLRating=?,HRRating=?,HrComments=? where Id=?";
        //if(roleName.equalsIgnoreCase("Employee"))
        // queryString = "update tblEmpReview set ReviewTypeId=?,EmpComments=?,ReviewName=?,ReviewDate=?,Status=? where Id=?";
        try {
            
            
    for (Object quesObj : questionList) {
			//System.out.println(temp);
                            Map tempQmap = (Map)quesObj;
                           // if(tempQmap.get("OptionType").toString().equals("Textbox")||tempQmap.get("OptionType").toString().equals("Checkbox")||tempQmap.get("OptionType").toString().equals("RadioButton"))
                          if(Integer.parseInt(tempQmap.get("Id").toString())==questionId){
                          subJson.put("Id", tempQmap.get("Id"));
subJson.put("Query",tempQmap.get("Query"));
subJson.put("OptionType", tempQmap.get("OptionType"));
subJson.put("CurrStatus", tempQmap.get("CurrStatus"));
subJson.put("Sequence", tempQmap.get("Sequence"));
subJson.put("SurveyId", tempQmap.get("SurveyId"));
subJson.put("RequiredFlag", tempQmap.get("RequiredFlag"));
 if(subJson.getString("OptionType").equals("Checkbox")||subJson.getString("OptionType").equals("RadioButton")){
     subJson.put("OptionCount", tempQmap.get("OptionCount"));
subJson.put("OptionLabel1",tempQmap.get("OptionLabel1"));
subJson.put("OptionSequence1",tempQmap.get("OptionSequence1"));
                         
                         if(Integer.parseInt(subJson.getString("OptionCount"))>1){
                             subJson.put("OptionLabel2",tempQmap.get("OptionLabel2"));
subJson.put("OptionSequence2",tempQmap.get("OptionSequence2"));
                         } if(Integer.parseInt(subJson.getString("OptionCount"))>2){
                             subJson.put("OptionLabel3",tempQmap.get("OptionLabel3"));
subJson.put("OptionSequence3",tempQmap.get("OptionSequence3"));
                         } if(Integer.parseInt(subJson.getString("OptionCount"))>3){
                             subJson.put("OptionLabel4",tempQmap.get("OptionLabel4"));
subJson.put("OptionSequence4",tempQmap.get("OptionSequence4"));
                         } if(Integer.parseInt(subJson.getString("OptionCount"))>4){
                             subJson.put("OptionLabel5",tempQmap.get("OptionLabel5"));
subJson.put("OptionSequence5",tempQmap.get("OptionSequence5"));
                         }if(Integer.parseInt(subJson.getString("OptionCount"))>5){
                             subJson.put("OptionLabel6",tempQmap.get("OptionLabel6"));
subJson.put("OptionSequence6",tempQmap.get("OptionSequence6"));
                         }
      
 }
break;
                          }
    }
                            
                           
     
            
            
         //   System.out.println("data->"+data);
          

     
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return subJson.toString();
      }
 
  public String updateQuestionSequence(AjaxHandlerAction ajaxHandlerAction) throws ServiceLocatorException{
           
 String message ="";
    
      String data = "";
        try {
                JSONObject jb = new JSONObject();
        
                jb.put("CreatedBy", ajaxHandlerAction.getCreatedBy());
                jb.put("Data", ajaxHandlerAction.getQuestionSequanceData());
     
         String serviceUrl = RestRepository.getInstance().getSrviceUrl("updateQuestionSequence");
              URL url = new URL(serviceUrl);
            URLConnection connection = url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setConnectTimeout(360*5000);
            connection.setReadTimeout(360*5000);
            OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
           out.write(jb.toString());
            out.close();

            BufferedReader in = new BufferedReader(new InputStreamReader(
                    connection.getInputStream()));
            String s = null;
            
            while ((s = in.readLine()) != null) {

                data = data + s;
            }

       // JSONObject jObject = new JSONObject(data);

        // message = jObject.getString("message");
     
//System.out.println("message-->"+message);

            in.close();
           
        } catch (IOException ex) {
            ex.printStackTrace();
        }catch(Exception e){
             throw new ServiceLocatorException(e);
        }
        return data;   
     }
  public String doUpdateSurveyFormExpiryDate(AjaxHandlerAction ajaxHandlerAction) throws ServiceLocatorException {

        String message = "";

        String data = "";
       try {
            JSONObject jb = new JSONObject();
            jb.put("surveyId", ajaxHandlerAction.getSurveyId());
           jb.put("ExpiryDate", ajaxHandlerAction.getExpiryDate());                              
            String serviceUrl = RestRepository.getInstance().getSrviceUrl("updateExpiry");
            URL url = new URL(serviceUrl);
            URLConnection connection = url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setConnectTimeout(360*5000);
            connection.setReadTimeout(360*5000);
            OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
            out.write(jb.toString());
            out.close();

            BufferedReader in = new BufferedReader(new InputStreamReader(
                    connection.getInputStream()));
            String s = null;
            while ((s = in.readLine()) != null) {

                data = data + s;
            }

             JSONObject jObject = new JSONObject(data);

             message = jObject.getString("message");

//System.out.println("message-->"+message);

            in.close();

        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (Exception e) {
            throw new ServiceLocatorException(e);
        }
        return message;
    }
/*
   * Emeet Methods 
   * date : 01/29/2016
   * Author : Phani Kanuri
   * 
   */
    public String doAddExecitiveMeeting(AjaxHandlerAction ajaxHandlerAction) throws Exception {
        // Connection connection = null;
        //PreparedStatement preparedStatement = null;
        // ResultSet resultSet = null;
        String message = "";
        boolean isUpdated = false;

        try {

            JSONObject jb = new JSONObject();
            // AjaxHandlerAction ac=new AjaxHandlerAction();
            jb.put("ExecutiveMeetingType", ajaxHandlerAction.getExecutiveMeetingType());

            jb.put("ExecutiveMeetMonth", ajaxHandlerAction.getExecutiveMeetMonth());

            // jb.put("RegistrationTextforExeMeet", ajaxHandlerAction.getRegistrationTextforExeMeet());

            jb.put("ExeMeetcreatedDateTo", ajaxHandlerAction.getExeMeetcreatedDateTo());
            jb.put("ExecutiveMeetingStatus", ajaxHandlerAction.getExecutiveMeetingStatus());
            jb.put("ExeMeetStartTime", ajaxHandlerAction.getExeMeetStartTime());
            jb.put("ExeMeetEndTime", ajaxHandlerAction.getExeMeetEndTime());
            jb.put("ExeMeetStartmidDayFrom", ajaxHandlerAction.getExeMeetStartmidDayFrom());
            jb.put("ExeMeetEndmidDayTo", ajaxHandlerAction.getExeMeetEndmidDayTo());
            jb.put("RegistrationLinkForEMeet", ajaxHandlerAction.getRegistrationLinkForEMeet());
            jb.put("CreatedBy", ajaxHandlerAction.getCreatedBy());
            jb.put("TimeZone", ajaxHandlerAction.getTimeZone());
            jb.put("Month", ajaxHandlerAction.getExecutiveMeetMonth());

            //   URL url = new URL("http://172.17.11.251:8080/WebRoot/resources/doJobPost/post");
            // URL url = new URL("http://172.17.14.226:8080/WebRoot/resources/doJobPost/post");
            // URL url = new URL(Properties.getProperty("WEB.REST.URL") +"doJobPost/post");
            String serviceUrl = RestRepository.getInstance().getSrviceUrl("doAddExecutiveMeeting");
            URL url = new URL(serviceUrl);
            URLConnection connection = url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setConnectTimeout(360*5000);
            connection.setReadTimeout(360*5000);
            OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
            out.write(jb.toString());
            out.close();

            BufferedReader in = new BufferedReader(new InputStreamReader(
                    connection.getInputStream()));
            String s = null;
            String data = "";
            while ((s = in.readLine()) != null) {

                data = data + s;
            }

            JSONObject jObject = new JSONObject(data);

            message = jObject.getString("message");
            // System.out.println("displaymessage-->" +message);


            in.close();

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return message;
    }

    public String editExeMeeting(int Id) throws Exception {
        String message = "";
        boolean isUpdated = false;
        //  String queryString ="";
        // System.out.println("first call");
        String data = "";

        try {


            JSONObject jb = new JSONObject();

            jb.put("Id", Id);
            //  URL url = new URL("http://172.17.11.251:8080/WebRoot/resources/doJobPost/edit");
            //  URL url = new URL("http://172.17.14.226:8080/WebRoot/resources/doJobPost/edit");
            // URL url = new URL(Properties.getProperty("WEB.REST.URL") +"doJobPost/edit");
            String serviceUrl = RestRepository.getInstance().getSrviceUrl("eMeetEdit");
            URL url = new URL(serviceUrl);
            URLConnection connection = url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setConnectTimeout(360*5000);
            connection.setReadTimeout(360*5000);
            OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
            out.write(jb.toString());
            out.close();

            BufferedReader in = new BufferedReader(new InputStreamReader(
                    connection.getInputStream()));
            String s = null;

            while ((s = in.readLine()) != null) {

                data = data + s;
            }

            //  JSONObject jObject = new JSONObject(data);

            // message = jObject.getString("message");
            //  System.out.println("displaymessage-->" +message);


            in.close();

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return data;
    }

    public String updateExecitiveMeeting(AjaxHandlerAction ajaxHandlerAction) throws Exception {
        // Connection connection = null;
        //PreparedStatement preparedStatement = null;
        // ResultSet resultSet = null;
        String message = "";
        boolean isUpdated = false;
        //  String queryString ="";
        //   System.out.println("first call");

        //queryString = "update tblEmpReview set ReviewTypeId=?,EmpComments=?,ModifiedBy=?,ModifiedDate=?,ReviewName=?,ReviewDate=?,Status=?,TLComments=?,TLRating=?,HRRating=?,HrComments=? where Id=?";
        //if(roleName.equalsIgnoreCase("Employee"))
        // queryString = "update tblEmpReview set ReviewTypeId=?,EmpComments=?,ReviewName=?,ReviewDate=?,Status=? where Id=?";
        try {


            JSONObject jb = new JSONObject();
            // AjaxHandlerAction ac=new AjaxHandlerAction();

            jb.put("ExeMeetId", ajaxHandlerAction.getId());
            jb.put("ExecutiveMeetingType", ajaxHandlerAction.getExecutiveMeetingType());

            jb.put("ExecutiveMeetMonth", ajaxHandlerAction.getExecutiveMeetMonth());

            //   jb.put("RegistrationTextforExeMeet", ajaxHandlerAction.getRegistrationTextforExeMeet());

            jb.put("ExeMeetcreatedDateTo", ajaxHandlerAction.getExeMeetcreatedDateTo());
            jb.put("ExecutiveMeetingStatus", ajaxHandlerAction.getExecutiveMeetingStatus());
            jb.put("ExeMeetStartTime", ajaxHandlerAction.getExeMeetStartTime());
            jb.put("ExeMeetEndTime", ajaxHandlerAction.getExeMeetEndTime());
            jb.put("ExeMeetStartmidDayFrom", ajaxHandlerAction.getExeMeetStartmidDayFrom());
            jb.put("ExeMeetEndmidDayTo", ajaxHandlerAction.getExeMeetEndmidDayTo());
            jb.put("RegistrationLinkForEMeet", ajaxHandlerAction.getRegistrationLinkForEMeet());
            jb.put("CreatedBy", ajaxHandlerAction.getCreatedBy());
            jb.put("TimeZone", ajaxHandlerAction.getTimeZone());
            jb.put("Month", ajaxHandlerAction.getExecutiveMeetMonth());
            //URL url = new URL("http://172.17.11.251:8080/WebRoot/resources/doJobPost/update");
            //URL url = new URL("http://172.17.14.226:8080/WebRoot/resources/doJobPost/update");
            //  URL url = new URL(Properties.getProperty("WEB.REST.URL") +"doJobPost/update");

            String serviceUrl = RestRepository.getInstance().getSrviceUrl("executiveMeetingUpdate");
            URL url = new URL(serviceUrl);
            URLConnection connection = url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setConnectTimeout(360*5000);
            connection.setReadTimeout(360*5000);
            OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
            out.write(jb.toString());
            out.close();

            BufferedReader in = new BufferedReader(new InputStreamReader(
                    connection.getInputStream()));
            String s = null;
            String data = "";
            while ((s = in.readLine()) != null) {

                data = data + s;
            }

            JSONObject jObject = new JSONObject(data);

            message = jObject.getString("message");
            //   System.out.println("displaymessage-->" +message);



            in.close();

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return message;
    }

    public String updateCompletedExecitiveMeetDetails(AjaxHandlerAction ajaxHandlerAction) throws Exception {
        // Connection connection = null;
        //PreparedStatement preparedStatement = null;
        // ResultSet resultSet = null;
        String message = "";
        boolean isUpdated = false;

        try {


            JSONObject jb = new JSONObject();
            // AjaxHandlerAction ac=new AjaxHandlerAction();

            jb.put("ExeMeetId", ajaxHandlerAction.getId());
            jb.put("ExecutiveMeetingStatus", ajaxHandlerAction.getExecutiveMeetingStatus());
            jb.put("CreatedBy", ajaxHandlerAction.getCreatedBy());
            jb.put("VideoLink", ajaxHandlerAction.getVideoLinkForEMeet());
            //URL url = new URL("http://172.17.11.251:8080/WebRoot/resources/doJobPost/update");
            //URL url = new URL("http://172.17.14.226:8080/WebRoot/resources/doJobPost/update");
            //  URL url = new URL(Properties.getProperty("WEB.REST.URL") +"doJobPost/update");

            String serviceUrl = RestRepository.getInstance().getSrviceUrl("updateCompletedExeMeetDetails");
            URL url = new URL(serviceUrl);
            URLConnection connection = url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setConnectTimeout(360*5000);
            connection.setReadTimeout(360*5000);
            OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
            out.write(jb.toString());
            out.close();

            BufferedReader in = new BufferedReader(new InputStreamReader(
                    connection.getInputStream()));
            String s = null;
            String data = "";
            while ((s = in.readLine()) != null) {

                data = data + s;
            }

            JSONObject jObject = new JSONObject(data);

            message = jObject.getString("message");
            //   System.out.println("displaymessage-->" +message);



            in.close();

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return message;
    }

    public String doActiveEmmet(AjaxHandlerAction ajaxHandlerAction) throws Exception {
        // Connection connection = null;
        //PreparedStatement preparedStatement = null;
        // ResultSet resultSet = null;
        String message = "";
        boolean isUpdated = false;

        try {


            JSONObject jb = new JSONObject();
            // AjaxHandlerAction ac=new AjaxHandlerAction();

            jb.put("ExeMeetId", ajaxHandlerAction.getId());

            jb.put("CreatedBy", ajaxHandlerAction.getCreatedBy());
            //URL url = new URL("http://172.17.11.251:8080/WebRoot/resources/doJobPost/update");
            //URL url = new URL("http://172.17.14.226:8080/WebRoot/resources/doJobPost/update");
            //  URL url = new URL(Properties.getProperty("WEB.REST.URL") +"doJobPost/update");

            String serviceUrl = RestRepository.getInstance().getSrviceUrl("doActiveEmmet");
            URL url = new URL(serviceUrl);
            URLConnection connection = url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setConnectTimeout(360*5000);
            connection.setReadTimeout(360*5000);
            OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
            out.write(jb.toString());
            out.close();

            BufferedReader in = new BufferedReader(new InputStreamReader(
                    connection.getInputStream()));
            String s = null;
            String data = "";
            while ((s = in.readLine()) != null) {

                data = data + s;
            }

            JSONObject jObject = new JSONObject(data);

            message = jObject.getString("message");
            //   System.out.println("displaymessage-->" +message);



            in.close();

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return message;
    }

    public JSONObject getUpcomingEmeets(String emeetType) throws ServiceLocatorException {
        List jobsList = null;
        List mainList = null;
        String createdDateFrom;
        String createdDateTo;
        String jobId;
        String title;
        String status;

        JSONObject jObject = null;
        try {

            JSONObject jb = new JSONObject();


            jb.put("EMeetCreatedDate", "none");
            jb.put("EmeetFromDate", "none");
            jb.put("EmeetToDate", "none");
            jb.put("EmeetStatusByDate", "U");
            jb.put("EmeetStatusForMails", "Published");
            jb.put("ExecutiveMeetMonthSearch", "none");
            if (!"Both".equals(emeetType)) {
                jb.put("EMeetType", emeetType);
            } else {
                jb.put("EMeetType", "none");
            }




            String serviceUrl = RestRepository.getInstance().getSrviceUrl("eMeetsearch");
            URL url = new URL(serviceUrl);
            URLConnection connection = url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setConnectTimeout(360*5000);
            connection.setReadTimeout(360*5000);
            OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
            out.write(jb.toString());
            out.close();

            BufferedReader in = new BufferedReader(new InputStreamReader(
                    connection.getInputStream()));
            String s = null;
            String data = "";
            while ((s = in.readLine()) != null) {
                //     System.out.println(s);
                data = data + s;
            }



            //  System.out.println("Data-->"+data);
            jObject = new JSONObject(data);


            in.close();
        } catch (Exception e) {
            System.out.println("\nError while calling REST Service");
            //System.out.println(e);
        }
        return jObject;
    }
/*Events functionality new enhancements start
     * Date : 01/23/2017
     * Author : Santosh Kola/Phani Kanuri
     */
    
public String doAddQmeetEventPost(AjaxHandlerAction ajaxHandlerAction) throws Exception{
       
 String message ="";
        boolean isUpdated = false;
      
        try {
            
              
                JSONObject jb = new JSONObject();
              
        jb.put("event_title", ajaxHandlerAction.getEventTitle());
  
        jb.put("event_description", ajaxHandlerAction.getEventDescription());
    
        jb.put("event_startdate", ajaxHandlerAction.getStartDate());
      
        //jb.put("evetnt_enddate",ajaxHandlerAction.getEndDate());
        jb.put("event_time_from",ajaxHandlerAction.getStartTime());
        jb.put("event_time_to",ajaxHandlerAction.getEndTime());  
        jb.put("midday_from",ajaxHandlerAction.getMidDayFrom()); 
        jb.put("midday_to",ajaxHandlerAction.getMidDayTo()); 
        jb.put("timezone",ajaxHandlerAction.getTimeZone()); 
        jb.put("location",ajaxHandlerAction.getEventLocation()); 
        jb.put("transport",ajaxHandlerAction.getTransportation()); 
        jb.put("createdby",ajaxHandlerAction.getCreatedBy()); 
       jb.put("status",ajaxHandlerAction.getEventStatus()); 
       
     //    System.out.println("getConferenceUrl-->"+ajaxHandlerAction.getConferenceUrl());
         //   URL url = new URL(Properties.getProperty("WEB.REST.URL") +"doEventPost/addEvent");
         String serviceUrl = RestRepository.getInstance().getSrviceUrl("addQmeetEvent");
              URL url = new URL(serviceUrl);
            URLConnection connection = url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setConnectTimeout(360*5000);
            connection.setReadTimeout(360*5000);
            OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
           out.write(jb.toString());
            out.close();

            BufferedReader in = new BufferedReader(new InputStreamReader(
                    connection.getInputStream()));
            String s = null;
            String data = "";
            while ((s = in.readLine()) != null) {

                data = data + s;
            }

        JSONObject jObject = new JSONObject(data);

         message = jObject.getString("message");
     


            in.close();
           
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return message;   
      }     
       public String editEventpost(String eventId) throws Exception{
          String message ="";
        boolean isUpdated = false;
        //  String queryString ="";
             // System.out.println("first call");
            String data = "";
        //queryString = "update tblEmpReview set ReviewTypeId=?,EmpComments=?,ModifiedBy=?,ModifiedDate=?,ReviewName=?,ReviewDate=?,Status=?,TLComments=?,TLRating=?,HRRating=?,HrComments=? where Id=?";
        //if(roleName.equalsIgnoreCase("Employee"))
        // queryString = "update tblEmpReview set ReviewTypeId=?,EmpComments=?,ReviewName=?,ReviewDate=?,Status=? where Id=?";
        try {
            
              
                JSONObject jb = new JSONObject();
      
        jb.put("event_id",eventId);        
          //  URL url = new URL("http://172.17.11.251:8080/WebRoot/resources/doJobPost/edit");
        //  URL url = new URL("http://172.17.14.226:8080/WebRoot/resources/doJobPost/edit");
       //  URL url = new URL(Properties.getProperty("WEB.REST.URL") +"doJobPost/edit");
        String serviceUrl = RestRepository.getInstance().getSrviceUrl("editEventPost");
              URL url = new URL(serviceUrl);
            URLConnection connection = url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setConnectTimeout(360*5000);
            connection.setReadTimeout(360*5000);
            OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
           out.write(jb.toString());
            out.close();

            BufferedReader in = new BufferedReader(new InputStreamReader(
                    connection.getInputStream()));
            String s = null;

            while ((s = in.readLine()) != null) {

                data = data + s;
            }

      //  JSONObject jObject = new JSONObject(data);

        // message = jObject.getString("message");
      //  System.out.println("displaymessage-->" +message);


            in.close();
            /* connection = ConnectionProvider.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(queryString);
            preparedStatement.setString(1,ajaxHandlerAction.getOverlayReviewType());
            preparedStatement.setString(2,ajaxHandlerAction.getOverlayDescription());
            
            
            
            preparedStatement.setString(3,ajaxHandlerAction.getOverlayReviewName());
            preparedStatement.setDate(4, DateUtility.getInstance().getMysqlDate(ajaxHandlerAction.getOverlayReviewDate()));
            preparedStatement.setString(5,ajaxHandlerAction.getReviewStatusOverlay());
            
            preparedStatement.setString(6,ajaxHandlerAction.getReviewId());
            isUpdated = preparedStatement.execute();*/
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return data;
      }
       
       
        public String updateQmeetEventPost(AjaxHandlerAction ajaxHandlerAction) throws Exception{
         
 String message ="";
        boolean isUpdated = false;
      
        try {
            
              
                JSONObject jb = new JSONObject();
              jb.put("event_id", String.valueOf(ajaxHandlerAction.getEventId()));
        jb.put("event_title", ajaxHandlerAction.getEventTitle());
  
        jb.put("event_description", ajaxHandlerAction.getEventDescription());
    
        jb.put("event_startdate", ajaxHandlerAction.getStartDate());
      
        //jb.put("evetnt_enddate",ajaxHandlerAction.getEndDate());
        jb.put("event_time_from",ajaxHandlerAction.getStartTime());
        jb.put("event_time_to",ajaxHandlerAction.getEndTime());  
        jb.put("midday_from",ajaxHandlerAction.getMidDayFrom()); 
        
       // System.out.println("getMidDayTo-->"+ajaxHandlerAction.getMidDayTo());
        jb.put("midday_to",ajaxHandlerAction.getMidDayTo()); 
        jb.put("timezone",ajaxHandlerAction.getTimeZone()); 
        jb.put("location",ajaxHandlerAction.getEventLocation()); 
        jb.put("transport",ajaxHandlerAction.getTransportation()); 
        jb.put("createdby",ajaxHandlerAction.getCreatedBy()); 
       jb.put("status",ajaxHandlerAction.getEventStatus()); 
       // jb.put("WebinarType",ajaxHandlerAction.getEventType()); 
       
         
        // System.out.println("VideoLink-->"+ajaxHandlerAction.getEventAfterVideoUrl());
       //  System.out.println("get VideoLink-->"+jb.getString("VideoLink"));
      //  System.out.println("getConferenceUrl-->"+ajaxHandlerAction.getConferenceUrl());
         //   URL url = new URL(Properties.getProperty("WEB.REST.URL") +"doEventPost/addEvent");
         String serviceUrl = RestRepository.getInstance().getSrviceUrl("updateQmeetEventPost");
              URL url = new URL(serviceUrl);
            URLConnection connection = url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setConnectTimeout(360*5000);
            connection.setReadTimeout(360*5000);
            OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
           out.write(jb.toString());
            out.close();

            BufferedReader in = new BufferedReader(new InputStreamReader(
                    connection.getInputStream()));
            String s = null;
            String data = "";
            while ((s = in.readLine()) != null) {

                data = data + s;
            }

        JSONObject jObject = new JSONObject(data);

         message = jObject.getString("message");
     


            in.close();
           
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return message;   
     } 
        
       
public String doAddExternalWebinarEventPost(AjaxHandlerAction ajaxHandlerAction) throws Exception{
       
 String message ="";
        boolean isUpdated = false;
      
        try {
            
              
                JSONObject jb = new JSONObject();
              
        jb.put("event_title", ajaxHandlerAction.getEventTitle());
  
        jb.put("event_description", ajaxHandlerAction.getEventDescription());
    
        jb.put("event_startdate", ajaxHandlerAction.getStartDate());
      
        jb.put("evetnt_enddate",ajaxHandlerAction.getEndDate());
        jb.put("event_time_from",ajaxHandlerAction.getStartTime());
        jb.put("event_time_to",ajaxHandlerAction.getEndTime());  
        jb.put("midday_from",ajaxHandlerAction.getMidDayFrom()); 
        jb.put("midday_to",ajaxHandlerAction.getMidDayTo()); 
        jb.put("timezone",ajaxHandlerAction.getTimeZone()); 
        jb.put("location",ajaxHandlerAction.getEventLocation()); 
       // jb.put("transport",ajaxHandlerAction.getTransportation()); 
        jb.put("createdby",ajaxHandlerAction.getCreatedBy()); 
       jb.put("status",ajaxHandlerAction.getEventStatus()); 
        //jb.put("WebinarType",ajaxHandlerAction.getEventType()); 
        
       // if(ajaxHandlerAction.getEventType().equalsIgnoreCase("I")|| ajaxHandlerAction.getEventType().equalsIgnoreCase("E")){
        jb.put("event_bold_Title",ajaxHandlerAction.getEventBoldtitle()); 
        jb.put("event_tagline",ajaxHandlerAction.getEventRegluarTitle()); 
        jb.put("OrganizerEmail",ajaxHandlerAction.getContactUsEmail()); 
        jb.put("RegistrationLink",ajaxHandlerAction.getEventRegistrationLink()); 
        
       // jb.put("PrimaryTrack",ajaxHandlerAction.getPrimaryTrack());
       // jb.put("SecondaryTrack",ajaxHandlerAction.getSecondaryTrack());
         if (!"".equals(ajaxHandlerAction.getPrimaryTrack()) && ajaxHandlerAction.getPrimaryTrack() != null) {
                jb.put("PrimaryTrack", ajaxHandlerAction.getPrimaryTrack());
            } else {
                jb.put("PrimaryTrack", "0");
            }
            if (!"".equals(ajaxHandlerAction.getSecondaryTrack()) && ajaxHandlerAction.getSecondaryTrack() != null) {
                jb.put("SecondaryTrack", ajaxHandlerAction.getSecondaryTrack());
            } else {
                jb.put("SecondaryTrack", "0");
            }
       // jb.put("Department",ajaxHandlerAction.getEventDepartment());
       // }
         
         //jb.put("VideoLink",ajaxHandlerAction.getEventAfterVideoUrl());
     //    System.out.println("getConferenceUrl-->"+ajaxHandlerAction.getConferenceUrl());
         //   URL url = new URL(Properties.getProperty("WEB.REST.URL") +"doEventPost/addEvent");
         String serviceUrl = RestRepository.getInstance().getSrviceUrl("externalWebinarEventAdd");
              URL url = new URL(serviceUrl);
            URLConnection connection = url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setConnectTimeout(360*5000);
            connection.setReadTimeout(360*5000);
            OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
           out.write(jb.toString());
            out.close();

            BufferedReader in = new BufferedReader(new InputStreamReader(
                    connection.getInputStream()));
            String s = null;
            String data = "";
            while ((s = in.readLine()) != null) {

                data = data + s;
            }

        JSONObject jObject = new JSONObject(data);

         message = jObject.getString("message");
     


            in.close();
           
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return message;   
      }     
       
        
         public String editExternalEventpost(String eventId) throws Exception{
          String message ="";
        boolean isUpdated = false;
        //  String queryString ="";
             // System.out.println("first call");
            String data = "";
        //queryString = "update tblEmpReview set ReviewTypeId=?,EmpComments=?,ModifiedBy=?,ModifiedDate=?,ReviewName=?,ReviewDate=?,Status=?,TLComments=?,TLRating=?,HRRating=?,HrComments=? where Id=?";
        //if(roleName.equalsIgnoreCase("Employee"))
        // queryString = "update tblEmpReview set ReviewTypeId=?,EmpComments=?,ReviewName=?,ReviewDate=?,Status=? where Id=?";
        try {
            
              
                JSONObject jb = new JSONObject();
      
        jb.put("event_id",eventId);        
          //  URL url = new URL("http://172.17.11.251:8080/WebRoot/resources/doJobPost/edit");
        //  URL url = new URL("http://172.17.14.226:8080/WebRoot/resources/doJobPost/edit");
       //  URL url = new URL(Properties.getProperty("WEB.REST.URL") +"doJobPost/edit");
        String serviceUrl = RestRepository.getInstance().getSrviceUrl("editExternalEventPost");
              URL url = new URL(serviceUrl);
            URLConnection connection = url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setConnectTimeout(360*5000);
            connection.setReadTimeout(360*5000);
            OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
           out.write(jb.toString());
            out.close();

            BufferedReader in = new BufferedReader(new InputStreamReader(
                    connection.getInputStream()));
            String s = null;

            while ((s = in.readLine()) != null) {

                data = data + s;
            }

      //  JSONObject jObject = new JSONObject(data);

        // message = jObject.getString("message");
      //  System.out.println("displaymessage-->" +message);


            in.close();
            /* connection = ConnectionProvider.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(queryString);
            preparedStatement.setString(1,ajaxHandlerAction.getOverlayReviewType());
            preparedStatement.setString(2,ajaxHandlerAction.getOverlayDescription());
            
            
            
            preparedStatement.setString(3,ajaxHandlerAction.getOverlayReviewName());
            preparedStatement.setDate(4, DateUtility.getInstance().getMysqlDate(ajaxHandlerAction.getOverlayReviewDate()));
            preparedStatement.setString(5,ajaxHandlerAction.getReviewStatusOverlay());
            
            preparedStatement.setString(6,ajaxHandlerAction.getReviewId());
            isUpdated = preparedStatement.execute();*/
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return data;
      }
         
         
          public String updateExternalWebinarEventposting(AjaxHandlerAction ajaxHandlerAction) throws Exception{
         
 String message ="";
        boolean isUpdated = false;
      
        try {
            
              
                JSONObject jb = new JSONObject();
              jb.put("event_id", String.valueOf(ajaxHandlerAction.getEventId()));
        jb.put("event_title", ajaxHandlerAction.getEventTitle());
  
        jb.put("event_description", ajaxHandlerAction.getEventDescription());
    
        jb.put("event_startdate", ajaxHandlerAction.getStartDate());
      
       // jb.put("evetnt_enddate",ajaxHandlerAction.getEndDate());
        jb.put("event_time_from",ajaxHandlerAction.getStartTime());
        jb.put("event_time_to",ajaxHandlerAction.getEndTime());  
        jb.put("midday_from",ajaxHandlerAction.getMidDayFrom()); 
        
       // System.out.println("getMidDayTo-->"+ajaxHandlerAction.getMidDayTo());
        jb.put("midday_to",ajaxHandlerAction.getMidDayTo()); 
        jb.put("timezone",ajaxHandlerAction.getTimeZone()); 
        jb.put("location",ajaxHandlerAction.getEventLocation()); 
        //jb.put("transport",ajaxHandlerAction.getTransportation()); 
        jb.put("createdby",ajaxHandlerAction.getCreatedBy()); 
       jb.put("status",ajaxHandlerAction.getEventStatus()); 
        //jb.put("WebinarType",ajaxHandlerAction.getEventType()); 
      // if(ajaxHandlerAction.getEventType().equalsIgnoreCase("I")|| ajaxHandlerAction.getEventType().equalsIgnoreCase("E")){
        jb.put("event_bold_Title",ajaxHandlerAction.getEventBoldtitle()); 
        jb.put("event_tagline",ajaxHandlerAction.getEventRegluarTitle()); 
        jb.put("OrganizerEmail",ajaxHandlerAction.getContactUsEmail()); 
        jb.put("RegistrationLink",ajaxHandlerAction.getEventRegistrationLink()); 
        
       if (!"".equals(ajaxHandlerAction.getPrimaryTrack()) && ajaxHandlerAction.getPrimaryTrack() != null) {
                jb.put("PrimaryTrack", ajaxHandlerAction.getPrimaryTrack());
            } else {
                jb.put("PrimaryTrack", "0");
            }
            if (!"".equals(ajaxHandlerAction.getSecondaryTrack()) && ajaxHandlerAction.getSecondaryTrack() != null) {
                jb.put("SecondaryTrack", ajaxHandlerAction.getSecondaryTrack());
            } else {
                jb.put("SecondaryTrack", "0");
            }
       // jb.put("Department",ajaxHandlerAction.getEventDepartment());
        
       // jb.put("IsAssociated",ajaxHandlerAction.getIsAssociated());
        //jb.put("AsociatedEventId",ajaxHandlerAction.getAssociatedEventId());
        
      //  }
       
      // if(ajaxHandlerAction.getEventType().equalsIgnoreCase("C")){
        //     jb.put("event_redirect",ajaxHandlerAction.getConferenceUrl()); 
        // }
        // jb.put("VideoLink",ajaxHandlerAction.getEventAfterVideoUrl());
         jb.put("SeriesId",ajaxHandlerAction.getSeriesId());
         
        // System.out.println("VideoLink-->"+ajaxHandlerAction.getEventAfterVideoUrl());
       //  System.out.println("get VideoLink-->"+jb.getString("VideoLink"));
      //  System.out.println("getConferenceUrl-->"+ajaxHandlerAction.getConferenceUrl());
         //   URL url = new URL(Properties.getProperty("WEB.REST.URL") +"doEventPost/addEvent");
         String serviceUrl = RestRepository.getInstance().getSrviceUrl("externalWebinarEventUpdate");
              URL url = new URL(serviceUrl);
            URLConnection connection = url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setConnectTimeout(360*5000);
            connection.setReadTimeout(360*5000);
            OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
           out.write(jb.toString());
            out.close();

            BufferedReader in = new BufferedReader(new InputStreamReader(
                    connection.getInputStream()));
            String s = null;
            String data = "";
            while ((s = in.readLine()) != null) {

                data = data + s;
            }

        JSONObject jObject = new JSONObject(data);

         message = jObject.getString("message");
     


            in.close();
           
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return message;   
     } 
     
       /*
           * 
           * 
           * phani methods
           */
          
          
    public String doAddIEEEventPost(AjaxHandlerAction ajaxHandlerAction) throws Exception {

        String message = "";
        boolean isUpdated = false;

        try {


            JSONObject jb = new JSONObject();

            jb.put("event_title", ajaxHandlerAction.getEventTitle());
            jb.put("event_startdate", ajaxHandlerAction.getStartDate());
            jb.put("evetnt_enddate",ajaxHandlerAction.getStartDate());
            jb.put("event_time_from", ajaxHandlerAction.getStartTime());
            jb.put("event_time_to", ajaxHandlerAction.getEndTime());
            jb.put("midday_from", ajaxHandlerAction.getMidDayFrom());
            jb.put("midday_to", ajaxHandlerAction.getMidDayTo());
            jb.put("timezone", ajaxHandlerAction.getTimeZone());
            jb.put("location", ajaxHandlerAction.getEventLocation());
            jb.put("createdby", ajaxHandlerAction.getCreatedBy());
            jb.put("status", ajaxHandlerAction.getEventStatus());

            //    System.out.println("getConferenceUrl-->"+ajaxHandlerAction.getConferenceUrl());
            //   URL url = new URL(Properties.getProperty("WEB.REST.URL") +"doEventPost/addEvent");
            String serviceUrl = RestRepository.getInstance().getSrviceUrl("addIEEEvent");
            URL url = new URL(serviceUrl);
            URLConnection connection = url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setConnectTimeout(360 * 5000);
            connection.setReadTimeout(360 * 5000);
            OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
            out.write(jb.toString());
            out.close();

            BufferedReader in = new BufferedReader(new InputStreamReader(
                    connection.getInputStream()));
            String s = null;
            String data = "";
            while ((s = in.readLine()) != null) {

                data = data + s;
            }

            JSONObject jObject = new JSONObject(data);

            message = jObject.getString("message");



            in.close();

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return message;
    }
    
    public String updateIEEEventPost(AjaxHandlerAction ajaxHandlerAction) throws Exception {

        String message = "";
        boolean isUpdated = false;

        try {


            JSONObject jb = new JSONObject();
            jb.put("event_id", String.valueOf(ajaxHandlerAction.getEventId()));
            jb.put("event_title", ajaxHandlerAction.getEventTitle());

         //   jb.put("event_description", ajaxHandlerAction.getEventDescription());

            jb.put("event_startdate", ajaxHandlerAction.getStartDate());

            //jb.put("evetnt_enddate",ajaxHandlerAction.getEndDate());
            jb.put("event_time_from", ajaxHandlerAction.getStartTime());
            jb.put("event_time_to", ajaxHandlerAction.getEndTime());
            jb.put("midday_from", ajaxHandlerAction.getMidDayFrom());

            // System.out.println("getMidDayTo-->"+ajaxHandlerAction.getMidDayTo());
            jb.put("midday_to", ajaxHandlerAction.getMidDayTo());
            jb.put("timezone", ajaxHandlerAction.getTimeZone());
            jb.put("location", ajaxHandlerAction.getEventLocation());
           // jb.put("transport", ajaxHandlerAction.getTransportation());
            jb.put("createdby", ajaxHandlerAction.getCreatedBy());
            jb.put("status", ajaxHandlerAction.getEventStatus());
            // jb.put("WebinarType",ajaxHandlerAction.getEventType()); 


            // System.out.println("VideoLink-->"+ajaxHandlerAction.getEventAfterVideoUrl());
            //  System.out.println("get VideoLink-->"+jb.getString("VideoLink"));
            //  System.out.println("getConferenceUrl-->"+ajaxHandlerAction.getConferenceUrl());
            //   URL url = new URL(Properties.getProperty("WEB.REST.URL") +"doEventPost/addEvent");
            String serviceUrl = RestRepository.getInstance().getSrviceUrl("updateIEEEventPost");
            URL url = new URL(serviceUrl);
            URLConnection connection = url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setConnectTimeout(360 * 5000);
            connection.setReadTimeout(360 * 5000);
            OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
            out.write(jb.toString());
            out.close();

            BufferedReader in = new BufferedReader(new InputStreamReader(
                    connection.getInputStream()));
            String s = null;
            String data = "";
            while ((s = in.readLine()) != null) {

                data = data + s;
            }

            JSONObject jObject = new JSONObject(data);

            message = jObject.getString("message");



            in.close();

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return message;
    }
    
    
    
public String doAddInternalWebinarEventPost(AjaxHandlerAction ajaxHandlerAction) throws Exception{
       
 String message ="";
        boolean isUpdated = false;
      
        try {
            
              
                JSONObject jb = new JSONObject();
              
        jb.put("event_title", ajaxHandlerAction.getEventTitle());
  
        jb.put("event_description", ajaxHandlerAction.getEventDescription());
    
        jb.put("event_startdate", ajaxHandlerAction.getStartDate());
      
       // jb.put("evetnt_enddate",ajaxHandlerAction.getEndDate());
        jb.put("event_time_from",ajaxHandlerAction.getStartTime());
        jb.put("event_time_to",ajaxHandlerAction.getEndTime());  
        jb.put("midday_from",ajaxHandlerAction.getMidDayFrom()); 
        jb.put("midday_to",ajaxHandlerAction.getMidDayTo()); 
        jb.put("timezone",ajaxHandlerAction.getTimeZone()); 
        jb.put("location",ajaxHandlerAction.getEventLocation()); 
        //jb.put("transport",ajaxHandlerAction.getTransportation()); 
        jb.put("createdby",ajaxHandlerAction.getCreatedBy()); 
       jb.put("status",ajaxHandlerAction.getEventStatus()); 
        //jb.put("WebinarType",ajaxHandlerAction.getEventType()); 
        
       // if(ajaxHandlerAction.getEventType().equalsIgnoreCase("I")|| ajaxHandlerAction.getEventType().equalsIgnoreCase("E")){
        jb.put("event_bold_Title",ajaxHandlerAction.getEventBoldtitle()); 
        jb.put("event_tagline",ajaxHandlerAction.getEventRegluarTitle()); 
        jb.put("OrganizerEmail",ajaxHandlerAction.getContactUsEmail()); 
        jb.put("RegistrationLink",ajaxHandlerAction.getEventRegistrationLink()); 
        
       // jb.put("PrimaryTrack",ajaxHandlerAction.getPrimaryTrack());
       // jb.put("SecondaryTrack",ajaxHandlerAction.getSecondaryTrack());
         if (!"".equals(ajaxHandlerAction.getPrimaryTrack()) && ajaxHandlerAction.getPrimaryTrack() != null) {
                jb.put("PrimaryTrack", ajaxHandlerAction.getPrimaryTrack());
            } else {
                jb.put("PrimaryTrack", "0");
            }
            if (!"".equals(ajaxHandlerAction.getSecondaryTrack()) && ajaxHandlerAction.getSecondaryTrack() != null) {
                jb.put("SecondaryTrack", ajaxHandlerAction.getSecondaryTrack());
            } else {
                jb.put("SecondaryTrack", "0");
            }
        jb.put("Department",ajaxHandlerAction.getEventDepartment());
       // }
         
         
     //    System.out.println("getConferenceUrl-->"+ajaxHandlerAction.getConferenceUrl());
         //   URL url = new URL(Properties.getProperty("WEB.REST.URL") +"doEventPost/addEvent");
         String serviceUrl = RestRepository.getInstance().getSrviceUrl("internalWebinarEventAdd");
              URL url = new URL(serviceUrl);
            URLConnection connection = url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setConnectTimeout(360*5000);
            connection.setReadTimeout(360*5000);
            OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
           out.write(jb.toString());
            out.close();

            BufferedReader in = new BufferedReader(new InputStreamReader(
                    connection.getInputStream()));
            String s = null;
            String data = "";
            while ((s = in.readLine()) != null) {

                data = data + s;
            }

        JSONObject jObject = new JSONObject(data);

         message = jObject.getString("message");
     


            in.close();
           
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return message;   
      }     
       public String updateInternalWebinarEvent(AjaxHandlerAction ajaxHandlerAction) throws Exception{
         
 String message ="";
        boolean isUpdated = false;
      
        try {
            
              
                JSONObject jb = new JSONObject();
              jb.put("event_id", String.valueOf(ajaxHandlerAction.getEventId()));
        jb.put("event_title", ajaxHandlerAction.getEventTitle());
  
        jb.put("event_description", ajaxHandlerAction.getEventDescription());
   // System.out.println("event_startdate-->"+ajaxHandlerAction.getStartDate());
        jb.put("event_startdate", ajaxHandlerAction.getStartDate());
      
        //jb.put("evetnt_enddate",ajaxHandlerAction.getEndDate());
        jb.put("event_time_from",ajaxHandlerAction.getStartTime());
        jb.put("event_time_to",ajaxHandlerAction.getEndTime());  
        jb.put("midday_from",ajaxHandlerAction.getMidDayFrom()); 
        
       // System.out.println("getMidDayTo-->"+ajaxHandlerAction.getMidDayTo());
        jb.put("midday_to",ajaxHandlerAction.getMidDayTo()); 
        jb.put("timezone",ajaxHandlerAction.getTimeZone()); 
        jb.put("location",ajaxHandlerAction.getEventLocation()); 
       // jb.put("transport",ajaxHandlerAction.getTransportation()); 
        jb.put("createdby",ajaxHandlerAction.getCreatedBy()); 
       jb.put("status",ajaxHandlerAction.getEventStatus()); 
        //jb.put("WebinarType",ajaxHandlerAction.getEventType()); 
       //if(ajaxHandlerAction.getEventType().equalsIgnoreCase("I")|| ajaxHandlerAction.getEventType().equalsIgnoreCase("E")){
        jb.put("event_bold_Title",ajaxHandlerAction.getEventBoldtitle()); 
        jb.put("event_tagline",ajaxHandlerAction.getEventRegluarTitle()); 
        jb.put("OrganizerEmail",ajaxHandlerAction.getContactUsEmail()); 
        jb.put("RegistrationLink",ajaxHandlerAction.getEventRegistrationLink()); 
        
       if (!"".equals(ajaxHandlerAction.getPrimaryTrack()) && ajaxHandlerAction.getPrimaryTrack() != null) {
                jb.put("PrimaryTrack", ajaxHandlerAction.getPrimaryTrack());
            } else {
                jb.put("PrimaryTrack", "0");
            }
            if (!"".equals(ajaxHandlerAction.getSecondaryTrack()) && ajaxHandlerAction.getSecondaryTrack() != null) {
                jb.put("SecondaryTrack", ajaxHandlerAction.getSecondaryTrack());
            } else {
                jb.put("SecondaryTrack", "0");
            }
        jb.put("Department",ajaxHandlerAction.getEventDepartment());
        
       // jb.put("IsAssociated",ajaxHandlerAction.getIsAssociated());
        //jb.put("AsociatedEventId",ajaxHandlerAction.getAssociatedEventId());
        
        //}
       
       
       //  jb.put("VideoLink",ajaxHandlerAction.getEventAfterVideoUrl());
         jb.put("SeriesId",ajaxHandlerAction.getSeriesId());
         
        // System.out.println("VideoLink-->"+ajaxHandlerAction.getEventAfterVideoUrl());
       //  System.out.println("get VideoLink-->"+jb.getString("VideoLink"));
      //  System.out.println("getConferenceUrl-->"+ajaxHandlerAction.getConferenceUrl());
         //   URL url = new URL(Properties.getProperty("WEB.REST.URL") +"doEventPost/addEvent");
         String serviceUrl = RestRepository.getInstance().getSrviceUrl("internalWebinarEventUpdate");
              URL url = new URL(serviceUrl);
            URLConnection connection = url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setConnectTimeout(360*5000);
            connection.setReadTimeout(360*5000);
            OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
           out.write(jb.toString());
            out.close();

            BufferedReader in = new BufferedReader(new InputStreamReader(
                    connection.getInputStream()));
            String s = null;
            String data = "";
            while ((s = in.readLine()) != null) {

                data = data + s;
            }

        JSONObject jObject = new JSONObject(data);

         message = jObject.getString("message");
     


            in.close();
           
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return message;   
     } 
       public String updateExternalConferenceEventposting(AjaxHandlerAction ajaxHandlerAction) throws Exception {

           String message = "";
           boolean isUpdated = false;

           try {

               // System.out.println("in updateExternalConferenceEventposting impl");
               JSONObject jb = new JSONObject();
               jb.put("event_id", String.valueOf(ajaxHandlerAction.getEventId()));
               jb.put("event_title", ajaxHandlerAction.getEventTitle());
               jb.put("event_startdate", ajaxHandlerAction.getStartDate());
               jb.put("evetnt_enddate", ajaxHandlerAction.getEndDate());
               jb.put("location", ajaxHandlerAction.getEventLocation());
               jb.put("createdby", ajaxHandlerAction.getCreatedBy());
               jb.put("status", ajaxHandlerAction.getEventStatus());
               jb.put("event_redirect", ajaxHandlerAction.getConferenceUrl());
               jb.put("WebinarType", ajaxHandlerAction.getEventType());
               jb.put("country", ajaxHandlerAction.getCountry());
              /*  if (!"".equals(ajaxHandlerAction.getAttachmentLocation())) {
                       jb.put("imagePath", ajaxHandlerAction.getAttachmentLocation());

                   } else {
                     
                       jb.put("imagePath", ajaxHandlerAction.getExistedFilePath());

                   } */
               
               if (ajaxHandlerAction.getFileFileName()!=null && !"".equals(ajaxHandlerAction.getFileFileName()) ) {
                  
                   jb.put("FileFlag", "NewFile");
                   jb.put("EventFileFileName", ajaxHandlerAction.getFileFileName());
      			 String encodedBase64 = null;
      		        try {
      		            FileInputStream fileInputStreamReader = new FileInputStream(ajaxHandlerAction.getFile());
      		          	
      		            byte[] bytes = new byte[(int)(ajaxHandlerAction.getFile()).length()];
      		            System.out.println("fileInputStreamReader.."+fileInputStreamReader+"bytes.."+bytes);
      					
      		            fileInputStreamReader.read(bytes);
      		            encodedBase64 = new String(Base64.encodeBytes(bytes));
      		        } catch (FileNotFoundException e) {
      		            e.printStackTrace();
      		        } catch (IOException e) {
      		            e.printStackTrace();
      		        }
      		        encodedBase64="data:application/pdf;base64,"+encodedBase64;
      			jb.put("Base64String",encodedBase64);
      			System.out.println("filename.."+ajaxHandlerAction.getFileFileName());
               } else {
                 
                   jb.put("imagePath", ajaxHandlerAction.getExistedFilePath());
                   jb.put("FileFlag", "ExistedFile");

               }
               jb.put("isNominated", ajaxHandlerAction.getIsNominated());
               if ("true".equals(ajaxHandlerAction.getIsNominated())) {
                   jb.put("nominationEndDate", DateUtility.getInstance().convertStringToMySQLDate(ajaxHandlerAction.getNominationEndDate()));
               }
               jb.put("EventCodrName", ajaxHandlerAction.getEventCodrName());
   			jb.put("EventCoordinatorEmail", ajaxHandlerAction.getEventCoordinatorEmail());
   			jb.put("EventCoordinatorPhone", ajaxHandlerAction.getEventCoordinatorPhone());
   			jb.put("OnsiteCodrName", ajaxHandlerAction.getOnsiteCodrName());
   			jb.put("OnsiteCodeEmail", ajaxHandlerAction.getOnsiteCodeEmail());
   			jb.put("OnsiteCodePhone", ajaxHandlerAction.getOnsiteCodePhone());

               String serviceUrl = RestRepository.getInstance().getSrviceUrl("externalConferenceEventUpdate");
               URL url = new URL(serviceUrl);
               URLConnection connection = url.openConnection();
               connection.setDoOutput(true);
               connection.setRequestProperty("Content-Type", "application/json");
               connection.setConnectTimeout(360 * 5000);
               connection.setReadTimeout(360 * 5000);
               OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
               out.write(jb.toString());
               out.close();

               BufferedReader in = new BufferedReader(new InputStreamReader(
                       connection.getInputStream()));
               String s = null;
               String data = "";
               while ((s = in.readLine()) != null) {

                   data = data + s;
               }

               JSONObject jObject = new JSONObject(data);

               message = jObject.getString("message");

           //    System.out.println("message..." + message);

               in.close();

           } catch (IOException ex) {
               ex.printStackTrace();
           }
           return message;
       }


   
    public String doAddExternalConferenceEventPost(AjaxHandlerAction ajaxHandlerAction) throws Exception {

        String message = "";
        boolean isUpdated = false;

        try {
            
            JSONObject jb = new JSONObject();

            jb.put("event_title", ajaxHandlerAction.getEventTitle());
            jb.put("event_startdate", ajaxHandlerAction.getStartDate());
            jb.put("evetnt_enddate", ajaxHandlerAction.getEndDate());
            jb.put("location", ajaxHandlerAction.getEventLocation());
            jb.put("createdby", ajaxHandlerAction.getCreatedBy());
            jb.put("status", ajaxHandlerAction.getEventStatus());
            jb.put("WebinarType", ajaxHandlerAction.getEventType());
            jb.put("event_redirect", ajaxHandlerAction.getConferenceUrl());
            jb.put("isNominated", ajaxHandlerAction.getIsNominated());
            jb.put("country", ajaxHandlerAction.getCountry());
          //  jb.put("imagePath", ajaxHandlerAction.getAttachmentLocation());

            if ("true".equals(ajaxHandlerAction.getIsNominated())) {
                jb.put("nominationEndDate", DateUtility.getInstance().convertStringToMySQLDate(ajaxHandlerAction.getNominationEndDate()));
                
            }
            jb.put("EventCodrName", ajaxHandlerAction.getEventCodrName());
			jb.put("EventCoordinatorEmail", ajaxHandlerAction.getEventCoordinatorEmail());
			jb.put("EventCoordinatorPhone", ajaxHandlerAction.getEventCoordinatorPhone());
			jb.put("OnsiteCodrName", ajaxHandlerAction.getOnsiteCodrName());
			jb.put("OnsiteCodeEmail", ajaxHandlerAction.getOnsiteCodeEmail());
			jb.put("OnsiteCodePhone", ajaxHandlerAction.getOnsiteCodePhone());
			
			if (ajaxHandlerAction.getFileFileName() != null && !"".equals(ajaxHandlerAction.getFileFileName())) {
			jb.put("EventFileFileName", ajaxHandlerAction.getFileFileName());
			
			 String encodedBase64 = null;
		        try {
		            FileInputStream fileInputStreamReader = new FileInputStream(ajaxHandlerAction.getFile());
		          	
		            byte[] bytes = new byte[(int)(ajaxHandlerAction.getFile()).length()];
		         //   System.out.println("fileInputStreamReader.."+fileInputStreamReader+"bytes.."+bytes);
					
		            fileInputStreamReader.read(bytes);
		            encodedBase64 = new String(Base64.encodeBytes(bytes));
		        } catch (FileNotFoundException e) {
		            e.printStackTrace();
		        } catch (IOException e) {
		            e.printStackTrace();
		        }
		        encodedBase64="data:application/pdf;base64,"+encodedBase64;
			jb.put("Base64String",encodedBase64);
			//System.out.println("filename.."+ajaxHandlerAction.getFileFileName());
			
			}

            String serviceUrl = RestRepository.getInstance().getSrviceUrl("externalConferenceEventAdd");
            URL url = new URL(serviceUrl);
            URLConnection connection = url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setConnectTimeout(360 * 5000);
            connection.setReadTimeout(360 * 5000);
            OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
            out.write(jb.toString());
            out.close();

            BufferedReader in = new BufferedReader(new InputStreamReader(
                    connection.getInputStream()));
            String s = null;
            String data = "";
            while ((s = in.readLine()) != null) {

                data = data + s;
            }

            JSONObject jObject = new JSONObject(data);

            message = jObject.getString("message");



            in.close();

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return message;
    }
   /*  public String editWebianarEventposting(String eventId) throws Exception {
        String message = "";
        boolean isUpdated = false;
        String data = "";
        try {
            JSONObject jb = new JSONObject();
            jb.put("event_id", eventId);
          //  String serviceUrl = RestRepository.getInstance().getSrviceUrl("webinarEventEdit");
              String serviceUrl = RestRepository.getInstance().getSrviceUrl("webinarEventEdit");
            URL url = new URL(serviceUrl);
            URLConnection connection = url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setConnectTimeout(360 * 5000);
            connection.setReadTimeout(360 * 5000);
            OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
            out.write(jb.toString());
            out.close();
            BufferedReader in = new BufferedReader(new InputStreamReader(
                    connection.getInputStream()));
            String s = null;

            while ((s = in.readLine()) != null) {

                data = data + s;
            }
            in.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return data;
    }
    */
    
    public String updateWebinarAfterEvent(AjaxHandlerAction ajaxHandlerAction) throws Exception {

        String message = "";
        boolean isUpdated = false;

        try {


            JSONObject jb = new JSONObject();

            jb.put("eventId", String.valueOf(ajaxHandlerAction.getEventId()));

            jb.put("eventAfterDescription", ajaxHandlerAction.getEventAfterDescription());

            jb.put("eventAfterVideoUrl", ajaxHandlerAction.getEventAfterVideoUrl());

            jb.put("eventStatus", ajaxHandlerAction.getEventStatus());
//            jb.put("primaryTrack", ajaxHandlerAction.getPrimaryTrack());
//            jb.put("secondaryTrack", ajaxHandlerAction.getSecondaryTrack());
            jb.put("eventType", ajaxHandlerAction.getEventType());
            //eventType

            //   URL url = new URL(Properties.getProperty("WEB.REST.URL") +"doEventPost/addEvent");
            String serviceUrl = RestRepository.getInstance().getSrviceUrl("updateWebinarAfterEvent");
            URL url = new URL(serviceUrl);
            URLConnection connection = url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setConnectTimeout(360 * 5000);
            connection.setReadTimeout(360 * 5000);
            OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
            out.write(jb.toString());
            out.close();

            BufferedReader in = new BufferedReader(new InputStreamReader(
                    connection.getInputStream()));
            String s = null;
            String data = "";
            while ((s = in.readLine()) != null) {

                data = data + s;
            }

            JSONObject jObject = new JSONObject(data);

            message = jObject.getString("message");



            in.close();

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return message;
    }
    
    
    
     public String getWebinarSeriesList(String seriesType) throws Exception {
        String message = "";
        boolean isUpdated = false;
        String data = "";
        try {
            JSONObject jb = new JSONObject();
            jb.put("SeriesType", seriesType);
            String serviceUrl = RestRepository.getInstance().getSrviceUrl("getWebinarSeriesList");
            URL url = new URL(serviceUrl);
            URLConnection connection = url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setConnectTimeout(360 * 5000);
            connection.setReadTimeout(360 * 5000);
            OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
            out.write(jb.toString());
            out.close();
            BufferedReader in = new BufferedReader(new InputStreamReader(
                    connection.getInputStream()));
            String s = null;

            while ((s = in.readLine()) != null) {

                data = data + s;
            }
            
           
            in.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return data;
    }
     
     
     
     
     
public String doAddWebinarSeries(AjaxHandlerAction ajaxHandlerAction) throws Exception {
       
 String message ="";
        boolean isUpdated = false;
      
        try {
            
              
                JSONObject jb = new JSONObject();
              
        jb.put("SeriesTitle", ajaxHandlerAction.getSeriesTitle());
  
        jb.put("SeriesType", ajaxHandlerAction.getSeriesType());
    
      //  jb.put("SeriesTrack", ajaxHandlerAction.getSeriesTrack());
        if (!"".equals(ajaxHandlerAction.getSeriesTrack()) && ajaxHandlerAction.getSeriesTrack() != null) {
                jb.put("SeriesTrack", ajaxHandlerAction.getSeriesTrack());
            } else {
                jb.put("SeriesTrack", "0");
            }
      
        jb.put("SeriesStatus",ajaxHandlerAction.getSeriesStatus());
       
       jb.put("CreatedBy",ajaxHandlerAction.getCreatedBy());
         //   URL url = new URL(Properties.getProperty("WEB.REST.URL") +"doEventPost/addEvent");
         String serviceUrl = RestRepository.getInstance().getSrviceUrl("addWebinarSeries");
              URL url = new URL(serviceUrl);
            URLConnection connection = url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setConnectTimeout(360*5000);
            connection.setReadTimeout(360*5000);
            OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
           out.write(jb.toString());
            out.close();

            BufferedReader in = new BufferedReader(new InputStreamReader(
                    connection.getInputStream()));
            String s = null;
            String data = "";
            while ((s = in.readLine()) != null) {

                data = data + s;
            }

        JSONObject jObject = new JSONObject(data);

         message = jObject.getString("message");
     


            in.close();
           
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return message;   
}


public String getWebinarSeriesDetails(String seriesId) throws Exception{
            String message ="";
        boolean isUpdated = false;
        //  String queryString ="";
             // System.out.println("first call");
            String data = "";
        //queryString = "update tblEmpReview set ReviewTypeId=?,EmpComments=?,ModifiedBy=?,ModifiedDate=?,ReviewName=?,ReviewDate=?,Status=?,TLComments=?,TLRating=?,HRRating=?,HrComments=? where Id=?";
        //if(roleName.equalsIgnoreCase("Employee"))
        // queryString = "update tblEmpReview set ReviewTypeId=?,EmpComments=?,ReviewName=?,ReviewDate=?,Status=? where Id=?";
        try {
            
              
                JSONObject jb = new JSONObject();
      
        jb.put("Id",seriesId);        
          //  URL url = new URL("http://172.17.11.251:8080/WebRoot/resources/doJobPost/edit");
        //  URL url = new URL("http://172.17.14.226:8080/WebRoot/resources/doJobPost/edit");
       //  URL url = new URL(Properties.getProperty("WEB.REST.URL") +"doJobPost/edit");
        String serviceUrl = RestRepository.getInstance().getSrviceUrl("getWebinarSeriesDetails");
              URL url = new URL(serviceUrl);
            URLConnection connection = url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setConnectTimeout(360*5000);
            connection.setReadTimeout(360*5000);
            OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
           out.write(jb.toString());
            out.close();

            BufferedReader in = new BufferedReader(new InputStreamReader(
                    connection.getInputStream()));
            String s = null;

            while ((s = in.readLine()) != null) {

                data = data + s;
            }

      //  JSONObject jObject = new JSONObject(data);

        // message = jObject.getString("message");
      //  System.out.println("displaymessage-->" +message);


            in.close();
            /* connection = ConnectionProvider.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(queryString);
            preparedStatement.setString(1,ajaxHandlerAction.getOverlayReviewType());
            preparedStatement.setString(2,ajaxHandlerAction.getOverlayDescription());
            
            
            
            preparedStatement.setString(3,ajaxHandlerAction.getOverlayReviewName());
            preparedStatement.setDate(4, DateUtility.getInstance().getMysqlDate(ajaxHandlerAction.getOverlayReviewDate()));
            preparedStatement.setString(5,ajaxHandlerAction.getReviewStatusOverlay());
            
            preparedStatement.setString(6,ajaxHandlerAction.getReviewId());
            isUpdated = preparedStatement.execute();*/
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return data;
       }


public String doUpdateWebinarSeriesDetails(AjaxHandlerAction ajaxHandlerAction) throws Exception{
    String message ="";
        boolean isUpdated = false;
      
        try {
            
              
                JSONObject jb = new JSONObject();
              jb.put("Id", ajaxHandlerAction.getSeriesId());
        jb.put("SeriesTitle", ajaxHandlerAction.getSeriesTitle());
  
        jb.put("SeriesType", ajaxHandlerAction.getSeriesType());
    
   //     jb.put("SeriesTrack", ajaxHandlerAction.getSeriesTrack());
         if (!"".equals(ajaxHandlerAction.getSeriesTrack()) && ajaxHandlerAction.getSeriesTrack() != null) {
                jb.put("SeriesTrack", ajaxHandlerAction.getSeriesTrack());
            } else {
                jb.put("SeriesTrack", "0");
            }
      
        jb.put("SeriesStatus",ajaxHandlerAction.getSeriesStatus());
       
       jb.put("CreatedBy",ajaxHandlerAction.getCreatedBy());
         //   URL url = new URL(Properties.getProperty("WEB.REST.URL") +"doEventPost/addEvent");
         String serviceUrl = RestRepository.getInstance().getSrviceUrl("updateWebinarSeriesDetails");
              URL url = new URL(serviceUrl);
            URLConnection connection = url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setConnectTimeout(360*5000);
            connection.setReadTimeout(360*5000);
            OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
           out.write(jb.toString());
            out.close();

            BufferedReader in = new BufferedReader(new InputStreamReader(
                    connection.getInputStream()));
            String s = null;
            String data = "";
            while ((s = in.readLine()) != null) {

                data = data + s;
            }

        JSONObject jObject = new JSONObject(data);

         message = jObject.getString("message");
     


            in.close();
           
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return message;   
}     


public String getQmeetAttendeeDetails(HttpServletRequest httpServletRequest,String infoId) throws Exception{
    String message ="";
        boolean isUpdated = false;
      JSONObject jb = new JSONObject();
        try {
            List qmeetAttendeeList = (List)httpServletRequest.getSession(false).getAttribute("qmeetAttendeesList");
            Map attendeeDetails ;
            for (int i = 0; i < qmeetAttendeeList.size(); i++) {
                attendeeDetails = (Map)qmeetAttendeeList.get(i);
                
                if(attendeeDetails.get("Id").toString().equals(infoId)){
                    Iterator entries = attendeeDetails.entrySet().iterator();
                        while (entries.hasNext()) {
                          Entry thisEntry = (Entry) entries.next();
                          jb.put((String)thisEntry.getKey(),(String)thisEntry.getValue());

                        }
                }
            }
              
                
             
           
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return jb.toString();   
}
public JSONObject getUpcomingExecutivemeets(String emeetType) throws ServiceLocatorException {
        List jobsList = null;
        List mainList = null;
        String createdDateFrom;
        String createdDateTo;
        String jobId;
        String title;
        String status;

        JSONObject jObject = null;
        try {

            JSONObject jb = new JSONObject();


            jb.put("EMeetCreatedDate", "none");
            jb.put("EmeetFromDate", "none");
            jb.put("EmeetToDate", "none");
            jb.put("EmeetStatusByDate", "U");
            jb.put("EmeetStatusForMails", "Published");
            jb.put("ExecutiveMeetMonthSearch", "none");
            if (!"Both".equals(emeetType)) {
                jb.put("EMeetType", emeetType);
            } else {
                jb.put("EMeetType", "none");
            }
            String serviceUrl = RestRepository.getInstance().getSrviceUrl("executiveMeetSearch");
            URL url = new URL(serviceUrl);
            URLConnection connection = url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setConnectTimeout(360 * 5000);
            connection.setReadTimeout(360 * 5000);
            OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
            out.write(jb.toString());
            out.close();

            BufferedReader in = new BufferedReader(new InputStreamReader(
                    connection.getInputStream()));
            String s = null;
            String data = "";
            while ((s = in.readLine()) != null) {
                //     System.out.println(s);
                data = data + s;
            }



            //  System.out.println("Data-->"+data);
            jObject = new JSONObject(data);


            in.close();
        } catch (Exception e) {
            System.out.println("\nError while calling REST Service");
            //System.out.println(e);
        }
        return jObject;
    }




     public String getWebinarsListBySeriesId(String seriesId) throws Exception {
        String message = "";
        boolean isUpdated = false;
        String data = "";
        try {
            JSONObject jb = new JSONObject();
          //  System.out.println("seriesId-->"+seriesId);
            jb.put("SeriesId", seriesId);
            String serviceUrl = RestRepository.getInstance().getSrviceUrl("webinarsListBySeriesId");
            URL url = new URL(serviceUrl);
            URLConnection connection = url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setConnectTimeout(360 * 5000);
            connection.setReadTimeout(360 * 5000);
            OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
            out.write(jb.toString());
            out.close();
            BufferedReader in = new BufferedReader(new InputStreamReader(
                    connection.getInputStream()));
            String s = null;

            while ((s = in.readLine()) != null) {

                data = data + s;
            }
            in.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        // System.out.println("web data-->"+data);
        return data;
    }
     
public String addOrRemoveWebinarToSeries(String seriesId,String eventId) throws Exception{
    String message ="";
        boolean isUpdated = false;
      
        try {
           
              
                JSONObject jb = new JSONObject();
              jb.put("SeriesId",seriesId);
        jb.put("event_id", eventId);
  
       
         //   URL url = new URL(Properties.getProperty("WEB.REST.URL") +"doEventPost/addEvent");
         String serviceUrl = RestRepository.getInstance().getSrviceUrl("addEventToSeries");
              URL url = new URL(serviceUrl);
            URLConnection connection = url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setConnectTimeout(360*5000);
            connection.setReadTimeout(360*5000);
            OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
           out.write(jb.toString());
            out.close();

            BufferedReader in = new BufferedReader(new InputStreamReader(
                    connection.getInputStream()));
            String s = null;
            String data = "";
            while ((s = in.readLine()) != null) {

                data = data + s;
            }

        JSONObject jObject = new JSONObject(data);

         message = jObject.getString("message");
     


            in.close();
           
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return message;   
}


    /*Events functionality new enhancements end
     * Date : 01/23/2017
     * Author : Santosh Kola/Phani Kanuri
     */



/*Events Nominations 
 * date : 05/29/2017
 * Author : Santosh Kola
 */
 public String getEventSuggestionDetails(HttpServletRequest httpServletRequest, int eventId) throws Exception {
     //   System.out.println("in getEventSuggestionDetails.." + eventId);
        String message = "";
        boolean isUpdated = false;
        JSONObject jb = new JSONObject();
        try {
            List eventSuggestionsList = (List) httpServletRequest.getSession(false).getAttribute("eventSuggestionsList");
           // System.out.println("eventSuggestionsList.size().." + eventSuggestionsList.size());
            Map eventSuggestionsDetails;
            for (int i = 0; i < eventSuggestionsList.size(); i++) {
                eventSuggestionsDetails = (Map) eventSuggestionsList.get(i);
             //   System.out.println("eventSuggestionsDetails.get(\"event_id\").toString().." + eventSuggestionsDetails.get("event_id") + "eventId..." + eventId);
                if (Integer.parseInt(eventSuggestionsDetails.get("event_id").toString()) == eventId) {
                  //  System.out.println("id matching..." + eventId);
                    Iterator entries = eventSuggestionsDetails.entrySet().iterator();
                    while (entries.hasNext()) {
                        Entry thisEntry = (Entry) entries.next();
                     //   System.out.println("key.." + (String) thisEntry.getKey() + "value...." + (String) thisEntry.getValue());
                        jb.put((String) thisEntry.getKey(), (String) thisEntry.getValue());

                    }
                }
            }

        //    System.out.println("jb.toString().." + jb.toString());


        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return jb.toString();
    }

    public String getEventNomineeSearch(NewAjaxHandlerAction ajaxHandlerAction) throws Exception {
        // Connection connection = null;
        //PreparedStatement preparedStatement = null;
        // ResultSet resultSet = null;
        String message = "";
        boolean isUpdated = false;
        String data = "";

        try {

            JSONObject jb = new JSONObject();
            // AjaxHandlerAction ac=new AjaxHandlerAction();
            jb.put("Event_Id", ajaxHandlerAction.getEvent_Id());
            jb.put("NomineeState", ajaxHandlerAction.getNomineeState());
            if (ajaxHandlerAction.getNomineeName() != null && !"".equals(ajaxHandlerAction.getNomineeName())) {
                jb.put("NomineeName", ajaxHandlerAction.getNomineeName());
            } else {
                jb.put("NomineeName", "none");
            }

          //  System.out.println("Event_Id---in ajaximpl---" + ajaxHandlerAction.getEvent_Id());


            //   URL url = new URL("http://172.17.11.251:8080/WebRoot/resources/doJobPost/post");
            // URL url = new URL("http://172.17.14.226:8080/WebRoot/resources/doJobPost/post");
            // URL url = new URL(Properties.getProperty("WEB.REST.URL") +"doJobPost/post");
            // String serviceUrl = RestRepository.getInstance().getSrviceUrl("doAddExecutiveMeeting");
            String serviceUrl = RestRepository.getInstance().getSrviceUrl("getEventNomineeSearch");
            URL url = new URL(serviceUrl);
            URLConnection connection = url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setConnectTimeout(360 * 5000);
            connection.setReadTimeout(360 * 5000);
            OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
            out.write(jb.toString());
            out.close();
            BufferedReader in = new BufferedReader(new InputStreamReader(
                    connection.getInputStream()));
            String s = null;

            while ((s = in.readLine()) != null) {
                //     System.out.println(s);
                data = data + s;
            }



            //  System.out.println("Data-->"+data);



            in.close();
        } catch (Exception e) {
            System.out.println("\nError while calling REST Service");
            //System.out.println(e);
        }
        return data;
    }

    public String getNomineeDetails(int nomineeId) throws Exception {
        String message = "";
        boolean isUpdated = false;
        //  String queryString ="";
        // System.out.println("first call");
        String data = "";
        try {


            JSONObject jb = new JSONObject();

            jb.put("NomineeId", nomineeId);
            //  URL url = new URL("http://172.17.11.251:8080/WebRoot/resources/doJobPost/edit");
            //  URL url = new URL("http://172.17.14.226:8080/WebRoot/resources/doJobPost/edit");
            // URL url = new URL(Properties.getProperty("WEB.REST.URL") +"doJobPost/edit");
            String serviceUrl = RestRepository.getInstance().getSrviceUrl("doGetNomineeDetails");
            URL url = new URL(serviceUrl);
            URLConnection connection = url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setConnectTimeout(360 * 5000);
            connection.setReadTimeout(360 * 5000);
            OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
            out.write(jb.toString());
            out.close();

            BufferedReader in = new BufferedReader(new InputStreamReader(
                    connection.getInputStream()));
            String s = null;

            while ((s = in.readLine()) != null) {

                data = data + s;
            }

            in.close();

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return data;
    }

    public String doUpdateNomineeStatus(NewAjaxHandlerAction ajaxHandlerAction) throws Exception {
        // Connection connection = null;
        //PreparedStatement preparedStatement = null;
        // ResultSet resultSet = null;
        String message = "error";
        boolean isUpdated = false;
        //  String queryString ="";
        //   System.out.println("first call");

        //queryString = "update tblEmpReview set ReviewTypeId=?,EmpComments=?,ModifiedBy=?,ModifiedDate=?,ReviewName=?,ReviewDate=?,Status=?,TLComments=?,TLRating=?,HRRating=?,HrComments=? where Id=?";
        //if(roleName.equalsIgnoreCase("Employee"))
        // queryString = "update tblEmpReview set ReviewTypeId=?,EmpComments=?,ReviewName=?,ReviewDate=?,Status=? where Id=?";
        try {


            JSONObject jb = new JSONObject();
            // AjaxHandlerAction ac=new AjaxHandlerAction();
            jb.put("ConferenceId", ajaxHandlerAction.getEvent_Id());
            jb.put("NomineeId", ajaxHandlerAction.getNomineeIds());
            jb.put("NomineeStatus", ajaxHandlerAction.getStatus());



            //System.out.println("ConferenceId---" + ajaxHandlerAction.getEvent_Id() + "--NomineeId---" + ajaxHandlerAction.getNomineeIds() + "--NomineeStatus--" + ajaxHandlerAction.getStatus());

            //URL url = new URL("http://172.17.11.251:8080/WebRoot/resources/doJobPost/update");
            //URL url = new URL("http://172.17.14.226:8080/WebRoot/resources/doJobPost/update");
            //  URL url = new URL(Properties.getProperty("WEB.REST.URL") +"doJobPost/update");

            String serviceUrl = RestRepository.getInstance().getSrviceUrl("updateEventnomineeStatus");
            URL url = new URL(serviceUrl);
            URLConnection connection = url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setConnectTimeout(360 * 5000);
            connection.setReadTimeout(360 * 5000);
            OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
            out.write(jb.toString());
            out.close();

            BufferedReader in = new BufferedReader(new InputStreamReader(
                    connection.getInputStream()));
            String s = null;
            String data = "";
            while ((s = in.readLine()) != null) {

                data = data + s;
            }

            JSONObject jObject = new JSONObject(data);

            
            if (ajaxHandlerAction.getStatus().equals("Approved")) {
                doSendEmailForApprovedNominees(ajaxHandlerAction,jObject);
            } else if (ajaxHandlerAction.getStatus().equals("Rejected")) {
                doSendEmailForRejectedNominees(ajaxHandlerAction,jObject);
            }
            //   System.out.println("displaymessage-->" +message);

message = "updated";

            in.close();

        } catch (IOException ex) {
            ex.printStackTrace();
        }
      //  System.out.println("message in impl----" + message);
        return message;
    }

    public String doSendEmailForApprovedNominees(NewAjaxHandlerAction ajaxHandlerAction,JSONObject recipientJson) throws ServiceLocatorException {

        String message = "";
String nomineeIds = "";
        try {
            Calendar cal = Calendar.getInstance();
            int year = cal.get(cal.YEAR);
           // System.out.println("doSendEmailForNominees");
            if (Properties.getProperty("Mail.Flag").equals("1")) {
                
                 Iterator<String> keysItr = recipientJson.keys();
                 int temp=0;
                while (keysItr.hasNext()) {
                    if(temp==0)
                    nomineeIds = keysItr.next();
                    else
                       nomineeIds = nomineeIds+","+ keysItr.next();
                    temp++;
                    // String key = keysItr.next();
                //String value = (String) recipientJson.get(key);
                }
               // Map employeeMap = DataSourceDataProvider.getInstance().getNominationEmployeeDetails(ajaxHandlerAction.getNomineeIds());
                 Map employeeMap = DataSourceDataProvider.getInstance().getNominationEmployeeDetails(nomineeIds);
                Iterator it = employeeMap.entrySet().iterator();
                 // String toAddress = "";
                    String cCAddress = "";
                    String subject = "";
                    String bodyContent = "";
                    String createdDate = "";
                    String mailDeliverDate = "";
                    //String mailFlag = "0";
                    String bCCAddress = "";
                while (it.hasNext()) {
                    Map.Entry empDetailsMap = (Map.Entry) it.next();
                    cCAddress = "";
                   

                   if(recipientJson.has(empDetailsMap.getKey().toString().split("@")[0])){
                       cCAddress = (String) recipientJson.get(empDetailsMap.getKey().toString().split("@")[0]);
                   }
                           
                    //    createdDate = DateUtility.getInstance().getCurrentMySqlDateTime();
                    // mailDeliverDate = DateUtility.getInstance().getCurrentMySqlDateTime1();
                    StringBuilder htmlText = new StringBuilder();
                    // bCCAddress = Properties.getProperty("bccAddress");
htmlText.append("<html xmlns='http://www.w3.org/1999/xhtml' class='gr__newsletters-2017-vnallamalla_c9users_io'><head>");
htmlText.append("<meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>");
htmlText.append("<meta name='viewport' content='width=device-width, initial-scale=1'>");
htmlText.append("<meta http-equiv='X-UA-Compatible' content='IE=edge'>");
htmlText.append("<meta name='format-detection' content='telephone=no'>");
htmlText.append("<title>Nominee Approved</title>");
htmlText.append("<style>");
htmlText.append("@import url('https://fonts.googleapis.com/css?family=Montserrat:400,700');");
htmlText.append("@import url('https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i');");
htmlText.append("html{width: 100%;}");
htmlText.append("body{");
htmlText.append("width: 100%;");
htmlText.append("background-color: #ffffff;");
htmlText.append("margin:0; padding:0;");
htmlText.append("-webkit-text-size-adjust:none;");
htmlText.append("-ms-text-size-adjust:none;");
htmlText.append("-webkit-font-smoothing: ;");
htmlText.append("mso-margin-top-alt:0px;");
htmlText.append("mso-margin-bottom-alt:0px;");
htmlText.append("mso-padding-alt: 0px 0px 0px 0px;}");
htmlText.append("div, p, a, li, td { -webkit-text-size-adjust:none; }");
htmlText.append("#outlook a {padding:0;}");
htmlText.append("p,h1,h2,h3,h4{ margin-top:0; margin-bottom:0; padding-top:0; padding-bottom:0;}");
htmlText.append("table{");
htmlText.append("mso-table-lspace:0pt;");
htmlText.append("mso-table-rspace:0pt;");
htmlText.append("}");
htmlText.append("table td {border-collapse: collapse;}");
htmlText.append("span.preheader{display: none;font-size: 1px; visibility: hidden; opacity: 0; color: transparent; height: 0; width: 0;}");
htmlText.append(".googlefix {width: 280px!important; text-align:center!important; min-width:0px!important}");
htmlText.append("a {outline:none;}");
htmlText.append("a {text-decoration: none;}");
htmlText.append("a img {border:none;}");
htmlText.append(".image_fix {display:block;}");
htmlText.append(".ReadMsgBody { width: 100%;}");
htmlText.append(".ExternalClass {width: 100%;}");
htmlText.append(".ExternalClass,");
htmlText.append(".ExternalClass p,");
htmlText.append(".ExternalClass span,");
htmlText.append(".ExternalClass font,");
htmlText.append(".ExternalClass td,");
htmlText.append(".ExternalClass div {");
htmlText.append("line-height: 100%;}");
htmlText.append("img{");
htmlText.append("-ms-interpolation-mode:bicubic;}");
htmlText.append("p {");
htmlText.append("margin:0 !important;");
htmlText.append("padding:0 !important;}");
htmlText.append(".button:hover {filter: brightness(120%);}");
htmlText.append("@media only screen and (max-width: 600px)");
htmlText.append("{");
htmlText.append("body{width:100% !important;}");
htmlText.append(".container           {width:100% !important; min-width:100% !important;}");
htmlText.append(".container-wrap      {display:inline-block !important; width:100% !important; height:auto !important; border-radius:0 !important;}");
htmlText.append(".image-container     {width: 100% !important; height: auto !important; }");
htmlText.append(".disable             {display: none !important;}");
htmlText.append(".enable              {display: inline !important;}");
htmlText.append(".padding-top-10      {padding-top: 10px !important;}");
htmlText.append(".padding-top-20      {padding-top: 20px !important;}");
htmlText.append(".padding-top-30      {padding-top: 30px !important;}");
htmlText.append(".padding-top-40      {padding-top: 40px !important;}");
htmlText.append(".padding-top-60      {padding-top: 60px !important;}");
htmlText.append(".padding-bottom-10   {padding-bottom: 10px !important;}");
htmlText.append(".padding-bottom-20   {padding-bottom: 20px !important;}");
htmlText.append(".padding-bottom-30   {padding-bottom: 30px !important;}");
htmlText.append(".padding-bottom-40   {padding-bottom: 40px !important;}");
htmlText.append(".padding-bottom-60   {padding-bottom: 60px !important;}");
htmlText.append(".padding-none        {padding:0  !important;}");
htmlText.append(".text                {width:90% !important; text-align:center !important;}");
htmlText.append(".text-on-bg          {width:90% !important; text-align:center !important;}");
htmlText.append(".text-center         {text-align: center !important;}");
htmlText.append(".text-right          {text-align: right !important;}");
htmlText.append(".text-left           {text-align: left !important;}");
htmlText.append(".text-white          {color: #ffffff !important;}");
htmlText.append(".background-cover    {background-size: cover !important;}");
htmlText.append(".background-cover-percent    {background-size: 100% 79% !important;}");
htmlText.append(".background-contain  {background-size: contain !important;}");
htmlText.append(".background-auto     {background-size: auto !important;}");
htmlText.append(".background-center   {background-position: center center !important;}");
htmlText.append(".background-none     {background-image: none !important;}");
htmlText.append(".border-none         {border:0 !important;}");
htmlText.append(".border-white        {border-color: #ffffff !important;}");
htmlText.append("}");
htmlText.append("@media only screen and (max-width: 451px)");
htmlText.append("{");
htmlText.append("body {width:100% !important;}");
htmlText.append(".container           {width:100% !important; min-width:100% !important;}");
htmlText.append(".container-wrap      {display:inline-block !important; width:100% !important; height:auto !important; border-radius:0 !important;}");
htmlText.append("}");
htmlText.append("@media only screen and (max-width: 341px)");
htmlText.append("{");
htmlText.append("body {width:100%!important;}");
htmlText.append(".container           {width:100% !important; min-width:100% !important;}");
htmlText.append(".container-wrap      {display:inline-block !important; width:100% !important; height:auto !important; border-radius:0 !important;}");
htmlText.append("}");
htmlText.append("</style>");
htmlText.append("</head>");
htmlText.append("<body marginwidth='0' marginheight='0' style='margin-top: 0; margin-bottom: 0; padding-top: 0; padding-bottom: 0; width: 100%; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%;' offset='0' topmargin='0' leftmargin='0' data-gr-c-s-loaded='true'>");
htmlText.append("<div class='parentOfBg'></div>");
htmlText.append("<div id='edit_link' class='hidden' style='display: none; left: 88px; top: 27956px;'>");
htmlText.append("");
htmlText.append("<div class='close_link'></div>");
htmlText.append("");
htmlText.append("<input type='text' id='edit_link_value' class='createlink' placeholder='Your URL'>");
htmlText.append("");
htmlText.append("<div id='change_image_wrapper' style='display: none;'>");
htmlText.append("");
htmlText.append("<div id='change_image'>");
htmlText.append("");
htmlText.append("<p id='change_image_button'>Change &nbsp; <span class='pixel_result'></span></p>");
htmlText.append("</div>");
htmlText.append("");
htmlText.append("<input type='button' value='' id='change_image_link'>");
htmlText.append("");
htmlText.append("<input type='button' value='' id='remove_image'>");
htmlText.append("</div>");
htmlText.append("");
htmlText.append("<div id='tip'></div>");
htmlText.append("</div>");
htmlText.append("<table mc:repeatable='layout' mc:hideable='' mc:variant='navigation-6' align='center' border='0' cellpadding='0' cellspacing='0' width='100%' style='margin:0 auto; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px; max-width:680px;' data-module='navigation-6'>");
htmlText.append("");
htmlText.append("<tbody>");
htmlText.append("<tr>");
htmlText.append("<td align='center' style='width:100%; height:100%; background-color:#ffffff;' bgcollor='#ffffff' data-bgcolor='nav-6-bg'>");
htmlText.append("<table width='600' align='center' class='container' border='0' cellpadding='0' cellspacing='0' style='width:600px; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px;'>");
htmlText.append("");
htmlText.append("<tbody>");
htmlText.append("<tr>");
htmlText.append("<td align='center' height='20' width='1' style='font-size: 1px; line-height: 20px; height:20px;'>&nbsp; ");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("");
htmlText.append("<tr>");
htmlText.append("<td align='center'>");
htmlText.append("<table width='600' align='center' class='container' border='0' cellpadding='0' cellspacing='0' style='width:600px; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px;'>");
htmlText.append("<tbody>");
htmlText.append("<tr>");
htmlText.append("");
htmlText.append("<th width='140' align='left' class='container-wrap' valign='top' style='vertical-align: top;'>");
htmlText.append("<table align='center' cellpadding='0' cellspacing='0'>");
htmlText.append("<tbody>");
htmlText.append("<tr>");
htmlText.append("<td width='160' align='center' valign='top'>");
htmlText.append("<table width='100%' align='center' cellpadding='0' cellspacing='0'>");
htmlText.append("<tbody>");
htmlText.append("");
htmlText.append("<tr>");
htmlText.append("<td align='center' valign='top' style='line-height: 0px !important;'>");
htmlText.append("<a style='text-decoration: none; display: block;' href='https://www.miraclesoft.com/' target='blank'>");
htmlText.append("<img src='https://www.miraclesoft.com/images/newsletters/miracle-logo-dark.png' alt='logo' width='100%' style='width: 100%; max-width: 100%;'>");
htmlText.append("</a>");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("</tbody>");
htmlText.append("</table>");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("</tbody>");
htmlText.append("</table>");
htmlText.append("</th>");
htmlText.append("");
htmlText.append("<th width='320' align='right' class='container-wrap' valign='top' style='vertical-align: top;'>");
htmlText.append("<table width='320' align='right' class='container' border='0' cellpadding='0' cellspacing='0' style='width:320px; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px;'>");
htmlText.append("");
htmlText.append("<tbody>");
htmlText.append("<tr>");
htmlText.append("<td align='center' height='1' width='1' style='font-size: 1px; line-height: 1px; height:1px;'>&nbsp;");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("");
htmlText.append("<tr>");
htmlText.append("<td align='right'>");
htmlText.append("<table width='auto' align='right' class='container' border='0' cellpadding='0' cellspacing='0' style='width:autopx; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px;'>");
htmlText.append("<tbody>");
htmlText.append("<tr>");
htmlText.append("<td align='center'>");
htmlText.append("<table width='auto' align='center' class='' border='0' cellpadding='0' cellspacing='0' style='border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px;'>");
htmlText.append("<tbody><tr>");
htmlText.append("<td align='center' height='15'>");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("<tr>");
htmlText.append("");
htmlText.append("<th align='center' height='1' width='4' style='font-size: 1px; line-height: 1px; width:4px;'>");
htmlText.append("</th>");
htmlText.append("");
htmlText.append("<th style='text-align:center; font-family: oPen sans; font-size:12px; line-height: 20px; text-decoration: none; color: #000000; font-weight:400; text-transform: uppercase; : 0.05em;'>");
htmlText.append("<a href='https://www.miraclesoft.com/company/' target='blank' style='text-align:center; font-family: oPen sans; font-size:12px; line-height: 20px; text-decoration: none; color: #000000; font-weight:600; text-transform: uppercase; : 0.05em;' data-size='nav-6-link-size' data-min='9' data-max='40' data-color='nav-6-link-color' mc:edit='nav-6-link-2'>");
htmlText.append("<multiline><b>About us</b> |</multiline>");
htmlText.append("</a>");
htmlText.append("</th>");
htmlText.append("");
htmlText.append("<th align='center' height='1' width='4' style='font-size: 1px; line-height: 1px; width:4px;'>");
htmlText.append("</th>");
htmlText.append("");
htmlText.append("<th style='text-align:center; font-family: oPen sans; font-size:12px; line-height: 20px; text-decoration: none; color: #000000; font-weight:400; text-transform: uppercase; : 0.05em;'>");
htmlText.append("<a href='https://www.miraclesoft.com/services/' target='blank' style='text-align:center; font-family: oPen sans; font-size:12px; line-height: 20px; text-decoration: none; color: #000000; font-weight:600; text-transform: uppercase; : 0.05em;' data-size='nav-6-link-size' data-min='9' data-max='40' data-color='nav-6-link-color' mc:edit='nav-6-link-3'>");
htmlText.append("<multiline><b>Our Services</b> |</multiline>");
htmlText.append("</a>");
htmlText.append("</th>");
htmlText.append("");
htmlText.append("<th align='center' height='1' width='4' style='font-size: 1px; line-height: 1px; width:4px;'>");
htmlText.append("</th>");
htmlText.append("");
htmlText.append("<th style='text-align:center; font-family: oPen sans; font-size:12px; line-height: 20px; text-decoration: none; color: #232527; font-weight:600; text-transform: uppercase; : 0.05em;'>");
htmlText.append("<a href='https://www.miraclesoft.com/contact/' target='blank' style='text-align:center; font-family: oPen sans; font-size:12px; line-height: 20px; text-decoration: none; color: #232527; font-weight:600; text-transform: uppercase; : 0.05em;' data-size='nav-6-signUp-size' data-min='9' data-max='40' data-color='nav-6-signUp-color' mc:edit='nav-6-signUp-4'>");
htmlText.append("<multiline><b>Contact us</b></multiline>");
htmlText.append("</a>");
htmlText.append("</th>");
htmlText.append("");
htmlText.append("</tr>");
htmlText.append("</tbody>");
htmlText.append("</table>");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("</tbody>");
htmlText.append("</table>");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("");
htmlText.append("<tr>");
htmlText.append("<td align='center' height='18' width='1' style='font-size: 1px; line-height: 18px; height:18px;'>&nbsp;");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("");
htmlText.append("</tbody>");
htmlText.append("</table>");
htmlText.append("</th>");
htmlText.append("");
htmlText.append("</tr>");
htmlText.append("</tbody>");
htmlText.append("</table>");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("</tbody>");
htmlText.append("</table>");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("");
htmlText.append("</tbody>");
htmlText.append("</table>");
htmlText.append("<table mc:repeatable='layout' mc:hideable='' mc:variant='navigation-margin' align='center' border='0' cellpadding='0' cellspacing='0' width='100%' style='margin:0 auto; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px; max-width:680px;' data-module='navigation-margin'>");
htmlText.append("");
htmlText.append("<tbody>");
htmlText.append("<tr>");
htmlText.append("<td align='center' style='width:100%; height:100%; background-color:#ffffff;' bgcollor='#ffffff' data-bgcolor='navigation-margin-bg'>");
htmlText.append("<table width='600' align='center' class='container' border='0' cellpadding='0' cellspacing='0' style='width:600px; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px;'>");
htmlText.append("");
htmlText.append("<tbody>");
htmlText.append("<tr>");
htmlText.append("<td align='center' height='20' width='1' style='font-size: 1px;line-height: 10px;height: 10px;'>&nbsp;");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("");
htmlText.append("</tbody>");
htmlText.append("</table>");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("");
htmlText.append("</tbody>");
htmlText.append("</table>");
htmlText.append("<div class='parentOfBg'></div>");
htmlText.append("<table align='center' border='0' cellpadding='0' cellspacing='0' width='100%' style='margin:0 auto; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px; max-width:680px;' data-module='text-block-1'>");
htmlText.append("");
htmlText.append("<tbody>");
htmlText.append("<tr>");
htmlText.append("<td align='center' style='width:100%; height:100%; background-color:#f8f8f8;' data-bgcolor='txt-blck-1-bg'>");
htmlText.append("<table width='600' align='center' class='text' border='0' cellpadding='0' cellspacing='0' style='width:600px; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px;'>");
htmlText.append("");
htmlText.append("<tbody>");
htmlText.append("<tr>");
htmlText.append("<td align='center' height='35' width='1' style='font-size: 1px; line-height: 35px; height:35px;'>");
htmlText.append("&nbsp;");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("");
htmlText.append("<tr>");
htmlText.append("<td style='text-align: center;font-family: 'Open Sans' , sans-serif;font-size: 25px;line-height: 20px;text-decoration: none;color: #232527;font-weight:700;' data-size='txt-blck-1-text-size' data-min='9' data-max='40' data-color='txt-blck-1-text-color'>");
htmlText.append("<multiline>Congratulations!</multiline>");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("");
htmlText.append("<tr>");
htmlText.append("<td align='center' height='25' width='1' style='font-size: 1px;line-height: 25px;height: 25px;'>");
htmlText.append("&nbsp;");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("<tr>");
htmlText.append("<td align='center'>");
htmlText.append("<table width='60' height='2' align='center' border='0' cellpadding='0' cellspacing='0' style='width:60px; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px; background-color: #232527' data-bgcolor='team-6-line-color'>");
htmlText.append("");
htmlText.append("<tbody>");
htmlText.append("<tr>");
htmlText.append("<td align='center' height='2' width='40' style='font-size: 2px; line-height: 2px; width:40px;'></td>");
htmlText.append("</tr>");
htmlText.append("");
htmlText.append("</tbody>");
htmlText.append("</table>");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("");
htmlText.append("</tbody>");
htmlText.append("</table>");
htmlText.append("</td>");
htmlText.append("");
htmlText.append("</tr>");
htmlText.append("");
htmlText.append("</tbody>");
htmlText.append("</table>");
htmlText.append("<table mc:repeatable='layout' mc:hideable='' mc:variant='text-block-1' align='center' border='0' cellpadding='0' cellspacing='0' width='100%' style='margin:0 auto; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px; max-width:680px;' data-module='text-block-1'>");
htmlText.append("");
htmlText.append("<tbody>");
htmlText.append("<tr>");
htmlText.append("<td align='center' style='width:100%; height:100%; background-color:#f8f8f8;' bgcollor='#f8f8f8' data-bgcolor='txt-blck-1-bg'>");
htmlText.append("<table width='600' align='center' class='text' border='0' cellpadding='0' cellspacing='0' style='width:600px; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px;'>");
htmlText.append("");
htmlText.append("<tbody>");
htmlText.append("<tr>");
htmlText.append("<td align='center' height='25' width='1' style='font-size: 1px; line-height: 25px; height:25px;'>&nbsp;");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("");
htmlText.append("<tr>");
htmlText.append("<td style='text-align:left;font-family: 'Open Sans' , sans-serif;font-size: 14px;line-height: 20px;text-decoration: none;color: #232527;font-weight:400;' data-size='txt-blck-1-text-size' data-min='9' data-max='40' data-color='txt-blck-1-text-color' mc:edit='txt-blck-1-text-1'>");
htmlText.append("<multiline><b>" + empDetailsMap.getValue() + ",</b>");
htmlText.append("</multiline>");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("");
htmlText.append("</tbody>");
htmlText.append("</table>");
htmlText.append("</td>");
htmlText.append("");
htmlText.append("</tr>");
htmlText.append("");
htmlText.append("</tbody>");
htmlText.append("</table>");
htmlText.append("");
htmlText.append("");
htmlText.append("<table mc:repeatable='layout' mc:hideable='' mc:variant='text-block-1' align='center' border='0' cellpadding='0' cellspacing='0' width='100%' style='margin:0 auto; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px; max-width:680px;' data-module='text-block-1'>");
htmlText.append("");
htmlText.append("<tbody>");
htmlText.append("<tr>");
htmlText.append("<td align='center' style='width:100%; height:100%; background-color:#f8f8f8;' bgcollor='#f8f8f8' data-bgcolor='txt-blck-1-bg'>");
htmlText.append("<table width='600' align='center' class='text' border='0' cellpadding='0' cellspacing='0' style='width:600px; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px;'>");
htmlText.append("");
htmlText.append("<tbody>");
htmlText.append("<tr>");
htmlText.append("<td align='center' height='25' width='1' style='font-size: 1px;line-height: 15px;height: 15px;'>&nbsp;");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("");
htmlText.append("<tr>");
htmlText.append("<td style='text-align:justify; font-family: 'Open Sans' , sans-serif; font-size:14px; line-height: 24px; text-decoration: none; color: #232527; font-weight:400;' data-size='txt-blck-1-text-size' data-min='9' data-max='40' data-color='txt-blck-1-text-color' mc:edit='txt-blck-1-text-1'>");
htmlText.append("<multiline><b></b> Your nomination has been approved to attend <b>" + ajaxHandlerAction.getEventName() + "</b> at <b>" + ajaxHandlerAction.getEventLocation() + "</b> from <b>" + DateUtility.getInstance().getDateWithMonthNameFromUsFormat(ajaxHandlerAction.getEventStartDate()) + "</b> to <b>" + DateUtility.getInstance().getDateWithMonthNameFromUsFormat(ajaxHandlerAction.getEventEndDate()) + "</b>. Our team will contact you regarding further details for the event soon. </multiline>");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("");
htmlText.append("</tbody>");
htmlText.append("</table>");
htmlText.append("</td>");
htmlText.append("");
htmlText.append("</tr>");
htmlText.append("");
htmlText.append("</tbody>");
htmlText.append("</table>");
htmlText.append("<table mc:repeatable='layout' mc:hideable='' mc:variant='text-block-1' align='center' border='0' cellpadding='0' cellspacing='0' width='100%' style='margin:0 auto; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px; max-width:680px;' data-module='text-block-1'>");
htmlText.append("");
htmlText.append("<tbody>");
htmlText.append("<tr>");
htmlText.append("<td align='center' style='width:100%; height:100%; background-color:#f8f8f8;' bgcollor='#f8f8f8' data-bgcolor='txt-blck-1-bg'>");
htmlText.append("<table width='600' align='center' class='text' border='0' cellpadding='0' cellspacing='0' style='width:600px; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px;'>");
htmlText.append("");
htmlText.append("<tbody>");
htmlText.append("<tr>");
htmlText.append("<td align='center' height='25' width='1' style='font-size: 1px;line-height: 15px;height: 15px;'>&nbsp;");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("");
htmlText.append("<tr>");
htmlText.append("<td style='text-align:left; font-family: 'Open Sans' , sans-serif; font-size:14px; line-height: 24px; text-decoration: none; color: #232527; font-weight:400;' data-size='txt-blck-1-text-size' data-min='9' data-max='40' data-color='txt-blck-1-text-color' mc:edit='txt-blck-1-text-1'>");
htmlText.append("<multiline>For more details regarding <b>" + ajaxHandlerAction.getEventName() + "</b> please visit <a href='" + ajaxHandlerAction.getEventLink() + "'><span style='color:#00aae7;'>" + ajaxHandlerAction.getEventLink() + "</span></a>.</multiline>");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("<tr>");
htmlText.append("<td align='center' height='15' width='1' style='font-size: 1px;line-height: 15px;height: 15px;'>&nbsp;");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("");
htmlText.append("<tr>");
htmlText.append("<td style='text-align:left; font-family: 'Open Sans' , sans-serif; font-size:14px; line-height: 24px; text-decoration: none; color: #232527; font-weight:400;' data-size='txt-blck-1-text-size' data-min='9' data-max='40' data-color='txt-blck-1-text-color' mc:edit='txt-blck-1-text-1'>");
htmlText.append("<multiline>For more information please reach out to <a href='mailto:partnerships@miraclesoft.com'><span style='color:#00aae7;'>partnerships@miraclesoft.com</span></a> (or) visit <a href='https://www.miraclesoft.com/conferencenominations' target='blank'><span style='color:#00aae7;'>www.miraclesoft.com/conferencenominations.</span></a>");
htmlText.append("</multiline>");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("</tbody>");
htmlText.append("</table>");
htmlText.append("</td>");
htmlText.append("");
htmlText.append("</tr>");
htmlText.append("");
htmlText.append("</tbody>");
htmlText.append("</table><table mc:repeatable='layout' mc:hideable='' mc:variant='text-block-1' align='center' border='0' cellpadding='0' cellspacing='0' width='100%' style='margin:0 auto; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px; max-width:680px;' data-module='text-block-1'>");
htmlText.append("");
htmlText.append("<tbody>");
htmlText.append("<tr>");
htmlText.append("<td align='center' style='width:100%; height:100%; background-color:#f8f8f8;' bgcollor='#f8f8f8' data-bgcolor='txt-blck-1-bg'>");
htmlText.append("<table width='600' align='center' class='text' border='0' cellpadding='0' cellspacing='0' style='width:600px; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px;'>");
htmlText.append("");
htmlText.append("<tbody>");
htmlText.append("<tr>");
htmlText.append("<td align='center' height='25' width='1' style='font-size: 1px;line-height: 15px;height: 15px;'>&nbsp;");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("");
htmlText.append("<tr>");
htmlText.append("<td style='text-align:left; font-family: 'Open Sans' , sans-serif; font-size:14px; line-height: 24px; text-decoration: none; color: #232527; font-weight:400;' data-size='txt-blck-1-text-size' data-min='9' data-max='40' data-color='txt-blck-1-text-color' mc:edit='txt-blck-1-text-1'>");
htmlText.append("<multiline><b>Thanks &amp; Regards</b><br><b>Partnerships Team</b><br>");
htmlText.append("");
htmlText.append("Phone : (248)-412-0430<br>");
htmlText.append("Email : <a href='mailto:partnerships@miraclesoft.com'><span style='color:#232527;'>partnerships@miraclesoft.com</span></a>");
htmlText.append("</multiline>");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("");
htmlText.append("<tr>");
htmlText.append("<td align='center' height='46' width='1' style='font-size: 1px; line-height: 46px; height:46px;'>&nbsp;");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("");
htmlText.append("</tbody>");
htmlText.append("</table>");
htmlText.append("</td>");
htmlText.append("");
htmlText.append("</tr>");
htmlText.append("");
htmlText.append("</tbody>");
htmlText.append("</table>");
htmlText.append("<table mc:repeatable='layout' mc:hideable='' mc:variant='footer-5' align='center' border='0' cellpadding='0' cellspacing='0' width='100%' style='margin:0 auto; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px; max-width:680px;' data-module='footer-5'>");
htmlText.append("");
htmlText.append("<tbody>");
htmlText.append("<tr>");
htmlText.append("<td align='center' style='width:100%; height:100%; background-color:#ffffff;' bgcollor='#ffffff' data-bgcolor='footer-5-bg'>");
htmlText.append("<table width='600' align='center' class='container' border='0' cellpadding='0' cellspacing='0' style='width:600px; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px;'>");
htmlText.append("");
htmlText.append("<tbody>");
htmlText.append("<tr>");
htmlText.append("<td align='center' height='35' width='1' style='font-size: 1px;line-height: 15px;height: 15px;'>&nbsp;");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("");
htmlText.append("<tr>");
htmlText.append("<td align='center'>");
htmlText.append("<table width='600' align='center' class='text' border='0' cellpadding='0' cellspacing='0' style='width:600px; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px;'>");
htmlText.append("<tbody>");
htmlText.append("<tr>");
htmlText.append("");
htmlText.append("<th width='360' align='left' class='container-wrap' valign='top' style='vertical-align: top;'>");
htmlText.append("<table width='360' align='left' class='container' border='0' cellpadding='0' cellspacing='0' style='width:360px; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px;'>");
htmlText.append("");
htmlText.append("<tbody>");
htmlText.append("<tr>");
htmlText.append("<td class='text-left' style='vertical-align:top;text-align:left;font-family: 'Open Sans', sans-serif;font-size:13px;padding-top: 5px;line-height: 20px;text-decoration: none;color: #232527;font-weight: 400;' data-size='footer-5-rights-size' data-min='9' data-max='40' data-color='footer-5-rights-color' mc:edit='footer-5-rights-1.1'>");
htmlText.append("<multiline>Â© Copyrights 2017 | Miracle Software Systems, Inc.</multiline>");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("");
htmlText.append("<tr>");
htmlText.append("<td align='center' height='13' width='1' style='font-size: 1px; line-height: 13px; height:13px;'>&nbsp;");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("");
htmlText.append("<tr>");
htmlText.append("<td align='center' height='39' width='1' style='font-size: 1px;line-height: 20px;height: 20px;'>&nbsp;                                                      </td>");
htmlText.append("</tr>");
htmlText.append("");
htmlText.append("</tbody>");
htmlText.append("</table>");
htmlText.append("</th>");
htmlText.append("");
htmlText.append("<th width='280' align='right' class='container-wrap' valign='top' style='vertical-align: top;'>");
htmlText.append("<table width='280' align='right' class='container' border='0' cellpadding='0' cellspacing='0' style='width:280px; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px;'>");
htmlText.append("");
htmlText.append("<tbody>");
htmlText.append("<tr>");
htmlText.append("<td class='disable' align='center' height='5' width='1' style='font-size: 1px; line-height: 5px; height:5px;'>&nbsp;");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("");
htmlText.append("<tr>");
htmlText.append("<td align='right'>");
htmlText.append("<table width='135' align='right' class='container' border='0' cellpadding='0' cellspacing='0' style='width:135px; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px;'>");
htmlText.append("");
htmlText.append("<tbody>");
htmlText.append("<tr>");
htmlText.append("<td align='left'>");
htmlText.append("<table style='border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;border: 0px;' align='left' border='0' cellpadding='0' cellspacing='0' width='auto'>");
htmlText.append("<tbody>");
htmlText.append("<tr>");
htmlText.append("<th>");
htmlText.append("<a href='https://www.facebook.com/miracle45625/' target='blank' style='color:#ef4048; text-decoration: none;'><img src='https://www.miraclesoft.com/images/newsletters/facebook1.png' width='20' height='20' alt='#' style='display:block; vertical-align:middle;'></a>   </th>");
htmlText.append("");
htmlText.append("<td align='center' height='1' width='10' style='font-size: 1px; line-height: 1px; width:10px;'>");
htmlText.append("</td>");
htmlText.append("");
htmlText.append("<th>");
htmlText.append("<a href='https://plus.google.com/+Team_MSS' target='blank' style='color:#ef4048; text-decoration: none;'><img src='https://www.miraclesoft.com/images/newsletters/googleplus1.png' width='20' height='20' alt='#' style='display:block; vertical-align:middle;'></a>     </th>");
htmlText.append("");
htmlText.append("<td align='center' height='1' width='10' style='font-size: 1px; line-height: 1px; width:10px;'>");
htmlText.append("</td>");
htmlText.append("");
htmlText.append("<th>");
htmlText.append("<a href='https://www.linkedin.com/company/miracle-software-systems-inc' target='blank' style='color:#ef4048; text-decoration: none;'><img src='https://www.miraclesoft.com/images/newsletters/Linkedin1.png' width='20' height='20' alt='#' style='display:block; vertical-align:middle;'></a>     </th>");
htmlText.append("");
htmlText.append("<td align='center' height='1' width='10' style='font-size: 1px; line-height: 1px; width:10px;'>");
htmlText.append("</td>");
htmlText.append("");
htmlText.append("<th>");
htmlText.append("</th><td>                        <a href='https://www.youtube.com/c/Team_MSS' target='blank' style='color:#ef4048; text-decoration: none;'><img src='https://www.miraclesoft.com/images/newsletters/youtube1.png' width='20' height='20' alt='#' style='display:block; vertical-align:middle;'></a>     ");
htmlText.append("");
htmlText.append("</td><td align='center' height='1' width='10' style='font-size: 1px; line-height: 1px; width:10px;'>");
htmlText.append("</td>");
htmlText.append("");
htmlText.append("<th>");
htmlText.append("</th><td>                        <a href='https://twitter.com/Team_MSS' target='blank' style='color:#ef4048; text-decoration: none;'><img src='https://www.miraclesoft.com/images/newsletters/Twitter1.png' width='20' height='20' alt='#' style='display:block; vertical-align:middle;'></a>     ");
htmlText.append("");
htmlText.append("</td><td align='center' height='1' width='10' style='font-size: 1px; line-height: 1px; width:10px;'>");
htmlText.append("</td>");
htmlText.append("");
htmlText.append("<th>");
htmlText.append("</th></tr>");
htmlText.append("</tbody>");
htmlText.append("</table>");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("");
htmlText.append("</tbody>");
htmlText.append("</table>");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("");
htmlText.append("<tr>");
htmlText.append("<td align='center' height='39' width='1' style='font-size: 1px;line-height: 20px;height: 20px;'>&nbsp;");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("");
htmlText.append("</tbody>");
htmlText.append("</table>");
htmlText.append("</th>");
htmlText.append("");
htmlText.append("</tr>");
htmlText.append("</tbody>");
htmlText.append("</table>");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("</tbody>");
htmlText.append("</table>");
htmlText.append("</td>");
htmlText.append("");
htmlText.append("</tr>");
htmlText.append("");
htmlText.append("</tbody>");
htmlText.append("</table>");
htmlText.append("");
htmlText.append("<span class='gr__tooltip'><span class='gr__tooltip-content'></span><i class='gr__tooltip-logo'></i><span class='gr__triangle'></span></span></body></html>");

                    bodyContent = htmlText.toString();
                    // toAddress = emailId;
                   // System.out.println("inserted------------hello");
                    //ServiceLocator.getMailServices().doAddEmailLog(toAddress, cCAddress, issueTitle, bodyContent, createdDate, bCCAddress);
                    ServiceLocator.getMailServices().doAddEmailLogNew(empDetailsMap.getKey().toString(), cCAddress, "Reg: Nominee Approved", bodyContent, createdDate, bCCAddress, "Nominee For Conference Event");

                    message = "<font color=\"green\" size=\"2px\">Status Updated Succesfully and Mail had been sent to Attendees!</font>";

                }
            }
            //in.close();

        } catch (Exception e) {
            throw new ServiceLocatorException(e);
        }
        return message;
    }

    public String doSendEmailForRejectedNominees(NewAjaxHandlerAction ajaxHandlerAction,JSONObject recipientJson) throws ServiceLocatorException {

        String message = "";
String nomineeIds="";
        try {

            Calendar cal = Calendar.getInstance();
            int year = cal.get(cal.YEAR);
           // System.out.println("doSendEmailForNominees");
            if (Properties.getProperty("Mail.Flag").equals("1")) {
                Iterator<String> keysItr = recipientJson.keys();
                 int temp=0;
                while (keysItr.hasNext()) {
                    if(temp==0)
                    nomineeIds = keysItr.next();
                    else
                       nomineeIds = nomineeIds+","+ keysItr.next();
                    temp++;
                }
             //  Map employeeMap = DataSourceDataProvider.getInstance().getNominationEmployeeDetails(ajaxHandlerAction.getNomineeIds());
                 Map employeeMap = DataSourceDataProvider.getInstance().getNominationEmployeeDetails(nomineeIds);
                Iterator it = employeeMap.entrySet().iterator();
                
                 String cCAddress = "";
                    String subject = "";
                    String bodyContent = "";
                    String createdDate = "";
                    String mailDeliverDate = "";
                    //String mailFlag = "0";
                    String bCCAddress = "";
                
                
                
                
                while (it.hasNext()) {
                    Map.Entry empDetailsMap = (Map.Entry) it.next();
                    cCAddress = "";
                   if(recipientJson.has(empDetailsMap.getKey().toString().split("\\@")[0])){
                       cCAddress = (String) recipientJson.get(empDetailsMap.getKey().toString().split("\\@")[0]);
                   }

                    // String toAddress = "";
                   
                    //    createdDate = DateUtility.getInstance().getCurrentMySqlDateTime();
                    // mailDeliverDate = DateUtility.getInstance().getCurrentMySqlDateTime1();
                    StringBuilder htmlText = new StringBuilder();
                    // bCCAddress = Properties.getProperty("bccAddress");

                   htmlText.append("<html xmlns='http://www.w3.org/1999/xhtml' class='gr__newsletters-2017-vnallamalla_c9users_io'><head>");
htmlText.append("<meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>");
htmlText.append("<meta name='viewport' content='width=device-width, initial-scale=1'>");
htmlText.append("<meta http-equiv='X-UA-Compatible' content='IE=edge'>");
htmlText.append("<meta name='format-detection' content='telephone=no'>");
htmlText.append("<title>Nominee Not Prroved</title>");
htmlText.append("<style>");
htmlText.append("@import url('https://fonts.googleapis.com/css?family=Montserrat:400,700');");
htmlText.append("@import url('https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i');");
htmlText.append("html{width: 100%;}");
htmlText.append("body{");
htmlText.append("width: 100%;");
htmlText.append("background-color: #ffffff;");
htmlText.append("margin:0; padding:0;");
htmlText.append("-webkit-text-size-adjust:none;");
htmlText.append("-ms-text-size-adjust:none;");
htmlText.append("-webkit-font-smoothing: ;");
htmlText.append("mso-margin-top-alt:0px;");
htmlText.append("mso-margin-bottom-alt:0px;");
htmlText.append("mso-padding-alt: 0px 0px 0px 0px;}");
htmlText.append("div, p, a, li, td { -webkit-text-size-adjust:none; }");
htmlText.append("#outlook a {padding:0;}");
htmlText.append("p,h1,h2,h3,h4{ margin-top:0; margin-bottom:0; padding-top:0; padding-bottom:0;}");
htmlText.append("table{");
htmlText.append("mso-table-lspace:0pt;");
htmlText.append("mso-table-rspace:0pt;");
htmlText.append("}");
htmlText.append("table td {border-collapse: collapse;}");
htmlText.append("span.preheader{display: none;font-size: 1px; visibility: hidden; opacity: 0; color: transparent; height: 0; width: 0;}");
htmlText.append(".googlefix {width: 280px!important; text-align:center!important; min-width:0px!important}");
htmlText.append("a {outline:none;}");
htmlText.append("a {text-decoration: none;}");
htmlText.append("a img {border:none;}");
htmlText.append(".image_fix {display:block;}");
htmlText.append(".ReadMsgBody { width: 100%;}");
htmlText.append(".ExternalClass {width: 100%;}");
htmlText.append(".ExternalClass,");
htmlText.append(".ExternalClass p,");
htmlText.append(".ExternalClass span,");
htmlText.append(".ExternalClass font,");
htmlText.append(".ExternalClass td,");
htmlText.append(".ExternalClass div {");
htmlText.append("line-height: 100%;}");
htmlText.append("img{");
htmlText.append("-ms-interpolation-mode:bicubic;}");
htmlText.append("p {");
htmlText.append("margin:0 !important;");
htmlText.append("padding:0 !important;}");
htmlText.append(".button:hover {filter: brightness(120%);}");
htmlText.append("@media only screen and (max-width: 600px)");
htmlText.append("{");
htmlText.append("body{width:100% !important;}");
htmlText.append(".container           {width:100% !important; min-width:100% !important;}");
htmlText.append(".container-wrap      {display:inline-block !important; width:100% !important; height:auto !important; border-radius:0 !important;}");
htmlText.append(".image-container     {width: 100% !important; height: auto !important; }");
htmlText.append(".disable             {display: none !important;}");
htmlText.append(".enable              {display: inline !important;}");
htmlText.append(".padding-top-10      {padding-top: 10px !important;}");
htmlText.append(".padding-top-20      {padding-top: 20px !important;}");
htmlText.append(".padding-top-30      {padding-top: 30px !important;}");
htmlText.append(".padding-top-40      {padding-top: 40px !important;}");
htmlText.append(".padding-top-60      {padding-top: 60px !important;}");
htmlText.append(".padding-bottom-10   {padding-bottom: 10px !important;}");
htmlText.append(".padding-bottom-20   {padding-bottom: 20px !important;}");
htmlText.append(".padding-bottom-30   {padding-bottom: 30px !important;}");
htmlText.append(".padding-bottom-40   {padding-bottom: 40px !important;}");
htmlText.append(".padding-bottom-60   {padding-bottom: 60px !important;}");
htmlText.append(".padding-none        {padding:0  !important;}");
htmlText.append(".text                {width:90% !important; text-align:center !important;}");
htmlText.append(".text-on-bg          {width:90% !important; text-align:center !important;}");
htmlText.append(".text-center         {text-align: center !important;}");
htmlText.append(".text-right          {text-align: right !important;}");
htmlText.append(".text-left           {text-align: left !important;}");
htmlText.append(".text-white          {color: #ffffff !important;}");
htmlText.append(".background-cover    {background-size: cover !important;}");
htmlText.append(".background-cover-percent    {background-size: 100% 79% !important;}");
htmlText.append(".background-contain  {background-size: contain !important;}");
htmlText.append(".background-auto     {background-size: auto !important;}");
htmlText.append(".background-center   {background-position: center center !important;}");
htmlText.append(".background-none     {background-image: none !important;}");
htmlText.append(".border-none         {border:0 !important;}");
htmlText.append(".border-white        {border-color: #ffffff !important;}");
htmlText.append("}");
htmlText.append("@media only screen and (max-width: 451px)");
htmlText.append("{");
htmlText.append("body {width:100% !important;}");
htmlText.append(".container           {width:100% !important; min-width:100% !important;}");
htmlText.append(".container-wrap      {display:inline-block !important; width:100% !important; height:auto !important; border-radius:0 !important;}");
htmlText.append("}");
htmlText.append("@media only screen and (max-width: 341px)");
htmlText.append("{");
htmlText.append("body {width:100%!important;}");
htmlText.append(".container           {width:100% !important; min-width:100% !important;}");
htmlText.append(".container-wrap      {display:inline-block !important; width:100% !important; height:auto !important; border-radius:0 !important;}");
htmlText.append("}");
htmlText.append("</style>");
htmlText.append("</head>");
htmlText.append("<body marginwidth='0' marginheight='0' style='margin-top: 0; margin-bottom: 0; padding-top: 0; padding-bottom: 0; width: 100%; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%;' offset='0' topmargin='0' leftmargin='0' data-gr-c-s-loaded='true'>");
htmlText.append("<div class='parentOfBg'></div>");
htmlText.append("<div id='edit_link' class='hidden' style='display: none; left: 88px; top: 27956px;'>");
htmlText.append("");
htmlText.append("<div class='close_link'></div>");
htmlText.append("");
htmlText.append("<input type='text' id='edit_link_value' class='createlink' placeholder='Your URL'>");
htmlText.append("");
htmlText.append("<div id='change_image_wrapper' style='display: none;'>");
htmlText.append("");
htmlText.append("<div id='change_image'>");
htmlText.append("");
htmlText.append("<p id='change_image_button'>Change &nbsp; <span class='pixel_result'></span></p>");
htmlText.append("</div>");
htmlText.append("");
htmlText.append("<input type='button' value='' id='change_image_link'>");
htmlText.append("");
htmlText.append("<input type='button' value='' id='remove_image'>");
htmlText.append("</div>");
htmlText.append("");
htmlText.append("<div id='tip'></div>");
htmlText.append("</div>");
htmlText.append("<table mc:repeatable='layout' mc:hideable='' mc:variant='navigation-6' align='center' border='0' cellpadding='0' cellspacing='0' width='100%' style='margin:0 auto; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px; max-width:680px;' data-module='navigation-6'>");
htmlText.append("");
htmlText.append("<tbody>");
htmlText.append("<tr>");
htmlText.append("<td align='center' style='width:100%; height:100%; background-color:#ffffff;' bgcollor='#ffffff' data-bgcolor='nav-6-bg'>");
htmlText.append("<table width='600' align='center' class='container' border='0' cellpadding='0' cellspacing='0' style='width:600px; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px;'>");
htmlText.append("");
htmlText.append("<tbody>");
htmlText.append("<tr>");
htmlText.append("<td align='center' height='20' width='1' style='font-size: 1px; line-height: 20px; height:20px;'>&nbsp;");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("");
htmlText.append("<tr>");
htmlText.append("<td align='center'>");
htmlText.append("<table width='600' align='center' class='container' border='0' cellpadding='0' cellspacing='0' style='width:600px; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px;'>");
htmlText.append("<tbody>");
htmlText.append("<tr>");
htmlText.append("");
htmlText.append("<th width='140' align='left' class='container-wrap' valign='top' style='vertical-align: top;'>");
htmlText.append("<table align='center' cellpadding='0' cellspacing='0'>");
htmlText.append("<tbody>");
htmlText.append("<tr>");
htmlText.append("<td width='160' align='center' valign='top'>");
htmlText.append("<table width='100%' align='center' cellpadding='0' cellspacing='0'>");
htmlText.append("<tbody>");
htmlText.append("");
htmlText.append("<tr>");
htmlText.append("<td align='center' valign='top' style='line-height: 0px !important;'>");
htmlText.append("<a style='text-decoration: none; display: block;' href='https://www.miraclesoft.com/' target='blank'>");
htmlText.append("<img src='https://www.miraclesoft.com/images/newsletters/miracle-logo-dark.png' alt='logo' width='100%' style='width: 100%; max-width: 100%;'>");
htmlText.append("</a>");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("</tbody>");
htmlText.append("</table>");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("</tbody>");
htmlText.append("</table>");
htmlText.append("</th>");
htmlText.append("");
htmlText.append("<th width='320' align='right' class='container-wrap' valign='top' style='vertical-align: top;'>");
htmlText.append("<table width='320' align='right' class='container' border='0' cellpadding='0' cellspacing='0' style='width:320px; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px;'>");
htmlText.append("");
htmlText.append("<tbody>");
htmlText.append("<tr>");
htmlText.append("<td align='center' height='1' width='1' style='font-size: 1px; line-height: 1px; height:1px;'>&nbsp;");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("");
htmlText.append("<tr>");
htmlText.append("<td align='right'>");
htmlText.append("<table width='auto' align='right' class='container' border='0' cellpadding='0' cellspacing='0' style='width:autopx; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px;'>");
htmlText.append("<tbody>");
htmlText.append("<tr>");
htmlText.append("<td align='center'>");
htmlText.append("<table width='auto' align='center' class='' border='0' cellpadding='0' cellspacing='0' style='border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px;'>");
htmlText.append("<tbody><tr>");
htmlText.append("<td align='center' height='15'>");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("<tr>");
htmlText.append("");
htmlText.append("<th align='center' height='1' width='4' style='font-size: 1px; line-height: 1px; width:4px;'>");
htmlText.append("</th>");
htmlText.append("");
htmlText.append("<th style='text-align:center; font-family: oPen sans; font-size:12px; line-height: 20px; text-decoration: none; color: #000000; font-weight:400; text-transform: uppercase; : 0.05em;'>");
htmlText.append("<a href='https://www.miraclesoft.com/company/' target='blank' style='text-align:center; font-family: oPen sans; font-size:12px; line-height: 20px; text-decoration: none; color: #000000; font-weight:600; text-transform: uppercase; : 0.05em;' data-size='nav-6-link-size' data-min='9' data-max='40' data-color='nav-6-link-color' mc:edit='nav-6-link-2'>");
htmlText.append("<multiline><b>About us </b>|</multiline>");
htmlText.append("</a>");
htmlText.append("</th>");
htmlText.append("");
htmlText.append("<th align='center' height='1' width='4' style='font-size: 1px; line-height: 1px; width:4px;'>");
htmlText.append("</th>");
htmlText.append("");
htmlText.append("<th style='text-align:center; font-family: oPen sans; font-size:12px; line-height: 20px; text-decoration: none; color: #000000; font-weight:400; text-transform: uppercase; : 0.05em;'>");
htmlText.append("<a href='https://www.miraclesoft.com/services/' target='blank' style='text-align:center; font-family: oPen sans; font-size:12px; line-height: 20px; text-decoration: none; color: #000000; font-weight:600; text-transform: uppercase; : 0.05em;' data-size='nav-6-link-size' data-min='9' data-max='40' data-color='nav-6-link-color' mc:edit='nav-6-link-3'>");
htmlText.append("<multiline> <b>Our Services</b> |</multiline>");
htmlText.append("</a>");
htmlText.append("</th>");
htmlText.append("");
htmlText.append("<th align='center' height='1' width='4' style='font-size: 1px; line-height: 1px; width:4px;'>");
htmlText.append("</th>");
htmlText.append("");
htmlText.append("<th style='text-align:center; font-family: oPen sans; font-size:12px; line-height: 20px; text-decoration: none; color: #232527; font-weight:600; text-transform: uppercase; : 0.05em;'>");
htmlText.append("<a href='https://www.miraclesoft.com/contact/' target='blank' style='text-align:center; font-family: oPen sans; font-size:12px; line-height: 20px; text-decoration: none; color: #232527; font-weight:600; text-transform: uppercase; : 0.05em;' data-size='nav-6-signUp-size' data-min='9' data-max='40' data-color='nav-6-signUp-color' mc:edit='nav-6-signUp-4'>");
htmlText.append("<multiline><b>Contact us</b></multiline>");
htmlText.append("</a>");
htmlText.append("</th>");
htmlText.append("");
htmlText.append("</tr>");
htmlText.append("</tbody>");
htmlText.append("</table>");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("</tbody>");
htmlText.append("</table>");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("");
htmlText.append("<tr>");
htmlText.append("<td align='center' height='18' width='1' style='font-size: 1px; line-height: 18px; height:18px;'>&nbsp;");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("");
htmlText.append("</tbody>");
htmlText.append("</table>");
htmlText.append("</th>");
htmlText.append("");
htmlText.append("</tr>");
htmlText.append("</tbody>");
htmlText.append("</table>");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("</tbody>");
htmlText.append("</table>");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("");
htmlText.append("</tbody>");
htmlText.append("</table>");
htmlText.append("<table mc:repeatable='layout' mc:hideable='' mc:variant='navigation-margin' align='center' border='0' cellpadding='0' cellspacing='0' width='100%' style='margin:0 auto; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px; max-width:680px;' data-module='navigation-margin'>");
htmlText.append("");
htmlText.append("<tbody>");
htmlText.append("<tr>");
htmlText.append("<td align='center' style='width:100%; height:100%; background-color:#ffffff;' bgcollor='#ffffff' data-bgcolor='navigation-margin-bg'>");
htmlText.append("<table width='600' align='center' class='container' border='0' cellpadding='0' cellspacing='0' style='width:600px; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px;'>");
htmlText.append("");
htmlText.append("<tbody>");
htmlText.append("<tr>");
htmlText.append("<td align='center' height='20' width='1' style='font-size: 1px;line-height: 10px;height: 10px;'>&nbsp;");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("");
htmlText.append("</tbody>");
htmlText.append("</table>");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("");
htmlText.append("</tbody>");
htmlText.append("</table>");
htmlText.append("<div class='parentOfBg'></div>");
htmlText.append("<table align='center' border='0' cellpadding='0' cellspacing='0' width='100%' style='margin:0 auto; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px; max-width:680px;' data-module='text-block-1'>");
htmlText.append("");
htmlText.append("<tbody>");
htmlText.append("<tr>");
htmlText.append("<td align='center' style='width:100%; height:100%; background-color:#f8f8f8;' data-bgcolor='txt-blck-1-bg'>");
htmlText.append("<table width='600' align='center' class='text' border='0' cellpadding='0' cellspacing='0' style='width:600px; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px;'>");
htmlText.append("");
htmlText.append("<tbody>");
htmlText.append("<tr>");
htmlText.append("<td align='center' height='35' width='1' style='font-size: 1px; line-height: 35px; height:35px;'>");
htmlText.append("&nbsp;");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("");
htmlText.append("<tr>");
htmlText.append("<td style='text-align: center;font-family: 'Open Sans' , sans-serif;font-size: 25px;line-height: 20px;text-decoration: none;color: #232527;font-weight:700;' data-size='txt-blck-1-text-size' data-min='9' data-max='40' data-color='txt-blck-1-text-color'>");
htmlText.append("<multiline>We're Sorry !</multiline>");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("");
htmlText.append("<tr>");
htmlText.append("<td align='center' height='25' width='1' style='font-size: 1px;line-height: 25px;height: 25px;'>");
htmlText.append("&nbsp;");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("<tr>");
htmlText.append("<td align='center'>");
htmlText.append("<table width='60' height='2' align='center' border='0' cellpadding='0' cellspacing='0' style='width:60px; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px; background-color: #232527' data-bgcolor='team-6-line-color'>");
htmlText.append("");
htmlText.append("<tbody>");
htmlText.append("<tr>");
htmlText.append("<td align='center' height='2' width='40' style='font-size: 2px; line-height: 2px; width:40px;'></td>");
htmlText.append("</tr>");
htmlText.append("");
htmlText.append("</tbody>");
htmlText.append("</table>");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("");
htmlText.append("</tbody>");
htmlText.append("</table>");
htmlText.append("</td>");
htmlText.append("");
htmlText.append("</tr>");
htmlText.append("");
htmlText.append("</tbody>");
htmlText.append("</table>");
htmlText.append("<table mc:repeatable='layout' mc:hideable='' mc:variant='text-block-1' align='center' border='0' cellpadding='0' cellspacing='0' width='100%' style='margin:0 auto; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px; max-width:680px;' data-module='text-block-1'>");
htmlText.append("");
htmlText.append("<tbody>");
htmlText.append("<tr>");
htmlText.append("<td align='center' style='width:100%; height:100%; background-color:#f8f8f8;' bgcollor='#f8f8f8' data-bgcolor='txt-blck-1-bg'>");
htmlText.append("<table width='600' align='center' class='text' border='0' cellpadding='0' cellspacing='0' style='width:600px; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px;'>");
htmlText.append("");
htmlText.append("<tbody>");
htmlText.append("<tr>");
htmlText.append("<td align='center' height='25' width='1' style='font-size: 1px; line-height: 25px; height:25px;'>&nbsp;");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("");
htmlText.append("<tr>");
htmlText.append("<td style='text-align:left;font-family: 'Open Sans' , sans-serif;font-size: 14px;line-height: 20px;text-decoration: none;color: #232527;font-weight:400;' data-size='txt-blck-1-text-size' data-min='9' data-max='40' data-color='txt-blck-1-text-color' mc:edit='txt-blck-1-text-1'>");
htmlText.append("<multiline><b>" + empDetailsMap.getValue() + ",</b>");
htmlText.append("</multiline>");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("");
htmlText.append("</tbody>");
htmlText.append("</table>");
htmlText.append("</td>");
htmlText.append("");
htmlText.append("</tr>");
htmlText.append("");
htmlText.append("</tbody>");
htmlText.append("</table>");
htmlText.append("");
htmlText.append("");
htmlText.append("<table mc:repeatable='layout' mc:hideable='' mc:variant='text-block-1' align='center' border='0' cellpadding='0' cellspacing='0' width='100%' style='margin:0 auto; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px; max-width:680px;' data-module='text-block-1'>");
htmlText.append("");
htmlText.append("<tbody>");
htmlText.append("<tr>");
htmlText.append("<td align='center' style='width:100%; height:100%; background-color:#f8f8f8;' bgcollor='#f8f8f8' data-bgcolor='txt-blck-1-bg'>");
htmlText.append("<table width='600' align='center' class='text' border='0' cellpadding='0' cellspacing='0' style='width:600px; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px;'>");
htmlText.append("");
htmlText.append("<tbody>");
htmlText.append("<tr>");
htmlText.append("<td align='center' height='25' width='1' style='font-size: 1px;line-height: 15px;height: 15px;'>&nbsp;");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("");
htmlText.append("<tr>");
htmlText.append("<td style='text-align:justify; font-family: 'Open Sans' , sans-serif; font-size:14px; line-height: 24px; text-decoration: none; color: #232527; font-weight:400;' data-size='txt-blck-1-text-size' data-min='9' data-max='40' data-color='txt-blck-1-text-color' mc:edit='txt-blck-1-text-1'>");
htmlText.append("<multiline>Due to high volume of nominations for this event we were not able to shortlist everybody. We appreciate your interest to attend <b>" + ajaxHandlerAction.getEventName() + "</b> and look forward to working with you on future events.</multiline>");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("");
htmlText.append("</tbody>");
htmlText.append("</table>");
htmlText.append("</td>");
htmlText.append("");
htmlText.append("</tr>");
htmlText.append("");
htmlText.append("</tbody>");
htmlText.append("</table>");
htmlText.append("");
htmlText.append("<table mc:repeatable='layout' mc:hideable='' mc:variant='text-block-1' align='center' border='0' cellpadding='0' cellspacing='0' width='100%' style='margin:0 auto; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px; max-width:680px;' data-module='text-block-1'>");
htmlText.append("");
htmlText.append("<tbody>");
htmlText.append("<tr>");
htmlText.append("<td align='center' style='width:100%; height:100%; background-color:#f8f8f8;' bgcollor='#f8f8f8' data-bgcolor='txt-blck-1-bg'>");
htmlText.append("<table width='600' align='center' class='text' border='0' cellpadding='0' cellspacing='0' style='width:600px; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px;'>");
htmlText.append("");
htmlText.append("<tbody>");
htmlText.append("<tr>");
htmlText.append("<td align='center' height='25' width='1' style='font-size: 1px;line-height: 15px;height: 15px;'>&nbsp;");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("");
htmlText.append("<tr>");
htmlText.append("<td style='text-align:justify; font-family: 'Open Sans'; font-size:14px; line-height: 24px; text-decoration: none; color: #232527; font-weight:400;' data-size='txt-blck-1-text-size' data-min='9' data-max='40' data-color='txt-blck-1-text-color' mc:edit='txt-blck-1-text-1'>");
htmlText.append("<multiline>For more information please reach out to <a href='mailto:partnerships@miraclesoft.com'><span style='color:#00aae7;'>partnerships@miraclesoft.com</span></a> (or) visit <a href='https://www.miraclesoft.com/conferencenominations' target='blank'><span style='color:#00aae7;'>www.miraclesoft.com/conferencenominations.</span></a>");
htmlText.append("</multiline>");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("");
htmlText.append("</tbody>");
htmlText.append("</table>");
htmlText.append("</td>");
htmlText.append("");
htmlText.append("</tr>");
htmlText.append("");
htmlText.append("</tbody>");
htmlText.append("</table>");
htmlText.append("");
htmlText.append("");
htmlText.append("");
htmlText.append("<table mc:repeatable='layout' mc:hideable='' mc:variant='text-block-1' align='center' border='0' cellpadding='0' cellspacing='0' width='100%' style='margin:0 auto; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px; max-width:680px;' data-module='text-block-1'>");
htmlText.append("");
htmlText.append("<tbody>");
htmlText.append("<tr>");
htmlText.append("<td align='center' style='width:100%; height:100%; background-color:#f8f8f8;' bgcollor='#f8f8f8' data-bgcolor='txt-blck-1-bg'>");
htmlText.append("<table width='600' align='center' class='text' border='0' cellpadding='0' cellspacing='0' style='width:600px; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px;'>");
htmlText.append("");
htmlText.append("<tbody>");
htmlText.append("<tr>");
htmlText.append("<td align='center' height='25' width='1' style='font-size: 1px;line-height: 15px;height: 15px;'>&nbsp;");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("");
htmlText.append("<tr>");
htmlText.append("<td style='text-align:left; font-family:'Open Sans' , sans-serif; font-size:14px; line-height: 24px; text-decoration: none; color: #232527; font-weight:400;' data-size='txt-blck-1-text-size' data-min='9' data-max='40' data-color='txt-blck-1-text-color' mc:edit='txt-blck-1-text-1'>");
htmlText.append("<multiline><b>Thanks &amp; Regards</b><br><b>Partnerships Team</b><br>");
htmlText.append("");
htmlText.append("Phone : (248)-412-0430<br>");
htmlText.append("Email : <a href='mailto:partnerships@miraclesoft.com'><span style='color:#232527;'>partnerships@miraclesoft.com</span></a>");
htmlText.append("</multiline>");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("");
htmlText.append("<tr>");
htmlText.append("<td align='center' height='46' width='1' style='font-size: 1px; line-height: 46px; height:46px;'>&nbsp;");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("");
htmlText.append("</tbody>");
htmlText.append("</table>");
htmlText.append("</td>");
htmlText.append("");
htmlText.append("</tr>");
htmlText.append("");
htmlText.append("</tbody>");
htmlText.append("</table>");
htmlText.append("<table mc:repeatable='layout' mc:hideable='' mc:variant='footer-5' align='center' border='0' cellpadding='0' cellspacing='0' width='100%' style='margin:0 auto; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px; max-width:680px;' data-module='footer-5'>");
htmlText.append("");
htmlText.append("<tbody>");
htmlText.append("<tr>");
htmlText.append("<td align='center' style='width:100%; height:100%; background-color:#ffffff;' bgcollor='#ffffff' data-bgcolor='footer-5-bg'>");
htmlText.append("<table width='600' align='center' class='container' border='0' cellpadding='0' cellspacing='0' style='width:600px; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px;'>");
htmlText.append("");
htmlText.append("<tbody>");
htmlText.append("<tr>");
htmlText.append("<td align='center' height='35' width='1' style='font-size: 1px;line-height: 15px;height: 15px;'>&nbsp;");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("");
htmlText.append("<tr>");
htmlText.append("<td align='center'>");
htmlText.append("<table width='600' align='center' class='text' border='0' cellpadding='0' cellspacing='0' style='width:600px; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px;'>");
htmlText.append("<tbody>");
htmlText.append("<tr>");
htmlText.append("");
htmlText.append("<th width='360' align='left' class='container-wrap' valign='top' style='vertical-align: top;'>");
htmlText.append("<table width='360' align='left' class='container' border='0' cellpadding='0' cellspacing='0' style='width:360px; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px;'>");
htmlText.append("");
htmlText.append("<tbody>");
htmlText.append("<tr>");
htmlText.append("<td class='text-left' style='vertical-align:top;text-align:left;font-family: 'Open Sans', sans-serif;font-size:13px;padding-top: 5px;line-height: 20px;text-decoration: none;color: #232527;font-weight: 400;' data-size='footer-5-rights-size' data-min='9' data-max='40' data-color='footer-5-rights-color' mc:edit='footer-5-rights-1.1'>");
htmlText.append("<multiline>Â© Copyrights 2017 | Miracle Software Systems, Inc.</multiline>");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("");
htmlText.append("<tr>");
htmlText.append("<td align='center' height='13' width='1' style='font-size: 1px; line-height: 13px; height:13px;'>&nbsp;");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("");
htmlText.append("<tr>");
htmlText.append("<td align='center' height='39' width='1' style='font-size: 1px;line-height: 20px;height: 20px;'>&nbsp;");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("");
htmlText.append("</tbody>");
htmlText.append("</table>");
htmlText.append("</th>");
htmlText.append("");
htmlText.append("<th width='280' align='right' class='container-wrap' valign='top' style='vertical-align: top;'>");
htmlText.append("<table width='280' align='right' class='container' border='0' cellpadding='0' cellspacing='0' style='width:280px; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px;'>");
htmlText.append("");
htmlText.append("<tbody>");
htmlText.append("<tr>");
htmlText.append("<td class='disable' align='center' height='5' width='1' style='font-size: 1px; line-height: 5px; height:5px;'>&nbsp;");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("");
htmlText.append("<tr>");
htmlText.append("<td align='right'>");
htmlText.append("<table width='135' align='right' class='container' border='0' cellpadding='0' cellspacing='0' style='width:135px; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px;'>");
htmlText.append("");
htmlText.append("<tbody>");
htmlText.append("<tr>");
htmlText.append("<td align='left'>");
htmlText.append("<table style='border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;border: 0px;' align='left' border='0' cellpadding='0' cellspacing='0' width='auto'>");
htmlText.append("<tbody>");
htmlText.append("<tr>");
htmlText.append("<th>");
htmlText.append("<a href='https://www.facebook.com/miracle45625/' target='blank' style='color:#ef4048; text-decoration: none;'><img src='https://www.miraclesoft.com/images/newsletters/facebook1.png' width='20' height='20' alt='#' style='display:block; vertical-align:middle;'></a>   </th>");
htmlText.append("");
htmlText.append("<td align='center' height='1' width='10' style='font-size: 1px; line-height: 1px; width:10px;'>");
htmlText.append("</td>");
htmlText.append("");
htmlText.append("<th>");
htmlText.append("<a href='https://plus.google.com/+Team_MSS' target='blank' style='color:#ef4048; text-decoration: none;'><img src='https://www.miraclesoft.com/images/newsletters/googleplus1.png' width='20' height='20' alt='#' style='display:block; vertical-align:middle;'></a>     </th>");
htmlText.append("");
htmlText.append("<td align='center' height='1' width='10' style='font-size: 1px; line-height: 1px; width:10px;'>");
htmlText.append("</td>");
htmlText.append("");
htmlText.append("<th>");
htmlText.append("<a href='https://www.linkedin.com/company/miracle-software-systems-inc' target='blank' style='color:#ef4048; text-decoration: none;'><img src='https://www.miraclesoft.com/images/newsletters/Linkedin1.png' width='20' height='20' alt='#' style='display:block; vertical-align:middle;'></a>     </th>");
htmlText.append("");
htmlText.append("<td align='center' height='1' width='10' style='font-size: 1px; line-height: 1px; width:10px;'>");
htmlText.append("</td>");
htmlText.append("");
htmlText.append("<th>");
htmlText.append("</th><td>                        <a href='https://www.youtube.com/c/Team_MSS' target='blank' style='color:#ef4048; text-decoration: none;'><img src='https://www.miraclesoft.com/images/newsletters/youtube1.png' width='20' height='20' alt='#' style='display:block; vertical-align:middle;'></a>     ");
htmlText.append("");
htmlText.append("</td><td align='center' height='1' width='10' style='font-size: 1px; line-height: 1px; width:10px;'>");
htmlText.append("</td>");
htmlText.append("");
htmlText.append("<th>");
htmlText.append("</th><td>                        <a href='https://twitter.com/Team_MSS' target='blank' style='color:#ef4048; text-decoration: none;'><img src='https://www.miraclesoft.com/images/newsletters/Twitter1.png' width='20' height='20' alt='#' style='display:block; vertical-align:middle;'></a>     ");
htmlText.append("");
htmlText.append("</td><td align='center' height='1' width='10' style='font-size: 1px; line-height: 1px; width:10px;'>");
htmlText.append("</td>");
htmlText.append("");
htmlText.append("<th>");
htmlText.append("</th></tr>");
htmlText.append("</tbody>");
htmlText.append("</table>");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("");
htmlText.append("</tbody>");
htmlText.append("</table>");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("");
htmlText.append("<tr>");
htmlText.append("<td align='center' height='39' width='1' style='font-size: 1px;line-height: 20px;height: 20px;'>&nbsp;");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("");
htmlText.append("</tbody>");
htmlText.append("</table>");
htmlText.append("</th>");
htmlText.append("");
htmlText.append("</tr>");
htmlText.append("</tbody>");
htmlText.append("</table>");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("</tbody>");
htmlText.append("</table>");
htmlText.append("</td>");
htmlText.append("");
htmlText.append("</tr>");
htmlText.append("");
htmlText.append("</tbody>");
htmlText.append("</table>");
htmlText.append("");
htmlText.append("<span class='gr__tooltip'><span class='gr__tooltip-content'></span><i class='gr__tooltip-logo'></i><span class='gr__triangle'></span></span></body></html>");
                    bodyContent = htmlText.toString();
                    // toAddress = emailId;
                   // System.out.println("inserted------------hello");
                    //ServiceLocator.getMailServices().doAddEmailLog(toAddress, cCAddress, issueTitle, bodyContent, createdDate, bCCAddress);
                    ServiceLocator.getMailServices().doAddEmailLogNew(empDetailsMap.getKey().toString(), cCAddress, "Reg: Nominee Not Approved", bodyContent, createdDate, bCCAddress, "Nominee For Conference Event");

                    message = "<font color=\"green\" size=\"2px\">Status Updated Succesfully and Mail had been sent to Attendees!</font>";

                }
            }
            //in.close();

        } catch (Exception e) {
            throw new ServiceLocatorException(e);
        }
        return message;
    }

    public String getEmployeeDetailsForEvents(String loginId) throws ServiceLocatorException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;


        String queryString = "";
        JSONObject subJson = null;
        try {
            queryString = "SELECT * FROM tblEmployee WHERE LoginId='" + loginId + "'";
            connection = ConnectionProvider.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(queryString);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                subJson = new JSONObject();


                if (resultSet.getString("MName") != null && !"".equals(resultSet.getString("MName"))) {
                    subJson.put("EmployeeName", resultSet.getString("FName") + " " + resultSet.getString("MName") + "." + resultSet.getString("LName"));
                } else {
                    subJson.put("EmployeeName", resultSet.getString("FName") + "." + resultSet.getString("LName"));
                }
                subJson.put("Email1", resultSet.getString("Email1"));

                if (resultSet.getString("EmpNo") != null && !"".equals(resultSet.getString("EmpNo"))) {
                    subJson.put("EmpNo", resultSet.getString("EmpNo"));
                } else {
                    subJson.put("EmpNo", "");
                }
                if (resultSet.getString("OpsContactId") != null && !"".equals(resultSet.getString("OpsContactId"))) {
                    subJson.put("OpsContactId", DataSourceDataProvider.getInstance().getEmpNameByEmpId(resultSet.getInt("OpsContactId")));
                } else {
                    subJson.put("OpsContactId", "");
                }

                if (resultSet.getString("Country") != null && !"".equals(resultSet.getString("Country"))) {
                    subJson.put("Country", resultSet.getString("Country"));
                } else {
                    subJson.put("Country", "");
                }
                if (resultSet.getString("DepartmentId") != null && !"".equals(resultSet.getString("DepartmentId"))) {
                    subJson.put("DepartmentId", resultSet.getString("DepartmentId"));
                } else {
                    subJson.put("DepartmentId", "");
                }
                if (resultSet.getString("PracticeId") != null && !"".equals(resultSet.getString("PracticeId"))) {
                    subJson.put("PracticeId", resultSet.getString("PracticeId"));
                } else {
                    subJson.put("PracticeId", "");
                }
                if (resultSet.getString("ReportsTo") != null && !"".equals(resultSet.getString("ReportsTo"))) {
                    subJson.put("ReportsTo", DataSourceDataProvider.getInstance().getemployeenamebyloginId(resultSet.getString("ReportsTo")));
                } else {
                    subJson.put("ReportsTo", "");
                }
                if (resultSet.getString("TitleTypeId") != null && !"".equals(resultSet.getString("TitleTypeId"))) {
                    subJson.put("TitleTypeId", resultSet.getString("TitleTypeId"));
                } else {
                    subJson.put("TitleTypeId", "");
                }
                if (resultSet.getString("Location") != null && !"".equals(resultSet.getString("Location"))) {
                    subJson.put("Location", resultSet.getString("Location"));
                } else {
                    subJson.put("Location", "");
                }
                if (resultSet.getString("WorkPhoneNo") != null && !"".equals(resultSet.getString("WorkPhoneNo"))) {
                    subJson.put("WorkPhoneNo", resultSet.getString("WorkPhoneNo"));
                } else {
                    subJson.put("WorkPhoneNo", "");
                }
                if (resultSet.getString("HireDate") != null && !"".equals(resultSet.getString("HireDate"))) {
                    subJson.put("HireDate", DateUtility.getInstance().convertToviewFormat(resultSet.getString("HireDate")));
                } else {
                    subJson.put("HireDate", "");
                }


            }


        } catch (Exception sqe) {
            sqe.printStackTrace();
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                    resultSet = null;
                }
                if (preparedStatement != null) {
                    preparedStatement.close();
                    preparedStatement = null;
                }
                if (connection != null) {
                    connection.close();
                    connection = null;
                }
            } catch (SQLException sqle) {
            }
        }
        return subJson.toString();
    }
    /*Events functionality new enhancements end
     * Date : 01/23/2017
     * Author : Santosh Kola/Phani Kanuri
     */
    
    
    /*Event Reminder
     * Author : Santosh Kola
     * Date : 05/18/2017
     */
    
    public String sendEventNominationReminder(NewAjaxHandlerAction newAjaxHandlerAction)  throws Exception  {
      
    
        String message = "error";
        boolean isUpdated = false;
      JSONObject responseJobj = new JSONObject();
        try {
 

            JSONObject jb = new JSONObject();
            // AjaxHandlerAction ac=new AjaxHandlerAction();
            jb.put("EventId", String.valueOf(newAjaxHandlerAction.getEvent_Id()));
            jb.put("LoginId", newAjaxHandlerAction.getLoginId());
            responseJobj.put("Response","<font color='red' size='3px'>Opps! some thing went wrong.Please try again.</font>");
            


            String serviceUrl = RestRepository.getInstance().getSrviceUrl("getNominatedListByLoginId");
            URL url = new URL(serviceUrl);
            URLConnection connection = url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setConnectTimeout(360 * 5000);
            connection.setReadTimeout(360 * 5000);
            OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
            out.write(jb.toString());
            out.close();

            BufferedReader in = new BufferedReader(new InputStreamReader(
                    connection.getInputStream()));
            String s = null;
            String data = "";
            while ((s = in.readLine()) != null) {
                data = data + s;
            }

            JSONObject jObject = new JSONObject(data);
String CC="";
String NomineeName="";
String to = newAjaxHandlerAction.getLoginId()+"@miraclesoft.com";
            Iterator<?> keys = jObject.keys();
            int i =0;
            String nominatorNames="";
while(keys.hasNext() ) {
    String key = (String)keys.next();
    if ( jObject.get(key) instanceof JSONObject ) {
        JSONObject xx = new JSONObject(jObject.get(key).toString());
        if(i==0){
             CC = xx.getString("NominatorEmail");
             nominatorNames = xx.getString("NominatorName");
        }else{
             CC = CC+","+xx.getString("NominatorEmail");
             nominatorNames =nominatorNames+","+ xx.getString("NominatorName");
        }
       
        
        to = xx.getString("CollegueEmail");
        NomineeName = xx.getString("CollegueName");
      i++;
    }
}



String nominaterNameFormat = "";
String nominatorArr[] = nominatorNames.split("\\,");

for(int j=0;j<nominatorArr.length;j++){
    if(j==0)
    nominaterNameFormat = nominatorArr[j];
    else{
        if(j==nominatorArr.length-1)
            nominaterNameFormat = nominaterNameFormat+" and "+nominatorArr[j];
        else
             nominaterNameFormat = nominaterNameFormat+","+nominatorArr[j];
    }
}

            in.close();
            
            StringBuffer htmlText = new StringBuffer("");
              // for  reminder  Mail body Start   
htmlText.append("<html xmlns='http://www.w3.org/1999/xhtml' class='gr__newsletters-2017-vnallamalla_c9users_io'><head>");
htmlText.append("<meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>");
htmlText.append("<meta name='viewport' content='width=device-width, initial-scale=1'>");
htmlText.append("<meta http-equiv='X-UA-Compatible' content='IE=edge'>");
htmlText.append("<meta name='format-detection' content='telephone=no'>");
htmlText.append("<title>Your nomination has been submitted</title>");
htmlText.append("<style>");
htmlText.append("@import url('https://fonts.googleapis.com/css?family=Montserrat:400,700');");
htmlText.append("@import url('https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i');");
htmlText.append("html{width: 100%;}");
htmlText.append("body{");
htmlText.append("width: 100%;");
htmlText.append("background-color: #ffffff;");
htmlText.append("margin:0; padding:0;");
htmlText.append("-webkit-text-size-adjust:none;");
htmlText.append("-ms-text-size-adjust:none;");
htmlText.append("-webkit-font-smoothing: ;");
htmlText.append("mso-margin-top-alt:0px;");
htmlText.append("mso-margin-bottom-alt:0px;");
htmlText.append("mso-padding-alt: 0px 0px 0px 0px;}");
htmlText.append("div, p, a, li, td { -webkit-text-size-adjust:none; }");
htmlText.append("#outlook a {padding:0;}");
htmlText.append("p,h1,h2,h3,h4{ margin-top:0; margin-bottom:0; padding-top:0; padding-bottom:0;}");
htmlText.append("table{");
htmlText.append("mso-table-lspace:0pt;");
htmlText.append("mso-table-rspace:0pt;");
htmlText.append("}");
htmlText.append("table td {border-collapse: collapse;}");
htmlText.append("span.preheader{display: none;font-size: 1px; visibility: hidden; opacity: 0; color: transparent; height: 0; width: 0;}");
htmlText.append(".googlefix {width: 280px!important; text-align:center!important; min-width:0px!important}");
htmlText.append("a {outline:none;}");
htmlText.append("a {text-decoration: none;}");
htmlText.append("a img {border:none;}");
htmlText.append(".image_fix {display:block;}");
htmlText.append(".ReadMsgBody { width: 100%;}");
htmlText.append(".ExternalClass {width: 100%;}");
htmlText.append(".ExternalClass,");
htmlText.append(".ExternalClass p,");
htmlText.append(".ExternalClass span,");
htmlText.append(".ExternalClass font,");
htmlText.append(".ExternalClass td,");
htmlText.append(".ExternalClass div {");
htmlText.append("line-height: 100%;}");
htmlText.append("img{");
htmlText.append("-ms-interpolation-mode:bicubic;}");
htmlText.append("p {");
htmlText.append("margin:0 !important;");
htmlText.append("padding:0 !important;}");
htmlText.append(".button:hover {filter: brightness(120%);}");
htmlText.append("@media only screen and (max-width: 600px)");
htmlText.append("{");
htmlText.append("body{width:100% !important;}");
htmlText.append(".container           {width:100% !important; min-width:100% !important;}");
htmlText.append(".container-wrap      {display:inline-block !important; width:100% !important; height:auto !important; border-radius:0 !important;}");
htmlText.append(".image-container     {width: 100% !important; height: auto !important; }");
htmlText.append(".disable             {display: none !important;}");
htmlText.append(".enable              {display: inline !important;}");
htmlText.append(".padding-top-10      {padding-top: 10px !important;}");
htmlText.append(".padding-top-20      {padding-top: 20px !important;}");
htmlText.append(".padding-top-30      {padding-top: 30px !important;}");
htmlText.append(".padding-top-40      {padding-top: 40px !important;}");
htmlText.append(".padding-top-60      {padding-top: 60px !important;}");
htmlText.append(".padding-bottom-10   {padding-bottom: 10px !important;}");
htmlText.append(".padding-bottom-20   {padding-bottom: 20px !important;}");
htmlText.append(".padding-bottom-30   {padding-bottom: 30px !important;}");
htmlText.append(".padding-bottom-40   {padding-bottom: 40px !important;}");
htmlText.append(".padding-bottom-60   {padding-bottom: 60px !important;}");
htmlText.append(".padding-none        {padding:0  !important;}");
htmlText.append(".text                {width:90% !important; text-align:center !important;}");
htmlText.append(".text-on-bg          {width:90% !important; text-align:center !important;}");
htmlText.append(".text-center         {text-align: center !important;}");
htmlText.append(".text-right          {text-align: right !important;}");
htmlText.append(".text-left           {text-align: left !important;}");
htmlText.append(".text-white          {color: #ffffff !important;}");
htmlText.append(".background-cover    {background-size: cover !important;}");
htmlText.append(".background-cover-percent    {background-size: 100% 79% !important;}");
htmlText.append(".background-contain  {background-size: contain !important;}");
htmlText.append(".background-auto     {background-size: auto !important;}");
htmlText.append(".background-center   {background-position: center center !important;}");
htmlText.append(".background-none     {background-image: none !important;}");
htmlText.append(".border-none         {border:0 !important;}");
htmlText.append(".border-white        {border-color: #ffffff !important;}");
htmlText.append("}");
htmlText.append("@media only screen and (max-width: 451px)");
htmlText.append("{");
htmlText.append("body {width:100% !important;}");
htmlText.append(".container           {width:100% !important; min-width:100% !important;}");
htmlText.append(".container-wrap      {display:inline-block !important; width:100% !important; height:auto !important; border-radius:0 !important;}");
htmlText.append("}");
htmlText.append("@media only screen and (max-width: 341px)");
htmlText.append("{");
htmlText.append("body {width:100%!important;}");
htmlText.append(".container           {width:100% !important; min-width:100% !important;}");
htmlText.append(".container-wrap      {display:inline-block !important; width:100% !important; height:auto !important; border-radius:0 !important;}");
htmlText.append("}");
htmlText.append("</style>");
htmlText.append("</head>");
htmlText.append("<body marginwidth='0' marginheight='0' style='margin-top: 0; margin-bottom: 0; padding-top: 0; padding-bottom: 0; width: 100%; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%;' offset='0' topmargin='0' leftmargin='0' data-gr-c-s-loaded='true'>");
htmlText.append("<div class='parentOfBg'></div>");
htmlText.append("<div id='edit_link' class='hidden' style='display: none; left: 88px; top: 27956px;'>");
htmlText.append("");
htmlText.append("<div class='close_link'></div>");
htmlText.append("");
htmlText.append("<input type='text' id='edit_link_value' class='createlink' placeholder='Your URL'>");
htmlText.append("");
htmlText.append("<div id='change_image_wrapper' style='display: none;'>");
htmlText.append("");
htmlText.append("<div id='change_image'>");
htmlText.append("");
htmlText.append("<p id='change_image_button'>Change &nbsp; <span class='pixel_result'></span></p>");
htmlText.append("</div>");
htmlText.append("");
htmlText.append("<input type='button' value='' id='change_image_link'>");
htmlText.append("");
htmlText.append("<input type='button' value='' id='remove_image'>");
htmlText.append("</div>");
htmlText.append("");
htmlText.append("<div id='tip'></div>");
htmlText.append("</div>");
htmlText.append("<table mc:repeatable='layout' mc:hideable='' mc:variant='navigation-6' align='center' border='0' cellpadding='0' cellspacing='0' width='100%' style='margin:0 auto; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px; max-width:680px;' data-module='navigation-6'>");
htmlText.append("");
htmlText.append("<tbody>");
htmlText.append("<tr>");
htmlText.append("<td align='center' style='width:100%; height:100%; background-color:#ffffff;' bgcollor='#ffffff' data-bgcolor='nav-6-bg'>");
htmlText.append("<table width='600' align='center' class='container' border='0' cellpadding='0' cellspacing='0' style='width:600px; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px;'>");
htmlText.append("");
htmlText.append("<tbody>");
htmlText.append("<tr>");
htmlText.append("<td align='center' height='20' width='1' style='font-size: 1px; line-height: 20px; height:20px;'>&nbsp;");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("");
htmlText.append("<tr>");
htmlText.append("<td align='center'>");
htmlText.append("<table width='600' align='center' class='container' border='0' cellpadding='0' cellspacing='0' style='width:600px; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px;'>");
htmlText.append("<tbody>");
htmlText.append("<tr>");
htmlText.append("");
htmlText.append("<th width='140' align='left' class='container-wrap' valign='top' style='vertical-align: top;'>");
htmlText.append("<table align='center' cellpadding='0' cellspacing='0'>");
htmlText.append("<tbody>");
htmlText.append("<tr>");
htmlText.append("<td width='160' align='center' valign='top'>");
htmlText.append("<table width='100%' align='center' cellpadding='0' cellspacing='0'>");
htmlText.append("<tbody>");
htmlText.append("");
htmlText.append("<tr>");
htmlText.append("<td align='center' valign='top' style='line-height: 0px !important;'>");
htmlText.append("<a style='text-decoration: none; display: block;' href='https://www.miraclesoft.com/' target='blank'>");
htmlText.append("<img src='https://www.miraclesoft.com/images/newsletters/miracle-logo-dark.png' alt='logo' width='100%' style='width: 100%; max-width: 100%;'>");
htmlText.append("</a>");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("</tbody>");
htmlText.append("</table>");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("</tbody>");
htmlText.append("</table>");
htmlText.append("</th>");
htmlText.append("");
htmlText.append("<th width='320' align='right' class='container-wrap' valign='top' style='vertical-align: top;'>");
htmlText.append("<table width='320' align='right' class='container' border='0' cellpadding='0' cellspacing='0' style='width:320px; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px;'>");
htmlText.append("");
htmlText.append("<tbody>");
htmlText.append("<tr>");
htmlText.append("<td align='center' height='1' width='1' style='font-size: 1px; line-height: 1px; height:1px;'>&nbsp;");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("");
htmlText.append("<tr>");
htmlText.append("<td align='right'>");
htmlText.append("<table width='auto' align='right' class='container' border='0' cellpadding='0' cellspacing='0' style='width:autopx; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px;'>");
htmlText.append("<tbody>");
htmlText.append("<tr>");
htmlText.append("<td align='center'>");
htmlText.append("<table width='auto' align='center' class='' border='0' cellpadding='0' cellspacing='0' style='border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px;'>");
htmlText.append("<tbody><tr>");
htmlText.append("<td align='center' height='15'>");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("<tr>");
htmlText.append("");
htmlText.append("<th align='center' height='1' width='4' style='font-size: 1px; line-height: 1px; width:4px;'>");
htmlText.append("</th>");
htmlText.append("");
htmlText.append("<th style='text-align:center; font-family: oPen sans; font-size:12px; line-height: 20px; text-decoration: none; color: #000000; font-weight:400; text-transform: uppercase; : 0.05em;'>");
htmlText.append("<a href='https://www.miraclesoft.com/company/' target='blank' style='text-align:center; font-family: oPen sans; font-size:12px; line-height: 20px; text-decoration: none; color: #000000; font-weight:600; text-transform: uppercase; : 0.05em;' data-size='nav-6-link-size' data-min='9' data-max='40' data-color='nav-6-link-color' mc:edit='nav-6-link-2'>");
htmlText.append("<multiline>About us |</multiline>");
htmlText.append("</a>");
htmlText.append("</th>");
htmlText.append("");
htmlText.append("<th align='center' height='1' width='4' style='font-size: 1px; line-height: 1px; width:4px;'>");
htmlText.append("</th>");
htmlText.append("");
htmlText.append("<th style='text-align:center; font-family: oPen sans; font-size:12px; line-height: 20px; text-decoration: none; color: #000000; font-weight:400; text-transform: uppercase; : 0.05em;'>");
htmlText.append("<a href='https://www.miraclesoft.com/services/' target='blank' style='text-align:center; font-family: oPen sans; font-size:12px; line-height: 20px; text-decoration: none; color: #000000; font-weight:600; text-transform: uppercase; : 0.05em;' data-size='nav-6-link-size' data-min='9' data-max='40' data-color='nav-6-link-color' mc:edit='nav-6-link-3'>");
htmlText.append("<multiline> Our Services |</multiline>");
htmlText.append("</a>");
htmlText.append("</th>");
htmlText.append("");
htmlText.append("<th align='center' height='1' width='4' style='font-size: 1px; line-height: 1px; width:4px;'>");
htmlText.append("</th>");
htmlText.append("");
htmlText.append("<th style='text-align:center; font-family: oPen sans; font-size:12px; line-height: 20px; text-decoration: none; color: #232527; font-weight:600; text-transform: uppercase; : 0.05em;'>");
htmlText.append("<a href='https://www.miraclesoft.com/contact/' target='blank' style='text-align:center; font-family: oPen sans; font-size:12px; line-height: 20px; text-decoration: none; color: #232527; font-weight:600; text-transform: uppercase; : 0.05em;' data-size='nav-6-signUp-size' data-min='9' data-max='40' data-color='nav-6-signUp-color' mc:edit='nav-6-signUp-4'>");
htmlText.append("<multiline>Contact us</multiline>");
htmlText.append("</a>");
htmlText.append("</th>");
htmlText.append("");
htmlText.append("</tr>");
htmlText.append("</tbody>");
htmlText.append("</table>");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("</tbody>");
htmlText.append("</table>");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("");
htmlText.append("<tr>");
htmlText.append("<td align='center' height='18' width='1' style='font-size: 1px; line-height: 18px; height:18px;'>&nbsp;");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("");
htmlText.append("</tbody>");
htmlText.append("</table>");
htmlText.append("</th>");
htmlText.append("");
htmlText.append("</tr>");
htmlText.append("</tbody>");
htmlText.append("</table>");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("</tbody>");
htmlText.append("</table>");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("");
htmlText.append("</tbody>");
htmlText.append("</table>");
htmlText.append("<table mc:repeatable='layout' mc:hideable='' mc:variant='navigation-margin' align='center' border='0' cellpadding='0' cellspacing='0' width='100%' style='margin:0 auto; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px; max-width:680px;' data-module='navigation-margin'>");
htmlText.append("");
htmlText.append("<tbody>");
htmlText.append("<tr>");
htmlText.append("<td align='center' style='width:100%; height:100%; background-color:#ffffff;' bgcollor='#ffffff' data-bgcolor='navigation-margin-bg'>");
htmlText.append("<table width='600' align='center' class='container' border='0' cellpadding='0' cellspacing='0' style='width:600px; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px;'>");
htmlText.append("");
htmlText.append("<tbody>");
htmlText.append("<tr>");
htmlText.append("<td align='center' height='20' width='1' style='font-size: 1px;line-height: 10px;height: 10px;'>&nbsp;");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("");
htmlText.append("</tbody>");
htmlText.append("</table>");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("");
htmlText.append("</tbody>");
htmlText.append("</table>");
htmlText.append("<div class='parentOfBg'></div>");
htmlText.append("<table align='center' border='0' cellpadding='0' cellspacing='0' width='100%' style='margin:0 auto; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px; max-width:680px;' data-module='text-block-1'>");
htmlText.append("");
htmlText.append("<tbody>");
htmlText.append("<tr>");
htmlText.append("<td align='center' style='width:100%; height:100%; background-color:#f8f8f8;' data-bgcolor='txt-blck-1-bg'>");
htmlText.append("<table width='600' align='center' class='text' border='0' cellpadding='0' cellspacing='0' style='width:600px; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px;'>");
htmlText.append("");
htmlText.append("<tbody>");
htmlText.append("<tr>");
htmlText.append("<td align='center' height='35' width='1' style='font-size: 1px; line-height: 35px; height:35px;'>");
htmlText.append("&nbsp;");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("");
htmlText.append("<tr>");
htmlText.append("<td style='text-align: center;font-family: 'Open Sans' , sans-serif;font-size: 25px;line-height: 20px;text-decoration: none;color: #232527;font-weight:700;' data-size='txt-blck-1-text-size' data-min='9' data-max='40' data-color='txt-blck-1-text-color'>");
htmlText.append("<multiline>You Have Been Nominated | Reminder");
htmlText.append("</multiline>");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("");
htmlText.append("<tr>");
htmlText.append("<td align='center' height='25' width='1' style='font-size: 1px;line-height: 25px;height: 25px;'>");
htmlText.append("&nbsp;");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("<tr>");
htmlText.append("<td align='center'>");
htmlText.append("<table width='60' height='2' align='center' border='0' cellpadding='0' cellspacing='0' style='width:60px; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px; background-color: #232527' data-bgcolor='team-6-line-color'>");
htmlText.append("");
htmlText.append("<tbody>");
htmlText.append("<tr>");
htmlText.append("<td align='center' height='2' width='40' style='font-size: 2px; line-height: 2px; width:40px;'></td>");
htmlText.append("</tr>");
htmlText.append("");
htmlText.append("</tbody>");
htmlText.append("</table>");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("");
htmlText.append("</tbody>");
htmlText.append("</table>");
htmlText.append("</td>");
htmlText.append("");
htmlText.append("</tr>");
htmlText.append("");
htmlText.append("</tbody>");
htmlText.append("</table>");
htmlText.append("<table mc:repeatable='layout' mc:hideable='' mc:variant='text-block-1' align='center' border='0' cellpadding='0' cellspacing='0' width='100%' style='margin:0 auto; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px; max-width:680px;' data-module='text-block-1'>");
htmlText.append("");
htmlText.append("<tbody>");
htmlText.append("<tr>");
htmlText.append("<td align='center' style='width:100%; height:100%; background-color:#f8f8f8;' bgcollor='#f8f8f8' data-bgcolor='txt-blck-1-bg'>");
htmlText.append("<table width='600' align='center' class='text' border='0' cellpadding='0' cellspacing='0' style='width:600px; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px;'>");
htmlText.append("");
htmlText.append("<tbody>");
htmlText.append("<tr>");
htmlText.append("<td align='center' height='25' width='1' style='font-size: 1px; line-height: 25px; height:25px;'>&nbsp;");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("");
htmlText.append("<tr>");
htmlText.append("<td style='text-align:left;font-family: 'Open Sans' , sans-serif;font-size: 14px;line-height: 20px;text-decoration: none;color: #232527;font-weight:400;' data-size='txt-blck-1-text-size' data-min='9' data-max='40' data-color='txt-blck-1-text-color' mc:edit='txt-blck-1-text-1'>");
htmlText.append("<multiline> <b>" + NomineeName + ",</b>");
htmlText.append("</multiline>");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("");
htmlText.append("</tbody>");
htmlText.append("</table>");
htmlText.append("</td>");
htmlText.append("");
htmlText.append("</tr>");
htmlText.append("");
htmlText.append("</tbody>");
htmlText.append("</table>");
htmlText.append("");
htmlText.append("");
htmlText.append("<table mc:repeatable='layout' mc:hideable='' mc:variant='text-block-1' align='center' border='0' cellpadding='0' cellspacing='0' width='100%' style='margin:0 auto; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px; max-width:680px;' data-module='text-block-1'>");
htmlText.append("");
htmlText.append("<tbody>");
htmlText.append("<tr>");
htmlText.append("<td align='center' style='width:100%; height:100%; background-color:#f8f8f8;' bgcollor='#f8f8f8' data-bgcolor='txt-blck-1-bg'>");
htmlText.append("<table width='600' align='center' class='text' border='0' cellpadding='0' cellspacing='0' style='width:600px; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px;'>");
htmlText.append("");
htmlText.append("<tbody>");
htmlText.append("<tr>");
htmlText.append("<td align='center' height='25' width='1' style='font-size: 1px;line-height: 15px;height: 15px;'>&nbsp;");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("");
htmlText.append("<tr>");
htmlText.append("<td style='text-align:justify; font-family: 'Open Sans' , sans-serif; font-size:14px; line-height: 24px; text-decoration: none; color: #232527; font-weight:400;' data-size='txt-blck-1-text-size' data-min='9' data-max='40' data-color='txt-blck-1-text-color' mc:edit='txt-blck-1-text-1'>");
htmlText.append("<multiline label='content'>We would like to remind you to fill in your nomination details. You were nominated by <b>"+nominaterNameFormat+"</b> to attend <b>"+newAjaxHandlerAction.getEventName()+"</b> at <b>"+newAjaxHandlerAction.getEventLocation()+"</b> from <b>"+DateUtility.getInstance().getDateWithMonthNameFromUsFormat(newAjaxHandlerAction.getEventStartDate())+"</b> to <b>"+DateUtility.getInstance().getDateWithMonthNameFromUsFormat(newAjaxHandlerAction.getEventEndDate())+"</b>.");
htmlText.append("</multiline>");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("");
htmlText.append("");
htmlText.append("");
htmlText.append("");
htmlText.append("</tbody>");
htmlText.append("</table>");
htmlText.append("</td>");
htmlText.append("");
htmlText.append("</tr>");
htmlText.append("");
htmlText.append("</tbody>");
htmlText.append("</table>");
htmlText.append("");
htmlText.append("<table mc:repeatable='layout' mc:hideable='' mc:variant='text-block-1' align='center' border='0' cellpadding='0' cellspacing='0' width='100%' style='margin:0 auto; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px; max-width:680px;' data-module='text-block-1'>");
htmlText.append("");
htmlText.append("<tbody>");
htmlText.append("<tr>");
htmlText.append("<td align='center' style='width:100%; height:100%; background-color:#f8f8f8;' bgcollor='#f8f8f8' data-bgcolor='txt-blck-1-bg'>");
htmlText.append("<table width='600' align='center' class='text' border='0' cellpadding='0' cellspacing='0' style='width:600px; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px;'>");
htmlText.append("");
htmlText.append("<tbody>");
htmlText.append("<tr>");
htmlText.append("<td align='center' height='25' width='1' style='font-size: 1px;line-height: 15px;height: 15px;'>&nbsp;");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("");
htmlText.append("<tr>");
htmlText.append("<td style='text-align:justify; font-family: 'Open Sans' , sans-serif; font-size:14px; line-height: 24px; text-decoration: none; color: #232527; font-weight:400;' data-size='txt-blck-1-text-size' data-min='9' data-max='40' data-color='txt-blck-1-text-color' mc:edit='txt-blck-1-text-1'>");
htmlText.append("<multiline label='content'>Once you have submitted the application our team will go through the submissions and select a group to attend the event. For more information please reach out to <a href='mailto:partnerships@miraclesoft.com'><span style='color:#00aae7;'>partnerships@miraclesoft.com</span></a> (or) visit <a href='https://www.miraclesoft.com/conferencenominations' target='blank'><span style='color:#00aae7;'>www.miraclesoft.com/conferencenominations</span></a></multiline>");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("");
htmlText.append("</tbody>");
htmlText.append("</table>");
htmlText.append("</td>");
htmlText.append("");
htmlText.append("</tr>");
htmlText.append("");
htmlText.append("</tbody>");
htmlText.append("</table>");
htmlText.append("");
htmlText.append("");
htmlText.append("<table mc:repeatable='layout' mc:hideable='' mc:variant='text-block-1' align='center' border='0' cellpadding='0' cellspacing='0' width='100%' style='margin:0 auto; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px; max-width:680px;' data-module='text-block-1'>");
htmlText.append("");
htmlText.append("<tbody>");
htmlText.append("<tr>");
htmlText.append("<td align='center' style='width:100%; height:100%; background-color:#f8f8f8;' bgcollor='#f8f8f8' data-bgcolor='txt-blck-1-bg'>");
htmlText.append("<table width='600' align='center' class='text' border='0' cellpadding='0' cellspacing='0' style='width:600px; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px;'>");
htmlText.append("");
htmlText.append("<tbody>");
htmlText.append("<tr>");
htmlText.append("<td align='center' height='25' width='1' style='font-size: 1px;line-height: 15px;height: 15px;'>&nbsp;");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("");
htmlText.append("<tr>");
htmlText.append("<td style='text-align:left; font-family: 'Open Sans' , sans-serif; font-size:14px; line-height: 24px; text-decoration: none; color: #232527; font-weight:400;' data-size='txt-blck-1-text-size' data-min='9' data-max='40' data-color='txt-blck-1-text-color' mc:edit='txt-blck-1-text-1'>");
htmlText.append("<multiline><b>Thanks &amp; Regards</b><br><b>Partnerships Team</b><br>");
htmlText.append("");
htmlText.append("Phone : (248)-412-0430<br>");
htmlText.append("Email : <a href='mailto:partnerships@miraclesoft.com'><span style='color:#232527;'>partnerships@miraclesoft.com</span></a>");
htmlText.append("</multiline>");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("");
htmlText.append("<tr>");
htmlText.append("<td align='center' height='46' width='1' style='font-size: 1px; line-height: 46px; height:46px;'>&nbsp;");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("");
htmlText.append("</tbody>");
htmlText.append("</table>");
htmlText.append("</td>");
htmlText.append("");
htmlText.append("</tr>");
htmlText.append("");
htmlText.append("</tbody>");
htmlText.append("</table>");
htmlText.append("<table mc:repeatable='layout' mc:hideable='' mc:variant='footer-5' align='center' border='0' cellpadding='0' cellspacing='0' width='100%' style='margin:0 auto; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px; max-width:680px;' data-module='footer-5'>");
htmlText.append("");
htmlText.append("<tbody>");
htmlText.append("<tr>");
htmlText.append("<td align='center' style='width:100%; height:100%; background-color:#ffffff;' bgcollor='#ffffff' data-bgcolor='footer-5-bg'>");
htmlText.append("<table width='600' align='center' class='container' border='0' cellpadding='0' cellspacing='0' style='width:600px; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px;'>");
htmlText.append("");
htmlText.append("<tbody>");
htmlText.append("<tr>");
htmlText.append("<td align='center' height='35' width='1' style='font-size: 1px;line-height: 15px;height: 15px;'>&nbsp;");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("");
htmlText.append("<tr>");
htmlText.append("<td align='center'>");
htmlText.append("<table width='600' align='center' class='text' border='0' cellpadding='0' cellspacing='0' style='width:600px; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px;'>");
htmlText.append("<tbody>");
htmlText.append("<tr>");
htmlText.append("");
htmlText.append("<th width='360' align='left' class='container-wrap' valign='top' style='vertical-align: top;'>");
htmlText.append("<table width='360' align='left' class='container' border='0' cellpadding='0' cellspacing='0' style='width:360px; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px;'>");
htmlText.append("");
htmlText.append("<tbody>");
htmlText.append("<tr>");
htmlText.append("<td class='text-left' style='vertical-align:top;text-align:left;font-family: 'Open Sans', sans-serif;font-size:13px;padding-top: 5px;line-height: 20px;text-decoration: none;color: #232527;font-weight: 400;' data-size='footer-5-rights-size' data-min='9' data-max='40' data-color='footer-5-rights-color' mc:edit='footer-5-rights-1.1'>");
htmlText.append("<multiline>Â© Copyrights 2017 | Miracle Software Systems, Inc.</multiline>");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("");
htmlText.append("<tr>");
htmlText.append("<td align='center' height='13' width='1' style='font-size: 1px; line-height: 13px; height:13px;'>&nbsp;");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("");
htmlText.append("<tr>");
htmlText.append("<td align='center' height='39' width='1' style='font-size: 1px;line-height: 20px;height: 20px;'>&nbsp;");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("");
htmlText.append("</tbody>");
htmlText.append("</table>");
htmlText.append("</th>");
htmlText.append("");
htmlText.append("<th width='280' align='right' class='container-wrap' valign='top' style='vertical-align: top;'>");
htmlText.append("<table width='280' align='right' class='container' border='0' cellpadding='0' cellspacing='0' style='width:280px; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px;'>");
htmlText.append("");
htmlText.append("<tbody>");
htmlText.append("<tr>");
htmlText.append("<td class='disable' align='center' height='5' width='1' style='font-size: 1px; line-height: 5px; height:5px;'>&nbsp;");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("");
htmlText.append("<tr>");
htmlText.append("<td align='right'>");
htmlText.append("<table width='135' align='right' class='container' border='0' cellpadding='0' cellspacing='0' style='width:135px; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; border: 0px;'>");
htmlText.append("");
htmlText.append("<tbody>");
htmlText.append("<tr>");
htmlText.append("<td align='left'>");
htmlText.append("<table style='border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;border: 0px;' align='left' border='0' cellpadding='0' cellspacing='0' width='auto'>");
htmlText.append("<tbody>");
htmlText.append("<tr>");
htmlText.append("<th>");
htmlText.append("<a href='https://www.facebook.com/miracle45625/' target='blank' style='color:#ef4048; text-decoration: none;'><img src='https://www.miraclesoft.com/images/newsletters/facebook1.png' width='20' height='20' alt='#' style='display:block; vertical-align:middle;'></a>   </th>");
htmlText.append("");
htmlText.append("<td align='center' height='1' width='10' style='font-size: 1px; line-height: 1px; width:10px;'>");
htmlText.append("</td>");
htmlText.append("");
htmlText.append("<th>");
htmlText.append("<a href='https://plus.google.com/+Team_MSS' target='blank' style='color:#ef4048; text-decoration: none;'><img src='https://www.miraclesoft.com/images/newsletters/googleplus1.png' width='20' height='20' alt='#' style='display:block; vertical-align:middle;'></a>     </th>");
htmlText.append("");
htmlText.append("<td align='center' height='1' width='10' style='font-size: 1px; line-height: 1px; width:10px;'>");
htmlText.append("</td>");
htmlText.append("");
htmlText.append("<th>");
htmlText.append("<a href='https://www.linkedin.com/company/miracle-software-systems-inc' target='blank' style='color:#ef4048; text-decoration: none;'><img src='https://www.miraclesoft.com/images/newsletters/Linkedin1.png' width='20' height='20' alt='#' style='display:block; vertical-align:middle;'></a>     </th>");
htmlText.append("");
htmlText.append("<td align='center' height='1' width='10' style='font-size: 1px; line-height: 1px; width:10px;'>");
htmlText.append("</td>");
htmlText.append("");
htmlText.append("<th>");
htmlText.append("</th><td>                        <a href='https://www.youtube.com/c/Team_MSS' target='blank' style='color:#ef4048; text-decoration: none;'><img src='https://www.miraclesoft.com/images/newsletters/youtube1.png' width='20' height='20' alt='#' style='display:block; vertical-align:middle;'></a>     ");
htmlText.append("");
htmlText.append("</td><td align='center' height='1' width='10' style='font-size: 1px; line-height: 1px; width:10px;'>");
htmlText.append("</td>");
htmlText.append("");
htmlText.append("<th>");
htmlText.append("</th><td>                        <a href='https://twitter.com/Team_MSS' target='blank' style='color:#ef4048; text-decoration: none;'><img src='https://www.miraclesoft.com/images/newsletters/Twitter1.png' width='20' height='20' alt='#' style='display:block; vertical-align:middle;'></a>     ");
htmlText.append("");
htmlText.append("</td><td align='center' height='1' width='10' style='font-size: 1px; line-height: 1px; width:10px;'>");
htmlText.append("</td>");
htmlText.append("");
htmlText.append("<th>");
htmlText.append("</th></tr>");
htmlText.append("</tbody>");
htmlText.append("</table>");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("");
htmlText.append("</tbody>");
htmlText.append("</table>");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("");
htmlText.append("<tr>");
htmlText.append("<td align='center' height='39' width='1' style='font-size: 1px;line-height: 20px;height: 20px;'>&nbsp;");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("");
htmlText.append("</tbody>");
htmlText.append("</table>");
htmlText.append("</th>");
htmlText.append("");
htmlText.append("</tr>");
htmlText.append("</tbody>");
htmlText.append("</table>");
htmlText.append("</td>");
htmlText.append("</tr>");
htmlText.append("</tbody>");
htmlText.append("</table>");
htmlText.append("</td>");
htmlText.append("");
htmlText.append("</tr>");
htmlText.append("");
htmlText.append("</tbody>");
htmlText.append("</table>");
htmlText.append("");
htmlText.append("<span class='gr__tooltip'><span class='gr__tooltip-content'></span><i class='gr__tooltip-logo'></i><span class='gr__triangle'></span></span></body></html>");
//System.out.println("length-->"+htmlText.length());
         //Mail Body end for reminder   
            
        String  bCCAddress = "";
        String  createdDate = "";
        
                    // toAddress = emailId;
                  //  System.out.println("inserted------------hello");
                    //ServiceLocator.getMailServices().doAddEmailLog(toAddress, cCAddress, issueTitle, bodyContent, createdDate, bCCAddress);
                    ServiceLocator.getMailServices().doAddEmailLogNew(to, CC, "Nomination Reminder", htmlText.toString(), createdDate, bCCAddress, "Nomination Reminder");

                   // message = "<font color=\"green\" size=\"2px\">Status Updated Succesfully and Mail had been sent to Attendees!</font>";
    
            
            responseJobj.put("Response","<font color='green' size='3px'>Reminder sent successfully.</font>");
            

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        //System.out.println("message in impl----" + message);
        return responseJobj.toString();
    }
    
   
     public JSONObject doAddSurveyFormAndQuestionnaire(NewAjaxHandlerAction ajaxHandlerAction) throws ServiceLocatorException{
           
 String message ="";
    
        JSONObject jObject =null;
        try {
                JSONObject jb = new JSONObject();
       
              /*  System.out.println("ajaxHandlerAction.getMonth()"+ajaxHandlerAction.getMonth());
                System.out.println("ajaxHandlerAction.getExpireDate()"+ajaxHandlerAction.getExpireDate());
                System.out.println("ajaxHandlerAction.getMovieDates()"+ajaxHandlerAction.getMovieDates());
                System.out.println("ajaxHandlerAction.getMovieName()"+ajaxHandlerAction.getMovieName());
                System.out.println("ajaxHandlerAction.getCreatedBy()"+ajaxHandlerAction.getCreatedBy());*/
               
                
        jb.put("month", ajaxHandlerAction.getMonth());
        jb.put("ExpireDate", ajaxHandlerAction.getExpireDate());
        jb.put("MovieDates", ajaxHandlerAction.getMovieDates());
        jb.put("MovieName", ajaxHandlerAction.getMovieName());
       
        jb.put("CreatedBy", ajaxHandlerAction.getCreatedBy());
      
     
         String serviceUrl = RestRepository.getInstance().getSrviceUrl("doAddSurveyFormAndQuestionnaire");
              URL url = new URL(serviceUrl);
            URLConnection connection = url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setConnectTimeout(300*15000);
            connection.setReadTimeout(300*15000);
            OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
           out.write(jb.toString());
            out.close();

            BufferedReader in = new BufferedReader(new InputStreamReader(
                    connection.getInputStream()));
            String s = null;
            String data = "";
            while ((s = in.readLine()) != null) {

                data = data + s;
            }

         jObject = new JSONObject(data);

         
     


            in.close();
           
        } catch (IOException ex) {
            ex.printStackTrace();
        }catch(Exception e){
             throw new ServiceLocatorException(e);
        }
        return jObject;   
     }
 public JSONObject doAddRemainder(NewAjaxHandlerAction ajaxHandlerAction) throws ServiceLocatorException{
           
 String message ="";
    
        JSONObject jObject =null;
        try {
                JSONObject jb = new JSONObject();
       
           
              //  System.out.println("ajaxHandlerAction.getSurvyId()"+ajaxHandlerAction.getSurvyId());
             
       
  
        jb.put("survyId", ajaxHandlerAction.getSurvyId());
        
     
      
     
         String serviceUrl = RestRepository.getInstance().getSrviceUrl("doAddRemainder");
              URL url = new URL(serviceUrl);
            URLConnection connection = url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setConnectTimeout(300*5000);
            connection.setReadTimeout(300*5000);
            OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
           out.write(jb.toString());
            out.close();

            BufferedReader in = new BufferedReader(new InputStreamReader(
                    connection.getInputStream()));
            String s = null;
            String data = "";
            while ((s = in.readLine()) != null) {

                data = data + s;
            }

         jObject = new JSONObject(data);

         
     


            in.close();
           
        } catch (IOException ex) {
            ex.printStackTrace();
        }catch(Exception e){
             throw new ServiceLocatorException(e);
        }
        return jObject;   
     }
  
 public JSONObject doAddSurveyfeedBack(NewAjaxHandlerAction ajaxHandlerAction) throws ServiceLocatorException{
           
 String message ="";
    
        JSONObject jObject =null;
        try {
                JSONObject jb = new JSONObject();
       
              /*  System.out.println("ajaxHandlerAction.getMonth()"+ajaxHandlerAction.getMonth());
                System.out.println("ajaxHandlerAction.getExpireDate()"+ajaxHandlerAction.getExpireDate());
                System.out.println("ajaxHandlerAction.getMovieDates()"+ajaxHandlerAction.getYear());
                System.out.println("ajaxHandlerAction.getMovieName()"+ajaxHandlerAction.getSurvyId());
                System.out.println("ajaxHandlerAction.getCreatedBy()"+ajaxHandlerAction.getCreatedBy());*/
               
                
        jb.put("month", ajaxHandlerAction.getMonth());
        jb.put("year", ajaxHandlerAction.getYear().substring(ajaxHandlerAction.getYear().length()-2, ajaxHandlerAction.getYear().length()));
        jb.put("ExpireDate", ajaxHandlerAction.getExpireDate());
       // jb.put("survyId", ajaxHandlerAction.getSurvyId());
        
        jb.put("CreatedBy", ajaxHandlerAction.getCreatedBy());
      
     
         String serviceUrl = RestRepository.getInstance().getSrviceUrl("doAddSurveyfeedBack");
              URL url = new URL(serviceUrl);
            URLConnection connection = url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setConnectTimeout(300*5000);
            connection.setReadTimeout(300*5000);
            OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
           out.write(jb.toString());
            out.close();

            BufferedReader in = new BufferedReader(new InputStreamReader(
                    connection.getInputStream()));
            String s = null;
            String data = "";
            while ((s = in.readLine()) != null) {

                data = data + s;
            }

         jObject = new JSONObject(data);

         
     


            in.close();
           
        } catch (IOException ex) {
            ex.printStackTrace();
        }catch(Exception e){
             throw new ServiceLocatorException(e);
        }
        return jObject;   
     }     
 public String updateEmployeeVerificationDetails(String employeeVerificationJson,String modifiedBy) throws ServiceLocatorException{
     
 String message ="";
    
        JSONObject jObject =null;
        try {
                JSONObject jb = new JSONObject();
       
              // System.out.println("In web");
               
                
        jb.put("EmployeeDetailsJson", employeeVerificationJson);
       
        
        jb.put("ModifiedBy", modifiedBy);
      
     
         String serviceUrl = RestRepository.getInstance().getSrviceUrl("doUpdateEmployeeVerificationDetails");
              URL url = new URL(serviceUrl);
            URLConnection connection = url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setConnectTimeout(300*5000);
            connection.setReadTimeout(300*5000);
            OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
           out.write(jb.toString());
            out.close();

            BufferedReader in = new BufferedReader(new InputStreamReader(
                    connection.getInputStream()));
            String s = null;
            String data = "";
            while ((s = in.readLine()) != null) {

                data = data + s;
            }

         jObject = new JSONObject(data);
         message = jObject.getString("message");
         
         


            in.close();
           
        } catch (IOException ex) {
            ex.printStackTrace();
        }catch(Exception e){
             throw new ServiceLocatorException(e);
        }
        return message;   
     }     
 public String deleteEmpVerification(int id) throws Exception {
     // Connection connection = null;
     //PreparedStatement preparedStatement = null;
     // ResultSet resultSet = null;
     String message = "error";
     boolean isUpdated = false;
          try {


         JSONObject jb = new JSONObject();
         // AjaxHandlerAction ac=new AjaxHandlerAction();
         jb.put("Id", id);
        



         String serviceUrl = RestRepository.getInstance().getSrviceUrl("deleteEmpVerify");
         URL url = new URL(serviceUrl);
         URLConnection connection = url.openConnection();
         connection.setDoOutput(true);
         connection.setRequestProperty("Content-Type", "application/json");
         connection.setConnectTimeout(360 * 5000);
         connection.setReadTimeout(360 * 5000);
         OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
         out.write(jb.toString());
         out.close();

         BufferedReader in = new BufferedReader(new InputStreamReader(
                 connection.getInputStream()));
         String s = null;
         String data = "";
         while ((s = in.readLine()) != null) {

             data = data + s;
         }

         JSONObject jObject = new JSONObject(data);

         
         message = jObject.getString("message");
         
        // System.out.println("message-->"+message);

         in.close();

     } catch (IOException ex) {
         ex.printStackTrace();
     }
   //  System.out.println("message in impl----" + message);
     return message;
 }
 
 public String doUpdateEventDetailsForConference(NewAjaxHandlerAction ajaxHandlerAction)
			throws ServiceLocatorException {

		String message = "";

		JSONObject jObject = null;
		try {
			JSONObject jb = new JSONObject();

			jb.put("EventCoordinatorName", ajaxHandlerAction.getEventCoordinatorName());
			jb.put("EventCoordinatorEmail", ajaxHandlerAction.getEventCoordinatorEmail());
			jb.put("EventCoordinatorPhone", ajaxHandlerAction.getEventCoordinatorPhone());
			jb.put("OnsiteCoordinatorName", ajaxHandlerAction.getOnsiteCoordinatorName());
			jb.put("OnsiteCoordinatorEmail", ajaxHandlerAction.getOnsiteCoordinatorEmail());
			jb.put("OnsiteCoordinatorPhone", ajaxHandlerAction.getOnsiteCoordinatorPhone());
			jb.put("ConferenceId", ajaxHandlerAction.getConferenceId());
			jb.put("ModifiedBy", ajaxHandlerAction.getCreatedBy());

			String serviceUrl = RestRepository.getInstance().getSrviceUrl("doUpdateEventCoordinatorDetailsForConference");
			URL url = new URL(serviceUrl);
			URLConnection connection = url.openConnection();
			connection.setDoOutput(true);
			connection.setRequestProperty("Content-Type", "application/json");
			connection.setConnectTimeout(300 * 5000);
			connection.setReadTimeout(300 * 5000);
			OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
			out.write(jb.toString());
			out.close();

			BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			String s = null;
			String data = "";
			while ((s = in.readLine()) != null) {

				data = data + s;
			}

			jObject = new JSONObject(data);
			message=jObject.getString("message");

			in.close();

		} catch (IOException ex) {
			ex.printStackTrace();
		} catch (Exception e) {
			throw new ServiceLocatorException(e);
		}
		return message;
	}

 public String doAddLogisticDetailsForConference(NewAjaxHandlerAction ajaxHandlerAction)
			throws ServiceLocatorException {

		String message = "";

		JSONObject jObject = null;
		try {
			JSONObject jb = new JSONObject();

			jb.put("CreatedBy", ajaxHandlerAction.getCreatedBy());
			jb.put("Data", ajaxHandlerAction.getLogisticDetails());
			jb.put("FlightAttachmentLocation", ajaxHandlerAction.getFlightAttachmentLocation());
			jb.put("HotelAttachmentLocation", ajaxHandlerAction.getHotelAttachmentLocation());
			jb.put("TransportAttachmentLocation", ajaxHandlerAction.getTransportAttachmentLocation());
			String serviceUrl = RestRepository.getInstance().getSrviceUrl("doAddLogisticDetailsForMCON");
			URL url = new URL(serviceUrl);
			URLConnection connection = url.openConnection();
			connection.setDoOutput(true);
			connection.setRequestProperty("Content-Type", "application/json");
			connection.setConnectTimeout(300 * 5000);
			connection.setReadTimeout(300 * 5000);
			OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
			out.write(jb.toString());
			out.close();

			BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			String s = null;
			String data = "";
			while ((s = in.readLine()) != null) {

				data = data + s;
			}

			jObject = new JSONObject(data);
			message=jObject.getString("message");

			in.close();

		} catch (IOException ex) {
			ex.printStackTrace();
		} catch (Exception e) {
			throw new ServiceLocatorException(e);
		}
		return message;
	}
	
	
	public String doUpdateLogisticDetailsForConference(NewAjaxHandlerAction ajaxHandlerAction)
			throws ServiceLocatorException {

		String message = "";

		JSONObject jObject = null;
		try {
			JSONObject jb = new JSONObject();

			jb.put("CreatedBy", ajaxHandlerAction.getCreatedBy());
			jb.put("Data", ajaxHandlerAction.getLogisticDetails());
			jb.put("FlightAttachmentLocation", ajaxHandlerAction.getFlightAttachmentLocation());
			jb.put("HotelAttachmentLocation", ajaxHandlerAction.getHotelAttachmentLocation());
			jb.put("TransportAttachmentLocation", ajaxHandlerAction.getTransportAttachmentLocation());

			String serviceUrl = RestRepository.getInstance().getSrviceUrl("updateLogisticDetailsForMCON");
			URL url = new URL(serviceUrl);
			URLConnection connection = url.openConnection();
			connection.setDoOutput(true);
			connection.setRequestProperty("Content-Type", "application/json");
			connection.setConnectTimeout(300 * 5000);
			connection.setReadTimeout(300 * 5000);
			OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
			out.write(jb.toString());
			out.close();

			BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			String s = null;
			String data = "";
			while ((s = in.readLine()) != null) {

				data = data + s;
			}

			jObject = new JSONObject(data);
			message=jObject.getString("message");

			in.close();

		} catch (IOException ex) {
			ex.printStackTrace();
		} catch (Exception e) {
			throw new ServiceLocatorException(e);
		}
		return message;
	}
	
	public String doAddNomineeDetailsForConference(NewAjaxHandlerAction ajaxHandlerAction)
			throws ServiceLocatorException {

		String message = "";

		JSONObject jObject = null;
		try {
			JSONObject jb = new JSONObject();

			jb.put("Data", ajaxHandlerAction.getAddLogisticDetails());
			jb.put("CreatedBy", ajaxHandlerAction.getCreatedBy());

			String serviceUrl = RestRepository.getInstance().getSrviceUrl("doAddNomineeDetailsForConference");
			URL url = new URL(serviceUrl);
			URLConnection connection = url.openConnection();
			connection.setDoOutput(true);
			connection.setRequestProperty("Content-Type", "application/json");
			connection.setConnectTimeout(300 * 5000);
			connection.setReadTimeout(300 * 5000);
			OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
			out.write(jb.toString());
			out.close();

			BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			String s = null;
			String data = "";
			while ((s = in.readLine()) != null) {

				data = data + s;
			}

			jObject = new JSONObject(data);
			message=jObject.getString("message");

			in.close();

		} catch (IOException ex) {
			ex.printStackTrace();
		} catch (Exception e) {
			throw new ServiceLocatorException(e);
		}
		return message;
	}
	
	 public String getLogisticDetailsByConfIdAndNomineeId(int confId,String nominneLoginId) throws Exception {
			String message = "";
			boolean isUpdated = false;
			String data = "";
			try {

				JSONObject jb = new JSONObject();

				jb.put("ConferenceId", confId);
				jb.put("NomineeLoginId", nominneLoginId);
				jb.put("Authorization", "bXM1VzNiUzNSdiFjZTU6M2daQGQyKnlod2Jtcj1RKw==");
				String serviceUrl = RestRepository.getInstance().getSrviceUrl("getLogsticDetailsByConfIdAndNomineeId");
				URL url = new URL(serviceUrl);
				URLConnection connection = url.openConnection();
				connection.setDoOutput(true);
				connection.setRequestProperty("Content-Type", "application/json");
				connection.setConnectTimeout(360 * 5000);
				connection.setReadTimeout(360 * 5000);
				OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
				out.write(jb.toString());
				out.close();

				BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
				String s = null;

				while ((s = in.readLine()) != null) {

					data = data + s;
				}

				in.close();

			} catch (IOException ex) {
				ex.printStackTrace();
			}
			return data;
		}

    
}

