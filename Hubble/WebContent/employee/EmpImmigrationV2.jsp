<%--*******************************************************************************
 *
 * Project : Mirage V2
 *
 * Package :
 *
 * Date    : October 10, 2010, 7:50 PM
 *
 * Author  : NagaLakshmi Telluri<ntelluri@miraclesoft.com>
 *
 * File    : ImmigrationAction.java
 *
 * Copyright 2007 Miracle Software Systems, Inc. All rights reserved.
 * MIRACLE SOFTWARE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * *****************************************************************************
*/--%>



<%@ page contentType="text/html; charset=UTF-8"
	errorPage="../exception/ErrorDisplay.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%--<%@ taglib prefix="sx" uri="/struts-dojo-tags" %> --%>
<%@ page import="com.freeware.gridtag.*"%>
<%@ page import="java.sql.Connection"%>
<%@ page import="java.sql.DriverManager"%>
<%@ page import="java.sql.SQLException"%>
<%@ page import="com.mss.mirage.util.ConnectionProvider"%>
<%@ page import="com.mss.mirage.util.ApplicationConstants"%>
<%@ taglib uri="/WEB-INF/tlds/datagrid.tld" prefix="grd"%>

<html>
<head>
<title>Hubble Organization Portal :: Employee Immigration
	Details</title>
<sx:head cache="true" />
<link rel="stylesheet" type="text/css"
	href="<s:url value="/includes/css/mainStyle.css"/>">
<link rel="stylesheet" type="text/css"
	href="<s:url value="/includes/css/GridStyle.css"/>">
<link rel="stylesheet" type="text/css"
	href="<s:url value="/includes/css/leftMenu.css"/>">
<link rel="stylesheet" type="text/css"
	href="<s:url value="/includes/css/tabedPanel.css"/>">
<script type="text/JavaScript"
	src="<s:url value="/includes/javascripts/CalendarTime.js"/>"></script>
<script type="text/JavaScript"
	src="<s:url value="/includes/javascripts/AppConstants.js"/>"></script>
<script type="text/JavaScript"
	src="<s:url value="/includes/javascripts/EmpStandardClientValidations.js?ver=1.6"/>"></script>
<script type="text/JavaScript"
	src="<s:url value="/includes/javascripts/tabedPanel.js"/>"></script>
<script type="text/JavaScript"
	src="<s:url value="/includes/javascripts/ImmigrationAjax.js?ver=1.8"/>"></script>

<script type="text/JavaScript">
	function validateFields() {

		var curImgStatus = document.getElementById('curImgStatus').value;
		var startDate = document.getElementById('startDate').value;
		var statusExpire = document.getElementById('statusExpire').value;
		var sevisNumber = document.getElementById('sevisNumber').value;

		var eadNumber = document.getElementById('eadNumber').value;
		var receiptNumber = document.getElementById('receiptNumber').value;

		var ptstartDate = document.getElementById('ptstartDate').value;
		var pstatusExpire = document.getElementById('pstatusExpire').value;

		var passportNumber = document.getElementById('passportNumber').value;
		var i94Number = document.getElementById('i94Number').value;

		var uniName = document.getElementById('uniName').value;

		

		var phnoNumberImg = document.getElementById('phnoNumberImg').value;

		var emailImg = document.getElementById('emailImg').value;

		var dateNotified = document.getElementById('dateNotified').value;
		
		var sDateimg = document.getElementById('sDateimg').value;

		var terminationDateimg = document.getElementById('terminationDateimg').value;
		
		

		if (curImgStatus == '-1') {
			alert("Please select immigration status");
			return false;
		}

		if (curImgStatus == 'F1-OPT' || curImgStatus == 'F1-CPT')

		{

			if (startDate == '' || statusExpire == '' || sevisNumber == ''
					|| eadNumber == '' || ptstartDate == ''
					|| pstatusExpire == '' || passportNumber == ''
					|| i94Number == '') {
				alert('Please enter mandataory fileds');
				return false;
			}

		}

		else if (curImgStatus == 'H1B' || curImgStatus == 'EAD'
				|| curImgStatus == 'TN-VISA' || curImgStatus == 'E3') {
			if (startDate == '' || statusExpire == '' || receiptNumber == ''
					|| ptstartDate == '' || pstatusExpire == ''
					|| passportNumber == '' || i94Number == '') {
				alert('Please enter mandataory fileds');
				return false;
			}
		} else if (curImgStatus == 'GreenCard') {
			if (startDate == '' || statusExpire == '' || receiptNumber == ''
					|| ptstartDate == '' || pstatusExpire == ''
					|| passportNumber == '') {
				alert('Please enter mandataory fileds');
				return false;
			}
		}

		else if (curImgStatus == 'USCitizen') {
			if (ptstartDate == '' || pstatusExpire == ''
					|| passportNumber == '') {
				alert('Please enter mandataory fileds');
				return false;
			}
		}

		else if (uniName == '' || sDateimg == '' || phnoNumberImg == ''
				|| emailImg == '' || dateNotified == ''
				|| terminationDateimg == '') {

			alert('Please enter mandataory fileds');
			return false;

		}

		else {
			return true;
		}

	}
</script>

<style>
.odd {
	background-color: Red;
}

.even {
	background-color: gray;
}
</style>
</head>
<body class="bodyGeneral" oncontextmenu="return false;">

	<%--  <%!
        /* Declarations */
        Connection connection;
        String queryString;
        String currentEmployeeId;
        String strTmp;
        String strSortCol;
        String strSortOrd;
        int intSortOrd = 0;
        int intCurr;
        boolean blnSortAsc = true;
        %> --%>

	<!--//START MAIN TABLE : Table for template Structure-->
	<table class="templateTable1000x580" align="center" cellpadding="0"
		cellspacing="0">

		<!--//START HEADER : Record for Header Background and Mirage Logo-->
		<tr class="headerBg">
			<td valign="top"><s:include
					value="/includes/template/Header.jsp" /></td>
		</tr>
		<!--//END HEADER : Record for Header Background and Mirage Logo-->

		<!--//START DATA RECORD : Record for LeftMenu and Screen Content-->
		<tr>
			<td>
				<table class="innerTable1000x515" cellpadding="0" cellspacing="0">
					<tr>

						<!--//START DATA COLUMN : Coloumn for LeftMenu-->
						<td width="150px;" class="leftMenuBgColor" valign="top"><s:include
								value="/includes/template/LeftMenu.jsp" /></td>
						<!--//START DATA COLUMN : Coloumn for LeftMenu-->

						<!--//START DATA COLUMN : Coloumn for Screen Content-->
						<td width="850px" class="cellBorder" valign="top">
							<table cellpadding="0" cellspacing="0" width="100%" border="0">
								<tr>
									<td><span class="fieldLabel">Employee Name :</span>&nbsp;
										<s:if test="currentImmigration == null ">


											<a class="navigationText"
												href="<s:url action="getEmployee"><s:param name="empId" value="%{empId}"/></s:url>"><s:property
													value="%{employeeName}" /></a>
										</s:if> <s:else>


											<a class="navigationText"
												href="<s:url action="getEmployee"><s:param name="empId" value="%{currentImmigration.empId}"/></s:url>"><s:property
													value="%{employeeName}" /></a>
										</s:else></td>
								</tr>
								<tr>
									<td valign="top" style="padding: 5x 5px 5px 5px;">
										<!--//START TABBED PANNEL : -->
										<ul id="accountTabs" class="shadetabs">
											<li><a href="#" class="selected"
												rel="personalDetailsTab"> Employee Immigration Details </a></li>
										</ul>
										<div
											style="border: 1px solid gray; width: 850px; height: 550px; overflow: auto; margin-bottom: 1em;">
											<%--  <sx:tabbedpanel id="activityPannel" cssStyle="width:850px; height:550px;padding: 5x 5px 5px 5px;" doLayout="true"> --%>

											<!--//START TAB : -->
											<%-- <sx:div id="personalDetailsTab" label="Employee Ancillary Details" cssStyle="overflow:auto" > --%>
											<div id="personalDetailsTab" class="tabcontent">


												<s:form name="immigrationForm" action="%{actionType}"
													theme="simple" onsubmit="return validateFields();">
													<table cellpadding="1" cellspacing="1" border="0"
														width="100%">


														<tr>
															<%-- START OFFSHORE ADDRESS TABLE --%>
															<td>
																<table width="100%" border="0" cellpadding="1"
																	cellspacing="1">
																	<tr class="headerText">

																		<td colspan="2" align="left">Immigration Details:</td>
																		<td colspan="5" align="right"><s:property
																				value="#request.resultMessage" /> <s:if
																				test="currentImmigration == null ">
																				<s:hidden name="empId" id="empId" value="%{empId}" />
																			</s:if> <s:else>
																				<s:hidden name="empId" id="empId"
																					value="%{currentImmigration.empId}" />
																			</s:else> <s:submit value="save" cssClass="buttonBg" /></td>
																	</tr>

																	<tr>
																		<td class="fieldLabel" width="200px" align="right">Cur
																			Immig Status:<FONT color="red" SIZE="0.5"><em>*</em></FONT>
																		</td>

																		<s:if test="%{actionType=='addImmigrationNew'}">

																			<td><s:select name="curImgStatus"
																					id="curImgStatus"
																					list="{'F1-OPT','F1-CPT','H1B','EAD','TN-VISA','E3','GreenCard','USCitizen'}"
																					cssClass="inputSelect" headerKey="-1"
																					headerValue="--Select Status--"
																					value="%{curImgStatus}"
																					onchange="selectedStatus();" /></td>

																		</s:if>

																		<s:else>


																			<td><s:select name="curImgStatus"
																					id="curImgStatus"
																					list="{'F1-OPT','F1-CPT','H1B','EAD','TN-VISA','E3','GreenCard','USCitizen'}"
																					cssClass="inputSelect" headerKey="-1"
																					headerValue="--Select Status--"
																					value="%{currentImmigration.curImgStatus}"
																					onchange="selectedStatus();" /></td>

																		</s:else>

																	</tr>
																	<tr id="datesTr">

																		<td class="fieldLabel" width="200px" align="right">Start
																			Date:<FONT color="red" SIZE="0.5"><em>*</em></FONT>
																		</td>
																		<td><s:textfield name="startDate" id="startDate"
																				cssClass="inputTextBlueMedium"
																				value="%{currentImmigration.startDate}" /><a
																			href="javascript:cal1.popup();"> <img
																				src="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/showCalendar.gif"
																				width="20" height="20" border="0"></td>

																		<td class="fieldLabel" width="200px" align="right">Date&nbsp;Status&nbsp;Expire:<FONT
																			color="red" SIZE="0.5"><em>*</em></FONT></td>
																		<td><s:textfield name="statusExpire"
																				id="statusExpire" cssClass="inputTextBlueMedium"
																				value="%{currentImmigration.statusExpire}" /><a
																			href="javascript:cal2.popup();"> <img
																				src="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/showCalendar.gif"
																				width="20" height="20" border="0"></td>
																	</tr>

																	<tr id="ImageStatus">
																		<td class="fieldLabel" width="200px" align="right">Sevis
																			Number:<FONT color="red" SIZE="0.5"><em>*</em></FONT>
																		</td>
																		<td><s:textfield name="sevisNumber"
																				id="sevisNumber" cssClass="inputTextBlue"
																				value="%{currentImmigration.sevisNumber}"
																				onchange="fieldLengthValidator(this);" size="30" /></td>

																		<td class="fieldLabel" width="200px" align="right">EAD
																			Number:<FONT color="red" SIZE="0.5"><em>*</em></FONT>
																		</td>

																		<td><s:textfield name="eadNumber" id="eadNumber"
																				cssClass="inputTextBlue"
																				value="%{currentImmigration.eadNumber}"
																				onchange="fieldLengthValidator(this);" size="30" /></td>
																	</tr>

																	<tr id="newImageStatus">
																		<td class="fieldLabel" width="200px" align="right">Application/Petition&nbsp;ReceiptNumber:<FONT
																			color="red" SIZE="0.5"><em>*</em></FONT></td>
																		<td><s:textfield name="receiptNumber"
																				id="receiptNumber" cssClass="inputTextBlue"
																				value="%{currentImmigration.receiptNumber}"
																				onchange="fieldLengthValidator(this);" size="30" /></td>
																	</tr>

																</table>

															</td>

														</tr>


														<tr>
															<%-- START OFFSHORE ADDRESS TABLE --%>
															<td>
																<table width="100%" border="0" cellpadding="1"
																	cellspacing="1">

																	<tr>

																		<!-- <td colspan="6" class="headerText" align="left">Post Graduation Details: </td> -->
																		<td colspan="6" class="headerText">Passport
																			Details:(<span id="inspanId"></span>)
																		</td>
																	</tr>

																	<tr>

																		<td class="fieldLabel" width="200px" align="right">Passport&nbsp;Validity&nbsp;Start
																			Date:<FONT color="red" SIZE="0.5"><em>*</em></FONT>
																		<td><s:textfield name="ptstartDate"
																				id="ptstartDate" cssClass="inputTextBlueMedium"
																				value="%{currentImmigration.ptstartDate}" /><a
																			href="javascript:cal3.popup();"> <img
																				src="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/showCalendar.gif"
																				width="20" height="20" border="0"></td>



																		<td class="fieldLabel" width="200px" align="right">Date&nbsp;Status&nbsp;Expire:<FONT
																			color="red" SIZE="0.5"><em>*</em></FONT></td>
																		<td><s:textfield name="pstatusExpire"
																				id="pstatusExpire" cssClass="inputTextBlueMedium"
																				value="%{currentImmigration.pstatusExpire}" /><a
																			href="javascript:cal4.popup();"> <img
																				src="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/showCalendar.gif"
																				width="20" height="20" border="0"></td>
																	</tr>

																	<tr>
																		<td class="fieldLabel" width="200px" align="right">Passport&nbsp;Number:<FONT
																			color="red" SIZE="0.5"><em>*</em></FONT></td>
																		<td><s:textfield name="passportNumber"
																				id="passportNumber" cssClass="inputTextBlue"
																				value="%{currentImmigration.passportNumber}"
																				onchange="fieldLengthValidator(this);" size="30" /></td>

																		<td id="I94td" class="fieldLabel" width="200px"
																			align="right" style="visibility: visible;">I94
																			Number&nbsp;:<FONT color="red" SIZE="0.5"><em>*</em></FONT>
																		</td>

																		<td id="I94td1" style="visibility: visible;"><s:textfield
																				name="i94Number" id="i94Number"
																				cssClass="inputTextBlue"
																				value="%{currentImmigration.i94Number}"
																				onchange="fieldLengthValidator(this);" size="30" /></td>
																	</tr>

																</table>
															</td>
														</tr>


														<tr id="universityTr">
															<%-- START OFFSHORE ADDRESS TABLE --%>
															<td>
																<table width="100%" border="0" cellpadding="1"
																	cellspacing="1">

																	<tr>
																		<td colspan="6" class="headerText" align="left">University
																			Details:</td>
																	</tr>

																	<tr>
																		<td class="fieldLabel" width="200px" align="right">University&nbsp;Name
																			:<FONT color="red" SIZE="0.5"><em>*</em></FONT>
																		</td>
																		<td colspan="5"><s:textfield name="uniName"
																				cssClass="inputTextBlueComment1"
																				value="%{currentImmigration.uniName}" id="uniName"
																				onchange="fieldLengthValidator(this);" size="30" /></td>

																	</tr>


																<%-- 	<tr>
																		<td class="fieldLabel" width="200px" align="right">University&nbsp;Contact&nbsp;Name
																			:<FONT color="red" SIZE="0.5"><em>*</em></FONT>
																		</td>
																		<td colspan="5"><s:textfield
																				name="uniContactName"
																				cssClass="inputTextBlueComment1"
																				value="%{currentImmigration.uniContactName}"
																				id="uniContactName"
																				onchange="fieldLengthValidator(this);" size="30" /></td>

																	</tr>
 --%>

																	<tr>

																		<td class="fieldLabel" width="200px" align="right">
																			Phone&nbsp;Number:<FONT color="red" SIZE="0.5"><em>*</em></FONT>
																		</td>

																		<td><s:textfield name="phnoNumberImg"
																				cssClass="inputTextBlue"
																				value="%{currentImmigration.phnoNumberImg}"
																				onchange="formatPhone(this);" id="phnoNumberImg"
																				size="30" /></td>

																		<td class="fieldLabel" width="200px" align="right">
																			Email:<FONT color="red" SIZE="0.5"><em>*</em></FONT>
																		</td>

																		<td><s:textfield name="emailImg"
																				cssClass="inputTextBlue"
																				value="%{currentImmigration.emailImg}"
																				onchange="return checkEmail(this);" id="emailImg"
																				size="30" /></td>

																	</tr>


																	<tr>

																		<td class="fieldLabel" width="200px" align="right">Date&nbsp;Notified&nbsp;University
																			:<FONT color="red" SIZE="0.5"><em>*</em></FONT>
																		<td><s:textfield name="dateNotified"
																				id="dateNotified" cssClass="inputTextBlueMedium"
																				value="%{currentImmigration.dateNotified}" /><a
																			href="javascript:cal5.popup();"> <img
																				src="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/showCalendar.gif"
																				width="20" height="20" border="0"></td>



																		<td class="fieldLabel" width="200px" align="right">Start&nbsp;Date&nbsp;Of Employee:<FONT
																			color="red" SIZE="0.5"><em>*</em></FONT></td>
																		<td><s:textfield name="sDateimg"
																				id="sDateimg"
																				cssClass="inputTextBlueMedium"
																				value="%{currentImmigration.sDateimg}" /><a
																			href="javascript:cal7.popup();"> <img
																				src="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/showCalendar.gif"
																				width="20" height="20" border="0"></td>
																	</tr>
																	
																	<tr>
																	
																	
																		<td class="fieldLabel" width="200px" align="right">Term&nbsp;Date&nbsp;Of Employee:<FONT
																			color="red" SIZE="0.5"><em>*</em></FONT></td>
																		<td><s:textfield name="terminationDateimg"
																				id="terminationDateimg"
																				cssClass="inputTextBlueMedium"
																				value="%{currentImmigration.terminationDateimg}" /><a
																			href="javascript:cal6.popup();"> <img
																				src="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/showCalendar.gif"
																				width="20" height="20" border="0"></td>
																	
																	</tr>

																</table>

															</td>

														</tr>




													</table>
												</s:form>
												<!--//END TABBED PANNEL : -->

												<!-- this script for After loading Form it will instantiate Calender Objects as you require -->
												<script type="text/JavaScript">
													var cal1 = new CalendarTime(
															document.forms['immigrationForm'].elements['startDate']);
													cal1.year_scroll = true;
													cal1.time_comp = false;

													var cal2 = new CalendarTime(
															document.forms['immigrationForm'].elements['statusExpire']);
													cal2.year_scroll = true;
													cal2.time_comp = false;

													var cal3 = new CalendarTime(
															document.forms['immigrationForm'].elements['ptstartDate']);
													cal3.year_scroll = true;
													cal3.time_comp = false;

													var cal4 = new CalendarTime(
															document.forms['immigrationForm'].elements['pstatusExpire']);
													cal4.year_scroll = true;
													cal4.time_comp = false;

													var cal5 = new CalendarTime(
															document.forms['immigrationForm'].elements['dateNotified']);
													cal5.year_scroll = true;
													cal5.time_comp = false;

													var cal6 = new CalendarTime(
															document.forms['immigrationForm'].elements['terminationDateimg']);
													cal6.year_scroll = true;
													cal6.time_comp = false;
													
													var cal7 = new CalendarTime(
															document.forms['immigrationForm'].elements['sDateimg']);
													cal7.year_scroll = true;
													cal7.time_comp = false;
													
													
												</script>
												<%-- </sx:div > --%>
											</div>
											<!--//END TAB : -->

											<!--//START TAB : -->
											<%--    <s:div id="Ancillary Tab" label="Employee Ancillary List" >
                                                    <%
                                                    /* String Variable for storing current position of records in dbgrid*/
                                                    strTmp = request.getParameter("txtCurr");
                                                    
                                                    intCurr = 1;
                                                    
                                                    if (strTmp != null)
                                                        intCurr = Integer.parseInt(strTmp);
                                                    
                                                    /* Specifing Shorting Column */
                                                    strSortCol = request.getParameter("Colname");
                                                    
                                                    if (strSortCol == null) strSortCol = "AccountName";
                                                    
                                                    strSortOrd = request.getParameter("txtSortAsc");
                                                    if (strSortOrd == null) strSortOrd = "ASC";
                                                    
                                                    try{
                                                        
                                                        /* Getting DataSource using Service Locator */
                                                        
                                                        connection = ConnectionProvider.getInstance().getConnection();
                                                        
                                                        /* Sql query for retrieving resultset from DataBase */
                                                        queryString = "Select EmpId,FatherName,ReferalName,PrevJobTitle from tblEmpAncillary";
                                                        //queryString = queryString+" FROM tblEmpAncillary";
                                                    %>
                                                    
                                                    <s:form action="" name="frmDBGrid" theme="simple">
                                                        
                                                        
                                                        
                                                        <table cellpadding="0" cellspacing="0" width="100%">
                                                            <tr align="right">
                                                                <td class="headerText">
                                                                    <img alt="Home" 
                                                                         src="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/spacer.gif" 
                                                                         width="100%" 
                                                                         height="13px" 
                                                                         border="0">
                                                                </td>
                                                            </tr>    
                                                            <!---BEGIN:: DBGrid Specific ---->  
                                                            <tr>
                                                                <td class="subHeader" width="700"></td>
                                                            </tr>
                                                            <tr>
                                                                <td width="100%">
                                                                    
                                                                    <grd:dbgrid id="tblAccountList" name="tblAccountList" width="100" pageSize="7" 
                                                                                currentPage="<%=intCurr%>" border="0" cellSpacing="1" cellPadding="2" 
                                                                                dataMember="<%=queryString%>" dataSource="<%=connection%>" cssClass="gridTable">
                                                                        
                                                                        <grd:gridpager imgFirst="../includes/images/DBGrid/First.gif" imgPrevious="../includes/images/DBGrid/Previous.gif" 
                                                                                       imgNext="../includes/images/DBGrid/Next.gif" imgLast="../includes/images/DBGrid/Last.gif"/>
                                                                        
                                                                        <grd:gridsorter sortColumn="<%=strSortCol%>" sortAscending="<%=blnSortAsc%>" 
                                                                                        imageAscending="../includes/images/DBGrid/ImgAsc.gif" 
                                                                                        imageDescending="../includes/images/DBGrid/ImgDesc.gif"/>    
                                                                        
                                                                        <grd:imagecolumn headerText="Edit" width="4" HAlign="center" imageSrc="../includes/images/DBGrid/Edit.gif"
                                                                                         linkUrl="getAncillary.action?id={EmpId}" imageBorder="0"
                                                                                         imageWidth="16" imageHeight="16" alterText="Click to edit"></grd:imagecolumn>
                                                                        
                                                                        <grd:textcolumn dataField="FatherName"         headerText="Father Name" width="25" sortable="true"/>
                                                                        <grd:textcolumn dataField="ReferalName"          headerText="Referal Name" width="10" sortable="true"/>
                                                                        <grd:textcolumn dataField="PrevJobTitle"	headerText="Previous JobTitle" width="15" sortable="true"/>
                                                                        
                                                                    </grd:dbgrid>
                                                                    
                                                                    <input TYPE="hidden" NAME="txtCurr" VALUE="<%=intCurr%>">
                                                                    <input TYPE="hidden" NAME="txtSortCol" VALUE="<%=strSortCol%>">
                                                                    <input TYPE="hidden" NAME="txtSortAsc" VALUE="<%=strSortOrd%>">
                                                                    
                                                                </td>
                                                            </tr>
                                                        </table>                   
                                                        
                                                        
                                                    </s:form>
                                                    <%
                                                    }catch(Exception ex){
                                                        out.println(ex.toString());
                                                    }finally{
                                                        if(connection!= null){
                                                            connection.close();
                                                        }
                                                    }
                                                    %>
                                                </s:div >--%>
											<!--//END TAB : -->

											<%--  </sx:tabbedpanel> --%>
										</div> <!--//END TABBED PANNEL : --> <script type="text/javascript">
											var countries = new ddtabcontent(
													"accountTabs")
											countries.setpersist(false)
											countries
													.setselectedClassTarget("link") //"link" or "linkparent"
											countries.init()
										</script>
									</td>
								</tr>
							</table>

						</td>
						<!--//END DATA COLUMN : Coloumn for Screen Content-->
					</tr>
				</table>
			</td>
		</tr>
		<!--//END DATA RECORD : Record for LeftMenu and Body Content-->

		<!--//START FOOTER : Record for Footer Background and Content-->
		<tr class="footerBg">
			<td align="center"><s:include
					value="/includes/template/Footer.jsp" /></td>
		</tr>
		<!--//END FOOTER : Record for Footer Background and Content-->

	</table>
	<!--//END MAIN TABLE : Table for template Structure-->

	<script type="text/javascript">
		$(window).load(function() {
			selectedStatus();
		});
	</script>
</body>
</html>
