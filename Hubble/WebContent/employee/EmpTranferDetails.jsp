
<%@ page contentType="text/html; charset=UTF-8"
	errorPage="../exception/ErrorDisplay.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<%@ page import="com.mss.mirage.util.ApplicationConstants"%>
<html>
<head>
<title>Hubble Organization Portal :: ${title} ListAll</title>
<%--<sx:head cache="true"/> --%>
<link rel="stylesheet" type="text/css"
	href="<s:url value="/includes/css/mainStyle.css"/>">
<link rel="stylesheet" type="text/css"
	href="<s:url value="/includes/css/GridStyle.css"/>">
<link rel="stylesheet" type="text/css"
	href="<s:url value="/includes/css/leftMenu.css"/>">
<link rel="stylesheet" type="text/css"
	href="<s:url value="/includes/css/tabedPanel.css"/>">
<script type="text/JavaScript"
	src="<s:url value="/includes/javascripts/CalendarTime.js"/>"></script>
<script type="text/JavaScript"
	src="<s:url value="/includes/javascripts/employee/DateValidator.js"/>"></script>
<script type="text/JavaScript"
	src="<s:url value="/includes/javascripts/EmpStandardClientValidations.js?ver=1.3"/>"></script>

<script type="text/JavaScript"
	src="<s:url value="/includes/javascripts/AppConstants.js"/>"></script>
<script type="text/JavaScript"
	src="<s:url value="/includes/javascripts/tabedPanel.js"/>"></script>
<script type="text/JavaScript"
	src="<s:url value="/includes/javascripts/employee/empNameSuggestionList.js?ver=3.6"/>"></script>
<script type="text/JavaScript"
	src="<s:url value="/includes/javascripts/EmployeeAjax.js?ver=5.0"/>"></script>
<link rel="stylesheet" type="text/css"
	href="<s:url value="/includes/css/new/x0popup.min.css?version=1.0"/>">
<script type="text/JavaScript"
	src="<s:url value="/includes/javascripts/payroll/x0popup.min.js"/>"></script>
<s:include value="/includes/template/headerScript.html" />

</head>
<body class="bodyGeneral" oncontextmenu="return false;">



	<!--//START MAIN TABLE : Table for template Structure-->
	<table class="templateTable1000x580" align="center" cellpadding="0"
		cellspacing="0">

		<!--//START HEADER : Record for Header Background and Mirage Logo-->
		<tr class="headerBg">
			<td valign="top"><s:include
					value="/includes/template/Header.jsp" /></td>
		</tr>
		<!--//END HEADER : Record for Header Background and Mirage Logo-->


		<!--//START DATA RECORD : Record for LeftMenu and Screen Content-->
		<tr>
			<td>
				<table class="innerTable1000x515" cellpadding="0" cellspacing="0">
					<tr>

						<!--//START DATA COLUMN : Coloumn for LeftMenu-->
						<td width="150px;" class="leftMenuBgColor" valign="top"><s:include
								value="/includes/template/LeftMenu.jsp" /></td>
						<!--//END DATA COLUMN : Coloumn for LeftMenu-->

						<!--//START DATA COLUMN : Coloumn for Screen Content-->
						<td width="850px" class="cellBorder" valign="top"
							style="padding: 10px">
							<!--//START TABBED PANNEL : --> <%--   <sx:tabbedpanel id="accountListPannel" cssStyle="width: 840px; height: 500px;padding-left:10px;padding-top:5px;" doLayout="true"> --%>
							<ul id="accountTabs" class="shadetabs">

								<li><a href="#" rel="accountsSearchTab">Transfer
										Location Details</a></li>

							</ul>
							<div
								style="border: 1px solid gray; width: 840px; height: 650px; overflow: auto; margin-bottom: 1em;">


								<div id="accountsSearchTab" class="tabcontent">

									<s:form name="employeeForm" action="%{currentAction}"
										theme="simple" onsubmit="return validField();">
										<%
											if (request.getAttribute(ApplicationConstants.RESULT_MSG) != null) {
													out.println(request.getAttribute(ApplicationConstants.RESULT_MSG));
												}
										%>
										<table cellpadding="1" cellspacing="1" border="0" width="86%"
											align="center">

											<div id="resultMessage"></div>
											<s:hidden id="currentAction" value="%{currentAction}" />
											<s:hidden id="myDeptId" value="%{#session.myDeptId}" />

											<s:hidden id="sessionEmpPractice"
												value="%{#session.sessionEmpPractice}" />


											<s:hidden id="isNextLocationHR" name="isNextLocationHR"
												value="%{isNextLocationHR}" />
											<s:hidden id="id" name="id" value="%{id}" />
											<s:hidden id="createdBy" name="createdBy"
												value="%{createdBy}" />
											<s:hidden id="isUserManager"
												value="%{#session.isUserManager}" />

											<s:if
												test="%{#session.myDeptId == 'Operations' && #session.sessionEmpPractice == 'Systems'}">
												<s:hidden name="status" value="%{status}" />
												<s:hidden name="fromLocation" value="%{fromLocation}" />
												<s:hidden name="toLocation" value="%{toLocation}" />
												<s:hidden name="itTeamFlag" id="itTeamFlag"
													value="%{itTeamFlag}" />
											</s:if>
											<s:elseif
												test="%{#session.isAdminAccess != 1 && #session.isManager != 1 }">
												<s:hidden name="itTeamStatus" value="%{itTeamStatus}" />


											</s:elseif>

											<s:if test="#session.roleName=='Operations'">
												<tr align="right">
													<td class="" colspan="6"><a
														href="<s:url action="../employee/getEmpTransfers.action"></s:url>"
														class="navigationText"> <img alt="Back to List"
															src="<s:url value="/includes/images/backToList_66x19.gif"/>"
															width="66px" height="19px" border="0" align="bottom"></a>
													</td>
												</tr>
											</s:if>
											<s:elseif test="#session.roleName=='Employee'">
												<tr align="right">
													<td class="" colspan="6"><a
														href="<s:url action="../employee/getEmpTransfers.action"></s:url>"
														class="navigationText"> <img alt="Back to List"
															src="<s:url value="/includes/images/backToList_66x19.gif"/>"
															width="66pxF" height="19px" border="0" align="bottom"></a>
													</td>
												</tr>
											</s:elseif>


											<tr>
												<td>&nbsp;&nbsp;</td>
												<td>&nbsp;&nbsp;</td>
											</tr>

											<td class="fieldLabel">Transfer&nbsp;Type<font
												color="red">*</font>:
											</td>
											<td><s:select
													list="#@java.util.LinkedHashMap@{'OffshoreTransfer':'OffshoreTransfer','CountryTransfer':'CountryTransfer'}"
													name="transferType" id="transferType"
													cssClass="inputTextBlue"
													onchange="return transferTypeDetails(this.value);"
													headerValue="-PleaseSelect" headerKey="" /></td>

											<tr>
												<s:hidden id="flag" value="%{#session.roleName}" />


												<td class="fieldLabel">EmpName <font color="red">*</font>:
												</td>
												<td><s:textfield name="empName" id="empName"
														value="%{empName}" autocomplete="off"
														cssClass="inputTextBlue" onkeyup="getEmployeeNames();" />
													<span id="empNoSpan"></span><label id="rejectedId"></label>
													<div style="margin: 0px 25px 0px -20px;" class="fieldLabel"
														id="validationMessage"></div>

													<div
														style="display: none; position: absolute; overflow: auto; z-index: 500;"
														id="menu-popup">
														<table id="completeTable" border="1" bordercolor="#e5e4f2"
															style="border: 1px dashed gray;" cellpadding="0"
															class="cellBorder" cellspacing="0"></table>
													</div> <s:hidden id="userId" name="userId" /> <s:hidden
														id="empId" name="empId" /> <s:hidden id="empNo"
														name="empno" /></td>


												<s:hidden name="departmentId" id="departmentId"
													cssClass="inputTextBlue" value="%{departmentId}" />
												<s:hidden name="titleId" id="titleId"
													cssClass="inputTextBlue" value="%{titleId}" />
												<s:hidden id="isAdminAccess"
													value="%{#session.isAdminAccess}" />
												<td></td>
											</tr>
											<%-- <tr>
                                                    <td class="fieldLabel">Department</td>
                                                      <td><s:select list="departmentIdList" name="departmentId" id="departmentId" headerKey="" headerValue="--Select--" cssClass="inputSelect" value="%{departmentId}" onchange="return getEmpTitleDataV1();" /></td>
                                                      
                                                         <s:hidden name="departmentId" id="departmentId" cssClass="inputTextBlue" value="%{departmentId}"/>
                                                         <s:hidden name="titleId" id="titleId" cssClass="inputTextBlue" value="%{titleId}"/>
                                                         
                                                    <td class="fieldLabel">Title</td>
                                                   <td><s:select list="titleIdList" name="titleId" id="titleId" headerKey="" headerValue="--Select--" cssClass="inputSelect" value="%{titleId}" /></td>
                                                       
                                                        
                                                   
                                                    </tr>--%>

											<tr>
												<td class="fieldLabel">Status<font color="red">*</font>:
												</td>
												<td><s:select
														list="#@java.util.LinkedHashMap@{'Initiated':'Initiated','InProgress':'In Progress','OnHold':'On Hold','Completed':'Completed'}"
														name="status" id="status" headerKey=""
														headerValue="--Select--" cssClass="inputSelect"
														value="%{status}" /></td>

												<td class="fieldLabel">Initiated&nbsp;On&nbsp;<font
													color="red">*</font>:
												</td>

												<td><s:textfield name="initiatedOn" id="initiatedOn"
														cssClass="inputTextBlueSmall" value="%{initiatedOn}"
														onKeyPress="return selectFromCal(this);" /> <a
													class="underscore" href="javascript:cal3.popup();"> <img
														src="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/showCalendar.gif"
														id="calenderImg" width="20" height="20" border="0"></a>

												</td>

											</tr>
											<tr id="oldTrans">
												<td class="fieldLabel">From&nbsp;Location&nbsp;<font
													color="red">*</font>:
												</td>
												<td><s:select list="empLocationMap" name="fromLocation"
														id="fromLocation" headerKey="" headerValue="--Select--"
														cssClass="inputSelect" value="%{fromLocation}"
														onchange="return getDues(this.value);loadExpenses(this.value);"
														onblur="return getLocations();" /></td>
												<td class="fieldLabel">To&nbsp;Location&nbsp;<font
													color="red">*</font>:
												</td>
												<td><s:select list="empLocationMap" name="toLocation"
														id="toLocation" headerKey="" headerValue="--Select--"
														cssClass="inputSelect" value="%{toLocation}"
														onblur="return getLocations();"
														onchange="return getNeedTransport(this.value);" /></td>

											</tr>

											<tr id="newTrans">
												<td class="fieldLabel">From&nbsp;Location&nbsp;<font
													color="red">*</font>:
												</td>
												<td><s:select
														list="#@java.util.LinkedHashMap@{'India':'India','USA':'USA'}"
														name="fromLocationnew" id="fromLocationnew"
														headerValue="select" headerKey="" cssClass="inputSelect"
														value="%{fromLocationnew}"
														onblur="return getNewLocations();" /></td>
												<td class="fieldLabel">To&nbsp;Location&nbsp;<font
													color="red">*</font>:
												</td>
												<td><s:select
														list="#@java.util.LinkedHashMap@{'India':'India','USA':'USA'}"
														name="toLocationnew" id="toLocationnew"
														headerValue="select" headerKey="" cssClass="inputSelect"
														value="%{toLocationnew}"
														onchange="return getNewReportedTo(this.value);"
														onblur="return getNewLocations();" /></td>

											</tr>

											<tr>
												<td class="fieldLabel">Reason&nbsp;For&nbsp;Transfer&nbsp;:</td>
												<td colspan="4"><s:textarea name="reasonsForTransfer"
														id="reasonsForTransfer" cols="100" rows="2"
														value="%{reasonsForTransfer}" cssClass="inputTextarea"
														onchange="fieldLengthValidator(this);" /></td>

											</tr>

											<s:if test="%{#session.sessionEmpPractice != 'Systems'}">
												<tr id="mcityexp">
													<td class="fieldLabel">Accommodation:</td>
													<%--  <td><s:select list="accommodationList" id="homeState" name="accommodation" value="%{currentExpenses.accommodation}"
                                                                                  headerKey="" headerValue="--Please Select--" cssClass="inputSelect" onchange="selectedAccomdation()"/></td> --%>
													<td><s:select list="accommodationList" id="homeState"
															name="accommodation" value="%{accommodation}"
															headerKey="" headerValue="--Please Select--"
															cssClass="inputSelect" onchange="selectedAccomdation()" /></td>


													<td class="fieldLabel">Date&nbsp;of&nbsp;Occupancy&nbsp;:</td>
													<td><s:textfield name="dateOfOccupancy"
															value="%{dateOfOccupancy}" cssClass="inputTextBlue"
															id="dateOfOccupancy" size="50"
															onKeyPress="return selectFromCal(this);" /> <a
														href="javascript:cal4.popup();"> <img
															id="calenderImg2"
															src="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/showCalendar.gif"
															width="20" height="20" border="0"></td>
												</tr>
												<tr id="mcityexp1">
													<td class="fieldLabel">Occupancy&nbsp;Type&nbsp;:</td>
													<td><s:select name="occupancyType" id="occupancyType"
															cssClass="inputSelect" headerKey=""
															headerValue="--Please Select--"
															list="{'Single','Family'}" value="%{occupancyType}"></s:select>


													</td>

													<td class="fieldLabel">Room No:</td>
													<td><s:textfield name="roomNo" value="%{roomNo}"
															onchange="fieldLengthValidator(this);"
															cssClass="inputTextBlue" id="roomNo" size="50"
															onkeypress="return isNumber(event)" /></td>
												</tr>
												<tr id="mcityexp2">
													<td class="fieldLabel">Room Fee:</td>
													<td><s:textfield name="roomFee" value="%{roomFee}"
															onkeypress="return isNumber(event)"
															cssClass="inputTextBlue" id="roomFee" size="50"
															onchange="fieldLengthValidator(this);" /></td>

													<td class="fieldLabel">Electrical&nbsp;Charges&nbsp;:</td>
													<td><s:textfield name="electricalCharges"
															value="%{electricalCharges}" cssClass="inputTextBlue"
															id="electricalCharges" size="50"
															onkeypress="return isNumber(event)"
															onchange="fieldLengthValidator(this);" /></td>


												</tr>


												<tr id="mcityexp3">
													<td class="fieldLabel">Cafeteria:</td>

													<td><s:radio id="cafeteria" name="cafeteria"
															value="%{cafeteria}"
															list="#@java.util.LinkedHashMap@{'Yes':'Yes','No':'No'}" /></td>

													<td class="fieldLabel">Cafeteria Fee:</td>
													<td><s:textfield name="cafeteriaFee"
															value="%{cafeteriaFee}"
															onkeypress="return isNumber(event)"
															cssClass="inputTextBlue" id="cafeteriaFee" size="50"
															onchange="fieldLengthValidator(this);" /></td>


												</tr>

												<tr id="mirexp">
													<%-- <td class="fieldLabel">Transportation:</td>
                                                                        <td ><s:radio id="transportation" name="transportation" value="%{transportation}" list="#@java.util.LinkedHashMap@{'Yes':'Yes','No':'No'}" onchange="isTransportation();"/></td>  
                                                                       --%>
													<td class="fieldLabel">Transportation:</td>
													<s:if test="%{fromLocation=='Miracle City'}">

														<td><s:radio id="transportation"
																name="transportation" value="%{transportation}"
																list="#@java.util.LinkedHashMap@{'Yes':'Yes','No':'No'}" /></td>
													</s:if>
													<s:else>

														<td><s:radio id="transportation"
																name="transportation" value="%{transportation}"
																list="#@java.util.LinkedHashMap@{'Yes':'Yes','No':'No'}" /></td>

													</s:else>


													<td class="fieldLabel">Transport Location:</td>
													<td><s:select list="transportLocationList"
															id="transportLocation" name="transportLocation"
															value="%{transportLocation}" cssClass="inputSelect"
															headerKey="" headerValue="--Please Select--" /></td>
												</tr>
												<tr id="mirexp1">
													<td class="fieldLabel">Transportation Fee:</td>
													<td><s:textfield name="transportFee"
															value="%{transportFee}"
															onkeypress="return isNumber(event)"
															cssClass="inputTextBlue" id="transportFee" size="50"
															onchange="fieldLengthValidator(this);" /></td>
												</tr>


											</s:if>





											<tr id="mcityDues">
												<td class="fieldLabel">Hostel/Apartment&nbsp;Dues&nbsp;<font
													color="red">*</font>:
												</td>
												<td><s:select
														list="#@java.util.LinkedHashMap@{'SalaryDeduction':'SalaryDeduction','NotApplicable':'NotApplicable'}"
														name="hostelDues" id="hostelDues" headerKey=""
														headerValue="--Select--" cssClass="inputSelect"
														value="%{hostelDues}" /></td>

												<td class="fieldLabel">Is&nbsp;keys&nbsp;hand&nbsp;over&nbsp;<font
													color="red">*</font>?
												</td>
												<td><s:checkbox name="isKeysHandover"
														id="isKeysHandover" value="%{isKeysHandover}" /></td>

											</tr>

											<tr id="mcityDues1">
												<td class="fieldLabel">Cafeteria&nbsp;Dues&nbsp;<font
													color="red">*</font>:
												</td>
												<td><s:select
														list="#@java.util.LinkedHashMap@{'SalaryDeduction':'SalaryDeduction','NotApplicable':'NotApplicable'}"
														name="cafteriaDues" id="cafteriaDues" headerKey=""
														headerValue="--Select--" cssClass="inputSelect"
														value="%{cafteriaDues}" /></td>
												<td></td>
												<td></td>
											</tr>

											<tr id="otherDue">
												<td class="fieldLabel">Transportation&nbsp;Dues&nbsp;<font
													color="red">*</font>:
												</td>
												<td><s:select
														list="#@java.util.LinkedHashMap@{'SalaryDeduction':'SalaryDeduction','NotApplicable':'NotApplicable'}"
														name="transportationDues" id="transportationDues"
														headerKey="" headerValue="--Select--"
														cssClass="inputSelect" value="%{transportationDues}" /></td>
												<td></td>
												<td></td>
											</tr>


											<tr id="dueRemarks">
												<td class="fieldLabel">Remarks&nbsp;On&nbsp;Dues&nbsp;:</td>
												<td colspan="4"><s:textarea name="duesRemarks"
														id="duesRemarks" cols="100" rows="2"
														value="%{duesRemarks}" cssClass="inputTextarea"
														onchange="fieldLengthValidator(this);" /></td>
												<td></td>
												<td></td>
											</tr>

											<tr id="needTrans">
												<td class="fieldLabel">Is&nbsp;Tansport&nbsp;Facility&nbsp;Required&nbsp;<font
													color="red">*</font>:
												</td>
												<td><s:checkbox name="needTransport" id="needTransport"
														value="%{needTransport}" /></td>
												<td></td>
												<td></td>
											</tr>

											<tr id="itTeamTr">
												<td class="fieldLabel">Clearance&nbsp;From&nbsp;ITTeam&nbsp;<font
													color="red">*</font>:
												</td>
												<td><s:select
														list="#@java.util.LinkedHashMap@{'Approved':'Approved','Rejected':'Rejected','OnHold':'OnHold'}"
														name="itTeamStatus" id="itTeamStatus" headerKey=""
														headerValue="--Select--" cssClass="inputSelect"
														value="%{itTeamStatus}" /></td>
												<%--  <s:textfield name="itTeamStatus" value="%{itTeamStatus}" />  --%>
												<td></td>
												<td></td>
											</tr>
											<tr id="itTeamTr1">
												<td class="fieldLabel">IT&nbsp;Team&nbsp;Comments&nbsp;:</td>
												<td colspan="4"><s:textarea name="itTeamComments"
														id="itTeamComments" cols="100" rows="2"
														value="%{itTeamComments}" cssClass="inputTextarea"
														onchange="fieldLengthValidator(this);" /></td>

											</tr>


											<tr id="reportedTr">
												<td class="fieldLabel">Reported&nbsp;To&nbsp;<font
													color="red">*</font>
												<td><s:select list="opsContactIdMap" name="reportedTo"
														id="reportedTo" headerKey="" headerValue="--Select--"
														cssClass="inputSelect" value="%{reportedTo}" /></td>
												<td class="fieldLabel">Tentative&nbsp;Reported&nbsp;Date&nbsp;(mm/dd/yyyy)&nbsp;<font
													color="red">*</font>:
												</td>
												<td><s:textfield name="tentativeReportedDate"
														id="tentativeReportedDate" cssClass="inputTextBlueSmall"
														value="%{tentativeReportedDate}"
														onKeyPress="return selectFromCal(this);" /><a
													class="underscore" href="javascript:cal2.popup();"> <img
														id="calenderImg1"
														src="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/showCalendar.gif"
														width="20" height="20" border="0"></a></td>
											</tr>


											<tr id="NewreportedTr">
												<td class="fieldLabel">Reported&nbsp;To&nbsp;<font
													color="red">*</font>:
												</td>
												<td><s:select list="opsContactIdMapNew"
														name="reportedToNew" id="reportedToNew" headerKey=""
														headerValue="--Select--" cssClass="inputSelect"
														value="%{reportedToNew}" /></td>
												<td class="fieldLabel">Tentative&nbsp;Reported&nbsp;Date&nbsp;(mm/dd/yyyy)&nbsp;<font
													color="red">*</font>:
												</td>
												<td><s:textfield name="tentativeReportedDateNew"
														id="tentativeReportedDateNew"
														cssClass="inputTextBlueSmall"
														value="%{tentativeReportedDateNew}"
														onKeyPress="return selectFromCal(this);" /><a
													class="underscore" href="javascript:cal5.popup();"> <img
														src="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/showCalendar.gif"
														width="20" height="20" border="0"></a></td>
											</tr>

											<tr id="isReportedTr">
												<td class="fieldLabel">Is&nbsp;Reported&nbsp;</td>
												<td><s:checkbox name="isReported" id="isReported"
														value="%{isReported}"
														onchange="return getNextLocHRComments(this);" /></td>
												<td class="fieldLabel">Actual&nbsp;Reported&nbsp;Date&nbsp;(mm/dd/yyyy)&nbsp;<font
													color="red">*</font>:
												</td>
												<td><s:textfield name="actualReportedDate"
														id="actualReportedDate" cssClass="inputTextBlueSmall"
														value="%{actualReportedDate}"
														onKeyPress="return selectFromCal(this);" /><a
													class="underscore" href="javascript:cal1.popup();"> <img
														src="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/showCalendar.gif"
														width="20" height="20" border="0"></a></td>

											</tr>
											<tr id="nextLocationHRCommentsDiv">
												<td class="fieldLabel">Next&nbsp;Location&nbsp;HR&nbsp;Comments&nbsp;:</td>
												<td colspan="4"><s:textarea
														name="nextLocationHRComments" id="nextLocationHRComments"
														cols="100" rows="2" value="%{nextLocationHRComments}"
														cssClass="inputTextarea"
														onchange="fieldLengthValidator(this);" /></td>
											</tr>
											<tr>
												<td>&nbsp;&nbsp;</td>
												<td align="right" colspan="3"><s:if
														test="%{currentAction == 'addEmpTransfers' && #session.sessionEmpPractice != 'Systems'}">
														<s:if test="%{#session.roleName == 'Operations'}">
															<s:submit value="Add" cssClass="buttonBg" />
														</s:if>
													</s:if> <s:else>
														<s:if test="%{#session.roleName == 'Operations'}">
															<s:submit id="updateButton" value="Update"
																cssClass="buttonBg" />
														</s:if>
													</s:else></td>
											</tr>

										</table>
										<script type="text/JavaScript">
											var cal2 = new CalendarTime(
													document.forms['employeeForm'].elements['tentativeReportedDate']);
											cal2.year_scroll = true;
											cal2.time_comp = false;

											var cal5 = new CalendarTime(
													document.forms['employeeForm'].elements['tentativeReportedDateNew']);
											cal5.year_scroll = true;
											cal5.time_comp = false;

											var cal3 = new CalendarTime(
													document.forms['employeeForm'].elements['initiatedOn']);
											cal3.year_scroll = true;
											cal3.time_comp = false;
											var cal1 = new CalendarTime(
													document.forms['employeeForm'].elements['actualReportedDate']);
											cal1.year_scroll = true;
											cal1.time_comp = false;
										</script>
										<s:if test="%{#session.sessionEmpPractice != 'Systems'}">
											<script type="text/JavaScript">
												var cal4 = new CalendarTime(
														document.forms['employeeForm'].elements['dateOfOccupancy']);
												cal4.year_scroll = true;
												cal4.time_comp = false;
											</script>
										</s:if>
									</s:form>

									<%--  </sx:div> --%>
								</div>

								<!--//END TAB : -->


								<%--   </sx:tabbedpanel> --%>
							</div> <script type="text/javascript">
								var countries = new ddtabcontent("accountTabs")
								countries.setpersist(false)
								countries.setselectedClassTarget("link") //"link" or "linkparent"
								countries.init()
							</script> <script>
								$(document)
										.ready(
												function() {
													//  	alert("hiiiii")

													/*	 $('#homeState').attr('disabled', 'true'); 
														 $('#occupancyType').attr('disabled', 'true'); 
														 $('#dateOfOccupancy').attr('readonly', 'true'); 
														 $('#electricalCharges').attr('readonly', 'true');
														 $('#cafeteriaFee').attr('readonly', 'true'); 
														 $('#roomNo').attr('readonly', 'true'); 
														 $('#roomFee').attr('readonly', 'true');
														 $('#transportFee').attr('readonly', 'true'); 
														 $('#transportLocation').attr('disabled', 'true'); */

													if (document
															.getElementById("currentAction").value == "addEmpTransfers") {

														$('#status').attr(
																'disabled',
																'true'); // mark it as read only
														//  $('#status').css('background-color' , '#DEDEDE'); // change the background color
														document
																.getElementById("status").value = "Initiated";
													}
													if (document
															.getElementById("currentAction").value == "updateEmpTransfers") {
														$('#empName').attr(
																'readonly',
																'true'); // mark it as read only
														//  $('#empName').css('background-color' , '#DEDEDE'); // change the background color

													}

													//var isReported = document.getElementById("isReported").value;

													var location = document
															.getElementById("fromLocation").value;
													var toLocation = document
															.getElementById("toLocation").value;

													// getDues(location);
													getNeedTransport(toLocation);

													var myDeptId = document
															.getElementById("myDeptId").value;
													var sessionEmpPractice = document
															.getElementById("sessionEmpPractice").value;
													var isAdminAccess = document
															.getElementById("isAdminAccess").value;
													var isManager = document
															.getElementById("isUserManager").value;
													var isNextLocationHR = document
															.getElementById("isNextLocationHR").value;
													// 	alert(isManager+"--->"+isAdminAccess)
													//	alert("sessionEmpPractice"+sessionEmpPractice)
													var userId = document
															.getElementById("userId").value;
													var reportedTo = document
															.getElementById("reportedTo").value;
													var flag = document
															.getElementById("flag").value;
													//  alert(document.getElementById("status").value)

													if (document
															.getElementById("transferType").value == "OffshoreTransfer") {
														if (document
																.getElementById("status").value == "Completed") {
															$("#updateButton")
																	.hide();
														}

														if (document
																.getElementById("itTeamStatus").value == "Approved") {
															$("#updateButton")
																	.show();
														} else {
															$("#updateButton")
																	.hide();
														}
													}/* else
																																																			                            		   {
																																																			                            		   if(document.getElementById("isReported").checked){
																																																			                            			   $("#updateButton").show();
																																																			                            		 	$("#nextLocationHRCommentsDiv").show();
																																																			                            	    		
																																																			                            	    	}else
																																																			                            	    		{
																																																			                            	    		$("#nextLocationHRCommentsDiv").hide();
																																																			                            	    		$("#updateButton").hide();
																																																			                            	    		}
																																																			                            	    		
																																																			                            	    		} */

													if (flag == 'Employee') {

														$('#duesRemarks').attr(
																'readonly',
																'true'); // mark it as read only

														$('#empName').attr(
																'readonly',
																'true'); // mark it as read only
														// $('#empName').css('background-color' , '#DEDEDE'); // change the background color
														$('#status').attr(
																'disabled',
																'true'); // mark it as read only
														// $('#status').css('background-color' , '#DEDEDE'); // change the background color
														$('#initiatedOn').attr(
																'readonly',
																'true'); // mark it as read only
														// $('#initiatedOn').css('background-color' , '#DEDEDE'); // change the background color
														$('#fromLocation')
																.attr(
																		'disabled',
																		'true'); // mark it as read only
														// $('#fromLocation').css('background-color' , '#DEDEDE'); // change the background color
														$('#toLocation').attr(
																'disabled',
																'true'); // mark it as read only
														// $('#toLocation').css('background-color' , '#DEDEDE'); // change the background color
														$('#reasonsForTransfer')
																.attr(
																		'readonly',
																		'true'); // mark it as read only
														//   $('#reasonsForTransfer').css('background-color' , '#DEDEDE'); // change the background color
														$("#calenderImg")
																.hide();

														$("#calenderImg2")
																.hide();

														$('#itTeamStatus')
																.attr(
																		'disabled',
																		'true'); // mark it as read only
														// $('#itTeamStatus').css('background-color' , '#DEDEDE'); // change the background color
														$('#itTeamComments')
																.attr(
																		'readonly',
																		'true'); // mark it as read only

														$("#isReportedTr")
																.hide();
														$(
																"#nextLocationHRCommentsDiv")
																.hide();

														$('#reportedTo').attr(
																'disabled',
																'true'); // mark it as read only
														// $('#itTeamStatus').css('background-color' , '#DEDEDE'); // change the background color
														$(
																'#tentativeReportedDate')
																.attr(
																		'readonly',
																		'true'); // mark it as read only
														$("#calenderImg1")
																.hide();
														$('#hostelDues').attr(
																'disabled',
																'true');
														$('#cafteriaDues')
																.attr(
																		'disabled',
																		'true');
														$('#isKeysHandover')
																.attr(
																		'disabled',
																		'true');
														$('#needTrans').attr(
																'disabled',
																true);
														$('#homeState').attr(
																'disabled',
																'true');
														$('#occupancyType')
																.attr(
																		'disabled',
																		'true');
														$('#dateOfOccupancy')
																.attr(
																		'readonly',
																		'true');
														$('#electricalCharges')
																.attr(
																		'readonly',
																		'true');
														$('#cafeteriaFee')
																.attr(
																		'readonly',
																		'true');
														$('#roomNo').attr(
																'readonly',
																'true');
														$('#roomFee').attr(
																'readonly',
																'true');
														$('#transportFee')
																.attr(
																		'readonly',
																		'true');
														$('#transportLocation')
																.attr(
																		'disabled',
																		'true');

														if (location != "") {
															if (location == "Miracle City") {
																// alert(location)
																$("#mcityDues")
																		.show();
																$("#mcityDues1")
																		.show();
																$("#otherDue")
																		.hide();

																$("#mcityexp")
																		.show();
																$("#mcityexp1")
																		.show();
																$("#mcityexp2")
																		.show();
																$("#mcityexp3")
																		.show();

																$("#mirexp")
																		.hide();
																$("#mirexp1")
																		.hide();

																//document.getElementById("mcityDues").style.display = '';
																//document.getElementById("mcityDues1").style.display = '';
																//document.getElementById("otherDue").style.display = 'none';

															} else {

																$("#mcityexp")
																		.hide();
																$("#mcityexp1")
																		.hide();
																$("#mcityexp2")
																		.hide();
																$("#mcityexp3")
																		.hide();

																$("#mirexp")
																		.show();
																$("#mirexp1")
																		.show();
																// $("#calenderImg2").hide();
																$("#mcityDues")
																		.hide();
																$("#mcityDues1")
																		.hide();
																$("#otherDue")
																		.show();

																//document.getElementById("mcityDues").style.display = 'none';
																//document.getElementById("mcityDues1").style.display = 'none';
																//document.getElementById("otherDue").style.display = '';
															}
														} else {
															$("#mcityDues")
																	.hide();
															$("#mcityDues1")
																	.hide();
															$("#otherDue")
																	.hide();

															$("#mcityexp")
																	.hide();
															$("#mcityexp1")
																	.hide();
															$("#mcityexp2")
																	.hide();
															$("#mcityexp3")
																	.hide();

															$("#mirexp").hide();
															$("#mirexp1")
																	.hide();
														}

														if (toLocation == "Miracle Heights") {
															$("#needTrans")
																	.show();

														} else {
															$("#needTrans")
																	.hide();

														}
													} else {
														/* For IT Team */
														// if(isAdminAccess != 1 && isManager!= 1){
														if (isAdminAccess != 1) {

															//alert("hiiiiii")

															var reportedTo = document
																	.getElementById("reportedTo").value;
															$('#itTeamStatus')
																	.attr(
																			'disabled',
																			'true'); // mark it as read only
															// $('#itTeamStatus').css('background-color' , '#DEDEDE'); // change the background color
															$('#itTeamComments')
																	.attr(
																			'readonly',
																			'true'); // mark it as read only
															//  $('#itTeamComments').css('background-color' , '#DEDEDE'); // change the background color

															$("#dueRemarks")
																	.show();
															$("#reportedTr")
																	.show();
															$(
																	"#nextLocationHRCommentsDiv")
																	.hide();
															if (location != "") {
																if (location == "Miracle City") {
																	// alert(location)
																	$(
																			"#mcityDues")
																			.show();
																	$(
																			"#mcityDues1")
																			.show();
																	$(
																			"#otherDue")
																			.hide();

																	$(
																			"#mcityexp")
																			.show();
																	$(
																			"#mcityexp1")
																			.show();
																	$(
																			"#mcityexp2")
																			.show();
																	$(
																			"#mcityexp3")
																			.show();

																	$("#mirexp")
																			.hide();
																	$(
																			"#mirexp1")
																			.hide();

																	//document.getElementById("mcityDues").style.display = '';
																	//document.getElementById("mcityDues1").style.display = '';
																	//document.getElementById("otherDue").style.display = 'none';

																} else {

																	$(
																			"#mcityexp")
																			.hide();
																	$(
																			"#mcityexp1")
																			.hide();
																	$(
																			"#mcityexp2")
																			.hide();
																	$(
																			"#mcityexp3")
																			.hide();

																	$("#mirexp")
																			.show();
																	$(
																			"#mirexp1")
																			.show();
																	// $("#calenderImg2").hide();
																	$(
																			"#mcityDues")
																			.hide();
																	$(
																			"#mcityDues1")
																			.hide();
																	$(
																			"#otherDue")
																			.show();

																	//document.getElementById("mcityDues").style.display = 'none';
																	//document.getElementById("mcityDues1").style.display = 'none';
																	//document.getElementById("otherDue").style.display = '';
																}
															} else {
																$("#mcityDues")
																		.hide();
																$("#mcityDues1")
																		.hide();
																$("#otherDue")
																		.hide();

																$("#mcityexp")
																		.hide();
																$("#mcityexp1")
																		.hide();
																$("#mcityexp2")
																		.hide();
																$("#mcityexp3")
																		.hide();

																$("#mirexp")
																		.hide();
																$("#mirexp1")
																		.hide();
															}

															if (toLocation == "Miracle Heights") {
																$("#needTrans")
																		.show();

															} else {
																$("#needTrans")
																		.hide();

															}
															//$("#isReportedTr").show();

															//		if(userId == reportedTo){
															//			$("#isReportedTr").show();
															//         }
															// alert("isNextLocation-->"+isNextLocationHR)
															if (isNextLocationHR == 1
																	|| isManager == 1) {
																$(
																		'#itTeamStatus')
																		.attr(
																				'disabled',
																				'true'); // mark it as read only
																// $('#itTeamStatus').css('background-color' , '#DEDEDE'); // change the background color
																$(
																		'#itTeamComments')
																		.attr(
																				'readonly',
																				'true'); // mark it as read only
																//  $('#itTeamComments').css('background-color' , '#DEDEDE'); // change the background color

																$(
																		"#isReportedTr")
																		.show();
																if (document
																		.getElementById("isReported").checked) {
																	$(
																			"#nextLocationHRCommentsDiv")
																			.show();
																} else {
																	$(
																			"#nextLocationHRCommentsDiv")
																			.hide();
																}
															} else {

																$(
																		"#isReportedTr")
																		.hide();
																if (document
																		.getElementById("itTeamStatus").value == ""
																		|| document
																				.getElementById("itTeamStatus").value == null) {
																	$(
																			"#itTeamTr")
																			.hide();
																	$(
																			"#itTeamTr1")
																			.hide();
																} else {
																	$(
																			'#itTeamStatus')
																			.attr(
																					'disabled',
																					'true'); // mark it as read only
																	// $('#itTeamStatus').css('background-color' , '#DEDEDE'); // change the background color
																	$(
																			'#itTeamComments')
																			.attr(
																					'readonly',
																					'true'); // mark it as read only
																	//  $('#itTeamComments').css('background-color' , '#DEDEDE'); // change the background color

																}

															}

														}/*else if(isManager == 1 && isAdminAccess != 1){
																																																							$('#itTeamStatus').attr('disabled', 'true'); // mark it as read only
																																																						    $('#itTeamStatus').css('background-color' , '#DEDEDE'); // change the background color
																																																						   $('#itTeamComments').attr('readonly', 'true'); // mark it as read only
																																																						    $('#itTeamComments').css('background-color' , '#DEDEDE'); // change the background color
																																																						/*	$("#isReportedTr").show();
																																																							if(document.getElementById("isReported").checked){
																																																							$("#nextLocationHRCommentsDiv").show();
																																																						   }else{
																																																							 $("#nextLocationHRCommentsDiv").hide();
																																																						    } */
														//	}
														else {
															// 		$("#isReportedTr").show();
															// 			if(document.getElementById("isReported").checked){
															//     		$("#nextLocationHRCommentsDiv").show();
															//     	}else{
															//     		 $("#nextLocationHRCommentsDiv").hide();
															//     	}

															if (location != "") {
																if (location == "Miracle City") {
																	// alert(location)
																	$(
																			"#mcityDues")
																			.show();
																	$(
																			"#mcityDues1")
																			.show();
																	$(
																			"#otherDue")
																			.hide();

																	$(
																			"#mcityexp")
																			.show();
																	$(
																			"#mcityexp1")
																			.show();
																	$(
																			"#mcityexp2")
																			.show();
																	$(
																			"#mcityexp3")
																			.show();

																	$("#mirexp")
																			.hide();
																	$(
																			"#mirexp1")
																			.hide();

																	//document.getElementById("mcityDues").style.display = '';
																	//document.getElementById("mcityDues1").style.display = '';
																	//document.getElementById("otherDue").style.display = 'none';

																} else {

																	$(
																			"#mcityexp")
																			.hide();
																	$(
																			"#mcityexp1")
																			.hide();
																	$(
																			"#mcityexp2")
																			.hide();
																	$(
																			"#mcityexp3")
																			.hide();

																	$("#mirexp")
																			.show();
																	$(
																			"#mirexp1")
																			.show();

																	$(
																			"#mcityDues")
																			.hide();
																	$(
																			"#mcityDues1")
																			.hide();
																	$(
																			"#otherDue")
																			.show();

																	//document.getElementById("mcityDues").style.display = 'none';
																	//document.getElementById("mcityDues1").style.display = 'none';
																	//document.getElementById("otherDue").style.display = '';
																}
															} else {
																$("#mcityDues")
																		.hide();
																$("#mcityDues1")
																		.hide();
																$("#otherDue")
																		.hide();

																$("#mcityexp")
																		.hide();
																$("#mcityexp1")
																		.hide();
																$("#mcityexp2")
																		.hide();
																$("#mcityexp3")
																		.hide();

																$("#mirexp")
																		.hide();
																$("#mirexp1")
																		.hide();
															}

															if (toLocation == "Miracle Heights") {
																$("#needTrans")
																		.show();

															} else {
																$("#needTrans")
																		.hide();

															}

														}
													}

												});
							</script> <script type="text/javascript">
																																	$(
																																			window)
																																			.load(
																																					function() {

																																						var transferType = document
																																								.getElementById('transferType').value;
																																						transferTypeDetails(transferType);
																																					});
																																</script> <!--//END TABBED PANNEL : -->
						</td>
						<!--//END DATA COLUMN : Coloumn for Screen Content-->
					</tr>
				</table>
			</td>
		</tr>
		<!--//END DATA RECORD : Record for LeftMenu and Body Content-->

		<!--//START FOOTER : Record for Footer Background and Content-->
		<tr class="footerBg">
			<td align="center"><s:include
					value="/includes/template/Footer.jsp" /></td>
		</tr>
		<!--//END FOOTER : Record for Footer Background and Content-->

	</table>
	<!--//END MAIN TABLE : Table for template Structure-->

	<script type="text/JavaScript"
		src="<s:url value="/includes/javascripts/jquery.min.js"/>"></script>
	<script type="text/javascript"
		src="<s:url value="/includes/javascripts/reviews/jquery.js"/>"></script>


	<script type="text/javascript">
		$(window).load(function() {
			var transferType = document.getElementById('transferType').value;
			var toLocationnew = document.getElementById('toLocationnew').value;
			//alert("transferType"+transferType);
			transferTypeDetails1(transferType, toLocationnew);

		});
	</script>



</body>

</html>