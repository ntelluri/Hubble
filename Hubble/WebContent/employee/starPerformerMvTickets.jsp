<%@ page contentType="text/html; charset=UTF-8" errorPage="../exception/ErrorDisplay.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%--<%@ taglib prefix="sx" uri="/struts-dojo-tags" %> --%>
<%@ page import="com.freeware.gridtag.*" %> 
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="com.mss.mirage.util.ConnectionProvider"%>
<%@ page import="com.mss.mirage.util.ApplicationConstants"%>
<%@ taglib uri="/WEB-INF/tlds/datagrid.tld" prefix="grd" %>
<html>
    <head>
        <title>Hubble Organization Portal :: Employee Search</title>

        <link rel="stylesheet" type="text/css" href="<s:url value="/includes/css/mainStyle.css"/>">
        <link rel="stylesheet" type="text/css" href="<s:url value="/includes/css/GridStyle.css"/>">
        <link rel="stylesheet" type="text/css" href="<s:url value="/includes/css/leftMenu.css"/>">
        <link rel="stylesheet" type="text/css" href="<s:url value="/includes/css/tabedPanel.css"/>">
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/DBGrid.js"/>"></script>
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/AppConstants.js"/>"></script>
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/tabedPanel.js"/>"></script>
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/CalendarTime.js"/>"></script>
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/reviews/jquery.min.js"/>"></script>
        <script type="text/javascript" src="<s:url value="/includes/javascripts/reviews/jquery.js"/>"></script>   
        <script type="text/javascript" src="<s:url value="/includes/javascripts/StarPerformer.js?version=4.7"/>"></script>   

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <style>


            .round-button1 {
                display:inline-block;
                width:60px;
                height:20px;
                line-height:20px;
                border: 0px solid #f5f5f5;
                border-radius: 10%;
                color:#f5f5f5;
                text-align:center;
                text-decoration:none;
                background: #4679BD;
                box-shadow: 0 0 3px gray;
                font-size:12px;
                font-weight:bold;
                cursor: pointer;
            }
            .round-button1:hover {
                background:#30588e;
            }
        </style>
        <script>
          
        function validateFile(){
             document.getElementById('ForMovieTicketsResultMessage').innerHTML ="";
            var imagePath =document.getElementById('upload').value;
     //alert(imagePath.length);
     if(imagePath.length==0){
         document.getElementById('ForMovieTicketsResultMessage').innerHTML = "<font color=red>Please upload file.</font>";
         return false;
     }else{
           return true;
     }
        }
            function     getAllTickets(){
       
                var starId = document.getElementById("id").value;
                //alert(starId);
                window.location = "getAllTickets.action?starId="+starId;
            }
        </script>

        <s:include value="/includes/template/headerScript.html"/>

    </head>
    <body class="bodyGeneral">

        <%!
            /* Declarations */
            Connection connection;
            String queryString;
            String strTmp;
            String strSortCol;
            String strSortOrd;
            String submittedFrom;
            String searchSubmitValue;
            int intSortOrd = 0;
            int intCurr;
            boolean blnSortAsc = true;
        %>

        <!--//START MAIN TABLE : Table for template Structure-->
        <table class="templateTableLogin" align="center" cellpadding="0" cellspacing="0">

            <!--//START HEADER : Record for Header Background and Mirage Logo-->
            <tr class="headerBg">
            <td valign="top">
                <s:include value="/includes/template/Header.jsp"/>                    
            </td>
        </tr>
        <!--//END HEADER : Record for Header Background and Mirage Logo-->


        <!--//START DATA RECORD : Record for LeftMenu and Screen Content-->
        <tr>
        <td>
            <table class="innerTableLogin" cellpadding="0" cellspacing="0">
                <tr>

                    <!--//START DATA COLUMN : Coloumn for LeftMenu-->
                <td width="150px;" class="leftMenuBgColor" valign="top">
                    <s:include value="/includes/template/LeftMenu.jsp"/>
                </td>

                <!--//END DATA COLUMN : Coloumn for LeftMenu-->

                <!--//START DATA COLUMN : Coloumn for Screen Content-->
                <td width="850px" class="cellBorder" valign="top" style="padding: 5px 5px 5px 5px;">
                    <!--//START TABBED PANNEL : -->
                    <ul id="accountTabs" class="shadetabs" >
                        <li ><a href="#"  class="selected" rel="employeeSearchTab1" >Star Performers Movie Ticket</a></li>
                        <!--                                <li ><a href="#" rel="employeeSearchTab"  >Star Performers Search</a></li>-->

                    </ul>

                    <%--<sx:tabbedpanel id="employeeSearchPannel" cssStyle="width: 840px; height: 500px;padding: 5px 5px 5px 5px;" doLayout="true"> --%>
                    <div  style="border:1px solid gray; width:840px;height: 500px; overflow:auto; margin-bottom: 1em;">   

                        <div id="employeeSearchTab1" class="tabcontent"  >

                            <div id="overlayForMovieTickets"></div>
                            <div id="specialBoxForMovieTickets">
                                <s:form theme="simple" align="center" name="remainders" action="starPerformerMovieTickets" enctype="multipart/form-data" id="remainders" onsubmit="return validateFile();">


                                    <table align="center" border="0" cellspacing="0" style="width:100%;" >
                                        <tr>
                                        <td colspan="2" style="background-color: #288AD1" >
                                            <h3 style="color:darkblue;" align="left">
                                                <span id="remainderHeaderLabelForMovieTickets"></span>


                                            </h3></td>
                                        <td colspan="2" style="background-color: #288AD1" align="right">

                                            <a href="#" onmousedown="ticketsUpload()" >
                                                <img src="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/closeButton.png" />

                                            </a>

                                        </td></tr>



                                        <tr>
                                        <td colspan="4">
                                            <div id="ForMovieTicketsLodingg" style="color: green;display: none;">Loading..</div>
                                            <div id="ForMovieTicketsResultMessage"></div>
                                        </td>


                                        </tr>
                                        <tr><td colspan="4">
                                            <table style="width:80%;" align="center" border="0">
                                                <div id="ForMovieTicketsResultMessage12"></div>
                                                <tr>

                                                    <s:hidden name="EmpId" id="forMovieTicketsEmpId"/>
                                                    <s:hidden name="objectId" id="forMovieTicketsObjectId"/>
                                                    <s:hidden name="starId" id="forMovieTicketsStarId"/>
                                                   
                                      
                                         <s:hidden name="suvyId" id="forMoviesurvyId" value="%{survyId}"/>

                                                <div id="alertMessageforMovieTickets"></div>
                                                </tr>
                                                <tr>

                                                <td class="fieldLabel">Upload:<FONT color="red"  ><em>*</em></FONT></td>
                                                <td colspan="3"><s:file name="upload" label="file" cssClass="inputTextarea" id="upload"    theme="simple"/>
                                                </td>

                                                </tr>          

                                                <tr>
                                                <td colspan="10" style="text-align: right;">
                                                    <s:submit type="button"  value="Add"  cssClass="buttonBg" id="addRemainderButton"  style="margin-right: 17px;"/>
                                                </td></tr>
                                            </table>
                                        </td>
                                        </tr>
                                    </table>
                                </s:form>

                            </div>



                            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                <tr align="right">
                                <td class="headerText">
                                    <img alt="Home" 
                                         src="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/spacer.gif" 
                                         width="100%" 
                                         height="13px" 
                                         border="0"/>
                                    <%if (request.getAttribute(ApplicationConstants.RESULT_MSG) != null) {
                                                      out.println(request.getAttribute(ApplicationConstants.RESULT_MSG));
                                                  }%>  
                                </td>
                                </tr>
                                <tr>
                                <td>
                                     <a href="<s:url action="../employee/starPerformance.action"></s:url>" style="align:center;">

                                                <img alt="Back to List"
                                                     src="<s:url value="/includes/images/backToList_66x19.gif"/>" 
                                                width="66px" 
                                                height="19px"
                                                border="0" align="right"></a>
                                </td>
                                </tr>
                                      <s:hidden name="id" id="id" value="%{id}"/>
                                       <s:hidden name="statusId" id="statusId" value="%{statusId}"/>
                                         <s:hidden name="suvyId" id="suvyId" value="%{suvyId}"/>
                                
                            <tr><td colspan="6" align="center"><div id="reqloadMessageMvTickets" style="display: none"><font color="red" size="2px">Loading Please wait.. </font></div>
                                <div id="loadMessageMvTickets"></div>
                            </td></tr>
                            <tr>
                                <td height="20px" align="right">
                                
                            </td></tr><tr>
                            <td height="20px" align="right">
                                <input type="button" value="download All" id="downLoadAllTickets" style="margin-right:20px;display: none" onclick="getAllTickets();"/>
                            </td>
                            </tr>
                            <tr>
                            <td colspan="6" >

                                <div id="StarPerformerMvTicketsDiv">

                                    <table id="tblStarPerformerMvTicketsList" cellpadding='1' cellspacing='1' border='0' class="gridTable" width='100%' align="center">
                                       

                                                                                </table>
                                                                                <br>

                                                                                </div>
                                                                                </td>
                                                                                </tr>



                                                                                </table>

                                                                                <!-- End Overlay -->
                                                                                <!-- Start Special Centered Box -->

                                                                                <%-- </sx:div > --%>
                                                                                </div>

                                                                                <!--//END TAB : -->
                                                                                <%-- </sx:tabbedpanel> --%>
                                                                                </div>
                                                                                <script type="text/javascript">

                                                                                    var countries=new ddtabcontent("accountTabs")
                                                                                    countries.setpersist(false)
                                                                                    countries.setselectedClassTarget("link") //"link" or "linkparent"
                                                                                    countries.init()

                                                                                </script>
                                                                                <!--//END TABBED PANNEL : -->
                                                                                </td>
                                                                                <!--//END DATA COLUMN : Coloumn for Screen Content-->
                                                                                </tr>
                                                                                </table>
                                                                                </td>
                                                                                </tr>
                                                                                <!--//END DATA RECORD : Record for LeftMenu and Body Content-->

                                                                                <!--//START FOOTER : Record for Footer Background and Content-->
                                                                                <tr class="footerBg">
                                                                                <td align="center"><s:include value="/includes/template/Footer.jsp"/></td>
                                                                                </tr>
                                                                                <tr>

                                                                                <td>

                                                                                    <div style="display: none; position: absolute; top:170px;left:320px;overflow:auto;" id="menu-popup">
                                                                                        <table id="completeTable" border="1" bordercolor="#e5e4f2" style="border: 1px dashed gray;" cellpadding="0" class="cellBorder" cellspacing="0" />
                                                                                    </div>

                                                                                </td>
                                                                                </tr>

                                                                                <!--//END FOOTER : Record for Footer Background and Content-->

                                                                                </table>
                                                                                <!--//END MAIN TABLE : Table for template Structure-->

     <script type="text/javascript">
                                                                                                                                                                $(window).load(function(){
                                                                                                                                                                    getEmpMovieTicketList();
                                                                                                                                                                 

                                                                                                                                                                });
                                                                                                                                                            </script>

                                                                                </body>
                                                                                </html>



