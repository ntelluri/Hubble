<%@ page contentType="text/html; charset=UTF-8" errorPage="../exception/ErrorDisplay.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%--<%@ taglib prefix="sx" uri="/struts-dojo-tags" %> --%>
<%@ page import="com.freeware.gridtag.*" %> 
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="com.mss.mirage.util.ConnectionProvider"%>
<%@ page import="com.mss.mirage.util.ApplicationConstants"%>
<%@ taglib uri="/WEB-INF/tlds/datagrid.tld" prefix="grd" %>
<html>
    <head>
        <title>Hubble Organization Portal :: Employee Search</title>

        <link rel="stylesheet" type="text/css" href="<s:url value="/includes/css/mainStyle.css"/>">
        <link rel="stylesheet" type="text/css" href="<s:url value="/includes/css/GridStyle.css"/>">
        <link rel="stylesheet" type="text/css" href="<s:url value="/includes/css/leftMenu.css"/>">
        <link rel="stylesheet" type="text/css" href="<s:url value="/includes/css/tabedPanel.css"/>">
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/DBGrid.js"/>"></script>
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/AppConstants.js"/>"></script>
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/tabedPanel.js"/>"></script>
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/CalendarTime.js"/>"></script>
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/reviews/jquery.min.js"/>"></script>
        <script type="text/javascript" src="<s:url value="/includes/javascripts/reviews/jquery.js"/>"></script>   
        <script type="text/javascript" src="<s:url value="/includes/javascripts/StarPerformer.js?version=4.2"/>"></script>
         <link rel="stylesheet" type="text/css" href="<s:url value="/includes/css/new/x0popup.min.css"/>">
         
        <script>
            function StarPerformersAdd(){
                var year = document.getElementById("year").value;
                var month=document.getElementById('month').value;
                var status=document.getElementById('status').value;
                //      alert("mnth is---->"+month);
                //      alert("status is---->"+status);
                window.location = "getStarPerformersAdd.action?year="+year+"&month="+month+"&status="+status;
            }
        
           
        </script>
        <style>
            .round-button2 {
                display:inline-block;
                width:20px;
                height:20px;
                line-height:20px;
                border: 0px solid #f5f5f5;
                border-radius: 50%;
                color:#f5f5f5;
                text-align:center;
                text-decoration:none;
                background: #79bd46;
                box-shadow: 0 0 3px gray;
                font-size:8px;
                font-weight:bold;
                cursor: pointer;
            }
            .round-button2:hover {
                background:#30588e;
            }
        </style>
        <style>
            .round-button {
                display:inline-block;
                width:20px;
                height:20px;
                line-height:20px;
                border: 0px solid #f5f5f5;
                border-radius: 50%;
                color:#f5f5f5;
                text-align:center;
                text-decoration:none;
                background: #4679BD;
                box-shadow: 0 0 3px gray;
                font-size:8px;
                font-weight:bold;
                cursor: pointer;
            }
            .round-button:hover {
                background:#30588e;
            }
        </style>
        <style>


            .round-button1 {
                display:inline-block;
                width:60px;
                height:20px;
                line-height:20px;
                border: 0px solid #f5f5f5;
                border-radius: 10%;
                color:#f5f5f5;
                text-align:center;
                text-decoration:none;
                background: #4679BD;
                box-shadow: 0 0 3px gray;
                font-size:12px;
                font-weight:bold;
                cursor: pointer;
            }
            .round-button1:hover {
                background:#30588e;
            }
        </style>

        <s:include value="/includes/template/headerScript.html"/>

    </head>
    <body class="bodyGeneral">

        <%!
            /* Declarations */
            Connection connection;
            String queryString;
            String strTmp;
            String strSortCol;
            String strSortOrd;
            String submittedFrom;
            String searchSubmitValue;
            int intSortOrd = 0;
            int intCurr;
            boolean blnSortAsc = true;
        %>

        <!--//START MAIN TABLE : Table for template Structure-->
        <table class="templateTableLogin" align="center" cellpadding="0" cellspacing="0">

            <!--//START HEADER : Record for Header Background and Mirage Logo-->
            <tr class="headerBg">
            <td valign="top">
                <s:include value="/includes/template/Header.jsp"/>                    
            </td>
        </tr>
        <!--//END HEADER : Record for Header Background and Mirage Logo-->


        <!--//START DATA RECORD : Record for LeftMenu and Screen Content-->
        <tr>
        <td>
            <table class="innerTableLogin" cellpadding="0" cellspacing="0">
                <tr>

                    <!--//START DATA COLUMN : Coloumn for LeftMenu-->
                <td width="150px;" class="leftMenuBgColor" valign="top">
                    <s:include value="/includes/template/LeftMenu.jsp"/>
                </td>

                <!--//END DATA COLUMN : Coloumn for LeftMenu-->

                <!--//START DATA COLUMN : Coloumn for Screen Content-->
                <td width="850px" class="cellBorder" valign="top" style="padding: 5px 5px 5px 5px;">
                    <!--//START TABBED PANNEL : -->
                    <ul id="accountTabs" class="shadetabs" >
                        <li ><a href="#" class="selected" rel="employeeSearchTab"  >Star&nbsp;Performers&nbsp;List</a></li>
                    </ul>
                    <%--<sx:tabbedpanel id="employeeSearchPannel" cssStyle="width: 840px; height: 500px;padding: 5px 5px 5px 5px;" doLayout="true"> --%>
                    <div  style="border:1px solid gray; width:840px;height: 500px; overflow:auto; margin-bottom: 1em;">   
                        <!--//START TAB : -->

                        <div id="employeeSearchTab" class="tabcontent">
                               <div id="blockDiv"
										style="display: none; align: center; position: fixed; top: 50%; left: 48%;">
										<font
											style="font-weight: bold; position: absolute; color: #fff; font-size: 23px; top: -53px; left: 0%">
											Loading please wait...
										</font> <img src='../includes/images/ajax-loader.gif' WIDTH='35%'
											HEIGHT='5%' alt='block'>
									</div>
									<div id="displyID"></div>
                            <%-- Star performer for particular record Adding Start --%>
                            <div id="staroverlay" ></div>
                            <div id="starspecialBox" >
                                <s:form theme="simple" align="center" name="eventForm" id="eventForm">


                                    <table align="center" border="0" cellspacing="0" style="width:100%;" >
                                        <tr>

                                        <td colspan="2" style="background-color: #288AD1" >
                                            <h3 style="color:darkblue;" align="left">
                                                <span id="headerLabel"></span>



                                            </h3></td>
                                        <td colspan="2" style="background-color: #288AD1" align="right">

                                            <a href="#" onclick="mytoggleStarOverlay('0')" >
                                                <img src="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/closeButton.png" /> 

                                            </a>  

                                        </td></tr>
                                        <tr>
                                        <td colspan="4">
                                             <div id="load" style="color: red;display: none;font-size:14px;" align="center">Loading Please wait...</div>
                                            <div id="resultMessage"></div>
                                        </td>


                                        </tr>
                                        <tr><td colspan="4">
                                            <table style="width:80%;" align="center">
                                                <div id="resultMessage123"></div>
                                                <tr>
                                                <td class="fieldLabel">Year(YYYY):</td>
                                                <td><s:textfield name="overlayYear" id="overlayYear" maxlength="4" cssClass="inputTextBlueSmall" value="%{year}"/></td>
                                                <td class="fieldLabel">Month:</td>
                                               <%-- <td><s:select list="#@java.util.LinkedHashMap@{'01':'Jan','02':'Feb','03':'Mar','04':'Apr','05':'May','06':'June','07':'July','08':'Aug','09':'Sept','10':'Oct','11':'Nov','12':'Dec'}" name="overlayMonth" id="overlayMonth" value="%{month}" /></td> --%>
                                               <td><s:select  list="monthsMap" name="overlayMonth" id="overlayMonth" value="%{month}" /></td>    
                                                </tr>
                                                <s:hidden name="overlayStatus" id="overlayStatus" value="StarPerformer Submitted"/>
                                                <tr>

                                                <td colspan="4" align="right">
                                                    <input type="button" class="buttonBg"  align="right"  value="Add" onclick="StarPerformersAjaxAdd();" id="spInitiationAddButton"/>                     
                                                </td> </tr>
                                            </table>
                                        </td>
                                        </tr>



                                    </table>
                                    <div style="display: none; position: fixed; top:69px;left:242px;overflow:auto;" id="menu-popup">
                                        <table id="completeTable" border="1" bordercolor="#e5e4f2" style="border: 1px dashed gray;" cellpadding="0" class="cellBorder" cellspacing="0" ></table>
                                    </div>
                                </s:form>

                            </div> 

                            <%-- Star performer for particular record Adding end --%>



                            <div id="overlayForStarPerformerResources"></div>
                            <div id="specialBoxForStarPerformerResources">
                                <s:form theme="simple" align="center" name="resources" id="resources">


                                    <table align="center" border="0" cellspacing="0" style="width:100%;" >
                                        <tr>
                                        <td colspan="2" style="background-color: #288AD1" >
                                            <h3 style="color:darkblue;" align="left">
                                                <span id="starPerformerResourcesHeaderLabel234"></span>


                                            </h3></td>
                                        <td colspan="2" style="background-color: #288AD1" align="right">

                                            <a href="#" onmousedown="toggleOverlayForStarPerformerResources()" >
                                                <img src="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/closeButton.png" />

                                            </a>

                                        </td></tr>



                                        <tr>
                                        <td colspan="4">
                                               <div id="starPerformerResourcesLodingg" style="color: red;display: none;font-size:14px;" align="center">Loading Please wait...</div>
                                          
                                            <div id="starPerformerResourcesResultMessage"></div>
                                        </td>


                                        </tr>
                                        <tr><td colspan="4">
                                            <table style="width:80%;" align="center" border="0">
                                                <div id="starPerformerResourcesResultMessage12"></div>
                                                <tr>
                                                    <s:hidden name="resourcesObjectId" id="resourcesObjectId"/>




                                                </tr>
                                                <tr>
                                                <td class="fieldLabel">Year(YYYY):</td>
                                                <%--  <td><input type="text" name="year" autocomplete="off" maxlength="4" value="%{year}" id="year" class="inputTextBlue"/></td>  --%>
                                                <td><s:textfield name="resourceOverlayYear" id="resourceOverlayYear" maxlength="4" cssClass="inputTextBlueSmall" value="%{resourceOverlayYear}"/></td>
                                                <td class="fieldLabel">Month:</td>
                                              <%--  <td><s:select list="#@java.util.LinkedHashMap@{'':'All','01':'Jan','02':'Feb','03':'Mar','04':'Apr','05':'May','06':'June','07':'July','08':'Aug','09':'Sept','10':'Oct','11':'Nov','12':'Dec'}" name="resourceOverlayMonth" id="resourceOverlayMonth" value="%{resourceOverlayMonth}" onChange="ele(this.value);"/></td>  --%>
                                                <td><s:select  list="monthsMap" name="resourceOverlayMonth" id="resourceOverlayMonth" value="%{resourceOverlayMonth}" onChange="ele(this.value);"/></td>
                                               
                                                </tr>
                                                <tr>
                                                    <%--   <td class="fieldLabel">Total Star Performer Count:</td>
                                                     <td><input type="text" name="year" autocomplete="off" maxlength="4" value="%{year}" id="year" class="inputTextBlue"/></td> 
                                                      <td><s:textfield name="resourceOverlayCount" id="resourceOverlayCount" maxlength="4" cssClass="inputTextBlueSmall" value="%{resourceOverlayCount}"/></td>
                                                    --%>
                                                <td class="fieldLabel">Status:</td>
                                                <td><s:select list="starPerformerStatus" name="resourceOverlayStatus" id="resourceOverlayStatus" cssClass="inputSelect" value="%{resourceOverlayStatus}" /></td> 
                                                </tr>
                                                <tr>
                                                    <s:if test="%{#session.starPerformerAccessForHrMr == true}">
                                                    <td colspan="4" align="right">

                                                        <%--       <input type="button" class="buttonBg"  align="right"  value="Add" onclick="StarPerformersAdd();"/>       --%>              
                                                        <input type="button" class="buttonBg"  align="right"  value="Update" id="resourceOverlayUpdate" onclick="doResourceOverlyUpdate();"/>                     


                                                    </td></s:if>




                                                    </tr>
                                                </table>
                                            </td>
                                            </tr>
                                        </table>
                                </s:form>

                            </div>





                            <%-- Publish the campaign end--%>

                            <div id="overlayForCampaignSchedule"></div>
                            <div id="specialBoxForCampaignSchedule">
                                <s:form theme="simple" align="center" name="eventForm1" id="eventForm1">


                                    <table align="center" border="0" cellspacing="0" style="width:100%;" >
                                        <tr>
                                        <td colspan="2" style="background-color: #288AD1" >
                                            <h3 style="color:darkblue;" align="left">
                                                <span id="headerLabel2"></span>


                                            </h3></td>
                                        <td colspan="2" style="background-color: #288AD1" align="right">

                                            <a href="#" onmousedown="toggleOverlayForCampaignSchdule()" >
                                                <img src="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/closeButton.png" />

                                            </a>

                                        </td></tr>



                                        <tr>
                                        <td colspan="4">
                                            <div id="lodingg" style="color: green;display: none;">Loading..</div>
                                            <div id="resultMessage"></div>
                                        </td>


                                        </tr>
                                        <tr><td colspan="4">
                                            <table style="width:80%;" align="center" border="0">
                                                <div id="resultMessage12"></div>
                                                <tr>
                                                    <s:hidden name="objectId" id="objectId"/>
                                                    <s:hidden name="CCyear" id="CCyear"/>
                                                    <s:hidden name="CCmonth" id="CCmonth"/>
                                                <div id="alertMessagePublish"></div>
                                                <td class="fieldLabel" colspan="" style="text-align: left;" id="fromDateLabelId">Schedule&nbsp;Date&nbsp;:
                                                    <s:textfield name="startScheduleDate" id="startScheduleDate" cssClass="inputTextBlue"  style="margin-left:44px;" readonly="true"/>
                                                    <a href="javascript:cal3.popup();">
                                                        <img src="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/showCalendar.gif"
                                                             width="20" height="20" border="0"></a>
                                                </td>


                                                <td id="scheduleCampaign" style="text-align: right;">
                                                    <input type="button"  value="Add"  class="buttonBg" id="CampaignAddButton" onclick="doSMCampaignAdd()" style="margin-right: 17px;"/>
                                                </td>

                                                </tr>






                                            </table>
                                        </td>
                                        </tr>
                                    </table>
                                </s:form>

                            </div>


                            <%--Publish the campaign end--%>
                            <%-- Survey from add--%>
                            <div id="overlayForSurveyForms"></div>
                            <div id="specialBoxForSurveyForms">
                                <s:form theme="simple" align="center" name="surveyForm" id="surveyForm">


                                    <table align="center" border="0" cellspacing="0" style="width:100%;" >
                                        <tr>
                                        <td colspan="2" style="background-color: #288AD1" >
                                            <h3 style="color:darkblue;" align="left">
                                                <span id="headerLabel234"></span>


                                            </h3></td>
                                        <td colspan="2" style="background-color: #288AD1" align="right">

                                            <a href="#" onmousedown="toggleOverlayForSurveyForm()" >
                                                <img src="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/closeButton.png" />

                                            </a>

                                        </td></tr>



                                        <tr>
                                         <td colspan="4">
                                            <div id="lodingsurvy" style="color: red;display: none;font-size:14px;" align="center">Loading Please wait...</div>

                                        </td>


                                        </tr>
                                        <tr><td colspan="4">
                                            <table style="width:80%;" align="center" border="0">
                                                <div id="survyresultMessage"></div>
                                                <tr>


                                                    <s:hidden name="objectIdSurvy" id="objectIdSurvy"/>
                                                    <s:hidden name="Survyyear" id="Survyyear"/>
                                                    <s:hidden name="Survymonth" id="Survymonth"/>

                                                <div id="alertMessageSurveyForm"></div>
                                                </tr>
                                                <tr id="expiryDateRow">
                                                <td class="fieldLabel" style="text-align: left">Expiry&nbsp;Date:<FONT color="red"  ><em>*</em></FONT></td>
                                                <td><s:textfield name="expireDate" id="expireDate" cssClass="inputTextBlue" onchange="validateTimestamp(this);" readonly="true" />
                                                    <a href="javascript:cal4.popup();">
                                                        <img src="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/showCalendar.gif"
                                                             width="20" height="20" border="0"></a>
                                                </td>

                                                </tr>
                                                <tr id="movieDatesRow">
                                                <td class="fieldLabel">Movie Dates:</td>

                                                <td><s:textarea rows="5" cols="50" name="movieDates" id="movieDates"  title="Please enter comma separated values" cssClass="inputTextareaOverlay1"  onkeyup="commaValidator(this);" ondrop="return false;" onpaste="return false;" onkeypress="return dropdowncheck(event);"/>
                                                    <img  title="Please enter comma separated values in this field." src="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/info.jpg" height="18" width="20" border="0"/>
                                                </td> </tr>
                                                <tr id="movieNamesRow">
                                                <td class="fieldLabel">Movie Names:</td>

                                                <td><s:textarea rows="5" cols="50" name="movieName" id="movieName"  title="Please enter comma separated values" cssClass="inputTextareaOverlay1"  onkeyup="commaValidator(this);" ondrop="return false;" onpaste="return false;" onkeypress="return dropdowncheck(event);"/>
                                                    <img  title="Please enter comma separated values in this field." src="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/info.jpg" height="18" width="20" border="0"/>
                                                </td> </tr>                                

                                                <tr id="addSurveyFormRow">
                                                <td colspan="3" style="text-align: right;">
                                                    <input type="button"  value="Add"  class="buttonBg" id="sfAddButton" onclick="doAddSurveyFormAndQuestionnaire()" style="margin-right: 17px;"/>
                                                </td>
                                            </table>
                                        </td>
                                        </tr>
                                    </table>
                                </s:form>

                            </div>

                            <%-- Survey from End--%>
                            <%-- ccccccccccccccccccccccccccccccccccccccccccccccccc--%>
                            <div id="overlayForFeedBackForms"></div>
                            <div id="specialBoxForFeedBackForms">
                                <s:form theme="simple" align="center" name="feedBackForm" id="feedBackForm">


                                    <table align="center" border="0" cellspacing="0" style="width:100%;" >
                                        <tr>
                                        <td colspan="2" style="background-color: #288AD1" >
                                            <h3 style="color:darkblue;" align="left">
                                                <span id="headerLabelfeedback"></span>


                                            </h3></td>
                                        <td colspan="2" style="background-color: #288AD1" align="right">

                                            <a href="#" onmousedown="toggleOverlayForFeedbackForm()" >
                                                <img src="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/closeButton.png" />

                                            </a>

                                        </td></tr>



                                        <tr>
                                        <td colspan="4">
                                            <div id="lodingfeedBack" style="color: green;display: none;">Loading..</div>

                                        </td>


                                        </tr>
                                        <tr><td colspan="4">
                                            <table style="width:80%;" align="center" border="0">
                                                <div id="feedBackresultMessage"></div>
                                                <tr>
                                                    <s:hidden name="objectIdfeedBack" id="objectIdfeedBack"/>
                                                    <s:hidden name="feedBackyear" id="feedBackyear"/>
                                                    <s:hidden name="feedBackmonth" id="feedBackmonth"/>
                                                    <s:hidden name="survyId" id="survyId"/>

                                                <div id="alertMessageFeedback"></div>
                                                </tr>
                                                <tr id="feedbackExpiryDateRow">
                                                <td class="fieldLabel" style="text-align: right">Expiry&nbsp;Date:<FONT color="red"  ><em>*</em></FONT></td>
                                                <td><s:textfield name="expireFeedbackDate" id="expireFeedbackDate" cssClass="inputTextBlue" onchange="validateTimestamp(this);" readonly="true" />
                                                    <a href="javascript:cal6.popup();">
                                                        <img src="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/showCalendar.gif"
                                                             width="20" height="20" border="0"></a>
                                                </td>

                                                </tr>


                                                <tr id="feedbackAddRow">
                                                <td colspan="3" style="text-align: right;">
                                                    <input type="button"  value="Add"  class="buttonBg" id="feedBackbuttonBg" onclick="doAddSurveyfeedBack()" style="margin-right: 17px;"/>
                                                </td></tr>
                                            </table>
                                        </td>
                                        </tr>
                                    </table>
                                </s:form>

                            </div>


                            <%-- ccccccccccccccccccccccccccccccccccccccccccccccccc--%>





                            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                <tr align="right">
                                <td class="headerText">
                                    <img alt="Home" 
                                         src="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/spacer.gif" 
                                         width="100%" 
                                         height="13px" 
                                         border="0"/>
                                    <%if (request.getAttribute(ApplicationConstants.RESULT_MSG) != null) {
                                            out.println(request.getAttribute(ApplicationConstants.RESULT_MSG));
                                        }%>  
                                </td>
                                </tr>

                                <tr>
                                <td>

                                    <s:form name="starPerformance" action="" enctype="multipart/form-data" theme="simple">
                                    
                                         <div id="displayMessagePublish"></div>
                                         
                                        <s:hidden name="currentDate" id="currentDate" value="%{currentDate}"/>  
                                        <s:hidden name="starPerformerAccess" id="starPerformerAccess" value="%{#session.starPerformerAccessForHrMr}"/>  
                                        <s:hidden name="sessionStarPerformerMarketingAccess" id="sessionStarPerformerMarketingAccess" value="%{#session.sessionStarPerformerMarketingAccess}"/>  
                                        <table cellpadding="1" cellspacing="1" border="0" width="650px" align="center">
                                            <tr>
                                            <td class="fieldLabel">Year(YYYY):</td>
                                            <td><s:textfield name="year" id="year" maxlength="4" cssClass="inputTextBlueSmall" value="%{year}"/></td>
                                            <td class="fieldLabel">Month:</td>
                                            
                                            <%--<td><s:select list="#@java.util.LinkedHashMap@{'':'All','01':'Jan','02':'Feb','03':'Mar','04':'Apr','05':'May','06':'June','07':'July','08':'Aug','09':'Sept','10':'Oct','11':'Nov','12':'Dec'}" name="month" id="month" value="%{month}" onChange="ele(this.value);"/></td>  --%>
                                            <td><s:select headerKey="" headerValue="All" list="monthsMap" name="month" id="month" value="%{month}" onChange="ele(this.value);"/></td>
                                            </tr>

                                            <tr>

                                            <td colspan="4" align="right">
                                                <input type="button" class="buttonBg" id="starPerformersPeriodSearch"  align="right"  value="Search" onclick="getStarPerformers();"/>
                                                <s:if test="%{#session.starPerformerAccessForHrMr == true}">        <input type="button" class="buttonBg"  align="right" id="starPerformersPeriodAdd"  value="Add" onclick="mytoggleStarOverlay('add');"/>                     
                                                </s:if>

                                            </td>




                                            </tr>


                                            <tr><td colspan="6" align="center"><div id="reqloadMessage" style="display: none"><font color="red" size="2px">Loading Please wait.. </font></div></td></tr>
                                            <tr>
                                            <td colspan="6" >

                                                <div id="StarPerformerReqListDiv" style="display: block">
                                                    <table id="tblStarPerformer" cellpadding='1' cellspacing='1' border='0' class="gridTable" width='100%' align="center">

                                                    </table>
                                                    <br>

                                                </div>
                                            </td>
                                            </tr>

                                        </table>
                                    </s:form>
                                </td>

                                </tr>
                                <tr>
                                <td>
                                    <%

                                        if (request.getAttribute("submitFrom") != null) {
                                            submittedFrom = request.getAttribute("submitFrom").toString();
                                        }

                                        if (!"dbGrid".equalsIgnoreCase(submittedFrom)) {
                                            searchSubmitValue = submittedFrom;
                                        }

                                    %>

                                </td>
                                </tr>
                                <tr>
                                <td>

                                </td>
                                </tr>

                            </table>

                            <!-- End Overlay -->
                            <!-- Start Special Centered Box -->

                            <%-- </sx:div > --%>
                        </div>
                        <!--//END TAB : -->

                        <%-- </sx:tabbedpanel> --%>
                    </div>
                    <script type="text/javascript">


                        var cal3 = new CalendarTime(document.forms['eventForm1'].elements['startScheduleDate']);
                        cal3.year_scroll = true;
                        cal3.time_comp = true;
                        var cal4 = new CalendarTime(document.forms['surveyForm'].elements['expireDate']);
                        cal4.year_scroll = true;
                        cal4.time_comp = true;
                                                                                    
                                                                                    
                        var cal6 = new CalendarTime(document.forms['feedBackForm'].elements['expireFeedbackDate']);
                        cal6.year_scroll = true;
                        cal6.time_comp = true;

                        var countries=new ddtabcontent("accountTabs")
                        countries.setpersist(false)
                        countries.setselectedClassTarget("link") //"link" or "linkparent"
                        countries.init()

                    </script>
                    <!--//END TABBED PANNEL : -->
                </td>
                <!--//END DATA COLUMN : Coloumn for Screen Content-->
                </tr>
            </table>
        </td>
    </tr>
    <!--//END DATA RECORD : Record for LeftMenu and Body Content-->

    <!--//START FOOTER : Record for Footer Background and Content-->
    <tr class="footerBg">
    <td align="center"><s:include value="/includes/template/Footer.jsp"/></td>
</tr>
<!--//END FOOTER : Record for Footer Background and Content-->

</table>
<!--//END MAIN TABLE : Table for template Structure-->
                                                                                                                               <script type="text/javascript">
                                                                                                                                                                $(window).load(function(){
                                                                                                                                                                    getStarPerformers();
                                                                                                                                                                 

                                                                                                                                                                });
                                                                                                                                                            </script>
                                                                                                                                                            <script type="text/JavaScript" src="<s:url value="/includes/javascripts/payroll/x0popup.min.js"/>"></script>
</body>
</html>


