<%-- 
    Document   : InsideSalesStatus
    Created on : Jan 9, 2017, 3:43:14 PM
    Author     : miracle
--%>

<%@ page contentType="text/html; charset=UTF-8" errorPage="../exception/ErrorDisplay.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sx" uri="/struts-dojo-tags" %>
<%@ page import="com.freeware.gridtag.*" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="com.mss.mirage.util.ConnectionProvider"%>
<%@ page import="com.mss.mirage.util.ApplicationConstants"%>
<%@ taglib uri="/WEB-INF/tlds/datagrid.tld" prefix="grd"%>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="javax.sql.DataSource" %>
<%@ page import="com.mss.mirage.util.ConnectionProvider"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <title>Hubble Organization Portal :: Employee Details By Appraisal Status</title>
        <link rel="stylesheet" type="text/css" href="<s:url value="/includes/css/mainStyle.css"/>">
        <link rel="stylesheet" type="text/css" href="<s:url value="/includes/css/GridStyle.css"/>">
        <link rel="stylesheet" type="text/css" href="<s:url value="/includes/css/leftMenu.css"/>">
        <link rel="stylesheet" type="text/css" href="<s:url value="/includes/css/tabedPanel.css"/>">
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/DBGrid.js"/>"></script>
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/ClientValidations.js"/>"></script>        
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/AppConstants.js"/>"></script>
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/tabedPanel.js"/>"></script>
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/CalendarTime.js"/>"></script>   

        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/ajax/AjaxPopup.js"/>"></script>
        <script type="text/javascript" src="<s:url value="/includes/javascripts/searchIssue.js"/>"></script>
        <script type="text/javascript" src="<s:url value="/includes/javascripts/IssueFill.js"/>"></script>
         <script type="text/JavaScript" src="<s:url value="/includes/javascripts/EmployeeAjax.js?version=2.6"/>"></script>
      
     <%--   <script type="text/javascript" src="<s:url value="/includes/javascripts/GreenSheetUtil.js"/>"></script>
  <script type="text/JavaScript" src="<s:url value="/includes/javascripts/EmpStandardClientValidations.js"/>"></script> --%>
      
        <s:include value="/includes/template/headerScript.html"/> 
        <script>
          function  doGetManagerReport(){
              var year=document.getElementById('year').value;
              var quarterly=document.getElementById('quarterly').value;
              var isIncludeTeam=document.getElementById('isIncludeTeam').value;
              var country=document.getElementById('country').value;
              var resourceName=document.getElementById("resourceName").value;
              var practiceId=document.getElementById("practiceId").value;
              
              window.location="getQReviewPendingReportDashBoard.action?backToFlag=1&yearForManagersReport="+year+"&QuarterlyForManagersReport="+quarterly+"&country="+country+"&isIncludeTeam="+isIncludeTeam+
              "&resourceName="+resourceName+"&practiceId="+practiceId;;
          }
            </script>
        
    </head>
    <%-- <body class="bodyGeneral" oncontextmenu="return false;" onload="getRequirementsList1();"> --%>

   <!-- <body class="bodyGeneral" oncontextmenu="return false;" onload="init1();getInsideSalesStatusAccountDetailsList();"> -->

 <body class="bodyGeneral" oncontextmenu="return false;">
        

        <!-- Start oif the table -->
        <table class="templateTable1000x580" align="center" cellpadding="0" cellspacing="0">


            <!--//START HEADER : Record for Header Background and Mirage Logo-->
            <tr class="headerBg">
                <td valign="top">
                    <s:include value="/includes/template/Header.jsp"/>                    
                </td>
            </tr>
            <!--//END HEADER : Record for Header Background and Mirage Logo-->


            <!--//START DATA RECORD : Record for LeftMenu and Screen Content-->
            <tr>
                <td>
                    <table class="innerTable1000x515" cellpadding="0" cellspacing="0">
                        <tr>
                            <!--//START DATA COLUMN : Coloumn for LeftMenu-->
                            <td width="150px;" class="leftMenuBgColor" valign="top"> 
                                <s:include value="/includes/template/LeftMenu.jsp"/> 
                            </td>
                            <!--//START DATA COLUMN : Coloumn for LeftMenu-->

                            <!--//START DATA COLUMN : Coloumn for Screen Content-->
                            <td width="850px" class="cellBorder" valign="top" style="padding:10px 5px 5px 5px;">
                                <!--//START TABBED PANNEL : -->
                                <ul id="accountTabs" class="shadetabs" >
                                    <li ><a href="#" class="selected" rel="requirementList"> <s:property value="status"/> Employee Details</a></li>
                                    <!-- <li ><a href="#" rel="searchDiv">Requirement Search</a></li>-->
                                </ul>
                                <div  style="border:1px solid gray; width:845px;height: 530px; overflow:auto; margin-bottom: 1em;">
                                    <%--  <sx:tabbedpanel id="attachmentPannel2" cssStyle="width: 845px; height: 530px;padding:10px 5px 5px 5px" doLayout="true"> --%>

                                    <!--//START TAB : -->
                                    <%--  <sx:div id="List" label="" cssStyle="overflow:auto;"> --%>
                                    <div id="requirementList">

                                       


                                        <s:form name="frmEmpDetails" id="frmEmpDetails"  theme="simple" >  
                                            <s:hidden name="country" id="country" value="%{country}"/>
                                            <s:hidden name="isIncludeTeam" id="isIncludeTeam" value="%{isIncludeTeam}"/>
                                            <s:hidden name="year" id="year" value="%{year}"/>
                                            <s:hidden name="loginId" id="loginId" value="%{loginId}"/>
                                              <s:hidden name="status" id="status" value="%{status}"/>
                                            <s:hidden name="quarterly" id="quarterly" value="%{quarterly}"/>
                                        
                                          <s:hidden name="opsContactId" id="opsContactId" value="%{opsContactId}"/>
                                          <s:hidden name="practiceId" id="practiceId" value="%{practiceId}"/>
                                          <s:hidden name="resourceName" id="resourceName" value="%{resourceName}"/>
                                              
                                            <table cellpadding="0" cellspacing="0" align="left" width="100%">
                                               
                                                     <tr>
                    <td class="headerText" colspan="6">
                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                            <tr align="right">
                        <td valign="middle">
                       <INPUT type="button" CLASS="buttonBg" value="Back to List" onClick="doGetManagerReport()"/>
                        </td>
                         </tr>
                        </table>
                    </td>
</tr>
                                                
                                                
                                               
                                                                           
<%--                                                                        <a href="getQReviewDashBoard.action?backToFlag=1" style="align:center;">
                                                                            <img alt="Back to List"
                                                                                 src="<s:url value="/includes/images/backToList_66x19.gif"/>" 
                                                                                 width="66px" 
                                                                                 height="19px"
                                                                                 border="0" align="center"></a>--%>
 <tr><td colspan="4">
                                                        <table style="width:80%;" align="center">
                                                            <tr>

                                                               
                                                                <td class="fieldLabel"   >Manager&nbsp;Name : </td>
                                                                <td ><span style="color: green;"> <s:property value="empName"/></span></td>
                                                               
                                                            </tr></table></td></tr>
                                                                          
                                                <tr>
                                                    <td>

                                                        <table cellpadding="0" cellspacing="0" width="100%">
                                                            <tr>
                                                                <td>
                                                                    <table width="100%" cellpadding="1" cellspacing="1" border="0">
                                                                        <tr>
                                                                            <td>
                                                                                <div id="loadingMessage" style="color:red; display: none;" align="center"><b><font size="4px" >Loading...Please Wait...</font></b></div>

                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <%-- <DIV id="loadingMessage"> </DIV> --%>

                                                                                <TABLE id="tblEmpDetailsByAppraisalStatus" align="left"  
                                                                                       cellpadding='1' cellspacing='1' border='0' class="gridTable" width='100%'>
                                                                                    <COLGROUP ALIGN="left" >
                                                                                        <COL width="5%"> 
                                                                                        <COL width="20%"> 
                                                                                        <COL width="15%">
                                                                                        <COL width="15%">
                                                                                        <COL width="15%">
                                                                                        <COL width="15%">
                                                                                        <COL width="15%">
                                                                                </TABLE>

                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>

                                                        </table>


                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </s:form>
                            

                                    <%--   </sx:div> --%>


                            </td>
                            <!--//END DATA COLUMN : Coloumn for Screen Content-->
                        </tr>
                    </table>
                </td>
            </tr>

            <!--//END DATA RECORD : Record for LeftMenu and Body Content-->

            <!--//START FOOTER : Record for Footer Background and Content-->
            <tr class="footerBg">
                <td align="center"><s:include value="/includes/template/Footer.jsp"/>   </td>
            </tr>
            <tr>
                <td>

                    <div style="display: none; position: absolute; top:170px;left:320px;overflow:auto;" id="menu-popup">
                        <table id="completeTable" border="1" bordercolor="#e5e4f2" style="border: 1px dashed gray;" cellpadding="0" class="cellBorder" cellspacing="0" />
                    </div>

                </td>
            </tr>
            <!--//END FOOTER : Record for Footer Background and Content-->

        </table>
        <!--//END MAIN TABLE : Table for template Structure-->

        <!--  End of the main table -->        

<script type="text/javascript">
		$(window).load(function(){
		init1();
		getEmployeesForPendingMngrByApprasailStatus();
		});
		</script>
    </body>
</html>

