<%-- 
   Document   : QreviewReport
   Created on : Apr 11, 2017, 4:14:39 PM
   Author     : miracle
--%>




<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@ page contentType="text/html; charset=UTF-8" errorPage="../exception/ErrorDisplay.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags" %> 
<%-- <%@ taglib prefix="sx" uri="/struts-dojo-tags" %> --%>
<%@ page import="com.mss.mirage.util.ApplicationConstants"%>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="com.mss.mirage.util.ConnectionProvider"%>
<html>
    <head>
        <title>Hubble Organization Portal :: Q-Review Report DashBoard</title>
        <link rel="stylesheet" type="text/css" href="<s:url value="/includes/css/GridStyle.css"/>">
        <link rel="stylesheet" type="text/css" href="<s:url value="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css"/>">
        <link rel="stylesheet" type="text/css" href="<s:url value="/includes/css/mainStyle.css?version=2.1"/>">
        <link rel="stylesheet" type="text/css" href="<s:url value="/includes/css/tabedPanel.css"/>">
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/animatedcollapse.js"/>"></script>
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/jquery-1.2.2.pack.js"/>"></script>
        <link rel="stylesheet" type="text/css" href="<s:url value="/includes/css/leftMenu.css"/>">
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/CalendarTime.js"/>"></script>
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/AppConstants.js"/>"></script>
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/EnableEnter.js"/>"></script>
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/ReusableContainer.js"/>"></script>
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/tabedPanel.js"/>"></script>
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/EmployeeAjax.js?version=3.0"/>"></script>
         <link rel="stylesheet" type="text/css" href="<s:url value="/includes/css/new/x0popup.min.css?version=1.0"/>">
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/payroll/x0popup.min.js"/>"></script>
        <script type="text/javascript" src="https://www.google.com/jsapi"></script> 
        <s:include value="/includes/template/headerScript.html"/>
        <script language="JavaScript">
            google.load("visualization", "1", {packages:["corechart"]});   
            animatedcollapse.init();
        </script>
        <script language="JavaScript">
                
            animatedcollapse.addDiv('QReviewDashBoardDiv', 'fade=1;speed=400;persist=1;group=app');
            animatedcollapse.addDiv('QReviewPracticeVsStatusDiv', 'fade=1;speed=400;persist=1;group=app');
            animatedcollapse.addDiv('QReviewQuarterVsStatusDiv', 'fade=1;speed=400;persist=1;group=app');
            animatedcollapse.addDiv('QReviewManagerDashBoardDiv', 'fade=1;speed=400;persist=1;group=app');

           
            animatedcollapse.init();
            function hideSelect(){
                //document.getElementById("priorityId").style.display = 'none';
                
            }
        </script>
        <style type="text/css">
            .rcorners2 {
                border-radius: 25px;
                border: 2px solid #3e93d4;
                padding: 20px; 
                width: auto;
                height: auto;    
            }   
            .popupItem:hover {
                background: #F2F5A9;
                font: arial;
                font-size:10px;
                color: black;
            }

            .popupRow {
                background: #3E93D4;
            }

            .popupItem {
                padding: 2px;
                width: 100%;
                border: black;
                font:normal 9px verdana;
                color: white;
                text-decoration: none;
                line-height:13px;
                z-index:100;
            }
            #parent {
                height: auto;
                width:780px;
                max-height: 500px;
                overflow: visible;
            }

        </style>
    </head>
    <!--  <body  class="bodyGeneral" onload="javascript:animatedcollapse.show('TasksDashBoardDiv');" oncontextmenu="return false;"> -->
    <body  class="bodyGeneral" oncontextmenu="return false;">

        <%!
            /* Declarations */
            String queryString = null;
            Connection connection;
            Statement stmt;
            ResultSet rs;
            int userCounter = 0;
            int activityCounter = 0;
            int accountCounter = 0;
        %>


        <!--//START MAIN TABLE : Table for template Structure-->
        <table class="templateTable1000x580" align="center" cellpadding="0" cellspacing="0">

            <!--//START HEADER : Record for Header Background and Mirage Logo-->
            <tr class="headerBg">
            <td valign="top">
                <s:include value="/includes/template/Header.jsp"/>                    
            </td>
        </tr>
        <!--//END HEADER : Record for Header Background and Mirage Logo-->

        <!--//START DATA RECORD : Record for LeftMenu and Screen Content-->
        <tr>
        <td>
            <table class="innerTable1000x515" cellpadding="0" cellspacing="0">
                <tr>
                    <!--//START DATA COLUMN : Coloumn for LeftMenu-->
                <td width="850px;" class="leftMenuBgColor" valign="top"> 
                    <s:include value="/includes/template/LeftMenu.jsp"/>
                </td>
                <!--//START DATA COLUMN : Coloumn for LeftMenu-->

                <!--//START DATA COLUMN : Coloumn for Screen Content-->
                <td width="850px" class="cellBorder" valign="top" style="padding-left:10px;padding-top:5px;">
                    <!--//START TABBED PANNEL : -->
                    <%--      <sx:tabbedpanel id="resetPasswordPannel" cssStyle="width: 845px; height: 550px;padding-left:10px;padding-top:5px;" doLayout="true" useSelectedTabCookie="true" > 
                        
                        <!--//START TAB : -->
                        <sx:div id="dashBoardTab" label="DashBoard Details" cssStyle="overflow:auto;"> --%>
                    <ul id="qreviewTab" class="shadetabs" >
                        <li><a href="#" rel="QReviewDashBoardTab" class="selected">Q-Review&nbsp;DashBoard</a></li>

                    </ul>
                    <div  style="border:1px solid gray; width:840px;height:750px;overflow:auto; margin-bottom: 1em;">    
                        <br>
                        <div id="QReviewDashBoardTab" class="tabcontent" >  

                            <table cellpadding="0" cellspacing="0" border="0" width="100%">

                                <%-- Tasks DashBoard start --%>

                                <tr>
                                <td class="homePortlet" valign="top">
                                    <div class="portletTitleBar">
                                        <div class="portletTitleLeft">Q-Reviews Consoldated Report By Status</div>
                                        <div class="portletIcons">
                                            <a href="javascript:animatedcollapse.hide('QReviewDashBoardDiv')" title="Minimize">
                                                <img src="../../includes/images/portal/title_minimize.gif" alt="Minimize"/></a>
                                            <a href="javascript:animatedcollapse.show('QReviewDashBoardDiv')" title="Maximize">
                                                <img src="../../includes/images/portal/title_maximize.gif" alt="Maximize"/>
                                            </a>
                                        </div>
                                        <div class="clear"></div>
                                    </div>

                                    <div id="QReviewDashBoardDiv" style="background-color:#ffffff">
                                        <table cellpadding="0" cellspacing="0" border="0" width="100%"> 
                                            <tr>
                                            <td width="80%" valign="top" align="center">
                                                <s:form action="" theme="simple" name="QReviewDashBoard" id="QReviewDashBoard">  

                                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                        <tr>
                                                        <td>
                                                            <table border="0" align="center" cellpadding="0" cellspacing="0" width="700;">
                                                                <br>
                                                                <tr>

                                                                <td class="fieldLabel"> Year:</td>
                                                                <td><s:textfield name="year" id="year" value="%{year}" cssClass="inputTextBlue" onkeyup="isNumericYearQreview(this);" maxLength="4"/></td>
                                                                <td class="fieldLabel">Quarter:</td>
                                                                <td >

                                                                    <s:select headerKey="" headerValue="--Please Select--" id="quarterly"  name="quarterly" list="#@java.util.LinkedHashMap@{'Q1':'Q1','Q2':'Q2','Q3':'Q3','Q4':'Q4'}" cssClass="inputSelect"  disabled="False"/>


                                                                    <input  type="button" style="margin-left: 20px;" value="Search" class="buttonBg" onclick="getQReviewDashBoardByStatus();"/>
                                                                </td>

                                                                </tr>



                                                            </table>
                                                        </td>
                                                        </tr>
                                                        <tr>
                                                        <td>
                                                            <table border="0" align="center" cellpadding="0" cellspacing="0" >

                                                                <tr>
                                                                <td>

                                                                    <table align="center" cellpadding="2" border="0" cellspacing="1" width="50%" >
                                                                        <tr>
                                                                        <td height="20px" align="center" colspan="9">
                                                                            <div id="qreviewReport" style="display:none" class="error" ><b>Loading Please Wait.....</b></div>
                                                                        </td>
                                                                        </tr>

                                                                        <tr>
                                                                        <td>
                                                                            <div class="rcorners2" id="QReviewDashBoardByStatusPie" style="display:none;width: 700px;" align="center">
                                                                                <div id="QReviewDashBoardByStatusPieChart" style="width: 500px; height: 250px;"></div>
                                                                            </div>
                                                                        </td>

                                                                        </tr>

                                                                        <%--  <tr id="practiceStackedGraphIdTr">
                                                                        <td colspan="2">
                                                                            <div class="rcorners2" id="QReviewPracticeVsStatusStack" style="display:none;width: 692px;margin-left:10px;margin-top: 10px;" >
                                                                                <div id="PracticeVsStatusStackChart" style="width: 550px; height: 350px;"></div>
                                                                            </div>
                                                                        </td>
</tr>
                                                                        <tr id="QuarterStackedGraphIdTr">
                                                                        <td colspan="2">
                                                                            <div class="rcorners2" id="QReviewClosedVsQuarterStatusStack" style="display:none;width: 692px;margin-left:10px;margin-top: 10px;" >
                                                                                <div id="ClosedVsQuarterStackChart" style="width: 550px; height: 350px;"></div>
                                                                            </div>
                                                                        </td>
                                                                        </tr>  --%>


                                                                    </table>    
                                                                </td>
                                                                </tr>


                                                                <!-- new graph details start -->
                                                                <tr>
                                                                <td height="20px" align="center" colspan="9">
                                                                    <div id="projectReporssst" style="display:none" class="error" ><b>Loading Please Wait.....</b></div>
                                                                </td>
                                                                </tr>
                                                                <tr>
                                                                <td colspan="4">
                                                                    <table id="tblQReviewDashBoard" align='center' cellpadding='1' cellspacing='1' border='0' class="gridTable" width='800'>
                                                                        <COLGROUP ALIGN="left" >
                                                                            <COL width="7%">
                                                                                <COL width="25%">
                                                                                    <COL width="7%">
                                                                                        <COL width="7%">
                                                                                            <COL width="7%">
                                                                                                <COL width="7%">
                                                                                                    <COL width="7%"> 
                                                                                                        <COL width="7%">
                                                                                                        

                                                                                                                </table>  



                                                                                                                </td>
                                                                                                                </tr>
                                                                                                                <!-- new graph details end -->

                                                                                                                </table>    
                                                                                                                </td>
                                                                                                                </tr> 
                                                                                                                </table>
                                                                                                            </s:form>

                                                                                                            </td>
                                                                                                            </tr>
                                                                                                            </table>

                                                                                                            </div>

                                                                                                            </td>
                                                                                                            </tr>


                                                                                                            <%-- Tasks DashBoard end --%> 

                                                                                                            <%-- Practice Vs Status start --%>
                                                                                                            <tr>
                                                                                                            <td class="homePortlet" valign="top">
                                                                                                                <div class="portletTitleBar">
                                                                                                                    <div class="portletTitleLeft">Q-Reviews Consoldated Report By Practice Vs Status</div>
                                                                                                                    <div class="portletIcons">
                                                                                                                        <a href="javascript:animatedcollapse.hide('QReviewPracticeVsStatusDiv')" title="Minimize">
                                                                                                                            <img src="../../includes/images/portal/title_minimize.gif" alt="Minimize"/></a>
                                                                                                                        <a href="javascript:animatedcollapse.show('QReviewPracticeVsStatusDiv')" title="Maximize">
                                                                                                                            <img src="../../includes/images/portal/title_maximize.gif" alt="Maximize"/>
                                                                                                                        </a>
                                                                                                                    </div>
                                                                                                                    <div class="clear"></div>
                                                                                                                </div>

                                                                                                                <div id="QReviewPracticeVsStatusDiv" style="background-color:#ffffff">
                                                                                                                    <table cellpadding="0" cellspacing="0" border="0" width="100%"> 
                                                                                                                        <tr>
                                                                                                                        <td width="80%" valign="top" align="center">
                                                                                                                            <s:form action="" theme="simple" name="QReviewDashBoard" id="QReviewDashBoard">  

                                                                                                                                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                                                                                    <tr>
                                                                                                                                    <td>
                                                                                                                                        <table border="0" align="center" cellpadding="0" cellspacing="0" width="700;">
                                                                                                                                            <br>
                                                                                                                                            <tr>

                                                                                                                                            <td class="fieldLabel"> Year:</td>
                                                                                                                                            <td><s:textfield name="year1" id="year1" value="%{year}" cssClass="inputTextBlue" onkeyup="isNumericYearQreview(this);" maxLength="4"/></td>
                                                                                                                                            <td class="fieldLabel">Quarter:</td>
                                                                                                                                            <td >

                                                                                                                                                <s:select headerKey="" headerValue="--Please Select--" id="quarterly1"  name="quarterly1" list="#@java.util.LinkedHashMap@{'Q1':'Q1','Q2':'Q2','Q3':'Q3','Q4':'Q4'}" cssClass="inputSelect"  disabled="False"/>


                                                                                                                                                <input  type="button" style="margin-left: 20px;" value="Search" class="buttonBg" onclick="getPracticeVsStatusStackedChart();"/>
                                                                                                                                            </td>

                                                                                                                                            </tr>



                                                                                                                                        </table>
                                                                                                                                    </td>
                                                                                                                                    </tr>
                                                                                                                                    <tr>
                                                                                                                                    <td>
                                                                                                                                        <table border="0" align="center" cellpadding="0" cellspacing="0" >

                                                                                                                                            <tr>
                                                                                                                                            <td>

                                                                                                                                                <table align="center" cellpadding="2" border="0" cellspacing="1" width="50%" >
                                                                                                                                                    <tr>
                                                                                                                                                    <td height="20px" align="center" colspan="9">
                                                                                                                                                        <div id="qreviewReport1" style="display:none" class="error" ><b>Loading Please Wait.....</b></div>
                                                                                                                                                    </td>
                                                                                                                                                    </tr>



                                                                                                                                                    <tr id="practiceStackedGraphIdTr">
                                                                                                                                                    <td colspan="2">
                                                                                                                                                        <div class="rcorners2" id="QReviewPracticeVsStatusStack" style="display:none;width: 692px;margin-left:10px;margin-top: 10px;" >
                                                                                                                                                            <div id="PracticeVsStatusStackChart" style="width: 550px; height: 350px;"></div>
                                                                                                                                                        </div>
                                                                                                                                                    </td>
                                                                                                                                                    </tr>



                                                                                                                                                </table>    
                                                                                                                                            </td>
                                                                                                                                            </tr>


                                                                                                                                            <!-- new graph details start -->
                                                                                                                                            <tr>
                                                                                                                                            <td height="20px" align="center" colspan="9">
                                                                                                                                                <div id="projectReporssst1" style="display:none" class="error" ><b>Loading Please Wait.....</b></div>
                                                                                                                                            </td>
                                                                                                                                            </tr>
                                                                                                                                            <tr>
                                                                                                                                            <td colspan="4">
                                                                                                                                                <table id="tblQReviewDashBoard1" align='center' cellpadding='1' cellspacing='1' border='0' class="gridTable" width='800'>
                                                                                                                                                    <COLGROUP ALIGN="left" >
                                                                                                                                                        <COL width="7%">
                                                                                                                                                            <COL width="25%">
                                                                                                                                                                <COL width="7%">
                                                                                                                                                                    <COL width="7%">
                                                                                                                                                                        <COL width="7%">
                                                                                                                                                                            <COL width="7%">
                                                                                                                                                                                <COL width="7%"> 
                                                                                                                                                                                    <COL width="7%">
                                                                                                                                                                                        <COL width="7%">

                                                                                                                                                                                            </table>  



                                                                                                                                                                                            </td>
                                                                                                                                                                                            </tr>
                                                                                                                                                                                            <!-- new graph details end -->

                                                                                                                                                                                            </table>    
                                                                                                                                                                                            </td>
                                                                                                                                                                                            </tr> 
                                                                                                                                                                                            </table>
                                                                                                                                                                                        </s:form>

                                                                                                                                                                                        </td>
                                                                                                                                                                                        </tr>
                                                                                                                                                                                        </table>

                                                                                                                                                                                        </div>

                                                                                                                                                                                        </td>
                                                                                                                                                                                        </tr>
                                                                                                                                                                                        <%-- Practice vs Stataus End--%>


                                                                                                                                                                                        <%-- Quarter Vs Status start --%>
                                                                                                                                                                                        <tr>
                                                                                                                                                                                        <td class="homePortlet" valign="top">
                                                                                                                                                                                            <div class="portletTitleBar">
                                                                                                                                                                                                <div class="portletTitleLeft">Q-Reviews Operation Closed Report By Quarter Vs Status</div>
                                                                                                                                                                                                <div class="portletIcons">
                                                                                                                                                                                                    <a href="javascript:animatedcollapse.hide('QReviewQuarterVsStatusDiv')" title="Minimize">
                                                                                                                                                                                                        <img src="../../includes/images/portal/title_minimize.gif" alt="Minimize"/></a>
                                                                                                                                                                                                    <a href="javascript:animatedcollapse.show('QReviewQuarterVsStatusDiv')" title="Maximize">
                                                                                                                                                                                                        <img src="../../includes/images/portal/title_maximize.gif" alt="Maximize"/>
                                                                                                                                                                                                    </a>
                                                                                                                                                                                                </div>
                                                                                                                                                                                                <div class="clear"></div>
                                                                                                                                                                                            </div>

                                                                                                                                                                                            <div id="QReviewQuarterVsStatusDiv" style="background-color:#ffffff">
                                                                                                                                                                                                <table cellpadding="0" cellspacing="0" border="0" width="100%"> 
                                                                                                                                                                                                    <tr>
                                                                                                                                                                                                    <td width="80%" valign="top" align="center">
                                                                                                                                                                                                        <s:form action="" theme="simple" name="QReviewDashBoard" id="QReviewDashBoard">  

                                                                                                                                                                                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                                                                                                                                                        <tr>
                                                                                                                                                                                                        <td>
                                                                                                                                                                                                        <table border="0" align="center" cellpadding="0" cellspacing="0" width="700;">
                                                                                                                                                                                                        <br>

                                                                                                                                                                                                        <tr >

                                                                                                                                                                                                        <td class="fieldLabel" > Year:</td>
                                                                                                                                                                                                        <td> <s:textfield name="year2" id="year2" value="%{year}" cssClass="inputTextBlue" onkeyup="isNumericYearQreview(this);" maxLength="4"/></td>

                                                                                                                                                                                                        <td >



                                                                                                                                                                                                        <input  type="button"  value="Search" class="buttonBg" onclick="getClosedVsQuarterStackedChart();"/>
                                                                                                                                                                                                        </td>

                                                                                                                                                                                                        </tr>



                                                                                                                                                                                                        </table>
                                                                                                                                                                                                        </td>
                                                                                                                                                                                                        </tr>
                                                                                                                                                                                                        <tr>
                                                                                                                                                                                                        <td>
                                                                                                                                                                                                        <table border="0" align="center" cellpadding="0" cellspacing="0" >

                                                                                                                                                                                                        <tr>
                                                                                                                                                                                                        <td>

                                                                                                                                                                                                        <table align="center" cellpadding="2" border="0" cellspacing="1" width="50%" >
                                                                                                                                                                                                        <tr>
                                                                                                                                                                                                        <td height="20px" align="center" colspan="9">
                                                                                                                                                                                                        <div id="qreviewReport2" style="display:none" class="error" ><b>Loading Please Wait.....</b></div>
                                                                                                                                                                                                        </td>
                                                                                                                                                                                                        </tr>



                                                                                                                                                                                                        <tr id="QuarterStackedGraphIdTr">
                                                                                                                                                                                                        <td colspan="2">
                                                                                                                                                                                                        <div class="rcorners2" id="QReviewClosedVsQuarterStatusStack" style="display:none;width: 692px;margin-left:10px;margin-top: 10px;" >
                                                                                                                                                                                                        <div id="ClosedVsQuarterStackChart" style="width: 550px; height: 350px;"></div>
                                                                                                                                                                                                        </div>
                                                                                                                                                                                                        </td>
                                                                                                                                                                                                        </tr>



                                                                                                                                                                                                        </table>    
                                                                                                                                                                                                        </td>
                                                                                                                                                                                                        </tr>


                                                                                                                                                                                                        <!-- new graph details start -->
                                                                                                                                                                                                        <tr>
                                                                                                                                                                                                        <td height="20px" align="center" colspan="9">
                                                                                                                                                                                                        <div id="projectReporssst2" style="display:none" class="error" ><b>Loading Please Wait.....</b></div>
                                                                                                                                                                                                        </td>
                                                                                                                                                                                                        </tr>
                                                                                                                                                                                                        <tr>
                                                                                                                                                                                                        <td colspan="4">
                                                                                                                                                                                                        <table id="tblQReviewDashBoard2" align='center' cellpadding='1' cellspacing='1' border='0' class="gridTable" width='800'>
                                                                                                                                                                                                        <COLGROUP ALIGN="left" >
                                                                                                                                                                                                        <COL width="7%">
                                                                                                                                                                                                        <COL width="25%">
                                                                                                                                                                                                        <COL width="7%">
                                                                                                                                                                                                        <COL width="7%">
                                                                                                                                                                                                        <COL width="7%">
                                                                                                                                                                                                        <COL width="7%">
                                                                                                                                                                                                        <COL width="7%"> 
                                                                                                                                                                                                        <COL width="7%">
                                                                                                                                                                                                        <COL width="7%">

                                                                                                                                                                                                        </table>  



                                                                                                                                                                                                        </td>
                                                                                                                                                                                                        </tr>
                                                                                                                                                                                                        <!-- new graph details end -->


                                                                                                                                                                                                        </table>    
                                                                                                                                                                                                        </td>
                                                                                                                                                                                                        </tr> 

                                                                                                                                                                                                        </table>
                                                                                                                                                                                                        </s:form>

                                                                                                                                                                                                        </td>
                                                                                                                                                                                                        </tr>
                                                                                                                                                                                                        </table>

                                                                                                                                                                                                        </div>

                                                                                                                                                                                                        </td>
                                                                                                                                                                                                        </tr>
                                                                                                                                                                                                        <%-- Quarter vs Stataus End--%>

                                                                                                                                                                                                        <%-- Q-review Manager DashBoard start --%>

                                                                                                                                                                                                        <tr>
                                                                                                                                                                                                        <td class="homePortlet" valign="top">
                                                                                                                                                                                                        <div class="portletTitleBar">
                                                                                                                                                                                                        <div class="portletTitleLeft">Q-Reviews Pending Reports </div>
                                                                                                                                                                                                        <div class="portletIcons">
                                                                                                                                                                                                        <a href="javascript:animatedcollapse.hide('QReviewManagerDashBoardDiv')" title="Minimize">
                                                                                                                                                                                                        <img src="../../includes/images/portal/title_minimize.gif" alt="Minimize"/></a>
                                                                                                                                                                                                        <a href="javascript:animatedcollapse.show('QReviewManagerDashBoardDiv')" title="Maximize">
                                                                                                                                                                                                        <img src="../../includes/images/portal/title_maximize.gif" alt="Maximize"/>
                                                                                                                                                                                                        </a>
                                                                                                                                                                                                        </div>
                                                                                                                                                                                                        <div class="clear"></div>
                                                                                                                                                                                                        </div>

                                                                                                                                                                                                        <div id="QReviewManagerDashBoardDiv" style="background-color:#ffffff">
                                                                                                                                                                                                        <table cellpadding="0" cellspacing="0" border="0" width="100%"> 
                                                                                                                                                                                                        <tr>
                                                                                                                                                                                                        <td width="80%" valign="top" align="center">
                                                                                                                                                                                                        <s:form action="" theme="simple" name="QReviewManagerDashBoard" id="QReviewManagerDashBoard">  

                                                                                                                                                                                                          <s:hidden name="backToFlag" id="backToFlag" value="%{backToFlag}"/>
                                                                                                                                                                                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                                                                                                                                                        <tr>
                                                                                                                                                                                                        <td>
                                                                                                                                                                                                        <table border="0" align="center" cellpadding="0" cellspacing="0" width="700;">
                                                                                                                                                                                                        <br>
                                                                                                                                                                                                        <tr>

                                                                                                                                                                                                        <td class="fieldLabel"> Year:</td>
                                                                                                                                                                                                        <td><s:textfield name="yearForManagersReport" id="yearForManagersReport" value="%{yearForManagersReport}" cssClass="inputTextBlue" onkeyup="isNumericYearQreview(this);" maxLength="4"/></td>
                                                                                                                                                                                                        <td class="fieldLabel">Quarter:<FONT color="red"><em>* </em></FONT></td>
                                                                                                                                                                                                        <td >

                                                                                                                                                                                                        <s:select headerKey="" headerValue="--Please Select--" id="quarterlyForManagersReport"  name="quarterlyForManagersReport" list="#@java.util.LinkedHashMap@{'Q1':'Q1','Q2':'Q2','Q3':'Q3','Q4':'Q4'}" cssClass="inputSelect"  value="%{quarterlyForManagersReport}"  disabled="False"/>


                                                                                                                                                                                                      
                                                                                                                                                                                                        </td>

                                                                                                                                                                                                        </tr>
                                                                                                                                                                                                        <tr>
                                                                                                                                                                                                        <td class="fieldLabel">Country:</td>
                                                                                                                                                                                                        <td >

                                                                                                                                                                                                        <s:select headerKey="-1" headerValue="--Please Select--" id="quarterlyLivingCountry"  value="%{country}"   name="quarterlyLivingCountry" list="#@java.util.LinkedHashMap@{'India':'India','USA':'USA'}" cssClass="inputSelect" />
																																																							</td>  <td class="fieldLabel">
																																																				
                                                                                                                                                                                                         <s:checkbox name="isIncludeTeam" id="isIncludeTeam"  value="%{isIncludeTeam}"  theme="simple"/>
                                                                                                                                                                                                       
                                                                                                                                                                                                        :</td><td class="fieldLabel" style="text-align: left;"> Include&nbsp;Team&nbsp;Members</td> 
                                                                                            
												                                                                                            
												                                                                                                                                                                                                        </tr>
												                                                                                                                                                                                                       <tr>
												                                                                                                                                                                                                                      <td class="fieldLabel" >Operation&nbsp;Contact&nbsp;:</td>                           
                    
                                                                                                                                                                                                                                                                            <td>
                      
               
                                                                                                                                                                                                                                     <s:select label="Select Point of Contact" 
                                                                                                                                                                                                                                           name="opsContactId" 
                                                                                                                                                                                                                                          id="opsContactId"
                                                                                                                                                                                                                                                   headerKey="1"
                                                                                                                                                                                                                                                 headerValue="All"
                                                                                                                                                                                                                                           list="opsContactIdMap" 
                                                                                                                                                                                                                                               cssClass="inputSelect" 
                                                                                                                                                                                                                                                  value="%{opsContactId}" />
                                                                                                                                                                                                                                                                 </td>   
                                                                                                                                                                                                                                                                 
                                                                                                                                                                                                                            
                                                                                                                                                                                          <td class="fieldLabel" >Report BasedOn :</td><td>
                                                                                                                                                                                               <s:select cssClass="inputSelect" list="#@java.util.LinkedHashMap@{'Lead':'Lead','Manager':'Manager'}" name="ReportsToField" id="ReportsToField" headerValue="All" headerKey="All" value="%{resourceName}"/></td>                                      
                                                                                                                                                                                                                                                           </tr>
                                                                                                                                                                                                                                                          
                                                                                                                                                                                                                                                            <tr>    
                                                                                                                                                                                                    <td class="fieldLabel">Practice&nbsp;Name&nbsp;:</td>
                                                                                                                                                                                                <td><s:select name="practiceName"  id="practiceName" value="%{practiceId}" headerKey="-1" headerValue="All" list="empPracticeList" cssClass="inputSelect"/></td>
                                                                                                                                                                                                                                                    
                                                                                                                                                                                                                                                    
                                                                                                                                                                                                                                                    <td></td>
                                                                                                                                                                                                                                                  
												                                                                                                                                                                                                        <td colspan="4" align="right">
												                                                                                                                                                                                                          <input  type="button" style="margin-right: 120px;" value="Search" class="buttonBg" onclick="getQReviewListByManagers();" /></td>
												                                                                                                                                                                                                       
												                                                                                                                                                                                                       </tr>




                                                                                                                                                                                                        </table>
                                                                                                                                                                                                        </td>
                                                                                                                                                                                                        </tr>
                                                                                                                                                                                                        <tr>
                                                                                                                                                                                                        <td>
                                                                                                                                                                                                        <table border="0" align="center" cellpadding="0" cellspacing="0" >

                                                                                                                                                                                                        <!-- new graph details start -->
                                                                                                                                                                                                        <tr>
                                                                                                                                                                                                        <td height="20px" align="center" colspan="9">
                                                                                                                                                                                                        <div id="loadingForManagersReport" style="display:none" class="error" ><b>Loading Please Wait.....</b></div>
                                                                                                                                                                                                        </td>
                                                                                                                                                                                                        </tr>
                                                                                                                                                                                                        <tr>
                                                                                                                                                                                                        <td colspan="4"> <div id="parent">
                                                                                                                                                                                                        <table id="tblQReviewManagerDashBoard" align='center' cellpadding='1' cellspacing='1' border='0' class="gridTable" width='800'>

                                                                                                                                                                                                        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/wz_tooltip.js"/>"></script>
                                                                                                                                                                                                        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/jquery-2.1.3.js"/>"></script>
                                                                                                                                                                                                        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/tableHeadFixer.js"/>"></script>    
                                                                                                                                                                                                        </table>

                                                                                                                                                                                                        </div>     <div id="button_pageNation1"></div>





                                                                                                                                                                                                        </td>
                                                                                                                                                                                                        </tr>
                                                                                                                                                                                                        <!-- new graph details end -->

                                                                                                                                                                                                        </table>    
                                                                                                                                                                                                        </td>
                                                                                                                                                                                                        </tr> 
                                                                                                                                                                                                        </table>
                                                                                                                                                                                                        </s:form>

                                                                                                                                                                                                        </td>
                                                                                                                                                                                                        </tr>
                                                                                                                                                                                                        </table>

                                                                                                                                                                                                        </div>

                                                                                                                                                                                                        </td>
                                                                                                                                                                                                        </tr>


                                                                                                                                                                                                        <%-- Q-Review Manager DashBoard end --%> 
                                                                                                                                                                                                        </table>

                                                                                                                                                                                                        <%--     </sx:div>
                                                                                                                                                                                                        </sx:tabbedpanel> --%>
                                                                                                                                                                                                        <!--//END TABBED PANNEL : --> 
                                                                                                                                                                                                        </div>
                                                                                                                                                                                                        </div>
                                                                                                                                                                                                        <script type="text/javascript">

                                                                                                                                                                                                        var countries=new ddtabcontent("qreviewTab")
                                                                                                                                                                                                        countries.setpersist(false)
                                                                                                                                                                                                        countries.setselectedClassTarget("link") //"link" or "linkparent"
                                                                                                                                                                                                        countries.init()

                                                                                                                                                                                                        </script>

                                                                                                                                                                                                        </td>
                                                                                                                                                                                                        <!--//END DATA COLUMN : Coloumn for Screen Content-->
                                                                                                                                                                                                        </tr>
                                                                                                                                                                                                        </table>
                                                                                                                                                                                                        </td>
                                                                                                                                                                                                        </tr>
                                                                                                                                                                                                        <!--//END DATA RECORD : Record for LeftMenu and Body Content-->

                                                                                                                                                                                                        <!--//START FOOTER : Record for Footer Background and Content-->
                                                                                                                                                                                                        <tr class="footerBg">
                                                                                                                                                                                                        <td align="center"><s:include value="/includes/template/Footer.jsp"/></td>
                                                                                                                                                                                                        </tr>
                                                                                                                                                                                                        <!--//END FOOTER : Record for Footer Background and Content-->
                                                                                                                                                                                                        </table>
                                                                                                                                                                                                        <script type="text/javascript">
                                                                                                                                                                                                        var recordPage=10
                                                                                                                                                                                                        function pagerOption(){

                                                                                                                                                                                                        var paginationSize =10;
                                                                                                                                                                                                        if(isNaN(paginationSize))
                                                                                                                                                                                                        {
                       
                                                                                                                                                                                                        }
                                                                                                                                                                                                        recordPage=paginationSize;
                                                                                                                                                                                                        // alert(recordPage)
                                                                                                                                                                                                        $('#tblQReviewDashBoard').tablePaginate({navigateType:'navigator'},recordPage);

                                                                                                                                                                                                        };
                                                                                                                                                                                                            
                                                                                                                                                                                                        function pagerOption1(){

                                                                                                                                                                                                        var paginationSize =10;
                                                                                                                                                                                                        if(isNaN(paginationSize))
                                                                                                                                                                                                        {
                       
                                                                                                                                                                                                        }
                                                                                                                                                                                                        recordPage=paginationSize;
                                                                                                                                                                                                        // alert(recordPage)
                                                                                                                                                                                                        $('#tblQReviewDashBoard1').tablePaginate({navigateType:'navigator'},recordPage);

                                                                                                                                                                                                        };
                                                                                                                                                                                                            
                                                                                                                                                                                                        function pagerOption2(){

                                                                                                                                                                                                        var paginationSize =10;
                                                                                                                                                                                                        if(isNaN(paginationSize))
                                                                                                                                                                                                        {
                       
                                                                                                                                                                                                        }
                                                                                                                                                                                                        recordPage=paginationSize;
                                                                                                                                                                                                        // alert(recordPage)
                                                                                                                                                                                                        $('#tblQReviewDashBoard2').tablePaginate({navigateType:'navigator'},recordPage);

                                                                                                                                                                                                        };
                                                                                                                                                                                                        var recordPage=20;
                                                                                                                                                                                                        function pagerOptionForManagerGrid(){

                                                                                                                                                                                                        var paginationSize =20;
                                                                                                                                                                                                        if(isNaN(paginationSize))
                                                                                                                                                                                                        {
                       
                                                                                                                                                                                                        }
                                                                                                                                                                                                        recordPage=paginationSize;
                                                                                                                                                                                                        // alert(recordPage)
                                                                                                                                                                                                        $('#tblQReviewManagerDashBoard').tablePaginateForQReview({navigateType:'navigator'},recordPage,tblQReviewManagerDashBoard);

                                                                                                                                                                                                        };
                                                                                                              
                                                                                                            
                                                                                                                                                                                                      
                                                                                                                                                                                                        </script>
                                                                                                                                                                                                    <script type="text/JavaScript" src="<s:url value="/includes/javascripts/ajaxPaginationForTaskDashboard.js?version=1.0"/>"></script>                                                                                                                                                                   <!--//END MAIN TABLE : Table for template Structure-->
                                                                                                                                                                                                    <script type="text/JavaScript" src="<s:url value="/includes/javascripts/paginationForQReview.js?version=1.0"/>"></script>                                                                                                                                                                   <!--//END MAIN TABLE : Table for template Structure-->
                                                                                                                                                                                                        <script type="text/javascript">
                                                                                                                                                                                                        $(window).load(function(){
                                                                                                                                                                                                            if(document.getElementById('backToFlag').value==1){
                                                                                                                                                                                                            animatedcollapse.show('QReviewManagerDashBoardDiv');
                                                                                                                                                                                                            getQReviewListByManagers();
                                                                                                                                                                                                        }else{
                                                                                                                                                                                                        animatedcollapse.show('QReviewDashBoardDiv');
                                                                                                                                                                                                        }
                                                                                                                                                                                                        });
                                                                                                                                                                                                        </script>                                                                                                      
                                                                                                                                                                                                        </body>

                                                                                                                                                                                                        </html>

