<%@ page contentType="text/html; charset=UTF-8" errorPage="../exception/ErrorDisplay.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%--<%@ taglib prefix="sx" uri="/struts-dojo-tags" %> --%>
<%@ page import="com.freeware.gridtag.*" %> 
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="com.mss.mirage.util.ConnectionProvider"%>
<%@ page import="com.mss.mirage.util.ApplicationConstants"%>
<%@ taglib uri="/WEB-INF/tlds/datagrid.tld" prefix="grd" %>
<html>
    <head>
        <title>Hubble Organization Portal :: Employee Search</title>

        <link rel="stylesheet" type="text/css" href="<s:url value="/includes/css/mainStyle.css"/>">
        <link rel="stylesheet" type="text/css" href="<s:url value="/includes/css/GridStyle.css"/>">
        <link rel="stylesheet" type="text/css" href="<s:url value="/includes/css/leftMenu.css"/>">
        <link rel="stylesheet" type="text/css" href="<s:url value="/includes/css/tabedPanel.css"/>">
        
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/DBGrid.js"/>"></script>
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/AppConstants.js"/>"></script>
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/tabedPanel.js"/>"></script>
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/CalendarTime.js"/>"></script>
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/reviews/jquery.min.js"/>"></script>
        <script type="text/javascript" src="<s:url value="/includes/javascripts/reviews/jquery.js"/>"></script>   
        <script type="text/javascript" src="<s:url value="/includes/javascripts/StarPerformer.js?version=10.2"/>"></script>   

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
 <link rel="stylesheet" type="text/css" href="<s:url value="/includes/css/new/x0popup.min.css"/>">
   
        <script>

        function doStarPerformersAdd(){
                      document.getElementById('resultMessageAdd').innerHTML = "";
                       document.getElementById('nomineeListMessage').innerHTML = "";
                       var resourceName = document.getElementById("resourceName1").value; 
                       var resourceComments=document.getElementById("resourceComments").value; 
                       var objId=  document.getElementById("Id").value; 
                       var emplId=  document.getElementById("preAssignEmpId1").value;
                       var statusId=  document.getElementById("statusId").value;
                      
                       if(resourceName!=""){
                       	if(resourceComments.trim()!=""){
                       		
                       		
                       		x0p(' Do you want to Add Star Performer Details !', '', 'warning',
                   					function(button) {
                   				
                   						if (button == 'warning') {
                   							result = true;
                   							// alert(button)
                   						}
                   						if (button == 'cancel') {
                   							result = false;
                   						}
                   						if (result) {
                   							
                   							 document.getElementById("addButn").disabled = true;
                   		                        
                   		                        window.location = "doStarPerformersAdd.action?resourceName="+resourceName+"&employeeId="+emplId+"&objectId="+objId+"&statusId="+statusId+"&resourceComments="+resourceComments;
                   		                	   
                                                         }
                   					});
                       		
                       		
                       		
                       		
                              
                       	}else{
                       	    	x0p({
                       		        title:'',
                       		        text:'Please enter<b style="color:red"> Resource Comments </b>',
                        		         icon: '',
                       		        html: true
                       		    });
                       			document.getElementById("resourceComments").focus(); 
                               }
                       }
                       else{
                       	x0p({
               		        title:'',
               		        
               		        text:'Please select<b style="color:red"> Resource Name  </b>',
               		       icon: '',
               		        html: true
               		    });
               			document.getElementById("resourceName").focus();
                          
                       }}
        </script>

        <s:include value="/includes/template/headerScript.html"/>

    </head>
    <body class="bodyGeneral">

        <%!
            /* Declarations */
            Connection connection;
            String queryString;
            String strTmp;
            String strSortCol;
            String strSortOrd;
            String submittedFrom;
            String searchSubmitValue;
            int intSortOrd = 0;
            int intCurr;
            boolean blnSortAsc = true;
        %>

        <!--//START MAIN TABLE : Table for template Structure-->
        <table class="templateTableLogin" align="center" cellpadding="0" cellspacing="0">

            <!--//START HEADER : Record for Header Background and Mirage Logo-->
            <tr class="headerBg">
            <td valign="top">
                <s:include value="/includes/template/Header.jsp"/>                    
            </td>
        </tr>
        <!--//END HEADER : Record for Header Background and Mirage Logo-->


        <!--//START DATA RECORD : Record for LeftMenu and Screen Content-->
        <tr>
        <td>
            <table class="innerTableLogin" cellpadding="0" cellspacing="0">
                <tr>

                    <!--//START DATA COLUMN : Coloumn for LeftMenu-->
                <td width="150px;" class="leftMenuBgColor" valign="top">
                    <s:include value="/includes/template/LeftMenu.jsp"/>
                </td>

                <!--//END DATA COLUMN : Coloumn for LeftMenu-->

                <!--//START DATA COLUMN : Coloumn for Screen Content-->
                <td width="850px" class="cellBorder" valign="top" style="padding: 5px 5px 5px 5px;">
                    <!--//START TABBED PANNEL : -->
                    <ul id="accountTabs" class="shadetabs" >
                        <li ><a href="#"  class="selected" rel="employeeSearchTab1" >Star Performers Add </a></li>

                    </ul>

                    <%--<sx:tabbedpanel id="employeeSearchPannel" cssStyle="width: 840px; height: 500px;padding: 5px 5px 5px 5px;" doLayout="true"> --%>
                    <div  style="border:1px solid gray; width:840px;height: 500px; overflow:auto; margin-bottom: 1em;">   

                        <div id="employeeSearchTab1" class="tabcontent"  >

                            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                <tr align="right">
                                <td class="headerText">
                                    <img alt="Home" 
                                         src="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/spacer.gif" 
                                         width="100%" 
                                         height="13px" 
                                         border="0"/>
                                    <%if (request.getAttribute(ApplicationConstants.RESULT_MSG) != null) {
                                            out.println(request.getAttribute(ApplicationConstants.RESULT_MSG));
                                        }%>  
                                </td>
                                </tr>

                                <tr>
                                <td>

                                    <s:form name="starPerformance" id="starPerformance" action="" enctype="multipart/form-data" theme="simple">
                                        <a href="<s:url action="../employee/starPerformance.action"></s:url>" style="align:center;">

                                                <img alt="Back to List"
                                                     src="<s:url value="/includes/images/backToList_66x19.gif"/>" 
                                                width="66px" 
                                                height="19px"
                                                border="0" align="right"></a>
                                        <table cellpadding="1" cellspacing="1" border="0" width="650px" align="center">
                                            <tr>
                                            <td colspan="4">
                                                <div id="loadAdd" style="color: green;display: none;">Loading..</div>
                                                <div id="resultMessageAdd"></div>
                                            </td>


                                            </tr>    
                                            <s:hidden name="preAssignEmpId1" id="preAssignEmpId1"/>  
                                            <s:hidden name="Id" id="Id"/>  
                                            <s:hidden name="count" id="count"/>  
                                            <s:hidden name="isUserManager" id="isUserManager" value="%{#session.isUserManager}"/>  
                                            <s:hidden name="starPerformerAccess" id="starPerformerAccess" value="%{#session.starPerformerAccessForHrMr}"/>  
                                            <s:hidden name="statusId" id="statusId" value="%{statusId}"/>  
                                            <s:hidden name="starPerformerStatus" id="starPerformerStatus" value="%{#session.starPerformerStatus}"/>  
                                           <s:if test="%{statusId <= 2 && #session.sessionIsOperationContactTeamFlag == 1}">
                                                <tr id="resoureceFld">        
                                                <td class="fieldLabel">Resource&nbsp;Name&nbsp;:</td>
                                                <td ><s:textfield name="resourceName1" id="resourceName1" onkeyup="EmployeeForStarPerformer();"  cssClass="inputTextBlue"  theme="simple" readonly="false"/>
                                                    <div id="authorEmpValidationMessage1" style="position: absolute; overflow:hidden;"></div>  

                                                    </tr>  
                                                    
                                                     <tr id="resoureceCommentsTr">        
                                                <td class="fieldLabel">Comments&nbsp;:</td>
                                                <td><s:textarea name="resourceComments" id="resourceComments"   cssClass="inputTextArea"  theme="simple" readonly="false" cols="40" rows="4" onchange="return fieldLengthValidator(this);"
                                                onfocus="countResourceComments(this,'commentsCount');" onkeyup="countResourceComments(this,'commentsCount');"/>
                                                   </td>
                                                   <td><lable style="color:green;margin-left:-15vw;" id="commentsCount"></lable></td>
                                                    </tr> 

                                                <tr id="resoureceBtn">
                                                <td colspan="4" align="right">

                                                    <input type="button" class="buttonBg" id="addButn" align="right"  value="Add" onclick="doStarPerformersAdd();" />                      
                                                </td></tr>
                                            </s:if>


                                    </td>




                                    </tr>
                                </table>
                            </s:form>
                            </td>

                            </tr>

                            <tr><td colspan="6" align="center"><div id="reqloadMessageNomineeList" style="display: none"><font color="red" size="2px">Loading Please wait.. </font></div>
                                <div id="nomineeListMessage"></div>
                            </td></tr>
                            <tr>
                            <td colspan="6" >

                                <div id="StarPerformerNomineeListDiv">

                                    <table id="tblStarPerformerNomineeList" cellpadding='1' cellspacing='1' border='0' class="gridTable" width='100%' align="center">
                                    
                                          
                                                                                </table>
                                                                                <br>

                                                                                </div>
                                                                                </td>
                                                                                </tr>



                                                                                </table>

                                                                                <!-- End Overlay -->
                                                                                <!-- Start Special Centered Box -->

                                                                                <%-- </sx:div > --%>
                                                                                </div>

                                                                                <!--//END TAB : -->
                                                                                <%-- </sx:tabbedpanel> --%>
                                                                                </div>
                                                                                <script type="text/javascript">

                                                                                    var countries=new ddtabcontent("accountTabs")
                                                                                    countries.setpersist(false)
                                                                                    countries.setselectedClassTarget("link") //"link" or "linkparent"
                                                                                    countries.init()

                                                                                </script>
                                                                                <!--//END TABBED PANNEL : -->
                                                                                </td>
                                                                                <!--//END DATA COLUMN : Coloumn for Screen Content-->
                                                                                </tr>
                                                                                </table>
                                                                                </td>
                                                                                </tr>
                                                                                <!--//END DATA RECORD : Record for LeftMenu and Body Content-->

                                                                                <!--//START FOOTER : Record for Footer Background and Content-->
                                                                                <tr class="footerBg">
                                                                                <td align="center"><s:include value="/includes/template/Footer.jsp"/></td>
                                                                                </tr>
                                                                                <tr>

                                                                                <td>

                                                                                    <div style="display: none; position: absolute; top:170px;left:320px;overflow:auto;" id="menu-popup">
                                                                                        <table id="completeTable" border="1" bordercolor="#e5e4f2" style="border: 1px dashed gray;" cellpadding="0" class="cellBorder" cellspacing="0" />
                                                                                    </div>

                                                                                </td>
                                                                                </tr>

                                                                                <!--//END FOOTER : Record for Footer Background and Content-->

                                                                                </table>
                                                                                <!--//END MAIN TABLE : Table for template Structure-->
                                                                                
     <script type="text/javascript">
                                                                                                                                                                $(window).load(function(){
                                                                                                                                                                    dogetNomineesList();
                                                                                                                                                                 

                                                                                                                                                                });
                                                                                                                                                            </script>
                                                                                                                                                             <script type="text/JavaScript" src="<s:url value="/includes/javascripts/payroll/x0popup.min.js"/>"></script>


                                                                                </body>
                                                                                </html>