    <%@ page contentType="text/html; charset=UTF-8" errorPage="../exception/ErrorDisplay.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<%@ page import="com.mss.mirage.util.ApplicationConstants"%>
<html>
    <head>        
        <title>Hubble Organization Portal :: ${title} ListAll</title>
        <%--<sx:head cache="true"/> --%>
        <link rel="stylesheet" type="text/css" href="<s:url value="/includes/css/mainStyle.css"/>">
        <link rel="stylesheet" type="text/css" href="<s:url value="/includes/css/GridStyle.css"/>">
        <link rel="stylesheet" type="text/css" href="<s:url value="/includes/css/leftMenu.css"/>">
        <link rel="stylesheet" type="text/css" href="<s:url value="/includes/css/tabedPanel.css"/>">
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/CalendarTime.js"/>"></script>   
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/employee/DateValidator.js"/>"></script>
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/EmpStandardClientValidations.js?ver=1.2"/>"></script>
    
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/AppConstants.js"/>"></script>
         <script type="text/JavaScript" src="<s:url value="/includes/javascripts/tabedPanel.js"/>"></script>
	    <script type="text/JavaScript" src="<s:url value="/includes/javascripts/employee/empNameSuggestionList.js?ver=1.2"/>"></script>
	   <script type="text/JavaScript" src="<s:url value="/includes/javascripts/EmployeeAjax.js?ver=1.2"/>"></script>
	    <link rel="stylesheet" type="text/css" href="<s:url value="/includes/css/new/x0popup.min.css?version=1.0"/>">
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/payroll/x0popup.min.js"/>"></script>
	    <s:include value="/includes/template/headerScript.html"/>
        
    </head>
    <body class="bodyGeneral" oncontextmenu="return false;">
        
        
        
        <!--//START MAIN TABLE : Table for template Structure-->
        <table class="templateTable1000x580" align="center" cellpadding="0" cellspacing="0">
            
            <!--//START HEADER : Record for Header Background and Mirage Logo-->
            <tr class="headerBg">
                <td valign="top">
                    <s:include value="/includes/template/Header.jsp"/>                    
                </td>
            </tr>
            <!--//END HEADER : Record for Header Background and Mirage Logo-->
             
            
            <!--//START DATA RECORD : Record for LeftMenu and Screen Content-->
            <tr>
                <td>
                    <table class="innerTable1000x515" cellpadding="0" cellspacing="0">
                        <tr>
                            
                            <!--//START DATA COLUMN : Coloumn for LeftMenu-->
                            <td width="150px;" class="leftMenuBgColor" valign="top">
                                <s:include value="/includes/template/LeftMenu.jsp"/>
                                
                            </td>
                            <!--//END DATA COLUMN : Coloumn for LeftMenu-->
                            
                            <!--//START DATA COLUMN : Coloumn for Screen Content-->
                            <td width="850px" class="cellBorder" valign="top" style="padding: 10px">
                                
                                
                                <!--//START TABBED PANNEL : --> 
                                <%--   <sx:tabbedpanel id="accountListPannel" cssStyle="width: 840px; height: 500px;padding-left:10px;padding-top:5px;" doLayout="true"> --%>
                               <ul id="accountTabs" class="shadetabs" >
  
                                      <li ><a href="#"  rel="accountsSearchTab">Transfer Location Details</a></li>
                                     
                                </ul>
                                <div  style="border:1px solid gray; width:840px; height: 650px; overflow:auto; margin-bottom: 1em;">
                                  
                                    
                                     <div id="accountsSearchTab" class="tabcontent">
                                        
                                        <s:form name="employeeForm" action="%{currentAction}" theme="simple" onsubmit="return validField();">
                                            <%
										if (request.getAttribute(ApplicationConstants.RESULT_MSG) != null) {
											out.println(request.getAttribute(ApplicationConstants.RESULT_MSG));
										}
									%>
                                          <table cellpadding="1" cellspacing="1" border="0" width="86%" align="center">
                                                
                                                <div id="resultMessage" ></div>
                                                <s:hidden id="currentAction" value="%{currentAction}" />
                                                 <s:hidden id="myDeptId" value="%{#session.myDeptId}" />
                                                
                                                  <s:hidden id="sessionEmpPractice" value="%{#session.sessionEmpPractice}" />
                                             <s:hidden id="isNextLocationHR" name="isNextLocationHR" value="%{isNextLocationHR}" />
                                            <s:hidden id="id" name="id" value="%{id}" />
                                             <s:hidden id="createdBy" name="createdBy" value="%{createdBy}" />
                                              <s:hidden id="isUserManager" value="%{#session.isUserManager}" />
                                            <s:hidden id="reportedTo" name="reportedTo" value="%{reportedTo}" />
                                            
                                           <s:hidden name="status" value="%{status}" />
                                                 <s:hidden name="fromLocation" value="%{fromLocation}" />
                                                 <s:hidden name="toLocation" value="%{toLocation}" />
                                                  <s:hidden name="itTeamFlag" id="itTeamFlag" value="%{itTeamFlag}" />
                                           
                                            
                                            <tr align="right">
                                            <td class="" colspan="6">
                                            <a href="<s:url action="../employee/getEmpITTransfers.action"></s:url>" class="navigationText">
                                                            <img alt="Back to List"
                                                                 src="<s:url value="/includes/images/backToList_66x19.gif"/>" 
                                                            width="66px" 
                                                            height="19px"
                                                            border="0" align="bottom"></a>
                                                            </td></tr>
                                                           

                                            
                                             <tr>
                                             <td>&nbsp;&nbsp;</td><td>&nbsp;&nbsp;</td></tr>
                                                     <tr>     
                                                     <s:hidden id="flag" value="%{#session.roleName}" />  
                                                     <s:hidden id="isUserManager" value="%{#session.isUserManager}" />
                                                                                     
                                                       <td class="fieldLabel">EmpName <font color="red">*</font>:</td>
                                                            <td><s:textfield name="empName" id="empName" value="%{empName}" autocomplete="off" cssClass="inputTextBlue" onkeyup="getEmployeeNames();"/>
                                                            <span id="empNoSpan"></span><label id="rejectedId"></label>
                                                            <div style="margin:0px 25px 0px -20px;" class="fieldLabel" id="validationMessage"></div>

                                                                                                <div style="display: none; position: absolute; overflow:auto; z-index: 500;" id="menu-popup">
                                                                                                    <table id="completeTable" border="1" bordercolor="#e5e4f2" style="border: 1px dashed gray;" cellpadding="0" class="cellBorder" cellspacing="0" ></table>
                                                                                                </div>
                                                                                                <s:hidden id="userId" name="userId"/><s:hidden id="empId" name="empId"/></td> 
                                                         <s:hidden name="departmentId" id="departmentId" cssClass="inputTextBlue" value="%{departmentId}"/>
                                                         <s:hidden name="titleId" id="titleId" cssClass="inputTextBlue" value="%{titleId}"/>
                                                         <s:hidden id="isAdminAccess" value="%{#session.isAdminAccess}" />
                                                        <td></td>
                                                    </tr>
                                                   <%-- <tr>
                                                    <td class="fieldLabel">Department</td>
                                                      <td><s:select list="departmentIdList" name="departmentId" id="departmentId" headerKey="" headerValue="--Select--" cssClass="inputSelect" value="%{departmentId}" onchange="return getEmpTitleDataV1();" /></td>
                                                      
                                                         <s:hidden name="departmentId" id="departmentId" cssClass="inputTextBlue" value="%{departmentId}"/>
                                                         <s:hidden name="titleId" id="titleId" cssClass="inputTextBlue" value="%{titleId}"/>
                                                         
                                                    <td class="fieldLabel">Title</td>
                                                   <td><s:select list="titleIdList" name="titleId" id="titleId" headerKey="" headerValue="--Select--" cssClass="inputSelect" value="%{titleId}" /></td>
                                                       
                                                        
                                                   
                                                    </tr>--%>
                                                    
                                                    <tr>   
                                                      <td  class="fieldLabel">Status<font color="red">*</font>:</td>
                                                        <td><s:select list="#@java.util.LinkedHashMap@{'Initiated':'Initiated','InProgress':'In Progress','OnHold':'On Hold','Completed':'Completed'}" name="status" id="status" headerKey="" headerValue="--Select--" cssClass="inputSelect" value="%{status}" /></td>
                                                                                                   
                                                       <td class="fieldLabel">Initiated&nbsp;On&nbsp;<font color="red">*</font>:</td>
                                                     
                                                     <td><s:textfield name="initiatedOn" id="initiatedOn" cssClass="inputTextBlueSmall" value="%{initiatedOn}" onKeyPress="return selectFromCal(this);" />
                                                     
                                                     
                                                      </td> 
                                                         
                                                      </tr>
                                                     <tr>
                                                     <td class="fieldLabel">From&nbsp;Location&nbsp;<font color="red">*</font>:</td>
                                                       <td><s:select list="empLocationMap" name="fromLocation" id="fromLocation" headerKey="" headerValue="--Select--" cssClass="inputSelect" value="%{fromLocation}" onchange="return getDues(this.value);loadExpenses(this.value);" onblur="return getLocations();"  /></td>
                                                       <td class="fieldLabel">To&nbsp;Location&nbsp;<font color="red">*</font>:</td>
                                                      <td><s:select list="empLocationMap" name="toLocation" id="toLocation" headerKey="" headerValue="--Select--" cssClass="inputSelect" value="%{toLocation}" onblur="return getLocations();" onchange="return getNeedTransport(this.value);"/></td>
                                                     
                                                    </tr>
                                                     <tr>                                                    
                                                       <td class="fieldLabel">Reason&nbsp;For&nbsp;Transfer&nbsp;:</td>
                                                            <td colspan="4"><s:textarea name="reasonsForTransfer" id="reasonsForTransfer" cols="100" rows="2" value="%{reasonsForTransfer}" cssClass="inputTextarea" onchange="fieldLengthValidator(this);" />  </td>
                                                            
                                                      </tr>
                                                       
                                                      
                                                      <tr >
                                                      <td class="fieldLabel">Clearance&nbsp;From&nbsp;ITTeam&nbsp;<font color="red">*</font>:</td>
                                                       <td><s:select list="#@java.util.LinkedHashMap@{'Approved':'Approved','Rejected':'Rejected','OnHold':'OnHold'}" name="itTeamStatus" id="itTeamStatus" headerKey="" headerValue="--Select--" cssClass="inputSelect" value="%{itTeamStatus}" /></td>
                                                       <%--  <s:textfield name="itTeamStatus" value="%{itTeamStatus}" />  --%>
                                                       <td></td>
                                                       <td></td>
                                                      </tr>
                                                      <tr >                                                    
                                                       <td class="fieldLabel">IT&nbsp;Team&nbsp;Comments&nbsp;:</td>
                                                       <td colspan="4"><s:textarea name="itTeamComments" id="itTeamComments" cols="100" rows="2" value="%{itTeamComments}" cssClass="inputTextarea" onchange="fieldLengthValidator(this);" />  </td>
                                                          
                                                      </tr>
                                                      
          
		                                              <tr>
                                                      <td>&nbsp;&nbsp;</td><td align="right" colspan="3">
                                                      
                                                   
                                                      <s:submit id="updateButton" value="Update" cssClass="buttonBg" />
                                                    
                                                     
                                                      </td>
                                                      </tr>
                                                    
                                            </table>
                                      
                                        </s:form>
                                         
                                        <%--  </sx:div> --%>
                                    </div>
                                    
                                    <!--//END TAB : -->
                                     
                                     
                                    <%--   </sx:tabbedpanel> --%>
                                </div>
                                <script type="text/javascript">

                                var countries=new ddtabcontent("accountTabs")
                                countries.setpersist(false)
                                countries.setselectedClassTarget("link") //"link" or "linkparent"
                                countries.init()

                                </script>
                                <script>

                                $(document).ready(function() {
                                //	alert("hiiiii")
                                	
                                /*	 $('#homeState').attr('disabled', 'true'); 
                                	 $('#occupancyType').attr('disabled', 'true'); 
                                	 $('#dateOfOccupancy').attr('readonly', 'true'); 
                                	 $('#electricalCharges').attr('readonly', 'true');
                                	 $('#cafeteriaFee').attr('readonly', 'true'); 
                                	 $('#roomNo').attr('readonly', 'true'); 
                                	 $('#roomFee').attr('readonly', 'true');
                                	 $('#transportFee').attr('readonly', 'true'); 
                                	 $('#transportLocation').attr('disabled', 'true'); */
                                	 
                                	
                                	 
                                
                                	
                            
	/* For IT Team */

		

		 $('#empName').attr('readonly', 'true'); // mark it as read only
		   // $('#empName').css('background-color' , '#DEDEDE'); // change the background color
		 $('#status').attr('disabled', 'true'); // mark it as read only
	    // $('#status').css('background-color' , '#DEDEDE'); // change the background color
	  $('#initiatedOn').attr('readonly', 'true'); // mark it as read only
	   // $('#initiatedOn').css('background-color' , '#DEDEDE'); // change the background color
	   $('#fromLocation').attr('disabled', 'true'); // mark it as read only
	   // $('#fromLocation').css('background-color' , '#DEDEDE'); // change the background color
	   $('#toLocation').attr('disabled', 'true'); // mark it as read only
	   // $('#toLocation').css('background-color' , '#DEDEDE'); // change the background color
	   $('#reasonsForTransfer').attr('readonly', 'true'); // mark it as read only
	 //   $('#reasonsForTransfer').css('background-color' , '#DEDEDE'); // change the background color
	   // $("#calenderImg").hide();
	   // $("#calenderImg1").hide();
	 //   $("#calenderImg2").hide();
// 	    $("#mcityDues").hide();
// 		$("#mcityDues1").hide();
// 		$("#otherDue").hide();
// 		$("#dueRemarks").hide();
// 		$("#reportedTr").hide();
// 		$("#isReportedTr").hide();
// 		$("#nextLocationHRCommentsDiv").hide();
		  	var isManager = document.getElementById("isUserManager").value; 
                                
	  // alert(document.getElementById("itTeamStatus").value)
	  if(isManager == 1){
		  if(document.getElementById("itTeamStatus").value == "Approved" ||  document.getElementById("status").value == "Completed"){
			  $("#updateButton").hide();
		  }else{
		  $("#updateButton").show();
		  }
	  }else{
		if(document.getElementById("itTeamStatus").value == "OnHold"){
			$("#updateButton").show();
		}else if(document.getElementById("itTeamStatus").value == "Approved" || document.getElementById("itTeamStatus").value == "Rejected"){
			$("#updateButton").hide();
		}
	  }
});
                                
                               
                                </script>
                                <!--//END TABBED PANNEL : -->
                            </td>
                            <!--//END DATA COLUMN : Coloumn for Screen Content-->
                        </tr>
                    </table>
                </td>
            </tr>
            <!--//END DATA RECORD : Record for LeftMenu and Body Content-->
            
            <!--//START FOOTER : Record for Footer Background and Content-->
            <tr class="footerBg">
                <td align="center"><s:include value="/includes/template/Footer.jsp"/></td>
            </tr>
            <!--//END FOOTER : Record for Footer Background and Content-->
             
        </table>
        <!--//END MAIN TABLE : Table for template Structure-->
        
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/jquery.min.js"/>"></script>
    <script type="text/javascript" src="<s:url value="/includes/javascripts/reviews/jquery.js"/>"></script> 
    
        
    </body>
    
</html>