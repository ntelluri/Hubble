<%-- 
    Document   : LunchBoxData
    Created on : Mar 4, 2019, 11:05:14 PM
    Author     : miracle
--%>



<%@ page contentType="text/html; charset=UTF-8"
	errorPage="../exception/ErrorDisplay.jsp"%>

<%@ taglib prefix="s" uri="/struts-tags"%>
<%--<%@ taglib prefix="sx" uri="/struts-dojo-tags" %> --%>
<%@ page import="com.freeware.gridtag.*"%>
<%@ page import="java.sql.Connection"%>
<%@ page import="java.sql.SQLException"%>
<%@ page import="com.mss.mirage.util.ConnectionProvider"%>
<%@ page import="com.mss.mirage.util.ApplicationConstants"%>
<%@ taglib uri="/WEB-INF/tlds/datagrid.tld" prefix="grd"%>
<%@ page import="javax.servlet.http.HttpServletRequest"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Hubble Organization Portal :: Lunch Box Data</title>

<link rel="stylesheet" type="text/css"
	href="<s:url value="/includes/css/mainStyle.css"/>">
<link rel="stylesheet" type="text/css"
	href="<s:url value="/includes/css/GridStyle.css"/>">

<link rel="stylesheet" type="text/css"
	href="<s:url value="/includes/css/tabedPanel.css"/>">

<link rel="stylesheet" type="text/css"
	href="<s:url value="/includes/css/leftMenu.css"/>">
	
<link rel="stylesheet" type="text/css"
	href="<s:url value="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css"/>">	
<script type="text/JavaScript"
	src="<s:url value="/includes/javascripts/AppConstants.js"/>"></script>
<script type="text/JavaScript"
	src="<s:url value="/includes/javascripts/tabedPanel.js"/>"></script>

<script type="text/JavaScript"
	src="<s:url value="/includes/javascripts/CalendarTime.js"/>"></script>


<script type="text/JavaScript"
	src='<s:url value="/includes/javascripts/GridNavigation.js"/>'></script>

<script type="text/JavaScript"
	src="<s:url value="/includes/javascripts/reviews/jquery.min.js"/>"></script>
<script type="text/javascript"
	src="<s:url value="/includes/javascripts/reviews/jquery.js"/>"></script>

<script type="text/JavaScript"
	src="<s:url value="/includes/javascripts/EmployeeAjax.js?version=8.1"/>"></script>

<s:include value="/includes/template/headerScript.html" />
</head>
<body class="bodyGeneral" oncontextmenu="return false;">


	<%
		String contextPath = request.getContextPath();
	%>
	<!--//START MAIN TABLE : Table for template Structure-->
	<table class="templateTable1000x580" align="center" cellpadding="0"
		cellspacing="0">

		<!--//START HEADER : Record for Header Background and Mirage Logo-->
		<tr class="headerBg">
			<td valign="top"><s:include
					value="/includes/template/Header.jsp" /></td>
		</tr>
		<!--//END HEADER : Record for Header Background and Mirage Logo-->

		<!--//START DATA RECORD : Record for LeftMenu and Screen Content-->
		<tr>
			<td>
				<table class="innerTable1000x515" cellpadding="0" cellspacing="0">
					<tr>
						<!--//START DATA COLUMN : Coloumn for LeftMenu-->
						<td width="150px;" class="leftMenuBgColor" valign="top"><s:include
								value="/includes/template/LeftMenu.jsp" /></td>
						<!--//START DATA COLUMN : Coloumn for LeftMenu-->

						<!--//START DATA COLUMN : Coloumn for Screen Content-->
						<td width="850px" class="cellBorder" valign="top"
							style="padding: 5px 5px;">
							<!--//START TABBED PANNEL : -->


							<ul id="lunchBoxTabs" class="shadetabs">
								<li><a href="#" class="selected" rel="issuesTab">Lunch&nbsp;Box&nbsp;Data</a></li>
							</ul>

							<div
								style="border: 1px solid gray; width: 845px; height: 500px; overflow: auto; margin-bottom: 1em;">

								<div id="issuesTab" class="tabcontent">
									<div id="overlay"></div>
									<div id="specialBox">
										<s:form theme="simple" align="center">

											<input type="hidden" name="lunchBoxId" id="lunchBoxId" />

											<table align="center" border="0" cellspacing="0"
												style="width: 100%;">
												<tr>
													<td colspan="2" style="background-color: #288AD1">
														<h3 style="color: darkblue;" align="left">
															<span id="headerLabel"></span>


														</h3>
													</td>
													<td colspan="2" style="background-color: #288AD1"
														align="right"><a href="#"
														onmousedown="closeLunchBoxOverlay()"> <img
															src="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/closeButton.png" />

													</a></td>
												</tr>
												<tr>
													<td colspan="4">
														<div id="load" style="color: green; display: none;">Loading..</div>
														<div id="resultMessage"></div>
													</td>
												</tr>
												<tr>
													<td colspan="4" style="height: 134px;">
														<table style="width: 80%;" align="center">
															<tr>
																<td class="fieldLabel">First&nbsp;Name:</td>
																<td><s:textfield name="overlayfirstName"
																		id="overlayfirstName" cssClass="inputTextBlue"
																		readonly="true" /></td>

																<td class="fieldLabel">Last&nbsp;Name:</td>
																<td><s:textfield name="overlaylastName"
																		id="overlaylastName" cssClass="inputTextBlue"
																		readonly="true" /></td>


																<td class="fieldLabel">Email&nbsp;Id:</td>
																<td><s:textfield name="overlayemail"
																		id="overlayemail" cssClass="inputTextBlue"
																		readonly="true" /></td>


															</tr>
															<tr>


																<td class="fieldLabel">Phone:</td>
																<td><s:textfield name="overlayPhone"
																		id="overlayPhone" cssClass="inputTextBlue"
																		readonly="true" /></td>

																<td class="fieldLabel">Food&nbsp;Preference:</td>
																<td><s:textfield name="overlayfood"
																		id="overlayfood" cssClass="inputTextBlue"
																		readonly="true" /></td>


																<td class="fieldLabel">Company:</td>
																<td><s:textfield name="overlaycompany"
																		id="overlaycompany" cssClass="inputTextBlue"
																		readonly="true" /></td>

															</tr>
															<tr>

																<td class="fieldLabel">Job&nbsp;Title:</td>
																<td><s:textfield name="overlayjob" id="overlayjob"
																		cssClass="inputTextBlue" readonly="true" /></td>

																<%-- <td class="fieldLabel">Location:</td>
																<td><s:textfield name="overlaylocation"
																		id="overlaylocation" cssClass="inputTextBlue"
																		readonly="true" /></td> --%>
																		
																<td class="fieldLabel">Event&nbsp;Location:</td>
																<td><s:textfield name="overlayEventlocation"
																		id="overlayEventlocation" cssClass="inputTextBlue"
																		readonly="true" /></td>
																		
																<td class="fieldLabel">Status:</td>
																<td style="text-align: left; color: green"><span
																	id="overlayStatus"></span></td>		

															</tr>
															



															<tr>
																<td align="right" colspan="6"
																	><input
																	type="button" align="right" id="Approve"
																	value="Approve" class="buttonBg" style="display: none;"
																	onclick="return submitLunchBoxStatus('Approved');" />
																	&nbsp;&nbsp;&nbsp;&nbsp; <input type="button"
																	align="right" id="Reject" value="Reject"
																	class="buttonBg" style="display: none;"
																	onclick="return submitLunchBoxStatus('Rejected');" /></td>
															</tr>

														</table>
													</td>
												</tr>
											</table>
										</s:form>
									</div>

									<table cellpadding="0" cellspacing="0" align="left"
										width="100%">

										<tr>
											<td>
												<table border="0" cellpadding="0" cellspacing="2"
													align="left" width="100%">
													<tr>
														<td class="headerText" colspan="11"><img alt="Home"
															src="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/spacer.gif"
															width="100%" height="13px" border="0"></td>
													</tr>

													<tr>
														<td class="fieldLabel">First&nbsp;Name:</td>
														<td><s:textfield name="fname" id="fname"
																cssClass="inputTextBlue" /></td>
														<td class="fieldLabel">Last&nbsp;Name:</td>
														<td><s:textfield name="lname" id="lname"
																cssClass="inputTextBlue" /></td>

													</tr>
													<tr></tr>
													<tr></tr>
													<tr></tr>
													<tr></tr>
													<tr>
														<td class="fieldLabel">Email:</td>
														<td><s:textfield name="email" id="email"
																cssClass="inputTextBlue" /></td>
														<%-- <td class="fieldLabel">Location:</td>
														<td><s:textfield name="location" id="location"
																cssClass="inputTextBlue" /></td> --%>
																<td class="fieldLabel">Status:</td>
														<td><s:select
																list="#@java.util.LinkedHashMap@{'Registered':'Registered','Approved':'Approved','Rejected':'Rejected'}"
																name="status" id="status" headerValue="All"
																headerKey="" cssClass="inputSelect" /></td>


													</tr>
													<tr></tr>
													<tr></tr>
													<tr></tr>
													<tr>
														
																
														<td class="fieldLabel">Event&nbsp;Location:</td>
														<td><s:select
																list="#@java.util.LinkedHashMap@{'Atlanta':'Atlanta','Cincinnati':'Cincinnati','Georgia':'Georgia','Novi':'Novi'}"
																name="eventLocation" id="eventLocation" headerValue="All"
																headerKey="" cssClass="inputSelect" /></td>		
													</tr>


													<tr></tr>
													<tr></tr>
													<tr></tr>
													<tr></tr>
													<tr>

														<td align="center" colspan="4"
															style="padding-left: 460px;"><input type="button"
															align="right" id="search" value="Search" class="buttonBg"
															onclick="getLunchBoxData();" /></td>
													</tr>
												</table>
											</td>
										</tr>

										<tr align="center">
											<td>
												<div id="addLoadMessage" style="color: red; display: none">Loading
													please wait..</div>
											</td>
										</tr>

										<tr></tr>
										<tr></tr>
										<tr></tr>
										<tr>


											<td style="padding-top: 26px;">
												<table id="lunchboxTable" align='center' cellpadding='1'
													cellspacing='1' border='0' class="gridTable" width='700'>
													<COLGROUP ALIGN="left">
														<COL width="5%">
														<COL width="10%">
														<COL width="10%">
														<COL width="10%">
														<COL width="10%">
														<COL width="10%">
														
												</table>
											</td>
										</tr>
									</table>
								</div>
							</div> <script type="text/javascript">
								var countries = new ddtabcontent("lunchBoxTabs")
								countries.setpersist(false)
								countries.setselectedClassTarget("link") //"link" or "linkparent"
								countries.init()
							</script>
						</td>
					</tr>
				</table>
			</td>
		</tr>

		<!--//START FOOTER : Record for Footer Background and Content-->
		<tr class="footerBg">
			<td align="center"><s:include
					value="/includes/template/Footer.jsp" /></td>
		</tr>

	</table>

	<script>
		$(window).load(function() {
			getLunchBoxData();
		});

		function pagerOption() {
			var recordPage = 10;
			$('#lunchboxTable').tablePaginate({
				navigateType : 'navigator'
			}, recordPage, lunchboxTable);

		}
	</script>
	<script type="text/JavaScript"
		src="<s:url value="/includes/javascripts/pagination.js?ver=2.5"/>"></script>

</body>
</html>

