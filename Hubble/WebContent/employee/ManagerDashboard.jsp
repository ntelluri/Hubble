<%-- 
    Document   : ManagerDashboard
    Created on : Jan 21, 2016, 4:36:51 PM
    Author     : miracle
--%>



<%@page import="java.util.Map"%>
<%@ page contentType="text/html; charset=UTF-8" errorPage="../exception/ErrorDisplay.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags" %> 
<%-- <%@ taglib prefix="sx" uri="/struts-dojo-tags" %> --%>
<%@ page import="com.mss.mirage.util.ApplicationConstants"%>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="com.mss.mirage.util.ConnectionProvider"%>
<html>
    <head>
        <title>Hubble Organization Portal :: DashBoard</title>
        <link rel="stylesheet" type="text/css" href="<s:url value="/includes/css/GridStyle.css"/>">
        <link rel="stylesheet" type="text/css" href="<s:url value="/includes/css/mainStyle.css"/>">
        <link rel="stylesheet" type="text/css" href="<s:url value="/includes/css/tabedPanel.css"/>">
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/animatedcollapse.js"/>"></script>
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/jquery-1.2.2.pack.js"/>"></script>
        <link rel="stylesheet" type="text/css" href="<s:url value="/includes/css/leftMenu.css"/>">
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/CalendarTime.js"/>"></script>
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/AppConstants.js"/>"></script>
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/EnableEnter.js"/>"></script>
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/tabedPanel.js"/>"></script>
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/EmployeeProject.js?ver=2.0"/>"></script>
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/OperationsDashBoardAjax.js?version=13.5"/>"></script>
	<script type="text/JavaScript" src="<s:url value="/includes/javascripts/crm/BDMDashBoard.js?ver=1.1"/>"></script>
	
<script type="text/JavaScript" src="<s:url value="/includes/javascripts/employee/EmployeeDashboard.js?version=2.8"/>"></script>

        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/employee/DateValidator.js"/>"></script>
        <script type="text/JavaScript"
	src="<s:url value="/includes/javascripts/tooltip.js"/>"></script>

<%-- <script type="text/javascript" src="<s:url value="/includes/javascripts/IssueFill.js"/>"></script> --%>
<link rel="stylesheet" type="text/css"
	href="<s:url value="/includes/css/tooltip.css"/>">
	
	
	
	
        <s:include value="/includes/template/headerScript.html"/>
        <script language="JavaScript">
             
            
            animatedcollapse.addDiv('dmProjectsDashboardDiv', 'fade=1;persist=1;group=app');
            animatedcollapse.addDiv('projectDetailsByCustomer', 'fade=1;persist=1;group=app');
            animatedcollapse.addDiv('AvailableEmpList', 'fade=1;persist=1;group=app');
            animatedcollapse.addDiv('OffshoreDeliveryStrengthDashboardDiv','fade=1;persist=1;group=app');
            animatedcollapse.addDiv('CustomerProjectRollINRollOffDiv', 'fade=1;speed=400;persist=1;group=app');
            
            
            animatedcollapse.init();
            
            
            function showResourceTypeDiv(){

            	var state=document.getElementById('state').value;
            	if(state=='OnProject'){
            	document.getElementById('resTypeTr').style.display='';
            	document.getElementById('customerTr').style.display='';
            	}else{
            	document.getElementById('resTypeTr').style.display='none';
            	document.getElementById('customerTr').style.display='none';
            	}

            	} 
           
        </script>
           <style>

            div#overlayProjects {
                display: none;
                z-index: 2;
                background: #000;
                position: fixed;
                width: 100%;
                height: 100%;
                top: 0px;
                left: 0px;
                overflow: auto;
                text-align: center;
            }
            div#specialBoxProject {
                display: none;

                position: absolute;
                z-index: 3;
                margin: 100px auto 0px auto;
                width: 800px; 
                height: auto;
                background: #FFF;
                overflow: auto;
                overflow-y: auto;
                color: #000;
            }
        </style>
    </head>
   <%-- <body  class="bodyGeneral"  oncontextmenu="return false;" onload="defaultDates();"> --%>
    <body  class="bodyGeneral"  oncontextmenu="return false;">

        <%!
            /* Declarations */
            String queryString = null;
            Connection connection;
            Statement stmt;
            ResultSet rs;
            int userCounter = 0;
            int activityCounter = 0;
            int accountCounter = 0;
        %>


        <!--//START MAIN TABLE : Table for template Structure-->
        <table class="templateTable1000x580" align="center" cellpadding="0" cellspacing="0">

            <!--//START HEADER : Record for Header Background and Mirage Logo-->
            <tr class="headerBg">
                <td valign="top">
                    <s:include value="/includes/template/Header.jsp"/>                    
                </td>
            </tr>
            <!--//END HEADER : Record for Header Background and Mirage Logo-->

            <!--//START DATA RECORD : Record for LeftMenu and Screen Content-->
            <tr>
                <td>
                    <table class="innerTable1000x515" cellpadding="0" cellspacing="0">
                        <tr>
                            <!--//START DATA COLUMN : Coloumn for LeftMenu-->
                            <td width="850px;" class="leftMenuBgColor" valign="top"> 
                                <s:include value="/includes/template/LeftMenu.jsp"/>
                            </td>
                            <!--//START DATA COLUMN : Coloumn for LeftMenu-->

                            <!--//START DATA COLUMN : Coloumn for Screen Content-->
                            <td width="850px" class="cellBorder" valign="top" style="padding-left:10px;padding-top:5px;">
                                <!--//START TABBED PANNEL : -->
                                <%--      <sx:tabbedpanel id="resetPasswordPannel" cssStyle="width: 845px; height: 550px;padding-left:10px;padding-top:5px;" doLayout="true" useSelectedTabCookie="true" > 
                                    
                                    <!--//START TAB : -->
                                    <sx:div id="dashBoardTab" label="DashBoard Details" cssStyle="overflow:auto;"> --%>
                                <ul id="accountTabs" class="shadetabs" >
                                    <li><a href="#" rel="accountsListTab" class="selected">&nbsp;DashBoards</a></li>

                                </ul>
                                <div  style="border:1px solid gray; width:840px;height: 550px;overflow:auto; margin-bottom: 1em;">    
                                    <div id="accountsListTab" class="tabcontent" >  
                                      
                                           <!-- Over lay start -->

                                        <div id="overlayProjects"></div>              <!-- Start Overlay -->

                                        <div id="specialBoxProject" style="align: center" >

                                            <div id="addedProjects" style="display: none;align: center">
                                                <table align="center" border="0" cellspacing="0" style="width:100%;">
                                                    <tr>                               
                                                        <td colspan="1" style="background-color: #288AD1" >
                                                            <h3 style="color:darkblue;" align="left">
                                                                <span id="headerLabel"></span>


                                                            </h3></td>
                                                        <td colspan="2" style="background-color: #288AD1" align="right">

                                                            <a href="#" onmousedown="closeProjectMembers()" >
                                                                <img src="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/closeButton.png" /> 

                                                            </a>  

                                                        </td></tr>
                                                    <tr>
                                                        <td colspan="4">
                                                            <div id="load" style="color: green;display: none;">Loading..</div>
                                                            <div id="resultMessage"></div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td>
                                                            <table id="tblProjectMembers" align='center' cellpadding='1' cellspacing='1' border='0' class="gridTable" width='750'>
                                                                <%--   <script type="text/JavaScript" src="<s:url value="/includes/javascripts/wz_tooltip.js"/>"></script> --%>
                                                                <COLGROUP ALIGN="left" >
                                                                    <COL width="20%">
                                                                    <COL width="20%">
                                                                    <COL width="20%">
                                                                    <COL width="20%">
                                                                    <COL width="20%">
                                                            </table> 
                                                        </td>
                                                    </tr>

                                                </table>    

                                            </div>
                                        </div>

                                        <!-- Overlay end -->
                                        
                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                            <tr>
                                                <td height="20px" >

                                                </td>
                                            </tr>


                                          

                                            <!-- PreSAles Projects start start -->

                                            <tr>
                                                <td class="homePortlet" valign="top">
                                                    <div class="portletTitleBar">
                                                        <div class="portletTitleLeft">Projects</div>
                                                        <div class="portletIcons">
                                                            <a href="javascript:animatedcollapse.hide('dmProjectsDashboardDiv')" title="Minimize">
                                                                <img src="../includes/images/portal/title_minimize.gif" alt="Minimize"/></a>
                                                            <a href="javascript:animatedcollapse.show('dmProjectsDashboardDiv')" title="Maximize">
                                                                <img src="../includes/images/portal/title_maximize.gif" alt="Maximize"/>
                                                            </a>
                                                        </div>
                                                        <div class="clear"></div>
                                                    </div>
                                                    <div id="dmProjectsDashboardDiv" style="background-color:#ffffff">


                                                        <table cellpadding="0" cellspacing="0" border="0" width="100%"> 
                                                            <tr>
                                                                <td width="80%" valign="top" align="center">
                                                                    <s:form action="" theme="simple" id="projectDashboard" name="projectDashboard" onsubmit="return compareDates(document.getElementById('startDate').value,document.getElementById('endDate').value);">   

                                                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">

                                                                            <tr>
                                                                                <td>
                                                                                    <table border="0" align="center" cellpadding="0" cellspacing="0" >
                                                                                        <tr><td colspan="4"><br></td></tr>

                                                                                        <tr>   

                                                                                            <td class="fieldLabel">Start Date:</td> <!--value="%{dateWithOutTime}" -->
                                                                                            <td><s:textfield id="projectStartDate" name="projectStartDate" cssClass="inputTextBlue" value="" onchange="checkDates(this);"/> <a href="javascript:cal3.popup();">
                                                                                                    <img src="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/showCalendar.gif" 
                                                                                                         width="20" height="20" border="0"></td>    
                                                                                                    <td class="fieldLabel">End Date</td>
                                                                                                    <td><s:textfield name="projectEndDate" id="projectEndDate" cssClass="inputTextBlue" value="%{dateWithOutTime}" onchange="checkDates(this);"/><a href="javascript:cal4.popup();">
                                                                                                            <img src="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/showCalendar.gif" 
                                                                                                                 width="20" height="20" border="0">  </td>
                                                                                                            </tr>
                                                                                                          <tr>
                                                                                                                <td class="fieldLabel" width="200px" align="right">Team&nbsp;Members: </td>                           
                                                                                                                <td>

                                                                                                                    <s:select name="managerTeamMember" id="managerTeamMember" headerKey="-1" headerValue="--please select--" list="managerTeamMembersList" cssClass="inputSelect" value="%{managerTeamMember}"  />    

                                                                                                                </td>
                                                                                                                <td class="fieldLabel" width="200px" align="right">State: </td>     
                                                                                                                <td>
                                                                                                                    <s:select  list="{'Active','Completed','Terminated','Initiated'}" name="status" id="status" cssClass="inputSelect"  value="%{state}"/>
                                                                                                                </td>
                                                                                                            </tr> 
                                                                                                            <tr>
                                                                                                                <td colspan="3"></td><td>
                                                                                                                    <input type="button" id="ProjectSearchBUtton" class="buttonBg" onclick="getDeliverManagerProjects()" value="Search"/>

                                                                                                                </td>
                                                                                                            </tr>

                                                                                                            <tr>
                                                                                                                <td height="20px" align="center" colspan="4" >
                                                                                                                    <div id="loadProjectMessage" style="display:none" class="error" ><b>Loading Please Wait.....</b></div>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td height="20px" >

                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>

                                                                                                                <td colspan="4" >
                                                                                                            <lable id="noteLableForProject" style="display:none;color:red;font-size: 80%; font-style: italic;">Note :The details are order by  Due date ascending.</lable>
                                                                                                            <table id="tblDMProjects" align="center"  
                                                                                                                   cellpadding='1' cellspacing='1' border='0' class="gridTable" width='750'>
                                                                                                                <COLGROUP ALIGN="left" >
                                                                                                                    <COL width="5%">
                                                                                                                    <COL width="15%">
                                                                                                                    <COL width="5%">
                                                                                                                    <COL width="10%">
                                                                                                                    
                                                                                                                <COL width="15%">                                                                                                            </table>
                                                                                                    </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>


                                                                        </table>
                                                                    </s:form>
                                                                    <script type="text/JavaScript">
                                                                        var cal3 = new CalendarTime(document.forms['projectDashboard'].elements['projectStartDate']);
                                                                        cal3.year_scroll = true;
                                                                        cal3.time_comp = false;
                                                                        cal4 = new CalendarTime(document.forms['projectDashboard'].elements['projectEndDate']);
                                                                        cal4.year_scroll = true;
                                                                        cal4.time_comp = false;
                                                                    </script>
                                                                </td>
                                                            </tr>
                                                            <!--2nd td in First table -->
                                                        </table>
                                                    </div>
                                                </td>
                                            </tr>

                                            <!-- PreSAles Projects start end -->
                                            
                                            <%-- Project Details by Customer Start  --%>
										<%--   <s:if test="%{#session.livingCountryList == 'India' || #session.livingCountryList == 'USA' || isAdminFlag=='YES'}">  --%>

										<tr>
											<td class="homePortlet" valign="top">
												<div class="portletTitleBar">
													<div class="portletTitleLeft">Project Details By
														Customer</div>
													<div class="portletIcons">
														<a
															href="javascript:animatedcollapse.hide('projectDetailsByCustomer')"
															title="Minimize"> <img
															src="../includes/images/portal/title_minimize.gif"
															alt="Minimize" /></a> <a
															href="javascript:animatedcollapse.show('projectDetailsByCustomer')"
															title="Maximize"> <img
															src="../includes/images/portal/title_maximize.gif"
															alt="Maximize" />
														</a>
													</div>
													<div class="clear"></div>
												</div>
												<div id="projectDetailsByCustomer"
													style="background-color: #ffffff">
													<table cellpadding="0" cellspacing="0" border="0"
														width="100%">
														<tr>
															<td width="40%" valign="top" align="center"><s:form
																	theme="simple" name="frmAddGreenSheet"
																	id="frmAddGreenSheet">

																	<table cellpadding="0" cellspacing="0" border="0"
																		width="100%">
																		<tr align="right">
																			<td class="headerText" colspan="9"><img
																				src="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/spacer.gif"
																				width="100%" height="13px" border="0"></td>
																		</tr>

																		<tr>
																			<td>
																				<%
																					if (session.getAttribute("resultMessageForUtility") != null) {
																							out.println(session.getAttribute("resultMessageForUtility"));
																							session.removeAttribute("resultMessageForUtility");
																						}
																				%>
																			</td>
																		</tr>
																		<tr>
																			<td>
																				<table border="0" align="center" cellpadding="0"
																					cellspacing="0">

																					<tr>
																						<td class="fieldLabel">Customer Name :</td>
																						<td><s:textfield name="accountName"
																								id="accountName" value="" autocomplete="off"
																								headerKey="-1" cssClass="inputTextBlue"
																								onkeyup="getAccountNames();" /><label
																							id="rejectedId"></label>
																							<div class="fieldLabelLeft"
																								id="validationMessage"></div>

																							<div
																								style="display: none; position: absolute; overflow: auto; z-index: 500;"
																								id="menu-popup">
																								<table id="completeTable" border="1"
																									bordercolor="#e5e4f2"
																									style="border: 1px dashed gray;"
																									cellpadding="0" class="cellBorder"
																									cellspacing="0"></table>
																							</div> <s:hidden name="accountId" id="accountId"
																								value="-1" /></td>
																						<td class="fieldLabel">Projects:</td>

																						<td><s:select list="myProjects"
																								name="projectId" id="projectId" headerKey="-1"
																								headerValue="--Select Project--"
																								cssClass="inputSelectLarge" disabled="false"
																								onchange="getProjectStatus();" /></td>
																						<td rowspan="3">&nbsp;&nbsp;&nbsp;<span
																							id="projectStatusSpan"></span></td>
																					</tr>

																					<tr>
																						<td class="fieldLabel">Status:</td>
																						<td><s:select
																								list="{'All','Active','InActive'}"
																								id="currStatus" name="currStatus"
																								cssClass="inputSelect" /></td>
																						<td class="fieldLabel" align="right">Country
																							:</td>

																						<td><s:select label="Select Country"
																								name="resourceCountry" id="resourceCountry"
																								headerKey="-1" headerValue="-- Please Select --"
																								list="countryList" cssClass="inputSelectLarge" /></td>
																						<td></td>

																					</tr>
																					<tr>
																						<td colspan="3"><br></td>
																						<td align="right"><input type="button"
																							value="Search" class="buttonBg"
																							onclick="getProjectDetailsByCustomer()" /></td>
																						<td></td>
																					</tr>
																					<tr>

																					</tr>
																					<tr>
																						<td class="fieldLabel">Total&nbsp;Records:</td>
																						<td class="userInfoLeft"><span
																							id="totalCount"></span></td>

																					</tr>
																				</table>
																			</td>
																		</tr>

																		<%-- table grid --%>
																		<tr>
																			<td><br>
																				<table align="center" cellpadding="2" border="0"
																					cellspacing="1" width="50%">

																					<tr>
																						<td height="20px" align="center" colspan="9">
																							<div id="employeeReport" style="display: none"
																								class="error">
																								<b>Loading Please Wait.....</b>
																							</div>
																						</td>
																					</tr>


																					<tr>
																						<td><br>
																							<div id="employeeReport" style="display: block">

																								<!--style="color:#0000FF;font:italic 900 12px arial;"  bgcolor='#3E93D4'-->
																								<table id="tblEmpBasedOnCustomerReport"
																									align='center' i cellpadding='1'
																									cellspacing='1' border='0' class="gridTable"
																									width='750'>
																									<COLGROUP ALIGN="left">
																										<COL width="5%">
																										<COL width="15%">
																										<COL width="10%">
																										<COL width="15%">
																										<COL width="10%">
																										<COL width="15%">
																								</table>
																								<br>
																								<!--<center><span id="spnFast" class="activeFile" style="display: none;"></span></center>-->
																							</div></td>
																					</tr>
																				</table></td>
																		</tr>
																		<%-- table grid  end--%>
																	</table>
																</s:form></td>
														</tr>
													</table>

												</div>
											</td>
										</tr>

										<%--    </s:if>  --%>

										<%-- Project Details by Customer End  --%>
                                            
                                            <%-- Available Employees  List Report  --%>

										<tr>
											<td class="homePortlet" valign="top">
												<div class="portletTitleBar">
													<div class="portletTitleLeft">Employees Report By
														Current Status</div>
													<div class="portletIcons">
														<a
															href="javascript:animatedcollapse.hide('AvailableEmpList')"
															title="Minimize"> <img
															src="../includes/images/portal/title_minimize.gif"
															alt="Minimize" /></a> <a
															href="javascript:animatedcollapse.show('AvailableEmpList')"
															title="Maximize"> <img
															src="../includes/images/portal/title_maximize.gif"
															alt="Maximize" />
														</a>
													</div>
													<div class="clear"></div>
												</div>
												<div id="AvailableEmpList" style="background-color: #ffffff">
													<table cellpadding="0" cellspacing="0" border="0"
														width="100%">
														<tr>
															<td width="40%" valign="top" align="center"><s:form
																	theme="simple" name="availableEmpList"
																	id="availableEmpList">

																	<table cellpadding="0" cellspacing="0" border="0"
																		width="100%">
																		<tr align="right">
																			<td class="headerText" colspan="9"><img
																				src="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/spacer.gif"
																				width="100%" height="13px" border="0"></td>
																		</tr>

																		<tr>
																			<td>
																				<%
																					if (session.getAttribute("resultMessageForUtility") != null) {
																							out.println(session.getAttribute("resultMessageForUtility"));
																							session.removeAttribute("resultMessageForUtility");
																						}
																				%>
																			</td>
																		</tr>
																		<tr>
																			<td>
																				<table border="0" align="center" cellpadding="0"
																					cellspacing="0">


																					<tr>
																						<td class="fieldLabel" width="200px" align="right">Department
																							:</td>

																						<td><s:select headerKey="-1"
																								headerValue="--Please Select--"
																								list="{'SSG','GDC'}" name="departmentId1"
																								id="departmentId1" cssClass="inputSelect"
																								onchange="getPracticeDataV2();" /></td>
																						<td class="fieldLabel" width="200px" align="right">Practice
																							Name :</td>

																						<td><s:select name="practiceId1"
																								id="practiceId1" headerKey="-1"
																								headerValue="--Please Select--"
																								list="practiceIdList" cssClass="inputSelect"
																								onchange="getSubPracticeData2();" /></td>

																					</tr>

																					<tr>

																						<td class="fieldLabel" width="200px" align="right">Country
																							:</td>

																						<td><s:select label="Select Country"
																								name="country1" id="country1" headerKey="-1"
																								headerValue="-- Please Select --"
																								list="countryList" cssClass="inputSelect" /></td>

																						<td class="fieldLabel" width="200px" align="right">State
																							:<FONT color="red"><em>*</em></FONT>
																						</td>
																						<td><s:select name="state" id="state"
																								headerKey="-1" headerValue="--Please Select--"
																								list="stateList" cssClass="inputSelect"
																								onchange="displayUtilizationDiv();showResourceTypeDiv();" /></td>

																					</tr>
																					<tr>
																						<td class="fieldLabel" width="200px" align="right">Location
																							:</td>

																						<td><s:select name="location1" headerKey="-1"
																								headerValue="All" id="location1"
																								list="{'India - Other','LB Colony','Miracle City','Miracle Heights','Onsite','Work From Home'}"
																								cssClass="inputSelect" /></td>

																					</tr>
																					<tr>
																						<td class="fieldLabel" width="200px" align="right">SubPractice&nbsp;Name&nbsp:</td>
																						<td><s:select name="subPractice1"
																								id="subPractice1" headerKey="-1"
																								headerValue="--Please Select--"
																								list="subPracticeList" cssClass="inputSelect" /></td>
																						<td class="fieldLabel" width="200px" align="right">Sorting&nbsp;Based&nbspOn:</td>
																						<td><s:select name="sortedBy" id="sortedBy"
																								list="{'EmployeeName','Practice'}"
																								cssClass="inputSelect" /></td>


																					</tr>
																					<tr id="customerTr" style="display: none;">
																						<td class="fieldLabel">Customer Name :</td>
																						<td><s:select headerKey="-1"
																								headerValue="All" list="clientMap"
																								name="customerName" id="customerName"
																								cssClass="inputSelect" theme="simple"
																								onchange="getProjectsByAccountId();" /></td>
																						<td class="fieldLabel">ProjectName :</td>
																						<td><s:select headerKey="-1"
																								headerValue="All" list="{}" name="projectName"
																								id="projectName" cssClass="inputSelect"
																								theme="simple" /></td>
																					</tr>
																					<tr id="resTypeTr" style="display: none;">
																						<td class="fieldLabel" width="200px" align="right">Resource&nbsp;Type:</td>
																						<td><s:select name="resourceType"
																								id="resourceType" headerKey="-1"
																								headerValue="--Please Select--"
																								list="{'Main(Billable)','Main','Shadow','Training','Overhead'}"
																								cssClass="inputSelect" /></td>
																						<td class="fieldLabel" width="200px" align="right">Based&nbsp;On:</td>
																						<td><s:select name="basedOn" id="basedOn"
																								headerKey="-1" headerValue="--Please Select--"
																								list="#@java.util.LinkedHashMap@{'Utilization':'Highest Utilization Project','Billable':'Billable Project'}"
																								cssClass="inputSelect" /></td>
																					</tr>
																					<tr>
																						<td class="fieldLabel">Skills&nbsp;:</td>
																						<td><s:textfield name="skillSet"
																								id="skillSet" cssClass="inputTextBlue"
																								value="%{skillSet}" /> <img
																							src="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/info.jpg"
																							onmouseover="fixedtooltip('Search for only one skill<br><b>Ex: java</b><br><br>Search for both skills use \'&\' between skills <br><b>Ex: java&unix</b><br><br>Search for either one of the skill then use \'|\' between skills<br><b>Ex: java&unix|dotnet</b><br><br>Search for skills excluding some skills use \'-\'<br><b>Ex: java&unix|oracle-db2</b>',this,event, 200,-20,40)"
																							onmouseout="delayhidetip()" height="18"
																							width="20"></td>

																						<td class="fieldLabel" id="utilizationDiv"
																							style="visibility: hidden;">Available&nbsp;Utilization:
																						</td>
																						<td id="utilizationField"
																							style="visibility: hidden;"><s:select
																								list="{'<100','=100'}" headerKey="-1"
																								headerValue="--Please Select--"
																								name="utilizationSearch" id="utilizationSearch"
																								cssClass="inputSelect" />
																					</tr>
																					
																					
																					<tr>

																						<td colspan="3"></td>
																						<td width="200px" align="center"><input
																							type="button" value="Search" class="buttonBg"
																							onclick="getAvailableList()" /></td>
																					</tr>
																					<tr>
																						<td class="fieldLabel">Total&nbsp;Records:</td>
																						<td class="userInfoLeft" id="totalState1"></td>
																						<%--  <td class="fieldLabel" >Available&nbsp;Count: </td>
                                                                                              <td class="userInfoLeft"  id="totalAvailable" ></td>

                                                                                            <td class="fieldLabel" > OnBench&nbsp;Count: </td>
                                                                                            <td class="userInfoLeft"  id="totalOnBench" ></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="fieldLabel" > OnProject&nbsp;Count:  </td>
                                                                                            <td class="userInfoLeft"  id="totalOnProject" ></td>

                                                                                            <td class="fieldLabel" > R&D/POC&nbsp;Count: </td>
                                                                                            <td class="userInfoLeft"  id="totalRP" ></td>
                                                                                            <td class="fieldLabel" > Training&nbsp;Count: </td>
                                                                                            <td class="userInfoLeft"  id="totalTraining" ></td> --%>
																					</tr>
																				</table>
																			</td>
																		</tr>

																		<%-- table grid --%>
																		<tr>
																			<td><br>
																				<table align="center" cellpadding="2" border="0"
																					cellspacing="1" width="50%">

																					<tr>
																						<td height="20px" align="center" colspan="9">
																							<div id="availableReport" style="display: none"
																								class="error">
																								<b>Loading Please Wait.....</b>
																							</div>
																						</td>
																					</tr>


																					<tr>
																						<td><br>
																							<div id="availableReport" style="display: block">
																								<!--style="color:#0000FF;font:italic 900 12px arial;"  bgcolor='#3E93D4'-->
																								<table id="tblAvailableReport" align='center' i
																									cellpadding='1' cellspacing='1' border='0'
																									class="gridTable" width='700'>
																									<COLGROUP ALIGN="left">
																										<COL width="5%">
																										<COL width="15%">
																										<COL width="10%">
																										<COL width="10%">
																										<COL width="10%">
																										<COL width="50%">
																								</table>
																								<br>
																								<!--<center><span id="spnFast" class="activeFile" style="display: none;"></span></center>-->
																							</div></td>
																					</tr>
																				</table></td>
																		</tr>
																		<%-- table grid  end--%>
																	</table>
																</s:form></td>
														</tr>
													</table>

												</div>
											</td>
										</tr>
										<s:hidden id="isAdminFlag" value="%{isAdminFlag}"></s:hidden>
										<s:hidden id="userId" value="%{#session.userId}"></s:hidden>

										<%-- Available Employees List End --%>
                                            
                                            
                                            
                                                <%-- OffshoreDeliveryStrengthDashboardDiv Start--%>

										<tr>
											<td class="homePortlet" valign="top">
												<div class="portletTitleBar">
													<div class="portletTitleLeft">Offshore&nbsp;-&nbsp;Delivery&nbsp;Strength</div>
													<div class="portletIcons">
														<a
															href="javascript:animatedcollapse.hide('OffshoreDeliveryStrengthDashboardDiv')"
															title="Minimize"> <img
															src="../includes/images/portal/title_minimize.gif"
															alt="Minimize" /></a> <a
															href="javascript:animatedcollapse.show('OffshoreDeliveryStrengthDashboardDiv')"
															title="Maximize"> <img
															src="../includes/images/portal/title_maximize.gif"
															alt="Maximize" />
														</a>
													</div>
													<div class="clear"></div>
												</div>

												<div id="OffshoreDeliveryStrengthDashboardDiv"
													style="background-color: #ffffff">
													<table cellpadding="0" cellspacing="0" border="0"
														width="100%">
														<tr>
															<td width="80%" valign="top" align="center">
															<s:form
																	action="" theme="simple"
																	name="OffshoreDeliveryStrengthDashboard"
																	id="OffshoreDeliveryStrengthDashboard">

																	<table cellpadding="0" cellspacing="0" border="0"
																		width="100%">
																		<tr>
																			<td>
																				<table border="0" align="center" cellpadding="0"
																					cellspacing="0" width="800;">
																					<br>


																					<tr>

																						<td class="fieldLabel">Practice&nbsp;:</td>
																						<td><s:select cssClass="inputSelect"
																								list="practiceMap" name="offshorepracticeId"
																								id="offshorepracticeId"
																								headerValue="--Please Select--" headerKey="-1"
																								value="%{offshorepracticeId}"
																								onchange="getSubPracticeList();" /></td>


																						<td class="fieldLabel" width="200px" align="right">SubPractice&nbsp;Name&nbsp;:</td>

																						<td><s:select name="subPractice"
																								id="subPractice" headerKey="All"
																								headerValue="-- Please Select --" list="{}"
																								cssClass="inputSelectNew" theme="simple" /></td>

																					
																						
																						<td><input type="button" value="Search"
																							id="OffshoreDeliveryStrengthBTN" class="buttonBg"
																							onclick="OffshoreDeliveryStrengthDetails();" /></td>

																					</tr>
																				</table>
																			</td>
																		</tr>
																		<tr>
																			<td>
																				<table border="0" align="center" cellpadding="0"
																					cellspacing="0">

																					<!-- new graph details start -->
																					<tr>
																						<td height="20px" align="center" colspan="9">
																							<div id="OffshoreDeliveryStrengthLoad"
																								style="display: none" class="error">
																								<b>Loading Please Wait.....</b>
																							</div>
																						</td>
																					</tr>
																					<tr>
																						<td colspan="4">
																							<table id="OffshoreDeliveryStrengthReport"
																								align='center' cellpadding='1' cellspacing='1'
																								border='0' class="gridTable" width='700'>
																								<COLGROUP ALIGN="left">
																							</table>



																						</td>
																					</tr>
																					<!-- new graph details end -->

																				</table>
																			</td>
																		</tr>
																	</table>
																</s:form> 
															</td>
														</tr>
													</table>

												</div>

											</td>
										</tr>


										<%--OffshoreDeliveryStrength end --%>
                                            
                                            
                                            
                                            
                                           <%--Customer Project Roll In  start --%>

                                <tr>
                                <td class="homePortlet" valign="top">
                                    <div class="portletTitleBar">
                                        <div class="portletTitleLeft">Customer/Project-RollIn/RollOff&nbsp;Report</div>
                                        <div class="portletIcons">
                                            <a href="javascript:animatedcollapse.hide('CustomerProjectRollINRollOffDiv')" title="Minimize">
                                                <img src="../includes/images/portal/title_minimize.gif" alt="Minimize"/></a>
                                            <a href="javascript:animatedcollapse.show('CustomerProjectRollINRollOffDiv')" title="Maximize">
                                                <img src="../includes/images/portal/title_maximize.gif" alt="Maximize"/>
                                            </a>
                                        </div>
                                        <div class="clear"></div>
                                    </div>

                                    <div id="CustomerProjectRollINRollOffDiv" style="background-color:#ffffff">
                                        <table cellpadding="0" cellspacing="0" border="0" width="100%"> 
                                            <tr>
                                            <td width="80%" valign="top" align="center">
                                                <s:form action="" theme="simple" name="customerDashBoard" id="customerDashBoard">  

                                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                    
                                                    
                                                        <td>
                                                            <table border="0" align="center" cellpadding="0" cellspacing="0" width="800;">
                                                                <br>
                                                                <tr>

																						<td class="fieldLabel">Start&nbsp;Date&nbsp;(mm/dd/yyyy)&nbsp;:<FONT
																							color="red"><em>*</em></FONT></td>
																						<td><s:textfield name="startDate"
																								id="startDate"
																								cssClass="inputTextBlueSmall" value="%{startDate}" /><a
																							href="javascript:cal1.popup();"> <img
																								src="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/showCalendar.gif"
																								width="20" height="20" border="0"></a></td>

																						<td class="fieldLabel">End&nbsp;Date&nbsp;(mm/dd/yyyy)&nbsp;:<FONT
																							color="red"><em>*</em></FONT></td>
																						<td><s:textfield name="endDate"
																								id="endDate" cssClass="inputTextBlueSmall"
																								value="%{endDate}" /><a href="javascript:cal2.popup();">
																								<img
																								src="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/showCalendar.gif"
																								width="20" height="20" border="0">
																						</a></td>

																					</tr>
                                                    
                                                    
                                                        <tr>
                                                                <tr>
                                                               

                                                                <td class="fieldLabel">Type&nbsp;:</td>
                                                                 <td><s:select list="{'Roll-In','Roll-Off'}" name="type" id="type"  headerValue="select" headerKey="-1" /></td>

                                                                </tr>

                                                                <tr>
                                                              <td></td>
                                                              <td></td>
                                                              <td></td>
                                                              <td>
                                                              
                                                                    <input  type="button" value="Search" class="buttonBg" onclick="return getCustomerProjectRollInRollOffDetails();"/>
                                                                </td>

                                                                </tr>
                                                                
                                                                <script type="text/JavaScript">
                                                            var cal1 = new CalendarTime(document.forms['customerDashBoard'].elements['startDate']);
                                                            cal1.year_scroll = true;
                                                            cal1.time_comp = false;
                                                            var cal2 = new CalendarTime(document.forms['customerDashBoard'].elements['endDate']);
                                                            cal2.year_scroll = true;
                                                            cal2.time_comp = false;
                                                        </script>

                                                            </table>
                                                        </td>
                                                        </tr>
                                                        <%-- table grid --%>
                                                                                                                                                                                        <tr>
                                                                                                                                                                                        <td>
                                                                                                                                                                                            <br>
                                                                                                                                                                                            <table align="center" cellpadding="2" border="0" cellspacing="1" width="50%" >

                                                                                                                                                                                                <tr>
                                                                                                                                                                                                <td height="20px" align="center" colspan="9">
                                                                                                                                                                                                    <div id="loadingMessage1" style="display:none" class="error" ><b>Loading Please Wait.....</b></div>
                                                                                                                                                                                                </td>
                                                                                                                                                                                                </tr>


                                                                                                                                                                                                <tr>
                                                                                                                                                                                                <td ><br>
                                                                                                                                                                                                    <div id="customerProjectRol" style="display: block">
                                                                                                                                                                                                        <!--style="color:#0000FF;font:italic 900 12px arial;"  bgcolor='#3E93D4'-->
                                                                                                                                                                                                        <table id="tblResourcesRollInRollOff" align='center' i cellpadding='1' cellspacing='1' border='0' class="gridTable" width='750'>


                                                                                                                                                                                                        </table> <br>
                                                                                                                                                                                                        <!--<center><span id="spnFast" class="activeFile" style="display: none;"></span></center>-->
                                                                                                                                                                                                    </div>
                                                                                                                                                                                                </td>
                                                                                                                                                                                                </tr>
                                                                                                                                                                                            </table>    
                                                                                                                                                                                        </td>
                                                                                                                                                                                        </tr>
                                                                                                                                                                                        <%-- table grid  end--%>
                                                                                                                </table>
                                                                                                            </s:form>
                                                                                                           
                                                                                                            </td>
                                                                                                            </tr>
                                                                                                            </table>

                                                                                                            </div>

                                                                                                            </td>
                                                                                                            </tr>


                                                                                                            <%-- Customer Project RollIn RollOff end  --%>
                                                                                                            
                                        </table>

                                        <%--     </sx:div>
                                </sx:tabbedpanel> --%>
                                        <!--//END TABBED PANNEL : --> 
                                    </div>
                                </div>
                                <script type="text/javascript">

                                    var countries=new ddtabcontent("accountTabs")
                                    countries.setpersist(false)
                                    countries.setselectedClassTarget("link") //"link" or "linkparent"
                                    countries.init()

                                </script>

                            </td>
                            <!--//END DATA COLUMN : Coloumn for Screen Content-->
                        </tr>
                    </table>
                </td>
            </tr>
            <!--//END DATA RECORD : Record for LeftMenu and Body Content-->

            <!--//START FOOTER : Record for Footer Background and Content-->
            <tr class="footerBg">
                <td align="center"><s:include value="/includes/template/Footer.jsp"/></td>
            </tr>
            <!--//END FOOTER : Record for Footer Background and Content-->
        </table>
        <!--//END MAIN TABLE : Table for template Structure-->
        
		<script type="text/javascript">
		$(window).load(function(){
			
			init();
		defaultProjectDates();
		
		
		});
		</script>
    </body>

</html>

