<%-- 
    Document   :TimeSheethoursDetailsByWeekly
    Created on : Jun 5, 2017, 3:43:14 PM
    Author     : Nagalakshmi
--%>

<%@ page contentType="text/html; charset=UTF-8" errorPage="../exception/ErrorDisplay.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sx" uri="/struts-dojo-tags" %>
<%@ page import="com.freeware.gridtag.*" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="com.mss.mirage.util.ConnectionProvider"%>
<%@ page import="com.mss.mirage.util.ApplicationConstants"%>
<%@ taglib uri="/WEB-INF/tlds/datagrid.tld" prefix="grd"%>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="javax.sql.DataSource" %>
<%@ page import="com.mss.mirage.util.ConnectionProvider"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <title>Hubble Organization Portal :: TimeSheet Hours Details By Weekly</title>
        <link rel="stylesheet" type="text/css" href="<s:url value="/includes/css/mainStyle.css"/>">
        <link rel="stylesheet" type="text/css" href="<s:url value="/includes/css/GridStyle.css"/>">
        <link rel="stylesheet" type="text/css" href="<s:url value="/includes/css/leftMenu.css"/>">
        <link rel="stylesheet" type="text/css" href="<s:url value="/includes/css/tabedPanel.css"/>">
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/DBGrid.js"/>"></script>
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/ClientValidations.js"/>"></script>        
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/AppConstants.js"/>"></script>
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/tabedPanel.js"/>"></script>
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/CalendarTime.js"/>"></script>       
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/pmoDashBoardAjax.js?version=1.0"/>"></script>

        <s:include value="/includes/template/headerScript.html"/> 
        
         <script>
          function  doGetTimeSheetReport(){
        	 // var customerName=document.getElementById('customerName1').value;
        	  var practiceId=document.getElementById('practiceId1').value;
              var costModel=document.getElementById('costModel1').value;
              var year=document.getElementById('year1').value;
              var month=document.getElementById('month1').value;
              var customerId=document.getElementById('customerId1').value;
              window.location="pmoDashBoard.action?backToFlag=1&customerName="+customerId+"&practiceId="+practiceId+"&costModel="+escape(costModel)+"&year="+year+"&month="+month;
          }
            </script>
    </head>
  
    <body class="bodyGeneral" oncontextmenu="return false;">


        <%!    /*
             * Declarations
             */
            Connection connection;
            String queryString;
            //StringBuffer queryString;
            String strTmp;
            String strSortCol;
            String strSortOrd;
            String userId;
            String submittedFrom;
            String action;
            //new
            String userRoleName;
            int role;
            int intSortOrd = 0;
            int intCurr;
            boolean blnSortAsc = true;
        %>


        <!-- Start oif the table -->
        <table class="templateTable1000x580" align="center" cellpadding="0" cellspacing="0">


            <!--//START HEADER : Record for Header Background and Mirage Logo-->
            <tr class="headerBg">
                <td valign="top">
                    <s:include value="/includes/template/Header.jsp"/>                    
                </td>
            </tr>
            <!--//END HEADER : Record for Header Background and Mirage Logo-->


            <!--//START DATA RECORD : Record for LeftMenu and Screen Content-->
            <tr>
                <td>
                    <table class="innerTable1000x515" cellpadding="0" cellspacing="0">
                        <tr>
                            <!--//START DATA COLUMN : Coloumn for LeftMenu-->
                            <td width="150px;" class="leftMenuBgColor" valign="top"> 
                                <s:include value="/includes/template/LeftMenu.jsp"/> 
                            </td>
                            <!--//START DATA COLUMN : Coloumn for LeftMenu-->

                            <!--//START DATA COLUMN : Coloumn for Screen Content-->
                            <td width="850px" class="cellBorder" valign="top" style="padding:10px 5px 5px 5px;">
                                <!--//START TABBED PANNEL : -->
                                <ul id="accountTabs" class="shadetabs" >
                                    <li ><a href="#" class="selected" rel="requirementList">TimeSheetHoursDetailsByWeekly</a></li>
                                    <!-- <li ><a href="#" rel="searchDiv">Requirement Search</a></li>-->
                                </ul>
                                <div  style="border:1px solid gray; width:845px;height: 530px; overflow:auto; margin-bottom: 1em;">
                                    <%--  <sx:tabbedpanel id="attachmentPannel2" cssStyle="width: 845px; height: 530px;padding:10px 5px 5px 5px" doLayout="true"> --%>

                                    <!--//START TAB : -->
                                    <%--  <sx:div id="List" label="" cssStyle="overflow:auto;"> --%>
                                    <div id="requirementList">

                                        <%
                                            if (request.getAttribute(ApplicationConstants.RESULT_MSG) != null) {
                                                out.println(request.getAttribute(ApplicationConstants.RESULT_MSG));
                                            }
                                        %>


                                            <s:form name="TotalTimeSheetHours"  theme="simple" >                                            
                                            <table cellpadding="0" cellspacing="0" align="left" width="100%">
                                             <tr><td colspan="4"><div id="resultMessage"></div></td></tr>  
                                               <tr>
                                                   <td>
                                                       <table border="0" cellpadding="0" cellspacing="2" align="center" width="100%">
                                                            
                                                            <s:hidden name="customerName" id="customerName1" value="%{customerName}"/>
                                                            <s:hidden name="practiceId" id="practiceId1" value="%{practiceId}"/>
                                                            <s:hidden name="costModel" id="costModel1" value="%{costModel}"/>
                                                            <s:hidden name="projectId" id="projectId" value="%{projectId}"/>
                                                            <s:hidden name="year" id="year1" value="%{year}"/>
                                                             <s:hidden name="month" id="month1" value="%{month}"/>
                                                             <s:hidden name="Week" id="week1" value="%{week}"/>
                                                              <s:hidden name="projectName" id="projectName1" value="%{projectName}"/>
                                                            <s:hidden name="customerId" id="customerId1" value="%{customerId}"/>
                                                            
                                                                
                                                            <tr>
                                                               
                                                              <td class="fieldLabel" align="left">Customer&nbsp;Name :</td>
                                                              <td style="font-size: 12px;font-style: normal;color:green "><s:label><s:property value="customerName"/></s:label></td>

                                                                                                                               
                                                            <td class="fieldLabel" align="left">Project&nbsp;Name :</td>
                                                            <td style="font-size: 12px;font-style: normal;color:green "><s:label><s:property  value="projectName" /></s:label></td>
                                                         
                                                      
                                                                                 
                                                         <td valign="middle">
                                                        <INPUT type="button" CLASS="buttonBg" value="Back to List" onClick="doGetTimeSheetReport()">  
                                                    </td>
                                                                
                                                            </tr>
                                                            
                                                            <tr>
                                                                
                                                            
                                                            <td class="fieldLabel" align="left">Year :</td>
                                                            <td style="font-size: 12px;font-style: normal;color:green "><s:label><s:property  value="year" /></s:label></td>
                                                            
                                                                
                                                            <td class="fieldLabel" align="left">Month :</td>
                                                            <td style="font-size: 12px;font-style: normal;color:green "><s:label><s:property  value="monthName" /></s:label></td>
                                                            
                                                              <td class="fieldLabel" align="left">Week :</td>
                                                            <td style="font-size: 12px;font-style: normal;color:green "><s:label><s:property  value="week" /></s:label></td>
                                                            </tr>
                                                            
                                                             <tr>
                                                           <td class="fieldLabel" >Total&nbsp;Records:</td>
                                                           <td class="userInfoLeft" id="totalCount"></td>
                                                                                                                                                                             
                                                              </tr>   
                                                            

                                                        </table>  
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>

                                                        <table cellpadding="0" cellspacing="0" width="100%">
                                                            <tr>
                                                                <td>
                                                                    <table width="100%" cellpadding="1" cellspacing="1" border="0">
                                                                        <tr>
                                                                            <td>
                                                                                <div id="loadingMessage" style="color:red; display: none;" align="center"><b><font size="4px" >Loading...Please Wait...</font></b></div>

                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <%-- <DIV id="loadingMessage"> </DIV> --%>

                                                                                <TABLE id="tblTimeSheetHours" align="left"  
                                                                                       cellpadding='1' cellspacing='1' border='0' class="gridTable" width='100%'>
                                                                                        <COLGROUP ALIGN="left" >
                                                                                        <COL width="5%"> 
                                                                                        <COL width="25%"> 
                                                                                        <COL width="20%">
                                                                                             </TABLE>

                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>

                                                        </table>


                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </s:form>


                                    <%--   </sx:div> --%>


                            </td>
                            <!--//END DATA COLUMN : Coloumn for Screen Content-->
                        </tr>
                    </table>
                </td>
            </tr>

            <!--//END DATA RECORD : Record for LeftMenu and Body Content-->

            <!--//START FOOTER : Record for Footer Background and Content-->
            <tr class="footerBg">
                <td align="center"><s:include value="/includes/template/Footer.jsp"/>   </td>
            </tr>
            <!--//END FOOTER : Record for Footer Background and Content-->

        </table>
        <!--//END MAIN TABLE : Table for template Structure-->

        <!--  End of the main table -->        
<script type="text/javascript">
                                                                                                                                                                                   $(window).load(function(){
                                                                                                                                                                                	   getTotalTimeSheetHoursbyWeekTeja();
                                                                                                                                                                   });
                                                                                                                                                                                   </script>
              
    </body>
</html>

