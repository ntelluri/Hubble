<%@ page contentType="text/html; charset=UTF-8"
	errorPage="../../exception/ErrorDisplay.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%--<%@ taglib prefix="sx" uri="/struts-dojo-tags" %> --%>
<%@ page import="com.freeware.gridtag.*"%>
<%@ page import="java.sql.Connection"%>
<%@ page import="java.sql.DriverManager"%>
<%@ page import="java.sql.SQLException"%>
<%@ page import="com.mss.mirage.util.ConnectionProvider"%>
<%@ page import="com.mss.mirage.util.ApplicationConstants"%>
<%@ taglib uri="/WEB-INF/tlds/datagrid.tld" prefix="grd"%>
<html>
<head>
<title>Hubble Organization Portal :: Biometric</title>
<sx:head cache="true" />
<link rel="stylesheet" type="text/css"
	href="<s:url value="/includes/css/mainStyle.css"/>">
<link rel="stylesheet" type="text/css"
	href="<s:url value="/includes/css/GridStyle.css"/>">
<link rel="stylesheet" type="text/css"
	href="<s:url value="/includes/css/leftMenu.css"/>">
<link rel="stylesheet" type="text/css"
	href="<s:url value="/includes/css/tabedPanel.css"/>">
<script type="text/JavaScript"
	src="<s:url value="/includes/javascripts/DBGrid.js"/>"></script>
<link rel="stylesheet" type="text/css"
	href="<s:url value="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css"/>">

<script type="text/JavaScript"
	src="<s:url value="/includes/javascripts/AppConstants.js"/>"></script>
<script type="text/JavaScript"
	src="<s:url value="/includes/javascripts/tabedPanel.js"/>"></script>
<link rel="stylesheet" type="text/css"
	href="<s:url value="/includes/css/tooltip.css"/>">
<script type="text/JavaScript"
	src="<s:url value="/includes/javascripts/Biometric.js?version=11.0"/>"></script>
<script type="text/JavaScript"
	src="<s:url value="/includes/javascripts/tooltip.js"/>"></script>
<script type="text/JavaScript"
	src="<s:url value="/includes/javascripts/CalendarTime.js"/>"></script>
<script type="text/javascript"
	src="<s:url value="/includes/javascripts/reviews/jquery.js"/>"></script>
<script type="text/javascript"
	src="<s:url value="/includes/javascripts/reviews/jquery.min.js"/>"></script>

<link rel="stylesheet" type="text/css"
	href="<s:url value="/includes/css/new/x0popup.min.css?version=1.0"/>">
<script type="text/JavaScript"
	src="<s:url value="/includes/javascripts/payroll/x0popup.min.js"/>"></script>
<script type="text/JavaScript"
	src="<s:url value="/includes/javascripts/EmpStandardClientValidations.js?ver=1.6"/>"></script>



<s:include value="/includes/template/headerScript.html" />
</head>
<body class="bodyGeneral" oncontextmenu="return false;">

	<%!
/* Declarations */
Connection connection;
String queryString;
String strTmp;
String strSortCol;
String strSortOrd;
String submittedFrom;
String searchSubmitValue;
int intSortOrd = 0;
int intCurr;
boolean blnSortAsc = true;

String strTmp1;
String strSortCol1;
String strSortOrd1;
String submittedFrom1;
String searchSubmitValue1;
int intSortOrd1 = 0;
int intCurr1;
boolean blnSortAsc1 = true;
%>

	<!--//START MAIN TABLE : Table for template Structure-->
	<table class="templateTableLogin" align="center" cellpadding="0"
		cellspacing="0">

		<!--//START HEADER : Record for Header Background and Mirage Logo-->
		<tr class="headerBg">
			<td valign="top"><s:include
					value="/includes/template/Header.jsp" /></td>
		</tr>
		<!--//END HEADER : Record for Header Background and Mirage Logo-->


		<!--//START DATA RECORD : Record for LeftMenu and Screen Content-->
		<tr>
			<td>
				<table class="innerTableLogin" cellpadding="0" cellspacing="0">
					<tr>

						<!--//START DATA COLUMN : Coloumn for LeftMenu-->
						<td width="150px;" class="leftMenuBgColor" valign="top"><s:include
								value="/includes/template/LeftMenu.jsp" /></td>

						<!--//END DATA COLUMN : Coloumn for LeftMenu-->

						<!--//START DATA COLUMN : Coloumn for Screen Content-->
						<td width="850px" class="cellBorder" valign="top"
							style="padding: 5px 5px 5px 5px;">
							<!--//START TABBED PANNEL : -->
							<ul id="accountTabs" class="shadetabs">
								<li><a href="#" class="selected" rel="biometric" id="biometricTab">
										Biometric </a></li>
								<li><a href="#" rel="syncAttendance" id="syncAttendanceTab"> Sync
										Attendance </a></li>
							</ul> <%--<sx:tabbedpanel id="employeeSearchPannel" cssStyle="width: 840px; height: 500px;padding: 5px 5px 5px 5px;" doLayout="true"> --%>
							<div
								style="border: 1px solid gray; width: 840px; height: 500px; overflow: auto; margin-bottom: 1em;">
								<!--//START TAB : -->
								<%--  <sx:div id="employeeSearchTab" label="Employee Search" cssStyle="overflow:auto;"  > --%>



								<div id="syncAttendance" class="tabcontent">

									<div id="addAttendanceOverlay"></div>
									<div id="addAttendanceOverlayBox" align="center">
										<s:form theme="simple">
											<table style="width: 100%;">

												<tr class="bioOverlayHeader">
													<td colspan="2"><font
														style="vertical-align: middle; font-family: myHeaderFonts; color: white; font-size: 20px; text-align: left;">
															Add SyncAttendance</font></td>
													<td colspan="2" align="right"><a href="#"
														onclick="addSyncAttendanceOverlay();"> <img
															src="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/closeButton.png" /></a>

													</td>
												</tr>
												<tr>
													<td align="left"><span id="loadingEmpDetailsMessage"
														style="display: none"><font color="red" size="2px">loading
																please wait...</font></span></td>
												</tr>
												<tr>
													<td>
														<table style="width: 70%;">
															<tr>
																<span id="resultMessage"></span>
															</tr>
															<tr>
																<td class="fieldLabel">Year(YYYY):</td>
																<td><s:textfield name="syncYearOverlay"
																		id="syncYearOverlay" maxlength="4" cssClass=""
																		value="%{syncYear}" /></td>
																<td class="fieldLabel">Month:</td>
																<td><s:select
																		list="#@java.util.LinkedHashMap@{'1':'Jan','2':'Feb','3':'Mar','4':'Apr','5':'May','6':'June','7':'July','8':'Aug','9':'Sept','10':'Oct','11':'Nov','12':'Dec'}"
																		name="syncMonthOverlay" id="syncMonthOverlay"
																		headerValue="select" headerKey="0"
																		value="%{syncMonth}"
																		cssClass="inputSelectSmall" /></td>
															</tr>
															<tr>
																<td class="fieldLabel">Comments:</td>
																<td cssClass=""><s:textarea rows="2" cols="30"
																		name="comments" cssClass="inputTextarea"
																		onchange="fieldLengthValidator(this);" id="comments" />
																</td>
																<td colspan="1" align="right"><input type="button"
																	class="buttonBg" value="Add"
																	onclick="addSynAttendance();" /></td>
															</tr>
														</table>
													</td>
												</tr>
												<tr>
													<td>
														<table style="width: 70%;">

															<tr>
																<span id="response"></span>
															</tr>
															<tr>
																<td>
																	<table id="tblEmpDetails" class="gridTable" width="400"
																		cellspacing="1" cellpadding="7" border="0"
																		align="center" style="margin-left: 37px;">
																		<COLGROUP ALIGN="left">
																			<COL width="15%">
																			<COL width="15%">
																	</table>
																</td>
															</tr>
														</table>

													</td>
												</tr>

											</table>

										</s:form>
									</div>


									<div id="syncAttendanceOverlay"></div>
									<div id="syncAttendanceOverlayBox" align="center">
									
										<s:form theme="simple">
											<table style="width: 100%;">
											<s:hidden id="attendenceTabActive" name="attendenceTabActive" value="%{attendenceTabActive}"/>
												<!-- <input type="hidden" id="month" name="month" value="%{month}">
                                 <input type="hidden" name="year" id="year" value="%{year}"> -->
												<tr class="bioOverlayHeader">
													<td colspan="2"><font
														style="vertical-align: middle; font-family: myHeaderFonts; color: white; font-size: 20px; text-align: left;">
															Sync Attendance</font></td>
													<td colspan="2" align="right"><a href="#"
														onclick="syncingAttendanceOverlay('%{month}','%{year}');">
															<img
															src="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/closeButton.png" />
													</a></td>
												</tr>
												<tr>
													<td>

														<table style="width: 70%;">

															<tr>
																<td class="fieldLabel" align="right">Location:</td>
																<td><s:select label="Select Location"
																		name="locationId" id="locationId" headerKey=""
																		headerValue="-- Please Select --" list="locationList"
																		cssClass="inputSelect" value="%{location}"
																		onchange="getEmpNameByLocation();" /></td>
																<td class="fieldLabel">EmpName:</td>
																<td><s:select list="empnamesList"
																		id="employeeName" name="employeeName" headerKey=""
																		headerValue="Select EmpName" cssClass="inputSelect"
																		value="%{employeeName}" /></td>

															</tr>
															<tr>
																<td colspan="1" align="right"><input type="button"
																	class="buttonBg" value="SyncAttendance"
																	onclick="syncAttendance();" /></td>

															</tr>



														</table>

													</td>
												</tr>

											</table>

										</s:form>
									</div>





									<table cellpadding="0" cellspacing="0" border="0" width="100%">
										<tr align="right">
											<td class="headerText"><img alt="Home"
												src="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/spacer.gif"
												width="100%" height="13px" border="0"></td>
										</tr>
										<tr>
											<td>
												<% if(request.getAttribute(ApplicationConstants.RESULT_MSG) != null){
                                    out.println(request.getAttribute(ApplicationConstants.RESULT_MSG));
                                }
                                
                                if(request.getAttribute("submitFrom") != null){
                                    submittedFrom = request.getAttribute("submitFrom").toString();
                                }
                                
                                if(!"dbGrid".equalsIgnoreCase(submittedFrom)){
                                    searchSubmitValue = submittedFrom;
                                }
                                
                                %>

											</td>
										</tr>
										<tr>
											<td><s:form name="synAttendance"
													action="getBiometricSearch" theme="simple">
													<table cellpadding="0" cellspacing="0" width="100%"
														border="0">
														<tr>
															<td><br></td>
														</tr>
														<tr>
															<td>
																<div
																	style="width: auto; margin-left: 62px; margin-right: 60px;">
																	<table cellpadding="0" cellspacing="0" width="100%"
																		border="0">
																		<tr>
																			<td class="fieldLabel">Year(YYYY):</td>
																			<td><s:textfield name="syncYear" id="syncYear"
																					maxlength="4" cssClass="" value="%{syncYear}" /></td>
																			<td class="fieldLabel">Month:</td>
																			<td><s:select
																					list="#@java.util.LinkedHashMap@{'1':'Jan','2':'Feb','3':'Mar','4':'Apr','5':'May','6':'June','7':'July','8':'Aug','9':'Sept','10':'Oct','11':'Nov','12':'Dec'}"
																					name="syncMonth" id="syncMonth"
																					headerValue="select" headerKey="0"
																					value="%{syncMonth}" cssClass="inputSelectSmall" /></td>

																			<td align="right" colspan="4"><s:submit
																					cssClass="buttonBg" value="Search" /> <input
																				type="hidden" name="submitFrom"
																				value="<%=searchSubmitValue%>"></td>
																			<td align="right" colspan="4"><input
																				type="button" value="Add" class="buttonBg"
																				onclick="addSyncAttendanceOverlay();"
																				style="margin-right: 30px;" /></td>
																		</tr>
																	</table>
																</div>
															</td>
														</tr>

													</table>
												</s:form></td>
										</tr>
										<tr>
											<td>
												<%
                    /* String Variable for storing current position of records in dbgrid*/
                    strTmp1 = request.getParameter("txtCurr1");
                    
                    intCurr1 = 1;
                    
                    if (strTmp1 != null)
                        intCurr1 = Integer.parseInt(strTmp1);
                    
                    /* Specifing Shorting Column */
                    strSortCol1 = request.getParameter("Colname1");
                    
                    if (strSortCol1 == null) strSortCol1 = "Fname";
                    
                    strSortOrd1 = request.getParameter("txtSortAsc1");
                    if (strSortOrd1 == null) strSortOrd1 = "ASC";
                    
                    
                    try{
                        
                        /* Getting DataSource using Service Locator */
                        connection = ConnectionProvider.getInstance().getConnection();
                        
                        /* Sql query for retrieving resultset from DataBase */
                        queryString  =null;
                        queryString = request.getAttribute(ApplicationConstants.QUERY_STRING).toString();
                        
                      //out.println(queryString);
                        
                       
                    %> <s:form action="" theme="simple"
													name="frmDBGrid1">

													<table cellpadding="0" cellspacing="0" width="100%"
														border="0">


														<tr>
															<td width="100%"><grd:dbgrid id="tblStat"
																	name="tblStat" width="98" pageSize="10"
																	currentPage="<%=intCurr%>" border="0" cellSpacing="1"
																	cellPadding="2" dataMember="<%=queryString%>"
																	dataSource="<%=connection%>" cssClass="gridTable">

																	<grd:gridpager
																		imgFirst="../../includes/images/DBGrid/First.gif"
																		imgPrevious="../../includes/images/DBGrid/Previous.gif"
																		imgNext="../../includes/images/DBGrid/Next.gif"
																		imgLast="../../includes/images/DBGrid/Last.gif" />

																	<grd:gridsorter sortColumn="<%=strSortCol%>"
																		sortAscending="<%=blnSortAsc%>"
																		imageAscending="../../includes/images/DBGrid/ImgAsc.gif"
																		imageDescending="../../includes/images/DBGrid/ImgDesc.gif" />


																	<grd:rownumcolumn headerText="SNo" width="4"
																		HAlign="right" />

																	<%--  <grd:imagecolumn headerText="Edit" width="4" HAlign="center" imageSrc="../includes/images/DBGrid/newEdit_17x18.png"
                                                         linkUrl="getEmployee.action?empId={Id}" imageBorder="0"
                                                         imageWidth="16" imageHeight="16" alterText="Click to edit"></grd:imagecolumn> --%>

																	<grd:datecolumn dataField="MONTH" headerText="Month"
																		width="18" />
																	<grd:datecolumn dataField="YEAR" headerText="Year"
																		width="12" />
																	<grd:textcolumn dataField="SyncBy" headerText="SyncBy"
																		width="13" />
																	<%--    <grd:imagecolumn headerText="Services" width="4" HAlign="center" 
                                                                                 imageSrc="../../includes/images/DBGrid/View.gif"
                                                                                 linkUrl="javascript:syncingAttendanceOverlay({MONTH},{YEAR})" imageBorder="0"
                                                                                 imageWidth="20" imageHeight="15" alterText="Services"></grd:imagecolumn>  --%>
																	<grd:imagecolumn headerText="AttendanceLoad" width="4"
																		HAlign="center"
																		imageSrc="../../includes/images/DownloadFile.jpg"
																		linkUrl="javascript:getAttendanceLoad({MONTH},{YEAR})"
																		imageBorder="0" imageWidth="20" imageHeight="15"
																		alterText="Generate"></grd:imagecolumn>

																</grd:dbgrid> <input TYPE="hidden" NAME="txtCurr1"
																VALUE="<%=intCurr1%>"> <input TYPE="hidden"
																NAME="txtSortCol1" VALUE="<%=strSortCol1%>"> <input
																TYPE="hidden" NAME="txtSortAsc1"
																VALUE="<%=strSortOrd1%>"> <input type="hidden"
																name="submitFrom" value="dbGrid"></td>
														</tr>
													</table>

												</s:form> <%
                    connection.close();
                    connection = null;
                    }catch(Exception ex){
                        out.println(ex.toString());
                    }finally{
                        if(connection!= null){
                            connection.close();
                            connection = null;
                        }
                    }
                    %>
											</td>
										</tr>
									</table>
								</div>


								<div id="biometric" class="tabcontent">



									<table cellpadding="0" cellspacing="0" border="0" width="100%">
										<tr align="right">
											<td class="headerText"><img alt="Home"
												src="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/spacer.gif"
												width="100%" height="13px" border="0"></td>
										</tr>
										<tr>
											<td>
												<% if(request.getAttribute(ApplicationConstants.RESULT_MSG) != null){
                                    out.println(request.getAttribute(ApplicationConstants.RESULT_MSG));
                                }
                                
                                if(request.getAttribute("submitFrom") != null){
                                    submittedFrom = request.getAttribute("submitFrom").toString();
                                }
                                
                                if(!"dbGrid".equalsIgnoreCase(submittedFrom)){
                                    searchSubmitValue = submittedFrom;
                                }
                                
                                %>

											</td>
										</tr>
										<tr>
											<td><s:form name="employeeAddForm"
													action="getBiometricSearch" theme="simple">
													<table cellpadding="1" cellspacing="1" border="0"
														width="750px">
														<s:hidden name="empId" id="empId" value="%{empId}" />
														<tr>
															<td class="fieldLabel">Emp No:</td>
															<td><s:textfield name="empNo" id="empNo"
																	maxlength="5" cssClass="inputTextBlue" value="%{empNo}"
																	onkeyup="getEmpNameandId(this.value);isNumeric(this.value);"
																	onchange="clearEmpName();" /></td>
															<%-- <td class="fieldLabel">EmpName:</td>
                                                <td ><s:select list="empnamesList" id="empnameById" name="empnameById" headerKey="" headerValue="Select EmpName" cssClass="inputSelect" value="%{empnameById}"/></td> 
                                                --%>
															<td class="fieldLabel">EmpName:</td>
															<td><s:textfield name="empName" id="empName"
																	value="%{empName}" onkeyup="empNameList();"
																	cssClass="inputTextBlue" theme="simple"
																	readonly="false" onchange="clearEmpNo();" /> <s:hidden
																	name="loginId" id="loginId" value="%{loginId}" />
																<div id="authorEmpValidationMessage"
																	style="position: absolute; overflow: hidden;"></div>
														</tr>



														<tr>

															<td class="fieldLabel">Year(YYYY):<FONT color="red"><em>*</em></FONT></td>
															<td><s:textfield name="year" id="year" maxlength="4"
																	cssClass="inputTextBlue" value="%{year}"
																	onkeyup="isNumericYear(this.value);" /></td>
															<td class="fieldLabel">Month:<FONT color="red"><em>*</em></FONT></td>
															<td><s:select
																	list="#@java.util.LinkedHashMap@{'1':'Jan','2':'Feb','3':'Mar','4':'Apr','5':'May','6':'June','7':'July','8':'Aug','9':'Sept','10':'Oct','11':'Nov','12':'Dec'}"
																	name="month" id="month" headerValue="select"
																	headerKey="0" value="%{month}"
																	cssClass="inputSelectGender" /></td>
														</tr>




														<tr>
															<td class="fieldLabel" align="left">Location&nbsp;:</td>
															<td><s:select name="location" id="location"
																	cssClass="inputSelect" headerKey="All"
																	headerValue="All" list="locationsMap"
																	value="%{location}"></s:select></td>

															<td class="fieldLabel" align="right">Department:</td>
															<td><s:select label="Select Department"
																	name="departmentId" id="departmentId" headerKey=""
																	headerValue="-- Please Select --"
																	list="departmentIdList" cssClass="inputSelect"
																	value="%{departmentId}" onchange="getPracticeDataV1();" /></td>


														</tr>
														<tr>
															<td class="fieldLabel" align="right">Practice
																Name&nbsp;:</td>
															<td><s:select label="Select Practice Name"
																	name="practiceId" id="practiceId" headerKey=""
																	headerValue="-- Please Select --" list="practiceIdList"
																	cssClass="inputSelect" value="%{practiceId}" /> <%--onchange="getTeamData();" --%></td>

															<td class="fieldLabel">Operation&nbsp;Contact&nbsp;:</td>

															<td><s:select label="Select Point of Contact"
																	name="opsContactId" id="opsContactId" headerKey="1"
																	headerValue="All" list="opsContactIdMap"
																	cssClass="inputSelect" value="%{opsContactId}" /></td>

														</tr>




														<tr>

															<td class="fieldLabel">Review Type:</td>
															 <td><s:select
																	list="#@java.util.LinkedHashMap@{'all':'All','reviewed':'Reviewed'}"
																	name="reviewType" id="reviewType"
																	headerValue="Not Reviewed" headerKey="notReviewed"
																	cssClass="inputSelect" /></td>
																	
																	<%--  <td><s:select
																	list="#@java.util.LinkedHashMap@{'all':'All','notReviewed':'Not Reviewed'}"
																	name="reviewType" id="reviewType"
																	headerValue="Reviewed" headerKey="reviewed"
																	cssClass="inputSelect" /></td> --%>  

														</tr>

														<tr>


															<td class="fieldLabel">Is TeamLead&nbsp;:</td>
															<td colspan="1"><s:checkbox name="isTeamLead"
																	id="isTeamLead" theme="simple" /></td>
															<td class="fieldLabel">Is Manager&nbsp;:</td>
															<td colspan="1"><s:checkbox name="isManager"
																	id="isManager" theme="simple" /></td>
														</tr>

														<tr>
															<%-- <td class="fieldLabel">Reporting&nbsp;Person&nbsp;:</td>
                                <td colspan="2"><s:select list="reportingPersons" id="reportingpersonId" name="reportingpersonId" headerKey="All" headerValue="All" cssClass="inputSelectLarge" value="%{reportingpersonId}"/></td> --%>

															<td align="right" colspan="4"><input type="button" id="biometricSearch" 
																	Class="buttonBg" value="Search" onclick="getBiometricDetails()"/> <input
																type="hidden" name="submitFrom"
																value="<%=searchSubmitValue%>"></td>
														</tr>

														<tr>
															<td>
																 <div style="display: none; position: fixed; top:69px;left:242px;overflow:auto;" id="menu-popup">
                        <table id="completeTable" border="1" bordercolor="#e5e4f2" style="border: 1px dashed gray;" cellpadding="0" class="cellBorder" cellspacing="0" ></table>
                    </div>
															</td>
												
														
										
														</tr>
													</table>
												</s:form></td>
										</tr>
										
										<tr>
													<td align="center"><span id="loadingEmpDetails"
														style="display: none;"><font color="red" size="3px">loading
																please wait...</font></span></td>
												</tr>
												
												
										<tr>
											<td>
												<div id="biometricTableReport" style="display: block">


													<!--style="color:#0000FF;font:italic 900 12px arial;"  bgcolor='#3E93D4'-->
													
													
													
													
													<table cellpadding="1" cellspacing="1" border="0" width="750px" id="monthYearTable" style="display:none">
														
														<tbody>
														<tr >
															
																<td class="fieldLabel" style="text-align:left;text-indent:4.5em;">&nbsp;&nbsp;&nbsp;&nbsp;Month&nbsp;&&nbsp;Year&nbsp;:<span id="monthYear"></span></td>
																<!-- <td class="fieldLabel"  id="monthYear"></td> -->
																
																
										
														</tr>
														</tbody>
													</table>
													
													
													
													
													
													
													
													
													
													<table id="biometricTable" align='center'
														cellpadding='1' cellspacing='1' border='0'
														class="gridTable" width='700'>
														<COLGROUP ALIGN="left">
															<COL width="5%">
															<COL width="10%">
															<COL width="10%">
															<COL width="10%">

															<COL width="10%"></td>
                                                                                                    </table> <br>
                                                                                                    <!--<center><span id="spnFast" class="activeFile" style="display: none;"></span></center>-->
                                                                                                </div>
											
															<%
                    /* String Variable for storing current position of records in dbgrid*/
                    strTmp = request.getParameter("txtCurr");
                    
                    intCurr = 1;
                    
                    if (strTmp != null)
                        intCurr = Integer.parseInt(strTmp);
                    
                    /* Specifing Shorting Column */
                    strSortCol = request.getParameter("Colname");
                    
                    if (strSortCol == null) strSortCol = "Fname";
                    
                    strSortOrd = request.getParameter("txtSortAsc");
                    if (strSortOrd == null) strSortOrd = "ASC";
                    
                    
                    try{
                        
                        /* Getting DataSource using Service Locator */
                        connection = ConnectionProvider.getInstance().getConnection();
                         
                        /* Sql query for retrieving resultset from DataBase */
                   
                        queryString = request.getAttribute(ApplicationConstants.BIO_EMP_LIST).toString();
                        //out.println(queryString);
                        /* queryString="SELECT DISTINCT(tblEmployee.EmpNo),tblEmployee.Id, DepartmentId, LName, FName, MName, Email1,"+

   "  WorkPhoneNo, AlterPhoneNo, CellPhoneNo, CurStatus ,LoginId FROM tblEmployee JOIN tblBIOAttendanceLogs ON(tblEmployee.EmpNo=tblBIOAttendanceLogs. EmpNo)"+
 "  WHERE 1=1 AND MONTH(AttendanceDate)='8' AND YEAR(AttendanceDate)='2018' "; */
                     //out.println(queryString);
                        
                    //    out.println("--------"+submittedFrom);
                    %>
                    
                   <%--  <s:form action="" theme="simple" name="frmDBGrid">   
                        
                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                            
                            
                            <tr>
                                <td width="100%">
                                    
                                    <grd:dbgrid id="tblStat" name="tblStat" width="98" pageSize="10"
                                                currentPage="<%=intCurr%>" border="0" cellSpacing="1" cellPadding="2"
                                                dataMember="<%=queryString%>" dataSource="<%=connection%>" cssClass="gridTable">
                                        
                                        <grd:gridpager imgFirst="../../includes/images/DBGrid/First.gif" imgPrevious="../../includes/images/DBGrid/Previous.gif" 
                                                       imgNext="../../includes/images/DBGrid/Next.gif" imgLast="../../includes/images/DBGrid/Last.gif"/>
                                        
                                        <grd:gridsorter sortColumn="<%=strSortCol%>" sortAscending="<%=blnSortAsc%>" 
                                                        imageAscending="../../includes/images/DBGrid/ImgAsc.gif" 
                                                        imageDescending="../../includes/images/DBGrid/ImgDesc.gif"/>   
                                        
                                      
                                        <grd:rownumcolumn headerText="SNo" width="4" HAlign="right"/>
                                        
                                        <grd:imagecolumn headerText="Edit" width="4" HAlign="center" imageSrc="../includes/images/DBGrid/newEdit_17x18.png"
                                                         linkUrl="getEmployee.action?empId={Id}" imageBorder="0"
                                                         imageWidth="16" imageHeight="16" alterText="Click to edit"></grd:imagecolumn>
                                        
                                        <grd:textcolumn dataField="Fname" headerText="FirstName" width="18"/>
                                        <grd:textcolumn dataField="Lname" headerText="LastName" width="12"/>
                                        <grd:datecolumn dataField="EmpNo" headerText="EmployeeNo" width="12"/>
                                        <grd:textcolumn dataField="WorkPhoneNo" headerText="WorkPhone" width="13"/>
                                        <grd:textcolumn dataField="CellPhoneNo" headerText="CellPhone" width="13"/>
                                        <grd:textcolumn dataField="Email1" headerText="EmailId" width="20"/>
                                        <grd:textcolumn dataField="CurStatus" headerText="Status" width="12"/>
                                       <grd:imagecolumn headerText="Termination" width="4" HAlign="center" 
                                                         imageSrc="../includes/images/DBGrid/terminate.png"
                                                         linkUrl="deleteEmployee.action?empId={Id}" imageBorder="0"
                                                         imageWidth="69" imageHeight="20" alterText="Delete"></grd:imagecolumn>
                                        
                                     <grd:imagecolumn headerText="Review" width="4" HAlign="center" 
                                                                                 imageSrc="../../includes/images/DBGrid/View.gif"
                                                                                 linkUrl="getBiometricDetails.action?empId={Id}" imageBorder="0"
                                                                                 imageWidth="20" imageHeight="15" alterText="Delete"></grd:imagecolumn>  
                                    </grd:dbgrid>
                                    
                                    <input TYPE="hidden" NAME="txtCurr" VALUE="<%=intCurr%>">
                                    <input TYPE="hidden" NAME="txtSortCol" VALUE="<%=strSortCol%>">
                                    <input TYPE="hidden" NAME="txtSortAsc" VALUE="<%=strSortOrd%>">
                                    
                                    <input type="hidden" name="submitFrom" value="dbGrid">
                                    
                                </td>
                            </tr>
                        </table>                                
                        
                    </s:form> --%>
                    
                    <%
                    connection.close();
                    connection = null;
                    }catch(Exception ex){
                        out.println(ex.toString());
                    }finally{
                        if(connection!= null){
                            connection.close();
                            connection = null;
                        }
                    }
                    %>
                
														</td>
            </tr>
            </table>
            <%-- </sx:div > --%>
        </div>
        <!--//END TAB : -->
        <%-- </sx:tabbedpanel> --%>
   
     
                <!--//START TAB : -->
                <%--  <sx:div id="employeeSearchTab" label="Employee Search" cssStyle="overflow:auto;"  > --%>
                
    </div>
    <script type="text/javascript">

var countries=new ddtabcontent("accountTabs")
countries.setpersist(false)
countries.setselectedClassTarget("link") //"link" or "linkparent"
countries.init()

    </script>
    <!--//END TABBED PANNEL : -->
</td>
<!--//END DATA COLUMN : Coloumn for Screen Content-->
</tr>
</table>
</td>
</tr>
<!--//END DATA RECORD : Record for LeftMenu and Body Content-->

<!--//START FOOTER : Record for Footer Background and Content-->
<tr class="footerBg">
    <td align="center"><s:include
					value="/includes/template/Footer.jsp" /></td>
</tr>
<!--//END FOOTER : Record for Footer Background and Content-->

</table>
<!--//END MAIN TABLE : Table for template Structure-->
      
      <script type="text/javascript">
                                                                                                                                                                                                       
    function pagerOption(){

        // var paginationSize = document.getElementById("paginationOption").value;
                                                                                                                                                                                                      
        var  recordPage=5;
                                                                                                                                                                                                  
        $('#tblEmpDetails').tablePaginate({navigateType:'navigator'},recordPage,tblEmpDetails);

    }
                                                                                                                                                                                                        
    $(window).load(function() {
    	var attendancesynchtab = $("#attendenceTabActive").val();
    	//alert("attendancesynchtab"+attendancesynchtab);
    	if(attendancesynchtab == "SynchAttenadaceTab"){
    		$("#biometricTab").removeAttr("class");
    		$("#biometric").attr("style","display:none");
    		 $("#syncAttendanceTab").attr("class","selected");
    		 $("#syncAttendance").attr("style","display:block");
    	}
    	else
    		{
    		$("#syncAttendanceTab").removeAttr("class");
    		$("#syncAttendance").attr("style","display:none");
   		 $("#biometricTab").attr("class","selected");
   		$("#biometric").attr("style","display:block");
    		
    		}
    	getBiometricDetails();
    });                                                                                                                                                                                                    
                                                                                                                                                                                                        
                                                                                                                                                                                                           
</script>
<script type="text/JavaScript"
		src="<s:url value="/includes/javascripts/paginationForBiometric.js?ver=1.5"/>"></script>
        
</body>
</html>
