<%-- 
    Document   : ExecutiveDashboardforRequirement
    Created on : Sep 9, 2018, 2:41:06 PM
    Author     : miracle
--%>

<%@ page contentType="text/html; charset=UTF-8" errorPage="../exception/ErrorDisplay.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags" %> 
<%-- <%@ taglib prefix="sx" uri="/struts-dojo-tags" %> --%>
<%@ page import="com.mss.mirage.util.ApplicationConstants"%>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="com.mss.mirage.util.ConnectionProvider"%>
<html>
    <head>
        <title>Hubble Organization Portal :: DashBoard</title>
        <link rel="stylesheet" type="text/css" href="<s:url value="/includes/css/GridStyle.css"/>">
        <link rel="stylesheet" type="text/css" href="<s:url value="/includes/css/mainStyle.css"/>">
        <link rel="stylesheet" type="text/css" href="<s:url value="/includes/css/tabedPanel.css"/>">
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/animatedcollapse.js"/>"></script>
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/jquery-1.2.2.pack.js"/>"></script>
        <link rel="stylesheet" type="text/css" href="<s:url value="/includes/css/leftMenu.css"/>">
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/AccountDetailsClientValidation.js"/>"></script>   
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/CalendarTime.js"/>"></script>
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/ClientValidations.js"/>"></script>
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/AppConstants.js"/>"></script>
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/Activity.js"/>"></script>
        <%--  <script type="text/JavaScript" src="<s:url value="/includes/javascripts/oppdashboardAjaxUtil.js"/>"></script> --%>
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/GreensheetListSearch.js"/>"></script>
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/GreensheetListResultSearch.js"/>"></script>
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/EnableEnter.js"/>"></script>
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/ReusableContainer.js"/>"></script>
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/tabedPanel.js"/>"></script>
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/employee/DateValidator.js"/>"></script>
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/EmpStandardClientValidations.js"/>"></script>        
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/recruitment/ExecutiveDashboardforRequirement.js?ver=7.9"/>"></script>
        <script type="text/javascript" src="https://www.google.com/jsapi"></script> 
        <s:include value="/includes/template/headerScript.html"/>
        <script language="JavaScript">
            
         
            animatedcollapse.addDiv('recByTeamMembers', 'fade=1;speed=400;persist=1;group=app');
            animatedcollapse.init();
            
          
        </script>
        <style>
            #parent {
                height: auto;

                width:780px;
                max-height: 400px;
                overflow: visible;
            }
           
        </style>
    </head>
    <body  class="bodyGeneral" onload="" oncontextmenu="return false;">
        <%!
            /*
             * Declarations
             */
            String queryString = null;
            Connection connection;
            Statement stmt;
            ResultSet rs;
            int userCounter = 0;
            int activityCounter = 0;
            int accountCounter = 0;
        %>


        <!--//START MAIN TABLE : Table for template Structure-->
        <table class="templateTable1000x580" align="center" cellpadding="0" cellspacing="0">

            <!--//START HEADER : Record for Header Background and Mirage Logo-->
            <tr class="headerBg">
            <td valign="top">
                <s:include value="/includes/template/Header.jsp"/>                    
            </td>
        </tr>
        <!--//END HEADER : Record for Header Background and Mirage Logo-->

        <!--//START DATA RECORD : Record for LeftMenu and Screen Content-->
        <tr>
        <td>
            <table class="innerTable1000x515" cellpadding="0" cellspacing="0">
                <tr>
                    <!--//START DATA COLUMN : Coloumn for LeftMenu-->
                <td width="850px;" class="leftMenuBgColor" valign="top"> 
                    <s:include value="/includes/template/LeftMenu.jsp"/>
                </td>
                <!--//START DATA COLUMN : Coloumn for LeftMenu-->

                <!--//START DATA COLUMN : Coloumn for Screen Content-->
                <td width="850px" class="cellBorder" valign="top" style="padding-left:10px;padding-top:5px;">
                    <!--//START TABBED PANNEL : -->
                    <%--      <sx:tabbedpanel id="resetPasswordPannel" cssStyle="width: 845px; height: 550px;padding-left:10px;padding-top:5px;" doLayout="true" useSelectedTabCookie="true" > 
                        
                        <!--//START TAB : -->
                        <sx:div id="dashBoardTab" label="DashBoard Details" cssStyle="overflow:auto;"> --%>

                    <ul id="accountTabs" class="shadetabs" >
                        <li><a href="#" rel="accountsListTab" class="selected"> DashBoard Details </a></li>
                    </ul>

                    <div  style="border:1px solid gray; width:840px;height: 550px;overflow:auto; margin-bottom: 1em;">    
                        <div id="accountsListTab" class="tabcontent" >  

     

                                      
                                                                                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                                                            <tr>
                                                                                                            <td height="20px" >

                                                                                                            </td>
                                                                                                            </tr>                                            



                                                                                                                                                                                                                                                                                                                                                                                            
                                                                                                                                                                        
                                                                                                                                                                        
                                                                                                                                                                                                                                                                              
                                                                                                           <!--  New div for Requirements By Team members start -->
                                                                                                                                                                        
                                                                                                              <tr>
                                                                                                            <td class="homePortlet" valign="top">
                                                                                                                <div class="portletTitleBar">
                                                                                                                    <div class="portletTitleLeft">Recruitment Team Performance Report</div>
                                                                                                                    <div class="portletIcons">
                                                                                                                        <a href="javascript:animatedcollapse.hide('recByTeamMembers')" title="Minimize">
                                                                                                                            <img src="../../includes/images/portal/title_minimize.gif" alt="Minimize"/></a>
                                                                                                                        <a href="javascript:animatedcollapse.show('recByTeamMembers')" title="Maximize">
                                                                                                                            <img src="../../includes/images/portal/title_maximize.gif" alt="Maximize"/>
                                                                                                                        </a>                                                           
                                                                                                                    </div>
                                                                                                                    <div class="clear"></div>
                                                                                                                </div>
                                                                                                                <div id="recByTeamMembers" style="background-color:#ffffff">
                                                                                                                    <table cellpadding="0" cellspacing="0" border="0" width="100%"> 
                                                                                                                        <tr>
                                                                                                                        <td width="40%" valign="top" align="center">
                                                                                                                            <s:form action="#" theme="simple" name="dashboardTeamMembers" >   

                                                                                                                                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                                                                                    <tr align="right">
                                                                                                                                    <td class="headerText" colspan="9">
                                                                                                                                        <img src="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/spacer.gif" width="100%" height="13px" border="0">                                                        
                                                                                                                                    </td>
                                                                                                                                    </tr>
                                                                                                                                    <tr>
                                                                                                                                    <td>
                                                                                                                                         <table border="0" align="center" cellpadding="0" cellspacing="0">
                                                                                                                                         
                                                                                                                                            <tr>
                                                                                                                                            <td class="fieldLabel">FromDate:<FONT color="red"  ><em>*</em></FONT></td> <!--value="%{dateWithOutTime}" -->


                                                                                                                                            <td><s:textfield name="fromDateReport5" id="fromDateReport5" cssClass="inputTextBlue" onchange="checkDates(this);"/><a href="javascript:calReport9.popup();"><img src="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/showCalendar.gif" width="20" height="20" border="0"></a>
                                                                                                                                            </td>
                                                                                                                                            
                                                                                                                                            <td class="fieldLabel">ToDate:<FONT color="red"  ><em>*</em></FONT></td>

                                                                                                                                            <td><s:textfield name="toDateReport5" id="toDateReport5" cssClass="inputTextBlue" onchange="checkDates(this);"/><a href="javascript:calReport11.popup();"><img src="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/showCalendar.gif" width="20" height="20" border="0"></a>
                                                                                                                                            </td>
                                                                                                                                            </tr>
                                                                                                                                             
                                                                                                                                                                                                                                                            
                                                                                                                                            <tr>
                                                                                                                                            <td class="fieldLabel">Team member name:</td>
                                                                                                                                            <td><s:select headerKey="" headerValue="Please Select" list="recruiterMap1" name="recruiterLoginId3" id="recruiterLoginId3" cssClass="inputSelect" onchange="disableFlag();" value="%{recruiterLoginId3}"/></td>
                                                                                                                                            
                                                                                                                                      
                                                                                                                                      <td  class="fieldLabel">
																								                                       <input name="expFlag"
																								                                     id="includeTeam" 
																							                                     	type="checkbox">:
																							                         	     Include&nbsp;Team&nbsp;Members</td>
                                                                                                                                            
                                                                                                                                           
                                                                                                                                            <td>
                                                                                                                                               <input type="button" value="Search" class="buttonBg" onclick="getTeamMembers();"/>
                                                                                                                                            </td>
                                                                                                                                            </tr> 
                              
                                                                                                                                    </table>
                                                                                                                                    </td>
                                                                                                                                    </tr>

                                                                                                                                    <tr>
                                                                                                                                    <td align="center" colspan="6">
                                                                                                                                        <div id="loadActTeamMembers" style="display:none" class="error" ><b>Loading Please Wait.....</b></div>
                                                                                                                                    </td>
                                                                                                                                    </tr>
                                                                                                                                    
                                                                                                                                    <tr>
                                                                                                                                    <td>

                                                                                                                                        <table align="center" cellpadding="2" border="0" cellspacing="1" width="50%" >
                                                                                                                                                                                                                                  

                                                                                                                                            <tr>
                                                                                                                                            <td colspan="2">
                                                                                                                                                <br>
                                                                                                                                               
                                                                                                                                                    <table id="tblteamMembers" align='center' cellpadding='1' cellspacing='1' border='0' class="gridTable" width='750'>
                                                                                                                                                        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/wz_tooltip.js"/>"></script>
                                                                                                                                                        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/wz_tooltipNew.js"/>"></script>
                                                                                                                                                        <COLGROUP ALIGN="left" >
                                                                                                                                                            <COL width="5%">
                                                                                                                                                                <COL width="5%">
                                                                                                                                                                    <COL width="5%">
                                                                                                                                                                        <COL width="5%">
                                                                                                                                                                          <COL width="5%">
                                                                                                                                                                         <COL width="5%">
                                                                                                                                                                         <COL width="5%">
                                                                                                                                                                          <COL width="5%">
                                                                                                                                                                           <COL width="5%">
                                                                                                                                                                          <COL width="5%">
                                                                                                                                                                           </table>
                                                                                                                                                                            <br>
                                                                                                                                                                        
                                                                                                                                                                         
                                                                                                                                                                            </td>
                                                                                                                                                                            </tr>

                                                                                                                                                                            </table>
                                                                                                                                                                            </td></tr>

                                                      
                                                                                                                                                                            </table>
                                                                                                                                                                        </s:form>
                                                                                                                                                                        </td>
                                                                                                                                                                        </tr>
                                                                                                                                                                        </table>

                                                                                                                                                                        <script type="text/JavaScript">
                                                                                                                                                                            var calReport9 = new CalendarTime(document.forms['dashboardTeamMembers'].elements['fromDateReport5']);
                                                                                                                                                                            calReport9.year_scroll = true;
                                                                                                                                                                            calReport9.time_comp = false;
                                                                                                                                                                            var calReport11 = new CalendarTime(document.forms['dashboardTeamMembers'].elements['toDateReport5']);
                                                                                                                                                                            calReport11.year_scroll = true;
                                                                                                                                                                            calReport11.time_comp = false;
                                                                                                                                                                           
                                                                        
                                                                                                                                                                        </script>
                                                                                                                                                                        </div>
                                                                                                                                                                        </td>
                                                                                                                                                                        </tr>
                                                                                                                                                                        
                                                                                                                                                                        
                                                                                                                                                                        
                                                                                                                                                                        
                                                                                                                                                                        <!--  New div for Requirements By Team members end  -->
                                                                                                                                                                        

                                                                                                                                                                        <!-- new div for account list by priority end -->

                                                                                                                                                                        </table>

                                                                                                                                                                        <%--     </sx:div>
                                                                                                                                                                </sx:tabbedpanel> --%>
                                                                                                                                                                        <!--//END TABBED PANNEL : --> 
                                                                                                                                                                        </div>
                                                                                                                                                                        </div>
                                                                                                                                                                        <script type="text/javascript">

                                                                                                                                                                            var countries=new ddtabcontent("accountTabs")
                                                                                                                                                                            countries.setpersist(false)
                                                                                                                                                                            countries.setselectedClassTarget("link") //"link" or "linkparent"
                                                                                                                                                                            countries.init()
                                                                                                                                                                          

                                                                                                                                                                        </script>
                                                                                                                                                                        
                                                                                                                                                                  <script type="text/javascript">
                                                                                                                                                                $(window).load(function(){
                                                                                                                                                                
                                                                                                                                                                	disableFlag();
                                                                                                                                                                  
                                                                                                                                                                });
                                                                                                                                                            </script>
                                                                                                                                                                        
                                                                                                                                                                        
                                                                                                                                                                        

                                                                                                                                                                        </td>
                                                                                                                                                                        <!--//END DATA COLUMN : Coloumn for Screen Content-->
                                                                                                                                                                        </tr>
                                                                                                                                                                        </table>
                                                                                                                                                                        <script type="text/javascript">
                                                                                                                                                                                                       
                                                                                                                                                                                                          
                                                                                                                                                                                                        
                                                                                                                                                                            function pagerOption(){

                                                             
                                                                                                                                                                                var  recordPage=20;
                                                                                                                                                                                $('#tblSubmitReport').tablePaginate({navigateType:'navigator'},recordPage,tblSubmitReport);

                                                                                                                                                                                                                 
      

                                                                                                                                                                            }
                                                                                                                                                                                                                                                       </script>
                                                                                                                                                                        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/DynamicPagination.js"/>"></script>

                                                                                                                                                                        </td>
                                                                                                                                                                        </tr>
                                                                                                                                                                        <!--//END DATA RECORD : Record for LeftMenu and Body Content-->

                                                                                                                                                                        <!--//START FOOTER : Record for Footer Background and Content-->
                                                                                                                                                                        <tr class="footerBg">
                                                                                                                                                                        <td align="center"><s:include value="/includes/template/Footer.jsp"/></td>
                                                                                                                                                                        </tr>
                                                                                                                                                                        <!--//END FOOTER : Record for Footer Background and Content-->
                                                                                                                                                                        </table>
                                                                                                                                                                        <!--//END MAIN TABLE : Table for template Structure-->
                                                                                                                                                                        </body>

                                                                                                                                                                        </html>
