<%-- 
    Document   : pmoDashBoard
    Created on : Dec 18, 2015, 2:46:51 PM
    Author     : miracle
--%>

<%@page import="java.util.Map"%>
<%-- 
    Document   : pmoDashBoard
    Created on : Dec 17, 2015, 8:50:01 PM
    Author     : miracle
--%>


<%@ page contentType="text/html; charset=UTF-8" errorPage="../exception/ErrorDisplay.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags" %> 
<%-- <%@ taglib prefix="sx" uri="/struts-dojo-tags" %> --%>
<%@ page import="com.mss.mirage.util.ApplicationConstants"%>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="com.mss.mirage.util.ConnectionProvider"%>
<html>
    <head>
        <title>Hubble Organization Portal :: TimeSheet Audit</title>
        <link rel="stylesheet" type="text/css" href="<s:url value="/includes/css/GridStyle.css"/>">
        <link rel="stylesheet" type="text/css" href="<s:url value="/includes/css/mainStyle.css"/>">
        <link rel="stylesheet" type="text/css" href="<s:url value="/includes/css/tabedPanel.css"/>">
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/animatedcollapse.js"/>"></script>
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/jquery-1.2.2.pack.js"/>"></script>
        <link rel="stylesheet" type="text/css" href="<s:url value="/includes/css/leftMenu.css"/>">
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/CalendarTime.js"/>"></script>
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/AppConstants.js"/>"></script>
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/Activity.js"/>"></script>
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/EnableEnter.js"/>"></script>
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/ReusableContainer.js"/>"></script>
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/tabedPanel.js"/>"></script>
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/employee/DateValidator.js"/>"></script>
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/pmoDashBoardAjax.js?version=3.1"/>"></script>
        <script type="text/javascript" src="<s:url value="/includes/javascripts/IssueFill.js"/>"></script>

        <%-- <script type="text/JavaScript" src="<s:url value="/includes/javascripts/payroll/payrollajaxscript.js"/>"></script>
         <script type="text/JavaScript" src="<s:url value="/includes/javascripts/payroll/payrollclientvalidations.js"/>"></script> --%>
        <s:include value="/includes/template/headerScript.html"/>
        <script language="JavaScript">
         animatedcollapse.addDiv('consolidateReport', 'fade=1;persist=1;group=app');
            animatedcollapse.init();
            function hideSelect(){
                //document.getElementById("priorityId").style.display = 'none';
                
            }
           
        </script>

        <style type="text/css">

            .popupItem:hover {
                background: #F2F5A9;
                font: arial;
                font-size:10px;
                color: black;
            }

            .popupRow {
                background: #3E93D4;
            }

            .popupItem {
                padding: 2px;
                width: 100%;
                border: black;
                font:normal 9px verdana;
                color: white;
                text-decoration: none;
                line-height:13px;
                z-index:100;
            }


.totalCss {
    background-color: #b0c4de;
    border: 1px solid #fffaf0;
    color: #000;
    font-weight: bold;
    
}

.Main0 {
    background-color: #00BFFF;
   
    
}
.Main1 {
    background-color: #e1bfff;
   
    
}

.Shadow0 {
    background-color:  	#ADD8E6;
   
    
}
.Shadow1 {
    background-color: #E0FFFF;
   
    
}
    .filled {
    background-color: #F0E68C;
   
    
}
.readOnlyCss{
/* background-color: #b0c4de; */
border-width: 0;
}
        </style>
        

    </head>
     
    <body  class="bodyGeneral"  oncontextmenu="return false;">

        

        <%!
            /* Declarations */
            String queryString = null;
            Connection connection;
            Statement stmt;
            ResultSet rs;
            int userCounter = 0;
            int activityCounter = 0;
            int accountCounter = 0;
        %>


        <!--//START MAIN TABLE : Table for template Structure-->
        <table class="templateTable1000x580" align="center" cellpadding="0" cellspacing="0">

            <!--//START HEADER : Record for Header Background and Mirage Logo-->
            <tr class="headerBg">
            <td valign="top">
                <s:include value="/includes/template/Header.jsp"/>                    
            </td>
        </tr>
        <!--//END HEADER : Record for Header Background and Mirage Logo-->

        <!--//START DATA RECORD : Record for LeftMenu and Screen Content-->
        <tr>
        <td>
            <table class="innerTable1000x515" cellpadding="0" cellspacing="0">
                <tr>
                    <!--//START DATA COLUMN : Coloumn for LeftMenu-->
                <td width="850px;" class="leftMenuBgColor" valign="top"> 
                    <s:include value="/includes/template/LeftMenu.jsp"/>
                </td>
                <!--//START DATA COLUMN : Coloumn for LeftMenu-->

                <!--//START DATA COLUMN : Coloumn for Screen Content-->
                <td width="850px" class="cellBorder" valign="top" style="padding-left:10px;padding-top:5px;">
                    <!--//START TABBED PANNEL : -->
                    <%--      <sx:tabbedpanel id="resetPasswordPannel" cssStyle="width: 845px; height: 550px;padding-left:10px;padding-top:5px;" doLayout="true" useSelectedTabCookie="true" > 
                        
                        <!--//START TAB : -->
                        <sx:div id="dashBoardTab" label="DashBoard Details" cssStyle="overflow:auto;"> --%>
                    <ul id="reportsTab" class="shadetabs" >
                        <li><a href="#" rel="EmpReportsTab" class="selected">Time&nbsp;Sheet&nbsp;Audit&nbsp;Report</a></li>

                    </ul>
                    <div  style="border:1px solid gray; width:840px;height:490px;overflow:auto; margin-bottom: 1em;">    
                        <div id="EmpReportsTab" class="tabcontent" > 


					<div id="commentsOverlay" class="overlay"></div>
						<div id="commentsSpecialBox" class="specialBox"
										style="border-radius: 30px; background: transparent;">
										<s:form theme="simple" align="center">

											
											<input type="hidden" id="commentsId" value="" />
											<input type="hidden" id="auditId" value="" />
											<table align="center" border="0" cellspacing="0"
												style="background: white; width: 87%; border-radius: 30px 30px 30px 30px; xborder-collapse: collapse;">
												<td colspan=""
													style="background-color: #288AD1; width: 100%; border-radius: 30px 0px 0px 0px; xborder-collapse: collapse;">
													<h3 style="color: darkblue;" align="left">
														<span id="headerLabel" style="margin-left: 10px;color:white">Comments</span>


													</h3>
												</td>
												<td colspan=""
													style="background-color: #288AD1; width: 70%; border-radius: 0px 30px 0px 0px; xborder-collapse: collapse;"
													align="right"><a href="javascript:closeComments();"
													
													style="margin-right: 10px;"> <img
														src="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/closeButton.png" />

												</a></td>
												</tr>
												<tr style="border-radius: 0px 0px 30px 30px;">
													<td colspan="2">
														<div id="load1" style="color: green; display: none;">Loading..</div>
														<div id="resultMessage1"></div>
													</td>
												</tr>
												<tr
													style="border-radius: 0px 0px 30px 30px; xborder-collapse: collapse;">
													<td colspan="2"
														style="border-radius: 0px 0px 30px 30px; border: 1px solid #999; border-top: 0; xborder-collapse: collapse;">
														<table style="width: 80%;" align="center" border="0">

															<tr id="empDescriptionTr">
																<td colspan="2"><s:textarea style="width:476px;"
																		rows="5" cols="70"
																		onfocus="countChar(this,'descriptionCount');"
																		onkeyup="countChar(this,'descriptionCount');"
																		placeholder="Enter your comments here...."
																		name="description" cssClass="inputTextareaOverlay1"
																		
																		id="comments" /></td>
																<td><lable style="color:green"
																		id="descriptionCount"></lable></td>
															</tr>




															<tr id="addTr" style="display: none">

																
																	<td colspan="2" align="right"><input
																		type="button" value="Add"
																		onclick="addComments();" class="buttonBg"
																		id="addButton" /></td>
																
															</tr>
															<tr id="updateTr" style="display: none">

																<%--   <td class="fieldLabel">Job&nbsp;Logo:</td>
                                             <td colspan="2" ><s:file name="file" label="file" cssClass="inputTextarea" id="file" onchange="attachmentFileNameValidate();"/></td>  --%>
																
																	<td colspan="2" align="right"><input
																		type="button" value="Update"
																		onclick="updateComments();" class="buttonBg"
																		id="addButton" /></td>
																
															</tr>
															<tr>
																<td colspan="2"></td>
																<td><lable style="display:none">Testing</lable></td>
															</tr>
														</table>
													</td>
												</tr>
											</table>
										</s:form>
									</div>
                            <!-- Start Overlay -->                                      
                                      
								<div id="overlayTimeSheetAudit" class="overlay"></div>

									<div id="specialBoxTimeSheetAudit"
										 class="specialBox" style="width: 783px;top: 36px;background: rgba(0, 0, 0, 0) none ;">

										
											<table align="center" border="0" cellspacing="0"
												width="100%"  style="background-color: #fff">
												<tr>
													<td colspan="2" style="background-color: #288AD1">
														<h3 style="color: #fff;" align="left">
															<span id="headerLabel2">Time sheet details</span>


														</h3>
													</td>
													<td colspan="2" style="background-color: #288AD1"
														align="right"><a href="#"
														onmousedown="TimeSheetAuditOverlay()"> <img
															src="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/closeButton.png" />

													</a></td>
												</tr>
												<tr>
													<td colspan="4">
														<div id="load" style="color: green; display: none;">Loading..</div>
														<div id="resultMessage"></div>
													</td>
												</tr>

												<tr>
													<td>
														<div style="width: 783px;height:400px;overflow-y:scroll;">
														<table id="tblTimeSheetDayByDayReport" class="gridTable" width="50%"
															 cellspacing="1" cellpadding="7" border="0"
															align="center" style="margin-left: 20px;">
															<%--   <script type="text/JavaScript" src="<s:url value="/includes/javascripts/wz_tooltip.js"/>"></script> --%>
														
														</table>
														<table id="infoTblId" border='0' width="80%" style="display:none;">
													<tr>
													<td colspan="4" class='gridRowEven' style="background:none;font-weight: bold; ">
													Total Working hrs: <font id="TotlalWorkHRs" color="red"> 160</font>
													</td>
													<td  colspan="4" class='gridRowEven' style="background:none; font-weight: bold;">
													Total Project hrs: <font id="TotlalProjectHrs" color="red"> 20</font>
													</td>
													<td colspan="4"></td>
													<td><s:if test="%{#session.sessionEmpPractice == 'Accounts Payable' || #session.sessionEmpPractice == 'Accounts Receivable' || #session.isAdminAccess==1}" > 
													<!--   <input id="updateeinvoicebtn" style="display: block;" type="button" value="Update" class="buttonBg" onclick="updateInvoiceHrsDetails();"/> -->
                                                   <label  id="updateeinvoicebtn" align="right"></label>
                                                   </s:if>
                                                   <s:else>
                                                   <label  id="updateeinvoicebtn" align="right"></label>
                                                   </s:else>
													</td>
													</tr>
													<tr>
													<td colspan="4" class='gridRowEven' style="background:none;font-weight: bold; ">
													Billable Hrs: <font id="TotlalBillableHRs" color="red"> 160</font>
													</td>
													<td  colspan="4" class='gridRowEven' style="background:none; font-weight: bold;">
													Non Billable Hrs: <font id="TotlalNonBillableHrs" color="red"> 20</font>
													</td>
													
													
													</tr>
													<tr>
													<td class='Main1' style="height: 3px; width: 18px;"></td><td class='gridRowEven' style="background:none;">Main Billable</td>
													
													
													<td class='Main0' style="height: 3px; width: 18px;"></td><td class='gridRowEven' style="background:none;"> Main Not billable</td>
													
													<td class='Shadow1' style="height: 3px; width: 18px;"></td><td class='gridRowEven' style="background:none;">Shadow Billable</td>
													
													<td class='Shadow0' style="height: 3px; width: 18px;"></td><td class='gridRowEven' style="background:none;">Shadow Not billable</td>
													
													<td class='filled' style="height: 3px; width: 18px;"></td><td class='gridRowEven' style="background:none;">Just Filled</td>
													<tr>
													</table>
														</div>
														
													</td>

												</tr>
												<tr>
													<td>
													
													
													
                                                   </td>  
                                                   <td></td>                                                <br></div> </td>
												</tr>
											</table>
											 

										
									</div>
                            	


                                                            <!-- End Overlay -->
                                                            <table cellpadding="0" cellspacing="0" border="0" width="100%">

                                                                <!-- employee timesheet Consolidate report dashboard Start  -->
                                                                <tr>
                                                                <td  valign="top">
<!--                                                                    <div class="portletTitleBar">
                                                                        <div class="portletTitleLeft">Time&nbsp;Sheet&nbsp;Audit&nbsp;Report</div>
                                                                        <div class="portletIcons">
                                                                            <a href="javascript:animatedcollapse.hide('consolidateReport')" title="Minimize">
                                                                                <img src="../includes/images/portal/title_minimize.gif" alt="Minimize"/></a>
                                                                            <a href="javascript:animatedcollapse.show('consolidateReport')" title="Maximize">
                                                                                <img src="../includes/images/portal/title_maximize.gif" alt="Maximize"/>
                                                                            </a>
                                                                        </div>
                                                                        <div class="clear"></div>
                                                                    </div>-->
                                                                    <div id="ProjectList" style="background-color:#ffffff">
                                                                        <table cellpadding="0" cellspacing="0" border="0" width="100%"> 
                                                                            <tr>
                                                                            <td width="40%" valign="top" align="center">
                                                                                <s:form name="frmSearch" id="frmSearch" action="" theme="simple" >  

                                                                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                                        <tr align="right">
                                                                                        <td class="headerText" colspan="9">
                                                                                            <img src="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/spacer.gif" width="100%" height="13px" border="0">                                                        
                                                                                        </td>
                                                                                        </tr>

                                                                                        <tr>
                                                                                        <td>
                                                                                            <%
                                                                                                if (session.getAttribute("resultMessageForUtility") != null) {
                                                                                                    out.println(session.getAttribute("resultMessageForUtility"));
                                                                                                    session.removeAttribute("resultMessageForUtility");
                                                                                                }

                                                                                            %>
                                                                                        </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                        <td>
                                                                                        <s:hidden value="%{#session.sessionEmpPractice}" name="practiceTitle" id="practiceTitle"/>
                                                                                        <s:hidden value="%{#session.roleName}" name="roleName" id="roleName"/>
                                                                                            <table cellpadding="0" cellspacing="2" border="0" width="100%">
                                                                                               
                                                                                                <tr>
									                                                                <td class="fieldLabel">Year(YYYY):</td>
									                                                                <td>
									
									                                                                    <s:textfield name="year" id="year" maxlength="4" cssClass="inputTextBlue" value="%{year}" onblur="yearValidation(this.value,event)" onkeypress="yearValidation(this.value,event)"/>
									                                                                </td>
									                                                                <td class="fieldLabel">Month:</td>
									                                                                <td><s:select list="#@java.util.LinkedHashMap@{'1':'Jan','2':'Feb','3':'Mar','4':'Apr','5':'May','6':'June','7':'July','8':'Aug','9':'Sept','10':'Oct','11':'Nov','12':'Dec'}" name="month" id="month" onchange="" headerValue="select" headerKey="" value="%{month}"/></td>
									                                                            </tr>
																								<tr>
																								
																								 <td class="fieldLabel">Cost&nbsp;Model :&nbsp;</td>
															                                    <td > 
															                                        <s:select name="costModel"  id="costModel" onchange="getCustomerDetailsByCostModel(this);" cssClass="inputSelect" list="{'Fixed','Time & Material','Internal'}"  value="%{costModel}"  emptyOption="false" headerKey="-1" headerValue="--Please Select--" />
															                                    </td>
																	                                   <td class="fieldLabel">Customer Name :</td>
																	                                    <td>
																	                                       
																	                                        <s:select headerKey="" headerValue="All" list="clientMap" name="customerName" id="customerName" cssClass="inputSelect" theme="simple" onchange="getProjectsByAccountId();"/>
																	                                    </td>
																	                                    <tr>
																	                                    <td class="fieldLabel">Resource&nbsp;Name&nbsp;:</td>
                                                                                                <td ><s:textfield name="assignedToUID" id="assignedToUID" value="%{assignedToUID}" onblur="onBlurEmployeeForTimeSheetHrs()" onfocus="onFocusEmployeeForTimeSheetHrs()"  onkeyup="EmployeeForTimeSheetHrs();"  cssClass="inputTextBlue"  theme="simple" readonly="false"/>
                                                                                                        <div id="authorEmpValidationMessage" style="position: absolute; overflow:hidden;"></div>  
                                                                                                        <s:hidden name="preAssignEmpId" value="%{preAssignEmpId}" id="preAssignEmpId"/>  
                                                                                                </td> 
																	                                   <td class="fieldLabel">ProjectName :</td>
																	                                    <td>
																	                                        
																	                                         <s:select headerKey="" headerValue="All"  list="{}" name="projectName" id="projectName" cssClass="inputSelect" theme="simple"/>
																	                                    </td>
																	                               

                                                                                                
                                                                                                
                                                                                               
																								</tr>
																								<tr>
																								<td></td><td></td><td></td>
                                                                                                    <td><input type="button" value="Load For Auditing" class="buttonBg" onclick="getLoadForAudit();"/>
                                                                                                    <input type="button" value="Search" class="buttonBg" onclick="getTimeSheetAuditSearch();"/>
                                                                                                   
                                                                                                </td>
                                                                                                </tr>

                                                                                            </table>
                                                                                        </td>
                                                                                        </tr>

                                                                                        <%-- table grid --%>
                                                                                        <tr>
                                                                                        <td>
                                                                                            <br>
                                                                                            <table align="center" cellpadding="2" border="0" cellspacing="1" width="50%" >

                                                                                                <tr>
                                                                                                <td height="20px" align="center" colspan="9">
                                                                                                    <div id="loadInvoiceHrs" style="display:none" class="error" ><b>Loading Please Wait.....</b></div>
                                                                                                </td>
                                                                                                </tr>


                                                                                                <tr>
                                                                                                <td ><br>
                                                                                                    <div id="pmoProjectDetailsReport" style="display: block">
                                                                                                        <!--style="color:#0000FF;font:italic 900 12px arial;"  bgcolor='#3E93D4'-->
                                                                                                        <table id="tblEmpConsalodateReportList" align='center' i cellpadding='1' cellspacing='1' border='0' class="gridTable" width='750'>
                                                                                                            <COLGROUP ALIGN="left" >
                                                                                                                <COL width="5%">
                                                                                                                    <COL width="20%">
                                                                                                                        <COL width="20%">
                                                                                                                            <COL width="10%">
                                                                                                                                <COL width="10%">
                                                                                                                                    <COL width="10%">
                                                                                                                                        </table> 
                                                                                                                         <s:if test="%{#session.sessionEmpPractice == 'Accounts Payable' || #session.sessionEmpPractice == 'Accounts Receivable' || #session.roleName=='Operations' || #session.isAdminAccess==1}" >             
                                                                                                          <div  align="right"><br> <input id="saveinvoicebtn" style="display: none;" type="button" value="Save" class="buttonBg" onclick="saveInvoiceHrsDetails();"/>
                                                                                                          </s:if>   <s:else>
                                                                                                          <div id="saveinvoicebtn" align="right"></div>
                                                                                                          </s:else>
                                                                                                     </div>                              
                                                                                                          <br>
                                                                                                                                        <!--<center><span id="spnFast" class="activeFile" style="display: none;"></span></center>-->
                                                                                                                                        </div>
                                                                                                                                        </td>
                                                                                                                                        </tr>
                                                                                                                                        </table>    
                                                                                                                                        </td>
                                                                                                                                        </tr>
                                                                                                                                        <%-- table grid  end--%>
                                                                                                                                        </table>
                                                                                                                                    </s:form>
                                                                                                                                    </td>
                                                                                                                                    </tr>
                                                                                                                                    </table>

                                                                                                                                    </div>
                                                                                                                                   <%-- <script type="text/JavaScript">
                                                                                                                                        var cal8 = new CalendarTime(document.forms['empConsolidateSearch'].elements['empConsolidateStartDate']);
                                                                                                                                        cal8.year_scroll = true;
                                                                                                                                        cal8.time_comp = false;
                                                           
                                                                                                                                        var cal9 = new CalendarTime(document.forms['empConsolidateSearch'].elements['empConsolidateEndDate']);
                                                                                                                                        cal9.year_scroll = true;
                                                                                                                                        cal9.time_comp = false;
                                                                                                                                    </script> --%>
                                                                                                                                    </td>
                                                                                                                                    </tr>

                                                                                                                                    <%-- employee timesheet Consolidate report dashboard ENd --%>  

                                                                                                                                    </table>

                                                                                                                                    <%--     </sx:div>
                                                                                                                                    </sx:tabbedpanel> --%>
                                                                                                                                    <!--//END TABBED PANNEL : --> 
                                                                                                                                    </div>
                                                                                                                                    </div>
                                                                                                                                    <script type="text/javascript">

                                                                                                                                        var countries=new ddtabcontent("reportsTab")
                                                                                                                                        countries.setpersist(false)
                                                                                                                                        countries.setselectedClassTarget("link") //"link" or "linkparent"
                                                                                                                                        countries.init()

                                                                                                                                    </script>

                                                                                                                                    </td>
                                                                                                                                    <!--//END DATA COLUMN : Coloumn for Screen Content-->
                                                                                                                                    </tr>
                                                                                                                                    </table>
                                                                                                                                    </td>
                                                                                                                                    </tr>
                                                                                                                                    <!--//END DATA RECORD : Record for LeftMenu and Body Content-->

                                                                                                                                    <!--//START FOOTER : Record for Footer Background and Content-->
                                                                                                                                    <tr class="footerBg">

                                                                                                                                    <td align="center"><s:include value="/includes/template/Footer.jsp"/></td>
                                                                                                                                    </tr>
                                                                                                                                    <tr>
                                                                                                                                    <td>

                                                                                                                                        <div style="display: none; position: absolute; top:170px;left:320px;overflow:auto;" id="menu-popup">
                                                                                                                                            <table id="completeTable" border="1" bordercolor="#e5e4f2" style="border: 1px dashed gray;" cellpadding="0" class="cellBorder" cellspacing="0" />
                                                                                                                                        </div>

                                                                                                                                    </td>
                                                                                                                                    </tr>

                                                                                                                                    <!--//END FOOTER : Record for Footer Background and Content-->
                                                                                                                                    </table>
                                                                                                                                    <!--//END MAIN TABLE : Table for template Structure-->
                                                                                                                                    </body>

                                                                                                                                    </html>

