<%@ page contentType="text/html; charset=UTF-8"
	errorPage="../exception/ErrorDisplay.jsp"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%-- <%@ taglib prefix="sx" uri="/struts-dojo-tags" %> --%>
<%@ page import="com.mss.mirage.util.ApplicationConstants"%>
<html>
<head>
<title>Hubble Organization Portal :: Employee Addresses</title>
<sx:head cache="true" />
<link rel="stylesheet" type="text/css"
	href="<s:url value="/includes/css/mainStyle.css?version=1.4"/>">
<link rel="stylesheet" type="text/css"
	href="<s:url value="/includes/css/GridStyle.css"/>">
<link rel="stylesheet" type="text/css"
	href="<s:url value="/includes/css/leftMenu.css"/>">
<link rel="stylesheet" type="text/css"
	href="<s:url value="/includes/css/tabedPanel.css"/>">
<link rel="stylesheet" type="text/css"
	href="<s:url value="/includes/css/selectivity-full.css"/>">
<link rel="stylesheet" type="text/css"
	href="<s:url value="/includes/css/tooltip.css"/>">
<script type="text/JavaScript"
	src="<s:url value="/includes/javascripts/tooltip.js"/>"></script>
<script type="text/JavaScript"
	src="<s:url value="/includes/javascripts/Biometric.js?version=8.9"/>"></script>
<%--<script type="text/JavaScript" src="<s:url value="/includes/javascripts/clientValidations/AddressClientValidation.js"/>"></script>--%>
<script type="text/JavaScript"
	src="<s:url value="/includes/javascripts/AppConstants.js"/>"></script>
<script type="text/JavaScript"
	src="<s:url value="/includes/javascripts/tabedPanel.js"/>"></script>
<script type="text/JavaScript"
	src="<s:url value="/includes/javascripts/jquery.min.js"/>"></script>
<script type="text/JavaScript"
	src="<s:url value="/includes/javascripts/jquery-1.2.2.pack.js"/>"></script>
<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
 <link rel="stylesheet" type="text/css" href="<s:url value="/includes/css/new/x0popup.min.css?version=1.0"/>">
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/payroll/x0popup.min.js"/>"></script>
	 <script type="text/JavaScript" src="<s:url value="/includes/javascripts/EmpStandardClientValidations.js?ver=1.3"/>"></script>
      
</head>

<body class="bodyGeneral" oncontextmenu="return false;" >

	<!--//START MAIN TABLE : Table for template Structure-->
	<table class="templateTable1000x580" align="center" cellpadding="0"
		cellspacing="0">

		<!--//START HEADER : Record for Header Background and Mirage Logo-->
		<tr class="headerBg">
			<td valign="top"><s:include
					value="/includes/template/Header.jsp" /></td>
		</tr>
		<tr>
			<td>
				<table class="innerTable1000x515" cellpadding="0" cellspacing="0">
					<tr>
						<td width="150px;" class="leftMenuBgColor" valign="top"><s:include
								value="/includes/template/LeftMenu.jsp" /></td>
						<td width="850px" class="cellBorder" valign="top">
							<table cellpadding="0" cellspacing="0" width="100%" border="0">
								<tr>
									<td valign="top" style="padding: 5px 5px 5px 5px;">
										<!--//START TABBED PANNEL : -->
										<ul id="accountTabs" class="shadetabs">
											<li><a href="#" class="selected" rel="biometric">
													Biometric </a></li>
										</ul>

										<div id="biometricOverlay"></div>
										<div id="hubbleBiometricOverlay" align="center">
											<s:form theme="simple">
												<table align="center" border="0" cellspacing="0" style="width:100%;">
                                   <tr class="bioOverlayHeader">
														<td colspan="2"><font
															style="vertical-align: middle; font-family: myHeaderFonts; color: white; font-size: 20px; text-align: left;">
																Biometric</font></td>
														<td colspan="2" id="closeOverlay" align="right"><a href="#"
															onclick="getBiometricHoursOverlay();"> <img
																src="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/closeButton.png" /></a>

														</td>
													</tr>
                                    <tr>
														<td>
															<table style="width: 70%;">
															<s:hidden id="reportTypeHidden" value="%{reportTypeHidden}"  />
															<s:hidden id="attandanceLogId" name="attandanceLogId" value="%{attandanceLogId}"  />
															<s:hidden id="reviewHidden" name="reviewHidden" value=""  />
															
															<tr>
																			<span id="resultMessage" style="display:none"></span></tr>
																			
																		
																<tr>
																	<td class="fieldLabel">Date:</td>
																	<td cssClass=""><s:textfield cssClass="textbox"
																			name="date" id="date" readonly="true"/></td>
																	<td class="fieldLabel">Biometric&nbsp;Hours:</td>
																	<td cssClass=""><s:textfield name="biometricHrs"
																			id="biometricHrs" cssClass="inputSelectMini"
																			placeholder="Hrs" maxlength="2" readonly="true"/></td>


																</tr>
																<tr>
																	<td class="fieldLabel">Status:</td>
																	<td cssClass=""><s:select name="status"
																			id="status" list="biometricStatusMap"
																			Class="inputSelect" /></td>
																			

																	<td class="fieldLabel">TimeSheet&nbsp;Hours:</td>
																	<td cssClass=""><s:textfield name="timeSheetHrs"
																			id="timeSheetHrs" cssClass="inputSelectMini"
																			placeholder="Hrs" maxlength="2" readonly="true" /></td>
																	<td><span id="timeSheetStatus"></span></td>

																</tr>
																 <tr>
																	<td id="leaveStatusId" class="fieldLabel">Leave&nbsp;Status:</td>
																	<td cssClass=""><span id="leaveStatus"></span></td>
																	
																</tr> 
																
																<tr colspan="3" id="comUpdateId">
																	<td class="fieldLabel">Comments:</td>
																	<td cssClass=""><s:textarea rows="2" cols="30"
																			name="comments" cssClass="inputTextarea"
																			onchange="fieldLengthValidator(this);" id="comments" />
																	</td>
																	<td colspan="1" align="right"><input type="button"
																		class="buttonBg" value="Update"
																		onclick="updateAttendanceLoad();" /></td>
																</tr>
																<tr>
																			<td align="left"><span id="punchloadingMessage"
																				style="display: none"><font color="red"
																					size="2px">loading please wait...</font></span></td>
																		 </tr>
<tr>
                                                    <td id="tdPunchDetails" style="display:none;" class="fieldLabel">
                                                        PUNCH DETAILS :
                                                    </td>
                                                    </tr>
															</table>

														</td>
													</tr>

                                    
                                              <tr>
                                            <td>
                                                <table width="100%" cellpadding="1" cellspacing="1" border="0">
                                                    
                                                    <tr>
                                                    <td colspan="5">
                                                        <%-- <DIV id="loadingMessage"> </DIV> --%>

                                                        <div style="overflow: auto;max-height:400px;max-width:1000px;"> 
                                                            <table id="tblPunchDetails" cellpadding='1' cellspacing='1' border='0' class="gridTable" width='750' align="center">
                                                                <script type="text/JavaScript" src="<s:url value="/includes/javascripts/wz_tooltip.js"/>"></script>
                                                                

                                                                                            </table>
                                                                                            </div>
                                                                                           
                                                                                            
                                                                                            
                                                                                            </td>
                                                                                            </tr>

                                                                                            </table>


                                                                                            </td>
                                                                                            </tr>
                                                                                            




                                                                                            </table>
                                                                                            

											</s:form>
											<div id="officeHoursDivId" style="display:none;text-align:right;padding-right:2em">
											<label class="biofieldLabel" style="color:green;" id="officeHourslabelId">In Office Hours(HH:MM) :
										<span id="officehours"	></span></label>
										</div>
										</div>
										

										<div style="border: 1px solid gray; width: 845px; height: 500px; overflow: auto; margin-bottom: 1em;">
											<div id="biometric" align="center">
												<br>
												<s:form name="employeeAddForm" theme="simple">
													<table cellpadding="0" cellspacing="0" width="100%"
														border="0">
														<tr>
																			<span id="resultMessage1"></span>
																		</tr>
														<tr>
														<tr align="right">
														
															<td >
																 <a href="<s:url value="getBiometric.action"/>" style="align:right;">
                                            <img alt="Back to List"
                                                 src="<s:url value="/includes/images/backToList_66x19.gif"/>" 
                                                 width="66px" 
                                                 height="19px"
                                             border="0" align="center"></a></td>
														</tr>
														
															<td>
																<div style="width: auto; margin-left: 62px; margin-right: 60px;">

																	<table cellpadding="0" cellspacing="0" width="100%"
																		border="0">
																		<s:hidden name="empId" id="empId" value="%{empId}" />
																			<tr>
                                                                            <td class="fieldLabel">Emp No:</td>
																			<td>
                                                                             <s:textfield name="empNo" id="empNo" maxlength="5" cssClass="inputTextBlue" value="%{empNo}" onkeyup="getEmpNameandId(this.value);isNumeric(this.value);" />
                                                                            </td>
<%-- 																		<td><s:set var="empNoSpan" value="%{empNoSpan}" /> --%>
<!-- 																		<div id="empNoSpan"> -->
<%-- 																			<s:property value="empNoSpan" /> --%>
<!-- 																		</div>
                                                                            </td> -->

																			<td class="fieldLabel">EmpName:</td>
																			<td><s:textfield name="empName" id="empName" value="%{empName}" onkeyup="empNameList();" cssClass="inputTextBlue"  theme="simple" readonly="false"  />
                                                                                <div id="authorEmpValidationMessage" style="position: absolute; overflow:hidden;"></div>  
                                  
<%-- 																		<td><s:set var="empNameSpan" --%>
<%-- 																					value="%{empNameSpan}" /> --%>
<!-- 																				<div id="empNameSpan"> -->
<%-- 																					<s:property value="empNameSpan" /> --%>
<!-- 																	  			</div></td> -->
																	    	</tr>
																	    	<tr></tr>
																		    <tr>

																			<td class="fieldLabel">Year(YYYY):</td>
																			<td><s:textfield name="year" id="year"
																					maxlength="4" cssClass="inputTextBlue" value="%{year}" onkeyup="isNumericYear(this.value);" /></td>
																			<td class="fieldLabel">Month:</td>
																			<td><s:select
																					list="#@java.util.LinkedHashMap@{'1':'Jan','2':'Feb','3':'Mar','4':'Apr','5':'May','6':'June','7':'July','8':'Aug','9':'Sept','10':'Oct','11':'Nov','12':'Dec'}"
																					name="month" id="month" headerValue="select"
																					headerKey="0" value="%{month}"
																					cssClass="inputSelectGender" /></td>
																		   </tr>
																		   <tr>
																		    <td class="fieldLabel">Report&nbsp;Type:</td>
																			<td><s:select
																					list="#@java.util.LinkedHashMap@{'PayrollData':'Payroll Data','LiveData':'Live Data'}"
																					name="reportType" id="reportType"
																					headerKey="0" value="%{reportType}"
																					cssClass="inputSelectGender" /></td>
																					
																			<td align="right" colspan="4"><input
																				type="button" label="Submit" value="Load"
																				class="buttonBg" onclick="getBiometricLoad();"
																				style="margin-right: 30px;" /></td>
																				
																		 </tr>
																		 <tr>
																			<td align="center"><span id="loadingMessage"
																				style="display: none"><font color="red"
																					size="3px">loading please wait...</font></span></td>
																		 </tr>
																	</table>
																</div>
																<div id="loadingMsgForBioLoad"
																	style="color: red; font-size: 15px; display: none;text-align:center">Loading
																	please wait..</div>

															</td>
														</tr>
<tr>
        <td>
               <div style="display: none; position: fixed; top:69px;left:242px;overflow:auto;" id="menu-popup">
                        <table id="completeTable" border="1" bordercolor="#e5e4f2" style="border: 1px dashed gray;" cellpadding="0" class="cellBorder" cellspacing="0" ></table>
               </div>
        </td>
        </tr>

														<tr>
															<td><br></td>
															</tr>
<tr>
<td>													
<div id="noRecords" style="color: red; font-size: 15px; display: none;margin-left: 45%;">No Records Found!!</div>
</td>
</tr>
														<tr id="availableTr" >
														
															<td>
<div id="biometricBody" style="border-width: 2px; border-radius: 16px; border-style: double; border-color: #3e93d4; width: 90%; margin-left: 62px; display:none; /*! margin-right: 60px; *//*! margin-left: 62px; */">

<table cellpadding="0" cellspacing="0" width="90%" style="margin-left: -13px; margin-right: 0px;" border="0">
<tr><td></td></tr>
	<tr><td>

																<table id="tblbridges" align='center' cellpadding='1'
																	cellspacing='1' border='0' width='87.5%'
																	style="margin-left: 55px; margin-right: 65px; display: none;">
</table>
</td></tr>
<tr><td>
																 <div id="flagDesc" style="display: none">
																	<table cellpadding="0" cellspacing="0" width="90%" style="margin-left: 62px; margin-right: 60px;"
																		border="0">
																		<tr>
																			<td><br></td>
																		</tr>
																		<tr>
																			<td><img
																				src='/Hubble/includes/images/ecertification/green.png'
																				width='13px' height='13px' border='0'><font
																				size="2" color="darkblue">Present</font></td>
																			<td><img
																				src='/Hubble/includes/images/ecertification/red.png'
																				width='13px' height='13px' border='0'><font
																				size="2" color="darkblue">Absent</font></td>
																				
																			<td><img
																				src='/Hubble/includes/images/ecertification/darkgreen.png'
																				width='13px' height='13px' border='0'><font
																				size="2" color="darkblue">Holiday</font></td>
																			<td><img
																				src='/Hubble/includes/images/ecertification/violet.png'
																				width='13px' height='13px' border='0'><font
																				size="2" color="darkblue">WeeklyOff</font></td>

																		</tr>
																		<tr>
																			<td><img
																				src='/Hubble/includes/images/ecertification/babypink.png'
																				width='13px' height='13px' border='0'><font
																				size="2" color="darkblue">WeeklyOff Present</font></td>
																			<td><img
																				src='/Hubble/includes/images/ecertification/cyan.png'
																				width='13px' height='13px' border='0'><font
																				size="2" color="darkblue">Holiday Present</font></td>
																				
																			<td><img
																				src='/Hubble/includes/images/ecertification/blackgrey.png'
																				width='13px' height='13px' border='0'><font
																				size="2" color="darkblue">Holiday Absent</font></td>
																			<td><img
																				src='/Hubble/includes/images/ecertification/brown.png'
																				width='13px' height='13px' border='0'><font
																				size="2" color="darkblue">WeeklyOff Absent</font></td>

																		</tr>
																		<tr>
																			<td><img
																				src='/Hubble/includes/images/ecertification/orange.png'
																				width='13px' height='13px' border='0'><font
																				size="2" color="darkblue"> Half Present </font></td>
																			<td><img
																				src='/Hubble/includes/images/ecertification/blue1.png'
																				width='13px' height='13px' border='0'><font
																				size="2" color="darkblue">WeeklyOff Half
																					Present</font></td>
																					
																			<td><img
																				src='/Hubble/includes/images/ecertification/pink.png'
																				width='13px' height='13px' border='0'><font
																				size="2" color="darkblue">Holiday Half
																					Present</font></td>
																			<td><img
																				src='/Hubble/includes/images/ecertification/yellow2.png'
																				width='13px' height='13px' border='0'><font
																				size="2" color="darkblue">NoRecord</font></td>

																		</tr>
																	</table>
																</div> 
																</td></tr>
																<tr><td></td></tr>
																<tr><td>
																
																<div style="border-width: 1px; border-style: double; border-color: #3e93d4; margin-left: 12px;margin-right: -9px;"></div>			 
													</td></tr>
													<tr><td></td></tr>
													<tr><td><div id="AttendanceDetails" style="display: block">

																	<table cellpadding="0"  align='right' cellspacing="0"
																		width="40%" border="0">
																		
																		<tr>

																			<td class="biofieldLabel">Days&nbsp;In&nbsp;Month:</td>
																			<td><s:textfield name="daysinmonth"
																					id="daysinmonth" cssClass="inputSelectMini"
																					placeholder="" readonly="true" maxlength="2" /></td>
																			<td class="biofieldLabel">Days&nbsp;Worked:</td>
																			<td><s:textfield name="daysworked"
																					id="daysworked"  readonly="true" cssClass="inputSelectMini"
																					placeholder="" maxlength="2" /></td>
																		
																			<%-- <td class="biofieldLabel">Days&nbsp;Project:</td>
																			<td><s:textfield name="daysproject"
																					id="daysproject"  readonly="true" cssClass="inputSelectMini"
																					placeholder="" maxlength="2" /></td>--%>

																			<td class="biofieldLabel">Days&nbsp;Vacation:</td>
																			<td><s:textfield name="daysvacation"
																					id="daysvacation"  readonly="true" cssClass="inputSelectMini"
																					placeholder="" maxlength="2" /></td>

																		
																			<td class="biofieldLabel">Days&nbsp;Weekend:</td>
																			<td><s:textfield name="daysweekend"
																					id="daysweekend"  readonly="true" cssClass="inputSelectMini"
																					placeholder="" maxlength="2" /></td>
																			<td class="biofieldLabel">Days&nbsp;Holidays:</td>
																			<td><s:textfield name="daysholidays"
																					id="daysholidays"  readonly="true" cssClass="inputSelectMini"
																					placeholder="" maxlength="2" /></td>

																		
																	<%--</tr> </table>

																	<table cellpadding="0"  align="center" cellspacing="0" width="40%"
																		border="0">
																		<tr>
																			<td align="right"><input type="button"
																				label="Submit" value="Submit" class="buttonBio"
																				onclick="calculateAttendance();" style="" /></td> --%>
																		
																	
																</tr></table></div></td></tr>
																<tr><td></td></tr>
																
																 

																<%-- div id="resultData"></div>--%>
</div>
															</td>

														</tr>
														


													</table>
												</s:form>
												<%--  </sx:div> --%>
											</div>
											<!--//START TAB : -->

											<%--  </sx:tabbedpanel> --%>
										</div> <!--//END TABBED PANNEL : -->

									</td>
								</tr>
								<tr>
								<td><br></td></tr>
								<tr id="reviewBtn" style="display:none;" >
																<td align="left" ><input
																				type="button" id="reviewButton" label="Submit" value="Reviewed"
																				class="myButton" onclick="calculateAttendance();"
																				style="margin-left: 680px;" /></td>
																				</tr>
							</table>
						</td>
						<!--//END DATA COLUMN : Coloumn for Screen Content-->
					</tr>

				</table>
			</td>
		</tr>
		</table>
		</td>
		</tr>
		
		<!--//END DATA RECORD : Record for LeftMenu and Screen Content-->

		<!--//START FOOTER : Record for Footer Background and Content-->
		<tr class="footerBg">
			<td align="center"><s:include
					value="/includes/template/Footer.jsp" /></td>
		</tr>
<script type="text/javascript">
		$(window).load(function(){
			getBiometricLoad();

		});
		</script>

		<script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
		<script type="text/JavaScript"
			src="<s:url value="/includes/javascripts/selectivity-full.min.js"/>"></script>

		<!--//END FOOTER : Record for Footer Background and Content-->
</body>
</html>