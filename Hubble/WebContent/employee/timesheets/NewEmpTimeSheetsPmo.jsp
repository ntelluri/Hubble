<%-- 
    Document   : EmpTimeSheetSerach
    Created on : Jul 22, 2014, 1:26:02 AM
    Author     : miracle
--%>

<!--/*******************************************************************************
/*
 *Project: MirageV2
 *
 *$Author: vnukala $
 *
 *$Date: 2009-04-07 13:09:16 $
 *
 *$Revision: 1.4 $
 *
 *$Source: /Hubble/Hubble_CVS/Hubble/web/employee/timesheets/TeamTimeSheetSearch.jsp,v $
 *
 * @(#)TeamTimeSheetSearch.jsp	May 03, 2008, 2:17 AM 
 *
 * Copyright 2007 Miracle Software Systems, Inc. All rights reserved.
 * MIRACLE SOFTWARE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
*/ -->

<%@ page contentType="text/html; charset=UTF-8" errorPage="../../exception/ErrorDisplay.jsp"%>

<%@ page import="java.sql.*" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="com.mss.mirage.util.*,java.util.Date" %>
<%@ page import="com.freeware.gridtag.*" %> 
<%@ page import="com.mss.mirage.util.ApplicationConstants"%>
<%--<%@ page  import="org.apache.log4j.Level" %>--%>
<%--<%@ page import="org.apache.log4j.Logger" %>--%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%-- <%@ taglib prefix="sx" uri="/struts-dojo-tags" %> --%>
<%@ taglib uri="/WEB-INF/tlds/datagrid.tld" prefix="grd" %>

<html>
<head>
    
    <title>Hubble Organization Portal :: Employees TimeSheet Search</title>
    
 
    <link rel="stylesheet" type="text/css" href="<s:url value="/struts/tabs.css"/>">
          <link rel="stylesheet" type="text/css" href="<s:url value="/includes/css/mainStyle.css"/>">
          <link REL="StyleSheet" HREF="<s:url value="/includes/css/GridStyle.css"/>" type="text/css">
          <link REL="StyleSheet" HREF="<s:url value="/includes/css/leftMenu.css"/>" type="text/css">
          <link rel="stylesheet" type="text/css" href="<s:url value="/includes/css/tabedPanel.css"/>">
          <script language="javascript" src="<s:url value="/includes/javascripts/DBGrid.js"/>"></script>
    <script language="javascript" src="<s:url value="/includes/javascripts/TimeSheetWeekDays.js"/>"></script>
    <script type="text/JavaScript" src="<s:url value="/includes/javascripts/ClientValidations.js"/>"></script>
    <script type="text/JavaScript" src="<s:url value="/includes/javascripts/CalendarTime.js"/>"></script>
    <script type="text/JavaScript" src="<s:url value="/includes/javascripts/AppConstants.js"/>"></script>
    <script type="text/JavaScript" src="<s:url value="/includes/javascripts/tabedPanel.js"/>"></script>
       <script type="text/JavaScript" src="<s:url value="/includes/javascripts/EmpStandardClientValidations.js"/>"></script>
      <link rel="stylesheet" type="text/css" href="<s:url value="/includes/css/tooltip.css"/>">
    <script type="text/JavaScript" src="<s:url value="/includes/javascripts/tooltip.js"/>"></script>   
    
    <script type="text/JavaScript" src="<s:url value="/includes/javascripts/timesheets/EmpTimeSheetSerach.js?ver=1.3"/>"></script>
    
    <s:include value="/includes/template/headerScript.html"/>
        
    <style type="text/css">
            
            .popupItem:hover {
            background: #F2F5A9;
            font: arial;
            font-size:10px;
            color: black;
            }
            
            .popupRow {
            background: #3E93D4;
            }
            
            
            .popupItem {
            padding: 2px;
            width: 100%;
            border: black;
            font:normal 9px verdana;
            color: white;
            text-decoration: none;
            line-height:13px;
            z-index:100;
            }
            
        </style>
    <script type="text/JavaScript">

            
            function checkDates()
            {
             // alert("hi");
           
            var fromDate = document.getElementById('printStartDate').value;
            var toDate = document.getElementById('printEndDate').value;
            var empname = document.getElementById('empnameById1').value;
            
         if(fromDate!=null && fromDate!="")
            {
 
                if(toDate!=null && toDate!="")
                {
                     if(empname == -1)
                     {
                       alert("Please Select Employee Name ");
                        return false;
                     }
                     else{
                        return true;       
                     }
                  
                }
                else{
                alert("Please Enter End Date ");
                return false;
                }
            }
            else
            {
            alert("Please Eneter Start Date ");
            return false;
            }
            
            
            return true;
           
           
            
            }
     
			
			
        function clearTimeSheetSearch() {
              
                 document.teamSearch.startDate.value = "";
                document.teamSearch.endDate.value = "";
           document.teamSearch.customerName.value = '-1';
           document.teamSearch.projectId.value ='-1';
           document.teamSearch.empnameById.value = '-1';
           document.teamSearch.description.value = '-1'; 
           document.teamSearch.locationId.value = '-1'; 
           document.getElementById("week").value = '-1';
      
        }
        
         function clearTimeSheetReports() {
                 
        	  document.timeSeetReport.year.value = "";
              document.timeSeetReport.month.value = '-1';
         document.timeSeetReport.empnameById2.value = '-1';
         document.timeSeetReport.description2.value ='-1';
         document.timeSeetReport.week.value = '-1';
      
        }
        
        
        function disableEmpList(descObj) {
            if(descObj.value=="4") {
                document.timeSeetReport.empnameById2.value = -1;
            }
        }
        function checkStaus() {
            if(document.timeSeetReport.description2.value==4) {
                alert("Please change status except 'Not Entered'");
                getEmployeesByProjectId('print');
            }
        }
        
 function showDefault() {
            var isDual = document.getElementById('isDualReportingRequired').value;
            if(isDual=='false') {
                document.getElementById("teamTypeListTd").style.display = 'none';
        document.getElementById("teamTypeTd").style.display = 'none';
            }else {
                 document.getElementById("teamTypeListTd").style.display = '';
        document.getElementById("teamTypeTd").style.display = '';
            }
        }
       
       
       
       
       
        function getDescriptions(priStatus,secStatus) {
              var background = "#3E93D4";
    var title = "Timesheet Status";
   // var text1 = text; 
   // var size = text1.length;
    
    
    var statusConetent = "Status:&nbsp;&nbsp;"+priStatus;
    if(secStatus=='Approved'||secStatus=='Disapproved'||secStatus=='Submitted'){
        statusConetent = statusConetent+"<br>"+"Secondary Status:&nbsp;&nbsp;"+secStatus;
    }
    var size = statusConetent.length;
    
    //Now create the HTML code that is required to make the popup
    var content = "<html><head><title>"+title+"</title></head>\
    <body bgcolor='"+background +"' style='color:white;'><h4>"+title+"</h4>"+statusConetent+"<br />\
    </body></html>";
    
    if(size < 50){
        //Create the popup       
        popup = window.open("","window","channelmode=0,width=300,height=150,top=200,left=350,scrollbars=no,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no");    
        popup.document.write(content); //Write content into it.    
    }
    
    else if(size < 100){
        //Create the popup       
        popup = window.open("","window","channelmode=0,width=400,height=200,top=200,left=350,scrollbars=no,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no");    
        popup.document.write(content); //Write content into it.    
    }
    
    else if(size < 260){
        //Create the popup       
        popup = window.open("","window","channelmode=0,width=400,height=200,top=200,left=350,scrollbars=no,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no");    
        popup.document.write(content); //Write content into it.    
    } else {
        //Create the popup       
        popup = window.open("","window","channelmode=0,width=400,height=200,top=200,left=350,scrollbars=no,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no");    
        popup.document.write(content); //Write content into it.    
    }
    
        }
		
		
    </script>
    

    
</head>
<%!
Connection connection = null;
String userId = null;
String roleId = null;
String roleCusVen = null;
String workingCountry=null;
String empType = "";
String designation = "";
//Logger log=null;
//String empType = null;

String empType1 = null;
%>


<%-- <body class="bodyGeneral"  oncontextmenu="return false;" onload="init();" > --%>
<%-- <body class="bodyGeneral"  oncontextmenu="return false;" onload="showDefault();init();isSelectProject();" > --%>
<body class="bodyGeneral"  oncontextmenu="return false;">

<table class="templateTable1000x580" align="center" cellpadding="0" cellspacing="0">

<tr class="headerBg">
    <td valign="top">
        <s:include value="/includes/template/Header.jsp"/>                    
    </td>
</tr>
<tr>
<td>
    <table class="innerTable1000x515" cellpadding="0" cellspacing="0">
    <tr>
        <td width="150px;" class="leftMenuBgColor" valign="top">
            <s:include value="/includes/template/LeftMenu.jsp"/>                    
        </td>
        <td width="850px" class="cellBorder" valign="top">
        <table cellpadding="0" width="100%" cellspacing="0" bordercolor="#efefef">  
            <tr>
            <td style="padding: 5px 5px;">
                <ul id="accountTabs" class="shadetabs" >
                    <li ><a href="#" class="selected" rel="teamTimeSheetsListTab" >TimeSheet Search</a></li>
                    <li><a href="#" rel="printProjectTimesheetsTab" class="selected">TimeSheet Reports</a></li>
                    
                    <%--    <li><a href="#" rel="printTimesheetsTab" class="selected"> Print TimeSheet </a></li> --%>
                       
                   
                     
                </ul>
                 
                <%--  <sx:tabbedpanel  id="teamTimeSheetsListPannel" cssStyle="width: 845px; height: 400px;padding: 5px 5px;" doLayout="true">--%>
                <div  style="border:1px solid gray; width:845px;height: 500px; overflow:auto; margin-bottom: 1em;">
                    <div id="teamTimeSheetsListTab" class="tabcontent"  >
                        <%-- <sx:div id="teamTimeSheetsListTab" label="TimeSheet" theme="ajax" > --%>
                        <table cellpadding="0" width="100%" cellspacing="0" bordercolor="#efefef">  
                            <tr class="headerText">
                                <td align="right">
                                    <%
                                    if(request.getAttribute(ApplicationConstants.RESULT_MSG)!=null){
                                        out.println(request.getAttribute(ApplicationConstants.RESULT_MSG));
                                    }
                                    %>   
                                </td>
                                <td align="right">
                                    <img src="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/spacer.gif" width="100%" height="13px" border="0">                                    
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    
                                        
                                           
                                                        
                                                 <!-- We have to implement Search login in backend-->
                                                <s:form name="teamSearch" action="newempTimeSheetSearchPmo" theme="simple" >
                                                      <s:hidden name="isDualReportingRequired" id="isDualReportingRequired" value="%{isDualReportingRequired}"/>
                                                    <table cellpadding="0" cellspacing="2" border="0" width="100%">
                                                    <tr>
                                                    <td class="fieldLabel" width="25%">Start&nbsp;Date&nbsp;(mm/dd/yyyy)&nbsp;:</td>
                                                    <td><s:textfield name="startDate" id="startDate" cssClass="inputTextBlueSmall" value="%{startDate}" onchange="checkDates(this);"/><a href="javascript:cal1.popup();">
                                                        <img src="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/showCalendar.gif" width="20" height="20" border="0"></a>
                                                    </td>
                                                    
                                                    <td class="fieldLabel" width="25%">End&nbsp;Date&nbsp;(mm/dd/yyyy)&nbsp;:</td>
                                                    <td>  <s:textfield name="endDate" id="endDate" cssClass="inputTextBlueSmall" value="%{endDate}" onchange="checkDates(this);"/><a href="javascript:cal2.popup();">
                                                        <img src="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/showCalendar.gif" width="20" height="20" border="0"></a>
                                                    </td>
 
                                                </tr>
                                                
                                                <tr>
                                                  <td class="fieldLabel">Customer Name :</td>
                                    <td>
                                       
                                        <s:select headerKey="-1" headerValue="All" list="customerMapPmo" name="customerName" id="customerName" cssClass="inputSelect" value="%{customerName}" theme="simple" onchange="getProjectsByCustomerId('search');getEmployeesByCustomerIdPmo('search');"/>
                                    </td>
                                     <td class="fieldLabel" >Projects: </td>
                                        <td >
                                               <s:select list="projectMapPmo" name="projectId" id="projectId" onchange="isSelectProjectPmo();isOprDualReportsTo(this);getEmployeesByProjectIdPmo('search');" headerKey="-1" headerValue="--Select Project--" cssClass="inputSelect" value="%{projectId}" disabled="false"/>
                                             <%--  <s:select list="{}" name="projectId" id="projectId" onchange="getEmployeesByProjectId();" headerKey="-1" headerValue="--Select Project--" cssClass="inputSelect" value="%{projectId}" disabled="false"/> --%>
                                        </td>
                                        <td class="fieldLabel" id="teamTypeTd" style="display:none;">ReportsTo&nbsp;Level&nbsp;:</td>
                                                    
                                                    <td id="teamTypeListTd" style="display:none;">  
                                                        <%-- <s:select list="#@java.util.LinkedHashMap@{'2':'All','0':'Lower Level','1':'Higher Level'}" id="teamType" name="teamType" cssClass="inputSelect" value="%{teamType}" /> --%>
                                                        <s:select list="#@java.util.LinkedHashMap@{'0':'All','1':'Lower Level','2':'Higher Level'}" id="teamType" name="teamType" cssClass="inputSelect" value="%{teamType}" />
                                                    </td>
                                                </tr>
                                                
                                                
                                                <tr>
                                                 <td class="fieldLabel">EmpName&nbsp;:</td>
                                                    <td><s:select list="empnamesList" id="empnameById" name="empnameById" headerKey="-1" headerValue="All" cssClass="inputSelectLarge" value="%{empnameById}" />
                                                     
                                                
                                                     
                                                    <td class="fieldLabel">Status&nbsp;:</td>
                                                    <td >
                                                       
                                                          <%--  <s:select list="{'Submitted','Disapproved','Entered'}" id="description" name="description" cssClass="inputSelect" value="%{description}" headerKey="" headerValue="--Please Select--"/> --%>
                                                           <s:select list="{'Approved','Submitted','Disapproved','Entered'}" id="description" name="description" cssClass="inputSelect" value="%{description}" headerKey="" headerValue="--Please Select--"/>
                                                          
                                                      
                                                  <%--      <INPUT type="image"  onclick='document.teamSearch.action="empTimeSheetSearch.action"' src="<s:url value="/includes/images/go_21x21.gif"/>" > --%>
                                                    
                                                    </td>
                                                </tr>
                                                 <tr id="operationContactSearchTr" style="display: none;">
                                                 
                                                     
                                                
                                                     
                                                    <td class="fieldLabel">Locations&nbsp;&nbsp;:</td>
                                                    <td >
                                                       
                                                          <%--  <s:select list="{'Submitted','Disapproved','Entered'}" id="description" name="description" cssClass="inputSelect" value="%{description}" headerKey="" headerValue="--Please Select--"/> --%>
                                                           <s:select list="locationsMapByLivingCountry" id="locationId" name="locationId" cssClass="inputSelect" value="%{locationId}" headerKey="-1" headerValue="--Please Select--"/>
                                                          
                                                      
                                                  <%--      <INPUT type="image"  onclick='document.teamSearch.action="empTimeSheetSearch.action"' src="<s:url value="/includes/images/go_21x21.gif"/>" > --%>
                                                    
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                     <td></td>
                                                     <td></td>
                                                    <td >
                                                        
                                                         <s:submit name="submit" value="Search"  cssClass="buttonBg"/>
                                                     <input type="button" class="buttonBg" value="Clear" onclick="clearTimeSheetSearch();"/>
                                                    </td>
                                                </tr>
                                          <%--      <tr>
                                                    
                                                    
                                                 
                                                  <td></td>
                                                   <td></td>
                                                   <td></td>
                                                   
                                                  <td align="right"> 
                                                            
                                                            
                                                        <INPUT type="image"  onclick='document.teamSearch.action="empTimeSheetSearch.action"' src="<s:url value="/includes/images/go_21x21.gif"/>" > 
                                                         
                                                        </td>
                                                  
                                                  
                                                             
                                        </tr> --%>
                                    </table>
                                                </s:form>
                                                
                                     
                                </td>
                            </tr>
                            <tr>
                                <td> 
                                    <div id="displayResult">  
                                        <table align="center" cellpadding="0" cellspacing="0"  width="98%">
                                            <tr>
                                                <td align="center">
                                                    <br>
                                                                                                        
                                                    <%
                                                    
                                                    try{
                                                      
                                                        String strSQL = null;
                                                     
                                                         strSQL = (String)session.getAttribute("queryTeam"); 
                                                       
                                                        strSQL=request.getSession(false).getAttribute("queryTeam").toString();
                                                        int intCurr = 1;
                                                        int intSortOrd = 0;
                                                        String strTmp = null;
                                                        boolean blnSortAsc = true;
                                                        String strSortCol = null;
                                                        String strSortOrd = "DSC";
                                                        
                                                        strTmp = request.getParameter("txtCurr");
                                                        
                                                        if (strTmp != null)
                                                            intCurr = Integer.parseInt(strTmp);
                                                       
                                                        // for lookup connection
                                                        connection = ConnectionProvider.getInstance().getConnection();
                                                        
                                                        strSortCol = request.getParameter("Colname");
                                                        strSortOrd = request.getParameter("txtSortAsc");
                                                        if (strSortCol == null) strSortCol = "DateStart";
                                                        if (strSortOrd == null) strSortOrd = "DSC";
                                                        blnSortAsc = (strSortOrd.equals("ASC"));
                                                        
                                                  
                                                    
                                                    %>
                                                    
                                                        
                                                        <s:form method="post" id="frmDBGrid" name="frmDBGrid" theme="simple"  action="newempTimeSheetSearchPmo.action">           
                                                            
                                                            <grd:dbgrid id="tblStat" name="tblStat" width="100" pageSize="10" 
                                                                        currentPage="<%=intCurr%>" border="0" cellSpacing="1" cellPadding="2" 
                                                                        dataMember="<%=strSQL%>" dataSource="<%=connection%>" cssClass="gridTable">
                                                                        
                                                                        
                                                                <grd:gridpager imgFirst="../../includes/images/DBGrid/First.gif" imgPrevious="../../includes/images/DBGrid/Previous.gif" 
                                                                               imgNext="../../includes/images/DBGrid/Next.gif" imgLast="../../includes/images/DBGrid/Last.gif"/>
                                                                <grd:gridsorter sortColumn="<%=strSortCol%>" sortAscending="false"/>
                                                                
                                                                <grd:rownumcolumn headerText="SNO" width="2"/>
                                                              <%--  <grd:imagecolumn  headerText="Remind" width="5"  HAlign="center"  
                                                                                  imageSrc="../../includes/images/DBGrid/newEdit_17x18.png" 
                                                                                  linkUrl="newgetEmpTimeSheet.action?empID={EmpID}&timeSheetID={TimeSheetId}&resourceType={ResourceType}&statusValue={Description}" imageBorder="0" 
                                                                                  imageWidth="16" imageHeight="16" alterText="Click to edit" />--%>
                                                               
                                                              
   <grd:imagecolumn  headerText="Remind" width="5"  HAlign="center"  
                                                                                  imageSrc="../../includes/images/DBGrid/newEdit_17x18.png" 
                                                                                  linkUrl="newgetEmpTimeSheet.action?empID={EmpID}&timeSheetID={TimeSheetId}&resourceType={ResourceType}&statusValue={Description}&secStatusValue={SecDescription}" imageBorder="0" 
                                                                                  imageWidth="16" imageHeight="16" alterText="Click to edit" />
    <grd:numbercolumn dataField="EmpName" headerText="EmpName" width="19" dataFormat=" "/> 
																				  
     <grd:anchorcolumn dataField="Description" linkUrl="javascript:getDescriptions('{Description}','{SecDescription}')" headerText="Status"
                                                                                      linkText="Click To View"  width="10" />
																					  

                                                            <%--    <grd:textcolumn dataField="ProjectName" headerText="ProjectName" width="10" /> --%>
                                                           <%--     <grd:textcolumn dataField="Description" headerText="Status" width="10" /> --%>
                                                                <grd:datecolumn dataField="DateStart" headerText="Start Date" width="10" dataFormat="MM-dd-yyyy" />
                                                                <grd:datecolumn dataField="DateEnd"	headerText="End Date" width="10" dataFormat="MM-dd-yyyy"/>
                                                                <grd:numbercolumn dataField="TotalHrs" headerText="Billable Hrs" dataFormat="   .0 " width="8" />
                                                                    <grd:numbercolumn dataField="TotalHolidayHrs" headerText="Holiday Hrs" dataFormat="   0.0 " width="8" HAlign="right"/>
   <grd:numbercolumn dataField="TotalVacationHrs" headerText="Vacation Hrs" dataFormat="   0.0 " width="8" HAlign="right"/>
  	<grd:numbercolumn dataField="TotalCmtHrs" headerText="Comptime Hrs" dataFormat="   0.0 " width="8" HAlign="right"/>
                                                             <%--   <grd:numbercolumn dataField="TotalBmHrs" headerText="Biometric Hrs" dataFormat="   .0 " width="8" /> --%>
                                                                <grd:datecolumn dataField="SubmittedDate"	headerText="Submitted Date" width="12" dataFormat="MM-dd-yyyy"/>                                                                                            
                                                                <grd:datecolumn dataField="ApprovedDate" headerText="ApprovedDate" width="12" dataFormat="MM-dd-yyyy"/>
                                                            </grd:dbgrid>
                                                            
                                                            <input TYPE="hidden" NAME="txtCurr" VALUE="<%=intCurr%>">
                                                            <input TYPE="hidden" NAME="txtSortCol" VALUE="<%=strSortCol%>">
                                                            <input TYPE="hidden" NAME="txtSortAsc" VALUE="<%=strSortOrd%>">
                                                            <input type="hidden" name="submitFrom" value="dbGrid">     
                                                            <s:hidden name="empnameById" value="%{empnameById}"/>
                                                            <s:hidden name="startDate" value="%{startDate}"/>
                                                            <s:hidden name="endDate" value="%{endDate}"/>
                                                            <s:hidden name="description" value="%{description}"/>
                                                            <s:hidden name="opsContactId" value="%{opsContactId}"/>
                                                            <s:hidden name="locationId" value="%{locationId}"/>
                                                            <s:hidden name="customerName" value="%{customerName}"/>
                                                                <s:hidden name="projectId" value="%{projectId}"/>
                                                                <s:hidden name="teamType" value="%{teamType}"/>
                                                            <!-- Used for Storing the Current Database Primary Key Id Eg. AccountId/ContactId. -->
                                                            <input id="txtDBId" TYPE="hidden" NAME="txtDBId"	VALUE="0">
                                                            
                                                            <!-- Used for Storing the Current Row No. in the Table -->
                                                            <input id="txtTRId"TYPE="hidden" NAME="txtTRId"	VALUE="1">
                                                            
                                                        </s:form>
                                                    
                                                    
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    
                                    <%
                                    connection.close();
                                    connection = null;
                                                    }catch(Exception ex) {
                                                        ex.printStackTrace();
                                                        // log.setLevel((Level)Level.ERROR);
                                                        //log.error("The Error @addTimeSheet()",ex);
                                                    }finally {
                                                        if(connection!=null)
                                                            connection.close();
                                                        connection = null;
                                                    } // finally
                                    %>
                                </td>      
                            </tr>
                        </table>
                        <script type="text/JavaScript">
                                                           var cal1 = new CalendarTime(document.forms['teamSearch'].elements['startDate']);
                                                           cal1.year_scroll = true;
                                                           cal1.time_comp = false;
                                                           
                                                           var cal2 = new CalendarTime(document.forms['teamSearch'].elements['endDate']);
                                                           cal2.year_scroll = true;
                                                           cal2.time_comp = false;
                        </script>
                        <%-- </sx:div> --%>
                    </div>
                 
                    <div  id="printProjectTimesheetsTab" class="tabcontent" >
                        <table cellpadding="0" width="100%" cellspacing="0" bordercolor="#efefef">  
                            <tr class="headerText">
                                <td align="right">
                                    <%
                                   
                                    if(request.getAttribute(ApplicationConstants.RESULT_MSG)!=null){
                                                        out.println(request.getAttribute(ApplicationConstants.RESULT_MSG));
                                    }
                                    %>   
                                </td>
                                <td align="right">
                                    <img src="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/spacer.gif" width="100%" height="13px" border="0">                                    
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    
                                       
                                               
                                                <!-- We have to implement Search login in backend-->
                                                
                                                
                                                
                                                
                                <s:form name="timeSeetReport" action="getEmpProjectTimeSheetsReport" theme="simple" target="_blank">
                
                    
                    <div style="width:840px;" > 
                                               
                <table cellpadding="0" cellspacing="0" align="left" width="100%">
                    
                    <tr>
                        <td>
                      
                                 <div id="resultMessage"></div>
                            <table border="0" cellpadding="0" cellspacing="2" align="left" width="100%">
                               
                                <tr>
                                
                                                        <td class="fieldLabel">Year:<FONT color='red' SIZE='0.5'><em>*</em></FONT></td>
                                                        <td  ><s:textfield name="year" id="year" value="%{year}" cssClass="inputTextBlue" />
                                                            
                                                        </td>
                                                        
                                                       <td class="fieldLabel" width="200px" > Month:</td>  
                                                       <td><s:select list="#@java.util .LinkedHashMap@{'1':'Jan','2':'Feb','3':'Mar','4':'Apr','5':'May','6':'June','7':'July','8':'Aug','9':'Sept','10':'Oct','11':'Nov','12':'Dec'}" name="month" id="month" onchange="load(event);" headerValue="select" headerKey="0" value="%{month}" cssClass="inputTextBlue"  theme="simple" readonly="false"/></td>
                                                    </tr>
                                                    
                                               
                                                
                                                
                                                      <tr>
                                                
                                                 <td class="fieldLabel">Select&nbsp;EmpName&nbsp;:</td>
                                                 <td><s:select list="empnamesListReport" id="empnameById2" name="empnameById2" headerKey="-1" headerValue="All" cssClass="inputSelectLarge" value="%{empnameById2}" onchange="checkStaus();"/>
                                                     
                                              
                                                    
                                                   
                                                     
                                                    </td>
                                                   <td class="fieldLabel">Select&nbsp;Status&nbsp;:</td> 
                                                     <td> 
                                                       
                                           <%--                    <s:select list="{'Submitted','Disapproved','Entered','Approved','Not Entered'}" id="description2" name="description" cssClass="inputSelect" value="%{description}" headerKey="-1" headerValue="--Please Select--"/>  --%> 
                                                   <s:select list="#@java.util.LinkedHashMap@{'1':'Entered','2':'Submitted','3':'Approved','11':'Not entered','10':'Disapproved'}" id="description2" name="description2" cssClass="inputSelect" value="%{description2}" headerKey="-1" headerValue="All"/>  
                                                      
                                                        
                                                    
                                                    </td> 
                                                      </tr>
                                                      <tr>
                                                       <td class="fieldLabel">Select&nbsp;Week&nbsp;:</td> 
                                                     <td> 
                                                       
                                                          <s:select list="{'Week1','Week2','Week3','Week4','Week5','Week6'}" id="week" name="week" cssClass="inputSelect" value="%{week}" headerKey="-1" headerValue="--Please Select--"/> 
<%--                                                        <s:select list="#@java.util.LinkedHashMap@{'1':'Entered','2':'Submitted','3':'Approved','4':'Not entered'}" id="description2" name="description2" onchange="disableEmpList(this);" cssClass="inputSelect" value="%{description2}" headerKey="-1" headerValue="All"/>  --%>
                                                      
                                                        
                                                    
                                                    </td> 
                                                      </tr>
                                                      <tr>
                                                        <%-- <td   class="fieldLabelLeft">Select EmpName :</td>
                                                     <td ><s:select list="empnamesList" id="empnameById1" name="empnameById1" headerKey="-1" headerValue="All" cssClass="inputSelectLarge" value="%{empnameById}"/></td> 
                                                        
                                                        <td></td> --%>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td> 
                                                             <input type="button" class="buttonBg" value="Search" id="generateStatusButton" onclick="getTimeSheetStatusReportDetails('1');"/>
                                                             <input type="button" class="buttonBg" value="Clear" onclick="clearTimeSheetReports();"/>
                                                           
<%--                                                          <s:submit name="submit" value="Generate Excel"  cssClass="buttonBg"/>  --%>
                                                        </td>
                                                    </tr>
                                                  <tr>
                                                      <td class="fieldLabel" >Total&nbsp;Records:</td>
                                                       <td class="userInfoLeft" id="totalCount"></td>
                                               
                 </tr>     
                               
                            </table>  
                        </td>
                    </tr>
                    
                    
                    
                    <tr>
                                                        <td>
                                                            <table width="100%" cellpadding="1" cellspacing="1" border="0">
                                                                <tr>
                                                                    <td>
                                                                        <div id="loadingMessage" style="color:red; display: none;" align="center"><b><font size="4px" >Please wait...report is loading</font></b></div>

                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td height="20px" >
                                                                        <s:hidden name="pgNo" id="pgNo" cssClass="inputTextBlue" value="%{pgNo}" theme="simple"/>
                                                                        
                                                                        <s:hidden name="totalRecords" id="totalRecords" cssClass="inputTextBlue" theme="simple"/>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <%-- <DIV id="loadingMessage"> </DIV> --%>

                                                                        <TABLE id="tblTimeSheetReport" align="left"  
                                                                               cellpadding='1' cellspacing='1' border='0' class="gridTable" width='100%'>
                                                                         

                                                                        </TABLE>
                                                                        <div id="button_pageNation"></div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                    
                    
                                                </table>   
                                               
                                            </div>
                                        </s:form>           
                                         
                                         
                                         
                                         
                                         
                                         
                                         
                                         
                                         
                                         
                                    
                                </td>
                            </tr>
                            
                        </table>
                      
                        
                    </div>
              
                                
                                
                                 
                    
                                
                                
                                
                                
                                
                   <!-- customer print time sheet end -->


                    <%-- </sx:tabbedpanel> --%>
                </div>
                <script type="text/javascript">

var countries=new ddtabcontent("accountTabs")
countries.setpersist(true)
countries.setselectedClassTarget("link") //"link" or "linkparent"
countries.init()

                </script>
            </td>
        </tr>
    </table>
</td>
</tr>
</table>
</td>
</tr>
<tr class="footerBg">
    <td align="center"><s:include value="/includes/template/Footer.jsp"/></td>
</tr>
 <tr>
                <td>
                    
                    <div style="display: none; position: absolute; top:170px;left:320px;overflow:auto;" id="menu-popup">
                        <table id="completeTable" border="1" bordercolor="#e5e4f2" style="border: 1px dashed gray;" cellpadding="0" class="cellBorder" cellspacing="0" />
                    </div>
                    
                </td>
            </tr>
</table>
                        <script type="text/javascript">
		$(window).load(function(){
		showDefault();
		init();
		isSelectProject();
	//	getTimeSheetStatusReportDetails();
		});
		</script>
		
		 <script type="text/javascript">
                                                                                                                                                                                                       
                                                                                                                                                                                                          
                                                                                                                                                                                                        
            function pagerOption(){

                       //   alert("hii");                                   
                var  recordPage=10;
                                                                                                                                                                                                  
                $('#tblTimeSheetReport').tablePaginate({navigateType:'navigator'},recordPage,tblTimeSheetReport);

            }
        </script>
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/DynamicPagination.js?version=1.0"/>"></script>

		
</body>
</html>


