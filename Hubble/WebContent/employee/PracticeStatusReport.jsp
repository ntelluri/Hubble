<%-- 
   Document   : PracticeStatusReport
   Created on : Jan 18, 2018, 04:27:49 PM
   Author     : 
--%>



<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@ page contentType="text/html; charset=UTF-8" errorPage="../exception/ErrorDisplay.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags" %> 
<%-- <%@ taglib prefix="sx" uri="/struts-dojo-tags" %> --%>
<%@ page import="com.mss.mirage.util.ApplicationConstants"%>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="com.mss.mirage.util.ConnectionProvider"%>
<html>
    <head>
   
        <title>Hubble Organization Portal :: Executive DashBoard</title>
        <link rel="stylesheet" type="text/css" href="<s:url value="/includes/css/GridStyle.css"/>">
        <link rel="stylesheet" type="text/css" href="<s:url value="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css"/>">
        <link rel="stylesheet" type="text/css" href="<s:url value="/includes/css/mainStyle.css?version=2.6"/>">
        <link rel="stylesheet" type="text/css" href="<s:url value="/includes/css/tabedPanel.css"/>">
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/animatedcollapse.js"/>"></script>
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/jquery-1.2.2.pack.js"/>"></script>
        <link rel="stylesheet" type="text/css" href="<s:url value="/includes/css/leftMenu.css"/>">
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/CalendarTime.js"/>"></script>
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/AppConstants.js"/>"></script>
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/EnableEnter.js"/>"></script>
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/ReusableContainer.js"/>"></script>
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/tabedPanel.js"/>"></script>
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/employee/EmployeeDashboard.js?version=3.1"/>"></script>
        <script type="text/javascript" src="https://www.google.com/jsapi"></script>
	 
	  <script language="JavaScript">
            google.load("visualization", "1", {packages:["corechart"]});   
            animatedcollapse.init();
           // google.charts.setOnLoadCallback(drawChart);
        </script>
 <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans">
        <s:include value="/includes/template/headerScript.html"/>
        
        
        <style>
 
      .wholetable{
      border: 1px solid black;
      border-collapse: collapse;
      padding:8px;
   /*  font-family: Open Sans;*/
   font-size: 15px;
    font-weight: normal;
    font-family: Arial, sans-serif;
    font-style: normal;
      text-align:center;
      }
      .tdfont{
      font-size:0.9vw;
      color:#232527;
     
      border: 1px solid black;
      border-collapse: collapse;
      padding:8px;
      /*font-family: Open Sans;*/
        font-size: 12px;
    font-weight: bold;
    font-family: Arial, sans-serif;
    font-style: normal;
      text-align:center;
      }
      .total{
      background-color:#bbdcf7;
      color:#232527;
      font-size:1vw;
     
      border: 1px solid black;
      border-collapse: collapse;
      padding:8px;
      font-family: Arial, sans-serif;
       font-style: normal;
       font-size: 12px;
    font-weight: bold;
   
      text-align:center;
      }
      .total1{
      background-color:#52a6ea;
      color:#232527;
      font-size:1vw;
    
      border: 1px solid black;
      border-collapse: collapse;
      padding:8px;
       
      
      font-family: Arial, sans-serif;
       font-style: normal;
       font-size: 12px;
    font-weight: bold;
      text-align:center;
      }
      .total2{
      background-color:#00aae7;
      color:#232527;
     
      
      border: 1px solid black;
      border-collapse: collapse;
      padding:8px;
     font-family: Arial, sans-serif;
       font-style: normal;
       font-size: 12px;
    font-weight: bold;
     
      text-align:center;
      }
      .total3{
      background-color:#71daff;
      color:#232527;
     
      font-weight:600;
      border: 1px solid black;
      border-collapse: collapse;
      padding:8px;
     font-family: Arial, sans-serif;
       font-style: normal;
       font-size: 12px;
    font-weight: bold;
      text-align:center;
      }
      .total4{
      background-color:#f5888d;
      color:#232527;
     
      
      border: 1px solid black;
      border-collapse: collapse;
      padding:8px;
   font-family: Arial, sans-serif;
       font-style: normal;
       font-size: 12px;
    font-weight: bold;
      text-align:center;
      }
      .total5{
      background-color:#fbd1d3;
      color:#232527;
     
     
      border: 1px solid black;
      border-collapse: collapse;
      padding:8px;
     font-family: Arial, sans-serif;
       font-style: normal;
       font-size: 12px;
    font-weight: bold;
      text-align:center;
      }
    </style>
       
    </head>
   <!--  <body  class="bodyGeneral" onload="javascript:animatedcollapse.show('TasksDashBoardDiv');" oncontextmenu="return false;"> -->
    <body  class="bodyGeneral" oncontextmenu="return false;">

        <%!
            /* Declarations */
            String queryString = null;
            Connection connection;
            Statement stmt;
            ResultSet rs;
            int userCounter = 0;
            int activityCounter = 0;
            int accountCounter = 0;
        %>


        <!--//START MAIN TABLE : Table for template Structure-->
        <table class="templateTable1000x580" align="center" cellpadding="0" cellspacing="0">

            <!--//START HEADER : Record for Header Background and Mirage Logo-->
            <tr class="headerBg">
            <td valign="top">
                <s:include value="/includes/template/Header.jsp"/>                    
            </td>
        </tr>
        <!--//END HEADER : Record for Header Background and Mirage Logo-->

        <!--//START DATA RECORD : Record for LeftMenu and Screen Content-->
        <tr>
        <td>
            <table class="innerTable1000x515" cellpadding="0" cellspacing="0">
                <tr>
                    <!--//START DATA COLUMN : Coloumn for LeftMenu-->
                <td width="850px;" class="leftMenuBgColor" valign="top"> 
                    <s:include value="/includes/template/LeftMenu.jsp"/>
                </td>
                <!--//START DATA COLUMN : Coloumn for LeftMenu-->

                <!--//START DATA COLUMN : Coloumn for Screen Content-->
                <td width="850px" class="cellBorder" valign="top" style="padding-left:10px;padding-top:5px;">
                    <!--//START TABBED PANNEL : -->
                    <%--      <sx:tabbedpanel id="resetPasswordPannel" cssStyle="width: 845px; height: 550px;padding-left:10px;padding-top:5px;" doLayout="true" useSelectedTabCookie="true" > 
                        
                        <!--//START TAB : -->
                        <sx:div id="dashBoardTab" label="DashBoard Details" cssStyle="overflow:auto;"> --%>
                    <ul id="reportsTab" class="shadetabs" >
                        <li><a href="#" rel="TasksDashBoardTab" class="selected">Executive&nbsp;DashBoard</a></li>

                    </ul>
                    <div  style="border:1px solid gray; width:840px;height:675px;overflow:auto; margin-bottom: 1em;">    
                        <br>
                        <div id="TasksDashBoardTab" class="tabcontent" >  
                        
                        
                          <div id="overlayResourceType"></div>              <!-- Start Overlay -->

                            <div id="specialBoxResourceType">

                                <div id="resourceTypeDetails"  >
                                   
                                    <table align="center" border="0" cellspacing="0" style="width:100%;margin_left:100px;">
                                        <tr>                               
                                        <td colspan="2" style="background-color: #288AD1" >
                                            <h3 style="color:darkblue;" align="left">
                                                <span id="headerLabel1"></span>


                                            </h3></td>
                                        <td colspan="2" style="background-color: #288AD1" align="right">

                                            <a href="#" onmousedown="toggleCloseOverlay()" >
                                                <img src="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/closeButton.png" /> 

                                            </a>  

                                        </td></tr>
                                        <tr>
                                        <td colspan="4">
                                            <div id="load" style="color: green;display: none;">Loading..</div>
                                            <div id="resultMessage"></div>
                                        </td>
                                        </tr>

                                        <tr>
                                        <td>
                                            <table id="tblResourceTypeDetails"  class="gridTable" width="400" cellspacing="1" cellpadding="7" border="0" align="center" style="margin-left:37px;">
                                                <%--   <script type="text/JavaScript" src="<s:url value="/includes/javascripts/wz_tooltip.js"/>"></script> --%>
                                                <COLGROUP ALIGN="left" >
                                                    <COL width="15%">
                                                        <COL width="15%">


                                                            </table> 
                                                            </td>
                                                           
                                                            </tr>

                                                            </table>    

                                                            </div>
                                                            </div>
                        
                         <!-- Start Overlay -->
                          <div id="overlayTotalResources" class='overlay'></div>            

                   
<div id="specialBoxTotalResources" class='specialBox' style=" padding_left: 50px; margin: -130px auto 0 -100px;width: auto; height: auto;overflow: auto;  overflow-y: auto;">
                                <div id="totalResources"  >
                                    <table align="center" border="0" cellspacing="0" style="width:100%;">
                                        <tr>                               
                                        <td colspan="2" style="background-color: #288AD1" >
                                            <h3 style="color:darkblue;" align="left">
                                                <span id="headerLabel2"></span>


                                            </h3></td>
                                        <td colspan="2" style="background-color: #288AD1" align="right">

                                            <a href="#" onmousedown="toggleCloseOverlay1()" >
                                                <img src="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/closeButton.png" /> 

                                            </a>  

                                        </td></tr>
                                        <tr>
                                        <td colspan="4">
                                            <div id="totalResourcesresultMessage" style="color: green;display: none;"><h2>Loading..</h2></div>
                                            
                                        </td>
                                        </tr>

                                        <tr>
                                        <td>
                                            <table id="totalResourcesTable"  class="gridTable" width="400" cellspacing="1" cellpadding="7" border="0" align="center" style="margin-left:37px;">
                                                <%--   <script type="text/JavaScript" src="<s:url value="/includes/javascripts/wz_tooltip.js"/>"></script> --%>
                                                <COLGROUP ALIGN="left" >
                                                    <COL width="15%">
                                                        <COL width="15%">


                                                            </table> 
                                                            </td>
                                                           
                                                            </tr>

                                                            </table>    

                                                            </div>
                                                            </div>
                          <!-- Start Overlay End-->

                            <table cellpadding="0" cellspacing="0" border="0" width="100%">

                                <%-- Tasks DashBoard start --%>

                                <tr>
                                <td class="homePortlet" valign="top">
                                    <div class="portletTitleBar">
                                        <div class="portletTitleLeft">Practice Status Report</div>
                                        <div class="portletIcons">
                                            <a href="javascript:animatedcollapse.hide('TasksDashBoardDiv')" title="Minimize">
                                                <img src="../includes/images/portal/title_minimize.gif" alt="Minimize"/></a>
                                            <a href="javascript:animatedcollapse.show('TasksDashBoardDiv')" title="Maximize">
                                                <img src="../includes/images/portal/title_maximize.gif" alt="Maximize"/>
                                            </a>
                                        </div>
                                        <div class="clear"></div>
                                    </div>

                                    <div id="TasksDashBoardDiv" style="background-color:#ffffff">
                                        <table cellpadding="0" cellspacing="0" border="0" width="100%"> 
                                            <tr>
                                            <td width="80%" valign="top" align="center">
                                                <s:form action="" theme="simple" name="TasksDashBoard" id="TasksDashBoard">  

                                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                        <tr>
                                                        <td>
                                                            <table border="0" align="center" cellpadding="0" cellspacing="0" width="800;">
                                                                <br>
                                                             <%--   <tr>
                                                                <td class="fieldLabel">Start&nbsp;Date&nbsp;(mm/dd/yyyy)&nbsp;<FONT color="red"  ><em>*</em></FONT>:</td>
                                                                <td><s:textfield name="startDate" id="startDate" cssClass="inputTextBlueSmall" onchange="isValidDate(this)"/><a class="underscore" href="javascript:cal1.popup();">
                                                                        <img src="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/showCalendar.gif" width="20" height="20" border="0"></a>
                                                                </td>

                                                                <td class="fieldLabel">End&nbsp;Date&nbsp;(mm/dd/yyyy)&nbsp;<FONT color="red"  ><em>*</em></FONT>:</td>
                                                                <td>  <s:textfield name="endDate" id="endDate" cssClass="inputTextBlueSmall" onchange="isValidDate(this)"/><a class="underscore" href="javascript:cal2.popup();">
                                                                        <img src="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/showCalendar.gif" width="20" height="20" border="0"></a>
                                                                </td>
                                                                </tr>
--%>
                                                                <tr>
                                                                <td  class="fieldLabel">Department</td>
                                                                 <td ><s:select cssClass="inputSelect" list="#@java.util.LinkedHashMap@{'SSG':'SSG','GDC':'GDC'}" name="department" id="departmentId"  headerValue="--Please Select--" headerKey="All"  /></td>
        
                                                                 <td class="fieldLabel">Practice&nbsp;:</td>                             
                                                                <td ><s:select cssClass="inputSelect" list="practiceMap" name="practiceId" id="practiceId" headerValue="--Please Select--" headerKey="All" value="%{practiceId}" onchange="getOrderBy(this.value)" /></td>
                                                                
                                                                 </tr>
                                                                <tr>
                                                                
                                                                <td class="fieldLabel" > Is&nbsp;Billable&nbsp;Utilization&nbsp;: </td> <td><s:checkbox name="withShadow" id="withShadow" theme="simple" /></td> 
                                                               
                                                                
                                                               <td  class="fieldLabel" id="orderTd" style="display:block;">Order By:</td>
                                                                 <td><s:select cssClass="inputSelect"  style="display:block;" id="orderBy"   list="#@java.util.LinkedHashMap@{'Utilization':'Utilization','ActiveProjects':'ActiveProjects','Resources':'Resources','OnProject':'OnProject','Available':'Available'}" name="orderBy"   headerValue="--Please Select--" headerKey="-1"  /></td>
        
                                                              
                                                               <td >
                                                                 <input  type="button" value="Search" class="buttonBg" id="searchBtn" onclick="getPracticeStatusReportData();"/>
                                                               </td>

                                                                </tr>

                                                            </table>
                                                        </td>
                                                        </tr>
                                                        <tr>
                                                        <td>
                                                            <table border="0" align="center" cellpadding="0" cellspacing="0" >

                                                                <!-- new graph details start -->
                                                                <tr>
                                                                <td height="20px" align="center" colspan="9">
                                                                    <div id="statusReportLoad" style="display:none" class="error" ><b>Loading Please Wait.....</b></div>
                                                                </td>
                                                                </tr>
                                                                <tr>
                                                                <td colspan="4">
                                                                    <table id="tblStatusReport" align='center' cellpadding='1' cellspacing='1' border='0' class="gridTable" width='700' >
                                                                        <COLGROUP ALIGN="left" >
                                                                            <COL width="15%">
                                                                                <COL width="7%">
                                                                                    <COL width="7%">
                                                                                        <COL width="7%">
                                                                                            <COL width="7%">
                                                                                                <COL width="7%">
                                                                                                    <COL width="7%"> 
                                                                                                        <COL width="7%">
                                                                                                            <COL width="7%">

                                                                                                                </table>  



                                                                                                                </td>
                                                                                                                </tr>
                                                                                                                <!-- new graph details end -->

                                                                                                                </table>    
                                                                                                                </td>
                                                                                                                </tr> 
                                                                                                                </table>
                                                                                                            </s:form>
                                                                                                         <%--    <script type="text/JavaScript">
                                                                                                                var cal1 = new CalendarTime(document.forms['TasksDashBoard'].elements['startDate']);
                                                                                                                cal1.year_scroll = true;
                                                                                                                cal1.time_comp = false;
                                                                                                                var cal2 = new CalendarTime(document.forms['TasksDashBoard'].elements['endDate']);
                                                                                                                cal2.year_scroll = true;
                                                                                                                cal2.time_comp = false;
                                                                        
                                                                                                            </script> --%>
                                                                                                            </td>
                                                                                                            </tr>
                                                                                                            </table>

                                                                                                            </div>

                                                                                                            </td>
                                                                                                            </tr>


                                                                                                            <%-- Tasks DashBoard end --%> 		   

<%-- Shadows Utilization Report Start--%>

										<tr>
											<td class="homePortlet" valign="top">
												<div class="portletTitleBar">
													<div class="portletTitleLeft">Shadows Utilization  Report</div>
													<div class="portletIcons">
														<a
															href="javascript:animatedcollapse.hide('ShadowsUtilizationReport')"
															title="Minimize"> <img
															src="../includes/images/portal/title_minimize.gif"
															alt="Minimize" /></a> <a
															href="javascript:animatedcollapse.show('ShadowsUtilizationReport')"
															title="Maximize"> <img
															src="../includes/images/portal/title_maximize.gif"
															alt="Maximize" />
														</a>
													</div>
													<div class="clear"></div>
												</div>
												<div id="ShadowsUtilizationReport"
													style="background-color: #ffffff">
													<table cellpadding="0" cellspacing="0" border="0"
														width="100%">
														<tr>
															<td width="40%" valign="top" align="center"><s:form
																	theme="simple" name="frmShadowsUtilizationReport"
																	id="shadowsUtilizationReport">

																	<table cellpadding="0" cellspacing="0" border="0"
																		width="100%">
																		<tr align="right">
																			<td class="headerText" colspan="9"><img
																				src="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/spacer.gif"
																				width="auto;" height="13px" border="0"></td>
																		</tr>
																		<tr>
																		<td>
																		
																				<table align="center" cellpadding="2" border="0"
																					cellspacing="1" width="auto;">
																		<tr>
																		
                                                                                      <td class="fieldLabel">ReportType :</td>
                                                                                                                                                                                                           <td>
                                                                                                                                                                                                  <s:select list="{'Customer','Project'}" name="reportBasedOn" id="reportBasedOn" value="%{reportBasedOn}" cssClass="inputSelect"  headerKey="-1" headerValue="---Please select---" />
                                                                                                                                                                                                 
                                                                                                                                                                                                 </td>
                                                                                    
                                                                                                                                                                                        <td class="fieldLabel" >Customer Name :</td>
                                                                                                                                                                                                <td>

                                                                                                                                                                                                    <s:select headerKey="-1" headerValue="All" list="clientMap" name="customerName" id="customerNameId" cssClass="inputSelect" theme="simple" onchange="getProjectsByAccountIdProjectOverview();"/>
                                                                                                                                                                                                </td>
                                                                                                                                                                                               

                                                                                                                                                                                              
                                                                                                                                                                                                </tr>
                                                                                                                                                                                              
                                                                                                                                                                                                <tr id="projectNameTr" >
                                                                                                                                                                                                
                                                                                                                                                                                                 <td class="fieldLabel">ProjectName :</td>
                                                                                                                                                                                                <td>

                                                                                                                                                                                                    <s:select headerKey="-1" headerValue="All"  list="{}" name="projectName" id="projectNameId" cssClass="inputSelect" theme="simple"/>
                                                                                                                                                                                                </td>
                                                                                                                                                                                                  <td  class="fieldLabel">Project Status&nbsp;:</td>
                                                                                                                                                                                                <td><s:select list="{'Active','Completed','Terminated','Initiated'}"
                                                                                                                                                                                                        name="status"
                                                                                                                                                                                                        headerKey="-1" 
                                                                                                                                                                                                        headerValue="--Please Select--"
                                                                                                                                                                                                        value="%{status}"
                                                                                                                                                                                                        cssClass="inputSelect" id="statusId"/></td>
                                                                                                                                                                                                  
                                                                                                                                                                                                 </tr>
                                                                                                                                                                                                 <tr id="projectStatusTr">
                                                                                                                                                                                                 
                                                                                                                                                                                                 
                                                                                                                                                                                                 <td class="fieldLabel"  width="200px" >Cost&nbsp;Model :&nbsp;</td>
                                                                                                                                                                               <td> 
                                                                                                                                                                              <s:select name="costModel" id="costModel" cssClass="inputSelect" list="{'Fixed','Time & Material','Internal'}" emptyOption="false" headerKey="-1" headerValue="All" value="%{costModel}" />
                                                                                                                                                                              </td> 
                                                                                                                                                                               <td class="fieldLabel">Project Type :</td>
                                                                                                                                                                                                           <td>
                                                                                                                                                                                                  <s:select list="projectTypesList" name="ProjectTypeId" id="ProjectTypeId" value="%{ProjectTypeId}" cssClass="inputSelect"  headerKey="-1" headerValue="All"/>
                                                                                                                                                                                                 
                                                                                                                                                                                                 </td>
                                                                                                                                                                                                 
                                                                                                                                                     
                                                                                                                                                                                                 
                                                                                                                                                                                                 </tr>
                                                                                                                                                                                                 <tr>
                                                                                                                                                                                                 <td></td>
                                                                                                                                                                                                                                             <td align="right" colspan="3"><input
																							type="button" value="Search" class="buttonBg"
																							onclick="getShadowsUtilizationReportBasedOn();" /></td>
                                                                                                                                                                                                 </tr>
                                                                                                                                                                                                 </table>
                                                                                                                                                                                                 </td>
                                                                                                                                                                                                 </tr>
                                                                                                                                                                                                 
                                                                                                                                                                                                 
                                                                                                                                  

																		
														
																		<%-- table grid  start--%>
																		<tr>
																			<td><br>
																				<table align="center" cellpadding="2" border="0"
																					cellspacing="1" width="50%">

																					<tr>
																						<td height="20px" align="center" colspan="9">
																							<div id="shadowUtilityReport" style="display: none"
																								class="error">
																								<b>Loading Please Wait.....</b>
																							</div>
																						</td>
																					</tr>


																					<tr>
																						<td><br>
																							<div id="shadowsUtilityReport" style="display: block">
																								<!--style="color:#0000FF;font:italic 900 12px arial;"  bgcolor='#3E93D4'-->
																								<table id="tblShadowsUtilityReport" align='center' i
																									cellpadding='1' cellspacing='1' border='0'
																									class="gridTable" width='700'>
																									<COLGROUP ALIGN="left">
																										<COL width="50%">
																										<COL width="20%">
																										<COL width="10%">
																										<COL width="10%">
																										<COL width="10%">
																										
																									</td>
																								</table>
																								<br>
																								<!--<center><span id="spnFast" class="activeFile" style="display: none;"></span></center>-->
																							</div></td>
																					</tr>
																				</table></td>
																		</tr>
																		<%-- table grid  end--%>
																	</table>
																</s:form></td>
														</tr>
													</table>

												</div>
											</td>
										</tr>
										<%-- Shadows Utilization Report End--%>
										
										<%-- Offshore strength start --%>

                                <tr>
                                <td class="homePortlet" valign="top">
                                    <div class="portletTitleBar">
                                        <div class="portletTitleLeft">Offshore&nbsp;Strength&nbsp;Report</div>
                                        <div class="portletIcons">
                                            <a href="javascript:animatedcollapse.hide('OffshoreStrengthDashboardDiv')" title="Minimize">
                                                <img src="../includes/images/portal/title_minimize.gif" alt="Minimize"/></a>
                                            <a href="javascript:animatedcollapse.show('OffshoreStrengthDashboardDiv')" title="Maximize">
                                                <img src="../includes/images/portal/title_maximize.gif" alt="Maximize"/>
                                            </a>
                                        </div>
                                        <div class="clear"></div>
                                    </div>

                                    <div id="OffshoreStrengthDashboardDiv" style="background-color:#ffffff">
                                        <table cellpadding="0" cellspacing="0" border="0" width="100%"> 
                                            <tr>
                                            <td width="80%" valign="top" align="center">
                                                <s:form action="" theme="simple" name="TasksDashBoard" id="TasksDashBoard">  

                                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                        <tr>
                                                        <td>
                                                            <table border="0" align="center" cellpadding="0" cellspacing="0" width="800;">
                                                                <br>
                                                                <tr>
                                                                <td class="fieldLabel">Year&nbsp;:<FONT color="red"  ><em>*</em></FONT></td>
                                                                <td><s:textfield name="year" id="year" cssClass="inputTextBlueSmall" value="%{year}"/>
                                                                </td>

                                                                <td class="fieldLabel">Month&nbsp;:<FONT color="red"  ><em>*</em></FONT></td>
                                                                 <td><s:select list="#@java.util.LinkedHashMap@{'1':'Jan','2':'Feb','3':'Mar','4':'Apr','5':'May','6':'June','7':'July','8':'Aug','9':'Sept','10':'Oct','11':'Nov','12':'Dec'}" name="month" id="month" onchange="" headerValue="select" headerKey="" value="%{month}"/></td>

                                                                </tr>

                                                                <tr>
                                                              <td></td>
                                                              <td></td>
                                                              <td></td>
                                                              <td>
                                                              
                                                                    <input  type="button" value="Search" class="buttonBg" onclick="return getOffshoreStrengthDetails();"/>
                                                                </td>

                                                                </tr>

                                                            </table>
                                                        </td>
                                                        </tr>
                                                        <%-- table grid --%>
                                                                                                                                                                                        <tr>
                                                                                                                                                                                        <td>
                                                                                                                                                                                            <br>
                                                                                                                                                                                            <table align="center" cellpadding="2" border="0" cellspacing="1" width="50%" >

                                                                                                                                                                                                <tr>
                                                                                                                                                                                                <td height="20px" align="center" colspan="9">
                                                                                                                                                                                                    <div id="loadingMessage" style="display:none" class="error" ><b>Loading Please Wait.....</b></div>
                                                                                                                                                                                                </td>
                                                                                                                                                                                                </tr>


                                                                                                                                                                                                <tr>
                                                                                                                                                                                                <td ><br>
                                                                                                                                                                                                    <div id="OffshoreStrengthList" style="display: block">
                                                                                                                                                                                                        <!--style="color:#0000FF;font:italic 900 12px arial;"  bgcolor='#3E93D4'-->
                                                                                                                                                                                                        <table id="tblOffshoreStrength" align='center' i cellpadding='1' cellspacing='1' border='0' class="gridTable" width='750'>


                                                                                                                                                                                                        </table> <br>
                                                                                                                                                                                                        <!--<center><span id="spnFast" class="activeFile" style="display: none;"></span></center>-->
                                                                                                                                                                                                    </div>
                                                                                                                                                                                                </td>
                                                                                                                                                                                                </tr>
                                                                                                                                                                                            </table>    
                                                                                                                                                                                        </td>
                                                                                                                                                                                        </tr>
                                                                                                                                                                                        <%-- table grid  end--%>
                                                                                                                </table>
                                                                                                            </s:form>
                                                                                                           
                                                                                                            </td>
                                                                                                            </tr>
                                                                                                            </table>

                                                                                                            </div>

                                                                                                            </td>
                                                                                                            </tr>


                                                                                                            <%-- Offshore strength end  --%>	
                                                                                                            
                                                                                                            
                                                                                                            <%-- OffshoreDeliveryStrengthDashboardDiv Start--%>

										<tr>
											<td class="homePortlet" valign="top">
												<div class="portletTitleBar">
													<div class="portletTitleLeft">Offshore&nbsp;-&nbsp;Delivery&nbsp;Strength</div>
													<div class="portletIcons">
														<a
															href="javascript:animatedcollapse.hide('OffshoreDeliveryStrengthDashboardDiv')"
															title="Minimize"> <img
															src="../includes/images/portal/title_minimize.gif"
															alt="Minimize" /></a> <a
															href="javascript:animatedcollapse.show('OffshoreDeliveryStrengthDashboardDiv')"
															title="Maximize"> <img
															src="../includes/images/portal/title_maximize.gif"
															alt="Maximize" />
														</a>
													</div>
													<div class="clear"></div>
												</div>

												<div id="OffshoreDeliveryStrengthDashboardDiv"
													style="background-color: #ffffff">
													<table cellpadding="0" cellspacing="0" border="0"
														width="100%">
														<tr>
															<td width="80%" valign="top" align="center">
															<s:form
																	action="" theme="simple"
																	name="OffshoreDeliveryStrengthDashboard"
																	id="OffshoreDeliveryStrengthDashboard">

																	<table cellpadding="0" cellspacing="0" border="0"
																		width="100%">
																		<tr>
																			<td>
																				<table border="0" align="center" cellpadding="0"
																					cellspacing="0" width="800;">
																					<br>


																					<tr>

																						<td class="fieldLabel">Practice&nbsp;:</td>
																						<td><s:select cssClass="inputSelect"
																								list="practiceMap" name="offshorepracticeId"
																								id="offshorepracticeId"
																								headerValue="--Please Select--" headerKey="-1"
																								value="%{offshorepracticeId}"
																								onchange="getSubPracticeList();" /></td>


																						<td class="fieldLabel" width="200px" align="right">SubPractice&nbsp;Name&nbsp;:</td>

																						<td><s:select name="subPractice"
																								id="subPractice" headerKey="All"
																								headerValue="-- Please Select --" list="{}"
																								cssClass="inputSelectNew" theme="simple" /></td>

																					
																						
																						<td><input type="button" value="Search"
																							id="OffshoreDeliveryStrengthBTN" class="buttonBg"
																							onclick="OffshoreDeliveryStrengthDetails();" /></td>

																					</tr>
																				</table>
																			</td>
																		</tr>
																		<tr>
																			<td>
																				<table border="0" align="center" cellpadding="0"
																					cellspacing="0">

																					<!-- new graph details start -->
																					<tr>
																						<td height="20px" align="center" colspan="9">
																							<div id="OffshoreDeliveryStrengthLoad"
																								style="display: none" class="error">
																								<b>Loading Please Wait.....</b>
																							</div>
																						</td>
																					</tr>
																					<tr>
																						<td colspan="4">
																							<table id="OffshoreDeliveryStrengthReport"
																								align='center' cellpadding='1' cellspacing='1'
																								border='0' class="gridTable" width='700'>
																								<COLGROUP ALIGN="left">
																							</table>



																						</td>
																					</tr>
																					<!-- new graph details end -->

																				</table>
																			</td>
																		</tr>
																	</table>
																</s:form> 
															</td>
														</tr>
													</table>

												</div>

											</td>
										</tr>


										<%--OffshoreDeliveryStrength end --%>

                                                                                                            
                                                                                                            
                                                                                                            
										                                                                     <%--Customer Project Roll In  start --%>

                                <tr>
                                <td class="homePortlet" valign="top">
                                    <div class="portletTitleBar">
                                        <div class="portletTitleLeft">Customer/Project-RollIn/RollOff&nbsp;Report</div>
                                        <div class="portletIcons">
                                            <a href="javascript:animatedcollapse.hide('CustomerProjectRollINRollOffDiv')" title="Minimize">
                                                <img src="../includes/images/portal/title_minimize.gif" alt="Minimize"/></a>
                                            <a href="javascript:animatedcollapse.show('CustomerProjectRollINRollOffDiv')" title="Maximize">
                                                <img src="../includes/images/portal/title_maximize.gif" alt="Maximize"/>
                                            </a>
                                        </div>
                                        <div class="clear"></div>
                                    </div>

                                    <div id="CustomerProjectRollINRollOffDiv" style="background-color:#ffffff">
                                        <table cellpadding="0" cellspacing="0" border="0" width="100%"> 
                                            <tr>
                                            <td width="80%" valign="top" align="center">
                                                <s:form action="" theme="simple" name="customerDashBoard" id="customerDashBoard">  

                                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                    
                                                    
                                                        <td>
                                                            <table border="0" align="center" cellpadding="0" cellspacing="0" width="800;">
                                                                <br>
                                                                <tr>

																						<td class="fieldLabel">Start&nbsp;Date&nbsp;(mm/dd/yyyy)&nbsp;:<FONT
																							color="red"><em>*</em></FONT></td>
																						<td><s:textfield name="startDate"
																								id="startDate"
																								cssClass="inputTextBlueSmall" value="%{startDate}" /><a
																							href="javascript:cal1.popup();"> <img
																								src="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/showCalendar.gif"
																								width="20" height="20" border="0"></a></td>

																						<td class="fieldLabel">End&nbsp;Date&nbsp;(mm/dd/yyyy)&nbsp;:<FONT
																							color="red"><em>*</em></FONT></td>
																						<td><s:textfield name="endDate"
																								id="endDate" cssClass="inputTextBlueSmall"
																								value="%{endDate}" /><a href="javascript:cal2.popup();">
																								<img
																								src="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/showCalendar.gif"
																								width="20" height="20" border="0">
																						</a></td>

																					</tr>
                                                    
                                                    
                                                        <tr>
                                                                <tr>
                                                               

                                                                <td class="fieldLabel">Type&nbsp;:</td>
                                                                 <td><s:select list="{'Roll-In','Roll-Off'}" name="type" id="type"  headerValue="select" headerKey="-1" /></td>

                                                                </tr>

                                                                <tr>
                                                              <td></td>
                                                              <td></td>
                                                              <td></td>
                                                              <td>
                                                              
                                                                    <input  type="button" value="Search" class="buttonBg" onclick="return getCustomerProjectRollInRollOffDetails();"/>
                                                                </td>

                                                                </tr>
                                                                
                                                                <script type="text/JavaScript">
                                                            var cal1 = new CalendarTime(document.forms['customerDashBoard'].elements['startDate']);
                                                            cal1.year_scroll = true;
                                                            cal1.time_comp = false;
                                                            var cal2 = new CalendarTime(document.forms['customerDashBoard'].elements['endDate']);
                                                            cal2.year_scroll = true;
                                                            cal2.time_comp = false;
                                                        </script>

                                                            </table>
                                                        </td>
                                                        </tr>
                                                        <%-- table grid --%>
                                                                                                                                                                                        <tr>
                                                                                                                                                                                        <td>
                                                                                                                                                                                            <br>
                                                                                                                                                                                            <table align="center" cellpadding="2" border="0" cellspacing="1" width="50%" >

                                                                                                                                                                                                <tr>
                                                                                                                                                                                                <td height="20px" align="center" colspan="9">
                                                                                                                                                                                                    <div id="loadingMessage1" style="display:none" class="error" ><b>Loading Please Wait.....</b></div>
                                                                                                                                                                                                </td>
                                                                                                                                                                                                </tr>


                                                                                                                                                                                                <tr>
                                                                                                                                                                                                <td ><br>
                                                                                                                                                                                                    <div id="customerProjectRol" style="display: block">
                                                                                                                                                                                                        <!--style="color:#0000FF;font:italic 900 12px arial;"  bgcolor='#3E93D4'-->
                                                                                                                                                                                                        <table id="tblResourcesRollInRollOff" align='center' i cellpadding='1' cellspacing='1' border='0' class="gridTable" width='750'>


                                                                                                                                                                                                        </table> <br>
                                                                                                                                                                                                        <!--<center><span id="spnFast" class="activeFile" style="display: none;"></span></center>-->
                                                                                                                                                                                                    </div>
                                                                                                                                                                                                </td>
                                                                                                                                                                                                </tr>
                                                                                                                                                                                            </table>    
                                                                                                                                                                                        </td>
                                                                                                                                                                                        </tr>
                                                                                                                                                                                        <%-- table grid  end--%>
                                                                                                                </table>
                                                                                                            </s:form>
                                                                                                           
                                                                                                            </td>
                                                                                                            </tr>
                                                                                                            </table>

                                                                                                            </div>

                                                                                                            </td>
                                                                                                            </tr>


                                                                                                            <%-- Customer Project RollIn RollOff end  --%>
                                                                                                            
                                                                                                            
                                                                                                            
                                                                                                            <%--      Roll-In and Roll-Off – Overall Analysis: start --%>

										<tr>
											<td class="homePortlet" valign="top">
												<div class="portletTitleBar">
													<div class="portletTitleLeft">Onboard and Exit -
														Overall Analysis&nbsp;Report</div>
													<div class="portletIcons">
														<a
															href="javascript:animatedcollapse.hide('RollInandRollOffOverallAnalysisDiv')"
															title="Minimize"> <img
															src="../includes/images/portal/title_minimize.gif"
															alt="Minimize" /></a> <a
															href="javascript:animatedcollapse.show('RollInandRollOffOverallAnalysisDiv')"
															title="Maximize"> <img
															src="../includes/images/portal/title_maximize.gif"
															alt="Maximize" />
														</a>
													</div>
													<div class="clear"></div>
												</div>

												<div id="RollInandRollOffOverallAnalysisDiv"
													style="background-color: #ffffff" align="center">
													<table cellpadding="0" cellspacing="0" border="0"
														width="100%">
														<tr>
															<td width="80%" valign="top" align="center"><s:form
																	action="" theme="simple" name="RollInRollOffDashBoard"
																	id="RollInRollOffDashBoard">

																	<table cellpadding="0" cellspacing="0" border="0"
																		width="100%">

																			<tr>		
																		<td>
																			<table border="0" align="center" cellpadding="0"
																				cellspacing="0" width="800;">
																				<br>
																				<tr>

																					<td class="fieldLabel">Year&nbsp:<FONT
																						color="red"><em>*</em></FONT></td>
																					<td><s:textfield name="year" id="year1"
																							cssClass="inputTextBlueSmall" maxlength="4" value="%{year}" onkeyup="isNumericYearRoll(this);"/></td>

																					<td class="fieldLabel" width="200px" align="right">Country
																						:</td>

																					<td colspan="3"><s:select
																							label="Select Country" name="country"
																							id="country" 
																							list="countryList" cssClass="inputSelect"
																							value="%{country}" /></td>

																				</tr>


																				<tr>
																				<tr>


																					<td class="fieldLabel">Report&nbsp;Based&nbsp;on:</td>
																					<td><s:select list="{'Quarterly','Monthly'}"
																							name="reportBasedOn" id="reportBased"
																							 cssClass="inputSelect"
																							value="%{reportBasedOn}" /></td>
																							
																							
																							<td class="fieldLabel" width="200px" align="right">Department
																							:</td>
																						<td><s:select name="departmentId"
																								id="departmentId3"
																								list="{'GDC','Marketing','Recruiting','Sales','Operations','SSG'}" cssClass="inputSelect" headerKey="-1"
																								headerValue="All" /></td>

																				</tr>

																				<tr>




																					<td></td>
																					<td></td>
																					<td></td>
																					<td><input type="button" value="Search" id="rollSearch"
																						class="buttonBg"
																						onclick="return getRollInRollOffAnalysis();" /></td>

																				</tr>

																		

																			</table>
																		</td>
																		</tr>
																		<%-- table grid --%>
																		<tr>
																			<td><br>
																				<table align="center" cellpadding="2" border="0"
																					cellspacing="1" width="100%">

																					<tr>
																						<td height="px" align="center" colspan="9">
																							<div id="loadingMessage6" style="display: none"
																								class="error">
																								<b>Loading Please Wait.....</b>
																							</div>
																						</td>
																					</tr>


																					<tr>
																						<td><br>
																							<div id="ProjectRolinoff">
																								<!--style="color:#0000FF;font:italic 900 12px arial;"  bgcolor='#3E93D4'-->
																								<!--     <table id="tblRollInRollOffAnalysis" align='center' i cellpadding='1' cellspacing='1' border='0' class="gridTable" width='750'>


                                                                                                                                                                                                        </table> <br>
                                                                           -->
																								<!--<center><span id="spnFast" class="activeFile" style="display: none;"></span></center>-->
																							</div></td>
																							
																							
																					</tr>
																					<tr>
																					<td>
																					 <div class="rcorners2" id="onboardtatusStack" style="width: 692px;margin-left:10px;margin-top: 10px;" >
																							
                                                                                          </div>
                                                                                          </td>
																					</tr>
																					
																				</table></td>
																		</tr>
																		<%-- table grid  end--%>
																	</table>
																</s:form></td>
														</tr>
													</table>

												</div>

											</td>
										</tr>


										<%--  Roll-In and Roll-Off – Overall Analysis end  --%>
										
										
										

										<%--Project Resources -RollIn/RollOff Report   start --%>

										<tr>
											<td class="homePortlet" valign="top">
												<div class="portletTitleBar">
													<div class="portletTitleLeft">Project Resources
														-RollIn/RollOff&nbsp;Report</div>
													<div class="portletIcons">
														<a
															href="javascript:animatedcollapse.hide('ProjectResourcesRollInRollOffReportDiv')"
															title="Minimize"> <img
															src="../includes/images/portal/title_minimize.gif"
															alt="Minimize" /></a> <a
															href="javascript:animatedcollapse.show('ProjectResourcesRollInRollOffReportDiv')"
															title="Maximize"> <img
															src="../includes/images/portal/title_maximize.gif"
															alt="Maximize" />
														</a>
													</div>
													<div class="clear"></div>
												</div>

												<div id="ProjectResourcesRollInRollOffReportDiv"
													style="background-color: #ffffff">
													<table cellpadding="0" cellspacing="0" border="0"
														width="100%">
														<tr>
															<td width="80%" valign="top" align="center"><s:form
																	action="" theme="simple"
																	name="ProjectResourcesDashBoard"
																	id="ProjectResourcesDashBoard">

																	<table cellpadding="0" cellspacing="0" border="0"
																		width="100%">


																		<td>
																			<table border="0" align="center" cellpadding="0"
																				cellspacing="0" width="800;">
																				<br>
																				<tr>

																					<td class="fieldLabel">Start&nbsp;Date&nbsp;(mm/dd/yyyy)&nbsp;:<FONT
																						color="red"><em>*</em></FONT></td>
																					<td><s:textfield name="projstartDate"
																							id="projstartDate" cssClass="inputTextBlueSmall"
																							value="%{projstartDate}" /><a
																						href="javascript:cal1.popup();"> <img
																							src="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/showCalendar.gif"
																							width="20" height="20" border="0"></a></td>

																					<td class="fieldLabel">End&nbsp;Date&nbsp;(mm/dd/yyyy)&nbsp;:<FONT
																						color="red"><em>*</em></FONT></td>
																					<td><s:textfield name="endDate" id="endDate5"
																							cssClass="inputTextBlueSmall" value="%{endDate}" /><a
																						href="javascript:cal2.popup();"> <img
																							src="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/showCalendar.gif"
																							width="20" height="20" border="0">
																					</a></td>

																				</tr>

																					<tr>
																					
																					<td class="fieldLabel">Customer Name :</td>
																						<td><s:select headerKey="-1"
																								headerValue="All" list="clientMap"
																								name="customerName" id="customerNameId5"
																								cssClass="inputSelect" theme="simple"
																								 />
																						</td>
																					
																					<td class="fieldLabel" width="200px">Cost&nbsp;Model
																							:&nbsp;</td>
																						<td><s:select name="costModel" id="costModel5"
																								cssClass="inputSelect"
																								list="{'Fixed','Time & Material','Internal'}"
																								emptyOption="false" headerKey="-1"
																								headerValue="All" value="%{costModel}" /></td>
																					
																					</tr>
																				
																				<tr>
																					<td class="fieldLabel" width="200px" align="right">Department&nbsp;:</td>
																					<td><s:select label="Select Department"
																							name="departmentId" id="departmentId4"
																							headerKey="-1" headerValue="--Please Select--"
																							list="departmentIdList" cssClass="inputSelect"
																							value="%{departmentId}"
																							onchange="getPracticeDataV1();" /></td>
																					<td class="fieldLabel" align="right">Practice
																						Name&nbsp;:</td>
																					<td><s:select label="Select Practice Name"
																							name="practiceId" id="practiceId4" headerKey="-1"
																							headerValue="-- Please Select --"
																							list="practiceIdList" cssClass="inputSelect"
																							value="%{practiceId}" /> <%--onchange="getTeamData();" --%></td>


																				</tr>

																				<tr>
																				


																					<td class="fieldLabel">Type&nbsp;:</td>
																					<td><s:select list="{'Roll-In','Roll-Off'}"
																						headerValue="select"
																							headerKey="-1" 	name="type" id="type1" /></td>
																							</tr>

																				<tr>
																					<td></td>
																					<td></td>
																					<td></td>
																					<td><input type="button" value="Search"
																						class="buttonBg"
																						onclick="return getProjectResourcesRollInRollOffReportDetails();" />
																					</td>

																				</tr>

																				<script type="text/JavaScript">
                                                            var cal1 = new CalendarTime(document.forms['ProjectResourcesDashBoard'].elements['projstartDate']);
                                                            cal1.year_scroll = true;
                                                            cal1.time_comp = false;
                                                            var cal2 = new CalendarTime(document.forms['ProjectResourcesDashBoard'].elements['endDate5']);
                                                            cal2.year_scroll = true;
                                                            cal2.time_comp = false;
                                                        </script>

																			</table>
																		</td>
																		</tr>
																		<%-- table grid --%>
																		<tr>
																			<td><br>
																				<table align="center" cellpadding="2" border="0"
																					cellspacing="1" width="50%">

																					<tr>
																						<td height="20px" align="center" colspan="9">
																							<div id="loadingMessage8" style="display: none"
																								class="error">
																								<b>Loading Please Wait.....</b>
																							</div>
																						</td>
																					</tr>


																					<tr>
																						<td><br>
																							<div id="ResourceProjectRol"
																								style="display: block">
																								<!--style="color:#0000FF;font:italic 900 12px arial;"  bgcolor='#3E93D4'-->
																								<table id="tblProjectResourcesRollInRollOff"
																									align='center' i cellpadding='1'
																									cellspacing='1' border='0' class="gridTable"
																									width='750'>


																								</table>
																								<br>
																								<!--<center><span id="spnFast" class="activeFile" style="display: none;"></span></center>-->
																							</div></td>
																					</tr>
																				</table></td>
																		</tr>
																		<%-- table grid  end--%>
																	</table>
																</s:form></td>
														</tr>
													</table>

												</div>

											</td>
										</tr>


										<%-- Project Resources -RollIn/RollOff Report  end  --%>


                                                                                                            
                                                                                                            
                                                                                                            
                                                                                                            

                                                                                                            </table>

                                                                                                            <%--     </sx:div>
                                                                                                    </sx:tabbedpanel> --%>
                                                                                                            <!--//END TABBED PANNEL : --> 
                                                                                                            </div>
                                                                                                            </div>
                                                                                                            <script type="text/javascript">

                                                                                                                var countries=new ddtabcontent("reportsTab")
                                                                                                                countries.setpersist(false)
                                                                                                                countries.setselectedClassTarget("link") //"link" or "linkparent"
                                                                                                                countries.init()

                                                                                                            </script>

                                                                                                            </td>
                                                                                                            <!--//END DATA COLUMN : Coloumn for Screen Content-->
                                                                                                            </tr>
                                                                                                            </table>
                                                                                                            </td>
                                                                                                            </tr>
                                                                                                            <!--//END DATA RECORD : Record for LeftMenu and Body Content-->

                                                                                                            <!--//START FOOTER : Record for Footer Background and Content-->
                                                                                                            <tr class="footerBg">
                                                                                                            <td align="center"><s:include value="/includes/template/Footer.jsp"/></td>
                                                                                                            </tr>
                                                                                                            <!--//END FOOTER : Record for Footer Background and Content-->
                                                                                                            </table>
                                                                                                            <script type="text/javascript">
                                                                                                                var recordPage=10;
                                                                                                                function pagerOption(){

                                                                                                                    var paginationSize =10;
                                                                                                                    if(isNaN(paginationSize))
                                                                                                                    {
                       
                                                                                                                    }
                                                                                                                    recordPage=paginationSize;
                                                                                                                    // alert(recordPage)
                                                                                                                    $('#tblTasksDashBoard').tablePaginate({navigateType:'navigator'},recordPage);

                                                                                                                };
                                                                                                              
                                                                                                                                                                                                      
                                                                                                                                                                                                      
                                                                                                            </script>
                                                                                                            <script type="text/JavaScript" src="<s:url value="/includes/javascripts/ajaxPaginationForTaskDashboard.js"/>"></script>

                                                                                                            <!--//END MAIN TABLE : Table for template Structure-->
    <script type="text/javascript">
   
		$(window).load(function(){
		
	
		  animatedcollapse.addDiv('TasksDashBoardDiv', 'fade=1;persist=1;group=app');
		  animatedcollapse.addDiv('ShadowsUtilizationReport', 'fade=1;persist=1;group=app');
		  animatedcollapse.addDiv('OffshoreStrengthDashboardDiv', 'fade=1;speed=400;persist=1;group=app');
		  animatedcollapse.addDiv('OffshoreDeliveryStrengthDashboardDiv','fade=1;persist=1;group=app');

          animatedcollapse.addDiv('CustomerProjectRollINRollOffDiv', 'fade=1;speed=400;persist=1;group=app');
          animatedcollapse.addDiv('RollInandRollOffOverallAnalysisDiv', 'fade=1;speed=400;persist=1;group=app');
          animatedcollapse.addDiv('ProjectResourcesRollInRollOffReportDiv', 'fade=1;speed=400;persist=1;group=app');
		  animatedcollapse.init();
		});
		</script>     
		
		  <script type="text/javascript">
	$(window).load(function(){
	
//	animatedcollapse.show('OffshoreStrengthDashboardDiv');
	getOffshoreStrengthDetails();
	});
	</script>                                                                                                    
    </body>

                                                                                                            </html>