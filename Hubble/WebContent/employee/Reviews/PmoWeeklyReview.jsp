<%-- 
    Document   : pmoDashBoard
    Created on : Dec 18, 2015, 2:46:51 PM
    Author     : miracle
--%>

<%@page import="java.util.Map"%>
<%-- 
    Document   : pmoDashBoard
    Created on : Dec 17, 2015, 8:50:01 PM
    Author     : miracle
--%>


<%@ page contentType="text/html; charset=UTF-8"
	errorPage="../exception/ErrorDisplay.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%-- <%@ taglib prefix="sx" uri="/struts-dojo-tags" %> --%>
<%@ page import="com.mss.mirage.util.ApplicationConstants"%>
<%@ page import="java.sql.Connection"%>
<%@ page import="java.sql.Statement"%>
<%@ page import="java.sql.ResultSet"%>
<%@ page import="java.sql.SQLException"%>
<%@ page import="com.mss.mirage.util.ConnectionProvider"%>
<html>
<head>
<title>Hubble Organization Portal :: TimeSheet Audit</title>
    <link rel="stylesheet" type="text/css" href="<s:url value="/includes/css/new/x0popup.min.css"/>">
<link rel="stylesheet" type="text/css"
	href="<s:url value="/includes/css/GridStyle.css"/>">
<link rel="stylesheet" type="text/css"
	href="<s:url value="/includes/css/mainStyle.css"/>">
<link rel="stylesheet" type="text/css"
	href="<s:url value="/includes/css/tabedPanel.css"/>">
<script type="text/JavaScript"
	src="<s:url value="/includes/javascripts/animatedcollapse.js"/>"></script>
<script type="text/JavaScript"
	src="<s:url value="/includes/javascripts/jquery-1.2.2.pack.js"/>"></script>
<link rel="stylesheet" type="text/css"
	href="<s:url value="/includes/css/leftMenu.css"/>">
<script type="text/JavaScript"
	src="<s:url value="/includes/javascripts/CalendarTime.js"/>"></script>
<script type="text/JavaScript"
	src="<s:url value="/includes/javascripts/AppConstants.js"/>"></script>
<script type="text/JavaScript"
	src="<s:url value="/includes/javascripts/Activity.js"/>"></script>
<script type="text/JavaScript"
	src="<s:url value="/includes/javascripts/EnableEnter.js"/>"></script>
<script type="text/JavaScript"
	src="<s:url value="/includes/javascripts/ReusableContainer.js"/>"></script>
<script type="text/JavaScript"
	src="<s:url value="/includes/javascripts/tabedPanel.js"/>"></script>
<script type="text/JavaScript"
	src="<s:url value="/includes/javascripts/employee/DateValidator.js"/>"></script>
<script type="text/JavaScript"
	src="<s:url value="/includes/javascripts/pmoDashBoardAjax.js?version=1.7"/>"></script>
<script type="text/javascript"
	src="<s:url value="/includes/javascripts/IssueFill.js"/>"></script>
<%-- 	   <script type="text/JavaScript" src="<s:url value="/includes/javascripts/payroll/jquery.min.js"/>"></script> --%>
<link rel="stylesheet" type="text/css" href="<s:url value="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css"/>">
   
<%-- <script type="text/JavaScript" src="<s:url value="/includes/javascripts/payroll/payrollajaxscript.js"/>"></script>
         <script type="text/JavaScript" src="<s:url value="/includes/javascripts/payroll/payrollclientvalidations.js"/>"></script> --%>
<s:include value="/includes/template/headerScript.html" />
<script language="JavaScript">
	animatedcollapse.addDiv('consolidateReport', 'fade=1;persist=1;group=app');
	animatedcollapse.init();
	function hideSelect() {
		//document.getElementById("priorityId").style.display = 'none';

	}
</script>

<style type="text/css">
.popupItem:hover {
	background: #F2F5A9;
	font: arial;
	font-size: 10px;
	color: black;
}

.popupRow {
	background: #3E93D4;
}

.popupItem {
	padding: 2px;
	width: 100%;
	border: black;
	font: normal 9px verdana;
	color: white;
	text-decoration: none;
	line-height: 13px;
	z-index: 100;
}

.totalCss {
	background-color: #b0c4de;
	border: 1px solid #fffaf0;
	color: #000;
	font-weight: bold;
}

.Main0 {
	background-color: #00BFFF;
}

.Main1 {
	background-color: #e1bfff;
}

.Shadow0 {
	background-color: #ADD8E6;
}

.Shadow1 {
	background-color: #E0FFFF;
}

.filled {
	background-color: #F0E68C;
}

.readOnlyCss {
	/* background-color: #b0c4de; */
	border-width: 0;
}




.squaredGreen {
	width: 20px;
	height: 20px;
	background: #01680d;

	background: -webkit-linear-gradient(top, #379604 0%, #077c15 40%, #01680d 100%);
	background: -moz-linear-gradient(top, #379604 0%, #077c15 40%, #01680d 100%);
	background: -o-linear-gradient(top, #379604 0%, #077c15 40%, #01680d 100%);
	background: -ms-linear-gradient(top, #379604 0%, #077c15 40%, #01680d 100%);
	background: linear-gradient(top, #379604 0%, #077c15 40%, #01680d 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#077c15', endColorstr='#01680d',GradientType=0 );
	margin: 0px 0px 0px 0px;
	
		-webkit-border-radius: 50px;
	-moz-border-radius: 50px;
	
	-webkit-box-shadow: inset 0px 1px 1px #01680d, 0px 1px 3px rgba(0,0,0,0.5);
	-moz-box-shadow: inset 0px 1px 1px #01680d, 0px 1px 3px rgba(0,0,0,0.5);
	box-shadow: inset 0px 1px 1px #01680d, 0px 1px 3px rgba(0,0,0,0.5);
	position: relative;
}

.squaredGreen label {
	cursor: pointer;
	position: absolute;
	width: 11.5px;
	height: 11px;
	left: 4.5px;
	top: 3.5px;

	-webkit-box-shadow: inset 0px 1px 1px rgba(0,0,0,0.5), 0px 1px 0px rgba(255,255,255,1);
	-moz-box-shadow: inset 0px 1px 1px rgba(0,0,0,0.5), 0px 1px 0px rgba(255,255,255,1);
	box-shadow: inset 0px 1px 1px rgba(0,0,0,0.5), 0px 1px 0px rgba(255,255,255,1);

	background: -webkit-linear-gradient(top, #ffffff 0%, #ffffff 100%);
	background: -moz-linear-gradient(top, #ffffff 0%, #ffffff 100%);
	background: -o-linear-gradient(top, #ffffff 0%, #ffffff 100%);
	background: -ms-linear-gradient(top, #ffffff 0%, #ffffff 100%);
	background: linear-gradient(top, #ffffff 0%, #ffffff 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#ffffff',GradientType=0 );
}

.squaredGreen label:after {
	-ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";
	filter: alpha(opacity=0);
	opacity: 0;
	content: '';
	position: absolute;
	width: 9px;
	height: 9px;
	background: #01680d;

	background: -webkit-linear-gradient(top, #009400 0%, #01680d 100%);
	background: -moz-linear-gradient(top, #009400 0%, #01680d 100%);
	background: -o-linear-gradient(top, #009400 0%, #01680d 100%);
	background: -ms-linear-gradient(top, #009400 0%, #01680d 100%);
	background: linear-gradient(top, #009400 0%, #01680d 100%);

	top: 1px;
	left: 1.5px;

	-webkit-box-shadow: inset 0px 1px 1px white, 0px 1px 3px rgba(0,0,0,0.5);
	-moz-box-shadow: inset 0px 1px 1px white, 0px 1px 3px rgba(0,0,0,0.5);
	box-shadow: inset 0px 1px 1px white, 0px 1px 3px rgba(0,0,0,0.5);
}

.squaredGreen label:hover::after {
	-ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=30)";
	filter: alpha(opacity=30);
	opacity: 0.3;
}

.squaredGreen input[type=checkbox]:checked + label:after {
	-ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=100)";
	filter: alpha(opacity=100);
	opacity: 1;
}


.squaredAmber {
	width: 20px;
	height: 20px; 
	
	background: #e8a304;
margin: -20px 0px 0px 23px;
	background: -webkit-linear-gradient(top, #f9b311 0%, #eaa70e 40%, #e8a304 100%);
	background: -moz-linear-gradient(top, #f9b311 0%, #eaa70e 40%, #e8a304 100%);
	background: -o-linear-gradient(top, #f9b311 0%, #eaa70e 40%, #e8a304 100%);
	background: -ms-linear-gradient(top, #f9b311 0%, #eaa70e 40%, #e8a304 100%);
	background: linear-gradient(top, #f9b311 0%, #eaa70e 40%, #e8a304 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#eaa70e', endColorstr='#e8a304',GradientType=0 );

-webkit-border-radius: 50px;
	-moz-border-radius: 50px;

	-webkit-box-shadow: inset 0px 1px 1px #e8a304, 0px 1px 3px rgba(0,0,0,0.5);
	-moz-box-shadow: inset 0px 1px 1px #e8a304, 0px 1px 3px rgba(0,0,0,0.5);
	box-shadow: inset 0px 1px 1px #e8a304, 0px 1px 3px rgba(0,0,0,0.5);
	position: relative;
}

.squaredAmber label {
	cursor: pointer;
	position: absolute;
		 width: 11.5px;
	height: 11px;
	left: 4.5px;
	top: 3.5px;

	-webkit-box-shadow: inset 0px 1px 1px rgba(0,0,0,0.5), 0px 1px 0px rgba(255,255,255,1);
	-moz-box-shadow: inset 0px 1px 1px rgba(0,0,0,0.5), 0px 1px 0px rgba(255,255,255,1);
	box-shadow: inset 0px 1px 1px rgba(0,0,0,0.5), 0px 1px 0px rgba(255,255,255,1);

	background: -webkit-linear-gradient(top, #ffffff 0%, #ffffff 100%);
	background: -moz-linear-gradient(top, #ffffff 0%, #ffffff 100%);
	background: -o-linear-gradient(top, #ffffff 0%, #ffffff 100%);
	background: -ms-linear-gradient(top, #ffffff 0%, #ffffff 100%);
	background: linear-gradient(top, #ffffff 0%, #ffffff 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#ffffff',GradientType=0 );
}

.squaredAmber label:after {
	-ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";
	filter: alpha(opacity=0);
	opacity: 0;
	content: '';
	position: absolute;
	width: 9px;
	height: 9px;
	background: #e8a304;

	background: -webkit-linear-gradient(top, #f9b311 0%, #e8a304 100%);
	background: -moz-linear-gradient(top, #f9b311 0%, #e8a304 100%);
	background: -o-linear-gradient(top, #f9b311 0%, #e8a304 100%);
	background: -ms-linear-gradient(top, #f9b311 0%, #e8a304 100%);
	background: linear-gradient(top, #f9b311 0%, #e8a304 100%);

	top: 1px;
	left: 1.5px;

	-webkit-box-shadow: inset 0px 1px 1px white, 0px 1px 3px rgba(0,0,0,0.5);
	-moz-box-shadow: inset 0px 1px 1px white, 0px 1px 3px rgba(0,0,0,0.5);
	box-shadow: inset 0px 1px 1px white, 0px 1px 3px rgba(0,0,0,0.5);
}

.squaredAmber label:hover::after {
	-ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=30)";
	filter: alpha(opacity=30);
	opacity: 0.3;
}

.squaredAmber input[type=checkbox]:checked + label:after {
	-ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=100)";
	filter: alpha(opacity=100);
	opacity: 1;
}


.squaredRed {
	width: 20px;
	height: 20px;  
	background: #db0808;

	 margin: -20px 0px 0px 46px;
	background: -webkit-linear-gradient(top, #f20909 0%, #db0808 40%, #db0a0a 100%);
	background: -moz-linear-gradient(top, #f20909 0%, #db0808 40%, #db0a0a 100%);
	background: -o-linear-gradient(top, #f20909 0%, #db0808 40%, #db0a0a 100%);
	background: -ms-linear-gradient(top, #f20909 0%, #db0808 40%, #db0a0a 100%);
	background: linear-gradient(top, #f20909 0%, #db0808 40%, #db0a0a 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#db0808', endColorstr='#db0a0a',GradientType=0 );
	
	-webkit-border-radius: 50px;
	-moz-border-radius: 50px;
	
	-webkit-box-shadow: inset 0px 1px 1px #db0808, 0px 1px 3px rgba(0,0,0,0.5);
	-moz-box-shadow: inset 0px 1px 1px #db0808, 0px 1px 3px rgba(0,0,0,0.5);
	box-shadow: inset 0px 1px 1px #db0808, 0px 1px 3px rgba(0,0,0,0.5);
	position: relative;
}

.squaredRed label {
	cursor: pointer;
	position: absolute;
	 
	width: 11.5px;
	height: 11px;
	left: 4.5px;
	top: 3.5px;
	

	-webkit-box-shadow: inset 0px 1px 1px rgba(0,0,0,0.5), 0px 1px 0px rgba(255,255,255,1);
	-moz-box-shadow: inset 0px 1px 1px rgba(0,0,0,0.5), 0px 1px 0px rgba(255,255,255,1);
	box-shadow: inset 0px 1px 1px rgba(0,0,0,0.5), 0px 1px 0px rgba(255,255,255,1);

	background: -webkit-linear-gradient(top, #ffffff 0%, #ffffff 100%);
	background: -moz-linear-gradient(top, #ffffff 0%, #ffffff 100%);
	background: -o-linear-gradient(top, #ffffff 0%, #ffffff 100%);
	background: -ms-linear-gradient(top, #ffffff 0%, #ffffff 100%);
	background: linear-gradient(top, #ffffff 0%, #ffffff 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#ffffff',GradientType=0 );
}

.squaredRed label:after {
	-ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";
	filter: alpha(opacity=0);
	opacity: 0;
	content: '';
	position: absolute;
	width: 9px;
	height: 9px;
	background: #e80909;

	background: -webkit-linear-gradient(top, #e80909 0%, #e80909 100%);
	background: -moz-linear-gradient(top, #e80909 0%, #e80909 100%);
	background: -o-linear-gradient(top, #e80909 0%, #e80909 100%);
	background: -ms-linear-gradient(top, #e80909 0%, #e80909 100%);
	background: linear-gradient(top, #e80909 0%, #e80909 100%);

	top: 1px;
	left: 1.5px;

	-webkit-box-shadow: inset 0px 1px 1px white, 0px 1px 3px rgba(0,0,0,0.5);
	-moz-box-shadow: inset 0px 1px 1px white, 0px 1px 3px rgba(0,0,0,0.5);
	box-shadow: inset 0px 1px 1px white, 0px 1px 3px rgba(0,0,0,0.5);
}

.squaredRed label:hover::after {
	-ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=30)";
	filter: alpha(opacity=30);
	opacity: 0.3;
}

.squaredRed input[type=checkbox]:checked + label:after {
	-ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=100)";
	filter: alpha(opacity=100);
	opacity: 1;
}



</style>


</head>

<body class="bodyGeneral" oncontextmenu="return false;">



	<%!/* Declarations */
	String queryString = null;
	Connection connection;
	Statement stmt;
	ResultSet rs;
	int userCounter = 0;
	int activityCounter = 0;
	int accountCounter = 0;%>


	<!--//START MAIN TABLE : Table for template Structure-->
	<table class="templateTable1000x580" align="center" cellpadding="0"
		cellspacing="0">

		<!--//START HEADER : Record for Header Background and Mirage Logo-->
		<tr class="headerBg">
			<td valign="top"><s:include
					value="/includes/template/Header.jsp" /></td>
		</tr>
		<!--//END HEADER : Record for Header Background and Mirage Logo-->

		<!--//START DATA RECORD : Record for LeftMenu and Screen Content-->
		<tr>
			<td>
				<table class="innerTable1000x515" cellpadding="0" cellspacing="0">
					<tr>
						<!--//START DATA COLUMN : Coloumn for LeftMenu-->
						<td width="850px;" class="leftMenuBgColor" valign="top"><s:include
								value="/includes/template/LeftMenu.jsp" /></td>
						<!--//START DATA COLUMN : Coloumn for LeftMenu-->

						<!--//START DATA COLUMN : Coloumn for Screen Content-->
						<td width="850px" class="cellBorder" valign="top"
							style="padding-left: 10px; padding-top: 5px;">
							<!--//START TABBED PANNEL : --> <%--      <sx:tabbedpanel id="resetPasswordPannel" cssStyle="width: 845px; height: 550px;padding-left:10px;padding-top:5px;" doLayout="true" useSelectedTabCookie="true" > 
                        
                        <!--//START TAB : -->
                        <sx:div id="dashBoardTab" label="DashBoard Details" cssStyle="overflow:auto;"> --%>
							<ul id="reportsTab" class="shadetabs">
								<li><a href="#" rel="EmpReportsTab" class="selected">PMO&nbsp;Weekly&nbsp;Review</a></li>

							</ul>
							<div
								style="border: 1px solid gray; width: 840px; height: 490px; overflow: auto; margin-bottom: 1em;">
								<div id="EmpReportsTab" class="tabcontent">


									<div id="commentsOverlay" class="overlay"></div>
									<div id="commentsSpecialBox" class="specialBox"
										style="border-radius: 30px; background: transparent;">
										<s:form theme="simple" align="center">


											<input type="hidden" id="commentsId" value="" />
											
											<input type="hidden" id="row" value="" />
											
											<table align="center" border="0" cellspacing="0"
												style="background: white; width: 87%; border-radius: 30px 30px 30px 30px; xborder-collapse: collapse;">
												<td colspan=""
													style="background-color: #288AD1; width: 100%; border-radius: 30px 0px 0px 0px; xborder-collapse: collapse;">
													<h3 style="color: darkblue;" align="left">
														<span id="headerLabel"
															style="margin-left: 10px; color: white">Comments</span>


													</h3>
												</td>
												<td colspan=""
													style="background-color: #288AD1; width: 70%; border-radius: 0px 30px 0px 0px; xborder-collapse: collapse;"
													align="right"><a href="javascript:closeComments();"
													style="margin-right: 10px;"> <img
														src="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/closeButton.png" />

												</a></td>
												</tr>
												<tr style="border-radius: 0px 0px 30px 30px;">
													<td colspan="2">
														<div id="load1" style="color: green; display: none;">Loading..</div>
														<div id="resultMessage1"></div>
													</td>
												</tr>
												<tr
													style="border-radius: 0px 0px 30px 30px; xborder-collapse: collapse;">
													<td colspan="2"
														style="border-radius: 0px 0px 30px 30px; border: 1px solid #999; border-top: 0; xborder-collapse: collapse;">
														<table style="width: 80%;" align="center" border="0">
<input type="hidden" id="reviewId" value="%{reviewId}" />
<input type="hidden" id="added" value="" />
															<tr id="empDescriptionTr">
																<td colspan="2"><s:textarea style="width:476px;"
																		rows="5" cols="70"
																		onfocus="countCharForComments(this,'descriptionCount');"
																		onkeyup="countCharForComments(this,'descriptionCount');"
																		placeholder="Enter your comments here...."
																		name="description" cssClass="inputTextareaOverlay1"
																		id="comments" /></td>
																<td><lable style="color:green"
																		id="descriptionCount"></lable></td>
															</tr>




															<tr id="addTr" style="display: none">


																<td colspan="2" align="right"><input type="button"
																	value="Add" onclick="addCommentsForPMOReview();" class="buttonBg"
																	id="addButton" /></td>

															</tr>
															<tr id="updateTr" style="display: none">

																<%--   <td class="fieldLabel">Job&nbsp;Logo:</td>
                                             <td colspan="2" ><s:file name="file" label="file" cssClass="inputTextarea" id="file" onchange="attachmentFileNameValidate();"/></td>  --%>

																<td colspan="2" align="right"><input type="button"
																	value="Update" onclick="updateCommentsForPMOReview();"
																	class="buttonBg" id="addButton" /></td>

															</tr>
															<tr>
																<td colspan="2"></td>
																<td><lable style="display:none">Testing</lable></td>
															</tr>
														</table>
													</td>
												</tr>
											</table>
										</s:form>
									</div>
									<!-- Start Overlay -->




									<!-- End Overlay -->
									<table cellpadding="0" cellspacing="0" border="0" width="100%">

										<!-- employee timesheet Consolidate report dashboard Start  -->
										<tr>
											<td valign="top">
												<!--                                                                    <div class="portletTitleBar">
                                                                        <div class="portletTitleLeft">Time&nbsp;Sheet&nbsp;Audit&nbsp;Report</div>
                                                                        <div class="portletIcons">
                                                                            <a href="javascript:animatedcollapse.hide('consolidateReport')" title="Minimize">
                                                                                <img src="../includes/images/portal/title_minimize.gif" alt="Minimize"/></a>
                                                                            <a href="javascript:animatedcollapse.show('consolidateReport')" title="Maximize">
                                                                                <img src="../includes/images/portal/title_maximize.gif" alt="Maximize"/>
                                                                            </a>
                                                                        </div>
                                                                        <div class="clear"></div>
                                                                    </div>-->
												<div id="ProjectList" style="background-color: #ffffff">
													<table cellpadding="0" cellspacing="0" border="0"
														width="100%">
														<tr>
															<td width="40%" valign="top" align="center"><s:form
																	name="frmSearch" id="frmSearch" action=""
																	theme="simple">

																	<table cellpadding="0" cellspacing="0" border="0"
																		width="100%">
																		<tr align="right">
																			<td class="headerText" colspan="9"><img
																				src="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/spacer.gif"
																				width="100%" height="13px" border="0"></td>
																		</tr>

																		<tr>
																			<td>
																				<%
																					if (session.getAttribute("resultMessageForUtility") != null) {
																							out.println(session.getAttribute("resultMessageForUtility"));
																							session.removeAttribute("resultMessageForUtility");
																						}
																				%>
																			</td>
																		</tr>
																		<tr>
																			<td><s:hidden
																					value="%{#session.sessionEmpPractice}"
																					name="practiceTitle" id="practiceTitle" /> <s:hidden
																					value="%{#session.roleName}" name="roleName"
																					id="roleName" />
																					<s:hidden
																					value="%{#session.userId}"
																					name="userId" id="userId" /> 
																				<table cellpadding="0" cellspacing="2" border="0"
																					width="100%">
																					<tr>
																					<td>
																						
																						</td>
																					</tr>
																					<tr>
																						<td class="fieldLabel">Year(YYYY):</td>
																						<td><s:textfield name="year" id="year"
																								maxlength="4" cssClass="inputTextBlue"
																								value="%{year}"
																								onblur="yearValidation(this.value,event)"
																								onkeypress="yearValidation(this.value,event)" />
																						</td>
																						<td class="fieldLabel">Month:</td>
																						<td><s:select
																								list="#@java.util.LinkedHashMap@{'1':'Jan','2':'Feb','3':'Mar','4':'Apr','5':'May','6':'June','7':'July','8':'Aug','9':'Sept','10':'Oct','11':'Nov','12':'Dec'}"
																								name="month" id="month" headerValue="--Please Select--"
																								headerKey="" value="%{month}" cssClass="inputSelect"
																								onchange="getNoOfWeeks();" /></td>

																						<td class="fieldLabel">Week:</td>
																						<td><s:select list="{}" name="week" id="week" cssClass="inputSelect"
																								onchange="" headerValue="All" headerKey="All"
																								value="%{week}" /></td>
																						<%-- <s:select list="#@java.util.LinkedHashMap@{'Week1':'Week1','Week2':'Week2','Week3':'Week3','Week4':'Week4','Week5':'Week5','Week6':'Week6'}" name="week" id="week" onchange="" headerValue="select" headerKey="" value="%{week}"/></td>
									                                                            --%>

																					</tr>
																					<tr>

																						<td class="fieldLabel">Cost&nbsp;Model
																							:&nbsp;</td>
																						<td><s:select name="costModel" id="costModel"
																								onchange="getCustomerDetailsByCostModel(this);"
																								cssClass="inputSelect"
																								list="{'Fixed','Time & Material','Internal'}"
																								value="%{costModel}" emptyOption="false"
																								headerKey="-1" headerValue="--Please Select--" />
																						</td>
																						<td class="fieldLabel">Customer&nbsp;Name :</td>
																						<td><s:hidden id="ispmo"
																								value="%{#session.isPMO}" /> <s:hidden
																								id="weeksCount" value="%{weeksCount}"
																								name="weeksCount" /> <s:if
																								test="#session.isPMO=='1'">
																								<s:select headerKey="" headerValue="All"
																									list="clientMap" name="customerName"
																									id="customerName" cssClass="inputSelect"
																									theme="simple"
																									onchange="getProjectsByAccountIdProjectOverviewPMO();" />
																							</s:if>
																							<s:else>
																							
																								<s:select headerKey="" headerValue="All"
																									list="clientMap" name="customerName"
																									id="customerName" cssClass="inputSelect"
																									theme="simple"
																									onchange="getProjectsByAccountIdProjectOverview();" />

																							</s:else></td>
																						<td class="fieldLabel">Project&nbsp;Name :</td>
																						<td><s:select headerKey="" headerValue="All"
																								list="{}" name="projectName" id="projectName"
																								cssClass="inputSelect" theme="simple" /></td>
																					</tr>
																					<tr>
																						<td class="fieldLabel">Weekly&nbsp;Report:</td>
																						<td><s:select
																								list="#@java.util.LinkedHashMap@{'Added':'Submitted','NotEntered':'NotEntered'}"
																								name="weeklyReport" id="weeklyReport" cssClass="inputSelect"
																								
																								value="%{weeklyReport}" /></td>


																						<td></td>
																						<td></td>
																						<td></td>
																						 <td>
																						 <s:if test="#session.userId!='rkalaga'">
																								<input type="button"
																							value="Load For WeeklyReport" class="buttonBg"
																							onclick="getLoadForPMOWeelyReview();" /> 
																							</s:if>
																							<input
																							type="button" value="Search" class="buttonBg"
																							onclick="getPMOWeeklyReportSearch();" /></td> 
																							
																					</tr>

																				</table></td>
																		</tr>

																		<%-- table grid --%>
																		<tr>
																			<td><br>
																				<table align="center" cellpadding="2" border="0"
																					cellspacing="1" width="50%">

																					<tr>
																						<td height="20px" align="center" colspan="9">
																							<div id="loadInvoiceHrs" style="display: none"
																								class="error">
																								<b>Loading Please Wait.....</b>
																							</div>
																						</td>
																					</tr>


																					<tr>
																						<td><br>
																							<div id="pmoProjectDetailsReport"
																								style="display: block">
																								<!--style="color:#0000FF;font:italic 900 12px arial;"  bgcolor='#3E93D4'-->
																								<table id="tblEmpPMOWeeklyReportList"
																									align='center' i cellpadding='1'
																									cellspacing='1' border='0' class="gridTable"
																									width='750'>
																									
																									
																								</table>
																								<div
					style="display: none; position: absolute; top: 170px; left: 320px; overflow: auto;"
					id="menu-popup">
					<table id="completeTable" border="1" bordercolor="#e5e4f2"
						style="border: 1px dashed gray;" cellpadding="0"
						class="cellBorder" cellspacing="0" />
				</div>
																								<%--    <s:if test="%{#session.sessionEmpPractice == 'Accounts Payable' || #session.sessionEmpPractice == 'Accounts Receivable' || #session.roleName=='Operations' || #session.isAdminAccess==1}" >   
																								<div align="right">
																									<br> <input id="saveinvoicebtn"
																										 type="button"
																										value="Save" class="buttonBg"
																										onclick="saveInvoiceHrsDetails();" /> --%>
																									<%--  </s:if>   <s:else>
                                                                                                          <div id="saveinvoicebtn" align="right"></div>
                                                                                                          </s:else> --%>
																								</div>
																								<br>
																								<!--<center><span id="spnFast" class="activeFile" style="display: none;"></span></center>-->
																							</div></td>
																					</tr>
																				</table></td>
																		</tr>
																		<%-- table grid  end--%>
																	</table>
																</s:form></td>
														</tr>
													</table>

												</div> <%-- <script type="text/JavaScript">
                                                                                                                                        var cal8 = new CalendarTime(document.forms['empConsolidateSearch'].elements['empConsolidateStartDate']);
                                                                                                                                        cal8.year_scroll = true;
                                                                                                                                        cal8.time_comp = false;
                                                           
                                                                                                                                        var cal9 = new CalendarTime(document.forms['empConsolidateSearch'].elements['empConsolidateEndDate']);
                                                                                                                                        cal9.year_scroll = true;
                                                                                                                                        cal9.time_comp = false;
                                                                                                                                    </script> --%>
											</td>
										</tr>

										<%-- employee timesheet Consolidate report dashboard ENd --%>

									</table>

									<%--     </sx:div>
                                                                                                                                    </sx:tabbedpanel> --%>
									<!--//END TABBED PANNEL : -->
								</div>
							</div> <script type="text/javascript">
								var countries = new ddtabcontent("reportsTab")
								countries.setpersist(false)
								countries.setselectedClassTarget("link") //"link" or "linkparent"
								countries.init()
							</script>

						</td>
						<!--//END DATA COLUMN : Coloumn for Screen Content-->
					</tr>
				</table>
			</td>
		</tr>
		<!--//END DATA RECORD : Record for LeftMenu and Body Content-->
<!--//START FOOTER : Record for Footer Background and Content-->
		<tr  class="footerBg">

			<td colspan="4" align="center"><s:include value="/includes/template/Footer.jsp" /></td>
		</tr>
		<!--//END FOOTER : Record for Footer Background and Content-->
		
		<tr>
			<td>

				
				<script type="text/JavaScript" src="<s:url value="/includes/javascripts/payroll/x0popup.min.js"/>"></script> 
				<script>
				$(document).ready(function() {
		
			getNoOfWeeks();
			
			
			if(document.getElementById("userId").value == 'rkalaga'){
				getPMOWeeklyReportSearch()
			}else{
				getLoadForPMOWeelyReview()
			}
				});
				</script>
   <script type="text/javascript">
        function pagerOption(){

            // var paginationSize = document.getElementById("paginationOption").value;
                                                                                                                                                                                                      
            var  recordPage=15;
                                                                                                                                                                                                  
            $('#tblEmpPMOWeeklyReportList').tablePaginate({navigateType:'navigator'},recordPage);

        }
                                                                                                                                                                                                        
                                                                                                                                                                                                           
    </script>
    <script type="text/JavaScript" src="<s:url value="/includes/javascripts/paginationAll.js"/>"></script>


        
			</td>
		</tr>

	</table>
	<!--//END MAIN TABLE : Table for template Structure-->
</body>

</html>

