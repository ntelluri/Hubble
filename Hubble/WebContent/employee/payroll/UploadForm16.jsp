<%@ page contentType="text/html; charset=UTF-8" errorPage="../exception/ErrorDisplay.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%--<%@ taglib prefix="sx" uri="/struts-dojo-tags" %> --%>
<%@ page import="com.freeware.gridtag.*" %> 
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="com.mss.mirage.util.ConnectionProvider"%>
<%@ page import="com.mss.mirage.util.ApplicationConstants"%>
<%@ taglib uri="/WEB-INF/tlds/datagrid.tld" prefix="grd" %>
<html>
    <head>
        <title>Hubble Organization Portal :: Upload Form16</title>
    <sx:head cache="true"/>
    <link rel="stylesheet" type="text/css" href="<s:url value="/includes/css/mainStyle.css"/>">
    <link rel="stylesheet" type="text/css" href="<s:url value="/includes/css/new/x0popup.min.css"/>">
    <link rel="stylesheet" type="text/css" href="<s:url value="/includes/css/GridStyle.css"/>">
    <link rel="stylesheet" type="text/css" href="<s:url value="/includes/css/leftMenu.css"/>">
    <link rel="stylesheet" type="text/css" href="<s:url value="/includes/css/tabedPanel.css"/>">
    
    <script type="text/JavaScript" src="<s:url value="/includes/javascripts/DBGrid.js"/>"></script>
    <script type="text/JavaScript" src="<s:url value="/includes/javascripts/ClientValidations.js"/>"></script>
    <script type="text/JavaScript" src="<s:url value="/includes/javascripts/clientValidations/EmpSearchClientValidation.js"/>"></script>
    <script type="text/JavaScript" src="<s:url value="/includes/javascripts/AppConstants.js"/>"></script>
    <script type="text/JavaScript" src="<s:url value="/includes/javascripts/tabedPanel.js"/>"></script>
     <script type="text/JavaScript" src="<s:url value="/includes/javascripts/payroll/payrollajaxscript.js?version=2.8"/>"></script>
    
    
    <s:include value="/includes/template/headerScript.html"/>
</head>
<%-- <body class="bodyGeneral" oncontextmenu="return false;" onload="initForTefSearch();"> --%>
<body class="bodyGeneral" oncontextmenu="return false;">

    <%!
        /* Declarations */
        Connection connection;
        String queryString;
        String strTmp;
        String strSortCol;
        String strSortOrd;
        String submittedFrom;
        String searchSubmitValue;
        int intSortOrd = 0;
        int intCurr;
        boolean blnSortAsc = true;
    %>

    <!--//START MAIN TABLE : Table for template Structure-->
    <table class="templateTableLogin" align="center" cellpadding="0" cellspacing="0">

        <!--//START HEADER : Record for Header Background and Mirage Logo-->
        <tr class="headerBg">
            <td valign="top">
                <s:include value="/includes/template/Header.jsp"/>                    
            </td>
        </tr>
        <!--//END HEADER : Record for Header Background and Mirage Logo-->


        <!--//START DATA RECORD : Record for LeftMenu and Screen Content-->
        <tr>
            <td>
                <table class="innerTableLogin" cellpadding="0" cellspacing="0">

                    <tr>

                        <!--//START DATA COLUMN : Coloumn for LeftMenu-->
                        <td width="150px;" class="leftMenuBgColor" valign="top">
                            <s:include value="/includes/template/LeftMenu.jsp"/>
                        </td>

                        <!--//END DATA COLUMN : Coloumn for LeftMenu-->

                        <!--//START DATA COLUMN : Coloumn for Screen Content-->
                        <td width="850px" class="cellBorder" valign="top" style="padding: 5px 5px 5px 5px;">
                            <!--//START TABBED PANNEL : -->
                            <ul id="accountTabs" class="shadetabs" >
                                <li ><a href="#" class="selected" rel="employeeUploadPaySlips"  >Form 16B </a></li>
                            </ul>
                            <%--<sx:tabbedpanel id="employeeSearchPannel" cssStyle="width: 840px; height: 500px;padding: 5px 5px 5px 5px;" doLayout="true"> --%>
                            <div  style="border:1px solid gray; width:840px;height: 500px; overflow:auto; margin-bottom: 1em;">   
                                <!--//START TAB : -->
                                <%--  <sx:div id="employeeSearchTab" label="Employee Search" cssStyle="overflow:auto;"  > --%>
                                <div id="employeeUploadPaySlips" class="tabcontent"  >
                                  
                                  
                <div id="displyID"></div>
                        <div id="blockDiv" style="display: none"align="center;position:relative;top:30%">
                            <font style="font-weight:bold;position: absolute;color: #fff;font-size: 23px;top:-53px;">  Releasing Form16 Forms <font id="fontId">...</font> </font>
                            <img src='../../includes/images/ajax-loader.gif' WIDTH='35%' HEIGHT='5%'  alt='block'>
                        </div>                  
                                  
                                    
                                    <div id="overlay"></div> 
                                    <div id="specialBox">


                                        <s:form theme="simple"  align="center" name="empPaySlepsOverlay" action="" method="post">
                                        
                                            <table align="center" border="0" cellspacing="0" style="width:100%;">
                                                <tr>                               
                                                    <td colspan="2" style="background-color: #288AD1" >
                                                        <h3 style="color:darkblue;" align="left">
                                                            <span id="headerLabel1"></span>


                                                        </h3></td>
                                                    <td colspan="2" style="background-color: #288AD1" align="right">

                                                        <a href="#" onmousedown="empForm16UploadToggleOverlay()" >
                                                            <img src="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/closeButton.png" /> 

                                                        </a>  

                                                    </td></tr>
                                                <tr>
                                                    <td colspan="4">
                                                        <div id="load" style="color: green;display: none;">Loading Please Wait....</div>
                                                        <div id="resultMessage"></div>
                                                    </td>
                                                </tr>
                                                <tr><td colspan="4">
                                                    <table style="width:80%;" align="center" border="0">
                                               <tr>
                                               
                                                              <td  class="fieldLabel">Type:</td>
                                                            <td><s:select list="{'Form16','PartB'}" id="form16Type" name="form16Type"  cssClass="inputSelect" value="%{form16Type}" /></td>
 
                                               
                                                          <td class="fieldLabel">Year(YYYY):</td>
                                                          <td>
                                                            
                                                              <s:textfield name="overlayYear" id="overlayYear" maxlength="4" onkeyup="isNumericYear(this);" cssClass="inputTextBlue" value="%{year}" />
                                                          </td>
                                                       
                                                      </tr> 
                                                        
                                                         <tr>
                                                            <td  class="fieldLabel">Organization:</td>
                                                            <td><s:select list="orgIdMap" id="overlayOrgId" name="orgId" headerKey="" headerValue="All" cssClass="inputSelect" value="%{overlayOrgId}" /></td>
   
  <td class="fieldLabel"><span id="Attachment">Attachment:</span></td>
                                                                    <td colspan="2" ><s:file name="file" label="file" cssClass="inputTextarea" id="file" multiple="multiple" onchange="checkFileLength();"/></td> 
                                                          </tr>
                                                     <tr> 
                                                                 <td align="right" colspan="4"><input style="margin-right:36px;" type="button" id="uploadForm16" name="uploadForm16" value="Upload Form16" class="buttonBg" onclick="uploadFormAttachments();"/></td>         
                                                        </tr>                  
                                                        </table>
                                                    </td>
                                                </tr>


                                            </table>

                                        </s:form>              

                                    </div>
                                    
                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">

                                        <tr align="right">
                                            <td class="headerText">
                                                <img alt="Home" 
                                                     src="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/spacer.gif" 
                                                     width="100%" 
                                                     height="13px" 
                                                     border="0">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <% if (request.getAttribute(ApplicationConstants.RESULT_MSG) != null) {
                                                        out.println(request.getAttribute(ApplicationConstants.RESULT_MSG));
                                                    }

                                                    if (request.getAttribute("submitFrom") != null) {
                                                        submittedFrom = request.getAttribute("submitFrom").toString();
                                                    }

                                                    if (!"dbGrid".equalsIgnoreCase(submittedFrom)) {
                                                        searchSubmitValue = submittedFrom;
                                                    }

                                                %>

                                            </td>
                                        </tr>
                                        
                                        
                                        
                                        <tr>
                                            <td>
                                                <s:form name="frmEmpUploadForm16" id="frmEmpUploadForm16" enctype="multipart/form-data" method="post" action="uploadForm16" theme="simple" >
                                                
                                                    <table cellpadding="1" cellspacing="1" border="0" width="750px">
                                                       
<!--  <tr> -->
<!--                                                         <td colspan="4"> -->
<!--                                                             <div id="load" style="color: green;display: none;">Loading..</div> -->
<!--                                                             <div id="resultMessageNoDue"></div> -->
<!--                                                         </td> -->
<!--                                                     </tr> -->
                                                          
                                                       
                                                         <tr>
                                                          <td class="fieldLabel">Year(YYYY):</td>
                                                          <td>
                                                            
                                                              <s:textfield name="year" id="year" maxlength="4" cssClass="inputTextBlue" value="%{year}" onblur="yearValidation(this.value,event)" onkeypress="yearValidation(this.value,event)"/>
                                                          </td>
                                                      
                                                     
                                                            <td  class="fieldLabel">Organization:</td>
                                                            <td><s:select list="orgIdMap" id="orgId" name="orgId" headerKey="" headerValue="All" cssClass="inputSelect" value="%{orgId}" /></td>
 
                                                          
                                                      </tr> 
                                                     

<!--                                                      <tr id="attachmentTr">  -->
<%--                                                                     <td class="fieldLabel"><span id="Attachment">Attachment:</span></td> --%>
<%--                                                                     <td colspan="2" ><s:file name="file" label="file" cssClass="inputTextarea" id="file" /></td>  --%>
                                                                   
<!-- <!--                                                                         <input type="button" value="Save" onclick="return ajaxFileUploadTaxAssumptionFromPayroll();" class="buttonBg" id="addButton"/>  --> 
                                                                      
                                                                     <tr>
                                                           
                                                                 <td align="right" colspan="4"><input type="button" name="appPaySlipsOverlay" value="Add" class="buttonBg" onclick="empForm16UploadToggleOverlay();"/>   
                                                                <s:submit style="margin-right:83px;" name="searchForm16" value="Search" cssClass="buttonBg"/></td>     
                                                        </tr>


                                            </tr>
                                        </table>
                                    </s:form>
                                    
                                    </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <%
                                                /* String Variable for storing current position of records in dbgrid*/
                                                strTmp = request.getParameter("txtCurr");

                                                intCurr = 1;

                                                if (strTmp != null) {
                                                    intCurr = Integer.parseInt(strTmp);
                                                }

                                                /* Specifing Shorting Column */
                                                strSortCol = request.getParameter("Colname");

                                                if (strSortCol == null) {
                                                    strSortCol = "Fname";
                                                }

                                                strSortOrd = request.getParameter("txtSortAsc");
                                                if (strSortOrd == null) {
                                                    strSortOrd = "ASC";
                                                }


                                                try {
//.println("in sfdcad ");
                                                    /* Getting DataSource using Service Locator */
                                                    connection = ConnectionProvider.getInstance().getConnection();

                                                    /* Sql query for retrieving resultset from DataBase */
                                                     if (request.getAttribute(ApplicationConstants.QUERY_STRING) != null) {
                                                                queryString = request.getAttribute(ApplicationConstants.QUERY_STRING).toString();
                                                            }


                                                    //out.println("--------"+submittedFrom);
                                            %>

                                            <s:form action="" theme="simple" name="frmDBGrid">   

                                                <table cellpadding="0" cellspacing="0" width="100%" border="0">
<tr>
                                                                                                                                                                                                        <td colspan="5">
                                                                                                                                                                                                        <div id="loadingMessage3" style="color:red; display: none;" align="center"><b><font size="4px" >Loading...Please Wait...</font></b></div>
                                                                                                                                                                                                        <div id="resultMessage2" align="center"></div>
                                                                                                                                                                                                        </td>
                                                                                                                                                                                                        </tr>

                                                    <tr>
                                                        <td width="100%">

                                                            <grd:dbgrid id="tblStat" name="tblStat" width="98" pageSize="10"
                                                            currentPage="<%=intCurr%>" border="0" cellSpacing="1" cellPadding="2"
                                                            dataMember="<%=queryString%>" dataSource="<%=connection%>" cssClass="gridTable">

                                                                <grd:gridpager imgFirst="../../includes/images/DBGrid/First.gif" imgPrevious="../../includes/images/DBGrid/Previous.gif" 
                                                                               imgNext="../../includes/images/DBGrid/Next.gif" imgLast="../../includes/images/DBGrid/Last.gif"
                                                                               addImage="../../includes/images/DBGrid/Add.png"  addAction="javascript:empForm16UploadToggleOverlay()"
                                                                               />

                                                                <grd:gridsorter sortColumn="<%=strSortCol%>" sortAscending="<%=blnSortAsc%>" 
                                                                                imageAscending="../../includes/images/DBGrid/ImgAsc.gif" 
                                                                                imageDescending="../../includes/images/DBGrid/ImgDesc.gif"/>   

                                                                <grd:rownumcolumn headerText="SNo" width="4" HAlign="right"/>

                                                               <%-- <grd:imagecolumn headerText="Edit" width="4" HAlign="center" imageSrc="../../includes/images/DBGrid/newEdit_17x18.png"
                                                                                 linkUrl="editEmployeePayroll.action?payRollId={PayRollId}&empId={Id}&tdsId={TdsId}" imageBorder="0"
                                                                                 imageWidth="16" imageHeight="16" alterText="Click to edit"></grd:imagecolumn>  --%>
                                                               
                                                              
                                                                <grd:datecolumn dataField="YEAR" headerText="Year" width="10"/>
                                                                <grd:textcolumn dataField="Description" headerText="Organization" width="10"/>
                                                                <grd:textcolumn dataField="FormType" headerText="FormType" width="10"/>
                                                          <grd:textcolumn dataField="UploadedBy" headerText="UploadedBy" width="20"/>
                                               <grd:textcolumn dataField="CreatedDate" headerText="UploadedDate" width="10" dataFormat="MM-dd-yyyy" />
                                                        <grd:imagecolumn  headerText="Release Form16B Forms" width="5"  HAlign="center"  
                                                                          imageSrc="../../includes/images/go_21x21.gif" linkUrl="javascript:doReleaseForm16({Id},'{Year}','{FormType}','{Organization}')"
                                                                          imageBorder="0" imageWidth="18" imageHeight="15" alterText="Go" />

                                                            </grd:dbgrid>

                                                            <input TYPE="hidden" NAME="txtCurr" VALUE="<%=intCurr%>">
                                                            <input TYPE="hidden" NAME="txtSortCol" VALUE="<%=strSortCol%>">
                                                            <input TYPE="hidden" NAME="txtSortAsc" VALUE="<%=strSortOrd%>">

                                                            <input type="hidden" name="submitFrom" value="dbGrid">
                                                
                                                        </td>
                                                    </tr>
                                                </table>                                

                                            </s:form>

                                            <%
                                                    connection.close();
                                                    connection = null;
                                                } catch (Exception ex) {
                                                    out.println(ex.toString());
                                                } finally {
                                                    if (connection != null) {
                                                        connection.close();
                                                        connection = null;
                                                    }
                                                }
                                            %>
                                        </td>
                                    </tr>
                                    </table>
                                    <%-- </sx:div > --%>
                                </div>
                                <!--//END TAB : -->
                                <%-- </sx:tabbedpanel> --%>
                            </div>
                            <script type="text/javascript">

                                var countries=new ddtabcontent("accountTabs")
                                countries.setpersist(false)
                                countries.setselectedClassTarget("link") //"link" or "linkparent"
                                countries.init()

                            </script>
                          <script type="text/JavaScript" src="<s:url value="/includes/javascripts/reviews/jquery.min.js"/>"></script>
    <script type="text/javascript" src="<s:url value="/includes/javascripts/reviews/jquery.js"/>"></script>     
   
   <script type="text/javascript" src="<s:url value="/includes/javascripts/reviews/ajaxfileupload.js"/>"></script>  
    <script type="text/JavaScript" src="<s:url value="/includes/javascripts/reviews/FileUpload.js"/>"></script>
    <script type="text/JavaScript" src="<s:url value="/includes/javascripts/payroll/payrollajaxscript.js?version=5.4"/>"></script>
 
                            <!--//END TABBED PANNEL : -->
                        </td>
                        <!--//END DATA COLUMN : Coloumn for Screen Content-->
                    </tr>
                </table>
            </td>
        </tr>
        <!--//END DATA RECORD : Record for LeftMenu and Body Content-->

        <!--//START FOOTER : Record for Footer Background and Content-->
        <tr class="footerBg">
            <td align="center"><s:include value="/includes/template/Footer.jsp"/></td>
        </tr>
        <!--//END FOOTER : Record for Footer Background and Content-->
   
 
    </table>
    <!--//END MAIN TABLE : Table for template Structure-->



<%-- <script type="text/javascript"> --%>
<!-- // 		$(window).load(function(){ -->
<!-- //       initForTefSearch(); -->
<!-- // 		}); -->
<%-- 		</script> --%>
</body>
</html>

