<%@ taglib prefix="s" uri="/struts-tags" %> 
<%-- <%@ taglib prefix="sx" uri="/struts-dojo-tags" %> --%>
<%@ page import="com.mss.mirage.util.ApplicationConstants"%>


<html>
<head>
<title>Pre-Sales Documents</title>

    <link rel="stylesheet" type="text/css" href="<s:url value="/includes/css/lightbox.css?version=2.3"/>">
        <link rel="stylesheet" type="text/css" href="<s:url value="/includes/css/google-drive.css?version=2.3"/>">
        <link rel="stylesheet" type="text/css" href="<s:url value="/includes/css/mainStyle.css"/>">
      
        <link rel="stylesheet" type="text/css" href="<s:url value="/includes/css/leftMenu.css"/>">
        <link rel="stylesheet" type="text/css" href="<s:url value="/includes/css/tabedPanel.css"/>">
           
    
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/AppConstants.js"/>"></script>
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/tabedPanel.js"/>"></script>
<s:include value="/includes/template/headerScript.html"/>
</head>
<body class="bodyGeneral" oncontextmenu="return false;">

 <table class="templateTable1000x580" align="center" cellpadding="0" cellspacing="0">
            
            <!--//START HEADER : Record for Header Background and Mirage Logo-->
            <tr class="headerBg">
                <td valign="top">
                    <s:include value="/includes/template/Header.jsp"/>                    
                </td>
            </tr>
            
            <tr>
                <td>
                    <table class="innerTable1000x515" cellpadding="0" cellspacing="0">
                        <tr>
                            
                            <!--//START DATA COLUMN : Coloumn for LeftMenu-->
                            <td width="150px;" class="leftMenuBgColor" valign="top">
                                <s:include value="/includes/template/LeftMenu.jsp"/>
                                
                            </td>
                                                        <td width="850px" class="cellBorder" valign="top" style="padding: 10px">
            
<ul id="accountTabs" class="shadetabs" >
  
                                      <li ><a   rel="accountsSearchTab">Pre-Sales Documents</a></li>
                                     
                                </ul>
                                <div  style="border:1px solid gray; width:840px; height: 550px; overflow:auto; margin-bottom: 1em;">
 <div id="accountsSearchTab" class="tabcontent">
 
 
<div id="transparent-wrapper"></div>
<div id="login-box" class="hide" style="margin:175px;">
	<p>Please login on your google account.</p>
	<button id="btnLogin" onclick="handleAuthClick()" class="button">Login</button>
</div>

<div id="drive-box" class="hide">
	<div id="drive-breadcrumb">
        <span class='breadcrumb-arrow'></span> <a data-id='0B15xm1Rh7FWYdGY4SXJueUczZEk'
                                                  title="Browse Miracle_Presales 2.0" data-name="Miracle_Presales 2.0" data-has-permission="true"
                                                  data-level='0'>Miracle_Presales 2.0</a>
        <span id="span-navigation"></span>
    </div>
	
    <div id="drive-info" class="hide">
        <div class="user-item">Welcome <span id="span-name"></span></div>
  
        <div class="user-item"><a id="link-logout" class="logout-link" onclick="handleSignoutClick()">Logout</a></div>
    </div>
	
	<div id="drive-menu">
        <div id="button-reload" title="Refresh"></div>
		<div id="button-upload" title="Upload to Google Drive" class="button-opt"></div>
		<div id="button-addfolder" title="Add Folder" class="button-opt"></div>
       <%--   <div id="button-share" title="Show shared files only"></div> --%>
    </div>
	<div id="drive-content"></div>
	<div id="error-message" class="flash hidden"></div>
	<div id="status-message" class="flash hidden"></div>
</div>

<input type="file" id="fUpload" class="hide"/>
<div class="float-box" id="float-box">
    <div class="folder-form">
        <div class="close-x"><img id="imgClose" class="imgClose" src="../includes/images/googleDriveImgs/button_close.png" alt="close" /></div>
        <h3 class="clear">Add New Folder</h3>
        <div><input type="text" id="txtFolder" class="text-input" /></div>
		<button id="btnAddFolder" value="Save" class="button">Add</button>
		<button id="btnClose" value="Close" class="button btnClose">Close</button>
    </div>
</div>
<div id="float-box-info" class="float-box">
    <div class="info-form">
        <div class="close-x"><img id="imgCloseInfo" class="imgClose" src="../includes/images/googleDriveImgs/button_close.png" alt="close" /></div>
        <h3 class="clear">File information</h3>
        <table cellpadding="0" cellspacing="0" class="tbl-info">
            <tr>
                <td class="label">Created Date</td>
                <td><span id="spanCreatedDate"></span></td>
            </tr>
            <tr>
                <td class="label">Modified Date</td>
                <td><span id="spanModifiedDate"></span></td>
            </tr>
            <tr>
                <td class="label">Owner</td>
                <td><span id="spanOwner"></span></td>
            </tr>
            <tr>
                <td class="label">Title</td>
                <td><span id="spanTitle"></span></td>
            </tr>
            <tr>
                <td class="label">Size</td>
                <td><span id="spanSize"></span></td>
            </tr>
            <tr>
                <td class="label">Extension</td>
                <td><span id="spanExtension"></span></td>
            </tr>
        </table>
		<button id="btnCloseInfo" value="Close" class="button btnClose">Close</button>
    </div>
</div>
<div id="float-box-text" class="float-box">
    <div class="info-form">
        <div class="close-x"><img id="imgCloseText" class="imgClose" src="../includes/images/googleDriveImgs/button_close.png" alt="close" /></div>
        <h3 class="clear">Text Content</h3>
        <div id="text-content"></div>
		<button id="btnCloseText" value="Close" class="button btnClose">Close</button>
    </div>
</div>



</div>
</div>
 <script type="text/javascript">

                                var countries=new ddtabcontent("accountTabs")
                                countries.setpersist(false)
                                countries.setselectedClassTarget("link") //"link" or "linkparent"
                                countries.init()

                                </script>
 </td>
                            <!--//END DATA COLUMN : Coloumn for Screen Content-->
                        </tr>
                    </table>
</td></tr>

<tr class="footerBg">
                <td align="center"><s:include value="/includes/template/Footer.jsp"/></td>
            </tr>
            <!--//END FOOTER : Record for Footer Background and Content-->
             
        </table>
        
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>

<script type="text/JavaScript" src="<s:url value="/includes/javascripts/date.js"/>"></script>
<script type="text/JavaScript" src="<s:url value="/includes/javascripts/lightbox.min.js"/>"></script>
<script type="text/JavaScript" src="<s:url value="/includes/javascripts/google-drive.js?version=2.3"/>"></script>
<script async defer src="https://apis.google.com/js/api.js" 
      onload="this.onload=function(){};handleClientLoad()" 
      onreadystatechange="if (this.readyState === 'complete') this.onload()">
</script>

 <script type="text/JavaScript" src="<s:url value="/includes/javascripts/upload.js"/>"></script>


</body>
</html>
