<%-- 
    Document   : TeamReview
    Created on : Feb 13, 2015, 3:13:29 PM
    Author     : miracle
--%>

<%@ page contentType="text/html; charset=UTF-8" errorPage="../exception/ErrorDisplay.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%--<%@ taglib prefix="sx" uri="/struts-dojo-tags" %> --%>
<%@ page import="com.freeware.gridtag.*" %> 
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="com.mss.mirage.util.ConnectionProvider"%>
<%@ page import="com.mss.mirage.util.ApplicationConstants"%>
<%@ taglib uri="/WEB-INF/tlds/datagrid.tld" prefix="grd" %>
<html>
    <head>
        <title>Hubble Organization Portal :: Team Review Search</title>

        <link rel="stylesheet" type="text/css" href="<s:url value="/includes/css/mainStyle.css?ver=1.0"/>">
        <link rel="stylesheet" type="text/css" href="<s:url value="/includes/css/GridStyle.css"/>">
        <link rel="stylesheet" type="text/css" href="<s:url value="/includes/css/leftMenu.css"/>">
        <link rel="stylesheet" type="text/css" href="<s:url value="/includes/css/tabedPanel.css"/>">
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/DBGrid.js"/>"></script>
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/ClientValidations.js"/>"></script>
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/clientValidations/EmpSearchClientValidation.js"/>"></script>
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/AppConstants.js"/>"></script>
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/tabedPanel.js"/>"></script>
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/CalendarTime.js"/>"></script>
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/reviews/jquery.min.js"/>"></script>
        <script type="text/javascript" src="<s:url value="/includes/javascripts/reviews/jquery.js"/>"></script>   
        <script type="text/javascript" src="<s:url value="/includes/javascripts/reviews/ajaxfileupload.js"/>"></script>  
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/reviews/FileUpload.js?ver=1.1"/>"></script>
 <script type="text/JavaScript" src="<s:url value="/includes/javascripts/EmployeeAjax.js?ver=1.1"/>"></script>
    <link rel="stylesheet" type="text/css" href="<s:url value="/includes/css/new/x0popup.min.css?version=1.0"/>">
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/payroll/x0popup.min.js"/>"></script>
	  


        <s:include value="/includes/template/headerScript.html"/>
    </head>
    <body class="bodyGeneral" oncontextmenu="return false;">

        <%!
            /* Declarations */
            Connection connection;
            String queryString;
            String strTmp;
            String strSortCol;
            String strSortOrd;
            String submittedFrom;
            String searchSubmitValue;
            int intSortOrd = 0;
            int intCurr;
            boolean blnSortAsc = true;
        %>

        <!--//START MAIN TABLE : Table for template Structure-->
        <table class="templateTableLogin" align="center" cellpadding="0" cellspacing="0">

            <!--//START HEADER : Record for Header Background and Mirage Logo-->
            <tr class="headerBg">
            <td valign="top">
                <s:include value="/includes/template/Header.jsp"/>                    
            </td>
        </tr>
        <!--//END HEADER : Record for Header Background and Mirage Logo-->


        <!--//START DATA RECORD : Record for LeftMenu and Screen Content-->
        <tr>
        <td>
            <table class="innerTableLogin" cellpadding="0" cellspacing="0">
                <tr>

                    <!--//START DATA COLUMN : Coloumn for LeftMenu-->
                <td width="150px;" class="leftMenuBgColor" valign="top">
                    <s:include value="/includes/template/LeftMenu.jsp"/>
                </td>

                <!--//END DATA COLUMN : Coloumn for LeftMenu-->

                <!--//START DATA COLUMN : Coloumn for Screen Content-->
                <td width="850px" class="cellBorder" valign="top" style="padding: 5px 5px 5px 5px;">
                    <!--//START TABBED PANNEL : -->
                    <ul id="accountTabs" class="shadetabs" >
                        <li ><a href="#" class="selected" rel="employeeSearchTab"  >Emp Holidays </a></li>
                    </ul>
                    <%--<sx:tabbedpanel id="employeeSearchPannel" cssStyle="width: 840px; height: 500px;padding: 5px 5px 5px 5px;" doLayout="true"> --%>
                    <div  style="border:1px solid gray; width:840px;height: 500px; overflow:auto; margin-bottom: 1em;">   
                        <!--//START TAB : -->
                        <%--  <sx:div id="employeeSearchTab" label="Employee Search" cssStyle="overflow:auto;"  > --%>
                        <div id="employeeSearchTab" class="tabcontent"  >


<!-- overlay Start -->

                                      <div id="addHolidayOverlay"></div>
                                    <div id="addHolidayOverlayBox" align="center">


                                        <s:form theme="simple"  align="center" name="editColumnName" method="post" enctype="multipart/form-data" onsubmit="return validateForm();"   >
                                          
                                            <table align="center" border="0" cellspacing="0" style="width:100%;">
                                                <tr>                               
                                                    <td colspan="2" style="background-color: #288AD1" >
                                                        <h3 style="color:darkblue;" align="left">
                                                            <span id="headerLabel"></span>


                                                        </h3></td>
                                                    <td colspan="2" style="background-color: #288AD1" align="right">

                                                        <a href="#" onmousedown="addHolidayOverlay();" >
                                                            <img src="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/closeButton.png" /> 

                                                        </a>  

                                                    </td></tr>
                                                <tr>
                                                    <td colspan="4">
                                                      
                                                        <div id="resultMessage"></div>
                                                    </td>
                                                </tr>
                                                <tr><td colspan="4">
                                                    <table style="width:80%;" align="center" id="overlayFields" border="0">
                                               <tr>
                                               <td class="fieldLabelLeft">Holiday&nbsp;Date&nbsp;<font color="red">*</font>:</td>
                                            <td>  <s:textfield name="holidayDate" id="holidayDate" cssClass="inputTextBlueSmall" value="%{holidayDate}" onKeyPress="return selectFromCal(this);" /><a class="underscore" href="javascript:cal2.popup();">
                                            <img src="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/showCalendar.gif" width="20" height="20" border="0"></a>
                      </td>
                      
                      <td class="fieldLabelLeft">Country&nbsp;<font color="red">*</font>:</td>
                       <td><s:select list="#@java.util.LinkedHashMap@{'India':'India','USA':'USA'}" name="country" id="country" onchange="load(event);" headerValue="select" headerKey="0" value="%{country}"/></td>
                                                                                    
                                               </tr>
                                               <tr>
                                                <td class="fieldLabelLeft">Holiday&nbsp;Name&nbsp;<font color="red">*</font>:</td>
                     
                                               <td> <s:textarea rows="1" cols="30" id="description" name="description" value="%{description}" onchange="fieldLengthValidator(this);"/></td>
                                                 
                                                 <td>    <input type="button" value="Add" class="buttonBg" onclick="return addHolidays();" /> 
                                                        </td>                   
                                               </tr>

                                                  </table>
                                                    </td>
                                                </tr>
<script type="text/JavaScript">
                                             var cal2 = new CalendarTime(document.forms['editColumnName'].elements['holidayDate']);
				                 cal2.year_scroll = true;
				                 cal2.time_comp = false;  
				                 </script>

                                            </table>
                                        </s:form>              

                                       

                                    </div>

<!-- overlay end -->




                            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                <tr align="right">
                                <td class="headerText">
                                    <img alt="Home" 
                                         src="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/spacer.gif" 
                                         width="100%" 
                                         height="13px" 
                                         border="0"/>
                                    <%if (request.getAttribute(ApplicationConstants.RESULT_MSG) != null) {
                                       out.println(request.getAttribute(ApplicationConstants.RESULT_MSG));
                                   }%>
                                </td>
                                </tr>

                              
                                <tr>
                                <td>
                                    <%

                                        if (request.getAttribute("submitFrom") != null) {
                                            submittedFrom = request.getAttribute("submitFrom").toString();
                                        }

                                        if (!"dbGrid".equalsIgnoreCase(submittedFrom)) {
                                            searchSubmitValue = submittedFrom;
                                        }

                                    %>

                                </td>
                                </tr>
                                <tr>
                                <td>
<s:form name="frmEmpSearch" action="getEmpHolidaysSearch.action" theme="simple" onsubmit="return validFields();">
                                                   
                                                    <table cellpadding="1" cellspacing="1" border="0" width="420px" align="center">
                                                   
                                                      
                                                        
                                                         <tr>
                                                                <td class="fieldLabel">Year(YYYY):</td>
                                                                <td>
                                                                    <s:textfield name="year" id="year" maxlength="4" cssClass="inputTextBlue" value="%{year}" onkeyup="return isNumericYear(this);"  />
                                                                </td>
                                                                <td class="fieldLabel">Country:</td>
                                                                <td><s:select list="#@java.util.LinkedHashMap@{'India':'India','USA':'USA'}" name="country" id="country" onchange="load(event);" headerValue="select" headerKey="0" value="%{country}"/></td>
                                                                
                                                                
                                                             
                                                            </tr>
                                                            <tr>
                                                            
                                                        <td colspan="4" align="right">  <s:submit cssClass="buttonBg"  align="right"  value="Search" /> </td>
                                                 
         
                                                              <td>  <input type="button" value="Add" class="buttonBg" onclick="return addHolidayOverlay()" /> </td>
                                                        
                                                        </tr>
                                                         
                                                      
                                                    </table>
                                                </s:form>
                                </td>
                                </tr>
                                <tr>
                                <td>
                                    <%
                                        /* String Variable for storing current position of records in dbgrid*/
                                        strTmp = request.getParameter("txtCurr");

                                        intCurr = 1;

                                        if (strTmp != null) {
                                            intCurr = Integer.parseInt(strTmp);
                                        }

                                        /* Specifing Shorting Column */
                                        strSortCol = request.getParameter("Colname");

                                        if (strSortCol == null) {
                                            strSortCol = "Fname";
                                        }

                                        strSortOrd = request.getParameter("txtSortAsc");
                                        if (strSortOrd == null) {
                                            strSortOrd = "ASC";
                                        }


                                        try {

                                            /* Getting DataSource using Service Locator */
                                            connection = ConnectionProvider.getInstance().getConnection();

                                           // String empReviewAction = "../../employee/Reviews/addMyReview.action";

                                            /* Sql query for retrieving resultset from DataBase */
                                            /*queryString  =null;*/
                                            //queryString = "SELECT ReviewType FROM tblEmpReview JOIN tblLkReviews ON (ReviewTypeId = tblLkReviews.Id)";
                                            String empId = session.getAttribute(ApplicationConstants.SESSION_EMP_ID).toString();
                                            if (request.getAttribute(ApplicationConstants.EMP_REVIEWS_LIST) != null) {
                                                queryString = request.getAttribute(ApplicationConstants.EMP_REVIEWS_LIST).toString();
                                            }

                                         //  out.println(queryString);

                                            
                                            

                                          //  out.println("--------"+submittedFrom);
%>

                                    <s:form action="" theme="simple" name="frmDBGrid">   

                                        <table cellpadding="0" cellspacing="0" width="100%" border="0">


                                            <tr>
                                            <td width="100%">


                                             <grd:dbgrid id="tblStat" name="tblStat" width="98" pageSize="15"
                                                currentPage="<%=intCurr%>" border="0" cellSpacing="1" cellPadding="2"
                                                dataMember="<%=queryString%>" dataSource="<%=connection%>" cssClass="gridTable">

                                                    <grd:gridpager imgFirst="../includes/images/DBGrid/First.gif" imgPrevious="../includes/images/DBGrid/Previous.gif" 
                                                                   imgNext="../includes/images/DBGrid/Next.gif" imgLast="../includes/images/DBGrid/Last.gif"
                                                                   addImage="../includes/images/DBGrid/Add.png"  addAction="javascript:addHolidayOverlay();"
                                                                   />  
                                                    <%--    <grd:gridpager imgFirst="../../includes/images/DBGrid/First.gif" imgPrevious="../../includes/images/DBGrid/Previous.gif" 
                                                                       imgNext="../../includes/images/DBGrid/Next.gif" imgLast="../../includes/images/DBGrid/Last.gif"
                                                                     
                                                                       />          --%>  

                                                    <grd:gridsorter sortColumn="<%=strSortCol%>" sortAscending="<%=blnSortAsc%>" 
                                                                    imageAscending="../includes/images/DBGrid/ImgAsc.gif" 
                                                                    imageDescending="../includes/images/DBGrid/ImgDesc.gif"/>   

                                                    <grd:rownumcolumn headerText="SNo" width="4" HAlign="right" />
                                                    <grd:datecolumn dataField="Date" dataFormat="MM-dd-yyyy" headerText="Date" width="10"/>
                                                    <grd:textcolumn dataField="Country" headerText="Country" width="10"/>
                                                    <grd:textcolumn dataField="Description" headerText="Holiday Description" width="10"/>
                                              </grd:dbgrid>
                                           
                                                <input TYPE="hidden" NAME="txtCurr" VALUE="<%=intCurr%>">
                                                <input TYPE="hidden" NAME="txtSortCol" VALUE="<%=strSortCol%>">
                                                <input TYPE="hidden" NAME="txtSortAsc" VALUE="<%=strSortOrd%>">

                                                <input type="hidden" name="submitFrom" value="dbGrid">
												
												 <s:hidden  name="year" value="%{year}"/>
                                    <s:hidden  name="country" value="%{country}"/>
                                               
                                            </td>
                                            </tr>
                                        </table>                                

                                    </s:form>

                                    <%
                                            connection.close();
                                            connection = null;
                                        } catch (Exception ex) {
                                            out.println(ex.toString());
                                        } finally {
                                            if (connection != null) {
                                                connection.close();
                                                connection = null;
                                            }
                                        }
                                    %>
                                </td>
                                </tr>
                            </table>

                            <!-- End Overlay -->
                            <!-- Start Special Centered Box -->

                            <%-- </sx:div > --%>
                        </div>
                        <!--//END TAB : -->
                        <%-- </sx:tabbedpanel> --%>
                    </div>
                    <script type="text/javascript">

                        var countries=new ddtabcontent("accountTabs")
                        countries.setpersist(false)
                        countries.setselectedClassTarget("link") //"link" or "linkparent"
                        countries.init()

                    </script>
					 
                    <!--//END TABBED PANNEL : -->
                </td>
                <!--//END DATA COLUMN : Coloumn for Screen Content-->
                </tr>
            </table>
        </td>
    </tr>
    <!--//END DATA RECORD : Record for LeftMenu and Body Content-->

    <!--//START FOOTER : Record for Footer Background and Content-->
    <tr class="footerBg">
    <td align="center"><s:include value="/includes/template/Footer.jsp"/></td>
</tr>
<!--//END FOOTER : Record for Footer Background and Content-->

</table>
<!--//END MAIN TABLE : Table for template Structure-->



</body>
</html>



