/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*Don't Alter these Methods*/
var totalRec=0;
function newXMLHttpRequest() {
    var xmlreq = false;
    if(window.XMLHttpRequest) {
        xmlreq = new XMLHttpRequest();
    } else if(window.ActiveXObject) {
        try {
            xmlreq = new ActiveXObject("MSxm12.XMLHTTP");
        } catch(e1) {
            try {
                xmlreq = new ActiveXObject("Microsoft.XMLHTTP");
            } catch(e2) {
                xmlreq = false;
            }
        }
    }
    return xmlreq;
}

function readyStateHandler(req,responseXmlHandler) {
    return function() {
        if (req.readyState == 4) {
            if (req.status == 200) {
                responseXmlHandler(req.responseXML);
            } else {
                alert("HTTP error"+req.status+" : "+req.statusText);
            }
        }
    }
}
function readyStateHandlerText(req,responseTextHandler){
    return function() {
        if (req.readyState == 4) {
            if (req.status == 200) {               
                responseTextHandler(req.responseText);
            } else {
                alert("HTTP error"+req.status+" : "+req.statusText);
            }
        }
    }
} 

function generateTableHeader(tableBody,headerFields) {
    //alert("In generateTableHeader");
    var row;
    var cell;
    row = document.createElement( "TR" );
    row.className="gridHeader";
    tableBody.appendChild( row );
    
    for (var i=0; i<headerFields.length; i++) {
        cell = document.createElement( "TD" );
        cell.className="gridHeader";
        row.appendChild( cell );
        cell.innerHTML = headerFields[i];
        cell.width = 120;
    }
}

function ParseAndGenerateHTML1(oTable,responseString,headerFields) {
    
    //   alert("ParseAndGenerateHTML");
    var start = new Date();
    var fieldDelimiter = "#^$";
    var recordDelimiter = "*@!";   
    //alert('responseString%%%% ******'+responseString);
    //alert('rowCount%%%% ******'+rowCount);
      
    
    var records = responseString.split(recordDelimiter); 
    //  alert("records---->"+records);
    generateTable(oTable,headerFields,records,fieldDelimiter);
}

function generateTable(oTable, headerFields,records,fieldDelimiter) {	
    //   alert("In Generate Table "+fieldDelimiter);
    //alert(records);
  var tbody
     if(oTable.id == "tblPmoProjectList") {
        generateTableDynamicHeader(oTable,headerFields);
        tbody = document.createElement("TBODY");
        oTable.appendChild(tbody);
    }else{
    
     tbody = oTable.childNodes[0];    
    tbody = document.createElement("TBODY");
    oTable.appendChild(tbody);
    generateTableHeader(tbody,headerFields);
    }
    var rowlength;
    rowlength = records.length-1;
    // alert("rowlength--->"+rowlength);
    if(rowlength >0 && records!=""){
        //alert("rowlength-->^"+records);
        for(var i=0;i<rowlength;i++) {
            if(oTable.id == "tblPmoReport"){
                generatePMOAvailableReport(tbody,records[i],fieldDelimiter); 
            }else if(oTable.id == "tblPmoProjectList"){
                  totalRec=rowlength;
                generateProjectDetailsReport(tbody,records[i],fieldDelimiter);
            }else if(oTable.id == "tblProjectEmployeeDetails"){
                generateProjectEmpDetailsReport(tbody,records[i],fieldDelimiter); 
            }else if(oTable.id == "tblPmoCutomerProjectList"){
                  totalRec=rowlength;
                generateCutomerProjectDetailsReport(tbody,records[i],fieldDelimiter); 
            }
else if(oTable.id == "tblResourceTypeDetails"){
                generatetblResourceTypeDetails(tbody,records[i],fieldDelimiter); 
            }else if(oTable.id == "tblProjectListDetails"){
                generateProjectListDetails(tbody,records[i],fieldDelimiter); 
            }else if(oTable.id == "tblMultipleProjectEmployeeList"){
                generateMultipleProjectsEmployeeList(i,tbody,records[i],fieldDelimiter); 
            }else if(oTable.id == "tblMultipleProjectListDetails"){
                generateMultipleProjectsEmployeeListDetails(i,tbody,records[i],fieldDelimiter); 
            }else if(oTable.id == "tblTimeSheetReport"){
                generateTotalHoursbyProjectListDetails(tbody,records[i],fieldDelimiter); 
            }else if(oTable.id == "tblTimeSheetHours"){
                generateTotalHoursbyEmployee(tbody,records[i],fieldDelimiter); 
            } 
            else if(oTable.id == "tblProjectResourceTypeDetails"){
                generatetblProjectResourceTypeDetails(tbody,records[i],fieldDelimiter); 
            }else if(oTable.id == "tblEmpConsalodateReportList"){
            
            	generateEmpConsolidateReport(tbody,records[i],fieldDelimiter);
            }else if(oTable.id == "tblAvailableDetails"){
                generateAvailableProjectEmployeeDetailsReport(tbody,records[i],fieldDelimiter); 
            }else if(oTable.id == "tblActiveProjectDetails"){
            	generateActiveProjectDetailsReport(tbody,records[i],fieldDelimiter); 
            }

            else{
                generateRow(oTable,tbody,records[i],fieldDelimiter);  
            }
        }
       
    } else {
        generateNoRecords(tbody,oTable);
    }
   // generateFooter(tbody,oTable);
   if(oTable.id != "tblMultipleProjectListDetails"){
    generateFooter(tbody,oTable);
    }

}
function generateRow(oTable,tableBody,record,delimiter) {
    //  alert("In generate Row function")
    var row;
    var cell;
    var fieldLength;
    var fields = record.split(delimiter);
    fieldLength = fields.length ;
    var length;
    if(oTable.id == "tblPmoReport" ||  oTable.id == "tblOffshoreEmployeeDetails"){
        length = fieldLength;
    }
    row = document.createElement( "TR" );
    row.className="gridRowEven";
    tableBody.appendChild( row );
    
    for (var i=0;i<length;i++) {
        cell = document.createElement( "TD" );
        cell.className="gridColumn";
        cell.innerHTML = fields[i];
        if(fields[i]!=''){
            row.appendChild( cell );
        }
    }
}

function generateNoRecords(tbody,oTable) {
    var noRecords =document.createElement("TR");
    noRecords.className="gridRowEven";
    tbody.appendChild(noRecords);
    cell = document.createElement("TD");
    cell.className="gridColumn";
    if(oTable.id == "tblPmoReport"){
        cell.colSpan = "11";
    }else if(oTable.id == "tblPmoProjectList"){
        cell.colSpan = "11";
    }else if(oTable.id == "tblProjectEmployeeDetails"){
        cell.colSpan = "5";
    }else if(oTable.id == "tblPmoCutomerProjectList"){
        cell.colSpan = "5";
    }
else if(oTable.id == "tblResourceTypeDetails"){
        cell.colSpan = "5";
    }else if(oTable.id == "tblProjectListDetails"){
        cell.colSpan = "6";
    }else if(oTable.id == "tblMultipleProjectEmployeeList" ||  oTable.id == "tblOffshoreEmployeeDetails"){
        cell.colSpan = "7";
    }else if(oTable.id == "tblMultipleProjectListDetails"){
        cell.colSpan = "8";
    }else if(oTable.id == "tblAvaiableEmployeBySubPractice"){
cell.colSpan = "6";
}else if(oTable.id == "tblTimeSheetReport"){
cell.colSpan = "10";
}else if(oTable.id == "tblTimeSheetHours"){
cell.colSpan = "7";
}
else if(oTable.id == "tblProjectResourceTypeDetails"){
    cell.colSpan = "5";
}
else if(oTable.id == "tblEmpConsalodateReportList"){
	cell.colSpan = "13";
}  
else if(oTable.id == "tblTimeSheetDayByDayReport"){
	cell.colSpan = "12";
}else if(oTable.id == "tblAvailableDetails"){
    cell.colSpan = "7";
}else if(oTable.id == "tblActiveProjectDetails"){
    cell.colSpan = "7";
}	
    cell.innerHTML = "No Records Found for this Search";
    noRecords.appendChild(cell);
}

function generateFooter(tbody,oTable) {
    //alert(oTable.id);
    var footer =document.createElement("TR");
    //footer.className="gridPager";
     if(oTable.id != "tblPmoProjectList"){
    footer.className="gridPager";
     }
     else{
      $("#tblPmoProjectList").css("background","#3e93d4");   
     }
    tbody.appendChild(footer);
    cell = document.createElement("TD");
    cell.className="gridFooter";
    cell.id="footer"+oTable.id;
    //cell.colSpan = "5";
    if(oTable.id == "tblPmoReport"){
        cell.colSpan = "12";
        cell.innerHTML ="Total&nbsp;Records&nbsp;:"+totalRec;
     cell.setAttribute('align','right');
    }
    else if(oTable.id == "tblPmoProjectList"){
        cell.colSpan = "7";
        cell.innerHTML ="Total&nbsp;Records&nbsp;:"+totalRec;
     cell.setAttribute('align','right');
    }else if(oTable.id == "tblProjectEmployeeDetails"){
        cell.colSpan = "5";
    }else if(oTable.id == "tblPmoCutomerProjectList"){
        cell.colSpan = "5";
        cell.innerHTML ="Total&nbsp;Records&nbsp;:"+totalRec;
     cell.setAttribute('align','right');
    }
else if(oTable.id == "tblResourceTypeDetails"){
        cell.colSpan = "5";
    }else if(oTable.id == "tblProjectListDetails"){
        cell.colSpan = "6";
    }else if(oTable.id == "tblMultipleProjectEmployeeList" ||  oTable.id == "tblOffshoreEmployeeDetails"){
        cell.colSpan = "7";
    }else if(oTable.id == "tblMultipleProjectListDetails"){
        cell.colSpan = "8";
    }else if(oTable.id == "tblAvaiableEmployeBySubPractice"){
cell.colSpan = "6";
}else if(oTable.id == "tblTimeSheetReport"){
cell.colSpan = "10";
}else if(oTable.id == "tblTimeSheetHours"){
cell.colSpan = "7";
}	else if(oTable.id == "tblEmpConsalodateReportList"){
	cell.colSpan = "13";
}	
else if(oTable.id == "tblTimeSheetDayByDayReport"){
	cell.colSpan = "11";
}else if(oTable.id == "tblAvailableDetails"){
    cell.colSpan = "7";
}else if(oTable.id == "tblActiveProjectDetails"){
    cell.colSpan = "7";
}
	
    footer.appendChild(cell);
}


function clearTable(tableId) {
    var tbl =  tableId;
    var lastRow = tbl.rows.length; 
    while (lastRow > 0) { 
        tbl.deleteRow(lastRow - 1);  
        lastRow = tbl.rows.length; 
    } 
}


/*********************************************
*Available Employees  List Report start
********************************************/

function readyStateHandler80(req,responseXmlHandler) {
    return function() {
        if (req.readyState == 4) {
            if (req.status == 200) {
                responseXmlHandler(req.responseXML);
            } else {
                alert("HTTP error"+req.status+" : "+req.statusText);
            }
        }
    }
}

function getPracticeDataV2() {
  
    var departmentName=document.getElementById("departmentId").value;
    var req = newXMLHttpRequest();
    req.onreadystatechange = readyStateHandler80(req, populatePractice);
    var url = CONTENXT_PATH+"/getEmpDepartment.action?departmentName="+departmentName;
    req.open("GET",url,"true");    
    req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    req.send(null);
    
}

function populatePractice(resXML) {    
    
    var practiceId = document.getElementById("practiceId");
    var department = resXML.getElementsByTagName("DEPARTMENT")[0];
    var practices = department.getElementsByTagName("PRACTICE");
    practiceId.innerHTML=" ";
    
    for(var i=0;i<practices.length;i++) {
        var practiceName = practices[i];
        
        var name = practiceName.firstChild.nodeValue;
        var opt = document.createElement("option");
        if(i==1){
            opt.setAttribute("value","All");
        }else{
            opt.setAttribute("value",name);
        }
        opt.appendChild(document.createTextNode(name));
        practiceId.appendChild(opt);
    }
}

 function getPMOAvailableList()
{
	 document.getElementById("totalState1").innerHTML = "";
    var oTable = document.getElementById("tblPmoReport");
    clearTable2(oTable);
     $('span.pagination').empty().remove();
    document.getElementById("totalState1").innerHTML = "";
    var state=document.getElementById("state").value;
    var country=document.getElementById("country").value;
    var departmentId=document.getElementById("departmentId").value;
    var practiceId=document.getElementById("practiceId").value;
    var subPracticeId=document.getElementById("subPractice2").value;
    var sortedBy=document.getElementById("sortedBy").value;
  var resourceType=document.getElementById("resourceType").value;
  var skillSet=document.getElementById("skillSet").value;
  var utilization=document.getElementById("utilizationSearch").value;
  var accountId = document.getElementById("customerName").value;
  var projectId = document.getElementById("projectName").value;
  var location=document.getElementById("location1").value;
  var basedOn=document.getElementById("basedOn").value;	
    if(state=="-1")
    {
        alert("please select status");
        return false;
    }
    if(departmentId.trim()==""){
        departmentId="-1";
    }
    if(practiceId=="-1" || practiceId.trim()=="" || practiceId.trim()=="--Please Select--")
    {
        practiceId="-1";
    }
    if(subPracticeId=="-1" || subPracticeId=="All")
    {
        subPracticeId="-1";
    }
    var req = newXMLHttpRequest();
    req.onreadystatechange = function() {
        if (req.readyState == 4) {
            if (req.status == 200) {      
                document.getElementById("pmoAvailableReport").style.display = 'none';
                displayPMOAvailableReport(req.responseText);       
             
            } 
        }else {
            document.getElementById("pmoAvailableReport").style.display = 'block';
        // alert("HTTP error ---"+req.status+" : "+req.statusText);
        }
    }; 
    //   var url = CONTENXT_PATH+"/getAvailableEmpReport.action?state="+escape(state)+"&country="+country+"&departmentId="+departmentId+"&practiceId="+practiceId;
  //  var url = CONTENXT_PATH+"/getAvailableEmpReport.action?state="+escape(state)+"&country="+country+"&departmentId="+departmentId+"&practiceId="+practiceId+"&subPracticeId="+subPracticeId;
  //var url = CONTENXT_PATH+"/getAvailableEmpReport.action?state="+escape(state)+"&country="+country+"&departmentId="+departmentId+"&practiceId="+practiceId+"&subPracticeId="+subPracticeId+"&sortedBy="+sortedBy +"&resourceType="+resourceType;
  //  var url = CONTENXT_PATH+"/getAvailableEmpReport.action?state="+escape(state)+"&country="+country+"&departmentId="+departmentId+"&practiceId="+practiceId+"&subPracticeId="+subPracticeId+"&sortedBy="+sortedBy +"&resourceType="+resourceType+"&skillSet="+escape(skillSet)+"&utilization="+escape(utilization);;
    //var url = CONTENXT_PATH+"/getAvailableEmpReport.action?state="+escape(state)+"&country="+country+"&departmentId="+departmentId+"&practiceId="+practiceId+"&subPracticeId="+subPracticeId+"&sortedBy="+sortedBy +"&resourceType="+resourceType+"&skillSet="+escape(skillSet)+"&utilization="+escape(utilization)+"&accId="+accountId+"&projectId="+projectId+"&location="+location;
    var url = CONTENXT_PATH+"/getAvailableEmpReport.action?state="+escape(state)+"&country="+country+"&departmentId="+departmentId+"&practiceId="+practiceId+"&subPracticeId="+subPracticeId+"&sortedBy="+sortedBy +"&resourceType="+resourceType+"&skillSet="+escape(skillSet)+"&utilization="+escape(utilization)+"&accId="+accountId+"&projectId="+projectId+"&location="+location+"&basedOn="+basedOn;		
    
    req.open("GET",url,"true");    
    req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    req.send(null);
}



 function displayPMOAvailableReport(response) {
	// alert(response);
	    var oTable = document.getElementById("tblPmoReport");
	    clearTable(oTable);
	     var state=document.getElementById("state").value;

	    var dataArray = response;
	    
	    if(dataArray == "no data")
	    {
	        alert("No Records Found for this Search");   
	    }
	    else {
	        
	        // tbody = document.createElement("TBODY");
	     //   var headerFields = new Array("S.No","Reports To","Employee name","practice","Email","Phone Number","SkillSet");
	       if(state=="Available")
	        {
	            // tbody = document.createElement("TBODY");
	       //     var headerFields = new Array("S.No","Reports&nbsp;To","Employee&nbsp;name","Exp","Practice","Available Utilization","Available&nbsp;From","Email","Phone&nbsp;Number","SkillSet");
	           // var headerFields = new Array("S.No","Employee&nbsp;name","Total&nbsp;Exp","Practice","Available Utilization","Available&nbsp;From","Contact&nbsp;Details","SkillSet");
	    	   var headerFields = new Array("S.No","Employee&nbsp;name","Employee No","Total&nbsp;Exp","Practice","Available Utilization","Available&nbsp;From","Contact&nbsp;Details","SkillSet");
	        //generateHeader(headerArray,tableId);
	        }
	        else if(state=="OnProject"){
	            // var headerFields = new Array("S.No","Reports&nbsp;To","Employee&nbsp;name","Exp","Practice","Project&nbsp;Name","Res&nbsp;Type","Available Utilization","Email","Phone&nbsp;Number","SkillSet");
	             
	         //    var headerFields = new Array("S.No","Employee&nbsp;name","Total&nbsp;Exp","Practice","Project&nbsp;Name","ProjectExp","Res&nbsp;Type","Avail.Util","Contact&nbsp;Details","SkillSet");
	      //  var headerFields = new Array("S.No","Employee&nbsp;name","Total&nbsp;Exp","ProjectExp","Practice","Project&nbsp;Name","Res&nbsp;Type","Avail.Util","Contact&nbsp;Details","SkillSet");
	        	 var headerFields = new Array("S.No","Employee&nbsp;name","Employee No","Total&nbsp;Exp","ProjectExp","Practice","Customer&nbsp;Name","Project&nbsp;Name","Res&nbsp;Type","Util","Contact&nbsp;Details","SkillSet");
	       
	   }
	        else if(state == "OverHead")
	        {
	           // var headerFields = new Array("S.No","Reports&nbsp;To","Employee&nbsp;name","Exp","Practice","Project&nbsp;Name","Email","Phone&nbsp;Number","SkillSet");
	   
	 //  var headerFields = new Array("S.No","Employee&nbsp;name","Total&nbsp;Exp","Practice","Project&nbsp;Name","Contact&nbsp;Details","SkillSet");
	        	 var headerFields = new Array("S.No","Employee&nbsp;name","Employee No","Total&nbsp;Exp","Practice","Project&nbsp;Name","StartDate","Contact&nbsp;Details","SkillSet");
	        }
	        else
	        {
	           // var headerFields = new Array("S.No","Reports&nbsp;To","Employee&nbsp;name","Exp","Practice","Project&nbsp;Name","Email","Phone&nbsp;Number","SkillSet");
	   
	 //  var headerFields = new Array("S.No","Employee&nbsp;name","Total&nbsp;Exp","Practice","Project&nbsp;Name","Contact&nbsp;Details","SkillSet");
	        	 var headerFields = new Array("S.No","Employee&nbsp;name","Employee No","Total&nbsp;Exp","Practice","Project&nbsp;Name","Contact&nbsp;Details","SkillSet");
	        }
	        //generateHeader(headerArray,tableId);
	       
	        var temp = new Array;
	        temp = dataArray.split('addTo');            
	       
	        if(response!=''){
	        	 // generateTableDynamicHeader(oTable,headerFields);
	            document.getElementById("totalState1").innerHTML = temp[1];
	            totalRec= temp[1];
	            ParseAndGenerateHTML1(oTable,temp[0], headerFields);
	            
	        /* pagerOption();*/
	        }else{
	            alert('No Result For This Search...');
	            spnFast.innerHTML="No Result For This Search...";                
	        }
	    } 
	}
	
//function generatePMOAvailableReport(tableBody,record,fieldDelimiter){
//    var row;
//    var cell;
//    var fieldLength;
//     var state=document.getElementById("state").value;
//    var fields = record.split(fieldDelimiter);
//    fieldLength = fields.length ;
//    var length = fieldLength;
//    row = document.createElement( "TR" );
//    row.className="gridRowEven";
//    tableBody.appendChild( row );
//    for (var i=1;i<length-2;i++) {            
//        cell = document.createElement( "TD" );
//        cell.className="gridColumn";       
//       // cell.innerHTML = fields[i];
//      /*  if(parseInt(i,10)==2){
//              cell.innerHTML = fields[i];  
//            cell.innerHTML = "<a href='javascript:getEmployeeDetails("+fields[0]+")'>"+fields[i]+"</a>";
//        }*/
//        if(parseInt(i,10)==2){
//            cell.innerHTML = fields[2];
//         }
//       if(state=="Available" )
//        {
////            if(parseInt(i,10)==10){
////                cell.innerHTML = "<a href='javascript:getSkillSet("+fields[0]+")'>"+fields[i]+"</a>";
////            }
//
//
//if(parseInt(i,10)==8){
//                cell.innerHTML = "<a href='javascript:getSkillSet("+fields[0]+")'>"+fields[10]+"</a>";
//            }
//            if(parseInt(i,10)==7){
//                
//            
//            cell.innerHTML = "<a href='javascript:getEmployeeContactDetails("+fields[0]+")'>"+fields[9]+"</a>";
//            }
//            if(parseInt(i,10)==3){
//                
//           // alert("in exp");
//            cell.innerHTML = "<a href='javascript:getEmployeeExpDetails(\""+fields[3]+"\",\""+fields[7]+"\")'>"+fields[8]+"</a>";
//            }
//             if (i==4){
//            cell.innerHTML = fields[4];
//         }
////          if (i==3){
////            cell.innerHTML = fields[3];
////         }
//         if (i==5){
//            cell.innerHTML = fields[5];
//         }
//         if (i==6){
//            cell.innerHTML = fields[6];
//         }
//         
//
//           if (i==1){
//            cell.innerHTML = fields[1];
//         } 
//
//        }
//else if( state=="OnProject")
//        {
//
// if(parseInt(i,10)==10){
//            cell.innerHTML = "<a href='javascript:getSkillSet("+fields[0]+")'>"+fields[11]+"</a>";
//             }
//             if(parseInt(i,10)==9){
//                  cell.innerHTML = "<a href='javascript:getEmployeeContactDetails("+fields[0]+")'>"+fields[10]+"</a>";
//                
//            }
//             if(parseInt(i,10)==8){
//                 
//             cell.innerHTML = fields[7];
//           
//        }
//        if(parseInt(i,10)==3){
//                
//           // alert("in exp");
//            cell.innerHTML = "<a href='javascript:getEmployeeExpDetails(\""+fields[3]+"\",\""+fields[8]+"\")'>"+fields[9]+"</a>";
//            }
//         
//         if (i==4){
//            cell.innerHTML = fields[12];
//         }
//        
//         
//         if (i==5){
//            cell.innerHTML = fields[4];
//         }
//         if (i==6){
//            cell.innerHTML = fields[5];
//         }
//         if (i==7){
//            cell.innerHTML = fields[6];
//         }
//         if (i==1){
//            cell.innerHTML = fields[1];
//         }
//
//        }	
//        else
//        {
////            if(parseInt(i,10)==9){
////                cell.innerHTML = "<a href='javascript:getSkillSet("+fields[0]+")'>"+fields[i]+"</a>";
////            } 
//
//
// if(parseInt(i,10)==7){
//                cell.innerHTML = "<a href='javascript:getSkillSet("+fields[0]+")'>"+fields[9]+"</a>";
//            } 
//            
//             if(parseInt(i,10)==6){
//            cell.innerHTML = "<a href='javascript:getEmployeeContactDetails("+fields[0]+")'>"+fields[8]+"</a>";
//        }
//         if(parseInt(i,10)==3){
//                
//           // alert("in exp");
//            cell.innerHTML = "<a href='javascript:getEmployeeExpDetails(\""+fields[3]+"\",\""+fields[6]+"\")'>"+fields[7]+"</a>";
//            }
//         if (i==4){
//            cell.innerHTML = fields[4];
//         }
//         if (i==5){
//            cell.innerHTML = fields[5];
//         }
//         
//           if (i==1){
//            cell.innerHTML = fields[1];
//         } 
//
//
//        }
//
//          if(fields[i]!=''){
//            if((i==5 || i==6) && state=="Available")
//            {
//                cell.setAttribute("align","right");
//            }
//             else if((i==7) && state=="OnProject")
//            {
//                cell.setAttribute("align","right");
//            }else if(i==3){
//                 cell.setAttribute("align","right");
//            }
//            else
//            {
//                cell.setAttribute("align","left");     
//            }
//            row.appendChild( cell );
//        }
//    }
//   
//}


 function generatePMOAvailableReport(tableBody,record,fieldDelimiter){
		var projectId = document.getElementById("projectName").value;
		//alert("projectId"+projectId);
	    var row;
	    var cell;
	    var fieldLength;
	    var fields = record.split(fieldDelimiter);
	    fieldLength = fields.length ;
	    var state=document.getElementById("state").value;
	    var length = fieldLength;
	    row = document.createElement( "TR" );
	    row.className="gridRowEven";
	    tableBody.appendChild( row );
	// alert("fields[15]"+fields[15]);
	    for (var i=1;i<length-2;i++) {            
	        cell = document.createElement( "TD" );
	        cell.className="gridColumn";       
	      //
	        if(parseInt(i,10)==2){
	              cell.innerHTML = fields[i];  
	            cell.innerHTML = "<a href='javascript:getEmployeeDetails("+fields[0]+")'>"+fields[i]+"</a>";
	        }
	      if(state=="Available")
	        {
	         //   alert("available");
	           /* if(parseInt(i,10)==10){
	                cell.innerHTML = "<a href='javascript:getSkillSet("+fields[0]+")'>"+fields[i]+"</a>";
	            }*/
	            if(parseInt(i,10)==9){
	                cell.innerHTML = "<a href='javascript:getSkillSet("+fields[0]+")'>"+fields[11]+"</a>";
	            }
	            if(parseInt(i,10)==8){
	                
	            
	            cell.innerHTML = "<a href='javascript:getEmployeeContactDetails("+fields[0]+")'>"+fields[10]+"</a>";
	            }
	            if(parseInt(i,10)==4){
	                
	           // alert("in exp");
	            cell.innerHTML = "<a href='javascript:getEmployeeExpDetails(\""+fields[4]+"\",\""+fields[8]+"\")'>"+fields[9]+"</a>";
	            }
	            
	            if (parseInt(i,10)==6 ){
	            	if(fields[6] < 100)
	            		{
	            	  cell.innerHTML = "<a href='javascript:getEmployeeAvailableDetails("+fields[0]+")'>"+fields[6]+"</a>";
	             }else
	            	 {
	            	 cell.innerHTML = fields[6];
	            	 }
	            	 }
	          
	          
	          if (i==3){
	           cell.innerHTML = fields[3];
	         }
	         if (i==5){
	            cell.innerHTML = fields[5];
	         }
	        
	         if (i==7){
	             cell.innerHTML = fields[7];
	          }
	          


	           if (i==1){
	            cell.innerHTML = fields[1];
	         } 
	        } 
	      else if(state=="OnProject")
	      {
	    	  
	        if(parseInt(i,10)==12){
	          cell.innerHTML = "<a href='javascript:getSkillSet("+fields[0]+")'>"+fields[12]+"</a>";
	           }
	           if(parseInt(i,10)==11){
	        	   
	                cell.innerHTML = "<a href='javascript:getEmployeeContactDetails("+fields[0]+")'>"+fields[13]+"</a>";
	        	   }
	        	  
	           if(parseInt(i,10)==9){
	        	 
	        		   cell.innerHTML=fields[8];
	        		  
	      }
	           if(parseInt(i,10)==10){
		        	 
        		   cell.innerHTML=fields[9];
        		  
      }
	           if(parseInt(i,10)==7){
	        	   
	    		   cell.innerHTML=fields[7];
	    		  
	  }
	        	   
	           if(parseInt(i,10)==8){
	        	   if(projectId == '-1'){
	           	   cell.innerHTML = "<a href='javascript:getActiveProjectDetails("+fields[0]+")'>"+fields[6]+"</a>";  
	            
	             
	          }
	        	   else
	    		   {
	    		   cell.innerHTML=fields[6];
	    		   }
	  }
	           
	      if(parseInt(i,10)==4){
	              
	         // alert("in exp");
	          cell.innerHTML = "<a href='javascript:getEmployeeExpDetails(\""+fields[4]+"\",\""+fields[10]+"\")'>"+fields[11]+"</a>";
	          }
	       
	      if (parseInt(i,10)==5){
	    	   
	    	   cell.innerHTML = "<a href='javascript:getProjectExpDetails("+fields[15]+",\""+fields[11]+"\")'>"+fields[14]+"</a>";
	         // cell.innerHTML = fields[13];
	       }
	       
	       if (i==6){
	          cell.innerHTML = fields[5];
	       }
	       
	       
	       if (i==3){
	           cell.innerHTML = fields[3];
	        }
	       if (i==1){
	          cell.innerHTML = fields[1];
	       }
	       
	      } else if(state=="OverHead")
	      {
	    	  if(parseInt(i,10)==9){
	              cell.innerHTML = "<a href='javascript:getSkillSet("+fields[0]+")'>"+fields[10]+"</a>";
	          } 
	          
	           if(parseInt(i,10)==8){
	          cell.innerHTML = "<a href='javascript:getEmployeeContactDetails("+fields[0]+")'>"+fields[9]+"</a>";
	      }
	       if(parseInt(i,10)==4){
	              
	         // alert("in exp");
	          cell.innerHTML = "<a href='javascript:getEmployeeExpDetails(\""+fields[4]+"\",\""+fields[7]+"\")'>"+fields[8]+"</a>";
	          }
	        
	       if (i==3){
	          cell.innerHTML = fields[3];
	       }
	       if (i==5){
	          cell.innerHTML = fields[5];
	       }
	       if (i==6){
	           cell.innerHTML = fields[6];
	        }
	         if (i==1){
	          cell.innerHTML = fields[1];
	       }
	         
	         if(i==7){
	             cell.innerHTML = fields[11];
	         } 

	      }
	      
	      else
	      {
	         /* if(parseInt(i,10)==9){
	              cell.innerHTML = "<a href='javascript:getSkillSet("+fields[0]+")'>"+fields[i]+"</a>";
	          } */
	           if(parseInt(i,10)==8){
	              cell.innerHTML = "<a href='javascript:getSkillSet("+fields[0]+")'>"+fields[10]+"</a>";
	          } 
	          
	           if(parseInt(i,10)==7){
	          cell.innerHTML = "<a href='javascript:getEmployeeContactDetails("+fields[0]+")'>"+fields[9]+"</a>";
	      }
	       if(parseInt(i,10)==4){
	              
	         // alert("in exp");
	          cell.innerHTML = "<a href='javascript:getEmployeeExpDetails(\""+fields[4]+"\",\""+fields[7]+"\")'>"+fields[8]+"</a>";
	          }
	        
	       if (i==3){
	          cell.innerHTML = fields[3];
	       }
	       if (i==5){
	          cell.innerHTML = fields[5];
	       }
	       if (i==6){
	           cell.innerHTML = fields[6];
	        }
	         if (i==1){
	          cell.innerHTML = fields[1];
	       }
	         
	         

	      }

	      if(fields[i]!=''){
	          if(i==1)
	          {
	              cell.setAttribute("align","left");
	          }
	          else
	          {
	              cell.setAttribute("align","left");     
	          }
	          row.appendChild( cell );
	      }
	  }
	 
	}






function getSkillSet(empId){
  
    var req = newXMLHttpRequest();
    req.onreadystatechange = readyStateHandlerText(req, populateSkillset);
    var url = CONTENXT_PATH+"/popupSkillSetWindow.action?empId="+empId;
    req.open("GET",url,"true");    
    req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    req.send(null);
}

function populateSkillset(text) {
    
    var background = "#3E93D4";
    var title = "Emp Skill Set";
    var text1 = text; 
    var size = text1.length;
    
    var res = text1.split("|");

    var res1="";
    for(var i=0;i<res.length;i++){
        if((res[i].trim()== "null") || (res[i].trim()== "null") || (res[i].trim()== "" ) || (res[i].trim() == " " )){

        }else{
            if(i==res.length-1){
                res1=res1+res[i].trim().toUpperCase();
            }
            else{
                res1=res1+res[i].trim().toUpperCase()+",";
            }
        }
    }
        
    var res2=res1.split(",");
    var test2="";
    var count=0;
    for(var i=0;i<res2.length;i++){
        for(var j=i;j<res2.length;j++){
            if(res2[i].trim().toUpperCase()==res2[j].trim().toUpperCase()){
                count=count+1;
            }
            else{
                count=count;
            }
                
        }
            
        if(count==1){
            if(res2[i].trim() != ""){
                test2=test2+res2[i].trim().toUpperCase()+",";
            }
            count=0;
        }
        else{
            count=0;
        }    
    }
    //  alert(test2);
    var finalStr1 = test2.slice(0, test2.lastIndexOf(","));
    //   alert(finalStr1);
    var finalStr="";
    if(finalStr1.trim()== "" ){
        finalStr="No Data";
    }

        
    else{
        finalStr=finalStr1+".".big();
    }
    ///  alert(test2);

    
    //Now create the HTML code that is required to make the popup
    var content = "<html><head><title>"+title+"</title></head>\
    <body bgcolor='"+background +"' style='color:white;'>"+finalStr+"<br />\
    </body></html>";
    //alert(content);
    //    //alert("text1"+text1);
    //    // alert("size "+content.length);
    //    var indexof=(content.indexOf("|")+1);
    //    var lastindexof=(content.lastIndexOf("|"));
    
    popup = window.open("","window","channelmode=0,width=400,height=200,top=200,left=350,scrollbars=no,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no");    
  
 
    popup.document.write("<b>SkillS :</b>"+content.substr(0,content.length));
//Write content into it.  
//Write content into it.    
    
    
    
}

/*********************************************
* Project Experience Details List Start
********************************************/		

function getProjectExpDetails(projectContactId,totalExp){
	
	//alert("totalExp"+totalExp);
	//alert("projectContactId"+projectContactId);
//    var req = newXMLHttpRequest();
//    req.onreadystatechange = readyStateHandlerText(req, populateProjectExp,totalExp);
    
	var req = newXMLHttpRequest();
    req.onreadystatechange = function() {
        if (req.readyState == 4) {
            if (req.status == 200) {    
            	populateProjectExp(req.responseText,totalExp);
                
            } 
        }else {
          // alert("HTTP error ---"+req.status+" : "+req.statusText);
        }
    }; 
    
    var url = CONTENXT_PATH+"/popupProjectExp.action?projectContactId="+projectContactId;
    req.open("GET",url,"true");    
    req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    req.send(null);
}




function populateProjectExp(resText,totalExp) {
	
//	alert("resText"+resText);
//	
//	alert("enter into thos me")
	
	//alert("totalExp"+totalExp);

  var background = "#3E93D4";
var title = "ProjectExpDetailsList";
var size = resText.length;

var text = resText.split("addTo");
//alert("text[0]..."+text[0]+"text[1].."+text[1]+"text[2]....."+text[2]+"text[3]....."+text[3]+"text[4]....."+text[4]+"text[5]....."+text[5]);

var content = "<html><head><title>"+title+"</title></head>\
<body bgcolor='"+background +"' style='color:white;'><h4>"+title+"</h4>TotalExp:"+totalExp+"<br/><br>TotalProjectExp:"+text[1]+"<br><br/>\n";

	var tableData=text[0];
  
	//alert("tblRow" +tblRow);
	if(tableData == "no data")
	{
	  alert("No records found");
	}

	else
	{
   var tblRow=tableData.split("*@!");
	if(tblRow.length == 1)
	{
	  
	content=content+"<table border='1' style:align='center'>";

	content=content+"<tr><th>SNO</th><th>StartDate</th><th>EndDate</th><th>Duration</th><th>Status</th></tr>";
	content=content+"<tr> <td colspan='4'> No record found for this  search </td> </tr>";

	}

	else
	{

	content=content+"<table border='1' style:align='center'>";

	content=content+"<tr><th>SNO</th><th>StartDate</th><th>EndDate</th><th>Duration</th><th>Status</th></tr>";


	for(var n=0;n<tblRow.length;n++){
	content=content+"<tr>";
	var tblCol=tblRow[n].split("#^$");
	for(var j=0;j<tblCol.length;j++){
	content=content+"<td>"+tblCol[j]+"</td>";   
	}
	content=content+"</tr>";
	}
	content=content+"</table>";}}

	content=content+"</body></html>";
	  
	 if(size < 50){
	   //Create the popup       
	   popup = window.open("","window","channelmode=0,width=350,height=300,top=200,left=350,scrollbars=no,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no");    
	   popup.document.write(content); //Write content into it.    
	}

	else if(size < 100){
	   //Create the popup       channelmode
	   popup = window.open("","window","=0,width=400,height=300,top=200,left=350,scrollbars=no,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no");    
	   popup.document.write(content); //Write content into it.    
	}

	else if(size < 260){
	   //Create the popup       
	   popup = window.open("","window","channelmode=0,width=500,height=400,top=200,left=350,scrollbars=no,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no");    
	   popup.document.write(content); //Write content into it.    
	} else {
	   //Create the popup       
	   popup = window.open("","window","channelmode=0,width=600,height=500,top=200,left=350,scrollbars=no,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no");    
	   popup.document.write(content); //Write content into it.    
	}

	}



/*********************************************
* Project Experience Details List End
********************************************/	


/*********************************************
* Available Employees Details Start
********************************************/

function getEmployeeAvailableDetails(EmpId){
	//alert("Hai"+EmpId);  
	// toggleCloseUploadOverlayAvailable();
    var req = newXMLHttpRequest();
    req.onreadystatechange = readyStateHandlerText(req, displayAvailableDetails);
    var url = CONTENXT_PATH+"/getEmployeeAvailableDetails.action?EmpId="+EmpId;
    req.open("GET",url,"true");    
    req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    req.send(null);
}


function displayAvailableDetails(response) {
	
	//alert("response"+response);
    var tableId = document.getElementById("tblAvailableDetails");
    ClrTable(tableId);
    var headerFields = new Array("S.NO","ProjectName","StartDate","EndDate","Resource Type","Utilization");
   
    // alert("responseArray[1]-->"+responseArray[1]);
    //document.getElementById("totalRequirementsFound").innerHTML = responseArray[0];
    var dataArray = response;
    
    //  generateTableHeader(tableId,headerFields)
    ParseAndGenerateHTML1(tableId,dataArray, headerFields);
 
 
    document.getElementById("headerLabel6").style.color="white";
    document.getElementById("headerLabel6").innerHTML="Available Employee ProjectDetails";
            
    var overlay = document.getElementById('overlayAvailableEmployeeDashBoard');
    var specialBox = document.getElementById('specialBoxAvailableEmployeeDashBoard');
    overlay.style.opacity = .8;
    if(overlay.style.display == "block"){
        overlay.style.display = "none";
        specialBox.style.display = "none";
    }
    else {
        overlay.style.display = "block";
        specialBox.style.display = "block";
    }

    
}



function toggleCloseUploadOverlayAvailable() {
	
	//alert("Helloooo");
    var overlay = document.getElementById('overlayAvailableEmployeeDashBoard');
    var specialBox = document.getElementById('specialBoxAvailableEmployeeDashBoard');

    overlay.style.opacity = .8;
    if(overlay.style.display == "block"){
        overlay.style.display = "none";
        specialBox.style.display = "none";
    }
    else {
        overlay.style.display = "block";
        specialBox.style.display = "block";
    }
//window.location="empSearchAll.action";
}


function generateAvailableProjectEmployeeDetailsReport(tableBody,record,fieldDelimiter){
	//alert("heeeeeeeeee");
    var row;
    var cell;
    var fieldLength;
    var state=document.getElementById("state").value;
    var fields = record.split(fieldDelimiter);
    fieldLength = fields.length ;
    var length = fieldLength;
    row = document.createElement( "TR" );
    row.className="gridRowEven";
    tableBody.appendChild( row );
    for (var i=0;i<length;i++) {            
        cell = document.createElement( "TD" );
        cell.className="gridColumn";       
        cell.innerHTML = fields[i];  
       
        if(fields[i]!=''){
            if(i==1)
            {
                cell.setAttribute("align","left");
            }
            else
            {
                cell.setAttribute("align","left");     
            }
            row.appendChild( cell );
        }
    }
   
}


/*********************************************
* Available Employees Details End
********************************************/

/*********************************************
* OnProject Active Employees Details Start
********************************************/

function getActiveProjectDetails(EmpId){
	//alert("Hai"+EmpId);  
	// toggleCloseUploadOverlayAvailable();
    var req = newXMLHttpRequest();
    req.onreadystatechange = readyStateHandlerText(req, displayActiveProjectDetails);
    var url = CONTENXT_PATH+"/getEmployeeActiveProjectDetails.action?EmpId="+EmpId;
    req.open("GET",url,"true");    
    req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    req.send(null);
}


function displayActiveProjectDetails(response) {
	
	//alert("response"+response);
    var tableId = document.getElementById("tblActiveProjectDetails");
    ClrTable(tableId);
    var headerFields = new Array("S.NO","ProjectName","Resource Type","Utilization","StartDate","EndDate");
   
    // alert("responseArray[1]-->"+responseArray[1]);
    //document.getElementById("totalRequirementsFound").innerHTML = responseArray[0];
    var dataArray = response;
    
    //  generateTableHeader(tableId,headerFields)
    ParseAndGenerateHTML1(tableId,dataArray, headerFields);
 
 
    document.getElementById("headerLabel7").style.color="white";
    document.getElementById("headerLabel7").innerHTML="Employees Active ProjectDetails";
            
    var overlay = document.getElementById('overlayActiveProjectDetailsDashBoard');
    var specialBox = document.getElementById('specialBoxActiveProjectDetailsDashBoard');
    overlay.style.opacity = .8;
    if(overlay.style.display == "block"){
        overlay.style.display = "none";
        specialBox.style.display = "none";
    }
    else {
        overlay.style.display = "block";
        specialBox.style.display = "block";
    }

    
}



function toggleCloseUploadOverlayActiveProjects() {
	
	//alert("Helloooo");
    var overlay = document.getElementById('overlayActiveProjectDetailsDashBoard');
    var specialBox = document.getElementById('specialBoxActiveProjectDetailsDashBoard');

    overlay.style.opacity = .8;
    if(overlay.style.display == "block"){
        overlay.style.display = "none";
        specialBox.style.display = "none";
    }
    else {
        overlay.style.display = "block";
        specialBox.style.display = "block";
    }
//window.location="empSearchAll.action";
}


function generateActiveProjectDetailsReport(tableBody,record,fieldDelimiter){
	//alert("heeeeeeeeee");
    var row;
    var cell;
    var fieldLength;
    var state=document.getElementById("state").value;
    var fields = record.split(fieldDelimiter);
    fieldLength = fields.length ;
    var length = fieldLength;
    row = document.createElement( "TR" );
    row.className="gridRowEven";
    tableBody.appendChild( row );
    for (var i=0;i<length;i++) {            
        cell = document.createElement( "TD" );
        cell.className="gridColumn";       
        cell.innerHTML = fields[i];  
       
        if(fields[i]!=''){
            if(i==1)
            {
                cell.setAttribute("align","left");
            }
            else
            {
                cell.setAttribute("align","left");     
            }
            row.appendChild( cell );
        }
    }
   
}


/*********************************************
* OnProject Active Employees Details End
********************************************/



/*********************************************
* Available Employees  List Report End
********************************************/
		
		





		
/***********************************************************
 *PMO project dashboard suggestion list start
 * ********************************************************/
function EmployeeForProjectDetails() {
    var test=document.getElementById("assignedToUID").value;
   
    if (test == "") {

        clearTable1();
        hideScrollBar();
        var validationMessage=document.getElementById("authorEmpValidationMessage");
        validationMessage.innerHTML = "";
        document.frmSearch.preAssignEmpId.value="";
    //frmSearch issuesForm
    } else {
        if (test.length >2) {
            //alert("CONTENXT_PATH-- >"+CONTENXT_PATH)
            var url = CONTENXT_PATH+"/getEmployeeDetailforPMOActivity.action?customerName="+escape(test);
            var req = initRequest(url);
            req.onreadystatechange = function() {
                //    alert("req-->"+req);
                if (req.readyState == 4) {
                    if (req.status == 200) {
                        // alert(req.responseXML);
                    
                        parseEmpMessagesForProjectDeatils(req.responseXML);
                    } else if (req.status == 204){
                        clearTable1();
                    }
                }
            };
            req.open("GET", url, true);

            req.send(null);
        }
    }
}

function parseEmpMessagesForProjectDeatils(responseXML) {
    //alert("-->"+responseXML);
    autorow1 = document.getElementById("menu-popup");
    autorow1.style.display ="none";
    autorow = document.getElementById("menu-popup");
    autorow.style.display ="none";
    clearTable1();
    var employees = responseXML.getElementsByTagName("EMPLOYEES")[0];
    if (employees.childNodes.length > 0) {        
        completeTable.setAttribute("bordercolor", "black");
        completeTable.setAttribute("border", "0");        
    } else {
        clearTable1();
    }
    if(employees.childNodes.length<10) {
        autorow1.style.overflowY = "hidden";
        autorow.style.overflowY = "hidden";        
    }
    else {    
        autorow1.style.overflowY = "scroll";
        autorow.style.overflowY = "scroll";
    }
    
    var employee = employees.childNodes[0];
    var chk=employee.getElementsByTagName("VALID")[0];
    if(chk.childNodes[0].nodeValue =="true") {
        //var validationMessage=document.getElementById("validationMessage");
        var validationMessage;
        
        validationMessage=document.getElementById("authorEmpValidationMessage");
        isPriEmpExist = true;
         
        validationMessage.innerHTML = "";
        document.getElementById("menu-popup").style.display = "block";
        for (loop = 0; loop < employees.childNodes.length; loop++) {
            
            var employee = employees.childNodes[loop];
            var customerName = employee.getElementsByTagName("NAME")[0];
            var empid = employee.getElementsByTagName("EMPID")[0];
            appendEmployeeForProjectDetails(empid.childNodes[0].nodeValue,customerName.childNodes[0].nodeValue);
        }
        var position;
        
        
        position = findPosition(document.getElementById("assignedToUID"));
        
        posi = position.split(",");
        document.getElementById("menu-popup").style.left = posi[0]+"px";
        document.getElementById("menu-popup").style.top = (parseInt(posi[1])+20)+"px";
        document.getElementById("menu-popup").style.display = "block";
    } //if
    if(chk.childNodes[0].nodeValue =="false") {
        var validationMessage = '';
      
        isPriEmpExist = false;
        validationMessage=document.getElementById("authorEmpValidationMessage");
    
 
        validationMessage.innerHTML = " Invalid ! Select from Suggesstion List. ";
        validationMessage.style.color = "green";
        validationMessage.style.fontSize = "12px";
       
        document.getElementById("assignedToUID").value = "";
        document.getElementById("preAssignEmpId").value = "";
            
        
    }
}
function appendEmployeeForProjectDetails(empId,empName) {
    
    var row;
    var nameCell;
    if (!isIE) {
        row = completeTable.insertRow(completeTable.rows.length);
        nameCell = row.insertCell(0);
    } else {
        row = document.createElement("tr");
        nameCell = document.createElement("td");
        row.appendChild(nameCell);
        completeTable.appendChild(row);
    }
    row.className = "popupRow";
    nameCell.setAttribute("bgcolor", "#3E93D4");
    var linkElement = document.createElement("a");
    linkElement.className = "popupItem";
    // if(assignedToType=='pre'){
    linkElement.setAttribute("href", "javascript:set_emp('"+empName +"','"+ empId +"')");

    linkElement.appendChild(document.createTextNode(empName));
    linkElement["onclick"] = new Function("hideScrollBar()");
    nameCell.appendChild(linkElement);
//fillWorkPhone(empId);
}
function set_emp(eName,eID){
    clearTable1();
    document.frmSearch.assignedToUID.value =eName;
    document.frmSearch.preAssignEmpId.value =eID;
}
var isIE;
function initRequest(url) {
    
    if (window.XMLHttpRequest) {
        return new XMLHttpRequest();
    }
    else
    if (window.ActiveXObject) {
        isIE = true;
        return new ActiveXObject("Microsoft.XMLHTTP");
    }
    
}
function clearTable1() {
    if (completeTable) {
        completeTable.setAttribute("bordercolor", "white");
        completeTable.setAttribute("border", "0");
        completeTable.style.visible = false;
        for (loop = completeTable.childNodes.length -1; loop >= 0 ; loop--) {
            completeTable.removeChild(completeTable.childNodes[loop]);
        }
    }
}


function hideScrollBar() {
    autorow = document.getElementById("menu-popup");
    autorow.style.display = 'none';
}
function findPosition( oElement ) {
    if( typeof( oElement.offsetParent ) != undefined ) {
        for( var posX = 0, posY = 0; oElement; oElement = oElement.offsetParent ) {
            posX += oElement.offsetLeft;
            posY += oElement.offsetTop;
        }
        return posX+","+posY;
    } else {
        return oElement.x+","+oElement.y;
    }
}
/***********************************************************
 *PMO  project dashboard suggestion list end
 * ********************************************************/
		

function geProjectListDetails()
{
    var oTable = document.getElementById("tblPmoProjectList");
       
      ClrDashBordTable(oTable);
    
     $('span.pagination').empty().remove();
     var custName=document.getElementById("customerNameId").value;
     var  ProjectName=document.getElementById("projectNameId").value;
     var  status=document.getElementById("status").value;                                                            
    var ProjectStartDate=document.getElementById("projectStartDate").value;
    var pmoLoginId = document.getElementById("pmoLoginId").value;
    var preAssignEmpId = document.getElementById("preAssignEmpId").value;
    var practiceId = document.getElementById("practiceId1").value;
    var empStatus=document.getElementById("empStatus").value;
     var assignedToUID=document.getElementById("assignedToUID").value;
     var projectType=escape(document.getElementById("ProjectTypeIdPV").value);
     var projectBelongTo=escape(document.getElementById("projectBelongTo").value);
    if(assignedToUID.trim()==""){
       preAssignEmpId=""; 
    }
    var req = newXMLHttpRequest();
    req.onreadystatechange = function() {
        if (req.readyState == 4) {
            //  alert("1");
            if (req.status == 200) {    
                //      alert("2");
                document.getElementById("pmoProjectDetailsList").style.display = 'none';
                displayProjectDetailsResult(req.responseText);
                
            } 
        }else {
            document.getElementById("pmoProjectDetailsList").style.display = 'block';
        // alert("HTTP error ---"+req.status+" : "+req.statusText);
        }
    }; 
    var url = CONTENXT_PATH+"/projectDetailsDashboard.action?customerName="+custName+"&projectName="+ProjectName+"&projectType="+unescape(projectType)+"&status="+status+"&projectStartDate="+ProjectStartDate+"&pmoLoginId="+pmoLoginId+"&preAssignEmpId="+preAssignEmpId+"&practiceId="+practiceId+"&empStatus="+empStatus+"&projectBelongTo="+projectBelongTo;;


    req.open("GET",url,"true");    
    req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    req.send(null);


}

function displayProjectDetailsResult(resText) 
{
    // alert(resText);
    if(resText.length !=0)
    {
        var oTable = document.getElementById("tblPmoProjectList");
       
      ClrDashBordTable(oTable);
      
            
        // var headerFields = new Array("SNo","NAME","ProjectName","ProjectStartDate","Resources","Status","Activity");	
        // var headerFields = new Array("SNo","ProjectName","Account&nbsp;Name","ProjectStartDate","Resources","Status","Pmo","Activity");	
      var headerFields = new Array("SNo","Customer&nbsp;Name","Project&nbsp;Name","Project&nbsp;Type","CostModel","PMO","ActualStartDate","PlannedEndDate","Total&nbsp;Resources","OffShore&nbsp;Main(Billable)","OffShore&nbsp;Main","OffShore&nbsp;Shadow","OffShore&nbsp;Training","OffShore&nbsp;Overhead","Onsite&nbsp;Main(Billable)","Onsite&nbsp;Main","Onsite&nbsp;Shadow","Onsite&nbsp;Training","Onsite&nbsp;Overhead");

               
    
        if(resText!=''){
           
            //   document.getElementById("totalState1").innerHTML = temp[1];
        
            ParseAndGenerateHTML1(oTable,resText, headerFields);
            
               $("#tblPmoProjectList").tableHeadFixer({
        'left' : 4, 
        'foot' : false, 
        'head' : true
    });
     pagerOption1();
        }else{
            alert('No Result For This Search...');
            spnFast.innerHTML="No Result For This Search...";                
        }
        
       
       
    }
    else {
        alert("No Records Found");
    }
}


function generateProjectDetailsReport(tableBody,record,fieldDelimiter){
    var row;
    var cell;
    var fieldLength;
    var state=document.getElementById("state").value;
    var fields = record.split(fieldDelimiter);
    fieldLength = fields.length ;
    var length = fieldLength;
    row = document.createElement( "TR" );
    row.className="gridRowEven";
    tableBody.appendChild( row );
    for (var i=2;i<length;i++) {            
        cell = document.createElement( "TD" );
        cell.className="gridColumn";       
        cell.innerHTML = fields[i];
        if(fields[i]!=0)
        {
            if(parseInt(i,10)==10){
           
                cell.innerHTML = "<a href='javascript:getProjectEmployeeDetails("+fields[0]+",1)'>"+fields[i]+"</a>";
                
            }
            if(parseInt(i,10)==11){
                cell.innerHTML = "<a href='javascript:getProjectEmployeeDetails("+fields[0]+",2)'>"+fields[i]+"</a>";
            }
            if(parseInt(i,10)==12){
                cell.innerHTML = "<a href='javascript:getProjectEmployeeDetails("+fields[0]+",3)'>"+fields[i]+"</a>";
            }
            if(parseInt(i,10)==13){
                cell.innerHTML = "<a href='javascript:getProjectEmployeeDetails("+fields[0]+",4)'>"+fields[i]+"</a>";
            }
            if(parseInt(i,10)==14){
                cell.innerHTML= "<a href='javascript:getProjectEmployeeDetails("+fields[0]+",5)'>"+fields[i]+"</a>";
            }
             if(parseInt(i,10)==15){
                cell.innerHTML = "<a href='javascript:getProjectEmployeeDetails("+fields[0]+",6)'>"+fields[i]+"</a>";
            }
             if(parseInt(i,10)==16){
                cell.innerHTML = "<a href='javascript:getProjectEmployeeDetails("+fields[0]+",7)'>"+fields[i]+"</a>";
            }
             if(parseInt(i,10)==17){
                cell.innerHTML = "<a href='javascript:getProjectEmployeeDetails("+fields[0]+",8)'>"+fields[i]+"</a>";
            }
             if(parseInt(i,10)==18){
                cell.innerHTML = "<a href='javascript:getProjectEmployeeDetails("+fields[0]+",9)'>"+fields[i]+"</a>";
            }
             if(parseInt(i,10)==19){
                cell.innerHTML = "<a href='javascript:getProjectEmployeeDetails("+fields[0]+",10)'>"+fields[i]+"</a>";
            }
             if(parseInt(i,10)==20){
                cell.innerHTML = "<a href='javascript:getProjectEmployeeDetails("+fields[0]+",11)'>"+fields[i]+"</a>";
            }
        }
        else
        {
            cell.innerHTML = fields[i];
        }
        if(fields[i]!=''){
            if(i>=8)
            {
                cell.setAttribute("align","right");
            }
            else
            {
                cell.setAttribute("align","left");     
            }
            row.appendChild( cell );
        }
    }
   
}




function ClrTable(myHTMLTable) { 
    var tbl =  myHTMLTable;
    var lastRow = tbl.rows.length; 
    while (lastRow > 0) { 
        tbl.deleteRow(lastRow - 1);  
        lastRow = tbl.rows.length; 
    } 
}

	
function getProjectEmployeeDetails(proejctId,flag){
    var req = newXMLHttpRequest();
    var empStatus=document.getElementById("empStatus").value;
    req.onreadystatechange =function() {
        if (req.readyState == 4) {
            //  alert("1");
            if (req.status == 200) {    
                //      alert("2");
                document.getElementById("load").style.display = 'none';
                displayProjectEmpDetailsResult(req.responseText);                        
            } 
        }else {
            document.getElementById("load").style.display = 'block';
        // alert("HTTP error ---"+req.status+" : "+req.statusText);
        }
    }; 
    var url = CONTENXT_PATH+"/getProjectEmployeeDetails.action?projectId="+proejctId+"&empStatus="+empStatus+"&flag="+flag;
    req.open("GET",url,"true");
    req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    req.send(null);
}


function displayProjectEmpDetailsResult(response) {
  
    var tableId = document.getElementById("tblProjectEmployeeDetails");
    ClrTable(tableId);
   // var headerFields = new Array("Employee Name","Email","Phone Number","Utilization");
    var headerFields = new Array("Employee Name","Employee No","Email","Phone Number","Utilization");
    
   
    // alert("responseArray[1]-->"+responseArray[1]);
    //document.getElementById("totalRequirementsFound").innerHTML = responseArray[0];
    var dataArray = response;
    
    //  generateTableHeader(tableId,headerFields)
    ParseAndGenerateHTML1(tableId,dataArray, headerFields);
 
 
    document.getElementById("headerLabel1").style.color="white";
    document.getElementById("headerLabel1").innerHTML="Employee Details";
    //document.getElementById("total").innerHTML=total;
    var overlay = document.getElementById('overlayPmoDashBoard');
    var specialBox = document.getElementById('specialBoxPmoDashBoard');
    overlay.style.opacity = .8;
    if(overlay.style.display == "block"){
        overlay.style.display = "none";
        specialBox.style.display = "none";
    }
    else {
        overlay.style.display = "block";
        specialBox.style.display = "block";
    }

    
}

function generateProjectEmpDetailsReport(tableBody,record,fieldDelimiter){
    var row;
    var cell;
    var fieldLength;
    var state=document.getElementById("state").value;
    var fields = record.split(fieldDelimiter);
    fieldLength = fields.length ;
    var length = fieldLength;
    row = document.createElement( "TR" );
    row.className="gridRowEven";
    tableBody.appendChild( row );
    for (var i=0;i<length;i++) {            
        cell = document.createElement( "TD" );
        cell.className="gridColumn";       
        cell.innerHTML = fields[i];  
       
        if(fields[i]!=''){
            if(i==1)
            {
                cell.setAttribute("align","left");
            }
            else
            {
                cell.setAttribute("align","left");     
            }
            row.appendChild( cell );
        }
    }
   
}


function toggleCloseUploadOverlay1() {
    var overlay = document.getElementById('overlayPmoDashBoard');
    var specialBox = document.getElementById('specialBoxPmoDashBoard');

    overlay.style.opacity = .8;
    if(overlay.style.display == "block"){
        overlay.style.display = "none";
        specialBox.style.display = "none";
    }
    else {
        overlay.style.display = "block";
        specialBox.style.display = "block";
    }
//window.location="empSearchAll.action";
}


function getSubPracticeData3(){

    

    var practiceName = document.getElementById("practiceId").value;



    var req = newXMLHttpRequest();

    req.onreadystatechange = readyStateHandler80(req, populateSubPractices);

    var url = CONTENXT_PATH+"/getEmpPractice.action?practiceName="+practiceName;

    req.open("GET",url,"true");    

    req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");

    req.send(null);

    

}







function populateSubPractices(resXML) {
    var subPractice = document.getElementById("subPractice2");
    var practice = resXML.getElementsByTagName("PRACTICE")[0];
    var subPractices = practice.getElementsByTagName("SUBPRACTICE");
    subPractice.innerHTML=" ";

    for(var i=0;i<subPractices.length;i++) {

        var subPracticeName = subPractices[i];

        

        var name = subPracticeName.firstChild.nodeValue;

        var opt = document.createElement("option");

        if(i==0){

            opt.setAttribute("value","All");

        }else{

            opt.setAttribute("value",name);

        }

        opt.appendChild(document.createTextNode(name));

        subPractice.appendChild(opt);

    }

}


 function getEmployeeExpDetails(mss,prev) {
   // alert(mss);
   // alert(prev);
    var background = "#3E93D4";
    var title = "Experience Details:";
   
//    var size = text1.length;
    
    //Now create the HTML code that is required to make the popup
   var content = "<html><head><title>"+title+"</title></head>\
    <body bgcolor='"+background +"' style='color:white;'><br />\
    </body></html>";
    
    //alert("text1"+text1);
    // alert("size "+content.length);
  
    
    popup = window.open("","window","channelmode=0,width=300,height=150,top=200,left=350,scrollbars=no,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no");    
    if(content.indexOf("^")){
        //alert(content.substr(0,content.indexOf("//")));
        popup.document.write("<b>Mss EXP : </b>"+mss);
        popup.document.write("<br><br>");
        popup.document.write("<b>Prev Exp :</b>"+prev);
       popup.document.write(content);
      
    }
    
}

 function getEmployeeContactDetails(empId){
  
    var req = newXMLHttpRequest();
    req.onreadystatechange = readyStateHandlerText(req, populateContactDetails);
    var url = CONTENXT_PATH+"/popupContactsWindow.action?empId="+empId;
    req.open("GET",url,"true");    
    req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    req.send(null);
}


 function populateContactDetails(text) {
	    var background = "#3E93D4";
	    var title = "Contact Details:";
	    var text1 = text;
	    var size = text1.length;

	    //Now create the HTML code that is required to make the popup
	    var content = "<html><head><title>"+title+"</title></head>\
	    <body bgcolor='"+background +"' style='color:white;'>"+text1+"<br />\
	    </body></html>";

	    //alert("text1"+text1);
	    // alert("size "+content.length);
	    var indexof=(content.indexOf("^")+1);
	    var lastindexof=(content.lastIndexOf("^"));

	    var strArray = text.split("^");

	    popup = window.open("","window","channelmode=0,width=300,height=200,top=200,left=350,scrollbars=no,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no");
	    if(content.indexOf("^")){
	    //alert(content.substr(0,content.indexOf("//")));
	    /* popup.document.write("<b>Reports To : </b>"+content.substr((content.lastIndexOf("^")+1),content.length));
	    popup.document.write("<br><br>");
	    popup.document.write("<b>Email Address :</b>"+content.substr(0,content.indexOf("^")));
	    popup.document.write("<br><br>");
	    popup.document.write("<b>Work Phone No :</b>"+content.substr((content.indexOf("^")+1),(lastindexof-indexof)));
	    popup.document.write("<br><br>");*/


	    popup.document.write("<b>Reports To : </b>"+content.substr(0,content.indexOf("^")));
	    popup.document.write("<br><br>");
	    popup.document.write("<b>Location : </b>"+strArray[1]);
	    popup.document.write("<br><br>");
	    popup.document.write("<b>Email: </b>"+strArray[2]);
	    popup.document.write("<br><br>");
	    popup.document.write("<b>Work Phone No : </b>"+strArray[3]);
	    popup.document.write("<br><br>");


	    }

	    }



 function clearTable2(tableId) {
    var tbl =  tableId;
    var lastRow = tbl.rows.length; 
    while (lastRow > 0) { 
        tbl.deleteRow(lastRow - 1);  
        lastRow = tbl.rows.length; 
    } 
}



/*Customer Project details dashboard scipts start
 * Date : 02/09/2017
 * Author : Teja Kadamanti
 */


function geCustomerProjectListDetails()
{
    var oTable = document.getElementById("tblPmoCutomerProjectList");
       
    ClrTable(oTable);
    
     $('span.pagination').empty().remove();
    var custName=document.getElementById("customerName1").value;
    var  ProjectName=document.getElementById("projectName1").value;
    var  status=document.getElementById("status1").value;                                                            
    var ProjectStartDate=document.getElementById("projectStartDate1").value;
    var pmoLoginId = document.getElementById("pmoLoginId1").value;
    var preAssignEmpId = document.getElementById("preAssignEmpId1").value;
    var practiceId = document.getElementById("practiceId2").value;
    var projectType=escape(document.getElementById("ProjectTypeIdRC").value);
    var req = newXMLHttpRequest();
    req.onreadystatechange = function() {
        if (req.readyState == 4) {
            //  alert("1");
            if (req.status == 200) {    
                //      alert("2");
                document.getElementById("pmoCutomerProjectDetailsList").style.display = 'none';
                displayCustomerProjectDetailsResult(req.responseText);     
                
                
            } 
        }else {
            document.getElementById("pmoCutomerProjectDetailsList").style.display = 'block';
        // alert("HTTP error ---"+req.status+" : "+req.statusText);
        }
    }; 
    var url = CONTENXT_PATH+"/customerProjectDetailsDashboard.action?customerName="+custName+"&projectName="+ProjectName+"&projectType="+unescape(projectType)+"&status="+status+"&projectStartDate="+ProjectStartDate+"&pmoLoginId="+pmoLoginId+"&preAssignEmpId="+preAssignEmpId+"&practiceId="+practiceId;


    req.open("GET",url,"true");    
    req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    req.send(null);


}



function displayCustomerProjectDetailsResult(resText) 
{
    
    if(resText.length !=0)
    {
        var oTable = document.getElementById("tblPmoCutomerProjectList");
       
        var headerFields = new Array("SNo","Customer&nbsp;Name","Active&nbsp;Resources","Projects&nbsp;List");

               
      //  alert("test1");
        if(resText!=''){
         //   alert("test2");
            //   document.getElementById("totalState1").innerHTML = temp[1];
        
            ParseAndGenerateHTML1(oTable,resText, headerFields);
              /* pagerOption2();*/
        }else{
            alert('No Result For This Search...');
        // spnFast.innerHTML="No Result For This Search...";                
        }
       
    }
    else {
        alert("No Records Found");
    }
}
function generateCutomerProjectDetailsReport(tableBody,record,fieldDelimiter){
    
    var row;
    var cell;
    var fieldLength;
    var fields = record.split(fieldDelimiter);
    fieldLength = fields.length ;
    var length = fieldLength;
    row = document.createElement( "TR" );
    row.className="gridRowEven";
    tableBody.appendChild( row );
        
    for (var i=0;i<length;i++) { 
           
        cell = document.createElement( "TD" );
        cell.className="gridColumn";   
         cell.innerHTML = fields[i];
       if(fields[i]!=0)
        {
            if(parseInt(i,10)==2){
                cell = document.createElement( "TD" );
                cell.className="gridColumn";    
                cell.innerHTML = "<a href='javascript:getResourceTypeDetailsByCustomer("+fields[3]+")'>"+fields[2]+"</a>";
            }
            
          if(parseInt(i,10)==3){
                cell = document.createElement( "TD" );
                cell.className="gridColumn";    
                cell.innerHTML = "<a href='javascript:getProjectListByCustomer("+fields[3]+")'><img SRC='../includes/images/go_21x21.gif' WIDTH=18 HEIGHT=15 BORDER=0 ALTER='Click to Add'></a>";
            
        }  
        } else
         {
            cell.innerHTML = fields[i];
        }
    
        if(fields[i]!=''){
            if(i==3)
            {
                cell.setAttribute("align","center");
            }else if(i==2){
                 cell.setAttribute("align","right");
            }
            else
            {
                cell.setAttribute("align","left");     
            }
            row.appendChild( cell );
        }
    }
    
    
   
}



function getResourceTypeDetailsByCustomer(accId){
//   
      var req = newXMLHttpRequest();
    var  projectId=document.getElementById("projectName1").value;
    req.onreadystatechange = readyStateHandlerText(req, displayResourceTypeDetailsByCustomer);
    var url = CONTENXT_PATH+"/getResourceTypeDetailsByCustomer.action?accId="+accId+'&projectId='+projectId;
    req.open("GET",url,"true");
    req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    req.send(null);
}


function displayResourceTypeDetailsByCustomer(response) {
    var oTable = document.getElementById("tblResourceTypeDetails");
    ClrTable(oTable);
    var headerFields = new Array("Main","Shadow","Training","Overhead");
    var dataArray = response;
     // ParseAndGenerateHTML1(oTable,dataArray, headerFields);
      var records=dataArray.split("*@!");
      if(records.length>0){
      var data=records[0].split("#^$");
      
      var tbody = oTable.childNodes[0];    
  var tbody = oTable.childNodes[0];    
    tbody = document.createElement("TBODY");
    oTable.appendChild(tbody);
    var row;
    var cell;
    row = document.createElement( "TR" );
    row.className="gridHeader";
    tbody.appendChild( row );
     for (var i=0; i<headerFields.length; i++) {
        cell = document.createElement( "TD" );
        cell.className="gridHeader";
         cell.setAttribute("colspan","2");
           cell.setAttribute("align","center");
        row.appendChild( cell );
        cell.innerHTML = headerFields[i];
        cell.width = 120;
    }
     row = document.createElement( "TR" );
    row.className="gridRowEven";
	tbody.appendChild( row );
	  cell = document.createElement( "TD" );
        cell.className="gridColumn";    
         cell.setAttribute("colspan","2");
		cell.innerHTML = data[0];
		  cell.setAttribute("align","center");
                  
	 row.appendChild( cell );
           cell = document.createElement( "TD" );
        cell.className="gridColumn";    
         cell.setAttribute("colspan","2");
		cell.innerHTML = data[1];
		  cell.setAttribute("align","center");
	 row.appendChild( cell );
          cell = document.createElement( "TD" );
        cell.className="gridColumn";    
         cell.setAttribute("colspan","2");
		cell.innerHTML = data[2];
		  cell.setAttribute("align","center");
                   row.appendChild( cell );
                   cell = document.createElement( "TD" );
        cell.className="gridColumn";    
         cell.setAttribute("colspan","2");
		cell.innerHTML = data[3];
		  cell.setAttribute("align","center");
                  
	 row.appendChild( cell );
         headerFields = new Array("OnSite","OffShore","OnSite","OffShore","OnSite","OffShore","OnSite","OffShore");
         row = document.createElement( "TR" );
    row.className="gridHeader";
      cell.setAttribute("align","center");
    tbody.appendChild( row );
     for (var i=0; i<headerFields.length; i++) {
        cell = document.createElement( "TD" );
        cell.className="gridHeader";
        
        row.appendChild( cell );
        cell.innerHTML = headerFields[i];
        cell.width = 120;
    }
      row = document.createElement( "TR" );
    row.className="gridRowEven";
	tbody.appendChild( row );
	  cell = document.createElement( "TD" );
        cell.className="gridColumn";    
		cell.innerHTML = data[4];
		  cell.setAttribute("align","right");
	 row.appendChild( cell );
           cell = document.createElement( "TD" );
        cell.className="gridColumn";    
		cell.innerHTML = data[5];
		  cell.setAttribute("align","right");
	 row.appendChild( cell );
          cell = document.createElement( "TD" );
        cell.className="gridColumn";    
		cell.innerHTML = data[6];
		  cell.setAttribute("align","right");
	 row.appendChild( cell );
          cell = document.createElement( "TD" );
        cell.className="gridColumn";    
		cell.innerHTML = data[7];
		  cell.setAttribute("align","right");
	 row.appendChild( cell );
         
           cell = document.createElement( "TD" );
        cell.className="gridColumn";    
		cell.innerHTML = data[8];
		  cell.setAttribute("align","right");
	 row.appendChild( cell );
         
           cell = document.createElement( "TD" );
        cell.className="gridColumn";    
		cell.innerHTML = data[9];
		  cell.setAttribute("align","right");
	 row.appendChild( cell );
         
           cell = document.createElement( "TD" );
        cell.className="gridColumn";    
		cell.innerHTML = data[10];
		  cell.setAttribute("align","right");
	 row.appendChild( cell );
         
           cell = document.createElement( "TD" );
        cell.className="gridColumn";    
		cell.innerHTML = data[11];
		  cell.setAttribute("align","right");
	 row.appendChild( cell );
         
           var footer =document.createElement("TR");
    footer.className="gridPager";
    tbody.appendChild(footer);
    cell = document.createElement("TD");
    cell.className="gridFooter";
     cell.setAttribute("align","right");
      cell.innerHTML ="Total&nbsp;Resource&nbsp;:"+ data[12];
      cell.colSpan = "8";
      footer.appendChild(cell);
      }
    document.getElementById("headerLabel3").style.color="white";
    document.getElementById("headerLabel3").innerHTML="Resource Details";
            
    var overlay = document.getElementById('overlayResourceType');
    var specialBox = document.getElementById('specialBoxResourceType');
    overlay.style.opacity = .8;
    if(overlay.style.display == "block"){
        overlay.style.display = "none";
        specialBox.style.display = "none";
    }
    else {
        overlay.style.display = "block";
        specialBox.style.display = "block";
    }

    
}


function toggleCloseUploadOverlay2() {
    var overlay = document.getElementById('overlayResourceType');
    var specialBox = document.getElementById('specialBoxResourceType');

    overlay.style.opacity = .8;
    if(overlay.style.display == "block"){
        overlay.style.display = "none";
        specialBox.style.display = "none";
    }
    else {
        overlay.style.display = "block";
        specialBox.style.display = "block";
    }
//window.location="empSearchAll.action";
}





function generatetblResourceTypeDetails(tableBody,record,fieldDelimiter){
     var row;
    var cell;
    var fieldLength;
    var fields = record.split(fieldDelimiter);
    fieldLength = fields.length ;
    var length = fieldLength;
    row = document.createElement( "TR" );
    row.className="gridRowEven";
    tableBody.appendChild( row );
        
    for (var i=0;i<length;i++) { 
           
        cell = document.createElement( "TD" );
        cell.className="gridColumn";   
         cell.innerHTML = fields[i];
        
      if(fields[i]!=''){
            if(i==1)
            {
                cell.setAttribute("align","left");
            }
            else
            {
                cell.setAttribute("align","left");     
            }
            row.appendChild( cell );
        }
    }
    
    
   
}

function getProjectListByCustomer(accId){
    
      var  status=document.getElementById("status1").value;                                                            
    var ProjectStartDate=document.getElementById("projectStartDate1").value;
    var pmoLoginId = document.getElementById("pmoLoginId1").value;
    var practiceId = document.getElementById("practiceId2").value;
    var  ProjectName=document.getElementById("projectName1").value;
    var preAssignEmpId = document.getElementById("preAssignEmpId1").value;
//      
      var req = newXMLHttpRequest();
   
    req.onreadystatechange = readyStateHandlerText(req, displayProjectListByCustomer);
    var url = CONTENXT_PATH+"/getProjectListByCustomer.action?accId="+accId+"&projectName="+ProjectName+"&status="+status+"&projectStartDate="+ProjectStartDate+"&pmoLoginId="+pmoLoginId+"&practiceId="+practiceId+"&preAssignEmpId="+preAssignEmpId;
    req.open("GET",url,"true");
    req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    req.send(null);
}


function displayProjectListByCustomer(response) {
    var tableId = document.getElementById("tblProjectListDetails");
    ClrTable(tableId);
    var headerFields = new Array("Name","StartDate","Status","Practice","ProjectType","ActiveResources");
    var dataArray = response;
    ParseAndGenerateHTML1(tableId,dataArray, headerFields);
 
 
    document.getElementById("headerLabel2").style.color="white";
    document.getElementById("headerLabel2").innerHTML="Project Details";
            
    var overlay = document.getElementById('overlayProjectList');
    var specialBox = document.getElementById('specialBoxProjectList');
    overlay.style.opacity = .8;
    if(overlay.style.display == "block"){
        overlay.style.display = "none";
        specialBox.style.display = "none";
    }
    else {
        overlay.style.display = "block";
        specialBox.style.display = "block";
    }

    
}



function toggleCloseUploadOverlay3() {
    var overlay = document.getElementById('overlayProjectList');
    var specialBox = document.getElementById('specialBoxProjectList');

    overlay.style.opacity = .8;
    if(overlay.style.display == "block"){
        overlay.style.display = "none";
        specialBox.style.display = "none";
    }
    else {
        overlay.style.display = "block";
        specialBox.style.display = "block";
    }
//window.location="empSearchAll.action";
}





function generateProjectListDetails(tableBody,record,fieldDelimiter){
     var row;
    var cell;
    var fieldLength;
    var fields = record.split(fieldDelimiter);
    fieldLength = fields.length ;
    var length = fieldLength;
    row = document.createElement( "TR" );
    row.className="gridRowEven";
    tableBody.appendChild( row );
        
    for (var i=0;i<length-2;i++) { 
        
        cell = document.createElement( "TD" );
        cell.className="gridColumn";   
         cell.innerHTML = fields[i];

         if(fields[i]!=0)
         {
             if(parseInt(i,10)==5){
                 cell = document.createElement( "TD" );
                 cell.className="gridColumn";    
                 cell.innerHTML = "<a href='javascript:getResourceTypeDetailsByProject(\""+fields[6]+"\",\""+fields[7]+"\")'>"+fields[5]+"</a>";
             }
             
           
         } else
          {
             cell.innerHTML = fields[i];
         }
      if(fields[i]!=''){
    	  
    	  
            if(i==1)
            {
                cell.setAttribute("align","left");
            }
            if(i==6)
            {
                cell.setAttribute("align","center");
            }
            else
            {
                cell.setAttribute("align","left");     
            }
            row.appendChild( cell );
        }
    }
    
  
   
}



function EmployeeForProjectDetails1() {
    var test=document.getElementById("assignedToUID1").value;
   
    if (test == "") {

        clearTable1();
        hideScrollBar();
        var validationMessage=document.getElementById("authorEmpValidationMessage");
        validationMessage.innerHTML = "";
        document.frmSearch.preAssignEmpId.value="";
    //frmSearch issuesForm
    } else {
        if (test.length >2) {
            //alert("CONTENXT_PATH-- >"+CONTENXT_PATH)
            var url = CONTENXT_PATH+"/getEmployeeDetailforPMOActivity.action?customerName="+escape(test);
            var req = initRequest(url);
            req.onreadystatechange = function() {
                //    alert("req-->"+req);
                if (req.readyState == 4) {
                    if (req.status == 200) {
                        // alert(req.responseXML);
                    
                        parseEmpMessagesForProjectDeatils1(req.responseXML);
                    } else if (req.status == 204){
                        clearTable1();
                    }
                }
            };
            req.open("GET", url, true);

            req.send(null);
        }
    }
}

function parseEmpMessagesForProjectDeatils1(responseXML) {
    //alert("-->"+responseXML);
    autorow1 = document.getElementById("menu-popup");
    autorow1.style.display ="none";
    autorow = document.getElementById("menu-popup");
    autorow.style.display ="none";
    clearTable1();
    var employees = responseXML.getElementsByTagName("EMPLOYEES")[0];
    if (employees.childNodes.length > 0) {        
        completeTable.setAttribute("bordercolor", "black");
        completeTable.setAttribute("border", "0");        
    } else {
        clearTable1();
    }
    if(employees.childNodes.length<10) {
        autorow1.style.overflowY = "hidden";
        autorow.style.overflowY = "hidden";        
    }
    else {    
        autorow1.style.overflowY = "scroll";
        autorow.style.overflowY = "scroll";
    }
    
    var employee = employees.childNodes[0];
    var chk=employee.getElementsByTagName("VALID")[0];
    if(chk.childNodes[0].nodeValue =="true") {
        //var validationMessage=document.getElementById("validationMessage");
        var validationMessage;
        
        validationMessage=document.getElementById("authorEmpValidationMessage1");
        isPriEmpExist = true;
         
        validationMessage.innerHTML = "";
        document.getElementById("menu-popup").style.display = "block";
        for (loop = 0; loop < employees.childNodes.length; loop++) {
            
            var employee = employees.childNodes[loop];
            var customerName = employee.getElementsByTagName("NAME")[0];
            var empid = employee.getElementsByTagName("EMPID")[0];
            appendEmployeeForProjectDetails1(empid.childNodes[0].nodeValue,customerName.childNodes[0].nodeValue);
        }
        var position;
        
        
        position = findPosition(document.getElementById("assignedToUID1"));
        
        posi = position.split(",");
        document.getElementById("menu-popup").style.left = posi[0]+"px";
        document.getElementById("menu-popup").style.top = (parseInt(posi[1])+20)+"px";
        document.getElementById("menu-popup").style.display = "block";
    } //if
    if(chk.childNodes[0].nodeValue =="false") {
        var validationMessage = '';
      
        isPriEmpExist = false;
        validationMessage=document.getElementById("authorEmpValidationMessage");
    
 
        validationMessage.innerHTML = " Invalid ! Select from Suggesstion List. ";
        validationMessage.style.color = "green";
        validationMessage.style.fontSize = "12px";
       
        document.getElementById("assignedToUID1").value = "";
        document.getElementById("preAssignEmpId1").value = "";
            
        
    }
}
function appendEmployeeForProjectDetails1(empId,empName) {
    
    var row;
    var nameCell;
    if (!isIE) {
        row = completeTable.insertRow(completeTable.rows.length);
        nameCell = row.insertCell(0);
    } else {
        row = document.createElement("tr");
        nameCell = document.createElement("td");
        row.appendChild(nameCell);
        completeTable.appendChild(row);
    }
    row.className = "popupRow";
    nameCell.setAttribute("bgcolor", "#3E93D4");
    var linkElement = document.createElement("a");
    linkElement.className = "popupItem";
    // if(assignedToType=='pre'){
    linkElement.setAttribute("href", "javascript:set_emp1('"+empName +"','"+ empId +"')");

    linkElement.appendChild(document.createTextNode(empName));
    linkElement["onclick"] = new Function("hideScrollBar()");
    nameCell.appendChild(linkElement);
//fillWorkPhone(empId);
}
function set_emp1(eName,eID){
    clearTable1();
    document.frmSearch.assignedToUID1.value =eName;
    document.frmSearch.preAssignEmpId1.value =eID;
}
function getProjectsByAccountId(){
 
    var accountId = document.getElementById("customerName").value;
 if(accountId!=""){
    var req = newXMLHttpRequest();
    req.onreadystatechange = readyStateHandler80(req, populateProjectsList);
    var url = CONTENXT_PATH+"/getProjectsByAccountId.action?accountId="+accountId;
    req.open("GET",url,"true");
    req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    req.send(null);   
}else{
     var projects = document.getElementById("projectName");
      projects.innerHTML=" ";
      
        var opt = document.createElement("option");
        opt.setAttribute("value","");
        opt.appendChild(document.createTextNode("All"));
         projects.appendChild(opt);
}
}
function populateProjectsList(resXML) {
  
    //var projects = document.getElementById("projectName");
    var projects = document.getElementById("projectName");
 //  alert("test")
    
    var projectsList = resXML.getElementsByTagName("PROJECTS")[0];
   
    var users = projectsList.getElementsByTagName("USER");
    projects.innerHTML=" ";
    for(var i=0;i<users.length;i++) {
        var userName = users[i];
        var att = userName.getAttribute("projectId");
        var name = userName.firstChild.nodeValue;
        var opt = document.createElement("option");
        opt.setAttribute("value",att);
        opt.appendChild(document.createTextNode(name));
        projects.appendChild(opt);
    }
}

function getProjectsByAccountIdForResourceCount(){
 
    var accountId = document.getElementById("customerName1").value;
 if(accountId!=""){
    var req = newXMLHttpRequest();
    req.onreadystatechange = readyStateHandler80(req, populateProjectsListForResourceCount);
    var url = CONTENXT_PATH+"/getProjectsByAccountId.action?accountId="+accountId;
    req.open("GET",url,"true");
    req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    req.send(null);   
}else{
     var projects = document.getElementById("projectName1");
      projects.innerHTML=" ";
      
        var opt = document.createElement("option");
        opt.setAttribute("value","");
        opt.appendChild(document.createTextNode("All"));
         projects.appendChild(opt);
}
}
function populateProjectsListForResourceCount(resXML) {
  
    //var projects = document.getElementById("projectName");
    var projects = document.getElementById("projectName1");
 //  alert("test")
    
    var projectsList = resXML.getElementsByTagName("PROJECTS")[0];
   
    var users = projectsList.getElementsByTagName("USER");
    projects.innerHTML=" ";
    for(var i=0;i<users.length;i++) {
        var userName = users[i];
        var att = userName.getAttribute("projectId");
        var name = userName.firstChild.nodeValue;
        var opt = document.createElement("option");
        opt.setAttribute("value",att);
        opt.appendChild(document.createTextNode(name));
        projects.appendChild(opt);
    }
}




function generateTableDynamicHeader(tableBody,headerFields) {
    var row;
    var cell;
    
    var thead=document.createElement("thead");
    tableBody.appendChild(thead);
    row = document.createElement( "TR" );
    row.className="dashBoardgridHeader";
    thead.appendChild(row);
    
    for (var i=0; i<headerFields.length; i++) {
        cell = document.createElement( "TH" );
        cell.className="dashBoardgridHeader";
        row.appendChild( cell );
        cell.innerHTML = headerFields[i];
        cell.width = 120;
    }
}


function ClrDashBordTable(myHTMLTable) { 
    var tbl =  myHTMLTable;
    
tbl.deleteTHead();
    var lastRow = tbl.rows.length; 
    while (lastRow > 0) { 
        tbl.deleteRow(lastRow - 1);  
        lastRow = tbl.rows.length; 
    } 
}



function getMultipleProjectsEmployeeList(){
   var preAssignEmpId = document.getElementById("preAssignEmpId2").value;
   var departmentId = document.getElementById("departmentIdEmpPrj").value;
   var practiceId = document.getElementById("practiceIdEmpPrj").value;
   var subPracticeId = document.getElementById("subPracticeEmpPrj").value;
   if(departmentId.trim()==""){
        departmentId="-1";
    }
    if(practiceId=="-1" || practiceId.trim()=="" || practiceId.trim()=="--Please Select--")
    {
        practiceId="-1";
    }
    if(subPracticeId=="-1" || subPracticeId=="All")
    {
        subPracticeId="-1";
    }
    var req = newXMLHttpRequest();
    req.onreadystatechange = function() {
        if (req.readyState == 4) {
            //  alert("1");
            if (req.status == 200) {    
                //      alert("2");
                document.getElementById("multipleProjectEmployeeDetailsLoad").style.display = 'none';
                displayMultipleProjectsEmpList(req.responseText);
                
            } 
        }else {
            document.getElementById("multipleProjectEmployeeDetailsLoad").style.display = 'block';
        // alert("HTTP error ---"+req.status+" : "+req.statusText);
        }
    }; 
  //  var url = CONTENXT_PATH+"/multipleProjectsEmployeeList.action?preAssignEmpId="+preAssignEmpId;
 var url = CONTENXT_PATH+"/multipleProjectsEmployeeList.action?preAssignEmpId="+preAssignEmpId+"&departmentId="+departmentId+"&practiceId="+practiceId+"&subPracticeId="+subPracticeId;
    req.open("GET",url,"true");    
    req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    req.send(null);

}





function displayMultipleProjectsEmpList(resText) 
{
    
    if(resText.length !=0)
    {
        var oTable = document.getElementById("tblMultipleProjectEmployeeList");
        ClrTable(oTable);
        // var headerFields = new Array("SNo","Employee&nbsp;Name","Department","Practice","Projects");
        //   var headerFields = new Array("SNo","Employee&nbsp;Name","Department","Practice","SubPractice","Projects");
        var headerFields = new Array("SNo","Employee&nbsp;Name","Employee No","Department","Practice","SubPractice","Projects");
      
      //  alert("test1");
        if(resText!=''){
         //   alert("test2");
            //   document.getElementById("totalState1").innerHTML = temp[1];
        
            ParseAndGenerateHTML1(oTable,resText, headerFields);
             /*  pagerOption3();*/
        }else{
            alert('No Result For This Search...');
        // spnFast.innerHTML="No Result For This Search...";                
        }
       
    }
    else {
        alert("No Records Found");
    }
}
function generateMultipleProjectsEmployeeList(index,tableBody,record,fieldDelimiter){
    
    var row;
    var cell;
    var fieldLength;
    var fields = record.split(fieldDelimiter);
    fieldLength = fields.length ;
    var length = fieldLength;
    row = document.createElement( "TR" );
    row.className="gridRowEven";
    tableBody.appendChild( row );
    
          cell = document.createElement( "TD" );
          cell.className="gridColumn";   
          cell.innerHTML = index+1;
          row.appendChild(cell);
           
    for (var i=1;i<length-1;i++) { 
           
        cell = document.createElement( "TD" );
        cell.className="gridColumn";   
        cell.innerHTML = fields[i];
        row.appendChild(cell);
     
    }
    
     cell = document.createElement( "TD" );
        cell.className="gridColumn";   
    //   cell.innerHTML = "<a href='javascript:getMultipleProjectsList("+fields[0]+")'>"+fields[4]+"</a>";
           cell.innerHTML = "<a href='javascript:getMultipleProjectsList("+fields[0]+")'>"+fields[6]+"</a>";
     

        row.appendChild(cell);
    
   
}


function getMultipleProjectsList(preAssignEmpId){
   
     var req = newXMLHttpRequest();
    req.onreadystatechange = function() {
        if (req.readyState == 4) {
            //  alert("1");
            if (req.status == 200) {    
                //      alert("2");
                setMultipleProjectsList(req.responseText);
                
            } 
        }else {
          // alert("HTTP error ---"+req.status+" : "+req.statusText);
        }
    }; 
    var url = CONTENXT_PATH+"/multipleProjectsEmployeeListDetails.action?preAssignEmpId="+preAssignEmpId;

    req.open("GET",url,"true");    
    req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    req.send(null);
}

function setMultipleProjectsList(response) {
   var tableId = document.getElementById("tblMultipleProjectListDetails");
    ClrTable(tableId);
    var headerFields = new Array("S.No","Customer&nbsp;Name","Project&nbsp;Name","StartDate","EndDate","EmpProjStatus","Billable","Utilization","Project Type");

    var dataArray ;
    
     dataArray = response.split("addTo");
             
     ParseAndGenerateHTML1(tableId,dataArray[0], headerFields);
 
 
    document.getElementById("headerLabel4").style.color="white";
    document.getElementById("headerLabel4").innerHTML="Employee Project Details";
            
    var overlay = document.getElementById('overlayMultipleProjectList');
    var specialBox = document.getElementById('specialBoxMultipleProjectList');
    overlay.style.opacity = .9;
    if(overlay.style.display == "block"){
        overlay.style.display = "none";
        specialBox.style.display = "none";
    }
    else {
        overlay.style.display = "block";
        specialBox.style.display = "block";
    }
    
    
  var  tbody = tableId.childNodes[0];    
    tbody = document.createElement("TBODY");
    tableId.appendChild(tbody);
    var cell;
    var footer =document.createElement("TR");
    footer.className="gridPager";
    tbody.appendChild(footer);
    cell = document.createElement("TD");
    cell.className="gridFooter";
     cell.setAttribute("align","right");
      cell.innerHTML ="Total&nbsp;Utilization&nbsp;:"+ dataArray[1];
      cell.colSpan = "9";
      footer.appendChild(cell);

    
}

function toggleCloseUploadOverlayMultipleProjects() {
    var overlay = document.getElementById('overlayMultipleProjectList');
    var specialBox = document.getElementById('specialBoxMultipleProjectList');

    overlay.style.opacity = .9;
    if(overlay.style.display == "block"){
        overlay.style.display = "none";
        specialBox.style.display = "none";
    }
    else {
        overlay.style.display = "block";
        specialBox.style.display = "block";
    }
//window.location="empSearchAll.action";
}



function generateMultipleProjectsEmployeeListDetails(index,tableBody,record,fieldDelimiter){
    
    var row;
    var cell;
    var fieldLength;
    var fields = record.split(fieldDelimiter);
    fieldLength = fields.length ;
    var length = fieldLength;
    row = document.createElement( "TR" );
    row.className="gridRowEven";
    tableBody.appendChild( row );
    
          cell = document.createElement( "TD" );
          cell.className="gridColumn";   
          cell.innerHTML = index+1;
          row.appendChild(cell);
           
    for (var i=0;i<length;i++) { 
           
        cell = document.createElement( "TD" );
        cell.className="gridColumn";   
        cell.innerHTML = fields[i];
        row.appendChild(cell);
     
    }
     
    
   
}





function EmployeeMultipleProjectDetails() {
    var test=document.getElementById("assignedToUID2").value;
   
    if (test == "") {

        clearTable1();
        hideScrollBar();
        var validationMessage=document.getElementById("authorEmpValidationMessage2");
        validationMessage.innerHTML = "";
        document.frmSearch.preAssignEmpId.value="";
    //frmSearch issuesForm
    } else {
        if (test.length >2) {
            //alert("CONTENXT_PATH-- >"+CONTENXT_PATH)
            var url = CONTENXT_PATH+"/getEmployeeDetailforPMOActivity.action?customerName="+escape(test);
            var req = initRequest(url);
            req.onreadystatechange = function() {
                //    alert("req-->"+req);
                if (req.readyState == 4) {
                    if (req.status == 200) {
                        // alert(req.responseXML);
                    
                        parseEmpMessagesForMultipleProjectDeatils(req.responseXML);
                    } else if (req.status == 204){
                        clearTable1();
                    }
                }
            };
            req.open("GET", url, true);

            req.send(null);
        }
    }
}

function parseEmpMessagesForMultipleProjectDeatils(responseXML) {
    //alert("-->"+responseXML);
    autorow1 = document.getElementById("menu-popup");
    autorow1.style.display ="none";
    autorow = document.getElementById("menu-popup");
    autorow.style.display ="none";
    clearTable1();
    var employees = responseXML.getElementsByTagName("EMPLOYEES")[0];
    if (employees.childNodes.length > 0) {        
        completeTable.setAttribute("bordercolor", "black");
        completeTable.setAttribute("border", "0");        
    } else {
        clearTable1();
    }
    if(employees.childNodes.length<10) {
        autorow1.style.overflowY = "hidden";
        autorow.style.overflowY = "hidden";        
    }
    else {    
        autorow1.style.overflowY = "scroll";
        autorow.style.overflowY = "scroll";
    }
    
    var employee = employees.childNodes[0];
    var chk=employee.getElementsByTagName("VALID")[0];
    if(chk.childNodes[0].nodeValue =="true") {
        //var validationMessage=document.getElementById("validationMessage");
        var validationMessage;
        
        validationMessage=document.getElementById("authorEmpValidationMessage2");
        isPriEmpExist = true;
         
        validationMessage.innerHTML = "";
        document.getElementById("menu-popup").style.display = "block";
        for (loop = 0; loop < employees.childNodes.length; loop++) {
            
            var employee = employees.childNodes[loop];
            var customerName = employee.getElementsByTagName("NAME")[0];
            var empid = employee.getElementsByTagName("EMPID")[0];
            appendEmployeeForMultipleProjectDetails(empid.childNodes[0].nodeValue,customerName.childNodes[0].nodeValue);
        }
        var position;
        
        
        position = findPosition(document.getElementById("assignedToUID2"));
        
        posi = position.split(",");
        document.getElementById("menu-popup").style.left = posi[0]+"px";
        document.getElementById("menu-popup").style.top = (parseInt(posi[1])+20)+"px";
        document.getElementById("menu-popup").style.display = "block";
    } //if
    if(chk.childNodes[0].nodeValue =="false") {
        var validationMessage = '';
      
        isPriEmpExist = false;
        validationMessage=document.getElementById("authorEmpValidationMessage2");
    
 
        validationMessage.innerHTML = " Invalid ! Select from Suggesstion List. ";
        validationMessage.style.color = "green";
        validationMessage.style.fontSize = "12px";
       
        document.getElementById("assignedToUID2").value = "";
        document.getElementById("preAssignEmpId2").value = "";
            
        
    }
}
function appendEmployeeForMultipleProjectDetails(empId,empName) {
    
    var row;
    var nameCell;
    if (!isIE) {
        row = completeTable.insertRow(completeTable.rows.length);
        nameCell = row.insertCell(0);
    } else {
        row = document.createElement("tr");
        nameCell = document.createElement("td");
        row.appendChild(nameCell);
        completeTable.appendChild(row);
    }
    row.className = "popupRow";
    nameCell.setAttribute("bgcolor", "#3E93D4");
    var linkElement = document.createElement("a");
    linkElement.className = "popupItem";
    // if(assignedToType=='pre'){
    linkElement.setAttribute("href", "javascript:set_empMultipleProjects('"+empName +"','"+ empId +"')");

    linkElement.appendChild(document.createTextNode(empName));
    linkElement["onclick"] = new Function("hideScrollBar()");
    nameCell.appendChild(linkElement);
//fillWorkPhone(empId);
}


function set_empMultipleProjects(eName,eID){
    clearTable1();
    document.empSearch.assignedToUID2.value =eName;
    document.empSearch.preAssignEmpId2.value =eID;
}


/*Author : Tirumala Teja kadamanti
 *
 *Date :04/24/2017
 * Populating Practice and Subpractice
 * 
 */


function getPracticeDataForMultipleProjects() {
  
    var departmentName=document.getElementById("departmentIdEmpPrj").value;
    var req = newXMLHttpRequest();
    req.onreadystatechange = readyStateHandler80(req, populatePracticeForResource);
    var url = CONTENXT_PATH+"/getEmpDepartment.action?departmentName="+departmentName;
    req.open("GET",url,"true");    
    req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    req.send(null);
    
}

function populatePracticeForResource(resXML) {    
   
    var practiceId = document.getElementById("practiceIdEmpPrj");
    var department = resXML.getElementsByTagName("DEPARTMENT")[0];
    var practices = department.getElementsByTagName("PRACTICE");
    practiceId.innerHTML=" ";
    
    for(var i=0;i<practices.length;i++) {
        var practiceName = practices[i];
        var name = practiceName.firstChild.nodeValue;
        var opt = document.createElement("option");
        if(i==1){
            opt.setAttribute("value","All");
        }else{
            opt.setAttribute("value",name);
        }
        opt.appendChild(document.createTextNode(name));
        practiceId.appendChild(opt);
    }
}



function getSubPracticeDataForMultipleProjects(){

    

    var practiceName = document.getElementById("practiceIdEmpPrj").value;



    var req = newXMLHttpRequest();

    req.onreadystatechange = readyStateHandler80(req, populateSubPracticesResources);

    var url = CONTENXT_PATH+"/getEmpPractice.action?practiceName="+practiceName;

    req.open("GET",url,"true");    

    req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");

    req.send(null);

    

}



function populateSubPracticesResources(resXML) {
    var subPractice = document.getElementById("subPracticeEmpPrj");
    var practice = resXML.getElementsByTagName("PRACTICE")[0];
    var subPractices = practice.getElementsByTagName("SUBPRACTICE");
    subPractice.innerHTML=" ";

    for(var i=0;i<subPractices.length;i++) {

        var subPracticeName = subPractices[i];

        

        var name = subPracticeName.firstChild.nodeValue;

        var opt = document.createElement("option");

        if(i==0){

            opt.setAttribute("value","All");

        }else{

            opt.setAttribute("value",name);

        }

        opt.appendChild(document.createTextNode(name));

        subPractice.appendChild(opt);

    }
}



function getAvaiableEmployeBySubPracticeList(){
    document.getElementById("avaiableEmployeBySubPracticeLoad").style.display ='block';
      var projectName = document.getElementById("practiceName").value;
        var tableId = document.getElementById("tblAvaiableEmployeBySubPractice"); 
    clearTableAvaiableEmployeBySubPracticeList(tableId);
 //   alert("nn")
     var req = newXMLHttpRequest();
    req.onreadystatechange = function() {
        if (req.readyState == 4) {
            if (req.status == 200) {      
                document.getElementById("avaiableEmployeBySubPracticeLoad").style.display = 'none';
                displayAvaiableEmployeBySubPracticeList(req.responseText);       
              //  pagerOption4();
            } else{
             document.getElementById("avaiableEmployeBySubPracticeLoad").style.display = 'none';    
            }
        }
    };  
    var url = CONTENXT_PATH+"/avaiableEmployeBySubPracticeList.action?practiceId="+projectName;
///alert("url"+url)
    req.open("GET",url,"true");    
    req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    req.send(null);
}
function displayAvaiableEmployeBySubPracticeList(response){
  //alert(response);
    var tableId = document.getElementById("tblAvaiableEmployeBySubPractice"); 
    var headerFields = new Array("S.No","PracticeName","Technology","Shadow&nbsp;Count","Available&nbsp;Count","Project&nbsp;Training&nbsp;Count");
    var dataArray = response;
    if(response!='none')
    avaiableEmployeBySubPracticeParseAndGenerateHTML(tableId,dataArray, headerFields);
else {
    var tbody = tableId.childNodes[0];    
    tbody = document.createElement("TBODY");
    tableId.appendChild(tbody);
	 generateTableHeader(tbody,headerFields);
	generateNoRecords(tbody,tableId);
         generateFooter(tbody,tableId);
}
}
 
function avaiableEmployeBySubPracticeParseAndGenerateHTML(oTable,responseString,headerFields){
    var fieldDelimiter = "#^$";
    var recordDelimiter = "*@!";   
    var records = responseString.split(recordDelimiter); 
    generateAvaiableEmployeBySubPractice(oTable,headerFields,records,fieldDelimiter);
}

function generateAvaiableEmployeBySubPractice(oTable, headerFields,records,fieldDelimiter) {
    var tbody = oTable.childNodes[0];    
    tbody = document.createElement("TBODY");
    oTable.appendChild(tbody);
    generateTableHeader(tbody,headerFields);
    var rowlength;
    rowlength = records.length;
 
    if(rowlength >0 && records!=""){
        for(var i=0;i<rowlength-1;i++) {
            generateAvaiableEmployeBySubPracticeRow(oTable,tbody,records[i],fieldDelimiter,i);
        }
        
    }
    else {
        generateNoRecords(tbody,oTable);
    }
    generateFooter(tbody,oTable);
}


function generateAvaiableEmployeBySubPracticeRow(oTable,tableBody,record,delimiter,index){
    var row;
    var cell;
    var fieldLength;
    var fields = record.split("#^$");
    fieldLength = fields.length ;
    var length = fieldLength;
    
    row = document.createElement( "TR" );
    row.className="gridRowEven";
    tableBody.appendChild( row );
    
    for (var i=0;i<=length;i++) { 
        if (i==0){
            
            cell = document.createElement( "TD" );
            cell.className="gridColumn";    
          
            cell.innerHTML =index+1 ;
           // cell.setAttribute("align","center");   
            row.appendChild( cell ); 
        
       
        }else if(i==3 && fields[2]>0){
            cell = document.createElement( "TD" );
            cell.className="gridColumn"; 
            var j = document.createElement("a");
        cell.innerHTML = "<a href='javascript:getOffshoreDetails(\""+fields[0]+"\",\""+fields[1]+"\",\"Shadow\")'>"+fields[2]+"</a>";
     
            cell.appendChild(j);    
           }else if(i==4 && fields[3]>0){
               cell = document.createElement( "TD" );
            cell.className="gridColumn"; 
            var j = document.createElement("a");
            cell.innerHTML = "<a href='javascript:getOffshoreDetails(\""+fields[0]+"\",\""+fields[1]+"\",\"Available\")'>"+fields[3]+"</a>";
            cell.appendChild(j);    
           }else if(i==5 && fields[4]>0){
               cell = document.createElement( "TD" );
            cell.className="gridColumn"; 
            var j = document.createElement("a");
             cell.innerHTML = "<a href='javascript:getOffshoreDetails(\""+fields[0]+"\",\""+fields[1]+"\",\"ProjectTraining\")'>"+fields[4]+"</a>";
      
      
            cell.appendChild(j);    
           }
      
        else{
            cell = document.createElement( "TD" );
            cell.className="gridColumn";       
       
           // cell.setAttribute("align","center");
            
            cell.innerHTML = fields[i-1]; 
       }
            
     if(fields[i]!=''){
            if(i==1 || i==2)
            {
                cell.setAttribute("align","left");
            }
            else
            {
                cell.setAttribute("align","center");     
            }
            row.appendChild( cell );
        }
    }   
    row.appendChild( cell );
    
}

function clearTableAvaiableEmployeBySubPracticeList(tableId) {
    var tbl =  tableId;
    var lastRow = tbl.rows.length; 
    while (lastRow > 0) { 
        tbl.deleteRow(lastRow - 1);  
        lastRow = tbl.rows.length; 
    } 
}



function getOffshoreDetails(practice,subPractice,status){
 var tableId = document.getElementById("tblOffshoreEmployeeDetails");
    ClrTable(tableId);
      var req = newXMLHttpRequest();
    req.onreadystatechange = function() {
        if (req.readyState == 4) {
            if (req.status == 200) {    
                setOffshoreEmployeeList(req.responseText,practice,subPractice,status);
                
            } 
        }else {
          // alert("HTTP error ---"+req.status+" : "+req.statusText);
        }
    }; 
    var url = CONTENXT_PATH+"/getOffshoreEmployeeDetails.action?practiceId="+practice+"&subPracticeId="+subPractice+"&status="+status;

    req.open("GET",url,"true");    
    req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    req.send(null);
}

function setOffshoreEmployeeList(response,practice,subPractice,status){
    
   var tableId = document.getElementById("tblOffshoreEmployeeDetails");
    ClrTable(tableId);
 //  var headerFields = new Array("S.No","Employee&nbsp;Name","Department","StartDate","ReportsTo","Email","WorkPhone");
    var headerFields = new Array("S.No","Employee&nbsp;Name","Employee No","Department","StartDate","ReportsTo","Email","WorkPhone");
   var dataArray =response;
    
     //dataArray = response.split("addTo");
             
     ParseAndGenerateHTML1(tableId,dataArray, headerFields);
 
 
    document.getElementById("headerLabel5").style.color="white";
    document.getElementById("headerLabel5").innerHTML="Employee Details";
             document.getElementById("practiceLabel").innerHTML=practice;
              document.getElementById("subPracticeLabel").innerHTML=subPractice;
              document.getElementById("statusLabel").innerHTML=status;
    var overlay = document.getElementById('overlayOffshoreList');
    var specialBox = document.getElementById('specialBoxOffshoreList');
    overlay.style.opacity = .8;
    if(overlay.style.display == "block"){
        overlay.style.display = "none";
        specialBox.style.display = "none";
    }
    else {
        overlay.style.display = "block";
        specialBox.style.display = "block";
    }
    


 
}

function toggleCloseUploadOverlayOffshoreList() {
    var overlay = document.getElementById('overlayOffshoreList');
    var specialBox = document.getElementById('specialBoxOffshoreList');

    overlay.style.opacity = .8;
    if(overlay.style.display == "block"){
        overlay.style.display = "none";
        specialBox.style.display = "none";
    }
    else {
        overlay.style.display = "block";
        specialBox.style.display = "block";
    }
//window.location="empSearchAll.action";
}

function displayUtilizationDiv(){
	//alert(state);
	var state=document.getElementById("state").value;
	if(state=='Available'){
		
		document.getElementById("utilizationDiv").style.visibility = 'visible';
		document.getElementById("utilizationField").style.visibility = 'visible';

    }else{
    	document.getElementById("utilizationDiv").style.visibility = 'hidden';
		document.getElementById("utilizationField").style.visibility = 'hidden';
    }
}



/*
 * Description : Project Total Timesheet hours by Week
 * Author : Nagalakshmi 
 * Date : 09/07/2017
 * 
 */
function getTimeSheettotalhoursbyProjectList()
{
   
    var tableId = document.getElementById("tblTimeSheetReport");
    ClrDashBordTable(tableId);
     var year = document.getElementById("year").value;
    var month = document.getElementById("month").value;
    var Cname=document.getElementById("customerNameTS").value;
     
   
    var Practice4=document.getElementById("practiceIdTSR").value;
    var CostModel=document.getElementById("costModel2").value;
    
    
    
  
//        }
    var req = newXMLHttpRequest();
    req.onreadystatechange = function() {
    	document.getElementById("timeSheetSearch").disabled = true;
    	if (req.readyState == 4) {
    	            if (req.status == 200) {      
    	                document.getElementById("TimeSheetHoursbyProjectDetailsList").style.display = 'none';
    	                displayTimeSheettotalhoursbyProjectList(req.responseText); 
    					  document.getElementById("timeSheetSearch").disabled = false;
//    	                pagerOption();
    	            } 
    	        }else {
    	            document.getElementById("TimeSheetHoursbyProjectDetailsList").style.display = 'block';
    	         
    	        }
    	    }; 
    var url = CONTENXT_PATH+"/getTimeSheettotalhoursbyProjectList.action?Year="+year+"&Month="+month+"&customerName="+Cname+"&practiceId="+Practice4+"&costModel="+escape(CostModel);
    req.open("GET",url,"true");
    req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    req.send(null);
}


function  displayTimeSheettotalhoursbyProjectList(response)
{
 
   var oTable = document.getElementById("tblTimeSheetReport");
    ClrDashBordTable(oTable);
    
    var dataArray = response;
     //var dataArray = response.split("*@!");
//     if(flag=='1'){
//      
//        document.getElementById("pgNo").value=1;
//        document.getElementById("totalRecords").value=dataArray[1];
//    }
    if(dataArray == "no data")
    {
        alert("No Records Found for this Search");   
    }
    else {
    	var headerFields = new Array("Sno","Customer","ProjectName","CostModel","Week1","Week2","Week3","Week4","Week5","Week6");
        
         var  temp =new Array;
     
        temp = dataArray.split('addTo');            
       
        if(response!=''){
            
        ParseAndGenerateHTML1(oTable,temp[0], headerFields);
     
        document.getElementById("totalCount").innerHTML = temp[1];
       
           
       
           
        }else{
            alert('No Result For This Search...');
                          
        }
        

       // ParseAndGenerateHTML1(oTable,response, headerFields);
       
    } 
    

}																																							
function generateTotalHoursbyProjectListDetails(tableBody,record,fieldDelimiter){
    
    
   
    
    var row;
    var cell;
    var fieldLength;
    
    var fields = record.split(fieldDelimiter);
    fieldLength = fields.length ;
    var length = fieldLength;
    
    
    row = document.createElement( "TR" );
    row.className="gridRowEven";
    tableBody.appendChild( row );
    var year = document.getElementById("year").value;
    var month = document.getElementById("month").value;
    for (var i=0;i<length-1;i++) {
       
        var cusName=document.getElementById('customerNameTS').value;
        var Practice3=document.getElementById('practiceIdTSR').value;
        var costmodel=document.getElementById("costModel2").value;
         if(i==4 && fields[4]>0){
            cell = document.createElement( "TD" );
            cell.className="gridColumn"; 
            var j = document.createElement("a");
           // cell.innerHTML = "<a href='javascript:gettotalweekcountbyproject(\""+cusName+"\",\""+Practice3+"\",\""+fields[2]+"\",\""+costmodel+"\",\""+year+"\",\""+month+"\",\"Week1\",\""+fields[8]+"\")'>"+fields[4]+"</a>";
            cell.innerHTML = "<a href='javascript:gettotalweekcountbyproject(\""+fields[1]+"\",\""+Practice3+"\",\""+fields[2]+"\",\""+costmodel+"\",\""+year+"\",\""+month+"\",\"Week1\",\""+fields[10]+"\",\""+cusName+"\")'>"+fields[4]+"</a>";
            cell.appendChild(j);    
           } else if(i==5 && fields[5]>0){
            cell = document.createElement( "TD" );
            cell.className="gridColumn"; 
            var j = document.createElement("a");
           // cell.innerHTML = "<a href='javascript:gettotalweekcountbyproject(\""+cusName+"\",\""+Practice3+"\",\""+fields[2]+"\",\""+costmodel+"\",\""+year+"\",\""+month+"\",\"Week2\",\""+fields[8]+"\")'>"+fields[5]+"</a>";
            cell.innerHTML = "<a href='javascript:gettotalweekcountbyproject(\""+fields[1]+"\",\""+Practice3+"\",\""+fields[2]+"\",\""+costmodel+"\",\""+year+"\",\""+month+"\",\"Week2\",\""+fields[10]+"\",\""+cusName+"\")'>"+fields[5]+"</a>";
            cell.appendChild(j);    
           } else  if(i==6 && fields[6]>0){
            cell = document.createElement( "TD" );
            cell.className="gridColumn"; 
            var j = document.createElement("a");
           // cell.innerHTML = "<a href='javascript:gettotalweekcountbyproject(\""+cusName+"\",\""+Practice3+"\",\""+fields[2]+"\",\""+costmodel+"\",\""+year+"\",\""+month+"\",\"Week3\",\""+fields[8]+"\")'>"+fields[6]+"</a>";
            cell.innerHTML = "<a href='javascript:gettotalweekcountbyproject(\""+fields[1]+"\",\""+Practice3+"\",\""+fields[2]+"\",\""+costmodel+"\",\""+year+"\",\""+month+"\",\"Week3\",\""+fields[10]+"\",\""+cusName+"\")'>"+fields[6]+"</a>";
            
            cell.appendChild(j);    
           }
            else    if(i==7 && fields[7]>0){
            cell = document.createElement( "TD" );
            cell.className="gridColumn"; 
            var j = document.createElement("a");
           // cell.innerHTML = "<a href='javascript:gettotalweekcountbyproject(\""+cusName+"\",\""+Practice3+"\",\""+fields[2]+"\",\""+costmodel+"\",\""+year+"\",\""+month+"\",\"Week4\",\""+fields[8]+"\")'>"+fields[7]+"</a>";
            cell.innerHTML = "<a href='javascript:gettotalweekcountbyproject(\""+fields[1]+"\",\""+Practice3+"\",\""+fields[2]+"\",\""+costmodel+"\",\""+year+"\",\""+month+"\",\"Week4\",\""+fields[10]+"\",\""+cusName+"\")'>"+fields[7]+"</a>";
            cell.appendChild(j);    
           } else    if(i==8 && fields[8]>0){
               cell = document.createElement( "TD" );
               cell.className="gridColumn"; 
               var j = document.createElement("a");
              // cell.innerHTML = "<a href='javascript:gettotalweekcountbyproject(\""+cusName+"\",\""+Practice3+"\",\""+fields[2]+"\",\""+costmodel+"\",\""+year+"\",\""+month+"\",\"Week4\",\""+fields[8]+"\")'>"+fields[7]+"</a>";
               cell.innerHTML = "<a href='javascript:gettotalweekcountbyproject(\""+fields[1]+"\",\""+Practice3+"\",\""+fields[2]+"\",\""+costmodel+"\",\""+year+"\",\""+month+"\",\"Week5\",\""+fields[10]+"\",\""+cusName+"\")'>"+fields[8]+"</a>";
               cell.appendChild(j);    
              }  else  if(i==9 && fields[9]>0){
                  cell = document.createElement( "TD" );
                  cell.className="gridColumn"; 
                  var j = document.createElement("a");
                 // cell.innerHTML = "<a href='javascript:gettotalweekcountbyproject(\""+cusName+"\",\""+Practice3+"\",\""+fields[2]+"\",\""+costmodel+"\",\""+year+"\",\""+month+"\",\"Week4\",\""+fields[8]+"\")'>"+fields[7]+"</a>";
                  cell.innerHTML = "<a href='javascript:gettotalweekcountbyproject(\""+fields[1]+"\",\""+Practice3+"\",\""+fields[2]+"\",\""+costmodel+"\",\""+year+"\",\""+month+"\",\"Week6\",\""+fields[10]+"\",\""+cusName+"\")'>"+fields[9]+"</a>";
                  cell.appendChild(j);    
                 }  else{
              cell = document.createElement( "TD" );
              cell.className="gridColumn";       
       
                cell.setAttribute("align","center");
            
                cell.innerHTML = fields[i];   
         }  
           
        
        
        //cell = document.createElement( "TD" );
       // cell.className="gridColumn";       
        //cell.innerHTML = fields[i];  
       
        if(fields[i]!=''){
            if(i==1)
            {
                cell.setAttribute("align","left");
            }
            else
            {
                cell.setAttribute("align","left");     
            }
            row.appendChild( cell );
        }
    }
  }
  

 
function gettotalweekcountbyproject(CustomerName,Practice,ProjectName,CostModel,Year,Month,Week,projectId,customerId){
    
  
    

window.location=CONTENXT_PATH+"/employee/gettotalweekcountbyproject.action?customerName="+CustomerName+"&practiceId="+Practice+"&projectName="+ProjectName+"&CostModel="+escape(CostModel)+"&year="+Year+"&month="+Month+"&week="+Week+"&projectId="+projectId+"&customerId="+customerId;



}


function getTotalTimeSheetHoursbyWeekTeja() {
	  
	  
	    var tableId = document.getElementById("tblTimeSheetHours");
	    ClrDashBordTable(tableId);
	   var projectId=document.getElementById("projectId").value;
	      var practiceId=document.getElementById("practiceId1").value;
	   var costModel=document.getElementById("costModel1").value;
	    var year = document.getElementById("year1").value;
	     
	     
	    
	    var month = document.getElementById("month1").value;
	     
	    var week=document.getElementById("week1").value;
	     
	
	   
	    
	    
	  
	   
	    var req = newXMLHttpRequest();
	    req.onreadystatechange = function() {
	        if (req.readyState == 4) {
	            if (req.status == 200) {      
	                document.getElementById("loadingMessage").style.display = 'none';
	                displaytotalweekcountbyproject(req.responseText); 
//	                pagerOption();
	            } 
	        }else {
	            document.getElementById("loadingMessage").style.display = 'block';
	        }
	    }; 
	    var url = CONTENXT_PATH+"/gettotalweekcountbyprojectDetails.action?projectId="+projectId+"&Year="+year+"&Month="+month+"&week="+week;
	    req.open("GET",url,"true");
	    req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
	    req.send(null);
	    
	}



	function  displaytotalweekcountbyproject(response)
	{
	  
	   var oTable = document.getElementById("tblTimeSheetHours");
	    ClrDashBordTable(oTable);
	    
	    var dataArray = response;
	     //var dataArray = response.split("*@!");
//	     if(flag=='1'){
//	      
//	        document.getElementById("pgNo").value=1;
//	        document.getElementById("totalRecords").value=dataArray[1];
//	    }
	    if(dataArray == "no data")
	    {
	        alert("No Records Found for this Search");   
	    }
	    else {
	        var headerFields = new Array("Sno","EmployeeName","TotalHours","ResourceType","Billable","Email","Phone");
	        
	         var  temp =new Array;
	     
	        temp = dataArray.split('addTo');            
	       
	        if(response!=''){
	            
	        ParseAndGenerateHTML1(oTable,temp[0], headerFields);
	     
	        document.getElementById("totalCount").innerHTML = temp[1];
	       
	           
	       
	           
	        }else{
	            alert('No Result For This Search...');
	                          
	        }
	        

	       // ParseAndGenerateHTML1(oTable,response, headerFields);
	       
	    } 
	    

	}


	function generateTotalHoursbyEmployee(tableBody,record,fieldDelimiter){
		  
	    var row;
	    var cell;
	    var fieldLength;
	   var fields = record.split(fieldDelimiter);
	    fieldLength = fields.length ;
	    var length = fieldLength;
	    row = document.createElement( "TR" );
	    row.className="gridRowEven";
	    tableBody.appendChild( row );
	    for (var i=0;i<length-1;i++) {            
	     //   cell = document.createElement( "TD" );
	       // cell.className="gridColumn";       
	      //  cell.innerHTML = fields[i]; 
	    	
	    	 var projectId=document.getElementById("projectId").value;
	         var year = document.getElementById("year1").value;
	    	 var month = document.getElementById("month1").value;
	    	 var week=document.getElementById("week1").value;
	    	

	        if(i==2 && fields[2]>0){
	            cell = document.createElement( "TD" );
	            cell.className="gridColumn"; 
	            var j = document.createElement("a");
	           
	            cell.innerHTML = "<a href='javascript:getDateAndTotalHoursByProject(\""+year+"\",\""+month+"\",\""+projectId+"\",\""+week+"\",\""+fields[7]+"\",\""+fields[1]+"\")'>"+fields[2]+"</a>";
	            cell.appendChild(j);    
	           } else{
	               cell = document.createElement( "TD" );
	               cell.className="gridColumn";       
	        
	                 cell.setAttribute("align","center");
	             
	                 cell.innerHTML = fields[i];   
	          }  
	        
	       
	        if(fields[i]!=''){
	            if(i==1)
	            {
	                cell.setAttribute("align","left");
	            }
	            else
	            {
	                cell.setAttribute("align","left");     
	            }
	            row.appendChild( cell );
	        }
	    }
	   
	} 
	
	
function getDateAndTotalHoursByProject(year,month,projectId,week,empId,empName){
	
	//alert("enmter into thos method")
		
		var req = newXMLHttpRequest();
	    req.onreadystatechange = function() {
	        if (req.readyState == 4) {
	            if (req.status == 200) {    
	            	populategetDateAndTotalHoursByProject(req.responseText,empName,week);
	                
	            } 
	        }else {
	          // alert("HTTP error ---"+req.status+" : "+req.statusText);
	        }
	    }; 
	var url = CONTENXT_PATH+"/getDateAndTotalHoursByProject.action?year="+year+"&month="+month+"&projectId="+projectId+"&week="+week+"&empId="+empId;
	req.open("GET",url,"true");    
	req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
	req.send(null);
	}

	
	function populategetDateAndTotalHoursByProject(resText,empName,week) {
		
		//alert("enter into thos me")
	
		var projectName=document.getElementById('projectName1').value;
		// alert("projectName1" +projectName);
        var background = "#3E93D4";
        var title="["+week+"]"+"["+projectName+"]";
       // alert("title" +title);
        var text="daywise hours";
		var size = resText.length;
		var content = "<html><head><title>"+title+"</title></head>\
		<body bgcolor='"+background +"' style='color:white;'><h4>"+week+"   "+projectName+" "+text+"</h4>EmployeeName:"+empName+"<br/><br/>\n";

		var tableData=resText;
        var tblRow=tableData.split("*@!");
		//alert("tblRow" +tblRow);
		if(tableData == "no data")
		{
		  alert("No records found");
		}

		else
		{
	
		if(tblRow.length == 1)
		{
		  
		content=content+"<table border='1' style:align='center'>";

		content=content+"<tr><th>SNO</th><th>DATE</th><th>TotalHours</th></tr>";
		content=content+"<tr> <td colspan='4'> No record found for this  search </td> </tr>";

		}

		else
		{

		content=content+"<table border='1' style:align='center'>";

		content=content+"<tr><th>SNO</th><th>DATE</th><th>TotalHours</th></tr>";


		for(var n=0;n<tblRow.length;n++){
		content=content+"<tr>";
		var tblCol=tblRow[n].split("#^$");
		for(var j=0;j<tblCol.length;j++){
		content=content+"<td>"+tblCol[j]+"</td>";   
		}
		content=content+"</tr>";
		}
		content=content+"</table>";}}

		content=content+"</body></html>";
		  
		 if(size < 50){
		   //Create the popup       
		   popup = window.open("","window","channelmode=0,width=350,height=300,top=200,left=350,scrollbars=no,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no");    
		   popup.document.write(content); //Write content into it.    
		}

		else if(size < 100){
		   //Create the popup       channelmode
		   popup = window.open("","window","=0,width=400,height=300,top=200,left=350,scrollbars=no,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no");    
		   popup.document.write(content); //Write content into it.    
		}

		else if(size < 260){
		   //Create the popup       
		   popup = window.open("","window","channelmode=0,width=500,height=400,top=200,left=350,scrollbars=no,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no");    
		   popup.document.write(content); //Write content into it.    
		} else {
		   //Create the popup       
		   popup = window.open("","window","channelmode=0,width=600,height=500,top=200,left=350,scrollbars=no,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no");    
		   popup.document.write(content); //Write content into it.    
		}

		}
	
	
function getResourceTypeDetailsByProject(accId,pId){
		
		toggleCloseUploadOverlay3();
	   
	      var req = newXMLHttpRequest();
	    var  projectId=document.getElementById("projectName1").value;
	    projectId=pId;
	    //alert("projectId"+projectId);
	    req.onreadystatechange = readyStateHandlerText(req, displayResourceTypeDetailsByProject);
	    var url = CONTENXT_PATH+"/getResourceTypeDetailsByProject.action?accId="+accId+'&projectId='+projectId+"&random="+Math.random();
	    req.open("GET",url,"true");
	    req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
	    req.send(null);
	}

function displayResourceTypeDetailsByProject(response) {
    var oTable = document.getElementById("tblProjectResourceTypeDetails");
    ClrTable(oTable);
    var headerFields = new Array("Main","Shadow","Training","Overhead");
    var dataArray = response;
     // ParseAndGenerateHTML1(oTable,dataArray, headerFields);
      var records=dataArray.split("*@!");
      if(records.length>0){
      var data=records[0].split("#^$");
      
      var tbody = oTable.childNodes[0];    
  var tbody = oTable.childNodes[0];    
    tbody = document.createElement("TBODY");
    oTable.appendChild(tbody);
    var row;
    var cell;
    row = document.createElement( "TR" );
    row.className="gridHeader";
    tbody.appendChild( row );
     for (var i=0; i<headerFields.length; i++) {
        cell = document.createElement( "TD" );
        cell.className="gridHeader";
         cell.setAttribute("colspan","2");
           cell.setAttribute("align","center");
        row.appendChild( cell );
        cell.innerHTML = headerFields[i];
        cell.width = 120;
    }
     row = document.createElement( "TR" );
    row.className="gridRowEven";
	tbody.appendChild( row );
	  cell = document.createElement( "TD" );
        cell.className="gridColumn";    
         cell.setAttribute("colspan","2");
		cell.innerHTML = data[0];
		  cell.setAttribute("align","center");
                  
	 row.appendChild( cell );
           cell = document.createElement( "TD" );
        cell.className="gridColumn";    
         cell.setAttribute("colspan","2");
		cell.innerHTML = data[1];
		  cell.setAttribute("align","center");
	 row.appendChild( cell );
          cell = document.createElement( "TD" );
        cell.className="gridColumn";    
         cell.setAttribute("colspan","2");
		cell.innerHTML = data[2];
		  cell.setAttribute("align","center");
                   row.appendChild( cell );
                   cell = document.createElement( "TD" );
        cell.className="gridColumn";    
         cell.setAttribute("colspan","2");
		cell.innerHTML = data[3];
		  cell.setAttribute("align","center");
                  
	 row.appendChild( cell );
         headerFields = new Array("OnSite","OffShore","OnSite","OffShore","OnSite","OffShore","OnSite","OffShore");
         row = document.createElement( "TR" );
    row.className="gridHeader";
      cell.setAttribute("align","center");
    tbody.appendChild( row );
     for (var i=0; i<headerFields.length; i++) {
        cell = document.createElement( "TD" );
        cell.className="gridHeader";
        
        row.appendChild( cell );
        cell.innerHTML = headerFields[i];
        cell.width = 120;
    }
      row = document.createElement( "TR" );
    row.className="gridRowEven";
	tbody.appendChild( row );
	  cell = document.createElement( "TD" );
        cell.className="gridColumn";    
		cell.innerHTML = data[4];
		  cell.setAttribute("align","right");
	 row.appendChild( cell );
           cell = document.createElement( "TD" );
        cell.className="gridColumn";    
		cell.innerHTML = data[5];
		  cell.setAttribute("align","right");
	 row.appendChild( cell );
          cell = document.createElement( "TD" );
        cell.className="gridColumn";    
		cell.innerHTML = data[6];
		  cell.setAttribute("align","right");
	 row.appendChild( cell );
          cell = document.createElement( "TD" );
        cell.className="gridColumn";    
		cell.innerHTML = data[7];
		  cell.setAttribute("align","right");
	 row.appendChild( cell );
         
           cell = document.createElement( "TD" );
        cell.className="gridColumn";    
		cell.innerHTML = data[8];
		  cell.setAttribute("align","right");
	 row.appendChild( cell );
         
           cell = document.createElement( "TD" );
        cell.className="gridColumn";    
		cell.innerHTML = data[9];
		  cell.setAttribute("align","right");
	 row.appendChild( cell );
         
           cell = document.createElement( "TD" );
        cell.className="gridColumn";    
		cell.innerHTML = data[10];
		  cell.setAttribute("align","right");
	 row.appendChild( cell );
         
           cell = document.createElement( "TD" );
        cell.className="gridColumn";    
		cell.innerHTML = data[11];
		  cell.setAttribute("align","right");
	 row.appendChild( cell );
         
           var footer =document.createElement("TR");
    footer.className="gridPager";
    tbody.appendChild(footer);
    cell = document.createElement("TD");
    cell.className="gridFooter";
     cell.setAttribute("align","right");
      cell.innerHTML ="Total&nbsp;Resource&nbsp;:"+ data[12];
      cell.colSpan = "8";
      footer.appendChild(cell);
      }
    document.getElementById("headerLabel3").style.color="white";
    document.getElementById("headerLabel3").innerHTML="Resource Details";
            
    var overlay = document.getElementById('overlayProjectResourceType');
    var specialBox = document.getElementById('specialBoxProjectResourceType');
    overlay.style.opacity = .8;
    if(overlay.style.display == "block"){
        overlay.style.display = "none";
        specialBox.style.display = "none";
    }
    else {
        overlay.style.display = "block";
        specialBox.style.display = "block";
    }

    
}

function generatetblProjectResourceTypeDetails(tableBody,record,fieldDelimiter){
    var row;
   var cell;
   var fieldLength;
   var fields = record.split(fieldDelimiter);
   fieldLength = fields.length ;
   var length = fieldLength;
   row = document.createElement( "TR" );
   row.className="gridRowEven";
   tableBody.appendChild( row );
       
   for (var i=0;i<length;i++) { 
          
       cell = document.createElement( "TD" );
       cell.className="gridColumn";   
        cell.innerHTML = fields[i];
       
     if(fields[i]!=''){
           if(i==1)
           {
               cell.setAttribute("align","left");
           }
           else
           {
               cell.setAttribute("align","left");     
           }
           row.appendChild( cell );
       }
   }
   
   
  
}

function toggleCloseUploadOverlay4() {
	
    var overlay = document.getElementById('overlayProjectResourceType');
    var specialBox = document.getElementById('specialBoxProjectResourceType');

    overlay.style.opacity = .8;
    if(overlay.style.display == "block"){
        overlay.style.display = "none";
        specialBox.style.display = "none";
    }
    else {
        overlay.style.display = "block";
        specialBox.style.display = "block";
    }
    
    toggleCloseUploadOverlay3();
    

}
/*
 * End
 */


/* timeshet audit module*/

function getCustomerDetailsByCostModel(val){

var mainjson = {};

var costModel=val.value;

mainjson["costModel"]=costModel;
 var json = JSON.stringify(mainjson);

var url="getCustomerDetailsByCostModel.action?rand="+Math.random();
// alert(url);
$.ajax({
   
 	url:url,
     data:{jsonData: json},
     contentType: 'application/json',
     type: 'GET',
     context: document.body,
     success: function(responseText) {
     
    		var json = $.parseJSON(responseText);
    		 var data=json["data"];
    		 if(data.length>0){
    			 console.log( $('#customerName').html());
    			 $('#customerName').html('');
    			 console.log("after "+ $('#customerName').html());
    			  $('#customerName').append('<option value="">All</option>');
    			  console.log("after "+ $('#customerName').html());
    			  console.log(data);
    		  for (var i = 0; i < data.length; i++) {
 	            	var values=data[i];
 	            	console.log(values["Id"] +" "+values["CustomerName"]);
 	            	var option='<option value="'+values["Id"]+'">'+values["CustomerName"]+'</option>';
 	            	//$('#customerName').append('');
 	            	 $('#customerName').append(option);
 	            	  console.log("after "+ $('#customerName').html());
					
    		  }
    		 }else{
    			 
    		 }
     },
     error: function(e){
        alert('error');
     }
 });

}
function onFocusEmployeeForTimeSheetHrs() {
    var test=document.getElementById("assignedToUID").value;
   
    if (test == "") {
    	
    	 clearTable1();
	        hideScrollBar();
	        var validationMessage=document.getElementById("authorEmpValidationMessage");
	        validationMessage.innerHTML = "";
	        document.frmSearch.preAssignEmpId.value="";
        if (test.length >-1) {
            //alert("CONTENXT_PATH-- >"+CONTENXT_PATH)
            var url = CONTENXT_PATH+"/getEmployeeForTimeSheetHrs.action?employeeName="+escape(test);
            var req = initRequest(url);
            req.onreadystatechange = function() {
                //    alert("req-->"+req);
                if (req.readyState == 4) {
                    if (req.status == 200) {
                        // alert(req.responseXML);
                    
                        parseEmpMessagesForProjectDeatils(req.responseXML);
                    } else if (req.status == 204){
                        clearTable1();
                    }
                }
            };
            req.open("GET", url, true);

            req.send(null);
        }
    }
}
function onBlurEmployeeForTimeSheetHrs() {
    var test=document.getElementById("assignedToUID").value;
   
    if (test == "") {
    	
    	 clearTable1();
	        hideScrollBar();
	        var validationMessage=document.getElementById("authorEmpValidationMessage");
	        validationMessage.innerHTML = "";
	        document.frmSearch.preAssignEmpId.value="";
	        
    }
}

function EmployeeForTimeSheetHrs() {
    var test=document.getElementById("assignedToUID").value;
   
    

        clearTable1();
        hideScrollBar();
        var validationMessage=document.getElementById("authorEmpValidationMessage");
        validationMessage.innerHTML = "";
        document.frmSearch.preAssignEmpId.value="";
    //frmSearch issuesForm
    
        if (test.length >-1) {
            //alert("CONTENXT_PATH-- >"+CONTENXT_PATH)
            var url = CONTENXT_PATH+"/getEmployeeForTimeSheetHrs.action?employeeName="+escape(test);
            var req = initRequest(url);
            req.onreadystatechange = function() {
                //    alert("req-->"+req);
                if (req.readyState == 4) {
                    if (req.status == 200) {
                        // alert(req.responseXML);
                    
                        parseEmpMessagesForProjectDeatils(req.responseXML);
                    } else if (req.status == 204){
                        clearTable1();
                    }
                }
            };
            req.open("GET", url, true);

            req.send(null);
        }
    
}
function getLoadForAudit()
{
    var oTable = document.getElementById("tblEmpConsalodateReportList");
       
    ClrTable(oTable);
    document.getElementById("saveinvoicebtn").style.display='none';
    var customerId=document.getElementById("customerName").value;
    var year=document.getElementById("year").value;
    var month=document.getElementById("month").value;
    
    var projectId = document.getElementById("projectName").value;
    var costModel = document.getElementById("costModel").value;
    var empId=document.getElementById("preAssignEmpId").value;
    
    
    if(year.trim().length==0 ){
    	alert('Please enter year');
    	return false;
    }
    if(month==''){
    	alert('Please select month');
    	return false;
    }
    if(customerId=="" && empId==""){
    	alert("Please select customer name or resource name");
    	return false;
    }
    var myObj = {};  
    myObj["customerId"] = customerId;
    myObj["year"] = year;
    myObj["month"] = month;
    myObj["projectId"] = projectId;
    myObj["costModel"] = costModel;
    myObj["empId"] = empId;
    var json = JSON.stringify(myObj);
    //alert(json);
    var url = CONTENXT_PATH+"/getLoadForAudit.action?q="+Math.random();
    document.getElementById("loadInvoiceHrs").style.display = 'block';
    $.ajax({
	        
	     	
	     	url:url,
	         data:{jsonData: json},
	         contentType: 'application/json',
	         type: 'GET',
	         context: document.body,
	         success: function(responseText) {   
	        	 
	        	 document.getElementById("loadInvoiceHrs").style.display = 'none';
         displayConsolidateReportResult(responseText);      
	               
	         },
	         error: function(e){
	             document.getElementById('load').style.display='none';
	             document.getElementById('resultMessage').innerHTML="Please try again later";
	             
	         }
	     });
    
    

}


function displayConsolidateReportResult(resText) 
{
    // alert(resText);
	
        var oTable = document.getElementById("tblEmpConsalodateReportList");
        rowCount=0;
        ClrTable(oTable);
        // var headerFields = new Array("SNo","Customer&nbsp;Name","Project&nbsp;Name","Total&nbsp;Resources","OffShore&nbsp;Billable","OffShore&nbsp;Shadow","Onsite&nbsp;Billable","Onsite&nbsp;Shadow");
        var headerFields = new Array("S.No","Resource&nbsp;Name","Resouce&nbsp;Type","Country","Project&nbsp;Name","Customer&nbsp;Name","Start&nbsp;Date","End&nbsp;Date","Actual&nbsp;Hrs as&nbsp;per&nbsp;Utilization","Project&nbsp;Hours");
         var roleName=document.getElementById("roleName").value;
         if(roleName=="Operations"){
        	 headerFields = new Array("S.No","Resource&nbsp;Name","Resouce&nbsp;Type","Country","Project&nbsp;Name","Customer&nbsp;Name","Start&nbsp;Date","End&nbsp;Date","Actual&nbsp;Hrs as&nbsp;per&nbsp;Utilization","Project&nbsp;Hours","Invoice&nbsp;Hours","Comment","Action");
         }
    
        if(resText!=''){
           
            //   document.getElementById("totalState1").innerHTML = temp[1];
        
            ParseAndGenerateHTML1(oTable,resText, headerFields);
            document.getElementById("saveinvoicebtn").style.display='block';
        }else{
           // alert('No Result For This Search...');
           // spnFast.innerHTML="No Result For This Search...";   
            ParseAndGenerateHTML1(oTable,resText, headerFields);
        }
       
    
}

var rowCount=0;
function generateEmpConsolidateReport(tableBody,record,fieldDelimiter){
	
	rowCount=rowCount+1;
    var row;
    var cell;
    var fieldLength;
    var fields = record.split(fieldDelimiter);
    
    var ProjectHours=fields[4];
    var InternalHours=fields[5];
    var VacationHours=fields[6];
    var HolidayHours=fields[7];
    var CompHours=fields[7];
    // alert("fields--ProjectHours--"+ProjectHours+"---InternalHours---"+InternalHours+"--VacationHours---"+VacationHours+"---HolidayHours--"+HolidayHours);
    fieldLength = fields.length ;
    var length = fieldLength;
    row = document.createElement( "TR" );
    if(ProjectHours==0){
       
                        row.setAttribute("style", "background: #F08080;");
                  
                
            
    }
    
    row.className="gridRowEven";
    tableBody.appendChild( row );
    
    // td formation
  
    
    
    
    
    for (var i=0;i<length;i++) {      
    	
    	row.style.background="background-color:LightGray";
        cell = document.createElement( "TD" );
        cell.className="gridColumn";       
        cell.innerHTML = fields[i];  
       
        if(i==0){
        	
        	cell.innerHTML = rowCount;  
        }
        if(i==9 ){
        	if(isNaN(fields[i])){
        		cell.innerHTML=fields[i];
        	}else{
        	 cell.innerHTML = "<a href='javascript:loadTimeSheetData("+fields[10]+","+fields[11]+","+fields[12]+","+fields[14]+","+fields[15]+");'>"+fields[i]+"</a>";  
        	}
        }
        if(fields[i]!=''){
            if(i==1)
            {
                cell.setAttribute("align","left");
            }
            else
            {
                cell.setAttribute("align","left");     
            }
            row.appendChild( cell );
        }
        
        if(i==9){
        	
        	var roleName=document.getElementById("roleName").value;
        	 if(roleName=="Operations"){
        		 if(!isNaN(fields[i])){
        	 cell = document.createElement( "TD" );
 	        cell.className="gridColumn";       
 	        cell.innerHTML = "<input type='text' name='invoiceHrs' id='invoiceHrs"+rowCount+"' class='inputSelectSmall' onchange='isNumeric(this);'/>";  
 	       cell.setAttribute("align","left");   
 	      row.appendChild( cell );
 	      
 	      
 	     cell = document.createElement( "TD" );
 	        cell.className="gridColumn";       
 	        cell.innerHTML = "<a href='javascript:auditComments(\"comments"+rowCount+"\");'>Comment</a>";  
 	       cell.setAttribute("align","left");   
 	      row.appendChild( cell );
 	     
 	    
        
 	     cell = document.createElement( "TD" );
 	        cell.className="gridColumn";       
 	        cell.innerHTML = "<input type='checkbox' value='save' id='invoiceFlag"+rowCount+"' class='buttonBg'/>" +
 	       "<input type='hidden' value='"+fields[8]+"' id='hrsPerUtilization"+rowCount+"' />" +	
 	      "<input type='hidden' value='' id='comments"+rowCount+"' />" +
 	        "<input type='hidden' value='"+fields[10]+"' id='newCustomerId"+rowCount+"' />" +
 	        				"<input type='hidden' value='"+fields[11]+"' id='newProjectId"+rowCount+"' />" +
 	        						"<input type='hidden' value='"+fields[12]+"' id='newEmpId"+rowCount+"' />" +
 	        								"<input type='hidden' value='"+fields[13]+"' id='newCostModel"+rowCount+"' />" +
 	        								"<input type='hidden' value='"+fields[14]+"' id='newYear"+rowCount+"' />" +
 	        										"<input type='hidden' value='"+fields[15]+"' id='newMonth"+rowCount+"' />" +
 	        										"<input type='hidden' value='"+fields[2]+"' id='EmpStatus"+rowCount+"' />" +
 	        												"<input type='hidden' value='"+fields[9]+"' id='projectHrs"+rowCount+"' />";  
 	       cell.setAttribute("align","left");   
 	      row.appendChild( cell );
        		 }else{
        			 cell = document.createElement( "TD" );
        	 	        cell.className="gridColumn";       
        	 	        cell.innerHTML = "";  
        	 	       cell.setAttribute("align","left");   
        	 	      row.appendChild( cell );
        	 	      
        	 	     cell = document.createElement( "TD" );
     	 	        cell.className="gridColumn";       
     	 	        cell.innerHTML = "";  
     	 	       cell.setAttribute("align","left");   
     	 	      row.appendChild( cell );
     	 	      
     	 	    cell = document.createElement( "TD" );
	 	        cell.className="gridColumn";       
	 	        cell.innerHTML = "<input type='checkbox' value='save' id='invoiceFlag"+rowCount+"' class='buttonBg' style='display:none;'/>";  
	 	       cell.setAttribute("align","left");   
	 	      row.appendChild( cell );
	 	      
        		 }
         }
        break;
        }
    }
   
}

function auditComments(commentId){
    // alert("type---"+type);
    document.getElementById('resultMessage1').innerHTML ='';
    document.getElementById('descriptionCount').innerHTML ='';
  
  
  
    document.getElementById("headerLabel").style.color="white";
   
   
    showRow('addTr');
   
    hideRow('updateTr');
    
    var overlay = document.getElementById('commentsOverlay');
    var specialBox = document.getElementById('commentsSpecialBox');
    overlay.style.opacity = .8;
    if(overlay.style.display == "block"){
        overlay.style.display = "none";
        specialBox.style.display = "none";
    }
    else {
        overlay.style.display = "block";
        specialBox.style.display = "block";
        
      
             
            var desc =  document.getElementById(commentId).value;
          
            
            document.getElementById('comments').value=desc;
            document.getElementById('commentsId').value=commentId;
           
           
         // alert(status+"anand"+curretRole) 
        
        
       
           
        
    }
            



}
function closeComments(){
    // alert("type---"+type);
   
	document.getElementById('resultMessage1').innerHTML='';
    var overlay = document.getElementById('commentsOverlay');
    var specialBox = document.getElementById('commentsSpecialBox');
    overlay.style.opacity = .8;
    if(overlay.style.display == "block"){
        overlay.style.display = "none";
        specialBox.style.display = "none";
    }
    else {
        overlay.style.display = "block";
        specialBox.style.display = "block";
        
  
        
    }
            



}
function addComments(){
var commentId =  document.getElementById("commentsId").value;


var comment=document.getElementById('comments').value;
document.getElementById(commentId).value=comment;
auditComments(commentId);
}

function saveInvoiceHrsDetails(){
	 document.getElementById("loadInvoiceHrs").style.display = 'block';
	var rowCount = $('#tblEmpConsalodateReportList tr').length-2;
//	alert(rowCount);
	var mainjson = {};
	for(i=1;i<=rowCount;i++){
		//alert($('#invoiceFlag'+i).prop('checked'));
		if($('#invoiceFlag'+i).prop('checked')){
		var submyObj = {};
		var newCustomerId=$('#newCustomerId'+i).val();
		var newProjectId=$('#newProjectId'+i).val();
		var newEmpId=$('#newEmpId'+i).val();
		var newCostModel=$('#newCostModel'+i).val();
		var newYear=$('#newYear'+i).val();
		var newMonth=$('#newMonth'+i).val();
		var projectHrs=$('#projectHrs'+i).val();
		var invoiceHrs=$('#invoiceHrs'+i).val();
		var hrsPerUtilization=$('#hrsPerUtilization'+i).val();
	  var comments=$('#comments'+i).val();
	  var EmpStatus=$('#EmpStatus'+i).val();
	    if(invoiceHrs.trim()=="" || parseFloat(invoiceHrs)<1){
	    	$('#invoiceHrs'+i ).focus();
	    	alert("please enter invoice hrs");
	    	document.getElementById("loadInvoiceHrs").style.display = 'none';
	    	return false;
	    }
	    submyObj["newCustomerId"] = newCustomerId;
	    submyObj["newProjectId"] = newProjectId;
	    submyObj["newEmpId"] = newEmpId;
	    submyObj["newCostModel"] = newCostModel;
	    submyObj["newYear"] = newYear;
	    submyObj["newMonth"] = newMonth;
	    submyObj["projectHrs"] = projectHrs;
	    submyObj["invoiceHrs"] = invoiceHrs;
	    submyObj["hrsPerUtilization"] = hrsPerUtilization;
	    submyObj["comments"] = comments;
	    submyObj["EmpStatus"] = EmpStatus;
	    mainjson[i]=(submyObj);
		}
	   // submyObj["newCustomerId"] = newCustomerId;
	}
	 var json = JSON.stringify(mainjson);
  
   
   $.ajax({
      
    	url:'doSaveInvoiceDetails.action',
        data:{jsonData: json},
        contentType: 'application/json',
        type: 'GET',
        context: document.body,
        success: function(responseText) {
            if(responseText=='success'){
           	 document.getElementById("loadInvoiceHrs").style.display = 'none';
           	 getLoadForAudit();
            }else{
                document.getElementById('loadInvoiceHrs').style.display='none';
                document.getElementById('resultMessage2').innerHTML="Please try again later";
                document.getElementById('resultMessage2').style.color="red";
            }
              
        },
        error: function(e){
            document.getElementById('loadInvoiceHrs').style.display='none';
            document.getElementById('resultMessage2').innerHTML="Please try again later";
            document.getElementById('resultMessage2').style.color="red";
        }
    });
  
 
}
function loadTimeSheetData(customerId,projectId,empId,year,month){
	//  alert("hi");
	  TimeSheetAuditOverlay();
	 
	  var myObj = {};  
	    myObj["customerId"] = customerId+"";
	    myObj["year"] = year;
	    myObj["month"] = month;
	    myObj["projectId"] = projectId+"";
	    myObj["empId"] = empId;
	    var json = JSON.stringify(myObj);
	    var oTable = document.getElementById("tblTimeSheetDayByDayReport");
	    document.getElementById("infoTblId").style.display = 'none';
	    ClrTable(oTable);
	    var url = CONTENXT_PATH+"/getLoadTimeSheetDataForAudit.action?q="+new Date();
	    document.getElementById("load").style.display = 'block';
	    $.ajax({
 	        
 	     	
 	     	url:url,
 	         data:{jsonData: json},
 	         contentType: 'application/json',
 	         type: 'GET',
 	         context: document.body,
 	         success: function(responseText) {   
 	        	var json = $.parseJSON(responseText);
 	        	
 	        	 var headerFields = new Array("S.No","Work&nbsp;Date","Prj1Hrs","Prj2Hrs","Prj3Hrs","Prj4Hrs","Prj5Hrs","InternalHrs","VacationHrs","HolidayHrs","ComptimeHrs");
 	  	     var headerFields=json["header"];
 	  	 
 	          
 	             var dataNew=json["dataNew"];
 	             var hrs=dataNew.split("@@@")[1]; 
 	             //alert(hrs);
 	             var data=(dataNew.split("@@@")[0]).split("*@!");
 	        	
 	        	tbody = oTable.childNodes[0];    
 	           tbody = document.createElement("TBODY");
 	           oTable.appendChild(tbody);
 	          generateTableHeader(tbody,headerFields);
 	          if(data.length>0){
 	         var row;
 	         var totalhrs=[];
 	        var totalhrslength=data[0].split("#^$").length;
 	     
 	       totalhrslength=(totalhrslength-10)/2+1;
 	       
 	        for(var loop=0;loop<totalhrslength;loop++){
 	        	totalhrs[loop]=0;
 	        }
 	        //alert('first'+totalhrs.length);
 	        var Project1Hrs="0";
			var Project2Hrs="0";
			var Project3Hrs="0";
			var Project4Hrs="0";
			var Project5Hrs="0";
			var InternalHrs="0";
			var VacationHrs="0";
			var HolidayHrs="0";
			var ComptimeHrs="0";
 	         for (var i = 0; i < data.length-1; i++) {
 	            	var rowData=data[i].split("#^$");
 	         
 	            	row = document.createElement( "TR" );
 	           
 	      	   
 	      		 row.className="gridRowEven";
 	      		tbody.appendChild( row );
 	      		//td 1
 	      		
 	      	    cell = document.createElement( "TD" );
 	      	        cell.className="gridColumn";       
 	      	        cell.innerHTML = i+1;  
 	      			  cell.setAttribute("align","left");
 	      			   row.appendChild( cell );
 	      			  
 	      			var count=i+1;   
 	      			createTableCell(row,"<input type='hidden' id='TimesheetId"+count+"' class='inputTextHours' value='"+rowData[0]+"'/>"+rowData[1]);
 	      			//alert(rowData.length);
 	      			
 	      			var hrsindex=0;
 	      			for(var j=2;j<rowData.length;j=j+3){
 	      				totalhrs[hrsindex]=parseFloat(totalhrs[hrsindex])+parseFloat(rowData[j]);
 	      				if(rowData[j+1]=='1'){
 	      				createTableCell(row,"<input type='text' class='inputTextHours "+rowData[j+2]+"' id='Project"+hrsindex+"Hrs"+count+"' value='"+rowData[j]+"' onchange='validHrs(this);calculateHrs(this,"+count+");'/>");
 	      				}else{
 	      					createTableCell(row,"<input type='text' class='inputTextHours readOnlyCss' readOnly id='Project"+hrsindex+"Hrs"+count+"' value='"+rowData[j]+"' onchange='validHrs(this);calculateHrs(this,"+count+");'/>");
 	      				}
 	      				hrsindex=hrsindex+1;
 	      			}
 	      			
 	      		
 	         }
 	        var footer =document.createElement("TR");
 		   footer.className="gridPager";
 		     tbody.appendChild(footer);
 		     
 	         createFooterCell(footer,"");
 	         createFooterCell(footer,"");	
 	        // alert('footer'+totalhrs.length);
 	         var projecthrs=0;
 	         console.log("length=="+totalhrs.length)
 	         for(var i=0;i<headerFields.length-2;i++){
 	        	 
 	        	 if(i<totalhrs.length-4){
 	        		projecthrs=parseFloat(parseFloat(projecthrs)+parseFloat(totalhrs[i])).toFixed(2);
 	        	 }
 	        createFooterCell(footer,"<input type='text' class='totalCss inputTextHours' id='Project"+(i+1)+"TotalHrs' value='"+totalhrs[i].toFixed(2)+"' readonly/>");
 	        	 
 	         }
 	   
 	        $("#TotlalProjectHrs").html(projecthrs);
 	       $("#TotlalBillableHRs").html(hrs.split("*@!")[0]);
 	      $("#TotlalNonBillableHrs").html(hrs.split("*@!")[1]);
 	     $("#TotlalWorkHRs").html(hrs.split("*@!")[2]);
 	     
 	        document.getElementById("infoTblId").style.display = 'inline';
 	        
 	          }else{
 	        	 generateNoRecords(tbody,oTable);
 	        	 generateFooter(tbody,oTable);
 	        	   document.getElementById("infoTblId").style.display = 'none';
 	          }	           
 	        	 document.getElementById("load").style.display = 'none';
             
 	               
 	         },
 	         error: function(e){
 	             document.getElementById('load').style.display='none';
 	             alert("Please try again later");
 	             
 	         }
 	     });
  }
function createTableCell(row,data){
	cell = document.createElement( "TD" );
       cell.className="gridColumn";       
       cell.innerHTML = data;  
		  cell.setAttribute("align","left");
		   row.appendChild( cell );
}
function  createFooterCell(row,data){
	
    cell = document.createElement("TD");
	 cell.className="gridFooter";
	  cell.setAttribute('align','left');
	  cell.setAttribute('style','padding: 3px 6px;');
	  cell.innerHTML = data; 
	   row.appendChild( cell );
		
			
}
function TimeSheetAuditOverlay(){
	  var overlay = document.getElementById('overlayTimeSheetAudit');
	    var specialBox = document.getElementById('specialBoxTimeSheetAudit');
	    overlay.style.opacity = .8;
	    if(overlay.style.display == "block"){
	        overlay.style.display = "none";
	        specialBox.style.display = "none";
	    }
	    else {
	        overlay.style.display = "block";
	        specialBox.style.display = "block";
	    } 
}
function getTimeSheetAuditSearch(){
	   var oTable = document.getElementById("tblEmpConsalodateReportList");
    
	    ClrTable(oTable);
	    document.getElementById("saveinvoicebtn").style.display='none';
	    var customerId=document.getElementById("customerName").value;
	    var year=document.getElementById("year").value;
	    var month=document.getElementById("month").value;
	    
	    var projectId = document.getElementById("projectName").value;
	    var costModel = document.getElementById("costModel").value;
	    var empId = document.getElementById("preAssignEmpId").value;
	    if(year.trim().length==0 ){
	    	alert('Please enter year');
	    	return false;
	    }
	    if(month==''){
	    	alert('Please select month');
	    	return false;
	    }
	    var myObj = {};  
	    myObj["customerId"] = customerId;
	    myObj["year"] = year;
	    myObj["month"] = month;
	    myObj["projectId"] = projectId;
	    myObj["costModel"] = costModel;
	    myObj["empId"] = empId;
	    var json = JSON.stringify(myObj);
	    var url = CONTENXT_PATH+"/getTimeSheetAuditSearch.action?q="+Math.random();
	    document.getElementById("loadInvoiceHrs").style.display = 'block';
	    $.ajax({
	        
	     	
	     	url:url,
	         data:{jsonData: json},
	         contentType: 'application/json',
	         type: 'GET',
	         context: document.body,
	         success: function(responseText) {   
	        	var json = $.parseJSON(responseText);
	        	//var myJSON = JSON.stringify(responseText);
	        //	 alert(json.length);
	        	 var data=json;
	        	// EXTRACT VALUE FOR HTML HEADER. 
	            // ('Book ID', 'Book Name', 'Category' and 'Price')
	        	 var headerFields = new Array("S.No","Resource&nbsp;Name","Resource&nbsp;Type","Country","Project&nbsp;Name","Customer&nbsp;Name","Month","Year","Actual&nbsp;Hrs as&nbsp;per&nbsp;Utilization","Project&nbsp;Hours","Invoice&nbsp;Hours","Comments","AuditedBy");
	  	     
	        	
	        	tbody = oTable.childNodes[0];    
	           tbody = document.createElement("TBODY");
	           oTable.appendChild(tbody);
	          generateTableHeader(tbody,headerFields);
	         var row;
	         
	         if(data.length>0){
	         for (var i = 0; i < data.length; i++) {
	            	var rowData=data[i];
	         
	            	row = document.createElement( "TR" );
	           
	      	   
	      		 row.className="gridRowEven";
	      		tbody.appendChild( row );
	      		//td 1
	      	    cell = document.createElement( "TD" );
	      	        cell.className="gridColumn";       
	      	        cell.innerHTML = i+1;  
	      			  cell.setAttribute("align","left");
	      			   row.appendChild( cell );
	      		//td2	   
	      			cell = document.createElement( "TD" );
	      	        cell.className="gridColumn";       
	      	        cell.innerHTML = rowData["empName"];  
	      	      cell.innerHTML = "<a href='javascript:loadTimeSheetData("+rowData['CustomerId']+","+rowData['ProjectId']+","+rowData['EmpId']+","+rowData['Year']+","+rowData['Month']+");'>"+rowData["empName"]+"</a>";
	      			  cell.setAttribute("align","left");
	      			   row.appendChild( cell );
	      			
	      			   
	      			 cell = document.createElement( "TD" );
		      	        cell.className="gridColumn";       
		      	        cell.innerHTML = rowData["EmpStatus"];  
		      			  cell.setAttribute("align","left");
		      			   row.appendChild( cell );
		      			   
	      			 cell = document.createElement( "TD" );
		      	        cell.className="gridColumn";       
		      	        cell.innerHTML = rowData["WorkingCountry"];  
		      			  cell.setAttribute("align","left");
		      			   row.appendChild( cell );
		      			   
	      			 
	      			//td3	   
	 	      			cell = document.createElement( "TD" );
	 	      	        cell.className="gridColumn";       
	 	      	        cell.innerHTML = rowData["ProjectName"];  
	 	      			  cell.setAttribute("align","left");
	 	      			   row.appendChild( cell );
	 	      			//td4	   
		 	      			cell = document.createElement( "TD" );
		 	      	        cell.className="gridColumn";       
		 	      	        cell.innerHTML = rowData["customerName"];  
		 	      			  cell.setAttribute("align","left");
		 	      			   row.appendChild( cell );
		 	      			//td5	   
			 	      			cell = document.createElement( "TD" );
			 	      	        cell.className="gridColumn";       
			 	      	        cell.innerHTML = rowData["MonthName"];  
			 	      			  cell.setAttribute("align","left");
			 	      			   row.appendChild( cell );
			 	      			//td6	   
				 	      			cell = document.createElement( "TD" );
				 	      	        cell.className="gridColumn";       
				 	      	        cell.innerHTML = rowData["Year"];  
				 	      			  cell.setAttribute("align","left");
				 	      			   row.appendChild( cell );
				 	      			   
				 	      			//td6	   
					 	      			cell = document.createElement( "TD" );
					 	      	        cell.className="gridColumn";       
					 	      	        cell.innerHTML = rowData["ActualHrsPerUtilization"];  
					 	      			  cell.setAttribute("align","left");
					 	      			   row.appendChild( cell ); 
				 	      			//td5	   
					 	      			cell = document.createElement( "TD" );
					 	      	        cell.className="gridColumn";       
					 	      	        cell.innerHTML = rowData["ProjectHrs"];  
					 	      			  cell.setAttribute("align","left");
					 	      			   row.appendChild( cell );
					 	      			//td	   
						 	      			cell = document.createElement( "TD" );
						 	      	        cell.className="gridColumn";       
						 	      	        cell.innerHTML = rowData["InvoiceHrs"];  
						 	      			  cell.setAttribute("align","left");
						 	      			   row.appendChild( cell );
						 	      			//td	   
							 	      			cell = document.createElement( "TD" );
							 	      	        cell.className="gridColumn";       
							 	      	        cell.innerHTML = "<a href='javascript:viewComments(\"comment"+(i+1)+"\","+rowData['Id']+");'>View</a>" +
							 	      	        		"<input type='hidden' id=\'comment"+(i+1)+"\' value=\'"+rowData['Comments']+"\' />";  
							 	      			  cell.setAttribute("align","left");
							 	      			   row.appendChild( cell );
						 	      			//td2	   
							 	      			
							 	      			cell = document.createElement( "TD" );
							 	      	        cell.className="gridColumn";       
							 	      	      //  cell.innerHTML = "<input type='button' class='buttonBg' value='load' onclick='loadTimeSheetData("+rowData['CustomerId']+","+rowData['ProjectId']+","+rowData['EmpId']+","+rowData['Year']+","+rowData['Month']+");'/>";  
							 	      	     
							 	      	  cell.innerHTML = rowData["AuditedBy"]; 
							 	      	        cell.setAttribute("align","left");
							 	      			   row.appendChild( cell );
	         }}
	         else{
	        	generateNoRecords(tbody,oTable);
	         }
	        generateFooter(tbody,oTable);	   
	          
	        	 document.getElementById("loadInvoiceHrs").style.display = 'none';
        
	               
	         },
	         error: function(e){
	             document.getElementById('load').style.display='none';
	             document.getElementById('resultMessage').innerHTML="Please try again later";
	             
	         }
	     });
	    

}

function viewComments(commentId,auditId){
	
	
	var comments =  document.getElementById(commentId).value;
	document.getElementById('comments').value=comments;
    document.getElementById('commentsId').value=commentId;
    document.getElementById('auditId').value=auditId;

    var roleName=document.getElementById("roleName").value;
    if(roleName=="Operations"){
    showRow('updateTr');
    hideRow('addTr');
    }else{
    	hideRow('updateTr');
         hideRow('addTr');
    }
    var overlay = document.getElementById('commentsOverlay');
    var specialBox = document.getElementById('commentsSpecialBox');
   // alert('hi');
    overlay.style.opacity = .8;
    if(overlay.style.display == "block"){
        overlay.style.display = "none";
        specialBox.style.display = "none";
    }
    else {
        overlay.style.display = "block";
        specialBox.style.display = "block";
        
    }
}

function updateComments(){
	var commentsId =  document.getElementById("commentsId").value;
	 var comment=document.getElementById('comments').value;
	 var auditId=document.getElementById('auditId').value;
	
	 document.getElementById(commentsId).value=comment;
	
	 var mainjson={};
	 mainjson["auditId"]=auditId;
	 mainjson["comment"]=comment;
	 document.getElementById("load1").style.display = 'block';
	 var json = JSON.stringify(mainjson);
	   
	    var url="updateAuditComment.action?rand="+Math.random();
	  // alert(url);
	    $.ajax({
	       
	     	url:url,
	         data:{jsonData: json},
	         contentType: 'application/json',
	         type: 'GET',
	         context: document.body,
	         success: function(responseText) {
	        	 document.getElementById("load1").style.display = 'none';
	        		if(responseText=="success"){
	        			document.getElementById("resultMessage1").innerHTML="<font color='green' size='2'>Updated successfully</font>";
	        		}else{
	        			document.getElementById("resultMessage1").innerHTML="<font color='red' size='2'>Try again later</font>";
	        		}
	        			
	         },
	         error: function(e){
	            alert('error');
	         }
	     });
	 
}
	    function hideRow(id) {
	        //alert(id);
	        var row = document.getElementById(id);
	        row.style.display = 'none';
	    }
	    function showRow(id) {
	        //  alert(id);
	        var row = document.getElementById(id);
	        row.style.display = '';
	    } 
	    function countChar(val,displayId) {
	        var len = val.value.length;
	        if (len >= 500) {
	            val.value = val.value.substring(0, 500);
	            document.getElementById(displayId).innerHTML=(0)+'\\500';
	        } else {
	            document.getElementById(displayId).innerHTML=(500-len)+'\\500';
	        }
	    }
 function isNumeric(element){
		    
		    var val=element.value;
		    if (isNaN(val)) {
		        alert('Please Enter numeric values');
		        //   element.value=val.substring(0, val.length-1);      
		        element.value="0.00";    
		        element.focus();
		        return false;
		    }
		   else
		        return true;
		}
/* time sheet audit process end*/

 function getProjectsByAccountIdProjectOverview(){
    var accountId = document.getElementById("customerNameId").value;
     if(accountId!=""){
         var req = newXMLHttpRequest();
         req.onreadystatechange = readyStateHandler80(req, populateProjectsListProjectOverview);
         var url = CONTENXT_PATH+"/getProjectsByAccountId.action?accountId="+accountId;
         req.open("GET",url,"true");
         req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
         req.send(null);   
     }else{
         var projects = document.getElementById("projectName");
         projects.innerHTML=" ";
       
         var opt = document.createElement("option");
         opt.setAttribute("value","");
         opt.appendChild(document.createTextNode("All"));
         projects.appendChild(opt);
     }
 }
 function populateProjectsListProjectOverview(resXML) {
   
     //var projects = document.getElementById("projectName");
     var projects = document.getElementById("projectNameId");
     //  alert("test")
     
     var projectsList = resXML.getElementsByTagName("PROJECTS")[0];
    
     var users = projectsList.getElementsByTagName("USER");
     projects.innerHTML=" ";
     for(var i=0;i<users.length;i++) {
         var userName = users[i];
         var att = userName.getAttribute("projectId");
         var name = userName.firstChild.nodeValue;
         var opt = document.createElement("option");
         opt.setAttribute("value",att);
         opt.appendChild(document.createTextNode(name));
         projects.appendChild(opt);
     }
 }
 function getEmployeeDetails(empId)
 {
  
     // window.location='../employee/getEmployee.action?empId='+empId+'','target_blank';
      window.location='../employee/getEmployee.action?empId='+empId;
     
 }

 /* triveni start*/
  function getProjectsByAccountIdProjectOverviewPMO(){
 	 var accountId = document.getElementById("customerName").value;
 	 if(accountId!=""){
 	    var req = newXMLHttpRequest();
 	    req.onreadystatechange = readyStateHandler80(req, populateProjectsListProjectOverview);
 	    var url = CONTENXT_PATH+"/getProjectsByAccountIdPmo.action?accountId="+accountId;
 	    req.open("GET",url,"true");
 	    req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
 	    req.send(null);   
 	}else{
 	     var projects = document.getElementById("projectName");
 	      projects.innerHTML=" ";
 	      
 	        var opt = document.createElement("option");
 	        opt.setAttribute("value","");
 	        opt.appendChild(document.createTextNode("All"));
 	         projects.appendChild(opt);
 	}
  }
  function getProjectsByAccountIdProjectOverview(){
 	 var accountId = document.getElementById("customerName").value;
 	 if(accountId!=""){
 	    var req = newXMLHttpRequest();
 	    req.onreadystatechange = readyStateHandler80(req, populateProjectsListProjectOverview);
 	    var url = CONTENXT_PATH+"/getProjectsByAccountId.action?accountId="+accountId;
 	    req.open("GET",url,"true");
 	    req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
 	    req.send(null);   
 	}else{
 	     var projects = document.getElementById("projectName");
 	      projects.innerHTML=" ";
 	      
 	        var opt = document.createElement("option");
 	        opt.setAttribute("value","");
 	        opt.appendChild(document.createTextNode("All"));
 	         projects.appendChild(opt);
 	}
 	 }
  function populateProjectsListProjectOverview(resXML) {
    
 	  //var projects = document.getElementById("projectName");
 	    var projects = document.getElementById("projectName");
 	 
 	    
 	    var projectsList = resXML.getElementsByTagName("PROJECTS")[0];
 	   
 	    var users = projectsList.getElementsByTagName("USER");
 	    projects.innerHTML=" ";
 	    for(var i=0;i<users.length;i++) {
 	        var userName = users[i];
 	        var att = userName.getAttribute("projectId");
 	        var name = userName.firstChild.nodeValue;
 	        var opt = document.createElement("option");
 	        opt.setAttribute("value",att);
 	        opt.appendChild(document.createTextNode(name));
 	        projects.appendChild(opt);
  }
  }

  
  function getNoOfWeeks(){
 	 var year = document.getElementById("year").value;
 	 var month = document.getElementById("month").value;
 	 
 	
 		    var firstOfMonth = new Date(year, month - 1, 1);
 		
 		    var day = firstOfMonth.getDay();
 		 
 		   if(day == 0){
		    	var firstOfMonth = new Date(year, month-1, 1);
	 		    var lastOfMonth = new Date(year, month, 0);

	 		    var used = firstOfMonth.getDay() + lastOfMonth.getDate();

	 		  var weeksCount = Math.ceil( used / 7);
		    }
		    else if(day == 1){
			  var firstOfMonth = new Date(year, month-1, 1);
			    var lastOfMonth = new Date(year, month, 0);
			    var used = firstOfMonth.getDay() + 6 + lastOfMonth.getDate();
			   var weeksCount = Math.floor( used / 7);
		   }
		    else{
		    	var firstDayOfWeek = day || 0;
		    	  var firstOfMonth = new Date(year, month-1, 1);
		    	  var lastOfMonth = new Date(year, month, 0);
		    	  var numberOfDaysInMonth = lastOfMonth.getDate();
		    	  var firstWeekDay = (firstOfMonth.getDay() - firstDayOfWeek + 7) % 7;
		    	  var used = firstWeekDay + numberOfDaysInMonth;
		    	 var weeksCount = Math.ceil( used / 7);
		    }
 		    
 		    
// 		    day = day == 1 ? 0 : day;
// 		  
// 		    if (day) { day-- }
// 		    var diff = 7 - day;
// 		    var lastOfMonth = new Date(year, month, 0);
// 		    var lastDate = lastOfMonth.getDate();
// 		    if (lastOfMonth.getDay() == 0) {
// 		        diff--;
// 		    }
// 		    var result = Math.ceil((lastDate - diff) / 7);
// 		    var weeksCount = result + 1;
 		
 		
 		 var week = document.getElementById("week");
 	     //  
 	     document.getElementById("weeksCount").value = weeksCount;
 	     var myObj = {};  
 	     myObj["year"] = year;
 	     myObj["month"] = month;
 	     myObj["weeksCount"] = weeksCount;
 	    var json = JSON.stringify(myObj);
 	     var url = CONTENXT_PATH+"/getWeeksForGivenMonth.action?q="+Math.random();
 		  //  document.getElementById("loadInvoiceHrs").style.display = 'block';
 		    $.ajax({
 		        
 		     	
 		     	url:url,
 		         data:{jsonData: json},
 		         contentType: 'application/json',
 		         type: 'GET',
 		         context: document.body,
 		         success: function(responseText) {   
 		        
 		        		
 		        		 var fields = responseText.split("#^$");
      fieldLength = fields.length ;
      var length = fieldLength;
   
   
//      for (var i=0; i<fieldLength; i++) {
//     	 
//     	 
//      }
 		 	        //	 document.getElementById("loadInvoiceHrs").style.display = 'none';
 		        	 	
 		       //   displayLoadForPMOSearch(responseText,weeklyReport); 
 		        	 week.innerHTML=" ";
 		    	     for(var i=0;i<=weeksCount;i++) {
 		    	    	 if(i==0){
 		    	    		 var userName = "All";
 		    	    	 }else{
 		    	    		 var userName = "week"+i;
 		    	    	 }
 		    	         
 		    	         var att = userName;
 		    	         var name = fields[i];
 		    	         var opt = document.createElement("option");
 		    	         opt.setAttribute("value",att);
 		    	         opt.appendChild(document.createTextNode(name));
 		    	         week.appendChild(opt);
 		    	     }
 		 	               
 		 	         },
 		 	         error: function(e){
 		 	            
 		 	             document.getElementById('resultMessage').innerHTML="Please try again later";
 		 	             
 		 	         }
 		 	     });
 	     
 	   //  var projectsList = resXML.getElementsByTagName("PROJECTS")[0];
 	    
 	   //  var users = projectsList.getElementsByTagName("USER");
 	    
 	 
  }
  
  

  function getPMOWeeklyReportSearch(){
 	   var oTable = document.getElementById("tblEmpPMOWeeklyReportList");
 	  $('span.pagination').empty().remove();
 	  // document.getElementById("menu-popup").style.display = 'none';
 	     document.getElementById("pmoProjectDetailsReport").style.display = 'none';
 	    ClrTable(oTable);
 	    //document.getElementById("saveinvoicebtn").style.display='none';
 	     var customerId=document.getElementById("customerName").value;
 	     var year=document.getElementById("year").value;
 	     var month=document.getElementById("month").value;
 	     
 	     var projectId = document.getElementById("projectName").value;
 	     var costModel = document.getElementById("costModel").value;
 	     var week=document.getElementById("week").value;
 	     var weeklyReport=document.getElementById("weeklyReport").value;
 	     var weeksCount = document.getElementById("weeksCount").value;
 	     
 	     if(year.trim().length==0 ){
 	     	alert('Please enter year');
 	     	return false;
 	     }
 	     if(month==''){
 	     	alert('Please select month');
 	     	return false;
 	     }
// 	     if(customerId==""){
// 	     	alert("Please select customer name ");
// 	     	return false;
// 	     }
 	     var myObj = {};  
 	     myObj["customerId"] = customerId;
 	     myObj["year"] = year;
 	     myObj["month"] = month;
 	     myObj["projectId"] = projectId;
 	     myObj["costModel"] = costModel;
 	     myObj["week"] = week;
 	     myObj["weeklyReport"] = weeklyReport;
 	     myObj["weeksCount"] = weeksCount;
 	   
 	    var json = JSON.stringify(myObj);
 	    var url = CONTENXT_PATH+"/getPMOWeeklyReportSearch.action?q="+Math.random();
 	    document.getElementById("loadInvoiceHrs").style.display = 'block';
 	    $.ajax({
 	        
 	     	
 	     	url:url,
 	         data:{jsonData: json},
 	         contentType: 'application/json',
 	         type: 'GET',
 	         context: document.body,
 	         success: function(responseText) {   
 	        
 	        	 //	alert(responseText)
 	 	        	 document.getElementById("loadInvoiceHrs").style.display = 'none';
 	        	 	
 	          displayLoadForPMOSearch(responseText,weeklyReport); 
 	          document.getElementById("pmoProjectDetailsReport").style.display = 'block';
 	 	               
 	 	         },
 	 	         error: function(e){
 	 	             document.getElementById('load').style.display='none';
 	 	             document.getElementById('resultMessage').innerHTML="Please try again later";
 	 	             
 	 	         }
 	 	     });
 	     
 	     

 	 }


 	 function displayLoadForPMOSearch(response,weeklyReport){
 	     var tableId = document.getElementById("tblEmpPMOWeeklyReportList"); 
 	   
 	   
 	    
 	     if(weeklyReport == "NotEntered"){
         	 var headerFields = new Array("S.No","Customer&nbsp;Name","Project&nbsp;Name","StartDate","EndDate","PMO");
         	 }else {
         		 var headerFields = new Array("S.No","Customer&nbsp;Name","Project&nbsp;Name","StartDate","EndDate","Health","Scope","Schedule","Invoice","Risk","Resources","Comments","PMO","AddedDate");
  	        	 
         	 }
 	     var dataArray = response;
 	     ParseAndGenerateHTMLLoadForSearch(tableId,dataArray, headerFields,weeklyReport);
 	 }
 	  
 	 function ParseAndGenerateHTMLLoadForSearch(oTable,responseString,headerFields,weeklyReport) {
 	     var fieldDelimiter = "#^$";
 	     var recordDelimiter = "*@!";   
 	     var records = responseString.split(recordDelimiter); 
 	     generatePMOReviewLoadtoSearch(oTable,headerFields,records,fieldDelimiter,weeklyReport);
 	 }



 	 function generatePMOReviewLoadtoSearch(oTable, headerFields,records,fieldDelimiter,weeklyReport) {	

 	     tbody = oTable.childNodes[0];    
 	     var tbody = document.createElement("TBODY");
 	     oTable.appendChild(tbody);
 	     generateTableHeaderForSearch(tbody,headerFields);
 	     if(records == "NoData"){
 	    //	 alert(records)
 	 		 generateNoRecordsFound(tbody,oTable,headerFields.length);
 	    	
 	 	 }else{
 	     var rowlength;
 	     rowlength = records.length;
 	     if(rowlength >0 && records!=""){
 	         for(var i=0;i<rowlength-1;i++) {
 	        	 generatePMOReviewLoadForSearch(i,oTable,tbody,records[i],fieldDelimiter,weeklyReport);
 	        //	 document.getElementById("menu-popup").style.display = 'block';
 	             
// 	                 if(flag == "save"){
// 	                	 generateSavePMOReviewLoadFromRes(i,oTable,tbody,records[i],fieldDelimiter);	 
// 	                 }else{
// 	                 generateUpdatePMOReviewLoadFromRes(i,oTable,tbody,records[i],fieldDelimiter);
// 	         }
 	         }
 	         
 	     } else {
 	         generateNoRecordsFound(tbody,oTable,headerFields.length);
 	     }
 	     generateReviewFooter(tbody,oTable,headerFields.length);
 	     pagerOption();
 	 }
 	     
 	 }
 	 function generateTableHeaderForSearch(tableBody,headerFields) {
 		    var row;
 		    var cell;
 		    row = document.createElement( "TR" );
 		    row.className="gridHeader";
 		    tableBody.appendChild( row );
 		    for (var i=0; i<headerFields.length; i++) {
 		        cell = document.createElement( "TD" );
 		        cell.className="gridHeader";
 		        row.appendChild( cell );

 		        cell.setAttribute("width","10000px");
 		        cell.innerHTML = headerFields[i];
 		    }
 		}
  var rowCount2 = 0;
  function generatePMOReviewLoadForSearch(index,oTable,tableBody,record,delimiter,weeklyReport){
 	 rowCount2=rowCount2+1;
      var row;
      var cell;
      var fieldLength;
      //  var fields = record.split(delimiter);
      var fields = record.split("#^$");
      fieldLength = fields.length ;
      var length = fieldLength;
  
      
      row = document.createElement( "TR" );
      row.className="gridRowEven";
      tableBody.appendChild( row );
  
   
      for (var i=0; i<fieldLength; i++) {
     	 
     	
     	   if(weeklyReport == "NotEntered"){
              cell = document.createElement( "TD" );
              cell.className="gridColumn"; 
          
              cell.innerHTML =fields[i] ;
              cell.setAttribute("align","left");   
              row.appendChild( cell );  
         	 
     	 if(i=1){
              cell = document.createElement( "TD" );
              cell.className="gridColumn"; 
          
              cell.innerHTML =fields[i] ;
              cell.setAttribute("align","left");   
              row.appendChild( cell );  
         	 }
     	 if(i=2){
              cell = document.createElement( "TD" );
              cell.className="gridColumn";  
            
              cell.innerHTML =fields[i] ;
              cell.setAttribute("align","left");   
              row.appendChild( cell );  
         	 }
     	 if(i=3){
              cell = document.createElement( "TD" );
              cell.className="gridColumn";   
             
              cell.innerHTML =fields[i] ;
              cell.setAttribute("align","left");   
              row.appendChild( cell );  
         	 }
     	 if(i=4){
              cell = document.createElement( "TD" );
              cell.className="gridColumn";    
         
              cell.innerHTML =fields[i] ;
              cell.setAttribute("align","left");   
              row.appendChild( cell );  
         	 }
     	 if(i=5){
              cell = document.createElement( "TD" );
              cell.className="gridColumn";    
           
              cell.innerHTML =fields[i] ;
              cell.setAttribute("align","left");   
              row.appendChild( cell );  
         	 }
      }  	   
     	   
     	   else{
     		  	     		   	    	 
     		   	             cell = document.createElement( "TD" );
     		   	             cell.className="gridColumn"; 
     		   	         
     		   	             cell.innerHTML =fields[i] ;
     		   	             cell.setAttribute("align","left");   
     		   	             row.appendChild( cell );  
     		   	        	 
     		   	    	 if(i=1){
     		   	             cell = document.createElement( "TD" );
     		   	             cell.className="gridColumn"; 
     		   	           
     		   	             cell.innerHTML =fields[i] ;
     		   	             cell.setAttribute("align","left");   
     		   	             row.appendChild( cell );  
     		   	        	 }
     		   	    	 if(i=2){
     		   	             cell = document.createElement( "TD" );
     		   	             cell.className="gridColumn";  
     		   	           
     		   	             cell.innerHTML =fields[i] ;
     		   	             cell.setAttribute("align","left");   
     		   	             row.appendChild( cell );  
     		   	        	 }
     		   	    	 if(i=3){
     		   	             cell = document.createElement( "TD" );
     		   	             cell.className="gridColumn";   
     		   	          
     		   	             cell.innerHTML =fields[i] ;
     		   	             cell.setAttribute("align","left");   
     		   	             row.appendChild( cell );  
     		   	        	 }
     		   	    	 if(i=4){
     		   	             cell = document.createElement( "TD" );
     		   	             cell.className="gridColumn";    
     		   	          
     		   	             cell.innerHTML =fields[i] ;
     		   	             cell.setAttribute("align","left");   
     		   	             row.appendChild( cell );  
     		   	        	 }
     		   	    	 
     		   	    	if(i=5){
    		   	             cell = document.createElement( "TD" );
    		   	             cell.className="gridColumn";    
    		   	        
    		   	       cell.innerHTML =fields[i] ;
    		   	       cell.setAttribute('align','center');
    		   	       if(fields[i] == "Green"){
    	             cell.setAttribute('bgcolor','#00bf00');
    		   	       }else if(fields[i] == "Red"){
    		   	     cell.setAttribute('bgcolor','#ff0000');
    		   	       }else if(fields[i] == "Amber"){
    		   	     cell.setAttribute('bgcolor','#FFBF00');
    		   	       }
    		   	            row.appendChild( cell );  
    		   	        	 }
     		   	    	
     		   	    	if(i=6){
    		   	             cell = document.createElement( "TD" );
    		   	             cell.className="gridColumn";    
    		   	        
    		   	             cell.innerHTML =fields[i] ;
    		   	             cell.setAttribute('align','center');
    	   		   	       if(fields[i] == "Green"){
    	      	             cell.setAttribute('bgcolor','#00bf00');
    	      		   	       }else if(fields[i] == "Red"){
    	      		   	     cell.setAttribute('bgcolor','#ff0000');
    	      		   	       }else if(fields[i] == "Amber"){
    	      		   	     cell.setAttribute('bgcolor','#FFBF00');
    	      		   	       }   
    		   	             row.appendChild( cell );  
    		   	        	 }
     		   	    	if(i=7){
    		   	             cell = document.createElement( "TD" );
    		   	             cell.className="gridColumn";    
    		   	         
    		   	             cell.innerHTML =fields[i] ;
    		   	       cell.setAttribute('align','center');
 	   		   	       if(fields[i] == "Green"){
 	      	             cell.setAttribute('bgcolor','#00bf00');
 	      		   	       }else if(fields[i] == "Red"){
 	      		   	     cell.setAttribute('bgcolor','#ff0000');
 	      		   	       }else if(fields[i] == "Amber"){
 	      		   	     cell.setAttribute('bgcolor','#FFBF00');
 	      		   	       }   
    		   	             row.appendChild( cell );  
    		   	        	 }
     		   	    	if(i=8){
    		   	             cell = document.createElement( "TD" );
    		   	             cell.className="gridColumn";    
    		   	          
    		   	             cell.innerHTML =fields[i] ;
    		   	       cell.setAttribute('align','center');
 	   		   	       if(fields[i] == "Green"){
 	      	             cell.setAttribute('bgcolor','#00bf00');
 	      		   	       }else if(fields[i] == "Red"){
 	      		   	     cell.setAttribute('bgcolor','#ff0000');
 	      		   	       }else if(fields[i] == "Amber"){
 	      		   	     cell.setAttribute('bgcolor','#FFBF00');
 	      		   	       }      
    		   	             row.appendChild( cell );  
    		   	        	 }
     		   	    	if(i=9){
    		   	             cell = document.createElement( "TD" );
    		   	             cell.className="gridColumn";    
    		   	         
    		   	             cell.innerHTML =fields[i] ;
    		   	       cell.setAttribute('align','center');
 	   		   	       if(fields[i] == "Green"){
 	      	             cell.setAttribute('bgcolor','#00bf00');
 	      		   	       }else if(fields[i] == "Red"){
 	      		   	     cell.setAttribute('bgcolor','#ff0000');
 	      		   	       }else if(fields[i] == "Amber"){
 	      		   	     cell.setAttribute('bgcolor','#FFBF00');
 	      		   	       }      
    		   	             row.appendChild( cell );  
    		   	        	 }
     		   	    	if(i=10){
    		   	             cell = document.createElement( "TD" );
    		   	             cell.className="gridColumn";    
    		   	         
    		   	             cell.innerHTML =fields[i] ;
    		   	       cell.setAttribute('align','center');
 	   		   	       if(fields[i] == "Green"){
 	      	             cell.setAttribute('bgcolor','#00bf00');
 	      		   	       }else if(fields[i] == "Red"){
 	      		   	     cell.setAttribute('bgcolor','#ff0000');
 	      		   	       }else if(fields[i] == "Amber"){
 	      		   	     cell.setAttribute('bgcolor','#FFBF00');
 	      		   	       }     
    		   	             row.appendChild( cell );  
    		   	        	 }
     		   	    	if(i=11){
     		   			   cell = document.createElement( "TD" );
     		   				  cell.className="gridColumn";    
     		   				//  cell.innerHTML = "<a href='javascript:auditCommentsForPMOSearch(\"comments"+rowCount2+"\",\""+fields[11]+"\");'>View</a>";
     		   				
     		   			  cell.innerHTML = "<a href='javascript:auditCommentsForPMOSearch(\"comment"+rowCount2+"\");'>View</a>" +
     	 				  "<input type='hidden' id=\'comment"+rowCount2+"\' value=\'"+fields[11]+"\' />"; 
     	 			
     		   				  // cell.innerHTML = "<a href='javascript:auditCommentsForPMOReview(\""+rowCount2+"\",\""+fields[11]+"\");'>Comment</a>";  
     		   				 cell.setAttribute("align","left");   
     		   				 row.appendChild( cell );
     		   	  }
     		   	    	if(i=12){
    		   	             cell = document.createElement( "TD" );
    		   	             cell.className="gridColumn";    
    		   	         
    		   	             cell.innerHTML =fields[i] ;
    		   	             cell.setAttribute("align","left");   
    		   	             row.appendChild( cell );  
    		   	        	 }
     		   	    	if(i=13){
    		   	             cell = document.createElement( "TD" );
    		   	             cell.className="gridColumn";    
    		   	      
    		   	             cell.innerHTML =fields[i] ;
    		   	             cell.setAttribute("align","left");   
    		   	             row.appendChild( cell );  
    		   	        	 }
     		   	    	
     	 }
      
      
   	 
   

 }
  }
  
  function getLoadForPMOWeelyReview()
  {
      var oTable = document.getElementById("tblEmpPMOWeeklyReportList");
      $('span.pagination').empty().remove();
   //   document.getElementById("menu-popup").style.display = 'none';
      document.getElementById("pmoProjectDetailsReport").style.display = 'none';
      
      ClrTable(oTable);
    //  document.getElementById("saveinvoicebtn").style.display='none';
      var customerId=document.getElementById("customerName").value;
      var year=document.getElementById("year").value;
      var month=document.getElementById("month").value;
      
      var projectId = document.getElementById("projectName").value;
      var costModel = document.getElementById("costModel").value;
      var week=document.getElementById("week").value;
      var weeklyReport=document.getElementById("weeklyReport").value;
     var weeksCount = document.getElementById("weeksCount").value;
      
      if(year.trim().length==0 ){
      	alert('Please enter year');
      	return false;
      }
      if(month==''){
      	alert('Please select month');
      	return false;
      }
   
      var myObj = {};  
      myObj["customerId"] = customerId;
      myObj["year"] = year;
      myObj["month"] = month;
      myObj["projectId"] = projectId;
      myObj["costModel"] = costModel;
      myObj["week"] = week;
      myObj["weeklyReport"] = weeklyReport;
      myObj["weeksCount"] = weeksCount;
   //   myObj["flag"] = flag;
      var json = JSON.stringify(myObj);
  
      var url = CONTENXT_PATH+"/doGetLoadForPMOReview.action?q="+Math.random();
      document.getElementById("loadInvoiceHrs").style.display = 'block';
      $.ajax({
  	        
  	     	
  	     	url:url,
  	         data:{jsonData: json},
  	         contentType: 'application/json',
  	         type: 'GET',
  	         context: document.body,
  	         success: function(responseText) {   
  	        	// alert(responseText)
  	        	 document.getElementById("loadInvoiceHrs").style.display = 'none';
  	        	 
           displayLoadFromRes(responseText);      
         //  document.getElementById("menu-popup").style.display = 'block';
      	 document.getElementById("pmoProjectDetailsReport").style.display = 'block';
  	               
  	         },
  	         error: function(e){
  	             document.getElementById('load').style.display='none';
  	             document.getElementById('resultMessage').innerHTML="Please try again later";
  	             
  	         }
  	     });
      
      

  }


  function displayLoadFromRes(response){
      var tableId = document.getElementById("tblEmpPMOWeeklyReportList"); 
      var weeklyReport = document.getElementById("weeklyReport").value;
    
     if(weeklyReport == "Added" && (document.getElementById("userId").value != 'ljampana' && document.getElementById("userId").value != 'vkandregula')){
    	 
   	   var headerFields = new Array("SNo","CustomerName","ProjectName","StartDate","EndDate","Health","Scope","Schedule","Invoice","Risk","Resources","Comments");
     }
     else{
    	   var headerFields = new Array("SNo","CustomerName","ProjectName","StartDate","EndDate","Health","Scope","Schedule","Invoice","Risk","Resources","Comments","Action");
     }
       
      var dataArray = response;
      ParseAndGenerateHTMLLoad(tableId,dataArray, headerFields);
  }
   
  function ParseAndGenerateHTMLLoad(oTable,responseString,headerFields) {
      var fieldDelimiter = "#^$";
      var recordDelimiter = "*@!";   
      var records = responseString.split(recordDelimiter); 
      generatePMOReviewLoad(oTable,headerFields,records,fieldDelimiter);
  }



  function generatePMOReviewLoad(oTable, headerFields,records,fieldDelimiter) {	

      tbody = oTable.childNodes[0];    
      var tbody = document.createElement("TBODY");
      oTable.appendChild(tbody);
      
      generateTableHeaderForReview(tbody,headerFields);
      if(records == "NoData"){
  		 generateNoRecordsFound(tbody,oTable,headerFields.length);
  	 }else{
      var rowlength;
      rowlength = records.length;
      if(rowlength >0 && records!=""){
          for(var i=0;i<rowlength-1;i++) {
         	 generatePMOReviewLoadFromRes(i,oTable,tbody,records[i],fieldDelimiter);
         	 
//                  if(flag == "save"){
//                 	 generateSavePMOReviewLoadFromRes(i,oTable,tbody,records[i],fieldDelimiter);	 
//                  }else{
//                  generateUpdatePMOReviewLoadFromRes(i,oTable,tbody,records[i],fieldDelimiter);
//          }
          }
          
      } else {
          generateNoRecordsFound(tbody,oTable,headerFields.length);
      }
      generateReviewFooter(tbody,oTable,headerFields.length);
      pagerOption();
  	 }
    
  }
  function generateTableHeaderForReview(tableBody,headerFields) {
 	    var row;
 	    var cell;
 	    row = document.createElement( "TR" );
 	    row.className="gridHeader";
 	    tableBody.appendChild( row );
 	    for (var i=0; i<headerFields.length; i++) {
 	        cell = document.createElement( "TD" );
 	        cell.className="gridHeader";
 	        row.appendChild( cell );

 	        cell.setAttribute("width","10000px");
 	        cell.innerHTML = headerFields[i];
 	    }
 	}
  
  
  function selectOnlyThis(row) {
 	
 	    $('input.healthchk'+row).on('change', function() {
     $('input.healthchk'+row).not(this).prop('checked', false);  
 });
 	 $('input.scopechk'+row).on('change', function() {
 		    $('input.scopechk'+row).not(this).prop('checked', false);  
 		});
 	 $('input.schedulechk'+row).on('change', function() {
 		    $('input.schedulechk'+row).not(this).prop('checked', false);  
 		});
 	 $('input.invoicechk'+row).on('change', function() {
 		    $('input.invoicechk'+row).not(this).prop('checked', false);  
 		});
 	 $('input.riskchk'+row).on('change', function() {
 		    $('input.riskchk'+row).not(this).prop('checked', false);  
 		});
 	 $('input.resourceschk'+row).on('change', function() {
 		    $('input.resourceschk'+row).not(this).prop('checked', false);  
 		});

 	       
 	}
  
  
 var rowCount1 = 0

 function generatePMOReviewLoadFromRes(index,oTable,tableBody,record,delimiter){
 	rowCount1=rowCount1+1;
      var row;
      var cell;
      var fieldLength;
      
      //  var fields = record.split(delimiter);
      var fields = record.split("#^$");
      fieldLength = fields.length ;
      var length = fieldLength;
      
      row = document.createElement( "TR" );
      row.className="gridRowEven";
      tableBody.appendChild( row );
      
      var count = 0;
    
      for (var i=0; i<fieldLength-5; i++) {
     
     	 
              cell = document.createElement( "TD" );
              cell.className="gridColumn"; 
          
              cell.innerHTML =fields[i]  ;
              cell.setAttribute("align","left");   
              row.appendChild( cell );  
         	 
     	 if(i=1){
              cell = document.createElement( "TD" );
              cell.className="gridColumn"; 
          
              cell.innerHTML =fields[i] ;
              cell.setAttribute("align","left");   
              row.appendChild( cell );  
         	 }
     	 if(i=2){
              cell = document.createElement( "TD" );
              cell.className="gridColumn";  
         
              cell.innerHTML =fields[i] ;
              cell.setAttribute("align","left");   
              row.appendChild( cell );  
         	 }
     	 if(i=3){
              cell = document.createElement( "TD" );
              cell.className="gridColumn";   
           
              cell.innerHTML =fields[i] ;
              cell.setAttribute("align","left");   
              row.appendChild( cell );  
         	 }
     	 if(i=4){
              cell = document.createElement( "TD" );
              cell.className="gridColumn";    
            
              cell.innerHTML =fields[i] ;
              cell.setAttribute("align","left");   
              row.appendChild( cell );  
         	 }
     
      
     	 
 if(i=5){
 	
      cell = document.createElement( "TD" );
    cell.className="gridColumn"; 
    
    if(fields[i] == "-"){
    cell.innerHTML = "<div class='squaredGreen'>"+"<input type='checkbox' class='healthchk"+rowCount1+"'  onclick='selectOnlyThis("+rowCount1+")' id='healthGreen"+rowCount1+"'  />"+
    "<label for='healthGreen"+rowCount1+"'></label>"+
 	"</div>" + "<div class='squaredAmber'>"+"<input type='checkbox'  class='healthchk"+rowCount1+"' onclick='selectOnlyThis("+rowCount1+")' id='healthAmber"+rowCount1+"' />"+
 	   "<label for='healthAmber"+rowCount1+"'></label>"+
 		"</div>" + "<div class='squaredRed'>"+"<input type='checkbox' class='healthchk"+rowCount1+"' onclick='selectOnlyThis("+rowCount1+")' id='healthRed"+rowCount1+"' />"+
    "<label for='healthRed"+rowCount1+"'></label>"+
 	"</div>";
 }else{
 	
 	
    if(fields[i] == "Green"){
 	   cell.innerHTML = "<div class='squaredGreen'>"+"<input class='healthchk"+rowCount1+"'  onclick='selectOnlyThis("+rowCount1+")''  type='checkbox' checked = 'true' id='healthGreen"+rowCount1+"'  />"+
 	   "<label for='healthGreen"+rowCount1+"'></label>"+
 		"</div>"  + "<div class='squaredAmber'>"+"<input type='checkbox' class='healthchk"+rowCount1+"'  onclick='selectOnlyThis("+rowCount1+")'  id='healthAmber"+rowCount1+"'  />"+
 		   "<label for='healthAmber"+rowCount1+"'></label>"+
 			"</div>" + "<div class='squaredRed'>"+"<input type='checkbox' class='healthchk"+rowCount1+"'  onclick='selectOnlyThis("+rowCount1+")'  id='healthRed"+rowCount1+"'  />"+
 	   "<label for='healthRed"+rowCount1+"'></label>"+
 		"</div>";
 	
 	//   document.getElementById("healthGreen"+rowCount1).Checked = true;
    }
    else if(fields[i] == "Red"){
 	   cell.innerHTML = "<div class='squaredGreen'>"+"<input type='checkbox' class='healthchk"+rowCount1+"'  onclick='selectOnlyThis("+rowCount1+")'  id='healthGreen"+rowCount1+"'  />"+
 	   "<label for='healthGreen"+rowCount1+"'></label>"+
 		"</div>"  + "<div class='squaredAmber'>"+"<input type='checkbox' class='healthchk"+rowCount1+"'  onclick='selectOnlyThis("+rowCount1+")' id='healthAmber"+rowCount1+"'  />"+
 		   "<label for='healthAmber"+rowCount1+"'></label>"+
 			"</div>" + "<div class='squaredRed'>"+"<input type='checkbox' checked = 'true' class='healthchk"+rowCount1+"'  onclick='selectOnlyThis("+rowCount1+")' id='healthRed"+rowCount1+"'  />"+
 	   "<label for='healthRed"+rowCount1+"'></label>"+
 		"</div>" ;
 	   // $('#healthRed'+rowCount1).attr("checked", true);
 	 //  document.getElementById("healthRed"+rowCount1).checked = true;
    }
    else if(fields[i] == "Amber"){
 	   cell.innerHTML = "<div class='squaredGreen'>"+"<input type='checkbox' class='healthchk"+rowCount1+"'  onclick='selectOnlyThis("+rowCount1+")'  id='healthGreen"+rowCount1+"'  />"+
 	   "<label for='healthGreen"+rowCount1+"'></label>"+
 		"</div>"  + "<div class='squaredAmber'>"+"<input type='checkbox' class='healthchk"+rowCount1+"'  onclick='selectOnlyThis("+rowCount1+")' checked = 'true' id='healthAmber"+rowCount1+"'  />"+
 		   "<label for='healthAmber"+rowCount1+"'></label>"+
 			"</div>" + "<div class='squaredRed'>"+"<input type='checkbox' class='healthchk"+rowCount1+"'  onclick='selectOnlyThis("+rowCount1+")'  id='healthRed"+rowCount1+"'  />"+
 	   "<label for='healthRed"+rowCount1+"'></label>"+
 		"</div>";
 	   //  $('#healthAmber'+rowCount1).attr("checked", true);
 	 //  document.getElementById("healthAmber"+rowCount1).checked = true;
    }
 }
     cell.setAttribute("align","left");
    row.appendChild( cell );
 }
 if(i=6){
    cell = document.createElement( "TD" );
    cell.className="gridColumn"; 
  
    if(fields[i] == "-"){
 	   
    cell.innerHTML = "<div class='squaredGreen'>"+"<input type='checkbox' class='scopechk"+rowCount1+"' onclick='selectOnlyThis("+rowCount1+")' id='scopeGreen"+rowCount1+"' name='group2[]' />"+
    "<label for='scopeGreen"+rowCount1+"'></label>"+
 	"</div>" + "<div class='squaredAmber'>"+"<input type='checkbox' class='scopechk"+rowCount1+"' onclick='selectOnlyThis("+rowCount1+")' id='scopeAmber"+rowCount1+"' name='group2[]' />"+
 	   "<label for='scopeAmber"+rowCount1+"'></label>"+
 		"</div>" + "<div class='squaredRed'>"+"<input type='checkbox' class='scopechk"+rowCount1+"' onclick='selectOnlyThis("+rowCount1+")' id='scopeRed"+rowCount1+"' name='group2[]' />"+
 	   "<label for='scopeRed"+rowCount1+"'></label>"+
 		"</div>" ;
    }else{
 	 
   
    if(fields[i] == "Green"){
 	   cell.innerHTML = "<div class='squaredGreen'>"+"<input type='checkbox' checked = 'true' class='scopechk"+rowCount1+"' onclick='selectOnlyThis("+rowCount1+")' id='scopeGreen"+rowCount1+"'  />"+
 	   "<label for='scopeGreen"+rowCount1+"'></label>"+
 		"</div>"  + "<div class='squaredAmber'>"+"<input type='checkbox' class='scopechk"+rowCount1+"' onclick='selectOnlyThis("+rowCount1+")' id='scopeAmber"+rowCount1+"'  />"+
 		   "<label for='scopeAmber"+rowCount1+"'></label>"+
 			"</div>" + "<div class='squaredRed'>"+"<input type='checkbox' class='scopechk"+rowCount1+"' onclick='selectOnlyThis("+rowCount1+")' id='scopeRed"+rowCount1+"'  />"+
 		   "<label for='scopeRed"+rowCount1+"'></label>"+
 			"</div>";
 	   //  $('#scopeGreen'+rowCount1).attr("checked", true);
 	 //  document.getElementById("scopeGreen"+rowCount1).checked = true;
    }
    else if(fields[i] == "Red"){
 	   cell.innerHTML = "<div class='squaredGreen'>"+"<input type='checkbox' class='scopechk"+rowCount1+"' onclick='selectOnlyThis("+rowCount1+")'  id='scopeGreen"+rowCount1+"' name='group5[]' />"+
 	   "<label for='scopeGreen"+rowCount1+"'></label>"+
 		"</div>" + "<div class='squaredAmber'>"+"<input type='checkbox' class='scopechk"+rowCount1+"' onclick='selectOnlyThis("+rowCount1+")' id='scopeAmber"+rowCount1+"' name='group5[]' />"+
 		   "<label for='scopeAmber"+rowCount1+"'></label>"+
 			"</div>" + "<div class='squaredRed'>"+"<input type='checkbox' checked = 'true' class='scopechk"+rowCount1+"' onclick='selectOnlyThis("+rowCount1+")' id='scopeRed"+rowCount1+"' name='group5[]' />"+
 		   "<label for='scopeRed"+rowCount1+"'></label>"+
 			"</div>" ;
 	   //  $('#scopeRed'+rowCount1).attr("checked", true);
 	 //  document.getElementById("scopeRed"+rowCount1).checked = true;
    }
    else if(fields[i] == "Amber"){
 	   cell.innerHTML = "<div class='squaredGreen'>"+"<input type='checkbox' class='scopechk"+rowCount1+"' onclick='selectOnlyThis("+rowCount1+")'  id='scopeGreen"+rowCount1+"'  />"+
 	   "<label for='scopeGreen"+rowCount1+"'></label>"+
 		"</div>" + "<div class='squaredAmber'>"+"<input type='checkbox' checked = 'true' class='scopechk"+rowCount1+"' onclick='selectOnlyThis("+rowCount1+")' id='scopeAmber"+rowCount1+"'  />"+
 		   "<label for='scopeAmber"+rowCount1+"'></label>"+
 			"</div>" + "<div class='squaredRed'>"+"<input type='checkbox' class='scopechk"+rowCount1+"' onclick='selectOnlyThis("+rowCount1+")' id='scopeRed"+rowCount1+"'  />"+
 		   "<label for='scopeRed"+rowCount1+"'></label>"+
 			"</div>" ;
 	   //  $('#scopeAmber'+rowCount1).attr("checked", true);
 	 //  document.getElementById("scopeAmber"+rowCount1).checked = true;
    }
 }
    cell.setAttribute("align","left");
    row.appendChild( cell );
 }

 if(i=7){
    cell = document.createElement( "TD" );
    cell.className="gridColumn"; 
    if(fields[i] == "-"){
 	 
    cell.innerHTML = "<div class='squaredGreen'>"+"<input type='checkbox'  class='schedulechk"+rowCount1+"' onclick='selectOnlyThis("+rowCount1+")' id='scheduleGreen"+rowCount1+"'  />"+
    "<label for='scheduleGreen"+rowCount1+"'></label>"+
 	"</div>" + "<div class='squaredAmber'>"+"<input type='checkbox' class='schedulechk"+rowCount1+"' onclick='selectOnlyThis("+rowCount1+")' id='scheduleAmber"+rowCount1+"'  />"+
 	   "<label for='scheduleAmber"+rowCount1+"'></label>"+
 		"</div>" + "<div class='squaredRed'>"+"<input type='checkbox' class='schedulechk"+rowCount1+"' onclick='selectOnlyThis("+rowCount1+")' id='scheduleRed"+rowCount1+"' />"+
 	   "<label for='scheduleRed"+rowCount1+"'></label>"+
 		"</div>" ;
    }else{
 	
    if(fields[i] == "Green"){
 	   
 	   cell.innerHTML = "<div class='squaredGreen'>"+"<input type='checkbox' checked = 'true' class='schedulechk"+rowCount1+"' onclick='selectOnlyThis("+rowCount1+")' id='scheduleGreen"+rowCount1+"'  />"+
 	   "<label for='scheduleGreen"+rowCount1+"'></label>"+
 		"</div>" + "<div class='squaredAmber'>"+"<input type='checkbox' class='schedulechk"+rowCount1+"' onclick='selectOnlyThis("+rowCount1+")' id='scheduleAmber"+rowCount1+"'  />"+
 		   "<label for='scheduleAmber"+rowCount1+"'></label>"+
 			"</div>" + "<div class='squaredRed'>"+"<input type='checkbox' id='scheduleRed"+rowCount1+"' class='schedulechk"+rowCount1+"' onclick='selectOnlyThis("+rowCount1+")'  />"+
 		   "<label for='scheduleRed"+rowCount1+"'></label>"+
 			"</div>" ;
 	   
 	   //  $('#scheduleGreen'+rowCount1).attr("checked", true);
 	  // document.getElementById("scheduleGreen"+rowCount1).checked = true;
    }
    else if(fields[i] == "Red"){
 	   cell.innerHTML = "<div class='squaredGreen'>"+"<input type='checkbox' class='schedulechk"+rowCount1+"' onclick='selectOnlyThis("+rowCount1+")' id='scheduleGreen"+rowCount1+"' />"+
 	   "<label for='scheduleGreen"+rowCount1+"'></label>"+
 		"</div>" + "<div class='squaredAmber'>"+"<input type='checkbox' class='schedulechk"+rowCount1+"' onclick='selectOnlyThis("+rowCount1+")' id='scheduleAmber"+rowCount1+"' />"+
 		   "<label for='scheduleAmber"+rowCount1+"'></label>"+
 			"</div>" + "<div class='squaredRed'>"+"<input type='checkbox' class='schedulechk"+rowCount1+"' onclick='selectOnlyThis("+rowCount1+")' checked = 'true' id='scheduleRed"+rowCount1+"' />"+
 		   "<label for='scheduleRed"+rowCount1+"'></label>"+
 			"</div>" ;
 	   
 	   //  $('#scheduleRed'+rowCount1).attr("checked", true);
 	  // document.getElementById("scheduleRed"+rowCount1).checked = true;
    }
    else if(fields[i] == "Amber"){
 	   
 	   cell.innerHTML = "<div class='squaredGreen'>"+"<input type='checkbox' class='schedulechk"+rowCount1+"' onclick='selectOnlyThis("+rowCount1+")' id='scheduleGreen"+rowCount1+"'  />"+
 	   "<label for='scheduleGreen"+rowCount1+"'></label>"+
 		"</div>" + "<div class='squaredAmber'>"+"<input type='checkbox' class='schedulechk"+rowCount1+"' onclick='selectOnlyThis("+rowCount1+")' checked = 'true' id='scheduleAmber"+rowCount1+"' />"+
 		   "<label for='scheduleAmber"+rowCount1+"'></label>"+
 			"</div>" + "<div class='squaredRed'>"+"<input type='checkbox' class='schedulechk"+rowCount1+"' onclick='selectOnlyThis("+rowCount1+")' id='scheduleRed"+rowCount1+"'  />"+
 		   "<label for='scheduleRed"+rowCount1+"'></label>"+
 			"</div>" ;
 	   
 	   //  $('#scheduleAmber'+rowCount1).attr("checked", true);
 	 //  document.getElementById("scheduleAmber"+rowCount1).checked = true;
    }
    }
    cell.setAttribute("align","left");
    row.appendChild( cell );

 }
 if(i=8){
 	
    cell = document.createElement( "TD" );
    cell.className="gridColumn"; 
    if(fields[i] == "-"){
 	   
    cell.innerHTML = "<div class='squaredGreen'>"+"<input type='checkbox' class='invoicechk"+rowCount1+"'  onclick='selectOnlyThis("+rowCount1+")' id='invoiceGreen"+rowCount1+"'  />"+
    "<label for='invoiceGreen"+rowCount1+"'></label>"+
 	"</div>" + "<div class='squaredAmber'>"+"<input type='checkbox' class='invoicechk"+rowCount1+"'  onclick='selectOnlyThis("+rowCount1+")' id='invoiceAmber"+rowCount1+"'  />"+
 	   "<label for='invoiceAmber"+rowCount1+"'></label>"+
 		"</div>" + "<div class='squaredRed'>"+"<input type='checkbox' class='invoicechk"+rowCount1+"'  onclick='selectOnlyThis("+rowCount1+")' id='invoiceRed"+rowCount1+"' />"+
 	   "<label for='invoiceRed"+rowCount1+"'></label>"+
 		"</div>" ;
    }else{
 	  
 	
    if(fields[i] == "Green"){
 	   cell.innerHTML = "<div class='squaredGreen'>"+"<input type='checkbox' class='invoicechk"+rowCount1+"'  onclick='selectOnlyThis("+rowCount1+")' checked = 'true' id='invoiceGreen"+rowCount1+"' name='check' />"+
 	   "<label for='invoiceGreen"+rowCount1+"'></label>"+
 		"</div>"  + "<div class='squaredAmber'>"+"<input type='checkbox' class='invoicechk"+rowCount1+"'  onclick='selectOnlyThis("+rowCount1+")' id='invoiceAmber"+rowCount1+"' name='check' />"+
 		   "<label for='invoiceAmber"+rowCount1+"'></label>"+
 			"</div>" + "<div class='squaredRed'>"+"<input type='checkbox' class='invoicechk"+rowCount1+"'  onclick='selectOnlyThis("+rowCount1+")' id='invoiceRed"+rowCount1+"' name='check' />"+
 		   "<label for='invoiceRed"+rowCount1+"'></label>"+
 			"</div>";
 	   //   $('#invoiceGreen'+rowCount1).attr("checked", true);
 	  // document.getElementById("invoiceGreen"+rowCount1).checked = true;
    }
    else if(fields[i] == "Red"){
 	   cell.innerHTML = "<div class='squaredGreen'>"+"<input type='checkbox' class='invoicechk"+rowCount1+"'  onclick='selectOnlyThis("+rowCount1+")'  id='invoiceGreen"+rowCount1+"' name='check' />"+
 	   "<label for='invoiceGreen"+rowCount1+"'></label>"+
 		"</div>" + "<div class='squaredAmber'>"+"<input type='checkbox' class='invoicechk"+rowCount1+"'  onclick='selectOnlyThis("+rowCount1+")' id='invoiceAmber"+rowCount1+"' name='check' />"+
 		   "<label for='invoiceAmber"+rowCount1+"'></label>"+
 			"</div>" + "<div class='squaredRed'>"+"<input type='checkbox' checked = 'true' class='invoicechk"+rowCount1+"'  onclick='selectOnlyThis("+rowCount1+")' id='invoiceRed"+rowCount1+"' name='check' />"+
 		   "<label for='invoiceRed"+rowCount1+"'></label>"+
 			"</div>" ;
     	//	 $('#invoiceRed'+rowCount1).attr("checked", true);
 	//  document.getElementById("invoiceRed"+rowCount1).checked = true;
    }
    else if(fields[i] == "Amber"){
 	   
 	   cell.innerHTML = "<div class='squaredGreen'>"+"<input type='checkbox'  class='invoicechk"+rowCount1+"'  onclick='selectOnlyThis("+rowCount1+")' id='invoiceGreen"+rowCount1+"' name='check' />"+
 	   "<label for='invoiceGreen"+rowCount1+"'></label>"+
 		"</div>" + "<div class='squaredAmber'>"+"<input type='checkbox' class='invoicechk"+rowCount1+"'  onclick='selectOnlyThis("+rowCount1+")' checked = 'true' id='invoiceAmber"+rowCount1+"' name='check' />"+
 		   "<label for='invoiceAmber"+rowCount1+"'></label>"+
 			"</div>" + "<div class='squaredRed'>"+"<input type='checkbox' class='invoicechk"+rowCount1+"'  onclick='selectOnlyThis("+rowCount1+")' id='invoiceRed"+rowCount1+"' name='check' />"+
 		   "<label for='invoiceRed"+rowCount1+"'></label>"+
 			"</div>" ;
 	   
     	//	 $('#invoiceAmber'+rowCount1).attr("checked", true);
 	 //  document.getElementById("invoiceAmber"+rowCount1).checked = true;
    }
    }
    
    cell.setAttribute("align","left");
    row.appendChild( cell );
 }
    if(i=9){
    cell = document.createElement( "TD" );
    cell.className="gridColumn"; 
    if(fields[i] == "-"){
 	   
    cell.innerHTML = "<div class='squaredGreen'>"+"<input type='checkbox' class='riskchk"+rowCount1+"' onclick='selectOnlyThis("+rowCount1+")' id='riskGreen"+rowCount1+"' name='check' />"+
    "<label for='riskGreen"+rowCount1+"'></label>"+
 	"</div>"  + "<div class='squaredAmber'>"+"<input type='checkbox' class='riskchk"+rowCount1+"' onclick='selectOnlyThis("+rowCount1+")' id='riskAmber"+rowCount1+"' name='check' />"+
 	   "<label for='riskAmber"+rowCount1+"'></label>"+
 		"</div>" + "<div class='squaredRed'>"+"<input type='checkbox' class='riskchk"+rowCount1+"' onclick='selectOnlyThis("+rowCount1+")' id='riskRed"+rowCount1+"' name='check' />"+
 	   "<label for='riskRed"+rowCount1+"'></label>"+
 		"</div>";
    } else{
 	  
 	
    if(fields[i] == "Green"){
 	   
 	   cell.innerHTML = "<div class='squaredGreen'>"+"<input type='checkbox' checked = 'true' class='riskchk"+rowCount1+"' onclick='selectOnlyThis("+rowCount1+")' id='riskGreen"+rowCount1+"' name='check' />"+
 	   "<label for='riskGreen"+rowCount1+"'></label>"+
 		"</div>"  + "<div class='squaredAmber'>"+"<input type='checkbox' id='riskAmber"+rowCount1+"' class='riskchk"+rowCount1+"' onclick='selectOnlyThis("+rowCount1+")' name='check' />"+
 		   "<label for='riskAmber"+rowCount1+"'></label>"+
 			"</div>" + "<div class='squaredRed'>"+"<input type='checkbox' id='riskRed"+rowCount1+"' class='riskchk"+rowCount1+"' onclick='selectOnlyThis("+rowCount1+")' name='check' />"+
 		   "<label for='riskRed"+rowCount1+"'></label>"+
 			"</div>";
 	   
 	//    $('#riskGreen'+rowCount1).attr("checked", true);
 	 //  document.getElementById('riskGreen'+rowCount1).checked = true;
    }
    else if(fields[i] == "Red"){
 	   
 	   cell.innerHTML = "<div class='squaredGreen'>"+"<input type='checkbox'  id='riskGreen"+rowCount1+"' class='riskchk"+rowCount1+"' onclick='selectOnlyThis("+rowCount1+")' name='check' />"+
 	   "<label for='riskGreen"+rowCount1+"'></label>"+
 		"</div>" + "<div class='squaredAmber'>"+"<input type='checkbox' id='riskAmber"+rowCount1+"' class='riskchk"+rowCount1+"' onclick='selectOnlyThis("+rowCount1+")' name='check' />"+
 		   "<label for='riskAmber"+rowCount1+"'></label>"+
 			"</div>" + "<div class='squaredRed'>"+"<input type='checkbox' checked = 'true' id='riskRed"+rowCount1+"' class='riskchk"+rowCount1+"' onclick='selectOnlyThis("+rowCount1+")' name='check' />"+
 		   "<label for='riskRed"+rowCount1+"'></label>"+
 			"</div>" ;
 	   
 	//   $('#riskRed'+rowCount1).attr("checked", true);
 	//   document.getElementById('riskRed'+rowCount1).checked = true;
    }
    else if(fields[i] == "Amber"){
 	   
 	   cell.innerHTML = "<div class='squaredGreen'>"+"<input type='checkbox' class='riskchk"+rowCount1+"' onclick='selectOnlyThis("+rowCount1+")'  id='riskGreen"+rowCount1+"' name='check' />"+
 	   "<label for='riskGreen"+rowCount1+"'></label>"+
 		"</div>" + "<div class='squaredAmber'>"+"<input type='checkbox' class='riskchk"+rowCount1+"' onclick='selectOnlyThis("+rowCount1+")'  checked = 'true' id='riskAmber"+rowCount1+"' name='check' />"+
 		   "<label for='riskAmber"+rowCount1+"'></label>"+
 			"</div>" + "<div class='squaredRed'>"+"<input type='checkbox' class='riskchk"+rowCount1+"' onclick='selectOnlyThis("+rowCount1+")' id='riskRed"+rowCount1+"' name='check' />"+
 		   "<label for='riskRed"+rowCount1+"'></label>"+
 			"</div>" ;
 	   
 	//    $('#riskAmber'+rowCount1).attr("checked", true);
 	//  document.getElementById('riskAmber'+rowCount1).checked = true;
    }
    }
    cell.setAttribute("align","left");
    row.appendChild( cell );
    
    }
    if(i=10){
    cell = document.createElement( "TD" );
    cell.className="gridColumn"; 
    if(fields[i] == "-"){
 	   
    cell.innerHTML = "<div class='squaredGreen'>"+"<input type='checkbox'  class='resourceschk"+rowCount1+"' onclick='selectOnlyThis("+rowCount1+")' id='resourcesGreen"+rowCount1+"' name='check' />"+
    "<label for='resourcesGreen"+rowCount1+"'></label>"+
 	"</div>" + "<div class='squaredAmber'>"+"<input type='checkbox' class='resourceschk"+rowCount1+"' id='resourcesAmber"+rowCount1+"' name='check' />"+
 	   "<label for='resourcesAmber"+rowCount1+"'></label>"+
 		"</div>" + "<div class='squaredRed'>"+"<input type='checkbox' class='resourceschk"+rowCount1+"' id='resourcesRed"+rowCount1+"' name='check' />"+
 	   "<label for='resourcesRed"+rowCount1+"'></label>"+
 		"</div>" ;
    } else{
 	   
    if(fields[i] == "Green"){
 	   
 	   cell.innerHTML = "<div class='squaredGreen'>"+"<input type='checkbox' class='resourceschk"+rowCount1+"' onclick='selectOnlyThis("+rowCount1+")'  checked = 'true' id='resourcesGreen"+rowCount1+"' name='check' />"+
 	   "<label for='resourcesGreen"+rowCount1+"'></label>"+
 		"</div>" + "<div class='squaredAmber'>"+"<input type='checkbox' id='resourcesAmber"+rowCount1+"' class='resourceschk"+rowCount1+"' onclick='selectOnlyThis("+rowCount1+")' name='check' />"+
 		   "<label for='resourcesAmber"+rowCount1+"'></label>"+
 			"</div>" + "<div class='squaredRed'>"+"<input type='checkbox' id='resourcesRed"+rowCount1+"' class='resourceschk"+rowCount1+"' onclick='selectOnlyThis("+rowCount1+")' name='check' />"+
 		   "<label for='resourcesRed"+rowCount1+"'></label>"+
 			"</div>" ;
 	   
 	  // $('#resourcesGreen'+rowCount1).attr("checked", true);
 	 //  document.getElementById('resourcesGreen'+rowCount1).checked = true;
    }
    else if(fields[i] == "Red"){
 	   
 	   cell.innerHTML = "<div class='squaredGreen'>"+"<input type='checkbox'  id='resourcesGreen"+rowCount1+"' class='resourceschk"+rowCount1+"' onclick='selectOnlyThis("+rowCount1+")' name='check' />"+
 	   "<label for='resourcesGreen"+rowCount1+"'></label>"+
 		"</div>" + "<div class='squaredAmber'>"+"<input type='checkbox' id='resourcesAmber"+rowCount1+"' class='resourceschk"+rowCount1+"' onclick='selectOnlyThis("+rowCount1+")' name='check' />"+
 		   "<label for='resourcesAmber"+rowCount1+"'></label>"+
 			"</div>" + "<div class='squaredRed'>"+"<input type='checkbox' checked = 'true' id='resourcesRed"+rowCount1+"' class='resourceschk"+rowCount1+"' onclick='selectOnlyThis("+rowCount1+")' name='check' />"+
 		   "<label for='resourcesRed"+rowCount1+"'></label>"+
 			"</div>" ;
 	   
 	 //  $('#resourcesRed'+rowCount1).attr("checked", true);
 	 //  document.getElementById('resourcesRed'+rowCount1).checked = true;
    }
    else if(fields[i] == "Amber"){
 	   
 	   cell.innerHTML = "<div class='squaredGreen'>"+"<input type='checkbox'  id='resourcesGreen"+rowCount1+"' class='resourceschk"+rowCount1+"' onclick='selectOnlyThis("+rowCount1+")' name='check' />"+
 	   "<label for='resourcesGreen"+rowCount1+"'></label>"+
 		"</div>" + "<div class='squaredAmber'>"+"<input type='checkbox' checked = 'true' id='resourcesAmber"+rowCount1+"' class='resourceschk"+rowCount1+"' onclick='selectOnlyThis("+rowCount1+")' name='check' />"+
 		   "<label for='resourcesAmber"+rowCount1+"'></label>"+
 			"</div>" + "<div class='squaredRed'>"+"<input type='checkbox'  id='resourcesRed"+rowCount1+"' class='resourceschk"+rowCount1+"' onclick='selectOnlyThis("+rowCount1+")' name='check' />"+
 		   "<label for='resourcesRed"+rowCount1+"'></label>"+
 			"</div>" ;
 	   
 	   
 	 //  $('#resourcesAmber'+rowCount1).attr("checked", true);
 	 //  document.getElementById('resourcesAmber'+rowCount1).checked = true;
    }
    
    }
    cell.setAttribute("align","left");
    row.appendChild( cell );

      }

    if(i = 11){
  	  var userId = document.getElementById("userId").value;
 	   if(fields[12] == 1){
 	
 		   cell = document.createElement( "TD" );
 			  cell.className="gridColumn";    
 			 // cell.innerHTML = "<a href='javascript:auditCommentsForPMOReviewUpdate(\"comments"+rowCount1+"\",\""+fields[11]+"\",\""+fields[16]+"\");'>View</a>";
 			 // cell.innerHTML = "<a href='javascript:auditCommentsForPMOReview(\""+rowCount1+"\",\""+fields[11]+"\");'>Comment</a>";  
 			  cell.innerHTML = "<a href='javascript:auditCommentsForPMOReviewUpdate(\"comment"+rowCount1+"\",\""+fields[16]+"\",\""+userId+"\",\""+fields[12]+"\");'>View</a>" +
 	        		"<input type='hidden' id=\'comment"+rowCount1+"\' value=\'"+fields[11]+"\' />"; 
 			  
 			  cell.setAttribute("align","left");   
 			 row.appendChild( cell );
 	   }else{
 		   if(fields[11]!=''){
 			   cell = document.createElement( "TD" );
 				  cell.className="gridColumn";       
 				  cell.innerHTML = "<a href='javascript:auditCommentsForPMOReviewUpdate(\"comment"+rowCount1+"\",\""+fields[16]+"\",\""+userId+"\",\""+fields[12]+"\");'>View</a>" +
 				  "<input type='hidden' id=\'comment"+rowCount1+"\' value=\'"+fields[11]+"\' />"; 
 				  //  "<input type='hidden' id=\'comments"+rowCount1+"\' value='' />";  
 				 cell.setAttribute("align","left");   
 				 row.appendChild( cell );
 		   }else{
 	   cell = document.createElement( "TD" );
 		  cell.className="gridColumn";       
 		  cell.innerHTML = "<a href='javascript:auditCommentsForPMOReviewSave(\""+rowCount1+"\","+fields[16]+");'>Add</a>" +
 		// "<input type='hidden' id=\'comment"+rowCount1+"\' value=\'"+fields[11]+"\' />"; 
 		   "<input type='hidden' id=\'comments"+rowCount1+"\' value='' />";  
 		 cell.setAttribute("align","left");   
 		 row.appendChild( cell );
 		   }
 	   }
    }
//alert(fields[12])

  /*  if(fields[12] == 0){
    	if(i=12){
    		cell = document.createElement( "TD" );
    		 cell.className="gridColumn";         
    		 cell.innerHTML = "<a href='javascript:saveWeeklyPMOReviews(\""+fields[13]+"\",\""+fields[14]+"\",\""+fields[15]+"\",\""+fields[3]+"\",\""+fields[4]+"\",\""+rowCount1+"\");'>Save</a>";  
    		 cell.setAttribute("align","left");   
    		 row.appendChild(cell);
    	}
    }
    else if(document.getElementById("userId").value == 'ljampana' || document.getElementById("userId").value == 'vkandregula') {
	  if(i=12){ 
	  cell = document.createElement( "TD" );
	  cell.className="gridColumn";         
	  cell.innerHTML = "<a href='javascript:updateWeeklyPMOReviews(\""+fields[13]+"\",\""+fields[14]+"\",\""+fields[15]+"\",\""+fields[3]+"\",\""+fields[4]+"\",\""+rowCount1+"\");'>Update</a>";  
	  	   cell.setAttribute("align","left");   
	  	   row.appendChild( cell );
	  }
  }*/
    if(fields[12] == 0){
    	if(i=12){
    		cell = document.createElement( "TD" );
    		 cell.className="gridColumn";         
    		 cell.innerHTML = "<input type='button' class='buttonBg' onclick='javascript:saveWeeklyPMOReviews(\""+fields[13]+"\",\""+fields[14]+"\",\""+fields[15]+"\",\""+fields[3]+"\",\""+fields[4]+"\",\""+rowCount1+"\");' value='Save' />";  
    		 cell.setAttribute("align","left");   
    		 row.appendChild(cell);
    	}
    }
    else if(document.getElementById("userId").value == 'ljampana' || document.getElementById("userId").value == 'vkandregula') {
	  if(i=12){ 
	  cell = document.createElement( "TD" );
	  cell.className="gridColumn";         
	  cell.innerHTML = "<input type='button' class='buttonBg' onclick='javascript:updateWeeklyPMOReviews(\""+fields[13]+"\",\""+fields[14]+"\",\""+fields[15]+"\",\""+fields[3]+"\",\""+fields[4]+"\",\""+rowCount1+"\");' value='Update' />";  
	  	   cell.setAttribute("align","left");   
	  	   row.appendChild( cell );
	  }
  }
  
  
    	 
//    if(i=12){
// 	  
// 	   if(fields[i] == 0){
//    
// cell = document.createElement( "TD" );
// cell.className="gridColumn";         
// cell.innerHTML = "<a href='javascript:saveWeeklyPMOReviews(\""+fields[13]+"\",\""+fields[14]+"\",\""+fields[15]+"\",\""+fields[3]+"\",\""+fields[4]+"\",\""+rowCount1+"\");'>Save</a>";  
// cell.setAttribute("align","left");   
// row.appendChild(cell);
//    }else{
// cell = document.createElement( "TD" );
// cell.className="gridColumn";         
// cell.innerHTML = "<a href='javascript:updateWeeklyPMOReviews(\""+fields[13]+"\",\""+fields[14]+"\",\""+fields[15]+"\",\""+fields[3]+"\",\""+fields[4]+"\",\""+rowCount1+"\");'>Update</a>";  
// 	   cell.setAttribute("align","left");   
// 	   row.appendChild( cell );
//    }
//      }
  }
 }




  function generateNoRecordsFound(tbody,oTable,len) {
     
      var noRecords =document.createElement("TR");
      noRecords.className="gridRowEven";
      tbody.appendChild(noRecords);
      cell = document.createElement("TD");
      cell.className="gridColumn";
      cell.colSpan = len;
      
      cell.innerHTML = "No Records Found for this Search";
      noRecords.appendChild(cell);
  }
  function generateReviewFooter(tbody,oTable,len) {
    
      var cell;
      var footer =document.createElement("TR");
      footer.className="gridPager";
      footer.setAttribute("Id", "taskFooter");
      tbody.appendChild(footer);
      cell = document.createElement("TD");
      cell.className="gridFooter";
      

      // cell.colSpan = "7";
      
      cell.colSpan = len;
      
         
     

      footer.appendChild(cell);
  }

  function countCharForComments(val,displayId) {
      var len = val.value.length;
      if (len >= 1500) {
          val.value = val.value.substring(0, 1500);
          document.getElementById(displayId).innerHTML=(0)+'\\1500';
      } else {
          document.getElementById(displayId).innerHTML=(1500-len)+'\\1500';
      }
  }

  function auditCommentsForPMOReviewSave(row,id){
      // alert("type---"+type);
      document.getElementById('resultMessage1').innerHTML ='';
      document.getElementById('descriptionCount').innerHTML ='';
    
      document.getElementById("reviewId").value = id;
    
      document.getElementById("headerLabel").style.color="white";
     
     
      showRow('addTr');
     
      hideRow('updateTr');
      
      var overlay = document.getElementById('commentsOverlay');
      var specialBox = document.getElementById('commentsSpecialBox');
      overlay.style.opacity = .8;
      if(overlay.style.display == "block"){
          overlay.style.display = "none";
          specialBox.style.display = "none";
      }
      else {
          overlay.style.display = "block";
          specialBox.style.display = "block";
          
//        if(document.getElementById("comments"+row).value != '1'){
//     	   document.getElementById('comments').value = '';
//     	   }
//               
              var desc =  document.getElementById('comments'+row).value;
              document.getElementById('row').value = row;
              
              document.getElementById('comments').value=desc;
              document.getElementById('commentsId').value='comments'+row;
             
             
           // alert(status+"anand"+curretRole) 
          
          
         
             
          
      }
              



  }
  
  
  function auditCommentsForPMOReviewUpdate(commentId,id,userId,flag){
    
      
      document.getElementById('resultMessage1').innerHTML ='';
      document.getElementById('descriptionCount').innerHTML ='';
    
      var comments =  document.getElementById(commentId).value;
  	document.getElementById('comments').value=comments;
      document.getElementById('commentsId').value=commentId;
      document.getElementById("reviewId").value = id;
      
      if(flag == 1){
     if(userId == "ljampana" || userId == "vkandregula"){
    	 showRow('updateTr');
     }
     else{
    	 hideRow('updateTr');
     }
      }else{
    	 showRow('updateTr'); 
     }
     
      hideRow('addTr');
      
      var overlay = document.getElementById('commentsOverlay');
      var specialBox = document.getElementById('commentsSpecialBox');
     // alert('hi');
      overlay.style.opacity = .8;
      if(overlay.style.display == "block"){
          overlay.style.display = "none";
          specialBox.style.display = "none";
      }
      else {
          overlay.style.display = "block";
          specialBox.style.display = "block";
          
      }
      
  }
  
  function auditCommentsForPMOSearch(commentId){
 	
      //document.getElementById("reviewId").value = id;
     document.getElementById('resultMessage1').innerHTML ='';
     document.getElementById('descriptionCount').innerHTML ='';
   
   
   
     document.getElementById("headerLabel").style.color="white";
    
    
   
     
     var overlay = document.getElementById('commentsOverlay');
     var specialBox = document.getElementById('commentsSpecialBox');
     overlay.style.opacity = .8;
     if(overlay.style.display == "block"){
         overlay.style.display = "none";
         specialBox.style.display = "none";
     }
     else {
         overlay.style.display = "block";
         specialBox.style.display = "block";
         
         
         	hideRow('addTr');
        	    
        	     hideRow('updateTr');
        	     
        	     var comments =  document.getElementById(commentId).value;
        	   	document.getElementById('comments').value=comments;
        	       document.getElementById('commentsId').value=commentId;
        	     
        	 // document.getElementById('comments').value = comment;
//             var desc =  document.getElementById(commentId).value;
//           
//            // alert("desc---"+desc);
//             document.getElementById('comments').value=desc;
//             document.getElementById('commentsId').value=commentId;
      }
         
  }
  function closeCommentsForPMOReview(){
    
     
  	document.getElementById('resultMessage1').innerHTML='';
      var overlay = document.getElementById('commentsOverlay');
      var specialBox = document.getElementById('commentsSpecialBox');
      overlay.style.opacity = .8;
      if(overlay.style.display == "block"){
          overlay.style.display = "none";
          specialBox.style.display = "none";
      }
      else {
          overlay.style.display = "block";
          specialBox.style.display = "block";
          
    
          
      }
              



  }
  function addCommentsForPMOReview(){
  var commentId = document.getElementById("commentsId").value;
  var comment = document.getElementById('comments').value;
  var id = document.getElementById("reviewId").value;
  var row = document.getElementById('row').value;
//  document.getElementById("resultMessage1").innerHTML="<font color='green' size='2'>Added,Please Click On Save</font>";
  
  
 // window.location='../Reviews/addPMOWeeklyReviewComments.action?comments='+comment+"&reviewId="+id;
  
  var mainjson={};
  mainjson["comment"]=comment;
  mainjson["id"]=id;
 document.getElementById("load1").style.display = 'block';
 var json = JSON.stringify(mainjson);




 var url="updateWeeklyReviewComment.action?rand="+Math.random();

 $.ajax({

 	url:url,
  data:{jsonData: json},
  contentType: 'application/json',
  type: 'GET',
  context: document.body,
  success: function(responseText) {
 	 document.getElementById("load1").style.display = 'none';
 		if(responseText=="success"){
 			document.getElementById("resultMessage1").innerHTML="<font color='green' size='2'>Added successfully</font>";
 			// getLoadForPMOWeelyReview();
 			// auditCommentsForPMOReviewSave(row,id);
 			 document.getElementById(commentId).value=comment;
 		}else{
 			document.getElementById("resultMessage1").innerHTML="<font color='red' size='2'>Try again later</font>";
 		}
 			
  },
  error: function(e){
     alert('error');
  }
 });
  

	
  
  }
  	
  function updateCommentsForPMOReview(){
 		var commentsId =  document.getElementById("commentsId").value;
 		 var comment=document.getElementById('comments').value;
 		var id = document.getElementById("reviewId").value;
 		
 			
 		 var mainjson={};
 				 mainjson["comment"]=comment;
 				 mainjson["id"]=id;
 		 document.getElementById("load1").style.display = 'block';
 		 var json = JSON.stringify(mainjson);
 		   
 		 
 	
 		 
 		    var url="updateWeeklyReviewComment.action?rand="+Math.random();
 		
 		    $.ajax({
 		       
 		     	url:url,
 		         data:{jsonData: json},
 		         contentType: 'application/json',
 		         type: 'GET',
 		         context: document.body,
 		         success: function(responseText) {
 		        	 document.getElementById("load1").style.display = 'none';
 		        		if(responseText=="success"){
 		        			document.getElementById("resultMessage1").innerHTML="<font color='green' size='2'>Updated successfully</font>";
 		        			getLoadForPMOWeelyReview();
 		        		}else{
 		        			document.getElementById("resultMessage1").innerHTML="<font color='red' size='2'>Try again later</font>";
 		        		}
 		        			
 		         },
 		         error: function(e){
 		            alert('error');
 		         }
 		     });
 		 
 	}
 	
  function saveWeeklyPMOReviews(customerId,projectId,costModel,weekStartDate,weekEndDate,rowCount1){
 	
  	
 	 
 	 var comments = document.getElementById("comments").value;
 	
 	 var health = "-";
 	 var scope = "-";
 	 var schedule = "-";
 	 var invoice = "-";
 	 var risk = "-";
 	 var resources = "-";

 	
 		  if(document.getElementById("healthGreen"+rowCount1).checked == true){
 	   			health = "Green";
 	   		}else if(document.getElementById("healthRed"+rowCount1).checked == true){
 	   			health = "Red"
 	   		}else if(document.getElementById("healthAmber"+rowCount1).checked == true){
 	   			health = "Amber"
 	   		}
 	   		
 	   		if(document.getElementById("scopeGreen"+rowCount1).checked == true){
 	   			scope = "Green";
 	   		}else if(document.getElementById("scopeRed"+rowCount1).checked == true){
 	   			scope = "Red"
 	   		}else if(document.getElementById("scopeAmber"+rowCount1).checked == true){
 	   			scope = "Amber"
 	   		}
 	   		
 	   		if(document.getElementById("scheduleGreen"+rowCount1).checked == true){
 	   			schedule = "Green";
 	   		}else if(document.getElementById("scheduleRed"+rowCount1).checked == true){
 	   			schedule = "Red"
 	   		}else if(document.getElementById("scheduleAmber"+rowCount1).checked == true){
 	   			schedule = "Amber"
 	   		}
 	   		
 	   		if(document.getElementById("invoiceGreen"+rowCount1).checked == true){
 	   			invoice = "Green";
 	   		}else if(document.getElementById("invoiceRed"+rowCount1).checked == true){
 	   			invoice = "Red"
 	   		}else if(document.getElementById("invoiceAmber"+rowCount1).checked == true){
 	   			invoice = "Amber"
 	   		}
 	   		
 	   		if(document.getElementById("riskGreen"+rowCount1).checked == true){
 	   			risk = "Green";
 	   		}else if(document.getElementById("riskRed"+rowCount1).checked == true){
 	   			risk = "Red"
 	   		}else if(document.getElementById("riskAmber"+rowCount1).checked == true){
 	   			risk = "Amber"
 	   		}
 	   		
 	   		if(document.getElementById("resourcesGreen"+rowCount1).checked == true){
 	   			resources = "Green";
 	   		}else if(document.getElementById("resourcesRed"+rowCount1).checked == true){
 	   			resources = "Red"
 	   		}else if(document.getElementById("resourcesAmber"+rowCount1).checked == true){
 	   			resources = "Amber"
 	   		}
 	   	  
 		
// 		if(document.getElementById("scheduleGreen"+rowCount1).checked == true){
// 			schedule = "Green";
// 		}else if(document.getElementById("scheduleRed"+rowCount1).checked == true){
// 			schedule = "Red"
// 		}else if(document.getElementById("scheduleAmber"+rowCount1).checked == true){
// 			schedule = "Amber"
// 		}
// 		
// 		if(document.getElementById("invoiceGreen"+rowCount1).checked == true){
// 			invoice = "Green";
// 		}else if(document.getElementById("invoiceRed"+rowCount1).checked == true){
// 			invoice = "Red"
// 		}else if(document.getElementById("invoiceAmber"+rowCount1).checked == true){
// 			invoice = "Amber"
// 		}
// 		
 		
 		
// 		  if(document.getElementById("riskGreen"+rowCount1).checked == true){
// 			risk = "Green";
// 		}else if(document.getElementById("riskRed"+rowCount1).checked == true){
// 			risk = "Red"
// 		}else if(document.getElementById("riskAmber"+rowCount1).checked == true){
// 			risk = "Amber"
// 		}
 		  
 		 
// 		if(document.getElementById("resourcesGreen"+rowCount1).checked == true){
// 			resources = "Green";
// 		}else if(document.getElementById("resourcesRed"+rowCount1).checked == true){
// 			resources = "Red"
// 		}else if(document.getElementById("resourcesAmber"+rowCount1).checked == true){
// 			resources = "Amber"
// 		}
 	   
 	   
 	//   alert(health+"comments"+comments)
 	     
 	    // alert("fields[0]"+fields[0])
 	 
  //	var rowCount = $('#tblEmpPMOWeeklyReportList tr').length-2;
  //	alert(risk);
 	   
 	   if(health == "-"){
 		  x0p( '','Please check Health','info');
 	        return false;
 	   }
 	  if(scope == "-"){
 		  x0p( '','Please check Scope','info');
 	        return false;
 	   }
 	 if(schedule == "-"){
		  x0p( '','Please check Schedule','info');
	        return false;
	   }
 	 if(invoice == "-"){
		  x0p( '','Please check Invoice','info');
	        return false;
	   }
 	 if(risk == "-"){
		  x0p( '','Please check Risk','info');
	        return false;
	   }
	 if(resources == "-"){
		  x0p( '','Please check Resources','info');
	        return false;
	   }  
 	 if(comments == "" || comments == null){
 		 x0p( '','Please enter comments','info');
	        return false;
 	 }
 	  
 	   
  	  var myObj = {};  
      myObj["customerId"] =  customerId;
      myObj["projectId"] =  projectId;
      myObj["weekStartDate"] =  weekStartDate;
      myObj["weekEndDate"] =  weekEndDate;
      
      myObj["health"] =  health;
      myObj["scope"] =  scope;
      myObj["schedule"] =  schedule;
      myObj["invoice"] =  invoice;
      myObj["risk"] =  risk;
      myObj["resources"] =  resources;
      myObj["comments"] =  comments;
      myObj["costModel"] = costModel;
      
      var json = JSON.stringify(myObj);
//  	for(i=1;i<=rowCount;i++){
//  		alert($('#invoiceFlag'+i).prop('checked'));
//  		if($('#invoiceFlag'+i).prop('checked')){
//  		var submyObj = {};
//  		var newCustomerId=$('#newCustomerId'+i).val();
//  		var newProjectId=$('#newProjectId'+i).val();
//  		var newEmpId=$('#newEmpId'+i).val();
//  		var newCostModel=$('#newCostModel'+i).val();
//  		var newYear=$('#newYear'+i).val();
//  		var newMonth=$('#newMonth'+i).val();
//  		var projectHrs=$('#projectHrs'+i).val();
//  		var invoiceHrs=$('#invoiceHrs'+i).val();
//  		var hrsPerUtilization=$('#hrsPerUtilization'+i).val();
//  	  var comments=$('#comments'+i).val();
//  	  var EmpStatus=$('#EmpStatus'+i).val();
//  	    if(invoiceHrs.trim()=="" || parseFloat(invoiceHrs)<1){
//  	    	$('#invoiceHrs'+i ).focus();
//  	    	alert("please enter invoice hrs");
//  	    	document.getElementById("loadInvoiceHrs").style.display = 'none';
//  	    	return false;
//  	    }
//  	    submyObj["newCustomerId"] = newCustomerId;
//  	    submyObj["newProjectId"] = newProjectId;
//  	    submyObj["newEmpId"] = newEmpId;
//  	    submyObj["newCostModel"] = newCostModel;
//  	    submyObj["newYear"] = newYear;
//  	    submyObj["newMonth"] = newMonth;
//  	    submyObj["projectHrs"] = projectHrs;
//  	    submyObj["invoiceHrs"] = invoiceHrs;
//  	    submyObj["hrsPerUtilization"] = hrsPerUtilization;
//  	    submyObj["comments"] = comments;
//  	    submyObj["EmpStatus"] = EmpStatus;
//  	    mainjson[i]=(submyObj);
//  		}
//  	   // submyObj["newCustomerId"] = newCustomerId;
//  	}
  //	 var json = JSON.stringify(mainjson);
      
    //  x0p('Confirmation', 'Are you sure?', 'warning');
      
      x0p('Do you want to submit?', '', 'warning',
     	     function(button) {
     	       
     	        if(button == 'warning') {
     	            result1 = true;
     	            //alert(button)
     	        }
     	        if(button == 'cancel') { 
     	             result1 = false;
     	        }
     	        if(result1){
     	        	 document.getElementById("loadInvoiceHrs").style.display = 'block';
     $.ajax({
        
      	url:'saveWeeklyPMOReviews.action',
          data:{jsonData: json},
          contentType: 'application/json',
          type: 'GET',
          context: document.body,
          success: function(responseText) {
              if(responseText=='success'){
             	 document.getElementById("loadInvoiceHrs").style.display = 'none';
             	 getLoadForPMOWeelyReview();
              }else{
                  document.getElementById('loadInvoiceHrs').style.display='none';
                  document.getElementById('resultMessage2').innerHTML="Please try again later";
                  document.getElementById('resultMessage2').style.color="red";
              }
                
          },
          error: function(e){
              document.getElementById('loadInvoiceHrs').style.display='none';
              document.getElementById('resultMessage2').innerHTML="Please try again later";
              document.getElementById('resultMessage2').style.color="red";
          }
      });
     	        }else{
     	return false;
     }
      });
  }
  
  
  
  function updateWeeklyPMOReviews(customerId,projectId,costModel,weekStartDate,weekEndDate,rowCount1){
 	// alert(health+"rowCount1"+rowCount1)
  	
 	 
 	 var comments = document.getElementById("comments").value;
 	
 	 var health = "-";
 	 var scope = "-";
 	 var schedule = "-";
 	 var invoice = "-";
 	 var risk = "-";
 	 var resources = "-";
 	
 	
 		

 	
 		  if(document.getElementById("healthGreen"+rowCount1).checked == true){
 	   			health = "Green";
 	   		}else if(document.getElementById("healthRed"+rowCount1).checked == true){
 	   			health = "Red"
 	   		}else if(document.getElementById("healthAmber"+rowCount1).checked == true){
 	   			health = "Amber"
 	   		}
 		//	alert("health"+health)
 	   		if(document.getElementById("scopeGreen"+rowCount1).checked == true){
 	   			scope = "Green";
 	   		}else if(document.getElementById("scopeRed"+rowCount1).checked == true){
 	   			scope = "Red"
 	   		}else if(document.getElementById("scopeAmber"+rowCount1).checked == true){
 	   			scope = "Amber"
 	   		}
 	   		
 	   		if(document.getElementById("scheduleGreen"+rowCount1).checked == true){
 	   			schedule = "Green";
 	   		}else if(document.getElementById("scheduleRed"+rowCount1).checked == true){
 	   			schedule = "Red"
 	   		}else if(document.getElementById("scheduleAmber"+rowCount1).checked == true){
 	   			schedule = "Amber"
 	   		}
 	   		
 	   		if(document.getElementById("invoiceGreen"+rowCount1).checked == true){
 	   			invoice = "Green";
 	   		}else if(document.getElementById("invoiceRed"+rowCount1).checked == true){
 	   			invoice = "Red"
 	   		}else if(document.getElementById("invoiceAmber"+rowCount1).checked == true){
 	   			invoice = "Amber"
 	   		}
 	   		
 	   		if(document.getElementById("riskGreen"+rowCount1).checked == true){
 	   			risk = "Green";
 	   		}else if(document.getElementById("riskRed"+rowCount1).checked == true){
 	   			risk = "Red"
 	   		}else if(document.getElementById("riskAmber"+rowCount1).checked == true){
 	   			risk = "Amber"
 	   		}
 	   		
 	   		if(document.getElementById("resourcesGreen"+rowCount1).checked == true){
 	   			resources = "Green";
 	   		}else if(document.getElementById("resourcesRed"+rowCount1).checked == true){
 	   			resources = "Red"
 	   		}else if(document.getElementById("resourcesAmber"+rowCount1).checked == true){
 	   			resources = "Amber"
 	   		}
 	   	  
 	 
 	   
 	  // alert(health+"comments"+comments)
 	     
 	   	 if(health == "-"){
 	 		  x0p( '','Please check Health','info');
 	 	        return false;
 	 	   }
 	 	  if(scope == "-"){
 	 		  x0p( '','Please check Scope','info');
 	 	        return false;
 	 	   }
 	 	 if(schedule == "-"){
 			  x0p( '','Please check Schedule','info');
 		        return false;
 		   }
 	 	 if(invoice == "-"){
 			  x0p( '','Please check Invoice','info');
 		        return false;
 		   }
 	 	 if(risk == "-"){
 			  x0p( '','Please check Risk','info');
 		        return false;
 		   }
 		 if(resources == "-"){
 			  x0p( '','Please check Resources','info');
 		        return false;
 		   }  
 	 	 if(comments == "" || comments == null){
 	 		 x0p( '','Please enter comments','info');
 		        return false;
 	 	 }
 	 	  
 	   
 	   
  	 var myObj = {};  
      myObj["customerId"] =  customerId;
      myObj["projectId"] =  projectId;
      myObj["weekStartDate"] =  weekStartDate;
      myObj["weekEndDate"] =  weekEndDate;
      
      myObj["health"] =  health;
      myObj["scope"] =  scope;
      myObj["schedule"] =  schedule;
      myObj["invoice"] =  invoice;
      myObj["risk"] =  risk;
      myObj["resources"] =  resources;
      myObj["comments"] =  comments;
      myObj["costModel"] = costModel;
      
      var json = JSON.stringify(myObj);
    

//      x0p('Enter Your Name', '', 'warning').then(
//     		    function(button) {
//     		        if(button == 'info') {
//     		            x0p('Congratulations', 
//     		                '', 
//     		                'ok', false);
//     		        }
//     		        if(button == 'cancel') {
//     		            x0p('Canceled', 
//     		                '',
//     		                'error', false);
//     		        }
//     		    });
//      
    
//      var result;
//      x0p('Confirmation', 'Are you sure?', 'warning',
//     	     function(button) {
//     	       
//     	        if(button == 'submit') {
//     	            result = true;
//     	            alert("button"+button)
//     	        }
//     	        if(button == 'cancel') { 
//     	             result = false;
//     	        }
//      });
      
  x0p('Do you want to submit?', '', 'warning',
     	     function(button) {
     	       
     	        if(button == 'warning') {
     	            result1 = true;
     	            //alert(button)
     	        }
     	        if(button == 'cancel') { 
     	             result1 = false;
     	        }
     	        if(result1){
     	        	  document.getElementById("loadInvoiceHrs").style.display = 'block';
     $.ajax({
        
      	url:'updateWeeklyPMOReviews.action',
          data:{jsonData: json},
          contentType: 'application/json',
          type: 'GET',
          context: document.body,
          success: function(responseText) {
              if(responseText=='success'){
             	 document.getElementById("loadInvoiceHrs").style.display = 'none';
             	 getLoadForPMOWeelyReview();
              }else{
                  document.getElementById('loadInvoiceHrs').style.display='none';
                  document.getElementById('resultMessage2').innerHTML="Please try again later";
                  document.getElementById('resultMessage2').style.color="red";
              }
                
          },
          error: function(e){
              document.getElementById('loadInvoiceHrs').style.display='none';
              document.getElementById('resultMessage2').innerHTML="Please try again later";
              document.getElementById('resultMessage2').style.color="red";
          }
      });
     }else{
     	 document.getElementById('resultMessage2').innerHTML="Please try again later";
          document.getElementById('resultMessage2').style.color="red";
     
     }
  });
  }
  
  
  function getLocationsByCountry(obj){

	  var req = newXMLHttpRequest();
	  req.onreadystatechange = readyStateHandler(req, populateEmployeeLocations);
	  var url = CONTENXT_PATH+"/getEmployeeLocations.action?country="+obj.value;
	  req.open("GET",url,"true");
	  req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
	  req.send(null);
	  }


	  function populateEmployeeLocations(resXML) { 
	  var locationObj = document.getElementById("location1");
	  var country = resXML.getElementsByTagName("COUNTRY")[0];
	  //var teamMember = team.getElementsByTagName("TEAMMEMBER"); 
	  locationObj.innerHTML=" ";
	  for(var i=0;i<country.childNodes.length;i++) {
	  var location = country.childNodes[i];
	  var id =location.getElementsByTagName("LOCATION-ID")[0]; 
	  var name = location.getElementsByTagName("LOCATION-NAME")[0];
	  // alert(id.childNodes[0].nodeValue);
	  var opt = document.createElement("option");
	  opt.setAttribute("value",id.childNodes[0].nodeValue);
	  opt.appendChild(document.createTextNode(name.childNodes[0].nodeValue));
	  //alert(name.childNodes[0].nodeValue);
	  locationObj.appendChild(opt);
	  }
	  }

  