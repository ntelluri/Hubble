/**
 * 
 */

/*
 * NagaLakshmi 10/11/2019
 */

function ClrDashBordTable(myHTMLTable) {
	var tbl = myHTMLTable;

	tbl.deleteTHead();
	var lastRow = tbl.rows.length;
	while (lastRow > 0) {
		tbl.deleteRow(lastRow - 1);
		lastRow = tbl.rows.length;
	}
}

function readyStateHandlerText(req, responseTextHandler) {
	return function() {
		if (req.readyState == 4) {
			if (req.status == 200) {
				responseTextHandler(req.responseText);
			} else {
				alert("HTTP error" + req.status + " : " + req.statusText);
			}
		}
	}
}

function generateTableHeader(tableBody, headerFields) {
	var row;
	var cell;
	row = document.createElement("TR");
	row.className = "gridHeader";
	tableBody.appendChild(row);
	for (var i = 0; i < headerFields.length; i++) {
		cell = document.createElement("TD");
		cell.className = "gridHeader";
		row.appendChild(cell);

		cell.setAttribute("width", "10000px");
		cell.innerHTML = headerFields[i];
	}
}

function newXMLHttpRequest() {
	var xmlreq = false;
	if (window.XMLHttpRequest) {
		xmlreq = new XMLHttpRequest();
	} else if (window.ActiveXObject) {
		try {
			xmlreq = new ActiveXObject("MSxm12.XMLHTTP");
		} catch (e1) {
			try {
				xmlreq = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (e2) {
				xmlreq = false;
			}
		}
	}
	return xmlreq;
}

function ParseAndGenerateHTML(oTable, responseString, headerFields) {

	var start = new Date();
	var fieldDelimiter = "|";
	var recordDelimiter = "^";

	if (oTable.id == "tblImgDetailsList") {
		fieldDelimiter = "#^$";
		recordDelimiter = "*@!";
	}
	var records = responseString.split(recordDelimiter);
	generateTable(oTable, headerFields, records, fieldDelimiter);
}
function generateTable(oTable, headerFields, records, fieldDelimiter) {
	var tbody = oTable.childNodes[0];
	tbody = document.createElement("TBODY");
	oTable.appendChild(tbody);
	generateTableHeader(tbody, headerFields);
	var rowlength;
	rowlength = records.length - 1;
	if (rowlength >= 1 && records != "") {
		for (var i = 0; i < rowlength; i++) {
			if (oTable.id == "tblImgDetailsList") {

				generateRow(oTable, tbody, records[i], fieldDelimiter);
			}

		}
	} else {

		generateNoRecords(tbody, oTable);
	}

	generateFooter(tbody, oTable);
}
function generateRow(oTable, tableBody, record, delimiter) {
	// alert("In generateRow");
	var row;
	var cell;
	var fieldLength;
	var fields = record.split(delimiter);
	fieldLength = fields.length;
	var length;
	
	length = fieldLength;


	row = document.createElement("TR");
	

	var date1 = new Date(fields[8]);
	var date2 = new Date(fields[4]);


	var diffDays = parseInt((date2 - date1) / (1000 * 60 * 60 * 24), 10);
	
	if (fields[4] && diffDays < 30) {
		
		row.className = "gridRowEvenRed";
		//  row.setAttribute("style", "background: Red;");
	}else{
		row.className = "gridRowEven";
	}

	
	
	tableBody.appendChild(row);
	// alert("length..."+length);
	if (oTable.id == "tblImgDetailsList") {

		for (var i = 0; i < length - 2; i++) {

			
			cell = document.createElement( "TD" );
			 cell.className="gridColumn"; 
			 row.appendChild(cell);
			
				/*
			cell = document.createElement("TD");
			cell.className = "gridColumn";

		cell.innerHTML = fields[i];*/

			// alert(fields[2]);
			 
			 
			 if(i==1){
			
			  var anc = document.createElement("a");
			
			 anc.setAttribute("href",
			 "javascript:getpassportDetails('"+fields[9]+"')");
			 anc.appendChild(document.createTextNode(fields[1]));
			 cell.appendChild(anc);
			 
			 }
			 else{
	         
	            
	                cell.innerHTML = fields[i];   
	         }  
			 
			 
			if (fields[2] == 'F1-OPT' || fields[2] == 'F1-CPT') {

				fields[7] = "NA";
			}

			else if (fields[2] == 'H1B' || fields[2] == 'EAD'
					|| fields[2] == 'TN-VISA' || fields[2] == 'E3'
					|| fields[2] == 'GreenCard') {

				fields[5] = "NA";
				fields[6] = "NA";
			}

			else {
				fields[3] = "NA";
				fields[4] = "NA";
				fields[5] = "NA";
				fields[6] = "NA";
				fields[7] = "NA";
			}

		
			if (fields[i] != '') {
				row.appendChild(cell);
			}

		}

	}

	else {
		for (var i = 0; i < length - 1; i++) {

			cell = document.createElement("TD");
			cell.className = "gridColumn";
			cell.innerHTML = fields[i];
			if (fields[i] != '') {
				row.appendChild(cell);
			}

		}

		cell = document.createElement("TD");
		cell.className = "gridColumn";
		// cell.innerHTML = fields[8];
		cell.innerHTML = "<CENTER><img SRC='../includes/images/view.jpg' WIDTH=26 HEIGHT=23 BORDER=0 TITLE='"
				+ fields[7] + "' ALTER=''/></CENTER>";
		row.appendChild(cell);
	}
}

function generateNoRecords(tbody, oTable) {

	var noRecords = document.createElement("TR");
	noRecords.className = "gridRowEven";
	tbody.appendChild(noRecords);
	cell = document.createElement("TD");
	cell.className = "gridColumn";

	if (oTable.id == "tblImgDetailsList") {
		cell.colSpan = "10";
	}
	cell.innerHTML = "No Records Found for this Search";
	noRecords.appendChild(cell);
}

function generateFooter(tbody) {

	var cell;
	var footer = document.createElement("TR");
	footer.className = "gridPager";
	tbody.appendChild(footer);
	cell = document.createElement("TD");
	cell.className = "gridFooter";

	// cell.colSpan = "7";

	cell.colSpan = "10";

	footer.appendChild(cell);
}

function selectedStatus() {

	var imgStatus = document.getElementById("curImgStatus").value;

	if (imgStatus == -1) {
		document.getElementById("inspanId").innerHTML = "";
	} else {
		document.getElementById("inspanId").innerHTML = imgStatus;
	}

	if (imgStatus == '-1' || imgStatus == 'F1-OPT' || imgStatus == 'F1-CPT'
			|| imgStatus == 'H1B' || imgStatus == 'EAD'
			|| imgStatus == 'TN-VISA' || imgStatus == 'E3') {

		document.getElementById("I94td").style.visibility = "visible";
		document.getElementById("I94td1").style.visibility = "visible";
	}

	else {

		document.getElementById("I94td").style.visibility = "hidden";
		;
		document.getElementById("I94td1").style.visibility = "hidden";
	}

	if (imgStatus == 'F1-OPT' || imgStatus == 'F1-CPT') {

		$("#ImageStatus").show();
		$("#newImageStatus").hide();

		$("#datesTr").show();
		$("#universityTr").show();
		

	}

	else if (imgStatus == 'USCitizen') {

		$("#ImageStatus").hide();
		$("#newImageStatus").hide();
		$("#datesTr").hide();
		$("#universityTr").hide();

	}

	else if (imgStatus == 'H1B' || imgStatus == 'EAD' || imgStatus == 'TN-VISA'
			|| imgStatus == 'E3' || imgStatus == 'GreenCard') {

		$("#ImageStatus").hide();
		$("#newImageStatus").show();
		$("#datesTr").show();
		$("#universityTr").hide();

	}

	else {

		$("#ImageStatus").show();
		$("#newImageStatus").show();
		$("#datesTr").show();
		$("#universityTr").show();
	}

}

function getImgReportDashBoardByStatus() {

	var tableId = document.getElementById("tblImgDetailsList");
	ClrDashBordTable(tableId);

	var imgStatus = document.getElementById("curImgStatus").value;
	var req = newXMLHttpRequest();

	req.onreadystatechange = function() {
		document.getElementById("imgSearch").disabled = true;
		if (req.readyState == 4) {
			if (req.status == 200) {

				document.getElementById("imgDetailsReport").style.display = 'none';
				populateImgDetails(req.responseText);
				document.getElementById("imgSearch").disabled = false;
				// pagerOption();
			}
		} else {

			document.getElementById("imgDetailsReport").style.display = 'block';

		}
	};

	var url = CONTENXT_PATH + "/getImmigrationDetails.action?imgStatus="
			+ imgStatus;
	req.open("GET", url, "true");
	req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	req.send(null);

}

function populateImgDetails(response) {

	var oTable = document.getElementById("tblImgDetailsList");
	ClrDashBordTable(oTable);

	var dataArray = response;

	var headerFields = new Array("SNO", "EmpName", "Img&nbsp;Status",
			"Start&nbsp;Date", "Expiry&nbsp;Date", "SevisNumber", "EADNumber",
			"ApplicationNumber");

	ParseAndGenerateHTML(oTable, dataArray, headerFields);

}

function getpassportDetails(id) {
	// alert("id-->"+id+"----------- empname"+empName);
	var req = newXMLHttpRequest();
	req.onreadystatechange = readyStateHandlerText(req,
			popuppassportDetailsWindow);

	var url = CONTENXT_PATH + "/popuppassportDetailsWindow.action?id=" + id;
	req.open("GET", url, "true");
	req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	req.send(null);

}

function popuppassportDetailsWindow(text) {
	//alert(text);

	var background = "#3E93D4";
	var title = "Passport Details";
	var text1 = text;
	var size = text1.length;

	// alert("text "+text1);

	// Now create the HTML code that is required to make the popup
	var content = "<html><head><title>" + title
			+ "</title></head>\
    <body bgcolor='" + background
			+ "' style='color:white;'>" + text1 + "<br />\
    </body></html>";

	// alert("text1"+text1);

	var str = text1;
	var strArray = str.split("^");

	// alert("size "+content.length);
	var indexof = (content.indexOf("^") + 1);
	var lastindexof = (content.lastIndexOf("^"));
	// alert("indexof"+indexof+"-------"+"lastindexof"+lastindexof);

	popup = window
			.open(
					"",
					"window",
					"channelmode=0,width=400,height=250,top=200,left=350,scrollbars=yes,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no");
	if (content.indexOf("^")) {
		
		if(strArray[10] == 'F1-OPT' || strArray[10] == 'F1-CPT' )
		{
		popup.document.write("<b>PassPort StartDate:</b>"
				+ content.substr(0, content.indexOf("^")));
		popup.document.write("<br><br>");
		popup.document.write("<b>PassPort EndDate: </b>" + strArray[1]);
		popup.document.write("<br><br>");
		popup.document.write("<b>PassPort Number : </b>" + strArray[2]);
		popup.document.write("<br><br>");
		popup.document.write("<b>I94Number: </b>" + strArray[3]);
		popup.document.write("<br><br>");
		popup.document.write("<b>UniversityName: </b>" + strArray[4]);
		popup.document.write("<br><br>");
		popup.document.write("<b>PhoneNumber: </b>" + strArray[5]);
		popup.document.write("<br><br>");
		popup.document.write("<b>Email: </b>" + strArray[6]);
		popup.document.write("<br><br>");
		popup.document.write("<b>DateNotifiedTermination: </b>" + strArray[7]);
		popup.document.write("<br><br>");
		popup.document.write("<b>Start Date Of Employee: </b>" + strArray[8]);
		popup.document.write("<br><br>");
		popup.document.write("<b>Termination Date Of Employee : </b>" + strArray[9]);
		popup.document.write("<br><br>");
		
		}
		
		else if(strArray[10] == 'GreenCard' || strArray[10] == 'USCitizen')
			{
			popup.document.write("<b>PassPort StartDate:</b>"
					+ content.substr(0, content.indexOf("^")));
			popup.document.write("<br><br>");
			popup.document.write("<b>PassPort EndDate: </b>" + strArray[1]);
			popup.document.write("<br><br>");
			popup.document.write("<b>PassPort Number : </b>" + strArray[2]);
			popup.document.write("<br><br>");
			}
		
		else
			{
			
			popup.document.write("<b>PassPort StartDate:</b>"
					+ content.substr(0, content.indexOf("^")));
			popup.document.write("<br><br>");
			popup.document.write("<b>PassPort EndDate: </b>" + strArray[1]);
			popup.document.write("<br><br>");
			popup.document.write("<b>PassPort Number : </b>" + strArray[2]);
			popup.document.write("<br><br>");
			popup.document.write("<b>I94Number: </b>" + strArray[3]);
			popup.document.write("<br><br>");
			
			}
		

	}// Write content into it.
	// Write content into it.

}