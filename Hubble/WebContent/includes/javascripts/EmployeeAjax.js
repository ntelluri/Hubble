/*Don't Alter these Methods*/
var totalRec=0;
function newXMLHttpRequest() {
    var xmlreq = false;
    if(window.XMLHttpRequest) {
        xmlreq = new XMLHttpRequest();
    } else if(window.ActiveXObject) {
        try {
            xmlreq = new ActiveXObject("MSxm12.XMLHTTP");
        } catch(e1) {
            try {
                xmlreq = new ActiveXObject("Microsoft.XMLHTTP");
            } catch(e2) {
                xmlreq = false;
            }
        }
    }
    return xmlreq;
}

function readyStateHandler(req,responseXmlHandler) {
    return function() {
        if (req.readyState == 4) {
            if (req.status == 200) {
                responseXmlHandler(req.responseXML);
            } else {
               // alert("HTTP error"+req.status+" : "+req.statusText);
            }
        }
    }
}
function readyStateHandlerText(req,responseTextHandler){
    return function() {
        if (req.readyState == 4) {
            if (req.status == 200) {               
                responseTextHandler(req.responseText);
            } else {
               // alert("HTTP error"+req.status+" : "+req.statusText);
            }
        }
    }
} 
function readyStateHandlerXml(req,responseXmlHandler) {
    return function() {
        if (req.readyState == 4) {
            if (req.status == 200) {
                responseXmlHandler(req.responseXML);
            } else {
               // alert("HTTP error"+req.status+" : "+req.statusText);
            }
        }
    }
}
/* Methods for getting Practices by Department */

function getPracticeDataV1() {
    
    var departmentName = document.getElementById("departmentId").value;
    var req = newXMLHttpRequest();
    req.onreadystatechange = readyStateHandlerXml(req, populatePractices);
    var url = CONTENXT_PATH+"/getEmpDepartment.action?departmentName="+departmentName;
    req.open("GET",url,"true");    
    req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    req.send(null);
    
}

function populatePractices(resXML) {    
    
    var practiceId = document.getElementById("practiceId");
    var department = resXML.getElementsByTagName("DEPARTMENT")[0];
    var practices = department.getElementsByTagName("PRACTICE");
    practiceId.innerHTML=" ";
    
    for(var i=0;i<practices.length;i++) {
        var practiceName = practices[i];
        
        var name = practiceName.firstChild.nodeValue;
        var opt = document.createElement("option");
        if(i==0){
            opt.setAttribute("value","");
        }else{
            opt.setAttribute("value",name);
        }
        opt.appendChild(document.createTextNode(name));
        practiceId.appendChild(opt);
    }
}

/* Methods closing Practices by Department */


/* Methods for getting Employee Titles by Department */

function getEmpTitleDataV1() {
    
    var departmentName = document.employeeForm.departmentId.value;
    
    var req = newXMLHttpRequest();
    req.onreadystatechange = readyStateHandler(req, populateEmpTitles);
    var url = CONTENXT_PATH+"/getEmpForTitles.action?departmentName="+departmentName;
    req.open("GET",url,"true");    
    req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    req.send(null);
    
}

function populateEmpTitles(resXML) {
    
    
    var titleId = document.employeeForm.titleId;
    
    var department = resXML.getElementsByTagName("DEPARTMENT")[0];
    var titles = department.getElementsByTagName("TITLE");
    titleId.innerHTML=" ";
    
    for(var i=0;i<titles.length;i++) {
        var titleName = titles[i];
        
        var name = titleName.firstChild.nodeValue;
        var opt = document.createElement("option");
        if(i==0){
            opt.setAttribute("value","");
        }else{
            opt.setAttribute("value",name);
        }
        opt.appendChild(document.createTextNode(name));
        titleId.appendChild(opt);
    }
}

/* Methods closing Employee Titles by Department */

/* START: Methods for Sub Practice Data */
function getSubPracticeData(){
    
    var practiceName = document.getElementById("practiceId").value;

    if(practiceName == 'East' || practiceName == 'West' || practiceName == 'Central'){
        document.getElementById("territory_div").style.display='';
    }else{
        document.getElementById("territory_div").style.display='none';
    }
    
    var req = newXMLHttpRequest();
    req.onreadystatechange = readyStateHandlerXml(req, populateSubPractices);
    var url = CONTENXT_PATH+"/getEmpPractice.action?practiceName="+practiceName;
    req.open("GET",url,"true");    
    req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    req.send(null);
    
}


/*
 * =======================================================
 * 
 * =======================================================
 */
/* START: Methods for Sub Practice Data */
function getSubPracticeData1(){
    var practiceName = document.getElementById("practiceId").value;
    var req = newXMLHttpRequest();
    req.onreadystatechange = readyStateHandler(req, populateSubPractices);
    // var url =
	// CONTENXT_PATH+"/getEmpPractice.action?practiceName="+practiceName;
    var url = CONTENXT_PATH+"/getTerritory.action?practiceName="+practiceName;
    req.open("GET",url,"true");    
    req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    req.send(null);
    
}


function populateSubPractices(resXML) {
    // alert(resXML);
    var subPractice = document.getElementById("subPractice");
    
    var practice = resXML.getElementsByTagName("PRACTICE")[0];
    
    var subPractices = practice.getElementsByTagName("SUBPRACTICE");
    subPractice.innerHTML=" ";
    
    for(var i=0;i<subPractices.length;i++) {
        var subPracticeName = subPractices[i];
        
        var name = subPracticeName.firstChild.nodeValue;
        var opt = document.createElement("option");
        if(i==0){
            opt.setAttribute("value","All");
        }else{
            opt.setAttribute("value",name);
        }
        opt.appendChild(document.createTextNode(name));
        subPractice.appendChild(opt);
    }
}
/* CLOSE: Methods for Sub Practice Data */

/* Methods for getting Teams by Practices */

function getTeamData() {
    
    var subPracticeName = document.getElementById("subPractice").value;
    
    var req = newXMLHttpRequest();
    req.onreadystatechange = readyStateHandler(req, populateTeams);
    var url = CONTENXT_PATH+"/getEmpSubPractice.action?subPracticeName="+subPracticeName;
    req.open("GET",url,"true");
    req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    req.send(null);
    
}

function populateTeams(resXML) {
    
    var teamId = document.getElementById("teamId");
    var subPractice = resXML.getElementsByTagName("SUBPRACTICE")[0];
    var teams = subPractice.getElementsByTagName("TEAM");
    teamId.innerHTML=" ";
    for(var i=0;i<teams.length;i++) {
        var TeamName = teams[i];
        var name = TeamName.firstChild.nodeValue;
        var opt = document.createElement("option");
        
        if(i==0){
            opt.setAttribute("value","All");
        }else{
            opt.setAttribute("value",name);
        }
        
        opt.appendChild(document.createTextNode(name));
        teamId.appendChild(opt);
    }
}

/* Methods closing Teams by Practices */

/* Methods for getting TeamMembers by Teams */

function getTeamMemberData() {
    
    var teamName = document.getElementById("teamId").value;    
    var req = newXMLHttpRequest();
    req.onreadystatechange = readyStateHandler(req, populateTeamMemeberData);
    var url = CONTENXT_PATH+"/getEmpTeamNames.action?teamName="+teamName;
    req.open("GET",url,"true");
    req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    req.send(null);
}

function populateTeamMemeberData(resXML) {    
    var teamMemberId = document.getElementById("teamMemberId");
    var team = resXML.getElementsByTagName("TEAM")[0];
    // var teamMember = team.getElementsByTagName("TEAMMEMBER");
    teamMemberId.innerHTML=" ";
    for(var i=0;i<team.childNodes.length;i++) {
        var teamMember = team.childNodes[i];
        var id =teamMember.getElementsByTagName("TEAMMEMBER-ID")[0];        
        var name = teamMember.getElementsByTagName("TEAMMEMBER-NAME")[0];
        var opt = document.createElement("option");
        opt.setAttribute("value",id.childNodes[0].nodeValue);
        opt.appendChild(document.createTextNode(name.childNodes[0].nodeValue));
        teamMemberId.appendChild(opt);
    }
}

/* Methods closing TeamMembers by Teams */

/* START: Methods for getting reports to person data */

function getReportsToDataV1(){
    var deptName = document.employeeForm.departmentId.value;
    var req = newXMLHttpRequest();
    req.onreadystatechange = readyStateHandlerXml(req, populateReportsTo);
    var url = CONTENXT_PATH+"/getEmpForReportsTo.action?deptName="+deptName;
    req.open("GET",url,"true");
    req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    req.send(null);   
}

function populateReportsTo(resXML) {
    var reportsTo = document.employeeForm.reportsTo;
    var team = resXML.getElementsByTagName("TEAM")[0];
    var users = team.getElementsByTagName("USER");
    reportsTo.innerHTML=" ";
    for(var i=0;i<users.length;i++) {
        var userName = users[i];
        var att = userName.getAttribute("userId");
        var name = userName.firstChild.nodeValue;
        var opt = document.createElement("option");
        opt.setAttribute("value",att);
        opt.appendChild(document.createTextNode(name));
        reportsTo.appendChild(opt);
    }
}
/* END: Methods for getting reports to person data */

function getProjectsForAccountId(){
 
    var accountId = document.getElementById("customerId").value;
 
    var req = newXMLHttpRequest();
    req.onreadystatechange = readyStateHandler(req, populateProjects);
    var url = CONTENXT_PATH+"/getProjectsForAccountId.action?accountId="+accountId;
    req.open("GET",url,"true");
    req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    req.send(null);   
}


function populateProjects(resXML) {
  
    // var projects = document.getElementById("projectName");
    var projects = document.getElementById("projectId");
    
    var projectsList = resXML.getElementsByTagName("PROJECTS")[0];
   
    var users = projectsList.getElementsByTagName("USER");
    projects.innerHTML=" ";
    for(var i=0;i<users.length;i++) {
        var userName = users[i];
        var att = userName.getAttribute("projectId");
        var name = userName.firstChild.nodeValue;
        var opt = document.createElement("option");
        opt.setAttribute("value",att);
        opt.appendChild(document.createTextNode(name));
        projects.appendChild(opt);
    }
}





function getSkillSet(empId,currId){
    // alert("empId-->"+empId);
    // alert("currId-->"+currId);
    
    // var departmentName = document.getElementById("departmentId").value;
    var req = newXMLHttpRequest();
    req.onreadystatechange = readyStateHandlerText(req, populateSkillset);
    var url = CONTENXT_PATH+"/getEmployeeSkillSet.action?empId="+empId+"&currId="+currId;
    req.open("GET",url,"true");    
    req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    req.send(null);
}


function populateSkillset(resText) {
    var background = "#3E93D4";
    var title = "Skill Set";
    // var text1 = text;
    // var size = text1.length;
    
    
  
    
    var size = resText.length;
    
    // Now create the HTML code that is required to make the popup
    var content = "<html><head><title>"+title+"</title></head>\
    <body bgcolor='"+background +"' style='color:white;'><h4>"+title+"</h4>"+resText+"<br />\
    </body></html>";
    
    if(size < 50){
        // Create the popup
        popup = window.open("","window","channelmode=0,width=300,height=150,top=200,left=350,scrollbars=no,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no");    
        popup.document.write(content); // Write content into it.
    }
    
    else if(size < 100){
        // Create the popup
        popup = window.open("","window","channelmode=0,width=400,height=200,top=200,left=350,scrollbars=no,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no");    
        popup.document.write(content); // Write content into it.
    }
    
    else if(size < 260){
        // Create the popup
        popup = window.open("","window","channelmode=0,width=400,height=200,top=200,left=350,scrollbars=no,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no");    
        popup.document.write(content); // Write content into it.
    } else {
        // Create the popup
        popup = window.open("","window","channelmode=0,width=400,height=200,top=200,left=350,scrollbars=no,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no");    
        popup.document.write(content); // Write content into it.
    }
    
}
        
// ------------------
        
        
        

/*
 * Pmo activity ajax list date : 05/13/2015 Author : Santosh Kola
 */

function getpmoActivityList()
{
    var oTable = document.getElementById("tblUpdate");
    clearTable(oTable);
    
     $('span.pagination').empty().remove();
    var NAME=document.getElementById("customerName").value;
    var  ProjectName=document.getElementById("projectName").value;
    var  status=document.getElementById("status").value;                                                            
    var ProjectStartDate=document.getElementById("projectStartDate").value;
    var pmoLoginId = document.getElementById("pmoLoginId").value;
    var preAssignEmpId = document.getElementById("preAssignEmpId").value;
    var practiceId = document.getElementById("practiceId").value;
    var projectType=escape(document.getElementById("ProjectTypeId1").value);
    var  empStatus=document.getElementById("empStatus").value; 
    var req = newXMLHttpRequest();
    req.onreadystatechange = readyStateHandlerreq(req, displaypmoActivityResult); 

    // var url =
	// CONTENXT_PATH+"/searchpmoActivityAjaxList.action?customerName="+NAME+"&projectName="+ProjectName+"&status="+status+"&projectStartDate="+ProjectStartDate;
    // var url =
	// CONTENXT_PATH+"/searchpmoActivityAjaxList.action?customerName="+NAME+"&projectName="+ProjectName+"&status="+status+"&projectStartDate="+ProjectStartDate+"&pmoLoginId="+pmoLoginId+"&preAssignEmpId="+preAssignEmpId;
    var url = CONTENXT_PATH+"/searchpmoActivityAjaxList.action?customerName="+NAME+"&projectName="+ProjectName+"&projectType="+unescape(projectType)+"&status="+status+"&projectStartDate="+ProjectStartDate+"&pmoLoginId="+pmoLoginId+"&preAssignEmpId="+preAssignEmpId+"&practiceId="+practiceId+"&empStatus="+empStatus;


    req.open("GET",url,"true");    
    req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    req.send(null);


}





function displaypmoActivityResult(resText) 
{
  
    if(resText.length !=0)
    {
        var oTable = document.getElementById("tblUpdate");
       
        clearTable(oTable);
      
            
        // var headerFields = new
		// Array("SNo","NAME","ProjectName","ProjectStartDate","Resources","Status","Activity");
        // var headerFields = new
		// Array("SNo","ProjectName","Account&nbsp;Name","ProjectStartDate","Resources","Status","Pmo","Activity");
        // var headerFields = new
		// Array("SNo","Account&nbsp;Name","ProjectName","ProjectStartDate","Active&nbsp;Resources","Status","Pmo","Project","ProjectTeam");
        var headerFields = new Array("SNo","Account&nbsp;Name","ProjectName", "ProjectType", "ProjectStartDate","Active&nbsp;Resources","Status","Pmo","Project","ProjectTeam");
       
    
        tbody = document.createElement("TBODY");
        oTable.appendChild(tbody);
       
        var resTextSplit1 = resText.split("^");

        generateTableHeader(tbody,headerFields);
        
         totalRec=resTextSplit1.length-1;
        for(var index=0;index<resTextSplit1.length-1;index++) {
            resTextSplit2 = resTextSplit1[index].split("|");
            
            generateRow1(tbody,resTextSplit2,index);
            
        }
        generateFooter1(tbody);
       pagerOption();
    }
    else {
        alert("No Records Found");
    }
}
function generateRow1(tableBody,rowFeildsSplit,index){
    var row;
    var cell;
    row = document.createElement("TR");
    row.className="gridRowEven";
    cell = document.createElement("TD");
    cell.className="gridRowEven";
    cell.innerHTML = index+1;
    cell.setAttribute('align','left');
    row.appendChild(cell);
   
    cell = document.createElement( "TD" );
    cell.className="gridRowEven";
       
    cell.appendChild(document.createTextNode(rowFeildsSplit[2]));
    row.appendChild(cell);
   
    cell.setAttribute('align','left');
    cell.setAttribute('width','38%');
        
        
        
    cell = document.createElement( "TD" );
    cell.className="gridRowEven";
       
    cell.appendChild(document.createTextNode(rowFeildsSplit[3]));
    row.appendChild(cell);
    
    cell.setAttribute('align','left');

    cell = document.createElement( "TD" );
    cell.className="gridRowEven";
      
    cell.appendChild(document.createTextNode(rowFeildsSplit[4]));
    row.appendChild(cell);
    
    
    cell.setAttribute('align','left');

    cell = document.createElement( "TD" );
    cell.className="gridRowEven";
      
    cell.appendChild(document.createTextNode(rowFeildsSplit[5]));
    row.appendChild(cell);
     
    cell.setAttribute('align','right');

    cell = document.createElement( "TD" );
    cell.className="gridRowEven";
    cell.innerHTML = "<a href='javascript:getResourceTypeDetails(\""+rowFeildsSplit[1]+"\")'>"+rowFeildsSplit[6]+"</a>";
    // cell.appendChild(document.createTextNode(rowFeildsSplit[5]));
    row.appendChild(cell);
    
    cell.setAttribute('align','right');	

    cell = document.createElement( "TD" );
    cell.className="gridRowEven";
        
    cell.appendChild(document.createTextNode(rowFeildsSplit[7]));
    row.appendChild(cell);
     
    cell.setAttribute('align','left');
        
    cell = document.createElement( "TD" );
    cell.className="gridRowEven";
        
    cell.appendChild(document.createTextNode(rowFeildsSplit[8]));
    row.appendChild(cell);
     
    cell.setAttribute('align','left');
        
        
    cell = document.createElement( "TD" );
    cell.className="gridRowEven";
    var j = document.createElement("a");

    j.setAttribute("href", "getCustomerProjectDetails.action?projectId="+rowFeildsSplit[1]+"&accountId="+rowFeildsSplit[9]+"&accountName="+rowFeildsSplit[2]+"&backFlag=1");
           
    j.setAttribute("onmouseover","Tip('"+rowFeildsSplit[4]+"')");
    j.setAttribute("onmouseout","javascript:UnTip();");
    j.innerHTML = "<img SRC='../includes/images/go_21x21.gif' WIDTH=18 HEIGHT=15 BORDER=0 ALTER='Click to Add'>";
         
   
    cell.appendChild(j);
    cell.setAttribute('align','center');
    row.appendChild(cell);


    cell = document.createElement( "TD" );
    cell.className="gridRowEven";
    var j = document.createElement("a");
    j.setAttribute("href", "getProjectTeamQuery.action?projectId="+rowFeildsSplit[1]+"&accountId="+rowFeildsSplit[9]+"&customerName="+escape(rowFeildsSplit[2]));
         
    j.setAttribute("onmouseover","Tip('"+rowFeildsSplit[4]+"')");
    j.setAttribute("onmouseout","javascript:UnTip();");
    j.innerHTML = "<img SRC='../includes/images/go_21x21.gif' WIDTH=18 HEIGHT=15 BORDER=0 ALTER='Click to Add'>";
         
    document.create
    cell.appendChild(j);
    cell.setAttribute('align','center');
    row.appendChild(cell);


    tableBody.appendChild(row);
}



function readyStateHandlerreq(req,responseTextHandler) {

    // alert("ready");
    return function() {
        if (req.readyState == 4) {
            if (req.status == 200) {
                (document.getElementById("loadingMessage")).style.display = "none";
               
                responseTextHandler(req.responseText);
            } else {
                
               // alert("HTTP error ---"+req.status+" : "+req.statusText);
            }
        }else {
          
            (document.getElementById("loadingMessage")).style.display = "block";
        }
    }
}
function generateTableHeader(tableBody,headerFields) {
    var row;
    var cell;
    row = document.createElement( "TR" );
    row.className="gridHeader";
    tableBody.appendChild( row );
    for (var i=0; i<headerFields.length; i++) {
        cell = document.createElement( "TD" );
        cell.className="gridHeader";
        row.appendChild( cell );

        cell.setAttribute("width","10000px");
        cell.innerHTML = headerFields[i];
    }
}


function generateFooter(tbody) {
  
    var cell;
    var footer =document.createElement("TR");
    footer.className="gridPager";
    tbody.appendChild(footer);
    cell = document.createElement("TD");
    cell.className="gridFooter";

    // cell.colSpan = "7";
    
    cell.colSpan = "10";
    
       
   

    footer.appendChild(cell);
}

function clearTable(tableId) {
    var tbl =  tableId;
    var lastRow = tbl.rows.length; 
    while (lastRow > 0) { 
        tbl.deleteRow(lastRow - 1);  
        lastRow = tbl.rows.length; 
    } 
}

function getMyProjectsByAccountId(){
 
    var accountId = document.getElementById("customerId").value;
 
    var req = newXMLHttpRequest();
    req.onreadystatechange = readyStateHandler33(req, populateMyProjects);
    var url = CONTENXT_PATH+"/getMyProjectsByAccountId.action?accountId="+accountId;
    req.open("GET",url,"true");
    req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    req.send(null);   
}


function populateMyProjects(resXML) {
  
    // var projects = document.getElementById("projectName");
    var projects = document.getElementById("projectId");
    
    var projectsList = resXML.getElementsByTagName("PROJECTS")[0];
   
    var users = projectsList.getElementsByTagName("USER");
    projects.innerHTML=" ";
    for(var i=0;i<users.length;i++) {
        var userName = users[i];
        var att = userName.getAttribute("projectId");
        var name = userName.firstChild.nodeValue;
        var opt = document.createElement("option");
        opt.setAttribute("value",att);
        opt.appendChild(document.createTextNode(name));
        projects.appendChild(opt);
    }
}
function isActiveBridge(){
    $("#correctImg").hide();
    $("#wrongImg").hide();
    var bridgeCode=document.getElementById("bridgeCode").value;
    var req = newXMLHttpRequest();
    req.onreadystatechange = readyStateHandlerText(req,isActiveBridgeResponse); 

    // var
	// url=CONTENXT_PATH+"/updateMyReview.action?reviewId="+reviewId+"&overlayReviewType="+overlayReviewType+"&overlayReviewName="+overlayReviewName+"&overlayReviewDate="+overlayReviewDate+"&overlayDescription="+overlayDescription+"&reviewStatusOverlay="+reviewStatusOverlay;
    // var
	// url=CONTENXT_PATH+'/upadtePayrollTaxExemption.action?overLayExemptionType='+overLayexemptionType+'&overlayApprovedAmount='+overlayApprovedAmount+'&overLayStatus='+overLaystatus+'&comments='+comments+'&exemptionId='+exemptionId+"&paymentDateEmp="+paymentDateEmp;
    var url=CONTENXT_PATH+'/isActiveBridge.action?bridgeCode='+bridgeCode;
    // alert(url);
    req.open("GET",url,"true");
    req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    req.send(null);
}
function isActiveBridgeResponse(resText){
    // alert(resText);
    
    if(resText=='Yes'){
        // document.getElementById("correctImg").style.display="block";
        $("#correctImg").show();
       
    }else{
        // document.getElementById("wrongImg").style.display="block";
    	alert("Please Enter Active  bridge code");
        $("#wrongImg").show();
        document.getElementById("bridgeCode").value='';
    }
    
    
}
function taskValidation(){
    var issueRelType=document.getElementById("issueRelType").value;
    var issueType=document.getElementById("issueType").value;
    var bridgeCode=document.getElementById("bridgeCode").value;
    var primaryAssignToforOthers=document.getElementById("primaryAssignToforOthers").value;
    var primaryAssignTo=document.getElementById("primaryAssignTo").value;
    // alert(issueRelType+" "+issueType);
    var chkBoxList = document.getElementsByName("issuerelatedId");
    if(chkBoxList[2].checked){
        // alert("in check cond:")
        var  projectId  =document.getElementById("projectId").value;
        if(projectId==-1){
            alert("please select project name");
            return false;
        }
    }
    if(issueRelType=='3' && issueType=='4'){
        if(bridgeCode==""){
            alert("please enter Bridge Code");
            return false;
        }
    }
if(primaryAssignToforOthers.trim().length==0 && primaryAssignTo.trim().length==0){
    	
    	alert("Plese select Primary Assigned To ");
    	return false;
    	
    }
    return true;;
}
function getCustomersList()
{
    var oTable = document.getElementById("tblUpdate");
    clearTable(oTable);
    var NAME=document.getElementById("customerName").value;
    var req = newXMLHttpRequest();
    req.onreadystatechange = readyStateHandlerreq(req, displayCustomerProjectsAjaxList); 

        var accountId=document.getElementById('consultantId').value;
        var url = CONTENXT_PATH+"/searchCustomerProjectsAjaxList.action?accId="+accountId;
  // var url =
	// CONTENXT_PATH+"/searchCustomerProjectsAjaxList.action?customerName="+NAME;

    req.open("GET",url,"true");    
    req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    req.send(null);


}
function displayCustomerProjectsAjaxList(resText) 
{
  
    if(resText.length !=0)
    {
        var oTable = document.getElementById("tblUpdate");
       
        clearTableForCustomerProjects(oTable);
      
            
        var headerFields = new Array("SNo","CustomerNAME","No.Of.Projects","Projects List");	
               
    
        tbody = document.createElement("TBODY");
        oTable.appendChild(tbody);
       
        var resTextSplit1 = resText.split("*@!");

        generateTableHeader(tbody,headerFields);
        for(var index=0;index<resTextSplit1.length-1;index++) {
            resTextSplit2 = resTextSplit1[index].split("#^$");
            // alert("resTextSplit2.."+resTextSplit2);
            // clearTable(oTable);
            generateRow2(tbody,resTextSplit2,index);
            
        }
        generateFooter(tbody);
       
    }
    else {
        alert("No Records Found");
    }
    pagerOption();
}

function generateRow2(tableBody,rowFeildsSplit,index){
    var row;
    var cell;
    row = document.createElement("TR");
    row.className="gridRowEven";
    cell = document.createElement("TD");
    cell.className="gridRowEven";
    cell.innerHTML = index+1;
    cell.setAttribute('align','left');
    row.appendChild(cell);
   
    cell = document.createElement( "TD" );
    cell.className="gridRowEven";
       
    cell.appendChild(document.createTextNode(rowFeildsSplit[1]));
    row.appendChild(cell);
   
    cell.setAttribute('align','left');
    cell.setAttribute('width','38%');
        
        
        
        

    cell = document.createElement( "TD" );
    cell.className="gridRowEven";
      
    cell.appendChild(document.createTextNode(rowFeildsSplit[2]));
    row.appendChild(cell);
     
    cell.setAttribute('align','left');


    cell = document.createElement( "TD" );
    cell.className="gridRowEven";
    var j = document.createElement("a");
    
   // j.setAttribute("href",
	// "getCustomerProjectsDetailsList.action?accountId="+rowFeildsSplit[3]+"&customerName="+escape(rowFeildsSplit[1]));
var status='Active';
    
    j.setAttribute("href", "getCustomerProjectsDetailsList.action?accountId="+rowFeildsSplit[3]+"&customerName="+escape(rowFeildsSplit[1])+"&status="+status);   
           
    j.setAttribute("onmouseover","Tip('"+rowFeildsSplit[4]+"')");
    j.setAttribute("onmouseout","javascript:UnTip();");
    j.innerHTML = "<center><img SRC='../includes/images/go_21x21.gif' WIDTH=18 HEIGHT=15 BORDER=0 ALTER='Click to Add'></center>";
         
    document.create
    cell.appendChild(j);
    cell.setAttribute('align','left');
    row.appendChild(cell);
           

    tableBody.appendChild(row);
}
function clearTableForCustomerProjects(tableId) {
    // alert("clearTable")
    var tbl =  tableId;
    var lastRow = tbl.rows.length; 
    while (lastRow > 0) { 
        tbl.deleteRow(lastRow - 1);  
        lastRow = tbl.rows.length; 
    } 
}

function addProjectToCustomer(){var accountId=document.getElementById('consultantId').value;

var customerName=document.getElementById('customerName').value;
if(customerName==""){
alert("Please select customer name from the suggestion list")
return false;
}
// alert("escape(customerName)"+escape(customerName));
window.location="getAddProject.action?accountId="+accountId+"&accountName="+escape(customerName)+'&backFlag=3';

}






function doAddProjectToCustomer(){
    var accountId=document.getElementById('consultantId').value;
     
    var customerName=document.getElementById('customerName').value;
    var projectName=document.getElementById('projectName').value;
   // alert("projectName"+projectName);
    var pmoName=document.getElementById('pmoName').value;
    var projectStartDate=document.getElementById('projectStartDate').value;

    
    window.location="doAddProjectToCustomer.action?accountId="+accountId+"&accountName="+escape(customerName)+"&projectName="+projectName+"&pmoName="+pmoName+"&projectStartDate="+projectStartDate;
}


function doInactiveCustomerProject(projectId,accountId,ProjectEndDate,comments){

    if(ProjectEndDate.trim()!=""){
        if(confirm("Do you want to close the project?")){
            var accountName=document.getElementById('customerName').value;
            // alert("accountName"+accountName)
            window.location="getInActiveCustomerProject.action?projectId="+projectId+"&accountId="+accountId+"&accountName="+escape(accountName)+"&endDateActual="+ProjectEndDate+"&comments="+escape(comments);
        }
    }else{
        alert("Project end date is mandatory to complete the project");
    }
}

function getProjectOverlay(){
    // alert("add");
    // document.getElementById('resultMessageNoDue').innerHTML = "";
     
    // document.getElementById("headerLabel").style.color="white";
    // document.getElementById("headerLabel").innerHTML="Add Event";
 
    var overlay = document.getElementById('empProjectsOverlay');
    var specialBox = document.getElementById('empProjectsSpecialBox');
           
    overlay.style.opacity = .8;
    if(overlay.style.display == "block"){
        overlay.style.display = "none";
        specialBox.style.display = "none";
       
    } else {
       
        overlay.style.display = "block";
        specialBox.style.display = "block";
        employeeAvailableProjects();
    }
}
function employeeAvailableProjects()
{
    // alert("in ajax");
    var empId=document.getElementById("id").value;
    var status=document.getElementById("status").value;
    var startDate=document.getElementById("projectStartDate").value;
    var endDate=document.getElementById("projectEndDate").value;
    projectCheckDate(startDate);
    // alert(startDate);
    ClrTable(document.getElementById("tblEmpProjectDetails"));
    var req = newXMLHttpRequest();
    req.onreadystatechange = readyStateHandlerreq(req, employeeAvailableProjectsResponse); 

    // var url =
	// CONTENXT_PATH+"/searchpmoActivityAjaxList.action?customerName="+NAME+"&projectName="+ProjectName+"&status="+status+"&projectStartDate="+ProjectStartDate;
    var url = CONTENXT_PATH+"/employeeAvailableProjects.action?empId="+empId+"&status="+status+"&startDate="+startDate+"&endDate="+endDate;

    req.open("GET",url,"true");    
    req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    req.send(null);


}

function projectCheckDate() {
                   
    var startDate = document.getElementById('projectStartDate').value;
    var endDate = document.getElementById('projectEndDate').value;
    var mm = startDate.substring(0,2);
    var dd = startDate.substring(3,5);
    var yyyy = startDate.substring(6,10);
    var mm1 = endDate.substring(0,2);
    var dd1 = endDate.substring(3,5);
    var yyyy1 = endDate.substring(6,10);
    if(document.getElementById("projectStartDate").value != '' && document.getElementById("projectEndDate").value != '') {
        if(yyyy1 < yyyy) {
            alert('Start Date is older than End Date');
            document.getElementById('projectEndDate').value = '';
            return false;
        }
        else if((yyyy1 == yyyy) && (mm1 < mm)) {
            alert('Start Date is older than End Date');
            document.getElementById('projectEndDate').value = '';
            return false;
        }
        else if((yyyy1 == yyyy) && (mm1 == mm) && (dd1 < dd)) {
            alert('Start Date is older than End Date');
            document.getElementById('projectEndDate').value = '';
            return false;
        }
    }
}
            




function employeeAvailableProjectsResponse(resText) 
{
  
  
    var tableId = document.getElementById("tblEmpProjectDetails");
    // var headerFields = new
	// Array("SNo","Project&nbsp;Name","StartDate","EndDate","EmpStatus","Utilization","Resource&nbsp;Type");
    var headerFields = new Array("SNo","Project&nbsp;Name","StartDate","EndDate","EmpStatus","Utilization","Billable","Comments");
   
    ParseAndGenerateHTML(tableId,resText, headerFields);

     
     
  
}
function ClrTable(myHTMLTable) { 
    var tbl =  myHTMLTable;
    var lastRow = tbl.rows.length; 
    while (lastRow > 0) { 
        tbl.deleteRow(lastRow - 1);  
        lastRow = tbl.rows.length; 
    } 
}
function ParseAndGenerateHTML(oTable,responseString,headerFields) {
    
    var start = new Date();
    var fieldDelimiter = "|";
    var recordDelimiter = "^";   
    
    if(oTable.id=="tblEmpProjectDetails" || oTable.id == "tblResourceTypeDetails"  ||  oTable.id =="tblEmpDetailsByAppraisalStatus" ||  oTable.id =="tblEmpCertificationDetails" || oTable.id == "tblEmailDetails" ||  oTable.id == "tblMSAUpdate" ){
        fieldDelimiter = "#^$";
        recordDelimiter = "*@!"; 
    }
    var records = responseString.split(recordDelimiter); 
    generateTable(oTable,headerFields,records,fieldDelimiter);
}
function generateTable(oTable, headerFields,records,fieldDelimiter) {	
    var tbody = oTable.childNodes[0];    
    tbody = document.createElement("TBODY");
    oTable.appendChild(tbody);
    generateTableHeader(tbody,headerFields);
    var rowlength;
    rowlength = records.length-1;
    if(rowlength >=1 && records!=""){
        for(var i=0;i<rowlength;i++) {
            if(oTable.id=="tblEmpProjectDetails" || oTable.id == "tblResourceTypeDetails"  ||  oTable.id =="tblEmpDetailsByAppraisalStatus"  ||  oTable.id =="tblEmpCertificationDetails" || oTable.id == "tblEmailDetails" ||  oTable.id == "tblMSAUpdate" )
            {
                generateRow(oTable,tbody,records[i],fieldDelimiter);  
            }
            
        }    
    }    
    else {
        generateNoRecords(tbody,oTable);
    }
    
    generateFooter(tbody,oTable);
}
function generateRow(oTable,tableBody,record,delimiter) {
    // alert("In generateRow");
    var row;
    var cell;
    var fieldLength;
    var fields = record.split(delimiter);
    fieldLength = fields.length ;
    var length;
    // if(oTable.id == "tblAccountSummRep" || oTable.id ==
	// "tblUpdateForAccountsListByPriority"){
    length = fieldLength;
    // }
    
    // else {
    // length = fieldLength-1;
    // }
  
    row = document.createElement( "TR" );
    row.className="gridRowEven";
    tableBody.appendChild( row );
    // alert("length..."+length);
    if(oTable.id=="tblResourceTypeDetails"  ||  oTable.id =="tblEmpDetailsByAppraisalStatus" || oTable.id == "tblEmailDetails"){
        for (var i=0;i<length;i++) {
       
            cell = document.createElement( "TD" );
            cell.className="gridColumn";
            cell.innerHTML = fields[i];
            if(fields[i]!=''){
                row.appendChild( cell );
            }
       
        }   
    } else if(oTable.id == "tblMSAUpdate"){
    	
    
    	
    	 for (var i=0;i<length-1;i++) {
    	       
    		/*
			 * alert("HAII");
			 * 
			 * if(i==1){ cell = document.createElement( "TD" );
			 * cell.className="gridColumn"; var j = document.createElement("a"); //
			 * cell.innerHTML = "<a
			 * href='javascript:gettotalweekcountbyproject(\""+cusName+"\",\""+Practice3+"\",\""+fields[2]+"\",\""+costmodel+"\",\""+year+"\",\""+month+"\",\"Week1\",\""+fields[8]+"\")'>"+fields[4]+"</a>";
			 * cell.innerHTML = "<a
			 * href='javascript:doAddMSADates(\""+fields[4]+"\",\""+fields[2]+"\",\""+fields[3]+"\")'>"+fields[1]+"</a>";
			 * cell.appendChild(j); }
			 * 
			 * else{ cell = document.createElement( "TD" );
			 * cell.className="gridColumn";
			 * 
			 * cell.setAttribute("align","center");
			 * 
			 * cell.innerHTML = fields[i]; }
			 */
    		 
    		 
    		 
    		  cell = document.createElement( "TD" );
              cell.className="gridColumn";
            
              if(fields[i]!=''){
                  row.appendChild( cell );
              }
              
              
         	 if(i==2){
     			
   			  var anc = document.createElement("a");
   			
   			 anc.setAttribute("href",
   			 "javascript:addMSAToCustomer(\""+fields[5]+"\",\""+fields[3]+"\",\""+fields[4]+"\")");
   			 anc.appendChild(document.createTextNode(fields[2]));
   			 cell.appendChild(anc);
   			 
   			 }else
   				 {
   				cell.innerHTML = fields[i];
   				 }
    	 }
    	
    }
    
    
    
    else if(oTable.id =="tblEmpCertificationDetails"){
   	 for (var i=1;i<length-1;i++) {
	       
         cell = document.createElement( "TD" );
         cell.className="gridColumn";
         cell.innerHTML = fields[i];
         if(fields[i]!=''){
             row.appendChild( cell );
         }
    
     }
	 cell = document.createElement( "TD" );
     cell.className="gridColumn";
     // cell.innerHTML = fields[8];
      cell.innerHTML =" <a href='javascript:getReviewDescription("+fields[0]+")'><CENTER><img SRC='../includes/images/view.jpg' WIDTH=26 HEIGHT=23 BORDER=0  ALTER=''/></CENTER></a>";
     row.appendChild( cell );
	 
	 if(fields[4]!= null && fields[4]!='-'&& fields[4].trim().length>0)  {
		 cell = document.createElement( "TD" );
         cell.className="gridColumn";
         // cell.innerHTML = fields[8];
          cell.innerHTML =" <a href='javascript:getDownloadAttachment("+fields[0]+")'><CENTER><img SRC='../includes/images/download_11x10.jpg' WIDTH=15 HEIGHT=15 BORDER=0 ALTER=''/></CENTER></a>";
         row.appendChild( cell );
	 }else{
		 cell = document.createElement( "TD" );
         cell.className="gridColumn";
         // cell.innerHTML = fields[8];
          cell.innerHTML ="<CENTER><img SRC='../includes/images/download_11x10.jpg' WIDTH=15 HEIGHT=15 BORDER=0 TITLE='No file exist' ALTER=''/></CENTER>";
         row.appendChild( cell );
	 }
	 
} else{
        for (var i=0;i<length-1;i++) {
       
            cell = document.createElement( "TD" );
            cell.className="gridColumn";
            cell.innerHTML = fields[i];
            if(fields[i]!=''){
                row.appendChild( cell );
            }
       
        }
     
        cell = document.createElement( "TD" );
        cell.className="gridColumn";
        // cell.innerHTML = fields[8];
        cell.innerHTML ="<CENTER><img SRC='../includes/images/view.jpg' WIDTH=26 HEIGHT=23 BORDER=0 TITLE='"+fields[7]+"' ALTER=''/></CENTER>";
        row.appendChild( cell );
    }
}


function generateNoRecords(tbody,oTable) {
   
    var noRecords =document.createElement("TR");
    noRecords.className="gridRowEven";
    tbody.appendChild(noRecords);
    cell = document.createElement("TD");
    cell.className="gridColumn";
    
    if(oTable.id == "tblEmpProjectDetails" || oTable.id == "tblProjectRiskDashBoard"){
        cell.colSpan = "9";
    }else if(  oTable.id == "tblResourceTypeDetails")
    {
        cell.colSpan = "2";   
    } else if(  oTable.id == "tblQReviewDashBoard" || oTable.id == "tblQReviewDashBoard1" || oTable.id == "tblQReviewDashBoard2")
    {
        cell.colSpan = "11";   
    }
    else if(  oTable.id == "tblQReviewDashBoard" || oTable.id == "tblQReviewDashBoard1" || oTable.id == "tblQReviewDashBoard2")
    {
        cell.colSpan = "11";   
    }else if(  oTable.id == "tblEmpCertificationDetails")
    {
        cell.colSpan = "5";   
    }
    else if(  oTable.id == "tblQReviewManagerDashBoard")
    {
        cell.colSpan = "10";   
    }  else if(  oTable.id == "tblEmailDetails")
    {
        cell.colSpan = "10";   
    }
    
    else if(oTable.id == "tblMSAUpdate")
    {
        cell.colSpan = "10";   
    }
    
 
    cell.innerHTML = "No Records Found for this Search";
    noRecords.appendChild(cell);
}


/*******************************************************************************
 * PMO suggestion list start
 ******************************************************************************/
function EmployeeForProject() {
    var test=document.getElementById("assignedToUID").value;
   
    if (test == "") {

        clearTable1();
        hideScrollBar();
        var validationMessage=document.getElementById("authorEmpValidationMessage");
        validationMessage.innerHTML = "";
        document.frmSearch.preAssignEmpId.value="";
    // frmSearch issuesForm
    } else {
        if (test.length >2) {
            // alert("CONTENXT_PATH-- >"+CONTENXT_PATH)
            var url = CONTENXT_PATH+"/getEmployeeDetailforPMOActivity.action?customerName="+escape(test);
            var req = initRequest(url);
            req.onreadystatechange = function() {
                // alert("req-->"+req);
                if (req.readyState == 4) {
                    if (req.status == 200) {
                        // alert(req.responseXML);
                    
                        parseEmpMessagesForProject(req.responseXML);
                    } else if (req.status == 204){
                        clearTable1();
                    }
                }
            };
            req.open("GET", url, true);

            req.send(null);
        }
    }
}

function parseEmpMessagesForProject(responseXML) {
    // alert("-->"+responseXML);
    autorow1 = document.getElementById("menu-popup");
    autorow1.style.display ="none";
    autorow = document.getElementById("menu-popup");
    autorow.style.display ="none";
    clearTable1();
    var employees = responseXML.getElementsByTagName("EMPLOYEES")[0];
    if (employees.childNodes.length > 0) {        
        completeTable.setAttribute("bordercolor", "black");
        completeTable.setAttribute("border", "0");        
    } else {
        clearTable1();
    }
    if(employees.childNodes.length<10) {
        autorow1.style.overflowY = "hidden";
        autorow.style.overflowY = "hidden";        
    }
    else {    
        autorow1.style.overflowY = "scroll";
        autorow.style.overflowY = "scroll";
    }
    
    var employee = employees.childNodes[0];
    var chk=employee.getElementsByTagName("VALID")[0];
    if(chk.childNodes[0].nodeValue =="true") {
        // var validationMessage=document.getElementById("validationMessage");
        var validationMessage;
        
        validationMessage=document.getElementById("authorEmpValidationMessage");
        isPriEmpExist = true;
         
        validationMessage.innerHTML = "";
        document.getElementById("menu-popup").style.display = "block";
        for (loop = 0; loop < employees.childNodes.length; loop++) {
            
            var employee = employees.childNodes[loop];
            var customerName = employee.getElementsByTagName("NAME")[0];
            var empid = employee.getElementsByTagName("EMPID")[0];
            appendEmployeeForProject(empid.childNodes[0].nodeValue,customerName.childNodes[0].nodeValue);
        }
        var position;
        
        
        position = findPosition(document.getElementById("assignedToUID"));
        
        posi = position.split(",");
        document.getElementById("menu-popup").style.left = posi[0]+"px";
        document.getElementById("menu-popup").style.top = (parseInt(posi[1])+20)+"px";
        document.getElementById("menu-popup").style.display = "block";
    } // if
    if(chk.childNodes[0].nodeValue =="false") {
        var validationMessage = '';
      
        isPriEmpExist = false;
        validationMessage=document.getElementById("authorEmpValidationMessage");
    
 
        validationMessage.innerHTML = " Invalid ! Select from Suggesstion List. ";
        validationMessage.style.color = "green";
        validationMessage.style.fontSize = "12px";
       
        document.getElementById("assignedToUID").value = "";
        document.getElementById("preAssignEmpId").value = "";
            
        
    }
}
function appendEmployeeForProject(empId,empName) {
    
    var row;
    var nameCell;
    if (!isIE) {
        row = completeTable.insertRow(completeTable.rows.length);
        nameCell = row.insertCell(0);
    } else {
        row = document.createElement("tr");
        nameCell = document.createElement("td");
        row.appendChild(nameCell);
        completeTable.appendChild(row);
    }
    row.className = "popupRow";
    nameCell.setAttribute("bgcolor", "#3E93D4");
    var linkElement = document.createElement("a");
    linkElement.className = "popupItem";
    // if(assignedToType=='pre'){
    linkElement.setAttribute("href", "javascript:set_emp('"+empName +"','"+ empId +"')");

    linkElement.appendChild(document.createTextNode(empName));
    linkElement["onclick"] = new Function("hideScrollBar()");
    nameCell.appendChild(linkElement);
// fillWorkPhone(empId);
}
function set_emp(eName,eID){
    clearTable1();
    document.frmSearch.assignedToUID.value =eName;
    document.frmSearch.preAssignEmpId.value =eID;
}
var isIE;
function initRequest(url) {
    
    if (window.XMLHttpRequest) {
        return new XMLHttpRequest();
    }
    else
    if (window.ActiveXObject) {
        isIE = true;
        return new ActiveXObject("Microsoft.XMLHTTP");
    }
    
}
function clearTable1() {
    if (completeTable) {
        completeTable.setAttribute("bordercolor", "white");
        completeTable.setAttribute("border", "0");
        completeTable.style.visible = false;
        for (loop = completeTable.childNodes.length -1; loop >= 0 ; loop--) {
            completeTable.removeChild(completeTable.childNodes[loop]);
        }
    }
}


function hideScrollBar() {
    autorow = document.getElementById("menu-popup");
    autorow.style.display = 'none';
}
function findPosition( oElement ) {
    if( typeof( oElement.offsetParent ) != undefined ) {
        for( var posX = 0, posY = 0; oElement; oElement = oElement.offsetParent ) {
            posX += oElement.offsetLeft;
            posY += oElement.offsetTop;
        }
        return posX+","+posY;
    } else {
        return oElement.x+","+oElement.y;
    }
}
/*******************************************************************************
 * PMO suggestion list end
 ******************************************************************************/



/*
 * 
 * Termination Details Overaly scripts start Date : 03/30/2016 Author : Teja
 * kadamanti
 */
function checkTerminationDetails(empId,loginId,currStatus){
    if(currStatus=='Inactive'){
        alert("Employee already in Inactive state.");
    }else {
        getTerminationDetails(empId,loginId)
    }
}



function  getTerminationDetails(empId,loginId){
    
   
    $.ajax({
        url:CONTENXT_PATH+"/getTerminationDetails.action?empId="+empId+"&loginId="+loginId,
        context: document.body,
        success: function(responseText) {
            var resTextSplit1 = responseText.split("#^$");
           
            if(resTextSplit1=='reportsToExists'){
                alert("Please change the reports to of his team")  ; 
            }else if(resTextSplit1=='activeInProjects'){
                // alert("This employee allocated to project please contact to
				// PMO Before Termination! ") ;
                alert("This employee is still active in some of the projects .Please contact to PMO Before Termination!")  ; 
            }else{
                setDetails(empId,resTextSplit1);
            }
              
        },
        error: function(e){
            // document.getElementById("loading").style.display = 'none';
            alert("Please try again");
        }
    });
}


function setDetails(empId,resText){
    var today = new Date();
    var dd = today.getDate(); 
    var mm = today.getMonth()+1; 
    // alert("mm"+mm.toString().length);
    if(mm.toString().length==1){
        mm="0"+mm;  
    }
    var yyyy = today.getFullYear(); 
    var now=mm+"/"+dd+"/"+yyyy;
    // alert("now.."+now);
  
    document.getElementById('employeeId').value=empId;
    document.getElementById('designation').value=resText[2];
    document.getElementById('dateOfJoin').value=resText[3];
    if(resText[5]=='Inactive' || resText[5]=='InActive'){
        document.getElementById('dateOfTermination').value=resText[4];
             
    }else if(resText[4]!=null && resText[4]!=""){
        document.getElementById('dateOfTermination').value=resText[4];
    }else{
        document.getElementById('dateOfTermination').value=now;
    }
    if(resText[5]=='Inactive' || resText[5]=='InActive'){
        document.getElementById('reasonsForExit').innerHTML=resText[6];  
    }else if(resText[6]!=null && resText[6]!=""){
        document.getElementById('reasonsForExit').innerHTML=resText[6];  
             
    }else{
        document.getElementById('reasonsForExit').innerHTML="";     
    }
    document.getElementById('employeeName').innerHTML=resText[1];  
    document.getElementById("headerLabel1").style.color="white";
    document.getElementById("headerLabel1").innerHTML="Termination Details";
            
    var overlay = document.getElementById('overlay1');
    var specialBox = document.getElementById('specialBox1');
    overlay.style.opacity = .8;
    if(overlay.style.display == "block"){
        overlay.style.display = "none";
        specialBox.style.display = "none";
    }
    else {
        overlay.style.display = "block";
        specialBox.style.display = "block";
    }
}


function toggleCloseUploadOverlay2() {
    var overlay = document.getElementById('overlay1');
    var specialBox = document.getElementById('specialBox1');

    overlay.style.opacity = .8;
    if(overlay.style.display == "block"){
        overlay.style.display = "none";
        specialBox.style.display = "none";
    }
    else {
        overlay.style.display = "block";
        specialBox.style.display = "block";
    }
    document.frmEmpSearch.submit();
            
// window.location="getEmployee.action?empId="+;
}

function toggleCloseUploadOverlayEditPage() {
    var overlay = document.getElementById('overlay1');
    var specialBox = document.getElementById('specialBox1');

    overlay.style.opacity = .8;
    if(overlay.style.display == "block"){
        overlay.style.display = "none";
        specialBox.style.display = "none";
    }
    else {
        overlay.style.display = "block";
        specialBox.style.display = "block";
    }
    window.location="getEmployee.action?empId="+document.getElementById('id').value;
}

function checkIfStatusInActive(){
    var empId=document.getElementById("id").value;
    var currStatus=document.getElementById("currStatus").value;
    var loginId=document.getElementById("loginId").value;
    var prevCurStatus=document.getElementById("preCurrStatus").value;
    if(currStatus=='Inactive' && currStatus!=prevCurStatus){

        getTerminationDetails(empId,loginId);
    }else{
        // document.getElementById("employeeForm").action="employeeUpdate.action";
        if(empUpdateValidation())
            document.forms["employeeForm"].submit();
    }
     

}

function empUpdateValidation(){
    var country=document.getElementById("country").value;
    var empno=document.getElementById("empno").value;
    var location=document.getElementById("location").value;
    var orgId=document.getElementById("orgId").value;
    var prvExpMnths = document.getElementById("prvexpMnths").value;
    var prvexpYears = document.getElementById("prvexpYears").value;
    var lateralFlag = document.getElementById("lateralFlag");
    var employeeTitleId = document.getElementById("titleId").value;

    // var country=document.getElementById("country").value;
    if(country=='India'){
        if(empno.trim()==''){
            alert("EMPNO is mandatory.");
            return false;
        }
    }else{
        if(empno.trim()==''){
            document.getElementById("empno").value=0;
        }
    }
    
    if(orgId == '' || orgId == null){
        alert("Please select Organization.");
        return false;
    }
    
    // alert(location);
    if(country=='India'||country=='USA'){
        if(location=='-1'){
            alert("Please select location.");
            return false;
        }
    }
    if (isNaN(prvExpMnths)) 
    {
        alert("Please enter numeric values for months");
        return false;
    }
    
    if(parseInt(prvExpMnths,10) > 12 )
    {
            
        alert("Enter values between 1-12");
        return false;
    }
    if (isNaN(prvexpYears)) 
    {
        alert("Please enter numeric values for years");
        return false;
    }
    if(lateralFlag.checked){
        if(prvExpMnths=="" && prvexpYears=="" || prvExpMnths=='0' && prvexpYears=='0' || prvExpMnths==' ' && prvexpYears==' ' || prvExpMnths=='0' && prvexpYears==' ' || prvExpMnths==' ' && prvexpYears=='0'){
           
            alert("enter prev exp values");
            return false;
        }
    }
    // alert(employeeTitleId)
  /*
	 * if(employeeTitleId.trim().length==0){ alert("Please select Title!");
	 * return false; }
	 */
    return true;
}



function getTerminationReason(id){
   
    var  reasonsForTerminate=document.getElementById('reasonsForTerminate').value;
  
    var background = "#3E93D4";
    var title = "Reason for exit:";
  
    var size = reasonsForTerminate.length;
    
    // Now create the HTML code that is required to make the popup
    var content = "<html><head><title>"+title+"</title></head>\
    <body bgcolor='"+background +"' style='color:white;'><h4>"+title+"</h4>"+id.value+"<br />\
    </body></html>";
    
    if(size < 50){
        // Create the popup
        popup = window.open("","window","channelmode=0,width=300,height=150,top=200,left=350,scrollbars=no,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no");    
        popup.document.write(content); // Write content into it.
    }
    
    else if(size < 100){
        // Create the popup
        popup = window.open("","window","channelmode=0,width=400,height=200,top=200,left=350,scrollbars=no,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no");    
        popup.document.write(content); // Write content into it.
    }
    
    else if(size < 260){
        // Create the popup
        popup = window.open("","window","channelmode=0,width=400,height=200,top=200,left=350,scrollbars=no,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no");    
        popup.document.write(content); // Write content into it.
    } else {
        // Create the popup
        popup = window.open("","window","channelmode=0,width=400,height=200,top=200,left=350,scrollbars=no,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no");    
        popup.document.write(content); // Write content into it.
    }
    
}


function  doSaveTermainationDetails(){
    var  empId=document.getElementById('employeeId').value; 
    var  dateOfTermination=document.getElementById('dateOfTermination').value;
    var size = document.getElementById('reasonsForExit').value.length;
    var exitType=document.getElementById('exitType').value;
    // alert("size-->"+size);
    var  reasonsForTerminate=document.getElementById('reasonsForExit').value;
   
    if(dateOfTermination==''){
        document.getElementById('resultMessage2').innerHTML="Date of termination is mandatory";
        document.getElementById('resultMessage2').style.color="red";   
        return false;
    }
    if(exitType.trim()=='')
    {
    	
    	document.getElementById('resultMessage2').innerHTML="Please Select ExitType";
        document.getElementById('resultMessage2').style.color="red";   
        return false;
    }
    if(parseInt(size,10)>1000){
        document.getElementById('resultMessage2').innerHTML="<font color='red'>Reasons for Exit must be less than 1000 characters!</font>";
        return false;
    }

    document.getElementById('load').style.display='block';
    // alert("loginId.."+loginId);
   /*
	 * $.ajax({
	 * url:CONTENXT_PATH+"/doUpdateTerminationDetails.action?empId="+empId+"&dateOfTermination="+dateOfTermination+"&reasonsForTerminate="+escape(reasonsForTerminate),
	 * context: document.body, success: function(responseText) {
	 * if(responseText=='Success'){
	 * document.getElementById('load').style.display='none';
	 * document.getElementById('resultMessage2').innerHTML="Termination Details
	 * Updated Successfully";
	 * document.getElementById('resultMessage2').style.color="green"; }else{
	 * document.getElementById('load').style.display='none';
	 * document.getElementById('resultMessage2').innerHTML="Please try again
	 * later"; document.getElementById('resultMessage2').style.color="red"; } },
	 * error: function(e){ document.getElementById('load').style.display='none';
	 * document.getElementById('resultMessage2').innerHTML="Please try again
	 * later"; document.getElementById('resultMessage2').style.color="red"; }
	 * });
	 */
    
    var myObj = {};
    myObj["empId"]=empId;
    myObj["dateOfTermination"]=dateOfTermination;
    myObj["reasonsForTerminate"]=reasonsForTerminate;
    myObj["exitType"]=exitType;
    var json = JSON.stringify(myObj);
    $.ajax({
       // url:CONTENXT_PATH+"/doUpdateTerminationDetails.action?empId="+empId+"&dateOfTermination="+dateOfTermination+"&reasonsForTerminate="+escape(reasonsForTerminate),
        // context: document.body,
    	
    	url:'doUpdateTerminationDetails.action',
        data:{terminateDetailsJson: json},
        contentType: 'application/json',
        type: 'GET',
        context: document.body,
        success: function(responseText) {
            if(responseText=='Success'){
                document.getElementById('load').style.display='none';
                document.getElementById('resultMessage2').innerHTML="Termination Details Updated Successfully";
                document.getElementById('resultMessage2').style.color="green";
            }else{
                document.getElementById('load').style.display='none';
                document.getElementById('resultMessage2').innerHTML="Please try again later";
                document.getElementById('resultMessage2').style.color="red";
            }
              
        },
        error: function(e){
            document.getElementById('load').style.display='none';
            document.getElementById('resultMessage2').innerHTML="Please try again later";
            document.getElementById('resultMessage2').style.color="red";
        }
    });
}


/*
 * termination details overlay end
 * 
 */


function getLocationsByCountry(obj){
     
    var req = newXMLHttpRequest();
    req.onreadystatechange = readyStateHandler(req, populateEmployeeLocations);
    var url = CONTENXT_PATH+"/getEmployeeLocations.action?country="+obj.value;
    req.open("GET",url,"true");
    req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    req.send(null);
}



function populateEmployeeLocations(resXML) {    
    var locationObj = document.getElementById("location");
    var country = resXML.getElementsByTagName("COUNTRY")[0];
    // var teamMember = team.getElementsByTagName("TEAMMEMBER");
    locationObj.innerHTML=" ";
    for(var i=0;i<country.childNodes.length;i++) {
        var location = country.childNodes[i];
        var id =location.getElementsByTagName("LOCATION-ID")[0];        
        var name = location.getElementsByTagName("LOCATION-NAME")[0];
        // alert(id.childNodes[0].nodeValue);
        var opt = document.createElement("option");
        opt.setAttribute("value",id.childNodes[0].nodeValue);
        opt.appendChild(document.createTextNode(name.childNodes[0].nodeValue));
        // alert(name.childNodes[0].nodeValue);
        locationObj.appendChild(opt);
    }
}


function readyStateHandler33(req,responseXmlHandler) {
    return function() {
        if (req.readyState == 4) {
            if (req.status == 200) {
                responseXmlHandler(req.responseXML);
            } else {
               // alert("HTTP error"+req.status+" : "+req.statusText);
            }
        }
    }
}


function getResourceTypeDetails(projectId){
    var req = newXMLHttpRequest();
   
    req.onreadystatechange = readyStateHandlerText(req, displayResourceTypeDetails);
    
    var url = CONTENXT_PATH+"/getResourceTypeDetails.action?projectId="+projectId;
    req.open("GET",url,"true");
    req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    req.send(null);
}

function displayResourceTypeDetails(response) {
    var tableId = document.getElementById("tblResourceTypeDetails");
    ClrTable(tableId);
    var headerFields = new Array("Resource Type","Total");
   
    // alert("responseArray[1]-->"+responseArray[1]);
    // document.getElementById("totalRequirementsFound").innerHTML =
	// responseArray[0];
    var dataArray = response;
    
    // generateTableHeader(tableId,headerFields)
    ParseAndGenerateHTML(tableId,dataArray, headerFields);
 
 
    document.getElementById("headerLabel1").style.color="white";
    document.getElementById("headerLabel1").innerHTML="Resource Type Details";
            
    var overlay = document.getElementById('overlayResourceType');
    var specialBox = document.getElementById('specialBoxResourceType');
    overlay.style.opacity = .8;
    if(overlay.style.display == "block"){
        overlay.style.display = "none";
        specialBox.style.display = "none";
    }
    else {
        overlay.style.display = "block";
        specialBox.style.display = "block";
    }

    
}



function toggleCloseUploadOverlay1() {
    var overlay = document.getElementById('overlayResourceType');
    var specialBox = document.getElementById('specialBoxResourceType');

    overlay.style.opacity = .8;
    if(overlay.style.display == "block"){
        overlay.style.display = "none";
        specialBox.style.display = "none";
    }
    else {
        overlay.style.display = "block";
        specialBox.style.display = "block";
    }
// window.location="empSearchAll.action";
}



function getTerminationDetailsForWrong(id,loginId){
    
    var empId=document.getElementById("id").value;
    // var empId=id.value;
    var login=document.getElementById("loginId").value;
    // var login=loginId.value;
    $.ajax({
        url:CONTENXT_PATH+"/getTerminationDetailsForInActivePerson.action?empId="+empId+"&loginId="+login,
        context: document.body,
        success: function(responseText) {
            var resTextSplit1 = responseText.split("#^$");
           
            
            setDetails(empId,resTextSplit1);
             
              
        },
        error: function(e){
            // document.getElementById("loading").style.display = 'none';
            alert("Please try again");
        }
    });
}




/*
 * 
 * Tasks DashBoards Related Methods
 */


function getIssuesDashBoardByStatus()
{
    $('span.pagination').empty().remove();
    var taskStartDate=document.getElementById("taskStartDate").value;
    var taskEndDate=document.getElementById("taskEndDate").value;
    var reportsTo=document.getElementById("reportsTo").value;
    if(taskStartDate.trim().length==0){
        alert("please select start date");
        return false;
    }
    if(taskEndDate.trim().length==0){
        alert("please select end date");
        return false;
    }
    document.getElementById("IssuesDashBoardByStatusPie").style.display="none";
    document.getElementById("IssuesDashBoardByPriorityPie").style.display="none";
    document.getElementById("IssuesDashBoardByAssignmentPie").style.display="none";
    document.getElementById("IssuesDashBoardByCategoryPie").style.display="none";
    var tableId = document.getElementById("tblTasksDashBoard");
    ClrTable(tableId);
    var req = newXMLHttpRequest();
    req.onreadystatechange = function() {
        if (req.readyState == 4) {
            if (req.status == 200) {      
                document.getElementById("projectReport").style.display = 'none';
                displayIssuesDashBoardByStatusPieChart(req.responseText); 
                getIssuesDashBoardByPriority();
            } 
        }else {
            document.getElementById("projectReport").style.display = 'block';
        }
    }; 
    var url = CONTENXT_PATH+"/getIssuesDashBoardByStatus.action?reportsTo="+reportsTo+"&taskStartDate="+taskStartDate+"&taskEndDate="+taskEndDate+"&graphId=1";
    
    req.open("GET",url,"true");    
    req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    req.send(null);
}

function displayIssuesDashBoardByStatusPieChart(response) {
    var dataArray = response;
    if(response!=''){
           
        issuesDashBoardByStatusPieChart(response);
   
    }else{
        alert('No Result For This Search...');
        spnFast.innerHTML="No Result For This Search...";                
    }
     
// getIssuesDashBoardByPriority();
}


function issuesDashBoardByStatusPieChart(result){
    document.getElementById("IssuesDashBoardByStatusPie").style.display="block";
      
    var arraydata = [['Status', 'Count']];
    
    var result1=result.split("*@!");
    
    for(var i=0; i<result1.length-1; i++){
        
        var res = result1[i].split("#^$");
        var dArray = [res[0],parseInt(res[1])];
        arraydata.push(dArray);
    }

    var data = google.visualization.arrayToDataTable(arraydata);
    var options = {
        title: 'Tasks by Status' ,
        titleTextStyle: {
            color: ('blue', '#3E93D4'),    // any HTML string color ('red',
											// '#cc00cc')
            fontName: 'Times New Roman', // i.e. 'Times New Roman'
            fontSize: 15, // 12, 18 whatever you want (don't specify px)
            bold: true,    // true or false
            italic: false   // true of false
        },
        legend: 'right',
        chartArea:{
            
            width:"80%",
            height:"80%"
        },
        
        is3D: true,
        pieSliceText: 'value',
        sliceVisibilityThreshold: 0
         
    };
    // align:'center';
    var chart = new google.visualization.PieChart(document.getElementById("IssuesDashBoardByStatusPieChart"));
    function selectHandler() {
             
        var selectedItem = chart.getSelection()[0];
        // alert("selectedItem--"+data.getValue(selectedItem.row, 0));
        if (selectedItem) {
            var activityType = data.getValue(selectedItem.row, 0);
            // getBdmStatisticsDetailsByLoginId(activityType,bdmId);
            getTaskListByStatus(activityType,5);
        }
    }
    google.visualization.events.addListener(chart, 'select', selectHandler);   
    chart.draw(data, options,dArray);
    
    
}	


function getIssuesDashBoardByPriority()
{
    var taskStartDate=document.getElementById("taskStartDate").value;
    var taskEndDate=document.getElementById("taskEndDate").value;
    var reportsTo=document.getElementById("reportsTo").value;
    if(taskStartDate.trim().length==0){
        alert("please select start date");
        return false;
    }
    if(taskEndDate.trim().length==0){
        alert("please select end date");
        return false;
    }
    document.getElementById("IssuesDashBoardByPriorityPie").style.display="none";
    var req = newXMLHttpRequest();
    req.onreadystatechange = function() {
        if (req.readyState == 4) {
            if (req.status == 200) {      
                document.getElementById("projectReport").style.display = 'none';
                displayIssuesDashBoardByPriorityPieChart(req.responseText);  
                getIssuesDashBoardByAssignment();
            } 
        }else {
            document.getElementById("projectReport").style.display = 'block';
        }
    }; 
    var url = CONTENXT_PATH+"/getIssuesDashBoardByStatus.action?reportsTo="+reportsTo+"&taskStartDate="+taskStartDate+"&taskEndDate="+taskEndDate+"&graphId=2";
    
    req.open("GET",url,"true");    
    req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    req.send(null);
}

function displayIssuesDashBoardByPriorityPieChart(response) {
    var dataArray = response;
    if(response!=''){
           
        issuesDashBoardByPriorityPieChart(response);
   
    }else{
        alert('No Result For This Search...');
        spnFast.innerHTML="No Result For This Search...";                
    }
    
// getIssuesDashBoardByAssignment();
}


function issuesDashBoardByPriorityPieChart(result){
    document.getElementById("IssuesDashBoardByPriorityPie").style.display="block";
      
    var arraydata = [['Priority', 'Count']];
    
    var result1=result.split("*@!");
    
    for(var i=0; i<result1.length-1; i++){
        
        var res = result1[i].split("#^$");
        var dArray = [res[0],parseInt(res[1])];
        arraydata.push(dArray);
    }

    var data = google.visualization.arrayToDataTable(arraydata);
    var options = {
        title: 'Tasks by Priority' ,
        titleTextStyle: {
            color: ('blue', '#3E93D4'),    // any HTML string color ('red',
											// '#cc00cc')
            fontName: 'Times New Roman', // i.e. 'Times New Roman'
            fontSize: 15, // 12, 18 whatever you want (don't specify px)
            bold: true,    // true or false
            italic: false   // true of false
        },
        legend: 'right',
        chartArea:{
            
            width:"80%",
            height:"80%"
        },
        
        is3D: true,
        pieSliceText: 'value',
        sliceVisibilityThreshold: 0
         
    };
    // align:'center';
    var chart = new google.visualization.PieChart(document.getElementById("IssuesDashBoardByPriorityPieChart"));
    function selectHandler() {
             
        var selectedItem = chart.getSelection()[0];
        // alert("selectedItem--"+data.getValue(selectedItem.row, 0));
        if (selectedItem) {
            var activityType = data.getValue(selectedItem.row, 0);
            // getBdmStatisticsDetailsByLoginId(activityType,bdmId);
            getTaskListByStatus(activityType,6);
        }
    }
    google.visualization.events.addListener(chart, 'select', selectHandler);   
   
    chart.draw(data, options,dArray);
        
    
}


function getIssuesDashBoardByAssignment()
{
    var taskStartDate=document.getElementById("taskStartDate").value;
    var taskEndDate=document.getElementById("taskEndDate").value;
    var reportsTo=document.getElementById("reportsTo").value;
    if(taskStartDate.trim().length==0){
        alert("please select start date");
        return false;
    }
    if(taskEndDate.trim().length==0){
        alert("please select end date");
        return false;
    }
    document.getElementById("IssuesDashBoardByAssignmentPie").style.display="none";
    var req = newXMLHttpRequest();
    req.onreadystatechange = function() {
        if (req.readyState == 4) {
            if (req.status == 200) {      
                document.getElementById("projectReport").style.display = 'none';
                displayIssuesDashBoardByAssignmentPieChart(req.responseText);
                getIssuesDashBoardByCategory();
            } 
        }else {
            document.getElementById("projectReport").style.display = 'block';
        }
    }; 
    var url = CONTENXT_PATH+"/getIssuesDashBoardByStatus.action?reportsTo="+reportsTo+"&taskStartDate="+taskStartDate+"&taskEndDate="+taskEndDate+"&graphId=3";
    
    req.open("GET",url,"true");    
    req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    req.send(null);
}

function displayIssuesDashBoardByAssignmentPieChart(response) {
    var dataArray = response;
   
    if(response!=''){
           
        issuesDashBoardByAssignmentPieChart(response);
   
    }else{
        alert('No Result For This Search...');
        spnFast.innerHTML="No Result For This Search...";                
    }
    
// getIssuesDashBoardByCategory();
}


function issuesDashBoardByAssignmentPieChart(result){
    document.getElementById("IssuesDashBoardByAssignmentPie").style.display="block";
      
    var arraydata = [['Assignment', 'Count']];
    
    var result1=result.split("*@!");
    
    for(var i=0; i<result1.length-1; i++){
        
        var res = result1[i].split("#^$");
        var dArray = [res[0],parseInt(res[1])];
        arraydata.push(dArray);
    }

    var data = google.visualization.arrayToDataTable(arraydata);
    var options = {
        title: 'Tasks by Assignmnet' ,
        titleTextStyle: {
            color: ('blue', '#3E93D4'),    // any HTML string color ('red',
											// '#cc00cc')
            fontName: 'Times New Roman', // i.e. 'Times New Roman'
            fontSize: 15, // 12, 18 whatever you want (don't specify px)
            bold: true,    // true or false
            italic: false   // true of false
        },
        legend: 'right',
        chartArea:{
            
            width:"80%",
            height:"80%"
        },
        
        is3D: true,
        pieSliceText: 'value',
        sliceVisibilityThreshold: 0
         
    };
    // align:'center';
    var chart = new google.visualization.PieChart(document.getElementById("IssuesDashBoardByAssignmentPieChart"));

    function selectHandler() {
             
        var selectedItem = chart.getSelection()[0];
        // alert("selectedItem--"+data.getValue(selectedItem.row, 0));
        if (selectedItem) {
            var activityType = data.getValue(selectedItem.row, 0);
            // getBdmStatisticsDetailsByLoginId(activityType,bdmId);
            getTaskListByStatus(activityType,7);
        }
    }
    google.visualization.events.addListener(chart, 'select', selectHandler);   
    chart.draw(data, options,dArray);
    
}


function getIssuesDashBoardByCategory()
{
    var taskStartDate=document.getElementById("taskStartDate").value;
    var taskEndDate=document.getElementById("taskEndDate").value;
    var reportsTo=document.getElementById("reportsTo").value;
    if(taskStartDate.trim().length==0){
        alert("please select start date");
        return false;
    }
    if(taskEndDate.trim().length==0){
        alert("please select end date");
        return false;
    }
    document.getElementById("IssuesDashBoardByCategoryPie").style.display="none";
    var req = newXMLHttpRequest();
    req.onreadystatechange = function() {
        if (req.readyState == 4) {
            if (req.status == 200) {      
                document.getElementById("projectReport").style.display = 'none';
                displayIssuesDashBoardByCategoryPieChart(req.responseText);                        
            } 
        }else {
            document.getElementById("projectReport").style.display = 'block';
        }
    }; 
    var url = CONTENXT_PATH+"/getIssuesDashBoardByStatus.action?reportsTo="+reportsTo+"&taskStartDate="+taskStartDate+"&taskEndDate="+taskEndDate+"&graphId=4";
    
    req.open("GET",url,"true");    
    req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    req.send(null);
}

function displayIssuesDashBoardByCategoryPieChart(response) {
    var dataArray = response;
    
   
    if(response!=''){
           
        issuesDashBoardByCategoryPieChart(response);
   
    }else{
        alert('No Result For This Search...');
        spnFast.innerHTML="No Result For This Search...";                
    }
    
// getResourceUtilizationPieChart();
}


function issuesDashBoardByCategoryPieChart(result){
    document.getElementById("IssuesDashBoardByCategoryPie").style.display="block";
      
    var arraydata = [['Category', 'Count']];
    
    var result1=result.split("*@!");
    
    for(var i=0; i<result1.length-1; i++){
        
        var res = result1[i].split("#^$");
        var dArray = [res[0],parseInt(res[1])];
        arraydata.push(dArray);
    }

    var data = google.visualization.arrayToDataTable(arraydata);
    var options = {
        
        title: 'Tasks by Category' ,
        titleTextStyle: {
            color: ('blue', '#3E93D4'),    // any HTML string color ('red',
											// '#cc00cc')
            fontName: 'Times New Roman', // i.e. 'Times New Roman'
            fontSize: 15, // 12, 18 whatever you want (don't specify px)
            bold: true,    // true or false
            italic: false   // true of false
        },

        legend: 'right',
        chartArea:{
            
            width:"80%",
            height:"80%"
        },
        
        is3D: true,
        pieSliceText: 'value',
        sliceVisibilityThreshold: 0
         
    };
    // align:'center';
    var chart = new google.visualization.PieChart(document.getElementById("IssuesDashBoardByCategoryPieChart"));
    
    function selectHandler() {
             
        var selectedItem = chart.getSelection()[0];
        // alert("selectedItem--"+data.getValue(selectedItem.row, 0));
        if (selectedItem) {
            var activityType = data.getValue(selectedItem.row, 0);
            // getBdmStatisticsDetailsByLoginId(activityType,bdmId);
            getTaskListByStatus(activityType,8);
        }
    }
    google.visualization.events.addListener(chart, 'select', selectHandler);   
   
    chart.draw(data, options,dArray);
    
}

function  getTaskListByStatus(activityType,graphId){
    var taskStartDate=document.getElementById("taskStartDate").value;
    var taskEndDate=document.getElementById("taskEndDate").value;
    var reportsTo=document.getElementById("reportsTo").value;
    if(taskStartDate.trim().length==0){
        alert("please select start date");
        return false;
    }
    if(taskEndDate.trim().length==0){
        alert("please select end date");
        return false;
    }
    var tableId = document.getElementById("tblTasksDashBoard");
    ClrTable(tableId);
    // var startDate=document.getElementById('startDateSummaryGraph').value;
    // var endDate=document.getElementById('endDateSummaryGraph').value;
    document.getElementById('projectReport').style.display='block';
   
    var url = CONTENXT_PATH+"/getTaskListByStatus.action?reportsTo="+reportsTo+"&taskStartDate="+taskStartDate+"&taskEndDate="+taskEndDate+"&activityType="+activityType+"&graphId="+graphId;

    var req = newXMLHttpRequest();
    req.onreadystatechange = function() {
        if (req.readyState == 4) {
            if (req.status == 200) {      
                document.getElementById("projectReport").style.display = 'none';
                displayTaskDetailsByStatus(req.responseText,activityType);                        
            } 
        }else {
            document.getElementById("projectReport").style.display = 'block';
        }
    };
    req.open("GET", url, true);
    req.send(null); 
}
 
function displayTaskDetailsByStatus(response,activityType){
    var tableId = document.getElementById("tblTasksDashBoard");  
    var headerFields = new Array("S.No","Title","Status","Severity","StartDate","DueDate","PriAssignTo","SecAssignTo","CreatedBy","Description");
   
    var dataArray = response;
    taskDetailsByStatusParseAndGenerateHTML(tableId,dataArray, headerFields,activityType);
}
 
function taskDetailsByStatusParseAndGenerateHTML(oTable,responseString,headerFields,activityType) {
    var fieldDelimiter = "#^$";
    var recordDelimiter = "*@!";   
    var records = responseString.split(recordDelimiter); 
    generateTaskDetailsByStatusRows(oTable,headerFields,records,fieldDelimiter,activityType);
}

function generateTaskDetailsByStatusRows(oTable, headerFields,records,fieldDelimiter,activityType) {	

    var tbody = oTable.childNodes[0];    
    tbody = document.createElement("TBODY");
    oTable.appendChild(tbody);
    generateTableHeader(tbody,headerFields);
    var rowlength;
    rowlength = records.length;
    if(rowlength >0 && records!=""){
        for(var i=0;i<rowlength-1;i++) {
                
            generateTaskDetailsByStatus(oTable,tbody,records[i],fieldDelimiter,activityType);
        }
        
    } else {
        generateNoRecords(tbody,oTable);
    }
    generateTaskDashBoardFooter(tbody,oTable);
    pagerOption();
}


function generateTaskDetailsByStatus(oTable,tableBody,record,delimiter,activityType){
    var row;
    var cell;
    var fieldLength;
    // var fields = record.split(delimiter);
    var fields = record.split("#^$");
    fieldLength = fields.length ;
    var length = fieldLength;
    
    row = document.createElement( "TR" );
    row.className="gridRowEven";
    tableBody.appendChild( row );
    
    for (var i=0;i<length;i++) {  
        if (i==2){
            cell = document.createElement( "TD" );
            cell.className="gridColumn";    
          
            cell.innerHTML =fields[i] ;
            cell.setAttribute("align","left");   
            row.appendChild( cell );   
        }
        else if(i==9){
            cell = document.createElement( "TD" );
            cell.className="gridColumn";       
            // alert(i);
            cell.setAttribute("align","center");
            cell.innerHTML = "<a href='javascript:getDescriptionByTaskId("+fields[i]+")'>View</a>";
        }
        else{
            cell = document.createElement( "TD" );
            cell.className="gridColumn";       
       
            cell.setAttribute("align","center");
            
            cell.innerHTML = fields[i]; 
        }
            
        if(fields[i]!=''){
            if(i==1)
            {
                cell.setAttribute("align","left");
            }
            else
            {
                cell.setAttribute("align","center");     
            }
            row.appendChild( cell );
        }
    }   
    row.appendChild( cell );
    
}

function isValidDate(event){
    var date = event.value;
    // var regExp =
	// /^([1-9]|[1][012])\/|-([1-9]|[1][0-9]|[2][0-9]|[3][01])\/|-([1][6-9][0-9][0-9]|[2][0][01][0-9])$/;

    if (date.match(/^(?:(0[1-9]|1[012])[\- \/.](0[1-9]|[12][0-9]|3[01])[\- \/.](19|20)[0-9]{2})$/)){
        // if(regExp.test(event.value)) {
        return true;
    }else{
        alert("Invalid Date");
        event.value = "";
        
        return false;
    }
}
function generateTaskDashBoardFooter(tbody) {
  
    var cell;
    var footer =document.createElement("TR");
    footer.className="gridPager";
    footer.setAttribute("Id", "taskFooter")
    tbody.appendChild(footer);
    cell = document.createElement("TD");
    cell.className="gridFooter";
    

    // cell.colSpan = "7";
    
    cell.colSpan = "10";
    
       
   

    footer.appendChild(cell);
}



function getDescriptionByTaskId(taskId){
    // alert("empId-->"+taskId);
    // alert("currId-->"+currId);
    
    // var departmentName = document.getElementById("departmentId").value;
    var req = newXMLHttpRequest();
    req.onreadystatechange = readyStateHandlerText(req, populateDescriptionByTaskId);
    var url = CONTENXT_PATH+"/getDescriptionByTaskId.action?id="+taskId;
    req.open("GET",url,"true");    
    req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    req.send(null);
}


function populateDescriptionByTaskId(resText) {
    var background = "#3E93D4";
    var title = "Task description";
    // var text1 = text;
    // var size = text1.length;
    
    
  
    
    var size = resText.length;
    
    // Now create the HTML code that is required to make the popup
    var content = "<html><head><title>"+title+"</title></head>\
    <body bgcolor='"+background +"' style='color:white;'><h4>"+title+"</h4>"+resText+"<br />\
    </body></html>";
    
    if(size < 50){
        // Create the popup
        popup = window.open("","window","channelmode=0,width=300,height=150,top=200,left=350,scrollbars=no,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no");    
        popup.document.write(content); // Write content into it.
    }
    
    else if(size < 100){
        // Create the popup
        popup = window.open("","window","channelmode=0,width=400,height=200,top=200,left=350,scrollbars=no,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no");    
        popup.document.write(content); // Write content into it.
    }
    
    else if(size < 260){
        // Create the popup
        popup = window.open("","window","channelmode=0,width=400,height=200,top=200,left=350,scrollbars=no,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no");    
        popup.document.write(content); // Write content into it.
    } else {
        // Create the popup
        popup = window.open("","window","channelmode=0,width=400,height=200,top=200,left=350,scrollbars=no,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no");    
        popup.document.write(content); // Write content into it.
    }
    
}
function getProjectsByAccountId(){
 
    var accountId = document.getElementById("customerName").value;
    if(accountId!=""){
        var req = newXMLHttpRequest();
        req.onreadystatechange = readyStateHandlerXml(req, populateProjectsList);
        var url = CONTENXT_PATH+"/getProjectsByAccountId.action?accountId="+accountId;
        req.open("GET",url,"true");
        req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
        req.send(null);   
    }else{
        var projects = document.getElementById("projectName");
        projects.innerHTML=" ";
      
        var opt = document.createElement("option");
        opt.setAttribute("value","");
        opt.appendChild(document.createTextNode("All"));
        projects.appendChild(opt);
    }
}
function populateProjectsList(resXML) {
  
    // var projects = document.getElementById("projectName");
    var projects = document.getElementById("projectName");
    // alert("test")
    
    var projectsList = resXML.getElementsByTagName("PROJECTS")[0];
   
    var users = projectsList.getElementsByTagName("USER");
    projects.innerHTML=" ";
    for(var i=0;i<users.length;i++) {
        var userName = users[i];
        var att = userName.getAttribute("projectId");
        var name = userName.firstChild.nodeValue;
        var opt = document.createElement("option");
        opt.setAttribute("value",att);
        opt.appendChild(document.createTextNode(name));
        projects.appendChild(opt);
    }
}

function generateFooter1(tbody) {
  
   var cell;
    var footer =document.createElement("TR");
    footer.className="gridPager";
    tbody.appendChild(footer);
    cell = document.createElement("TD");
    cell.className="gridFooter";

       // cell.colSpan = "7";
    
        cell.colSpan = "10";
    
        cell.innerHTML ="Total&nbsp;Records&nbsp;:"+totalRec;
      
          cell.setAttribute('align','right');
   

    footer.appendChild(cell);
}


/*
 * Appraisals starts
 */
function empQuarterAppraisalAdd(){
    var year=document.getElementById("year").value;
    var quarterly=document.getElementById("quarterly").value;
   
    if(year==""){
        x0p( '','Please Enter Year','info');
        return false;
    }
    if(quarterly==""){
        x0p( '','Please Select Quarter','info');
        return false;
    }
    window.location=CONTENXT_PATH+"/employee/appraisal/myQuarterlyAppraisalAdd.action?year="+year+"&quarterly="+quarterly;
}
function addCommentsForQAppraisal(type,rowCount){
    // alert("type---"+type);
    document.getElementById('resultMessage').innerHTML ='';
    document.getElementById('descriptionCount').innerHTML ='';
    var status=document.getElementById("status").value;
    var curretRole=document.getElementById('curretRole').value;
    var dayCount=document.getElementById('dayCount').value;
    var operationTeamStatus=document.getElementById('operationTeamStatus').value;
   
   
    
   
    var keyFactorName=document.getElementById("keyFactorName"+rowCount).value;
    document.getElementById("headerLabel").style.color="white";
    document.getElementById("headerLabel").innerHTML=keyFactorName;
    
   
    showRow('addTr');
    
    var overlay = document.getElementById('empQuartAppraisalOverlay');
    var specialBox = document.getElementById('empQuartAppraisalSpecialBox');
    overlay.style.opacity = .8;
    if(overlay.style.display == "block"){
        overlay.style.display = "none";
        specialBox.style.display = "none";
    }
    else {
        overlay.style.display = "block";
        specialBox.style.display = "block";
        
        if(type=='Employee'){
             
            var desc =  document.getElementById('empDescription'+rowCount).value;
            var placeholder=document.getElementById('empDescription'+rowCount).placeholder;
            if(placeholder.trim()=="_" || placeholder.trim()==""){
                 placeholder="Enter "+keyFactorName+" Description here....";
             }
            document.getElementById('description').value=desc;
            document.getElementById('descriptionId').value='empDescription'+rowCount;
            document.getElementById('description').placeholder=placeholder;
           
         // alert(status+"anand"+curretRole)
         if(document.getElementById('roleName').value=='Operations'){
                 hideRow('addTr');
               document.getElementById('description').readOnly=true;
         
        }else{
            if(curretRole=="team"){
                hideRow('addTr');
               document.getElementById('description').readOnly=true;
            }else  if(curretRole=="my" && (status.trim()=="Approved" || status.trim()=="Submitted")){
                hideRow('addTr');
                document.getElementById('description').readOnly=true;
            }
            else{
                 document.getElementById('description').readOnly=false;
            }
        }
        }else if(type=='Manager'){
       
            var desc =  document.getElementById('mgrComments'+rowCount).value;
             var placeholder=document.getElementById('mgrComments'+rowCount).placeholder;
            if(placeholder.trim()=="_" || placeholder.trim()==""){
                 placeholder="Enter "+keyFactorName+" Commnets here....";
             }
            document.getElementById('description').value=desc;
            document.getElementById('description').placeholder=placeholder;
            document.getElementById('descriptionId').value='mgrComments'+rowCount;
            if(document.getElementById('roleName').value=='Operations'){
                 hideRow('addTr');
               document.getElementById('description').readOnly=true;
         
        }else{
            if(curretRole=="team"){
                if(status.trim()=="Approved" || status.trim()=="Entered" ){
                    hideRow('addTr');
                    document.getElementById('description').readOnly=true;
                }else{
                    document.getElementById('description').readOnly=false;
                }
                
                if(dayCount==2 && (status=='Submited' || (status=='Approved' && operationTeamStatus=='Rejected'))){
                    showRow('addTr');
                    document.getElementById('description').readOnly=false;
                }
                if(dayCount==1 && status=='Rejected'){
                hideRow('addTr');
                    document.getElementById('description').readOnly=true;
                }
            }else{
                if(status.trim()=="Approved" || status.trim()=="Submitted"){
                    hideRow('addTr');
                    document.getElementById('description').readOnly=true;
                }else{
                     document.getElementById('description').readOnly=false;
                }
            }
            var userId1=document.getElementById('userId1').value;
            var backToFlag=document.getElementById('backToFlag').value;
            if(userId1=='rkalaga' && backToFlag==1){
                 hideRow('addTr');
                    document.getElementById('description').readOnly=true;
            }
        }
           
        }
        
else{
        	

            
            var desc =  document.getElementById('hrComments'+rowCount).value;
             var placeholder=document.getElementById('hrComments'+rowCount).placeholder;
            if(placeholder.trim()=="_" || placeholder.trim()==""){
                 placeholder="Enter "+keyFactorName+" Commnets here....";
             }
            document.getElementById('description').value=desc;
            document.getElementById('description').placeholder=placeholder;
            document.getElementById('descriptionId').value='hrComments'+rowCount;
            if(document.getElementById('roleName').value=='Operations'){
            	 hideRow('addTr');
            	 document.getElementById('description').readOnly=true;
            	if(dayCount==0){
            		 hideRow('addTr');
                     document.getElementById('description').readOnly=true;
            	}
            	 if(dayCount==1 && status=='Approved'){
                     showRow('addTr');
                     document.getElementById('description').readOnly=false;
                 }
            	 if(dayCount==1 && status=='Rejected'){
            		 hideRow('addTr');
                     document.getElementById('description').readOnly=true;
                 }
            	 if(dayCount==2){
            		 hideRow('addTr');
                     document.getElementById('description').readOnly=true;
                 }
         
        }
            var userId1=document.getElementById('userId1').value;
            var backToFlag=document.getElementById('backToFlag').value;
            if(userId1=='rkalaga' && backToFlag==1){
                 hideRow('addTr');
                    document.getElementById('description').readOnly=true;
            }
        }

        
        
    }
            
// window.location = "jobSearch.action";
}
        
function toggleAppraisalOverlay(){
                 
    document.getElementById("headerLabel").style.color="white";
    document.getElementById("headerLabel").innerHTML="Add Employee Quarterly Appraisal";
           
    var overlay = document.getElementById('empQuartAppraisalOverlay');
    var specialBox = document.getElementById('empQuartAppraisalSpecialBox');
    // hideRow("approvedBy");
    // hideRow("tlcommentsTr");
    // hideRow("hrcommentsTr");
    overlay.style.opacity = .8;
    if(overlay.style.display == "block"){
        overlay.style.display = "none";
        specialBox.style.display = "none";
        document.getElementById("resultMessage").innerHTML='';
    }
    else {
        overlay.style.display = "block";
        specialBox.style.display = "block";
    }
            
// window.location = "jobSearch.action";
}

function addAppraisalComments(){
    setSessionTimeout();
    var descriptionId=document.getElementById('descriptionId').value;
    var description=document.getElementById('description').value;
    document.getElementById(descriptionId).value=description;
    document.getElementById(descriptionId+"btn").innerHTML="Update";
   document.getElementById("addButton").disabled=true;
  
    document.getElementById("load").style.display='block';
    document.getElementById("resultMessage").innerHTML='';
    
    if(descriptionId.indexOf("hrComments")>-1){
        // alert("descriptionId");
          
          var id=descriptionId.replace("hrComments","id");
          var idValue=document.getElementById(id).value;
          
          if(idValue=='-1'){
  	        var quarterly = document.getElementById("quarterly").value;
        	  var empId=document.getElementById("empId").value;

          	 var appraisalId=document.getElementById("appraisalId").value;
              
              var measurementId=descriptionId.replace("hrComments","measurementId");
              
             
              var measurement=document.getElementById(measurementId).value;
              

              var keyFactorId=descriptionId.replace("hrComments","keyFactorId");
              var keyFactor=document.getElementById(keyFactorId).value;
              
              var keyFactorWeightageId=descriptionId.replace("hrComments","keyFactorWeightage");
              var keyFactorWeightage=document.getElementById(keyFactorWeightageId).value;
              
              var HrWeightageId=descriptionId.replace("hrComments","HrWeightage");
              var HrWeightage=document.getElementById(HrWeightageId).value;
              
              var myObj = {};  
             
              myObj["comments"] = description;
              myObj["empId"]=empId;
              myObj["appraisalId"]=appraisalId;
              myObj["measurement"]=measurement;
              myObj["keyFactor"]=keyFactor;
              myObj["keyFactorWeightage"]=keyFactorWeightage;
              myObj["HrWeightage"]=HrWeightage;
              myObj["quarterly"]=quarterly;
              
              var json = JSON.stringify(myObj);
              // alert(json);
              var url = CONTENXT_PATH+"/addQReviewHrDescriptions.action?rand="+new Date();;
              $.ajax({
      	       
      	     	
      	     	url:url,
      	         data:{jsonData: json},
      	         contentType: 'application/json',
      	         type: 'GET',
      	         context: document.body,
      	         success: function(responseText) {
      	        	// alert(responseText);
      	        	   var  resultJson = JSON.parse(responseText);
      	        	   
      	        	 if(resultJson['result']=='success'){
      	        		document.getElementById(id).value=resultJson['id'];
                          document.getElementById("resultMessage").innerHTML="<font color=\"green\" size=\"2\">Description saved successfully!</font>";
      	        	// alert(resultJson['id']);
                          
      	        	 }else{
                          document.getElementById("resultMessage").innerHTML="<font color=\"red\" size=\"2\">Sorry! Please try again latter!</font>";
                      }
                    
                 document.getElementById("addButton").disabled=false;
                 document.getElementById("load").style.display='none';
      	               
      	         },
      	         error: function(e){
      	             document.getElementById('load').style.display='none';
      	             document.getElementById('resultMessage').innerHTML="<font color=\"red\" size=\"2\">Sorry! Please try again latter!</font>";
      	            document.getElementById("finalSaveButton").disabled=false;
                     document.getElementById("addButton").disabled=false;
                     document.getElementById("load").style.display='none';
      	         }
      	     });
              
          }

          else{
        	  var url = CONTENXT_PATH+"/setQReviewHrComments.action?id="+idValue+"&comments="+escape(description)+"&rand="+new Date();;
              // alert(url);
                var req = initRequest(url);
                 req.onreadystatechange = function() {
                     // alert("req-->"+req);
                     if (req.readyState == 4) {
                         if (req.status == 200) {
                              // alert(req.responseText);
                              if(req.responseText=='success'){
                                  document.getElementById("resultMessage").innerHTML="<font color=\"green\" size=\"2\">Comment saved successfully!</font>";
                              }else{
                                  document.getElementById("resultMessage").innerHTML="<font color=\"red\" size=\"2\">Sorry! Please try again latter!</font>";
                              }
                        document.getElementById("load").style.display='none';
                         document.getElementById("addButton").disabled=false;
        
                            // parseEmpMessagesForProject(req.responseXML);
                         } 
                     }
                 };
                 req.open("GET", url, true);

                 req.send(null);
          }
            
     }else if(descriptionId.indexOf("mgrComments")>-1){
       // alert("descriptionId");
         
         var id=descriptionId.replace("mgrComments","id");
         var idValue=document.getElementById(id).value;
           var url = CONTENXT_PATH+"/setQReviewManagerComments.action?id="+idValue+"&comments="+escape(description)+"&rand="+new Date();;
         // alert(url);
           var req = initRequest(url);
            req.onreadystatechange = function() {
                // alert("req-->"+req);
                if (req.readyState == 4) {
                    if (req.status == 200) {
                         // alert(req.responseText);
                         if(req.responseText=='success'){
                             document.getElementById("resultMessage").innerHTML="<font color=\"green\" size=\"2\">Comment saved successfully!</font>";
                         }else{
                             document.getElementById("resultMessage").innerHTML="<font color=\"red\" size=\"2\">Sorry! Please try again latter!</font>";
                         }
                   document.getElementById("load").style.display='none';
                    document.getElementById("addButton").disabled=false;
   
                       // parseEmpMessagesForProject(req.responseXML);
                    } 
                }
            };
            req.open("GET", url, true);

            req.send(null);
    }else{
         document.getElementById("finalSaveButton").disabled=true;
        var lineId=document.getElementById("lineId").value;
        var empId=document.getElementById("empId").value;
        var shortTermGoal=document.getElementById("shortTermGoal").value;
        var longTermGoal=document.getElementById("longTermGoal").value;
        var strength=document.getElementById("strength").value;
        var improvements=document.getElementById("improvements").value;
        var quarterly=document.getElementById("quarterly").value;
        var qyear=document.getElementById("qyear").value;
        if(lineId=='0'){
            
         var rowCount = $("#measurementTable td").closest("tr").length;
         document.getElementById("rowCount").value=rowCount;
         var measurementId="";
                var keyFactorId="";
                var keyFactorWeightage="";
                var empDescription="";
                var empRating="";
                var id="";
                var HrWeightage="";
          if(rowCount>0){
               for(var i=1;i<=rowCount;i++){
                    measurementId=measurementId+ document.getElementById("measurementId"+i).value+"#^$";
                        keyFactorId=keyFactorId+ document.getElementById("keyFactorId"+i).value+"#^$";
                        keyFactorWeightage=keyFactorWeightage+ document.getElementById("keyFactorWeightage"+i).value+"#^$";
                        empDescription=empDescription+ document.getElementById("empDescription"+i).value+"#^$";
                        HrWeightage=HrWeightage+ document.getElementById("HrWeightage"+i).value+"#^$";

                        if(document.getElementById("empRating"+i).value==""){
                             empRating=empRating+"0"+"#^$";
                        }else{
                        empRating=empRating+ document.getElementById("empRating"+i).value+"#^$";
                        }
                         if( document.getElementById("id"+i).value==""){
                             id=id+"0"+"#^$";
                        }else{
                        id=id+ document.getElementById("id"+i).value+"#^$";
                        }
                       
                    
               }
          }
    
        document.getElementById("status").value="Entered";
        var myObj = {};  
        myObj["ids"] = id;
       
        myObj["appraisalId"] = 0;
        myObj["empId"] = empId;
        myObj["measurementId"] = measurementId;
        myObj["keyFactorId"] = keyFactorId;
        myObj["keyFactorWeightage"] = keyFactorWeightage;
        myObj["empDescription"] = empDescription;
        myObj["empRating"] = empRating;
        myObj["status"] = "Entered";
        myObj["shortTermGoal"] = shortTermGoal;
        myObj["longTermGoal"] = longTermGoal;
        myObj["strength"] = strength;
        myObj["improvements"] = improvements;
        myObj["quarterly"] = quarterly;
        myObj["qyear"] = qyear;
        myObj["rowCount"] = rowCount;
        myObj["HrWeightage"] = HrWeightage;
        var json = JSON.stringify(myObj);
                    // document.getElementById("frmAppraisal").submit();
      
        var url = CONTENXT_PATH+"/saveQReviewDetails.action?rand="+new Date();;
        $.ajax({
 	        // url:CONTENXT_PATH+"/doUpdateTerminationDetails.action?empId="+empId+"&dateOfTermination="+dateOfTermination+"&reasonsForTerminate="+escape(reasonsForTerminate),
 	         // context: document.body,
 	     	
 	     	url:url,
 	         data:{jsonData: json},
 	         contentType: 'application/json',
 	         type: 'GET',
 	         context: document.body,
 	         success: function(responseText) {
                 // alert(responseText);
                 var  resultJson = JSON.parse(responseText);
                var QuarterAppraisalDetails = resultJson["QuarterAppraisalDetails"];
             // alert(QuarterAppraisalDetails);
                var LineId = resultJson["LineId"];
                var AppraisalId = resultJson["AppraisalId"];
                var individualRowDetails=QuarterAppraisalDetails.split("*@!");
              // alert(individualRowDetails.length);
                for (i = 1; i < individualRowDetails.length; i++) {
                	var individualColumn = individualRowDetails[i - 1].split("#^$");
                	if(individualColumn[11] != '-1'){
                                                
                        /*
						 * console.log(i+"--"+ individualColumn[11]);
						 * console.log(individualColumn);
						 */
                           
                        document.getElementById("id"+i).value=individualColumn[11];  
                	}
                }
                
                document.getElementById("lineId").value=LineId;
                document.getElementById("appraisalId").value=AppraisalId;
                document.getElementById("status").value="Entered";
                
               // alert("id=="+document.getElementById("id10").value);
                   // parseEmpMessagesForProject(req0.responseXML);
                   var result= resultJson["result"];
                   if(result=='success'){
                        document.getElementById("resultMessage").innerHTML="<font color=\"green\" size=\"2\">Description saved successfully!</font>";
                     }else{
                        document.getElementById("resultMessage").innerHTML='<font color=\"red" size=\"2\">Sorry! Please try again latter!</font>';
                   }
                document.getElementById("finalSaveButton").disabled=false;    
            document.getElementById("addButton").disabled=false;
            document.getElementById("load").style.display='none'; 
 	               
 	         },
 	         error: function(e){
 	             document.getElementById('load').style.display='none';
 	             document.getElementById('resultMessage').innerHTML="Please try again later";
 	             
 	         }
 	     });
         // alert(url);
          
        }else{
        	
        	
         var id=descriptionId.replace("empDescription","id");
         var idValue=document.getElementById(id).value;
         var myObj = {};  
         myObj["id"] = idValue;
         myObj["comments"] = description;
         var json = JSON.stringify(myObj);
         var url = CONTENXT_PATH+"/setQReviewEmpDescriptions.action?rand="+new Date();;
         $.ajax({
 	       
 	     	
 	     	url:url,
 	         data:{jsonData: json},
 	         contentType: 'application/json',
 	         type: 'GET',
 	         context: document.body,
 	         success: function(responseText) {
 	        	 if(responseText=='success'){
                     document.getElementById("resultMessage").innerHTML="<font color=\"green\" size=\"2\">Description saved successfully!</font>";
                 }else{
                     document.getElementById("resultMessage").innerHTML=innerHTML="<font color=\"red\" size=\"2\">Sorry! Please try again latter!</font>";
                 }
                 document.getElementById("finalSaveButton").disabled=false;
            document.getElementById("addButton").disabled=false;
            document.getElementById("load").style.display='none';
 	               
 	         },
 	         error: function(e){
 	             document.getElementById('load').style.display='none';
 	             document.getElementById('resultMessage').innerHTML="Please try again later";
 	             
 	         }
 	     });
         
           
        
       
        }
    }
   
    // alert("test")
  // toggleAppraisalOverlay();
}
function isNumericQreview(element){
    
    var val=element.value;
    var numbers = /^[0-9]+$/;  
    if(!val.match(numbers))  {
        x0p( '','Please Enter numeric values','info');
        // element.value=val.substring(0, val.length-1);
        element.value="";      
        return false;
    }
    else if( val< 0 || val >10){
        x0p( '','Please Enter numeric values between 0 - 10 ','info');
        element.value="";      
        return false;
    }else
        return true;
}
function isNumericYearQreview(element){
    
    var val=element.value;
    var numbers = /^[0-9]+$/;  
    if(!val.match(numbers))  {
        x0p( '','Please Enter numeric values','info');
        // element.value=val.substring(0, val.length-1);
        element.value="";      
        return false;
    }
    else
        return true;
}



function weightageCalculation(rowCount){  
   
    
    var w1=document.getElementById("appraisalManagerRating"+rowCount).value;
    var w2=document.getElementById("keyFactorWeightage"+rowCount).value;
    if(w1)
        var w3=(parseFloat(w1)/10)*parseFloat(w2);
    if (isNaN(w3)) {
        document.getElementById("calWeightage"+rowCount).value="";
    }else{
        document.getElementById("calWeightage"+rowCount).value=parseFloat(w3).toFixed(2);
    }
   
    // return (num/100)*per;
    totalQuarterCalculations();
} 



function totalQuarterCalculations(){
	  
    var array = ["keyFactorWeightage", "empRating", "appraisalManagerRating","calWeightage"];
    var keyFactorWeightageArray=[];
    var appraisalEmpRatingArray=[];
    var appraisalManagerRatingArray=[];
    var calWeightageArray=[];
    var keyFactorWeightageArrayValues=[];
    var appraisalEmpRatingArrayValues=[];
    var appraisalManagerRatingArrayValues=[];
    var calWeightageArrayValues=[];
    var curretRole=document.getElementById("curretRole").value;
    var rowCount = $("#measurementTable td").closest("tr").length;
      var status=document.getElementById("status").value;
    for(var i=1;i<=rowCount;i++){
        keyFactorWeightageArray[(i-1)]=array[0]+i;
        appraisalEmpRatingArray[(i-1)]=array[1]+i;
        if(curretRole=="team" || status.trim()=='Approved'){
            appraisalManagerRatingArray[(i-1)]=array[2]+i;
            calWeightageArray[(i-1)]=array[3]+i;
        }
       
        if(document.getElementById(keyFactorWeightageArray[i-1]).value.trim()==""){
            keyFactorWeightageArrayValues[(i-1)]=0;
        }else{
            keyFactorWeightageArrayValues[(i-1)]=document.getElementById(keyFactorWeightageArray[i-1]).value;
        }
        
        if(document.getElementById(appraisalEmpRatingArray[i-1]).value.trim()==""){
            appraisalEmpRatingArrayValues[(i-1)]=0;
        }else{
            appraisalEmpRatingArrayValues[(i-1)]=document.getElementById(appraisalEmpRatingArray[i-1]).value;
        }
      
        if(curretRole=="team" || status.trim()=='Approved'){
       
            if(document.getElementById(appraisalManagerRatingArray[i-1]).value.trim()==""){
                appraisalManagerRatingArrayValues[(i-1)]=0;
            }else{
                appraisalManagerRatingArrayValues[(i-1)]=document.getElementById(appraisalManagerRatingArray[i-1]).value;
            }
        
            if(document.getElementById(calWeightageArray[i-1]).value.trim()==""){
                calWeightageArrayValues[(i-1)]=0;
            }else{
                calWeightageArrayValues[(i-1)]=document.getElementById(calWeightageArray[i-1]).value;
            }
        }
       
    }
    var totalKeyFactorCount=0;
    var totalEmpRatingCount=0;
    var totalMgrRatingCount=0;
    var totalCalWeightage=0;
  
    for(var i=0;i<rowCount;i++){
        // alert(keyFactorWeightageArrayValues[i]+"---"+calWeightageArrayValues[i]);
        totalKeyFactorCount=totalKeyFactorCount+parseFloat(keyFactorWeightageArrayValues[i]);
        totalEmpRatingCount=totalEmpRatingCount+parseFloat(appraisalEmpRatingArrayValues[i]);
        if(curretRole=="team" || status.trim()=='Approved'){
            totalMgrRatingCount=totalMgrRatingCount+parseFloat(appraisalManagerRatingArrayValues[i]);
            totalCalWeightage=totalCalWeightage+parseFloat(calWeightageArrayValues[i]);
        }
    }
     
    document.getElementById("totalKeyFactor").value=parseFloat(totalKeyFactorCount).toFixed(2);
    document.getElementById("totalEmpRating").value=parseFloat(totalEmpRatingCount).toFixed(2);
    if(curretRole=="team" || status.trim()=='Approved'){
         
        document.getElementById("totalMgrRating").value=parseFloat(totalMgrRatingCount).toFixed(2);
        document.getElementById("totalCalWeightage").value=parseFloat(totalCalWeightage).toFixed(2);
    }
    
}


function HrWeightageCalculation(rowCount){  
   
    
    var w1=document.getElementById("appraisalHrRating"+rowCount).value;
    var w2=document.getElementById("HrWeightage"+rowCount).value;
    if(w1)
        var w3=(parseFloat(w1)/10)*parseFloat(w2);
    if (isNaN(w3)) {
        document.getElementById("hrCalWeightage"+rowCount).value="";
    }else{
        document.getElementById("hrCalWeightage"+rowCount).value=parseFloat(w3).toFixed(2);
    }
   
    totalQuarterHrCalculations();
}

function totalQuarterHrCalculations(){
	  // alert("totalQuarterHrCalculations");
    var array = ["appraisalHrRating","hrCalWeightage"];
  
    var appraisalHrRating=[];
    var hrCalWeightage=[];
   
    var appraisalHrRatingArrayValues=[];
    var hrCalWeightageArrayValues=[];
    var curretRole=document.getElementById("curretRole").value;
    var rowCount = $("#measurementTable td").closest("tr").length;
      
    for(var i=1;i<=rowCount;i++){
        
      
        
        	appraisalHrRating[(i-1)]=array[0]+i;
        	hrCalWeightage[(i-1)]=array[1]+i;
        
       
        
        
       
      // alert(document.getElementById(appraisalHrRating[i-1]).value);
        
       
            if(document.getElementById(appraisalHrRating[i-1]).value.trim()==""){
            	appraisalHrRatingArrayValues[(i-1)]=0;
            }else{
            	appraisalHrRatingArrayValues[(i-1)]=parseFloat(document.getElementById(appraisalHrRating[i-1]).value);
            }
        
            if(document.getElementById(hrCalWeightage[i-1]).value.trim()==""){
            	hrCalWeightageArrayValues[(i-1)]=0;
            }else{
            	hrCalWeightageArrayValues[(i-1)]=parseFloat(document.getElementById(hrCalWeightage[i-1]).value);
            }
        
       // alert(appraisalHrRatingArrayValues[(i-1)]);
    }
   
    var totalHrRatingCount=0;
    var totalHrCalWeightage=0;
  
    for(var i=0;i<rowCount;i++){
        
        
       
    	totalHrRatingCount=totalHrRatingCount+parseFloat(appraisalHrRatingArrayValues[i]);
    	totalHrCalWeightage=totalHrCalWeightage+parseFloat(hrCalWeightageArrayValues[i]);
        
    }
     
    console.log("totalHrRatingCount"+totalHrRatingCount);
    console.log("totalHrCalWeightage"+totalHrCalWeightage);
        document.getElementById("totalHrRating").value=parseFloat(totalHrRatingCount).toFixed(2);
        document.getElementById("totalHrCalWeightage").value=parseFloat(totalHrCalWeightage).toFixed(2);
    
    
}




function doAddEmpQuarterAppraisal(){
    var rowCount = $("#measurementTable td").closest("tr").length;
  
    document.getElementById("rowCount").value=rowCount;
    // window.location=CONTENXT_PATH+"/employee/appraisal/empQuarterlyAppraisalAdd.action";
    document.forms["frmAppraisal"].submit();
}
function quarterAppraisalAdd(status,buttonObj){
    var rowCount = $("#measurementTable td").closest("tr").length;
    document.getElementById("rowCount").value=rowCount;
   
    var result = false;
    if(status=="Submitted" || status=="Resubmitted"){
        for(var i =1;i<=rowCount;i++){
            var empDescription=document.getElementById("empDescription"+i);
            var empRating=document.getElementById("empRating"+i);
            var keyFactorName=document.getElementById("keyFactorName"+i).value;
            
            if(keyFactorName!='Appreciations & Awards'){
            if(empDescription.value.trim()==""){
                x0p({
                    title:'',
                    text:'Please enter <b style="color:red">description</b> for <b>'+keyFactorName+'</b>',
                    icon: '',
                    html: true
                });
                empDescription.focus();
                return false;
            }
            if(empRating.value.trim()==""){
                // x0p( '','Please Enter rating for '+keyFactorName,'info');
             
                x0p({
                    title:'',
                    text:'Please enter <b style="color:red">rating</b> for <b>'+keyFactorName+'</b>',
                    icon: 'info',
                    html: true
                });
                empRating.focus();
                return false;
            }
        }
        }
        var shortTermGoal=document.getElementById("shortTermGoal").value;
        var longTermGoal=document.getElementById("longTermGoal").value;
        var strength=document.getElementById("strength").value;
        var improvements=document.getElementById("improvements").value;
    
        if(strength.trim()==""){
            x0p({
                title:'',
                text:'Please enter  <b style="color:red">personality details</b> (Click on personality button to enter stregnths)',
                icon: 'info',
                html: true
            });
            return false
        }
        if(improvements.trim()==""){
            x0p({
                title:'',
                text:'Please enter <b style="color:red">personality details</b> (Click on personality button to enter improvements)',
                icon: 'info',
                html: true
            });
            return false
        }
    
        if(shortTermGoal.trim()==""){
            x0p({
                title:'',
                text:'Please enter <b style="color:red">goals</b> (Click on Goals button to enter short term goals)',
                icon: 'info',
                html: true
            });
            return false
        }
        if(longTermGoal.trim()==""){
            x0p({
                title:'',
                text:'Please enter <b style="color:red">goals</b> (Click on Goals button to enter long term goals)',
                icon: 'info',
                html: true
            });
            return false
        }
   
        x0p(' Do you want to submit quarterly review?', '', 'warning',
            function(button) {
       
                if(button == 'warning') {
                    result = true;
                // alert(button)
                }
                if(button == 'cancel') { 
                    result = false;
                }
                if(result){
                   document.getElementById("status").value=status;
                    document.getElementById("frmAppraisal").submit();
                }

            }
            );
    }
    else if(status=="Approved" || status=="Reapproved"){
        for(var i =1;i<=rowCount;i++){
            var mgrComments=document.getElementById("mgrComments"+i);
            var appraisalManagerRating=document.getElementById("appraisalManagerRating"+i);
            var keyFactorName=document.getElementById("keyFactorName"+i).value;
             if(keyFactorName!='Appreciations & Awards'){
            if(mgrComments.value.trim()==""){
                x0p({
                    title:'',
                    text:'Please enter <b style="color:red">manager comments</b> for <b>'+keyFactorName+'</b>',
                    icon: '',
                    html: true
                });
                mgrComments.focus();
                return false;
            }
            if(appraisalManagerRating.value.trim()==""){
                // x0p( '','Please Enter rating for '+keyFactorName,'info');
             
                x0p({
                    title:'',
                    text:'Please enter <b style="color:red">manager rating</b> for <b>'+keyFactorName+'</b>',
                    icon: 'info',
                    html: true
                });
                empRating.focus();
                return false;
            }
             }
        }
        var shortTermGoalComments=document.getElementById("shortTermGoalComments").value;
        var longTermGoalComments=document.getElementById("longTermGoalComments").value;
        var strengthsComments=document.getElementById("strengthsComments").value;
        var improvementsComments=document.getElementById("improvementsComments").value;
    
        if(strengthsComments.trim()==""){
            x0p({
                title:'',
                text:'Please update <b style="color:red">personality comments</b> (Click on personality button to enter stregnths)',
                icon: 'info',
                html: true
            });
            return false
        }
        if(improvementsComments.trim()==""){
            x0p({
                title:'',
                text:'Please update <b style="color:red">personality comments</b> (Click on personality button to enter improvements)',
                icon: 'info',
                html: true
            });
            return false
        }
    
        if(shortTermGoalComments.trim()==""){
            x0p({
                title:'',
                text:'Please update <b style="color:red">goals comments</b> (Click on Goals button to enter short term goals)',
                icon: 'info',
                html: true
            });
            return false
        }
        if(longTermGoalComments.trim()==""){
            x0p({
                title:'',
                text:'Please enter <b style="color:red">goals comments</b> (Click on Goals button to enter long term goals)',
                icon: 'info',
                html: true
            });
            return false
        }
        x0p(' Do you want to Approve quarterly review?', '', 'warning',
            function(button) {
       
                if(button == 'warning') {
                    result = true;
                // alert(button)
                }
                if(button == 'cancel') { 
                    result = false;
                }
                if(result){
                     document.getElementById("status").value=status;
                      buttonObj.disabled=true;
                    document.getElementById("frmAppraisal").submit();
                   
                }

            }
            );
    
 
    }
    else if(status=="Rejected"){
        
        var rejectedComments=document.getElementById("rejectedComments").value;
    
        if(rejectedComments.trim()==""){
            x0p({
                title:'',
                text:'Please enter <b style="color:red">rejected comments.</b>',
                icon: 'info',
                html: true
            });
            return false
        }else{
             document.getElementById("status").value=status;
             document.getElementById("rejectedCommentsNew").value=rejectedComments;
              buttonObj.disabled=true;
            document.getElementById("frmAppraisal").submit(); 
        }
    }
    else if(status=="Closed"){
    	
    	for(var i =1;i<=rowCount;i++){
            var mgrComments=document.getElementById("hrComments"+i);
          // alert(mgrComments.value+i);
            var appraisalManagerRating=document.getElementById("appraisalHrRating"+i);
            var keyFactorName=document.getElementById("keyFactorName"+i).value;
            var keyFactorWeightage=document.getElementById("HrWeightage"+i).value;
            
             if(keyFactorName!='Appreciations & Awards'){
            	 
            	 if(keyFactorWeightage.trim()!='0.00'){
            if(mgrComments.value.trim()==""){
                x0p({
                    title:'',
                    text:'Please enter <b style="color:red">hr comments</b> for <b>'+keyFactorName+'</b>',
                    icon: '',
                    html: true
                });
                mgrComments.focus();
                return false;
            }
            if(appraisalManagerRating.value.trim()==""){
                // x0p( '','Please Enter rating for '+keyFactorName,'info');
             
                x0p({
                    title:'',
                    text:'Please enter <b style="color:red">hr rating</b> for <b>'+keyFactorName+'</b>',
                    icon: 'info',
                    html: true
                });
                empRating.focus();
                return false;
            }
            	 }
             }
        }
        var shortTermGoalComments=document.getElementById("shortTermGoalHrComments").value;
        var longTermGoalComments=document.getElementById("longTermGoalHrComments").value;
        var strengthsComments=document.getElementById("strengthsHrComments").value;
        var improvementsComments=document.getElementById("improvementsHrComments").value;
    
        if(strengthsComments.trim()==""){
            x0p({
                title:'',
                text:'Please update <b style="color:red">personality comments</b> (Click on personality button to enter stregnths)',
                icon: 'info',
                html: true
            });
            return false
        }
        if(improvementsComments.trim()==""){
            x0p({
                title:'',
                text:'Please update <b style="color:red">personality comments</b> (Click on personality button to enter improvements)',
                icon: 'info',
                html: true
            });
            return false
        }
    
        if(shortTermGoalComments.trim()==""){
            x0p({
                title:'',
                text:'Please update <b style="color:red">goals comments</b> (Click on Goals button to enter short term goals)',
                icon: 'info',
                html: true
            });
            return false
        }
        if(longTermGoalComments.trim()==""){
            x0p({
                title:'',
                text:'Please enter <b style="color:red">goals comments</b> (Click on Goals button to enter long term goals)',
                icon: 'info',
                html: true
            });
            return false
        }
         
        
         x0p(' Are you sure to close the the quarterly review ?', '', 'warning',
            function(button) {
       
                if(button == 'warning') {
                    result = true;
                // alert(button)
                }
                if(button == 'cancel') { 
                    result = false;
                }
                if(result){
                     document.getElementById("status").value=status;
                      buttonObj.disabled=true;
                    document.getElementById("frmAppraisal").submit();
                   
                }

            }
            );
    

    }
    else{
         document.getElementById("status").value=status;
          buttonObj.disabled=true;
        document.getElementById("frmAppraisal").submit();  
    }
   
   
}

function addGoalsOverlay(){
 
     
  
    showRow('shortTeamGoalCommentsTr');
    showRow('longTeamGoalCommentsTr');
     var status=document.getElementById("status").value;
     var curretRole=document.getElementById('curretRole').value;
      var dayCount=document.getElementById('dayCount').value;
    var operationTeamStatus=document.getElementById('operationTeamStatus').value;
   
    
    var overlay = document.getElementById('goalsOverlay');
    var specialBox = document.getElementById('goalsSpecialBox');
    overlay.style.opacity = .8;
    if(overlay.style.display == "block"){
        overlay.style.display = "none";
        specialBox.style.display = "none";
        document.getElementById("resultMessageGoal").innerHTML='';
    }
    else {
        overlay.style.display = "block";
        specialBox.style.display = "block";
        if(document.getElementById('roleName').value=='Operations'){
        	 document.getElementById('shortTermGoal').readOnly=true;
             document.getElementById('longTermGoal').readOnly=true;
         document.getElementById('shortTermGoalComments').readOnly=true;
             document.getElementById('longTermGoalComments').readOnly=true;
             document.getElementById('shortTermGoalHrComments').readOnly=true;
         	document.getElementById('longTermGoalHrComments').readOnly=true;
         	hideRow('goalsBtnTr');
             if(status.trim()=='Submited' || status.trim()=="Entered"){
             	document.getElementById('shortTermGoalHrComments').readOnly=true;
             	document.getElementById('longTermGoalHrComments').readOnly=true;
             	hideRow('personalitybtnTr');
             }
             if(dayCount==1 && status=='Rejected'){
             	document.getElementById('shortTermGoalHrComments').readOnly=true;
             	document.getElementById('longTermGoalHrComments').readOnly=true;
             	hideRow('goalsBtnTr');
             }
             
             if(dayCount==1 && status=='Approved'){
             	document.getElementById('shortTermGoalHrComments').readOnly=false;
             	document.getElementById('longTermGoalHrComments').readOnly=false;
             	showRow('goalsBtnTr');
             }

        	
        }else{
        if(curretRole=='my'){
            hideRow('shortTeamGoalCommentsTr');
            hideRow('longTeamGoalCommentsTr');
            if(status.trim()=="Submitted" || status.trim()=="Approved"){
                document.getElementById('shortTermGoal').readOnly=true;
                document.getElementById('longTermGoal').readOnly=true;
                hideRow('goalsBtnTr');
            }else{
                document.getElementById('shortTermGoal').readOnly=false;
                document.getElementById('longTermGoal').readOnly=false;
                showRow('goalsBtnTr');
            }
        }else if(curretRole=='team'){
              if(status.trim()=="Approved" || status.trim()=="Entered" ){
                document.getElementById('shortTermGoalComments').readOnly=true;
                document.getElementById('longTermGoalComments').readOnly=true;
                hideRow('goalsBtnTr');
            }else{
                document.getElementById('shortTermGoalComments').readOnly=false;
                document.getElementById('longTermGoalComments').readOnly=false;
                showRow('goalsBtnTr');
            }
            if(dayCount==2 && (status=='Submited' || (status=='Approved' && operationTeamStatus=='Rejected'))){
                document.getElementById('shortTermGoalComments').readOnly=false;
                document.getElementById('longTermGoalComments').readOnly=false;
                showRow('goalsBtnTr');
            }
             if(dayCount==1 && status=='Rejected'){
               document.getElementById('shortTermGoalComments').readOnly=true;
                document.getElementById('longTermGoalComments').readOnly=true;
                hideRow('goalsBtnTr');
            }
        }
         var userId1=document.getElementById('userId1').value;
         var backToFlag=document.getElementById('backToFlag').value;
         if(userId1=='rkalaga' && backToFlag==1){
         
                 document.getElementById('shortTermGoalComments').readOnly=true;
                document.getElementById('longTermGoalComments').readOnly=true;
                hideRow('goalsBtnTr');
            }
        }
    }
    document.getElementById('shortTermGoalCount').innerHTML ='';
    document.getElementById('shortTermGoalCommentsCount').innerHTML ='';
    document.getElementById('longTermGoalCount').innerHTML ='';
    document.getElementById('longTermGoalCommentsCount').innerHTML ='';   
// window.location = "jobSearch.action";
}
  
function saveGoals(){
    setSessionTimeout();
    var shortTermGoal=document.getElementById("shortTermGoal").value;
    var shortTermGoalComments=document.getElementById("shortTermGoalComments").value;
    var longTermGoal=document.getElementById("longTermGoal").value;
    var longTermGoalComments=document.getElementById("longTermGoalComments").value;
    var curretRole=document.getElementById("curretRole").value;
    document.getElementById("savebtnGoal").disabled=true;
    document.getElementById("loadGoal").style.display='block';
    document.getElementById("resultMessageGoal").innerHTML='';
    var roleName=document.getElementById("roleName").value;

    if(curretRole=='my'){
        if(shortTermGoal.trim()==""){
            x0p({
                title:'',
                text:'Please enter <b style="color:red">short term goal</b>',
                icon: 'info',
                html: true
            });
            document.getElementById("savebtnGoal").disabled=false;
    document.getElementById("loadGoal").style.display='none';
            return false;
        }
        if(longTermGoal.trim()==""){
            x0p({
                title:'',
                text:'Please enter <b style="color:red">long term goal</b>',
                icon: 'info',
                html: true
            });
             document.getElementById("savebtnGoal").disabled=false;
    document.getElementById("loadGoal").style.display='none';
            return false;
        }
        
    }
    if(curretRole=='team'){
    	
    	if(roleName.trim()=='Employee'){
            if(shortTermGoalComments.trim()==""){
                x0p({
                    title:'',
                    text:'Please enter <b style="color:red">short term goal comments</b>',
                    icon: 'info',
                    html: true
                });
                 document.getElementById("savebtnGoal").disabled=false;
        document.getElementById("loadGoal").style.display='none';
                return false;
            }
            if(longTermGoalComments.trim()==""){
                x0p({
                    title:'',
                    text:'Please enter <b style="color:red">long term goal comments</b>',
                    icon: 'info',
                    html: true
                });
                 document.getElementById("savebtnGoal").disabled=false;
        document.getElementById("loadGoal").style.display='none';
                return false;
            }
        	}else{
        		 
        		var shortTermGoalHrComments= document.getElementById('shortTermGoalHrComments').value;
        		var longTermGoalHrComments= document.getElementById('longTermGoalHrComments').value;
        		if(shortTermGoalHrComments.trim()==""){
                    x0p({
                        title:'',
                        text:'Please enter <b style="color:red">short term goal comments</b>',
                        icon: 'info',
                        html: true
                    });
                     document.getElementById("savebtnGoal").disabled=false;
            document.getElementById("loadGoal").style.display='none';
                    return false;
                }
                if(longTermGoalHrComments.trim()==""){
                    x0p({
                        title:'',
                        text:'Please enter <b style="color:red">long term goal comments</b>',
                        icon: 'info',
                        html: true
                    });
                     document.getElementById("savebtnGoal").disabled=false;
            document.getElementById("loadGoal").style.display='none';
                    return false;
                }
        	        
        	 }
        

    }
    
     var lineId=document.getElementById("lineId").value;
        var empId=document.getElementById("empId").value;
        var strength=document.getElementById("strength").value;
        var improvements=document.getElementById("improvements").value;
        var quarterly=document.getElementById("quarterly").value;
        var qyear=document.getElementById("qyear").value;
        if(lineId=='0'){
            
         var rowCount = $("#measurementTable td").closest("tr").length;
         document.getElementById("rowCount").value=rowCount;
         var measurementId="";
                var keyFactorId="";
                var keyFactorWeightage="";
                var empDescription="";
                var HrWeightage="";
                var empRating="";
                var id="";
                
          if(rowCount>0){
               for(var i=1;i<=rowCount;i++){
                    measurementId=measurementId+ document.getElementById("measurementId"+i).value+"#^$";
                        keyFactorId=keyFactorId+ document.getElementById("keyFactorId"+i).value+"#^$";
                        keyFactorWeightage=keyFactorWeightage+ document.getElementById("keyFactorWeightage"+i).value+"#^$";
                        empDescription=empDescription+ document.getElementById("empDescription"+i).value+"#^$";
                        HrWeightage=HrWeightage+ document.getElementById("HrWeightage"+i).value+"#^$";
                        if(document.getElementById("empRating"+i).value==""){
                             empRating=empRating+"0"+"#^$";
                        }else{
                        empRating=empRating+ document.getElementById("empRating"+i).value+"#^$";
                        }
                         if( document.getElementById("id"+i).value==""){
                             id=id+"0"+"#^$";
                        }else{
                        id=id+ document.getElementById("id"+i).value+"#^$";
                        }
                       
                    
               }
          }
    
        document.getElementById("status").value="Entered";
        var myObj = {};  
        myObj["ids"] = id;
       
        myObj["appraisalId"] = 0;
        myObj["empId"] = empId;
        myObj["measurementId"] = measurementId;
        myObj["keyFactorId"] = keyFactorId;
        myObj["keyFactorWeightage"] = keyFactorWeightage;
        myObj["empDescription"] = empDescription;
        myObj["empRating"] = empRating;
        myObj["status"] = "Entered";
        myObj["shortTermGoal"] = shortTermGoal;
        myObj["longTermGoal"] = longTermGoal;
        myObj["strength"] = strength;
        myObj["improvements"] = improvements;
        myObj["quarterly"] = quarterly;
        myObj["qyear"] = qyear;
        myObj["rowCount"] = rowCount;
        myObj["HrWeightage"] = HrWeightage;
        var json = JSON.stringify(myObj);
                    // document.getElementById("frmAppraisal").submit();
       
        var url = CONTENXT_PATH+"/saveQReviewDetails.action?rand="+new Date();;
        $.ajax({
 	       
 	     	url:url,
 	         data:{jsonData: json},
 	         contentType: 'application/json',
 	         type: 'GET',
 	         context: document.body,
 	         success: function(responseText) {
                 // alert(req.responseText);
                 var  resultJson = JSON.parse(responseText);
                var QuarterAppraisalDetails = resultJson["QuarterAppraisalDetails"];
              // alert(QuarterAppraisalDetails);
                var LineId = resultJson["LineId"];
                var AppraisalId = resultJson["AppraisalId"];
                var individualRowDetails=QuarterAppraisalDetails.split("*@!");
              // alert(individualRowDetails.length);
                for (i = 1; i < individualRowDetails.length; i++) {
                                                var individualColumn = individualRowDetails[i - 1].split("#^$");
                                                
                                                if(individualColumn[11] != '-1'){
                                                    
                                                    /*
													 * console.log(i+"--"+
													 * individualColumn[11]);
													 * console.log(individualColumn);
													 */
                                                       
                                                    document.getElementById("id"+i).value=individualColumn[11];  
                                            	}                       
                }
                
                document.getElementById("lineId").value=LineId;
                document.getElementById("appraisalId").value=AppraisalId;
                document.getElementById("status").value="Entered";
                  var result= resultJson["result"];
                   if(result=='success'){
                       document.getElementById("resultMessageGoal").innerHTML="<font color=\"green\" size=\"2\">Details saved successfully!</font>";
                   }else{
                        document.getElementById("resultMessageGoal").innerHTML='<font color=\"red" size=\"2\">Sorry! Please try again latter!</font>';
                   }
                
               // alert("id=="+document.getElementById("id10").value);
                   // parseEmpMessagesForProject(req0.responseXML);
                    document.getElementById("savebtnGoal").disabled=false;
document.getElementById("loadGoal").style.display='none';
 	               
 	         },
 	         error: function(e){
 	             document.getElementById('loadGoal').style.display='none';
 	             document.getElementById('resultMessageGoal').innerHTML="Please try again later";
 	             
 	         }
 	     });            // document.getElementById("frmAppraisal").submit();
        
        }else{
        	
        	if(roleName.trim()=='Employee')	{
            	var myObj = {};  
            	
            	 
                myObj["id"] = lineId;
               
                myObj["shortTermGoal"] = shortTermGoal;
                myObj["longTermGoal"] = longTermGoal;
                myObj["shortTermGoalComments"] = shortTermGoalComments;
                myObj["longTermGoalComments"] = longTermGoalComments;
               
                var json = JSON.stringify(myObj);
                      
                var url = CONTENXT_PATH+"/setQReviewGoalsComments.action?rand="+new Date();;
                $.ajax({
         	       
         	     	url:url,
         	         data:{jsonData: json},
         	         contentType: 'application/json',
         	         type: 'GET',
         	         context: document.body,
         	         success: function(responseText) {
                         // alert(req.responseText);
                         if(responseText=='success'){
                             document.getElementById("resultMessageGoal").innerHTML="<font color=\"green\" size=\"2\">Details saved successfully!</font>";
                         }else{
                             document.getElementById("resultMessageGoal").innerHTML=innerHTML="<font color=\"red\" size=\"2\">Sorry! Please try again latter!</font>";
                         }
                       document.getElementById("savebtnGoal").disabled=false;
    document.getElementById("loadGoal").style.display='none';
         	               
         	         },
         	         error: function(e){
         	             document.getElementById('loadGoal').style.display='none';
         	             document.getElementById('resultMessageGoal').innerHTML="Please try again later";
         	             
         	         }
         	     });
                
               }else{
              	 // for operation role
              	 
              	 var shortTermGoalHrComments= document.getElementById('shortTermGoalHrComments').value;
           		var longTermGoalHrComments= document.getElementById('longTermGoalHrComments').value;
              	 
           		var myObj = {};  
            	
           	    
               myObj["id"] = lineId;
              
            
               myObj["shortTermGoalComments"] = shortTermGoalHrComments;
               myObj["longTermGoalComments"] = longTermGoalHrComments;
              
               var json = JSON.stringify(myObj);
                     
               var url = CONTENXT_PATH+"/setQReviewGoalsHrComments.action?rand="+new Date();;
               $.ajax({
        	       
        	     	url:url,
        	         data:{jsonData: json},
        	         contentType: 'application/json',
        	         type: 'GET',
        	         context: document.body,
        	         success: function(responseText) {
                        // alert(req.responseText);
                        if(responseText=='success'){
                            document.getElementById("resultMessageGoal").innerHTML="<font color=\"green\" size=\"2\">Details saved successfully!</font>";
                        }else{
                            document.getElementById("resultMessageGoal").innerHTML=innerHTML="<font color=\"red\" size=\"2\">Sorry! Please try again latter!</font>";
                        }
                      document.getElementById("savebtnGoal").disabled=false;
    document.getElementById("loadGoal").style.display='none';
        	               
        	         },
        	         error: function(e){
        	             document.getElementById('loadGoal').style.display='none';
        	             document.getElementById('resultMessageGoal').innerHTML="Please try again later";
        	             
        	         }
        	     });
                        
               }


        }
   
   // addGoalsOverlay();
}  
function addPersonalOverlay(){
 
    
    showRow('strengthsCommentsTr');
    showRow('improvementsCommentsTr');
   
   
    var status=document.getElementById("status").value;
    var curretRole=document.getElementById('curretRole').value;
     var dayCount=document.getElementById('dayCount').value;
    var operationTeamStatus=document.getElementById('operationTeamStatus').value;
    
    
   
     
    var overlay = document.getElementById('personalOverlay');
    var specialBox = document.getElementById('personalSpecialBox');
    overlay.style.opacity = .8;
    if(overlay.style.display == "block"){
        overlay.style.display = "none";
        specialBox.style.display = "none";
         document.getElementById("resultMessagePersonality").innerHTML='';
    }
    else {
        overlay.style.display = "block";
        specialBox.style.display = "block";
         if(document.getElementById('roleName').value=='Operations'){
        	 document.getElementById('strength').readOnly=true;
             document.getElementById('improvements').readOnly=true;
           document.getElementById('strengthsComments').readOnly=true;
             document.getElementById('improvementsComments').readOnly=true;
                           
             document.getElementById('strengthsHrComments').readOnly=true;
         	document.getElementById('improvementsHrComments').readOnly=true;
         	hideRow('personalitybtnTr');
             if(status.trim()=='Submited' || status.trim()=="Entered"){
             	document.getElementById('strengthsHrComments').readOnly=true;
             	document.getElementById('improvementsHrComments').readOnly=true;
             	hideRow('personalitybtnTr');
             }
             if(dayCount==1 && status=='Rejected'){
             	document.getElementById('strengthsHrComments').readOnly=true;
             	document.getElementById('improvementsHrComments').readOnly=true;
             	hideRow('personalitybtnTr');
             }
             
             if(dayCount==1 && status=='Approved'){
             	document.getElementById('strengthsHrComments').readOnly=false;
             	document.getElementById('improvementsHrComments').readOnly=false;
             	showRow('personalitybtnTr');
             }
     		
 
        	 
         }else{
        if(curretRole=='my'){
            hideRow('strengthsCommentsTr');
            hideRow('improvementsCommentsTr');
            if(status.trim()=="Submitted" || status.trim()=="Approved"){
                document.getElementById('strength').readOnly=true;
                document.getElementById('improvements').readOnly=true;
                hideRow('personalitybtnTr');
            }else{
                document.getElementById('strength').readOnly=false;
                document.getElementById('improvements').readOnly=false;
                showRow('personalitybtnTr');
            }
        }else if(curretRole=='team'){
              if(status.trim()=="Approved" || status.trim()=="Entered" ){
                document.getElementById('strengthsComments').readOnly=true;
                document.getElementById('improvementsComments').readOnly=true;
                hideRow('personalitybtnTr');
            }else{
                document.getElementById('strengthsComments').readOnly=false;
                document.getElementById('improvementsComments').readOnly=false;
                showRow('personalitybtnTr');
            }
             if(dayCount==2 && (status=='Submited' || (status=='Approved' && operationTeamStatus=='Rejected'))){
                 document.getElementById('strengthsComments').readOnly=false;
                document.getElementById('improvementsComments').readOnly=false;
                showRow('personalitybtnTr');
             }
             if(dayCount==1 && status=='Rejected'){
                document.getElementById('strengthsComments').readOnly=true;
                document.getElementById('improvementsComments').readOnly=true;
                hideRow('personalitybtnTr');
            }
        }
        var userId1=document.getElementById('userId1').value;
        var backToFlag=document.getElementById('backToFlag').value;
        if(userId1=='rkalaga' && backToFlag==1){
           
                 document.getElementById('strengthsComments').readOnly=true;
                document.getElementById('improvementsComments').readOnly=true;
                hideRow('personalitybtnTr');
            }
         }
    }
    document.getElementById('personalityCount').innerHTML ='';
    document.getElementById('personalityCommnetsCount').innerHTML ='';
    document.getElementById('improvementsCount').innerHTML ='';
    document.getElementById('improvementsCommentsCount').innerHTML ='';
// window.location = "jobSearch.action";

}

function savePersonality(){
    setSessionTimeout();
    var strength=document.getElementById("strength").value;
    var strengthsComments=document.getElementById("strengthsComments").value;
    var improvements=document.getElementById("improvements").value;
    var improvementsComments=document.getElementById("improvementsComments").value;
    var curretRole=document.getElementById("curretRole").value;
     document.getElementById("savebtnPersonality").disabled=true;
    document.getElementById("loadPersonality").style.display='block';
     document.getElementById("resultMessagePersonality").innerHTML='';
     var roleName=document.getElementById("roleName").value;

    if(curretRole=='my'){
        if(strength.trim()==""){
            x0p({
                title:'',
                text:'Please enter <b style="color:red">your strengths</b>',
                icon: 'info',
                html: true
            });
            document.getElementById("savebtnPersonality").disabled=false;
    document.getElementById("loadPersonality").style.display='none';
            return false;
        }
        if(improvements.trim()==""){
            x0p({
                title:'',
                text:'Please enter <b style="color:red">your improvements</b>',
                icon: 'info',
                html: true
            });
             document.getElementById("savebtnPersonality").disabled=false;
    document.getElementById("loadPersonality").style.display='none';
            return false;
        }
    }
    
    if(curretRole=='team'){
    	 if(roleName.trim()=='Employee'){
    	        if(strengthsComments.trim()==""){
    	            x0p({
    	                title:'',
    	                text:'Please enter <b style="color:red">strengths comments</b>',
    	                icon: 'info',
    	                html: true
    	            });
    	             document.getElementById("savebtnPersonality").disabled=false;
    	    document.getElementById("loadPersonality").style.display='none';
    	            return false;
    	        }
    	        if(improvementsComments.trim()==""){
    	            x0p({
    	                title:'',
    	                text:'Please enter <b style="color:red">improvement comments</b>',
    	                icon: 'info',
    	                html: true
    	            });
    	             document.getElementById("savebtnPersonality").disabled=false;
    	    document.getElementById("loadPersonality").style.display='none';
    	            return false;
    	        }
    	        
    	    	 }else{
    	    		 
    	    		var strengthsHrComments= document.getElementById('strengthsHrComments').value;
    	    		var improvementsHrComments= document.getElementById('improvementsHrComments').value;
    	        	
    	    		 if(strengthsHrComments.trim()==""){
    	    	            x0p({
    	    	                title:'',
    	    	                text:'Please enter <b style="color:red">strengths comments</b>',
    	    	                icon: 'info',
    	    	                html: true
    	    	            });
    	    	             document.getElementById("savebtnPersonality").disabled=false;
    	    	    document.getElementById("loadPersonality").style.display='none';
    	    	            return false;
    	    	        }
    	    	        if(improvementsHrComments.trim()==""){
    	    	            x0p({
    	    	                title:'',
    	    	                text:'Please enter <b style="color:red">improvement comments</b>',
    	    	                icon: 'info',
    	    	                html: true
    	    	            });
    	    	             document.getElementById("savebtnPersonality").disabled=false;
    	    	    document.getElementById("loadPersonality").style.display='none';
    	    	            return false;
    	    	        }
    	    	        
    	    	 }
    	    

    	
    }
    var lineId=document.getElementById("lineId").value;
        var empId=document.getElementById("empId").value;
        var shortTermGoal=document.getElementById("shortTermGoal").value;
        var longTermGoal=document.getElementById("longTermGoal").value;
        var quarterly=document.getElementById("quarterly").value;
        var qyear=document.getElementById("qyear").value;
        if(lineId=='0'){
            
         var rowCount = $("#measurementTable td").closest("tr").length;
         document.getElementById("rowCount").value=rowCount;
         var measurementId="";
                var keyFactorId="";
                var keyFactorWeightage="";
                var empDescription="";
                var HrWeightage="";
                var empRating="";
                var id="";
          if(rowCount>0){
               for(var i=1;i<=rowCount;i++){
                    measurementId=measurementId+ document.getElementById("measurementId"+i).value+"#^$";
                        keyFactorId=keyFactorId+ document.getElementById("keyFactorId"+i).value+"#^$";
                        keyFactorWeightage=keyFactorWeightage+ document.getElementById("keyFactorWeightage"+i).value+"#^$";
                        empDescription=empDescription+ document.getElementById("empDescription"+i).value+"#^$";
                        HrWeightage=HrWeightage+ document.getElementById("HrWeightage"+i).value+"#^$";
                        if(document.getElementById("empRating"+i).value==""){
                             empRating=empRating+"0"+"#^$";
                        }else{
                        empRating=empRating+ document.getElementById("empRating"+i).value+"#^$";
                        }
                         if( document.getElementById("id"+i).value==""){
                             id=id+"0"+"#^$";
                        }else{
                        id=id+ document.getElementById("id"+i).value+"#^$";
                        }
                       
                    
               }
          }
    
        document.getElementById("status").value="Entered";
        var myObj = {};  
        myObj["ids"] = id;
       
        myObj["appraisalId"] = 0;
        myObj["empId"] = empId;
        myObj["measurementId"] = measurementId;
        myObj["keyFactorId"] = keyFactorId;
        myObj["keyFactorWeightage"] = keyFactorWeightage;
        myObj["empDescription"] = empDescription;
        myObj["empRating"] = empRating;
        myObj["status"] = "Entered";
        myObj["shortTermGoal"] = shortTermGoal;
        myObj["longTermGoal"] = longTermGoal;
        myObj["strength"] = strength;
        myObj["improvements"] = improvements;
        myObj["quarterly"] = quarterly;
        myObj["qyear"] = qyear;
        myObj["rowCount"] = rowCount;
        myObj["HrWeightage"] = HrWeightage;
        var json = JSON.stringify(myObj);
                    // document.getElementById("frmAppraisal").submit();
      
        var url = CONTENXT_PATH+"/saveQReviewDetails.action?rand="+new Date();;
        $.ajax({
 	        
 	     	url:url,
 	         data:{jsonData: json},
 	         contentType: 'application/json',
 	         type: 'GET',
 	         context: document.body,
 	         success: function(responseText) {
                 // alert(req.responseText);
                 var  resultJson = JSON.parse(responseText);
                var QuarterAppraisalDetails = resultJson["QuarterAppraisalDetails"];
              // alert(QuarterAppraisalDetails);
                var LineId = resultJson["LineId"];
                var AppraisalId = resultJson["AppraisalId"];
                var individualRowDetails=QuarterAppraisalDetails.split("*@!");
              // alert(individualRowDetails.length);
                for (i = 1; i < individualRowDetails.length; i++) {
                                                var individualColumn = individualRowDetails[i - 1].split("#^$");
                                                if(individualColumn[11] != '-1'){
                                                    
                                                    /*
													 * console.log(i+"--"+
													 * individualColumn[11]);
													 * console.log(individualColumn);
													 */
                                                       
                                                    document.getElementById("id"+i).value=individualColumn[11];  
                                            	}                        
                }
                
                document.getElementById("lineId").value=LineId;
                document.getElementById("appraisalId").value=AppraisalId;
                document.getElementById("status").value="Entered";
                
               // alert("id=="+document.getElementById("id10").value);
                   // parseEmpMessagesForProject(req0.responseXML);
                   var result= resultJson["result"];
                   if(result=='success'){
                        document.getElementById("resultMessagePersonality").innerHTML="<font color=\"green\" size=\"2\">details saved successfully!</font>";
                     }else{
                        document.getElementById("resultMessagePersonality").innerHTML='<font color=\"red" size=\"2\">Sorry! Please try again latter!</font>';
                   }
                      document.getElementById("savebtnPersonality").disabled=false;
                      	document.getElementById("loadPersonality").style.display='none';
 	               
 	         },
 	         error: function(e){
 	             document.getElementById('loadPersonality').style.display='none';
 	             document.getElementById('resultMessagePersonality').innerHTML="Please try again later";
 	             
 	         }
 	     });             // document.getElementById("frmAppraisal").submit();
        
        }else{
        	
        	
            var lineId=document.getElementById("lineId").value;
            
            
            if(roleName.trim()=='Employee'){
              var url = CONTENXT_PATH+"/setQReviewPersonalityComments.action?&rand="+new Date();;

              var myObj = {};  
                   myObj["id"] = lineId;
                  
                   myObj["strength"] = strength;
                   myObj["improvements"] = improvements;
                   myObj["strengthsComments"] = strengthsComments;
                   myObj["improvementsComments"] = improvementsComments;
                   
                   var json = JSON.stringify(myObj);
                           
                   var url = CONTENXT_PATH+"/setQReviewPersonalityComments.action?rand="+new Date();;
                   $.ajax({
            	        // url:CONTENXT_PATH+"/doUpdateTerminationDetails.action?empId="+empId+"&dateOfTermination="+dateOfTermination+"&reasonsForTerminate="+escape(reasonsForTerminate),
            	         // context: document.body,
            	     	
            	     	url:url,
            	         data:{jsonData: json},
            	         contentType: 'application/json',
            	         type: 'GET',
            	         context: document.body,
            	         success: function(responseText) {
                            // alert(req.responseText);
                            if(responseText){
                                 document.getElementById("resultMessagePersonality").innerHTML="<font color=\"green\" size=\"2\">Details saved successfully!</font>";
                             }else{
                                 document.getElementById("resultMessagePersonality").innerHTML=innerHTML="<font color=\"red\" size=\"2\">Sorry! Please try again latter!</font>";
                             }
                           // parseEmpMessagesForProject(req.responseXML);
                              document.getElementById("savebtnPersonality").disabled=false;
        document.getElementById("loadPersonality").style.display='none';
            	               
            	         },
            	         error: function(e){
            	             document.getElementById('loadPersonality').style.display='none';
            	             document.getElementById('resultMessagePersonality').innerHTML="Please try again later";
            	             
            	         }
            	     });
            }else{
           	 // for operation role
           	 
           	 var strengthsHrComments= document.getElementById('strengthsHrComments').value;
        		var improvementsHrComments= document.getElementById('improvementsHrComments').value;
           	 
           	 var url = CONTENXT_PATH+"/setQReviewPersonalityHrComments.action?&rand="+new Date();;

                var myObj = {};  
                     myObj["id"] = lineId;
                    
                     
                     myObj["strengthsHrComments"] = strengthsHrComments;
                     myObj["improvementsHrComments"] = improvementsHrComments;
                     
                     var json = JSON.stringify(myObj);
                             
                     var url = CONTENXT_PATH+"/setQReviewPersonalityHrComments.action?rand="+new Date();;
                     $.ajax({
              	        // url:CONTENXT_PATH+"/doUpdateTerminationDetails.action?empId="+empId+"&dateOfTermination="+dateOfTermination+"&reasonsForTerminate="+escape(reasonsForTerminate),
              	         // context: document.body,
              	     	
              	     	url:url,
              	         data:{jsonData: json},
              	         contentType: 'application/json',
              	         type: 'GET',
              	         context: document.body,
              	         success: function(responseText) {
                              // alert(req.responseText);
                              if(responseText){
                                   document.getElementById("resultMessagePersonality").innerHTML="<font color=\"green\" size=\"2\">Details saved successfully!</font>";
                               }else{
                                   document.getElementById("resultMessagePersonality").innerHTML=innerHTML="<font color=\"red\" size=\"2\">Sorry! Please try again latter!</font>";
                               }
                             // parseEmpMessagesForProject(req.responseXML);
                                document.getElementById("savebtnPersonality").disabled=false;
          document.getElementById("loadPersonality").style.display='none';
              	               
              	         },
              	         error: function(e){
              	             document.getElementById('loadPersonality').style.display='none';
              	             document.getElementById('resultMessagePersonality').innerHTML="Please try again later";
              	             
              	         }
              	     });
                     
            }
              
           

        }
      
  // addPersonalOverlay();
}
function fieldLengthValidatorQuarter(element){
    var i=0;
 
    if(element.id=="opCode")
    { 
        i=200;
    }
            
    if(element.id=="description" 
        || element.id=="shortTermGoal" 
        ||element.id=="shortTermGoalComments"
        ||element.id=="longTermGoal"
        ||element.id=="longTermGoalComments"
        ||element.id=="strength"
        ||element.id=="strengthsComments"
        ||element.id=="improvements"
        ||element.id=="improvementsComments"
        ||element.id=="rejectedComments"
        ||element.id=="strengthsHrComments"
	    ||element.id=="improvementsHrComments"
	    ||element.id=="shortTermGoalHrComments"
	    ||element.id=="longTermGoalHrComments"

        )
        { 
        i=500;
    }
            
  
    var temp=0;
   
   
    
   
     
    if(temp==0 && element.value.replace(/^\s+|\s+$/g,"").length>i) {
        str=new String(element.value);
        element.value=str.substring(0,i);
        x0p( '','The '+element.id+' length must be less than '+i+' characters','info');
        // displayErrorMessage('The '+element.id+' length must be less than
		// '+i+' characters!','Error');
        element.focus();
        return false;
    }
    return true;
   
}
function countChar(val,displayId) {
    var len = val.value.length;
    if (len >= 500) {
        val.value = val.value.substring(0, 500);
        document.getElementById(displayId).innerHTML=(0)+'\\500';
    } else {
        document.getElementById(displayId).innerHTML=(500-len)+'\\500';
    }
}
function toggleRejectedCommentsOverlay() {
    var overlay = document.getElementById('rejectedCommentsOverlay');
    var specialBox = document.getElementById('rejectedCommentsSpecialBox');

    overlay.style.opacity = .8;
    if(overlay.style.display == "block"){
        overlay.style.display = "none";
        specialBox.style.display = "none";
         showRow('rejectBtnTr');
          document.getElementById('rejectedComments').readOnly=false;
    }
    else {
        overlay.style.display = "block";
        specialBox.style.display = "block";
        document.getElementById("rejectedComments").value="";
    }
    document.frmEmpSearch.submit();
            
// window.location="getEmployee.action?empId="+;
}
function showRejectedCommentsOverlay(id) {
    var overlay = document.getElementById('rejectedCommentsOverlay');
    var specialBox = document.getElementById('rejectedCommentsSpecialBox');

    overlay.style.opacity = .8;
    if(overlay.style.display == "block"){
        overlay.style.display = "none";
        specialBox.style.display = "none";
         showRow('rejectBtnTr');
          document.getElementById('rejectedComments').readOnly=false;
    }
    else {
        overlay.style.display = "block";
        specialBox.style.display = "block";
        hideRow('rejectBtnTr');
        
       var id= document.getElementById(id).value;
        document.getElementById('rejectedComments').readOnly=true;
         document.getElementById('rejectedComments').value=id;
    }
    document.frmEmpSearch.submit();
            
// window.location="getEmployee.action?empId="+;
}

function checkDoubleQuotes(obj){
   
   obj.value =  obj.value.replace(/"/g , "'");
       
    
    
}
var timeoutId;

function setSessionTimeout(){
clearTimeout(timeoutId);
// alert("test1"+new Date());
timeoutId = setTimeout(

function()

{
history.pushState(null, null, 'pagename');
window.addEventListener('popstate', function(event) {
history.pushState(null, null, 'pagename');
});
alert('You\'re session has timed out. Please re-login.');
window.location = "../../general/logout.action";
}, 1800000); // 1800000 -->30 min
}

/*
 * Author : Phani Kanuri Date : 05/10/2017
 * 
 */



function getQReviewDashBoardByStatus()
{
    $('span.pagination').empty().remove();
    var year=document.getElementById("year").value;
    var quarterly=document.getElementById("quarterly").value;
    if(year.trim().length==''){
        alert("Please Enter Year");
        return false;
    }
    if(quarterly==''){
        alert("Please Select Quarter");
        return false;
    }
    document.getElementById("QReviewDashBoardByStatusPie").style.display="none";
    document.getElementById("QReviewPracticeVsStatusStack").style.display="none";
    document.getElementById("QReviewClosedVsQuarterStatusStack").style.display="none";
    var tableId = document.getElementById("tblQReviewDashBoard");
    ClrTable(tableId);
    var req = newXMLHttpRequest();
    req.onreadystatechange = function() {
        if (req.readyState == 4) {
            if (req.status == 200) {      
                document.getElementById("qreviewReport").style.display = 'none';
                displayQReviewDashBoardByStatusPieChart(req.responseText); 
            // getPracticeVsStatusStackedChart();
            } 
        }else {
            document.getElementById("qreviewReport").style.display = 'block';
        }
    }; 
    var url = CONTENXT_PATH+"/getQReviewDashBoardByStatus.action?year="+year+"&quarter="+quarterly;
    
    req.open("GET",url,"true");    
    req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    req.send(null);
}

function displayQReviewDashBoardByStatusPieChart(response) {
    var dataArray = response;
    if(response!=''){
           
        qReviewDashBoardByStatusPieChart(response);
   
    }else{
        alert('No Result For This Search...');
        spnFast.innerHTML="No Result For This Search...";                
    }
     
// getIssuesDashBoardByPriority();
}


function qReviewDashBoardByStatusPieChart(result){
    document.getElementById("QReviewDashBoardByStatusPie").style.display="block";
      
    var arraydata = [['Status', 'Count']];
    
    var result1=result.split("*@!");
    
    for(var i=0; i<result1.length-1; i++){
        
        var res = result1[i].split("#^$");
        var dArray = [res[0],parseInt(res[1])];
        arraydata.push(dArray);
    }

    var data = google.visualization.arrayToDataTable(arraydata);
    var options = {
        title: 'Q-Reviews by Status' ,
        titleTextStyle: {
            color: ('blue', '#3E93D4'),    // any HTML string color ('red',
											// '#cc00cc')
            fontName: 'Times New Roman', // i.e. 'Times New Roman'
            fontSize: 15, // 12, 18 whatever you want (don't specify px)
            bold: true,    // true or false
            italic: false   // true of false
        },
        legend: 'right',
        chartArea:{
            
            width:"80%",
            height:"80%"
        },
        
        is3D: true,
        pieSliceText: 'value',
        sliceVisibilityThreshold: 0
         
    };
    // align:'center';
    var chart = new google.visualization.PieChart(document.getElementById("QReviewDashBoardByStatusPieChart"));
    function selectHandler() {
             
        var selectedItem = chart.getSelection()[0];
        // alert("selectedItem--"+data.getValue(selectedItem.row, 0));
        if (selectedItem) {
            var activityType = data.getValue(selectedItem.row, 0);
            // getBdmStatisticsDetailsByLoginId(activityType,bdmId);
            getQReviewListByStatus(activityType);
        }
    }
    google.visualization.events.addListener(chart, 'select', selectHandler);   
    chart.draw(data, options,dArray);
    
    
}



function  getQReviewListByStatus(activityType){
     $('span.pagination').empty().remove();
  
    var qYear=document.getElementById("year").value;
    var qQuarterly=document.getElementById("quarterly").value;

    var tableId = document.getElementById("tblQReviewDashBoard");
    ClrTable(tableId);
    // var startDate=document.getElementById('startDateSummaryGraph').value;
    // var endDate=document.getElementById('endDateSummaryGraph').value;
    document.getElementById('qreviewReport').style.display='block';
   
    var url = CONTENXT_PATH+"/getQReviewListByStatus.action?year="+qYear+"&quarter="+qQuarterly+"&activityType="+activityType;

    var req = newXMLHttpRequest();
    req.onreadystatechange = function() {
        if (req.readyState == 4) {
            if (req.status == 200) {      
                document.getElementById("qreviewReport").style.display = 'none';
                displayQReviewDetailsByStatus(req.responseText,tableId,activityType,activityType);                        
            } 
        }else {
            document.getElementById("qreviewReport").style.display = 'block';
        }
    };
    req.open("GET", url, true);
    req.send(null); 
}
 
function displayQReviewDetailsByStatus(response,tableId,activityType,columnLabel){
    // var tableId = document.getElementById("tblQReviewDashBoard");
    // var headerFields = new
	// Array("S.No","Employee","CreatedDate","STATUS","CurrentStatus","Practice","Title","ReportsTo","Status(Opp.Team)");
      if (activityType=='NotEntered' || columnLabel=='NotEntered'){
        var headerFields = new Array("S.No","Employee","Email","WorkPhone","ReportsTo","Department","Practice","SubPractice","Location");
    }
    else{
        var headerFields = new Array("S.No","Employee","EnteredDate","STATUS","CurrentStatus","Practice","Title","ReportsTo","Status(Opp.Team)");
    }
    var dataArray = response;
    qReviewDetailsByStatusParseAndGenerateHTML(tableId,dataArray, headerFields);
}
 
function qReviewDetailsByStatusParseAndGenerateHTML(oTable,responseString,headerFields) {
    var fieldDelimiter = "#^$";
    var recordDelimiter = "*@!";   
    var records = responseString.split(recordDelimiter); 
    generateQReviewDetailsByStatusRows(oTable,headerFields,records,fieldDelimiter);
}

function generateQReviewDetailsByStatusRows(oTable, headerFields,records,fieldDelimiter) {	

    var tbody = oTable.childNodes[0];    
    tbody = document.createElement("TBODY");
    oTable.appendChild(tbody);
    generateTableHeader(tbody,headerFields);
    var rowlength;
    rowlength = records.length;
    if(rowlength >0 && records!=""){
        for(var i=0;i<rowlength-1;i++) {
                
            generateQReviewDetailsByStatus(oTable,tbody,records[i],fieldDelimiter);
        }
        
    } else {
        generateNoRecords(tbody,oTable);
    }
    generateTaskDashBoardFooter(tbody,oTable);
    if(oTable.id=='tblQReviewDashBoard2'){
        pagerOption2();
    }else if(oTable.id=='tblQReviewDashBoard1'){
        pagerOption1();
    }else{
        pagerOption();
    }
}


function generateQReviewDetailsByStatus(oTable,tableBody,record,delimiter){
    var row;
    var cell;
    var fieldLength;
    // var fields = record.split(delimiter);
    var fields = record.split("#^$");
    fieldLength = fields.length ;
    var length = fieldLength;
    
    row = document.createElement( "TR" );
    row.className="gridRowEven";
    tableBody.appendChild( row );
    
    for (var i=0;i<length-1;i++) {  
   
        cell = document.createElement( "TD" );
        cell.className="gridColumn";       
       
        cell.setAttribute("align","center");
            
        cell.innerHTML = fields[i]; 
        
            
        if(fields[i]!=''){
            if(i==1)
            {
                cell.setAttribute("align","left");
            }
            else
            {
                cell.setAttribute("align","center");     
            }
            row.appendChild( cell );
        }
    }   
    row.appendChild( cell );
    
}


/*
 * Practice Vs Status Stacked Chart
 * 
 */



function getPracticeVsStatusStackedChart()
{
    $('span.pagination').empty().remove();
    var year=document.getElementById("year1").value;
    var quarterly=document.getElementById("quarterly1").value;
    if(year.trim().length==''){
        alert("Please Enter Year");
        return false;
    }
    if(quarterly==''){
        alert("Please Select Quarter");
        return false;
    }
    document.getElementById("QReviewPracticeVsStatusStack").style.display="none";
    // document.getElementById("resultMessage").style.display="none";
    var tableId = document.getElementById("tblQReviewDashBoard1");
    ClrTable(tableId);
    var req = newXMLHttpRequest();
    req.onreadystatechange = function() {
        if (req.readyState == 4) {
            if (req.status == 200) {      
                document.getElementById("qreviewReport1").style.display = 'none';
                
                displayPracticeVsStatusStackedChart(req.responseText);
            // getClosedVsQuarterStackedChart();
               
              
               
            } 
        }else {
            document.getElementById("qreviewReport1").style.display = 'block';
        }
    }; 
    var url = CONTENXT_PATH+"/getPracticeVsStatusStackChart.action?year="+year+"&quarter="+quarterly;
    
    req.open("GET",url,"true");    
    req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    req.send(null);
}

function displayPracticeVsStatusStackedChart(response) { 
    // alert("response--"+response);
   
    document.getElementById("QReviewPracticeVsStatusStack").style.display="block";

        
    var result1 = response.split("*@!");
    // PracticeUtilizationStackedChat(result1);
    PracticeVsStatusReportStackedChat(result1);
}
 
// }
function PracticeVsStatusReportStackedChat(result){
    google.load('visualization', '1.1', {
        packages: ['corechart']
    });
     var arraydata = [['Practice', 'NotEntered','Enetered','Submitted','ManagerApproved','ManagerRejected','Closed','HrRejected',{
        role: 'annotation'
    }]];
    for(var i=0; i<result.length-1; i++){
        
        var res = result[i].split("#^$");
        var dArray = [res[0],parseInt(res[1]),parseInt(res[2]),parseInt(res[3]),parseInt(res[4]),parseInt(res[5]),parseInt(res[6]),parseInt(res[7]),''];
        arraydata.push(dArray);
    }

    
    var data = google.visualization.arrayToDataTable(arraydata);
    var view=new google.visualization.DataView(data);
   
    var options = {
        title:'Practices Vs Status',
        titleTextStyle: {
            color: ('blue', '#3E93D4'),    // any HTML string color ('red',
											// '#cc00cc')
            fontName: 'Times New Roman', // i.e. 'Times New Roman'
            fontSize: 15, // 12, 18 whatever you want (don't specify px)
            bold: true,    // true or false
            italic: false   // true of false
           
     
        },
        
        vAxes:[{
            title:'Status',
            minValue: 0,
            ticks: [0,20, 40, 60, 80, 100,120,140,160,180,200],
            titleTextStyle:
            {
                color: ('blue', '#3E93D4'),    // any HTML string color ('red',
												// '#cc00cc')
                fontName: 'Times New Roman', // i.e. 'Times New Roman'
                fontSize: 15, // 12, 18 whatever you want (don't specify px)
                bold: true,    // true or false
                italic: false   // true of false
            }
        }],
        hAxis:{
            title:'Practice',
            titleTextStyle:
            {
                color: ('blue', '#3E93D4'),    // any HTML string color ('red',
												// '#cc00cc')
                fontName: 'Times New Roman', // i.e. 'Times New Roman'
                fontSize: 15, // 12, 18 whatever you want (don't specify px)
                bold: true,    // true or false
                italic: false   // true of false
            }
        },
        width: 700,
        height: 350,
        bar: {
            groupWidth: '50%'
        },
        isStacked: true,
        bars :'vertical',
        legend: {
            position: 'bottom', 
            maxLines: 8
        }
    };


    var chart = new google.visualization.ColumnChart(document.getElementById("PracticeVsStatusStackChart"));
    function selectHandler() {
             
        var selectedItem = chart.getSelection()[0];
      


     
        
       
        if (selectedItem) {
            var activityType = data.getValue(selectedItem.row, 0);
            var columnLabel = data.getColumnLabel(selectedItem.column);
            // getStudentInfoStackByCollege(activityType,columnLabel,3);
            getQReviewListByStatusVsPractice(activityType,columnLabel);

        }
    }
    google.visualization.events.addListener(chart, 'select', selectHandler);   
    chart.draw(data, options,dArray);

}



/*
 * Closed Vs Status Stacked Chart
 * 
 */



function getClosedVsQuarterStackedChart()
{
    $('span.pagination').empty().remove();
    var year=document.getElementById("year2").value;
   
    if(year.trim().length==''){
        alert("Please Enter Year");
        return false;
    }
   
    document.getElementById("QReviewClosedVsQuarterStatusStack").style.display="none";
    var tableId = document.getElementById("tblQReviewDashBoard2");
    ClrTable(tableId);
    // document.getElementById("resultMessage").style.display="none";
    var req = newXMLHttpRequest();
    req.onreadystatechange = function() {
        if (req.readyState == 4) {
            if (req.status == 200) {      
                document.getElementById("qreviewReport2").style.display = 'none';
                
                displayClosedVsQuarterStackedChart(req.responseText);
               
              
               
            } 
        }else {
            document.getElementById("qreviewReport2").style.display = 'block';
        }
    }; 
    var url = CONTENXT_PATH+"/getClosedVsQuarterStackChart.action?year="+year;
    
    req.open("GET",url,"true");    
    req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    req.send(null);
}

function displayClosedVsQuarterStackedChart(response) { 
    // alert("response--"+response);
   
    document.getElementById("QReviewClosedVsQuarterStatusStack").style.display="block";
if(response=="No Data"){
   // alert("in if");
    qReviewDashBoardByQuarterNoData(response);
}else{
   // alert("in else");
     var result1 = response.split("*@!");
    ClosedVsQuarterReportStackedChat(result1);
}
        
   
}
  function qReviewDashBoardByQuarterNoData(result){
    document.getElementById("QReviewClosedVsQuarterStatusStack").style.display="block";
      
    var arraydata = [['Status', 'Count']];
    
    var result1=result.split("*@!");
    
    for(var i=0; i<result1.length-1; i++){
        
        var res = result1[i].split("#^$");
        var dArray = [res[0],parseInt(res[1])];
        arraydata.push(dArray);
    }

    var data = google.visualization.arrayToDataTable(arraydata);
    var options = {
         title:'Operation Status Vs Quarter',
        titleTextStyle: {
            color: ('blue', '#3E93D4'),    // any HTML string color ('red',
											// '#cc00cc')
            fontName: 'Times New Roman', // i.e. 'Times New Roman'
            fontSize: 15, // 12, 18 whatever you want (don't specify px)
            bold: true,    // true or false
            italic: false   // true of false
        },
        legend: 'right',
        chartArea:{
            
            width:"80%",
            height:"80%"
        },
        
        is3D: true,
        pieSliceText: 'value',
        sliceVisibilityThreshold: 0
         
    };
    // align:'center';
    var chart = new google.visualization.PieChart(document.getElementById("ClosedVsQuarterStackChart"));
    function selectHandler() {
             
        var selectedItem = chart.getSelection()[0];
        // alert("selectedItem--"+data.getValue(selectedItem.row, 0));
        if (selectedItem) {
            var activityType = data.getValue(selectedItem.row, 0);
            // getBdmStatisticsDetailsByLoginId(activityType,bdmId);
            getQReviewListByStatus(activityType);
        }
    }
    google.visualization.events.addListener(chart, 'select', selectHandler);   
    chart.draw(data, options,dArray);
    
    
}
// }
function ClosedVsQuarterReportStackedChat(result){
    google.load('visualization', '1.1', {
        packages: ['corechart']
    });
    var arraydata = [['Quarter', 'HrClosed','HrRejected',{
        role: 'annotation'
    }]];
    for(var i=0; i<result.length-1; i++){
        
        var res = result[i].split("#^$");
        var dArray = [res[0],parseInt(res[1]),parseInt(res[2]),''];
        arraydata.push(dArray);
    }
    
    var data = google.visualization.arrayToDataTable(arraydata);
    var view=new google.visualization.DataView(data);
   
    var options = {
        title:'Operation Status Vs Quarter',
        titleTextStyle: {
            color: ('blue', '#3E93D4'),    // any HTML string color ('red',
											// '#cc00cc')
            fontName: 'Times New Roman', // i.e. 'Times New Roman'
            fontSize: 15, // 12, 18 whatever you want (don't specify px)
            bold: true,    // true or false
            italic: false   // true of false
           
     
        },
        
        vAxes:[{
            title:'Status',
            minValue: 0,
         ticks: [0,20, 40, 60, 80, 100,120,140,160,180,200],
            titleTextStyle:
            {
                color: ('blue', '#3E93D4'),    // any HTML string color ('red',
												// '#cc00cc')
                fontName: 'Times New Roman', // i.e. 'Times New Roman'
                fontSize: 15, // 12, 18 whatever you want (don't specify px)
                bold: true,    // true or false
                italic: false   // true of false
            }
        }],
        hAxis:{
            title:'Quarter',
            titleTextStyle:
            {
                color: ('blue', '#3E93D4'),    // any HTML string color ('red',
												// '#cc00cc')
                fontName: 'Times New Roman', // i.e. 'Times New Roman'
                fontSize: 15, // 12, 18 whatever you want (don't specify px)
                bold: true,    // true or false
                italic: false   // true of false
            }
        },
        width: 700,
        height: 350,
        bar: {
            groupWidth: '50%'
        },
        isStacked: true,
        bars :'vertical',
        legend: {
            position: 'bottom', 
            maxLines: 8
        }
    };


    var chart = new google.visualization.ColumnChart(document.getElementById("ClosedVsQuarterStackChart"));
    function selectHandler() {
             
        var selectedItem = chart.getSelection()[0];
      


     
        
       
        if (selectedItem) {
            var activityType = data.getValue(selectedItem.row, 0);
            var columnLabel = data.getColumnLabel(selectedItem.column);
            // getStudentInfoStackByCollege(activityType,columnLabel,3);
            getQReviewListByStatusVsQuarter(activityType,columnLabel);

        }
    }
    google.visualization.events.addListener(chart, 'select', selectHandler);   
    chart.draw(data, options,dArray);

}


function  getQReviewListByStatusVsPractice(activityType,columnLabel){
     $('span.pagination').empty().remove();
   
    var qYear=document.getElementById("year1").value;
    var qQuarterly=document.getElementById("quarterly1").value;

    var tableId = document.getElementById("tblQReviewDashBoard1");
    ClrTable(tableId);
    // var startDate=document.getElementById('startDateSummaryGraph').value;
    // var endDate=document.getElementById('endDateSummaryGraph').value;
    document.getElementById('qreviewReport1').style.display='block';
   
    var url = CONTENXT_PATH+"/getQReviewListByStatusVsPractice.action?year="+qYear+"&quarter="+qQuarterly+"&activityType="+activityType+"&columnLabel="+columnLabel;

    var req = newXMLHttpRequest();
    req.onreadystatechange = function() {
        if (req.readyState == 4) {
            if (req.status == 200) {      
                document.getElementById("qreviewReport1").style.display = 'none';
                displayQReviewDetailsByStatus(req.responseText,tableId,activityType,columnLabel);                        
            } 
        }else {
            document.getElementById("qreviewReport1").style.display = 'block';
        }
    };
    req.open("GET", url, true);
    req.send(null); 
}


function  getQReviewListByStatusVsQuarter(activityType,columnLabel){
    $('span.pagination').empty().remove();
   
    var qYear=document.getElementById("year2").value;
    // var qQuarterly=document.getElementById("quarterly2").value;

    var tableId = document.getElementById("tblQReviewDashBoard2");
    ClrTable(tableId);
    // var startDate=document.getElementById('startDateSummaryGraph').value;
    // var endDate=document.getElementById('endDateSummaryGraph').value;
    document.getElementById('qreviewReport2').style.display='block';
   
    var url = CONTENXT_PATH+"/getQReviewListByStatusVsQuarter.action?year="+qYear+"&quarter="+activityType+"&activityType="+activityType+"&columnLabel="+columnLabel;

    var req = newXMLHttpRequest();
    req.onreadystatechange = function() {
        if (req.readyState == 4) {
            if (req.status == 200) {      
                document.getElementById("qreviewReport2").style.display = 'none';
                displayQReviewDetailsByStatus(req.responseText,tableId,activityType,columnLabel);                        
            } 
        }else {
            document.getElementById("qreviewReport2").style.display = 'block';
        }
    };
    req.open("GET", url, true);
    req.send(null); 
}


/*
 * Q-Review Vs Manager's
 * 
 */


function  getQReviewListByManagers(){
    $('span.pagination').empty().remove();
    var yearForManagersReport=document.getElementById("yearForManagersReport").value;
    var quarterlyForManagersReport=document.getElementById("quarterlyForManagersReport").value;
    var quarterlyLivingCountry=document.getElementById("quarterlyLivingCountry").value;
    var isIncludeTeam=document.getElementById("isIncludeTeam").checked;
    var opsContactId=document.getElementById("opsContactId").value;
    var ReportsToField=document.getElementById("ReportsToField").value;
    var practiceName=document.getElementById("practiceName").value;
    
    // alert("opsContactId" +opsContactId);
   // alert("ReportsToField" +ReportsToField);
   // alert("practiceName" +practiceName);
    
    // alert("isIncludeTeam---"+isIncludeTeam+"--quarterlyLivingCountry--"+quarterlyLivingCountry);

    if(yearForManagersReport.trim().length==0){
        alert("please Enter Year");
        return false;
    }
    if(quarterlyForManagersReport.trim()==''){
        alert("please select Quarter");
        return false;
    }
    var tableId = document.getElementById("tblQReviewManagerDashBoard");
    ClrDashBordTable(tableId);
    // var startDate=document.getElementById('startDateSummaryGraph').value;
    // var endDate=document.getElementById('endDateSummaryGraph').value;
  document.getElementById('loadingForManagersReport').style.display='block';
    
    var myObj = {};
    myObj["yearForManagersReport"]=yearForManagersReport;
    myObj["quarterlyForManagersReport"]=quarterlyForManagersReport;
    myObj["quarterlyLivingCountry"]=quarterlyLivingCountry;
    myObj["isIncludeTeam"]=isIncludeTeam;
    myObj["opsContactId"]=opsContactId;
    myObj["ReportsToField"]=ReportsToField;
    myObj["practiceName"]=practiceName;
    var json = JSON.stringify(myObj);
    $.ajax({
   	url:'getQReviewListByManagers.action',
        data:{qReviewMnagersDetailsJson: json},
        contentType: 'application/json',
        type: 'GET',
        context: document.body,
        success: function(responseText) {
       // alert("responseText"+responseText);
       
       displayQReviewDetailsByManager(responseText);   
       document.getElementById('loadingForManagersReport').style.display='none';
       $("#tblQReviewManagerDashBoard").tableHeadFixer({
           'left' : 3, 
           'foot' : false, 
           'head' : true
       });
        },
        error: function(e){
        	  alert("Please try again");
        }
    });
}
 
function displayQReviewDetailsByManager(response){
    var tableId = document.getElementById("tblQReviewManagerDashBoard");  
    var headerFields = new Array("S.No","Manager&nbsp;Name","Total&nbsp;Resources","NotEntered","Submitted<br>Pending","MgrApproval<br>Pending","OperationTeam<br>Pending","MgrRejected","HrRejected","Closed");
   
    var dataArray = response;
    qReviewDetailsByManagerParseAndGenerateHTML(tableId,dataArray, headerFields);
}
 
function qReviewDetailsByManagerParseAndGenerateHTML(oTable,responseString,headerFields) {
    var fieldDelimiter = "#^$";
    var recordDelimiter = "*@!";   
    var records = responseString.split(recordDelimiter); 
    generateQReviewDetailsByManagerRows(oTable,headerFields,records,fieldDelimiter);
}

function generateQReviewDetailsByManagerRows(oTable, headerFields,records,fieldDelimiter) {	
	// alert("records---"+records);

    var tbody = oTable.childNodes[0];    
    tbody = document.createElement("TBODY");
    oTable.appendChild(tbody);
    generateTableHeaderForManagerQReview(oTable,headerFields);
    var rowlength;
    rowlength = records.length;
    if(rowlength >0 && records!=""){
        for(var i=0;i<rowlength-1;i++) {
                
            generateQReviewDetailsByManager(oTable,tbody,records[i],fieldDelimiter);
        }
        
    } else {
        generateNoRecords(tbody,oTable);
    }
    generateFooterForMangerQReview(tbody);
    pagerOptionForManagerGrid();
}


function generateQReviewDetailsByManager(oTable,tableBody,record,delimiter){
    // alert("record--"+record);
    var row;
    var cell;
    var fieldLength;
    // var fields = record.split(delimiter);
    var fields = record.split("#^$");
    fieldLength = fields.length ;
    var length = fieldLength;
    
    row = document.createElement( "TR" );
    row.className="gridRowEven";
    tableBody.appendChild( row );
    var yearForManagersReport=document.getElementById("yearForManagersReport").value;
    var quarterlyForManagersReport=document.getElementById("quarterlyForManagersReport").value;
    var country=document.getElementById("quarterlyLivingCountry").value;
    var isIncludeTeam=document.getElementById("isIncludeTeam").checked;

  for (var i=0;i<length-1;i++) {
            if(i==3 && fields[3]>0){
               cell = document.createElement( "TD" );
            cell.className="gridColumn"; 
            var j = document.createElement("a");
            cell.innerHTML = "<a href='javascript:getEmployeeDetailsByAppraisalStatus(\""+yearForManagersReport+"\",\""+quarterlyForManagersReport+"\",\""+country+"\",\""+isIncludeTeam+"\",\""+fields[10]+"\",\"NotEntered\",\""+fields[1]+"\")'>"+fields[3]+"</a>";
      
            cell.appendChild(j);    
           }else if(i==4 && fields[4]>0){
               cell = document.createElement( "TD" );
            cell.className="gridColumn"; 
            var j = document.createElement("a");
            cell.innerHTML = "<a href='javascript:getEmployeeDetailsByAppraisalStatus(\""+yearForManagersReport+"\",\""+quarterlyForManagersReport+"\",\""+country+"\",\""+isIncludeTeam+"\",\""+fields[10]+"\",\"NotSubmitted\",\""+fields[1]+"\")'>"+fields[4]+"</a>";
      
            cell.appendChild(j);    
           } else if(i==5 && fields[5]>0){
               cell = document.createElement( "TD" );
            cell.className="gridColumn"; 
            var j = document.createElement("a");
            cell.innerHTML = "<a href='javascript:getEmployeeDetailsByAppraisalStatus(\""+yearForManagersReport+"\",\""+quarterlyForManagersReport+"\",\""+country+"\",\""+isIncludeTeam+"\",\""+fields[10]+"\",\"Submitted\",\""+fields[1]+"\")'>"+fields[5]+"</a>";
      
            cell.appendChild(j);    
           } else if(i==6 && fields[6]>0){
               cell = document.createElement( "TD" );
            cell.className="gridColumn"; 
            var j = document.createElement("a");
            cell.innerHTML = "<a href='javascript:getEmployeeDetailsByAppraisalStatus(\""+yearForManagersReport+"\",\""+quarterlyForManagersReport+"\",\""+country+"\",\""+isIncludeTeam+"\",\""+fields[10]+"\",\"Approved\",\""+fields[1]+"\")'>"+fields[6]+"</a>";
      
            cell.appendChild(j);    
           } else if(i==7 && fields[7]>0){
               cell = document.createElement( "TD" );
            cell.className="gridColumn"; 
            var j = document.createElement("a");
            cell.innerHTML = "<a href='javascript:getEmployeeDetailsByAppraisalStatus(\""+yearForManagersReport+"\",\""+quarterlyForManagersReport+"\",\""+country+"\",\""+isIncludeTeam+"\",\""+fields[10]+"\",\"MgrRejected\",\""+fields[1]+"\")'>"+fields[7]+"</a>";
      
            cell.appendChild(j);    
           }  else if(i==8 && fields[8]>0){
               cell = document.createElement( "TD" );
            cell.className="gridColumn"; 
            var j = document.createElement("a");
            cell.innerHTML = "<a href='javascript:getEmployeeDetailsByAppraisalStatus(\""+yearForManagersReport+"\",\""+quarterlyForManagersReport+"\",\""+country+"\",\""+isIncludeTeam+"\",\""+fields[10]+"\",\"HrRejected\",\""+fields[1]+"\")'>"+fields[8]+"</a>";
      
            cell.appendChild(j);    
           }else if(i==9 && fields[9]>0){
               cell = document.createElement( "TD" );
            cell.className="gridColumn"; 
            var j = document.createElement("a");
            cell.innerHTML = "<a href='javascript:getEmployeeDetailsByAppraisalStatus(\""+yearForManagersReport+"\",\""+quarterlyForManagersReport+"\",\""+country+"\",\""+isIncludeTeam+"\",\""+fields[10]+"\",\"Closed\",\""+fields[1]+"\")'>"+fields[9]+"</a>";
      
            cell.appendChild(j);    
           }else{
              cell = document.createElement( "TD" );
              cell.className="gridColumn";       
       
                  cell.setAttribute("align","center");
            
                cell.innerHTML = fields[i];   
         }  
           
            if(fields[i]!=''){
            if(i==1)
            {
                cell.setAttribute("align","left");
            }
            else
            {
                cell.setAttribute("align","center");     
            }
            row.appendChild( cell );
        }
           
           
        } 
    
    
    
}


function generateFooterForMangerQReview(tbody) {
  
    var cell;
    var footer =document.createElement("TR");
    footer.className="gridPager";
    tbody.appendChild(footer);
    cell = document.createElement("TD");
    cell.className="gridFooter";

    // cell.colSpan = "7";
    
    cell.colSpan = "7";
    
       
   

    footer.appendChild(cell);
}

function generateTableHeaderForManagerQReview(tableBody,headerFields) {
     
  var row;
    var cell;
    
    var thead=document.createElement("thead");
    tableBody.appendChild(thead);
    row = document.createElement( "TR" );
    row.className="dashBoardgridHeader";
    thead.appendChild(row);
    
    for (var i=0; i<headerFields.length; i++) {
        cell = document.createElement( "TH" );
        cell.className="dashBoardgridHeader";
        if(i==1){
        cell.setAttribute("style", "padding:7px 150px 7px 7px;");
    }
        row.appendChild( cell );
        cell.innerHTML = headerFields[i];
        cell.width = 120;
    }
}
function ClrDashBordTable(myHTMLTable) { 
    var tbl =  myHTMLTable;
    
tbl.deleteTHead();
    var lastRow = tbl.rows.length; 
    while (lastRow > 0) { 
        tbl.deleteRow(lastRow - 1);  
        lastRow = tbl.rows.length; 
    } 
}


function getEmployeeDetailsByAppraisalStatus(year,quarter,country,isIncludeTeam,mgrLoginId,status,empName){
    var opsContactId=document.getElementById("opsContactId").value;
    var resourceName=document.getElementById("ReportsToField").value;
    var practiceId=document.getElementById("practiceName").value;
    
   window.location=CONTENXT_PATH+"/employee/appraisal/getEmployeeDetailsByAppraisalStatus.action?loginId="+mgrLoginId+"&year="+year+"&quarterly="+quarter+"&status="+status+
   "&empName="+empName+"&country="+country+"&isIncludeTeam="+isIncludeTeam+"&opsContactId="+opsContactId+
   "&resourceName="+resourceName+"&practiceId="+practiceId;
   
}



function getEmployeesForMngrByApprasailStatus(){
     var tableId = document.getElementById("tblEmpDetailsByAppraisalStatus");  
   ClrTable(tableId);
   
    
      var year=document.getElementById("year").value;
    var loginId=document.getElementById("loginId").value;
    var status=document.getElementById("status").value;
    var quarterly=document.getElementById("quarterly").value;
    var country=document.getElementById("country").value;
    var isIncludeTeam=document.getElementById("isIncludeTeam").value;
    
   
     document.getElementById("loadingMessage").style.display = 'block';
  
   
     var req = newXMLHttpRequest();
    req.onreadystatechange = readyStateHandlerText(req,displayEmployeesForMngrByApprasailStatus); 
    var url = CONTENXT_PATH+"/getEmployeesForMngrByApprasailStatus.action?loginId="+loginId+"&year="+year+"&quarter="+quarterly+"&status="+status+"&country="+country+"&isIncludeTeam="+isIncludeTeam;
    // alert(url);
    req.open("GET",url,"true");    
    req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    req.send(null);
}


function displayEmployeesForMngrByApprasailStatus(response){
      document.getElementById("loadingMessage").style.display = 'none';
      var tableId = document.getElementById("tblEmpDetailsByAppraisalStatus");  
       ClrTable(tableId);
    var headerFields = new Array("S.No","EmployeeName","Department","Practice","SubPractice","Email","WorkPhone","OperationContactName","Hire&nbsp;Date");
      var dataArray = response;
    ParseAndGenerateHTML(tableId,dataArray, headerFields);
}

function getMyQReviewDetalils(empId,lineId,appraisalId,curretRole){
	var searchYear=document.getElementById("year").value;
	var searchQuarterly=document.getElementById("quarterly").value;
	var txtCurr=document.getElementById("txtCurr").value;
	  window.location=CONTENXT_PATH+"/employee/appraisal/myQuarterlyAppraisalEdit.action?empId="+empId+"&lineId="+lineId+"&appraisalId="+appraisalId+"&curretRole="+curretRole+"&searchYear="+searchYear+"&searchQuarterly="+searchQuarterly+"&txtCurr="+txtCurr;;
	  
}

function getMyTeamQReviewDetails(empId,lineId,appraisalId,curretRole){
	var searchYear=document.getElementById("year").value;
	var searchQuarterly=document.getElementById("quarterly").value;
	var searchStatus=document.getElementById("status").value;
	var searchLoginId=document.getElementById("loginId").value;
	var txtCurr=document.getElementById("txtCurr").value;
	window.location=CONTENXT_PATH+"/employee/appraisal/myQuarterlyAppraisalEdit.action?empId="+empId+"&lineId="+lineId+"&appraisalId="+appraisalId+"&curretRole="+curretRole+"&searchYear="+searchYear+"&searchQuarterly="+searchQuarterly+"&searchStatus="+searchStatus+"&searchLoginId="+searchLoginId+"&txtCurr="+txtCurr;
	 
}

function getOpperationTeamQReviewDetails(empId,lineId,appraisalId,curretRole){
	var searchYear=document.getElementById("year").value;
	var searchQuarterly=document.getElementById("quarterly").value;
	var searchStatus=document.getElementById("status").value;
	var searchLoginId=document.getElementById("loginId").value;
	var searchDepartmentId=document.getElementById("departmentId").value;
	var searchPracticeId=document.getElementById("practiceId").value;
	var searchSubPractice=document.getElementById("subPractice").value;
	var searchOpsContactId=document.getElementById("opsContactId").value;
	var searchLocation=document.getElementById("location").value;
	var searchTitleId=document.getElementById("titleId").value;
	var weightageFrom=document.getElementById("weightageFrom").value;
	var weightageTo=document.getElementById("weightageTo").value;
	var txtCurr=document.getElementById("txtCurr").value;
	var backToFlag=document.getElementById("backToFlag").value;
	var searchOperationStatus=document.getElementById("operationTeamStatus").value;
	window.location=CONTENXT_PATH+"/employee/appraisal/myQuarterlyAppraisalEdit.action?empId="+empId+"&lineId="+lineId+"&appraisalId="+appraisalId+"&curretRole="+curretRole+"&searchYear="+searchYear+"&searchQuarterly="+searchQuarterly+"&searchStatus="+searchStatus+"&searchLoginId="+searchLoginId+"&searchDepartmentId="+searchDepartmentId+"&searchPracticeId="+searchPracticeId+"&searchSubPractice="+searchSubPractice+"&searchOpsContactId="+searchOpsContactId+"&searchLocation="+searchLocation+"&searchTitleId="+searchTitleId+"&weightageFrom="+weightageFrom+"&weightageTo="+weightageTo+"&txtCurr="+txtCurr+"&backToFlag="+backToFlag+"&searchOperationStatus="+searchOperationStatus;
	 
}


/*
 * for project risk dashboard
 */

function  getProjectRiskDetails(){
    var customerName=document.getElementById("customerName").value;
    var status=document.getElementById("status").value;
    var impact = document.getElementById("impact").value;
    var tableId = document.getElementById("tblProjectRiskDashBoard");
    ClrTable(tableId);
    // var startDate=document.getElementById('startDateSummaryGraph').value;
    // var endDate=document.getElementById('endDateSummaryGraph').value;
   // document.getElementById('projectReport').style.display='block';
   
    var url = CONTENXT_PATH+"/getProjectRiskDetails.action?customerName="+customerName+"&status="+status+"&impact="+impact;
// alert(url)
    var req = newXMLHttpRequest();
    req.onreadystatechange = function() {
        if (req.readyState == 4) {
            if (req.status == 200) {      
               // alert(url)
                document.getElementById("projectReport").style.display = 'none';
                displayProjectRiskDetails(req.responseText);                        
            } 
        }else {
            document.getElementById("projectReport").style.display = 'block';
        }
    };
    req.open("GET", url, true);
    req.send(null); 
}
 
function displayProjectRiskDetails(response){
   // alert(response)
    var tableId = document.getElementById("tblProjectRiskDashBoard");  
    var headerFields = new Array("SNo","Customer&nbspName","Project&nbspName","Description","Assigned&nbspto","Impact","Closed&nbspDate","Status","Resolution");
   
    var dataArray = response;
    ProjectRiskDetailsParseAndGenerateHTML(tableId,dataArray, headerFields);
}
 
function ProjectRiskDetailsParseAndGenerateHTML(oTable,responseString,headerFields) {
    var fieldDelimiter = "#^$";
    var recordDelimiter = "*@!";   
    var records = responseString.split(recordDelimiter); 
    generateProjectRiskDetailsRows(oTable,headerFields,records,fieldDelimiter);
}

function generateProjectRiskDetailsRows(oTable, headerFields,records,fieldDelimiter) {	

    var tbody = oTable.childNodes[0];    
    tbody = document.createElement("TBODY");
    oTable.appendChild(tbody);
    generateTableHeader(tbody,headerFields);
    var rowlength;
    rowlength = records.length;
    if(rowlength >0 && records!=""){
        for(var i=0;i<rowlength-1;i++) {
                
            generateProjectRiskDetails(oTable,tbody,records[i],fieldDelimiter);
        }
        
    } else {
        generateNoRecords(tbody,oTable);
    }
    generateProjectRiskDashBoardFooter(tbody,oTable);
    pagerOption();
}

function generateProjectRiskDashBoardFooter(tbody) {
  
    var cell;
    var footer =document.createElement("TR");
    footer.className="gridPager";
    footer.setAttribute("Id", "taskFooter")
    tbody.appendChild(footer);
    cell = document.createElement("TD");
    cell.className="gridFooter";
    

    // cell.colSpan = "7";
    
    cell.colSpan = "9";
    
       
   

    footer.appendChild(cell);
}


function generateProjectRiskDetails(oTable,tableBody,record,delimiter){
    var row;
    var cell;
    var fieldLength;
    // var fields = record.split(delimiter);
    var fields = record.split("#^$");
    fieldLength = fields.length ;
    var length = fieldLength;
    
    row = document.createElement( "TR" );
    row.className="gridRowEven";
    tableBody.appendChild( row );
    
    for (var i=0;i<length;i++) {  
    	 if(i==3){
             cell = document.createElement( "TD" );
             cell.className="gridColumn";       
            
             cell.setAttribute("align","center");
             if(fields[i] != '-'){
                // alert("----");
                  cell.innerHTML = "<a href='#' onclick='getProjectDescription(\""+fields[i]+"\")'>View</a>";
           
             }else{
                 // alert(fields[i]);
                  cell.innerHTML = fields[i]; 
             }

           // cell.innerHTML = "<a href='#' onclick
			// ='getProjectDescription("+fields[i]+");'>View</a>";
         }
         else if(i==8){
             cell = document.createElement( "TD" );
             cell.className="gridColumn";       
            // alert(fields[i]);
             cell.setAttribute("align","center");
             if(fields[i] != '-'){
                    cell.innerHTML =  "<a href='#' onclick='getResoulutionDescription(\""+fields[i]+"\")'>View</a>";
          }else{
                      cell.innerHTML = fields[i]; 
             }
           // cell.innerHTML = "<a href='#' onclick
			// ='getProjectDescription("+fields[i]+");'>View</a>";
         }

        else{
            cell = document.createElement( "TD" );
            cell.className="gridColumn";       
       
            cell.setAttribute("align","center");
            
            cell.innerHTML = fields[i]; 
        }
            
        if(fields[i]!=''){
            if(i==1)
            {
                cell.setAttribute("align","left");
            }
            else
            {
                cell.setAttribute("align","center");     
            }
            row.appendChild( cell );
        }
    }   
    row.appendChild( cell );
    
}

function getProjectDescription(desc){
    // alert(desc)
     var background = "#3E93D4";
    var title = "Project Risk description";
    // var text1 = text;
    // var size = text1.length;
    
    

    
    var size = desc.length;
    
    // Now create the HTML code that is required to make the popup
    var content = "<html><head><title>"+title+"</title></head><body bgcolor='"+background +"' style='color:white;'><h4>"+title+"</h4>"+desc+"<br /></body></html>";
    
    if(size < 50){
        // Create the popup
        popup = window.open("","window","channelmode=0,width=300,height=150,top=200,left=350,scrollbars=no,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no");    
        popup.document.write(content); // Write content into it.
    }
    
    else if(size < 100){
        // Create the popup
        popup = window.open("","window","channelmode=0,width=400,height=200,top=200,left=350,scrollbars=no,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no");    
        popup.document.write(content); // Write content into it.
    }
    
    else if(size < 260){
        // Create the popup
        popup = window.open("","window","channelmode=0,width=400,height=200,top=200,left=350,scrollbars=no,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no");    
        popup.document.write(content); // Write content into it.
    } else {
        // Create the popup
        popup = window.open("","window","channelmode=0,width=400,height=200,top=200,left=350,scrollbars=no,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no");    
        popup.document.write(content); // Write content into it.
    }
    
}

/* project risk dashbaord end */


function getResoulutionDescription(desc){
    var background = "#3E93D4";
   var title = "Resolution";
   // var text1 = text;
   // var size = text1.length;
   
   

   
   var size = desc.length;
   
   // Now create the HTML code that is required to make the popup
   var content = "<html><head><title>"+title+"</title></head><body bgcolor='"+background +"' style='color:white;'><h4>"+title+"</h4>"+desc+"<br /></body></html>";
   
   if(size < 50){
       // Create the popup
       popup = window.open("","window","channelmode=0,width=300,height=150,top=200,left=350,scrollbars=no,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no");    
       popup.document.write(content); // Write content into it.
   }
   
   else if(size < 100){
       // Create the popup
       popup = window.open("","window","channelmode=0,width=400,height=200,top=200,left=350,scrollbars=no,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no");    
       popup.document.write(content); // Write content into it.
   }
   
   else if(size < 260){
       // Create the popup
       popup = window.open("","window","channelmode=0,width=400,height=200,top=200,left=350,scrollbars=no,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no");    
       popup.document.write(content); // Write content into it.
   } else {
       // Create the popup
       popup = window.open("","window","channelmode=0,width=400,height=200,top=200,left=350,scrollbars=no,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no");    
       popup.document.write(content); // Write content into it.
   }
   
}



/*
 * Populating performance details Author : Nagalakshmi Telluri Date : 08/04/2017
 */
// adding performance field


function getPerformance(empId,currId,stateStartDate,stateEndDate){
	var loginId=$("#loginId").val();
	
	
var req = newXMLHttpRequest();
req.onreadystatechange = readyStateHandlerText(req, populatePerformance);
var url = CONTENXT_PATH+"/getEmployeePerformance.action?empId="+empId+"&currId="+currId+"&stateStartDate="+stateStartDate+"&stateEndDate="+stateEndDate+"&loginId="+loginId;
req.open("GET",url,"true");    
req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
req.send(null);
}


function populatePerformance(resText) {
var background = "#3E93D4";
var title = "Training Performance";
var size = resText.length;

var text = resText.split("addTo");

// alert("text[0]..."+text[0]+"text[1].."+text[1]+"text[2]....."+text[2]+"text[3]....."+text[3]+"text[4]....."+text[4]+"text[5]....."+text[5]);
// Now create the HTML code that is required to make the popup
var content = "<html><head><title>"+title+"</title></head>\
<body bgcolor='"+background +"' style='color:white;'><h4>"+title+"</h4>Comments:"+text[1]+"<br/>Rating:"+text[2]+"<br>Ecertification Score<br>\n";
var tableData=text[0];
if(tableData == "no data")
{
  alert("No records found");
}

else
{
var tblRow=tableData.split("*@!");

if(tblRow.length == 1)
{
  
content=content+"<table border='1' style:align='center'>";

content=content+"<tr><th>TopicName</th><th>Percentage</th><th>Marks</th><th>ExamStatus</th></tr>";
content=content+"<tr> <td colspan='4'> No record found for this  search </td> </tr>";

  // alert("No records found");
}

else
{


content=content+"<table border='1' style:align='center'>";

content=content+"<tr><th>TopicName</th><th>Percentage</th><th>Marks</th><th>ExamStatus</th></tr>";


for(var n=0;n<tblRow.length;n++){
content=content+"<tr>";
var tblCol=tblRow[n].split("#^$");
for(var j=0;j<tblCol.length;j++){
content=content+"<td>"+tblCol[j]+"</td>";   
}
content=content+"</tr>";
}
content=content+"</table>";}}

content=content+"</body></html>";
  
 if(size < 50){
   // Create the popup
   popup = window.open("","window","channelmode=0,width=350,height=200,top=200,left=350,scrollbars=no,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no");    
   popup.document.write(content); // Write content into it.
}

else if(size < 100){
   // Create the popup channelmode
   popup = window.open("","window","=0,width=400,height=200,top=200,left=350,scrollbars=no,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no");    
   popup.document.write(content); // Write content into it.
}

else if(size < 260){
   // Create the popup
   popup = window.open("","window","channelmode=0,width=500,height=300,top=200,left=350,scrollbars=no,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no");    
   popup.document.write(content); // Write content into it.
} else {
   // Create the popup
   popup = window.open("","window","channelmode=0,width=600,height=400,top=200,left=350,scrollbars=no,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no");    
   popup.document.write(content); // Write content into it.
}

}

// Resource ut






function getEmpReportsCharts()
{
    
    var country=document.getElementById("country").value;
    var departmentId=document.getElementById("departmentId").value;
    var practiceId=document.getElementById("practiceId").value;
    var subPractice2=document.getElementById("subPractice2").value;
    var availableUtl=document.getElementById("availableUtl").value;
   
    document.getElementById("countVsStatusPie").style.display="none";
    document.getElementById("resourcesVsMainPie").style.display="none";
    document.getElementById('projectReportNote').style.display='none';
    var tableId = document.getElementById("tblReportDashBoard");
    ClrTable(tableId);
    var req = newXMLHttpRequest();
    req.onreadystatechange = function() {
        if (req.readyState == 4) {
            if (req.status == 200) {      
                document.getElementById("projectReport").style.display = 'none';
                displaycountVsStatusPieChart(req.responseText); 
                document.getElementById('projectReportNote').style.display='block';
                getresourcesVsMainPieCharts1();
            } 
        }else {
            document.getElementById("projectReport").style.display = 'block';
        }
    }; 
    var url = CONTENXT_PATH+"/getCountVsStatusReport.action?graphId=0&country="+country+"&department="+departmentId+"&practiceId="+practiceId+"&subPractice="+subPractice2+"&availableUtl="+availableUtl+"";
    
    req.open("GET",url,"true");    
    req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    req.send(null);
}

function displaycountVsStatusPieChart(response) {
    var dataArray = response;
    if(response!=''){
           
        countVsStatusPieChart(response);
   
    }else{
        alert('No Result For This Search...');
        spnFast.innerHTML="No Result For This Search...";                
    }
     
// getIssuesDashBoardByPriority();
}


function countVsStatusPieChart(result){
    document.getElementById("countVsStatusPie").style.display="block";
      
    var arraydata = [['Status', 'Count']];
    
    var result1=result.split("*@!");
    var cou=0;
    for(var i=0; i<result1.length-1; i++){
        
        var res = result1[i].split("#^$");
        var dArray = [res[0],parseInt(res[1])];
        cou=cou+parseInt(res[1]);
        arraydata.push(dArray);
    }

   
    var data = google.visualization.arrayToDataTable(arraydata);
    var options = {
        title: 'Resource by Status ('+cou+')' ,
        titleTextStyle: {
            color: ('blue', '#3E93D4'),    // any HTML string color ('red',
											// '#cc00cc')
            fontName: 'Times New Roman', // i.e. 'Times New Roman'
            fontSize: 15, // 12, 18 whatever you want (don't specify px)
            bold: true,    // true or false
            italic: false   // true of false
        },
        legend: 'right',
        chartArea:{
            
            width:"75%",
            height:"75%"
        },
        
        is3D: true,
        pieSliceText: 'value',
        sliceVisibilityThreshold: 0
         
    };
    // align:'center';
    var chart = new google.visualization.PieChart(document.getElementById("countVsStatusPieChart"));
    function selectHandler() {
             
        var selectedItem = chart.getSelection()[0];
        // alert("selectedItem--"+data.getValue(selectedItem.row, 0));
        if (selectedItem) {
            var statusType = data.getValue(selectedItem.row, 0);
            $('span.pagination').empty().remove();
            getResourcesByStatusList(statusType);
        }
    }
    google.visualization.events.addListener(chart, 'select', selectHandler);   
    chart.draw(data, options,dArray);
    
    
}	


function getresourcesVsMainPieCharts1()
{
    $('span.pagination').empty().remove();
 
    var country=document.getElementById("country").value;
    var departmentId=document.getElementById("departmentId").value;
    var practiceId=document.getElementById("practiceId").value;
    var subPractice2=document.getElementById("subPractice2").value;
    var availableUtl=document.getElementById("availableUtl").value;
    // alert("2nd"+country+departmentId+practiceId+subPractice2+availableUtl);

    document.getElementById("resourcesVsMainPie").style.display="none";
  
    var tableId = document.getElementById("tblReportDashBoard");
    ClrTable(tableId);
    
    var req = newXMLHttpRequest();
    req.onreadystatechange = function() {
        if (req.readyState == 4) {
            if (req.status == 200) {      
                document.getElementById("projectReport").style.display = 'none';
                displayresourcesVsMainPieChart(req.responseText); 
            // getIssuesDashBoardByPriority();
            } 
        }else {
            document.getElementById("projectReport").style.display = 'block';
        }
    }; 
    var url = CONTENXT_PATH+"/getCountVsStatusReport.action?graphId=1&country="+country+"&department="+departmentId+"&practiceId="+practiceId+"&subPractice="+subPractice2+"&availableUtl="+availableUtl+"";
    
    req.open("GET",url,"true");    
    req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    req.send(null);
}

function displayresourcesVsMainPieChart(response) {
    var dataArray = response;
    if(response!=''){
           
        resourcesVsMainPieChart(response);
   
    }else{
        alert('No Result For This Search...');
        spnFast.innerHTML="No Result For This Search...";                
    }
    
// getIssuesDashBoardByAssignment();
}


function resourcesVsMainPieChart(result){
    document.getElementById("resourcesVsMainPie").style.display="block";
      
    var arraydata = [['Resources', 'Count']];
    
    var result1=result.split("*@!");
    var cou=0;
    for(var i=0; i<result1.length-1; i++){
        
        var res = result1[i].split("#^$");
        var dArray = [res[0],parseInt(res[1])];
        cou=cou+parseInt(res[1]);
        arraydata.push(dArray);
    }

    var data = google.visualization.arrayToDataTable(arraydata);
    var options = {
        title: 'Resources by ProjectStatus ('+cou+')' ,
        titleTextStyle: {
            color: ('blue', '#3E93D4'),    // any HTML string color ('red',
											// '#cc00cc')
            fontName: 'Times New Roman', // i.e. 'Times New Roman'
            fontSize: 15, // 12, 18 whatever you want (don't specify px)
            bold: true,    // true or false
            italic: false   // true of false
        },
        legend: 'right',
        chartArea:{
            
            width:"75%",
            height:"75%"
        },
        
        is3D: true,
        pieSliceText: 'value',
        sliceVisibilityThreshold: 0
         
    };
    // align:'center';
    var chart = new google.visualization.PieChart(document.getElementById("resourcesVsMainPieChart"));
    function selectHandler() {
             
        var selectedItem = chart.getSelection()[0];
        // alert("selectedItem--"+data.getValue(selectedItem.row, 0));
        if (selectedItem) {
            var activityType = data.getValue(selectedItem.row, 0);
            // getBdmStatisticsDetailsByLoginId(activityType,bdmId);
           $('span.pagination').empty().remove();
            getResourcesVsMainList(activityType);
            
        }
    }
    google.visualization.events.addListener(chart, 'select', selectHandler);   
   
    chart.draw(data, options,dArray);
        
    
}

function  getResourcesByStatusList(activityType){
    var country=document.getElementById("country").value;
    var departmentId=document.getElementById("departmentId").value;
    var practiceId=document.getElementById("practiceId").value;
    var subPractice2=document.getElementById("subPractice2").value;
    var availableUtl=document.getElementById("availableUtl").value;
    var tableId = document.getElementById("tblReportDashBoard");
    ClrTable(tableId);
    
    
  
    document.getElementById('projectReport').style.display='block';
   
    document.getElementById('projectReportNote').style.display='none';
    // var url =
	// CONTENXT_PATH+"/getTaskListByStatus.action?reportsTo="+reportsTo+"&taskStartDate="+taskStartDate+"&taskEndDate="+taskEndDate+"&activityType="+activityType+"&graphId="+graphId;
    var url = CONTENXT_PATH+"/getResourcesByStatusList.action?activityType="+activityType+"&graphId=0&country="+country+"&department="+departmentId+"&practiceId="+practiceId+"&subPractice="+subPractice2+"&availableUtl="+availableUtl+"";

    var req = newXMLHttpRequest();
    req.onreadystatechange = function() {
        if (req.readyState == 4) {
            if (req.status == 200) {  
                document.getElementById('projectReportNote').style.display='block';
                document.getElementById("projectReport").style.display = 'none';
                displayResourcesByStatusList(req.responseText,activityType);                        
            } 
        }else {
            document.getElementById("projectReport").style.display = 'block';
        }
    };
    req.open("GET", url, true);
    req.send(null); 
}


function displayResourcesByStatusList(response,activityType){
    var tableId = document.getElementById("tblReportDashBoard"); 
  
    // alert(activityType);
    if (activityType=='Training' || activityType=='OverHead' || activityType=='R&D/POC'){
        var headerFields = new Array("S.No","Emp Name","ReportsTo","Email","WorkPhone","HireDate","Practice","SubPractice","StartDate");
    }else if (activityType=='OnProject'){
        var headerFields = new Array("S.No","Emp Name","ReportsTo","Email","WorkPhone","HireDate","Practice","SubPractice","StartDate","Utilization %","Exp");
    }
    else{
        var headerFields = new Array("S.No","Emp Name","ReportsTo","Email","WorkPhone","HireDate","Practice","SubPractice","StartDate",activityType+" Utilization %","Experience");
    }
    var dataArray = response;
    ResourcesByStatusListParseAndGenerateHTML(tableId,dataArray, headerFields,activityType);
}
 
function ResourcesByStatusListParseAndGenerateHTML(oTable,responseString,headerFields,activityType) {
    var fieldDelimiter = "#^$";
    var recordDelimiter = "*@!";   
    var records = responseString.split(recordDelimiter); 
    generateResourcesByStatusListRows(oTable,headerFields,records,fieldDelimiter,activityType);
}



function generateResourcesByStatusListRows(oTable, headerFields,records,fieldDelimiter,activityType) {	

    tbody = oTable.childNodes[0];    
    var tbody = document.createElement("TBODY");
    oTable.appendChild(tbody);
    generateTableHeader(tbody,headerFields);
    var rowlength;
    rowlength = records.length;
    if(rowlength >0 && records!=""){
        for(var i=0;i<rowlength-1;i++) {
                
            generateResourcesByStatusList(i,oTable,tbody,records[i],fieldDelimiter,activityType);
        }
        
    } else {
        generateNoRecordsFound(tbody,oTable,headerFields.length);
    }
    generateResourcesByStatusListFooter(tbody,oTable,headerFields.length);
    pagerOption();
}


function generateResourcesByStatusList(index,oTable,tableBody,record,delimiter,activityType){
    var row;
    var cell;
    var fieldLength;
    // var fields = record.split(delimiter);
    var fields = record.split("#^$");
    fieldLength = fields.length ;
    var length = fieldLength;
    
       
    
    row = document.createElement( "TR" );
    row.className="gridRowEven";
    tableBody.appendChild( row );
    
    if (activityType=='Training' || activityType=='OverHead' || activityType=='R&D/POC'){
        cell = document.createElement( "TD" );
        cell.className="gridColumn";    
          
        cell.innerHTML =(index+1) ;
        cell.setAttribute("align","left");   
        row.appendChild( cell ); 
            
            
            
            
        cell = document.createElement( "TD" );
        cell.className="gridColumn";    
        cell.innerHTML =fields[2] ;
        cell.setAttribute("align","left");   
        row.appendChild( cell );  
    
 
        cell = document.createElement( "TD" );
        cell.className="gridColumn";    
        cell.innerHTML =fields[3] ;
        cell.setAttribute("align","left");   
        row.appendChild( cell );  
            
        cell = document.createElement( "TD" );
        cell.className="gridColumn";    
        cell.innerHTML =fields[4] ;
        cell.setAttribute("align","left");   
        row.appendChild( cell );  
            
            
        cell = document.createElement( "TD" );
        cell.className="gridColumn";    
        cell.innerHTML =fields[5] ;
        cell.setAttribute("align","left");   
        row.appendChild( cell );  
            
        cell = document.createElement( "TD" );
        cell.className="gridColumn";    
        cell.innerHTML =fields[6] ;
        cell.setAttribute("align","left");   
        row.appendChild( cell ); 
        
        cell = document.createElement( "TD" );
        cell.className="gridColumn";    
        cell.innerHTML =fields[7] ;
        cell.setAttribute("align","left");   
        row.appendChild( cell );
        
        cell = document.createElement( "TD" );
        cell.className="gridColumn"; 
         cell.innerHTML =fields[8] ;
        cell.setAttribute("align","left");   
        row.appendChild( cell );
        
        cell = document.createElement( "TD" );
        cell.className="gridColumn"; 
        cell.innerHTML =fields[9] ;
        cell.setAttribute("align","left");   
        row.appendChild( cell );
            
            
              
    }else if (activityType=='OnProject'){
          
        cell = document.createElement( "TD" );
        cell.className="gridColumn";    
        cell.innerHTML =(index+1) ;
        cell.setAttribute("align","left");   
        row.appendChild( cell ); 
            
            
            
            
        cell = document.createElement( "TD" );
        cell.className="gridColumn";    
        cell.innerHTML =fields[2] ;
        cell.setAttribute("align","left");   
        row.appendChild( cell );  
    
 
        cell = document.createElement( "TD" );
        cell.className="gridColumn";    
        cell.innerHTML =fields[3] ;
        cell.setAttribute("align","left");   
        row.appendChild( cell );  
            
        cell = document.createElement( "TD" );
        cell.className="gridColumn";    
        cell.innerHTML =fields[4] ;
        cell.setAttribute("align","left");   
        row.appendChild( cell );  
            
            
        cell = document.createElement( "TD" );
        cell.className="gridColumn";    
        cell.innerHTML =fields[5] ;
        cell.setAttribute("align","left");   
        row.appendChild( cell );  
            
        cell = document.createElement( "TD" );
        cell.className="gridColumn";    
        cell.innerHTML =fields[6] ;
        cell.setAttribute("align","left");   
        row.appendChild( cell ); 
        
        cell = document.createElement( "TD" );
        cell.className="gridColumn";    
         cell.innerHTML =fields[7] ;
        cell.setAttribute("align","left");   
        row.appendChild( cell );
        
        cell = document.createElement( "TD" );
        cell.className="gridColumn"; 
        cell.innerHTML =fields[8] ;
        cell.setAttribute("align","left");   
        row.appendChild( cell );
        
        cell = document.createElement( "TD" );
        cell.className="gridColumn"; 
        cell.innerHTML =fields[9] ;
        cell.setAttribute("align","left");   
        row.appendChild( cell );
        
        cell = document.createElement( "TD" );
        cell.className="gridColumn"; 
        cell.innerHTML = "<a href='javascript:getUtilizationDetails("+fields[0]+")'>"+fields[10]+"</a>";
        cell.setAttribute("align","left");   
        row.appendChild( cell );
        
        cell = document.createElement( "TD" );
        cell.className="gridColumn"; 
        cell.innerHTML =fields[11] ;
        cell.setAttribute("align","left");   
        row.appendChild( cell );
          
          
    }else{
        
        cell = document.createElement( "TD" );
        cell.className="gridColumn";    
        cell.innerHTML =(index+1) ;
        cell.setAttribute("align","left");   
        row.appendChild( cell ); 
            
            
            
            
        cell = document.createElement( "TD" );
        cell.className="gridColumn";    
        cell.innerHTML =fields[2] ;
        cell.setAttribute("align","left");   
        row.appendChild( cell );  
    
 
        cell = document.createElement( "TD" );
        cell.className="gridColumn";    
        cell.innerHTML =fields[3] ;
        cell.setAttribute("align","left");   
        row.appendChild( cell );  
            
        cell = document.createElement( "TD" );
        cell.className="gridColumn";    
        cell.innerHTML =fields[4] ;
        cell.setAttribute("align","left");   
        row.appendChild( cell );  
            
            
        cell = document.createElement( "TD" );
        cell.className="gridColumn";    
        cell.innerHTML =fields[5] ;
        cell.setAttribute("align","left");   
        row.appendChild( cell );  
            
        
        cell = document.createElement( "TD" );
        cell.className="gridColumn";    
        cell.innerHTML =fields[9] ;
        cell.setAttribute("align","left");   
        row.appendChild( cell );  
            
        cell = document.createElement( "TD" );
        cell.className="gridColumn";    
        cell.innerHTML =fields[10] ;
        cell.setAttribute("align","left");   
        row.appendChild( cell ); 
        
        cell = document.createElement( "TD" );
        cell.className="gridColumn";    
         cell.innerHTML =fields[11] ;
        cell.setAttribute("align","left");   
        row.appendChild( cell );
        
        cell = document.createElement( "TD" );
        cell.className="gridColumn"; 
        cell.innerHTML =fields[12] ;
        cell.setAttribute("align","left");   
        row.appendChild( cell );
       
        
       cell = document.createElement( "TD" );
        cell.className="gridColumn";    
        cell.innerHTML = "<a href='javascript:getUtilizationDetails("+fields[0]+")'>"+fields[6]+"</a>";
        cell.setAttribute("align","left");   
        row.appendChild( cell );  
            
        cell = document.createElement( "TD" );
        cell.className="gridColumn";    
        cell.innerHTML =fields[7] ;
        cell.setAttribute("align","left");   
        row.appendChild( cell ); 
             
          
           
    }
            
           
}
function generateNoRecordsFound(tbody,oTable,len) {
   
    var noRecords =document.createElement("TR");
    noRecords.className="gridRowEven";
    tbody.appendChild(noRecords);
    cell = document.createElement("TD");
    cell.className="gridColumn";
    cell.colSpan = len;
    
    cell.innerHTML = "No Records Found for this Search";
    noRecords.appendChild(cell);
}
function generateResourcesByStatusListFooter(tbody,oTable,len) {
  
    var cell;
    var footer =document.createElement("TR");
    footer.className="gridPager";
    footer.setAttribute("Id", "taskFooter");
    tbody.appendChild(footer);
    cell = document.createElement("TD");
    cell.className="gridFooter";
    

    // cell.colSpan = "7";
    
    cell.colSpan = len;
    
       
   

    footer.appendChild(cell);
}


function  getResourcesVsMainList(activityType){
    var country=document.getElementById("country").value;
    var departmentId=document.getElementById("departmentId").value;
    var practiceId=document.getElementById("practiceId").value;
    var subPractice2=document.getElementById("subPractice2").value;
    var tableId = document.getElementById("tblReportDashBoard");
    ClrTable(tableId);
  
    document.getElementById('projectReport').style.display='block';
   
    // var url =
	// CONTENXT_PATH+"/getTaskListByStatus.action?reportsTo="+reportsTo+"&taskStartDate="+taskStartDate+"&taskEndDate="+taskEndDate+"&activityType="+activityType+"&graphId="+graphId;
    var url = CONTENXT_PATH+"/getResourcesByStatusList.action?activityType="+activityType+"&graphId=1&&country="+country+"&department="+departmentId+"&practiceId="+practiceId+"&subPractice="+subPractice2+"";

    var req = newXMLHttpRequest();
    req.onreadystatechange = function() {
        if (req.readyState == 4) {
            if (req.status == 200) {      
                document.getElementById("projectReport").style.display = 'none';
                displayResourcesVsMainList(req.responseText,activityType);                        
            } 
        }else {
            document.getElementById("projectReport").style.display = 'block';
        }
    };
    req.open("GET", url, true);
    req.send(null); 
}


function displayResourcesVsMainList(response,activityType){
    var tableId = document.getElementById("tblReportDashBoard");  
    var headerFields = new Array("S.No","ResourceName","CustomerName","ReportsTo","ProjectName","StartDate","EndDate","Utilization","EmpProjType","Billable","Practice","SubPractice");
   
    var dataArray = response;
    ResourcesVsMainListParseAndGenerateHTML(tableId,dataArray, headerFields,activityType);
}
 
function ResourcesVsMainListParseAndGenerateHTML(oTable,responseString,headerFields,activityType) {
    var fieldDelimiter = "#^$";
    var recordDelimiter = "*@!";   
    var records = responseString.split(recordDelimiter); 
    generateResourcesVsMainListRows(oTable,headerFields,records,fieldDelimiter,activityType);
}



function generateResourcesVsMainListRows(oTable, headerFields,records,fieldDelimiter,activityType) {	

    tbody = oTable.childNodes[0];    
    var tbody = document.createElement("TBODY");
    oTable.appendChild(tbody);
    generateTableHeader(tbody,headerFields);
    var rowlength;
    rowlength = records.length;
    if(rowlength >0 && records!=""){
        for(var i=0;i<rowlength-1;i++) {
                
            generateResourcesVsMainListRows1(i,oTable,tbody,records[i],fieldDelimiter,activityType);
        }
        
    } else {
        generateNoRecordsFound(tbody,oTable,headerFields.length);
    }
    generateResourcesByStatusListFooter(tbody,oTable,headerFields.length);
    pagerOption();
}

// Santhosh.Kola#^$Miracle Software Systems . Inc(Intrenal
// Projects)#^$Hubble#^$2012-06-11 00:00:00#^$-#^$90#^$-#^$Shadow#^$
function generateResourcesVsMainListRows1(index,oTable,tableBody,record,delimiter,activityType){
    var row;
    var cell;
    var fieldLength;
    // var fields = record.split(delimiter);
    
  
    var fields = record.split("#^$");
    fieldLength = fields.length ;
    var length = fieldLength;
    
        
    
    row = document.createElement( "TR" );
    row.className="gridRowEven";
    tableBody.appendChild( row );
    
    
    cell = document.createElement( "TD" );
    cell.className="gridColumn";    
          
    cell.innerHTML =(index+1) ;
    cell.setAttribute("align","left");   
    row.appendChild(cell); 
            
    cell = document.createElement( "TD" );
    cell.className="gridColumn";    
          
    cell.innerHTML =fields[0] ;
    cell.setAttribute("align","left");   
    row.appendChild( cell );  
    
 
    cell = document.createElement( "TD" );
    cell.className="gridColumn";    
          
    cell.innerHTML =fields[1] ;
    cell.setAttribute("align","left");   
    row.appendChild( cell );  
            
    cell = document.createElement( "TD" );
    cell.className="gridColumn";    
          
    cell.innerHTML =fields[2] ;
    cell.setAttribute("align","left");   
    row.appendChild( cell );
    
    
          cell = document.createElement( "TD" );
    cell.className="gridColumn";    
          
    cell.innerHTML =fields[3] ;
    cell.setAttribute("align","left");   
    row.appendChild( cell );   
    
    cell = document.createElement( "TD" );
    cell.className="gridColumn";    
          
    cell.innerHTML =fields[10] ;
    cell.setAttribute("align","left");   
    row.appendChild( cell );  
            
    cell = document.createElement( "TD" );
    cell.className="gridColumn";    
          
    cell.innerHTML =fields[4] ;
    cell.setAttribute("align","left");   
    row.appendChild( cell );
            
    cell = document.createElement( "TD" );
    cell.className="gridColumn";    
          
    cell.innerHTML =fields[5] ;
    cell.setAttribute("align","left");   
    row.appendChild( cell );
            
            
    cell = document.createElement( "TD" );
    cell.className="gridColumn";    
          
    cell.innerHTML =fields[7] ;
    cell.setAttribute("align","left");   
    row.appendChild( cell );
            
    cell = document.createElement( "TD" );
    cell.className="gridColumn";    
          
    cell.innerHTML =fields[6] ;
    cell.setAttribute("align","left");   
    row.appendChild( cell );
            
            
    cell = document.createElement( "TD" );
    cell.className="gridColumn";    
          
    cell.innerHTML =fields[8] ;
    cell.setAttribute("align","left");   
    row.appendChild( cell );
    cell = document.createElement( "TD" );
    cell.className="gridColumn";    
          
    cell.innerHTML =fields[9] ;
    cell.setAttribute("align","left");   
    row.appendChild( cell );
}

function generateTableHeader11(tableBody,headerFields) {
    var row;
    var cell;
    row = document.createElement( "TR" );
    row.className="gridHeader";
    tableBody.appendChild( row );
    for (var i=0; i<headerFields.length; i++) {
        cell = document.createElement( "TD" );
        cell.className="gridHeader";
        row.appendChild( cell );

        cell.setAttribute("width","10000px");
        cell.innerHTML = headerFields[i];
    }
}


function getUtilizationDetails(empId){
    var url = CONTENXT_PATH+"/getUtilizationDetails.action?empId="+empId;

    var req = newXMLHttpRequest();
    req.onreadystatechange = function() {
        if (req.readyState == 4) {
            if (req.status == 200) {      
               displayUtilizationDetails(req.responseText);                        
            } 
        }else {
        // alert("Http-Error");
        }
    };
    req.open("GET", url, true);
    req.send(null);
}

function displayUtilizationDetails(response){
    var background = "#3E93D4";
    var title = "Utilization Details";
    var size = response.length;
  
    // var text = response.split("addTo");

    var content = "<html><head><title>"+title+"</title></head>\
   <body bgcolor='"+background +"' style='color:white;'><h4>"+title+"</h4><br/>\n";
    var tableData=response;
    if(tableData == "NoData")
    {
        alert("No records found");
    }
   
    else
    {
        var tblRow=tableData.split("*@!");
       if(tblRow.length == 1)
        {
       
            content=content+"<table border='1' style:align='center'>";

            content=content+"<tr><th>ProjectName</th><th>Utilization</th></tr>";
            content=content+"<tr> <td colspan='4'> No record found for this  search </td> </tr>";

            alert("No records found");
        }
   
        else
        {
            
            content=content+"<table border='1' style:align='center'>";

            content=content+"<tr><th>ProjectName</th><th>Utilization</th></tr>";


            for(var n=0;n<tblRow.length;n++){
                content=content+"<tr>";
                var tblCol=tblRow[n].split("#^$");
                for(var j=0;j<tblCol.length;j++){
                    content=content+"<td>"+tblCol[j]+"</td>";   
                }
                content=content+"</tr>";
            }
            content=content+"</table>";
        }
    }

content=content+"</body></html>";
       
if(size < 50){
    // Create the popup
    popup = window.open("","window","channelmode=0,width=300,height=150,top=200,left=350,scrollbars=no,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no");    
    popup.document.write(content); // Write content into it.
}
    
else if(size < 100){
    // Create the popup channelmode
    popup = window.open("","window","=0,width=400,height=200,top=200,left=350,scrollbars=no,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no");    
    popup.document.write(content); // Write content into it.
}
    
else if(size < 260){
    // Create the popup
    popup = window.open("","window","channelmode=0,width=400,height=200,top=200,left=350,scrollbars=no,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no");    
    popup.document.write(content); // Write content into it.
} else {
    // Create the popup
    popup = window.open("","window","channelmode=0,width=400,height=200,top=200,left=350,scrollbars=no,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no");    
    popup.document.write(content); // Write content into it.
}
}



/* Employee Transfr Starts */

function getDues(location){
 	 
  	if(location != ""){
  		if(document.getElementById("empId").value != 0 && document.getElementById("empId").value.length > 2){
  			getExpencesDetails(document.getElementById("empId").value)
  		}
  	if(location == "Miracle City"){
  		// alert(location)
  		        $("#mcityDues").show();
  		        $("#mcityDues1").show();
  		        $("#otherDue").hide();
  		        
  		      $("#mcityexp").show();
  		    $("#mcityexp1").show();
  		  $("#mcityexp2").show();
  		 $("#mcityexp3").show();
  		 
  		 $("#mirexp").hide();
  		 $("#mirexp1").hide();
  		      
  		   
  		// document.getElementById("mcityDues").style.display = '';
  		// document.getElementById("mcityDues1").style.display = '';
  		// document.getElementById("otherDue").style.display = 'none';
  		
  	}else{
  		
  	  $("#mcityexp").hide();
	    $("#mcityexp1").hide();
	  $("#mcityexp2").hide();
	 $("#mcityexp3").hide();
	 
	 $("#mirexp").show();
	 $("#mirexp1").show();
  		
  		 $("#mcityDues").hide();
  		 $("#mcityDues1").hide();
  		 $("#otherDue").show();
  		
  		// document.getElementById("mcityDues").style.display = 'none';
  		// document.getElementById("mcityDues1").style.display = 'none';
  		// document.getElementById("otherDue").style.display = '';
  	}
  	}else{
  		 $("#mcityDues").hide();
  		 $("#mcityDues1").hide();
  		 $("#otherDue").hide();
  		 
  		
		 
  		 
  		$("#mcityexp").hide();
	    $("#mcityexp1").hide();
	  $("#mcityexp2").hide();
	 $("#mcityexp3").hide();
	 
	 $("#mirexp").hide();
	 $("#mirexp1").hide();
  	}
  	
  }
  
function validField(){
	 
	 
   	
   	var isManager = document.getElementById("isUserManager").value; 
   	var isAdminAccess = document.getElementById("isAdminAccess").value; 
             
    var myDeptId = document.getElementById("myDeptId").value;
    var sessionEmpPractice = document.getElementById("sessionEmpPractice").value;
                 	
    var empName = document.getElementById("empName").value;
    var status = document.getElementById("status").value;
    // alert("hiiiiiiii"+status)
    var initiatedOn = document.getElementById("initiatedOn").value;
    var initiatedOnDate=new Date(initiatedOn);
    
    var fromLocation = document.getElementById("fromLocation").value;
    var toLocation = document.getElementById("toLocation").value;
    
    var fromLocationnew = document.getElementById("fromLocationnew").value;
    var toLocationnew = document.getElementById("toLocationnew").value;
    
    var transferType = document.getElementById("transferType").value;
   	
    var userId = document.getElementById("userId").value; 
	var reportedTo = document.getElementById("reportedTo").value;
   	var tentativeReportedDate = document.getElementById("tentativeReportedDate").value;
   	
   	var reportedTonew = document.getElementById("reportedToNew").value;
   	
   	var tentativeReportedDatenew = document.getElementById("tentativeReportedDateNew").value;
   
  
    var tentativeReportedDate1=new Date(tentativeReportedDate);
    var tentativeReportedDate2=new Date(tentativeReportedDatenew);
  // alert(sessionEmpPractice+"----"+userId)
    
  
    
	if(transferType == null || transferType == ""){
   		x0p( '','Please Select TransferType','info');
   		return false;
   	}
	
	
   	if(empName == null || empName == ""){
   		x0p( '','Please Enter EmpName','info');
   		return false;
   	}
   	if(status == null || status == ""){
   		x0p( '','Please Enter Status','info');
   		return false;
   	}
   	if(initiatedOn == null || initiatedOn == ""){
   		x0p( '','Please Enter InitiatedOn','info');
   		return false;
   	}
   	
   	if(transferType == "CountryTransfer")
   	{
   	
   	if(fromLocationnew == null || fromLocationnew == ""){
	   		x0p( '','Please Enter FromLocation','info');
	   		return false;
	   	}
	   	if(toLocationnew == null || toLocationnew == ""){
	   		x0p( '','Please Enter ToLocation','info');
	   		return false;
	   	}
	   	
	   	if(toLocationnew == 'India')
	   		{
	   	  	
	   		
	   		
	   	   	if(reportedTo == null || reportedTo == ""){
	   	   		x0p( '','Please Enter ReportedTo','info');
	   	   		return false;
	   	   	}
	   	   	if(tentativeReportedDate == null || tentativeReportedDate == ""){
	   	   		x0p( '','Please Enter TentativeReportedDate','info');
	   	   		return false;
	   	   	}
	   	   	
	   	 if(tentativeReportedDate1 <= initiatedOnDate){
	    		x0p( '','TentativeReportedDate must be greater than InitiatedOn','info');
	    		return false;
	    	}
	   	
	   		}
	   	
	   	
	   	else
	   		{
	   		if(reportedTonew == null || reportedTonew == ""){
	   		
 	   		x0p( '','Please Enter  ReportedTo','info');
 	   		return false;
	   		}
 	   	if(tentativeReportedDatenew == null || tentativeReportedDatenew == ""){
 	   		x0p( '','Please Enter TentativeReportedDate','info');
 	   		return false;
 	   	}
 	   	
 	 	if(tentativeReportedDate2 <= initiatedOnDate){
 	   		x0p( '','TentativeReportedDate must be greater than InitiatedOn','info');
 	   		return false;
 	   	}
   	}
 	   
 	  
 	   	
 	   	return true;	
	   	
	 
   	}   else
   		{
   		if(fromLocation == null || fromLocation == ""){
   	   		x0p( '','Please Enter FromLocation','info');
   	   		return false;
   	   	}
   	   	if(toLocation == null || toLocation == ""){
   	   		x0p( '','Please Enter ToLocation','info');
   	   		return false;
   	   	
   	   	}
   		}
		
   	if(sessionEmpPractice != "Systems" ){
   	if(fromLocation == "Miracle City"){
   		var cafteriaDues = document.getElementById("cafteriaDues").value;
       	var hostelDues = document.getElementById("hostelDues").value;
       	
   	if(cafteriaDues == null || cafteriaDues == ""){
   		x0p( '','Please Enter CafteriaDues','info');
   		return false;
   	}
   	if(hostelDues == null || hostelDues == ""){
   		x0p( '','Please Enter HostelDues','info');
   		return false;
   	}
   	}else{
   		var transportationDues = document.getElementById("transportationDues").value;
           
   		if(transportationDues == null || transportationDues == ""){
       		x0p( '','Please Enter TransportationDues','info');
       		return false;
       	}	
   	}
   	
   	
       	
    
     	
   	if(reportedTo == null || reportedTo == ""){
   		x0p( '','Please Enter ReportedTo','info');
   		return false;
   	}
   	if(tentativeReportedDate == null || tentativeReportedDate == ""){
   		x0p( '','Please Enter TentativeReportedDate','info');
   		return false;
   	}
   
   	if(tentativeReportedDate1 <= initiatedOnDate){
   		x0p( '','TentativeReportedDate must be greater than InitiatedOn','info');
   		return false;
   	}
     		
   	}
   	
   	
   	
   	if((myDeptId == "Operations" && sessionEmpPractice == "Systems") || isAdminAccess ==1){
   		
   		var itTeamStatus = document.getElementById("itTeamStatus").value;
   		// alert("itTeamStatus"+itTeamStatus)
   		if(itTeamStatus == null || itTeamStatus == ""){
   		x0p( '','Please Select IT Team Status','info');
   		return false;
   		}else{
   		// alert("itTeamFlag")
   			document.getElementById("itTeamFlag").value = "1";
   		}
   	}
   	
   	var isNextLocationHR = document.getElementById("isNextLocationHR").value;
   	if(isNextLocationHR == 1){
   		var actualReportedDate = document.getElementById("actualReportedDate").value;
   		if(actualReportedDate == null || actualReportedDate == ""){
       		x0p( '','Please Enter ActualReportedDate','info');
       		return false;
       	}
   		if(actualReportedDate <= initiatedOn){
       		x0p( '','ActualReportedDate must be greater than TentativeReportedDate','info');
       		return false;
       	}
   	}/*
		 * else { alert("Hello in else"); if(empName == null || empName == ""){
		 * x0p( '','Please Enter EmpName','info'); return false; } if(status ==
		 * null || status == ""){ x0p( '','Please Enter Status','info'); return
		 * false; } if(initiatedOn == null || initiatedOn == ""){ x0p(
		 * '','Please Enter InitiatedOn','info'); return false; } }
		 */
   	
   	
   	
   	/*
	 * if(myDeptId == "Operations" && isManager == 1){
	 * 
	 * var actualReportedDate =
	 * document.getElementById("actualReportedDate").value;
	 * if(actualReportedDate == null || actualReportedDate == ""){ x0p(
	 * '','Please Enter ActualReportedDate','info'); return false; }
	 * if(actualReportedDate <= initiatedOn){ x0p( '','ActualReportedDate must
	 * be greater than TentativeReportedDate','info'); return false; } }
	 */
   }


function getNewLocations(){
   	// alert("getLocations-->")
   	var from = document.getElementById("fromLocationnew").value;
   	var to = document.getElementById("toLocationnew").value;
   	// alert(from+"<----getLocations-->"+to)
   	if(from == to){
   		
   		x0p( '','From and To Locations must be different','info');
   	}
   }



   function getNextLocHRComments(isReported){
   	// alert("isReported"+isReported.checked)
   	if(isReported.checked){
   		$("#nextLocationHRCommentsDiv").show();
   	}else{
   		 $("#nextLocationHRCommentsDiv").hide();
   	}
   }

   function selectFromCal(element){
   	x0p( '','Please select from Calender','info');
   element.value = "";
   }
   function getLocations(){
   	// alert("getLocations-->")
   	var from = document.getElementById("fromLocation").value;
   	var to = document.getElementById("toLocation").value;
   	// alert(from+"<----getLocations-->"+to)
   	if(from == to){
   		
   		x0p( '','From and To Locations must be different','info');
   	}
   }

   
   /* biometric */
   

   function getBiometricLoad() {
  // alert("---->+hiiiiii");
      // var empNo = document.getElementById("empNo").value;
        var empId = document.getElementById("empId").value;
        var year = document.getElementById("year").value;
        var month = document.getElementById("month").value;
        var reportType = document.getElementById("reportType").value;
   // alert(empNo+"--"+empId);
    // document.getElementById("empNoSpan").value="";
    // document.getElementById("empNameSpan").value="";
        document.getElementById('biometricBody').style.display = 'none';
      document.getElementById('tblbridges').style.display = 'none';
      document.getElementById('AttendanceDetails').style.display = 'none';
     document.getElementById('flagDesc').style.display = 'none';
     document.getElementById("noRecords").style.display="none";
      document.getElementById("loadingMsgForBioLoad").style.display="block";

        var req = newXMLHttpRequest();
        req.onreadystatechange = readyStateHandlerText(req,populateBiometricLoad); 

       var url=CONTENXT_PATH+"/getBiometricLoad.action?empId="+empId+"&year="+year+"&month="+month+"&reportType="+reportType;
      // alert(url)
       
      
       req.open("GET",url,"true");
       req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
       req.send(null);

   }



   function populateBiometricLoad(resText){
   // alert(resText)
   	  var oTable = document.getElementById("tblbridges");
   	if(resText == "NoRecord")
   		{
   		 document.getElementById("loadingMsgForBioLoad").style.display="none";
   		 document.getElementById("biometricBody").style.display="none";
   		 document.getElementById("noRecords").style.display="block";
   		
   		}else{
   			 document.getElementById("noRecords").style.display="none";
   		
     document.getElementById("loadingMsgForBioLoad").style.display="none";
     // alert(resText)
    var empId = document.getElementById("empId").value;
    var result='';
        var resTextSplit2=new Array();
    
      
        document.getElementById('biometricBody').style.display = 'block';
          document.getElementById('flagDesc').style.display = 'block';
        document.getElementById('AttendanceDetails').style.display = 'block';
           document.getElementById('tblbridges').style.display = 'block';
           clearTable(oTable);
           var headerFields = new Array("&nbsp;&nbsp;&nbsp;Sunday&nbsp;&nbsp;&nbsp;","&nbsp;&nbsp;Monday&nbsp;&nbsp;","&nbsp;&nbsp;Tuesday&nbsp;&nbsp;","Wednesday","Thursday","&nbsp;&nbsp;&nbsp;Friday&nbsp;&nbsp;&nbsp;","&nbsp;&nbsp;Saturday&nbsp;&nbsp;");	
            tbody = document.createElement("TBODY");
           oTable.appendChild(tbody);
           var count=0;
           resTextSplit2[0]="";
           generateTableHeaderForAvailableBiometricLoad(tbody,headerFields);
             
          var present = "0";
          var holiday = "0";
          var weeklyOff = "0";
          var absent = "0";
          var daysinMonth = "0";
          // generateRow(oTable,tbody,resTextSplit2,0);
      
           var datarows=resText.split('*@!');
      // alert(datarows.length)
           for(var i=0;i<datarows.length-1;i++){
               var data=datarows[i].split('#^$');
             // alert(data[0])
               var image="";
       
            
            data[1] = data[1].replace("??", "");
            
             if(data[1]=="Present"){
                  present++;
                   image="green.png";
               }
               else if(data[1]=="Absent" ){
                   absent++;
                   image="red.png";
               }
                 else if(data[1]=="Holiday" ){
                     holiday++;
                   image="darkgreen.png";
               }
               else if(data[1]=="WeeklyOff"){
                   weeklyOff++;
                   image="violet.png";
               }
              
               else if(data[1]=="WeeklyOff Present"){
                  image="babypink.png";
               }
                 else if(data[1]=="Holiday Present"){
                    image="cyan.png";
               }
                 else if(data[1]=="Holiday Absent"){
                     image="blackgrey.png";
                }
                 else if(data[1]=="WeeklyOff Absent"){
                     image="brown.png";
                }
                 else if(data[1]=="Half Present"){
                     image="orange.png";
                }
                 else if(data[1]=="WeeklyOff HalfPresent"){
                     image="blue1.png";
                }
                 else if(data[1]=="Holiday HalfPresent"){
                     image="pink.png";
                }
               else if(data[1]=="noRecord"){
                   image="yellow2.png";
               }
             daysinMonth = data[4];
     // alert("daysinMonth"+daysinMonth)
                   // document.getElementById("daysproject").value="0";
           
                  
   var dateobj= new Date(data[0]) ;

      

   var month = dateobj.getMonth() + 1;

   var year = dateobj.getFullYear() + 1;
   var reportType = document.getElementById("reportType").value;


      if(data[1] == "Blank"){
                  
                     resTextSplit2[count] = " ";
               }else{
                   var day = dateobj.getDate();
                  // alert(data[1]+"\",\""+data[2])
                  // resTextSplit2[count] = "<font
					// size='2'>"+day+"</font>.<img title='"+data[1]+"'
					// src='/Hubble/includes/images/ecertification/"+image+"'
					// width='13px' height='20px' border='0'
					// ><br>&nbsp;&nbsp;&nbsp;&nbsp;<font
					// size='2'>"+data[1].replace(/\ /g,"&nbsp;");+"</font>";
           var bioOverlay = "";
           if(data[1] != 'noRecord'){
                   if(data[2] != 0){
           	 
                   resTextSplit2[count] = "<a onclick='javascript:getBiometricHoursOverlay(\""+data[0]+"\",\""+data[1]+"\",\""+data[2]+"\",\""+data[3].replace("??", "")+"\",\""+empId+"\",\""+reportType+"\");' ><font size='2' color='#3300FF'>"+day+"</font>.<font size='2'>("+data[2]+"Hrs)</font><img title='"+data[1]+"' src='/Hubble/includes/images/ecertification/"+image+"'  width='13px' height='20px' border='0' /></a>";
            }else{
                resTextSplit2[count] = "<a onclick='javascript:getBiometricHoursOverlay(\""+data[0]+"\",\""+data[1]+"\",\""+data[2]+"\",\""+data[3].replace("??", "")+"\",\""+empId+"\",\""+reportType+"\");' ><font size='2' color='#3300FF'>"+day+"</font>.<img title='"+data[1]+"' src='/Hubble/includes/images/ecertification/"+image+"'  width='13px' height='20px' border='0' /></a>";
                 
            }
               }else{
            	   resTextSplit2[count] = "<font size='2' color='#3300FF'>"+day+"</font>.<img title='"+data[1]+"' src='/Hubble/includes/images/ecertification/"+image+"'  width='13px' height='20px' border='0' />";
                    
               }// alert(resTextSplit2[count])
          // resTextSplit2[count] = "<font size='2'>"+day+"</font>";
            
               }
               

                // resTextSplit2[count]="<div class='boxdiv'>"+data[0]+".
				// <b>"+data[2]+"</b><img
				// src='/Hubble/includes/images/ecertification/"+image+"'
				// width='20px' height='20px' border='0'
				// ><br>&nbsp;&nbsp;&nbsp;&nbsp;<font
				// size='2'>"+data[3]+"</font></div>";
               // resTextSplit2[count]=""+data[0]+". <b><a
				// href='javascript:getBcodeAndNumber(\""+data[2]+"\",\""+data[3]+"\",\""+data[5]+"\");'
				// >"+data[2]+"</a></b><img title='"+data[4]+"'
				// src='/Hubble/includes/images/ecertification/"+image+"'
				// width='20px' height='20px' border='0'
				// ><br>&nbsp;&nbsp;&nbsp;&nbsp;<font
				// size='2'>"+data[3].replace(/\ /g,"&nbsp;");+"</font>";
          
              
              // alert( resTextSplit2[count])
               // result+="<div class='boxdiv'>"+data[0]+".
				// <b>"+data[2]+"</b><img
				// src='/Hubble/includes/images/ecertification/"+image+"'
				// width='20px' height='20px' border='0'
				// ><br>&nbsp;&nbsp;&nbsp;&nbsp;<font
				// size='2'>"+data[3]+"</font></div>"
      // alert("(i+1)%7==0"+(i+1)%7)
      if((i+1)%7==0){
             // alert("test")
                generateRowAvailableBiometricLoad(oTable,tbody,resTextSplit2,0,data[1]);
                resTextSplit2=new Array();
                count=-1;
           }
         
           count++;
         // alert("count"+count)
       }
           document.getElementById("daysworked").value=present;
           document.getElementById("daysholidays").value=holiday;
             document.getElementById("daysweekend").value=weeklyOff;
               document.getElementById("daysvacation").value=absent;
                 document.getElementById("daysinmonth").value=daysinMonth;
           calculateAttendance(daysinMonth,present,holiday,weeklyOff,absent);
    // alert("last")
     // if(resTextSplit2.length>0)
      // generateRowAvailableBiometricLoad(oTable,tbody,resTextSplit2,0,data[1]);
         // generateTableHeader(tbody,headerFields);
          // var resTextSplit1 = resText.split("*@!");
        // for(var index=0;index<resTextSplit1.length-1;index++) {
               // resTextSplit2 = resTextSplit1[index].split("#^$");
             // generateRow(oTable,tbody,resTextSplit2,index);
         // }
         // generateFooter(tbody,oTable);
      
      // document.getElementById("resultData").innerHTML=result;
          $('.pagination-btn').click();
   		}
   }

   function generateTableHeaderForAvailableBiometricLoad(tableBody,headerFields) {
       var row;
       var cell;
       row = document.createElement( "TR" );
        tableBody.appendChild( row );
     // alert(headerFields)
     // alert("row"+row)
       // row.className="gridHeader";
       tableBody.appendChild(row);
       for (var i=0; i<headerFields.length; i++) {
           cell = document.createElement( "TD" );
           cell.className="daysHeader";
           row.appendChild( cell );
            cell.setAttribute('align','center');
             cell.setAttribute('bgcolor','#3e93d4');
          // cell.setAttribute('width', '120px');
           cell.innerHTML = headerFields[i];
           
         
           
     // cell.width = 150;
       }
   }
   function generateRowAvailableBiometricLoad(oTable,tableBody,rowFeildsSplit,index,data) {
       
   // alert("data "+data[1]);
       var row;
       var cell;
       row = document.createElement("TR");
       // row.className="gridRowEven";
       // cell = document.createElement("TD");
       // cell.className="gridRowEven";
       // cell.innerHTML = index+1;daysinmonth
       // row.appendChild(cell);
       tableBody.appendChild(row);
       var totalLeaves;
      
       for (var i=0; i<=rowFeildsSplit.length-1; i++) {
       
   // alert("rowFeildsSplit.length"+rowFeildsSplit.length);
           cell = document.createElement( "TD" );
           cell.setAttribute('width',10);
            // cell.width = 10;
             if(rowFeildsSplit[i] != " "){
           cell.className="bioboxdiv";
         
           cell.baseURI
             }
           /*
			 * if(data=="Present"){ // present++; image="#e6ffe6"; } else
			 * if(data=="Absent" ){ // absent++; image="#ffe6e6"; } else
			 * if(data=="Holiday" ){ // holiday++; image="#b3ffcc"; } else
			 * if(data=="WeeklyOff"){ // weeklyOff++; image="#fae6ff"; }
			 * 
			 * else if(data=="WeeklyOff Present"){ image="#ffe6f2"; } else
			 * if(data=="Holiday Present"){ image="#e6ffff"; } else
			 * if(data=="Holiday Absent"){ image="#f0f0f5"; } else
			 * if(data=="WeeklyOff Absent"){ image="#ffe6cc"; } else
			 * if(data=="Half Present"){ image="#ffd6cc"; } else
			 * if(data=="WeeklyOff HalfPresent"){ image="#e6f2ff"; } else
			 * if(data=="Holiday HalfPresent"){ image="#ffcce6"; } else
			 * if(data=="noRecord"){ image="#ffffcc"; }
			 */
           row.appendChild(cell);
             cell.setAttribute('align','center');
             // cell.setAttribute('bgcolor',image);
               
           cell.innerHTML = rowFeildsSplit[i];
        
       }
     // alert("----------------")
   }
               
   function closeConferenceSessionOverlay(){
       document.getElementById('resultMessage').innerHTML ='';
   //
   // clearEventData();

       document.getElementById("headerLabel").style.color="white";
       document.getElementById("headerLabel").innerHTML="Add Session Conference";
     // showRow('addTr');
       var overlay = document.getElementById('overlay');
       var specialBox = document.getElementById('specialBox');

       overlay.style.opacity = .8;
       if(overlay.style.display == "block"){
       overlay.style.display = "none";
       specialBox.style.display = "none";

    // document.getElementById("frmDBGrid").submit();

       }
       else {
       overlay.style.display = "block";
       specialBox.style.display = "block";
       }






    
      document.getElementById("frmDBGrid").submit();
   }

               
   function getBiometricHoursOverlay(date,status,biometricHrs,statusCode,empId,reportType){
       var overlay = document.getElementById('biometricOverlay');
       var specialBox = document.getElementById('hubbleBiometricOverlay');
      // alert(reportType+"-->reportType")
       // alert(status+"-->Status")
       document.getElementById('resultMessage').style.display = 'none';
           document.getElementById('date').value = date;
           document.getElementById('timeSheetHrs').value = ' ';
           document.getElementById('biometricHrs').value = biometricHrs;
           document.getElementById('status').value = statusCode;
         // document.getElementById('reportTypeHidden').value = reportType;
         // document.getElementById('statusHidden').value = status;
          // alert("in if"+reportType)
       if(reportType == "LiveData"){

       	document.getElementById("status").disabled=true;
   	
   	 $('#comUpdateId').hide();
   	// document.getElementById("comUpdateId").style.display="none";
   	
       }else{
   	 $('#comUpdateId').show();
   	 document.getElementById("status").disabled=false;
   	/*
	 * var mytd = document.getElementById("closeOverlay"); var aTag =
	 * document.createElement('a'); onclick="getBiometricHoursOverlay();"
	 * aTag.setAttribute('href',"#");
	 * aTag.setAttribute('onclick',"getBiometricHoursOverlay();");
	 * aTag.innerHTML = "link text"; mydiv.appendChild(aTag);
	 */
   	// document.getElementById("comUpdateId").style.display="block";
       }
          
          overlay.style.opacity = .8;
       if(overlay.style.display == "block"){
       	 
       	
           overlay.style.display = "none";
           specialBox.style.display = "none";
                      
       } else {
        // alert("in else")
    	  
           getAttendanceDetails(date,empId,status);
    	  
           overlay.style.display = "block";
           specialBox.style.display = "block";
       }   






   }

   function getAttendanceDetails(date,empId,status){
   	 var tableId = document.getElementById("tblPunchDetails");
   	    
   	    clearTable(tableId);
   // alert(empId+""+date+""+status)
           $.ajax({
           url:'getBioAttendanceDetails.action?biometricDate='+date+'&empId='+empId+'&status='+status,
           context: document.body,
           success: function(responseText) {
            // alert("res"+responseText)
            
             var datarows=responseText.split('*@!');
         
          
           for(var i=0;i<datarows.length-1;i++){
               var data=datarows[i].split('#^$');
              // alert("data[3]"+data[3]);
                  document.getElementById("timeSheetHrs").value=data[0];
                  document.getElementById("timeSheetStatus").innerHTML="<font size='2' color='green'>("+data[1]+")</font>";
                  document.getElementById("leaveStatusId").style.display="none";
                  document.getElementById("leaveStatus").style.display="none";
                 
                 
                if(status == 'Absent'){
                    document.getElementById("leaveStatusId").style.display="block";
                    document.getElementById("leaveStatus").style.display="block";
                    document.getElementById("leaveStatus").innerHTML="<font size='2' color='green'>"+data[2]+"</font>"; 
                }else{
               	 getPunchDetails(data[3]);
               	 document.getElementById("tdPunchDetails").style.display="block";
               	 
                }
           }
              
           },
           error: function(e){
              
               alert("Please try again");
           }
       });
   }








   function getPunchDetails(resText) 






   {
       var tableId = document.getElementById("tblPunchDetails");
       
       clearTable(tableId);
       var tbody = tableId.childNodes[0];    
       tbody = document.createElement("TBODY");
       tableId.appendChild(tbody);
       if(resText == 'NoRecords'){
       	generateNoRecordsPunchDetails(tbody,tableId);
       }
       else{
       // var headerFields = new
		// Array("SNo","Project&nbsp;Name","StartDate","EndDate","EmpStatus","Utilization","Resource&nbsp;Type");
       var headerFields = new Array("IN","In Device","OUT","Out Device");
      
       ParseAndGenerateHTMLPunchDetails(tableId,tbody,resText,headerFields);
       }
   }

   function ParseAndGenerateHTMLPunchDetails(oTable,tbody,responseString,headerFields) {
       
     /*
		 * var start = new Date(); var fieldDelimiter = "|"; var recordDelimiter =
		 * "^";
		 * 
		 * if(oTable.id=="tblPunchDetails"){ fieldDelimiter = "#^$";
		 * recordDelimiter = "*@!"; }
		 */
   	// var recordDelimiter = ",";
      // var records = responseString.split(recordDelimiter);
       generateTablePunchDetails(oTable,tbody,headerFields,responseString);
   }
   function generateTablePunchDetails(oTable,tbody,headerFields,responseString) {
   	// alert("-----"+responseString)
      
       generateTableHeader(tbody,headerFields);
       var rowlength;
      rowlength = responseString.length-1;
      
       if(rowlength >=1 && responseString!=""){
       	
       	  var recordDelimiter = ",";   
       	    var records = responseString.split(recordDelimiter);
       	    
       	    var field = new Array();
       	    var flag = 0;
       	    var fieldLength;
       	    var temp = false;
       	    var temp1 = false;
       	  
       	    fieldLength = records.length;
       	  // var init = records.indexOf('(');
       	  // var fin = records.indexOf(')');
       	    var response;
       	    for (var i=0;i<fieldLength;i++) {
       	    	// alert(records[i]+"-----records----"+records[i].substring(init
				// + 1, fin))
       	    	  
       	    	temp = records[i].includes("in");
       	    	  temp1 = records[i+1].includes("out");
       	    	  if(temp == true){
       	    		  if(temp1 == true){
       	    		  field[0] = records[i].substring(0, 5);
       	    		  field[1] = records[i].match(/\((.*?)\)/)[1];
      	        	     // field[1] = records[i].substring(init + 1, fin);
      	        	      field[2] = records[i+1].substring(0, 5);
   	        	      field[3] = records[i+1].match(/\((.*?)\)/)[1];
   	        	      i=i+1;
       	    		  }
       	    		  if(temp1 == false){
       	    			  field[0] = records[i].substring(0, 5);
          	        	      field[1] = records[i].match(/\((.*?)\)/)[1];
          	        	      field[2] = "-";
       	        	      field[3] = "-";
       	    		  }
       	    	  }else if(temp == false){
       	    		  field[0] = "-";
   	        	      field[1] = "-";
           	          field[2] = records[i].substring(0, 5);
          	        	  field[3] = records[i].match(/\((.*?)\)/)[1];
          	        	     } 
       	    	  
       	    	  response = field.toString();
       	    	  generateRowPunchDetails(oTable,tbody,response); 
       	    		  
       	    	  }
       	          
       	    	/*
				 * if(flag = 0 && records[i].includes("in")){ field[0] =
				 * records[i].substring(0, 5); field[1] =
				 * records[i].substring(init + 1, fin);
				 * 
				 * 
				 * 
				 * }else if(flag = 0 && records[i].includes("out")){ field[0] =
				 * "-"; field[1] = "-"; field[2] = records[i].substring(0, 5);
				 * field[3] = records[i].substring(init + 1, fin); flag = 1;
				 * 
				 * }else if(flag > 0 && records[i].includes("in")){ field[0] =
				 * records[i].substring(0, 5); field[1] =
				 * records[i].substring(init + 1, fin); }else if(flag > 0 &&
				 * records[i].includes("out")){ field[2] =
				 * records[i].substring(0, 5); field[3] =
				 * records[i].substring(init + 1, fin); }
				 */
       	    	  
       	    }
       	
       	     
      
       else {
           generateNoRecordsPunchDetails(tbody,oTable);
       }
       
       generateFooterPunchDetails(tbody,oTable);
   }
   function generateRowPunchDetails(oTable,tableBody,response) {
     // alert("In generateRow"+responseString);
     
       
   	  var row;
   	    var cell;
   	    var fieldLength;
   	    delimiter = ","
   	  var fields = response.split(delimiter);
   	    fieldLength = fields.length ;
   	    var length;
   	    // if(oTable.id == "tblAccountSummRep" || oTable.id ==
		// "tblUpdateForAccountsListByPriority"){
   	    length = fieldLength;
   	    // }
   	    
   	    // else {
   	    // length = fieldLength-1;
   	    // }
   	  
   	    row = document.createElement( "TR" );
   	    row.className="gridRowEven";
   	    tableBody.appendChild( row );
   	    // alert("length..."+length);
   	 
   	        for (var i=0;i<length;i++) {
   	       
   	            cell = document.createElement( "TD" );
   	            cell.className="gridColumn";
   	            cell.innerHTML = fields[i];
   	            if(fields[i]!=''){
   	                row.appendChild( cell );
   	            }
   	       
   	        } 
       // }
       
       // else {
       // length = fieldLength-1;
       // }
     
     
       // alert("length..."+length);
       
      /*
		 * if(oTable.id=="tblPunchDetails"){ for (var i=0;i<length;i++) { //
		 * field = ["-","-","-","-"];
		 * alert(records[i]+"-----records----"+records[i].substring(init + 1,
		 * fin)) if(flag = 0 && records[i].includes("in")){ field[0] =
		 * records[i].substring(0, 5); field[1] = records[i].substring(init + 1,
		 * fin); flag = 1;
		 * 
		 * }else if(flag = 0 && records[i].includes("out")){ field[0] = "-";
		 * field[1] = "-"; field[2] = records[i].substring(0, 5); field[3] =
		 * records[i].substring(init + 1, fin); }else if(flag > 0 &&
		 * records[i].includes("in")){ field[0] = records[i].substring(0, 5);
		 * field[1] = records[i].substring(init + 1, fin); }else if(flag > 0 &&
		 * records[i].includes("out")){ field[2] = records[i].substring(0, 5);
		 * field[3] = records[i].substring(init + 1, fin); } if(field.length ==
		 * 3){ row = document.createElement("TR"); row.className="gridRowEven";
		 * tableBody.appendChild( row ); }
		 * 
		 * 
		 * for(var j=0;j<4;j++){ cell = document.createElement( "TD" );
		 * cell.className="gridColumn"; alert(field[j]+"-----field[j]----")
		 * cell.innerHTML = field[j]; row.appendChild( cell ); } } }
		 */

   }


   function generateNoRecordsPunchDetails(tbody,oTable) {
      
       var noRecords =document.createElement("TR");
       noRecords.className="gridRowEven";
       tbody.appendChild(noRecords);
       cell = document.createElement("TD");
       cell.className="gridColumn";
       cell.colSpan = "3";   
       cell.innerHTML = "No Punch Records Found";
       noRecords.appendChild(cell);
   }
   function generateFooterPunchDetails(tbody) {
   	  
       var cell;
       var footer =document.createElement("TR");
       footer.className="gridPager";
       tbody.appendChild(footer);
       cell = document.createElement("TD");
       cell.className="gridFooter";

       // cell.colSpan = "7";
       
       cell.colSpan = "3";
       
       footer.appendChild(cell);

   }
   function calculateAttendance(daysinmonth,daysworked,daysholidays,daysweekend,daysvacation){
        var year = document.getElementById("year").value;
        var month = document.getElementById("month").value;
      // var daysinmonth = document.getElementById("daysinmonth").value;
     // var daysworked = document.getElementById("daysworked").value;
      // var daysproject = document.getElementById("daysproject").value;
      // var daysvacation = document.getElementById("daysvacation").value;
      // var daysweekend = document.getElementById("daysweekend").value;
      // var daysholidays = document.getElementById("daysholidays").value;
        var empId = document.getElementById("empId").value;
       // alert(month)
      // alert(year)
         $.ajax({
           url:'calculateAttendance.action?daysInMonth='+daysinmonth+'&daysWorked='+daysworked+'&daysVacation='+daysvacation+'&daysWeekends='+daysweekend+'&daysHolidays='+daysholidays+'&empId='+empId+'&year='+year+'&month='+month,
           context: document.body,
           success: function(responseText) {
             // alert(responseText)
              document.getElementById('resultMessage').innerHTML="<font style='color:red;font-size:15px;'>Submitted Successfully!!!</font>"
           },
           error: function(e){
                alert("Please try again");
           }
       });
    }





   function updateAttendanceLoad(){
         var date = document.getElementById("date").value;
       var biometricHrs = document.getElementById("biometricHrs").value;
        var statusCode = document.getElementById("status").value;
      // var status = document.getElementById("status").text.value;
        var e = document.getElementById("status");
        var status = e.options[e.selectedIndex].text;
        // var statusHidden = document.getElementById("statusHidden").value;
     // alert(status)
       // alert(strUser)
        var timeSheetHrs = document.getElementById("timeSheetHrs").value;
    // if(statusCode == "Absent"){
// var leaveStatusId = document.getElementById("leaveStatusId").value;
// }else{
// leaveStatusId = "";
// }
        var comments = document.getElementById("comments").value;
        var empId = document.getElementById("empId").value;
         $.ajax({
           url:'updateAttendanceLoad.action?biometricDate='+date+'&statusCode='+statusCode+'&status='+status+'&comments='+comments+'&empId='+empId,
           context: document.body,
           success: function(responseText) {
             // alert(responseText)
            
                 
                  
              document.getElementById('resultMessage').style.display = "block";
              document.getElementById('resultMessage').innerHTML="<font style='color:red;font-size:15px;'>updated Successfully!!!</font>"
           	   getBiometricLoad();
            },
           error: function(e){
              
               alert("Please try again");
           }
       });
   }





   function addSyncAttendanceOverlay(){
       
       var overlay = document.getElementById('addAttendanceOverlay');
       var specialBox = document.getElementById('addAttendanceOverlayBox');
        
          overlay.style.opacity = .8;
       if(overlay.style.display == "block"){
          overlay.style.display = "none";
           specialBox.style.display = "none";
             window.location="getBiometric.action";
      } else {
            overlay.style.display = "block";
           specialBox.style.display = "block";
       }   
       
   }



   function addSynAttendance(){
       var year = document.getElementById('syncYearOverlay').value;
       var month = document.getElementById('syncMonthOverlay').value;
       var comments = document.getElementById('comments').value;
        $.ajax({
           url:'addSynAttendance.action?syncYearOverlay='+year+'&syncMonthOverlay='+month+'&comments='+comments,
           context: document.body,
           success: function(responseText) {
             // alert(responseText)
              document.getElementById('resultMessage').innerHTML="<font style='color:red;font-size:15px;'>Added Successfully!!!</font>"
            },
           error: function(e){
              
               alert("Please try again");
           }
       });

   }

   function syncingAttendanceOverlay(Month,Year){
      
       
       var overlay = document.getElementById('syncAttendanceOverlay');
       var specialBox = document.getElementById('syncAttendanceOverlayBox');
        overlay.style.opacity = .8;
       if(overlay.style.display == "block"){
          // alert(Year)
          overlay.style.display = "none";
           specialBox.style.display = "none";
          
       } else {
         // alert(Month)
           document.getElementById('month').value=Month;
            document.getElementById('year').value=Year;
        overlay.style.display = "block";
           specialBox.style.display = "block";
           
       }   
       
   }



   function getEmpNameByLocation(){
       var location = document.getElementById('locationId').value;
       
          $.ajax({
           url:'getEmpNameByLocation.action?locationId='+location,
           context: document.body,
           success: function(resXML) {
           // alert(resXML)
            var empId = document.getElementById("empnameById");
       var team = resXML.getElementsByTagName("TEAM")[0];
       var users = team.getElementsByTagName("USER");
       empId.innerHTML=" ";
       for(var i=0;i<users.length;i++) {
           var userName = users[i];
           var att = userName.getAttribute("userId");
           var name = userName.firstChild.nodeValue;
           var opt = document.createElement("option");
           opt.setAttribute("value",att);
           opt.appendChild(document.createTextNode(name));
           empId.appendChild(opt);
       }
   },
           error: function(e){
              
               alert("Please try again");
           }
       });
   }

   function syncAttendance(){
       var location = document.getElementById('locationId').value;
// var e = document.getElementById("empnameById");
// var empName = e.options[e.selectedIndex].text;
      var empName = document.getElementById('empnameById').value;
       var month = document.getElementById('month').value;
       var year = document.getElementById('year').value;
          $.ajax({
           url:'syncAttendance.action?locationId='+location+'&empNameById='+empName+'&month='+month+'&year='+year,
           context: document.body,
           success: function(responseMessage) {
          // alert(responseMessage)
    },
           error: function(e){
              
               alert("Please try again");
           }
       });
   }


   function empNameList() {
       var test=document.getElementById("empName").value;
      
       if (test == "") {


           clearTable1();
           hideScrollBar();
           var validationMessage=document.getElementById("authorEmpValidationMessage");
           validationMessage.innerHTML = "";
           document.employeeAddForm.empId.value="";
       // frmSearch issuesForm
       } else {
           if (test.length >2) {
               // alert("CONTENXT_PATH-- >"+CONTENXT_PATH)
               var url = CONTENXT_PATH+"/getEmpFullNames.action?empName="+escape(test);
               var req = initRequest(url);
               req.onreadystatechange = function() {
                   // alert("req-->"+req);
                   if (req.readyState == 4) {
                       if (req.status == 200) {
                           // alert(req.responseXML);
                       
                           parseEmpNames(req.responseXML);
                       } else if (req.status == 204){
                           clearTable1();
                       }
                   }
               };
               req.open("GET", url, true);

               req.send(null);
           }
       }





   }


   function parseEmpNames(responseXML) {
       // alert("-->"+responseXML);
       autorow1 = document.getElementById("menu-popup");
       autorow1.style.display ="none";
       autorow = document.getElementById("menu-popup");
       autorow.style.display ="none";
       clearTable1();
       var employees = responseXML.getElementsByTagName("EMPLOYEES")[0];
       if (employees.childNodes.length > 0) {        
           completeTable.setAttribute("bordercolor", "black");
           completeTable.setAttribute("border", "0");        
       } else {
           clearTable1();
       }
       if(employees.childNodes.length<10) {
           autorow1.style.overflowY = "hidden";
           autorow.style.overflowY = "hidden";        
       }
       else {    
           autorow1.style.overflowY = "scroll";
           autorow.style.overflowY = "scroll";
       }
       
       var employee = employees.childNodes[0];
       var chk=employee.getElementsByTagName("VALID")[0];
       if(chk.childNodes[0].nodeValue =="true") {
           // var
			// validationMessage=document.getElementById("validationMessage");
           var validationMessage;
           
           validationMessage=document.getElementById("authorEmpValidationMessage");
           isPriEmpExist = true;
            
           validationMessage.innerHTML = "";
           document.getElementById("menu-popup").style.display = "block";
           for (loop = 0; loop < employees.childNodes.length; loop++) {
               
               var employee = employees.childNodes[loop];
               var customerName = employee.getElementsByTagName("NAME")[0];
               var empid = employee.getElementsByTagName("EMPID")[0];
               var empno = employee.getElementsByTagName("EMPNO")[0];
               var loginId = employee.getElementsByTagName("LOGINID")[0];
               appendEmpNames(empid.childNodes[0].nodeValue,customerName.childNodes[0].nodeValue,empno.childNodes[0].nodeValue,loginId.childNodes[0].nodeValue);
           }
           var position;
           
           
           position = findPosition(document.getElementById("empName"));
           
           posi = position.split(",");
           document.getElementById("menu-popup").style.left = posi[0]+"px";
           document.getElementById("menu-popup").style.top = (parseInt(posi[1])+20)+"px";
           document.getElementById("menu-popup").style.display = "block";
       } // if
       if(chk.childNodes[0].nodeValue =="false") {
           var validationMessage = '';
         
           isPriEmpExist = false;
           validationMessage=document.getElementById("authorEmpValidationMessage");
       
    
           validationMessage.innerHTML = " Invalid ! Select from Suggesstion List. ";
           validationMessage.style.color = "green";
           validationMessage.style.fontSize = "12px";
          
           document.getElementById("empName").value = "";
           document.getElementById("empId").value = "";
           document.getElementById("loginId").value = "";
               
           
       }

   }

   function appendEmpNames(empId,empName,empNo,loginId) {
       
       var row;
       var nameCell;
       if (!isIE) {
           row = completeTable.insertRow(completeTable.rows.length);
           nameCell = row.insertCell(0);
       } else {
           row = document.createElement("tr");
           nameCell = document.createElement("td");
           row.appendChild(nameCell);
           completeTable.appendChild(row);
       }
       row.className = "popupRow";
       nameCell.setAttribute("bgcolor", "#3E93D4");
       var linkElement = document.createElement("a");
       linkElement.className = "popupItem";
       // if(assignedToType=='pre'){
       linkElement.setAttribute("href", "javascript:set_emp1('"+empName +"','"+ empId +"','"+empNo+"','"+loginId+"')");


       linkElement.appendChild(document.createTextNode(empName));
       linkElement["onclick"] = new Function("hideScrollBar()");
       nameCell.appendChild(linkElement);
   // fillWorkPhone(empId);
   }
   function set_emp1(eName,eID,eNo,loginId){
       clearTable1();
       document.employeeAddForm.empName.value =eName;
       document.employeeAddForm.empId.value =eID;
       if( document.employeeAddForm.empNo.value == ""){
       	document.employeeAddForm.empNo.value = eNo;
       }
       document.employeeAddForm.loginId.value =loginId;
   }
   var isIE;
   function initRequest(url) {
       
       if (window.XMLHttpRequest) {
           return new XMLHttpRequest();
       }
       else
       if (window.ActiveXObject) {
           isIE = true;
           return new ActiveXObject("Microsoft.XMLHTTP");
       }
       
   }
   function clearTable1() {
       if (completeTable) {
           completeTable.setAttribute("bordercolor", "white");
           completeTable.setAttribute("border", "0");
           completeTable.style.visible = false;
           for (loop = completeTable.childNodes.length -1; loop >= 0 ; loop--) {
               completeTable.removeChild(completeTable.childNodes[loop]);
           }
       }






   }


   function hideScrollBar() {
       autorow = document.getElementById("menu-popup");
       autorow.style.display = 'none';
   }
   function findPosition( oElement ) {
       if( typeof( oElement.offsetParent ) != undefined ) {
           for( var posX = 0, posY = 0; oElement; oElement = oElement.offsetParent ) {
               posX += oElement.offsetLeft;
               posY += oElement.offsetTop;
           }
           return posX+","+posY;
       } else {
           return oElement.x+","+oElement.y;
       }




   }
   function isNumeric(element) {

   	var val = element;
   	if (isNaN(val)) {
   		document.getElementById("empNo").value = "";
   		alert('EmpNo must be numeric values');
   		
   		
   	} 
   }
  
   function isNumericYear(element) {
       
	   	var val = element.value;
	   	
	   	if (isNaN(val)) {
	   	
	   		
	   		alert('Year must be numeric values');
	   		document.getElementById('year').value = "";
	   		return false;
	   	} 
	   }
   

   function getEmpNameandId(empNo){
// alert("empNo"+empNo)
   	document.getElementById("empName").value = "";
   	document.getElementById("empId").value = "";
   	if (empNo.length == 4) {
   	 $.ajax({
   	        url:'getEmpNameandId.action?empNo='+empNo,
   	        context: document.body,
   	        success: function(responseMessage) {
   	        // alert(responseMessage)
   	        	var data=responseMessage.split('@#');
   	            
   	            
   	         
   	              // alert("data[3]"+data[0]);
   	                  
   	                document.employeeAddForm.empName.value =data[0];
   	                document.employeeAddForm.empId.value =data[1];
   	                document.employeeAddForm.loginId.value =data[2];
   	                
   	                
   	                 
   	        
   	         // alert(responseMessage)
   	           
   	 },
   	        error: function(e){
   	           
   	            alert("Please try again");
   	        }
   	    });
   	}








   }

   function clearEmpNo(){
   	document.getElementById("empNo").value = "";
   	document.getElementById("empId").value = "";
   }
   function clearEmpName(){
   	document.getElementById("empName").value = "";
   	document.getElementById("loginId").value = "";
   }


   function getAttendanceLoad(month,year)
   {
     
      
       window.location = "generateExcelTimesheets.action?year="+year+"&month="+month;

   }

   
 function addHolidayOverlay(){
	 document.getElementById("headerLabel").style.color="white";
     document.getElementById("headerLabel").innerHTML="Add Holidays";
       var overlay = document.getElementById('addHolidayOverlay');
       var specialBox = document.getElementById('addHolidayOverlayBox');
        
          overlay.style.opacity = .8;
       if(overlay.style.display == "block"){
          overlay.style.display = "none";
           specialBox.style.display = "none";
          
           window.location="getEmpHolidays.action";    
      } else {
    	  
            overlay.style.display = "block";
           specialBox.style.display = "block";
       }   
       
   }


   
   
   function addHolidays(){	
   // alert("hi");
  var holidayDate = document.getElementById("holidayDate").value;
  var description = document.getElementById("description").value;
  var country = document.getElementById("country").value;
  if(holidayDate == "" || holidayDate == null){
		x0p( '','Please select holidayDate from Calender','info');
		return false;
  }
  if(country == "" || country == null){
		x0p( '','Please select country','info');
		return false;
}
  if(description == "" || description == null){
		x0p( '','Please enter Holiday Name','info');
		return false;
}
 
 // alert("date"+holidayDate+"desc"+description+"country"+country)
   	  // document.getElementById("resultMessage").innerHTML='';
   	    // document.getElementById("load").style.display = 'block';
   		      
   		        
   		     $.ajax({
   		    	 url:'addHolidays.action?country='+country+'&description='+description+'&holidayDate='+holidayDate,//
     		        context: document.body,
   	           success: function(responseText) {
   	             // alert(responseText)
   	              document.getElementById('resultMessage').innerHTML="<font style='color:red;font-size:15px;'>Added Successfully!!!</font>"
   	            },
   	           error: function(e){
   	              
   	               alert("Please try again");
   	           }
   	       });
   	

   }

   

   function getEmpHolidaysList(){
	   var year = document.getElementById("year").value;
	   var country = document.getElementById("country").value;
	   var tableId = document.getElementById("tblHolidayList"); 
	   clearTable(tableId);
   	   var req = newXMLHttpRequest();
   	    req.onreadystatechange = function() {
   	        if (req.readyState == 4) {
   	            if (req.status == 200) {      
   	                document.getElementById("loadHolidayList").style.display = 'none';
   	                displayEmpHolidayList(req.responseText);                        
   	            } 
   	        }else {
   	            document.getElementById("loadHolidayList").style.display = 'block';
   	        }
   	    }; 
   	  var url = CONTENXT_PATH+"/getEmpHolidaysList.action?year="+year+"&country="+country;
   	 // var url =
		// CONTENXT_PATH+"/getlookUpReport.action?tableName="+tableName+"&columnName="+columnName;

   	// alert("url====>"+url);
   	    req.open("GET",url,"true");    
   	    req.setRequestHeader("Content-Type","application/x-www-form-urlencoded"); 
   	    req.send(null);
   }



   function displayEmpHolidayList(response){
	  // alert("response"+response)
       var tableId = document.getElementById("tblHolidayList"); 
	   clearTable(tableId);
       var headerFields = new Array("S.No","Date","Country","Holiday Description");
      
       var dataArray = response;
       displayEmpHolidayListGenerateHTML(tableId,dataArray, headerFields);
   }
    
   function displayEmpHolidayListGenerateHTML(oTable,responseString,headerFields) {
       var fieldDelimiter = "#^$";
       var recordDelimiter = "*@!";   
       var records = responseString.split(recordDelimiter); 
       displayEmpHolidayListRows(oTable,headerFields,records,fieldDelimiter);
   }



   function displayEmpHolidayListRows(oTable, headerFields,records,fieldDelimiter) {	

       tbody = oTable.childNodes[0];    
       var tbody = document.createElement("TBODY");
       oTable.appendChild(tbody);
       generateEmpHolidayTableHeader(tbody,headerFields);
       var rowlength;
       rowlength = records.length;
       if(rowlength >0 && records!=""){
           for(var i=0;i<rowlength-1;i++) {
                   
        	   displayEmpHolidayListRows1(i,oTable,tbody,records[i],fieldDelimiter);
           }
           
       } else {
           generateNoRecordsFound(tbody,oTable,headerFields.length);
       }
       generateResourcesByStatusListFooter(tbody,oTable,headerFields.length);
       pagerOption();
   }

   // Santhosh.Kola#^$Miracle Software Systems . Inc(Intrenal
	// Projects)#^$Hubble#^$2012-06-11 00:00:00#^$-#^$90#^$-#^$Shadow#^$
   function displayEmpHolidayListRows1(index,oTable,tableBody,record,delimiter){
       var row;
       var cell;
       var fieldLength;
       // var fields = record.split(delimiter);
       
     
       var fields = record.split("#^$");
       fieldLength = fields.length ;
       var length = fieldLength;
       
           

       row = document.createElement( "TR" );
       row.className="gridRowEven";
   // alert(index+"index%2"+index%2+"index/2"+index/2)

       tableBody.appendChild( row );
       
       if(index%2 == 0){
    	   backgroundColor = '#F5FFFA';
    	 
    		// row.setAttribute("style", "background-color: #ADD8E6;");
    	       }else{
    	    	   backgroundColor = '#FFE4E1' ;
    	    	  
    	    	  // row.setAttribute("style", "background-color: #FFB6C1;");
    	       }  
       
// alert("index"+(index+1))
// cell = document.createElement( "TD" );
// cell.className="gridColumn";
//             
// cell.innerHTML =(index+1) ;
// cell.setAttribute("align","left");
// row.appendChild(cell);
               
       cell = document.createElement( "TD" );
       cell.className="gridColumn";    
     
       cell.innerHTML =fields[0] ;
       cell.style.backgroundColor = backgroundColor;
       cell.setAttribute("align","left");   
       row.appendChild( cell );  
       
    
       cell = document.createElement( "TD" );
       cell.className="gridColumn";    
      
       cell.innerHTML =fields[1] ;
       cell.style.backgroundColor = backgroundColor;
       cell.setAttribute("align","left");   
       row.appendChild( cell );  
               
       cell = document.createElement( "TD" );
       cell.className="gridColumn";    
          
       cell.innerHTML =fields[2] ;
       cell.style.backgroundColor = backgroundColor;
       cell.setAttribute("align","left");   
       row.appendChild( cell );
       
       cell = document.createElement( "TD" );
       cell.className="gridColumn";    
       
       cell.innerHTML =fields[3] ;
       cell.style.backgroundColor = backgroundColor;
       cell.setAttribute("align","left");   
       row.appendChild( cell );
       
       
          
   }

   function generateEmpHolidayTableHeader(tableBody,headerFields) {
       var row;
       var cell;
       row = document.createElement( "TR" );
       row.className="gridHeader";
       tableBody.appendChild( row );
       for (var i=0; i<headerFields.length; i++) {
           cell = document.createElement( "TD" );
           cell.className="gridHeader";
           row.appendChild( cell );

           cell.setAttribute("width","10000px");
           cell.innerHTML = headerFields[i];
       }
   }
   
   
   
   function getRequirementsListVP() {
	    
	    var req = newXMLHttpRequest();
	     req.onreadystatechange = readyStateHandlerText(req, displayReqListResultVP); 
	    
	    var url = CONTENXT_PATH+"/requirementAjaxListVP.action";
	 
	    req.open("GET",url,"true");    
	    req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
	    req.send(null);
	    
	}
  

  function getSearchReqList(){
	   document.getElementById('loadingMessage12').style.display='block';
     status=document.getElementById("status").value;
    // createdBy=document.getElementById("createdBy").value;
     assignedTo=document.getElementById("assignedTo").value;
     title=document.getElementById("title").value;
     postedDate1=document.getElementById("postedDate1").value;
     postedDate2=document.getElementById("postedDate2").value;
  assignedBy=document.getElementById("assignedBy").value;
     country=document.getElementById("country").value;
     practiceid=document.getElementById("practiceId").value;
   requirementId=document.getElementById("requirementId").value;
    state=document.getElementById("state").value;
    preSalesPerson=document.getElementById("preSalesPerson").value;
    clientId=document.getElementById("clientId").value;
    
          var req = newXMLHttpRequest();
       req.onreadystatechange = readyStateHandlerText(req, displayReqListResultVP); 
       var url = CONTENXT_PATH+"/searchRequirementAjaxListVP.action?assignedTo="+assignedTo+"&title="+title+"&postedDate1="+postedDate1+"&postedDate2="+postedDate2+"&status="+status +"&assignedBy="+assignedBy+"&country="+country+"&practiceid="+practiceid+"&requirementId="+requirementId+"&state="+state+"&preSalesPerson="+preSalesPerson+"&clientId="+clientId;
    // alert("url-----"+url)
      req.open("GET",url,"true");    
      req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
      req.send(null);

  }

  function displayReqListResultVP(resText) {
	   document.getElementById('loadingMessage12').style.display='none';
      var oTable = document.getElementById("tblUpdate1");
      ClrDashBordTable(oTable);
      if(resText.length !=0 && resText!="addto0"){
          
          
         
                  var headerFields = new Array("SNo","Req&nbsp;Id","RequirementJobTitle","AccountName","Status","Location","AssignedDate","SubmittedDate","No.of Pos","Resumes Submitted","Recruiter","Pre-Sales");	
          
          tbody = document.createElement("TBODY");
          oTable.appendChild(tbody);
         
          var resTextSplit1 = resText.split("^");

           // generateTableHeader(tbody,headerFields);
             generateTableDynamicHeader(oTable,headerFields);
             
          for(var index=0;index<resTextSplit1.length-1;index++) {
              resTextSplit2 = resTextSplit1[index].split("|");
              
                 generateRowforVPRequirements(tbody,resTextSplit2,index);
          }
          generateFooter(tbody);
          $("#tblUpdate1").tableHeadFixer({
          'left' : 4, 
          'foot' : false, 
          'head' : true
      });
      // pagerOption1();
      }else {
          alert("No Records Found");
      }
  }
  
  
  

  function generateRowforVPRequirements(tableBody,rowFeildsSplit,index){

     var row;
     var cell;
     row = document.createElement("TR");
     row.className="gridRowEven";
     cell = document.createElement("TD");
     cell.className="gridRowEven";
     cell.innerHTML = index+1;
     cell.setAttribute('align','center');
     row.appendChild(cell);
     tableBody.appendChild(row);
  
     for (var i=1; i<=rowFeildsSplit.length-5; i++) {
         cell = document.createElement( "TD" );
         cell.className="gridRowEven";
         row.appendChild(cell);

         if(i==1) {

              cell.innerHTML = " ";

              var j = document.createElement("a");
             j.setAttribute("href", "javascript:getRequirementData('"+rowFeildsSplit[2]+"')");
             j.appendChild(document.createTextNode(rowFeildsSplit[2]));
             cell.appendChild(j);
              
             cell.align = "center";
       
         }else if(i==2){ 

             // job details

              var jobTitle = rowFeildsSplit[4].substring(0,25);
             var j = document.createElement("a");
             j.setAttribute("href", "javascript:getRequirementSkills('"+rowFeildsSplit[2]+"')");
             j.setAttribute("onmouseover","javascript:tooltip.show('"+rowFeildsSplit[4]+"')");
             j.setAttribute("onmouseout","javascript:tooltip.hide();");
             j.appendChild(document.createTextNode(jobTitle+"..."));      
             cell.appendChild(j);


         }else if(i==3){
       	  var jobTitle = rowFeildsSplit[16].substring(0,25);
             var j = document.createElement("a");
            j.setAttribute("onmouseover","Tip('"+rowFeildsSplit[16]+"')");
            j.setAttribute("onmouseout","javascript:UnTip();");
            j.appendChild(document.createTextNode(jobTitle+"..."));      
             cell.appendChild(j);
         }else if(i==5){
             cell.setAttribute('align','center');
             cell.innerHTML = rowFeildsSplit[6];
         }else if(i==4){
                 cell.setAttribute('align','center');
                cell.innerHTML = rowFeildsSplit[7];
         }else if(i==7){
                 var ass_Date = rowFeildsSplit[8].split(" ");

                 if(ass_Date[0] == "null"){
                     cell.innerHTML = " ";
                     cell.setAttribute('align','center');
                 }else{ 
                 cell.innerHTML = ass_Date[0];
                 cell.setAttribute('align','center');
                 }
         }else if(i==6){
                 var sub_date=rowFeildsSplit[9].split(" ");

                 if(sub_date[0] == "null"){
                     cell.setAttribute('align','center');
                     cell.innerHTML = " ";
                 }    
                 else{
                     cell.setAttribute('align','center');
                    cell.innerHTML = sub_date[0];
                 }
         }else if(i==8){
              if(rowFeildsSplit[10] == "0"){
              cell.setAttribute('align','center');
               cell.innerHTML = "1";
             }else{
             cell.setAttribute('align','center');
             cell.innerHTML = rowFeildsSplit[10];
             }
         }else if(i==9){
            
              if(rowFeildsSplit[5]=="-" || rowFeildsSplit[5]=="null"){
              cell.setAttribute('align','center');
                 cell.innerHTML = "0";
             }
             else{
             cell.setAttribute('align','center');
             cell.innerHTML = rowFeildsSplit[1];
             }
         }else if(i==10){

                // recruiter

                if(rowFeildsSplit[11]=="null" || rowFeildsSplit[11]=="" || rowFeildsSplit[11]==" ")
                     cell.innerHTML = "-";
               else{
                    
                     var j = document.createElement("a");
                     j.setAttribute("href", "javascript:getRecruiterDetails('"+rowFeildsSplit[11]+"')");
                     j.appendChild(document.createTextNode(rowFeildsSplit[11]));
                     cell.appendChild(j);
                     // cell.innerHTML = rowFeildsSplit[11];
               }
         }else if(i==11){
               // pre- sales
                 
                 if(rowFeildsSplit[13]=="null" || rowFeildsSplit[13]=="" || rowFeildsSplit[13]==" ")
                     cell.innerHTML = "-";
                 else{ 
                    
                     var j = document.createElement("a");
                     j.setAttribute("href", "javascript:getRecruiterDetails('"+rowFeildsSplit[13]+"')");
                     j.appendChild(document.createTextNode(rowFeildsSplit[13]));
                     cell.appendChild(j);
                     
                         
                 }
         }
         cell.width = 120;
     }
     

 }
  
  

  function generateTableDynamicHeader(tableBody,headerFields) {
      var row;
      var cell;
      // alert("haer");
      var thead=document.createElement("thead");
      tableBody.appendChild(thead);
      row = document.createElement( "TR" );
      row.className="dashBoardgridHeader";
      thead.appendChild(row);
      
      for (var i=0; i<headerFields.length; i++) {
          cell = document.createElement( "TH" );
          cell.className="dashBoardgridHeader";
          row.appendChild( cell );
          cell.innerHTML = headerFields[i];
          cell.width = 120;
      }
  }

  
  

  function getRequirementData(requirementId) {
     document.location="/Hubble/employee/getRequirement.action?objectId="+requirementId+"&requirementAdminFlag=";
 }
  
  

  function getRecruiterDetails(Recruiter) {
      
      var test = Recruiter;
      // alert(test.substr(0,1));
      // alert("test --"+test);
      // alert(indexOf("."));
      // alert(test.substr(test.indexOf(".")+1,test.length));
      // var
		// loginId=(test.substr(0,1)+test.substr(test.indexOf(".")+1,test.length));
        var loginId=Recruiter;
      // alert("loginId"+loginId);
      var aId = Recruiter;
      var req = newXMLHttpRequest();
      req.onreadystatechange = readyStateHandlerText(req,populateReqPersonSkills);    
      // var url =
		// CONTENXT_PATH+"/AjaxHandlerServlet?from=gridAjax&activityId="+aId;
      var url=CONTENXT_PATH+"/popupReqRecruiterWindow.action?recruiterId="+loginId;
      req.open("GET",url,"true");
      req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
      req.send(null);
  }


  function populateReqPersonSkills(text) {
      var background = "#3E93D4";
      var title = "Employee Details";
      var text1 = text; 
      var size = text1.length;
      
      // alert("text "+text1);
      
      // Now create the HTML code that is required to make the popup
      var content = "<html><head><title>"+title+"</title></head>\
      <body bgcolor='"+background +"' style='color:white;'>"+text1+"<br />\
      </body></html>";
      
      // alert("text1"+text1);
      // alert("size "+content.length);
      var indexof=(content.indexOf("^")+1);
      var lastindexof=(content.lastIndexOf("^"));
      
      popup = window.open("","window","channelmode=0,width=300,height=150,top=200,left=350,scrollbars=no,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no");    
      if(content.indexOf("^")){
          // alert(content.substr(0,content.indexOf("//")));
          popup.document.write("<b>Employee Name : </b>"+content.substr(0,content.indexOf("^")));
          popup.document.write("<br><br>");
          popup.document.write("<b>Work Phone No :</b>"+content.substr((content.indexOf("^")+1),(lastindexof-indexof)));
          popup.document.write("<br><br>");
          popup.document.write("<b>Email Address :</b>"+content.substr((content.lastIndexOf("^")+1),content.length));
      }// Write content into it.
      // Write content into it.
      
      
      
  }

  
  
  function loadExpenses(empId){
		if(empId != 0 && empId > 2){
				getExpencesDetails(empId)
			}
	}

	function getExpencesDetails(empId){
		 // alert(empId)
		// var req = newXMLHttpRequest();
		// req.onreadystatechange = readyStateHandlerXml(req,
		// populateExpencesDetails);
		var url = CONTENXT_PATH+"/getExpencesDetails.action?empId="+empId;
		 var req = initRequest(url);
	     req.onreadystatechange = function() {
	         if (req.readyState == 4) {
	             if (req.status == 200) {
	            	 populateExpencesDetails(req.responseXML);
	             }
	         }
	     };
	     req.open("GET", url, true);
	     req.send(null);
		
	}
	function populateExpencesDetails(responseXML){
		 var accountNames = responseXML.getElementsByTagName("EMPLOYEES")[0];
		   // alert("test"+responseXML);
		    
		    var employee = accountNames.childNodes[0];
		    var chk=employee.getElementsByTagName("VALID")[0];
		    // alert("Before If");
		    if(chk.childNodes[0].nodeValue =="true") {
		    	  for (loop = 0; loop < accountNames.childNodes.length; loop++) {
		    		  var employee = accountNames.childNodes[loop];
		    		  var Accommodation = employee.getElementsByTagName("Accommodation")[0];
		    		  if(Accommodation==null || Accommodation == " "){
		    			  Accommodation = "";
		    		  }
		              var RoomNo = employee.getElementsByTagName("RoomNo")[0];
		              var RoomFee = employee.getElementsByTagName("RoomFee")[0];
		              var Cafeteria = employee.getElementsByTagName("Cafeteria")[0];
		              if(Cafeteria==null || Cafeteria == " "){
		            	  Cafeteria = "";
		    		  }
		              var CafeteriaFee = employee.getElementsByTagName("CafeteriaFee")[0];
		              var Transportation = employee.getElementsByTagName("Transportation")[0];
		              if(Transportation==null || Transportation == " "){
		            	  Transportation = "";
		    		  }
		              var TransportLocation = employee.getElementsByTagName("TransportLocation")[0];
		            // alert("hiiiii")
		              if(TransportLocation==null || TransportLocation == " "){
		            	  TransportLocation = "";
		    		  }
		              var TransportFee = employee.getElementsByTagName("TransportFee")[0];
		              var OccupancyType = employee.getElementsByTagName("OccupancyType")[0];
		              if(OccupancyType==null || OccupancyType == " "){
		            	  OccupancyType = "";
		    		  }
		              var DateOfOccupancy = employee.getElementsByTagName("DateOfOccupancy")[0];
		              if(DateOfOccupancy==null || DateOfOccupancy == " "){
		            	  DateOfOccupancy = "";
		    		  }
		              var ElectricalCharges = employee.getElementsByTagName("ElectricalCharges")[0];
		              var FromLocation = employee.getElementsByTagName("FromLocation")[0];
		              if(FromLocation==null || FromLocation == " "){
		            	  FromLocation = "";
		    		  }
		    	  }
		    	  appendExpencesDetails(Accommodation.childNodes[0].nodeValue,RoomNo.childNodes[0].nodeValue,RoomFee.childNodes[0].nodeValue,
		    				Cafeteria.childNodes[0].nodeValue,CafeteriaFee.childNodes[0].nodeValue,Transportation.childNodes[0].nodeValue,
		    				TransportLocation.childNodes[0].nodeValue,TransportFee.childNodes[0].nodeValue,OccupancyType.childNodes[0].nodeValue,
		    				DateOfOccupancy.childNodes[0].nodeValue,ElectricalCharges.childNodes[0].nodeValue,FromLocation.childNodes[0].nodeValue);
		    	  
		    	  
		    }
		   }
	function appendExpencesDetails(Accommodation,RoomNo,RoomFee,Cafeteria,CafeteriaFee,Transportation,TransportLocation,TransportFee,OccupancyType,DateOfOccupancy,ElectricalCharges,FromLocation){
// alert(Transportation+"Cafeteria"+ElectricalCharges+"FromLocation"+FromLocation+"accomodation"+OccupancyType+"-->"+DateOfOccupancy)
		document.getElementById("homeState").value=Accommodation;
		    document.getElementById("roomNo").value=RoomNo;
		    document.getElementById("roomFee").value=RoomFee;
		    
		   // document.getElementById("cafeteria").value='Yes';

		 // alert("test"+Accommodation);
		    $("input[name='cafeteria'][value='"+Cafeteria+"']").attr("checked", true);


		   // $("input[name='cafeteria']:checked").val() = 'Yes';
		    document.getElementById("cafeteriaFee").value=CafeteriaFee;
		   // document.getElementById("transportation").value=Transportation;
		    $("input[name='transportation'][value='"+Transportation+"']").attr("checked", true);
		    document.getElementById("transportLocation").value=TransportLocation;
		    document.getElementById("transportFee").value=TransportFee;
		    document.getElementById("occupancyType").value=OccupancyType;
		    document.getElementById("dateOfOccupancy").value=DateOfOccupancy;
		    document.getElementById("electricalCharges").value=ElectricalCharges;
		   // document.getElementById("fromLocation").value=FromLocation;
	}

	function getNeedTransport(toLocation){
		 if(toLocation == "Miracle Heights"){
		     	$("#needTrans").show();
		     	
		     }
			 else{
					$("#needTrans").hide();
			         
			 }
	}

	
	


	function getCertificationsOverlay(){
		document.getElementById("reviewStartDate").value='';
		document.getElementById("reviewEndDate").value='';
	    var overlay = document.getElementById('empCertificationsOverlay');
	    var specialBox = document.getElementById('empCertificationsSpecialBox');
	           
	    overlay.style.opacity = .8;
	    if(overlay.style.display == "block"){
	        overlay.style.display = "none";
	        specialBox.style.display = "none";
	       
	    } else {
	       
	        overlay.style.display = "block";
	        specialBox.style.display = "block";
	        employeeCertificationDetails();
	    }
	}



	function employeeCertificationDetails()
	{
	    var startDate=document.getElementById("reviewStartDate").value;
	    var endDate=document.getElementById("reviewEndDate").value;
	    var loginId=document.getElementById("loginId").value;
	   
	    ClrTable(document.getElementById("tblEmpCertificationDetails"));
		  document.getElementById('loadingMessage1').style.display='block';
	    var req = newXMLHttpRequest();
	    req.onreadystatechange = readyStateHandlerreq(req, employeeCertificationDetailsResponse); 

	    // var url =
		// CONTENXT_PATH+"/searchpmoActivityAjaxList.action?customerName="+NAME+"&projectName="+ProjectName+"&status="+status+"&projectStartDate="+ProjectStartDate;
	    var url = CONTENXT_PATH+"/getEmployeeCertificationDetails.action?startDate="+startDate+"&endDate="+endDate+"&loginId="+loginId;

	    req.open("GET",url,"true");    
	    req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
	    req.send(null);
	 

	}




	function employeeCertificationDetailsResponse(res){
	 document.getElementById('loadingMessage1').style.display='none';
		 var tableId = document.getElementById("tblEmpCertificationDetails");
		    // var headerFields = new
			// Array("SNo","Project&nbsp;Name","StartDate","EndDate","EmpStatus","Utilization","Resource&nbsp;Type");
		    var headerFields = new Array("SNo","Review&nbsp;Name","ReviewDate","Review&nbsp;Comments","Download");
		   
		    ParseAndGenerateHTML(tableId,res, headerFields);
	}


	function getReviewDescription(reviewId) {
			var url = CONTENXT_PATH + "/getReviewDescription.action?id="+reviewId
				var req = initRequest(url);
				req.onreadystatechange = function() {
					if (req.readyState == 4) {
						if (req.status == 200) {
							reviewDescriptionPoPUp(req.responseText);
						} else if (req.status == 204) {
							clearTable1();
						}
					}
				};
				req.open("GET", url, true);

				req.send(null);
			
		
	}
	

	function reviewDescriptionPoPUp(comments) {
	    var background = "#3E93D4";
	    var title = "Comments";
	    // var text1 = text;
	    // var size = text1.length;
	      if(comments=='null' || comments==''){
	    	  comments="-"; 
	      }
	    var size = comments.length;
	    
	    // Now create the HTML code that is required to make the popup
	    var content = "<html><head><title>"+title+"</title></head>\
	    <body bgcolor='"+background +"' style='color:white;'><h4>"+title+"</h4>"+comments+"<br />\
	    </body></html>";
	    
	    if(size < 50){
	        // Create the popup
	        popup = window.open("","window","channelmode=0,width=300,height=150,top=200,left=350,scrollbars=no,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no");    
	        popup.document.write(content); // Write content into it.
	    }
	    
	    else if(size < 100){
	        // Create the popup
	        popup = window.open("","window","channelmode=0,width=400,height=200,top=200,left=350,scrollbars=no,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no");    
	        popup.document.write(content); // Write content into it.
	    }
	    
	    else if(size < 260){
	        // Create the popup
	        popup = window.open("","window","channelmode=0,width=400,height=200,top=200,left=350,scrollbars=no,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no");    
	        popup.document.write(content); // Write content into it.
	    } else {
	        // Create the popup
	        popup = window.open("","window","channelmode=0,width=400,height=200,top=200,left=350,scrollbars=no,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no");    
	        popup.document.write(content); // Write content into it.
	    }
	    
	}
	
	function getDownloadAttachment(id){
	window.location = "Reviews/download.action?reviewId="+id;
}
	
	/*
	 * Naga Lakshmi Telluri 6/04/2018
	 */
	
	
	function  getQReviewPendingListByManagers(){
		// alert("helooooo");
		
	    $('span.pagination').empty().remove();
	    var yearForManagersReport=document.getElementById("yearForManagersReport").value;
	    var quarterlyForManagersReport=document.getElementById("quarterlyForManagersReport").value;
	    var quarterlyLivingCountry=document.getElementById("quarterlyLivingCountry").value;
	    var isIncludeTeam=document.getElementById("isIncludeTeam").checked;
	   // var opsContactId=document.getElementById("opsContactId").value;
	    var ReportsToField=document.getElementById("ReportsToField").value;
	    var practiceName=document.getElementById("practiceName").value;
	    
	    // alert("opsContactId" +opsContactId);
	   // alert("ReportsToField" +ReportsToField);
	   // alert("practiceName" +practiceName);
	    
	    // alert("isIncludeTeam---"+isIncludeTeam+"--quarterlyLivingCountry--"+quarterlyLivingCountry);

	    if(yearForManagersReport.trim().length==0){
	        alert("please Enter Year");
	        return false;
	    }
	    if(quarterlyForManagersReport.trim()==''){
	        alert("please select Quarter");
	        return false;
	    }
	    var tableId = document.getElementById("tblQReviewManagerDashBoard");
	    ClrDashBordTable(tableId);
	    // var startDate=document.getElementById('startDateSummaryGraph').value;
	    // var endDate=document.getElementById('endDateSummaryGraph').value;
	  document.getElementById('loadingForManagersReport').style.display='block';
	    
	    var myObj = {};
	    myObj["yearForManagersReport"]=yearForManagersReport;
	    myObj["quarterlyForManagersReport"]=quarterlyForManagersReport;
	    myObj["quarterlyLivingCountry"]=quarterlyLivingCountry;
	    myObj["isIncludeTeam"]=isIncludeTeam;
	    myObj["ReportsToField"]=ReportsToField;
	    myObj["practiceName"]=practiceName;
	    var json = JSON.stringify(myObj);
	    $.ajax({
	   	url:'getQReviewPendingListByManagers.action',
	        data:{qReviewMnagersDetailsJson: json},
	        contentType: 'application/json',
	        type: 'GET',
	        context: document.body,
	        success: function(responseText) {
	       // alert("responseText"+responseText);
	       
	        	displayQReviewPendingDetailsByManager(responseText);   
	       document.getElementById('loadingForManagersReport').style.display='none';
	       $("#tblQReviewManagerDashBoard").tableHeadFixer({
	           'left' : 3, 
	           'foot' : false, 
	           'head' : true
	       });
	        },
	        error: function(e){
	        	  alert("Please try again");
	        }
	    });
	}

	function displayQReviewPendingDetailsByManager(response){
	    var tableId = document.getElementById("tblQReviewManagerDashBoard");  
	    var headerFields = new Array("S.No","Manager&nbsp;Name","Total&nbsp;Resources","NotEntered","Submitted<br>Pending","MgrApproval<br>Pending","OperationTeam<br>Pending","MgrRejected","HrRejected","Closed");
	   
	    var dataArray = response;
	    qReviewPendingDetailsByManagerParseAndGenerateHTML(tableId,dataArray, headerFields);
	}
	
	function qReviewPendingDetailsByManagerParseAndGenerateHTML(oTable,responseString,headerFields) {
	    var fieldDelimiter = "#^$";
	    var recordDelimiter = "*@!";   
	    var records = responseString.split(recordDelimiter); 
	    generateQReviewPendingDetailsByManagerRows(oTable,headerFields,records,fieldDelimiter);
	}

	function generateQReviewPendingDetailsByManagerRows(oTable, headerFields,records,fieldDelimiter) {	
		// alert("records---"+records);

	    var tbody = oTable.childNodes[0];    
	    tbody = document.createElement("TBODY");
	    oTable.appendChild(tbody);
	    generateTableHeaderForManagerQReview(oTable,headerFields);
	    var rowlength;
	    rowlength = records.length;
	    if(rowlength >0 && records!=""){
	        for(var i=0;i<rowlength-1;i++) {
	                
	            generateQReviewPendingDetailsByManager(oTable,tbody,records[i],fieldDelimiter);
	        }
	        
	    } else {
	        generateNoRecords(tbody,oTable);
	    }
	    generateFooterForMangerQReview(tbody);
	    pagerOptionForManagerGrid();
	}

	
	function generateQReviewPendingDetailsByManager(oTable,tableBody,record,delimiter){
	    // alert("record--"+record);
	    var row;
	    var cell;
	    var fieldLength;
	    // var fields = record.split(delimiter);
	    var fields = record.split("#^$");
	    fieldLength = fields.length ;
	    var length = fieldLength;
	    
	    row = document.createElement( "TR" );
	    row.className="gridRowEven";
	    tableBody.appendChild( row );
	    var yearForManagersReport=document.getElementById("yearForManagersReport").value;
	    var quarterlyForManagersReport=document.getElementById("quarterlyForManagersReport").value;
	    var country=document.getElementById("quarterlyLivingCountry").value;
	    var isIncludeTeam=document.getElementById("isIncludeTeam").checked;

	  for (var i=0;i<length-1;i++) {
	            if(i==3 && fields[3]>0){
	               cell = document.createElement( "TD" );
	            cell.className="gridColumn"; 
	            var j = document.createElement("a");
	            cell.innerHTML = "<a href='javascript:getEmployeePendingDetailsByAppraisalStatus(\""+yearForManagersReport+"\",\""+quarterlyForManagersReport+"\",\""+country+"\",\""+isIncludeTeam+"\",\""+fields[10]+"\",\"NotEntered\",\""+fields[1]+"\")'>"+fields[3]+"</a>";
	      
	            cell.appendChild(j);    
	           }else if(i==4 && fields[4]>0){
	               cell = document.createElement( "TD" );
	            cell.className="gridColumn"; 
	            var j = document.createElement("a");
	            cell.innerHTML = "<a href='javascript:getEmployeePendingDetailsByAppraisalStatus(\""+yearForManagersReport+"\",\""+quarterlyForManagersReport+"\",\""+country+"\",\""+isIncludeTeam+"\",\""+fields[10]+"\",\"NotSubmitted\",\""+fields[1]+"\")'>"+fields[4]+"</a>";
	      
	            cell.appendChild(j);    
	           } else if(i==5 && fields[5]>0){
	               cell = document.createElement( "TD" );
	            cell.className="gridColumn"; 
	            var j = document.createElement("a");
	            cell.innerHTML = "<a href='javascript:getEmployeePendingDetailsByAppraisalStatus(\""+yearForManagersReport+"\",\""+quarterlyForManagersReport+"\",\""+country+"\",\""+isIncludeTeam+"\",\""+fields[10]+"\",\"Submitted\",\""+fields[1]+"\")'>"+fields[5]+"</a>";
	      
	            cell.appendChild(j);    
	           } else if(i==6 && fields[6]>0){
	               cell = document.createElement( "TD" );
	            cell.className="gridColumn"; 
	            var j = document.createElement("a");
	            cell.innerHTML = "<a href='javascript:getEmployeePendingDetailsByAppraisalStatus(\""+yearForManagersReport+"\",\""+quarterlyForManagersReport+"\",\""+country+"\",\""+isIncludeTeam+"\",\""+fields[10]+"\",\"Approved\",\""+fields[1]+"\")'>"+fields[6]+"</a>";
	      
	            cell.appendChild(j);    
	           } else if(i==7 && fields[7]>0){
	               cell = document.createElement( "TD" );
	            cell.className="gridColumn"; 
	            var j = document.createElement("a");
	            cell.innerHTML = "<a href='javascript:getEmployeePendingDetailsByAppraisalStatus(\""+yearForManagersReport+"\",\""+quarterlyForManagersReport+"\",\""+country+"\",\""+isIncludeTeam+"\",\""+fields[10]+"\",\"MgrRejected\",\""+fields[1]+"\")'>"+fields[7]+"</a>";
	      
	            cell.appendChild(j);    
	           }  else if(i==8 && fields[8]>0){
	               cell = document.createElement( "TD" );
	            cell.className="gridColumn"; 
	            var j = document.createElement("a");
	            cell.innerHTML = "<a href='javascript:getEmployeePendingDetailsByAppraisalStatus(\""+yearForManagersReport+"\",\""+quarterlyForManagersReport+"\",\""+country+"\",\""+isIncludeTeam+"\",\""+fields[10]+"\",\"HrRejected\",\""+fields[1]+"\")'>"+fields[8]+"</a>";
	      
	            cell.appendChild(j);    
	           }else if(i==9 && fields[9]>0){
	               cell = document.createElement( "TD" );
	            cell.className="gridColumn"; 
	            var j = document.createElement("a");
	            cell.innerHTML = "<a href='javascript:getEmployeePendingDetailsByAppraisalStatus(\""+yearForManagersReport+"\",\""+quarterlyForManagersReport+"\",\""+country+"\",\""+isIncludeTeam+"\",\""+fields[10]+"\",\"Closed\",\""+fields[1]+"\")'>"+fields[9]+"</a>";
	      
	            cell.appendChild(j);    
	           }else{
	              cell = document.createElement( "TD" );
	              cell.className="gridColumn";       
	       
	                  cell.setAttribute("align","center");
	            
	                cell.innerHTML = fields[i];   
	         }  
	           
	            if(fields[i]!=''){
	            if(i==1)
	            {
	                cell.setAttribute("align","left");
	            }
	            else
	            {
	                cell.setAttribute("align","center");     
	            }
	            row.appendChild( cell );
	        }
	           
	           
	        } 
	    
	}
	
	function getEmployeePendingDetailsByAppraisalStatus(year,quarter,country,isIncludeTeam,mgrLoginId,status,empName){
	   // var opsContactId=document.getElementById("opsContactId").value;
	    var resourceName=document.getElementById("ReportsToField").value;
	    var practiceId=document.getElementById("practiceName").value;
	    
	   window.location=CONTENXT_PATH+"/employee/appraisal/getEmployeePendingDetailsByAppraisalStatus.action?loginId="+mgrLoginId+"&year="+year+"&quarterly="+quarter+"&status="+status+
	   "&empName="+empName+"&country="+country+"&isIncludeTeam="+isIncludeTeam+
	   "&resourceName="+resourceName+"&practiceId="+practiceId;
	   
	}
	
	function getEmployeesForPendingMngrByApprasailStatus(){
	     var tableId = document.getElementById("tblEmpDetailsByAppraisalStatus");  
	   ClrTable(tableId);
	   
	    
	      var year=document.getElementById("year").value;
	    var loginId=document.getElementById("loginId").value;
	    var status=document.getElementById("status").value;
	    var quarterly=document.getElementById("quarterly").value;
	    var country=document.getElementById("country").value;
	    var isIncludeTeam=document.getElementById("isIncludeTeam").value;
	    
	   
	     document.getElementById("loadingMessage").style.display = 'block';
	  
	   
	     var req = newXMLHttpRequest();
	    req.onreadystatechange = readyStateHandlerText(req,displayEmployeesForMngrByApprasailStatus); 
	    var url = CONTENXT_PATH+"/getEmployeesForPendingMngrByApprasailStatus.action?loginId="+loginId+"&year="+year+"&quarter="+quarterly+"&status="+status+"&country="+country+"&isIncludeTeam="+isIncludeTeam;
	    // alert(url);
	    req.open("GET",url,"true");    
	    req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
	    req.send(null);
	}


	
	function getEmployeePrjDetails(){
		  var loginId  = document.getElementById('loginId').value;
		  var req = newXMLHttpRequest();
			req.onreadystatechange = readyStateHandlerText(req, empExpTenureReports);
			var url = CONTENXT_PATH + "/popupTenurStatus.action?loginId=" + loginId
					+ "";
			req.open("GET", url, "true");
			req.setRequestHeader("Content-Type", "application/x-www-form-urlencodetd");
			req.send(null);
	}
	
	function empExpTenureReports(response) {
		var tableId = document.getElementById("tblExpReports");
		clearTable(tableId);
		var headerFields = new Array("Status", "Tenure", "Details");
		var dataArray = response;

		// generateTableHeader(tableId,headerFields)
		empExpTenureReportsGenerate(tableId, dataArray, headerFields);

		document.getElementById("headerLabel2").style.color = "white";
		document.getElementById("headerLabel2").innerHTML = "Tenure Details";

		var overlay = document.getElementById('overlayExperienceReport');
		var specialBox = document.getElementById('specialBoxExperienceReport');
		overlay.style.opacity = .8;
		if (overlay.style.display == "block") {
			overlay.style.display = "none";
			specialBox.style.display = "none";
		} else {
			overlay.style.display = "block";
			specialBox.style.display = "block";
		}

	}
	
	
	function empExpTenureReportsGenerate(oTable, responseString, headerFields) {
		var records = responseString.split("*@!");
		// alert("records---->"+records);

		var tbody = oTable.childNodes[0];
		tbody = document.createElement("TBODY");
		oTable.appendChild(tbody);
		generateTableHeaderEmpExp(tbody, headerFields);
		var rowlength;

		rowlength = records.length;

		for (var i = 0; i < rowlength - 1; i++) {

			var loop = records[i].split("#^$");

			var row = document.createElement("TR");
			row.className = "gridRowEven";
			tbody.appendChild(row);
			var cell = document.createElement("TD");
			cell.className = "gridColumn";
			var str = loop[0] + '';
			if (loop[2] != '-') {
				str = str + '<br>(' + loop[2] + ')';
			}
			if (loop[3] != '-') {
				str = str + '<br>(' + loop[3] + ')';
			}
			if (loop[4] != '-') {
				str = str + '<br>(' + loop[4] + ')';
			}
			if (loop[5] != '-') {
				str = str + '<br>(' + loop[5] + ')';
			}
			if (loop[6] != '-') {
				str = str + '<br>(' + loop[6] + ')';
			}
			cell.innerHTML = str;
			cell.setAttribute("align", "left");
			row.appendChild(cell);

			var cell = document.createElement("TD");
			cell.className = "gridColumn";
			cell.innerHTML = loop[7];
			cell.setAttribute("align", "left");
			row.appendChild(cell);

			var cell1 = document.createElement("TD");
			cell1.className = "gridColumn";

			if (loop[0] == "OnProject") {

				var content = "<table align='center' width='100%' cellpadding='1' cellspacing='1' border='0' class='gridTable'><tr class='gridHeader1' style='background:#A9A9A9 url(../images/DBGrid/bg_table_heading.jpg) repeat top left'><th>From Date</th><th>ToDate</th><th>ResType</th><th>Project Status</th>"
						+ "<th>IsBillable</th><th>Utilization</th><th>Duration</th></tr>";
				var loop1 = loop[1].split("##");
				// /alert("loop1-----"+loop[1]);
				for (var j = 0; j < loop1.length - 1; j++) {

					var loop2 = loop1[j].split("$$");
					content = content + "<tr class='gridRowEven'><td>" + loop2[0]
							+ "</td><td>" + loop2[1] + "</td><td>" + loop2[2]
							+ "  - (" + loop2[3] + ")</td><td>" + loop2[4]
							+ "</td><td>" + loop2[5] + "</td><td>" + loop2[6]
							+ "</td><td>" + loop2[7] + "</td></tr>";

				}

				content = content + "</table>";
				cell1.innerHTML = content;
			} else if (loop[0] == "Training") {
				var content = "<table align='center' width='100%' cellpadding='1' cellspacing='1' border='0' class='gridTable'><tr class='gridHeader1' style='background:#A9A9A9 url(../images/DBGrid/bg_table_heading.jpg) repeat top left'><th>From Date</th><th>ToDate</th><th width='100px'>Skills</th><th>Duration</th></tr>";

				var loop1 = loop[1].split("##");
				// alert("loop1-----"+loop[1]);
				for (var j = 0; j < loop1.length - 1; j++) {

					var loop2 = loop1[j].split("$$");
					content = content + "<tr class='gridRowEven'><td>" + loop2[0]
							+ "</td><td>" + loop2[1] + "</td><td>" + loop2[2]
							+ "</td><td>" + loop2[3] + "</td></tr>";

				}
				content = content + "</table>";

				cell1.innerHTML = content;
			} else {

				cell1.innerHTML = "";
			}

			// /////////////////////////////////////

			cell1.setAttribute("align", "center");
			row.appendChild(cell1);

		}

	}
	
	
	function generateTableHeaderEmpExp(tableBody, headerFields) {
		var row;
		var cell;
		row = document.createElement("TR");
		row.className = "gridHeader";
		tableBody.appendChild(row);
		for (var i = 0; i < headerFields.length; i++) {
			cell = document.createElement("TD");
			cell.className = "gridHeader";
			row.appendChild(cell);

			cell.setAttribute("width", "10000px");
			cell.setAttribute("align", "left");
			cell.innerHTML = headerFields[i];
		}
	}

	function toggleCloseUploadOverlayEmp() {
		var overlay = document.getElementById('overlayExperienceReport');
		var specialBox = document.getElementById('specialBoxExperienceReport');

		overlay.style.opacity = .8;
		if (overlay.style.display == "block") {
			overlay.style.display = "none";
			specialBox.style.display = "none";
		} else {
			overlay.style.display = "block";
			specialBox.style.display = "block";
		}
		// window.location="empSearchAll.action";
	}
	
	function transferTypeDetails(transferType){
		// alert("transferType"+transferType);
		
		var toLocationnew=document.getElementById('toLocationnew').value;
		
		 if(transferType == "CountryTransfer"){
		     	$("#newTrans").show();
		     	$("#oldTrans").hide();
		    	$("#NewreportedTr").show();
		    	
		    	
		    	 $("#reportedTr").hide();
		    	 $("#dueRemarks").hide();
		    	 $("#needTrans").hide();
		    	 $("#itTeamTr").hide();
		    	 $("#itTeamTr1").hide();
		    	 
		    	 
		    	 $("#mcityexp").hide();
		    	 $("#mcityexp1").hide();
		    	 $("#mcityexp2").hide();
		    	 $("#mcityexp3").hide();
		    	 $("#mirexp").hide();
		    	 $("#mirexp1").hide();
		    	 
		    	 $("#mcityDues").hide();
		    	 $("#mcityDues1").hide();
		    	 $("#otherDue").hide();
		    	 $("#dueRemarks").hide();
				    
		    	 
		    	 
		    	 
		     } else if(transferType == ""){
					$("#newTrans").hide();
					$("#oldTrans").show();
					$("#NewreportedTr").hide();
					  $("#reportedTr").show();
					  
					  $("#dueRemarks").show();
				    	 $("#needTrans").show();
				    	 $("#itTeamTr").show();
				    	 $("#itTeamTr1").show();
				    	 
				    	
			 }
			 else{
					$("#newTrans").hide();
					$("#oldTrans").show();
					$("#NewreportedTr").hide();
					  $("#reportedTr").show(); 
					  
					  $("#dueRemarks").show();
				    	 $("#needTrans").show();
				    	 $("#itTeamTr").show();
				    	 $("#itTeamTr1").show();
				    	 
				    
				    	 
			 }
	}
	
	
	function transferTypeDetails1(transferType,toLocationnew)
	{
		if(transferType == "CountryTransfer" && toLocationnew == 'India') 
			{
			  $("#reportedTr").show(); 
			  $("#NewreportedTr").hide();
			}else if(transferType == "OffshoreTransfer")
				{
				  $("#reportedTr").show(); 
				  $("#NewreportedTr").hide();
				}
				else
				{
				$("#NewreportedTr").show();
				  $("#reportedTr").hide(); 
				}
	}
	
	function getNewReportedTo(tolocation)
	
	{
	// alert("tolocation"+tolocation);
	
	if(tolocation == "USA"){
		
	  	   $("#reportedTr").hide();
	    	$("#NewreportedTr").show();
	     	
	     }else if(tolocation == ""){
	    	 $("#reportedTr").show();
			 $("#NewreportedTr").hide();
		         
		 }
		 else{
			 $("#reportedTr").show();
			 $("#NewreportedTr").hide();
		         
		 }
	}
	function getLunchBoxData(){
		// alert("in lunch box");
		$("#addLoadMessage").show();
		$("#search").attr("disabled", true);
		var tableId = document.getElementById("lunchboxTable");
		ClrTable(tableId);
		
		var fname=document.getElementById("fname").value;
		var lname=document.getElementById("lname").value;
		var email=document.getElementById("email").value;
	    var Status=document.getElementById("status").value;
	   // var location=document.getElementById("location").value;
	    var eventLocation=document.getElementById("eventLocation").value;
	    
	   // alert("Status==>"+Status);
	    var myObj={};
	    myObj["FName"]=fname;
	    myObj["LName"]=lname;
	    
	    myObj["Status"]=Status;
	    myObj["Email"]=email;
	   // myObj["Location"]=location;
	    myObj["eventLocation"]=eventLocation;
	   
	    var json = JSON.stringify(myObj);
		
		$.ajax({
		
			url:CONTENXT_PATH+"/getLunchBoxData.action",
			data:{jsonData: json},
			contentType: 'application/json',
			type: 'GET',
			context: document.body,
			success: function(responseText) {

				var json = $.parseJSON(responseText);

				var data=json;

				var headerFields = new Array("Sno","Name","Work&nbsp;Phone","EmailId","Event&nbsp;Location","Status");


				tbody = tableId.childNodes[0];    
				tbody = document.createElement("TBODY");
				tableId.appendChild(tbody);
				generateTableHeader(tbody,headerFields);
				var row;

				if(data.length>0){
					for (var i = 0; i < data.length; i++) {
						var rowData=data[i];

						row = document.createElement( "TR" );
						row.className="gridRowEven";
						tbody.appendChild( row );
						
						cell = document.createElement( "TD" );
						cell.className="gridColumn";       
						cell.innerHTML = i+1;  
						cell.setAttribute("align","left");
						row.appendChild( cell ); 	

							   
						cell = document.createElement( "TD" );
						cell.className="gridColumn";       
						cell.innerHTML = "<a style='color:#C00067' href='javascript:loadlunchBoxDataOverlay("+rowData['Id']+",\""+rowData["FirstName"]+"\"," +
								"\""+rowData["LastName"]+"\",\""+rowData["EmailId"]+"\",\""+rowData["PhoneNumber"]+"\",\""+rowData["FoodPreference"]+"\"," +
										"\""+rowData["Company"]+"\",\""+rowData["JobTitle"]+"\",\""+rowData["Status"]+"\",\""+rowData["EventLocation"]+"\");'>"+rowData["FirstName"]+"&nbsp;"+rowData["LastName"]+"</a>";
						cell.setAttribute("align","left");
						row.appendChild( cell );

						cell = document.createElement( "TD" );
						cell.className="gridColumn";       
						cell.innerHTML = rowData["PhoneNumber"];  
						cell.setAttribute("align","left");
						row.appendChild( cell );

						cell = document.createElement( "TD" );
						cell.className="gridColumn";       
						cell.innerHTML = rowData["EmailId"];  
						cell.setAttribute("align","left");
						row.appendChild( cell );

						/*
						 * cell = document.createElement( "TD" );
						 * cell.className="gridColumn"; cell.innerHTML =
						 * rowData["Location"];
						 * cell.setAttribute("align","left"); row.appendChild(
						 * cell );
						 */
						
						
						cell = document.createElement( "TD" );
						cell.className="gridColumn";       
						cell.innerHTML = rowData["EventLocation"];  
						cell.setAttribute("align","left");
						row.appendChild( cell );

						cell = document.createElement( "TD" );
						cell.className="gridColumn";       
						cell.innerHTML = rowData["Status"];  
						cell.setAttribute("align","left");
						row.appendChild( cell );
						
						
					}
					
					$("#addLoadMessage").hide();
					 pagerOption();
					 $("#search").attr("disabled", false); 
				}
				else{
					
					$(".pagination").hide();
					generateNoRecordsForLunchBox(tbody,tableId);
					$("#addLoadMessage").hide();
					$("#search").attr("disabled", false); 
				}
				
				// generateFooter(tbody,tableId);


			},
			error: function(e){
	        	$("#addLoadMessage").hide();
	        	$("#search").attr("disabled", false);
	        	  alert("Please try again");
	        	  
	        }
		});


		

	}
	
	
	function loadlunchBoxDataOverlay(id,fname,lname,email,phone,food,company,job,status,eventLocation){
		$("#Approve").hide();
		$("#Reject").hide();
		if(status == "Registered"){
			$("#Approve").show();
			$("#Reject").show();
		}
		
		document.getElementById('load').style.display='none';
		document.getElementById('resultMessage').innerHTML='';
		document.getElementById("overlayfirstName").value='';
		document.getElementById("overlaylastName").value='';
		document.getElementById("overlayemail").value='';
		document.getElementById("overlayPhone").value='';
		document.getElementById("overlayfood").value='';
		document.getElementById("overlaycompany").value='';
		document.getElementById("overlayjob").value='';
		// document.getElementById("overlaylocation").value='';
		document.getElementById("overlayEventlocation").value='';
		document.getElementById("overlayStatus").value='Registered';
	     
	        document.getElementById("headerLabel").style.color="white";
	        document.getElementById("headerLabel").innerHTML="Lunch Box Details";
	 
	    var overlay = document.getElementById('overlay');
	    var specialBox = document.getElementById('specialBox');
	           
	    overlay.style.opacity = .8;
	    if(overlay.style.display == "block"){
	        overlay.style.display = "none";
	        specialBox.style.display = "none";
	       
	    } else {
	       
	        overlay.style.display = "block";
	        specialBox.style.display = "block";
	       
	        document.getElementById("lunchBoxId").value=id;
	        document.getElementById("overlayfirstName").value=fname;
			document.getElementById("overlaylastName").value=lname;
			document.getElementById("overlayemail").value=email;
			document.getElementById("overlayPhone").value=phone;
			document.getElementById("overlayfood").value=food;
			document.getElementById("overlaycompany").value=company;
			document.getElementById("overlayjob").value=job;
			// document.getElementById("overlaylocation").value=location;
			document.getElementById("overlayEventlocation").value=eventLocation;
			$("#overlayStatus").html(status);
			
		}
	}
	
	function submitLunchBoxStatus(status)
	{
		$("#Approve").attr("disabled", true);
		$("#Reject").attr("disabled", true);
		var id=$("#lunchBoxId").val();
		var Email=$("#overlayemail").val();
		
		var fname=$("#overlayfirstName").val();
		var lname=$("#overlaylastName").val();
		var phone=$("#overlayPhone").val();
		var food=$("#overlayfood").val();
		var company=$("#overlaycompany").val();
		var job=$("#overlayjob").val();
		// var location=$("#overlaylocation").val();
		var eventLocation=$("#overlayEventlocation").val();
	
		var r = null;
		
		if(status == "Approved"){
			 r = confirm("Do you want to Approve?");	
		}else{
			r = confirm("Do you want to Reject?");	
		}
		
		  if (r == true) {
			  document.getElementById('load').style.display='block';
				document.getElementById('resultMessage').innerHTML='';
				 var myObj={};
				    myObj["id"]=id;
				    myObj["status"]=status;
				    myObj["Email"]=Email;
				  // myObj["location"]=location;
				    myObj["fname"]=fname;
				    myObj["lname"]=lname;
				    myObj["phone"]=phone;
				    myObj["food"]=food;
				    myObj["company"]=company;
				    myObj["job"]=job;
				    myObj["eventLocation"]=eventLocation;
				    
				    var json = JSON.stringify(myObj);
				    $.ajax({
						
						url:CONTENXT_PATH+"/updateLunchBoxStatus.action",
						data:{jsonData: json},
						contentType: 'application/json',
						type: 'GET',
						context: document.body,
						success: function(responseText) {
							// alert("responseText"+responseText);
							if(responseText=='Success'){
				                document.getElementById('load').style.display='none';
				                document.getElementById('resultMessage').innerHTML="Lunch box status updated successfully";
				                document.getElementById('resultMessage').style.color="green";
				                $("#overlayStatus").html(status);
				                $("#Approve").hide();
				    			$("#Reject").hide();
				    			$("#Approve").attr("disabled", false);
				    			$("#Reject").attr("disabled", false);
				    			
				            }else{
				                document.getElementById('load').style.display='none';
				                document.getElementById('resultMessage').innerHTML="Please try again later";
				                document.getElementById('resultMessage').style.color="red";
				                $("#Approve").attr("disabled", false);
				    			$("#Reject").attr("disabled", false);
				            }
							getLunchBoxData();
							
						},
						error: function(e){
				        	$("#addLoadMessage").hide();
				        	$("#Approve").attr("disabled", false);
			    			$("#Reject").attr("disabled", false);
				        	  alert("Please try again");
				        	  
				        }
					});
		  } else {
			$("#Approve").attr("disabled", false);
  			$("#Reject").attr("disabled", false);
		    return false;
		  }
		
	}
	
	
	function generateNoRecordsForLunchBox(tbody,oTable) {
		   
	    var noRecords =document.createElement("TR");
	    noRecords.className="gridRowEven";
	    tbody.appendChild(noRecords);
	    cell = document.createElement("TD");
	    cell.className="gridColumn";
	    cell.colSpan = "6";   
	    cell.innerHTML = "No Records Found for this Search";
	    noRecords.appendChild(cell);
	}
	
	function closeLunchBoxOverlay() {
	    var overlay = document.getElementById('overlay');
	    var specialBox = document.getElementById('specialBox');

	    overlay.style.opacity = .8;
	    if(overlay.style.display == "block"){
	        overlay.style.display = "none";
	        specialBox.style.display = "none";
	    }
	    else {
	        overlay.style.display = "block";
	        specialBox.style.display = "block";
	    }
	  
	}
	
	/*
	 * nagalakshmi Telluri 5/10/2019 Email Check New Method
	 */
		
		/***********************************************************************
		 * Email Checking Data Starts
		 **********************************************************************/
		
		
		
			
		
	/*
	 * function checkEmail(element){
	 * if(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(element.value)){
	 * return(true); }element.value=""; alert("Invalid E-mail Address! Please
	 * re-enter."); return(false); }
	 */
		
		function enableEnter(e) {
		    var keynum;
		    var keychar;
		    var numcheck;
		    
		    if(window.event) {
		        keynum = e.keyCode;
		    }
		    else if(e.which) // Netscape/Firefox/Opera
		    {
		        keynum = e.which;
		    }
		    try{
		        if(keynum==13){
		        
		        	checkEmailExistsOrNot();
		            return false;  
		        } 
		     
		    }catch(e){
		        alert("Error"+e);
		    }
		};

		  function checkEmailExistsOrNot() {
		// alert("in checkEmailExistsOrNot");
			  var email = document.getElementById("email").value;
			  var emailCheck =new Array();
			  emailCheck = email.split("@");
			  // alert(emailCheck[1])
			  if(emailCheck[1]!="miraclesoft.com")
			      {
				 
				  document.getElementById("email").value="";
			          alert(" Please enter miraclesoft email!");
			        
			          return false;
			      }
			  
			  
			     
		  	document.getElementById("loadActMessageAS").style.display = 'block';
		  	
		
		  	
		  	var oTable = document.getElementById("tblEmailDetails");
		  	clearTable(oTable);
		  	var req = newXMLHttpRequest();
		  	req.onreadystatechange = function() {
		  		if (req.readyState == 4) {
		  			if (req.status == 200) {
		  				/*
						 * document.getElementById("PFPortal").style.display =
						 * 'none';
						 */
		  				document.getElementById("loadActMessageAS").style.display = 'none';
		  				displayEmailDetails(req.responseText);
		  			}
		  		} else {
		  			document.getElementById("loadActMessageAS").style.display = 'block';
		  			/*
					 * document.getElementById("PFPortal").style.display =
					 * 'block';
					 */
		  			// alert("HTTP error ---"+req.status+" : "+req.statusText);
		  		}
		  	};
		  	
		 
		  	var url = CONTENXT_PATH+"/checkEmailNew.action?email="+email;
			// alert(url);
		  	req.open("GET", url, "true");
		  	req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		  	req.send(null);
		  }

		  function displayEmailDetails(response) {

		  	// alert("response" + response);

		  	var oTable = document.getElementById("tblEmailDetails");
		  	clearTable(oTable);
		  	var dataArray = response;
		  	if (dataArray == "no data") {
		  		alert("No Records Found for this Search");
		  	} else {

		  		var headerFields = new Array("EmpNo", "Name", "Gender","Mobile&nbsp;Number","ReportsTo","Department","Practice","Country","CurStaus");
		  		ParseAndGenerateHTML(oTable, dataArray, headerFields);
		  	}
		  	
		  

		  }

		 
		  /*******************************************************************
			 * MSA Data Starts Ends
			 ******************************************************************/
	
	/*
	 * NagaLakshmi Telluri 10/24/2019
	 */	  
		  
		
		  function addMSAToCustomer(accountId,startDate,endDate){
			  
			  

				
			  if(startDate != '-')
			  {
				  
				
		
		  document.getElementById("msaStartDate").value= startDate;
		  
		  
			  }else
				  {
				  document.getElementById("msaStartDate").value= '';
				  }
		  
		
		
		  
		  if(endDate != '-')
		  
		  {
			  document.getElementById("msaExpiryDate").value= endDate;
		  }
		  else
			  {
			 
			
			  document.getElementById("msaExpiryDate").value= '';
			  }
			  

			     
			    document.getElementById("headerLabel").style.color="white";
			    document.getElementById("headerLabel").innerHTML="Add MSA dates to customer";
			  
			    var overlay = document.getElementById('overlay');
			    var specialBox = document.getElementById('specialBox');
			  
			    overlay.style.opacity = .8;
			    if(overlay.style.display == "block"){
			        overlay.style.display = "none";
			        specialBox.style.display = "none";
			    }
			       else {
			        overlay.style.display = "block";
			        specialBox.style.display = "block";
			    }
				
				
			    document.getElementById('customerId').value=accountId;
			
			    
			}
		  
		    
		  
		
		  
		  function toggleMSADatesOverlay(){
				
			  document.getElementById("resultMessage").innerHTML='';
				
				 
			    document.getElementById("headerLabel").style.color="white";
			    document.getElementById("headerLabel").innerHTML="Add MSA dates to customer";
			  // showRow('addTr');
			    var overlay = document.getElementById('overlay');
			    var specialBox = document.getElementById('specialBox');

			    overlay.style.opacity = .8;
			    if(overlay.style.display == "block"){
			    overlay.style.display = "none";
			    specialBox.style.display = "none";

			 // document.getElementById("frmDBGrid").submit();

			    }
			    else {
			    overlay.style.display = "block";
			    specialBox.style.display = "block";
			    }
			    
			 
			   
			}	  
	
		  
		  function doAddMSADates()
		  {
			 
			  
			  document.getElementById("resultMessage").innerHTML=''; 
			  
			 var msaStartDate= document.getElementById("msaStartDate").value;
			 
			 // alert("msaStartDate"+msaStartDate);
			 
			 
			 var msaExpiryDate= document.getElementById("msaExpiryDate").value;
			 
			 if(msaStartDate == '' || msaExpiryDate == ''  )
				 {
				 alert("Please enter Mandatory fields");
				 return false;
				 }
			 
			// alert("msaExpiryDate"+msaExpiryDate);
			 var accountId=   document.getElementById('customerId').value;
			// alert("accountId"+accountId);
			 
			  var url = CONTENXT_PATH+"/addMSADatesToCustomer.action?msaStartDate="+msaStartDate+"&msaExpiryDate="+msaExpiryDate+"&accountId="+accountId;

			    var req = newXMLHttpRequest();
			    req.onreadystatechange = function() {
			        if (req.readyState == 4) {
			            if (req.status == 200) {  
			              
			                document.getElementById("loadmsadates").style.display = 'none';
			                displayResultMessageForMSADates(req.responseText);                        
			            } 
			        }else {
			            document.getElementById("loadmsadates").style.display = 'block';
			        }
			    };
			    req.open("GET", url, true);
			    req.send(null); 
			}
 
			 
		function displayResultMessageForMSADates(responseText)
		{
			
			if(responseText == "1")
				{
			  document.getElementById('resultMessage').innerHTML = "<font style='color:green;'>MSA dates added Succesfully</font>";
				}
			
			else
				{
				  document.getElementById('resultMessage').innerHTML="<font style='color:red;'>Please try again later!!!</font>";
				}
			
			
			getCustomersMSAList();
			
			 document.getElementById("responseText").value =responseText;
			
		}

		
	
/*
 * NagaLakhsmi Telluri getCustomerMSAlIST
 * 
 */		
		
		function getCustomersMSAList()
		{
		var oTable = document.getElementById("tblMSAUpdate");
		clearTable(oTable);
		var NAME=document.getElementById("customerName").value;
		
		
		var msafrmStartDate=document.getElementById("frmMSAstartDate").value;
		var msatoStartDate=document.getElementById("frmMSAendDate").value;
		
		var msafrmExpiryDate=document.getElementById("fromExpiryDate").value;
		var msatoExpiryDate=document.getElementById("toExpiryDate").value;

		var req = newXMLHttpRequest();
		req.onreadystatechange = readyStateHandlerreq(req, displayMSACustomerProjectsAjaxList);

		var accountId=document.getElementById('consultantId').value;
	
	
		
		var url = CONTENXT_PATH+"/searchMSACustomerProjectsAjaxList.action?accId="+accountId+"&msafrmStartDate="+msafrmStartDate+"&msatoStartDate="+msatoStartDate+"&msafrmExpiryDate="+msafrmExpiryDate+"&msatoExpiryDate="+msatoExpiryDate; 
		
		

		req.open("GET",url,"true");
		req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
		req.send(null);


		}

		function displayMSACustomerProjectsAjaxList(resText)
		{

	// alert("resText"+resText);
		var oTable = document.getElementById("tblMSAUpdate");

		clearTableForCustomerProjects(oTable);


		var headerFields = new Array("SNo","CustomerNAME","MSA&nbsp;StartDate","MSA&nbsp;ExpiryDate");

		ParseAndGenerateHTML(oTable,resText, headerFields);
	
		pagerOption();
		} 		
		
	
	