/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*Don't Alter these Methods*/
function newXMLHttpRequest() {
	var xmlreq = false;
	if (window.XMLHttpRequest) {
		xmlreq = new XMLHttpRequest();
	} else if (window.ActiveXObject) {
		try {
			xmlreq = new ActiveXObject("MSxm12.XMLHTTP");
		} catch (e1) {
			try {
				xmlreq = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (e2) {
				xmlreq = false;
			}
		}
	}
	return xmlreq;
}

function readyStateHandler(req, responseXmlHandler) {
	return function() {
		if (req.readyState == 4) {
			if (req.status == 200) {
				responseXmlHandler(req.responseXML);
			} else {
				alert("HTTP error" + req.status + " : " + req.statusText);
			}
		}
	}
}
function readyStateHandlerText(req, responseTextHandler) {
	return function() {
		if (req.readyState == 4) {
			if (req.status == 200) {
				responseTextHandler(req.responseText);
			} else {
				alert("HTTP error" + req.status + " : " + req.statusText);
			}
		}
	}
}

/* Methods for getting Practices by Department */

function getPracticeDataV1() {

	var departmentName = document.getElementById("departmentId").value;
	var req = newXMLHttpRequest();
	req.onreadystatechange = readyStateHandler(req, populatePractices);
	var url = CONTENXT_PATH + "/getEmpDepartment.action?departmentName="
			+ departmentName;
	req.open("GET", url, "true");
	req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	req.send(null);

}

function populatePractices(resXML) {

	var practiceId = document.getElementById("practiceId");
	var department = resXML.getElementsByTagName("DEPARTMENT")[0];
	var practices = department.getElementsByTagName("PRACTICE");
	practiceId.innerHTML = " ";

	for (var i = 0; i < practices.length; i++) {
		var practiceName = practices[i];

		var name = practiceName.firstChild.nodeValue;
		var opt = document.createElement("option");
		if (i == 0) {
			opt.setAttribute("value", "All");
		} else {
			opt.setAttribute("value", name);
		}
		opt.appendChild(document.createTextNode(name));
		practiceId.appendChild(opt);
	}
}

/*******************************************************************************
 * Utilization Report start
 ******************************************************************************/

function getUtilizationReport() {

	var oTable = document.getElementById("tblutilityReport");
	clearTable(oTable);

	document.getElementById("totalState").innerHTML = "";
	document.getElementById("totalAvailable").innerHTML = "";
	document.getElementById("totalOnBench").innerHTML = "";
	document.getElementById("totalOnProject").innerHTML = "";
	document.getElementById("totalRP").innerHTML = "";
	document.getElementById("totalTraining").innerHTML = "";

	var opsContactId = document.getElementById("opsContactId").value;
	var country = document.getElementById("country").value;
	var departmentId = document.getElementById("departmentId").value;
	var practiceId = document.getElementById("practiceId").value;
	// alert("****** opsContactId ****"+opsContactId);
	// alert("****** country ****"+country);
	// alert("****** departmentId ****"+departmentId);
	// alert("****** practiceId ****"+practiceId);

	if (opsContactId == "-1") {
		alert("please select operational contact");
		return false;
	}

	var req = newXMLHttpRequest();
	req.onreadystatechange = function() {
		if (req.readyState == 4) {
			if (req.status == 200) {
				document.getElementById("utilityReport").style.display = 'none';
				displayUtilizationReport(req.responseText);
			}
		} else {
			document.getElementById("utilityReport").style.display = 'block';
			// alert("HTTP error ---"+req.status+" : "+req.statusText);
		}
	};
	var url = CONTENXT_PATH + "/getUtilizationReport.action?opsContactId="
			+ opsContactId + "&country=" + country + "&departmentId="
			+ departmentId + "&practiceId=" + practiceId;

	req.open("GET", url, "true");
	req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	req.send(null);
}

function displayUtilizationReport(response) {
	// alert(response);

	var oTable = document.getElementById("tblutilityReport");
	clearTable(oTable);

	var dataArray = response;

	if (dataArray == "no data") {
		alert("No Records Found for this Search");
	} else {

		// tbody = document.createElement("TBODY");
		// var headerFields = new Array("S.No","Reports To","Employee
		// name","State","HireDate(DOJ)","Project","SkillSet");
		var headerFields = new Array("S.No", "Reports To",
				"Employee&nbsp; name", "Employee No", "State", "HireDate(DOJ)",
				"Project", "SkillSet");
		// generateHeader(headerArray,tableId);

		var temp = new Array;
		temp = dataArray.split('addTo');

		if (response != '') {

			document.getElementById("totalState").innerHTML = temp[1];
			document.getElementById("totalAvailable").innerHTML = temp[2];
			document.getElementById("totalOnBench").innerHTML = temp[3];
			document.getElementById("totalOnProject").innerHTML = temp[4];
			document.getElementById("totalRP").innerHTML = temp[5];
			document.getElementById("totalTraining").innerHTML = temp[6];
			ParseAndGenerateHTML(oTable, temp[0], headerFields);
			// document.getElementById(("footer"+myHTMLTable.id)).innerHTML =
			// "Total Value is: "+storeSum;

			// // myHeadTable.innerHTML = "Total Rows"+temp[1];
			// }else{
			// alert('No Result For This Search...');
			// spnFast.innerHTML="No Result For This Search...";
			// }
		}

	}
}

function generateUtilizationReport(tableBody, record, fieldDelimiter) {
	// alert("generateAccountsListByPriority");
	// var empLoginId="";
	// var empLoginId = document.getElementById("teamMemberId1").value;
	var row;
	var cell;
	var fieldLength;
	// alert("delimetr-->"+fieldDelimiter);
	var fields = record.split(fieldDelimiter);
	// alert("fileds-->"+fields)
	fieldLength = fields.length;
	// alert("fileds-->"+fieldLength);
	var length = fieldLength;

	row = document.createElement("TR");
	row.className = "gridRowEven";
	tableBody.appendChild(row);
	// alert(fields);
	for (var i = 2; i < length; i++) {
		cell = document.createElement("TD");
		cell.className = "gridColumn";
		cell.innerHTML = fields[i];
		if (parseInt(i, 10) == 4) {
			// alert(fields[0]);
			cell.innerHTML = "<a href='javascript:getEmployeeDetails("
					+ fields[0] + ")'>" + fields[i] + "</a>";
		}
		if (parseInt(i, 10) == 8 && fields[6] == "OnProject") {
			// alert(fields[0]);
			cell.innerHTML = "<a href='javascript:getProjectUtilizationDetails("
					+ fields[0]
					+ ",\""
					+ fields[6]
					+ "\")'>"
					+ fields[i]
					+ "</a>";
		}
		if (parseInt(i, 10) == 9) {
			// alert(fields[0]);
			cell.innerHTML = "<a href='javascript:getSkillSet(" + fields[0]
					+ ")'>" + fields[i] + "</a>";
		}

		if (fields[i] != '') {
			if (i == 1) {
				cell.setAttribute("align", "left");
			} else {
				cell.setAttribute("align", "left");
			}
			row.appendChild(cell);
		}
	}
	empLoginId = "";
}

function ParseAndGenerateHTML(oTable, responseString, headerFields) {

	// alert("ParseAndGenerateHTML");
	var start = new Date();
	var fieldDelimiter = "#^$";
	var recordDelimiter = "*@!";
	// alert('responseString%%%% ******'+responseString);
	// alert('rowCount%%%% ******'+rowCount);

	var records = responseString.split(recordDelimiter);
	// alert("records---->"+records);
	generateTable(oTable, headerFields, records, fieldDelimiter);
}

function generateTableDynamicHeader(tableBody, headerFields) {

	var row;
	var cell;

	var thead = document.createElement("thead");
	tableBody.appendChild(thead);
	row = document.createElement("TR");
	row.className = "dashBoardgridHeader";
	thead.appendChild(row);

	for (var i = 0; i < headerFields.length; i++) {
		cell = document.createElement("TH");
		cell.className = "dashBoardgridHeader";
		row.appendChild(cell);
		cell.innerHTML = headerFields[i];
		cell.width = 120;
	}
}
function generateTable(oTable, headerFields, records, fieldDelimiter) {
	// alert("In Generate Table "+fieldDelimiter);
	// alert(records);
	var tbody;

	if (oTable.id == "tblReviewStatusReport") {
		generateTableDynamicHeader(oTable, headerFields);
		tbody = document.createElement("TBODY");
		oTable.appendChild(tbody);
	} else if (oTable.id == "tblReviewStatusReview") {
		generateTableDynamicHeader(oTable, headerFields);
		tbody = document.createElement("TBODY");
		oTable.appendChild(tbody);
	}

	else {

		tbody = oTable.childNodes[0];
		tbody = document.createElement("TBODY");
		oTable.appendChild(tbody);
		generateTableHeader(tbody, headerFields);
	}

	var rowlength;
	if (oTable.id == "tblutilityReport") {
		rowlength = records.length;
	} else
		rowlength = records.length - 1;
	// alert("rowlength--->"+rowlength);
	if (rowlength > 0 && records != "") {
		// alert("rowlength-->^"+records);
		for (var i = 0; i < rowlength; i++) {
			// alert("i-->"+i);
			if (oTable.id == "tblutilityReport") {
				generateUtilizationReport(tbody, records[i], fieldDelimiter);
			} else if (oTable.id == "tblleaveReport") {
				// alert("in if condition");
				generateLeavesReport(tbody, records[i], fieldDelimiter);
			} else if (oTable.id == "tblAccomadationReport") {
				// alert("in if condition");
				generateAccomadationReport(tbody, records[i], fieldDelimiter);
			} else if (oTable.id == "tblLeaves") {
				// alert("in if condition");
				generateLeaveOverLay(tbody, records[i], fieldDelimiter);
			} else if (oTable.id == "tblsalesReport") {
				// alert("in if condition");
				generateSalesKpiReport(tbody, records[i], fieldDelimiter);
			} else if (oTable.id == "tblAvailableReport") {
				generateAvailableReport(tbody, records[i], fieldDelimiter);
			} else if (oTable.id == "tblrecruitmentReport") {
				generateRecruitmentReport(tbody, records[i], fieldDelimiter);
			} else if (oTable.id == "tblPFPortalReport") {
				generatePFPortalReport(tbody, records[i], fieldDelimiter);
			} else if (oTable.id == "tblEmpBasedOnCustomerReport") {
				generateEmpBasedOnCustomerReport(tbody, records[i],
						fieldDelimiter);
			} else if (oTable.id == "tblOnboardData") {
				generateOnboardReport(tbody, records[i], fieldDelimiter);
			} else if (oTable.id == "tblMissingData") {
				generateMissingDataReport(tbody, records[i], fieldDelimiter);
			} else if (oTable.id == "tblProjectUtilDetails") {
				generateProjectUtilDetails(tbody, records[i], fieldDelimiter);
			} else if (oTable.id == "tblExperienceRptData") {
				generateExpReports(tbody, records[i], fieldDelimiter);
			} else if (oTable.id == "tblAvailableDetails") {

				generateAvailableProjectEmployeeDetailsReport(tbody,
						records[i], fieldDelimiter);
			} else if (oTable.id == "tblActiveProjectDetails") {
				// alert("tblActiveProjectDetails");
				generateActiveProjectDetailsReport(tbody, records[i],
						fieldDelimiter);
			} else if (oTable.id == "tblReviewStatusReport") {
				generateSalesReviewsStatusReport(tbody, records[i],
						fieldDelimiter);

			} else if (oTable.id == "tblReviewStatusReview") {
				generateSalesReviewsStatusReview(tbody, records[i],
						fieldDelimiter);
			} else if (oTable.id == "tblemployeeExperienceReport") {
				generateOffShoreEmployeeExperienceReport(tbody, records[i],
						fieldDelimiter);
			}

			else {
				generateRow(oTable, tbody, records[i], fieldDelimiter);
			}
		}

	} else {
		generateNoRecords(tbody, oTable);
	}
	generateFooter(tbody, oTable);
}

function generateRow(oTable, tableBody, record, delimiter) {
	// alert("In generate Row function")
	var row;
	var cell;
	var fieldLength;
	var fields = record.split(delimiter);
	// alert(fields);
	fieldLength = fields.length;
	var length;
	if (oTable.id == "utilityReport") {
		length = fieldLength;
	} else if (oTable.id == "tblleaveReport") {
		length = fieldLength;
	} else if (oTable.id == "tblAccomadationReport") {
		length = fieldLength;
	} else if (oTable.id == "tblsalesReport") {
		length = fieldLength;
	} else if (oTable.id == "tblAvailableReport") {
		length = fieldLength;
	} else if (oTable.id == "tblPFPortalReport") {
		length = fieldLength;

	} else if (oTable.id == "tblLeaves") {
		length = fieldLength;

		// alert("in grid formation");
		// for (var i=0;i<length;i++) {
		// cell = document.createElement( "TD" );
		// cell.className="gridColumn";
		// // if(parseInt(i,10)==15){
		// // var jobTitle = fields[i].substring(0,10);
		// // cell.innerHTML = jobTitle
		// //
		// // }
		// // else{
		// cell.innerHTML = fields[i];
		// // }
		// row.appendChild( cell );
		// }
	} else if (oTable.id == "tblrecruitmentReport") {
		length = fieldLength;
	} else if (oTable.id == "tblEmpBasedOnCustomerReport") {
		length = fieldLength;
	} else if (oTable.id == "tblOnboardData") {
		length = fieldLength;
	} else if (oTable.id == "tblemployeeExperienceReport") {
		length = fieldLength;
	} else {
		length = fieldLength - 1;
	}
	row = document.createElement("TR");
	row.className = "gridRowEven";
	tableBody.appendChild(row);

	for (var i = 0; i < length; i++) {
		// cell = document.createElement( "TD" );
		// cell.className="gridColumn";
		// alert(fields[i]+"fields[i]");
		// cell.innerHTML = fields[i];

		cell = document.createElement("TD");
		cell.className = "gridColumn";
		// alert(fields[i]+"fields[i]");
		cell.innerHTML = fields[i];

		// if(fields[i]!=''){
		if (fields[i] != '') {
			row.appendChild(cell);
		}
	}
}

function generateNoRecords(tbody, oTable) {
	var noRecords = document.createElement("TR");
	noRecords.className = "gridRowEven";
	tbody.appendChild(noRecords);
	cell = document.createElement("TD");
	cell.className = "gridColumn";

	if (oTable.id == "tblutilityReport") {
		cell.colSpan = "8";
	} else if (oTable.id == "tblleaveReport") {
		cell.colSpan = "8";
	} else if (oTable.id == "tblAccomadationReport") {
		cell.colSpan = "10";
	} else if (oTable.id == "tblLeaves") {
		cell.colSpan = "3";
	} else if (oTable.id == "tblsalesReport") {
		cell.colSpan = "8";
	} else if (oTable.id == "tblrecruitmentReport") {
		cell.colSpan = "9";
	} else if (oTable.id == "tblAvailableReport") {
		cell.colSpan = "11";
	} else if (oTable.id == "tblPFPortalReport") {
		cell.colSpan = "12";
	} else if (oTable.id == "tblEmpBasedOnCustomerReport") {
		cell.colSpan = "10";
	} else if (oTable.id == "tblOnboardData") {
		cell.colSpan = "11";
	} else if (oTable.id == "tblMissingData") {
		cell.colSpan = "5";
	} else if (oTable.id == "tblProjectUtilDetails") {
		cell.colSpan = "6";
	} else if (oTable.id == "tblAvailableDetails") {
		cell.colSpan = "7";
	} else if (oTable.id == "tblActiveProjectDetails") {
		cell.colSpan = "7";
	} else if (oTable.id == "tblReviewStatusReport") {
		cell.colSpan = "20";
	} else if (oTable.id == "tblemployeeExperienceReport") {
		cell.colSpan = "19";
	}

	cell.innerHTML = "No Records Found for this Search";
	noRecords.appendChild(cell);
}

function generateFooter(tbody, oTable) {
	// alert(oTable.id);
	var footer = document.createElement("TR");
	footer.className = "gridPager";
	tbody.appendChild(footer);
	cell = document.createElement("TD");
	cell.className = "gridFooter";
	cell.id = "footer" + oTable.id;
	// cell.colSpan = "5";
	if (oTable.id == "tblutilityReport") {
		cell.colSpan = "7";
	} else if (oTable.id == "tblleaveReport") {
		cell.colSpan = "8";
	} else if (oTable.id == "tblAccomadationReport") {
		cell.colSpan = "10";
	} else if (oTable.id == "tblLeaves") {
		cell.colSpan = "3";
	} else if (oTable.id == "tblsalesReport") {
		cell.colSpan = "7";
	} else if (oTable.id == "tblrecruitmentReport") {
		cell.colSpan = "8";
	} else if (oTable.id == "tblAvailableReport") {
		cell.colSpan = "13";
	} else if (oTable.id == "tblPFPortalReport") {
		cell.colSpan = "12";
	} else if (oTable.id == "tblEmpBasedOnCustomerReport") {
		cell.colSpan = "9";
	} else if (oTable.id == "tblOnboardData") {
		cell.colSpan = "12";
	} else if (oTable.id == "tblMissingData") {
		cell.colSpan = "4";
	} else if (oTable.id == "tblProjectUtilDetails") {
		cell.colSpan = "5";
	} else if (oTable.id == "tblAvailableDetails") {
		cell.colSpan = "7";
	} else if (oTable.id == "tblActiveProjectDetails") {
		cell.colSpan = "7";
	} else if (oTable.id == "tblReviewStatusReport") {
		cell.colSpan = "20";
	} else if (oTable.id == "tblemployeeExperienceReport") {
		cell.colSpan = "19";
	}
	footer.appendChild(cell);
}

function clearTable(tableId) {
	var tbl = tableId;
	var lastRow = tbl.rows.length;
	while (lastRow > 0) {
		tbl.deleteRow(lastRow - 1);
		lastRow = tbl.rows.length;
	}
}

function generateTableHeader(tableBody, headerFields) {
	var row;
	var cell;
	row = document.createElement("TR");
	row.className = "gridHeader";
	tableBody.appendChild(row);
	for (var i = 0; i < headerFields.length; i++) {
		cell = document.createElement("TD");
		cell.className = "gridHeader";
		row.appendChild(cell);

		cell.setAttribute("width", "10000px");
		cell.innerHTML = headerFields[i];
	}
}

function getEmployeeDetails(empId) {

	// window.location='../employee/getEmployee.action?empId='+empId+'','target_blank';
	window.location = '../employee/getEmployee.action?empId=' + empId;

}

function getSkillSet(empId) {

	var req = newXMLHttpRequest();
	req.onreadystatechange = readyStateHandlerText(req, populateSkillset);
	var url = CONTENXT_PATH + "/popupSkillSetWindow.action?empId=" + empId;
	req.open("GET", url, "true");
	req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	req.send(null);
}

function populateSkillset(text) {

	var background = "#3E93D4";
	var title = "Emp Skill Set";
	var text1 = text;
	var size = text1.length;

	var res = text1.split("|");

	var res1 = "";
	for (var i = 0; i < res.length; i++) {
		if ((res[i].trim() == "null") || (res[i].trim() == "null")
				|| (res[i].trim() == "") || (res[i].trim() == " ")) {

		} else {
			if (i == res.length - 1) {
				res1 = res1 + res[i].trim().toUpperCase();
			} else {
				res1 = res1 + res[i].trim().toUpperCase() + ",";
			}
		}
	}

	var res2 = res1.split(",");
	var test2 = "";
	var count = 0;
	for (var i = 0; i < res2.length; i++) {
		for (var j = i; j < res2.length; j++) {
			if (res2[i].trim().toUpperCase() == res2[j].trim().toUpperCase()) {
				count = count + 1;
			} else {
				count = count;
			}

		}

		if (count == 1) {
			if (res2[i].trim() != "") {
				test2 = test2 + res2[i].trim().toUpperCase() + ",";
			}
			count = 0;
		} else {
			count = 0;
		}
	}
	// alert(test2);
	var finalStr1 = test2.slice(0, test2.lastIndexOf(","));
	// alert(finalStr1);
	var finalStr = "";
	if (finalStr1.trim() == "") {
		finalStr = "No Data";
	}

	else {
		finalStr = finalStr1 + ".".big();
	}
	// / alert(test2);

	// Now create the HTML code that is required to make the popup
	var content = "<html><head><title>" + title
			+ "</title></head>\
    <body bgcolor='" + background
			+ "' style='color:white;'>" + finalStr
			+ "<br />\
    </body></html>";
	// alert(content);
	// //alert("text1"+text1);
	// // alert("size "+content.length);
	// var indexof=(content.indexOf("|")+1);
	// var lastindexof=(content.lastIndexOf("|"));

	popup = window
			.open(
					"",
					"window",
					"channelmode=0,width=400,height=200,top=200,left=350,scrollbars=no,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no");

	popup.document.write("<b>SkillS :</b>" + content.substr(0, content.length));
	// Write content into it.
	// Write content into it.

}

/*******************************************************************************
 * Utilization Report end
 ******************************************************************************/

/*******************************************************************************
 * Accomadation Report start
 ******************************************************************************/
function getAccomadationReport() {
	var opsContactIdForAcc = document.getElementById("opsContactIdForAcc").value;
	var oTable = document.getElementById("tblAccomadationReport");
	clearTable(oTable);
	if (opsContactIdForAcc == "-1") {
		alert("please select operational contact");
		return false;
	}

	var req = newXMLHttpRequest();
	req.onreadystatechange = function() {
		if (req.readyState == 4) {
			if (req.status == 200) {
				document.getElementById("accomadationReport").style.display = 'none';
				displayAccomadationReport(req.responseText);
			}
		} else {
			document.getElementById("accomadationReport").style.display = 'block';
			// alert("HTTP error ---"+req.status+" : "+req.statusText);
		}
	};
	var url = CONTENXT_PATH
			+ "/getAccomadationReport.action?opsContactIdForAcc="
			+ opsContactIdForAcc;

	req.open("GET", url, "true");
	req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	req.send(null);

}
function displayAccomadationReport(response) {
	// alert("response"+response);

	var oTable = document.getElementById("tblAccomadationReport");
	clearTable(oTable);

	var dataArray = response;

	if (dataArray == "no data") {
		alert("No Records Found for this Search");
	} else {

		// tbody = document.createElement("TBODY");
		var headerFields = new Array("S.No", "Employee name", "Employee Id",
				"Department", "Accomadation", "Room Number", "Cafeteria",
				"Occupancy Type", "Occupancy Date", "Contact Number");

		ParseAndGenerateHTML(oTable, response, headerFields);
		// document.getElementById(("footer"+myHTMLTable.id)).innerHTML = "Total
		// Value is: "+storeSum;

		// myHeadTable.innerHTML = "Total Rows"+temp[1];

	}

}

function generateAccomadationReport(tableBody, record, fieldDelimiter) {
	// alert("generateAccountsListByPriority");
	// var empLoginId="";
	// var empLoginId = document.getElementById("teamMemberId1").value;
	var row;
	var cell;
	var fieldLength;
	// alert("delimetr-->"+fieldDelimiter);
	var fields = record.split(fieldDelimiter);
	// alert("fileds-->"+fields)
	fieldLength = fields.length;
	// alert("fileds-->"+fieldLength);
	var length = fieldLength;

	row = document.createElement("TR");
	row.className = "gridRowEven";
	tableBody.appendChild(row);
	// alert(fields);
	for (var i = 0; i < length; i++) {
		cell = document.createElement("TD");
		cell.className = "gridColumn";
		cell.innerHTML = fields[i];
		// if(parseInt(i,10)==3){
		// cell.innerHTML = "<a
		// href='javascript:getEmployeeDetails("+fields[0]+")'>"+fields[i]+"</a>";
		// }
		//       

		if (fields[i] != '') {
			if (i == 1) {
				cell.setAttribute("align", "left");
			} else {
				cell.setAttribute("align", "left");
			}
			row.appendChild(cell);
		}
	}
	empLoginId = "";
}

/*******************************************************************************
 * Accomadation Report end
 ******************************************************************************/

/*******************************************************************************
 * Sales KPI Report Start
 ******************************************************************************/
function getSalesKPIReport() {
	var oTable = document.getElementById("tblsalesReport");
	clearTable(oTable);
	var salesLeadId = document.getElementById("salesLeadId").value;
	var startDate = document.getElementById("salesStartDate").value;
	var endDate = document.getElementById("salesEndDate").value;
	if ((salesLeadId == "-1") || (startDate == "") || (endDate == "")) {
		alert("Please select mandatory fields.");
		return false;
	}
	var req = newXMLHttpRequest();
	req.onreadystatechange = function() {
		if (req.readyState == 4) {
			if (req.status == 200) {
				document.getElementById("salesKPIReport").style.display = 'none';
				displaySalesKpiReport(req.responseText);
			}
		} else {
			document.getElementById("salesKPIReport").style.display = 'block';
		}
	};
	var url = CONTENXT_PATH + "/getsalesKPIReport.action?salesLeadId="
			+ salesLeadId + "&salesStartDate=" + startDate + "&salesEndDate="
			+ endDate;
	req.open("GET", url, "true");
	req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	req.send(null);
}

function displaySalesKpiReport(response) {
	var oTable = document.getElementById("tblsalesReport");
	clearTable(oTable);
	var dataArray = response;
	if (dataArray == "no data") {
		alert("No Records Found for this Search");
	} else {
		// var headerFields = new Array("S.No","Employee name","Reports
		// To","Country","Logos Count","No, of Placements","No, of
		// ConferenceCall");
		var headerFields = new Array("S.No", "Employee name", "Employee No",
				"Reports To", "Country", "Logos Count", "No, of Placements",
				"No, of ConferenceCall");
		ParseAndGenerateHTML(oTable, dataArray, headerFields);
	}

}

function generateSalesKpiReport(tableBody, record, fieldDelimiter) {
	var row;
	var cell;
	var fieldLength;
	var fields = record.split(fieldDelimiter);
	fieldLength = fields.length;
	var length = fieldLength;
	row = document.createElement("TR");
	row.className = "gridRowEven";
	tableBody.appendChild(row);
	for (var i = 0; i < length; i++) {
		cell = document.createElement("TD");
		cell.className = "gridColumn";
		cell.innerHTML = fields[i];
		if (fields[i] != '') {
			if (i == 1) {
				cell.setAttribute("align", "left");
			} else {
				cell.setAttribute("align", "left");
			}
			row.appendChild(cell);
		}
	}

}

var temp = "0";
function defaultDates() {
	// alert("default date");
	var currentYear = new Date().getFullYear();
	var currentMonth = new Date().getMonth() + 1;
	var currentDay = "01";
	/*
	 * var currentDay = new Date().getDate(); if(currentDay <10 ){ currentDay =
	 * temp+ currentDay; }
	 */
	if (currentMonth < 10) {
		currentMonth = temp + currentMonth;
	}
	// month-date-year
	var firstDayOfMonth = currentMonth + '/' + currentDay + '/' + currentYear;
	// alert('firstDayOfMonth'+firstDayOfMonth)

	var intCurrentYear = parseInt(currentYear);

	var now = new Date();// Add 30 days
	now.setDate(1);
	now.setDate(now.getDate() + 29);

	var currentYear = now.getFullYear();
	var currentMonth = now.getMonth() + 1;
	var currentDay = now.getDate();
	if (currentDay < 10) {
		currentDay = temp + currentDay;
	}
	if (currentMonth < 10) {
		currentMonth = temp + currentMonth;
	}
	var lastDate;
	var lastDate = currentMonth + '/' + currentDay + '/' + currentYear;
	// alert('lastDate'+lastDate)

	var currentDate = new Date();
	var day = currentDate.getDate();
	var month = currentDate.getMonth() + 1;
	var year = currentDate.getFullYear();

	var endDate = month + "/" + day + "/" + year;

	var today = new Date();
	var priorDate = new Date(endDate);
	priorDate.setDate(priorDate.getDate() - 60);
	var priorDay = priorDate.getDate();
	var priorMonth = priorDate.getMonth() + 1;
	var priorYear = priorDate.getFullYear();

	// var startDate = priorMonth+"/"+priorDay+"/"+priorYear;
	var startDate = "";
	if (parseInt(priorMonth) < 10) {
		startDate = "0" + priorMonth + "/" + "01" + "/" + priorYear;
	} else {
		startDate = priorMonth + "/" + "01" + "/" + priorYear;
	}

	/* Start date before last month first date and end date as current date */

	document.getElementById('salesStartDate').value = startDate;
	document.getElementById('salesEndDate').value = endDate;
	document.getElementById('startDateKPI').value = startDate;
	document.getElementById('endDateKPI').value = endDate;

}

/*******************************************************************************
 * Sales KPI Report end
 ******************************************************************************/

/*******************************************************************************
 * Recruitment KPI Report start author:Indira Soujanya Boni Date :12/13/2015
 * 
 ******************************************************************************/

function getRecruitmentKpiReport() {
	var oTable = document.getElementById("tblrecruitmentReport");
	clearTable(oTable);

	var recManagerId = document.getElementById("recManagerId").value;
	var startDate = document.getElementById("startDateKPI").value;
	var endDate = document.getElementById("endDateKPI").value;
	if ((recManagerId == "-1")) {
		alert("Please select mandatory fields.");
		return false;
	}
	var req = newXMLHttpRequest();
	req.onreadystatechange = function() {
		if (req.readyState == 4) {
			if (req.status == 200) {
				document.getElementById("recruitmentReport").style.display = 'none';
				displayRecruitmentReport(req.responseText);
			}
		} else {
			document.getElementById("recruitmentReport").style.display = 'block';
		}
	};
	var url = CONTENXT_PATH + "/getRecruitmentKpiReport.action?recManagerId="
			+ recManagerId + "&startDate=" + startDate + "&endDate=" + endDate;

	req.open("GET", url, "true");
	req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	req.send(null);
}

function displayRecruitmentReport(response) {
	var oTable = document.getElementById("tblrecruitmentReport");
	clearTable(oTable);

	var dataArray = response;

	if (dataArray == "no data") {
		alert("No Records Found for this Search");
	} else {

		// var headerFields = new Array("S.No","Name","Reports
		// To","Country","Requirements","Resumes
		// Submitted","Interviews","Closures");
		var headerFields = new Array("S.No", "Name", "Employee No",
				"Reports To", "Country", "Requirements", "Resumes Submitted",
				"Interviews", "Closures");
		ParseAndGenerateHTML(oTable, dataArray, headerFields);
	}

}

function generateRecruitmentReport(tableBody, record, fieldDelimiter) {
	var row;
	var cell;
	var fieldLength;
	var fields = record.split(fieldDelimiter);
	fieldLength = fields.length;
	var length = fieldLength;

	row = document.createElement("TR");
	row.className = "gridRowEven";
	tableBody.appendChild(row);
	for (var i = 0; i < length; i++) {
		cell = document.createElement("TD");
		cell.className = "gridColumn";
		cell.innerHTML = fields[i];
		if (fields[i] != '') {
			if (i == 1) {
				cell.setAttribute("align", "left");
			} else {
				cell.setAttribute("align", "left");
			}
			row.appendChild(cell);
		}
	}

}

/*******************************************************************************
 * Recruitment KPI Report end
 ******************************************************************************/

/*******************************************************************************
 * Available Employees List Report start
 ******************************************************************************/

function getPracticeDataV2() {

	var departmentName = document.getElementById("departmentId1").value;
	var req = newXMLHttpRequest();
	req.onreadystatechange = readyStateHandler(req, populatePractice);
	var url = CONTENXT_PATH + "/getEmpDepartment.action?departmentName="
			+ departmentName;
	req.open("GET", url, "true");
	req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	req.send(null);

}

function populatePractice(resXML) {

	var practiceId = document.getElementById("practiceId1");
	var department = resXML.getElementsByTagName("DEPARTMENT")[0];
	var practices = department.getElementsByTagName("PRACTICE");
	practiceId.innerHTML = " ";

	for (var i = 0; i < practices.length; i++) {
		var practiceName = practices[i];

		var name = practiceName.firstChild.nodeValue;
		var opt = document.createElement("option");
		if (i == 1) {
			opt.setAttribute("value", "All");
		} else {
			opt.setAttribute("value", name);
		}
		opt.appendChild(document.createTextNode(name));
		practiceId.appendChild(opt);
	}
}

function getPracticeDataV3() {
	// alert("hi");
	var departmentName = document.getElementById("departmentIdExp").value;
	var req = newXMLHttpRequest();
	req.onreadystatechange = readyStateHandler(req, populatePractice1);
	var url = CONTENXT_PATH + "/getEmpDepartment.action?departmentName="
			+ departmentName;
	req.open("GET", url, "true");
	req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	req.send(null);

}

function populatePractice1(resXML) {

	var practiceId = document.getElementById("practiceIdExp");
	var department = resXML.getElementsByTagName("DEPARTMENT")[0];
	var practices = department.getElementsByTagName("PRACTICE");
	practiceId.innerHTML = " ";

	for (var i = 0; i < practices.length; i++) {
		var practiceName = practices[i];

		var name = practiceName.firstChild.nodeValue;
		var opt = document.createElement("option");
		if (i == 1) {
			opt.setAttribute("value", "All");
		} else {
			opt.setAttribute("value", name);
		}
		opt.appendChild(document.createTextNode(name));
		practiceId.appendChild(opt);
	}
}

function getAvailableList() {
	document.getElementById("totalState1").innerHTML = "";
	var oTable = document.getElementById("tblAvailableReport");
	clearTable1(oTable);
	var state = document.getElementById("state").value;
	var country = document.getElementById("country1").value;
	var departmentId = document.getElementById("departmentId1").value;
	var practiceId = document.getElementById("practiceId1").value;
	var subPracticeId = document.getElementById("subPractice1").value;
	var sortedBy = document.getElementById("sortedBy").value;

	var resourceType = document.getElementById("resourceType").value;
	var skillSet = document.getElementById("skillSet").value;
	var utilization = document.getElementById("utilizationSearch").value;
	var accountId = document.getElementById("customerName").value;
	var projectId = document.getElementById("projectName").value;
	var location = document.getElementById("location1").value;
	var basedOn = document.getElementById("basedOn").value;

	if (state == "-1") {
		alert("please select state");
		return false;
	}
	if (practiceId == "--Please Select--") {
		practiceId = "-1";
	}
	if (subPracticeId == "All") {
		subPracticeId = "-1";
	}
	var req = newXMLHttpRequest();
	req.onreadystatechange = function() {
		if (req.readyState == 4) {
			if (req.status == 200) {
				document.getElementById("availableReport").style.display = 'none';
				displayAvailableReport(req.responseText);
			}
		} else {
			document.getElementById("availableReport").style.display = 'block';
			// alert("HTTP error ---"+req.status+" : "+req.statusText);
		}
	};
	// var url =
	// CONTENXT_PATH+"/getAvailableEmpReport.action?state="+escape(state)+"&country="+country+"&departmentId="+departmentId+"&practiceId="+practiceId;
	// var url =
	// CONTENXT_PATH+"/getAvailableEmpReport.action?state="+escape(state)+"&country="+country+"&departmentId="+departmentId+"&practiceId="+practiceId+"&subPracticeId="+subPracticeId+"&sortedBy="+sortedBy+"&resourceType="+resourceType;
	// var url =
	// CONTENXT_PATH+"/getAvailableEmpReport.action?state="+escape(state)+"&country="+country+"&departmentId="+departmentId+"&practiceId="+practiceId+"&subPracticeId="+subPracticeId+"&sortedBy="+sortedBy+"&resourceType="+resourceType+"&skillSet="+escape(skillSet);
	// var url =
	// CONTENXT_PATH+"/getAvailableEmpReport.action?state="+escape(state)+"&country="+country+"&departmentId="+departmentId+"&practiceId="+practiceId+"&subPracticeId="+subPracticeId+"&sortedBy="+sortedBy+"&resourceType="+resourceType+"&skillSet="+escape(skillSet)+"&utilization="+escape(utilization);
	// var url =
	// CONTENXT_PATH+"/getAvailableEmpReport.action?state="+escape(state)+"&country="+country+"&departmentId="+departmentId+"&practiceId="+practiceId+"&subPracticeId="+subPracticeId+"&sortedBy="+sortedBy+"&resourceType="+resourceType+"&skillSet="+escape(skillSet)+"&utilization="+escape(utilization)+"&accId="+accountId+"&projectId="+projectId+"&location="+location;
	var url = CONTENXT_PATH + "/getAvailableEmpReport.action?state="
			+ escape(state) + "&country=" + country + "&departmentId="
			+ departmentId + "&practiceId=" + practiceId + "&subPracticeId="
			+ subPracticeId + "&sortedBy=" + sortedBy + "&resourceType="
			+ resourceType + "&skillSet=" + escape(skillSet) + "&utilization="
			+ escape(utilization) + "&accId=" + accountId + "&projectId="
			+ projectId + "&location=" + location + "&basedOn=" + basedOn;

	req.open("GET", url, "true");
	req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	req.send(null);
}

function displayAvailableReport(response) {
	// alert(response);
	var oTable = document.getElementById("tblAvailableReport");
	clearTable1(oTable);
	var state = document.getElementById("state").value;
	var dataArray = response;

	if (dataArray == "no data") {
		alert("No Records Found for this Search");
	} else {

		// tbody = document.createElement("TBODY");
		// var headerFields = new Array("S.No","Reports To","Employee
		// name","practice","Email","Phone Number","SkillSet");
		if (state == "Available") {
			// tbody = document.createElement("TBODY");
			// var headerFields = new
			// Array("S.No","Reports&nbsp;To","Employee&nbsp;name","Exp","Practice","Available
			// Utilization","Available&nbsp;From","Email","Phone&nbsp;Number","SkillSet");
			// var headerFields = new
			// Array("S.No","Employee&nbsp;name","Total&nbsp;Exp","Practice","Available
			// Utilization","Available&nbsp;From","Contact&nbsp;Details","SkillSet");
			var headerFields = new Array("S.No", "Employee&nbsp;name",
					"Employee No", "Total&nbsp;Exp", "Practice",
					"Available Utilization", "Available&nbsp;From",
					"Contact&nbsp;Details", "SkillSet");

			// generateHeader(headerArray,tableId);
		} else if (state == "OnProject") {
			// var headerFields = new
			// Array("S.No","Reports&nbsp;To","Employee&nbsp;name","Exp","Practice","Project&nbsp;Name","Res&nbsp;Type","Available
			// Utilization","Email","Phone&nbsp;Number","SkillSet");
			// var headerFields = new
			// Array("S.No","Employee&nbsp;name","Total&nbsp;Exp","Practice","Project&nbsp;Name","ProjectExp","Res&nbsp;Type","Avail.Util","Contact&nbsp;Details","SkillSet");
			// var headerFields = new
			// Array("S.No","Employee&nbsp;name","Total&nbsp;Exp","ProjectExp","Practice","Project&nbsp;Name","Res&nbsp;Type","Avail.Util","Contact&nbsp;Details","SkillSet");
			var headerFields = new Array("S.No", "Employee&nbsp;name",
					"Employee No", "Total&nbsp;Exp", "ProjectExp", "Practice",
					"Project&nbsp;Name", "Res&nbsp;Type", "Avail.Util",
					"Contact&nbsp;Details", "SkillSet");

		} else if (state == "OverHead") {
			// var headerFields = new
			// Array("S.No","Reports&nbsp;To","Employee&nbsp;name","Exp","Practice","Project&nbsp;Name","Email","Phone&nbsp;Number","SkillSet");

			// var headerFields = new
			// Array("S.No","Employee&nbsp;name","Total&nbsp;Exp","Practice","Project&nbsp;Name","Contact&nbsp;Details","SkillSet");
			var headerFields = new Array("S.No", "Employee&nbsp;name",
					"Employee No", "Total&nbsp;Exp", "Practice",
					"Project&nbsp;Name", "StartDate", "Contact&nbsp;Details",
					"SkillSet");
		} else {
			// var headerFields = new
			// Array("S.No","Reports&nbsp;To","Employee&nbsp;name","Exp","Practice","Project&nbsp;Name","Email","Phone&nbsp;Number","SkillSet");
			// var headerFields = new
			// Array("S.No","Employee&nbsp;name","Total&nbsp;Exp","Practice","Project&nbsp;Name","Contact&nbsp;Details","SkillSet");
			var headerFields = new Array("S.No", "Employee&nbsp;name",
					"Employee No", "Total&nbsp;Exp", "Practice",
					"Project&nbsp;Name", "Contact&nbsp;Details", "SkillSet");

		}

		// generateHeader(headerArray,tableId);

		var temp = new Array;
		temp = dataArray.split('addTo');

		if (response != '') {

			document.getElementById("totalState1").innerHTML = temp[1];

			ParseAndGenerateHTML(oTable, temp[0], headerFields);

		} else {
			alert('No Result For This Search...');
			// spnFast.innerHTML="No Result For This Search...";
		}
	}

}
// function generateAvailableReport(tableBody,record,fieldDelimiter){
// var row;
// var cell;
// var fieldLength;
// var fields = record.split(fieldDelimiter);
// fieldLength = fields.length ;
// var state=document.getElementById("state").value;
// var length = fieldLength;
// row = document.createElement( "TR" );
// row.className="gridRowEven";
// tableBody.appendChild( row );
// for (var i=1;i<length-2;i++) {
// cell = document.createElement( "TD" );
// cell.className="gridColumn";
// //
// if(parseInt(i,10)==2){
// cell.innerHTML = fields[i];
// cell.innerHTML = "<a
// href='javascript:getEmployeeDetails("+fields[0]+")'>"+fields[i]+"</a>";
// }
// if(state=="Available")
// {
// // alert("available");
// /* if(parseInt(i,10)==10){
// cell.innerHTML = "<a
// href='javascript:getSkillSet("+fields[0]+")'>"+fields[i]+"</a>";
// }*/
// if(parseInt(i,10)==8){
// cell.innerHTML = "<a
// href='javascript:getSkillSet("+fields[0]+")'>"+fields[10]+"</a>";
// }
// if(parseInt(i,10)==7){
//                
//            
// cell.innerHTML = "<a
// href='javascript:getEmployeeContactDetails("+fields[0]+")'>"+fields[9]+"</a>";
// }
// if(parseInt(i,10)==3){
//                
// // alert("in exp");
// cell.innerHTML = "<a
// href='javascript:getEmployeeExpDetails(\""+fields[3]+"\",\""+fields[7]+"\")'>"+fields[8]+"</a>";
// }
// if (i==4){
// cell.innerHTML = fields[4];
// }
// // if (i==3){
// // cell.innerHTML = fields[3];
// // }
// if (i==5){
// cell.innerHTML = fields[5];
// }
// if (i==6){
// cell.innerHTML = fields[6];
// }
//         
//
// if (i==1){
// cell.innerHTML = fields[1];
// }
// }
// else if(state=="OnProject")
// {
// if(parseInt(i,10)==10){
// cell.innerHTML = "<a
// href='javascript:getSkillSet("+fields[0]+")'>"+fields[11]+"</a>";
// }
// if(parseInt(i,10)==9){
// cell.innerHTML = "<a
// href='javascript:getEmployeeContactDetails("+fields[0]+")'>"+fields[10]+"</a>";
//                
// }
// if(parseInt(i,10)==8){
//                 
// cell.innerHTML = fields[7];
//           
// }
// if(parseInt(i,10)==3){
//                
// // alert("in exp");
// cell.innerHTML = "<a
// href='javascript:getEmployeeExpDetails(\""+fields[3]+"\",\""+fields[8]+"\")'>"+fields[9]+"</a>";
// }
//         
// if (i==4){
// cell.innerHTML = fields[12];
// }
//        
//         
// if (i==5){
// cell.innerHTML = fields[4];
// }
// if (i==6){
// cell.innerHTML = fields[5];
// }
// if (i==7){
// cell.innerHTML = fields[6];
// }
// if (i==1){
// cell.innerHTML = fields[1];
// }
//         
// }
// else
// {
// /* if(parseInt(i,10)==9){
// cell.innerHTML = "<a
// href='javascript:getSkillSet("+fields[0]+")'>"+fields[i]+"</a>";
// } */
// if(parseInt(i,10)==7){
// cell.innerHTML = "<a
// href='javascript:getSkillSet("+fields[0]+")'>"+fields[9]+"</a>";
// }
//            
// if(parseInt(i,10)==6){
// cell.innerHTML = "<a
// href='javascript:getEmployeeContactDetails("+fields[0]+")'>"+fields[8]+"</a>";
// }
// if(parseInt(i,10)==3){
//                
// // alert("in exp");
// cell.innerHTML = "<a
// href='javascript:getEmployeeExpDetails(\""+fields[3]+"\",\""+fields[6]+"\")'>"+fields[7]+"</a>";
// }
// if (i==4){
// cell.innerHTML = fields[4];
// }
// if (i==5){
// cell.innerHTML = fields[5];
// }
//         
// if (i==1){
// cell.innerHTML = fields[1];
// }
//
// }
//
// if(fields[i]!=''){
// if(i==1)
// {
// cell.setAttribute("align","left");
// }
// else
// {
// cell.setAttribute("align","left");
// }
// row.appendChild( cell );
// }
// }
//   
// }

function generateAvailableReport(tableBody, record, fieldDelimiter) {

	var projectId = document.getElementById("projectName").value;
	// alert("projectId"+projectId);
	var row;
	var cell;
	var fieldLength;
	var fields = record.split(fieldDelimiter);
	fieldLength = fields.length;
	var state = document.getElementById("state").value;
	var length = fieldLength;
	row = document.createElement("TR");
	row.className = "gridRowEven";
	tableBody.appendChild(row);
	// alert("fields[15]"+fields[15]);
	for (var i = 1; i < length - 2; i++) {
		cell = document.createElement("TD");
		cell.className = "gridColumn";
		//
		if (parseInt(i, 10) == 2) {
			cell.innerHTML = fields[i];
			cell.innerHTML = "<a href='javascript:getEmployeeDetails("
					+ fields[0] + ")'>" + fields[i] + "</a>";
		}
		if (state == "Available") {
			// alert("available");
			/*
			 * if(parseInt(i,10)==10){ cell.innerHTML = "<a
			 * href='javascript:getSkillSet("+fields[0]+")'>"+fields[i]+"</a>"; }
			 */
			if (parseInt(i, 10) == 9) {
				cell.innerHTML = "<a href='javascript:getSkillSet(" + fields[0]
						+ ")'>" + fields[11] + "</a>";
			}
			if (parseInt(i, 10) == 8) {

				cell.innerHTML = "<a href='javascript:getEmployeeContactDetails("
						+ fields[0] + ")'>" + fields[10] + "</a>";
			}
			if (parseInt(i, 10) == 4) {

				// alert("in exp");
				cell.innerHTML = "<a href='javascript:getEmployeeExpDetails(\""
						+ fields[4] + "\",\"" + fields[8] + "\")'>" + fields[9]
						+ "</a>";
			}

			if (parseInt(i, 10) == 6) {
				if (fields[6] < 100) {
					cell.innerHTML = "<a href='javascript:getEmployeeAvailableDetails("
							+ fields[0] + ")'>" + fields[6] + "</a>";
				} else {
					cell.innerHTML = fields[6];
				}
			}

			if (i == 3) {
				cell.innerHTML = fields[3];
			}
			if (i == 5) {
				cell.innerHTML = fields[5];
			}

			if (i == 7) {
				cell.innerHTML = fields[7];
			}

			if (i == 1) {
				cell.innerHTML = fields[1];
			}
		} else if (state == "OnProject") {
			if (parseInt(i, 10) == 11) {
				cell.innerHTML = "<a href='javascript:getSkillSet(" + fields[0]
						+ ")'>" + fields[12] + "</a>";
			}
			if (parseInt(i, 10) == 10) {

				cell.innerHTML = "<a href='javascript:getEmployeeContactDetails("
						+ fields[0] + ")'>" + fields[13] + "</a>";
			}

			if (parseInt(i, 10) == 9) {

				cell.innerHTML = fields[9];

			}
			if (parseInt(i, 10) == 8) {

				cell.innerHTML = fields[8];

			}

			if (parseInt(i, 10) == 7) {
				if (projectId == '-1') {
					cell.innerHTML = "<a href='javascript:getActiveProjectDetails("
							+ fields[0]
							+ ")'>"
							+ fields[6]
							+ "("
							+ fields[7]
							+ ")" + "</a>";

				} else {
					cell.innerHTML = fields[6] + "(" + fields[7] + ")";
				}
			}

			if (parseInt(i, 10) == 4) {

				// alert("in exp");
				cell.innerHTML = "<a href='javascript:getEmployeeExpDetails(\""
						+ fields[4] + "\",\"" + fields[9] + "\")'>"
						+ fields[11] + "</a>";
			}

			if (parseInt(i, 10) == 5) {

				cell.innerHTML = "<a href='javascript:getProjectExpDetails("
						+ fields[14] + ",\"" + fields[15] + "\")'>"
						+ fields[14] + "</a>";
				// cell.innerHTML = fields[13];
			}

			if (i == 6) {
				cell.innerHTML = fields[5];
			}

			if (i == 3) {
				cell.innerHTML = fields[3];
			}
			if (i == 1) {
				cell.innerHTML = fields[1];
			}

		} else if (state == "OverHead") {

			/*
			 * if(parseInt(i,10)==9){ cell.innerHTML = "<a
			 * href='javascript:getSkillSet("+fields[0]+")'>"+fields[i]+"</a>"; }
			 */
			if (parseInt(i, 10) == 9) {
				cell.innerHTML = "<a href='javascript:getSkillSet(" + fields[0]
						+ ")'>" + fields[10] + "</a>";
			}

			if (parseInt(i, 10) == 8) {
				cell.innerHTML = "<a href='javascript:getEmployeeContactDetails("
						+ fields[0] + ")'>" + fields[9] + "</a>";
			}
			if (parseInt(i, 10) == 4) {

				// alert("in exp");
				cell.innerHTML = "<a href='javascript:getEmployeeExpDetails(\""
						+ fields[4] + "\",\"" + fields[7] + "\")'>" + fields[8]
						+ "</a>";
			}

			if (i == 3) {
				cell.innerHTML = fields[3];
			}
			if (i == 5) {
				cell.innerHTML = fields[5];
			}
			if (i == 6) {
				cell.innerHTML = fields[6];
			}
			if (i == 1) {
				cell.innerHTML = fields[1];
			}

			if (i == 7) {
				cell.innerHTML = fields[11];
			}
		} else {
			/*
			 * if(parseInt(i,10)==9){ cell.innerHTML = "<a
			 * href='javascript:getSkillSet("+fields[0]+")'>"+fields[i]+"</a>"; }
			 */
			if (parseInt(i, 10) == 8) {
				cell.innerHTML = "<a href='javascript:getSkillSet(" + fields[0]
						+ ")'>" + fields[10] + "</a>";
			}

			if (parseInt(i, 10) == 7) {
				cell.innerHTML = "<a href='javascript:getEmployeeContactDetails("
						+ fields[0] + ")'>" + fields[9] + "</a>";
			}
			if (parseInt(i, 10) == 4) {

				// alert("in exp");
				cell.innerHTML = "<a href='javascript:getEmployeeExpDetails(\""
						+ fields[4] + "\",\"" + fields[7] + "\")'>" + fields[8]
						+ "</a>";
			}

			if (i == 3) {
				cell.innerHTML = fields[3];
			}
			if (i == 5) {
				cell.innerHTML = fields[5];
			}
			if (i == 6) {
				cell.innerHTML = fields[6];
			}
			if (i == 1) {
				cell.innerHTML = fields[1];
			}

		}

		if (fields[i] != '') {
			if (i == 1) {
				cell.setAttribute("align", "left");
			} else {
				cell.setAttribute("align", "left");
			}
			row.appendChild(cell);
		}
	}

}

function getSkillSet(empId) {

	var req = newXMLHttpRequest();
	req.onreadystatechange = readyStateHandlerText(req, populateSkillset);
	var url = CONTENXT_PATH + "/popupSkillSetWindow.action?empId=" + empId;
	req.open("GET", url, "true");
	req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	req.send(null);
}

function populateSkillset(text) {

	var background = "#3E93D4";
	var title = "Emp Skill Set";
	var text1 = text;
	var size = text1.length;

	var res = text1.split("|");

	var res1 = "";
	for (var i = 0; i < res.length; i++) {
		if ((res[i].trim() == "null") || (res[i].trim() == "null")
				|| (res[i].trim() == "") || (res[i].trim() == " ")) {

		} else {
			if (i == res.length - 1) {
				res1 = res1 + res[i].trim().toUpperCase();
			} else {
				res1 = res1 + res[i].trim().toUpperCase() + ",";
			}
		}
	}

	var res2 = res1.split(",");
	var test2 = "";
	var count = 0;
	for (var i = 0; i < res2.length; i++) {
		for (var j = i; j < res2.length; j++) {
			if (res2[i].trim().toUpperCase() == res2[j].trim().toUpperCase()) {
				count = count + 1;
			} else {
				count = count;
			}

		}

		if (count == 1) {
			if (res2[i].trim() != "") {
				test2 = test2 + res2[i].trim().toUpperCase() + ",";
			}
			count = 0;
		} else {
			count = 0;
		}
	}
	// alert(test2);
	var finalStr1 = test2.slice(0, test2.lastIndexOf(","));
	// alert(finalStr1);
	var finalStr = "";
	if (finalStr1.trim() == "") {
		finalStr = "No Data";
	}

	else {
		finalStr = finalStr1 + ".".big();
	}
	// / alert(test2);

	// Now create the HTML code that is required to make the popup
	var content = "<html><head><title>" + title
			+ "</title></head>\
    <body bgcolor='" + background
			+ "' style='color:white;'>" + finalStr
			+ "<br />\
    </body></html>";
	// alert(content);
	// //alert("text1"+text1);
	// // alert("size "+content.length);
	// var indexof=(content.indexOf("|")+1);
	// var lastindexof=(content.lastIndexOf("|"));

	popup = window
			.open(
					"",
					"window",
					"channelmode=0,width=400,height=200,top=200,left=350,scrollbars=no,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no");

	popup.document.write("<b>SkillS :</b>" + content.substr(0, content.length));
	// Write content into it.
	// Write content into it.

}
/*******************************************************************************
 * Available Employees List Report End
 ******************************************************************************/

/*******************************************************************************
 * Project Experience Details List Start
 ******************************************************************************/

function getProjectExpDetails(totalExp, projectContactId) {

	// alert("totalExp"+totalExp);
	// alert("projectContactId"+projectContactId);
	// var req = newXMLHttpRequest();
	// req.onreadystatechange = readyStateHandlerText(req,
	// populateProjectExp,totalExp);

	var req = newXMLHttpRequest();
	req.onreadystatechange = function() {
		if (req.readyState == 4) {
			if (req.status == 200) {
				populateProjectExp(req.responseText, totalExp);

			}
		} else {
			// alert("HTTP error ---"+req.status+" : "+req.statusText);
		}
	};

	var url = CONTENXT_PATH + "/popupProjectExp.action?projectContactId="
			+ projectContactId;
	req.open("GET", url, "true");
	req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	req.send(null);
}

function populateProjectExp(resText, totalExp) {

	// alert("resText"+resText);
	//	
	// alert("enter into thos me")

	// alert("totalExp"+totalExp);

	var background = "#3E93D4";
	var title = "ProjectExpDetailsList";
	var size = resText.length;

	var text = resText.split("addTo");
	// alert("text[0]..."+text[0]+"text[1].."+text[1]+"text[2]....."+text[2]+"text[3]....."+text[3]+"text[4]....."+text[4]+"text[5]....."+text[5]);

	var content = "<html><head><title>" + title
			+ "</title></head>\
<body bgcolor='" + background
			+ "' style='color:white;'><h4>" + title + "</h4>TotalExp:"
			+ totalExp + "<br/><br>TotalProjectExp:" + text[1] + "<br><br/>\n";

	var tableData = text[0];

	// alert("tblRow" +tblRow);
	if (tableData == "no data") {
		alert("No records found");
	}

	else {
		var tblRow = tableData.split("*@!");
		if (tblRow.length == 1) {

			content = content + "<table border='1' style:align='center'>";

			content = content
					+ "<tr><th>SNO</th><th>StartDate</th><th>EndDate</th><th>Duration</th><th>Status</th></tr>";
			content = content
					+ "<tr> <td colspan='4'> No record found for this  search </td> </tr>";

		}

		else {

			content = content + "<table border='1' style:align='center'>";

			content = content
					+ "<tr><th>SNO</th><th>StartDate</th><th>EndDate</th><th>Duration</th><th>Status</th></tr>";

			for (var n = 0; n < tblRow.length; n++) {
				content = content + "<tr>";
				var tblCol = tblRow[n].split("#^$");
				for (var j = 0; j < tblCol.length; j++) {
					content = content + "<td>" + tblCol[j] + "</td>";
				}
				content = content + "</tr>";
			}
			content = content + "</table>";
		}
	}

	content = content + "</body></html>";

	if (size < 50) {
		// Create the popup
		popup = window
				.open(
						"",
						"window",
						"channelmode=0,width=350,height=300,top=200,left=350,scrollbars=no,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no");
		popup.document.write(content); // Write content into it.
	}

	else if (size < 100) {
		// Create the popup channelmode
		popup = window
				.open(
						"",
						"window",
						"=0,width=400,height=300,top=200,left=350,scrollbars=no,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no");
		popup.document.write(content); // Write content into it.
	}

	else if (size < 260) {
		// Create the popup
		popup = window
				.open(
						"",
						"window",
						"channelmode=0,width=500,height=400,top=200,left=350,scrollbars=no,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no");
		popup.document.write(content); // Write content into it.
	} else {
		// Create the popup
		popup = window
				.open(
						"",
						"window",
						"channelmode=0,width=600,height=500,top=200,left=350,scrollbars=no,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no");
		popup.document.write(content); // Write content into it.
	}

}

/*******************************************************************************
 * Project Experience Details List End
 ******************************************************************************/

/*******************************************************************************
 * Available Employees Details Start
 ******************************************************************************/

function getEmployeeAvailableDetails(EmpId) {
	// alert("Hai"+EmpId);
	// toggleCloseUploadOverlayAvailable();

	var req = newXMLHttpRequest();
	req.onreadystatechange = readyStateHandlerText(req, displayAvailableDetails);
	var url = CONTENXT_PATH + "/getEmployeeAvailableDetails.action?EmpId="
			+ EmpId;
	req.open("GET", url, "true");
	req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	req.send(null);
}

function displayAvailableDetails(response) {

	// alert("response"+response);
	var tableId = document.getElementById("tblAvailableDetails");
	clearTable1(tableId);
	var headerFields = new Array("S.NO", "ProjectName", "StartDate", "EndDate",
			"Resource Type", "Utilization");

	// alert("responseArray[1]-->"+responseArray[1]);
	// document.getElementById("totalRequirementsFound").innerHTML =
	// responseArray[0];
	var dataArray = response;

	// generateTableHeader(tableId,headerFields)
	ParseAndGenerateHTML(tableId, dataArray, headerFields);

	document.getElementById("headerLabel6").style.color = "white";
	document.getElementById("headerLabel6").innerHTML = "Available Employee ProjectDetails";

	var overlay = document.getElementById('overlayAvailableEmployeeDashBoard');
	var specialBox = document
			.getElementById('specialBoxAvailableEmployeeDashBoard');
	overlay.style.opacity = .8;
	if (overlay.style.display == "block") {
		overlay.style.display = "none";
		specialBox.style.display = "none";
	} else {
		overlay.style.display = "block";
		specialBox.style.display = "block";
	}

}

function toggleCloseUploadOverlayAvailable() {

	// alert("Helloooo");
	var overlay = document.getElementById('overlayAvailableEmployeeDashBoard');
	var specialBox = document
			.getElementById('specialBoxAvailableEmployeeDashBoard');

	overlay.style.opacity = .8;
	if (overlay.style.display == "block") {
		overlay.style.display = "none";
		specialBox.style.display = "none";
	} else {
		overlay.style.display = "block";
		specialBox.style.display = "block";
	}
	// window.location="empSearchAll.action";
}

function generateAvailableProjectEmployeeDetailsReport(tableBody, record,
		fieldDelimiter) {
	// alert("heeeeeeeeee");
	var row;
	var cell;
	var fieldLength;
	var fields = record.split(fieldDelimiter);
	fieldLength = fields.length;
	var length = fieldLength;
	row = document.createElement("TR");
	row.className = "gridRowEven";
	tableBody.appendChild(row);
	for (var i = 0; i < length; i++) {
		cell = document.createElement("TD");
		cell.className = "gridColumn";
		cell.innerHTML = fields[i];

		if (fields[i] != '') {
			if (i == 1) {
				cell.setAttribute("align", "left");
			} else {
				cell.setAttribute("align", "left");
			}
			row.appendChild(cell);
		}
	}

}

/*******************************************************************************
 * Available Employees Details End
 ******************************************************************************/

/*******************************************************************************
 * OnProject Active Employees Details Start
 ******************************************************************************/

function getActiveProjectDetails(EmpId) {
	// alert("Hai"+EmpId);
	// toggleCloseUploadOverlayAvailable();
	var req = newXMLHttpRequest();
	req.onreadystatechange = readyStateHandlerText(req,
			displayActiveProjectDetails);
	var url = CONTENXT_PATH + "/getEmployeeActiveProjectDetails.action?EmpId="
			+ EmpId;
	req.open("GET", url, "true");
	req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	req.send(null);
}

function displayActiveProjectDetails(response) {

	// alert("response"+response);
	var tableId = document.getElementById("tblActiveProjectDetails");
	clearTable1(tableId);
	var headerFields = new Array("S.NO", "ProjectName", "Resource Type",
			"Utilization", "StartDate", "EndDate");

	// alert("responseArray[1]-->"+responseArray[1]);
	// document.getElementById("totalRequirementsFound").innerHTML =
	// responseArray[0];
	var dataArray = response;

	// generateTableHeader(tableId,headerFields)
	ParseAndGenerateHTML(tableId, dataArray, headerFields);
	// alert("headerLabel7");

	document.getElementById("headerLabel7").style.color = "white";
	document.getElementById("headerLabel7").innerHTML = "Employees Active ProjectDetails";

	var overlay = document
			.getElementById('overlayActiveProjectDetailsDashBoard');
	var specialBox = document
			.getElementById('specialBoxActiveProjectDetailsDashBoard');
	overlay.style.opacity = .8;
	if (overlay.style.display == "block") {
		overlay.style.display = "none";
		specialBox.style.display = "none";
	} else {
		overlay.style.display = "block";
		specialBox.style.display = "block";
	}

}

function toggleCloseUploadOverlayActiveProjects() {

	// alert("Helloooo");
	var overlay = document
			.getElementById('overlayActiveProjectDetailsDashBoard');
	var specialBox = document
			.getElementById('specialBoxActiveProjectDetailsDashBoard');

	overlay.style.opacity = .8;
	if (overlay.style.display == "block") {
		overlay.style.display = "none";
		specialBox.style.display = "none";
	} else {
		overlay.style.display = "block";
		specialBox.style.display = "block";
	}
	// window.location="empSearchAll.action";
}

function generateActiveProjectDetailsReport(tableBody, record, fieldDelimiter) {
	// alert("heeeeeeeeee");
	var row;
	var cell;
	var fieldLength;
	var fields = record.split(fieldDelimiter);
	fieldLength = fields.length;
	var length = fieldLength;
	row = document.createElement("TR");
	row.className = "gridRowEven";
	tableBody.appendChild(row);
	for (var i = 0; i < length; i++) {
		cell = document.createElement("TD");
		cell.className = "gridColumn";
		cell.innerHTML = fields[i];

		if (fields[i] != '') {
			if (i == 1) {
				cell.setAttribute("align", "left");
			} else {
				cell.setAttribute("align", "left");
			}
			row.appendChild(cell);
		}
	}

}

/*******************************************************************************
 * OnProject Active Employees Details End
 ******************************************************************************/

function getProjectSheets() {
	var startDate = document.getElementById('startDateProj').value;
	var endDate = document.getElementById('endDateProj').value;
	var country = document.getElementById('country2').value;
	var flag = document.getElementById('flog').value;
	var departmentId = document.getElementById('departmentId2').value;

	var ActiveProjectFlag = document.getElementById('ActiveProjects').value;
	// alert(country);
	var checkResult = compareDates(startDate, endDate);
	if (!checkResult) {
		return false;
	}
	if ((flag == "-1")) {
		alert("Please select mandatory fields.");
		return false;
	}
	if ((endDate == "")) {
		alert("Please select mandatory fields.");
		return false;
	}
	if ((startDate == "")) {
		alert("Please select mandatory fields.");
		return false;
	}

	window.location = "../reports/generateProjectXls.action?startDate="
			+ startDate + "&endDate=" + endDate + "&country=" + country
			+ "&flag=" + flag + "&departmentId=" + departmentId
			+ "&activeProjectFlag=" + ActiveProjectFlag;
}
// sarada
function getProjectSheetsForEmployeeExperienceReport() {

	var country = document.getElementById('country4').value;
	var currStatus1 = document.getElementById('currStatus1').value;
	var departmentId = document.getElementById('departmentIdExp').value;
	var practiceId = document.getElementById('practiceIdExp').value;
	var subPractice = document.getElementById('subPracticeExp').value;

	if (practiceId == '--Please Select--' || practiceId == 'All'
			|| practiceId == '-1') {
		practiceId = -1;

	}
	if (subPractice == 'All' || subPractice == '-1'
			|| subPractice == '--Please Select--') {
		subPractice = -1;
	}

	window.location = "../reports/generateEmployeeExperienceReport.action?country="
			+ country
			+ "&currStatus1="
			+ currStatus1
			+ "&departmentId="
			+ departmentId
			+ "&practiceId="
			+ practiceId
			+ "&subPractice="
			+ subPractice;
}

//

function getdatesforProject() {
	// alert("Hai");
	var now = new Date();
	var firstDayPrevMonth = new Date(now.getFullYear(), now.getMonth() - 1, 1);
	// var firstday = firstDayPrevMonth.toString("MM/dd/yyyy");
	var sessionUserId = document.getElementById("userId").value;
	var sessionIsAdminFlag = document.getElementById("isAdminFlag").value;
	var currentYear = firstDayPrevMonth.getFullYear();
	var currentMonth = firstDayPrevMonth.getMonth() + 1;
	var currentDay = firstDayPrevMonth.getDate();
	// alert(currentMonth+"/"+currentDay+"/"+currentYear);

	var startDate = currentMonth + "/" + currentDay + "/" + currentYear;

	var LastDayPrevMonth = new Date(now.getFullYear(), now.getMonth(), 0);
	var lastYear = LastDayPrevMonth.getFullYear();
	var lastMonth = LastDayPrevMonth.getMonth() + 1;
	var lastDay = LastDayPrevMonth.getDate();

	var endDate = lastMonth + "/" + lastDay + "/" + lastYear;
	if (sessionUserId == "rijju" || sessionIsAdminFlag == "YES") {
		document.getElementById('startDateProj').value = startDate;
		document.getElementById('endDateProj').value = endDate;
	}

	// alert("startDateProj "+startDate+" endDateProj "+endDate);
	// startDate=now.getMonth()+"/1/"+now.getFullYear();
	// endDate=now.getMonth()+"/30/"+now.getFullYear();
	document.getElementById('startDateOnboard').value = startDate;
	document.getElementById('endDateOnboard').value = endDate;
	// alert("startDateOnboard "+startDate+" endDateOnboard "+endDate);
}

/*******************************************************************************
 * PF Portal Report Start
 ******************************************************************************/

function getPFPortalReport() {

	var opsContactId = document.getElementById("opsContactId1").value;
	var oTable = document.getElementById("tblPFPortalReport");
	clearTable(oTable);

	var docType = document.getElementById("docTypeId").value;
	if (docType == 'SSN')
		docType = 'PAN';

	var req = newXMLHttpRequest();
	req.onreadystatechange = function() {
		if (req.readyState == 4) {
			if (req.status == 200) {
				document.getElementById("PFPortal").style.display = 'none';
				PFPortalReport(req.responseText);
			}
		} else {
			document.getElementById("PFPortal").style.display = 'block';
			// alert("HTTP error ---"+req.status+" : "+req.statusText);
		}
	};
	var url = CONTENXT_PATH + "/getPFPortalReport.action?opsContactIdForPF="
			+ opsContactId + "&docType=" + docType;
	// alert(url);
	req.open("GET", url, "true");
	req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	req.send(null);
}

function PFPortalReport(response) {

	var oTable = document.getElementById("tblPFPortalReport");
	clearTable(oTable);
	var dataArray = response;
	if (dataArray == "no data") {
		alert("No Records Found for this Search");
	} else {

		var headerFields = new Array("S.No", "EmpNo", "UANNo", "PFNo", "NAME",
				"docNumber", "IfscCode", "PhyChallenged", "PhyCategory",
				"Gender", "MaritalStatus", "Int'lWorker");
		ParseAndGenerateHTML(oTable, dataArray, headerFields);
	}

}

function generatePFPortalReport(tableBody, record, fieldDelimiter) {
	var row;
	var cell;
	var fieldLength;
	var fields = record.split(fieldDelimiter);
	fieldLength = fields.length;
	var length = fieldLength;

	row = document.createElement("TR");
	row.className = "gridRowEven";
	tableBody.appendChild(row);
	for (var i = 0; i < length; i++) {
		cell = document.createElement("TD");
		cell.className = "gridColumn";
		cell.innerHTML = fields[i];
		if (fields[i] != '') {
			if (i == 1) {
				cell.setAttribute("align", "left");
			} else {
				cell.setAttribute("align", "left");
			}
			row.appendChild(cell);
		}
	}

}

/*******************************************************************************
 * PF Portal End
 ******************************************************************************/

/*******************************************************************************
 * PF Portal XL Start
 ******************************************************************************/

function getPFPortalXLReport() {
	var opsContactId = document.getElementById("opsContactId1").value;
	var docType = document.getElementById("docTypeId").value;

	window.location = "generatePFPortalReport.action?opsContactId="
			+ opsContactId + "&docType=" + docType;

}
/*******************************************************************************
 * PF Portal XL End
 ******************************************************************************/
var Billable = 0;
var Shadows = 0;
var Main = 0;
var Training = 0;
var OverHead = 0;

function getProjectDetailsByCustomer() {
	Billable = 0;
	Shadows = 0;
	Main = 0;
	Training = 0;
	OverHead = 0;

	var oTable = document.getElementById("tblEmpBasedOnCustomerReport");
	clearTable1(oTable);
	var customerName = document.getElementById("accountName").value;
	var accountId = document.getElementById("accountId").value;
	var projectId = document.getElementById("projectId").value;
	var currStatus = document.getElementById("currStatus").value;
	var resourceCountry = document.getElementById("resourceCountry").value;

	if (accountId == "-1") {
		alert("please enter customer name");
		return false;
	}
	if (projectId == "-1") {
		alert("please select project");
		return false;
	}
	var req = newXMLHttpRequest();
	req.onreadystatechange = function() {
		if (req.readyState == 4) {
			if (req.status == 200) {
				document.getElementById("employeeReport").style.display = 'none';
				displayEmpBasedOnCustomerReport(req.responseText);
			}
		} else {
			document.getElementById("employeeReport").style.display = 'block';
			// alert("HTTP error ---"+req.status+" : "+req.statusText);
		}
	};
	var url = CONTENXT_PATH + "/getEmployeesBasedOnCustomer.action?accountId="
			+ accountId + "&projectId=" + projectId + "&status=" + currStatus
			+ "&country=" + resourceCountry;

	req.open("GET", url, "true");
	req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	req.send(null);
}

function displayEmpBasedOnCustomerReport(response) {
	// alert(response);
	var oTable = document.getElementById("tblEmpBasedOnCustomerReport");
	clearTable1(oTable);
	// var state=document.getElementById("state").value;
	var dataArray = response;

	if (dataArray == "no data") {
		alert("No Records Found for this Search");
	} else {

		// var headerFields = new
		// Array("S.No","Resource&nbsp;Name","Status","Role","Country","Resource&nbsp;Start&nbsp;date","Resource&nbsp;End&nbsp;date","Billable","Utilization");
		var headerFields = new Array("S.No", "Resource&nbsp;Name", "EmpNo",
				"Status", "Role", "Country", "Resource&nbsp;Start&nbsp;date",
				"Resource&nbsp;End&nbsp;date", "Billable", "Utilization");
		// generateHeader(headerArray,tableId);

		var temp = new Array;
		temp = dataArray.split('addTo');

		if (response != '') {

			ParseAndGenerateHTML(oTable, temp[0], headerFields);
			// document.getElementById("totalCount").innerHTML =
			// temp[1]+"(Billable:"+Billable+"+ Shadows:"+Shadows+")";
			document.getElementById("totalCount").innerHTML = temp[1]
					+ "( Main(Billable):" + Billable + "+ Main:"
					+ (Main - Billable) + "+ Shadows:" + Shadows
					+ "<br/> Training:" + Training + "+ OverHead:" + OverHead
					+ ")";
		} else {
			alert('No Result For This Search...');
			// spnFast.innerHTML="No Result For This Search...";
		}
	}

}

function generateEmpBasedOnCustomerReport(tableBody, record, fieldDelimiter) {
	var row;
	var cell;
	var fieldLength;
	var fields = record.split(fieldDelimiter);
	fieldLength = fields.length;
	var state = document.getElementById("state").value;
	var length = fieldLength;
	row = document.createElement("TR");
	row.className = "gridRowEven";
	tableBody.appendChild(row);
	for (var i = 0; i < length; i++) {

		if (i != 9) {
			cell = document.createElement("TD");
			cell.className = "gridColumn";
			cell.innerHTML = fields[i];
		}
		if (i == 8) {
			if (fields[i] == 'YES')
				Billable = parseInt(Billable, 10) + 1;
		}

		if (i == 9) {
			if (fields[i] == 'Main')
				Main = parseInt(Main, 10) + 1;
			else if (fields[i] == 'Shadow')
				Shadows = parseInt(Shadows, 10) + 1;
			else if (fields[i] == 'Training')
				Training = parseInt(Training, 10) + 1;
			else if (fields[i] == 'OverHead')
				OverHead = parseInt(OverHead, 10) + 1;
		}

		if (fields[i] != '') {
			if (i == 1) {
				cell.setAttribute("align", "left");
			} else {
				cell.setAttribute("align", "left");
			}
			row.appendChild(cell);
		}
	}

}

function clearTable1(tableId) {
	var tbl = tableId;
	var lastRow = tbl.rows.length;
	while (lastRow > 0) {
		tbl.deleteRow(lastRow - 1);
		lastRow = tbl.rows.length;
	}
}
// onboard report dashboard start
function getOnBoardReport() {

	var oTable = document.getElementById("tblOnboardData");
	clearTable(oTable);
	// alert("in getOnBoardReport");
	var startDate = document.getElementById('startDateOnboard').value;
	var endDate = document.getElementById('endDateOnboard').value;
	var country = document.getElementById('country3').value;
	var flag = document.getElementById('flog1').value;
	var departmentId3 = document.getElementById('departmentId3').value;
	var itgBatch = document.getElementById('itgBatch').value;

	var checkResult = compareDates(startDate, endDate);
	if (!checkResult) {
		return false;
	}

	if ((flag == "-1")) {
		alert("Please select mandatory fields.");
		return false;
	}
	if ((endDate == "")) {
		alert("Please select mandatory fields.");
		return false;
	}
	if ((startDate == "")) {
		alert("Please select mandatory fields.");
		return false;
	}

	var req = newXMLHttpRequest();
	req.onreadystatechange = function() {
		if (req.readyState == 4) {
			if (req.status == 200) {
				document.getElementById("onBoardReport").style.display = 'none';
				displayOnboardReport(req.responseText);
			}
		} else {
			document.getElementById("onBoardReport").style.display = 'block';
		}
	};
	var url = CONTENXT_PATH + "/getOnboardReport.action?country=" + country
			+ "&startDate=" + startDate + "&endDate=" + endDate + "&flag="
			+ flag + "&departmentName=" + departmentId3 + "&itgBatch="
			+ itgBatch;

	req.open("GET", url, "true");
	req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	req.send(null);
}

function displayOnboardReport(response) {
	// alert(response);
	var oTable = document.getElementById("tblOnboardData");
	clearTable1(oTable);
	var flag = document.getElementById('flog1').value;
	var dataArray = response;

	if (dataArray == "no data") {
		alert("No Records Found for this Search");
	} else {
		if (flag == "1") {
			var headerFields = new Array("S.No", "MSS/CNE", "EmpId", "LName",
					"FName", "Gender", "DoJ", "ITG", "Department", "Practice",
					"SubPractice", "Location", "Other");
		}
		if (flag == "2") {
			var headerFields = new Array("S.No", "MSS/CNE", "EmpId", "LName",
					"FName", "Gender", "DoJ", "ITG", "Department", "Practice",
					"SubPractice", "Location", "other",
					"Roll-Off&nbsp;&nbsp;Date");
		}
		ParseAndGenerateHTML(oTable, dataArray, headerFields);
	}

}

function generateOnboardReport(tableBody, record, fieldDelimiter) {

	var adminFlag = document.getElementById('isAdminFlag').value;
	var rollOffAccess = document.getElementById('rollOffAccess').value;
	var userManager = document.getElementById('userManager').value;

	var row;
	var cell;
	var fieldLength;
	var fields = record.split(fieldDelimiter);
	fieldLength = fields.length;
	var length = fieldLength;

	row = document.createElement("TR");
	row.className = "gridRowEven";
	tableBody.appendChild(row);
	// for (var i=0;i<length;i++) {
	// i==0 contains employee Id
	for (var i = 1; i <= length; i++) {
		cell = document.createElement("TD");
		cell.className = "gridColumn";

		if (fields[i] != '') {
			// alert("fields[10]" +fields[10] );
			// alert(i);
			var data = fields[12].split("|");

			var finalString2 = data[1];

			// alert("finalString2"+finalString2);

			if (parseInt(i, 10) == 12) {

				cell.innerHTML = finalString2;
			}

			else if (parseInt(i, 10) == 13) {
				cell.innerHTML = "<a href='javascript:getOtherDetails(\""
						+ fields[12] + "\")'>View</a>";
			}

			else if (parseInt(i, 10) == 14) {

				if (adminFlag == 'YES' || rollOffAccess == "1"
						|| userManager == "1") {

					cell.innerHTML = "<a href='javascript:getReasonForExit(\""
							+ fields[0] + "\")'>" + fields[13] + "</a>";
				} else {
					cell.innerHTML = fields[13];

				}
			} else {
				cell.innerHTML = fields[i];
			}

			if (i == 1) {
				cell.setAttribute("align", "left");
			} else {
				cell.setAttribute("align", "left");
			}
			row.appendChild(cell);
		}
	}

}

// onboard report dashboard end

function disbaleDates() {
	// alert("test")
	var flag = document.getElementById('flog').value;
	document.getElementById('startDateProj').readOnly = false;
	// $("#test").prop("onclick", null);
	if (flag == 1 || flag == 2) {
		document.getElementById('ActiveProjects').disabled = false;
	} else {
		document.getElementById('ActiveProjects').disabled = true;
	}

	if (flag == 1 || flag == 3 || flag == 6) {
		var endDate = document.getElementById('endDateProj').value;
		// alert(endDate);
		var d = new Date(endDate);
		// alert(d);
		var month = d.getMonth() + 1;
		var year = d.getFullYear();
		// alert(month+" "+year);
		var statDate = month + '/01/' + year;
		// alert(statDate);
		document.getElementById('startDateProj').value = statDate;
		document.getElementById('startDateProj').readOnly = true;
		document.getElementById('startDateProj').disabled = true;
		$("#startDateTag").removeAttr("href");
	}
	if (flag == 2 || flag == 4 || flag == 5) {
		// $("#test").prop("onclick", null);
		document.getElementById('startDateProj').readOnly = false;
		document.getElementById('startDateProj').disabled = false;
		$("#startDateTag").attr("href", "javascript:cal7.popup();");
	}

}

function changeStartDateByEnddate() {
	// alert("test1")
	var flag = document.getElementById('flog').value;
	// alert(flag)
	if (flag == 1 || flag == 3 || flag == 6) {

		var endDate = document.getElementById('endDateProj').value;
		// alert(endDate);
		var d = new Date(endDate);
		// alert(d);
		var month = d.getMonth() + 1;
		var year = d.getFullYear();
		// alert(month+" "+year);
		var statDate = month + '/01/' + year;
		// alert(statDate);
		document.getElementById('startDateProj').value = statDate;
		// alert("please select")
	}
}

function getOtherDetails(data) {
	// alert(data);
	var background = "#3E93D4";
	var title = "Emp Other Details";
	var data1 = data.split("|");
	var finalString1 = "Reporting Manager : " + data1[0];
	var finalString2 = "Current Location : " + data1[1];
	var finalString3 = "HR Contact : " + data1[2];
	var content = "<html><head><title>" + title
			+ "<br /></title></head>\
    <body bgcolor='" + background
			+ "' style='color:white;'><br />" + finalString1 + "<br />"
			+ finalString2 + "<br />" + finalString3
			+ "<br />\
    </body></html>";
	// alert(content);
	// //alert("text1"+text1);
	// // alert("size "+content.length);
	// var indexof=(content.indexOf("|")+1);
	// var lastindexof=(content.lastIndexOf("|"));

	popup = window
			.open(
					"",
					"window",
					"channelmode=0,width=400,height=200,top=200,left=350,scrollbars=no,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no");

	popup.document.write("<b>Emp Other Dtails :</b>"
			+ content.substr(0, content.length));
}
// missing fields dashboard start
function getMissingFileldsReport() {

	var oTable = document.getElementById("tblMissingData");
	clearTable(oTable);
	// alert("in getOnBoardReport");
	document.getElementById("totalCount2").innerHTML = '';
	// var country = document.getElementById('country3').value;
	var missingField = document.getElementById('missingField').value;
	var location = document.getElementById('location').value;
	var country = document.getElementById('countryForMissingData').value;
	var opsContactId = document.getElementById('opsContactIdRMD').value;
	var Practice4 = document.getElementById("practiceIdTSR").value;
	// alert("in getOnBoardReport"+Practice4);
	document.getElementById("recordDisplay").style.display = 'none';
	// document.getElementByClassName("pagination").style.display = 'none';
	$(".pagination").css("display", "none");
	if (missingField == '-1') {
		alert("please select the Based on field!");
		return false;
	}

	document.getElementById("missingInfoSearch").disabled = true;
	
	var req = newXMLHttpRequest();
	
	
	req.onreadystatechange = function() {
		
		
		if (req.readyState == 4) {
			if (req.status == 200) {
				
				 document.getElementById("missingInfoSearch").disabled = false;
				document.getElementById("missingFileldReport").style.display = 'none';
				displayMissingDataReport(req.responseText);
				
				 
			}
		} else {
			document.getElementById("missingFileldReport").style.display = 'block';
		}
	};
	// var url =
	// CONTENXT_PATH+"/getOnboardReport.action?country="+country+"&startDate="+startDate+"&endDate="+endDate+"&flag="+flag;
	var url = CONTENXT_PATH + "/getMissingDataReport.action?missedField="
			+ missingField + "&country=" + country + "&location=" + location
			+ "&opsContactId=" + opsContactId + "&practiceId=" + Practice4;

	req.open("GET", url, "true");
	req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	req.send(null);
}

function displayMissingDataReport(response) {
	// alert(response);
	var oTable = document.getElementById("tblMissingData");
	clearTable1(oTable);
	// var flag = document.getElementById('flog1').value;
	var dataArray = response;

	if (dataArray == "no data") {
		alert("No Records Found for this Search");
	} else {

		var temp2 = dataArray.split('addTo');

		// var headerFields = new Array("S.No","Employee
		// Name","E-Mail","Work-Phone");
		var headerFields = new Array("S.No", "Employee Name", "Employee No",
				"E-Mail", "Work-Phone");

		ParseAndGenerateHTML(oTable, temp2[0], headerFields);
		document.getElementById("recordDisplay").style.display = 'block';

		document.getElementById("totalCount2").innerHTML = temp2[1];
		if (temp2[1] == undefined) {
			document.getElementById("totalCount2").innerHTML = 0;
		}

		// document.getElementByClassName("pagination").style.display = 'block';
		$(".pagination").css("display", "block");
	}
	$('#tblMissingData').tablePaginate({
		navigateType : 'navigator'
	}, recordPage);
}

function generateMissingDataReport(tableBody, record, fieldDelimiter) {
	var row;
	var cell;
	var fieldLength;
	var fields = record.split(fieldDelimiter);
	fieldLength = fields.length;
	var length = fieldLength;

	row = document.createElement("TR");
	row.className = "gridRowEven";
	tableBody.appendChild(row);
	for (var i = 0; i < length; i++) {
		cell = document.createElement("TD");
		cell.className = "gridColumn";
		cell.innerHTML = fields[i];
		if (fields[i] != '') {

			cell.setAttribute("align", "left");

			row.appendChild(cell);
		}
	}

}

// missing fields dashboard end

function getLocationsByCountry(obj) {

	var req = newXMLHttpRequest();
	req.onreadystatechange = readyStateHandler(req, populateEmployeeLocations);
	var url = CONTENXT_PATH + "/getEmployeeLocations.action?country="
			+ obj.value;
	req.open("GET", url, "true");
	req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	req.send(null);
}

function populateEmployeeLocations(resXML) {
	var locationObj = document.getElementById("location");
	var country = resXML.getElementsByTagName("COUNTRY")[0];
	// var teamMember = team.getElementsByTagName("TEAMMEMBER");
	locationObj.innerHTML = " ";
	for (var i = 0; i < country.childNodes.length; i++) {
		var location = country.childNodes[i];
		var id = location.getElementsByTagName("LOCATION-ID")[0];
		var name = location.getElementsByTagName("LOCATION-NAME")[0];
		// alert(id.childNodes[0].nodeValue);
		var opt = document.createElement("option");
		opt.setAttribute("value", id.childNodes[0].nodeValue);
		opt.appendChild(document.createTextNode(name.childNodes[0].nodeValue));
		// alert(name.childNodes[0].nodeValue);
		locationObj.appendChild(opt);
	}
}
function getLocationsByCountryOnload() {
	var country = document.getElementById("countryForMissingData").value;

	var req = newXMLHttpRequest();
	req.onreadystatechange = readyStateHandler(req, populateEmployeeLocations);
	var url = CONTENXT_PATH + "/getEmployeeLocations.action?country=" + country;
	req.open("GET", url, "true");
	req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	req.send(null);
}

function getEmployeeContactDetails(empId) {

	var req = newXMLHttpRequest();
	req.onreadystatechange = readyStateHandlerText(req, populateContactDetails);
	var url = CONTENXT_PATH + "/popupContactsWindow.action?empId=" + empId;
	req.open("GET", url, "true");
	req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	req.send(null);
}

function populateContactDetails(text) {
	var background = "#3E93D4";
	var title = "Contact Details:";
	var text1 = text;
	var size = text1.length;

	// Now create the HTML code that is required to make the popup
	var content = "<html><head><title>" + title
	+ "</title></head>\
	<body bgcolor='" + background
	+ "' style='color:white;'>" + text1 + "<br />\
	</body></html>";

	// alert("text1"+text1);
	// alert("size "+content.length);
	var indexof = (content.indexOf("^") + 1);
	var lastindexof = (content.lastIndexOf("^"));

	var strArray = text.split("^");


	popup = window
	.open(
	"",
	"window",
	"channelmode=0,width=300,height=200,top=200,left=350,scrollbars=no,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no");
	if (content.indexOf("^")) {
	// alert(content.substr(0,content.indexOf("//")));

	popup.document.write("<b>Reports To : </b>"+content.substr(0,content.indexOf("^")));
	popup.document.write("<br><br>");
	popup.document.write("<b>Location : </b>"+strArray[1]);
	popup.document.write("<br><br>");
	popup.document.write("<b>Email : </b>"+strArray[2]);
	popup.document.write("<br><br>");
	popup.document.write("<b>Work Phone No : </b>"+strArray[3]);
	popup.document.write("<br><br>");

	}

	}

function getEmployeeExpDetails(mss, prev) {

	var background = "#3E93D4";
	var title = "Experience Details:";

	// var size = text1.length;

	// Now create the HTML code that is required to make the popup
	var content = "<html><head><title>" + title
			+ "</title></head>\
    <body bgcolor='" + background
			+ "' style='color:white;'><br />\
    </body></html>";

	// alert("text1"+text1);
	// alert("size "+content.length);

	popup = window
			.open(
					"",
					"window",
					"channelmode=0,width=300,height=150,top=200,left=350,scrollbars=no,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no");
	if (content.indexOf("^")) {
		// alert(content.substr(0,content.indexOf("//")));
		popup.document.write("<b>Mss EXP : </b>" + mss);
		popup.document.write("<br><br>");
		popup.document.write("<b>Prev Exp :</b>" + prev);
		popup.document.write(content);

	}

}

function getSubPracticeData2() {

	var practiceName = document.getElementById("practiceId1").value;

	var req = newXMLHttpRequest();
	req.onreadystatechange = readyStateHandler(req, populateSubPractices);
	var url = CONTENXT_PATH + "/getEmpPractice.action?practiceName="
			+ practiceName;
	req.open("GET", url, "true");
	req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	req.send(null);

}

function populateSubPractices(resXML) {
	// alert(resXML);
	var subPractice = document.getElementById("subPractice1");

	var practice = resXML.getElementsByTagName("PRACTICE")[0];

	var subPractices = practice.getElementsByTagName("SUBPRACTICE");
	subPractice.innerHTML = " ";

	for (var i = 0; i < subPractices.length; i++) {
		var subPracticeName = subPractices[i];

		var name = subPracticeName.firstChild.nodeValue;
		var opt = document.createElement("option");
		if (i == 0) {
			opt.setAttribute("value", "All");
		} else {
			opt.setAttribute("value", name);
		}
		opt.appendChild(document.createTextNode(name));
		subPractice.appendChild(opt);
	}
}
function getSubPracticeData3() {

	var practiceName = document.getElementById("practiceIdExp").value;

	var req = newXMLHttpRequest();
	req.onreadystatechange = readyStateHandler(req, populateSubPractices1);
	var url = CONTENXT_PATH + "/getEmpPractice.action?practiceName="
			+ practiceName;
	req.open("GET", url, "true");
	req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	req.send(null);

}

function populateSubPractices1(resXML) {
	// alert(resXML);
	var subPractice = document.getElementById("subPracticeExp");

	var practice = resXML.getElementsByTagName("PRACTICE")[0];

	var subPractices = practice.getElementsByTagName("SUBPRACTICE");
	subPractice.innerHTML = " ";

	for (var i = 0; i < subPractices.length; i++) {
		var subPracticeName = subPractices[i];

		var name = subPracticeName.firstChild.nodeValue;
		var opt = document.createElement("option");
		if (i == 0) {
			opt.setAttribute("value", "All");
		} else {
			opt.setAttribute("value", name);
		}
		opt.appendChild(document.createTextNode(name));
		subPractice.appendChild(opt);
	}
}
function getProjectStatus() {

	document.getElementById("projectStatusSpan").innerHTML = '';

	var projectId = document.getElementById("projectId").value;
	// alert(projectId);
	if (projectId == "-1") {
		alert("please select project");
		return false;
	}
	var req = newXMLHttpRequest();
	req.onreadystatechange = function() {
		if (req.readyState == 4) {
			if (req.status == 200) {

				displayProjectStatus(req.responseText);
			}
		} else {

		}
	};
	var url = CONTENXT_PATH + "/getProjectStatusById.action?projectId="
			+ projectId;

	req.open("GET", url, "true");
	req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	req.send(null);

}

function displayProjectStatus(resTest) {
	// alert(resTest);
	// (document.getElementById("projStatus")).style.display = "none";
	var res = resTest.split("#^$");

	// document.getElementById("projectStatusSpan").innerHTML='<font
	// color="green">'+res[0]+','+res[1]+','+res[2]+'</font>';
	document.getElementById("projectStatusSpan").innerHTML = '<font color="green" size="2px">Project Status:<b>'
			+ res[0]
			+ '</b><br>&nbsp;&nbsp;&nbsp;Start Date:<b>'
			+ res[1]
			+ '</b><br>&nbsp;&nbsp;&nbsp;End Date<b>' + res[2] + '</b></font>';
}

function hideRow(id) {
	// alert(id);
	var row = document.getElementById(id);
	row.style.display = 'none';
}
function showRow(id) {
	// alert(id);
	var row = document.getElementById(id);
	row.style.display = '';
}

function displayUtilizationDiv() {
	// alert(state);
	var state = document.getElementById("state").value;
	if (state == 'Available') {

		document.getElementById("utilizationDiv").style.visibility = 'visible';
		document.getElementById("utilizationField").style.visibility = 'visible';

	} else {
		document.getElementById("utilizationDiv").style.visibility = 'hidden';
		document.getElementById("utilizationField").style.visibility = 'hidden';
	}
}

function getProjectUtilizationDetails(empId, state) {
	var req = newXMLHttpRequest();
	req.onreadystatechange = function() {
		if (req.readyState == 4) {
			if (req.status == 200) {

				displayOnProjectEmployeeUtilizationDetails(req.responseText);
			}
		} else {

		}
	};
	var url = CONTENXT_PATH
			+ "/getOnProjectEmployeeUtilizationDetails.action?empId=" + empId;

	req.open("GET", url, "true");
	req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	req.send(null);
}

function displayOnProjectEmployeeUtilizationDetails(response) {
	var tableId = document.getElementById("tblProjectUtilDetails");
	clearTable(tableId);
	var headerFields = new Array("ProjectName", "StartDate", "EndDate",
			"Utilization", "Billable");
	var dataArray = response;

	// generateTableHeader(tableId,headerFields)
	ParseAndGenerateHTML(tableId, dataArray, headerFields);

	document.getElementById("headerLabel1").style.color = "white";
	document.getElementById("headerLabel1").innerHTML = "Project Details";

	var overlay = document.getElementById('overlayProjectUtilDetails');
	var specialBox = document.getElementById('specialBoxProjectUtilDetails');
	overlay.style.opacity = .8;
	if (overlay.style.display == "block") {
		overlay.style.display = "none";
		specialBox.style.display = "none";
	} else {
		overlay.style.display = "block";
		specialBox.style.display = "block";
	}

}

function toggleCloseUploadOverlay1() {
	var overlay = document.getElementById('overlayProjectUtilDetails');
	var specialBox = document.getElementById('specialBoxProjectUtilDetails');

	overlay.style.opacity = .8;
	if (overlay.style.display == "block") {
		overlay.style.display = "none";
		specialBox.style.display = "none";
	} else {
		overlay.style.display = "block";
		specialBox.style.display = "block";
	}
	// window.location="empSearchAll.action";
}

function generateProjectUtilDetails(tableBody, record, fieldDelimiter) {
	var row;
	var cell;
	var fieldLength;
	var fields = record.split(fieldDelimiter);
	fieldLength = fields.length;
	var length = fieldLength;

	row = document.createElement("TR");
	row.className = "gridRowEven";
	tableBody.appendChild(row);
	for (var i = 0; i < length; i++) {
		cell = document.createElement("TD");
		cell.className = "gridColumn";
		cell.innerHTML = fields[i];
		if (fields[i] != '') {

			cell.setAttribute("align", "left");

			row.appendChild(cell);
		}
	}

}

/*
 * Description : Reason for exit Comments Authror : Teja Kadamanti Date :
 * 08/31/2017
 */

function getReasonForExit(empId) {
	var req = newXMLHttpRequest();
	req.onreadystatechange = function() {
		if (req.readyState == 4) {
			if (req.status == 200) {
				popUpReasonForExit(req.responseText);
			}
		} else {

		}
	};
	var url = CONTENXT_PATH + "/getReasonForExit.action?empId=" + empId;

	req.open("GET", url, "true");
	req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	req.send(null);
}

function popUpReasonForExit(data) {
	var background = "#3E93D4";

	var title = "Termination Details";
	var response = data.split("#^$");

	var content = "<html><head><title>" + title
			+ "</title><br /><br /></head>\
    <body bgcolor='" + background
			+ "' style='color:white;'>Exit Type:  " + response[0]
			+ "<br /><br />\Reason For Exit :<br />" + response[1]
			+ "<br />\
    </body></html>";

	popup = window
			.open(
					"",
					"window",
					"channelmode=0,width=400,height=200,top=200,left=350,scrollbars=no,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no");

	popup.document.write("<b>Termination Details </b>"
			+ content.substr(0, content.length));
}

// //////////////////////////////////////////////////

function EmployeeForStarPerformer() {
	var test = document.getElementById("resourceName1").value;

	if (test == "") {

		clearTable11();
		hideScrollBar();
		var validationMessage = document
				.getElementById("authorEmpValidationMessage1");
		validationMessage.innerHTML = "";

		document.getElementById("preAssignEmpId1").value = "";

	} else {

		if (test.length > 2) {
			var url = CONTENXT_PATH
					+ "/getEmployeeDetailforPMOActivity.action?customerName="
					+ escape(test);
			var req = initRequest(url);
			req.onreadystatechange = function() {
				if (req.readyState == 4) {
					if (req.status == 200) {
						// alert("req.responseXML"+req.responseXML);
						parseEmpMessagesForProject(req.responseXML);
					} else if (req.status == 204) {
						clearTable11();
					}
				}
			};
			req.open("GET", url, true);

			req.send(null);
		}
	}
}
var completeTable1;
function init12() {
	completeTable1 = document.getElementById("completeTable1");
}

function clearTable11() {
	// alert(completeTable)

	if (completeTable1) {
		completeTable1.setAttribute("bordercolor", "white");
		completeTable1.setAttribute("border", "0");
		completeTable1.style.visible = false;
		for (loop = completeTable1.childNodes.length - 1; loop >= 0; loop--) {
			completeTable1.removeChild(completeTable1.childNodes[loop]);
		}
	}
}

function hideScrollBar() {
	autorow = document.getElementById("menu-popup1");
	autorow.style.display = 'none';
}

function initRequest(url) {

	if (window.XMLHttpRequest) {
		return new XMLHttpRequest();
	} else if (window.ActiveXObject) {
		isIE = true;
		return new ActiveXObject("Microsoft.XMLHTTP");
	}

}
function set_emp(eName, eID) {
	clearTable11();

	document.getElementById("resourceName1").value = eName;
	document.getElementById("preAssignEmpId1").value = eID;
	// document.starPerformance.resourceName1.value = eName;
	// document.starPerformance.preAssignEmpId1.value = eID;

}
var isIE;

function appendEmployeeForProject(empId, empName) {
	// alert(empId);
	var row;
	var nameCell;
	if (!isIE) {
		row = completeTable1.insertRow(completeTable1.rows.length);
		nameCell = row.insertCell(0);
	} else {
		row = document.createElement("tr");
		nameCell = document.createElement("td");
		row.appendChild(nameCell);
		completeTable1.appendChild(row);
	}
	row.className = "popupRow";
	nameCell.setAttribute("bgcolor", "#3E93D4");
	var linkElement = document.createElement("a");
	linkElement.className = "popupItem";
	linkElement.setAttribute("href", "javascript:set_emp('" + empName + "','"
			+ empId + "')");

	linkElement.appendChild(document.createTextNode(empName));
	linkElement["onclick"] = new Function("hideScrollBar()");
	nameCell.appendChild(linkElement);
}
function findPosition(oElement) {
	if (typeof (oElement.offsetParent) != undefined) {
		for (var posX = 0, posY = 0; oElement; oElement = oElement.offsetParent) {
			posX += oElement.offsetLeft;
			posY += oElement.offsetTop;
		}
		return posX + "," + posY;
	} else {
		return oElement.x + "," + oElement.y;
	}
}

function parseEmpMessagesForProject(responseXML) {

	autorow1 = document.getElementById("menu-popup1");
	autorow1.style.display = "none";
	autorow = document.getElementById("menu-popup1");
	autorow.style.display = "none";
	clearTable11();
	// alert("-->"+responseXML);
	var employees = responseXML.getElementsByTagName("EMPLOYEES")[0];
	if (employees.childNodes.length > 0) {
		completeTable1.setAttribute("bordercolor", "black");
		completeTable1.setAttribute("border", "0");
	} else {
		clearTable11();
	}
	if (employees.childNodes.length < 10) {
		autorow1.style.overflowY = "hidden";
		autorow.style.overflowY = "hidden";
	} else {
		autorow1.style.overflowY = "scroll";
		autorow.style.overflowY = "scroll";
	}

	var employee = employees.childNodes[0];
	var chk = employee.getElementsByTagName("VALID")[0];
	if (chk.childNodes[0].nodeValue == "true") {

		var validationMessage;

		validationMessage = document
				.getElementById("authorEmpValidationMessage1");
		isPriEmpExist = true;

		validationMessage.innerHTML = "";
		document.getElementById("menu-popup1").style.display = "block";
		for (loop = 0; loop < employees.childNodes.length; loop++) {

			var employee = employees.childNodes[loop];
			var customerName = employee.getElementsByTagName("NAME")[0];
			var empid = employee.getElementsByTagName("EMPID")[0];
			appendEmployeeForProject(empid.childNodes[0].nodeValue,
					customerName.childNodes[0].nodeValue);
		}
		var position;

		position = findPosition(document.getElementById("resourceName1"));
		// alert(position);
		var posi = position.split(",");
		document.getElementById("menu-popup1").style.left = posi[0] + "px";
		document.getElementById("menu-popup1").style.top = (parseInt(posi[1]) + 20)
				+ "px";
		document.getElementById("menu-popup1").style.display = "block";
	} // if
	if (chk.childNodes[0].nodeValue == "false") {
		var validationMessage = '';

		isPriEmpExist = false;
		validationMessage = document
				.getElementById("authorEmpValidationMessage1");

		validationMessage.innerHTML = " Invalid ! Select from Suggesstion List. ";
		validationMessage.style.color = "green";
		validationMessage.style.fontSize = "12px";

		document.getElementById("resourceName1").value = "";
		document.getElementById("preAssignEmpId1").value = "";

	}
}

function toggleCloseUploadOverlay2() {
	var overlay = document.getElementById('overlayExperienceReport');
	var specialBox = document.getElementById('specialBoxExperienceReport');

	overlay.style.opacity = .8;
	if (overlay.style.display == "block") {
		overlay.style.display = "none";
		specialBox.style.display = "none";
	} else {
		overlay.style.display = "block";
		specialBox.style.display = "block";
	}
	// window.location="empSearchAll.action";
}

function expTenureReports(response) {
	var tableId = document.getElementById("tblExpReports");
	clearTable(tableId);
	var headerFields = new Array("Status", "Tenure", "Details");
	var dataArray = response;

	// generateTableHeader(tableId,headerFields)
	expTenureReportsGenerate(tableId, dataArray, headerFields);

	document.getElementById("headerLabel2").style.color = "white";
	document.getElementById("headerLabel2").innerHTML = "Tenure Details";

	var overlay = document.getElementById('overlayExperienceReport');
	var specialBox = document.getElementById('specialBoxExperienceReport');
	overlay.style.opacity = .8;
	if (overlay.style.display == "block") {
		overlay.style.display = "none";
		specialBox.style.display = "none";
	} else {
		overlay.style.display = "block";
		specialBox.style.display = "block";
	}

}
function expTenureReportsGenerate(oTable, responseString, headerFields) {

	var records = responseString.split("*@!");
	// alert("records---->"+records);

	var tbody = oTable.childNodes[0];
	tbody = document.createElement("TBODY");
	oTable.appendChild(tbody);
	generateTableHeaderExp(tbody, headerFields);
	var rowlength;

	rowlength = records.length;

	for (var i = 0; i < rowlength - 1; i++) {

		var loop = records[i].split("#^$");

		var row = document.createElement("TR");
		row.className = "gridRowEven";
		tbody.appendChild(row);
		var cell = document.createElement("TD");
		cell.className = "gridColumn";
		var str = loop[0] + '';
		if (loop[2] != '-') {
			str = str + '<br>(' + loop[2] + ')';
		}
		if (loop[3] != '-') {
			str = str + '<br>(' + loop[3] + ')';
		}
		if (loop[4] != '-') {
			str = str + '<br>(' + loop[4] + ')';
		}
		if (loop[5] != '-') {
			str = str + '<br>(' + loop[5] + ')';
		}
		if (loop[6] != '-') {
			str = str + '<br>(' + loop[6] + ')';
		}
		cell.innerHTML = str;
		cell.setAttribute("align", "left");
		row.appendChild(cell);

		var cell = document.createElement("TD");
		cell.className = "gridColumn";
		cell.innerHTML = loop[7];
		cell.setAttribute("align", "left");
		row.appendChild(cell);

		var cell1 = document.createElement("TD");
		cell1.className = "gridColumn";

		if (loop[0] == "OnProject") {

			var content = "<table align='center' width='100%' cellpadding='1' cellspacing='1' border='0' class='gridTable'><tr class='gridHeader1' style='background:#A9A9A9 url(../images/DBGrid/bg_table_heading.jpg) repeat top left'><th>From Date</th><th>ToDate</th><th>ResType</th><th>Project Status</th>"
					+ "<th>IsBillable</th><th>Utilization</th><th>Duration</th></tr>";
			var loop1 = loop[1].split("##");
			// /alert("loop1-----"+loop[1]);
			for (var j = 0; j < loop1.length - 1; j++) {

				var loop2 = loop1[j].split("$$");
				content = content + "<tr class='gridRowEven'><td>" + loop2[0]
						+ "</td><td>" + loop2[1] + "</td><td>" + loop2[2]
						+ "  - (" + loop2[3] + ")</td><td>" + loop2[4]
						+ "</td><td>" + loop2[5] + "</td><td>" + loop2[6]
						+ "</td><td>" + loop2[7] + "</td></tr>";

			}

			content = content + "</table>";
			cell1.innerHTML = content;
		} else if (loop[0] == "Training") {
			var content = "<table align='center' width='100%' cellpadding='1' cellspacing='1' border='0' class='gridTable'><tr class='gridHeader1' style='background:#A9A9A9 url(../images/DBGrid/bg_table_heading.jpg) repeat top left'><th>From Date</th><th>ToDate</th><th width='100px'>Skills</th><th>Duration</th></tr>";

			var loop1 = loop[1].split("##");
			// alert("loop1-----"+loop[1]);
			for (var j = 0; j < loop1.length - 1; j++) {

				var loop2 = loop1[j].split("$$");
				content = content + "<tr class='gridRowEven'><td>" + loop2[0]
						+ "</td><td>" + loop2[1] + "</td><td>" + loop2[2]
						+ "</td><td>" + loop2[3] + "</td></tr>";

			}
			content = content + "</table>";

			cell1.innerHTML = content;
		} else {

			cell1.innerHTML = "";
		}

		// /////////////////////////////////////

		cell1.setAttribute("align", "center");
		row.appendChild(cell1);

	}

}

function getEmpExperienceRpt() {

	var oTable = document.getElementById("tblExperienceRptData");
	clearTable(oTable);
	// alert("in getOnBoardReport");
	document.getElementById("tblExperienceRptResultMsg").style.display = 'block';

	var country = document.getElementById('countryForExperienceRpt').value;

	var location = document.getElementById('locationForExperienceRpt').value;
	var opsContactId = document.getElementById('opsContactIdForExp').value;
	var practiceId = document.getElementById('practiceIdForprojectExcelReport').value;
	if (practiceId == "All") {
		practiceId = "";
	}
	// alert(opsContactId)
	if (location == '-1') {
		location = "";
	}

	var empId = document.getElementById('preAssignEmpId1').value;
	;
	// alert("bbb "+empId);
	var departmentName = document
			.getElementById('departmentIdForExperienceRpt').value;
	// alert(departmentName);
	var expMonth = document.getElementById('expMonth').value;
	var expYear = document.getElementById('expYear').value;

	var objId;
	if (document.getElementById("expFlag").checked) {
		objId = 1;
	} else {
		objId = 0;
	}
	document.getElementById("recordDisplayExpRpt").style.display = 'none';
	// document.getElementByClassName("pagination").style.display = 'none';

	$(".pagination").css("display", "none");
	// if(missingField=='-1'){
	// alert("please select the Based on field!");
	// return false;
	// }

	var req = newXMLHttpRequest();
	req.onreadystatechange = function() {
		if (req.readyState == 4) {
			if (req.status == 200) {
				document.getElementById("tblExperienceRptResultMsg").style.display = 'none';
				displayExpReport(req.responseText);
			}
		} else {
			// document.getElementById("missingFileldReport").style.display =
			// 'block';
		}
	};
	// var url =
	// CONTENXT_PATH+"/getOnboardReport.action?country="+country+"&startDate="+startDate+"&endDate="+endDate+"&flag="+flag;
	var url = CONTENXT_PATH + "/getExperienceReport.action?country=" + country
			+ "&location=" + location + "&departmentId=" + departmentName
			+ "&expMonth=" + expMonth + "&expYear=" + expYear + "&objId="
			+ objId + "&empId=" + empId + "&opsContactId=" + opsContactId
			+ "&practiceId=" + practiceId + "";

	req.open("GET", url, "true");
	req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	req.send(null);
}

function displayExpReport(response) {
	// alert(response);
	var oTable = document.getElementById("tblExperienceRptData");
	clearTable1(oTable);
	// var flag = document.getElementById('flog1').value;
	var dataArray = response;

	if (dataArray == "NoData") {
		alert("No Records Found for this Search");
	} else {

		// var headerFields = new Array("S.No","Employee
		// Name","E-Mail","Department","ITG Batch","DOJ","Tenure
		// Compeleted","Status");
		var headerFields = new Array("S.No", "Employee Name", "Employee No",
				"E-Mail", "Department", "ITG Batch", "DOJ",
				"Tenure Compeleted", "Status");

		ParseAndGenerateHTMLExp(oTable, dataArray, headerFields);
		document.getElementById("recordDisplayExpRpt").style.display = 'block';
		// / document.getElementByClassName("pagination").style.display =
		// 'block';
		$(".pagination").css("display", "block");
		pagerOption1();
	}

}

function generateExpReports(tableBody, record, fieldDelimiter) {
	// alert(record);
	var row;
	var cell;
	var fieldLength;
	var fields = record.split(fieldDelimiter);
	fieldLength = fields.length;
	var length = fieldLength;

	row = document.createElement("TR");
	row.className = "gridRowEven";
	tableBody.appendChild(row);
	for (var i = 0; i < length - 3; i++) {
		cell = document.createElement("TD");
		cell.className = "gridColumn";
		cell.innerHTML = fields[i];
		if (fields[i] != '') {

			cell.setAttribute("align", "left");

			row.appendChild(cell);
		}
	}
	cell = document.createElement("TD");
	cell.className = "gridColumn";
	cell.innerHTML = fields[length - 2];
	row.appendChild(cell);
	cell = document.createElement("TD");
	cell.className = "gridColumn";

	var j = document.createElement("a");

	j.setAttribute("href", "javascript:getTenurStatus('" + fields[length - 3]
			+ "')");
	j.appendChild(document.createTextNode(fields[length - 1]));
	cell.appendChild(j);
	row.appendChild(cell);

}

function ParseAndGenerateHTMLExp(oTable, responseString, headerFields) {

	// alert("ParseAndGenerateHTML");
	var start = new Date();
	var fieldDelimiter = "#^$";
	var recordDelimiter = "*@!";
	// alert('responseString%%%% ******'+responseString);
	// alert('rowCount%%%% ******'+rowCount);

	var records = responseString.split(recordDelimiter);
	// alert("records---->"+records);
	generateTable(oTable, headerFields, records, fieldDelimiter);
}

function getTenurStatus(loginId) {
	// alert(loginId);
	var req = newXMLHttpRequest();
	req.onreadystatechange = readyStateHandlerText(req, expTenureReports);
	var url = CONTENXT_PATH + "/popupTenurStatus.action?loginId=" + loginId
			+ "";
	req.open("GET", url, "true");
	req.setRequestHeader("Content-Type", "application/x-www-form-urlencodetd");
	req.send(null);
}

function generateTableHeaderExp(tableBody, headerFields) {
	var row;
	var cell;
	row = document.createElement("TR");
	row.className = "gridHeader";
	tableBody.appendChild(row);
	for (var i = 0; i < headerFields.length; i++) {
		cell = document.createElement("TD");
		cell.className = "gridHeader";
		row.appendChild(cell);

		cell.setAttribute("width", "10000px");
		cell.setAttribute("align", "left");
		cell.innerHTML = headerFields[i];
	}
}

function getLocationsByCountryForExpRpt(obj) {

	var req = newXMLHttpRequest();

	req.onreadystatechange = readyStateHandler(req,
			populateEmployeeLocationsForExpRpt);

	var url = CONTENXT_PATH + "/getEmployeeLocations.action?country="
			+ obj.value;

	req.open("GET", url, "true");

	req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

	req.send(null);

}

function populateEmployeeLocationsForExpRpt(resXML) {

	var locationObj = document.getElementById("locationForExperienceRpt");

	var country = resXML.getElementsByTagName("COUNTRY")[0];

	// var teamMember = team.getElementsByTagName("TEAMMEMBER");

	locationObj.innerHTML = " ";

	for (var i = 0; i < country.childNodes.length; i++) {

		var location = country.childNodes[i];

		var id = location.getElementsByTagName("LOCATION-ID")[0];

		var name = location.getElementsByTagName("LOCATION-NAME")[0];

		// alert(id.childNodes[0].nodeValue);

		var opt = document.createElement("option");

		opt.setAttribute("value", id.childNodes[0].nodeValue);

		opt.appendChild(document.createTextNode(name.childNodes[0].nodeValue));

		// alert(name.childNodes[0].nodeValue);

		locationObj.appendChild(opt);

	}

}

function getProjectsByAccountId() {

	var accountId = document.getElementById("customerName").value;
	if (accountId != "") {
		var req = newXMLHttpRequest();
		req.onreadystatechange = readyStateHandler(req, populateProjectsList);
		var url = CONTENXT_PATH + "/getProjectsByAccountId.action?accountId="
				+ accountId;
		req.open("GET", url, "true");
		req.setRequestHeader("Content-Type",
				"application/x-www-form-urlencoded");
		req.send(null);
	} else {
		var projects = document.getElementById("projectName");
		projects.innerHTML = " ";

		var opt = document.createElement("option");
		opt.setAttribute("value", "");
		opt.appendChild(document.createTextNode("All"));
		projects.appendChild(opt);
	}
}
function populateProjectsList(resXML) {

	// var projects = document.getElementById("projectName");
	var projects = document.getElementById("projectName");
	// alert("test")

	var projectsList = resXML.getElementsByTagName("PROJECTS")[0];

	var users = projectsList.getElementsByTagName("USER");
	projects.innerHTML = " ";
	for (var i = 0; i < users.length; i++) {
		var userName = users[i];
		var att = userName.getAttribute("projectId");
		var name = userName.firstChild.nodeValue;
		var opt = document.createElement("option");
		opt.setAttribute("value", att);
		opt.appendChild(document.createTextNode(name));
		projects.appendChild(opt);
	}
}

function getEmpProjectExcelRpt() {

	// alert("in getOnBoardReport");
	document.getElementById("tblExperienceRptResultMsg").style.display = 'block';

	// document.getElementById("resultMessageForfrmprojectExcelReport").innerHTML
	// = "";
	// document.getElementById("resultMessageForfrmprojectExcelReport").style.display
	// = 'none';

	var startDate = document.getElementById('projectExcelReportStartDate').value;
	var endDate = document.getElementById('projectExcelReportEndDate').value;
	var country = document.getElementById('countryForExperienceRpt').value;

	var location = document.getElementById('locationForExperienceRpt').value;
	var opsContactId = document.getElementById('opsContactIdForExp').value;
	var practiceId = document.getElementById('practiceIdForprojectExcelReport').value;
	if (practiceId == "All") {
		practiceId = "";
	}
	// alert(opsContactId)
	if (location == '-1') {
		location = "";
	}
	if (opsContactId == '') {
		opsContactId = "-1";
	}

	var empId = document.getElementById('preAssignEmpId1').value;
	;
	// alert("bbb "+empId);
	var departmentId = document.getElementById('departmentIdForExperienceRpt').value;
	// var departmentId =
	// document.getElementById('departmentIdForExperienceRpt').value;
	if (departmentId == "All") {
		departmentId = "-1";
	}
	window.location = "getEmpProjectExcelRpt.action?country=" + country
			+ "&startDate=" + startDate + "&endDate=" + endDate
			+ "&departmentId=" + departmentId + "&practiceId=" + practiceId
			+ "&location=" + location + "&empId=" + empId + "&opsContactId="
			+ opsContactId + "";
	document.getElementById("tblExperienceRptResultMsg").style.display = 'none';

}

function getEmpProjectRpt() {
	document.getElementById("tblExperienceRptResultMsg").style.display = 'block';
	var tableId = document.getElementById("tblExperienceRptData");
	clearTable(tableId);
	var startDate = document.getElementById('projectExcelReportStartDate').value;
	var endDate = document.getElementById('projectExcelReportEndDate').value;
	var country = document.getElementById('countryForExperienceRpt').value;

	var location = document.getElementById('locationForExperienceRpt').value;
	var opsContactId = document.getElementById('opsContactIdForExp').value;
	var practiceId = document.getElementById('practiceIdForprojectExcelReport').value;
	if (practiceId == "All") {
		practiceId = "";
	}
	var departmentId = document.getElementById('departmentIdForExperienceRpt').value;
	if (departmentId == "All") {
		departmentId = "";
	}
	// alert(opsContactId)
	if (location == '-1') {
		location = "";
	}
	if (opsContactId == '') {
		opsContactId = "-1";
	}

	var empId = document.getElementById('preAssignEmpId1').value;
	;
	// alert("bbb "+empId);

	var req = newXMLHttpRequest();
	req.onreadystatechange = function() {
		if (req.readyState == 4) {
			if (req.status == 200) {

				displayProjectDetailsExcelReport(req.responseText);
			}
		} else {

		}
	};
	var url = CONTENXT_PATH + "/getEmpProjectExcelReport.action?country="
			+ country + "&startDate=" + startDate + "&endDate=" + endDate
			+ "&departmentId=" + departmentId + "&practiceId=" + practiceId
			+ "&location=" + location + "&empId=" + empId + "&opsContactId="
			+ opsContactId + "";

	req.open("GET", url, "true");
	req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	req.send(null);
}

function displayProjectDetailsExcelReport(response) {
	var tableId = document.getElementById("tblExperienceRptData");
	clearTable(tableId);
	var headerFields = new Array("Resource Name", "Training", "Available",
			"OverHead", "R&DPOC", "OnProjectCount", "ProjecjtDetils");
	var dataArray = response;

	// generateTableHeader(tableId,headerFields)
	ProjectDetailsExcelReportGenerate(tableId, dataArray, headerFields);
	document.getElementById("tblExperienceRptResultMsg").style.display = 'none';

}

function ProjectDetailsExcelReportGenerate(oTable, responseString, headerFields) {

	var records = responseString.split("$$$");
	// alert("records---->"+records);

	var tbody = oTable.childNodes[0];
	tbody = document.createElement("TBODY");
	oTable.appendChild(tbody);
	generateTableHeaderExp(tbody, headerFields);
	var rowlength;

	rowlength = records.length;

	for (var i = 0; i < rowlength - 1; i++) {

		var loop = records[i].split("^^^");

		var row = document.createElement("TR");
		row.className = "gridRowEven";
		tbody.appendChild(row);

		var cell = document.createElement("TD");
		cell.className = "gridColumn";

		cell.innerHTML = loop[0];
		cell.setAttribute("align", "left");
		row.appendChild(cell);

		var cell = document.createElement("TD");
		cell.className = "gridColumn";
		cell.innerHTML = loop[1];
		cell.setAttribute("align", "left");
		row.appendChild(cell);

		var cell = document.createElement("TD");
		cell.className = "gridColumn";
		cell.innerHTML = loop[2];
		cell.setAttribute("align", "left");
		row.appendChild(cell);

		var cell = document.createElement("TD");
		cell.className = "gridColumn";
		cell.innerHTML = loop[3];
		cell.setAttribute("align", "left");
		row.appendChild(cell);

		var cell = document.createElement("TD");
		cell.className = "gridColumn";
		cell.innerHTML = loop[4];
		cell.setAttribute("align", "left");
		row.appendChild(cell);

		var cell = document.createElement("TD");
		cell.className = "gridColumn";
		cell.innerHTML = loop[5];
		cell.setAttribute("align", "left");
		row.appendChild(cell);

		// alert(loop[5])

		var cell1 = document.createElement("TD");
		cell1.className = "gridColumn";

		// alert(loop[6])
		if (loop[6] != "") {

			var content = "<table align='center' width='100%' cellpadding='1' cellspacing='1' border='0' class='gridTable'><tr class='gridHeader1' style='background:#A9A9A9 url(../images/DBGrid/bg_table_heading.jpg) repeat top left'><th>Project</th><th>OverHead</th><th>MainBillable</th>"
					+ "<th>Main</th><th>Shadow</th><th>Training</th></tr>";
			var loop1 = loop[6].split("@@");
			// /alert("loop1-----"+loop[1]);
			for (var j = 0; j < loop1.length - 1; j++) {

				var loop2 = loop1[j].split("##");
				if (j == loop1.length - 2) {

					content = content
							+ "<tr class='gridHeader1' style='background:#5F9EA0 url(../images/DBGrid/bg_table_heading.jpg) repeat top left'><td>"
							+ loop2[0] + "</td><td>" + loop2[1] + "</td><td>"
							+ loop2[2] + "</td><td>" + loop2[3] + "</td><td>"
							+ loop2[4] + "</td><td>" + loop2[5] + "</td></tr>";
				} else {
					content = content + "<tr class='gridRowEven'><td>"
							+ loop2[0] + "</td><td>" + loop2[1] + "</td><td>"
							+ loop2[2] + "</td><td>" + loop2[3] + "</td><td>"
							+ loop2[4] + "</td><td>" + loop2[5] + "</td></tr>";
				}

			}

			content = content + "</table>";
			cell1.innerHTML = content;
		} else {

			cell1.innerHTML = "";
		}

		cell1.setAttribute("align", "center");
		row.appendChild(cell1);

	}

}

function getPracticeDataV44() {

	var departmentName = document
			.getElementById("departmentIdForExperienceRpt").value;
	var req = newXMLHttpRequest();
	req.onreadystatechange = readyStateHandler(req, populatePracticesForDataV44);
	var url = CONTENXT_PATH + "/getEmpDepartment.action?departmentName="
			+ departmentName;
	req.open("GET", url, "true");
	req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	req.send(null);

}

function populatePracticesForDataV44(resXML) {

	var practiceId = document.getElementById("practiceIdForprojectExcelReport");
	var department = resXML.getElementsByTagName("DEPARTMENT")[0];
	var practices = department.getElementsByTagName("PRACTICE");
	practiceId.innerHTML = " ";

	for (var i = 0; i < practices.length; i++) {
		var practiceName = practices[i];

		var name = practiceName.firstChild.nodeValue;
		var opt = document.createElement("option");
		if (i == 0) {
			opt.setAttribute("value", "All");
		} else {
			opt.setAttribute("value", name);
		}
		opt.appendChild(document.createTextNode(name));
		practiceId.appendChild(opt);
	}
}

function ClrDashBordTable(myHTMLTable) {
	var tbl = myHTMLTable;

	tbl.deleteTHead();
	var lastRow = tbl.rows.length;
	while (lastRow > 0) {
		tbl.deleteRow(lastRow - 1);
		lastRow = tbl.rows.length;
	}
}

function getSalesReviewsStatusReport() {

	var tableId = document.getElementById("tblReviewStatusReport");
	ClrDashBordTable(tableId);
	document.getElementById("button_pageNation").innerHTML = "";

	var startDateSales = document.getElementById("startDateSales").value;
	var endDateSales = document.getElementById("endDateSales").value;
	var teamMemberIdSales = document.getElementById("teamMemberIdSales").value;

	var country = document.getElementById("countrySales").value;

	if (startDateSales == '') {
		alert("Please select start date");
		return false;
	}

	if (endDateSales == '') {
		alert("Please select end date");
		return false;
	}
	document.getElementById("loadActMessageReviews").style.display = 'block';

	var req = newXMLHttpRequest();
	req.onreadystatechange = function() {
		if (req.readyState == 4) {
			if (req.status == 200) {
				document.getElementById("loadActMessageReviews").style.display = 'none';
				displaySalesReviewStatusReport(req.responseText);
			}
		} else {
			document.getElementById("loadActMessageReviews").style.display = 'block';
			// alert("HTTP error ---"+req.status+" : "+req.statusText);
		}
	};
	var url = CONTENXT_PATH + "/reviewSalesStatusReport.action?startDate="
			+ startDateSales + "&endDate=" + endDateSales + "&teamMemberId="
			+ teamMemberIdSales + "&country=" + country;
	// alert(url);
	req.open("GET", url, "true");
	req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	req.send(null);

}
function displaySalesReviewStatusReport(response) {
	// alert("response..."+response);
	var oTable = document.getElementById("tblReviewStatusReport");

	ClrDashBordTable(oTable);

	var dataArray = response;

	if (dataArray == " " || dataArray == null) {
		alert("No Records Found for this Search");
	} else {
		/*
		 * var headerFields = new Array("SNo", "Name", "Exp", "ReportsTo",
		 * "TotalActivities", "Quote&nbsp;Procured", "Quote&nbsp;Submitted",
		 * "Quote&nbsp;Won", "CallsSchd", "CallsExe", "OnSiteVisitSchd",
		 * "OnSiteVisitExe", "ConfMeetSchd", "ConfMeetExe", "Oppr.Punched",
		 * "Oppr.Won", "Req Punched", "PositionsClosed", "NewLogo");
		 */
		/*
		 * var headerFields = new Array("SNo", "Name", "Exp", "ReportsTo",
		 * "TotalActivities", "Quote&nbsp;Procured",
		 * "Quote&nbsp;Submitted","Software&nbsp;Renewals&nbsp;Procured","Software&nbsp;New&nbsp;License&nbsp;Procured","Quote&nbsp;Won",
		 * "CallsSchd", "CallsExe", "OnSiteVisitSchd", "OnSiteVisitExe",
		 * "ConfMeetSchd", "ConfMeetExe", "Oppr.Punched", "Oppr.Won", "Req
		 * Punched",
		 * "PositionsClosed","New&nbsp;Software&nbsp;License&nbsp;sold","NewLogo","Closure");
		 */

		var headerFields = new Array("SNo", "Emp&nbsp;Id", "Name", "Exp",
				"Team", "ReportsTo", "TotalActivities", "Campaign",
				"Email-OutBound", "Quote&nbsp;Procured",
				"Quote&nbsp;Submitted", "Software&nbsp;Renewals&nbsp;Procured",
				"Software&nbsp;New&nbsp;License&nbsp;Procured",
				"Quote&nbsp;Won", "CallsSchd", "CallsExe", "OnSiteVisitSchd",
				"OnSiteVisitExe", "ConfMeetSchd", "ConfMeetExe",
				"Oppr.Punched", "Oppr.Won", "Req Punched", "PositionsClosed",
				"Software&nbsp;Renewal",
				"New&nbsp;Software&nbsp;License&nbsp;Sold", "NewLogo",
				"Closure");

		ParseAndGenerateHTML(oTable, dataArray, headerFields);
	}
	$("#tblReviewStatusReport").tableHeadFixer({

		'foot' : false,
		'left' : 7,
		'head' : true
	});

}

function generateSalesReviewsStatusReport(tableBody, record, fieldDelimiter) {
	var row;
	var cell;
	var fieldLength;
	var fields = record.split(fieldDelimiter);
	fieldLength = fields.length;
	var length = fieldLength;

	row = document.createElement("TR");
	row.className = "gridRowEven";
	tableBody.appendChild(row);
	for (var i = 0; i < length; i++) {
		cell = document.createElement("TD");
		cell.className = "gridColumn";
		cell.innerHTML = fields[i];
		if (fields[i] != '') {
			if (i == 1) {
				cell.setAttribute("align", "left");
			} else {
				cell.setAttribute("align", "left");
			}
			row.appendChild(cell);
		}
	}
}

/*******************************************************************************
 * OffShore Employee Experience Report
 ******************************************************************************/

function getOffshoreEmployeeExperienceSearch() {
	// alert("in getOffshoreEmployeeExperienceSearch");
	var country = document.getElementById('country4').value;
	var status = document.getElementById('currStatus1').value;

	var departmentId1 = document.getElementById('departmentIdExp').value;
	var practiceId1 = document.getElementById('practiceIdExp').value;
	var subPractice1 = document.getElementById('subPracticeExp').value;
	// var Curstatus4 = document.getElementById('Curstatus4').value;
	if (practiceId1 == '--Please Select--' || practiceId1 == 'All'
			|| practiceId1 == '-1') {
		practiceId1 = -1;

	}
	if (subPractice1 == 'All' || subPractice1 == '-1'
			|| subPractice1 == '--Please Select--') {
		subPractice1 = -1;
	}
	document.getElementById("loadActMessageReviews1").style.display = 'block';

	// alert("departmentId1"+departmentId1);

	var oTable = document.getElementById("tblemployeeExperienceReport");
	clearTable(oTable);
	var req = newXMLHttpRequest();
	req.onreadystatechange = function() {
		if (req.readyState == 4) {
			if (req.status == 200) {
				/* document.getElementById("PFPortal").style.display = 'none'; */
				document.getElementById("loadActMessageReviews1").style.display = 'none';
				OffshoreEmployeeExperienceSearch(req.responseText);
			}
		} else {
			document.getElementById("loadActMessageReviews1").style.display = 'block';
			/* document.getElementById("PFPortal").style.display = 'block'; */
			// alert("HTTP error ---"+req.status+" : "+req.statusText);
		}
	};
	var url = CONTENXT_PATH
			+ "/getOffshoreEmployeeExperienceSearch.action?country=" + country
			+ "&status=" + status + "&departmentId1=" + departmentId1
			+ "&practiceId1=" + practiceId1 + "&subPractice1=" + subPractice1; // +
																				// "&Curstatus4="
																				// +
																				// Curstatus4;
	// alert(url);
	req.open("GET", url, "true");
	req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	req.send(null);
}

function OffshoreEmployeeExperienceSearch(response) {

	// alert("response" + response);

	var oTable = document.getElementById("tblemployeeExperienceReport");
	clearTable(oTable);
	var dataArray = response;
	if (dataArray == "no data") {
		alert("No Records Found for this Search");
	} else {

		var headerFields = new Array("S.No", "MSS/CNE", "EmpId", "LName",
				"FName", "Itg&nbsp;batch", "Date&nbsp;Of&nbsp;Join",
				"Prev&nbsp;Exp", "MSS&nbsp;Exp", "Total&nbsp;Exp",
				"CurrStatus", "ProjectName", "Practice", "SubPractice",
				"Overhead(Months)", "Main&nbsp;Billable(Months)",
				"Shadow(Months)", "Main(Months)", "Available(Months)",
				"Training(Months)");
		ParseAndGenerateHTML(oTable, dataArray, headerFields);
	}

	$("#tblemployeeExperienceReport").tableHeadFixer({

		'foot' : false,
		'left' : 5,
		'head' : true
	});

}

function generateOffShoreEmployeeExperienceReport(tableBody, record,
		fieldDelimiter) {
	var row;
	var cell;
	var fieldLength;
	var fields = record.split(fieldDelimiter);
	fieldLength = fields.length;
	var length = fieldLength;

	row = document.createElement("TR");
	row.className = "gridRowEven";
	tableBody.appendChild(row);
	for (var i = 0; i < length; i++) {
		cell = document.createElement("TD");
		cell.className = "gridColumn";
		cell.innerHTML = fields[i];
		if (fields[i] != '') {
			if (i == 1) {
				cell.setAttribute("align", "left");
			} else {
				cell.setAttribute("align", "left");
			}
			row.appendChild(cell);
		}
	}

}

/*******************************************************************************
 * OffShore Employee Experience DashBoard End
 ******************************************************************************/

/*******************************************************************************
 * OnProject Employees Statstics Start:
 ******************************************************************************/

function getonProjectEmployeesStatisticsXLReport() {

	var country = document.getElementById('country7').value;
	var reportBasedOn = document.getElementById('reportBasedOn').value;
	window.location = "../reports/generateonProjectEmployeesStatisticsXLReport.action?country="
			+ country + "&reportBasedOn=" + reportBasedOn;
}

/*******************************************************************************
 * sales performance review report
 ******************************************************************************/

function getSalesReviewsStatusReview() {

	var tableId = document.getElementById("tblReviewStatusReview");
	ClrDashBordTable(tableId);
	document.getElementById("button_pageNation1").innerHTML = "";

	var startDateSales = document.getElementById("startDateSales1").value;
	var endDateSales = document.getElementById("endDateSales1").value;
	var teamMemberIdSales = document.getElementById("teamMemberIdSales1").value;
	var subPracticeId = document.getElementById("subpracticeIdSales1").value;
	var country = document.getElementById("countrySales1").value;
	// alert("country"+country);
	if (startDateSales == '') {
		alert("Please select start dates");
		return false;
	}

	if (endDateSales == '') {
		alert("Please select end dates");
		return false;
	}
	/*var date1 = new Date(startDateSales);
	
	
	var date2 = new Date(endDateSales);
	

	
	var yearDiff = date1.getFullYear() - date2.getFullYear();
	var year1 = date1.getFullYear();
	

	var year2 = date2.getFullYear();
	var monthDiff = (date2.getMonth() + year2 * 12)
			- (date1.getMonth() + year1 * 12);
	
	if ( monthDiff == 1  ||  monthDiff + 1 == 3 || monthDiff + 1 == 6 || monthDiff + 1 == 12) {
		// alert("please enter valid dates");
		// return false;
	} else {
		alert("please enter valid dates");
		return false;

	}*/
	
	
	
   var date1 = new Date(startDateSales);
	
	
	var date2 = new Date(endDateSales);
	
	if(date1 > date2){
 		alert("StartDate must lessthan EndDate");
 		return false;
 		}
 		 
     var firstDay =  
         new Date(date1.getFullYear(), date1.getMonth(), 1); 
           
     var lastDay =  
        new Date(date2.getFullYear(), date2.getMonth() + 1, 0); 
	
	
     if(date1.getTime() !=  firstDay.getTime())
    	 {
    
    	 alert("Please Select StartDate Of Month");
    	 return false;
    	 }
    
     
     
     
     if(date2.getTime() !=  lastDay.getTime())
	 {

    	 alert("Please Select EndDate Of Month");
    	 return false;
	 }

	
     
 	
     
	document.getElementById("loadActMessageReviews12").style.display = 'block';

	var req = newXMLHttpRequest();
	req.onreadystatechange = function() {
		if (req.readyState == 4) {
			if (req.status == 200) {
				document.getElementById("loadActMessageReviews12").style.display = 'none';
				displaySalesReviewStatusReview(req.responseText);
			}
		} else {
			document.getElementById("loadActMessageReviews12").style.display = 'block';
			// alert("HTTP error ---"+req.status+" : "+req.statusText);
		}
	};
	var url = CONTENXT_PATH + "/reviewSalesStatusReview.action?startDate="
			+ startDateSales + "&endDate=" + endDateSales + "&teamMemberId="
			+ teamMemberIdSales + "&subPracticeId=" + subPracticeId
			+ "&country=" + country;
	// alert(url);
	req.open("GET", url, "true");
	req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	req.send(null);

}
/*
function daysInMonth(month, year) { 
    return new Date(year, month, 0).getDate(); 
} 
*/
function displaySalesReviewStatusReview(response) {
	// alert("response..."+response);
	var oTable = document.getElementById("tblReviewStatusReview");

	ClrDashBordTable(oTable);

	var dataArray = response;

	if (dataArray == " " || dataArray == null) {
		alert("No Records Found for this Search");
	} else {
		// alert("hiii");
		var headerFields = new Array(
				"Emp&nbsp;ID",
				"Name",
				"Reporting",
				"Experience",
				"Team",
				"From&nbsp;date",
				"To&nbsp;date",
				"Standard&nbsp;Performance&nbsp;Status&nbsp;%(Capped&nbsp;to&nbsp;100%)",
				"Standard&nbsp;Performance&nbsp;Grade",
				"Business&nbsp;Performance&nbsp;Status",
				"Standard&nbsp;Performance&nbsp;Status&nbsp;%(Uncapped)",
				"Schd&nbsp;Calls&nbsp;%", "Calls&nbsp;Exe&nbsp;%",
				"Meetings&nbsp;Schedulled&nbsp;%",
				"Meetings&nbsp;Executed&nbsp;%", "Oppr&nbsp;Punched&nbsp;%",
				"Req Punched&nbsp;%", "Quote&nbsp;Procured&nbsp;%",
				"Quote&nbsp;Submitted&nbsp;%",
				"Software&nbsp;Renewals&nbsp;Procured&nbsp;%",
				"Software&nbsp;New&nbsp;License&nbsp;Procured&nbsp;%",
				"Positions&nbsp;Closed", "Software&nbsp;Renewal",
				"New&nbsp;Software&nbsp;License&nbsp;sold", "New&nbsp;Logo");

		ParseAndGenerateHTML(oTable, dataArray, headerFields);
	}
	$("#tblReviewStatusReview").tableHeadFixer({

		'foot' : false,
		'left' : 5,
		'head' : true
	});

}

function generateSalesReviewsStatusReview(tableBody, record, fieldDelimiter) {
	var row;
	var cell;
	var fieldLength;
	var fields = record.split(fieldDelimiter);
	fieldLength = fields.length;
	var length = fieldLength;

	row = document.createElement("TR");
	row.className = "gridRowEven";
	tableBody.appendChild(row);
	for (var i = 0; i < length; i++) {
		cell = document.createElement("TD");
		cell.className = "gridColumn";
		cell.innerHTML = fields[i];
		if (fields[i] == 'Excellent') {
			// fields[i].style.backgroundColor = "yellow";
			cell.setAttribute("style", "background-color: yellow;")
		} else if (fields[i] == 'Very Good') {
			// fields[i].style.backgroundColor = "Green";
			cell.setAttribute("style", "background-color: Green;")
		} else if (fields[i] == 'Good') {
			// fields[i].style.backgroundColor = "Blue";
			cell.setAttribute("style", "background-color: Blue;")
		} else if (fields[i] == 'No Revenue') {
			// fields[i].style.backgroundColor = "Red";
			cell.setAttribute("style", "background-color: Red;")
		} else if (fields[i] == 'Fair') {
			// fields[i].style.backgroundColor = "Red";
			cell.setAttribute("style", "background-color: Green;")
		} else if (fields[i] == 'Moderate') {
			// fields[i].style.backgroundColor = "Red";
			cell.setAttribute("style", "background-color: orange;")
		} else if (fields[i] == 'Low') {
			// fields[i].style.backgroundColor = "Red";
			cell.setAttribute("style", "background-color: pink;")
		} else if (fields[i] == 'Poor') {
			// fields[i].style.backgroundColor = "Red";
			cell.setAttribute("style", "background-color: Red;")
		} else if (fields[i] == 'No Performance') {
			// fields[i].style.backgroundColor = "Red";
			cell.setAttribute("style", "background-color: Red;")
		}

		if (fields[i] != '') {
			if (i == 1) {
				cell.setAttribute("align", "left");
			} else {
				cell.setAttribute("align", "left");
			}
			row.appendChild(cell);
		}
	}
}

function getExceldataforSalesperformancestatusreview() {

	var subPractice = document.getElementById('subpracticeIdSales1').value;
	var teamMember = document.getElementById('teamMemberIdSales1').value;
	var endDate = document.getElementById('endDateSales1').value;
	var startDate = document.getElementById('startDateSales1').value;
	var country = document.getElementById('countrySales1').value;
	
	if (teamMember == '--Please Select--') {
		teamMember = -1;

	}
	if (subPractice == 'All') {
		subPractice = -1;
	}

	window.location = "../reports/generatesalesperformancereviewdata.action?startDate="
			+ startDate
			+ "&endDate="
			+ endDate
			+ "&teamMember="
			+ teamMember
			+ "&subPractice=" + subPractice + "&country=" + country;
}


/*NagaLakshmi Telluri
 * 9/17/2019
 * 
 * */


function readyStateHandler80(req, responseXmlHandler) {
	return function() {
	if (req.readyState == 4) {
	if (req.status == 200) {

	responseXmlHandler(req.responseXML);
	} else {
	alert("HTTP error" + req.status + " : " + req.statusText);
	}
	}
	}
	}


function getSalesTeamMembersByCountry() {

	var country = document.getElementById("countrySales").value;

	var req = newXMLHttpRequest();
	req.onreadystatechange = readyStateHandler80(req, populateTeamMembersSales);
	var url = CONTENXT_PATH + "/getSalesTeamMemberByCountry.action?country="
	+ country;


	req.open("GET", url, "true");
	req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	req.send(null);

	}



function populateTeamMembersSales(resXML) {

	

	var teamMemberIdSales = document.getElementById("teamMemberIdSales");
	var country = resXML.getElementsByTagName("COUNTRY")[0];
	var employeeNames = country.getElementsByTagName("EMPLOYEENAME");
	teamMemberIdSales.innerHTML = " ";

	for (var i = 0; i < employeeNames.length; i++) {
	var employeeName = employeeNames[i];
	var att = employeeName.getAttribute("userId");
	var name = employeeName.firstChild.nodeValue;
	var opt = document.createElement("option");
	opt.setAttribute("value",att);
	opt.appendChild(document.createTextNode(name));
	teamMemberIdSales.appendChild(opt);
	}

}
	
	function getSalesReviewTeamMembersByCountry() {

		var country = document.getElementById("countrySales1").value;

		var req = newXMLHttpRequest();
		req.onreadystatechange = readyStateHandler80(req, populateTeamMembersSalesReview);
		var url = CONTENXT_PATH + "/getSalesTeamMemberByCountry.action?country="
		+ country;


		req.open("GET", url, "true");
		req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		req.send(null);

		}



	function populateTeamMembersSalesReview(resXML) {

		

		var teamMemberIdSales = document.getElementById("teamMemberIdSales1");
		var country = resXML.getElementsByTagName("COUNTRY")[0];
		var employeeNames = country.getElementsByTagName("EMPLOYEENAME");
		teamMemberIdSales.innerHTML = " ";

		for (var i = 0; i < employeeNames.length; i++) {
		var employeeName = employeeNames[i];
		var att = employeeName.getAttribute("userId");
		var name = employeeName.firstChild.nodeValue;
		var opt = document.createElement("option");
		opt.setAttribute("value",att);
		opt.appendChild(document.createTextNode(name));
		teamMemberIdSales.appendChild(opt);
		}

	


	}