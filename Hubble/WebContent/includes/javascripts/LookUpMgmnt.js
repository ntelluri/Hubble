/**
 * 
 */

var totalRec=0;
var completeTable;
var autorow1;
var autorow;
function newXMLHttpRequest() {
    var xmlreq = false;
    if(window.XMLHttpRequest) {
        xmlreq = new XMLHttpRequest();
    } else if(window.ActiveXObject) {
        try {
            xmlreq = new ActiveXObject("MSxm12.XMLHTTP");
        } catch(e1) {
            try {
                xmlreq = new ActiveXObject("Microsoft.XMLHTTP");
            } catch(e2) {
                xmlreq = false;
            }
        }
    }
    return xmlreq;
}

function readyStateHandler(req,responseXmlHandler) {
    return function() {
        if (req.readyState == 4) {
            if (req.status == 200) {
                responseXmlHandler(req.responseXML);
            } else {
                alert("HTTP error"+req.status+" : "+req.statusText);
            }
        }
    }

}
function readyStateHandlerText(req,responseTextHandler){
    return function() {
        if (req.readyState == 4) {
            if (req.status == 200) {               
                responseTextHandler(req.responseText);
            } else {
                alert("HTTP error"+req.status+" : "+req.statusText);
            }
        }
    }
} 



//function init() {
//    
//    
//    //var menu = document.getElementById("auto-row");
//    //var menu1 = document.getElementById("auto-row1");
//	alert("init");
//    autorow = document.getElementById("menu-popup");
//    autorow.style.display ="none";
//    autorow1 = document.getElementById("menu-popup");
//    autorow1.style.display ="none";
//    //autorow.style.top = getElementY(menu) + "px";
//    //autorow1.style.top = getElementY(menu1) + "px";
//    var height = document.documentElement.clientHeight - 120;
//    autorow1.style.height = Math.max(height, 150);
//    autorow1.style.overflowY = "scroll";
//    autorow.style.height = Math.max(height, 150);
//    autorow.style.overflowY = "scroll";
//    
//    completeTable = document.getElementById("completeTable");    
//    completeTable.setAttribute("bordercolor", "white");
//}

    function mytoggleOverlay(){
    	//alert("close mytoggleOverlay()");
        document.getElementById("resultMessage").value = '';        
        document.getElementById("headerLabel").style.color="white";
        document.getElementById("headerLabel").innerHTML="Add LookUp Data";
        var overlay = document.getElementById('overlay');
        var specialBox = document.getElementById('specialBox');
        var valueOfheaderFields=document.getElementById("headerFields").value;
        var tableName=document.getElementById("tableName").value;
        var partsOfStr = valueOfheaderFields.split(',');
     
        for (i = 1; i < partsOfStr.length; i++) {
        	   var table = document.getElementById("overlayFields");
               var row = document.createElement( "TR" );
               table.appendChild( row );
        var cell1 = document.createElement( "TD" );
        cell1.setAttribute("class", "fieldLabel");
        row.appendChild( cell1 );
        var data=partsOfStr[i].split('@');
        var textNode = document.createTextNode(data[0]+" :");
        cell1.appendChild(textNode);
        if(data[0]=="CreatedDate"){
       // 	alert("Created Date case");
             var dateTime=getCurrentTime();
        	 var cell = document.createElement( "TD" );
        	 cell.innerHTML = "<FONT color='red' SIZE='0.5'><em>*</em></FONT>";
             row.appendChild( cell );
             var t1=document.createElement("input");
             t1.id = data[0];
             t1.value=dateTime;
             t1.setAttribute("class", "inputTextBlue");
             cell.appendChild(t1);
        }
        else if(data[0]=="Status"){
        	//alert(partsOfStr.length);
        	if(data[0].length>2){
        		var cell = document.createElement( "TD" );
        		cell.innerHTML = "<FONT color='red' SIZE='0.5'><em>*</em></FONT>";
                row.appendChild( cell );
                var name1 = document.createElement("select");
                name1.setAttribute('id',data[0]);
                var option = new Option ("Active", "Active");
                var option1 = new Option ("InActive", "InActive");
                name1.options[0] = option;
                name1.options[1] = option1;
                name1.setAttribute("class", "inputSelect");
                cell.appendChild(name1);
        	}
        	else{
        		 var cell1 = document.createElement( "TD" );
        		 cell.innerHTML = "<FONT color='red' SIZE='0.5'><em>*</em></FONT>";
        		 row.appendChild( cell1 );
        		 var t1=document.createElement("input");
                 t1.id = data[0];
                 t1.setAttribute("class", "inputTextBlue");
              //   t1.class="inputTextBlue";
                 cell1.appendChild(t1);
        	        
        	}
        }
        else{
       // 	alert("else case");
        	  var cell = document.createElement( "TD" );
        	  cell.innerHTML = "<FONT color='red' SIZE='0.5'><em>*</em></FONT>";
              row.appendChild( cell );
              var t1=document.createElement("input");
              t1.id = data[0];
              
              t1.setAttribute("class", "inputTextBlue");
         //     t1.setAttribute("onblur", "javascript:checkFieldLength('"+data[0]+"','"+data[1]+"')");
           
              cell.appendChild(t1);
        }
      
       
        }
        var cellButton = document.createElement( "TD" );
        row.appendChild( cellButton );
        cellButton.innerHTML = "<a onClick='javascript:addColumnData(\""+valueOfheaderFields+"\")'><img SRC='../includes/images/go_21x21.gif' WIDTH=18 HEIGHT=15 BORDER=0 ALTER='Click to Add Row'/></a>";
  //   window.location="../admin/getLookUpColumnData.action?tableName="+tableName+"";
        overlay.style.opacity = .8;
        if(overlay.style.display == "block"){
            overlay.style.display = "none";
            specialBox.style.display = "none";   
        } else {
           overlay.style.display = "block";
            specialBox.style.display = "block";
        }
    }

    
    function toggleEditOverlay(lookUpId){
    	//alert("toggleEditOverlay");
        var overlay = document.getElementById('overlay');
        var specialBox = document.getElementById('specialBox');
        document.getElementById("headerLabel").style.color="white";
        document.getElementById("headerLabel").innerHTML="Edit LookUp Data";
        var valueOfheaderFields=document.getElementById("headerFields").value;
        var tableName=document.getElementById("tableName").value;
        var partsOfStr = valueOfheaderFields.split(',');
     
        for (i = 1; i < partsOfStr.length; i++) {
        	   var table = document.getElementById("overlayFields");
               var row = document.createElement( "TR" );
               table.appendChild( row );
        var cell1 = document.createElement( "TD" );
        cell1.setAttribute("class", "fieldLabel");
        row.appendChild( cell1 );
        var data=partsOfStr[i].split('@');
        var textNode = document.createTextNode(data[0]+" :");
        cell1.appendChild(textNode);
        if(data[0]=="CreatedDate"){
        //	alert("Created Date case");
             var dateTime=getCurrentTime();
        	 var cell = document.createElement( "TD" );
        	 cell.innerHTML = "<FONT color='red' SIZE='0.5'><em>*</em></FONT>";
             row.appendChild( cell );
             var t1=document.createElement("input");
             t1.id = data[0];
             t1.value=dateTime;
             t1.setAttribute("class", "inputTextBlue");
             cell.appendChild(t1);
        }
        else if(data[0]=="Status"){
        	 var cell = document.createElement( "TD" );
        	 cell.innerHTML = "<FONT color='red' SIZE='0.5'><em>*</em></FONT>";
             row.appendChild( cell );
             var name1 = document.createElement("select");
             name1.setAttribute('id',data[0]);
             var option = new Option ("Active", "Active");
             var option1 = new Option ("InActive", "InActive");
             name1.options[0] = option;
             name1.options[1] = option1;
             t1.setAttribute("class", "inputSelect");
             cell.appendChild(name1);
        }
        else{
        //	alert("else case");
        	  var cell = document.createElement( "TD" );
        	  cell.innerHTML = "<FONT color='red' SIZE='0.5'><em>*</em></FONT>";
              row.appendChild( cell );
              var t1=document.createElement("input");
             
             // alert("data[0]====>"+data[0]);
              t1.id = data[0];
            
              t1.setAttribute("class", "inputTextBlue");
         //     t1.setAttribute("onblur", "javascript:checkFieldLength('"+data[0]+"','"+data[1]+"')");
           //   t1.class="inputTextBlue";
              
          
              cell.appendChild(t1);
        }
      
       
        }
        var hiddenCell = document.createElement( "TD" );
        row.appendChild( hiddenCell );
        var hidden = document.createElement("input");

        hidden.setAttribute("type", "hidden");

        hidden.setAttribute("name", "id");
        hidden.setAttribute("id", "id");
      //  hidden.setAttribute("value", "id");
        hiddenCell.appendChild(hidden);
        
        var cellButton = document.createElement( "TD" );
        row.appendChild( cellButton );
//        var btn = document.createElement("BUTTON");
//        var t = document.createTextNode("UPDATE");
//        btn.appendChild(t);
//        cellButton.appendChild(btn);
//        btn.setAttribute("onclick","updateLookUpColumnData(\""+valueOfheaderFields+"\")");
        cellButton.innerHTML = "<a onClick='javascript:updateLookUpColumnData(\""+valueOfheaderFields+"\")'><img SRC='../includes/images/go_21x21.gif' WIDTH=18 HEIGHT=15 BORDER=0 ALTER='Click to Add Row'></a>";
   
        $.ajax({
            url:'getLookUpDetails.action?lookUpId='+lookUpId+'&tableName='+tableName,//
            context: document.body,
            success: function(responseText) {
            //	alert("responseText====>"+responseText);
                                var json = $.parseJSON(responseText);
                                for (i = 1; i < partsOfStr.length; i++) {
                                	var data=partsOfStr[i].split('@');
                                	var idValue=data[0];
                                	// alert("json[partsOfStr[i]]====>"+json[""+partsOfStr[i]+""]);
                                	data[0] = json[""+data[0]+""];
                                	 document.getElementById(idValue).value=    data[0];   
                                	 var Id = json["Id"];
                                	 document.getElementById("id").value=    Id;   
                                }
                             
 
               document.getElementById("load").style.display = 'none';  
               document.getElementById("resultMessage").style.display = 'none';  
            
                
            }, error: function(e){
                document.getElementById("load").style.display = 'none';
                alert("error-->"+e);
            }
        });
        
        overlay.style.opacity = .8;
        if(overlay.style.display == "block"){
            overlay.style.display = "none";
            specialBox.style.display = "none";   
        } else {
           overlay.style.display = "block";
            specialBox.style.display = "block";
        }
    }
    
    
    
    
    function Date1(){
    	  d = new Date();
    	  curr_year = d.getFullYear();
    	  for(i = 0; i < 5; i++)
    	  {
    	    document.getElementById('year1').options[i] = new Option((curr_year-1)-i,(curr_year-1)-i);
    	  }
    	}
    
    function addColumn()
    {
      
       var overlayColumnName = document.getElementById('overlayColumnName').value;
       var overlayStatus = document.getElementById('overlayStatus').value;
      
       overlayColumnName = escape(overlayColumnName);
       overlayStatus = escape(overlayStatus);    
        document.getElementById("load").style.display = 'block';
        $.ajaxFileUpload({
            url:'addColumn.action?overlayColumnName='+overlayColumnName+'&overlayStatus='+overlayStatus,//
            secureuri:false,//false
            fileElementId:'file',//id  <input type="file" id="file" name="file" />
            dataType: 'json',// json
            success: function(data,status){
                
                var displaymessage = "<font color=red>Please try again later</font>";
              
                if(data.indexOf("uploaded")>0){
                   
                    displaymessage = "<font color=green>Review added Successfully.</font>";
                }
                if(data.indexOf("Notvalid")>0){
                    
                    displaymessage = "<font color=red>Not a valid file!,Please check the file and try again.</font>";
                }
                if(data.indexOf("Error")>0){
                   
                    displaymessage = "<font color=red>Internal Error!, Please try again later.</font>"
                }
               
                document.getElementById("load").style.display = 'none';
                document.getElementById('resultMessage').innerHTML = displaymessage;//"<font color=green>File uploaded successfully</font>";
            
            },
            error: function(e){
                
                document.getElementById("load").style.display = 'none';
                document.getElementById('resultMessage').innerHTML = "<font color=red>Please try again later</font>";
           
            }
        });
        
       
       //}	
        return false;

    }


function getLookUpList(ele)
{
//	alert("getLookUpList()");
    var oTable = document.getElementById("tblLookUpReport");
    clearTable2(oTable);
    var tableName=document.getElementById("tableName").value;
 //   var columnName=document.getElementById("columnName").value;
 //   var headerFields=document.getElementById("headerFields").value;
   // alert("headerFields initial====>"+headerFields);
    columnData=document.getElementById("columnName").value;
//    alert("headerFields====>"+headerFields);
      $('span.pagination').empty().remove();
    document.getElementById("totalState1").innerHTML = "";
     
    var req = newXMLHttpRequest();
    req.onreadystatechange = function() {
        if (req.readyState == 4) {
            if (req.status == 200) {      
                document.getElementById("lookUpAvailableReport").style.display = 'none';
                displayLookUpAvailableReport(req.responseText);                        
            } 
        }else {
            document.getElementById("lookUpAvailableReport").style.display = 'block';
        }
    }; 
  var url = CONTENXT_PATH+"/getlookUpReport.action?tableName="+tableName+"&columnName="+ele+"&columnData="+columnData;
 // var url = CONTENXT_PATH+"/getlookUpReport.action?tableName="+tableName+"&columnName="+columnName;

//  alert("url====>"+url);
    req.open("GET",url,"true");    
    req.setRequestHeader("Content-Type","application/x-www-form-urlencoded"); 
    req.send(null);
}

function clearTable2(tableId) {
    var tbl =  tableId;
    var lastRow = tbl.rows.length; 
    while (lastRow > 0) { 
        tbl.deleteRow(lastRow - 1);  
        lastRow = tbl.rows.length; 
    } 
}

function displayLookUpAvailableReport(response) {
	//  alert(response);
	    var oTable = document.getElementById("tblLookUpReport");
	    
	    clearTable(oTable);
	    

	    var dataArray = response;
	 //   alert("dataArray====>"+dataArray);
	    if(dataArray == "no data")
	    {
	        alert("No Records Found for this Search");   
	    }
	    else {
	    	//alert("in else");
	        //     var headerFields = new Array("S.No","Name","Status");
	    	var headerFields =  [];
	               var temp = new Array;
	               var str = new Array;
	              // var columnName=dataArray.split('*@#$');
//	               var columnName=dataArray.substring(dataArray.indexOf("*@#$") + 1);
//	               alert("columnName====>"+columnName);
	        temp = dataArray.split('addto');   
	     //   alert("temp====>"+temp);
	        
	        str = dataArray.substring(dataArray.indexOf("addto") + 1);
	      //  alert("str before is====>"+str);
	        str=str.split('#^$');
	   //     alert("str after is====>"+str);
	      var headerLength = str.length-1;
	 //     alert("headerLength====>"+headerLength);
	      headerFields.push("S.No");
	      for (var i=2; i<str.length; i++) {
	    	//  alert(str[i]);
	    	  
	    	  headerFields.push(str[i]);
	    	  
	      }
	      
	      
	      document.getElementById("headerFields").value=headerFields;
	 //       alert("temp====>"+temp);
	   //   alert("document.getElementById('headerFields').value====>"+document.getElementById("headerFields").value);
	               if(response!=''){
	            document.getElementById("totalState1").innerHTML = temp[0];
	           // alert("temp[0]====>"+temp[0]);
	           totalRec= temp[1];
	         
	           ParseAndGenerateHTML1(oTable,temp[0], headerFields);
	               pagerOption();
	        }else{
	            alert('No Result For This Search...');
	            spnFast.innerHTML="No Result For This Search...";                
	        }

	    } 
	    
	     $('#tblLookUpReport').tablePaginate({navigateType:'navigator'},recordPage);
	}

function ParseAndGenerateHTML1(oTable,responseString,headerFields) {
    
     //  alert("ParseAndGenerateHTML");
    var start = new Date();
    var fieldDelimiter = "#^$";
    var recordDelimiter = "*@!";   
 //   alert('responseString%%%% ******'+responseString);
    //alert('rowCount%%%% ******'+rowCount);
      
    document.getElementById("headerFields").value=headerFields;
    
    var records = responseString.split(recordDelimiter); 
    //  alert("records---->"+records);
    generateTable(oTable,headerFields,records,fieldDelimiter);
}

function generateTable(oTable, headerFields,records,fieldDelimiter) {	
    //   alert("In Generate Table "+fieldDelimiter);
    //alert(records);
    var tbody = oTable.childNodes[0];    
    tbody = document.createElement("TBODY");
    oTable.appendChild(tbody);
     if(oTable.id == "tblLookUpReport"){
          generateTableHeaderLookUp(tbody,headerFields);
                
            }
            else{
             generateTableHeader(tbody,headerFields);    
            }
   
    
    var rowlength;
    rowlength = records.length-1;
    // alert("rowlength--->"+rowlength);
    if(rowlength >0 && records!=""){
        //alert("rowlength-->^"+records);
        for(var i=0;i<rowlength;i++) {
           

           if(oTable.id == "tblLookUpReport"){
                generateLookUpListDetails(tbody,records[i],fieldDelimiter); 
            }
            else{
                generateRow(oTable,tbody,records[i],fieldDelimiter);  
            }
        }
       
    } else {
        generateNoRecords(tbody,oTable);
    }
    generateFooter(tbody,oTable,headerFields);
}


function generateTableHeaderLookUp(tableBody,headerFields) {
    //alert("In generateTableHeader");
    var row;
    var cell;
    row = document.createElement( "TR" );
    row.className="gridHeader";
    tableBody.appendChild( row );
   // alert(headerFields.length);
    for (var i=0; i<headerFields.length; i++) {
        cell = document.createElement( "TD" );
        cell.className="gridHeader";
        row.appendChild( cell );
        var data=headerFields[i].split('@');
        cell.innerHTML = data[0];
//        document.getElementById("searchHeaderField").value=headerFields[2];
//        alert("document.getElementById('searchHeaderField').value"+document.getElementById("searchHeaderField").value);
     //   cell.width = 20;
    }
    
     
}

function generateRow(oTable,tableBody,record,delimiter) {
     // alert("In generate Row function")
    var row;
    var cell;
    var fieldLength;
    var fields = record.split(delimiter);
  //  alert("fields in generateRow"+fields);
    fieldLength = fields.length ;
    var length;
    if(oTable.id == "tblLookUpReport" ){
        length = fieldLength;
    }
    row = document.createElement( "TR" );
    row.className="gridRowEven";
    tableBody.appendChild( row );
    
    for (var i=0;i<length;i++) {
        cell = document.createElement( "TD" );
        cell.className="gridColumn";
        if(parseInt(i,10)==1){
        //	alert("hiiiii one ");
            cell.innerHTML = "<a href='javascript:getAccountDetails("+fields[0]+")'>"+fields[i]+"</a>";
           
        }
        else{
        	 cell.innerHTML = fields[i];
        }
       
       // alert(fields[i]);
        if(fields[i]!=''){
            row.appendChild( cell );
        }
    }
}


function generateNoRecords(tbody,oTable) {
    var noRecords =document.createElement("TR");
    noRecords.className="gridRowEven";
    tbody.appendChild(noRecords);
    cell = document.createElement("TD");
    cell.className="gridColumn";
    if(oTable.id == "tblLookUpReport"){
        cell.colSpan = "13";
    }

   
    cell.innerHTML = "No Records Found for this Search";
    noRecords.appendChild(cell);
}


function generateFooter(tbody,oTable,headerFields) {
    //alert(oTable.id);
    var footer =document.createElement("TR");
    footer.className="gridPager";
    tbody.appendChild(footer);
    cell = document.createElement("TD");
    cell.className="gridFooter";
    cell.id="footer"+oTable.id;
    //cell.colSpan = "5";
    if(oTable.id == "tblLookUpReport"){
        cell.colSpan = "13";
 
        cell.innerHTML = "<a href='javascript:mytoggleOverlay()'><img SRC='../includes/images/add.gif' WIDTH=18 HEIGHT=15 BORDER=0 ALTER='Click to Add Columns'></a>";
        
   //     cell.innerHTML = "<a href='javascript:addColumnData(\""+headerFields+"\")'><img SRC='../includes/images/go_21x21.gif' WIDTH=18 HEIGHT=15 BORDER=0 ALTER='Click to Add Row'></a>&nbsp;&nbsp;<a href='javascript:addNewRow()'><img SRC='../includes/images/add.gif' WIDTH=18 HEIGHT=15 BORDER=0 ALTER='Click to Add Columns'></a>";
   //     cell.innerHTML = "<a href='javascript:addNewRow()'><img SRC='../includes/images/add.gif' WIDTH=18 HEIGHT=15 BORDER=0 ALTER='Click to Add Columns'></a>";
//        cell.innerHTML ="Total&nbsp;Records&nbsp;:"+totalRec;
//     cell.setAttribute('align','right');
    }
   

    
    footer.appendChild(cell);
}


function clearTable(tableId) {
    var tbl =  tableId;
    var lastRow = tbl.rows.length; 
    while (lastRow > 0) { 
        tbl.deleteRow(lastRow - 1);  
        lastRow = tbl.rows.length; 
    } 
}

function generateLookUpListDetails(tableBody,record,fieldDelimiter){
    var row;
    var cell;
    var fieldLength;
     
    var fields = record.split(fieldDelimiter);
   // alert(fields);
    fieldLength = fields.length ;
    var length = fieldLength;
  //  alert("length is---->"+length);
    row = document.createElement( "TR" );
    row.className="gridRowEven";
    tableBody.appendChild( row );
    for (var i=1;i<length;i++) {            
       cell = document.createElement( "TD" );
       cell.className="gridColumn";       
       cell.innerHTML = fields[i];
    //   alert(fields[i]);
       if(parseInt(i,10)==2){
      // 	alert("hiiiii one ");
           cell.innerHTML = "<a href='javascript:toggleEditOverlay("+fields[1]+")'>"+fields[i]+"</a>";
       }
      
            //  cell.innerHTML = fields[i];  
           cell.setAttribute("align","left");   
           cell.setAttribute("width","20px");   
            if(parseInt(i,10)==1){
                cell.innerHTML =fields[0];
            }
////            if(parseInt(i,10)==0){
////                cell.innerHTML =fields[1];
////            }
//              if(parseInt(i,10)==1){
//                cell.innerHTML =fields[2];
//            }
//              if(parseInt(i,10)==2){
//                cell.innerHTML =fields[3];
//            }
        //   cell.innerHTML =fields[i];
           row.appendChild( cell );
        
       
               
        }
   }


function addNewRow(){
	//alert("hi asha");
	var table = document.getElementById("tblLookUpReport");
	  var row = table.insertRow(table.rows.length-1);
	  row.className="gridRowEven";
	  var columnCount=document.getElementById('tblLookUpReport').rows[0].cells.length;
	//  alert(columnCount);
	  var cell;
  for (i = 0; i < columnCount; i++) {
	   var celli=row.insertCell(i);
       var ti=document.createElement("input");
           ti.id = "txtName"+i;
           celli.appendChild(ti);
  }
}


function addColumnData(headerfieldValues){	
	//alert("hi");
	//alert("headerfields====>"+headerfieldValues);
	var tableName=document.getElementById("tableName").value;
	//alert("tableName====>"+tableName);
	var reqFields=headerfieldValues.split(",");
//	alert("in");
//	var stringdata = "";
//	alert(reqFields.length);
	var txtName;
	
	var stringdata="";
	var headerfields="";
	for(i=1;i<reqFields.length;i++){
		//alert("reqFields[i]====>"+reqFields[i]);
		var name= reqFields[i].split('@');
		//alert("name is====>"+name)
		 var values = document.getElementById(name[0]).value;
		// alert("values====>"+values);
		 if(values.trim()==""){
		//	 alert("null value");
		//	 alert("please enter mandatory fields");
			 document.getElementById('resultMessage').innerHTML ="<font color=red>please enter mandatory fields</font>"; 
			 return false;
		 }
		// var valueLength=document.getElementById(name[1]).value;
// alert("valueLength====>"+valueLength);
		 if(values.trim().length>name[1]){
			 document.getElementById('resultMessage').innerHTML ="<font color=red>The '"+name[0]+"' value should be less than '"+name[1]+"' characters</font>"; 
			 return false;
		 }
		// alert("hi hello");
		 stringdata = stringdata + ',' + values;
		 headerfields = headerfields + ',' + name[0];
	//	 stringdata=stringdata+values;
	// alert("stringdata in====>"+stringdata);
	// alert("headerfields in====>"+headerfields);
	}
	var stringData=stringdata.substring(1);
	var headerFields=headerfields.substring(1);
	//alert("stringdata out====>"+stringData);
	//alert("headerFields out====>"+headerFields);
//		alert(reqFields[i]);
//	
//		stringdata=stringdata+reqFields[i]+",";
//	 //	alert("stringdata====>"+stringdata);
//	//	stringdata = stringdata + '{reqFields[i]:'+reqFields[i]+'+","},';
//	alert("stringdata====>"+stringdata);
//		
		
		      
//	}
	   document.getElementById("resultMessage").innerHTML='';
	    document.getElementById("load").style.display = 'block';
		      $.ajax({
		        url:'addColumnNames.action?headerFields='+headerFields+'&stringData='+escape(stringData)+'&tableName='+tableName,//
		        
		        secureuri:false,//false
		        fileElementId:'file',//id  <input type="file" id="file" name="file" />
		      context: document.body,
		            success: function(responseText){
		        //  alert(responseText);
		            document.getElementById("load").style.display = 'none';
		            document.getElementById('resultMessage').innerHTML = responseText;//"<font color=green>File uploaded successfully</font>";
		        
		        },
		        error: function(e){
		            
		            document.getElementById("load").style.display = 'none';
		            document.getElementById('resultMessage').innerHTML = "<font color=red>Please try again later</font>";
		       
		        }
		    });
		    
		   
		 //   return false;
	

}

function updateLookUpColumnData(headerfieldValues){
	//alert("in updateLookUpColumnData");
//	alert(headerfieldValues);	
	var tableName=document.getElementById("tableName").value;
	var lookUpId = document.getElementById("id").value;
	var reqFields=headerfieldValues.split(",");
	var txtName;
	var stringdata="";
	var headerfields="";
	for(i=1;i<reqFields.length;i++){
	//	alert(reqFields[i]);
		var name= reqFields[i].split('@');
	//	alert("name====>"+name[0]);
		 var values = document.getElementById(name[0]).value;
		 if(values.trim()==""){
				//	 alert("null value");
				//	 alert("please enter mandatory fields");
					 document.getElementById('resultMessage').innerHTML ="<font color=red>please enter mandatory fields</font>"; 
					 document.getElementById('resultMessage').style.display="block";
					 return false;
				 }
//		 var valueLength=document.getElementById(name[1]).value;
//		 alert("valueLength====>"+valueLength);
		 if(values.trim().length>name[1]){
	//		 alert("name[1]====>"+name[1]);
			 document.getElementById('resultMessage').innerHTML ="<font color=red>The '"+name[0]+"' value should be less than '"+name[1]+"' characters</font>"; 
			 document.getElementById('resultMessage').style.display="block";
			 return false;
		 }
		 stringdata = stringdata + ',' + values;
		 headerfields = headerfields + ',' + name[0];
	}
	var stringData=stringdata.substring(1);
var headerFields=headerfields.substring(1);
//alert("stringData===>"+stringData);
//alert("headerFields===>"+headerFields);
	   document.getElementById("resultMessage").innerHTML='';
	    document.getElementById("load").style.display = 'block';
		   
	    var req = newXMLHttpRequest();
	    req.onreadystatechange = readyStateHandler(req,ppulateMyReviewUpadteResult); 
	    //var url=CONTENXT_PATH+"/updateMyReview.action?reviewId="+reviewId+"&overlayReviewType="+overlayReviewType+"&overlayReviewName="+overlayReviewName+"&overlayReviewDate="+overlayReviewDate+"&overlayDescription="+overlayDescription+"&reviewStatusOverlay="+reviewStatusOverlay;
	    var url=CONTENXT_PATH+"/updateColumnNames.action?headerFields="+headerFields+"&stringData="+escape(stringData)+"&tableName="+tableName+"&lookUpId="+lookUpId;
	   // alert("url====>"+url);
	    req.open("GET",url,"true");
	    req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
	    req.send(null);
}


function ppulateMyReviewUpadteResult(resText){
	 document.getElementById("load").style.display = 'none';
    document.getElementById('resultMessage').innerHTML = "<font color=green size=2px>Updated Successfully</font>";
    document.getElementById('resultMessage').style.display="block";
 //  alert("Updated Successfully");
   toggleOverlay();
}

function getCurrentTime(){
	   var date = new Date(),
       year = date.getFullYear(),
       month = (date.getMonth() + 1).toString(),
       formatedMonth = (month.length === 1) ? ("0" + month) : month,
       day = date.getDate().toString(),
       formatedDay = (day.length === 1) ? ("0" + day) : day,
       hour = date.getHours().toString(),
       formatedHour = (hour.length === 1) ? ("0" + hour) : hour,
       minute = date.getMinutes().toString(),
       formatedMinute = (minute.length === 1) ? ("0" + minute) : minute,
       second = date.getSeconds().toString(),
       formatedSecond = (second.length === 1) ? ("0" + second) : second;
  //   alert(formatedDay + "-" + formatedMonth + "-" + year + " " + formatedHour + ':' + formatedMinute + ':' + formatedSecond);
//return formatedDay + "-" + formatedMonth + "-" + year + " " + formatedHour + ':' + formatedMinute + ':' + formatedSecond;
return year + "-" + formatedMonth + "-" + formatedDay + " " + formatedHour + ':' + formatedMinute + ':' + formatedSecond;
}


function toggleCloseOverlay() {
   

  
	  var tableName=document.getElementById("tableName").value;
        var overlay = document.getElementById('overlay');
        var specialBox = document.getElementById('specialBox');
        window.location="../admin/getLookUpColumnData.action?tableName="+tableName+"";
        overlay.style.opacity = .8;
        if(overlay.style.display == "block"){
            overlay.style.display = "none";
            specialBox.style.display = "none";

            //   document.getElementById("frmDBGrid").submit();

        }
        else {
            overlay.style.display = "block";
            specialBox.style.display = "block";
        }

    


    //document.getElementById("frmDBGrid").submit();
}


function fillCustomer() {
    var test=document.getElementById("tableName").value; 

    if (test == "") {
        clearTable();
    } else {
        if (test.length >2) {
            var url = CONTENXT_PATH+"/getLookUpTableDetails.action?tableName="+ escape(test);   

         //   alert("URL--->"+url);
            var req = initRequest(url);
            req.onreadystatechange = function() {
                if (req.readyState == 4) {
                    if (req.status == 200) {                        
                        parseCustMessages(req.responseXML);                        
                    } else if (req.status == 204){
                        clearTable();
                    }
                }
            };
            req.open("GET", url, true);
            req.send(null);
        }
    }
}


function parseCustMessages(responseXML) {
//	alert("in parseCustMessages");
    clearTable();
 //   alert(responseXML);
    autorow1 = document.getElementById("menu-popup");
    autorow = document.getElementById("menu-popup");
    var employees = responseXML.getElementsByTagName("EMPLOYEES")[0];
    if (employees.childNodes.length > 0) {        
        completeTable.setAttribute("bordercolor", "black");
        completeTable.setAttribute("border", "0");        
    } else {
        clearTable();
    }
    if(employees.childNodes.length<10) {
        autorow1.style.overflowY = "hidden";
        autorow.style.overflowY = "hidden";        
    }
    else {    
        autorow1.style.overflowY = "scroll";
        autorow.style.overflowY = "scroll";
    }
    
    var employee = employees.childNodes[0];
    var chk=employee.getElementsByTagName("VALID")[0];
    if(chk.childNodes[0].nodeValue =="true") {
        var validationMessage=document.getElementById("validationMessage");
        validationMessage.innerHTML = "";
        document.getElementById("menu-popup").style.display = "block";
        for (loop = 0; loop < employees.childNodes.length; loop++) {
            
            var employee = employees.childNodes[loop];
            var customerName = employee.getElementsByTagName("NAME")[0];
            var empid = employee.getElementsByTagName("EMPID")[0];
            appendCustomer(empid.childNodes[0].nodeValue,customerName.childNodes[0].nodeValue);
        }
        var position = findPosition(document.getElementById("tableName"));
        posi = position.split(",");
        document.getElementById("menu-popup").style.left = posi[0]+"px";
        document.getElementById("menu-popup").style.top = (parseInt(posi[1])+20)+"px";
        document.getElementById("menu-popup").style.display = "block";
    } //if
    if(chk.childNodes[0].nodeValue =="false") {
        var validationMessage=document.getElementById("validationMessage");
        validationMessage.innerHTML = " Name  is invalid.Please select the name from sugestion list";
    }
}


function appendCustomer(empId,empName) {
    var row;
    var nameCell;
    if (!isIE) {
        row = completeTable.insertRow(completeTable.rows.length);
        nameCell = row.insertCell(0);
    } else {
        row = document.createElement("tr");
        nameCell = document.createElement("td");
        row.appendChild(nameCell);
        completeTable.appendChild(row);
    }
    row.className = "popupRow";
    nameCell.setAttribute("bgcolor", "#3E93D4");
    var linkElement = document.createElement("a");
    linkElement.className = "popupItem";
    linkElement.setAttribute("href", "javascript:set_cust('"+empName.replace(/'/g, "\\'") +"','"+ empId +"')");
    
    linkElement.appendChild(document.createTextNode(empName));
    linkElement["onclick"] = new Function("hideScrollBar()");
    nameCell.appendChild(linkElement);
}


function set_cust(eName,eID){
    clearTable();
    document.frmEmpSearch.tableName.value =eName;
    document.frmEmpSearch.tableId.value =eID;
    //alert("hii");
}


var isIE;
var vendorName;

function initRequest(url) {
    if (window.XMLHttpRequest) {
        return new XMLHttpRequest();
    }
    else
        if (window.ActiveXObject) {
            isIE = true;
            return new ActiveXObject("Microsoft.XMLHTTP");
        }
    
}

//function clearTable() {
//    if (completeTable) {
//        completeTable.setAttribute("bordercolor", "white");
//        completeTable.setAttribute("border", "0");
//        completeTable.style.visible = false;
//        for (loop = completeTable.childNodes.length -1; loop >= 0 ; loop--) {
//            completeTable.removeChild(completeTable.childNodes[loop]);
//        }
//    }
//}


//function clearTable(tableId) {
////	alert("clearTable");
//    var tbl =  tableId;
//    var lastRow = tbl.rows.length; 
//    while (lastRow > 0) { 
//        tbl.deleteRow(lastRow - 1);  
//        lastRow = tbl.rows.length; 
//    } 
//}

function clearTable() {
	 completeTable = document.getElementById("completeTable");  
    if (completeTable) {
        completeTable.setAttribute("bordercolor", "white");
        completeTable.setAttribute("border", "0");
        completeTable.style.visible = false;
        for (loop = completeTable.childNodes.length -1; loop >= 0 ; loop--) {
            completeTable.removeChild(completeTable.childNodes[loop]);
        }
    }
}

function hideScrollBar() {
    autorow = document.getElementById("menu-popup");
    autorow.style.display = 'none';
}

function getTablesList(){
	
var tableId=document.getElementById("tableId").value;
	window.location="getLookupTableData.action?tableId="+tableId;
}


function getColumnsList(ele){
	//alert(ele);
	columnData=document.getElementById("columnName").value;
//	alert("columnData====>"+columnData);
	   var req = newXMLHttpRequest();
	    req.onreadystatechange = function() {
	        if (req.readyState == 4) {
	            if (req.status == 200) {      
	                document.getElementById("lookUpAvailableReport").style.display = 'none';
	                displayLookUpAvailableReport(req.responseText);                        
	            } 
	        }else {
	            document.getElementById("lookUpAvailableReport").style.display = 'block';
	        }
	    }; 
	  var url = CONTENXT_PATH+"/getlookUpReport.action?columnName="+ele+"&columnData="+columnData;
	 // var url = CONTENXT_PATH+"/getlookUpReport.action?tableName="+tableName+"&columnName="+columnName;

	//  alert("url====>"+url);
	    req.open("GET",url,"true");    
	    req.setRequestHeader("Content-Type","application/x-www-form-urlencoded"); 
	    req.send(null);
}

function checkFieldLength(ele,length){
//	alert("ele====>"+ele);
	//alert("length====>"+length)
	var eleValue=document.getElementById(ele).value;
//	alert("eleValue====>"+eleValue);
	if(eleValue.length>length){
	//	alert("greater");
		document.getElementById(ele).value='';
		document.getElementById('resultMessage').innerHTML ="<font color=red>The '"+ele+"' value should be less than '"+length+"' characters</font>"; 
		 return false;
	}
	
}