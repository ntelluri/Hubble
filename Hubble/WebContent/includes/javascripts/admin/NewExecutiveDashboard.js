/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */



function getSelectedRoleDashboard(role){
   
    // alert(role)
    if(role== 2){
         $("#EmplyeeCountDiv").show();
         $("#SalesCountDiv").hide();
        getEmpCountryCount();
    }else if(role == 4) {
    	 $("#SalesCountDiv").show();
     $("#EmplyeeCountDiv").hide();
     getSalesAccStatus();
    }
    else{
    	 $("#SalesCountDiv").hide();
         $("#EmplyeeCountDiv").hide();
    }
}




function newXMLHttpRequest() {
    var xmlreq = false;
    if(window.XMLHttpRequest) {
        xmlreq = new XMLHttpRequest();
    } else if(window.ActiveXObject) {
        try {
            xmlreq = new ActiveXObject("MSxm12.XMLHTTP");
        } catch(e1) {
            try {
                xmlreq = new ActiveXObject("Microsoft.XMLHTTP");
            } catch(e2) {
                xmlreq = false;
            }
        }
    }
    return xmlreq;
}
function readyStateHandler(req,responseTextHandler) {
    return function() {
        if (req.readyState == 4) {
            if (req.status == 200) {
                (document.getElementById("loadActMessageAS")).style.display = "none";
                responseTextHandler(req.responseText);
            } else {
                
                alert("HTTP error ---"+req.status+" : "+req.statusText);
            }
        }else {
            (document.getElementById("loadActMessageAS")).style.display = "block";
        }
    }
}


function ClrTable(myHTMLTable) { 
    var tbl =  myHTMLTable;
    var lastRow = tbl.rows.length; 
    while (lastRow > 0) { 
        tbl.deleteRow(lastRow - 1);  
        lastRow = tbl.rows.length; 
    } 
}
function ParseAndGenerateHTML(oTable,responseString,headerFields) {
    
    var start = new Date();
    var fieldDelimiter = "|";
    var recordDelimiter = "^";   
    
   
        fieldDelimiter = "#^$";
        recordDelimiter = "*@!"; 
   
   var records = responseString.split(recordDelimiter); 
    
    generateTable(oTable,headerFields,records,fieldDelimiter);
}

function generateTable(oTable, headerFields,records,fieldDelimiter) {	
    var tbody = oTable.childNodes[0];    
    tbody = document.createElement("TBODY");
    oTable.appendChild(tbody);
    generateTableHeader(tbody,headerFields);
    var rowlength;
    rowlength = records.length-1;
    if(rowlength >=1 && records!=""){
        for(var i=0;i<rowlength;i++) {
           
        generateRow(oTable,tbody,records[i],fieldDelimiter);
            
        }    
    }    
    else {
        generateNoRecords(tbody,oTable);
    }
    
    generateFooter(tbody,oTable);
   
         pagerOption();
   
    
    
}

function generateNoRecords(tbody,oTable) {
    var noRecords =document.createElement("TR");
    noRecords.className="gridRowEven";
    tbody.appendChild(noRecords);
    cell = document.createElement("TD");
    cell.className="gridColumn";
    
   
        cell.colSpan = "11";   
   
    cell.innerHTML = "No Records Found for this Search";
    noRecords.appendChild(cell);
}

function generateTableHeader(tableBody,headerFields) {
    var row;
    var cell;
    row = document.createElement( "TR" );
    row.className="gridHeader";
    tableBody.appendChild( row );
    
    for (var i=0; i<headerFields.length; i++) {
        cell = document.createElement( "TD" );
        cell.className="gridHeader";
        row.appendChild( cell );
        cell.innerHTML = headerFields[i];
        cell.width = 120;
    }
}

function generateFooter(tbody,oTable) {
    var footer =document.createElement("TR");
    footer.className="gridPager";
    tbody.appendChild(footer);
    cell = document.createElement("TD");
    cell.className="gridFooter";
    cell.id="footer"+oTable.id;
    
        cell.colSpan = "25";   
   
    footer.appendChild(cell);
}

function generateRow(oTable,tableBody,record,delimiter) {
    //alert("In generateRow");
    var row;
    var cell;
    var fieldLength;
    var fields = record.split(delimiter);
    fieldLength = fields.length ;
    var length;
    //if(oTable.id == "tblAccountSummRep" || oTable.id == "tblUpdateForAccountsListByPriority"){
    length = fieldLength;
    //  }
    
    // else {
    //     length = fieldLength-1;
    // }
  
    row = document.createElement( "TR" );
    row.className="gridRowEven";
    tableBody.appendChild( row );
     
    
    //  document.getElementById("totalRec").value = length-1;
        for (var i=0;i<length;i++) {
       
            cell = document.createElement( "TD" );
            cell.className="gridColumn";
            cell.innerHTML = fields[i];
            if(fields[i]!=''){
                row.appendChild( cell );
            }
       
        }
}



function getEmpCountryCount()
{
  //  alert("getEmpCountryVsStatusStackedChart");
     document.getElementById("tblReportDashBoard1").style.display="none";
  // document.getElementById("EmpStatePie").style.display="none";
  document.getElementById("EmpSubPracticePie").style.display="none";
    document.getElementById("empPracticeCountPie").style.display="none";
     document.getElementById("countVsStatusPie").style.display="none";
      document.getElementById("resourcesVsMainPie").style.display="none";
    document.getElementById("employeeCountryVsCountStack").style.display="none";
    document.getElementById("empOppertunitiesPie").style.display="none";
    document.getElementById("empRequirementPie").style.display="none";
    // document.getElementById("loadEmployeeCount").style.display="block";
    
    //document.getElementById("resultMessage").style.display="none";
    var req = newXMLHttpRequest();
    req.onreadystatechange = function() {
        if (req.readyState == 4) {
            if (req.status == 200) {      
                  document.getElementById("loadEmployeeCount").style.display = 'none';
                displayEmpCountryVsCountStackedChart(req.responseText);
              //  getRequirementCount();
             } 
        }else {
         document.getElementById("loadEmployeeCount").style.display = 'block';
        }
    }; 
    var url = CONTENXT_PATH+"/getEmpCountryCount.action";
    req.open("GET",url,"true");    
    req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    req.send(null);
}
function displayEmpCountryVsCountStackedChart(response) { 
   // alert("1");
   
    document.getElementById("employeeCountryVsCountStack").style.display="block";

        
  
 //  alert("result1")
    // PracticeUtilizationStackedChat(result1);
    EmpCountryVsCountReportStackedChart(response);
    
}


function EmpCountryVsCountReportStackedChart(response){
     google.load('visualization', '1.1', {
        packages: ['corechart']
    });
   
   var result = response.split("*@!");

   var title = [];
   
   var ress = result[0].split(",");
   //alert(ress.length)
   for (var j=0;j<ress.length-1;j++){
   title.push(ress[j]);
   }
    var arraydata = [title]; 
 
//alert(result.length-1)
    for(var i=1; i<result.length-1; i++){
        var dArray = [];
           var res = result[i].split("#^$");
     
dArray.push(res[0]);
for(var k=1;k<res.length-1;k++){
 dArray.push(parseInt(res[k]));
}

     arraydata.push(dArray);
}

 //alert(arraydata) 
   
    var data = google.visualization.arrayToDataTable(arraydata);
    var view=new google.visualization.DataView(data);
   
    var options = {
        title:'Employee Count Vs Country',
        titleTextStyle: {
            color: ('blue', '#3E93D4'),    // any HTML string color ('red', '#cc00cc')
            fontName: 'Times New Roman', // i.e. 'Times New Roman'
            fontSize: 15, // 12, 18 whatever you want (don't specify px)
            bold: true,    // true or false
            italic: false   // true of false
         },
        0: {
        type: 'bars'
      },
        1: {
        type: 'line',
        color: 'black',
        lineWidth: 3,
        pointSize: 0,
        visibleInLegend: false
      },
        vAxes:[{
            title:'Count',
            minValue: 0,
            ticks: [0,1,150, 450, 750, 1050,1350],
            titleTextStyle:
            {
                color: ('blue', '#3E93D4'),    // any HTML string color ('red', '#cc00cc')
                fontName: 'Times New Roman', // i.e. 'Times New Roman'
                fontSize: 15, // 12, 18 whatever you want (don't specify px)
                bold: true,    // true or false
                italic: false   // true of false
            }
        }],
        hAxis:{
            title:'Country',
            titleTextStyle:
            {
                color: ('blue', '#3E93D4'),    // any HTML string color ('red', '#cc00cc')
                fontName: 'Times New Roman', // i.e. 'Times New Roman'
                fontSize: 15, // 12, 18 whatever you want (don't specify px)
                bold: true,    // true or false
                italic: false   // true of false
            }
        },
        width: 700,
        height: 350,
        bar: {
            groupWidth: '50%'
        },
        isStacked: true,
        bars :'vertical',
        // bars: 'horizontal',
        legend: {
            position: 'bottom', 
            maxLines: 8
        }
    };

 // document.getElementById("loadEmployeeCount").style.display="none";
    var chart = new google.visualization.ColumnChart(document.getElementById("employeeCountryVsCountChart"));
    
    function selectHandler() {
             
        var selectedItem = chart.getSelection()[0];
      //  alert("selctedItem"+selectedItem)
         if (selectedItem) {
          //   alert(selectedItem)
            var country = data.getValue(selectedItem.row, 0);
            var role = data.getColumnLabel(selectedItem.column);
       //   var total = data.getColumnId(selectedItem.column, 1);
          // alert("country..."+country+"role...."+role);
            //   getStudentInfoStackByCollege(activityType,columnLabel,3);
            getEmpPracticeCount(country,role);

        }
    }
    google.visualization.events.addListener(chart, 'select', selectHandler);   
    chart.draw(view, options,dArray);

}

function getEmpPracticeCount(country,role)
{
   // alert("getPracticeVsCountStackedChart");
   var practice = "";
    
     document.getElementById("tblReportDashBoard1").style.display="none";
  // document.getElementById("EmpStatePie").style.display="none";
  document.getElementById("EmpSubPracticePie").style.display="none";
    document.getElementById("countVsStatusPie").style.display="none";
      document.getElementById("resourcesVsMainPie").style.display="none";
    document.getElementById("empPracticeCountPie").style.display="none";
    document.getElementById("empOppertunitiesPie").style.display="none";
    document.getElementById("empRequirementPie").style.display="none";
    // document.getElementById("loadEmployeeCount").style.display="block";
   // alert("practice"+practice+"country"+country+"role"+role)
    //document.getElementById("resultMessage").style.display="none";
    var req = newXMLHttpRequest();
    req.onreadystatechange = function() {
        if (req.readyState == 4) {
            if (req.status == 200) {      
                document.getElementById("loadPracticePie").style.display = 'none';
                displayEmpPracticeCountStackedChart(req.responseText,country,role);
                getEmpSubPracticePieChart(practice,country,role);
               
              //  getRequirementCount();
             }
        }else {
          document.getElementById("loadPracticePie").style.display = 'block';
        }
    }; 
    var url = CONTENXT_PATH+"/getEmpPracticeCount.action?country="+country+"&departmentId="+role;
    
    req.open("GET",url,"true");    
    req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    req.send(null);
}
function displayEmpPracticeCountStackedChart(response,country,role) { 
   // alert("1");
   
    document.getElementById("empPracticeCountPie").style.display="block";
 if(response!=''){
           
        EmpPracticeCountReportStackedChart(response,country,role);
   
    }else{
           document.getElementById("empPracticeCountPie").style.display="block";
        document.getElementById("empPracticeCountPieChart").innerHTML = "NO-DATA";         
    }
    
}
 
function EmpPracticeCountReportStackedChart(result,country,role){
    
     document.getElementById("empPracticeCountPie").style.display="block";
      
    var arraydata = [['Practice', 'Count']];
    
    var result1=result.split("*@!");
    
    for(var i=0; i<result1.length-1; i++){
        
        var res = result1[i].split("#^$");
        var dArray = [res[0],parseInt(res[1])];
        arraydata.push(dArray);
    }

    var data = google.visualization.arrayToDataTable(arraydata);
    var options = {
        title: 'Employee Count Vs Practice' ,
        titleTextStyle: {
            color: ('blue', '#3E93D4'),    // any HTML string color ('red', '#cc00cc')
            fontName: 'Times New Roman', // i.e. 'Times New Roman'
            fontSize: 15, // 12, 18 whatever you want (don't specify px)
            bold: true,    // true or false
            italic: false   // true of false
            
     
        },
        
       
        legend: 'right',
        chartArea:{
            
            width:"100%"
        },
        
        is3D: true,
        pieSliceText: 'value',
        sliceVisibilityThreshold: 0
         
    };
    // align:'center';
    var chart = new google.visualization.PieChart(document.getElementById("empPracticeCountPieChart"));
       
    function selectHandler() {
             
        var selectedItem = chart.getSelection()[0];
        //alert("selectedItem--"+data.getValue(selectedItem.row, 0));
        if (selectedItem) {
            var practice = data.getValue(selectedItem.row, 0);
           // alert(practice)
            // getBdmStatisticsDetailsByLoginId(activityType,bdmId);
            getEmpSubPracticePieChart(practice,country,role);
        }
    }
    google.visualization.events.addListener(chart, 'select', selectHandler);   
    chart.draw(data, options,dArray);
        
    
    
    
    
//    google.load('visualization', '1.1', {
//        packages: ['corechart']
//    });
//   // alert("response"+response);
//    
//      var result = response.split("*@!");
//    alert("result---->"+result);
//for(var i=0; i<result.length; i++){
//   var title = [];
//   
//   var ress = result[i].split(",");
//   //alert(ress.length)
//   for (var j=0;j<ress.length-1;j++){
//   title.push(ress[j]);
//   }
//    var arraydata = [title]; 
//    alert("arraydata---->"+arraydata)
//    var l = i+1;
//
//alert(l+"----"+result[l])
//   
//        var dArray = [];
//           var res = result[l].split("#^$");
//     
//dArray.push(res[0]);
//for(var k=1;k<res.length-1;k++){
// dArray.push(parseInt(res[k]));
//}
//i=i+2;
//     arraydata.push(dArray);
//     alert("arraydata after push-->"+arraydata)
//      var data = google.visualization.arrayToDataTable(arraydata);
//}



}


function getEmpSubPracticePieChart(practice,country,role)
{
   var subPractice = "";
    // alert("hiii");
    // transperent();
   // document.getElementById("ProjectReportPieChart").innerHTML="";
   // document.getElementById("PracticeStackedGraph").innerHTML="";
   // document.getElementById("ProjectReportBarChart").innerHTML="";
     document.getElementById("tblReportDashBoard1").style.display="none";
 //  document.getElementById("EmpStatePie").style.display="none";
     document.getElementById("empOppertunitiesPie").style.display="none";
     document.getElementById("empRequirementPie").style.display="none";
    document.getElementById("countVsStatusPie").style.display="none";
      document.getElementById("resourcesVsMainPie").style.display="none";
  document.getElementById("EmpSubPracticePie").style.display="none";
    var req = newXMLHttpRequest();
    req.onreadystatechange = function() {
        if (req.readyState == 4) {
            if (req.status == 200) {      
                document.getElementById("loadEmpSubPracticePie").style.display = 'none';
                displayEmpSubPracticePieChart(req.responseText,practice,country,role);
                if(role == 'GDC' || role == 'SSG'){
                	document.getElementById("GDCSSGPieChartsLoad").style.display = "block";
                	document.getElementById("GDCSSGPieCharts").style.display = "block";
            getEmpReportsCharts(subPractice,practice,country,role);
             }
                if(role == 'Sales'){
                	document.getElementById("SalesPieChartsLoad").style.display = "block";
                	document.getElementById("SalesPieCharts").style.display = "block";
                	getEmpSalesOppertunitiesPieChart(subPractice,practice,country,role)
                }
                // getResourceUtilizationPieChart(projectName,practice,costModel,sector,projectStatus,resourceStatus,customerName);
               // getProjectUtilizationBarChart();
            } 
        }else {
            document.getElementById("loadEmpSubPracticePie").style.display = 'block';
        }
    }; 
     var url = CONTENXT_PATH+"/getEmpSubPracticeCount.action?country="+country+"&departmentId="+role+"&practiceId="+practice;
   // alert(url);
    //var url = CONTENXT_PATH+"/getProjectUtilizationPieChart.action?graphType=1";
    // alert(url);
    req.open("GET",url,"true");    
    req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    req.send(null);
}

function displayEmpSubPracticePieChart(response,practice,country,role) {
   // alert("response--1"+response)
    var dataArray = response;
    
  
    if(response!=''){
           
        EmpSubPracticePieChart(response,practice,country,role);
   
    }else{
        document.getElementById("EmpSubPracticePie").style.display="block";
        document.getElementById("EmpSubPracticePieChart").innerHTML = "NO-DATA";         
       // spnFast.innerHTML="No Result For This Search...";                
    }
// } 
// getResourceUtilizationPieChart();
}


function EmpSubPracticePieChart(result,practice,country,role){
  
    document.getElementById("EmpSubPracticePie").style.display="block";
      
    var arraydata = [['SubPractice', 'Count']];
    
    var result1=result.split("*@!");
    
    for(var i=0; i<result1.length-1; i++){
        
        var res = result1[i].split("#^$");
        var dArray = [res[0],parseInt(res[1])];
        arraydata.push(dArray);
    }

    var data = google.visualization.arrayToDataTable(arraydata);
    var options = {
        title: 'Employee Count Vs SubPractice' ,
        titleTextStyle: {
            color: ('blue', '#3E93D4'),    // any HTML string color ('red', '#cc00cc')
            fontName: 'Times New Roman', // i.e. 'Times New Roman'
            fontSize: 15, // 12, 18 whatever you want (don't specify px)
            bold: true,    // true or false
            italic: false   // true of false
            
     
        },
        
       
        legend: 'right',
        chartArea:{
            
            width:"100%"
        },
        
        is3D: true,
        pieSliceText: 'value',
        sliceVisibilityThreshold: 0
         
    };
    // align:'center';
 //   alert("hiiii1"+practice) 
    var chart = new google.visualization.PieChart(document.getElementById("EmpSubPracticePieChart"));
  //     alert("hiiii"+practice) 
    function selectHandler() {
             
        var selectedItem = chart.getSelection()[0];
      // alert("selectedItem--"+data.getValue(selectedItem.row, 0));
        if (selectedItem) {
            var subPractice = data.getValue(selectedItem.row, 0);
      //     alert(role+"---role---"+subPractice)
            // getBdmStatisticsDetailsByLoginId(activityType,bdmId);
            if(role == 'GDC' || role == 'SSG'){
            getEmpReportsCharts(subPractice,practice,country,role);
           
            }
        }
    }
    google.visualization.events.addListener(chart, 'select', selectHandler);   
    chart.draw(data, options,dArray);
        
    
}



function getEmpReportsCharts(subPractice,practice,country,role)
{
    // $('span.pagination').empty().remove();
//    alert(role+"---role- empReportsCharts--"+subPractice)
//    var country=document.getElementById("country").value;
//    var departmentId=document.getElementById("departmentId").value;
//    var practiceId=document.getElementById("practiceId").value;
//    var subPractice2=document.getElementById("subPractice2").value;
//    var availableUtl=document.getElementById("availableUtl").value;
    //alert(country+departmentId+practiceId+subPractice2+availableUtl);
  document.getElementById("empOppertunitiesPie").style.display="none";
    document.getElementById("empRequirementPie").style.display="none";
    document.getElementById("countVsStatusPie").style.display="none";

document.getElementById("resourcesVsMainPie").style.display="none";
    document.getElementById('projectReportNote').style.display='none';
    var tableId = document.getElementById("tblReportDashBoard1");
    ClrTable(tableId);
    var req = newXMLHttpRequest();
    req.onreadystatechange = function() {
        if (req.readyState == 4) {
            if (req.status == 200) {      
                document.getElementById("projectReport").style.display = 'none';
                displaycountVsStatusPieChart(req.responseText,subPractice,practice,country,role); 
               document.getElementById('projectReportNote').style.display='none';
                getresourcesVsMainPieCharts(subPractice,practice,country,role);
            } 
        }else {
            document.getElementById("projectReport").style.display = 'block';
        }
    }; 
  //  var url = CONTENXT_PATH+"/getCountVsStatusReport.action?graphId=0&country="+country+"&department="+role+"&practiceId="+practice+"&subPractice="+subPractice;
    var url = CONTENXT_PATH+"/getEmpCurStatePieChart.action?graphId=0&country="+country+"&departmentId="+role+"&practiceId="+practice+"&subPractice="+subPractice;
  
    req.open("GET",url,"true");    
    req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    req.send(null);
}

function displaycountVsStatusPieChart(response,subPractice,practice,country,role) {
    var dataArray = response;
    if(response!=''){
           
        countVsStatusPieChart(response,subPractice,practice,country,role);
   
    }else{
        alert('No Result For This Search...');
        spnFast.innerHTML="No Result For This Search...";                
    }
     
//getIssuesDashBoardByPriority();
}


function countVsStatusPieChart(result,subPractice,practice,country,role){
    document.getElementById("countVsStatusPie").style.display="block";
      
    var arraydata = [['Status', 'Count']];
    
    var result1=result.split("*@!");
    
    for(var i=0; i<result1.length-1; i++){
        
        var res = result1[i].split("#^$");
        var dArray = [res[0],parseInt(res[1])];
        arraydata.push(dArray);
    }

    var data = google.visualization.arrayToDataTable(arraydata);
    var options = {
        title: 'Employee Count by Status' ,
        titleTextStyle: {
            color: ('blue', '#3E93D4'),    // any HTML string color ('red', '#cc00cc')
            fontName: 'Times New Roman', // i.e. 'Times New Roman'
            fontSize: 15, // 12, 18 whatever you want (don't specify px)
            bold: true,    // true or false
            italic: false   // true of false
        },
        legend: 'right',
        chartArea:{
            
            width:"100%"
           
        },
        
        is3D: true,
        pieSliceText: 'value',
        sliceVisibilityThreshold: 0
         
    };
    // align:'center';
    var chart = new google.visualization.PieChart(document.getElementById("countVsStatusPieChart"));
    function selectHandler() {
             
        var selectedItem = chart.getSelection()[0];
        //alert("selectedItem--"+data.getValue(selectedItem.row, 0));
        if (selectedItem) {
            var statusType = data.getValue(selectedItem.row, 0);
          //  alert("status"+statusType);
            // getBdmStatisticsDetailsByLoginId(activityType,bdmId);
            $('span.pagination').empty().remove();
            getResourcesByStatusList(statusType,subPractice,practice,country,role);
        }
    }
    google.visualization.events.addListener(chart, 'select', selectHandler);   
    chart.draw(data, options,dArray);
    
    
}	


function getresourcesVsMainPieCharts(subPractice,practice,country,role)
{
    $('span.pagination').empty().remove();
 
//    var country=document.getElementById("country").value;
//    var departmentId=document.getElementById("departmentId").value;
//    var practiceId=document.getElementById("practiceId").value;
//    var subPractice2=document.getElementById("subPractice2").value;
//    var availableUtl=document.getElementById("availableUtl").value;
    // alert("2nd"+country+departmentId+practiceId+subPractice2+availableUtl);
    document.getElementById("empOppertunitiesPie").style.display="none";
    document.getElementById("empRequirementPie").style.display="none";
    document.getElementById("resourcesVsMainPie").style.display="none";
  
    var tableId = document.getElementById("tblReportDashBoard1");
    ClrTable(tableId);
    
    var req = newXMLHttpRequest();
    req.onreadystatechange = function() {
        if (req.readyState == 4) {
            if (req.status == 200) {      
                document.getElementById("projectReport1").style.display = 'none';
                displayresourcesVsMainPieChart(req.responseText,subPractice,practice,country,role); 
            // getIssuesDashBoardByPriority();
            } 
        }else {
            document.getElementById("projectReport1").style.display = 'block';
        }
    }; 
  //  var url = CONTENXT_PATH+"/getCountVsStatusReport.action?graphId=1&country="+country+"&department="+role+"&practiceId="+practiceId+"&subPractice="+subPractice2+"&availableUtl="+availableUtl+"";
      var url = CONTENXT_PATH+"/getEmpCurStatePieChart.action?graphId=1&country="+country+"&departmentId="+role+"&practiceId="+practice+"&subPractice="+subPractice;
  
    req.open("GET",url,"true");    
    req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    req.send(null);
}

function displayresourcesVsMainPieChart(response,subPractice,practice,country,role) {
    var dataArray = response;
    if(response!=''){
           
        resourcesVsMainPieChart(response,subPractice,practice,country,role);
   
    }else{
        alert('No Result For This Search...');
        spnFast.innerHTML="No Result For This Search...";                
    }
    
// getIssuesDashBoardByAssignment();
}


function resourcesVsMainPieChart(result,subPractice,practice,country,role){
    document.getElementById("resourcesVsMainPie").style.display="block";
      
    var arraydata = [['EmployeeCount', 'Count']];
    
    var result1=result.split("*@!");
    
    for(var i=0; i<result1.length-1; i++){
        
        var res = result1[i].split("#^$");
        var dArray = [res[0],parseInt(res[1])];
        arraydata.push(dArray);
    }

    var data = google.visualization.arrayToDataTable(arraydata);
    var options = {
        title: 'Employee Count by ProjectStatus' ,
        titleTextStyle: {
            color: ('blue', '#3E93D4'),    // any HTML string color ('red', '#cc00cc')
            fontName: 'Times New Roman', // i.e. 'Times New Roman'
            fontSize: 15, // 12, 18 whatever you want (don't specify px)
            bold: true,    // true or false
            italic: false   // true of false
        },
        legend: 'right',
        chartArea:{
            
            width:"100%"
        },
        
        is3D: true,
        pieSliceText: 'value',
        sliceVisibilityThreshold: 0
         
    };
    // align:'center';
    var chart = new google.visualization.PieChart(document.getElementById("resourcesVsMainPieChart"));
    function selectHandler() {
             
        var selectedItem = chart.getSelection()[0];
        //alert("selectedItem--"+data.getValue(selectedItem.row, 0));
        if (selectedItem) {
            var activityType = data.getValue(selectedItem.row, 0);
            // getBdmStatisticsDetailsByLoginId(activityType,bdmId);
         //   alert(activityType);
            $('span.pagination').empty().remove();
            getResourcesVsMainList(activityType,subPractice,practice,country,role);
            
        }
    }
    google.visualization.events.addListener(chart, 'select', selectHandler);   
   
    chart.draw(data, options,dArray);
        
    
}




function  getResourcesByStatusList(activityType,subPractice,practice,country,role){
//    var country=document.getElementById("country").value;
//    var departmentId=document.getElementById("departmentId").value;
//    var practiceId=document.getElementById("practiceId").value;
//    var subPractice2=document.getElementById("subPractice2").value;
//    var availableUtl=document.getElementById("availableUtl").value;
//	alert("hiiiiiii")
    var tableId = document.getElementById("tblReportDashBoard1");
   ClrTable(tableId);
   document.getElementById("empOppertunitiesPie").style.display="none";
   document.getElementById("empRequirementPie").style.display="none";
    
  
    document.getElementById('projectReporssst').style.display='block';
   
    document.getElementById('projectReportNote').style.display='none';
    // var url = CONTENXT_PATH+"/getTaskListByStatus.action?reportsTo="+reportsTo+"&taskStartDate="+taskStartDate+"&taskEndDate="+taskEndDate+"&activityType="+activityType+"&graphId="+graphId;
    var url = CONTENXT_PATH+"/getEmployeeDetailsThroughState.action?state="+activityType+"&graphId=0&country="+country+"&departmentId="+role+"&practiceId="+practice+"&subPractice="+subPractice;

    var req = newXMLHttpRequest();
    req.onreadystatechange = function() {
        if (req.readyState == 4) {
            if (req.status == 200) {  
                document.getElementById('projectReportNote').style.display='block';
                document.getElementById("projectReporssst").style.display = 'none';
                displayResourcesByStatusList(req.responseText,activityType,subPractice,practice,country,role);   
              pagerOption2();
              document.getElementById("tblReportDashBoard1").style.display='block';
           //   alert("finidh");
            } 
        }else {
            document.getElementById("projectReporssst").style.display = 'block';
        }
    };
    req.open("GET", url, true);
    req.send(null); 
}


function displayResourcesByStatusList(response,activityType,subPractice,practice,country,role){
    var tableId = document.getElementById("tblReportDashBoard1"); 
  
      //   alert(response);
    if (activityType=='Training' || activityType=='OverHead' || activityType=='R&D/POC'){
        var headerFields = new Array("S.No","Emp Name","ReportsTo","Email","WorkPhone","HireDate","Practice","SubPractice","StartDate");
    }else if (activityType=='OnProject'){
        var headerFields = new Array("S.No","Emp Name","ReportsTo","Email","WorkPhone","HireDate","Practice","SubPractice","StartDate","Utilization %","Exp");
    }
    else{
        var headerFields = new Array("S.No","Emp Name","ReportsTo","Email","WorkPhone",activityType+" Utilization %","Experience");
    }
    var dataArray = response;
    ResourcesByStatusListParseAndGenerateHTML(tableId,dataArray, headerFields,activityType,subPractice,practice,country,role);
}
 
function ResourcesByStatusListParseAndGenerateHTML(oTable,responseString,headerFields,activityType,subPractice,practice,country,role) {
    var fieldDelimiter = "#^$";
    var recordDelimiter = "*@!";   
 //   var records = responseString.split("*@!"); 
    generateResourcesByStatusListRows(oTable,headerFields,responseString,fieldDelimiter,activityType);
}



function generateResourcesByStatusListRows(oTable, headerFields,responseString,fieldDelimiter,activityType) {	


    tbody = oTable.childNodes[0];    
    var tbody = document.createElement("TBODY");
    oTable.appendChild(tbody);
    
    generateTableHeader(tbody,headerFields);
    
    var records = responseString.split("*@!");
//	alert("records"+records);
    var rowlength;
    rowlength = records.length;
    if(rowlength >0 && records!=""){
        for(var i=0;i<rowlength-1;i++) {
              //  alert(i+"..records  "+records[i]);
           generateResourcesByStatusList(i,oTable,tbody,records[i],fieldDelimiter,activityType);
        }
        
    } else {
        generateNoRecordsFound(tbody,oTable,headerFields.length);
    }
    generateResourcesByStatusListFooter(tbody,oTable,headerFields.length);
   // pagerOption();
}


function generateResourcesByStatusList(index,oTable,tableBody,record,delimiter,activityType){
    var row;
    var cell;
    var fieldLength;
    //  var fields = record.split(delimiter);
   // alert("fields--->"+record);
    var fields = record.split("#^$");
   
    fieldLength = fields.length;
    var length = fieldLength;
    
   // alert(fields[2]);   
    
    row = document.createElement( "TR" );
    row.className="gridRowEven";
    tableBody.appendChild( row );
   //alert("ccccc"+activityType)
    if (activityType=='Training' || activityType=='OverHead' || activityType=='R&D/POC'){
        cell = document.createElement( "TD" );
        cell.className="gridColumn";    
          
        cell.innerHTML =(index+1) ;
        cell.setAttribute("align","left");   
        row.appendChild( cell ); 
            
            
            
            
        cell = document.createElement( "TD" );
        cell.className="gridColumn";    
        cell.innerHTML =fields[2] ;
        cell.setAttribute("align","left");   
        row.appendChild( cell );  
    
 
        cell = document.createElement( "TD" );
        cell.className="gridColumn";    
        cell.innerHTML =fields[3] ;
        cell.setAttribute("align","left");   
        row.appendChild( cell );  
            
        cell = document.createElement( "TD" );
        cell.className="gridColumn";    
        cell.innerHTML =fields[4] ;
        cell.setAttribute("align","left");   
        row.appendChild( cell );  
            
            
        cell = document.createElement( "TD" );
        cell.className="gridColumn";    
        cell.innerHTML =fields[5] ;
        cell.setAttribute("align","left");   
        row.appendChild( cell );  
            
        cell = document.createElement( "TD" );
        cell.className="gridColumn";    
        cell.innerHTML =fields[6] ;
        cell.setAttribute("align","left");   
        row.appendChild( cell ); 
        
        cell = document.createElement( "TD" );
        cell.className="gridColumn";    
        cell.innerHTML =fields[7] ;
        cell.setAttribute("align","left");   
        row.appendChild( cell );
        
        cell = document.createElement( "TD" );
        cell.className="gridColumn"; 
         cell.innerHTML =fields[8] ;
        cell.setAttribute("align","left");   
        row.appendChild( cell );
        
        cell = document.createElement( "TD" );
        cell.className="gridColumn"; 
        cell.innerHTML =fields[9] ;
        cell.setAttribute("align","left");   
        row.appendChild( cell );
            
            
              
    }else if (activityType=='OnProject'){
          
        cell = document.createElement( "TD" );
        cell.className="gridColumn";    
        cell.innerHTML =(index+1) ;
        cell.setAttribute("align","left");   
        row.appendChild( cell ); 
            
            
            
            
        cell = document.createElement( "TD" );
        cell.className="gridColumn";    
        cell.innerHTML =fields[2] ;
        cell.setAttribute("align","left");   
        row.appendChild( cell );  
    
 
        cell = document.createElement( "TD" );
        cell.className="gridColumn";    
        cell.innerHTML =fields[3] ;
        cell.setAttribute("align","left");   
        row.appendChild( cell );  
            
        cell = document.createElement( "TD" );
        cell.className="gridColumn";    
        cell.innerHTML =fields[4] ;
        cell.setAttribute("align","left");   
        row.appendChild( cell );  
            
            
        cell = document.createElement( "TD" );
        cell.className="gridColumn";    
        cell.innerHTML =fields[5] ;
        cell.setAttribute("align","left");   
        row.appendChild( cell );  
            
        cell = document.createElement( "TD" );
        cell.className="gridColumn";    
        cell.innerHTML =fields[6] ;
        cell.setAttribute("align","left");   
        row.appendChild( cell ); 
        
        cell = document.createElement( "TD" );
        cell.className="gridColumn";    
         cell.innerHTML =fields[7] ;
        cell.setAttribute("align","left");   
        row.appendChild( cell );
        
        cell = document.createElement( "TD" );
        cell.className="gridColumn"; 
        cell.innerHTML =fields[8] ;
        cell.setAttribute("align","left");   
        row.appendChild( cell );
        
        cell = document.createElement( "TD" );
        cell.className="gridColumn"; 
        cell.innerHTML =fields[9] ;
        cell.setAttribute("align","left");   
        row.appendChild( cell );
        
        cell = document.createElement( "TD" );
        cell.className="gridColumn"; 
        cell.innerHTML = "<a href='javascript:getUtilizationDetails("+fields[0]+")'>"+fields[10]+"</a>";
        cell.setAttribute("align","left");   
        row.appendChild( cell );
        
        cell = document.createElement( "TD" );
        cell.className="gridColumn"; 
        cell.innerHTML =fields[11] ;
        cell.setAttribute("align","left");   
        row.appendChild( cell );
          
          
    }else{
       // alert("fields--->"+fields[2]);   
        cell = document.createElement( "TD" );
        cell.className="gridColumn";    
        cell.innerHTML =(index+1) ;
        cell.setAttribute("align","left");   
        row.appendChild( cell ); 
            
            
            
            
        cell = document.createElement( "TD" );
        cell.className="gridColumn";    
        cell.innerHTML =fields[2] ;
        cell.setAttribute("align","left");   
        row.appendChild( cell );  
    
 
        cell = document.createElement( "TD" );
        cell.className="gridColumn";    
        cell.innerHTML =fields[3] ;
        cell.setAttribute("align","left");   
        row.appendChild( cell );  
            
        cell = document.createElement( "TD" );
        cell.className="gridColumn";    
        cell.innerHTML =fields[4] ;
        cell.setAttribute("align","left");   
        row.appendChild( cell );  
            
            
        cell = document.createElement( "TD" );
        cell.className="gridColumn";    
        cell.innerHTML =fields[5] ;
        cell.setAttribute("align","left");   
        row.appendChild( cell );  
            
       cell = document.createElement( "TD" );
        cell.className="gridColumn";    
        cell.innerHTML = "<a href='javascript:getUtilizationDetails("+fields[0]+")'>"+fields[6]+"</a>";
        cell.setAttribute("align","left");   
        row.appendChild( cell );  
            
        cell = document.createElement( "TD" );
        cell.className="gridColumn";    
        cell.innerHTML =fields[7] ;
        cell.setAttribute("align","left");   
        row.appendChild( cell ); 
             
          
           
    }
            
           
           
}


function  getResourcesVsMainList(activityType,subPractice,practice,country,role){
//    var country=document.getElementById("country").value;
//    var departmentId=document.getElementById("departmentId").value;
//    var practiceId=document.getElementById("practiceId").value;
//    var subPractice2=document.getElementById("subPractice2").value;
    var tableId = document.getElementById("tblReportDashBoard1");
    ClrTable(tableId);
  
    document.getElementById('projectReporssst').style.display='block';
   
    // var url = CONTENXT_PATH+"/getTaskListByStatus.action?reportsTo="+reportsTo+"&taskStartDate="+taskStartDate+"&taskEndDate="+taskEndDate+"&activityType="+activityType+"&graphId="+graphId;
  //  var url = CONTENXT_PATH+"/getEmployeeDetailsThroughState.action?activityType="+activityType+"&graphId=1&&country="+country+"&departmentId="+role+"&practiceId="+practice+"&subPractice="+subPractice2+"";
 var url = CONTENXT_PATH+"/getEmployeeDetailsThroughState.action?state="+activityType+"&graphId=1&country="+country+"&departmentId="+role+"&practiceId="+practice+"&subPractice="+subPractice;

    var req = newXMLHttpRequest();
    req.onreadystatechange = function() {
        if (req.readyState == 4) {
            if (req.status == 200) {      
                document.getElementById("projectReporssst").style.display = 'none';
                displayResourcesVsMainList(req.responseText,activityType,subPractice,practice,country,role); 
                 pagerOption2();
                   document.getElementById("tblReportDashBoard1").style.display='block';
                
            } 
        }else {
            document.getElementById("projectReporssst").style.display = 'block';
        }
    };
    req.open("GET", url, true);
    req.send(null); 
}


function displayResourcesVsMainList(response,activityType,subPractice,practice,country,role){
    var tableId = document.getElementById("tblReportDashBoard1");  
    var headerFields = new Array("S.No","ResourceName","CustomerName","ProjectName","StartDate","EndDate","Utilization","Billable","EmpProjStatus","Practice","SubPractice");
   
    var dataArray = response;
    ResourcesVsMainListParseAndGenerateHTML(tableId,dataArray, headerFields,activityType);
}
 
function ResourcesVsMainListParseAndGenerateHTML(oTable,responseString,headerFields,activityType) {
    var fieldDelimiter = "#^$";
    var recordDelimiter = "*@!";   
    var records = responseString.split(recordDelimiter); 
    generateResourcesVsMainListRows(oTable,headerFields,records,fieldDelimiter,activityType);
}



function generateResourcesVsMainListRows(oTable, headerFields,records,fieldDelimiter,activityType) {	

    tbody = oTable.childNodes[0];    
    var tbody = document.createElement("TBODY");
    oTable.appendChild(tbody);
    generateTableHeader(tbody,headerFields);
    var rowlength;
    rowlength = records.length;
    if(rowlength >0 && records!=""){
        for(var i=0;i<rowlength-1;i++) {
                
            generateResourcesVsMainListRows1(i,oTable,tbody,records[i],fieldDelimiter,activityType);
        }
        
    } else {
        generateNoRecordsFound(tbody,oTable,headerFields.length);
    }
    generateResourcesByStatusListFooter(tbody,oTable,headerFields.length);
   
}

// Santhosh.Kola#^$Miracle Software Systems . Inc(Intrenal Projects)#^$Hubble#^$2012-06-11 00:00:00#^$-#^$90#^$-#^$Shadow#^$
function generateResourcesVsMainListRows1(index,oTable,tableBody,record,delimiter,activityType){
    var row;
    var cell;
    var fieldLength;
    //  var fields = record.split(delimiter);
    
  
    var fields = record.split("#^$");
  //  alert(fields)
    fieldLength = fields.length ;
    var length = fieldLength;
    
  //  alert(fields[0])    
    
    row = document.createElement( "TR" );
    row.className="gridRowEven";
    tableBody.appendChild( row );
    
    
    cell = document.createElement( "TD" );
    cell.className="gridColumn";    
          
    cell.innerHTML =(index+1) ;
    cell.setAttribute("align","left");   
    row.appendChild(cell); 
            
    cell = document.createElement( "TD" );
    cell.className="gridColumn";    
          
    cell.innerHTML =fields[0] ;
    cell.setAttribute("align","left");   
    row.appendChild( cell );  
    
 
    cell = document.createElement( "TD" );
    cell.className="gridColumn";    
          
    cell.innerHTML =fields[1] ;
    cell.setAttribute("align","left");   
    row.appendChild( cell );  
            
    cell = document.createElement( "TD" );
    cell.className="gridColumn";    
          
    cell.innerHTML =fields[2] ;
    cell.setAttribute("align","left");   
    row.appendChild( cell );  
            
    cell = document.createElement( "TD" );
    cell.className="gridColumn";    
          
    cell.innerHTML =fields[3] ;
    cell.setAttribute("align","left");   
    row.appendChild( cell );  
            
    cell = document.createElement( "TD" );
    cell.className="gridColumn";    
          
    cell.innerHTML =fields[4] ;
    cell.setAttribute("align","left");   
    row.appendChild( cell );
            
    cell = document.createElement( "TD" );
    cell.className="gridColumn";    
          
    cell.innerHTML =fields[5] ;
    cell.setAttribute("align","left");   
    row.appendChild( cell );
            
            
    cell = document.createElement( "TD" );
    cell.className="gridColumn";    
          
    cell.innerHTML =fields[6] ;
    cell.setAttribute("align","left");   
    row.appendChild( cell );
            
    cell = document.createElement( "TD" );
    cell.className="gridColumn";    
          
    cell.innerHTML =fields[7] ;
    cell.setAttribute("align","left");   
    row.appendChild( cell );
            
            
    cell = document.createElement( "TD" );
    cell.className="gridColumn";    
          
    cell.innerHTML =fields[8] ;
    cell.setAttribute("align","left");   
    row.appendChild( cell );
    cell = document.createElement( "TD" );
    cell.className="gridColumn";    
          
    cell.innerHTML =fields[9] ;
    cell.setAttribute("align","left");   
    row.appendChild( cell );
}

function generateTableHeader11(tableBody,headerFields) {
    var row;
    var cell;
    row = document.createElement( "TR" );
    row.className="gridHeader";
    tableBody.appendChild( row );
    for (var i=0; i<headerFields.length; i++) {
        cell = document.createElement( "TD" );
        cell.className="gridHeader";
        row.appendChild( cell );

        cell.setAttribute("width","10000px");
        cell.innerHTML = headerFields[i];
    }
}


function getUtilizationDetails(empId){
    var url = CONTENXT_PATH+"/getUtilizationDetails.action?empId="+empId;

    var req = newXMLHttpRequest();
    req.onreadystatechange = function() {
        if (req.readyState == 4) {
            if (req.status == 200) {      
              //  alert(req.responseText);
                displayUtilizationDetails(req.responseText);                        
            } 
        }else {
        // alert("Http-Error");
        }
    };
    req.open("GET", url, true);
    req.send(null);
}

function displayUtilizationDetails(response){
    var background = "#3E93D4";
    var title = "Utilization Details";
    var size = response.length;
  
    // var text = response.split("addTo");

    var content = "<html><head><title>"+title+"</title></head>\
   <body bgcolor='"+background +"' style='color:white;'><h4>"+title+"</h4><br/>\n";
    var tableData=response;
    if(tableData == "no data")
    {
        alert("No records found");
    }
   
    else
    {
        var tblRow=tableData.split("*@!");
      //  alert(tblRow.length);
        if(tblRow.length == 1)
        {
       
            content=content+"<table border='1' style:align='center'>";

            content=content+"<tr><th>ProjectName</th><th>Utilization</th></tr>";
            content=content+"<tr> <td colspan='4'> No record found for this  search </td> </tr>";

            alert("No records found");
        }
   
        else
        {
          //  alert(tblRow);
  
            content=content+"<table border='1' style:align='center'>";

            content=content+"<tr><th>ProjectName</th><th>Utilization</th></tr>";


            for(var n=0;n<tblRow.length;n++){
                content=content+"<tr>";
                var tblCol=tblRow[n].split("#^$");
                for(var j=0;j<tblCol.length;j++){
                    content=content+"<td>"+tblCol[j]+"</td>";   
                }
                content=content+"</tr>";
            }
            content=content+"</table>";
        }
    }

content=content+"</body></html>";
       
if(size < 50){
    //Create the popup       
    popup = window.open("","window","channelmode=0,width=300,height=150,top=200,left=350,scrollbars=no,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no");    
    popup.document.write(content); //Write content into it.    
}
    
else if(size < 100){
    //Create the popup       channelmode
    popup = window.open("","window","=0,width=400,height=200,top=200,left=350,scrollbars=no,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no");    
    popup.document.write(content); //Write content into it.    
}
    
else if(size < 260){
    //Create the popup       
    popup = window.open("","window","channelmode=0,width=400,height=200,top=200,left=350,scrollbars=no,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no");    
    popup.document.write(content); //Write content into it.    
} else {
    //Create the popup       
    popup = window.open("","window","channelmode=0,width=400,height=200,top=200,left=350,scrollbars=no,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no");    
    popup.document.write(content); //Write content into it.    
}
}


function generateNoRecordsFound(tbody,oTable,len) {
   
    var noRecords =document.createElement("TR");
    noRecords.className="gridRowEven";
    tbody.appendChild(noRecords);
    cell = document.createElement("TD");
    cell.className="gridColumn";
    cell.colSpan = len;
    
    cell.innerHTML = "No Records Found for this Search";
    noRecords.appendChild(cell);
}
function generateResourcesByStatusListFooter(tbody,oTable,len) {
  
    var cell;
    var footer =document.createElement("TR");
    footer.className="gridPager";
    footer.setAttribute("Id", "taskFooter");
    tbody.appendChild(footer);
    cell = document.createElement("TD");
    cell.className="gridFooter";
    

    // cell.colSpan = "7";
    
    cell.colSpan = len;
    
       
   

    footer.appendChild(cell);
}



function getEmpSalesOppertunitiesPieChart(subpractice,practice,country,role)
{
   var subPractice = "";
    // alert("hiii");
    // transperent();
   // document.getElementById("ProjectReportPieChart").innerHTML="";
   // document.getElementById("PracticeStackedGraph").innerHTML="";
   // document.getElementById("ProjectReportBarChart").innerHTML="";
     document.getElementById("tblReportDashBoard1").style.display="none";
 //  document.getElementById("EmpStatePie").style.display="none";
     document.getElementById("empOppertunitiesPie").style.display="none";
     document.getElementById("empRequirementPie").style.display="none";
    document.getElementById("countVsStatusPie").style.display="none";
      document.getElementById("resourcesVsMainPie").style.display="none";
 
    var req = newXMLHttpRequest();
    req.onreadystatechange = function() {
        if (req.readyState == 4) {
            if (req.status == 200) {      
                document.getElementById("empOppertunitiesLoad").style.display = 'none';
                displayEmpSalesOppertunitiesPieChart(req.responseText,subpractice,practice,country,role);
                getEmpSalesRequirementPieChart(subpractice,practice,country,role)
                // getResourceUtilizationPieChart(projectName,practice,costModel,sector,projectStatus,resourceStatus,customerName);
               // getProjectUtilizationBarChart();
            } 
        }else {
            document.getElementById("empOppertunitiesLoad").style.display = 'block';
        }
    }; 
     var url = CONTENXT_PATH+"/getEmpSalesOppertunitiesPieChart.action?country="+country+"&departmentId="+role+"&practiceId="+practice+"&subPractice="+subpractice;
   // alert(url);
    //var url = CONTENXT_PATH+"/getProjectUtilizationPieChart.action?graphType=1";
    // alert(url);
    req.open("GET",url,"true");    
    req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    req.send(null);
}

function displayEmpSalesOppertunitiesPieChart(response,subpractice,practice,country,role) {
   // alert("response--1"+response)
    var dataArray = response;
    
  
    if(response!=''){
           
        EmpSalesOppertunitiesPieChart(response,subpractice,practice,country,role);
   
    }else{
        document.getElementById("empOppertunitiesPie").style.display="block";
        document.getElementById("empOppertunitiesPieChart").innerHTML = "NO-DATA";         
       // spnFast.innerHTML="No Result For This Search...";                
    }
// } 
// getResourceUtilizationPieChart();
}


function EmpSalesOppertunitiesPieChart(result,subpractice,practice,country,role){
  
    document.getElementById("empOppertunitiesPie").style.display="block";
      
    var arraydata = [['Status', 'Count']];
    
    var result1=result.split("*@!");
    
    for(var i=0; i<result1.length-1; i++){
        
        var res = result1[i].split("#^$");
        var dArray = [res[0],parseInt(res[1])];
        arraydata.push(dArray);
    }

    var data = google.visualization.arrayToDataTable(arraydata);
    var options = {
        title: 'Employee Count Vs Oppertunities Status' ,
        titleTextStyle: {
            color: ('blue', '#3E93D4'),    // any HTML string color ('red', '#cc00cc')
            fontName: 'Times New Roman', // i.e. 'Times New Roman'
            fontSize: 15, // 12, 18 whatever you want (don't specify px)
            bold: true,    // true or false
            italic: false   // true of false
            
     
        },
        
       
        legend: 'right',
        chartArea:{
            
            width:"100%"
        },
        
        is3D: true,
        pieSliceText: 'value',
        sliceVisibilityThreshold: 0
         
    };
    // align:'center';
 //   alert("hiiii1"+practice) 
    var chart = new google.visualization.PieChart(document.getElementById("empOppertunitiesPieChart"));
  //     alert("hiiii"+practice) 
    
    google.visualization.events.addListener(chart);   
    chart.draw(data, options,dArray);
        
    
}


function getEmpSalesRequirementPieChart(subpractice,practice,country,role)
{
   var subPractice = "";
    // alert("hiii");
    // transperent();
   // document.getElementById("ProjectReportPieChart").innerHTML="";
   // document.getElementById("PracticeStackedGraph").innerHTML="";
   // document.getElementById("ProjectReportBarChart").innerHTML="";
     document.getElementById("tblReportDashBoard1").style.display="none";
 //  document.getElementById("EmpStatePie").style.display="none";
   
     document.getElementById("empRequirementPie").style.display="none";
    document.getElementById("countVsStatusPie").style.display="none";
      document.getElementById("resourcesVsMainPie").style.display="none";
 
    var req = newXMLHttpRequest();
    req.onreadystatechange = function() {
        if (req.readyState == 4) {
            if (req.status == 200) {      
                document.getElementById("empRequirementLoad").style.display = 'none';
                displayEmpSalesRequirementPieChart(req.responseText,practice,country,role);
               
                // getResourceUtilizationPieChart(projectName,practice,costModel,sector,projectStatus,resourceStatus,customerName);
               // getProjectUtilizationBarChart();
            } 
        }else {
            document.getElementById("empRequirementLoad").style.display = 'block';
        }
    }; 
     var url = CONTENXT_PATH+"/getEmpSalesRequirementPieChart.action?country="+country+"&departmentId="+role+"&practiceId="+practice+"&subPractice="+subpractice;
   // alert(url);
    //var url = CONTENXT_PATH+"/getProjectUtilizationPieChart.action?graphType=1";
    // alert(url);
    req.open("GET",url,"true");    
    req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    req.send(null);
}

function displayEmpSalesRequirementPieChart(response,subpractice,practice,country,role) {
   // alert("response--1"+response)
    var dataArray = response;
    
  
    if(response!=''){
           
        EmpSalesRequirementPieChart(response,practice,country,role);
   
    }else{
        document.getElementById("empRequirementPie").style.display="block";
        document.getElementById("empRequirementPieChart").innerHTML = "NO-DATA";         
       // spnFast.innerHTML="No Result For This Search...";                
    }
// } 
// getResourceUtilizationPieChart();
}


function EmpSalesRequirementPieChart(result,subprac,ticepractice,country,role){
  
    document.getElementById("empRequirementPie").style.display="block";
      
    var arraydata = [['Status', 'Count']];
    
    var result1=result.split("*@!");
    
    for(var i=0; i<result1.length-1; i++){
        
        var res = result1[i].split("#^$");
        var dArray = [res[0],parseInt(res[1])];
        arraydata.push(dArray);
    }

    var data = google.visualization.arrayToDataTable(arraydata);
    var options = {
        title: 'Employee Count Vs Requirement Status' ,
        titleTextStyle: {
            color: ('blue', '#3E93D4'),    // any HTML string color ('red', '#cc00cc')
            fontName: 'Times New Roman', // i.e. 'Times New Roman'
            fontSize: 15, // 12, 18 whatever you want (don't specify px)
            bold: true,    // true or false
            italic: false   // true of false
            
     
        },
        
       
        legend: 'right',
        chartArea:{
            
            width:"100%"
        },
        
        is3D: true,
        pieSliceText: 'value',
        sliceVisibilityThreshold: 0
         
    };
    // align:'center';
 //   alert("hiiii1"+practice) 
    var chart = new google.visualization.PieChart(document.getElementById("empRequirementPieChart"));
  //     alert("hiiii"+practice) 
    
    google.visualization.events.addListener(chart);   
    chart.draw(data, options,dArray);
        
    
}


// ============ Function For Sales ======================= //



function getSalesAccStatus()
{
	 
	    document.getElementById("salesAccRegionPie").style.display="none";
	      document.getElementById("salesAccTerritoryPie").style.display="none";
	      document.getElementById("salesAccStatePie").style.display="none";
	      
    var req = newXMLHttpRequest();
    req.onreadystatechange = function() {
        if (req.readyState == 4) {
            if (req.status == 200) {      
                document.getElementById("SalesAccStatusLoad").style.display = 'none';
                displaySalescountVsStatusPieChart(req.responseText); 
             
                getSalesAccRegion();
            } 
        }else {
            document.getElementById("SalesAccStatusLoad").style.display = 'block';
        }
    }; 
  //  var url = CONTENXT_PATH+"/getCountVsStatusReport.action?graphId=0&country="+country+"&department="+role+"&practiceId="+practice+"&subPractice="+subPractice;
    var url = CONTENXT_PATH+"/getSalesAccStatusPieChart.action?graphId=0";
  
    req.open("GET",url,"true");    
    req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    req.send(null);
}

function displaySalescountVsStatusPieChart(response) {
    var dataArray = response;
    if(response!=''){
           
    	salescountVsStatusPieChart(response);
   
    }else{
        alert('No Result For This Search...');
        spnFast.innerHTML="No Result For This Search...";                
    }
     
//getIssuesDashBoardByPriority();
}


function salescountVsStatusPieChart(result){
    document.getElementById("salesAccStatusPie").style.display="block";
      
    var arraydata = [['Status', 'Count']];
    
    var result1=result.split("*@!");
    
    for(var i=0; i<result1.length-1; i++){
        
        var res = result1[i].split("#^$");
        var dArray = [res[0],parseInt(res[1])];
        arraydata.push(dArray);
    }

    var data = google.visualization.arrayToDataTable(arraydata);
    var options = {
        title: 'Accounts Count by Status' ,
        titleTextStyle: {
            color: ('blue', '#3E93D4'),    // any HTML string color ('red', '#cc00cc')
            fontName: 'Times New Roman', // i.e. 'Times New Roman'
            fontSize: 15, // 12, 18 whatever you want (don't specify px)
            bold: true,    // true or false
            italic: false   // true of false
        },
        legend: 'right',
        chartArea:{
            
            width:"100%"
           
        },
        
        is3D: true,
        pieSliceText: 'value',
        sliceVisibilityThreshold: 0
         
    };
    // align:'center';
    var chart = new google.visualization.PieChart(document.getElementById("salesAccStatusPieChart"));
    function selectHandler() {
             
        var selectedItem = chart.getSelection()[0];
        //alert("selectedItem--"+data.getValue(selectedItem.row, 0));
        if (selectedItem) {
            var statusType = data.getValue(selectedItem.row, 0);
          //  alert("status"+statusType);
            // getBdmStatisticsDetailsByLoginId(activityType,bdmId);
            $('span.pagination').empty().remove();
         //   getResourcesByStatusList(statusType,subPractice,practice,country,role);
        }
    }
    google.visualization.events.addListener(chart, 'select', selectHandler);   
    chart.draw(data, options,dArray);
    
    
}	




function getSalesAccRegion()
{
	 document.getElementById("salesAccTerritoryPie").style.display="none";
     document.getElementById("salesAccStatePie").style.display="none";
    var req = newXMLHttpRequest();
    req.onreadystatechange = function() {
        if (req.readyState == 4) {
            if (req.status == 200) {      
                document.getElementById("SalesAccRegionLoad").style.display = 'none';
                salesdisplaycountVsRegionPieChart(req.responseText); 
                getSalesAccTerritory();
              
            } 
        }else {
            document.getElementById("SalesAccRegionLoad").style.display = 'block';
        }
    }; 
  //  var url = CONTENXT_PATH+"/getCountVsStatusReport.action?graphId=0&country="+country+"&department="+role+"&practiceId="+practice+"&subPractice="+subPractice;
    var url = CONTENXT_PATH+"/getSalesAccRegionPieChart.action?graphId=1";
  
    req.open("GET",url,"true");    
    req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    req.send(null);
}

function salesdisplaycountVsRegionPieChart(response) {
    var dataArray = response;
    if(response!=''){
           
        salescountVsRegionPieChart(response);
   
    }else{
        alert('No Result For This Search...');
        spnFast.innerHTML="No Result For This Search...";                
    }
     
//getIssuesDashBoardByPriority();
}


function salescountVsRegionPieChart(result){
    document.getElementById("salesAccRegionPie").style.display="block";
      
    var arraydata = [['Status', 'Count']];
    
    var result1=result.split("*@!");
    
    for(var i=0; i<result1.length-1; i++){
        
        var res = result1[i].split("#^$");
        var dArray = [res[0],parseInt(res[1])];
        arraydata.push(dArray);
    }

    var data = google.visualization.arrayToDataTable(arraydata);
    var options = {
        title: 'Accounts Count by Region' ,
        titleTextStyle: {
            color: ('blue', '#3E93D4'),    // any HTML string color ('red', '#cc00cc')
            fontName: 'Times New Roman', // i.e. 'Times New Roman'
            fontSize: 15, // 12, 18 whatever you want (don't specify px)
            bold: true,    // true or false
            italic: false   // true of false
        },
        legend: 'right',
        chartArea:{
            
            width:"100%"
           
        },
        
        is3D: true,
        pieSliceText: 'value',
        sliceVisibilityThreshold: 0
         
    };
    // align:'center';
    var chart = new google.visualization.PieChart(document.getElementById("salesAccRegionPieChart"));
    function selectHandler() {
             
        var selectedItem = chart.getSelection()[0];
        //alert("selectedItem--"+data.getValue(selectedItem.row, 0));
        if (selectedItem) {
            var statusType = data.getValue(selectedItem.row, 0);
           // alert("status"+statusType);
            // getBdmStatisticsDetailsByLoginId(activityType,bdmId);
            $('span.pagination').empty().remove();
          //  getResourcesByStatusList(statusType,subPractice,practice,country,role);
        }
    }
    google.visualization.events.addListener(chart, 'select', selectHandler);   
    chart.draw(data, options,dArray);
    
    
}	



function getSalesAccTerritory()
{
	 document.getElementById("salesAccStatePie").style.display="none";
    var req = newXMLHttpRequest();
    req.onreadystatechange = function() {
        if (req.readyState == 4) {
            if (req.status == 200) {      
                document.getElementById("SalesAccTerritoryLoad").style.display = 'none';
                displaySalescountVsTerritoryPieChart(req.responseText); 
             
                getSalesAccState();
            } 
        }else {
            document.getElementById("SalesAccTerritoryLoad").style.display = 'block';
        }
    }; 
  //  var url = CONTENXT_PATH+"/getCountVsStatusReport.action?graphId=0&country="+country+"&department="+role+"&practiceId="+practice+"&subPractice="+subPractice;
    var url = CONTENXT_PATH+"/getSalesAccTerritoryPieChart.action?graphId=2";
  
    req.open("GET",url,"true");    
    req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    req.send(null);
}

function displaySalescountVsTerritoryPieChart(response) {
    var dataArray = response;
    if(response!=''){
           
    	salescountVsTerritoryPieChart(response);
   
    }else{
        alert('No Result For This Search...');
        spnFast.innerHTML="No Result For This Search...";                
    }
     
//getIssuesDashBoardByPriority();
}


function salescountVsTerritoryPieChart(result){
    document.getElementById("salesAccTerritoryPie").style.display="block";
      
    var arraydata = [['Status', 'Count']];
    
    var result1=result.split("*@!");
    
    for(var i=0; i<result1.length-1; i++){
        
        var res = result1[i].split("#^$");
        var dArray = [res[0],parseInt(res[1])];
        arraydata.push(dArray);
    }

    var data = google.visualization.arrayToDataTable(arraydata);
    var options = {
        title: 'Accounts Count by Territory' ,
        titleTextStyle: {
            color: ('blue', '#3E93D4'),    // any HTML string color ('red', '#cc00cc')
            fontName: 'Times New Roman', // i.e. 'Times New Roman'
            fontSize: 15, // 12, 18 whatever you want (don't specify px)
            bold: true,    // true or false
            italic: false   // true of false
        },
        legend: 'right',
        chartArea:{
            
            width:"100%"
           
        },
        
        is3D: true,
        pieSliceText: 'value',
        sliceVisibilityThreshold: 0
         
    };
    // align:'center';
    var chart = new google.visualization.PieChart(document.getElementById("salesAccTerritoryPieChart"));
    function selectHandler() {
             
        var selectedItem = chart.getSelection()[0];
        //alert("selectedItem--"+data.getValue(selectedItem.row, 0));
        if (selectedItem) {
            var statusType = data.getValue(selectedItem.row, 0);
          //  alert("status"+statusType);
            // getBdmStatisticsDetailsByLoginId(activityType,bdmId);
            $('span.pagination').empty().remove();
         //   getResourcesByStatusList(statusType,subPractice,practice,country,role);
        }
    }
    google.visualization.events.addListener(chart, 'select', selectHandler);   
    chart.draw(data, options,dArray);
    
    
}	




function getSalesAccState()
{
	 
   
      
    var req = newXMLHttpRequest();
    req.onreadystatechange = function() {
        if (req.readyState == 4) {
            if (req.status == 200) {      
                document.getElementById("SalesAccStateLoad").style.display = 'none';
                salesdisplaycountVsStatePieChart(req.responseText); 
             
              
            } 
        }else {
            document.getElementById("SalesAccStateLoad").style.display = 'block';
        }
    }; 
  //  var url = CONTENXT_PATH+"/getCountVsStatusReport.action?graphId=0&country="+country+"&department="+role+"&practiceId="+practice+"&subPractice="+subPractice;
    var url = CONTENXT_PATH+"/getSalesAccStatePieChart.action?graphId=3";
  
    req.open("GET",url,"true");    
    req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    req.send(null);
}

function salesdisplaycountVsStatePieChart(response) {
    var dataArray = response;
    if(response!=''){
           
        salescountVsStatePieChart(response);
   
    }else{
        alert('No Result For This Search...');
        spnFast.innerHTML="No Result For This Search...";                
    }
     
//getIssuesDashBoardByPriority();
}


function salescountVsStatePieChart(result){
    document.getElementById("salesAccStatePie").style.display="block";
      
    var arraydata = [['Status', 'Count']];
    
    var result1=result.split("*@!");
    
    for(var i=0; i<result1.length-1; i++){
        
        var res = result1[i].split("#^$");
        var dArray = [res[0],parseInt(res[1])];
        arraydata.push(dArray);
    }

    var data = google.visualization.arrayToDataTable(arraydata);
    var options = {
        title: 'Accounts Count by State' ,
        titleTextStyle: {
            color: ('blue', '#3E93D4'),    // any HTML string color ('red', '#cc00cc')
            fontName: 'Times New Roman', // i.e. 'Times New Roman'
            fontSize: 15, // 12, 18 whatever you want (don't specify px)
            bold: true,    // true or false
            italic: false   // true of false
        },
        legend: 'right',
        chartArea:{
            
            width:"100%"
           
        },
        
        is3D: true,
        pieSliceText: 'value',
        sliceVisibilityThreshold: 0
         
    };
    // align:'center';
    var chart = new google.visualization.PieChart(document.getElementById("salesAccStatePieChart"));
    function selectHandler() {
             
        var selectedItem = chart.getSelection()[0];
        //alert("selectedItem--"+data.getValue(selectedItem.row, 0));
        if (selectedItem) {
            var statusType = data.getValue(selectedItem.row, 0);
          //  alert("status"+statusType);
            // getBdmStatisticsDetailsByLoginId(activityType,bdmId);
            $('span.pagination').empty().remove();
          //  getResourcesByStatusList(statusType,subPractice,practice,country,role);
        }
    }
    google.visualization.events.addListener(chart, 'select', selectHandler);   
    chart.draw(data, options,dArray);
    
    
}	


