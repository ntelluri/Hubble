/**
 * 
 */

function newXMLHttpRequest() {
    var xmlreq = false;
    if(window.XMLHttpRequest) {
        xmlreq = new XMLHttpRequest();
    } else if(window.ActiveXObject) {
        try {
            xmlreq = new ActiveXObject("MSxm12.XMLHTTP");
        } catch(e1) {
            try {
                xmlreq = new ActiveXObject("Microsoft.XMLHTTP");
            } catch(e2) {
                xmlreq = false;
            }
        }
    }
    return xmlreq;
}


function readyStateHandlerText(req,responseTextHandler){
    return function() {
        if (req.readyState == 4) {
            if (req.status == 200) {               
                responseTextHandler(req.responseText);
            } else {
                alert("HTTP error"+req.status+" : "+req.statusText);
            }
        }
    }
}  


function readyStateHandler(req,responseXmlHandler) {
    return function() {
        if (req.readyState == 4) {
            if (req.status == 200) {
                responseXmlHandler(req.responseXML);
            } else {
                alert("HTTP error"+req.status+" : "+req.statusText);
            }
        }
    }
}

function clearTablePracticeStatusReport(tableId) {
   
    var tbl =  tableId;
    var lastRow = tbl.rows.length; 
    while (lastRow > 0) { 
        tbl.deleteRow(lastRow - 1);  
        lastRow = tbl.rows.length; 
    } 
}


function ParseAndGenerateHTML1(oTable,responseString,headerFields) {
    
	
    var start = new Date();
    var fieldDelimiter = "#^$";
    var recordDelimiter = "*@!";   
 
    //alert('rowCount%%%% ******'+rowCount);
      
    
    var records = responseString.split(recordDelimiter); 
    // alert("records---->"+records);
    generateTable1(oTable,headerFields,records,fieldDelimiter);
}



function generateTable1(oTable, headerFields,records,fieldDelimiter) {	
	  
    var tbody = oTable.childNodes[0];    
    tbody = document.createElement("TBODY");
    oTable.appendChild(tbody);
    generateTableHeader(tbody,headerFields);
    var rowlength;
    if(oTable.id == "tblutilityReport" ) {
        rowlength = records.length;
    }
    else
        rowlength = records.length-1;
    // alert("rowlength--->"+rowlength);
    if(rowlength >0 && records!=""){
        //alert("rowlength-->^"+records);
        for(var i=0;i<rowlength;i++) {
            // alert("i-->"+i);
             if(oTable.id == "tblShadowsUtilityReport"){
            	//alert("tblActiveProjectDetails");
            	generateShadowsUtilityReport(tbody,records[i],fieldDelimiter); 
            }
            
            
            
            else{
                generateRow(oTable,tbody,records[i],fieldDelimiter);  
            }
        }
       
    }    else {
        generateNoRecords(tbody,oTable);
    }
    generateFooter(tbody,oTable);
}





function generateTableHeaderAllPracticeStatusReport(tableBody,headerFields) {
    var row;
    var cell;
    row = document.createElement( "TR" );
    row.className="gridHeader";
    tableBody.appendChild( row );
    for (var i=0; i<headerFields.length; i++) {
        cell = document.createElement( "TD" );
        cell.className="gridHeader";
        row.appendChild( cell );

        cell.setAttribute("width","10000px");
        cell.innerHTML = headerFields[i];
    }
}



function generateFooterPracticeStatusReport(tbody) {
 	  
    var cell;
    var footer =document.createElement("TR");
    footer.className="gridPager";
    tbody.appendChild(footer);
    cell = document.createElement("TD");
    cell.className="gridFooter";

    // cell.colSpan = "7";
    
    cell.colSpan = "12";
    
    footer.appendChild(cell);

}


function getPracticeStatusReportData(){
   
	document.getElementById("statusReportLoad").style.display = 'block';
	$("#searchBtn").prop("disabled",true);

	 var oTable = document.getElementById("tblStatusReport");
	clearTablePracticeStatusReport(oTable);

    
    var practice=document.getElementById("practiceId").value;
    var withShadow=document.getElementById("withShadow").checked;
    var department=document.getElementById("departmentId").value;
    var orderBy=document.getElementById("orderBy").value;
  
   if(document.getElementById("withShadow").checked == true){
    	withShadow = 1;
    }else{
    	withShadow = 0;
    }
    
    
    
  
     var url = CONTENXT_PATH+"/getPracticeStatusReport.action?practiceId="+practice+"&shadowFlag="+withShadow+"&department="+department+"&orderBy="+orderBy;
   //alert(url)
    var req = newXMLHttpRequest();
    req.onreadystatechange = function() {
        if (req.readyState == 4) {
            if (req.status == 200) {      
               // alert(url)
                document.getElementById("statusReportLoad").style.display = 'none';
                displayPracticeStatusReportData(req.responseText,practice);   
                $("#searchBtn").prop("disabled",false);
            } 
        }
    };
    req.open("GET", url, true);
    req.send(null);
}
 
function displayPracticeStatusReportData(response,practice){
	 
    var tableId = document.getElementById("tblStatusReport");  
    if(practice == 'All'){
    var headerFields = new Array("Practice","Active Projects","Closed Projects","Customers","Resources","OnProject","Available","Training","OverHead","T&M","FPP","Utilization");	
    }else{
    	 var headerFields = new Array("Active Projects","Closed Projects","Customers","Resources","OnProject","Main(Billable)","Main","Shadow","Project OverHead","Project Training","Available","Training","OverHead","T&M","FPP","Utilization");	
    }
    
    var dataArray = response;
    
  // alert(dataArray)
    
    
    PracticeStatusReportDataParseAndGenerateHTML(tableId,dataArray, headerFields,practice);
}
function PracticeStatusReportDataParseAndGenerateHTML(oTable,responseString,headerFields,practice) {
    var fieldDelimiter = "#^$";
    var recordDelimiter = "*@!";   
    var records = responseString.split(recordDelimiter); 
    generatePracticeStatusReportDataRows(oTable,headerFields,records,fieldDelimiter,practice);
}

function generatePracticeStatusReportDataRows(oTable, headerFields,records,fieldDelimiter,practice) {	

    var tbody = oTable.childNodes[0];    
    tbody = document.createElement("TBODY");
    oTable.appendChild(tbody);
    if(practice == 'All'){
  generateTableHeaderAllPracticeStatusReport(tbody,headerFields);
    }/*else{
    	   generateTableHeaderPracticeStatusReport(tbody,headerFields);
    }*/
    var rowlength;
    rowlength = records.length;
   if(records == 'NoRecords'){
	   generateNoRecords(tbody,oTable); 
   }
   else if(rowlength >0 && records!=""){
	   
	         
        	  if(practice == 'All'){
        		  for(var i=0;i<rowlength-1;i++) {
        		  generateAllPracticeStatusReportDetails(oTable,tbody,records[i],fieldDelimiter,practice);
        		  }
        	  }else{ 
        		 
        		  generatePracticeStatusReportDetails(oTable,tbody,records,fieldDelimiter,practice,headerFields,i);
        	  }
     
        
    }else{
    	 generateNoRecords(tbody,oTable); 
    }
   if(practice == 'All'){
	   generateFooterPracticeStatusReport(tbody,oTable);
	   }
   
   // pagerOption();
}


function generatePracticeStatusReportDetails(oTable,tableBody,record,delimiter,practice,headers,index){
    var row;
    var cell;
    var fieldLength;
  
    var length = headers.length;
    
  

    for (var j=0;j<length;j++) { 
    	
        row = document.createElement( "TR" );
        row.className="gridRowEven";
        tableBody.appendChild( row );
        
        
    for (var i=0;i<record.length;i++) {  
    	
    	
    	if(i==0){
    		cell = document.createElement( "td" );
            cell.className="gridRowEven";       
       
            cell.setAttribute("align","center");
            cell.setAttribute("width","50%");
            cell.innerHTML = headers[j];
            row.appendChild( cell );
    	}
    	else{
    	 cell = document.createElement( "td" );
         cell.className="gridRowEven";       
         cell.setAttribute("width","50%");
         cell.setAttribute("align","center");
     	
         
         if(j==0){
             cell.innerHTML = record[i-1].split("#^$")[0]; 
 }
 else if(j==1){
     cell.innerHTML = record[i-1].split("#^$")[1]; 
}
 else if(j==2){
     cell.innerHTML = record[i-1].split("#^$")[2]; 
}
 else if(j==3){
	 //cell.innerHTML = "<a href='javascript:displayResources(\""+record[i-1].split("#^$")[11]+"\")'>"+record[i-1].split("#^$")[3]+"</a>";
	 cell.innerHTML = "<a href='javascript:displayResources(\""+record[i-1].split("#^$")[11]+"\",\"All\")'>"+record[i-1].split("#^$")[3]+"</a>";
	 
   //  cell.innerHTML = record[i-1].split("#^$")[3]; 
}
 else if(j==4){
     //cell.innerHTML = record[i-1].split("#^$")[4];
     cell.innerHTML = "<a href='javascript:displayOnProjectResourceDetails(\""+record[i-1].split("#^$")[12]+"\")'>"+record[i-1].split("#^$")[4]+"</a>";

}
     if(j==5){
    	var rec =  record[i-1].split("#^$")[12];
    	
    	 cell.innerHTML =rec.split("##")[0]; 
    }
    else if(j==6){
    	var rec =  record[i-1].split("#^$")[12];
   	 cell.innerHTML =rec.split("##")[1]; 
    }
    else if(j==7){
    	var rec =  record[i-1].split("#^$")[12];
   	 cell.innerHTML =rec.split("##")[2]; 
    }
    else if(j==8){
    	var rec =  record[i-1].split("#^$")[12];
   	 cell.innerHTML =rec.split("##")[3]; 
    }
    else if(j==9){
    	var rec =  record[i-1].split("#^$")[12];
    	var str = rec.split("##")[4];
    	
   	 cell.innerHTML =str.substring(0, str.length - 2);; 
    }
    else if(j==10){
    	 cell.innerHTML = "<a href='javascript:displayResources(\""+record[i-1].split("#^$")[11]+"\",\"Available\")'>"+record[i-1].split("#^$")[5]+"</a>";
         //       cell.innerHTML = record[i-1].split("#^$")[5]; 
    }
    else if(j==11){
    	// cell.setAttribute('bgcolor','#3e93d4');
    	 cell.innerHTML = "<a href='javascript:displayResources(\""+record[i-1].split("#^$")[11]+"\",\"Training\")'>"+record[i-1].split("#^$")[6]+"</a>";
      //  cell.innerHTML = record[i-1].split("#^$")[6]; 
}
    else if(j==12){
    	cell.innerHTML = "<a href='javascript:displayResources(\""+record[i-1].split("#^$")[11]+"\",\"OverHead\")'>"+record[i-1].split("#^$")[7]+"</a>";
      //  cell.innerHTML = record[i-1].split("#^$")[7]; 
}
    else if(j==13){
        cell.innerHTML = record[i-1].split("#^$")[8]; 
}else if(j==14){
        cell.innerHTML = +record[i-1].split("#^$")[9]; 
}else if(j==15){

        cell.innerHTML = record[i-1].split("#^$")[10]; 
}

                
                
     
            
            row.appendChild( cell );
    	}
    }   
    }
}


function generateAllPracticeStatusReportDetails(oTable,tableBody,record,delimiter,practice){
    var row;
    var cell;
    var fieldLength;
   
    var fields = record.split("#^$");
    fieldLength = fields.length ;
    var length = fieldLength;
    
    row = document.createElement( "TR" );
   
    row.className="gridRowEven";
    tableBody.appendChild( row );
   
    for (var i=0;i<(length-2);i++) {  
    	
    	 cell = document.createElement( "TD" );
         cell.className="gridColumn";       
    
         cell.setAttribute("align","center");
         
        
         
        	if(i==5){
        	
        		 cell.innerHTML = "<a href='javascript:displayOnProjectResourceDetails(\""+fields[length-1]+"\")'>"+fields[i]+"</a>";
        	      
        	}
        	else if(i==4){
        		 cell.innerHTML = "<a href='javascript:displayResources(\""+fields[length-2]+"\",\"All\")'>"+fields[i]+"</a>";
             		
        		
        	}
        	else if(i==6){
       		 cell.innerHTML = "<a href='javascript:displayResources(\""+fields[length-2]+"\",\"Available\")'>"+fields[i]+"</a>";
            		
       		
       	}
        	else if(i==7){
       		 cell.innerHTML = "<a href='javascript:displayResources(\""+fields[length-2]+"\",\"Training\")'>"+fields[i]+"</a>";
            		
       		
       	}
        	else if(i==8){
          		 cell.innerHTML = "<a href='javascript:displayResources(\""+fields[length-2]+"\",\"OverHead\")'>"+fields[i]+"</a>";
               		
          		
          	}	
        	else{
               
                cell.innerHTML = fields[i]; 
        	}
        	
          
            
            row.appendChild( cell );
       
    }   
 
}








function toggleCloseOverlay() {
    var overlay = document.getElementById('overlayResourceType');
    var specialBox = document.getElementById('specialBoxResourceType');

    overlay.style.opacity = .8;
    if(overlay.style.display == "block"){
        overlay.style.display = "none";
        specialBox.style.display = "none";
    }
    else {
        overlay.style.display = "block";
        specialBox.style.display = "block";
    }
//window.location="empSearchAll.action";
}


function displayOnProjectResourceDetails(response){ {
	
    var tableId = document.getElementById("tblResourceTypeDetails");
    clearTable(tableId);
    var headerFields = new Array("Main(Billable)","Main","Shadow","OverHead","Training");
   
    
    var dataArray = response;
    
    
    ParseAndGenerateHTML(tableId,dataArray, headerFields);
 
 
    document.getElementById("headerLabel1").style.color="white";
    document.getElementById("headerLabel1").innerHTML="OnProject Resource Details";
            
    var overlay = document.getElementById('overlayResourceType');
    var specialBox = document.getElementById('specialBoxResourceType');
    overlay.style.opacity = .8;
    if(overlay.style.display == "block"){
        overlay.style.display = "none";
        specialBox.style.display = "none";
    }
    else {
        overlay.style.display = "block";
        specialBox.style.display = "block";
    }

    
}


function ParseAndGenerateHTML(oTable,responseString,headerFields) {
    
    var start = new Date();
    var fieldDelimiter = "##";
    var recordDelimiter = "@@";   
  
    var records = responseString.split(recordDelimiter); 
    generateTable(oTable,headerFields,records,fieldDelimiter);
}


function generateTable(oTable, headerFields,records,fieldDelimiter) {
	
    var tbody = oTable.childNodes[0];    
    tbody = document.createElement("TBODY");
    oTable.appendChild(tbody);
    generateTableHeader(tbody,headerFields);
    var rowlength;
 
    rowlength = records.length-1;
    
  //  alert("rowlength--->"+rowlength)
    
    if(rowlength >=1 && records!=""){
        for(var i=0;i<rowlength;i++) {
        	
                generateRow(oTable,tbody,records[i],fieldDelimiter);  
             
        
        }    
    }    
    else {
        generateNoRecords(tbody,oTable);
    }
    
    generateFooter(tbody,oTable);
}







}



function generateRow(oTable,tableBody,record,delimiter) {
    //alert("In generateRow");
    var row;
    var cell;
    var fieldLength;
    var fields = record.split(delimiter);
    fieldLength = fields.length ;
    var length;
    
    length = fieldLength;
    
  
    row = document.createElement( "TR" );
    row.className="gridRowEven";
    tableBody.appendChild( row );
   
   
        for (var i=0;i<length;i++) {
     
            cell = document.createElement( "TD" );
            cell.className="gridColumn";
            cell.innerHTML = fields[i];
            if(fields[i]!=''){
                row.appendChild( cell );
            }
       
        }   
   
	
	 
}

function generateTableHeader(tableBody,headerFields) {
    var row;
    var cell;
    row = document.createElement( "TR" );
    row.className="gridHeader";
    tableBody.appendChild( row );
    for (var i=0; i<headerFields.length; i++) {
        cell = document.createElement( "TD" );
        cell.className="gridHeader";
        row.appendChild( cell );

        cell.setAttribute("width","10000px");
        cell.innerHTML = headerFields[i];
    }
}


function generateFooter(tbody,oTable) {
  
    var cell;
    var footer =document.createElement("TR");
    footer.className="gridPager";
    tbody.appendChild(footer);
    cell = document.createElement("TD");
    cell.className="gridFooter";

    if(oTable.id == "tblShadowsUtilityReport"){
    	cell.colSpan = "6";  
    }else{
    cell.colSpan = "11";
    
    }
   

    footer.appendChild(cell);
}

function generateNoRecords(tbody,oTable) {
	   
    var noRecords =document.createElement("TR");
    noRecords.className="gridRowEven";
    tbody.appendChild(noRecords);
    cell = document.createElement("TD");
    cell.className="gridColumn";
    
    if(oTable.id == "tblShadowsUtilityReport"){
    	cell.colSpan = "6";  
    }else{
        cell.colSpan = "12";  
    }
 
    cell.innerHTML = "No Records Found for this Search";
    noRecords.appendChild(cell);
}

function clearTable(tableId) {
    var tbl =  tableId;
    var lastRow = tbl.rows.length; 
    while (lastRow > 0) { 
        tbl.deleteRow(lastRow - 1);  
        lastRow = tbl.rows.length; 
    } 
}

function getOrderBy(practiceId){
	
	if(practiceId == "All"){
		
		 document.getElementById("orderTd").style.display='block';
		 document.getElementById("orderBy").style.display='block';
	}
	else{
		document.getElementById("orderTd").style.display='none';
		 document.getElementById("orderBy").style.display='none';
	}
}




/* shadows utilization report start */


function getShadowsUtilizationReport(){
	 var accId = document.getElementById("customerNameId").value; 
	 var oTable = document.getElementById("tblShadowsUtilityReport");
	 clearTable(oTable);
	document.getElementById('shadowUtilityReport').style.display='block';
	
   var req = newXMLHttpRequest();
  req.onreadystatechange = function() {
      if (req.readyState == 4) {
          if (req.status == 200) {      
           
              displayShadowsUtilizationReport(req.responseText);                        
          } 
      }else {
       
     
      }
  }; 
  var url = CONTENXT_PATH+"/getShadowsUtilizationReport.action?customerId="+accId;
  
  req.open("GET",url,"true");    
  req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
  req.send(null);
}

function displayShadowsUtilizationReport(response){
	
	document.getElementById('shadowUtilityReport').style.display='none';
	  var oTable = document.getElementById("tblShadowsUtilityReport");
	  clearTable(oTable);
	
	  //  var state=document.getElementById("state").value;
	    var dataArray = response;
	    
	    if(dataArray == "no data")
	    {
	        alert("No Records Found for this Search");   
	    }
	    else {
	        var headerFields = new Array("Customer&nbsp;Name","Main(Billable)","Main","Shadow","GrandTotal","ShadowPerc(%)");
	        if(response!=''){
	        	
	        	  ParseAndGenerateHTML1(oTable,response, headerFields);
	         }else{
	            alert('No Result For This Search...');
	         }
	    }
}




function generateShadowsUtilityReport(tableBody,record,fieldDelimiter){
// alert("heeeeeeeeee");
    var row;
    var cell;
    var fieldLength;
    var fields = record.split(fieldDelimiter);
    fieldLength = fields.length ;
    var length = fieldLength;
    row = document.createElement( "TR" );
    row.className="gridRowEven";
    tableBody.appendChild( row );
    for (var i=0;i<length;i++) {            
        cell = document.createElement( "TD" );
        cell.className="gridColumn";       
        cell.innerHTML = fields[i];  
       
        if(fields[i]!=''){
            if(i==1)
            {
                cell.setAttribute("align","left");
            }
            else
            {
                cell.setAttribute("align","left");     
            }
            row.appendChild( cell );
        }
    }
   
}




	function getShadowsUtilizationReportByProject(){
		 var accId = document.getElementById("customerNameId").value; 
		 var projectId = document.getElementById("projectNameId").value; 
		 var status = document.getElementById("statusId").value; 
		 var projectType = document.getElementById("ProjectTypeId").value; 
		 var costModel = document.getElementById("costModel").value; 
		 
		 var oTable = document.getElementById("tblShadowsUtilityReport");
		 clearTable(oTable);
		document.getElementById('shadowUtilityReport').style.display='block';
	    var req = newXMLHttpRequest();
	   req.onreadystatechange = function() {
	       if (req.readyState == 4) {
	           if (req.status == 200) {      
	            
	               displayShadowsUtilizationReportByProject(req.responseText);                        
	           } 
	       }else {
	        
	      
	       }
	   }; 
	   var url = CONTENXT_PATH+"/getShadowsUtilizationReportByProject.action?customerId="+accId+"&projectId="+projectId+"&status="+status+"&projectType="+projectType+"&costModel="+escape(costModel);
	   
	   req.open("GET",url,"true");    
	   req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
	   req.send(null);
	}
	
	function displayShadowsUtilizationReportByProject(response){
		document.getElementById('shadowUtilityReport').style.display='none';
		  var oTable = document.getElementById("tblShadowsUtilityReport");
		  clearTable(oTable);
		  //  var state=document.getElementById("state").value;
		    var dataArray = response;
		    
		    if(dataArray == "no data")
		    {
		        alert("No Records Found for this Search");   
		    }
		    else {
		        var headerFields = new Array("Customer&nbsp;Name","Project&nbsp;Name","Main(Billable)","Main","Shadow","GrandTotal");
		        if(response!=''){
		        	ParseAndGenerateHTML1(oTable,response, headerFields);
		         }else{
		            alert('No Result For This Search...');
		         }
		    }
	}
	
	
	

	 function generateShadowsUtilityReport(tableBody,record,fieldDelimiter){
	// alert("heeeeeeeeee");
	     var row;
	     var cell;
	     var fieldLength;
	     var fields = record.split(fieldDelimiter);
	     fieldLength = fields.length ;
	     var length = fieldLength;
	     row = document.createElement( "TR" );
	     row.className="gridRowEven";
	     tableBody.appendChild( row );
	     for (var i=0;i<length;i++) {            
	         cell = document.createElement( "TD" );
	         cell.className="gridColumn";       
	         cell.innerHTML = fields[i];  
	        
	         if(fields[i]!=''){
	             if(i==1)
	             {
	                 cell.setAttribute("align","left");
	             }
	             else
	             {
	                 cell.setAttribute("align","left");     
	             }
	             row.appendChild( cell );
	         }
	     }
	    
	 }
function getShadowsUtilizationReportBasedOn(){
	 var reportBasedOn = document.getElementById("reportBasedOn").value; 
	 
	 if(reportBasedOn=='-1'){
		 alert("Please select ReportTpe");
		 return false;
	 }else if(reportBasedOn=='Customer'){
		 getShadowsUtilizationReport();
	 }else{
		 getShadowsUtilizationReportByProject();
	 }
	 
}


function getProjectsByAccountIdProjectOverview(){
	    var accountId = document.getElementById("customerNameId").value;
	     if(accountId!=""){
	         var req = newXMLHttpRequest();
	         req.onreadystatechange = readyStateHandler(req, populateProjectsListProjectOverview);
	         var url = CONTENXT_PATH+"/getProjectsByAccountId.action?accountId="+accountId;
	         req.open("GET",url,"true");
	         req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
	         req.send(null);   
	     }else{
	         var projects = document.getElementById("projectName");
	         projects.innerHTML=" ";
	       
	         var opt = document.createElement("option");
	         opt.setAttribute("value","");
	         opt.appendChild(document.createTextNode("All"));
	         projects.appendChild(opt);
	     }
	 }
	 function populateProjectsListProjectOverview(resXML) {
	   
	     //var projects = document.getElementById("projectName");
	     var projects = document.getElementById("projectNameId");
	     //  alert("test")
	     
	     var projectsList = resXML.getElementsByTagName("PROJECTS")[0];
	    
	     var users = projectsList.getElementsByTagName("USER");
	     projects.innerHTML=" ";
	     for(var i=0;i<users.length;i++) {
	         var userName = users[i];
	         var att = userName.getAttribute("projectId");
	         var name = userName.firstChild.nodeValue;
	         var opt = document.createElement("option");
	         opt.setAttribute("value",att);
	         opt.appendChild(document.createTextNode(name));
	         projects.appendChild(opt);
	     }
	 }
	 
function showProjectFields(){
var reportBasedOn = document.getElementById("reportBasedOn").value; 
if(reportBasedOn=='Customer'){
	document.getElementById('projectNameTr').style.display='none';
	document.getElementById('projectStatusTr').style.display='none';
}else if(reportBasedOn=='Project') {
	document.getElementById('projectNameTr').style.display='block';
	document.getElementById('projectStatusTr').style.display='block';
}	 
	 }
	 


/*shadows utilization report end */


/* Resouce table...*/
function displayResources(response,state){ 
	
	   
    var tableId = document.getElementById("totalResourcesTable");
    clearTable(tableId);
   
 
 
    
    document.getElementById("headerLabel2").style.color="white";
    document.getElementById("headerLabel2").innerHTML="Total Resource";
    
    var overlay = document.getElementById('overlayTotalResources');
    var specialBox = document.getElementById('specialBoxTotalResources');
    overlay.style.opacity = .8;
    if(overlay.style.display == "block"){
        overlay.style.display = "none";
        specialBox.style.display = "none";
    }
    else {
        overlay.style.display = "block";
        specialBox.style.display = "block";
        document.getElementById('totalResourcesresultMessage').style.display = "block"; 
        var practice=response.split('||')[1];
       
        var department=response.split('||')[0];
      
          
         var url = CONTENXT_PATH+"/getResourceTotal.action?practiceId="+practice+"&department="+department+"&state="+state;
     //  alert(url)
        var req = newXMLHttpRequest();
        req.onreadystatechange = function() {
            if (req.readyState == 4) {
                if (req.status == 200) {      
                   
                	document.getElementById('totalResourcesresultMessage').style.display = "none"; 
                    displayTotalResourcesData(req.responseText,practice);   
                 
                } 
            }
        };
        req.open("GET", url, true);
        req.send(null);
    }


}

function toggleCloseOverlay1() {
    var overlay = document.getElementById('overlayTotalResources');
    var specialBox = document.getElementById('specialBoxTotalResources');

    overlay.style.opacity = .8;
    if(overlay.style.display == "block"){
        overlay.style.display = "none";
        specialBox.style.display = "none";
    }
    else {
        overlay.style.display = "block";
        specialBox.style.display = "block";
    }

}


function displayTotalResourcesData(response){
	
	  var oTable = document.getElementById("totalResourcesTable");
	 clearTable(oTable);

	    var dataArray = response;
	    
	    if(dataArray == "no data")
	    {
	        alert("No Records Found for this Search"); 
	        
	        var tbody = oTable.childNodes[0];    
    	    tbody = document.createElement("TBODY");
    	    oTable.appendChild(tbody);
    	    generateNoRecordsForResource(tbody,oTable);
	    }
	    else {

	       // var headerFields = new Array("SNo","Employee&nbsp;Name","Customer&nbsp;Name","Project&nbsp;Name","Email","Mobile&nbsp;Number","Utilization");
	    	var  headerFields = new Array("SNo","Employee&nbsp;Name","Customer&nbsp;Name","Project&nbsp;Name","EmpState","Email","Mobile&nbsp;Number","Utilization");

	    	if(response!=''){
	        	ParseAndGenerateHTML1ForResource(oTable,response, headerFields);
	         }else{
	            alert('No Result For This Search...');
	            var tbody = oTable.childNodes[0];    
	    	    tbody = document.createElement("TBODY");
	    	    oTable.appendChild(tbody);
	    	    generateNoRecordsForResource(tbody,oTable);
	    	
	         }
	    }
}


function ParseAndGenerateHTML1ForResource(oTable,responseString,headerFields) {
    
	
    var start = new Date();
    var fieldDelimiter = "#^$";
    var recordDelimiter = "*@!";   
 
    //alert('rowCount%%%% ******'+rowCount);
      
    
    var records = responseString.split(recordDelimiter); 
    // alert("records---->"+records);
    generateTableForResource(oTable,headerFields,records,fieldDelimiter);
}



function generateTableForResource(oTable, headerFields,records,fieldDelimiter) {	
	  
    var tbody = oTable.childNodes[0];    
    tbody = document.createElement("TBODY");
    oTable.appendChild(tbody);
    generateTableHeader(tbody,headerFields);
    var rowlength;
   
        rowlength = records.length-1;
    // alert("rowlength--->"+rowlength);
    if(rowlength >0 && records!=""){
        //alert("rowlength-->^"+records);
        for(var i=0;i<rowlength;i++) {
          
        	//generateShadowsUtilityForResourceReport(i+1,oTable,tbody,records[i],fieldDelimiter); 
        	
        	generateForResourceReport(i+1,oTable,tbody,records[i],fieldDelimiter); 
          
        }
       
    }    else {
    	generateNoRecordsForResource(tbody,oTable);
    }
    generateFooterForResource(tbody,oTable);
}

function generateForResourceReport(index,oTable,tableBody,record,delimiter) {
    //alert("In generateRow");
    var row;
    var cell;
    var fieldLength;
    var fields = record.split("#^$");
  /// alert("records"+record); 
 // var fields=record.split(delimiter);
    fieldLength = fields.length ;
    var length;
    
    length = fieldLength; //2
    
    var name= fields[0];
    var data= fields[1];
   // alert("data"+data);
    
    var data1=data.split('@@');
  //  alert("data1"+data1);
    
// var data2=data1[0].split('##');
  
    row = document.createElement( "TR" );
    row.className="gridRowEven";
    tableBody.appendChild( row );
   
    cell = document.createElement( "TD");
    cell.className="gridRowEven";
    cell.innerHTML = index;
    cell.rowSpan=data1.length-1;
        row.appendChild( cell );
   
        cell = document.createElement( "TD" );
        cell.className="gridRowEven";
        cell.innerHTML = name;
        cell.rowSpan=data1.length-1;
        
            row.appendChild( cell );  
           
        for(var i=0; i<data1.length-1;i++){
        	
        	  var data2=data1[i].split('##');
        	   
        	   if(i!=0){
        		   row = document.createElement( "TR" );
           	    row.className="gridRowEven";
        	   }
        	   
        	   tableBody.appendChild( row );
        	
    		for(var j=0;j<(data2.length);j++)
    		{
    		cell=document.createElement("TD");
    		cell.className="gridColumn";
    		cell.innerHTML=data2[j];
    		row.appendChild(cell);	
    		}
    		
    	
    }

    
	 
}


/*
function generateShadowsUtilityForResourceReport(index,oTable,tableBody,record,delimiter) {
    //alert("In generateRow");
    var row;
    var cell;
    var fieldLength;
    var fields = record.split(delimiter);
    fieldLength = fields.length ;
    var length;
    
    length = fieldLength;
    
  
    row = document.createElement( "TR" );
    row.className="gridRowEven";
    tableBody.appendChild( row );
   
    cell = document.createElement( "TD" );
    cell.className="gridColumn";
    cell.innerHTML = index;
    
        row.appendChild( cell );
   
        for (var i=0;i<length;i++) {
     
            cell = document.createElement( "TD" );
            cell.className="gridColumn";
            cell.innerHTML = fields[i];
            
                row.appendChild( cell );
            
       
        }   
   
	
	 
}
*/

function generateFooterForResource(tbody,oTable) {
	  
    var cell;
    var footer =document.createElement("TR");
    footer.className="gridPager";
    tbody.appendChild(footer);
    cell = document.createElement("TD");
    cell.className="gridFooter";

    
    cell.colSpan = "8";
    
    
   

    footer.appendChild(cell);
}

function generateNoRecordsForResource(tbody,oTable) {
	   
    var noRecords =document.createElement("TR");
    noRecords.className="gridRowEven";
    tbody.appendChild(noRecords);
    cell = document.createElement("TD");
    cell.className="gridColumn";
    
    
        cell.colSpan = "7";  
  
 
    cell.innerHTML = "No Records Found for this Search";
    noRecords.appendChild(cell);
}




function getOffshoreStrengthDetails(){
	var year=document.getElementById('year').value;
	var month=document.getElementById('month').value;
	
	
	if(year==''){
		alert("Please enter year");
		return false;
	}
	
	if(month==''){
		alert("Please select month");
		return false;
	}
	
	clearTable(document.getElementById("tblOffshoreStrength"));
		  document.getElementById('loadingMessage').style.display='block';
	    var req = newXMLHttpRequest();
	    req.onreadystatechange = readyStateHandlerText(req, getOffshoreStregnthResponse); 

	    //var url = CONTENXT_PATH+"/searchpmoActivityAjaxList.action?customerName="+NAME+"&projectName="+ProjectName+"&status="+status+"&projectStartDate="+ProjectStartDate;
	    var url = CONTENXT_PATH+"/getOffshoreStrengthDetails.action?year="+year+"&month="+month;

	    req.open("GET",url,"true");    
	    req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
	    req.send(null);
}


function getOffshoreStregnthResponse(response){

 document.getElementById('loadingMessage').style.display='none';
 var tableId = document.getElementById("tblOffshoreStrength");
    // var headerFields = new Array("SNo","Project&nbsp;Name","StartDate","EndDate","EmpStatus","Utilization","Resource&nbsp;Type");
    var headerFields = new Array("SNo","Department","Count&nbsp;as&nbsp;of&nbsp;Today","Roll&nbsp;In&nbsp;</br>Current&nbsp;Month","Roll&nbsp;Off&nbsp;</br>Current&nbsp;Month","Roll&nbsp;In&nbsp;</br>Previous&nbsp;Month","Roll&nbsp;Off&nbsp;</br>Previous&nbsp;Month");
   
    ParseAndGenerateHTML1(tableId,response, headerFields);
}


function getCustomerProjectRollInRollOffDetails(){
var startDate=document.getElementById('startDate').value;
var endDate=document.getElementById('endDate').value;
var type=document.getElementById('type').value;
if(startDate==''){
	alert("Please select start date");
	return false;
}

if(endDate==''){
	alert("Please select end date");
	return false;
}
clearTable(document.getElementById("tblResourcesRollInRollOff"));
	  document.getElementById('loadingMessage1').style.display='block';
    var req = newXMLHttpRequest();
    req.onreadystatechange = readyStateHandlerText(req, getProjectRollInRollOffResponse); 

    //var url = CONTENXT_PATH+"/searchpmoActivityAjaxList.action?customerName="+NAME+"&projectName="+ProjectName+"&status="+status+"&projectStartDate="+ProjectStartDate;
    var url = CONTENXT_PATH+"/getCustomerProjectRollInRollOffDetails.action?startDate="+startDate+"&endDate="+endDate+"&type="+type;

    req.open("GET",url,"true");    
    req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    req.send(null);
}

function getProjectRollInRollOffResponse(response){
  document.getElementById('loadingMessage1').style.display='none';
  var tableId = document.getElementById("tblResourcesRollInRollOff");
    // var headerFields = new Array("SNo","Project&nbsp;Name","StartDate","EndDate","EmpStatus","Utilization","Resource&nbsp;Type");
    var headerFields = new Array("SNo","CustomerName","ProjectName","StartDate","Practice","ProjectType","Sector","CostModel","ResourcesCount","Roll-In/Roll-Off");
   
    ParseAndGenerateHTML1(tableId,response, headerFields);
}



function getSubPracticeList(){
    
    var practiceName = document.getElementById("offshorepracticeId").value;

  
    
    var req = newXMLHttpRequest();
    req.onreadystatechange = readyStateHandler(req, populateSubPractices);
    var url = CONTENXT_PATH+"/getEmpPractice.action?practiceName="+practiceName;
    req.open("GET",url,"true");    
    req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    req.send(null);
    
}



function populateSubPractices(resXML) {
    // alert(resXML);
    var subPractice = document.getElementById("subPractice");
    
    var practice = resXML.getElementsByTagName("PRACTICE")[0];
    
    var subPractices = practice.getElementsByTagName("SUBPRACTICE");
    subPractice.innerHTML=" ";
    
    for(var i=0;i<subPractices.length;i++) {
        var subPracticeName = subPractices[i];
        
        var name = subPracticeName.firstChild.nodeValue;
        var opt = document.createElement("option");
        if(i==0){
            opt.setAttribute("value","All");
        }else{
            opt.setAttribute("value",name);
        }
        opt.appendChild(document.createTextNode(name));
        subPractice.appendChild(opt);
    }
}




function OffshoreDeliveryStrengthDetails(){
	
	 var practiceId = document.getElementById("offshorepracticeId").value; 
	 var subPractice = document.getElementById("subPractice").value; 
	 
	 var oTable = document.getElementById("OffshoreDeliveryStrengthReport");
	 clearTable(oTable);
	document.getElementById('OffshoreDeliveryStrengthLoad').style.display='block';
	
	document.getElementById("OffshoreDeliveryStrengthBTN").disabled = true;
  var req = newXMLHttpRequest();
 req.onreadystatechange = function() {
     if (req.readyState == 4) {
         if (req.status == 200) {      
       	  document.getElementById("OffshoreDeliveryStrengthBTN").disabled = false;
       	  OffshoreDeliveryStrengthReportDetails(req.responseText,practiceId,subPractice);                        
         } 
     }else {
   	  document.getElementById("OffshoreDeliveryStrengthBTN").disabled = false;
   	//	document.getElementById('OffshoreDeliveryStrengthLoad').style.display='none';
     }
 }; 
 var url = CONTENXT_PATH+"/offshoreDeliveryStrength.action?practiceId="+practiceId+"&subPractice="+subPractice;
 
 req.open("GET",url,"true");    
 req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
 req.send(null);
}


//displayShadowsUtilizationReportByProject
function OffshoreDeliveryStrengthReportDetails(response,practiceId,subPractice){
	document.getElementById('OffshoreDeliveryStrengthLoad').style.display='none';
	 var oTable = document.getElementById("OffshoreDeliveryStrengthReport");
	 clearTable(oTable);

	    var dataArray = response;
	   
	    if(dataArray == "")
	    {
	        alert("No Records Found for this Search");   
	    }
	    else {
	    	if(practiceId == '-1' && subPractice == 'All'){
	        var headerFields = new Array("Practice","Main(Billable)","Shadow","Main","ProjectTraining","ProjectOverHead","Available",
	        		"Training","OverHead","GrandTotal");}
	    	else if(practiceId != '-1' && subPractice == 'All'){
	    		 var headerFields = new Array("SubPractice","Main(Billable)","Shadow","Main","ProjectTraining","ProjectOverHead","Available",
	 	        		"Training","OverHead","GrandTotal");
	    	}else{
	    		var headerFields = new Array("SubPractice","Main(Billable)","Shadow","Main","ProjectTraining","ProjectOverHead","Available",
	 	        		"Training","OverHead","GrandTotal");
	    	}
	        if(response!=''){
	        	offshoreDeliveryStrengthGenerateHTML(oTable,response, headerFields);
	         }else{
	            alert('No Result For This Search...');
	         }
	    }
}





function offshoreDeliveryStrengthGenerateHTML(oTable,responseString,headerFields) {
   

   var fieldDelimiter = "#^$";
   var recordDelimiter = "*@!";   
    
   
   var records = responseString.split(recordDelimiter); 
  
   generateTableForOffshoreDeliveryStrength(oTable,headerFields,records,fieldDelimiter);
}



function generateTableForOffshoreDeliveryStrength(oTable, headerFields,records,fieldDelimiter) {	
	  
   var tbody = oTable.childNodes[0];    
   tbody = document.createElement("TBODY");
   oTable.appendChild(tbody);
   generateTableHeader(tbody,headerFields);
   var rowlength;
  
       rowlength = records.length-1;
   // alert("rowlength--->"+rowlength);
   if(rowlength >0 && records!=""){
       //alert("rowlength-->^"+records);
       for(var i=0;i<rowlength;i++) {
         
       	generateOffshoreDeliveryStrengthReport(i+1,oTable,tbody,records[i],fieldDelimiter); 
         
       }
      
   }    else {
   	generateNoRecordsForOffshoreDeliveryStrength(tbody,oTable,headerFields);
   }
   generateFooterForOffshoreDeliveryStrength(tbody,oTable,headerFields);
}



function generateOffshoreDeliveryStrengthReport(index,oTable,tableBody,record,delimiter) {
   //alert("In generateRow");
   var row;
   var cell;
   var fieldLength;
   var fields = record.split(delimiter);
   fieldLength = fields.length ;
   var length;
   
   length = fieldLength;
   
 
   row = document.createElement( "TR" );
   row.className="gridRowEven";
   tableBody.appendChild( row );
 
  
  
       for (var i=1;i<length;i++) {
    
           cell = document.createElement( "TD" );
           cell.className="gridColumn";
           cell.innerHTML = fields[i];
           cell.align="center";
           if (fields[i]==fields[1] ||  fields[i]==0)
    	   {
    	   cell.style="font-weight:normal;";
    	   }
           else
    	   {
    	   cell.style="font-weight:bold;";
    	   }
               row.appendChild( cell );
           
      
       }   
  
	
	 
}

function generateFooterForOffshoreDeliveryStrength(tbody,oTable,headerFields) {
	  
   var cell;
   var footer =document.createElement("TR");
   footer.className="gridPager";
   tbody.appendChild(footer);
   cell = document.createElement("TD");
   cell.className="gridFooter";

   
   cell.colSpan = headerFields.length;
   
   
  

   footer.appendChild(cell);
}

function generateNoRecordsForOffshoreDeliveryStrength(tbody,oTable,headerFields) {
	   
   var noRecords =document.createElement("TR");
   noRecords.className="gridRowEven";
   tbody.appendChild(noRecords);
   cell = document.createElement("TD");
   cell.className="gridColumn";
   
   
       cell.colSpan = headerFields.length;  
 

   cell.innerHTML = "No Records Found for this Search";
   noRecords.appendChild(cell);
}


//changes Regarding RollinRolloff overall analysis


function getRollInRollOffAnalysis(){
	
	
	var year=document.getElementById('year1').value;
	
	var country=document.getElementById('country').value;
	var reportBasedOn=document.getElementById('reportBased').value;
	
	var departmentId=document.getElementById('departmentId3').value;
	
	
	//alert("departmentId"+departmentId);
	
	
		 // document.getElementById('loadingMessage6').style.display='block';
	
		  
			document.getElementById("rollSearch").disabled = true;
	    var req = newXMLHttpRequest();
	    
	    
	    if(reportBasedOn=='Quarterly' && departmentId=='-1' ){
	    	
	    	
	    	req.onreadystatechange = function() {
	        	document.getElementById("rollSearch").disabled = true;
	        	if (req.readyState == 4) {
	        	            if (req.status == 200) { 
	        	            	document.getElementById("loadingMessage6").style.display = 'none';
	        	            	document.getElementById("ProjectRolinoff").style.display = 'block';
	        	            	document.getElementById("onboardtatusStack").style.display = 'block';
	        	            	
	        	            	
	        	            	getRollInRollOffAnalysisResponse(req.responseText); 
	        	            	document.getElementById("rollSearch").disabled = false;
	        	            }
	        	}
	        	else{
	        		document.getElementById('loadingMessage6').style.display='block';
	        		document.getElementById('ProjectRolinoff').style.display='none';
	        		document.getElementById('onboardtatusStack').style.display='none';
	        		
	        		
	        		}
	        	}
	    
    	
	   
	    var url = CONTENXT_PATH+"/getRollInRollOffAnalysisDetails.action?year="+year+"&country="+country+"&reportBasedOn="+reportBasedOn;

	    req.open("GET",url,"true");    
	    req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
	    req.send(null);
	
	    }
	    
	    
	    else  if(reportBasedOn=='Monthly' && departmentId=='-1' )
	    	{
	    	
	    	
	    	
	    	req.onreadystatechange = function() {
	        	document.getElementById("rollSearch").disabled = true;
	        	if (req.readyState == 4) {
	        	            if (req.status == 200) { 
	        	            	document.getElementById("loadingMessage6").style.display = 'none';
	        	            	document.getElementById("ProjectRolinoff").style.display = 'block';
	        	            	document.getElementById("onboardtatusStack").style.display = 'block';
	        	            	
	        	            	getRollInRollOffAnalysisMonthlyResponse(req.responseText); 
	        	            	document.getElementById("rollSearch").disabled = false;
	        	            }
	        	}
	        	else{
	        		document.getElementById('loadingMessage6').style.display='block';
	        		document.getElementById('ProjectRolinoff').style.display='none';
	        		document.getElementById('onboardtatusStack').style.display='none';
	        		
	        		}
	        	}
	    
    	
	  
	    var url = CONTENXT_PATH+"/getRollInRollOffAnalysisDetails.action?year="+year+"&country="+country+"&reportBasedOn="+reportBasedOn;

	    req.open("GET",url,"true");    
	    req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
	    req.send(null);
	    	
	    	}
	    else if(reportBasedOn=='Quarterly' && departmentId!='-1' ){
	    	

	    	
	    	
	    	req.onreadystatechange = function() {
	    		document.getElementById("rollSearch").disabled = true;
	        	if (req.readyState == 4) {
	        	            if (req.status == 200) { 
	        	            	document.getElementById("loadingMessage6").style.display = 'none';
	        	            	document.getElementById("ProjectRolinoff").style.display = 'block';
	        	            	document.getElementById("onboardtatusStack").style.display = 'block';
	        	            	
	        	            	
	        	            	getRollInRollOffAnalysisResponsePractice(req.responseText); 
	        	            	document.getElementById("rollSearch").disabled = false;
	        	            }
	        	}
	        	else{
	        		document.getElementById('loadingMessage6').style.display='block';
	        		document.getElementById('ProjectRolinoff').style.display='none';
	        		document.getElementById('onboardtatusStack').style.display='none';
	        		
	        		}
	        	}
	    
		
	  
	 var url = CONTENXT_PATH+"/getRollInRollOffAnalysisDetailsPractice.action?year="+year+"&country="+country+"&reportBasedOn="+reportBasedOn+"&departmentId="+departmentId;

	    req.open("GET",url,"true");    
	    req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
	    req.send(null);

	    
	    
	    }
	    
	    else
	    	{

	    	
	    	
	    	

	    	req.onreadystatechange = function() {
	    		document.getElementById("rollSearch").disabled = true;
	        	if (req.readyState == 4) {
	        	            if (req.status == 200) { 
	        	            	document.getElementById("loadingMessage6").style.display = 'none';
	        	            	document.getElementById("ProjectRolinoff").style.display = 'block';
	        	            	document.getElementById("onboardtatusStack").style.display = 'block';
	        	            	
	        	            	getRollInRollOffAnalysisMonthlyResponsePractice(req.responseText); 
	        	            	document.getElementById("rollSearch").disabled = false;
	        	            }
	        	}
	        	else{
	        		document.getElementById('loadingMessage6').style.display='block';
	        		document.getElementById('ProjectRolinoff').style.display='none';
	        		document.getElementById('onboardtatusStack').style.display='none';
	        		
	        		}
	        	}
	    
		  
		    var url = CONTENXT_PATH+"/getRollInRollOffAnalysisDetailsPractice.action?year="+year+"&country="+country+"&reportBasedOn="+reportBasedOn+"&departmentId="+departmentId;

		    req.open("GET",url,"true");    
		    req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
		    req.send(null);
	    	
	    	
	    	}
	    
	    
	    
	    
	    
	    
}
function getRollInRollOffAnalysisMonthlyResponse(response){
	
	
	var year=document.getElementById('year1').value;

	
	
	  var tableData=response;
	
	var cf="<div  align='center' style='margin-top: 0; margin-bottom: 0; padding-top: 0; padding-bottom: 0; width: 100%; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%;' offset='0' margin='0' data-gr-c-s-loaded='true' bgcolor='#ffffff' topmargin='0' marginheight='0' leftmargin='0'>" +
"    <table cellpadding='0' cellspacing='0' border='0' align='center' width='100%' style='border:0px !important;max-width:1230px;'>" +
"      <tbody>" +
"        <tr>" +
"          <td align='left' border='0' style='border:0px !important;'>";

			  
			  
			  
	 
	  var cb="</td></tr></tbody></table></div>";
	  

	 //onboard data 
	  
	  var dv1b="</tbody></table></div> </td></tr>";
			  
			
			  
				
			var dv1f="<table><tr><div cellpadding='0' cellspacing='0' align='center' width='100%' style='padding:5px;max-width:610px;display:inline-block;'>" +
"              <table class='wholetable' align='center' width='50%'>" +
"                <tbody>" +
"                  <tr>" +
"                    <th class='wholetable' bgcolor='#00aae7'>" +
"                      <span style='font-size: 1.5vw;color: #ffffff;'><b>"+year+"</b></span>" +
"                    </th>" +
"                    <th class='wholetable' colspan='31' bgcolor='#232527'>" +
"                      <span style='color: #ffffff;font-size: 1.5vw;'><b>Onboard Monthly</b></span>" +
"                    </th>" +
"                  </tr>" +
"                  <tr>" +
"                    <th class='total1'><b>Departments</b></th>" +

"					 <th class='total' colspan='2'><b>Jan</b></th>" +
"                    <th class='total' colspan='2'><b>Feb</b></th>" +
"                    <th class='total' colspan='2'><b>Mar</b></th>" +
"                    <th class='total' colspan='2'><b>Apr</b></th>" +
"                    <th class='total' colspan='2'><b>May</b></th>" +
"                    <th class='total' colspan='2'><b>Jun</b></th>" +
"                    <th class='total' colspan='2'><b>Jul</b></th>" +
"                    <th class='total' colspan='2'><b>Aug</b></th>" +
"                    <th class='total' colspan='2'><b>Sept</b></th>" +
"                    <th class='total' colspan='2'><b>Oct</b></th>" +
"                    <th class='total' colspan='2'><b>Nov</b></th>" +
"                    <th class='total' colspan='2'><b>Dec</b></th>" +
"                    <th class='total1' colspan='2'><b>Total</b></th>" +
"                    <th class='total1'><b>Grand Total</b></th>" +
"                  </tr>" +
"                  <tr>" +
"                    <td class='total'></td>" +
"                    <td class='total2'>M</td>" +
"                    <td class='total3'>F</td>" +
"                    <td class='total2'>M</td>" +
"                    <td class='total3'>F</td>" +
"                    <td class='total2'>M</td>" +
"                    <td class='total3'>F</td>" +
"                    <td class='total2'>M</td>" +
"                    <td class='total3'>F</td>" +
"                    <td class='total2'>M</td>" +
"                    <td class='total3'>F</td>" +
"                    <td class='total2'>M</td>" +
"                    <td class='total3'>F</td>" +
"                    <td class='total2'>M</td>" +
"                    <td class='total3'>F</td>" +
"                    <td class='total2'>M</td>" +
"                    <td class='total3'>F</td>" +
"                    <td class='total2'>M</td>" +
"                    <td class='total3'>F</td>" +
"                    <td class='total2'>M</td>" +
"                    <td class='total3'>F</td>" +
"                    <td class='total2'>M</td>" +
"                    <td class='total3'>F</td>" +
"                    <td class='total2'>M</td>" +
"                    <td class='total3'>F</td>" +
"                    <td class='total2'>M</td>" +
"                    <td class='total3'>F</td>" +
"                    <td class='total2'></td>" +
"                  </tr>";

					   
					     
					  //   alert("tableData"+tableData);
					     
					     var tblRow=tableData.split("*@!");
					    
					   
						  var tr="";
					   for(var i=0;i<tblRow.length-1;i++)
					   {   		tr=tr+"<tr>"; 
					   var tblCol=tblRow[i].split("#^$");
					   //alert("tblRow.length-1out"+i);
					   if(i==tblRow.length-2){
						   
		            	   
		            	   for(var j=0;j<tblCol.length;j++){
		            		   if(j==0)
		            			   {
		            		   tr=tr+"<th class='total1'><b>"+tblCol[j]+"</b></th>";
		            			   }
		            		   else
		            			   {
		            			   tr=tr+"<th class='total' colspan='2'>"+tblCol[j]+"</th>";
		            			   }
		            			   
		            	   }
					   }
					   
					   else{	
	               
	               
						   				for(var j=0;j<tblCol.length;j++)
						   				{
						   						if(j==0 || j==27){
						   							tr=tr+"<td class='total'>"+tblCol[j]+"</td>";
						   						}
						   						else if(j==25)
						   						{
						   							tr=tr+"<td class='total2'>"+tblCol[j]+"</td>";
						   						}
						   						else if(j==26)
						   						{
						   							tr=tr+"<td class='total3'>"+tblCol[j]+"</td>";
						   						}
						
						   						else{
						   							tr=tr+"<td class='tdfont'>"+tblCol[j]+"</td>";
						   						}
						
						   				}
						   		tr=tr+"</tr>";
					   }
					}
					   
					   

					

	//onbord end 
	
	//exit start
					   var dv2b="</tbody></table></div></td></tr></table>";
						  
						
						var dv2f="<tr><td border='0' style='border:0px !important;'><br><div cellpadding='0' cellspacing='0' align='center' width='100%' style='padding:0px;max-width:610px;display:inline-block;'>" +
"              <table class='wholetable' align='left' width='50%'>" +
"                <tbody>" +
"                  <tr>" +
"                    <th class='wholetable' bgcolor='#00aae7'>" +
"                      <span style='font-size: 1.5vw;color: #ffffff;'><b>"+year+"</b></span>" +
"                    </th>" +
"                    <th class='wholetable' colspan='31' bgcolor='#232527'>" +
"                      <span style='color: #ffffff;font-size: 1.5vw;'><b>Exit Monthly</b></span>" +
"                    </th>" +
"                  </tr>" +
"                  <tr>" +
"                    <th class='total4'><b>Departments</b></th>" +
"                    <th class='total5' colspan='2'><b>Jan</b></th>" +
"                    <th class='total5' colspan='2'><b>Feb</b></th>" +
"                    <th class='total5' colspan='2'><b>Mar</b></th>" +
"                    <th class='total5' colspan='2'><b>Apr</b></th>" +
"                    <th class='total5' colspan='2'><b>May</b></th>" +
"                    <th class='total5' colspan='2'><b>Jun</b></th>" +
"                    <th class='total5' colspan='2'><b>Jul</b></th>" +
"                    <th class='total5' colspan='2'><b>Aug</b></th>" +
"                    <th class='total5' colspan='2'><b>Sept</b></th>" +
"                    <th class='total5' colspan='2'><b>Oct</b></th>" +
"                    <th class='total5' colspan='2'><b>Nov</b></th>" +
"                    <th class='total5' colspan='2'><b>Dec</b></th>" +
"                    <th class='total4' colspan='2'><b>Total</b></th>" +
"                    <th class='total4'><b>Grand Total</b></th>" +
"                  </tr>" +

"                  <tr>" +
"                    <td class='total5'></td>" +
"                    <td class='total2'>M</td>" +
"                    <td class='total3'>F</td>" +
"                    <td class='total2'>M</td>" +
"                    <td class='total3'>F</td>" +
"                    <td class='total2'>M</td>" +
"                    <td class='total3'>F</td>" +
"                    <td class='total2'>M</td>" +
"                    <td class='total3'>F</td>" +
"                    <td class='total2'>M</td>" +
"                    <td class='total3'>F</td>" +
"                    <td class='total2'>M</td>" +
"                    <td class='total3'>F</td>" +
"                    <td class='total2'>M</td>" +
"                    <td class='total3'>F</td>" +
"                    <td class='total2'>M</td>" +
"                    <td class='total3'>F</td>" +
"                    <td class='total2'>M</td>" +
"                    <td class='total3'>F</td>" +
"                    <td class='total2'>M</td>" +
"                    <td class='total3'>F</td>" +
"                    <td class='total2'>M</td>" +
"                    <td class='total3'>F</td>" +
"                    <td class='total2'>M</td>" +
"                    <td class='total3'>F</td>" +
"                    <td class='total2'>M</td>" +
"                    <td class='total3'>F</td>" +
"                    <td class='total2'></td>" +
"                  </tr>";

								    
								     
								   //  alert("tableData"+tableData);
								     
						
					     
					   //  alert("tableData"+tableData);
					     
					     var tblRow=tableData.split("*#!");
					    //alert("tblRow"+tblRow.length);
					   
						  var div2tr="";
					   for(var i=0;i<tblRow.length-1;i++)
					   {   		div2tr=div2tr+"<tr>"; 
					   var tblCol=tblRow[i].split("$^$");
					  // alert("tblCol"+tblCol.length);
					   if(i==tblRow.length-2){
						   
		            	   
		            	   for(var j=0;j<tblCol.length;j++){
		            		   
		            		   if(j==0)
		            			   {
		            			   div2tr=div2tr+"<th class='total4'><b>"+tblCol[j]+"</b></th>";
		            			   }
		            		   else
		            			   {
		            			   div2tr=div2tr+"<th class='total5' colspan='2'>"+tblCol[j]+"</th>";
		            			   }
		            			   
		            	   }
					   }
					   else if(i==0){
						   
		            	   
		            	   for(var j=0;j<tblCol.length;j++){
		            		   
		            		   if(j==0)
		            			   {
		            			   div2tr=div2tr+"<td class='total5'>GDC</td>";
		            			   }
		            		   else if(j==27)
		            			   {
		            			   div2tr=div2tr+"<td class='total5'>"+tblCol[j]+"</td>";
		            			   }
		            		   else if(j==25)
		   						{
		   							div2tr=div2tr+"<td class='total2'>"+tblCol[j]+"</td>";
		   						}
		   						else if(j==26)
		   						{
		   							div2tr=div2tr+"<td class='total3'>"+tblCol[j]+"</td>";
		   						}
		
		   						else{
		   							div2tr=div2tr+"<td class='tdfont'>"+tblCol[j]+"</td>";
		   						}
		            			   
		            	   }
					   }
					   
					   
					   
					   
					   else{	
	               
	               
						   				for(var j=0;j<tblCol.length;j++)
						   				{
						   						
						   						
						   						if(j==0 || j==27  )
						   							{
						   						
						   						div2tr=div2tr+"<td class='total5'>"+tblCol[j]+"</td>";
						   							}
						   						else if(j==25)
						   						{
						   							div2tr=div2tr+"<td class='total2'>"+tblCol[j]+"</td>";
						   						}
						   						else if(j==26)
						   						{
						   							div2tr=div2tr+"<td class='total3'>"+tblCol[j]+"</td>";
						   						}
						
						   						else{
						   							div2tr=div2tr+"<td class='tdfont'>"+tblCol[j]+"</td>";
						   						}
						
						   				}
						   				div2tr=div2tr+"</tr>";
					   }
					}
					   

	
	
	
	
	var content=cf+dv1f+tr+dv1b+"<br>"+"</br>"+dv2f+div2tr+dv2b+cb;
	

	document.getElementById('ProjectRolinoff').innerHTML=content;  

	// sarada chngEnd
	


	
	drawChartMonthly(response,year);
	
}
	
function drawChartMonthly(response,year) {
	
	var janontotal;
	var febontotal;
	var marontotal;
	var aprontotal;
	var mayontotal;
	var juneontotal;
	var julyontotal;
	var augontotal;
	var sepontotal;
	var octontotal;
	var novontotal;
	var decontotal;
	
	
	var janexittotal;
	var febexittotal;
	var marexittotal;
	var aprexittotal;
	var mayexittotal;
	var junexittotal;
	var julyexittotal;
	var augexittotal;
	var sepexittotal;
	var octexittotal;
	var novexittotal;
	var decexittotal;
	

	
	
	var tableData=response;
	 var tblRow=tableData.split("*@!");
	

  for(var i=0;i<tblRow.length;i++){
	   
var tblCol=tblRow[i].split("#^$");

for(var j=0;j<tblCol.length;j++){
  
	if(j==tblCol.length-3){
		
		decontotal=tblCol[j];
	
	}
	if(j==tblCol.length-4){
		novontotal=tblCol[j];
		}
	
	if(j==tblCol.length-5){
		octontotal=tblCol[j];
		}
	
	if(j==tblCol.length-6){
		sepontotal=tblCol[j];
		}
	if(j==tblCol.length-7){
		
	augontotal=tblCol[j];
	
	}
	if(j==tblCol.length-8){
		julyontotal=tblCol[j];
		}
	
	if(j==tblCol.length-9){
		juneontotal=tblCol[j];
		}
	
	if(j==tblCol.length-10){
		mayontotal=tblCol[j];
		}
	if(j==tblCol.length-11){
		
		aprontotal=tblCol[j];
		
		}
		if(j==tblCol.length-12){
			marontotal=tblCol[j];
			}
		
		if(j==tblCol.length-13){
			febontotal=tblCol[j];
			}
		
		if(j==tblCol.length-14){
			janontotal=tblCol[j];
			}
  }
	
}
  
 
	
	 
  var tblRow2=tableData.split("*#!");
	

  for(var i=0;i<tblRow2.length;i++){
	   
var tblCol2=tblRow2[i].split("$^$");

for(var j=0;j<tblCol2.length;j++){
	


	if(j==tblCol2.length-3){
		
		decexittotal=tblCol2[j];
	
	}
	if(j==tblCol2.length-4){
		novexittotal=tblCol2[j];
		}
	
	if(j==tblCol2.length-5){
		octexittotal=tblCol2[j];
		}
	
	if(j==tblCol2.length-6){
		sepexittotal=tblCol2[j];
		}
	if(j==tblCol2.length-7){
		
		augexittotal=tblCol2[j];
	
	}
	if(j==tblCol2.length-8){
		julyexittotal=tblCol2[j];
		}
	
	
	
	if(j==tblCol2.length-9){
		junexittotal=tblCol2[j];
		}
	
	if(j==tblCol2.length-10){
		mayexittotal=tblCol2[j];
		}
	if(j==tblCol2.length-11){
		
		aprexittotal=tblCol2[j];
		
		}
		if(j==tblCol2.length-12){
			marexittotal=tblCol2[j];
			}
		
		if(j==tblCol2.length-13){
			febexittotal=tblCol2[j];
			}
		
		if(j==tblCol2.length-14){
			janexittotal=tblCol2[j];
			}
  }
	
  }
	


	 

	
	
var data = google.visualization.arrayToDataTable([
   

    
    
    ['Month', 'Onboard', { role: 'annotation'} , { role: 'style' },'Exit', { role: 'annotation'}, { role: 'style' }],
    ['Jan',  parseInt(janontotal),janontotal,'#00aae7', parseInt(janexittotal), janexittotal,'#f5888d'],
    ['Feb',  parseInt(febontotal),febontotal,'#00aae7',  parseInt(febexittotal), febexittotal ,'#f5888d'],
    ['Mar',  parseInt(marontotal),marontotal,'#00aae7',  parseInt(marexittotal), marexittotal ,'#f5888d'],
    ['Apr',  parseInt(aprontotal),aprontotal, '#00aae7',  parseInt(aprexittotal), aprexittotal,'#f5888d'],
 	['May',  parseInt(mayontotal),mayontotal,'#00aae7', parseInt(mayexittotal), mayexittotal,'#f5888d'],
    ['Jun',  parseInt(juneontotal),juneontotal,'#00aae7',  parseInt(junexittotal), junexittotal,'#f5888d'],
    ['Jul',  parseInt(julyontotal),julyontotal,'#00aae7',  parseInt(julyexittotal), julyexittotal,'#f5888d'],
    ['Aug',  parseInt(augontotal),augontotal, '#00aae7',  parseInt(augexittotal), augexittotal,'#f5888d'],
 	['Sep',  parseInt(sepontotal),sepontotal,'#00aae7', parseInt(sepexittotal), sepexittotal,'#f5888d'],
    ['Oct',  parseInt(octontotal),octontotal,'#00aae7',  parseInt(octexittotal), octexittotal,'#f5888d'],
    ['Nov',  parseInt(novontotal),novontotal,'#00aae7',  parseInt(novexittotal), novexittotal,'#f5888d'],
    ['Dec',  parseInt(decontotal),decontotal,  '#00aae7', parseInt(decexittotal), decexittotal,'#f5888d']
 	
        
 ]);



    
    
    var options = {title: ''+year+' - Onboard & Exit Status',
    		legend: {  position: 'bottom'  },
    colors: ['#00aae7', '#f5888d']
    };  

    // Instantiate and draw the chart.
    var chart = new google.visualization.ColumnChart(document.getElementById('onboardtatusStack'));
    chart.draw(data, options);
 }
	
	
	///monthly report



function getRollInRollOffAnalysisResponse(response){
	
	
	 document.getElementById('loadingMessage6').style.display='none';
	
	var year=document.getElementById('year1').value;
	
	  var tableData=response;
	 
	var cf="<div  align='center' style='margin-top: 0; margin-bottom: 0; padding-top: 0; padding-bottom: 0; width: 100%; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%;' offset='0' margin='0' data-gr-c-s-loaded='true' bgcolor='#ffffff' topmargin='0' marginheight='0' leftmargin='0'>" +
"    <table cellpadding='0' cellspacing='0' border='0' align='center' width='100%' style='border:0px !important;max-width:1230px;'>" +
"      <tbody>" +
"        <tr>" +
"          <td align='center' border='0' style='border:0px !important;'>";

			  
			  
			  
	 
	  var cb="</td></tr></tbody></table></div>";
	  

	 //onboard data 
	  
	  var dv1b="</tbody></table></div> </td></tr>";
			  
			
			  
				
			var dv1f="<table><tr><div cellpadding='0' cellspacing='0' align='center' width='100%' style='padding:5px;max-width:610px;display:inline-block;'>" +
"              <table class='wholetable' align='left' width='50%'>" +
"                <tbody>" +
"                  <tr>" +
"                    <th class='wholetable' bgcolor='#00aae7'>" +
"                      <span style='font-size: 1.5vw;color: #ffffff;'><b>"+year+"</b></span>" +
"                    </th>" +
"                    <th class='wholetable' colspan='11' bgcolor='#232527'>" +
"                      <span style='color: #ffffff;font-size: 1.5vw;'><b>Onboard Quarterly</b></span>" +
"                    </th>" +
"                  </tr>" +
"                  <tr>" +
"                    <th class='total1'><b>Departments</b></th>" +
"                    <th class='total' colspan='2'><b>Q1</b></th>" +
"                    <th class='total' colspan='2'><b>Q2</b></th>" +
"                    <th class='total' colspan='2'><b>Q3</b></th>" +
"                    <th class='total' colspan='2'><b>Q4</b></th>" +
"                    <th class='total1' colspan='2'><b>Total</b></th>" +
"                    <th class='total1'><b>Grand Total</b></th>" +
"                  </tr>" +
"                  <tr>" +
"                    <td class='total'></td>" +
"                    <td class='total2'>M</td>" +
"                    <td class='total3'>F</td>" +
"                    <td class='total2'>M</td>" +
"                    <td class='total3'>F</td>" +
"                    <td class='total2'>M</td>" +
"                    <td class='total3'>F</td>" +
"                    <td class='total2'>M</td>" +
"                    <td class='total3'>F</td>" +
"                    <td class='total2'>M</td>" +
"                    <td class='total3'>F</td>" +
"                    <td class='total2'></td>" +
"                  </tr>";

					   
					     
					    // alert("tableData"+tableData);
					     
					     var tblRow=tableData.split("*@!");
					    
					   
						  var tr="";
					   for(var i=0;i<tblRow.length-1;i++)
					   {   		tr=tr+"<tr>"; 
					   var tblCol=tblRow[i].split("#^$");
					   //alert("tblRow.length-1out"+i);
					   if(i==tblRow.length-2){
						   
		            	   
		            	   for(var j=0;j<tblCol.length;j++){
		            		   if(j==0)
		            			   {
		            		   tr=tr+"<th class='total1'><b>"+tblCol[j]+"</b></th>";
		            			   }
		            		   else
		            			   {
		            			   tr=tr+"<th class='total' colspan='2'>"+tblCol[j]+"</th>";
		            			   }
		            			   
		            	   }
					   }
					   
					   else{	
	               
	               
						   				for(var j=0;j<tblCol.length;j++)
						   				{
						   						if(j==0 || j==11){
						   							tr=tr+"<td class='total'>"+tblCol[j]+"</td>";
						   						}
						   						else if(j==9)
						   						{
						   							tr=tr+"<td class='total2'>"+tblCol[j]+"</td>";
						   						}
						   						else if(j==10)
						   						{
						   							tr=tr+"<td class='total3'>"+tblCol[j]+"</td>";
						   						}
						
						   						else{
						   							tr=tr+"<td class='tdfont'>"+tblCol[j]+"</td>";
						   						}
						
						   				}
						   		tr=tr+"</tr>";
					   }
					}
					   
					   

					  

	//onbord end 
	
	//exit start

					   var dv2b="</tbody></table></div></td></tr></table>";
						  
						
						var dv2f="<tr><td align='left' border='0' style='border:0px !important;'><br><div cellpadding='0' cellspacing='0' align='center' width='100%' style='padding:0px;max-width:610px;display:inline-block;'>" +
"              <table class='wholetable' align='left' width='60%'>" +
"                <tbody>" +
"                  <tr>" +
"                    <th class='wholetable' bgcolor='#00aae7'>" +
"                      <span style='font-size: 1.5vw;color: #ffffff;'><b>"+year+"</b></span>" +
"                    </th>" +
"                    <th class='wholetable' colspan='11' bgcolor='#232527'>" +
"                      <span style='color: #ffffff;font-size: 1.5vw;'><b>Exit Quarterly</b></span>" +
"                    </th>" +
"                  </tr>" +
"                  <tr>" +
"                    <th class='total4'><b>Departments</b></th>" +
"                    <th class='total5' colspan='2'><b>Q1</b></th>" +
"                    <th class='total5' colspan='2'><b>Q2</b></th>" +
"                    <th class='total5' colspan='2'><b>Q3</b></th>" +
"                    <th class='total5' colspan='2'><b>Q4</b></th>" +
"                    <th class='total4' colspan='2'><b>Total</b></th>" +
"                    <th class='total4'><b>Grand Total</b></th>" +
"                  </tr>" +
"                  <tr>" +
"                    <td class='total5'></td>" +
"                    <td class='total2'>M</td>" +
"                    <td class='total3'>F</td>" +
"                    <td class='total2'>M</td>" +
"                    <td class='total3'>F</td>" +
"                    <td class='total2'>M</td>" +
"                    <td class='total3'>F</td>" +
"                    <td class='total2'>M</td>" +
"                    <td class='total3'>F</td>" +
"                    <td class='total2'>M</td>" +
"                    <td class='total3'>F</td>" +
"                    <td class='total2'></td>" +
"                  </tr>";

								    
								     
								   //  alert("tableData"+tableData);
								     
						
					     
					   //  alert("tableData"+tableData);
					     
					     var tblRow=tableData.split("*#!");
					    //alert("tblRow"+tblRow.length);
					   
						  var div2tr="";
					   for(var i=0;i<tblRow.length-1;i++)
					   {   		div2tr=div2tr+"<tr>"; 
					   var tblCol=tblRow[i].split("$^$");
					  // alert("tblCol"+tblCol.length);
					   if(i==tblRow.length-2){
						   
		            	   
		            	   for(var j=0;j<tblCol.length;j++){
		            		   
		            		   if(j==0)
		            			   {
		            			   div2tr=div2tr+"<th class='total4'><b>"+tblCol[j]+"</b></th>";
		            			   }
		            		   else
		            			   {
		            			   div2tr=div2tr+"<th class='total5' colspan='2'>"+tblCol[j]+"</th>";
		            			   }
		            			   
		            	   }
					   }
					   else if(i==0){
						   
		            	   
		            	   for(var j=0;j<tblCol.length;j++){
		            		   
		            		   if(j==0)
		            			   {
		            			   div2tr=div2tr+"<td class='total5'>GDC</td>";
		            			   }
		            		   else if(j==11)
		            			   {
		            			   div2tr=div2tr+"<td class='total5'>"+tblCol[j]+"</td>";
		            			   }
		            		   else if(j==9)
		   						{
		   							div2tr=div2tr+"<td class='total2'>"+tblCol[j]+"</td>";
		   						}
		   						else if(j==10)
		   						{
		   							div2tr=div2tr+"<td class='total3'>"+tblCol[j]+"</td>";
		   						}
		
		   						else{
		   							div2tr=div2tr+"<td class='tdfont'>"+tblCol[j]+"</td>";
		   						}
		            			   
		            	   }
					   }
					   
					   
					   
					   
					   else{	
	               
	               
						   				for(var j=0;j<tblCol.length;j++)
						   				{
						   						
						   						
						   						if(j==0 || j==11  )
						   							{
						   						
						   						div2tr=div2tr+"<td class='total5'>"+tblCol[j]+"</td>";
						   							}
						   						else if(j==9)
						   						{
						   							div2tr=div2tr+"<td class='total2'>"+tblCol[j]+"</td>";
						   						}
						   						else if(j==10)
						   						{
						   							div2tr=div2tr+"<td class='total3'>"+tblCol[j]+"</td>";
						   						}
						
						   						else{
						   							div2tr=div2tr+"<td class='tdfont'>"+tblCol[j]+"</td>";
						   						}
						
						   				}
						   				div2tr=div2tr+"</tr>";
					   }
					}
					   

	
	
	
	
	var content=cf+dv1f+tr+dv1b+dv2f+div2tr+dv2b+cb;
	

	document.getElementById('ProjectRolinoff').innerHTML=content;  

	// sarada chngEnd
	


	
	drawChart(response,year);
	
}
	
function drawChart(response,year) {
	
	var q1ontotal;
	var q2ontotal;
	
	var q3ontotal;
	var q4ontotal;
	//exit dec
	var q1exittotal;
	var q2exittotal;
	
	var q3exittotal;
	var q4exittotal;
	var tableData=response;
	 var tblRow=tableData.split("*@!");
	

 for(var i=0;i<tblRow.length;i++){
	   
var tblCol=tblRow[i].split("#^$");

for(var j=0;j<tblCol.length;j++){
 
	if(j==tblCol.length-3){
		
	q4total=tblCol[j];
	
	}
	if(j==tblCol.length-4){
		q3total=tblCol[j];
		}
	
	if(j==tblCol.length-5){
		q2total=tblCol[j];
		}
	
	if(j==tblCol.length-6){
		q1total=tblCol[j];
		}
 }
	
}
 

	
	 
 var tblRow2=tableData.split("*#!");
	

 for(var i=0;i<tblRow2.length;i++){
	   
var tblCol2=tblRow2[i].split("$^$");

for(var j=0;j<tblCol2.length;j++){
 
	if(j==tblCol2.length-3){
		
		q4exittotal=tblCol2[j];
	
	}
	if(j==tblCol2.length-4){
		q3exittotal=tblCol2[j];
		}
	
	if(j==tblCol2.length-5){
		q2exittotal=tblCol2[j];
		}
	
	if(j==tblCol2.length-6){
		q1exittotal=tblCol2[j];
		}
 }
	
}


	
	
	var data = google.visualization.arrayToDataTable([
       ['Quarter', 'Onboard', { role: 'annotation'} , { role: 'style' },'Exit', { role: 'annotation'}, { role: 'style' }],
       ['Q-1',  parseInt(q1total),q1total,'#00aae7', parseInt(q1exittotal), q1exittotal,'#f5888d'],
       ['Q-2',  parseInt(q2total),q2total,'#00aae7',  parseInt(q2exittotal), q2exittotal,'#f5888d'],
       ['Q-3',  parseInt(q3total),q3total,'#00aae7',  parseInt(q3exittotal), q3exittotal,'#f5888d'],
       ['Q-4',  parseInt(q4total),q4total,'#00aae7',   parseInt(q4exittotal), q4exittotal,'#f5888d']
       
    ]);

   
   
   var options = {title: ''+year+' - Onboard & Exit Status',
		   
		   legend: {
       	 	
	            position: 'bottom' 
	         
	        },
   colors: ['#00aae7', '#f5888d']
		   
   };  

   // Instantiate and draw the chart.
   var chart = new google.visualization.ColumnChart(document.getElementById('onboardtatusStack'));
   chart.draw(data, options);
}
	
	
//getRollInRollOffAnalysisResponsePractice

function getRollInRollOffAnalysisResponsePractice(response){
	
	
	
	
	var year=document.getElementById('year1').value;
	
	  var tableData=response;
	//alert("response"+response);

	var cf="<div  align='center' style='margin-top: 0; margin-bottom: 0; padding-top: 0; padding-bottom: 0; width: 100%; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%;' offset='0' margin='0' data-gr-c-s-loaded='true' bgcolor='#ffffff' topmargin='0' marginheight='0' leftmargin='0'>" +
"    <table cellpadding='0' cellspacing='0' border='0' align='center' width='100%' style='border:0px !important;max-width:1230px;'>" +
"      <tbody>" +
"        <tr>" +
"          <td align='left' border='0' style='border:0px !important;'>";

			  
			  
			  
			  
			  var cb="</td></tr></tbody></table></div>";
			  
		  
			 //onboard data 
			  
			  var dv1b="</tbody></table></div> </td></tr>";
			  
			  
		
			 
			var dv1f="<div cellpadding='0' cellspacing='0' align='center' width='50%' style='padding:5px;max-width:610px;display:inline-block;'>" +
"              <table class='wholetable' align='left' width='100%'>" +
"                <tbody>" +
"                  <tr>" +
"                    <th class='wholetable' bgcolor='#00aae7'>" +
"                      <span style='font-size: 1.5vw;color: #ffffff;'><b>"+year+"</b></span>" +
"                    </th>" +
"                    <th class='wholetable' colspan='11' bgcolor='#232527'>" +
"                      <span style='color: #ffffff;font-size: 1.5vw;'><b>Onboard Quarterly</b></span>" +
"                    </th>" +
"                  </tr>" +
"                  <tr>" +
"                    <th class='total1'><b>Practices</b></th>" +
"                    <th class='total' colspan='2'><b>Q1</b></th>" +
"                    <th class='total' colspan='2'><b>Q2</b></th>" +
"                    <th class='total' colspan='2'><b>Q3</b></th>" +
"                    <th class='total' colspan='2'><b>Q4</b></th>" +
"                    <th class='total1' colspan='2'><b>Total</b></th>" +
"                    <th class='total1'><b>Grand Total</b></th>" +
"                  </tr>" +
"                  <tr>" +
"                    <td class='total'></td>" +
"                    <td class='total2'>M</td>" +
"                    <td class='total3'>F</td>" +
"                    <td class='total2'>M</td>" +
"                    <td class='total3'>F</td>" +
"                    <td class='total2'>M</td>" +
"                    <td class='total3'>F</td>" +
"                    <td class='total2'>M</td>" +
"                    <td class='total3'>F</td>" +
"                    <td class='total2'>M</td>" +
"                    <td class='total3'>F</td>" +
"                    <td class='total2'></td>" +
"                  </tr>";

					   
					     
					  //   alert("tableData"+tableData);
					     
					     var tblRow=tableData.split("*@!");
					    
					   
						  var tr="";
					   for(var i=0;i<tblRow.length-1;i++)
					   {   		tr=tr+"<tr>"; 
					   var tblCol=tblRow[i].split("#^$");
					   //alert("tblRow.length-1out"+i);
					   if(i==tblRow.length-2){
						   
		            	   
		            	   for(var j=0;j<tblCol.length;j++){
		            		   if(j==0)
		            			   {
		            		   tr=tr+"<th class='total1'><b>"+tblCol[j]+"</b></th>";
		            			   }
		            		   else
		            			   {
		            			   tr=tr+"<th class='total' colspan='2'>"+tblCol[j]+"</th>";
		            			   }
		            			   
		            	   }
					   }
					   
					   else{	
	               
	               
						   				for(var j=0;j<tblCol.length;j++)
						   				{
						   						if(j==0 || j==11){
						   							tr=tr+"<td class='total'>"+tblCol[j]+"</td>";
						   						}
						   						else if(j==9)
						   						{
						   							tr=tr+"<td class='total2'>"+tblCol[j]+"</td>";
						   						}
						   						else if(j==10)
						   						{
						   							tr=tr+"<td class='total3'>"+tblCol[j]+"</td>";
						   						}
						
						   						else{
						   							tr=tr+"<td class='tdfont'>"+tblCol[j]+"</td>";
						   						}
						
						   				}
						   		tr=tr+"</tr>";
					   }
					}
					   
					   

					   

	//onbord end 
	
	//exit start
					   var dv2b="</tbody></table></div></td></tr></table>";
						  
						
						var dv2f="<tr><td align='left' border='0' style='border:0px !important;'><br><div cellpadding='0' cellspacing='0' align='center' width='50%' style='padding:0px;max-width:610px;display:inline-block;'>" +
"              <table class='wholetable' align='left' width='100%'>" +
"                <tbody>" +
"                  <tr>" +
"                    <th class='wholetable' bgcolor='#00aae7'>" +
"                      <span style='font-size: 1.5vw;color: #ffffff;'><b>"+year+"</b></span>" +
"                    </th>" +
"                    <th class='wholetable' colspan='11' bgcolor='#232527'>" +
"                      <span style='color: #ffffff;font-size: 1.5vw;'><b>Exit Quarterly</b></span>" +
"                    </th>" +
"                  </tr>" +
"                  <tr>" +
"                    <th class='total4'><b>Practices</b></th>" +
"                    <th class='total5' colspan='2'><b>Q1</b></th>" +
"                    <th class='total5' colspan='2'><b>Q2</b></th>" +
"                    <th class='total5' colspan='2'><b>Q3</b></th>" +
"                    <th class='total5' colspan='2'><b>Q4</b></th>" +
"                    <th class='total4' colspan='2'><b>Total</b></th>" +
"                    <th class='total4'><b>Grand Total</b></th>" +
"                  </tr>" +
"                  <tr>" +
"                    <td class='total5'></td>" +
"                    <td class='total2'>M</td>" +
"                    <td class='total3'>F</td>" +
"                    <td class='total2'>M</td>" +
"                    <td class='total3'>F</td>" +
"                    <td class='total2'>M</td>" +
"                    <td class='total3'>F</td>" +
"                    <td class='total2'>M</td>" +
"                    <td class='total3'>F</td>" +
"                    <td class='total2'>M</td>" +
"                    <td class='total3'>F</td>" +
"                    <td class='total2'></td>" +
"                  </tr>";

								    
								     
								   //  alert("tableData"+tableData);
								     
						
					     
					   //  alert("tableData"+tableData);
					     
					     var tblRow=tableData.split("*#!");
					   // alert("tblRow"+tblRow.length);
					   
						  var div2tr="";
					   for(var i=0;i<tblRow.length-1;i++)
					   {   		div2tr=div2tr+"<tr>"; 
					   var tblCol=tblRow[i].split("$^$");
					 //  alert("tblCol"+tblCol.length);
					   if(i==tblRow.length-2){
						   
		            	   
		            	   for(var j=0;j<tblCol.length;j++){
		            		   
		            		   if(j==0)
		            			   {
		            			   div2tr=div2tr+"<th class='total4'><b>"+tblCol[j]+"</b></th>";
		            			   }
		            		   else
		            			   {
		            			   div2tr=div2tr+"<th class='total5' colspan='2'>"+tblCol[j]+"</th>";
		            			   }
		            			   
		            	   }
					   }
					   else if(i==0){
						   
						   var tblExit=tblRow[i].split("Exit");
						   
						   
						   var tblColexit=tblExit[i+1].split("$^$");
		            	   for(var j=0;j<tblCol.length;j++){
		            		   
		            		   if(j==0)
		            			   {
		            			   
		            			   div2tr=div2tr+"<td class='total5'>"+tblColexit[0]+"</td>";
		            			   }
		            		   else if(j==11)
		            			   {
		            			   div2tr=div2tr+"<td class='total5'>"+tblCol[j]+"</td>";
		            			   }
		            		   else if(j==9)
		   						{
		   							div2tr=div2tr+"<td class='total2'>"+tblCol[j]+"</td>";
		   						}
		   						else if(j==10)
		   						{
		   							div2tr=div2tr+"<td class='total3'>"+tblCol[j]+"</td>";
		   						}
		
		   						else{
		   							div2tr=div2tr+"<td class='tdfont'>"+tblCol[j]+"</td>";
		   						}
		            			   
		            	   }
					   }
					   
					   
					   
					   
					   else{	
	               
	               
						   				for(var j=0;j<tblCol.length;j++)
						   				{
						   						
						   						
						   						if(j==0 || j==11  )
						   							{
						   						
						   						div2tr=div2tr+"<td class='total5'>"+tblCol[j]+"</td>";
						   							}
						   						else if(j==9)
						   						{
						   							div2tr=div2tr+"<td class='total2'>"+tblCol[j]+"</td>";
						   						}
						   						else if(j==10)
						   						{
						   							div2tr=div2tr+"<td class='total3'>"+tblCol[j]+"</td>";
						   						}
						
						   						else{
						   							div2tr=div2tr+"<td class='tdfont'>"+tblCol[j]+"</td>";
						   						}
						
						   				}
						   				div2tr=div2tr+"</tr>";
					   }
					}
					   

	
	
	
	
	var content=cf+dv1f+tr+dv1b+dv2f+div2tr+dv2b+cb;
	

	document.getElementById('ProjectRolinoff').innerHTML=content;  

	// sarada chngEnd
	


	
	drawChartPractice(response,year);
	
}


	
function drawChartPractice(response,year) {
	
	var q1ontotal;
	var q2ontotal;
	
	var q3ontotal;
	var q4ontotal;
	//exit dec
	var q1exittotal;
	var q2exittotal;
	
	var q3exittotal;
	var q4exittotal;
	var tableData=response;
	 var tblRow=tableData.split("*@!");
	

 for(var i=0;i<tblRow.length;i++){
	   
var tblCol=tblRow[i].split("#^$");

for(var j=0;j<tblCol.length;j++){
 
	if(j==tblCol.length-3){
		
	q4total=tblCol[j];
	
	}
	if(j==tblCol.length-4){
		q3total=tblCol[j];
		}
	
	if(j==tblCol.length-5){
		q2total=tblCol[j];
		}
	
	if(j==tblCol.length-6){
		q1total=tblCol[j];
		}
 }
	
}
 

	
	 
 var tblRow2=tableData.split("*#!");
	

 for(var i=0;i<tblRow2.length;i++){
	   
var tblCol2=tblRow2[i].split("$^$");

for(var j=0;j<tblCol2.length;j++){
 
	if(j==tblCol2.length-3){
		
		q4exittotal=tblCol2[j];
	
	}
	if(j==tblCol2.length-4){
		q3exittotal=tblCol2[j];
		}
	
	if(j==tblCol2.length-5){
		q2exittotal=tblCol2[j];
		}
	
	if(j==tblCol2.length-6){
		q1exittotal=tblCol2[j];
		}
 }
	
}


	
	
	var data = google.visualization.arrayToDataTable([
       ['Quarter', 'Onboard', { role: 'annotation'}, { role: 'style' } ,'Exit', { role: 'annotation'}, { role: 'style' }],
       ['Q-1',  parseInt(q1total),q1total,'#00aae7', parseInt(q1exittotal), q1exittotal,'#f5888d'],
       ['Q-2',  parseInt(q2total),q2total,'#00aae7',  parseInt(q2exittotal), q2exittotal,'#f5888d'],
       ['Q-3',  parseInt(q3total),q3total,'#00aae7',  parseInt(q3exittotal), q3exittotal,'#f5888d'],
       ['Q-4',  parseInt(q4total),q4total,'#00aae7',   parseInt(q4exittotal), q4exittotal,'#f5888d']
       
    ]);

   
   
   var options = {
		   title: ''+year+' - Practices Onboard & Exit Status',
		   
	        legend: {
	        	 	
	            position: 'bottom' 
	         
	        },
   colors: ['#00aae7', '#f5888d']
		   
   };  

   // Instantiate and draw the chart.
   var chart = new google.visualization.ColumnChart(document.getElementById('onboardtatusStack'));
   chart.draw(data, options);
}

//Response showing all Practices data  monthly wise

function getRollInRollOffAnalysisMonthlyResponsePractice(response){
//	alert("getRollInRollOffAnalysisMonthlyResponsePractice");
	
	// document.getElementById('loadingMessage7').style.display='none';
	
	var year=document.getElementById('year1').value;
	
	  var tableData=response;
	
	var cf="<div  align='center' style='margin-top: 0; margin-bottom: 0; padding-top: 0; padding-bottom: 0; width: 100%; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%;' offset='0' margin='0' data-gr-c-s-loaded='true' bgcolor='#ffffff' topmargin='0' marginheight='0' leftmargin='0'>" +
"    <table cellpadding='0' cellspacing='0' border='0' align='center' width='100%' style='border:0px !important;max-width:1230px;'>" +
"      <tbody>" +
"        <tr>" +
"          <td align='center' border='0' style='border:0px !important;'>";

			  
			  
			  
	  
	  var cb="</td></tr></tbody></table></div>";
	  

	 //onboard data 
	  
	  var dv1b="</tbody></table></div> </td></tr>";
			  
			  
				
			var dv1f="<table><tr><div cellpadding='0' cellspacing='0' align='center' width='100%' style='padding:5px;max-width:610px;display:inline-block;'>" +
"              <table class='wholetable' align='center' width='100%'>" +
"                <tbody>" +
"                  <tr>" +
"                    <th class='wholetable' bgcolor='#00aae7'>" +
"                      <span style='font-size: 1.5vw;color: #ffffff;'><b>"+year+"</b></span>" +
"                    </th>" +
"                    <th class='wholetable' colspan='31' bgcolor='#232527'>" +
"                      <span style='color: #ffffff;font-size: 1.5vw;'><b>Onboard Monthly</b></span>" +
"                    </th>" +
"                  </tr>" +
"                  <tr>" +
"                    <th class='total1'><b>Practices</b></th>" +

"					 <th class='total' colspan='2'><b>Jan</b></th>" +
"                    <th class='total' colspan='2'><b>Feb</b></th>" +
"                    <th class='total' colspan='2'><b>Mar</b></th>" +
"                    <th class='total' colspan='2'><b>Apr</b></th>" +
"                    <th class='total' colspan='2'><b>May</b></th>" +
"                    <th class='total' colspan='2'><b>Jun</b></th>" +
"                    <th class='total' colspan='2'><b>Jul</b></th>" +
"                    <th class='total' colspan='2'><b>Aug</b></th>" +
"                    <th class='total' colspan='2'><b>Sept</b></th>" +
"                    <th class='total' colspan='2'><b>Oct</b></th>" +
"                    <th class='total' colspan='2'><b>Nov</b></th>" +
"                    <th class='total' colspan='2'><b>Dec</b></th>" +
"                    <th class='total1' colspan='2'><b>Total</b></th>" +
"                    <th class='total1'><b>Grand Total</b></th>" +
"                  </tr>" +
"                  <tr>" +
"                    <td class='total'></td>" +
"                    <td class='total2'>M</td>" +
"                    <td class='total3'>F</td>" +
"                    <td class='total2'>M</td>" +
"                    <td class='total3'>F</td>" +
"                    <td class='total2'>M</td>" +
"                    <td class='total3'>F</td>" +
"                    <td class='total2'>M</td>" +
"                    <td class='total3'>F</td>" +
"                    <td class='total2'>M</td>" +
"                    <td class='total3'>F</td>" +
"                    <td class='total2'>M</td>" +
"                    <td class='total3'>F</td>" +
"                    <td class='total2'>M</td>" +
"                    <td class='total3'>F</td>" +
"                    <td class='total2'>M</td>" +
"                    <td class='total3'>F</td>" +
"                    <td class='total2'>M</td>" +
"                    <td class='total3'>F</td>" +
"                    <td class='total2'>M</td>" +
"                    <td class='total3'>F</td>" +
"                    <td class='total2'>M</td>" +
"                    <td class='total3'>F</td>" +
"                    <td class='total2'>M</td>" +
"                    <td class='total3'>F</td>" +
"                    <td class='total2'>M</td>" +
"                    <td class='total3'>F</td>" +
"                    <td class='total2'></td>" +
"                  </tr>";

					   
					     
					   ///  alert("tableData"+tableData);
					     
					     var tblRow=tableData.split("*@!");
					    
					   
						  var tr="";
					   for(var i=0;i<tblRow.length-1;i++)
					   {   		tr=tr+"<tr>"; 
					   var tblCol=tblRow[i].split("#^$");
					   //alert("tblRow.length-1out"+i);
					   if(i==tblRow.length-2){
						   
		            	   
		            	   for(var j=0;j<tblCol.length;j++){
		            		   if(j==0)
		            			   {
		            		   tr=tr+"<th class='total1'><b>"+tblCol[j]+"</b></th>";
		            			   }
		            		   else
		            			   {
		            			   tr=tr+"<th class='total' colspan='2'>"+tblCol[j]+"</th>";
		            			   }
		            			   
		            	   }
					   }
					   
					   else{	
	               
	               
						   				for(var j=0;j<tblCol.length;j++)
						   				{
						   						if(j==0 || j==27){
						   							tr=tr+"<td class='total'>"+tblCol[j]+"</td>";
						   						}
						   						else if(j==25)
						   						{
						   							tr=tr+"<td class='total2'>"+tblCol[j]+"</td>";
						   						}
						   						else if(j==26)
						   						{
						   							tr=tr+"<td class='total3'>"+tblCol[j]+"</td>";
						   						}
						
						   						else{
						   							tr=tr+"<td class='tdfont'>"+tblCol[j]+"</td>";
						   						}
						
						   				}
						   		tr=tr+"</tr>";
					   }
					}
					   
					   

					  

	//onbord end 
	
	//exit start
					   var dv2b="</tbody></table></div></td></tr></table>";
						  
						
						var dv2f="<tr><td border='0' style='border:0px !important;'><br><div cellpadding='0' cellspacing='0' align='center' width='100%' style='padding:0px;max-width:610px;display:inline-block;'>" +
"              <table class='wholetable' align='center' width='100%'>" +
"                <tbody>" +
"                  <tr>" +
"                    <th class='wholetable' bgcolor='#00aae7'>" +
"                      <span style='font-size: 1.5vw;color: #ffffff;'><b>"+year+"</b></span>" +
"                    </th>" +
"                    <th class='wholetable' colspan='31' bgcolor='#232527'>" +
"                      <span style='color: #ffffff;font-size: 1.5vw;'><b>Exit Monthly</b></span>" +
"                    </th>" +
"                  </tr>" +
"                  <tr>" +
"                    <th class='total4'><b>Practices</b></th>" +
"                    <th class='total5' colspan='2'><b>Jan</b></th>" +
"                    <th class='total5' colspan='2'><b>Feb</b></th>" +
"                    <th class='total5' colspan='2'><b>Mar</b></th>" +
"                    <th class='total5' colspan='2'><b>Apr</b></th>" +
"                    <th class='total5' colspan='2'><b>May</b></th>" +
"                    <th class='total5' colspan='2'><b>Jun</b></th>" +
"                    <th class='total5' colspan='2'><b>Jul</b></th>" +
"                    <th class='total5' colspan='2'><b>Aug</b></th>" +
"                    <th class='total5' colspan='2'><b>Sept</b></th>" +
"                    <th class='total5' colspan='2'><b>Oct</b></th>" +
"                    <th class='total5' colspan='2'><b>Nov</b></th>" +
"                    <th class='total5' colspan='2'><b>Dec</b></th>" +
"                    <th class='total4' colspan='2'><b>Total</b></th>" +
"                    <th class='total4'><b>Grand Total</b></th>" +
"                  </tr>" +

"                  <tr>" +
"                    <td class='total5'></td>" +
"                    <td class='total2'>M</td>" +
"                    <td class='total3'>F</td>" +
"                    <td class='total2'>M</td>" +
"                    <td class='total3'>F</td>" +
"                    <td class='total2'>M</td>" +
"                    <td class='total3'>F</td>" +
"                    <td class='total2'>M</td>" +
"                    <td class='total3'>F</td>" +
"                    <td class='total2'>M</td>" +
"                    <td class='total3'>F</td>" +
"                    <td class='total2'>M</td>" +
"                    <td class='total3'>F</td>" +
"                    <td class='total2'>M</td>" +
"                    <td class='total3'>F</td>" +
"                    <td class='total2'>M</td>" +
"                    <td class='total3'>F</td>" +
"                    <td class='total2'>M</td>" +
"                    <td class='total3'>F</td>" +
"                    <td class='total2'>M</td>" +
"                    <td class='total3'>F</td>" +
"                    <td class='total2'>M</td>" +
"                    <td class='total3'>F</td>" +
"                    <td class='total2'>M</td>" +
"                    <td class='total3'>F</td>" +
"                    <td class='total2'>M</td>" +
"                    <td class='total3'>F</td>" +
"                    <td class='total2'></td>" +
"                  </tr>";

								    
								     
								   //  alert("tableData"+tableData);
								     
						
					     
					   //  alert("tableData"+tableData);
					     
					     var tblRow=tableData.split("*#!");
					    //alert("tblRow"+tblRow.length);
					   
						  var div2tr="";
					   for(var i=0;i<tblRow.length-1;i++)
					   {   		div2tr=div2tr+"<tr>"; 
					   var tblCol=tblRow[i].split("$^$");
					  // alert("tblCol"+tblCol.length);
					   if(i==tblRow.length-2){
						   
		            	   
		            	   for(var j=0;j<tblCol.length;j++){
		            		   
		            		   if(j==0)
		            			   {
		            			   div2tr=div2tr+"<th class='total4'><b>"+tblCol[j]+"</b></th>";
		            			   }
		            		   else
		            			   {
		            			   div2tr=div2tr+"<th class='total5' colspan='2'>"+tblCol[j]+"</th>";
		            			   }
		            			   
		            	   }
					   }
					   else if(i==0){
						   
						   var tblExit=tblRow[i].split("Exit");
						   
						   
						   var tblColexit=tblExit[i+1].split("$^$");
		            	   for(var j=0;j<tblCol.length;j++){
		            		   
		            		   if(j==0)
		            			   {
		            			   div2tr=div2tr+"<td class='total5'>"+tblColexit[0]+"</td>";
		            			   }
		            		   else if(j==27)
		            			   {
		            			   div2tr=div2tr+"<td class='total5'>"+tblCol[j]+"</td>";
		            			   }
		            		   else if(j==25)
		   						{
		   							div2tr=div2tr+"<td class='total2'>"+tblCol[j]+"</td>";
		   						}
		   						else if(j==26)
		   						{
		   							div2tr=div2tr+"<td class='total3'>"+tblCol[j]+"</td>";
		   						}
		
		   						else{
		   							div2tr=div2tr+"<td class='tdfont'>"+tblCol[j]+"</td>";
		   						}
		            			   
		            	   }
					   }
					   
					   
					   
					   
					   else{	
	               
	               
						   				for(var j=0;j<tblCol.length;j++)
						   				{
						   						
						   						
						   						if(j==0 || j==27  )
						   							{
						   						
						   						div2tr=div2tr+"<td class='total5'>"+tblCol[j]+"</td>";
						   							}
						   						else if(j==25)
						   						{
						   							div2tr=div2tr+"<td class='total2'>"+tblCol[j]+"</td>";
						   						}
						   						else if(j==26)
						   						{
						   							div2tr=div2tr+"<td class='total3'>"+tblCol[j]+"</td>";
						   						}
						
						   						else{
						   							div2tr=div2tr+"<td class='tdfont'>"+tblCol[j]+"</td>";
						   						}
						
						   				}
						   				div2tr=div2tr+"</tr>";
					   }
					}
					   

	
	
	
	
	var content=cf+dv1f+tr+dv1b+"<br>"+"</br>"+dv2f+div2tr+dv2b+cb;
	

	document.getElementById('ProjectRolinoff').innerHTML=content;  

	// sarada chngEnd
	


	
	drawChartMonthlyPractice(response,year);
	
}	
function drawChartMonthlyPractice(response,year) {
	
	var janontotal;
	var febontotal;
	var marontotal;
	var aprontotal;
	var mayontotal;
	var juneontotal;
	var julyontotal;
	var augontotal;
	var sepontotal;
	var octontotal;
	var novontotal;
	var decontotal;
	
	
	var janexittotal;
	var febexittotal;
	var marexittotal;
	var aprexittotal;
	var mayexittotal;
	var junexittotal;
	var julyexittotal;
	var augexittotal;
	var sepexittotal;
	var octexittotal;
	var novexittotal;
	var decexittotal;
	

	
	
	var tableData=response;
	 var tblRow=tableData.split("*@!");
	

 for(var i=0;i<tblRow.length;i++){
	   
var tblCol=tblRow[i].split("#^$");

for(var j=0;j<tblCol.length;j++){
 
	if(j==tblCol.length-3){
		
		decontotal=tblCol[j];
	
	}
	if(j==tblCol.length-4){
		novontotal=tblCol[j];
		}
	
	if(j==tblCol.length-5){
		octontotal=tblCol[j];
		}
	
	if(j==tblCol.length-6){
		sepontotal=tblCol[j];
		}
	if(j==tblCol.length-7){
		
	augontotal=tblCol[j];
	
	}
	if(j==tblCol.length-8){
		julyontotal=tblCol[j];
		}
	
	if(j==tblCol.length-9){
		juneontotal=tblCol[j];
		}
	
	if(j==tblCol.length-10){
		mayontotal=tblCol[j];
		}
	if(j==tblCol.length-11){
		
		aprontotal=tblCol[j];
		
		}
		if(j==tblCol.length-12){
			marontotal=tblCol[j];
			}
		
		if(j==tblCol.length-13){
			febontotal=tblCol[j];
			}
		
		if(j==tblCol.length-14){
			janontotal=tblCol[j];
			}
 }
	
}
 

	
	 
 var tblRow2=tableData.split("*#!");
	

 for(var i=0;i<tblRow2.length;i++){
	   
var tblCol2=tblRow2[i].split("$^$");

for(var j=0;j<tblCol2.length;j++){
	


	if(j==tblCol2.length-3){
		
		decexittotal=tblCol2[j];
	
	}
	if(j==tblCol2.length-4){
		novexittotal=tblCol2[j];
		}
	
	if(j==tblCol2.length-5){
		octexittotal=tblCol2[j];
		}
	
	if(j==tblCol2.length-6){
		sepexittotal=tblCol2[j];
		}
	if(j==tblCol2.length-7){
		
		augexittotal=tblCol2[j];
	
	}
	if(j==tblCol2.length-8){
		julyexittotal=tblCol2[j];
		}
	
	
	
	if(j==tblCol2.length-9){
		junexittotal=tblCol2[j];
		}
	
	if(j==tblCol2.length-10){
		mayexittotal=tblCol2[j];
		}
	if(j==tblCol2.length-11){
		
		aprexittotal=tblCol2[j];
		
		}
		if(j==tblCol2.length-12){
			marexittotal=tblCol2[j];
			}
		
		if(j==tblCol2.length-13){
			febexittotal=tblCol2[j];
			}
		
		if(j==tblCol2.length-14){
			janexittotal=tblCol2[j];
			}
 }
	
 }
	


	 

	
	
var data = google.visualization.arrayToDataTable([
   ['Month', 'Onboard', { role: 'annotation'} , { role: 'style' },'Exit', { role: 'annotation'}, { role: 'style' }],
   ['Jan',  parseInt(janontotal),janontotal,'#00aae7', parseInt(janexittotal), janexittotal,'#f5888d'],
   ['Feb',  parseInt(febontotal),febontotal,'#00aae7',  parseInt(febexittotal), febexittotal ,'#f5888d'],
   ['Mar',  parseInt(marontotal),marontotal,'#00aae7',  parseInt(marexittotal), marexittotal ,'#f5888d'],
   ['Apr',  parseInt(aprontotal),aprontotal, '#00aae7',  parseInt(aprexittotal), aprexittotal,'#f5888d'],
	['May',  parseInt(mayontotal),mayontotal,'#00aae7', parseInt(mayexittotal), mayexittotal,'#f5888d'],
   ['Jun',  parseInt(juneontotal),juneontotal,'#00aae7',  parseInt(junexittotal), junexittotal,'#f5888d'],
   ['Jul',  parseInt(julyontotal),julyontotal,'#00aae7',  parseInt(julyexittotal), julyexittotal,'#f5888d'],
   ['Aug',  parseInt(augontotal),augontotal, '#00aae7',  parseInt(augexittotal), augexittotal,'#f5888d'],
	['Sep',  parseInt(sepontotal),sepontotal,'#00aae7', parseInt(sepexittotal), sepexittotal,'#f5888d'],
   ['Oct',  parseInt(octontotal),octontotal,'#00aae7',  parseInt(octexittotal), octexittotal,'#f5888d'],
   ['Nov',  parseInt(novontotal),novontotal,'#00aae7',  parseInt(novexittotal), novexittotal,'#f5888d'],
   ['Dec',  parseInt(decontotal),decontotal,  '#00aae7', parseInt(decexittotal), decexittotal,'#f5888d']
	

       
]);



   
   
   var options = {title: ''+year+' - Onboard & Exit Status',
		   
		   legend: {
       	 	
	            position: 'bottom'
	         
	        },
   colors: ['#00aae7', '#f5888d']
   };  
   

   // Instantiate and draw the chart.
   var chart = new google.visualization.ColumnChart(document.getElementById('onboardtatusStack'));
   chart.draw(data, options);
}
	

function isNumericYearRoll(element){
    
    var val=element.value;
    var numbers = /^[0-9]+$/;  
    if(!val.match(numbers))  {
        alert('Please Enter numeric values');
        //   element.value=val.substring(0, val.length-1);      
        element.value="";      
        return false;
    }
    else
        return true;
}



function getPracticeDataV1() {
	//alert("getPracticeDataV1");
    
    var departmentName = document.getElementById("departmentId4").value;
    var req = newXMLHttpRequest();
    req.onreadystatechange = readyStateHandler80(req, populatePractices);
    var url = CONTENXT_PATH+"/getEmpDepartment.action?departmentName="+departmentName;
    req.open("GET",url,"true");    
    req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    req.send(null);
    
}

function populatePractices(resXML) { 
	
	//alert("resXML"+resXML);
    
    var practiceId = document.getElementById("practiceId4");
    var department = resXML.getElementsByTagName("DEPARTMENT")[0];
    var practices = department.getElementsByTagName("PRACTICE");
    practiceId.innerHTML=" ";
    
    for(var i=0;i<practices.length;i++) {
        var practiceName = practices[i];
        
        var name = practiceName.firstChild.nodeValue;
        var opt = document.createElement("option");
        if(i==0){
            opt.setAttribute("value","");
        }else{
            opt.setAttribute("value",name);
        }
        opt.appendChild(document.createTextNode(name));
        practiceId.appendChild(opt);
    }
}


function readyStateHandler80(req,responseXmlHandler) {
    return function() {
        if (req.readyState == 4) {
            if (req.status == 200) {
                responseXmlHandler(req.responseXML);
            } else {
                alert("HTTP error"+req.status+" : "+req.statusText);
            }
        }
    }
}

/*Methods closing Practices by Department*/
	
function getProjectResourcesRollInRollOffReportDetails(){
	var projstartDate=document.getElementById('projstartDate').value;
	var endDate=document.getElementById('endDate5').value;
	var type=document.getElementById('type1').value;
	var departmentId=document.getElementById('departmentId4').value;
	var practiceId=document.getElementById('practiceId4').value;
	var costModel=document.getElementById('costModel5').value;
	var customerName=document.getElementById('customerNameId5').value;
	
	//alert("costModel"+costModel);
	//alert("customerName"+customerName);
	if(projstartDate==''){
		alert("Please select start date");
		return false;
	}

	if(endDate==''){
		alert("Please select end date");
		return false;
	}
	
	
	if(practiceId=='')
		{
		//alert("practiceId"+practiceId);
		practiceId='-1';
		}
	clearTable(document.getElementById("tblProjectResourcesRollInRollOff"));
		  document.getElementById('loadingMessage8').style.display='block';
	    var req = newXMLHttpRequest();
	    req.onreadystatechange = readyStateHandlerText(req, getProjectResourcesRollInRollOffResponse); 

	    //var url = CONTENXT_PATH+"/searchpmoActivityAjaxList.action?customerName="+NAME+"&projectName="+ProjectName+"&status="+status+"&projectStartDate="+ProjectStartDate;
	    var url = CONTENXT_PATH+"/getProjectResourcesRollInRollOffReportDetails.action?projstartDate="+projstartDate+"&endDate="+endDate+"&type="+type+"&departmentId="+departmentId+"&practiceId="+practiceId+"&costModel="+escape(costModel)+"&customerName="+customerName;
	  //  alert("url"+url);
	    req.open("GET",url,"true");    
	    req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
	    req.send(null);
	}

	function getProjectResourcesRollInRollOffResponse(response){
		//alert("response"+response);
	  document.getElementById('loadingMessage8').style.display='none';
	  var tableId = document.getElementById("tblProjectResourcesRollInRollOff");
	    // var headerFields = new Array("SNo","Project&nbsp;Name","StartDate","EndDate","EmpStatus","Utilization","Resource&nbsp;Type");
	    var headerFields = new Array("EmpNo","ResourceName","CustomerName","ProjectName","CostModel","HireDate","StartDate","EndDate","ResourceType","Utilization","Type");
	   
	    ParseAndGenerateHTML1(tableId,response, headerFields);
	}



	
	

	
	




