/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

var isIE;
var completeTable;
var autorow;
var completeField;


function init() {
    //var menu = document.getElementById("auto-row");
    //var menu1 = document.getElementById("auto-row1");
    autorow = document.getElementById("menu-popup");
    autorow.style.display ="none";
   
    
    //autorow.style.top = getElementY(menu) + "px";
    //autorow1.style.top = getElementY(menu1) + "px";
    var height = document.documentElement.clientHeight - 120;
    // autorow1.style.height = Math.max(height, 150);
    //    autorow1.style.height = "auto";
    //    autorow1.style.overflowY = "scroll";
    //    autorow.style.height = "auto";
    //    autorow.style.overflowY = "scroll";
    
    completeTable = document.getElementById("completeTable");
    completeTable.setAttribute("bordercolor", "white");
   
}
function initRequest(url) {
    if (window.XMLHttpRequest) {
        return new XMLHttpRequest();
    }
    else
    if (window.ActiveXObject) {
        isIE = true;
        return new ActiveXObject("Microsoft.XMLHTTP");
    }
    
}
function getEmployeeNames() {
    completeField = document.getElementById("empName");
    // alert(completeField.value)
     autorow = document.getElementById("menu-popup");
    autorow.style.display ="none";
    //alert(test);
     completeTable = document.getElementById("completeTable");
    completeTable.setAttribute("bordercolor", "white");
    if (completeField.value== "") {
         var validationMessage=document.getElementById("validationMessage");
        validationMessage.innerHTML = " ";
        clearTableSuggestion(completeTable);
    } else {
        var test = document.getElementById("empName").value;
       // alert(test)
        if (test.length >2) {
            //  var url = CONTENXT_PATH+"/getConsultantList.action?email="+ escape(test);
            var url = CONTENXT_PATH+"/getTransferLocationEmployeeNames.action?employeeName="+ escape(test);
            //alert(url);
            var req = initRequest(url);
            req.onreadystatechange = function() {
                if (req.readyState == 4) {
                    if (req.status == 200) {
                        parseCustMessages(req.responseXML);
                    } else if (req.status == 204){
                        clearTableSuggestion(completeTable);
                    }
                }
            };
            req.open("GET", url, true);
            req.send(null);
        }
    }
}

function clearTableSuggestion(completeTable) {
    if (completeTable) {
        //alert("In Clear Table");
        completeTable.setAttribute("bordercolor", "white");
        completeTable.setAttribute("border", "0");
        completeTable.style.visible = false;
        
        //document.consultantRequirementForm.email2.value ="";
       for (loop = completeTable.childNodes.length -1; loop >= 0 ; loop--) {
            completeTable.removeChild(completeTable.childNodes[loop]);
        }
    }
}


function parseCustMessages(responseXML) {
    //alert("beforeClear");
    var validationMessage=document.getElementById("validationMessage");
        validationMessage.innerHTML = " ";
    clearTableSuggestion(completeTable);
    //  alert(responseXML.getElementsByTagName("ACCOUNTNAMES")[0]);
  
    var accountNames = responseXML.getElementsByTagName("EMPLOYEES")[0];
    // alert("test");
    if (accountNames.childNodes.length > 0) {
        completeTable.setAttribute("bordercolor", "black");
        completeTable.setAttribute("border", "0");
    } else {
        clearTableSuggestion(completeTable);
    }
    if(accountNames.childNodes.length<10) {
       
        autorow.style.overflowY = "hidden";
    //alert("in If");
    }
    else {
       
        autorow.style.overflowY = "scroll";
    //alert("in Else");
    }
    var employee = accountNames.childNodes[0];
    var chk=employee.getElementsByTagName("VALID")[0];
    //alert("Before If");
    if(chk.childNodes[0].nodeValue =="true") {
        //alert("Again In If");
        var validationMessage=document.getElementById("validationMessage");
        validationMessage.innerHTML = "";
        document.getElementById("menu-popup").style.display = "block";
        for (loop = 0; loop < accountNames.childNodes.length; loop++) {
            //alert("in For");
            var employee = accountNames.childNodes[loop];
//            var id = accountName.getElementsByTagName("ID")[0];
//            var accName = accountName.getElementsByTagName("ACCNAME")[0];
             var empName = employee.getElementsByTagName("NAME")[0];
            var empLoginId = employee.getElementsByTagName("EMPLOGINID")[0];
            var empId = employee.getElementsByTagName("EMPID")[0];
            var empNo = employee.getElementsByTagName("EMPNO")[0];
            var deptId = employee.getElementsByTagName("DEPTID")[0];
            var titleId = employee.getElementsByTagName("TITLEID")[0];
            
          //  getExpencesDetails(empId);
          //  alert(titleId.childNodes[0].nodeValue)
            //  alert(id.childNodes[0].nodeValue+" "+accName.childNodes[0].nodeValue);
            // alert(targetRate.childNodes[0].nodeValue);
            // appendCustomer(email.childNodes[0].nodeValue,id.childNodes[0].nodeValue,phone.childNodes[0].nodeValue,availableFrom.childNodes[0].nodeValue,isReject.childNodes[0].nodeValue,targetRate.childNodes[0].nodeValue,workauthorization.childNodes[0].nodeValue);
            appendEmployeeNames(empName.childNodes[0].nodeValue,empLoginId.childNodes[0].nodeValue,empId.childNodes[0].nodeValue,empNo.childNodes[0].nodeValue,deptId.childNodes[0].nodeValue,titleId.childNodes[0].nodeValue);
            		
            //appendCustomer(email.childNodes[0].nodeValue,id.childNodes[0].nodeValue);
            vmessage = 1;
        }
        
    } //if
    //alert("After IF");
    if(chk.childNodes[0].nodeValue =="false") {
        var validationMessage=document.getElementById("validationMessage");
        validationMessage.innerHTML = "Employee Name is INVALID ";
        vmessage = 2;
       
    }
}

function appendEmployeeNames(empName,empLoginId,empId,empNo,deptId,titleId){
    var row;
    var nameCell;
    if (!isIE) {
        row = completeTable.insertRow(completeTable.rows.length);
        nameCell = row.insertCell(0);
    } else {
        row = document.createElement("tr");
        nameCell = document.createElement("td");
        row.appendChild(nameCell);
        completeTable.appendChild(row);
    }
    row.className = "popupRow";
    nameCell.setAttribute("bgcolor", "#3E93D4");
    var linkElement = document.createElement("a");
    linkElement.className = "popupItem";
    linkElement.setAttribute("href","javascript:set_EmployeeName('"+empName+"','"+empLoginId+"',"+empId+","+empNo+",'"+deptId+"','"+titleId+"')");
    linkElement.appendChild(document.createTextNode(empName));
    linkElement["onclick"] = new Function("hideScrollBar()");
    //alert(empId)
   /* if(empId != 0 && empId != null){
    linkElement["onclick"] = new Function("getExpencesDetails("+empId+")");
    }*/
    nameCell.appendChild(linkElement); 
}
function set_EmployeeName(empName,empLoginId,empId,empNo,deptId,titleId){
    
    completeTable = document.getElementById("completeTable");
    clearTableSuggestion(completeTable);
    
    document.getElementById("userId").value=empLoginId;
    document.getElementById("empName").value=empName;
    document.getElementById("empNoSpan").value=empNo;
    document.getElementById("empNo").value=empNo;
    document.getElementById("empId").value=empId;
    document.getElementById("departmentId").value=deptId;
    document.getElementById("titleId").value=titleId;
    
  
    
   // getEmployeeNumberByLoginId(document.getElementById("empLoginId"));
}

function hideScrollBar() {
    autorow = document.getElementById("menu-popup");
    autorow.style.display = 'none';
}
function readyStateHandlerXml(req,responseXmlHandler) {
    return function() {
        if (req.readyState == 4) {
            if (req.status == 200) {
                responseXmlHandler(req.responseXML);
            } else {
                alert("HTTP error"+req.status+" : "+req.statusText);
            }
        }
    }
}
