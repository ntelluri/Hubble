/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

function ClrTable(myHTMLTable) { 
    var tbl =  myHTMLTable;
    var lastRow = tbl.rows.length; 
    //document.getElementById('addlabel1').style.display = 'none'; 
    while (lastRow > 0) { 
        tbl.deleteRow(lastRow - 1);  
        lastRow = tbl.rows.length; 
    } 
}
function newXMLHttpRequest() {
    var xmlreq = false;
    if(window.XMLHttpRequest) {
        xmlreq = new XMLHttpRequest();
    } else if(window.ActiveXObject) {
        try {
            xmlreq = new ActiveXObject("MSxm12.XMLHTTP");
        } catch(e1) {
            try {
                xmlreq = new ActiveXObject("Microsoft.XMLHTTP");
            } catch(e2) {
                xmlreq = false;
            }
        }
    }
    return xmlreq;
}

  function readyStateHandlerMobileApp(req,responseTextHandler) {
    return function() {
        if (req.readyState == 4) {
            if (req.status == 200) {
                (document.getElementById("loadingMessage")).style.display = "none";
                responseTextHandler(req.responseText);
            } else {
                
                alert("HTTP error ---"+req.status+" : "+req.statusText);
            }
        }else {
            (document.getElementById("loadingMessage")).style.display = "block";
        }
    }
}      
  

function getMobileAppDetails(){
    var tableId = document.getElementById("tblMobileAppHistoryReport");  
    document.getElementById('totalState1').innerHTML="";
   ClrTable(tableId);
   
    
      var departmentId=document.getElementById("departmentId").value;
    var location=document.getElementById("location").value;
      var activityType=document.getElementById("activityType").value;
       var empName=document.getElementById("empName").value;
        var empId=document.getElementById("empId").value;
    var country=document.getElementById("country").value;
  
   
    document.getElementById("loadingMessage").style.display = 'block';
  
   
     var req = newXMLHttpRequest();
    req.onreadystatechange = readyStateHandlerMobileApp(req,displayMobileAppHistoryDetails); 
    var url = CONTENXT_PATH+"/getMobileAppDetails.action?activityType="+activityType+"&departmentId="+departmentId+"&location="+location+"&employeeName="+empName+"&empId="+empId+"&country="+country;
    //alert(url);
    req.open("GET",url,"true");    
    req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    req.send(null);
}


function displayMobileAppHistoryDetails(res){
  var tableId = document.getElementById("tblMobileAppHistoryReport");  
       ClrTable(tableId);
    var headerFields = new Array("S.No","Employee&nbsp;Name","Login&nbsp;Id","Activity&nbsp;DateTime","Activity&nbsp;Status");
      var dataArray = res;
    MobileAppHistoryParseAndGenerateHTML(tableId,dataArray, headerFields);
}

 function MobileAppHistoryParseAndGenerateHTML(oTable,responseString,headerFields) {
   
    var fieldDelimiter = "#^$";
    var recordDelimiter = "*@!";  
    responseString=responseString.split("addTo"); 
    var total=responseString[1];
     
   document.getElementById('totalState1').innerHTML=total;
    var records = responseString[0].split(recordDelimiter); 
    generateMobileAppHistoryTable(oTable,headerFields,records,fieldDelimiter);
}


function generateTableHeader(tableBody,headerFields0) {
    var row;
    var cell;
    
    row = document.createElement( "TR" );
    row.className="gridHeader";
    tableBody.appendChild( row );
    
    for (var i=0; i<headerFields0.length; i++) {
        cell = document.createElement( "TD" );
        cell.className="gridHeader";
        row.appendChild( cell );
        cell.innerHTML = headerFields0[i];
       // cell.width = 100;
    }
    //generateFooter(tableBody);
}
function generateMobileAppHistoryTable(oTable, headerFields,records,fieldDelimiter,total) {	

  var tbody = oTable.childNodes[0];    
    tbody = document.createElement("TBODY");
    oTable.appendChild(tbody);
     var rowlength;
        rowlength = records.length;
    if(rowlength >0 && records!=""){
    generateTableHeader(tbody,headerFields);
   
     
        
        for(var i=0;i<rowlength-1;i++) {
          
               
                 generateMobileAppHistoryTableDetails(oTable,tbody,records[i],fieldDelimiter);
                 
               
        }
        generateFooter(tbody,oTable);
    } else {
      
        generateNoRecords(tbody,oTable);
    }
    
}

function generateMobileAppHistoryTableDetails(oTable,tableBody,record,delimiter){
    var row;
    var cell;
    var fieldLength;
  //  var fields = record.split(delimiter);
    var fields = record.split("#^$");
    fieldLength = fields.length ;
    var length = fieldLength;
    
    row = document.createElement( "TR" );
    row.className="gridRowEven";
    tableBody.appendChild( row );
 
     for (var i=0;i<length-1;i++) {
       cell = document.createElement( "TD" );
              cell.className="gridColumn";       
       
              //    cell.setAttribute("align","center");
            
                cell.innerHTML = fields[i];   
       //  }  
           
            if(fields[i]!=''){
            if(i==1)
            {
                cell.setAttribute("align","left");
            }
            else
            {
               // cell.setAttribute("align","center");     
            }
            row.appendChild( cell );
        }
           
           
        }
    
}


function generateNoRecords(tbody,oTable) {
    var noRecords =document.createElement("TR");
    noRecords.className="gridRowEven";
    tbody.appendChild(noRecords);
    cell = document.createElement("TD");
    cell.className="gridColumn";
     if(oTable.id == "tblMobileAppHistoryReport"){
        cell.colSpan = "6";
    }
      
    cell.innerHTML = "No Records Found for this Search";
    noRecords.appendChild(cell);
}
	
function generateFooter(tbody,oTable) {
    var footer =document.createElement("TR");
    footer.className="gridPager";
    tbody.appendChild(footer);
    cell = document.createElement("TD");
    cell.className="gridFooter";
    cell.id="footer"+oTable.id;
      if(oTable.id == "tblMobileAppHistoryReport"){
        cell.colSpan = "6";
    }
     footer.appendChild(cell); 
}	