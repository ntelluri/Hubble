/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

function generateTableHeader(tableBody, headerFields0) {
	var row;
	var cell;
	row = document.createElement("TR");
	row.className = "gridHeader";
	tableBody.appendChild(row);

	for (var i = 0; i < headerFields0.length; i++) {
		cell = document.createElement("TD");
		cell.className = "gridHeader";
		cell.setAttribute('align', 'center');
		row.appendChild(cell);
		cell.innerHTML = headerFields0[i];
		cell.width = 100;
	}
}

function newXMLHttpRequest() {
	var xmlreq = false;
	if (window.XMLHttpRequest) {
		xmlreq = new XMLHttpRequest();
	} else if (window.ActiveXObject) {
		try {
			xmlreq = new ActiveXObject("MSxm12.XMLHTTP");
		} catch (e1) {
			try {
				xmlreq = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (e2) {
				xmlreq = false;
			}
		}
	}
	return xmlreq;
}

function ClrTable(myHTMLTable) {
	var tbl = myHTMLTable;
	var lastRow = tbl.rows.length;
	while (lastRow > 0) {
		tbl.deleteRow(lastRow - 1);
		lastRow = tbl.rows.length;
	}
}

function getStarPerformers() {

	var year = document.getElementById("year").value;
	var month = document.getElementById("month").value;
	if (isNumericYear(year)) {
		var myHTMLTable = document.getElementById("tblStarPerformer");
		ClrTable(myHTMLTable);
		document.getElementById("reqloadMessage").style.display = "block";
		document.getElementById("starPerformersPeriodSearch").disabled = true;
		if (document.getElementById("starPerformerAccess").value == 'true') {
			document.getElementById("starPerformersPeriodAdd").disabled = true;
		}
		var req = newXMLHttpRequest();
		req.onreadystatechange = readyStateHandler1(req);
		var url = CONTENXT_PATH + "/starPerformers.action?year=" + year
				+ "&month=" + month;
		req.open("GET", url, "true");
		req.setRequestHeader("Content-Type",
				"application/x-www-form-urlencoded");
		req.send(null);
	}

}

function readyStateHandler1(req) {
	return function() {
		var myHTMLTable = document.getElementById("tblStarPerformer");
		ClrTable(myHTMLTable);
		if (req.readyState == 4) {
			if (req.status == 200) {
				document.getElementById("reqloadMessage").style.display = "none";
				document.getElementById("starPerformersPeriodSearch").disabled = false;
				if (document.getElementById("starPerformerAccess").value == 'true') {
					document.getElementById("starPerformersPeriodAdd").disabled = false;
				}

				var starPerformerAccess = document
						.getElementById("starPerformerAccess").value;
				var sessionStarPerformerMarketingAccess = document
						.getElementById("sessionStarPerformerMarketingAccess").value;


				if (sessionStarPerformerMarketingAccess == '1' && starPerformerAccess == 'true') {
					var headerFields0 = new Array("SNo", "Month", "Year",
							"Status","Star Performers", "CampaignServices","RSVPCampaignServices");
				}
				
				else if (sessionStarPerformerMarketingAccess == '1') {
					var headerFields0 = new Array("SNo", "Month", "Year",
							"Status", "RSVPCampaignServices");
				} else if (starPerformerAccess == 'true') {
					var headerFields0 = new Array("SNo", "Month", "Year",
							"Status", "Star Performers", "CampaignServices");
				} else {

					var headerFields0 = new Array("SNo", "Month", "Year",
							"Status", "Star Performers");
				}
				var getResponseData;
				getResponseData = req.responseText;
				var temp = new Array();
				temp = getResponseData.split('addto');
				if (req.responseText != '') {

					ParseAndGenerateHTML(myHTMLTable, temp[0], headerFields0,
							temp[1]); // this function implementation in
										// VenusReportAjax.js
				} else {
					alert('No Result For This Search...');
					document.getElementById("reqloadMessage").style.display == "none";
				}
			} else {
				alert("please wait");
			}
		} else { // document.getElementById("loadMessage").style.display =
					// 'block';
		}
	}
}

function ParseAndGenerateHTML(oTable, responseString, headerFields0, rowCount) {
	var start = new Date();
	var fieldDelimiter = "#^$";
	var recordDelimiter = "*@!";
	var records = responseString.split(recordDelimiter);
	generateTable(oTable, headerFields0, records, fieldDelimiter);
}

function generateTable(oTable, headerFields0, records, fieldDelimiter) {
	var starPerformerAccess = document.getElementById("starPerformerAccess").value;
	var sessionStarPerformerMarketingAccess = document
			.getElementById("sessionStarPerformerMarketingAccess").value;
	var tbody = oTable.childNodes[0];
	tbody = document.createElement("TBODY");
	oTable.appendChild(tbody);
	if (records.length >= 1 && records != "") {
		generateTableHeader(tbody, headerFields0);
	}
	for (var i = 0; i < records.length - 1; i++) {
		generateRow(tbody, records[i], fieldDelimiter);
	}
	var footer = document.createElement("TR");
	footer.className = "gridPager";
	tbody.appendChild(footer);
	cell = document.createElement("TD");
	cell.className = "gridFooter";
	cell.colSpan = "" + headerFields0.length + "";
	footer.appendChild(cell);
}

function generateRow(tableBody, record, delimiter) {
	var row;
	var cell;
	var starPerformerAccess = document.getElementById("starPerformerAccess").value;
	var sessionStarPerformerMarketingAccess = document
			.getElementById("sessionStarPerformerMarketingAccess").value;
	row = document.createElement("TR");
	row.className = "gridRowEven";
	tableBody.appendChild(row);
	var fields = record.split(delimiter);
	cell = document.createElement("TD");
	cell.className = "gridColumn";
	cell.innerHTML = fields[0];
	cell.setAttribute('align', 'center');
	row.appendChild(cell);
	if (starPerformerAccess == 'true') {
		cell = document.createElement("TD");
		cell.className = "gridColumn";
		var j = document.createElement("a");
		j.setAttribute("href", "javascript:getOverlayResourceDetails('"
				+ fields[4] + "','" + fields[2] + "','" + fields[11] + "','"
				+ fields[12] + "')");
		j.appendChild(document.createTextNode(fields[5]));
		cell.appendChild(j);
		cell.setAttribute('align', 'center');
		row.appendChild(cell);
	} else {
		cell = document.createElement("TD");
		cell.className = "gridColumn";
		cell.innerHTML = fields[5];
		cell.setAttribute('align', 'center');
		row.appendChild(cell);
	}
	cell = document.createElement("TD");
	cell.className = "gridColumn";
	cell.innerHTML = fields[2];
	cell.setAttribute('align', 'center');
	row.appendChild(cell);
	cell = document.createElement("TD");
	cell.className = "gridColumn";
	cell.innerHTML = fields[3];
	cell.setAttribute('align', 'center');
	row.appendChild(cell);
	if (sessionStarPerformerMarketingAccess != '1' || starPerformerAccess == 'true') {
		cell = document.createElement("TD");
		cell.className = "gridColumn";
		var j = document.createElement("a");
		j.setAttribute("href", "javascript:starPerformersList('" + fields[4]
				+ "','" + fields[12] + "')");
		j.innerHTML = "<center><img SRC='../includes/images/go_21x21.gif' WIDTH=25 HEIGHT=25  ALTER='Click Add'></center>";

		document.create
		cell.appendChild(j);
		cell.setAttribute('align', 'center');
		row.appendChild(cell);

	}
	if (starPerformerAccess == 'true') {

		cell = document.createElement("TD");
		cell.className = "gridColumn";
		cell.innerHTML = // "<a herf='#' class='round-button' title='Generate
							// Template'
							// onclick=\"getStarPerformerTemplate('"+fields[4]+"')\">GT</a>&nbsp;"+
		// "<a herf='#' class='round-button' title='Preview'
		// onclick=\"getPreview('"+fields[6]+"','"+fields[2]+"','"+fields[5]+"')\">Prw</a>&nbsp;"+
		"<a herf='#' class='round-button1' title='publish'  onclick=\"publish('"
				+ fields[4] + "','" + fields[5] + "','" + fields[2] + "','"
				+ fields[12] + "')\">Publish</a>&nbsp;";
		// "<a herf='#' class='round-button' title='Create Email_Campaign In CC'
		// onclick=\"setEmailCampaignInCC('"+fields[4]+"','"+fields[1]+"','"+fields[2]+"')\">CEC</a>&nbsp;"+
		// "<a herf='#' class='round-button' title='Update Scheduling Date'
		// onclick=\"campaignSchedule('"+fields[4]+"')\">UpSh</a>&nbsp;";

		cell.setAttribute('align', 'center');
		row.appendChild(cell);

	}

	if (sessionStarPerformerMarketingAccess == '1') {

		cell = document.createElement("TD");
		cell.className = "gridColumn";

		if (parseInt(fields[12], 10) >= 6) {
			cell.innerHTML = "<a herf='#' class='round-button2' title='Edit RSVP SURVEY FORM'  onclick=\"SurveyFormRsvp('"
					+ fields[4]
					+ "','"
					+ fields[5]
					+ "','"
					+ fields[2]
					+ "','"
					+ fields[12]
					+ "','"
					+ fields[9]
					+ "')\">SF</a>&nbsp;"
					+
					// "<a herf='#' class='round-button' title='For Uploading
					// movie Tickets'
					// onclick=\"ticketsUpload('"+fields[9]+"','"+fields[4]+"','"+fields[12]+"')\">MvTc</a>&nbsp;"+
					"<a herf='#' class='round-button' title='For Uploading movie Tickets'  onclick=\"movieTicketsUpload('"
					+ fields[4]
					+ "','"
					+ fields[9]
					+ "','"
					+ fields[12]
					+ "')\">MvTc</a>&nbsp;"
					+ "<a herf='#' class='round-button' title='RSVP FeedBack'  onclick=\"Feedback('"
					+ fields[4]
					+ "','"
					+ fields[9]
					+ "','"
					+ fields[5]
					+ "','"
					+ fields[2] + "','" + fields[12] + "')\">F</a>&nbsp;";

		} else {
			cell.innerHTML = "<a herf='#' class='round-button' title='Create RSVP SURVEY FORM'  onclick=\"SurveyFormRsvp('"
					+ fields[4]
					+ "','"
					+ fields[5]
					+ "','"
					+ fields[2]
					+ "','"
					+ fields[12] + "','" + fields[9] + "')\">SF</a>&nbsp;";
			// "<a herf='#' class='round-button' title='RSVP Remainders'
			// onclick=\"Remainders('"+fields[9]+"','"+fields[4]+"')\">R</a>&nbsp;"+
			// "<a herf='#' class='round-button' title='RSVP FeedBack'
			// onclick=\"Feedback('"+fields[4]+"','"+fields[9]+"','"+fields[1]+"','"+fields[2]+"','"+fields[12]+"')\">F</a>&nbsp;";

		}

		cell.setAttribute('align', 'left');

		row.appendChild(cell);
	}

}

function getOverlayResourceDetails(ObjectId, year, month, status) {

	document.getElementById("starPerformerResourcesHeaderLabel234").style.color = "white";

	document.getElementById("starPerformerResourcesHeaderLabel234").innerHTML = "Star Performer Details";

	document.getElementById("resourceOverlayYear").value = year;
	document.getElementById("resourceOverlayMonth").value = month;
	document.getElementById("resourceOverlayStatus").value = status;
	// document.getElementById("resourceOverlayCount").value=count;
	document.getElementById("resourcesObjectId").value = ObjectId;

	var overlayForStarPerformerResources = document
			.getElementById('overlayForStarPerformerResources');

	var specialBoxForStarPerformerResources = document
			.getElementById('specialBoxForStarPerformerResources');

	overlayForStarPerformerResources.style.opacity = .8;
	if (overlayForStarPerformerResources.style.display == "block") {
		overlayForStarPerformerResources.style.display = "none";
		specialBoxForStarPerformerResources.style.display = "none";

	} else {
		overlayForStarPerformerResources.style.display = "block";
		specialBoxForStarPerformerResources.style.display = "block";

	}
}

function toggleOverlayForStarPerformerResources() {
	document.getElementById("starPerformerResourcesResultMessage12").innerHTML = "";
	var overlayForStarPerformerResources = document
			.getElementById('overlayForStarPerformerResources');
	var specialBoxForStarPerformerResources = document
			.getElementById('specialBoxForStarPerformerResources');

	overlayForStarPerformerResources.style.opacity = .8;
	if (overlayForStarPerformerResources.style.display == "block") {
		overlayForStarPerformerResources.style.display = "none";
		specialBoxForStarPerformerResources.style.display = "none";
		document.getElementById("resources").submit();

	} else {
		overlayForStarPerformerResources.style.display = "block";
		specialBoxForRemainders.style.display = "block";
	}

}

function readyStateHandlerT(req, responseTextHandler) {
	return function() {
		if (req.readyState == 4) {
			if (req.status == 200) {

				responseTextHandler(req.responseText);
			} else {

				alert("HTTP error ---" + req.status + " : " + req.statusText);
			}
		} else {

		}
	}
}


function doResourceOverlyUpdate() {

	document.getElementById("starPerformerResourcesResultMessage12").innerHTML = "";
	document.getElementById('starPerformerResourcesResultMessage').innerHTML = "";

	var overlayYear = document.getElementById("resourceOverlayYear").value;
	var overlayMonth = document.getElementById("resourceOverlayMonth").value;
	var overlayStatus = document.getElementById("resourceOverlayStatus").value;
	var objectId = document.getElementById("resourcesObjectId").value;

	if (overlayYear != "" && overlayYear != null && overlayMonth != ""
			&& overlayMonth != null) {
		if (isNumericYear(overlayYear)) {

			
			x0p(' Do you want to Update Details!', '', 'warning',
					function(button) {
				
						if (button == 'warning') {
							result = true;
							// alert(button)
						}
						if (button == 'cancel') {
							result = false;
						}
						if (result) {
							
							document.getElementById('resourceOverlayUpdate').disabled=true;
							document.getElementById("starPerformerResourcesLodingg").style.display = "block";
							
							var req = newXMLHttpRequest();
							req.onreadystatechange = readyStateHandlerT(req,
									displaydoUpdateResource);

							var url = CONTENXT_PATH + "/doUpdateStarPerformer.action?objectId="
									+ objectId + "&overlayYear=" + overlayYear
									+ "&overlayMonth=" + overlayMonth + "&overlayStatus="
									+ overlayStatus;

							req.open("GET", url, "true");
							req.setRequestHeader("Content-Type",
									"application/x-www-form-urlencoded");
							req.send(null);
							
							
}
					});
			
			
			
	
			
		}
	} else {
		alert("please enter all the fields")
	}
}
function displaydoUpdateResource(result) {

	document.getElementById("starPerformerResourcesLodingg").style.display = "none";
	document.getElementById("starPerformerResourcesResultMessage12").innerHTML = result;
}

// Star Performer Add related methods start

function StarPerformersAjaxAdd() {
	document.getElementById("resultMessage123").innerHTML = "";
	document.getElementById('resultMessage').innerHTML = "";
        

	var overlayYear = document.getElementById("overlayYear").value;
	var overlayMonth = document.getElementById("overlayMonth").value;
	var overlayStatus = document.getElementById("overlayStatus").value;
	var month = document.getElementById('overlayMonth').options[document
			.getElementById('overlayMonth').selectedIndex].text;

	if (overlayYear != "" && overlayYear != null && overlayMonth != ""
			&& overlayMonth != null) {
		if (isNumericYear(overlayYear)) {
			
			x0p(' Do you want to Add !', '', 'warning',
					function(button) {
				
						if (button == 'warning') {
							result = true;
							// alert(button)
						}
						if (button == 'cancel') {
							result = false;
						}
						if (result) {
							

		                    document.getElementById('spInitiationAddButton').disabled=true;
					document.getElementById("load").style.display = 'block';
					$
							.ajax({
								url : 'addStarDetails.action?overlayYear='
										+ overlayYear + '&overlayMonth=' + overlayMonth
										+ '&overlayStatus=' + overlayStatus + '&month='
										+ month,
								context : document.body,
								success : function(responseText) {

									document.getElementById("load").style.display = 'none';
									document.getElementById('resultMessage').innerHTML = responseText;// "<font
																										// color=green>File
										document.getElementById('spInitiationAddButton').disabled=false;																// uploaded
																										// successfully</font>";
								},
								error : function(e) {
									document.getElementById("load").style.display = 'none';
									document.getElementById('resultMessage').innerHTML = "<font color=red>Please try again later</font>";
		                                                        document.getElementById('spInitiationAddButton').disabled=false;
								}
							});
							
							
}
					});
			
			
			
			
			
		}

	} else {
		document.getElementById("resultMessage123").innerHTML = "<font color=red>Fields  Should Not Be Empty.</font>";

	}
	return false;

}


function mytoggleStarOverlay(flag) {

	document.getElementById("resultMessage").value = '';
	var overlayYear = document.getElementById("overlayYear").value;
	document.getElementById("overlayYear").value = overlayYear;
	var overlayMonth = document.getElementById("overlayMonth").value;
	document.getElementById("overlayMonth").value = overlayMonth;
	document.getElementById("headerLabel").style.color = "white";
	document.getElementById("headerLabel").innerHTML = "Add Star Performers";
	var overlay = document.getElementById('staroverlay');
	var specialBox = document.getElementById('starspecialBox');

	overlay.style.opacity = .8;
	if (overlay.style.display == "block") {
		document.getElementById("eventForm").submit();
		overlay.style.display = "none";
		specialBox.style.display = "none";
	} else {
		overlay.style.display = "block";
		specialBox.style.display = "block";
	}
	if (flag != "add") {
		window.location = "starPerformance.action";
	}
}

/* Resource Adding */

function starPerformersList(Id, statusId) {
	window.location = "starPerformersList.action?Id=" + Id + "&statusId="
			+ statusId;
}

function dogetNomineesList() {

	document.getElementById('nomineeListMessage').innerHTML = "";

	var objectId = document.getElementById("Id").value;

	document.getElementById("reqloadMessageNomineeList").style.display = "block";
	var req = newXMLHttpRequest();
	req.onreadystatechange = readyStateHandlerTxt(req, displayNomineesList);

	var url = CONTENXT_PATH + "/dogetNomineesList.action?objectId=" + objectId;

	req.open("GET", url, "true");
	req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	req.send(null);
}

function readyStateHandlerTxt(req, responseTextHandler) {
	return function() {
		if (req.readyState == 4) {
			if (req.status == 200) {

				responseTextHandler(req.responseText);
				document.getElementById("reqloadMessageNomineeList").style.display = "none";
			} else {

				alert("HTTP error ---" + req.status + " : " + req.statusText);
			}
		} else {

		}
	}
}

function displayNomineesList(resText) {
	if (resText.length != 0) {
		var oTable = document.getElementById("tblStarPerformerNomineeList");

		clearTable(oTable);
		var starPerformerStatus = document
				.getElementById("starPerformerStatus").value;
		var starPerformerStatus = document
				.getElementById("starPerformerStatus").value;
		var statusId = document.getElementById("statusId").value;
		var objectId = document.getElementById("Id").value;
		var headerFields = new Array("SNo", "Resource Name", "Year", "Month",
				"Department", "Title", "Status", "CreatedBy", "Comments" , "All",  "Delete");

		tbody = document.createElement("TBODY");
		oTable.appendChild(tbody);

		var resTextSplit1 = resText.split("*@!");

		generateTableHeader1(tbody, headerFields, starPerformerStatus, statusId);
		count = resTextSplit1.length - 1;
		document.getElementById("count").value = parseInt(resTextSplit1.length - 1);
		for (var index = 0; index < resTextSplit1.length - 1; index++) {

			resTextSplit2 = resTextSplit1[index].split("#^$");

			generateRow1(tbody, resTextSplit2, index, starPerformerStatus,
					statusId, objectId);

		}
		generateFooter1(tbody, resTextSplit1[0].split("#^$")[1]);

	} else {
		alert("No Records Found");
		document.getElementById('nomineeListMessage').innerHTML = "<font size='2.5' color='red'>Nominees are not added</font>";
	}
}

function clearTable(tableId) {
	var tbl = tableId;
	var lastRow = tbl.rows.length;
	while (lastRow > 0) {
		tbl.deleteRow(lastRow - 1);
		lastRow = tbl.rows.length;
	}
}

function generateTableHeader1(tableBody, headerFields, starPerformerStatus,
		statusId) {
	var row;
	var cell;
	row = document.createElement("TR");
	row.className = "gridHeader";
	tableBody.appendChild(row);
	for (var i = 0; i < headerFields.length; i++) {
		if (i == (headerFields.length - 2)) {
			if (starPerformerStatus == 'true' && parseInt(statusId, 10) == 3) {
				cell = document.createElement("TD");
				var j = document.createElement("input");
				j.setAttribute("type", "checkbox");
				j.setAttribute("id", "Maincheckbox");
				j.setAttribute("cssFloat", "left");
				j.setAttribute("onclick", "javascript:selectAll()");
				var k = document.createElement("LABEL");
				k.setAttribute("value", "All");

				k.setAttribute("for", j);
				k.innerHTML = "isApproved";

				cell.appendChild(j);
				cell.appendChild(k);
				cell.setAttribute("width", "15%");
				cell.setAttribute('align', 'center');
				row.appendChild(cell);

			}
		} else if (i == (headerFields.length - 1)) {

			if (parseInt(statusId, 10) <= 2) {
				cell = document.createElement("TD");
				cell.className = "gridHeader";
				row.appendChild(cell);

				cell.setAttribute("width", "3%");
				cell.setAttribute('align', 'center');
				cell.innerHTML = headerFields[i];
			}
		} else {
			cell = document.createElement("TD");
			cell.className = "gridHeader";
			row.appendChild(cell);

			cell.setAttribute("width", "5%");
			cell.innerHTML = headerFields[i];
		}
	}
}


function generateRow1(tableBody, rowFeildsSplit, index, starPerformerStatus,
		statusId, objectId) {
	var row;
	var cell;
	row = document.createElement("TR");
	row.className = "gridRowEven";

	cell = document.createElement("TD");
	cell.className = "gridRowEven";
	cell.innerHTML = index + 1;
	cell.setAttribute("width", "5%");
	cell.setAttribute('align', 'left');
	row.appendChild(cell);

	cell = document.createElement("TD");
	cell.className = "gridRowEven";

	cell.appendChild(document.createTextNode(rowFeildsSplit[3]));
	row.appendChild(cell);
	cell.setAttribute("width", "15%");
	cell.setAttribute('align', 'left');

	cell = document.createElement("TD");
	cell.className = "gridRowEven";

	cell.appendChild(document.createTextNode(rowFeildsSplit[2]));
	row.appendChild(cell);
	cell.setAttribute("width", "10%");
	cell.setAttribute('align', 'left');

	cell = document.createElement("TD");
	cell.className = "gridRowEven";

	cell.appendChild(document.createTextNode(rowFeildsSplit[1]));
	row.appendChild(cell);
	cell.setAttribute("width", "10%");
	cell.setAttribute('align', 'left');

	cell = document.createElement("TD");
	cell.className = "gridRowEven";

	cell.appendChild(document.createTextNode(rowFeildsSplit[6]));
	row.appendChild(cell);
	cell.setAttribute("width", "10%");
	cell.setAttribute('align', 'left');
	cell = document.createElement("TD");
	cell.className = "gridRowEven";

	cell.appendChild(document.createTextNode(rowFeildsSplit[5]));
	row.appendChild(cell);
	cell.setAttribute("width", "10%");
	cell.setAttribute('align', 'left');

	cell = document.createElement("TD");
	cell.className = "gridRowEven";

	cell.appendChild(document.createTextNode(rowFeildsSplit[7]));
	row.appendChild(cell);
	cell.setAttribute("width", "10%");
	cell.setAttribute('align', 'left');

	cell = document.createElement("TD");
	cell.className = "gridRowEven";

	cell.appendChild(document.createTextNode(rowFeildsSplit[8]));
	row.appendChild(cell);
	cell.setAttribute("width", "10%");
	cell.setAttribute('align', 'left');
	
	cell = document.createElement("TD");
	cell.className = "gridColumn";
	var j = document.createElement("a");
	j.setAttribute("href", "javascript:starResourceComments('"+ rowFeildsSplit[4] + "' )");
    j.innerHTML = "View";
    document.create
	cell.appendChild(j);
	cell.setAttribute('align', 'center');
	row.appendChild(cell);

	if (starPerformerStatus == 'true' && parseInt(statusId, 10) == 3) {

		cell = document.createElement("TD");
		cell.className = "gridRowEven";
		var j = document.createElement("input");
		j.setAttribute("type", "checkbox");
		j.setAttribute("value", rowFeildsSplit[4]);
		j.setAttribute("id", "checkbox" + (index + 1));
		j.setAttribute("onclick", "javascript:SelectEach(checkbox"
				+ (index + 1) + ")");
		cell.appendChild(j);
		cell.setAttribute("width", "15%");
		cell.setAttribute('align', 'center');
		row.appendChild(cell);
	}

	else {

		if (parseInt(statusId, 10) <= 2) {
			cell = document.createElement("TD");
			cell.className = "gridColumn";
			var j = document.createElement("a");
			j.setAttribute("href", "javascript:starDelete('"
					+ rowFeildsSplit[4] + "','" + parseInt(statusId, 10)
					+ "','" + objectId + "')");

			j.innerHTML = "<i class='fa fa-trash-o' style='font-size:24px'></i>";

			document.create
			cell.appendChild(j);
			cell.setAttribute('align', 'center');
			row.appendChild(cell);
		}
	}

	tableBody.appendChild(row);
}

function generateFooter1(tbody, month) {

	var cell;
	var starPerformerStatus = document.getElementById("starPerformerStatus").value;
	var starPerformerAccess = document.getElementById("starPerformerAccess").value;
	var statusId = document.getElementById("statusId").value;

	var footer = document.createElement("TR");
	footer.className = "gridPager";
	tbody.appendChild(footer);
	cell = document.createElement("TD");
	cell.className = "gridFooter";

	if (starPerformerStatus == 'true' && parseInt(statusId, 10) == 3) {

		var j = document.createElement("input");
		j.setAttribute("type", "button");
		j.setAttribute("value", "Complete Approval Process");

		j.setAttribute("onclick", "javascript:getApproval('" + month + "')");
		j.setAttribute("id", "ADD");
		cell.appendChild(j);

		cell.colSpan = "11";

	} else if (starPerformerAccess == 'true' && parseInt(statusId, 10) <= 2) {
		var j = document.createElement("input");
		j.setAttribute("type", "button");
		j.setAttribute("value", "Submit for Approval");

		j.setAttribute("onclick", "javascript:doSubmitForApproval()");
		j.setAttribute("id", "SubmitForApprovalBTN");
		cell.appendChild(j);


		cell.colSpan = "11";
	}

	cell.setAttribute('align', 'center');

	footer.appendChild(cell);
}

function selectAll() {

	var check = "checkbox";

	var count = parseInt(document.getElementById("count").value);
	for (var i = 1; i <= count; i++) {
		var vall = check + i;

		if (document.getElementById("Maincheckbox").checked) {
			document.getElementById(vall).checked = true;
		} else {
			document.getElementById(vall).checked = false;
		}

	}
}

function SelectEach(ele) {
	var count = parseInt(document.getElementById("count").value);
	if (!ele.checked) {

		document.getElementById("Maincheckbox").checked = false;
	} else {
		var check = "checkbox";
		var count1 = 0;
		for (var i = 1; i <= count; i++) {
			var vall = check + i;

			if (document.getElementById(vall).checked) {
				count1 = count1 + 1;
			}

		}
		if (count1 == count) {

			document.getElementById("Maincheckbox").checked = true;
		} else {

			document.getElementById("Maincheckbox").checked = false;

		}
	}

}
function getApproval(month) {
	
	var check = "checkbox";
	var nomineeIds = "";

	var count = parseInt(document.getElementById("count").value);

	for (var i = 1; i <= count; i++) {
		var vall = check + i;

		if (document.getElementById(vall).checked) {
			var data = document.getElementById(vall).value;
			nomineeIds += data + ",";

		}
	}

	var objectId = document.getElementById("Id").value;

var selectedSize=nomineeIds.split(",").length;

	if (nomineeIds.length > 0 && nomineeIds != "") {

		if (parseInt(selectedSize,10) > 30) {

			document.getElementById('nomineeListMessage').innerHTML = "<font color='red'>Maximum 30 members can be approved</font>";
		} else {

			
			x0p(' Do you want to confirm selected candidates!', '', 'warning',
					function(button) {
				
						if (button == 'warning') {
							result = true;
							// alert(button)
						}
						if (button == 'cancel') {
							result = false;
						}
						if (result) {
							
							document.getElementById("ADD").disabled = true;
							window.location = "doNomineeApproval.action?nomineeIds="
									+ nomineeIds + "&id=" + objectId + "&starMonth="
									+ month + "";
							
							
}
					});
			
			
			/* if (confirm("Do you want to confirm selected candidates!")) {
				document.getElementById("ADD").disabled = true;
				window.location = "doNomineeApproval.action?nomineeIds="
						+ nomineeIds + "&id=" + objectId + "&starMonth="
						+ month + "";
			} */
		}

	} else {

		document.getElementById('nomineeListMessage').innerHTML = "<font color='red'>No employee selected for approval</font>";
	}
}
function doSubmitForApproval() {
	var objectId = document.getElementById("Id").value;
     
	x0p(' Do you want to Submit!', '', 'warning',
			function(button) {
		
				if (button == 'warning') {
					result = true;
					// alert(button)
				}
				if (button == 'cancel') {
					result = false;
				}
				if (result) {
					document.getElementById("SubmitForApprovalBTN").disabled = true;
					document.getElementById('nomineeListMessage').innerHTML = "";
                	document.getElementById("reqloadMessageNomineeList").style.display = "block";
					var req = newXMLHttpRequest();
					req.onreadystatechange = readyStateHandlerTxt(req, displaySubmitForApproval);

					var url = CONTENXT_PATH + "/doSubmitForApproval.action?objId=" + objectId;

					req.open("GET", url, "true");
					req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
					req.send(null);
					
					
}
			});

}
function displaySubmitForApproval(restxt) {
	document.getElementById('nomineeListMessage').innerHTML = restxt;

	hideRow("resoureceFld");
	hideRow("resoureceCommentsTr");
	
	hideRow("resoureceBtn");

	document.getElementById("SubmitForApprovalBTN").disabled = true;
}
function starDelete(starId, statusId, objectId) {
	window.location = "starPerformerDelete.action?starId=" + starId
			+ "&statusId=" + statusId + "&objectId=" + objectId;
}

function hideRow(id) {
	var row = document.getElementById(id);
	row.style.display = 'none';
}

function EmployeeForStarPerformer() {
	var test = document.getElementById("resourceName1").value;

	if (test == "") {

		clearTable1();
		hideScrollBar();
		var validationMessage = document
				.getElementById("authorEmpValidationMessage1");
		validationMessage.innerHTML = "";
		document.eventForm.preAssignEmpId.value = "";
	} else {

		if (test.length > 2) {
			var url = CONTENXT_PATH
					+ "/getEmployeeDetailStarList.action?customerName="
					+ escape(test);
			var req = initRequest(url);
			req.onreadystatechange = function() {
				if (req.readyState == 4) {
					if (req.status == 200) {
						// alert("req.responseXML"+req.responseXML);
						parseEmpMessagesForProject(req.responseXML);
					} else if (req.status == 204) {
						clearTable1();
					}
				}
			};
			req.open("GET", url, true);

			req.send(null);
		}
	}
}

function clearTable1() {
	if (completeTable) {
		completeTable.setAttribute("bordercolor", "white");
		completeTable.setAttribute("border", "0");
		completeTable.style.visible = false;
		for (loop = completeTable.childNodes.length - 1; loop >= 0; loop--) {
			completeTable.removeChild(completeTable.childNodes[loop]);
		}
	}
}

function hideScrollBar() {
	autorow = document.getElementById("menu-popup");
	autorow.style.display = 'none';
}

function parseEmpMessagesForProject(responseXML) {
	// alert("-->"+responseXML);
	autorow1 = document.getElementById("menu-popup");
	autorow1.style.display = "none";
	autorow = document.getElementById("menu-popup");
	autorow.style.display = "none";
	clearTable1();
	var employees = responseXML.getElementsByTagName("EMPLOYEES")[0];
	if (employees.childNodes.length > 0) {
		completeTable.setAttribute("bordercolor", "black");
		completeTable.setAttribute("border", "0");
	} else {
		clearTable1();
	}
	if (employees.childNodes.length < 10) {
		autorow1.style.overflowY = "hidden";
		autorow.style.overflowY = "hidden";
	} else {
		autorow1.style.overflowY = "scroll";
		autorow.style.overflowY = "scroll";
	}

	var employee = employees.childNodes[0];
	var chk = employee.getElementsByTagName("VALID")[0];
	if (chk.childNodes[0].nodeValue == "true") {
		// var validationMessage=document.getElementById("validationMessage");
		var validationMessage;

		validationMessage = document
				.getElementById("authorEmpValidationMessage1");
		isPriEmpExist = true;

		validationMessage.innerHTML = "";
		document.getElementById("menu-popup").style.display = "block";
		for (loop = 0; loop < employees.childNodes.length; loop++) {

			var employee = employees.childNodes[loop];
			var customerName = employee.getElementsByTagName("NAME")[0];
			var empid = employee.getElementsByTagName("EMPID")[0];
			appendEmployeeForProject(empid.childNodes[0].nodeValue,
					customerName.childNodes[0].nodeValue);
		}
		var position;

		position = findPosition(document.getElementById("resourceName1"));
		// alert(position);
		var posi = position.split(",");
		document.getElementById("menu-popup").style.left = posi[0] + "px";
		document.getElementById("menu-popup").style.top = (parseInt(posi[1]) + 20)
				+ "px";
		document.getElementById("menu-popup").style.display = "block";
	} // if
	if (chk.childNodes[0].nodeValue == "false") {
		var validationMessage = '';

		isPriEmpExist = false;
		validationMessage = document
				.getElementById("authorEmpValidationMessage1");

		validationMessage.innerHTML = " Invalid ! Select from Suggesstion List. ";
		validationMessage.style.color = "green";
		validationMessage.style.fontSize = "12px";

		document.getElementById("resourceName1").value = "";
		document.getElementById("preAssignEmpId1").value = "";

	}
}

function initRequest(url) {

	if (window.XMLHttpRequest) {
		return new XMLHttpRequest();
	} else if (window.ActiveXObject) {
		isIE = true;
		return new ActiveXObject("Microsoft.XMLHTTP");
	}

}
function set_emp(eName, eID) {
	clearTable1();
	document.starPerformance.resourceName1.value = eName;
	document.starPerformance.preAssignEmpId1.value = eID;

}
var isIE;

function appendEmployeeForProject(empId, empName) {

	var row;
	var nameCell;
	if (!isIE) {
		row = completeTable.insertRow(completeTable.rows.length);
		nameCell = row.insertCell(0);
	} else {
		row = document.createElement("tr");
		nameCell = document.createElement("td");
		row.appendChild(nameCell);
		completeTable.appendChild(row);
	}
	row.className = "popupRow";
	nameCell.setAttribute("bgcolor", "#3E93D4");
	var linkElement = document.createElement("a");
	linkElement.className = "popupItem";
	linkElement.setAttribute("href", "javascript:set_emp('" + empName + "','"
			+ empId + "')");

	linkElement.appendChild(document.createTextNode(empName));
	linkElement["onclick"] = new Function("hideScrollBar()");
	nameCell.appendChild(linkElement);
}
function findPosition(oElement) {
	if (typeof (oElement.offsetParent) != undefined) {
		for (var posX = 0, posY = 0; oElement; oElement = oElement.offsetParent) {
			posX += oElement.offsetLeft;
			posY += oElement.offsetTop;
		}
		return posX + "," + posY;
	} else {
		return oElement.x + "," + oElement.y;
	}
}

// campaign integration
function showRow(id) {
	var row = document.getElementById(id);
	row.style.display = 'block';
}

function publish(objectId, month, year, status) {
	document.getElementById("displayMessagePublish").innerHTML ="";
	var currentStatus = 5;

	if (status >= currentStatus) {
	    document.getElementById("displayMessagePublish").style.color = "green";
		document.getElementById("displayMessagePublish").innerHTML = "sorry! it is already published";
	}
	if (status < (currentStatus - 1)) {
        document.getElementById("displayMessagePublish").style.color = "green";
		document.getElementById("displayMessagePublish").innerHTML = "plz get the status to be approved";
	}
	if (status == currentStatus - 1) {

	
		x0p(' Do you want to Publish!', '', 'warning',
				function(button) {
			
					if (button == 'warning') {
						result = true;
						// alert(button)
					}
					if (button == 'cancel') {
						result = false;
					}
					if (result) {
						transperent();
						window.location= 'doSMCampaignIntegration.action?objectId='+objectId+"&monthName="+month+"&year="+year;
					}
				});
		
	
	}

}

function toggleOverlayForCampaignSchdule() {
	document.getElementById("startScheduleDate").value = "";
	document.getElementById("resultMessage12").innerHTML = "";
	var overlayForCampaignSchedule = document
			.getElementById('overlayForCampaignSchedule');
	var specialBoxForCampaignSchedule = document
			.getElementById('specialBoxForCampaignSchedule');

	overlayForCampaignSchedule.style.opacity = .8;
	if (overlayForCampaignSchedule.style.display == "block") {
		overlayForCampaignSchedule.style.display = "none";
		specialBoxForCampaignSchedule.style.display = "none";
		document.getElementById("eventForm1").submit();

	} else {
		overlayForCampaignSchedule.style.display = "block";
		specialBoxForCampaignSchedule.style.display = "block";
	}

}

function doSMCampaignAdd() {

	document.getElementById("resultMessage12").innerHTML = "";

	var objectId = document.getElementById("objectId").value;
	var CCmonth = document.getElementById("CCmonth").value;
	var CCyear = document.getElementById("CCyear").value;
	var campaignStartDate = document.getElementById("startScheduleDate").value;
	var currentDate = document.getElementById("currentDate").value;
	if (campaignStartDate != "" && campaignStartDate != null) {
		if (validateSheduleDate(currentDate, campaignStartDate)) {
			document.getElementById("CampaignAddButton").disabled = true;

			document.getElementById("lodingg").style.display = 'block';

			var req = newXMLHttpRequest();
			req.onreadystatechange = readyStateHandlerT(req,
					displaydoSMCampaignSchedual);

			var url = CONTENXT_PATH
					+ "/doSMCampaignIntegration.action?objectId=" + objectId
					+ "&campaignStartDate=" + campaignStartDate + "&month="
					+ CCmonth + "&year=" + CCyear + "";

			req.open("GET", url, "true");
			req.setRequestHeader("Content-Type",
					"application/x-www-form-urlencoded");
			req.send(null);

		}
	} else {
		alert("Please enter all the fields");
	}

}

function displaydoSMCampaignSchedual(result) {

	document.getElementById("lodingg").style.display = "none";
	document.getElementById("resultMessage12").innerHTML = result;
}

function SurveyFormRsvp(objectId, month, year, status, surveyId) {

	var currentStatus = 6;

	if (status == currentStatus) {
		window.location = "../marketing/surveyform/dogetStarPerformerSurveyForm.action?surveyId="
				+ surveyId + "&starId=" + objectId;

	}
	if (status > currentStatus) {
		hideRow('expiryDateRow');
		hideRow('movieDatesRow');
		hideRow('movieNamesRow');

		hideRow('addSurveyFormRow');
		document.getElementById("headerLabel234").style.color = "white";
		document.getElementById("headerLabel234").innerHTML = "Survey Form";

		document.getElementById("objectIdSurvy").value = objectId;
		document.getElementById("Survymonth").value = month;
		document.getElementById("Survyyear").value = year;

		var overlayForCampaignSchedule = document
				.getElementById('overlayForSurveyForms');

		var specialBoxForCampaignSchedule = document
				.getElementById('specialBoxForSurveyForms');

		document.getElementById("alertMessageSurveyForm").style.color = "green";
		document.getElementById("alertMessageSurveyForm").innerHTML = "sorry! the survey form has been generated already";

		overlayForCampaignSchedule.style.opacity = .8;
		if (overlayForCampaignSchedule.style.display == "block") {
			overlayForCampaignSchedule.style.display = "none";
			specialBoxForCampaignSchedule.style.display = "none";

		} else {
			overlayForCampaignSchedule.style.display = "block";
			specialBoxForCampaignSchedule.style.display = "block";
		}
	}
	if (status < (currentStatus - 1)) {
		hideRow('expiryDateRow');
		hideRow('movieDatesRow');
		hideRow('movieNamesRow');

		// showRow('emailTemplete');
		hideRow('addSurveyFormRow');
		document.getElementById("headerLabel234").style.color = "white";
		document.getElementById("headerLabel234").innerHTML = "Survey Form";

		document.getElementById("objectIdSurvy").value = objectId;
		document.getElementById("Survymonth").value = month;
		document.getElementById("Survyyear").value = year;

		var overlayForCampaignSchedule = document
				.getElementById('overlayForSurveyForms');

		var specialBoxForCampaignSchedule = document
				.getElementById('specialBoxForSurveyForms');

		document.getElementById("alertMessageSurveyForm").style.color = "green";
		document.getElementById("alertMessageSurveyForm").innerHTML = "plz get the status to be published";

		overlayForCampaignSchedule.style.opacity = .8;
		if (overlayForCampaignSchedule.style.display == "block") {
			overlayForCampaignSchedule.style.display = "none";
			specialBoxForCampaignSchedule.style.display = "none";

		} else {
			overlayForCampaignSchedule.style.display = "block";
			specialBoxForCampaignSchedule.style.display = "block";
		}
	}
	if (status == currentStatus - 1) {
		document.getElementById("headerLabel234").style.color = "white";
		document.getElementById("headerLabel234").innerHTML = "Survey Form";

		document.getElementById("objectIdSurvy").value = objectId;
		document.getElementById("Survymonth").value = month;
		document.getElementById("Survyyear").value = year;

		var overlayForCampaignSchedule = document
				.getElementById('overlayForSurveyForms');

		var specialBoxForCampaignSchedule = document
				.getElementById('specialBoxForSurveyForms');

		overlayForCampaignSchedule.style.opacity = .8;
		if (overlayForCampaignSchedule.style.display == "block") {
			overlayForCampaignSchedule.style.display = "none";
			specialBoxForCampaignSchedule.style.display = "none";

		} else {
			overlayForCampaignSchedule.style.display = "block";
			specialBoxForCampaignSchedule.style.display = "block";
		}
	}

}

function toggleOverlayForSurveyForm() {

	document.getElementById("survyresultMessage").innerHTML = "";
	var overlayForCampaignSchedule = document
			.getElementById('overlayForSurveyForms');
	var specialBoxForCampaignSchedule = document
			.getElementById('specialBoxForSurveyForms');

	overlayForCampaignSchedule.style.opacity = .8;
	if (overlayForCampaignSchedule.style.display == "block") {
		overlayForCampaignSchedule.style.display = "none";
		specialBoxForCampaignSchedule.style.display = "none";
		document.getElementById("surveyForm").submit();

	} else {
		overlayForCampaignSchedule.style.display = "block";
		specialBoxForCampaignSchedule.style.display = "block";
	}

}

function doAddSurveyFormAndQuestionnaire() {

	document.getElementById("survyresultMessage").innerHTML = "";

	var objectId = document.getElementById("objectIdSurvy").value;
	var CCmonth = document.getElementById("Survymonth").value;
	var CCyear = document.getElementById("Survyyear").value;
	var expireDate = document.getElementById("expireDate").value;
	var movieDates = document.getElementById("movieDates").value;
	var movieName = document.getElementById("movieName").value;
	var currentDate = document.getElementById("currentDate").value;
	if (expireDate != "" && expireDate != null && movieName != ""
			&& movieName != null && movieDates != "" && movieDates != null) {
		if (compareDates(currentDate, expireDate)) {

	        
			
			x0p(' Do you want to confirm the survey form!', '', 'warning',
					function(button) {
				
						if (button == 'warning') {
							result = true;
							// alert(button)
						}
						if (button == 'cancel') {
							result = false;
						}
						if (result) {
							
							document.getElementById("sfAddButton").disabled = true;
							document.getElementById("lodingsurvy").style.display = 'block';
							
						    var req = newXMLHttpRequest();
							req.onreadystatechange = readyStateHandlerT(req,
									displaydoSurveyFormAndQuestionnaire);

							var url = CONTENXT_PATH
									+ "/doAddSurveyFormAndQuestionnaire.action?objectId="
									+ objectId + "&month=" + CCmonth + "&year=" + CCyear
									+ "&expireDate=" + expireDate + "&movieDates=" + movieDates
									+ "&movieName=" + movieName + "";

							req.open("GET", url, "true");
							req.setRequestHeader("Content-Type",
									"application/x-www-form-urlencoded");
							req.send(null);
							
							
							
}
					});
			
		}
	} else {

		alert("Please Enter All the fields in proper manner")
	}

}

function displaydoSurveyFormAndQuestionnaire(result) {

	document.getElementById("lodingsurvy").style.display = "none";
	document.getElementById("survyresultMessage").innerHTML = result;
	document.getElementById("sfAddButton").style.display = 'none';
}

function movieTicketsUpload(objectId, suvyId, status) {

	window.location = "doStarPerformerMvTickets.action?id=" + objectId
			+ "&suvyId=" + suvyId + "&statusId=" + status;
}

function getEmpMovieTicketList() {

	document.getElementById("loadMessageMvTickets").innerHTML = "";

	document.getElementById("downLoadAllTickets").style.display = "none";
	var id = document.getElementById("id").value;
	var statusId = document.getElementById("statusId").value;
	var suvyId = document.getElementById("suvyId").value;

	document.getElementById("reqloadMessageMvTickets").style.display = "block";
	var req = newXMLHttpRequest();
	req.onreadystatechange = readyStateHandlerTxt1(req, displayMessageMvTickets);

	var url = CONTENXT_PATH + "/getEmpMovieTicketList.action?objectId=" + id
			+ "&survyId=" + suvyId + "&statusId=" + statusId;
	// alert(url);

	req.open("GET", url, "true");
	req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	req.send(null);
}

function readyStateHandlerTxt1(req, responseTextHandler) {
	return function() {
		if (req.readyState == 4) {
			if (req.status == 200) {
				// alert(req.responseText);
				responseTextHandler(req.responseText);
				document.getElementById("reqloadMessageMvTickets").style.display = "none";

			} else {

				alert("HTTP error ---" + req.status + " : " + req.statusText);
			}
		} else {

		}
	}
}

function displayMessageMvTickets(resText) {
	document.getElementById("reqloadMessageMvTickets").style.display = "none";

	if (resText.length != 0) {
		var oTable = document.getElementById("tblStarPerformerMvTicketsList");

		clearTable(oTable);

		var headerFields = new Array("SNo", "Emp Name", "Emp Email",
				"Upload Ticket", "SendEmail", "Download","SocialTile");

		tbody = document.createElement("TBODY");
		oTable.appendChild(tbody);

		var resTextSplit1 = resText.split("*@!");

		generateTableHeader22(tbody, headerFields);

		for (var index = 0; index < resTextSplit1.length - 1; index++) {

			resTextSplit2 = resTextSplit1[index].split("#^$");

			generateRow22(tbody, resTextSplit2, index);

		}
		generateFooter22(tbody);
		document.getElementById("downLoadAllTickets").style.display = "block";
	} else {
		alert("No Records Found");
		document.getElementById('loadMessageMvTickets').innerHTML = "<font size='2.5' color='red'>No response</font>";
	}
}

function generateTableHeader22(tableBody, headerFields) {
	var row;
	var cell;
	row = document.createElement("TR");
	row.className = "gridHeader";
	tableBody.appendChild(row);
	for (var i = 0; i < headerFields.length; i++) {

		cell = document.createElement("TD");
		cell.className = "gridHeader";
		row.appendChild(cell);

		cell.setAttribute("width", "5%");
		cell.innerHTML = headerFields[i];
	}

}

function generateRow22(tableBody, rowFeildsSplit, index) {
	
	
	
	var row;
	var cell;
	row = document.createElement("TR");
	row.className = "gridRowEven";

	cell = document.createElement("TD");
	cell.className = "gridRowEven";
	cell.innerHTML = index + 1;
	cell.setAttribute("width", "5%");
	cell.setAttribute('align', 'left');
	row.appendChild(cell);

	cell = document.createElement("TD");
	cell.className = "gridRowEven";
	cell.appendChild(document.createTextNode(rowFeildsSplit[3]));
	row.appendChild(cell);
	cell.setAttribute("width", "15%");
	cell.setAttribute('align', 'left');

	cell = document.createElement("TD");
	cell.className = "gridRowEven";
	cell.appendChild(document.createTextNode(rowFeildsSplit[5]));
	row.appendChild(cell);
	cell.setAttribute("width", "10%");
	cell.setAttribute('align', 'left');

	cell = document.createElement("TD");
	cell.className = "gridColumn";
	cell.innerHTML = "<a herf='#' class='round-button1' title='MovieTicket Upload'  onclick=\"ticketsUpload('"
			+ rowFeildsSplit[1]
			+ "','"
			+ rowFeildsSplit[4]
			+ "','"
			+ rowFeildsSplit[2]
			+ "','"
			+ rowFeildsSplit[7]
			+ "')\">MovieTicket</a>&nbsp;";
	cell.setAttribute('align', 'center');
	row.appendChild(cell);
	
	 
	if (rowFeildsSplit[6] != "") {
		if(rowFeildsSplit[8]==1){
			cell = document.createElement("TD");
			cell.className = "gridColumn";
		//	cell.innerHTML = "<a  class='round-button1' title='Mail had already been sent' href='javascript:void(0)' style='cursor: default;background-color:green;'>Send</a>&nbsp;";
			cell.innerHTML = "<a herf='#' class='round-button1' title='Mail had already been sent' style='cursor: default;background-color:green;' onclick=\"reSendMovieTicket('"
				+ rowFeildsSplit[1]
				+ "','"
				+ rowFeildsSplit[4]
				+ "','"
				+ rowFeildsSplit[2]
				+ "','"
				+ rowFeildsSplit[7]
				+ "')\">Send</a>&nbsp;"
			
			//cell.innerHTML = "<a  class='round-button1' title='Mail had already been sent' style='cursor: default;background-color:green;' href='javascript:reSendMovieTicket(\"" + rowFeildsSplit[1] + "\",\"" + rowFeildsSplit[4] + "\",\"" + rowFeildsSplit[2] + "\",\"" + rowFeildsSplit[7] + "\",\"" + rowFeildsSplit[3] + "\",\"" + rowFeildsSplit[9] + "\",\"" + rowFeildsSplit[10] + "\",\"" + rowFeildsSplit[11] + "\",\"" + rowFeildsSplit[5] + "\",\"" + rowFeildsSplit[12] + "\",\"" + rowFeildsSplit[13] + "\",\"" + rowFeildsSplit[14] + "\")'>Send</a>";
			cell.setAttribute('align', 'center');
			row.appendChild(cell);
		}else{
		cell = document.createElement("TD");
		cell.className = "gridColumn";
		cell.innerHTML = "<a herf='#' class='round-button1' title='MovieTicket Upload'  onclick=\"reSendMovieTicket('"
				+ rowFeildsSplit[1]
				+ "','"
				+ rowFeildsSplit[4]
				+ "','"
				+ rowFeildsSplit[2]
				+ "','"
				+ rowFeildsSplit[7]
				+ "')\">Send</a>&nbsp;";
		
		//cell.innerHTML = "<a class='round-button1' title='MovieTicket Upload' href='javascript:reSendMovieTicket(\"" + rowFeildsSplit[1] + "\",\"" + rowFeildsSplit[4] + "\",\"" + rowFeildsSplit[2] + "\",\"" + rowFeildsSplit[7] + "\",\"" + rowFeildsSplit[3] + "\",\"" + rowFeildsSplit[9] + "\",\"" + rowFeildsSplit[10] + "\",\"" + rowFeildsSplit[11] + "\",\"" + rowFeildsSplit[5] + "\",\"" + rowFeildsSplit[12] + "\",\"" + rowFeildsSplit[13] + "\",\"" + rowFeildsSplit[14] + "\")'>Send</a>";
		cell.setAttribute('align', 'center');
		row.appendChild(cell);
		}
	} else {
		cell = document.createElement("TD");
		cell.className = "gridColumn";
		cell.innerHTML = "";
		cell.setAttribute('align', 'center');
		row.appendChild(cell);

	}
	if (rowFeildsSplit[6] != "") {
		cell = document.createElement("TD");
		cell.className = "gridColumn";
		cell.innerHTML = '<a href="javascript:downloadStarPerformerTicket('
				+ rowFeildsSplit[1]
				+ ')"><img width="18" height="14" border="0" alt="download" src="../includes/images/download_11x10.jpg" title="Click here to download"></a>';
		cell.setAttribute('align', 'center');
		row.appendChild(cell);
	}
	
	else {
		cell = document.createElement("TD");
		cell.className = "gridColumn";
		cell.innerHTML = "";
		cell.setAttribute('align', 'center');
		row.appendChild(cell);

	}
		cell = document.createElement("TD");
		cell.className = "gridColumn";
		
		cell.innerHTML = "<a href='javascript:downloadStarPerformerSocialTile(\"" + rowFeildsSplit[1] + "\",\"" + rowFeildsSplit[2] + "\",\"" + rowFeildsSplit[4] + "\")'><img width='18' height='14' border='0' alt='download' src='../includes/images/download_11x10.jpg' title='Click here to download'></a>";
		
		cell.setAttribute('align', 'center');
		row.appendChild(cell);

	tableBody.appendChild(row);
}
function generateFooter22(tbody) {

	var 
	cell;

	var footer = document.createElement("TR");
	footer.className = "gridPager";
	tbody.appendChild(footer);
	cell = document.createElement("TD");
	cell.className = "gridFooter";

	cell.colSpan = "7";

	footer.appendChild(cell);
}

function ticketsUpload(starId, EmpId, objectId, survyId) {

	document.getElementById("remainderHeaderLabelForMovieTickets").style.color = "white";
	document.getElementById("remainderHeaderLabelForMovieTickets").innerHTML = "Movie Tckets";

	document.getElementById("forMovieTicketsObjectId").value = objectId;
	// alert(starId);
	document.getElementById("forMovieTicketsStarId").value = starId;
	document.getElementById("forMovieTicketsEmpId").value = EmpId;
	document.getElementById("forMoviesurvyId").value = survyId;

	var overlayForCampaignSchedule = document
			.getElementById('overlayForMovieTickets');

	var specialBoxForCampaignSchedule = document
			.getElementById('specialBoxForMovieTickets');

	overlayForCampaignSchedule.style.opacity = .8;
	if (overlayForCampaignSchedule.style.display == "block") {
		overlayForCampaignSchedule.style.display = "none";
		specialBoxForCampaignSchedule.style.display = "none";

	} else {
		overlayForCampaignSchedule.style.display = "block";
		specialBoxForCampaignSchedule.style.display = "block";
	}

}

function reSendMovieTicket(starId, EmpId, objectId, suvyId) {
	
	window.location = "reSendStarperformerTicket.action?starId=" + starId
			+ "&objectId=" + objectId + "&suvyId=" + suvyId + "&EmpId=" + EmpId;
}

function downloadStarPerformerTicket(starId) {

	// alert(starId);
	window.location = "downloadStarPerformerTicket.action?starId=" + starId;
}


function downloadStarPerformerSocialTile(starId,objectId,EmpId) {

	//alert("starId"+starId);
	 
	window.location = "downloadStarPerformerSocialTile.action?starId=" +starId +"&EmpId=" + EmpId+ "&objectId=" + objectId;
	
}


function Feedback(objectId, survyId, month, year, status) {

	var currentStatus = 7;
	if (status >= currentStatus) {
		hideRow('feedbackExpiryDateRow');
		hideRow('feedbackAddRow');

		document.getElementById("headerLabelfeedback").style.color = "white";
		document.getElementById("headerLabelfeedback").innerHTML = "Feedback Form";

		document.getElementById("objectIdfeedBack").value = objectId;
		document.getElementById("feedBackmonth").value = month;
		document.getElementById("feedBackyear").value = year;
		document.getElementById("survyId").value = survyId;

		var overlayForCampaignSchedule = document
				.getElementById('overlayForFeedBackForms');

		var specialBoxForCampaignSchedule = document
				.getElementById('specialBoxForFeedBackForms');

		document.getElementById("alertMessageFeedback").style.color = "green";
		document.getElementById("alertMessageFeedback").innerHTML = "Sorry! your feedback has been generated already";

		overlayForCampaignSchedule.style.opacity = .8;
		if (overlayForCampaignSchedule.style.display == "block") {
			overlayForCampaignSchedule.style.display = "none";
			specialBoxForCampaignSchedule.style.display = "none";

		} else {
			overlayForCampaignSchedule.style.display = "block";
			specialBoxForCampaignSchedule.style.display = "block";
		}
	}
	if (status < (currentStatus - 1)) {
		hideRow('feedbackExpiryDateRow');
		hideRow('feedbackAddRow');
		document.getElementById("headerLabelfeedback").style.color = "white";
		document.getElementById("headerLabelfeedback").innerHTML = "Feedback Form";

		document.getElementById("objectIdfeedBack").value = objectId;
		document.getElementById("feedBackmonth").value = month;
		document.getElementById("feedBackyear").value = year;
		document.getElementById("survyId").value = survyId;

		var overlayForCampaignSchedule = document
				.getElementById('overlayForFeedBackForms');

		var specialBoxForCampaignSchedule = document
				.getElementById('specialBoxForFeedBackForms');

		document.getElementById("alertMessageFeedback").style.color = "green";
		document.getElementById("alertMessageFeedback").innerHTML = "plz fill the RSVP form";

		overlayForCampaignSchedule.style.opacity = .8;
		if (overlayForCampaignSchedule.style.display == "block") {
			overlayForCampaignSchedule.style.display = "none";
			specialBoxForCampaignSchedule.style.display = "none";

		} else {
			overlayForCampaignSchedule.style.display = "block";
			specialBoxForCampaignSchedule.style.display = "block";
		}
	}
	if (status == currentStatus - 1) {
		document.getElementById("headerLabelfeedback").style.color = "white";
		document.getElementById("headerLabelfeedback").innerHTML = "Feedback Form";

		document.getElementById("objectIdfeedBack").value = objectId;
		document.getElementById("feedBackmonth").value = month;
		document.getElementById("feedBackyear").value = year;
		document.getElementById("survyId").value = survyId;

		var overlayForCampaignSchedule = document
				.getElementById('overlayForFeedBackForms');

		var specialBoxForCampaignSchedule = document
				.getElementById('specialBoxForFeedBackForms');

		overlayForCampaignSchedule.style.opacity = .8;
		if (overlayForCampaignSchedule.style.display == "block") {
			overlayForCampaignSchedule.style.display = "none";
			specialBoxForCampaignSchedule.style.display = "none";

		} else {
			overlayForCampaignSchedule.style.display = "block";
			specialBoxForCampaignSchedule.style.display = "block";
		}
	}

}

function toggleOverlayForFeedbackForm() {

	document.getElementById("feedBackresultMessage").innerHTML = "";
	var overlayForCampaignSchedule = document
			.getElementById('overlayForFeedBackForms');
	var specialBoxForCampaignSchedule = document
			.getElementById('specialBoxForFeedBackForms');

	overlayForCampaignSchedule.style.opacity = .8;
	if (overlayForCampaignSchedule.style.display == "block") {
		overlayForCampaignSchedule.style.display = "none";
		specialBoxForCampaignSchedule.style.display = "none";
		document.getElementById("feedBackForm").submit();

	} else {
		overlayForCampaignSchedule.style.display = "block";
		specialBoxForCampaignSchedule.style.display = "block";
	}

}

function doAddSurveyfeedBack() {

	document.getElementById("feedBackresultMessage").innerHTML = "";

	var objectId = document.getElementById("objectIdfeedBack").value;
	var CCmonth = document.getElementById("feedBackmonth").value;
	var CCyear = document.getElementById("feedBackyear").value;
	var expireDate = document.getElementById("expireFeedbackDate").value;
	var currentDate = document.getElementById("currentDate").value;
	var survyId = document.getElementById("survyId").value;
	if (expireDate != "" && expireDate != null) {
		if (compareDates(currentDate, expireDate)) {

			document.getElementById("feedBackbuttonBg").disabled = true;
			document.getElementById("lodingfeedBack").style.display = 'block';
			var req = newXMLHttpRequest();
			req.onreadystatechange = readyStateHandlerT(req,
					displaydoSurveyFeedbackForm);

			var url = CONTENXT_PATH + "/doAddSurveyfeedBack.action?objectId="
					+ objectId + "&month=" + CCmonth + "&year=" + CCyear
					+ "&expireDate=" + expireDate + "&survyId=" + survyId + "";
			// alert(url);

			req.open("GET", url, "true");
			req.setRequestHeader("Content-Type",
					"application/x-www-form-urlencoded");
			req.send(null);
			// 
			//    
		}
	} else {

		alert("Please Enter expireDate")
	}
}

function displaydoSurveyFeedbackForm(result) {
	document.getElementById("feedBackbuttonBg").style.display = 'none';
	document.getElementById("lodingfeedBack").style.display = "none";
	document.getElementById("feedBackresultMessage").innerHTML = result;
}

function isNumericYear(element) {

	var val = element;
	if (isNaN(val)) {
		alert('Year must be numeric values');

		return false;
	} else {
		if (val.length != 4) {
			alert('Please Enter Year');

			return false;
		} else {
			return true;
		}
	}
}

function compareDates(curr, expir) {
	var startDate = curr;
	var endDate = expir;
	var sDate = new Date(curr);
	var eDate = new Date(expir);
	// alert(sDate);
	// alert(eDate);
	if (sDate < eDate) {
		return true;

	} else {
		alert("Selected Date must Greater Than Current Date");
		return false;
	}

};
function validateSheduleDate(curr, expir) {
	var startDate = curr;
	var endDate = expir;
	var sDate = new Date(curr);
	var eDate = new Date(expir);

	if (sDate < eDate) {
		var difference = eDate.getTime() - sDate.getTime(); // This will give
															// difference in
															// milliseconds
		var resultInMinutes = Math.round(difference / 60000);
		if (parseInt(resultInMinutes, 10) < 30) {
			alert("Scheduled date time must be greater than 30 minutes from current date time!");
			return false;
		} else {
			return true;
		}

	} else {
		alert("Selected Date must Greater Than Current Date");
		return false;
	}

};



function starResourceCommentsPoPUp(comments) {
    var background = "#3E93D4";
    var title = "Comments";
    // var text1 = text; 
    // var size = text1.length;
      if(comments=='null' || comments==''){
    	  comments="-"; 
      }
    var size = comments.length;
    
    //Now create the HTML code that is required to make the popup
    var content = "<html><head><title>"+title+"</title></head>\
    <body bgcolor='"+background +"' style='color:white;'><h4>"+title+"</h4>"+comments+"<br />\
    </body></html>";
    
    if(size < 50){
        //Create the popup       
        popup = window.open("","window","channelmode=0,width=300,height=150,top=200,left=350,scrollbars=no,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no");    
        popup.document.write(content); //Write content into it.    
    }
    
    else if(size < 100){
        //Create the popup       
        popup = window.open("","window","channelmode=0,width=400,height=200,top=200,left=350,scrollbars=no,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no");    
        popup.document.write(content); //Write content into it.    
    }
    
    else if(size < 260){
        //Create the popup       
        popup = window.open("","window","channelmode=0,width=400,height=200,top=200,left=350,scrollbars=no,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no");    
        popup.document.write(content); //Write content into it.    
    } else {
        //Create the popup       
        popup = window.open("","window","channelmode=0,width=400,height=200,top=200,left=350,scrollbars=no,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no");    
        popup.document.write(content); //Write content into it.    
    }
    
}




function transperent(){
    
          

    var overlay = document.getElementById('displyID');
    var specialBox = document.getElementById('blockDiv');
          
    overlay.style.opacity = .8;
    if(overlay.style.display == "block"){
        overlay.style.display = "none";
        specialBox.style.display = "none";
    } else {
        overlay.style.display = "block";
        specialBox.style.display = "block";
    }
}



function fieldLengthValidator(element){
	  var i=0;
 if(element.id=="resourceComments") {
       
        i=200;
    }
     var temp=0;
   
    if(temp==0 && element.value.replace(/^\s+|\s+$/g,"").length>i) {
        str=new String(element.value);
        element.value=str.substring(0,i);
        x0p( '','The Resource Comments length must be less than '+i+' characters','info');
        // displayErrorMessage('The '+element.id+' length must be less than '+i+' characters!','Error');
        element.focus();
        return false;
    }
    return true;
    }



function starResourceComments(starId) {
	var url = CONTENXT_PATH + "/getStarResourceComments.action?id="+starId
		var req = initRequest(url);
		req.onreadystatechange = function() {
			if (req.readyState == 4) {
				if (req.status == 200) {
					starResourceCommentsPoPUp(req.responseText);
				} else if (req.status == 204) {
					clearTable1();
				}
			}
		};
		req.open("GET", url, true);

		req.send(null);
	

}



 function countResourceComments(val,displayId) {
	
    var len = val.value.length;
    if (len >= 200) {
        val.value = val.value.substring(0, 200);
        document.getElementById(displayId).innerHTML=(0)+'\\200';
    } else {
        document.getElementById(displayId).innerHTML=(200-len)+'\\200';
    }
}






