/**
 * 
 */
function newXMLHttpRequest() {
    var xmlreq = false;
    if(window.XMLHttpRequest) {
        xmlreq = new XMLHttpRequest();
    } else if(window.ActiveXObject) {
        try {
            xmlreq = new ActiveXObject("MSxm12.XMLHTTP");
        } catch(e1) {
            try {
                xmlreq = new ActiveXObject("Microsoft.XMLHTTP");
            } catch(e2) {
                xmlreq = false;
            }
        }
    }
    return xmlreq;
}

function readyStateHandler(req,responseXmlHandler) {
    return function() {
        if (req.readyState == 4) {
            if (req.status == 200) {
                responseXmlHandler(req.responseXML);
            } else {
                alert("HTTP error"+req.status+" : "+req.statusText);
            }
        }
    }
}
function readyStateHandlerText(req,responseTextHandler){
    return function() {
        if (req.readyState == 4) {
            if (req.status == 200) {               
                responseTextHandler(req.responseText);
            } else {
                alert("HTTP error"+req.status+" : "+req.statusText);
            }
        }
    }
} 
function readyStateHandlerXml(req,responseXmlHandler) {
    return function() {
        if (req.readyState == 4) {
            if (req.status == 200) {
                responseXmlHandler(req.responseXML);
            } else {
                alert("HTTP error"+req.status+" : "+req.statusText);
            }
        }
    }
}


/*Methods for getting Practices by Department*/

function getPracticeDataV1() {
    
    var departmentName = document.getElementById("departmentId").value;
    var req = newXMLHttpRequest();
    req.onreadystatechange = readyStateHandlerXml(req, populatePractices);
    var url = CONTENXT_PATH+"/getEmpDepartment.action?departmentName="+departmentName;
    req.open("GET",url,"true");    
    req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    req.send(null);
    
}

function populatePractices(resXML) {    
    
    var practiceId = document.getElementById("practiceId");
    var department = resXML.getElementsByTagName("DEPARTMENT")[0];
    var practices = department.getElementsByTagName("PRACTICE");
    practiceId.innerHTML=" ";
    
    for(var i=0;i<practices.length;i++) {
        var practiceName = practices[i];
        
        var name = practiceName.firstChild.nodeValue;
        var opt = document.createElement("option");
        if(i==0){
            opt.setAttribute("value","");
        }else{
            opt.setAttribute("value",name);
        }
        opt.appendChild(document.createTextNode(name));
        practiceId.appendChild(opt);
    }
}

/*Methods closing Practices by Department*/



function clearTable(tableId) {
	
	    //alert("clearTable")
	    var tbl =  tableId;
	    var lastRow = tbl.rows.length; 
	    while (lastRow > 0) { 
	        tbl.deleteRow(lastRow - 1);  
	        lastRow = tbl.rows.length; 
	    } 
	}
   /* biometric */
   

   function getBiometricLoad() {
  // 	alert("---->+hiiiiii");
      //  var empNo = document.getElementById("empNo").value;
        var empId = document.getElementById("empId").value;
        var year = document.getElementById("year").value;
        var month = document.getElementById("month").value;
        var reportType = document.getElementById("reportType").value;
   //  alert(empNo+"--"+empId);
    // document.getElementById("empNoSpan").value="";
    //  document.getElementById("empNameSpan").value="";
        //alert(empId)
        
     	if(empId == null || empId == ""){
       		x0p( '','Please Enter EmpNo Or EmpName','info');
       		return false;
       	}
       	if(year == null || year == ""){
       		x0p( '','Please Enter Year','info');
       		return false;
       	}
       	if(month == null || month == "" ||  month == 0){
       		x0p( '','Please Select Month','info');
       		return false;
       	}
       
        
        document.getElementById('biometricBody').style.display = 'none';
      document.getElementById('tblbridges').style.display = 'none';
      document.getElementById('AttendanceDetails').style.display = 'none';
     document.getElementById('flagDesc').style.display = 'none';
     document.getElementById("noRecords").style.display="none";
      document.getElementById("loadingMsgForBioLoad").style.display="block";
      document.getElementById("reviewBtn").style.display="none";

        var req = newXMLHttpRequest();
        req.onreadystatechange = readyStateHandlerText(req,populateBiometricLoad); 

       var url=CONTENXT_PATH+"/getBiometricLoad.action?empId="+empId+"&year="+year+"&month="+month+"&reportType="+reportType;
      //   alert(url)   
       
      
       req.open("GET",url,"true");
       req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
       req.send(null);

   }



   function populateBiometricLoad(res){
	   //alert(res);
	    json=$.parseJSON(res);
	   var resText=json["response"];
	   var exist=json["isExist"];
	  // alert("exist"+exist);
	   
	   if(exist)
		   {
		   //alert("in if")
		   $("#reviewHidden").val("1");
		   $("#reviewButton").attr("value","Reviewed");
		   }
	   else
		   {
		   $("#reviewHidden").val("0");
		  // alert("in else")
		   $("#reviewButton").attr("value","Review Pending");
		   }
	   
	   var reportType = document.getElementById("reportType").value;
  //alert(reportType)
   	  var oTable = document.getElementById("tblbridges");
   	if(resText == "NoRecord")
   		{
   		document.getElementById("reviewBtn").style.display="none";
   		 document.getElementById("loadingMsgForBioLoad").style.display="none";
   		 document.getElementById("biometricBody").style.display="none";
   		 document.getElementById("noRecords").style.display="block";
   		
   		}else{
   			if(reportType == "PayrollData"){
   				//alert("I am in")
   			document.getElementById("reviewBtn").style.display="block";
   			}
   			 document.getElementById("noRecords").style.display="none";
   		
     document.getElementById("loadingMsgForBioLoad").style.display="none";
     // alert(resText)
    var empId = document.getElementById("empId").value;
  
    var result='';
        var resTextSplit2=new Array();
    
      
        document.getElementById('biometricBody').style.display = 'block';
          document.getElementById('flagDesc').style.display = 'block';
        document.getElementById('AttendanceDetails').style.display = 'block';
           document.getElementById('tblbridges').style.display = 'block';
           clearTable(oTable);
           var headerFields = new Array("&nbsp;&nbsp;&nbsp;Sunday&nbsp;&nbsp;&nbsp;","&nbsp;&nbsp;Monday&nbsp;&nbsp;","&nbsp;&nbsp;Tuesday&nbsp;&nbsp;","Wednesday","Thursday","&nbsp;&nbsp;&nbsp;Friday&nbsp;&nbsp;&nbsp;","&nbsp;&nbsp;Saturday&nbsp;&nbsp;");	
            tbody = document.createElement("TBODY");
           oTable.appendChild(tbody);
           var count=0;
           resTextSplit2[0]="";
           generateTableHeaderForAvailableBiometricLoad(tbody,headerFields);
             
          var present = "0";
          var holiday = "0";
          var weeklyOff = "0";
          var absent = "0";
          var daysinMonth = "0";
          // generateRow(oTable,tbody,resTextSplit2,0);
      
           var datarows=resText.split('*@!');
      // alert(datarows.length)
           for(var i=0;i<datarows.length-1;i++){
               var data=datarows[i].split('#^$');
             //  alert(data[0])
               var image="";
               
            
       
            
            data[1] = data[1].replace("??", "");
            
            //alert(data[1]+"--not if--"+data[3]);
            
            if(data[1] == "�Present" && data[3] == "�P"){
            	data[1]="Half Present";
            	data[3]="1/2P";
            	
            }   
            
            if(data[1] == "WeeklyOff  �Present" && data[3] == "WO�P"){
            	data[1]="WeeklyOff HalfPresent";
            	data[3]="WO1/2P";
            	
            }
            
            if(data[1]=="Present"){
                present++;
                 image="green.png";
             }
             else if(data[1]=="Absent" ){
                 absent++;
                 image="red.png";
             }
               else if(data[1]=="Holiday" ){
                   holiday++;
                 image="darkgreen.png";
             }
             else if(data[1]=="WeeklyOff"){
                 weeklyOff++;
                 image="violet.png";
             }
            
             else if(data[1]=="WeeklyOff Present"){
                image="babypink.png";
             }
               else if(data[1]=="Holiday Present"){
                  image="cyan.png";
             }
               else if(data[1]=="Holiday Absent"){
                   image="blackgrey.png";
              }
               else if(data[1]=="WeeklyOff Absent"){
                   image="brown.png";
              }
               else if(data[1]=="Half Present"){
                   image="orange.png";
              }
               else if(data[1]=="WeeklyOff HalfPresent"){
                   image="blue1.png";
              }
               else if(data[1]=="Holiday HalfPresent"){
                   image="pink.png";
              }
             else if(data[1]=="noRecord"){
                 image="yellow2.png";
             }
             daysinMonth = data[4];
     // alert("daysinMonth"+daysinMonth)
                   //  document.getElementById("daysproject").value="0";
           
                  
   var dateobj= new Date(data[0]) ;

      

   var month = dateobj.getMonth() + 1;

   var year = dateobj.getFullYear() + 1;
   var reportType = document.getElementById("reportType").value;


      if(data[1] == "Blank"){
                  
                     resTextSplit2[count] = " ";
               }else{
                   var day = dateobj.getDate();
                  // alert(data[1]+"\",\""+data[2])
                  //  resTextSplit2[count] = "<font size='2'>"+day+"</font>.<img title='"+data[1]+"' src='/Hubble/includes/images/ecertification/"+image+"'  width='13px' height='20px' border='0' ><br>&nbsp;&nbsp;&nbsp;&nbsp;<font size='2'>"+data[1].replace(/\ /g,"&nbsp;");+"</font>";
           var bioOverlay = "";
           if(data[1] != 'noRecord'){
                   if(data[2] != 0){
           	 
                   resTextSplit2[count] = "<a onclick='javascript:getBiometricHoursOverlay(\""+data[0]+"\",\""+data[1]+"\",\""+data[2]+"\",\""+data[3].replace("??", "")+"\",\""+empId+"\",\""+reportType+"\",\""+data[5]+"\",\""+data[6]+"\");' ><font size='2' color='#3300FF'>"+day+"</font>.<font size='2'>("+data[2]+"Hrs)</font><img title='"+data[1]+"' src='/Hubble/includes/images/ecertification/"+image+"'  width='13px' height='20px' border='0' /></a>";
            }else{
                resTextSplit2[count] = "<a onclick='javascript:getBiometricHoursOverlay(\""+data[0]+"\",\""+data[1]+"\",\""+data[2]+"\",\""+data[3].replace("??", "")+"\",\""+empId+"\",\""+reportType+"\",\""+data[5]+"\",\""+data[6]+"\");' ><font size='2' color='#3300FF'>"+day+"</font>.<img title='"+data[1]+"' src='/Hubble/includes/images/ecertification/"+image+"'  width='13px' height='20px' border='0' /></a>";
                 
            }
               }else{
            	   resTextSplit2[count] = "<font size='2' color='#3300FF'>"+day+"</font>.<img title='"+data[1]+"' src='/Hubble/includes/images/ecertification/"+image+"'  width='13px' height='20px' border='0' />";
                    
               }//alert(resTextSplit2[count])
          //  resTextSplit2[count] = "<font size='2'>"+day+"</font>";
            
               }
               

                //resTextSplit2[count]="<div class='boxdiv'>"+data[0]+". <b>"+data[2]+"</b><img  src='/Hubble/includes/images/ecertification/"+image+"'  width='20px' height='20px' border='0' ><br>&nbsp;&nbsp;&nbsp;&nbsp;<font size='2'>"+data[3]+"</font></div>";
               //  resTextSplit2[count]=""+data[0]+". <b><a href='javascript:getBcodeAndNumber(\""+data[2]+"\",\""+data[3]+"\",\""+data[5]+"\");' >"+data[2]+"</a></b><img title='"+data[4]+"' src='/Hubble/includes/images/ecertification/"+image+"'  width='20px' height='20px' border='0' ><br>&nbsp;&nbsp;&nbsp;&nbsp;<font size='2'>"+data[3].replace(/\ /g,"&nbsp;");+"</font>";
          
              
              //alert( resTextSplit2[count])
               //result+="<div class='boxdiv'>"+data[0]+". <b>"+data[2]+"</b><img  src='/Hubble/includes/images/ecertification/"+image+"'  width='20px' height='20px' border='0' ><br>&nbsp;&nbsp;&nbsp;&nbsp;<font size='2'>"+data[3]+"</font></div>"
      //    alert("(i+1)%7==0"+(i+1)%7)
      if((i+1)%7==0){
             //  alert("test")
                generateRowAvailableBiometricLoad(oTable,tbody,resTextSplit2,0,data[1]);
                resTextSplit2=new Array();
                count=-1;
           }
         
           count++;
         //  alert("count"+count)
       }
          // alert(reportType+"present"+present)
           document.getElementById("daysworked").value=present;
           document.getElementById("daysholidays").value=holiday;
             document.getElementById("daysweekend").value=weeklyOff;
               document.getElementById("daysvacation").value=absent;
                 document.getElementById("daysinmonth").value=daysinMonth;
          // calculateAttendance(daysinMonth,present,holiday,weeklyOff,absent,reportType);
    //   alert("last")
     //  if(resTextSplit2.length>0)
      //     generateRowAvailableBiometricLoad(oTable,tbody,resTextSplit2,0,data[1]);
         //  generateTableHeader(tbody,headerFields);
          // var resTextSplit1 = resText.split("*@!");
        //   for(var index=0;index<resTextSplit1.length-1;index++) {
               //resTextSplit2 = resTextSplit1[index].split("#^$");
             //  generateRow(oTable,tbody,resTextSplit2,index);
         //  }
         //  generateFooter(tbody,oTable);
      
      // document.getElementById("resultData").innerHTML=result;
          $('.pagination-btn').click();
   		}
   }
   function generateTableHeader(tableBody,headerFields) {
	    var row;
	    var cell;
	    row = document.createElement( "TR" );
	    row.className="gridHeader";
	    tableBody.appendChild( row );
	    for (var i=0; i<headerFields.length; i++) {
	        cell = document.createElement( "TD" );
	        cell.className="gridHeader";
	        row.appendChild( cell );

	        cell.setAttribute("width","10000px");
	        cell.innerHTML = headerFields[i];
	    }
	}
   function generateTableHeaderForAvailableBiometricLoad(tableBody,headerFields) {
       var row;
       var cell;
       row = document.createElement( "TR" );
        tableBody.appendChild( row );
     //  alert(headerFields)
     //  alert("row"+row)
       //row.className="gridHeader";
       tableBody.appendChild(row);
       for (var i=0; i<headerFields.length; i++) {
           cell = document.createElement( "TD" );
           cell.className="daysHeader";
           row.appendChild( cell );
            cell.setAttribute('align','center');
             cell.setAttribute('bgcolor','#3e93d4');
          //   cell.setAttribute('width', '120px');
           cell.innerHTML = headerFields[i];
           
         
           
     //  cell.width = 150;
       }
   }
   function generateRowAvailableBiometricLoad(oTable,tableBody,rowFeildsSplit,index,data) {
       
   //alert("data "+data[1]);
       var row;
       var cell;
       row = document.createElement("TR");
       // row.className="gridRowEven";
       // cell = document.createElement("TD");
       // cell.className="gridRowEven";
       // cell.innerHTML = index+1;daysinmonth
       //row.appendChild(cell);
       tableBody.appendChild(row);
       var totalLeaves;
      
       for (var i=0; i<=rowFeildsSplit.length-1; i++) {
       
   //   alert("rowFeildsSplit.length"+rowFeildsSplit.length);
           cell = document.createElement( "TD" );
           cell.setAttribute('width',10);
            // cell.width = 10;
             if(rowFeildsSplit[i] != " "){
           cell.className="bioboxdiv";
         
           cell.baseURI
             }
           /*  if(data=="Present"){
                 // present++;
                   image="#e6ffe6";
               }
               else if(data=="Absent" ){
               //    absent++;
                   image="#ffe6e6";
               }
                 else if(data=="Holiday" ){
                   //  holiday++;
                   image="#b3ffcc";
               }
               else if(data=="WeeklyOff"){
                  // weeklyOff++;
                   image="#fae6ff";
               }
              
               else if(data=="WeeklyOff Present"){
                  image="#ffe6f2";
               }
                 else if(data=="Holiday Present"){
                    image="#e6ffff";
               }
                 else if(data=="Holiday Absent"){
                     image="#f0f0f5";
                }
                 else if(data=="WeeklyOff Absent"){
                     image="#ffe6cc";
                }
                 else if(data=="Half Present"){
                     image="#ffd6cc";
                }
                 else if(data=="WeeklyOff HalfPresent"){
                     image="#e6f2ff";
                }
                 else if(data=="Holiday HalfPresent"){
                     image="#ffcce6";
                }
               else if(data=="noRecord"){
                   image="#ffffcc";
               } */
           row.appendChild(cell);
             cell.setAttribute('align','center');
             // cell.setAttribute('bgcolor',image);
               
           cell.innerHTML = rowFeildsSplit[i];
        
       }
     //  alert("----------------")
   }
               
   function closeConferenceSessionOverlay(){
       document.getElementById('resultMessage').innerHTML ='';
   //
   //clearEventData();

       document.getElementById("headerLabel").style.color="white";
       document.getElementById("headerLabel").innerHTML="Add Session Conference";
     //  showRow('addTr');
       var overlay = document.getElementById('overlay');
       var specialBox = document.getElementById('specialBox');

       overlay.style.opacity = .8;
       if(overlay.style.display == "block"){
       overlay.style.display = "none";
       specialBox.style.display = "none";

    //   document.getElementById("frmDBGrid").submit();

       }
       else {
       overlay.style.display = "block";
       specialBox.style.display = "block";
       }






    
      document.getElementById("frmDBGrid").submit();
   }

               
   function getBiometricHoursOverlay(date,status,biometricHrs,statusCode,empId,reportType,attandanceLogId,comments){
       var overlay = document.getElementById('biometricOverlay');
       var specialBox = document.getElementById('hubbleBiometricOverlay');
      // alert(reportType+"-->reportType")
       // alert(status+"-->Status")
       document.getElementById('resultMessage').style.display = 'none';
           document.getElementById('date').value = date;
           document.getElementById('timeSheetHrs').value = ' ';
           document.getElementById('biometricHrs').value = biometricHrs;
           document.getElementById('status').value = statusCode;
           document.getElementById('attandanceLogId').value = attandanceLogId;
           
         //  document.getElementById('reportTypeHidden').value = reportType;
         //  document.getElementById('statusHidden').value = status;
          // alert("in if"+reportType)
       if(reportType == "LiveData"){

       	document.getElementById("status").disabled=true;
   	
   	 $('#comUpdateId').hide();
   	// document.getElementById("comUpdateId").style.display="none";
   	
       }else{
   	 $('#comUpdateId').show();
   	 document.getElementById("status").disabled=false;
   	if(comments!=null) {
   		document.getElementById("comments").value = comments;
   	}else{
   		document.getElementById("comments").value = "";
   	}
   	
   	/* var mytd = document.getElementById("closeOverlay");
   	 var aTag = document.createElement('a');
   	 onclick="getBiometricHoursOverlay();"
   	 aTag.setAttribute('href',"#");
   	 aTag.setAttribute('onclick',"getBiometricHoursOverlay();");
   	 aTag.innerHTML = "link text";
   	 mydiv.appendChild(aTag); */
   	// document.getElementById("comUpdateId").style.display="block";
       }
          
          overlay.style.opacity = .8;
       if(overlay.style.display == "block"){
       	 
       	
           overlay.style.display = "none";
           specialBox.style.display = "none";
                      
       } else {
         //alert(date+"=="+empId+"=="+status+"=="+attandanceLogId);
    	  
           getAttendanceDetails(date,empId,status,attandanceLogId);
    	  
           overlay.style.display = "block";
           specialBox.style.display = "block";
       }   






   }

   function getAttendanceDetails(date,empId,status,attandanceLogId){
	   //alert(empId+""+date+""+status+""+attandanceLogId)
   	 var tableId = document.getElementById("tblPunchDetails");
	   if(status != 'Absent'){
	   document.getElementById('punchloadingMessage').style.display = 'block';
	   }
	   $("#officeHoursDivId").hide();
   	    clearTable(tableId);
   //alert(empId+""+date+""+status)
           $.ajax({
           url:'getBioAttendanceDetails.action?biometricDate='+date+'&empId='+empId+'&status='+status,
           context: document.body,
           success: function(responseText) {
            // alert("res"+responseText)
            
             var datarows=responseText.split('*@!');
         
          
           for(var i=0;i<datarows.length-1;i++){
               var data=datarows[i].split('#^$');
              // alert("data[3]"+data[3]);
                  document.getElementById("timeSheetHrs").value=data[0];
                  document.getElementById("timeSheetStatus").innerHTML="<font size='2' color='green'>("+data[1]+")</font>";
                  document.getElementById("leaveStatusId").style.display="none";
                  document.getElementById("leaveStatus").style.display="none";
                 
                 
                if(status == 'Absent'){
                    document.getElementById("leaveStatusId").style.display="block";
                    document.getElementById("leaveStatus").style.display="block";
                    document.getElementById("leaveStatus").innerHTML="<font size='2' color='green'>"+data[2]+"</font>"; 
                }else{
                	   document.getElementById('punchloadingMessage').style.display = 'none';
               	 getPunchDetails(data[3]);
               	 document.getElementById("tdPunchDetails").style.display="block";
               	 
                }
           }
              
           },
           error: function(e){
              
               alert("Please try again");
           }
       });
   }








   function getPunchDetails(resText) 

   {
	   //alert(resText)
       var tableId = document.getElementById("tblPunchDetails");
       
       clearTable(tableId);
       var tbody = tableId.childNodes[0];    
       tbody = document.createElement("TBODY");
       tableId.appendChild(tbody);
       if(resText == 'NoRecords'){
       	generateNoRecordsPunchDetails(tbody,tableId);
       }
       else{
       // var headerFields = new Array("SNo","Project&nbsp;Name","StartDate","EndDate","EmpStatus","Utilization","Resource&nbsp;Type");
       var headerFields = new Array("IN","In Device","OUT","Out Device");
      
       ParseAndGenerateHTMLPunchDetails(tableId,tbody,resText,headerFields);
       }
   }

   function ParseAndGenerateHTMLPunchDetails(oTable,tbody,responseString,headerFields) {
       
     /*  var start = new Date();
       var fieldDelimiter = "|";
       var recordDelimiter = "^";   
       
       if(oTable.id=="tblPunchDetails"){
           fieldDelimiter = "#^$";
           recordDelimiter = "*@!"; 
       } */
   	//var recordDelimiter = ",";   
      // var records = responseString.split(recordDelimiter);
       generateTablePunchDetails(oTable,tbody,headerFields,responseString);
   }
   function generateTablePunchDetails(oTable,tbody,headerFields,responseString) {
   	// alert("-----"+responseString)
	   $("#officehours").html("");
	 
       generateTableHeader(tbody,headerFields);
       var rowlength;
      rowlength = responseString.length-1;
      
       if(rowlength >=1 && responseString!=""){
       	
       	  var recordDelimiter = ",";   
       	    var records = responseString.split(recordDelimiter);
       	    var inArray = new Array();
       	    var outArray = new Array();
       	    var field = new Array();
       	    var flag = 0;
       	    var fieldLength;
       	    var temp = false;
       	    var temp1 = false;
       	  
       	    fieldLength = records.length;
       	  //  var init = records.indexOf('(');
       	  //  var fin = records.indexOf(')');
       	    var response;
       	    var count=0;
       	    for (var i=0;i<fieldLength-1;i++) {
       	    	
       	    	//  alert(records[i]+"-----records----"+records[i].substring(init + 1, fin))
       	    	  
       	    	temp = records[i].includes("in");
       	    	temp1 = records[i+1].includes("out");
       	    	  
       	    	  
       	    	  if(temp == true){
       	    		  if(temp1 == true){
       	    		  field[0] = records[i].substring(0, 5);
       	    		  field[1] = records[i].match(/\((.*?)\)/)[1];
      	        	     // field[1] = records[i].substring(init + 1, fin);
      	        	      field[2] = records[i+1].substring(0, 5);
   	        	      field[3] = records[i+1].match(/\((.*?)\)/)[1];
   	        	      i=i+1;
       	    		  }
       	    		  if(temp1 == false){
       	    			  field[0] = records[i].substring(0, 5);
          	        	      field[1] = records[i].match(/\((.*?)\)/)[1];
          	        	      field[2] = "-";
       	        	      field[3] = "-";
       	    		  }
       	    	  }else if(temp == false){
       	    		  field[0] = "-";
   	        	      field[1] = "-";
           	          field[2] = records[i].substring(0, 5);
          	        	  field[3] = records[i].match(/\((.*?)\)/)[1];
          	        	  
          	        	     } 
       	    	  
       	    	  response = field.toString();
       	    	  
       	    	  inArray[count]=field[0];
       	    	  outArray[count]=field[2];
       	    	  
       	    	  console.log(inArray);
       	    	  console.log(outArray);
       	    	  
       	    	  generateRowPunchDetails(oTable,tbody,response); 
       	    		count++;  
       	    	  }
       	    
       	    
       	
       	 var hours=0;
         var minutes=0;
         if(!(inArray.includes("-") || outArray.includes("-")))
 		{
        for(var i = 0; i < inArray.length; i++){
        	
        var inHoursandMinutes=inArray[i].split(":");
        var outHoursandMinutes=outArray[i].split(":");

        if(outHoursandMinutes[0] >= inHoursandMinutes[0] )
        {
        //alert("in if")
        hours+=outHoursandMinutes[0]-inHoursandMinutes[0];
        //alert(outHoursandMinutes[0]-inHoursandMinutes[0])
        }
        else
        {
        //alert("in else")
         hours+=(24-parseInt(inHoursandMinutes[0]))+parseInt(outHoursandMinutes[0]);
        
        }
        
        if(outHoursandMinutes[1] >= inHoursandMinutes[1] )
        {
        minutes+=parseInt(outHoursandMinutes[1]) - parseInt(inHoursandMinutes[1]);
        }
        else
        {
         minutes+=(parseInt(outHoursandMinutes[1]) - parseInt(inHoursandMinutes[1]))+60
         hours-=1;
        }
        
        }
        //alert("hours"+hours);
        //alert("minutes"+minutes);
        if(minutes>=60)
        {
        //alert("minutes mod"+minutes%60)
        hours+=minutes/60;
        minutes=minutes%60;
        }
        // alert("hours"+parseInt(hours));
         //alert("minutes"+minutes);
        
        if(minutes < 10){
        	minutes="0"+minutes;
        }
        $("#officeHourslabelId").attr("style","color:green;;font-size: 14px");
         $("#officehours").html(Math.trunc(hours)+":"+minutes);
       	   }
         else
        	 {
        	 $("#officeHourslabelId").attr("style","color:red;;font-size: 14px");
        	 $("#officehours").html("In/Out Punch has not been recorded");
        	 }
       	
       }    
      
       else {
           generateNoRecordsPunchDetails(tbody,oTable);
       }
       $("#officeHoursDivId").show();
       generateFooterPunchDetails(tbody,oTable);
   }
   function generateRowPunchDetails(oTable,tableBody,response) {
     //  alert("In generateRow"+responseString);
     
       
   	  var row;
   	    var cell;
   	    var fieldLength;
   	    delimiter = ","
   	  var fields = response.split(delimiter);
   	    fieldLength = fields.length ;
   	    var length;
   	    //if(oTable.id == "tblAccountSummRep" || oTable.id == "tblUpdateForAccountsListByPriority"){
   	    length = fieldLength;
   	    //  }
   	    
   	    // else {
   	    //     length = fieldLength-1;
   	    // }
   	  
   	    row = document.createElement( "TR" );
   	    row.className="gridRowEven";
   	    tableBody.appendChild( row );
   	    // alert("length..."+length);
   	 
   	        for (var i=0;i<length;i++) {
   	       
   	            cell = document.createElement( "TD" );
   	            cell.className="gridColumn";
   	            cell.innerHTML = fields[i];
   	            if(fields[i]!=''){
   	                row.appendChild( cell );
   	            }
   	       
   	        } 
       //  }
       
       // else {
       //     length = fieldLength-1;
       // }
     
     
       // alert("length..."+length);
       
      /* if(oTable.id=="tblPunchDetails"){
           for (var i=0;i<length;i++) {
           	
           //  field = ["-","-","-","-"];
               alert(records[i]+"-----records----"+records[i].substring(init + 1, fin))
              if(flag = 0 && records[i].includes("in")){
           	   field[0] = records[i].substring(0, 5);
           	   field[1] = records[i].substring(init + 1, fin);
           	   flag = 1;
           	
              }else if(flag = 0 && records[i].includes("out")){
           	   field[0] = "-";
           	   field[1] = "-";
           	   field[2] = records[i].substring(0, 5);
           	   field[3] = records[i].substring(init + 1, fin);
              }else if(flag > 0 && records[i].includes("in")){
           	   field[0] = records[i].substring(0, 5);
           	   field[1] = records[i].substring(init + 1, fin);
              }else if(flag > 0 && records[i].includes("out")){
           	   field[2] = records[i].substring(0, 5);
           	   field[3] = records[i].substring(init + 1, fin);
              }
           	if(field.length == 3){
           		 row = document.createElement("TR");
           	 	    row.className="gridRowEven";
           	 	    tableBody.appendChild( row );
           	}
            	 
            	
           	 for(var j=0;j<4;j++){
                    cell = document.createElement( "TD" );
                    cell.className="gridColumn";
                    alert(field[j]+"-----field[j]----")
                	cell.innerHTML = field[j];
                	  row.appendChild( cell );
                }        	
             
           }   
       } */

   }


   function generateNoRecordsPunchDetails(tbody,oTable) {
      
       var noRecords =document.createElement("TR");
       noRecords.className="gridRowEven";
       tbody.appendChild(noRecords);
       cell = document.createElement("TD");
       cell.className="gridColumn";
       cell.colSpan = "3";   
       cell.innerHTML = "No Punch Records Found";
       noRecords.appendChild(cell);
   }
   function generateFooterPunchDetails(tbody) {
   	  
       var cell;
       var footer =document.createElement("TR");
       footer.className="gridPager";
       tbody.appendChild(footer);
       cell = document.createElement("TD");
       cell.className="gridFooter";

       // cell.colSpan = "7";
       
       cell.colSpan = "4";
       
       footer.appendChild(cell);

   }
   //function calculateAttendance(daysinmonth,daysworked,daysholidays,daysweekend,daysvacation,reportType){
   function calculateAttendance(){
	   //$("#reviewButton").attr("value","Reviewed");
	   //alert("year")
	   var result=false;
	   x0p(' Do you want to submit the Review?', '', 'warning',
	            function(button) {
	       
	                if(button == 'warning') {
	                    result = true;
	                //alert(button)
	                }
	                if(button == 'cancel') { 
	                    result = false;
	                }
	                if(!result){
	                   return false;
	                }else{
	                	$("#loadingMsgForBioLoad").show();
	                    var year = document.getElementById("year").value;
	                    var month = document.getElementById("month").value;
	                   var daysinmonth = document.getElementById("daysinmonth").value;
	                    var daysworked = document.getElementById("daysworked").value;
	                  // var daysproject = document.getElementById("daysproject").value;
	                   var daysvacation = document.getElementById("daysvacation").value;
	                   var daysweekend = document.getElementById("daysweekend").value;
	                    var daysholidays = document.getElementById("daysholidays").value;
	                    var reportType = document.getElementById("reportType").value;
	                    var empId = document.getElementById("empId").value;
	                    //alert(reportType)
	                   
	                   // if(reportType == "PayrollData"){
	                     $.ajax({
	                       url:'calculateAttendance.action?daysInMonth='+daysinmonth+'&daysWorked='+daysworked+'&daysVacation='+daysvacation+'&daysWeekends='+daysweekend+'&daysHolidays='+daysholidays+'&empId='+empId+'&year='+year+'&month='+month+'&reportType='+reportType,
	                       context: document.body,
	                       success: function(responseText) {
	                    	   $("#loadingMsgForBioLoad").hide();
//	                    	   document.getElementById('resultMessage1').style.display = "block";
//	                          document.getElementById('resultMessage1').innerHTML="<font style='color:red;font-size:15px;padding-right:80%'>Submitted Successfully!!!</font>";
	                    	   var review=$("#reviewHidden").val();
	                    	   //alert(review+"review")
	                    	   if(review=="0")
	                    		   {
	                    		   
	                          x0p( '','Review Submitted Successfully!!!','text');
	                          $("#reviewButton").attr("value","Reviewed");
	                    		   }
	                    	   else
	                    		   {
	                    		   x0p( '','Review Updated Successfully!!!','text');
	                    		   }
	                       },
	                       error: function(e){
	                    	   $("#loadingMsgForBioLoad").hide();
	                            alert("Please try again");
	                       }
	                   });
	                }

	            }
	            );
	   
	   
      //  }
    }





   function updateAttendanceLoad(){
         var date = document.getElementById("date").value;
       var biometricHrs = document.getElementById("biometricHrs").value;
        var statusCode = document.getElementById("status").value;
        var attandanceLogId = document.getElementById("attandanceLogId").value;
      //alert(attandanceLogId)
      //  var status = document.getElementById("status").text.value;
        var e = document.getElementById("status");
        var status = e.options[e.selectedIndex].text;
        //var statusHidden = document.getElementById("statusHidden").value;
     //   alert(status)
       // alert(strUser)
        var timeSheetHrs = document.getElementById("timeSheetHrs").value;
    //    if(statusCode == "Absent"){
//        var leaveStatusId = document.getElementById("leaveStatusId").value;
//        }else{
//              leaveStatusId = "";
//        }
        var comments = document.getElementById("comments").value;
        var empId = document.getElementById("empId").value;
         $.ajax({
           url:'updateAttendanceLoad.action?biometricDate='+date+'&statusCode='+statusCode+'&status='+status+'&comments='+comments+'&empId='+empId+'&attendanceLogId='+attandanceLogId,
           context: document.body,
           success: function(responseText) {
             // alert(responseText)
            
                 
                  
              document.getElementById('resultMessage').style.display = "block";
              document.getElementById('resultMessage').innerHTML="<font style='color:red;font-size:15px;'>updated Successfully!!!</font>"
           	   getBiometricLoad();
            },
           error: function(e){
              
               alert("Please try again");
           }
       });
   }





   function addSyncAttendanceOverlay(){
       
       var overlay = document.getElementById('addAttendanceOverlay');
       var specialBox = document.getElementById('addAttendanceOverlayBox');
        
          overlay.style.opacity = .8;
       if(overlay.style.display == "block"){
          overlay.style.display = "none";
           specialBox.style.display = "none";
           window.location="getBiometric.action";
           
      } else {
            overlay.style.display = "block";
           specialBox.style.display = "block";
       }   
       
   }



   function addSynAttendance(){
	   document.getElementById('resultMessage').innerHTML="";
       var year = document.getElementById('syncYearOverlay').value;
       var month = document.getElementById('syncMonthOverlay').value;
       
    	if(year == null || year == ""){
       		x0p( '','Please Enter Year','info');
       		return false;
       	}
       	if(month == null || month == "" ||  month == 0){
       		x0p( '','Please Select Month','info');
       		return false;
       	}
        document.getElementById('response').innerHTML="";
        document.getElementById('loadingEmpDetailsMessage').style.display = 'block';
        var tableId = document.getElementById("tblEmpDetails");
	    clearTable(tableId);
       var comments = document.getElementById('comments').value;
        $.ajax({
           url:'addSynAttendance.action?syncYearOverlay='+year+'&syncMonthOverlay='+month+'&comments='+comments,
           context: document.body,
           success: function(responseText) {
              //alert(responseText)
        	   if(responseText == "success"){
        		   document.getElementById('loadingEmpDetailsMessage').style.display = 'none';
        		   document.getElementById('resultMessage').innerHTML="<font style='color:red;font-size:12px;'>Added Successfully!!!</font>"
        	   }else if(responseText == "error"){
        		   document.getElementById('loadingEmpDetailsMessage').style.display = 'none';
        		   document.getElementById('resultMessage').innerHTML="<font style='color:red;font-size:12px;'>Please Try Again</font>"
        	   }else if(responseText == "NoData"){
        		   document.getElementById('loadingEmpDetailsMessage').style.display = 'none';
        		   document.getElementById('resultMessage').innerHTML="<font style='color:red;font-size:12px;'>No Data Available for the Requested Month & Year</font>"
        	   }else{
        		   document.getElementById('response').innerHTML="";
        		   
        			   ParseAndGenerateHTMLResponse(responseText)
        		  pagerOption();
        	   }
            },
           error: function(e){
              
               alert("Please try again");
           }
       });

   }
   
   
   function ParseAndGenerateHTMLResponse(responseString) {
       
	   var tableId = document.getElementById("tblEmpDetails");
	   
	   
	    var headerFields = new Array("EmpNo","EmpName");
	   
	  
       var fieldDelimiter = "#^$";
       var recordDelimiter = "*@!";   
     // alert("responseString--->"+responseString)
       var records = responseString.split(recordDelimiter); 
       generateTableFromResponse(tableId,headerFields,records,fieldDelimiter);
   }
   function generateTableFromResponse(oTable, headerFields,records,fieldDelimiter) {	
       var tbody = oTable.childNodes[0];    
       tbody = document.createElement("TBODY");
       oTable.appendChild(tbody);
       
       var rowlength;
        //alert("records--->"+records)
        rowlength = records.length-1;
       
      
       
       if(rowlength >=1){
       generateTableHeader(tbody,headerFields);
       var rowlength;
      //alert("records--->"+records)
       rowlength = records.length-1;
       $("#totalRecords1").val(rowlength);
     // alert("rowlength--->"+rowlength)
       
       if(rowlength >=1 && records!=""){
           for(var i=0;i<rowlength;i++) {
        	   document.getElementById('loadingEmpDetailsMessage').style.display = 'none';
        	  
                   generateRowFromResponse(oTable,tbody,records[i],fieldDelimiter);  
           
           }    
       } 
       }
       else {
           generateNoRecords(tbody,oTable);
       }
       
     //  generateFooter(tbody,oTable);
   }



   function generateRowFromResponse(oTable,tableBody,record,delimiter) {
       //alert("In generateRow");
       var row;
       var cell;
       var fieldLength;
       var fields = record.split(delimiter);
       fieldLength = fields.length ;
       var length;
       length = fieldLength;
       document.getElementById('response').innerHTML="<font style='color:green;font-size:12px;'>Please review the following employees biometric data to add the sync attendance </font>"
 		  
     
       row = document.createElement( "TR" );
       row.className="gridRowEven";
       tableBody.appendChild( row );
    
      
           for (var i=0;i<length;i++) {
      
               cell = document.createElement( "TD" );
               cell.className="gridColumn";
               cell.innerHTML = fields[i];
               if(fields[i]!=''){
                   row.appendChild( cell );
               }
          
           }   
      
   	
   	 
   }


   function generateNoRecords(tbody,oTable) {
      
       var noRecords =document.createElement("TR");
       noRecords.className="gridRowEven";
       tbody.appendChild(noRecords);
       cell = document.createElement("TD");
       cell.className="gridColumn";
       cell.colSpan = "12"; 
       cell.innerHTML = "No Records Found for this Search";
       noRecords.appendChild(cell);
   }


   function syncingAttendanceOverlay(Month,Year){
      
       
       var overlay = document.getElementById('syncAttendanceOverlay');
       var specialBox = document.getElementById('syncAttendanceOverlayBox');
        overlay.style.opacity = .8;
       if(overlay.style.display == "block"){
          //  alert(Year)
          overlay.style.display = "none";
           specialBox.style.display = "none";
          
       } else {
         //  alert(Month)
           document.getElementById('month').value=Month;
            document.getElementById('year').value=Year;
        overlay.style.display = "block";
           specialBox.style.display = "block";
           
       }   
       
   }



   function getEmpNameByLocation(){
       var location = document.getElementById('locationId').value;
       
          $.ajax({
           url:'getEmpNameByLocation.action?locationId='+location,
           context: document.body,
           success: function(resXML) {
           //   alert(resXML)
            var empId = document.getElementById("empnameById");
       var team = resXML.getElementsByTagName("TEAM")[0];
       var users = team.getElementsByTagName("USER");
       empId.innerHTML=" ";
       for(var i=0;i<users.length;i++) {
           var userName = users[i];
           var att = userName.getAttribute("userId");
           var name = userName.firstChild.nodeValue;
           var opt = document.createElement("option");
           opt.setAttribute("value",att);
           opt.appendChild(document.createTextNode(name));
           empId.appendChild(opt);
       }
   },
           error: function(e){
              
               alert("Please try again");
           }
       });
   }

   function syncAttendance(){
       var location = document.getElementById('locationId').value;
//         var e = document.getElementById("empnameById");
//        var empName = e.options[e.selectedIndex].text;
      var empName = document.getElementById('employeeName').value;
       var month = document.getElementById('month').value;
       var year = document.getElementById('year').value;
          $.ajax({
           url:'syncAttendance.action?locationId='+location+'&empNameById='+empName+'&month='+month+'&year='+year,
           context: document.body,
           success: function(responseMessage) {
          //    alert(responseMessage)
        	  
    },
           error: function(e){
              
               alert("Please try again");
           }
       });
   }


   function empNameList() {
       var test=document.getElementById("empName").value;
       document.getElementById("empNo").value = "";
      // alert(test);
      
       if (test == "") {


           clearTable1();
           hideScrollBar();
           var validationMessage=document.getElementById("authorEmpValidationMessage");
           validationMessage.innerHTML = "";
           document.employeeAddForm.empId.value="";
       //frmSearch issuesForm
       } else {
           if (test.length >2) {
               //alert("CONTENXT_PATH-- >"+CONTENXT_PATH)
               var url = CONTENXT_PATH+"/getEmpFullNames.action?empName="+escape(test);
               var req = initRequest(url);
               req.onreadystatechange = function() {
                   //    alert("req-->"+req);
                   if (req.readyState == 4) {
                       if (req.status == 200) {
                           //alert(req.responseXML);
                       
                           parseEmpNames(req.responseXML);
                       } else if (req.status == 204){
                           clearTable1();
                       }
                   }
               };
               req.open("GET", url, true);

               req.send(null);
           }
       }





   }


   function parseEmpNames(responseXML) {
     // alert("-->"+responseXML);
       autorow1 = document.getElementById("menu-popup");
       autorow1.style.display ="none";
       autorow = document.getElementById("menu-popup");
       autorow.style.display ="none";
       clearTable1();
       var employees = responseXML.getElementsByTagName("EMPLOYEES")[0];
       if (employees.childNodes.length > 0) {        
           completeTable.setAttribute("bordercolor", "black");
           completeTable.setAttribute("border", "0");        
       } else {
           clearTable1();
       }
       if(employees.childNodes.length<10) {
           autorow1.style.overflowY = "hidden";
           autorow.style.overflowY = "hidden";        
       }
       else {    
           autorow1.style.overflowY = "scroll";
           autorow.style.overflowY = "scroll";
       }
       
       var employee = employees.childNodes[0];
       var chk=employee.getElementsByTagName("VALID")[0];
       if(chk.childNodes[0].nodeValue =="true") {
    	  // alert("true");
           //var validationMessage=document.getElementById("validationMessage");
           var validationMessage;
           
           validationMessage=document.getElementById("authorEmpValidationMessage");
           isPriEmpExist = true;
            
           validationMessage.innerHTML = "";
           document.getElementById("menu-popup").style.display = "block";
           for (loop = 0; loop < employees.childNodes.length; loop++) {
               
               var employee = employees.childNodes[loop];
               var customerName = employee.getElementsByTagName("NAME")[0];
               var empid = employee.getElementsByTagName("EMPID")[0];
               var empno = employee.getElementsByTagName("EMPNO")[0];
               var loginId = employee.getElementsByTagName("LOGINID")[0];
               appendEmpNames(empid.childNodes[0].nodeValue,customerName.childNodes[0].nodeValue,empno.childNodes[0].nodeValue,loginId.childNodes[0].nodeValue);
           }
           var position;
           
           
           position = findPosition(document.getElementById("empName"));
           
           posi = position.split(",");
           document.getElementById("menu-popup").style.left = posi[0]+"px";
           document.getElementById("menu-popup").style.top = (parseInt(posi[1])+20)+"px";
           document.getElementById("menu-popup").style.display = "block";
       } //if
       if(chk.childNodes[0].nodeValue =="false") {
    	   //alert("false");
           var validationMessage = '';
         
           isPriEmpExist = false;
           validationMessage=document.getElementById("authorEmpValidationMessage");
       
    
           validationMessage.innerHTML = " Invalid ! Select from Suggesstion List. ";
           validationMessage.style.color = "green";
           validationMessage.style.display = "block";
           validationMessage.style.fontSize = "12px";
          
           document.getElementById("empName").value = "";
           document.getElementById("empId").value = "";
           document.getElementById("loginId").value = "";
               
           
       }

   }

   function appendEmpNames(empId,empName,empNo,loginId) {
       
       var row;
       var nameCell;
       if (!isIE) {
           row = completeTable.insertRow(completeTable.rows.length);
           nameCell = row.insertCell(0);
       } else {
           row = document.createElement("tr");
           nameCell = document.createElement("td");
           row.appendChild(nameCell);
           completeTable.appendChild(row);
       }
       row.className = "popupRow";
       nameCell.setAttribute("bgcolor", "#3E93D4");
       var linkElement = document.createElement("a");
       linkElement.className = "popupItem";
       // if(assignedToType=='pre'){
       linkElement.setAttribute("href", "javascript:set_emp1('"+empName +"','"+ empId +"','"+empNo+"','"+loginId+"')");


       linkElement.appendChild(document.createTextNode(empName));
       linkElement["onclick"] = new Function("hideScrollBar()");
       nameCell.appendChild(linkElement);
   //fillWorkPhone(empId);
   }
   function set_emp1(eName,eID,eNo,loginId){
       clearTable1();
       document.employeeAddForm.empName.value =eName;
       document.employeeAddForm.empId.value =eID;
       if( document.employeeAddForm.empNo.value == ""){
       	document.employeeAddForm.empNo.value = eNo;
       }
       document.employeeAddForm.loginId.value =loginId;
   }
   var isIE;
   function initRequest(url) {
       
       if (window.XMLHttpRequest) {
           return new XMLHttpRequest();
       }
       else
       if (window.ActiveXObject) {
           isIE = true;
           return new ActiveXObject("Microsoft.XMLHTTP");
       }
       
   }
   function clearTable1() {
       if (completeTable) {
           completeTable.setAttribute("bordercolor", "white");
           completeTable.setAttribute("border", "0");
           completeTable.style.visible = false;
           for (loop = completeTable.childNodes.length -1; loop >= 0 ; loop--) {
               completeTable.removeChild(completeTable.childNodes[loop]);
           }
       }






   }


   function hideScrollBar() {
       autorow = document.getElementById("menu-popup");
       autorow.style.display = 'none';
   }
   function findPosition( oElement ) {
       if( typeof( oElement.offsetParent ) != undefined ) {
           for( var posX = 0, posY = 0; oElement; oElement = oElement.offsetParent ) {
               posX += oElement.offsetLeft;
               posY += oElement.offsetTop;
           }
           return posX+","+posY;
       } else {
           return oElement.x+","+oElement.y;
       }




   }
   function isNumeric(element) {

   	var val = element;
   	if (isNaN(val)) {
   		document.getElementById("empNo").value = "";
   		alert('EmpNo must be numeric values');
   		
   		
   	} 
   }
   function isNumericYear(element) {

   	var val = element;
   	
   	if (isNaN(val)) {
   		document.getElementById("year").value = "";
   		alert('Year must be numeric values');

   		
   	} 
   }
   function getEmpNameandId(empNo){
   	//alert("empNo"+empNo);
	//alert("empNo"+empNo.length);
   	document.getElementById("empName").value = "";
   	document.getElementById("empId").value = "";
   	if (empNo.length == 4) {
   		//alert("hi");
   	 $.ajax({
   	        url:'getEmpNameandId.action?empNo='+empNo,
   	        context: document.body,
   	        success: function(responseMessage) {
   	        //	alert(responseMessage)
   	        	var data=responseMessage.split('@#');
   	            
   	            
   	         
   	              //  alert("data[3]"+data[0]);
   	                  
   	                document.employeeAddForm.empName.value =data[0];
   	                document.employeeAddForm.empId.value =data[1];
   	                document.employeeAddForm.loginId.value =data[2];
   	             document.getElementById("authorEmpValidationMessage").style.display='none';
   	                
   	                
   	                 
   	        
   	         //  alert(responseMessage)
   	           
   	 },
   	        error: function(e){
   	           
   	            alert("Please try again");
   	        }
   	    });
   	}








   }

   function clearEmpNo(){
   	document.getElementById("empNo").value = "";
   	document.getElementById("empId").value = "";
   }
   function clearEmpName(){
   	document.getElementById("empName").value = "";
   	document.getElementById("loginId").value = "";
   }


   function getAttendanceLoad(month,year)
   {
     
      
       window.location = "generateExcelTimesheets.action?year="+year+"&month="+month;

   }
   
   
   function getBiometricDetails()
   {
	   
	   $("#loadingEmpDetails").show();
	   $("#monthYearTable").hide();
	   $("#biometricSearch").attr("disabled",true);
	   var empNo=document.getElementById('empNo').value;
	   var empName=document.getElementById('empName').value;
	   var year=document.getElementById('year').value;
	   if(year == 0 || year=="" || year==null){
		   x0p('','Please Enter Year','info');
		   $("#year").val('');
		   $("#loadingEmpDetails").hide();
		   $("#biometricSearch").attr("disabled",false);
		   return false;
	   }
	   var month=document.getElementById('month').value;
	   if(month == 0 || month=="" || month==null){
		   x0p('','Please Select Month','info');
		   $("#loadingEmpDetails").hide();
		   $("#biometricSearch").attr("disabled",false);
		 return false;
	   }
	   var monthName=$("#month").children("option:selected").html();
	   var location=document.getElementById('location').value;
	   var departmentId=document.getElementById('departmentId').value;
	   var practiceId=document.getElementById('practiceId').value;
	   var opsContactId=document.getElementById('opsContactId').value;
	   var reviewType=document.getElementById('reviewType').value;
	   var empId=document.getElementById('empId').value;
	   var loginId=document.getElementById('loginId').value;
	   var isTeamLead=false;
	   var monthsArray=["January", "February", "March", "April", "May", "June", "July", "August", "September", "October","November", "December"];
	   var isManager=false;
	   if($("#isTeamLead").prop("checked") == true){
		   isTeamLead=true;
       }
	   if($("#isManager").prop("checked") == true){
		   isManager=true;
       }
	   
	  if(location==-1)
		  {
		  location="";
		  }
	  if(month==-1)
	  {
		  month="";
	  }
	  if(departmentId==-1)
	  {
		  departmentId="";
	  }
	  if(practiceId==-1)
	  {
		  practiceId="";
	  }
	  if(opsContactId==-1)
	  {
		  opsContactId="";
	  }
	  if(reviewType==-1)
	  {
		  reviewType="";
	  }
	  
	  
	  var tableId = document.getElementById("biometricTable");
	   
	    ClrTable(tableId);
	 	
	    
	    var myObj = {};
	    myObj["empNo"]=empNo;
	    myObj["empName"]=empName;
	    myObj["year"]=year;
	    myObj["month"]=month;
	    myObj["location"]=location;
	    myObj["departmentId"]=departmentId;
	    myObj["practiceId"]=practiceId;
	    myObj["opsContactId"]=opsContactId;
	    myObj["reviewType"]=reviewType;
	    myObj["isTeamLead"]=isTeamLead;
	    myObj["isManager"]=isManager;
	    myObj["empId"]=empId;
	    myObj["loginId"]=loginId;
	    
	    var json = JSON.stringify(myObj);
	  
	   // alert("json"+json);
	    $.ajax({
	   	url:'getEmpDetailsForBioMetric.action',
	        data:{jsonData: json},
	        contentType: 'application/json',
	        type: 'GET',
	        context: document.body,
	        success: function(responseText) {
	        	//alert("responseText"+responseText);	
	        	
	        
	    // alert("responseText"+responseText);	
	     
	     var json = $.parseJSON(responseText);
    	//var myJSON = JSON.stringify(responseText);
  //	 alert(json.length);
    	 var data=json;
    
    	 var headerFields = new Array("Sno","First&nbsp;Name","Last&nbsp;Name","Employee&nbsp;No","Work&nbsp;Phone","EmailId","Status","Review&nbsp;Type","Review");
	     
    	
    	tbody = tableId.childNodes[0];    
       tbody = document.createElement("TBODY");
       tableId.appendChild(tbody);
      generateTableHeader(tbody,headerFields);
     var row;
     
     if(data.length>0){
     for (var i = 0; i < data.length; i++) {
        	var rowData=data[i];
     
        	row = document.createElement( "TR" );
       
        	
        	
        	 row.className="gridRowEven";
     		 
     		 
  		
  		tbody.appendChild( row );
  		//td 1
  	    cell = document.createElement( "TD" );
  	        cell.className="gridColumn";       
  	        cell.innerHTML = i+1;  
  			  cell.setAttribute("align","left");
  			   row.appendChild( cell );
	     
  		//alert("id"+rowData["empId"]);
  			//td	   
	      			cell = document.createElement( "TD" );
	      	        cell.className="gridColumn";       
	      	        cell.innerHTML = rowData["fName"];  
	      	       // cell.innerHTML = "<a href='javascript:loadEmployeeExpensesDetails("+rowData['Id']+",\""+location+"\",\""+rowData['EmployeeName']+"\",);'>"+rowData["EmployeeName"]+"</a>";
	      			  cell.setAttribute("align","left");
	      			   row.appendChild( cell );
	      			   
	      			 cell = document.createElement( "TD" );
		      	        cell.className="gridColumn";       
		      	        cell.innerHTML = rowData["lName"];  
		      	       // cell.innerHTML = "<a href='javascript:loadEmployeeExpensesDetails("+rowData['Id']+",\""+location+"\",\""+rowData['EmployeeName']+"\",);'>"+rowData["EmployeeName"]+"</a>";
		      			  cell.setAttribute("align","left");
		      			   row.appendChild( cell );
	      			   
	      				cell = document.createElement( "TD" );
	      	   	        cell.className="gridColumn";       
	      	   	        cell.innerHTML = rowData["empNo"];  
	      	   			  cell.setAttribute("align","left");
	      	   			   row.appendChild( cell );
	      	   			   
	      	   			cell = document.createElement( "TD" );
	 	      	        cell.className="gridColumn";       
	 	      	        cell.innerHTML = rowData["workPhoneNo"];  
	 	      			  cell.setAttribute("align","left");
	 	      			   row.appendChild( cell );
       
	 	      		/*	cell = document.createElement( "TD" );
	 	      	        cell.className="gridColumn";       
	 	      	        cell.innerHTML = rowData["cellPhoneNo"];  
	 	      			  cell.setAttribute("align","left");
	 	      			   row.appendChild( cell );
       */
	     
	      			 cell = document.createElement( "TD" );
	 	      	        cell.className="gridColumn";       
	 	      	        cell.innerHTML = rowData["email"];  
	 	      			  cell.setAttribute("align","left");
	 	      			   row.appendChild( cell );
	 	      			   
	 	      			   
	 	      			   
	 	      		/*	cell = document.createElement( "TD" );
	 	      	        cell.className="gridColumn"; 
	 	      	        month=rowData["month"]; 
	 	      	        cell.innerHTML = rowData["month"];  
	 	      			  cell.setAttribute("align","left");
	 	      			   row.appendChild( cell );
	 	      			   
	 	      			cell = document.createElement( "TD" );
	 	      	        cell.className="gridColumn";       
	 	      	        cell.innerHTML = rowData["year"];  
	 	      			  cell.setAttribute("align","left");
	 	      			   row.appendChild( cell );*/
	 	      			   
	 	      			   
	 	      			   
	 	      			   
	 	      			cell = document.createElement( "TD" );
	 	      	        cell.className="gridColumn";       
	 	      	        cell.innerHTML = rowData["status"];  
	 	      			  cell.setAttribute("align","left");
	 	      			   row.appendChild( cell );
	 	      			   
	 	      			cell = document.createElement( "TD" );
	 	      	        cell.className="gridColumn";       
	 	      	        cell.innerHTML = rowData["reviewType"];  
	 	      			  cell.setAttribute("align","left");
	 	      			   row.appendChild( cell );
	 	      			   
	 	      			cell = document.createElement( "TD" );
	 	      	        cell.className="gridColumn";       
	 	      	        //cell.innerHTML = rowData["status"]; 
	 	      	     cell.innerHTML = "<a href='javascript:monthlyReview("+rowData['empId']+",\""+rowData['monthNo']+"\",\""+rowData['year']+"\");'><img src='../../includes/images/DBGrid/View.gif' width='20' height='15' border='0' alter='Delete'></a>"
	 	      	    // cell.innerHTML = "<a href='getBiometricDetails.action?empId="+rowData['empId']+"><img src='../../includes/images/DBGrid/View.gif' width='20' height='15' border='0' alter='Delete'></a>";
	 	      			  cell.setAttribute("align","left");
	 	      			   row.appendChild( cell );
	 	      			// href="getBiometricDetails.action?empId=3785"><img src="../../includes/images/DBGrid/View.gif" width="20" height="15" border="0" alter="Delete"
	 	      			   
	 	      			//td	   
	 	      			   
		 	      			
	       
     }
    
     $("#loadingEmpDetails").hide();
     }
     else{
    	generateNoRecords(tbody,tableId);
    	$("#loadingEmpDetails").hide();
     }
     //alert("month"+month);
     //document.getElementById("monthYear").value=month+" "+year;
     $("#monthYear").html(" "+monthsArray[month-1]+" "+year);
     $("#monthYearTable").show();
    generateFooter(tbody,tableId);	   
      
    	// document.getElementById("loadingEmployeeExpenses").style.display = 'none';

          
    $("#biometricSearch").attr("disabled",false);
	        },
	        error: function(e){
	        	$("#loadingEmpDetails").hide();
	        	  alert("Please try again");
	        	  $("#biometricSearch").attr("disabled",false);  
	        }
	    });
   }
   function monthlyReview(Id,month,year)
   {
	   //alert("hi");
	   window.location="getBiometricDetails.action?empId="+Id+"&month="+month+"&year="+year;
   }
   
   
   function generateFooter(tbody) {
	   
	    var cell;
	    var footer =document.createElement("TR");
	    footer.className="gridPager";
	    tbody.appendChild(footer);
	    cell = document.createElement("TD");
	    cell.className="gridFooter";

	    // cell.colSpan = "7";
	    
	    cell.colSpan = "12";
	    
	       
	   

	    footer.appendChild(cell);
	}

   
   function ClrTable(tableId) {
	    var tbl =  tableId;
	    var lastRow = tbl.rows.length; 
	    while (lastRow > 0) { 
	        tbl.deleteRow(lastRow - 1);  
	        lastRow = tbl.rows.length; 
	    } 
	}