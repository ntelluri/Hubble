var _countDowncontainer=0;
var _currentSeconds=0;
var Clock = {
		  
		
			
		  start: function (remainigTime,flag) {
			 // alert("in start");
			  var countDownId=document.getElementById("CountDownPanel");
		    var self = this;
		    
		    
		    //alert("remianigtime in start"+remainigTime);
	    	//alert("totalSeconds"+sec);

		    this.interval = setInterval(function () {
		   // alert("seconds"+remainigTime);
		    	var sec=remainigTime
		    	
		      remainigTime -= 1;
		      _currentSeconds =  sec;
		      CountDown();
		      
		      
		      
		      var minutes=Math.floor(sec / 60 % 60);
		   
		  	
		  	//shrink:
		  	var seconds = parseInt(sec % 60);
		      //alert("seconds"+seconds);
		  	
		  	//get hours:
		  	var hours=Math.floor(sec / 3600);
		    //alert("hours"+hours);
		  	
		  	//shrink:
		  	//minutes = (minutes%60);
		    //alert(minutes+"minutes");
		  	var strText = AddZero(hours) + ":" + AddZero(minutes) + ":" + AddZero(seconds);
		  	countDownId.innerHTML = strText;
		  	if(flag!="I"){
		  	 $("#overlayLoading").hide();
		  	}
		      /* $("#hour").text(Math.floor(self.totalSeconds / 3600));
		      $("#min").text(Math.floor(self.totalSeconds / 60 % 60));
		      $("#sec").text(parseInt(self.totalSeconds % 60)); */
		    }, 1000);
		    
		  },

		  pause: function () {
			  //alert("pause");
		    clearInterval(this.interval);
		    delete this.interval;
		  },

		  resume: function () {
			 // alert("resume");
		    if (!this.interval) this.start(countTime,"R");
		  }
		};


function ActivateCountDown(strContainerID, initialValue) {
	_countDowncontainer = document.getElementById(strContainerID);
	
	if (!_countDowncontainer) {
		alert("count down error: container does not exist: "+strContainerID+
			"\nmake sure html element with this ID exists");
		return;
	}
	
	SetCountdownText(initialValue);
	window.setTimeout("CountDownTick()", 1000);
}

function CountDownTick() {
	//alert("in count down"+_currentSeconds);
	if (_currentSeconds <= 0) {
		alert("your time has expired!");
               
                document.forms["ecertificationForm"].submit();
		return;
	}
	
	SetCountdownText(_currentSeconds-1);
	window.setTimeout("CountDownTick()", 1000);
}

function CountDown() {
	//alert("in count down"+_currentSeconds);
	if (_currentSeconds <= 0) {
		Clock.pause();
		alert("your time has expired!");
               
                document.forms["ecertificationForm"].submit();
		return;
	}
}


function SetCountdownText(seconds) {
	//store:
	_currentSeconds = seconds;
	
	//get minutes:
	var minutes=parseInt(seconds/60);
	
	//shrink:
	seconds = (seconds%60);
	
	//get hours:
	var hours=parseInt(minutes/60);
	
	//shrink:
	minutes = (minutes%60);
	
	//build text:
	var strText = AddZero(hours) + ":" + AddZero(minutes) + ":" + AddZero(seconds);
	
	//apply:
	_countDowncontainer.innerHTML = strText;
}

function AddZero(num) {
	return ((num >= 0)&&(num < 10))?"0"+num:num+"";
}