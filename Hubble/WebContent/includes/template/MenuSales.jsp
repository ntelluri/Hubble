<!doctype html>
<html lang=''>
    <%@ page contentType="text/html; charset=UTF-8" errorPage="../exception/ErrorDisplay.jsp"%>
<%@ page import="com.mss.mirage.util.ApplicationConstants"%>
 <%@ taglib prefix="s" uri="/struts-tags" %>
 <%@ page import="com.opensymphony.xwork2.ActionContext"%>
<head>
   <meta charset='utf-8'>
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <link rel="stylesheet" type="text/css" href="<s:url value="/includes/css/menu/toggleMenu.css"/>">
   <%--  <script type="text/JavaScript" src="<s:url value="/includes/javascripts/jquery-latest.min.js"/>"></script>   --%>
    <script type="text/JavaScript" src="<s:url value="/includes/javascripts/reviews/jquery.min.js"/>"></script>
    <script type="text/javascript" src="<s:url value="/includes/javascripts/reviews/jquery.js"/>"></script>  
   <script type="text/JavaScript" src="<s:url value="/includes/javascripts/menu/menuSalesNew.js"/>"></script>
       <script type="text/javascript" src="<s:url value="/includes/javascripts/AppConstants.js"/>"></script>
 <script type="text/javascript" src="<s:url value="/includes/javascripts/GreenSheetClientValidation.js"/>"></script>
   
</head>
<body>
<div id='cssmenu'>
        <%
                String actionName = ActionContext.getContext().getName();
                  String greenSheets= request.getParameter("teamValue");
                //System.out.println("action name"+actionName);
                %>
                <span id="action" style="display: none"><%=actionName%></span> 
                  <span id="greenSheets" style="display: none"><%=greenSheets%></span> 
<ul>
   
    
   <li class='has-sub'><a href='#' id="iconToggleMy"><span id="myAdmin">My</span></a>
      <ul id="myDisplay">
          <li ><a  id="accountsListMy" href="/<%=ApplicationConstants.CONTEXT_PATH%>/crm/accounts/accountsListMy.action?accList=1" onClick="leftMenuToggleId(this,'myAdmin');"> <span>Accounts</span></a></li>
          <li ><a  id="untouchedAccounts" href="/<%=ApplicationConstants.CONTEXT_PATH%>/crm/accounts/untouchedAccounts.action" onClick="leftMenuToggleId(this,'myAdmin');"> <span>UntouchedAccounts</span></a></li>
         <li ><a id="myActivitiesInfo"  href="/<%=ApplicationConstants.CONTEXT_PATH%>/crm/activities/myActivitiesInfo.action" onClick="leftMenuToggleId(this,'myAdmin');"><span>Activities</span></a></li>                 
        <s:if test="#session.isUserManager == 0">
             <li><a   id="dashBoardMy" href="/<%=ApplicationConstants.CONTEXT_PATH%>/crm/accounts/dashBoard.action?dashboardFlag=My" onClick="leftMenuToggleId(this,'myAdmin');"><span>Dashboard</span></a></li>
         </s:if>
         <li><a   id="greenSheet" href="/<%=ApplicationConstants.CONTEXT_PATH%>/crm/greensheets/greenSheet.action?teamValue=notAvailable" onClick="leftMenuToggleId(this,'myAdmin');"><span>Greensheets</span></a></li>
          <li><a id="getCalendar" href="/<%=ApplicationConstants.CONTEXT_PATH%>/crm/calendar/getCalendar.action" onClick="leftMenuToggleId(this,'myAdmin');"><span>Calendar</span></a></li>
          <s:if test="#session.sessionEmpPractice == 'Practice'">
               <li><a id="myOpportunities" href="/<%=ApplicationConstants.CONTEXT_PATH%>/crm/opportunities/myOpportunities.action" onClick="leftMenuToggleId(this,'myAdmin');"><span>Opportunities</span></a></li>
          </s:if>
      </ul>
   </li>
   <s:hidden id="userManager" value="%{#session.isUserManager}"></s:hidden> 
   <s:hidden id="userTeamLead" value="%{#session.isUserTeamLead}"></s:hidden> 
    <s:if test="#session.isUserManager != 0  || #session.isUserTeamLead != 0">
   <li class='has-sub'><a href='#' id="iconToggleTeam"><span id="teamAdmin">Team</span></a>
      <ul id="teamDisplay">
         <li><a id="accountAssign" href="/<%=ApplicationConstants.CONTEXT_PATH%>/crm/accounts/accountAssign.action" onClick="leftMenuToggleId(this,'teamAdmin');"><span>Account&nbsp;Operations</span></a></li>
        
 <li><a id="dashBoardTeam"  href="/<%=ApplicationConstants.CONTEXT_PATH%>/crm/accounts/dashBoard.action?dashboardFlag=Team" onClick="leftMenuToggleId(this,'teamAdmin');"><span>Dashboard</span></a></li>
        		 
 <li><a id="accountsListMyTeam"  href="/<%=ApplicationConstants.CONTEXT_PATH%>/crm/accounts/accountsListMyTeam.action?accList=1" onClick="leftMenuToggleId(this,'teamAdmin');"><span>Accounts</span></a></li>
         <li><a id="untouchedTeamAccounts"  href="/<%=ApplicationConstants.CONTEXT_PATH%>/crm/accounts/untouchedTeamAccounts.action" onClick="leftMenuToggleId(this,'teamAdmin');"><span>UntouchedAccounts</span></a></li>
         <li><a id="teamActivitiesInfo"  href="/<%=ApplicationConstants.CONTEXT_PATH%>/crm/activities/teamActivitiesInfo.action" onClick="leftMenuToggleId(this,'teamAdmin');"><span>Activities</span></a></li>
         
         <li><a id="greenSheetTeam"  href="/<%=ApplicationConstants.CONTEXT_PATH%>/crm/greensheets/greenSheet.action?teamValue=available" onClick="leftMenuToggleId(this,'teamAdmin');"><span>Greensheets</span></a></li>
         <li><a id="accessTeamCalendar"  href="/<%=ApplicationConstants.CONTEXT_PATH%>/crm/calendar/accessTeamCalendar.action" onClick="leftMenuToggleId(this,'teamAdmin');"><span>Calendar</span></a></li>
         <li><a id="myTeamTree"  href="/<%=ApplicationConstants.CONTEXT_PATH%>/employee/myTeamTree.action" onClick="leftMenuToggleId(this,'teamAdmin');"><span>Hierarchy</span></a></li>
      </ul>
   </li>
    </s:if>
    <li class='has-sub'><a href='#' id="iconToggleServices"><span id="servicesAdmin">Services</span></a>
      <ul id="servicesDisplay">
     <%--   <li><a id="accessCalendar" href="/<%=ApplicationConstants.CONTEXT_PATH%>/crm/calendar/accessCalendar.action"><span>Access Calendar</span></a></li> --%>
         <li><a id="accountSearch"  href="/<%=ApplicationConstants.CONTEXT_PATH%>/crm/accounts/accountSearch.action" onClick="leftMenuToggleId(this,'servicesAdmin');"><span>Accounts Search</span></a></li>
         <li><a id="bdmDashBoard" href="/<%=ApplicationConstants.CONTEXT_PATH%>/crm/accounts/bdmDashBoard.action" onClick="leftMenuToggleId(this,'servicesAdmin');"><span>New&nbsp;Dashboard</span></a></li>
         <li><a id="availableEmployeeList" href="/<%=ApplicationConstants.CONTEXT_PATH%>/employee/pmoDashBoard.action" onClick="leftMenuToggleId(this,'servicesAdmin');"><span>Available&nbsp;Employee&nbsp;list</span></a></li>
         <li><a id="contactSearch" href="/<%=ApplicationConstants.CONTEXT_PATH%>/crm/contacts/contactSearch.action" onClick="leftMenuToggleId(this,'servicesAdmin');"><span>Contacts</span></a></li>
         <li><a id="campaignSearchAction" href="/<%=ApplicationConstants.CONTEXT_PATH%>/marketing/campaignSearchAction.action" onClick="leftMenuToggleId(this,'servicesAdmin');"><span>Campaign</span></a></li>
         <li class='last'><a id="clientReqEngagementSearch"  href="/<%=ApplicationConstants.CONTEXT_PATH%>/crm/accounts/clientReqEngagementSearch.action?backToFlag=No" onClick="leftMenuToggleId(this,'servicesAdmin');"><span>PSCER/RFP</span></a></li>
      </ul>
    </li>
     <li class='has-sub'><a href='#' id="iconToggleRequirement"><span id="requirementAdmin">Requirement</span></a>
      <ul id="requirementDisplay">
        <li><a id="requirementList" href="/<%=ApplicationConstants.CONTEXT_PATH%>/crm/requirement/requirementList.action?ajaxId=-1" onClick="leftMenuToggleId(this,'requirementAdmin');"><span>List</span></a></li>         
      </ul>
    </li>
      </ul>
   </li>
   
</ul>
</div>
    

</body>
</html>

    
    
     