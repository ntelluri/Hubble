<!doctype html>
<html lang=''>
    <%@ page contentType="text/html; charset=UTF-8" errorPage="../exception/ErrorDisplay.jsp"%>
    <%@ page import="com.mss.mirage.util.ApplicationConstants"%>
    <%@page import="com.opensymphony.xwork2.ActionContext"%>
    <%@ taglib prefix="s" uri="/struts-tags" %>
    <head>
        <meta charset='utf-8'>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">



        <%-- <script type="text/JavaScript" src="<s:url value="/includes/javascripts/jquery-latest.min.js"/>"></script>  --%>
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/reviews/jquery.min.js"/>"></script>
        <script type="text/javascript" src="<s:url value="/includes/javascripts/reviews/jquery.js"/>"></script>  
        <script src="script.js"></script><link rel="stylesheet" type="text/css" href="<s:url value="/includes/css/menu/toggleMenu.css"/>">
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/menu/menuOperationsNew.js?ver=1.1"/>"></script>
        <script type="text/javascript" src="<s:url value="/includes/javascripts/AppConstants.js"/>"></script> 
        <script type="text/javascript" src="<s:url value="/includes/javascripts/reviews/ajaxfileupload.js"/>"></script> 




    </head>
    <body>

        <div id='cssmenu'>
            <%
                String actionName = ActionContext.getContext().getName();
                //System.out.println("action name"+actionName);
%>
            <span id="action" style="display: none"><%=actionName%></span> 
            <ul>

                <li class='has-sub' ><a href='#' id="iconToggleMy"><span id="myAdmin">My</span></a>
                    <ul id="myDisplay">
                    </ul>
                </li>
                <s:hidden id="userManager" value="%{#session.isUserManager}"></s:hidden> 
                <s:hidden id="teamLead" value="%{#session.isUserTeamLead}"></s:hidden> 
                <%-- <s:if test="#session.isUserManager != 0"> --%>
                <s:if test="#session.isUserManager == 1 || #session.isUserTeamLead==1">
                    <li class='has-sub'><a href='#' id="iconToggleTeam"><span id="teamAdmin">Team</span></a>
                        <ul id="teamDisplay">
                            <%--  <li><a id="userSearch" href="/<%=ApplicationConstants.CONTEXT_PATH%>/admin/userSearch.action"> <span>Assign Roles</span></a></li>  --%>
                            <li class='last'><a id="myOperationsTeamTree" href="/<%=ApplicationConstants.CONTEXT_PATH%>/employee/myOperationsTeamTree.action" onClick="leftMenuToggleId(this,'teamAdmin');"><span>Hierarchy</span></a></li>
                        </ul>
                    </li>
                </s:if>
                <li class='has-sub'><a href='#' id="iconToggleServices"><span id="servicesAdmin">Services</span></a>
                    <ul id="servicesDisplay">
                    <li><a id="empLeaveReport" href="/<%=ApplicationConstants.CONTEXT_PATH%>/reports/empLeaveReport.action" onClick="leftMenuToggleId(this,'servicesAdmin');"> <span>Emp&nbsp;Leaves&nbsp;List</span></a></li>
                    <s:if test="#session.isAdminAccess == 1 || #session.workCountryList == 'USA'">
                            <li><a id="greenSheet" href="/<%=ApplicationConstants.CONTEXT_PATH%>/crm/greensheets/greenSheet.action" onClick="leftMenuToggleId(this,'servicesAdmin');"> <span>GreenSheets&nbsp;List</span></a></li>                            
                        </s:if>
                        <li><a id="newempTimeSheetSearch" href="/<%=ApplicationConstants.CONTEXT_PATH%>/employee/timesheets/newempTimeSheetSearch.action" onClick="leftMenuToggleId(this,'servicesAdmin');"> <span>Emp&nbsp;TimeSheets&nbsp;List</span></a></li>
                        <li class='last'><a id="getMyAppreciation" href="/<%=ApplicationConstants.CONTEXT_PATH%>/employee/appreciation/getMyAppreciation.action?searchflag=opt" onClick="leftMenuToggleId(this,'servicesAdmin');"><span>Appreciations&nbsp;List</span></a></li>
                        <s:if test ="#session.starPerformerAccess || #session.sessionIsOperationContactTeamFlag == 1" >
                            <li class='last'><a id="getMyStarPerformers" href="/<%=ApplicationConstants.CONTEXT_PATH%>/employee/starPerformance.action" onClick="leftMenuToggleId(this,'servicesAdmin');"><span>Star&nbsp;Performers&nbsp;List</span></a></li>
                        </s:if>
                          <s:hidden id="noDueRemainders" value="%{#session.noDueRemainders}"></s:hidden> 
                        <s:hidden id="noDueApprovers" value="%{#session.noDueApprovers}"></s:hidden> 
                        <s:if test="#session.noDueRemainders == 1||#session.noDueApprovers == 1">
                            <li><a id="getEmployeeNoDuesOperations"  href="/<%=ApplicationConstants.CONTEXT_PATH%>/employee/payroll/getEmployeeNoDuesOperations.action" onClick="leftMenuToggleId(this,'servicesAdmin');"><span>No&nbsp;Dues&nbsp;List</span></a></li>   
                        </s:if>
                        <li><a id="empSearchAll" href="/<%=ApplicationConstants.CONTEXT_PATH%>/employee/empSearchAll.action" onClick="leftMenuToggleId(this,'servicesAdmin');"> <span>Emp&nbsp;Search</span></a></li>
                        <li><a id="empEmailCheck" href="/<%=ApplicationConstants.CONTEXT_PATH%>/employee/empEmailCheck.action" onClick="leftMenuToggleId(this,'servicesAdmin');"><span>Email&nbsp;Check</span></a></li>
                        <li><a id="userSearch" href="/<%=ApplicationConstants.CONTEXT_PATH%>/admin/userSearch.action" onClick="leftMenuToggleId(this,'servicesAdmin');"> <span>Assign Roles</span></a></li>
                        <li><a id="surveyFormList" href="/<%=ApplicationConstants.CONTEXT_PATH%>/marketing/surveyform/surveyFormList.action" onClick="leftMenuToggleId(this,'servicesAdmin');"> <span>Survey&nbsp;Form</span></a></li>
                        <s:hidden id="bdmAssociationAccess" value="%{#session.bdmAssociationAccess}"></s:hidden> 
                        <s:if test ="#session.bdmAssociationAccess">
                            <li class='last'><a id="getBdmAssociates" href="/<%=ApplicationConstants.CONTEXT_PATH%>/admin/getBdmAssociates.action" onClick="leftMenuToggleId(this,'servicesAdmin');"><span>BDM&nbsp;Associations</span></a></li>
                        </s:if>                                                   
                        <li><a id="resetPassword" href="/<%=ApplicationConstants.CONTEXT_PATH%>/general/resetPassword.action" onClick="leftMenuToggleId(this,'servicesAdmin');"> <span>Reset&nbsp;My&nbsp;Pwd</span></a></li>
                        <li><a id="servicesResetUserPwd" href="/<%=ApplicationConstants.CONTEXT_PATH%>/admin/changePassword.action" onClick="leftMenuToggleId(this,'servicesAdmin');"><span>Reset User Pwd</span></a></li>
                        <!-- <li><a id="changeCustPassword" href="/<%=ApplicationConstants.CONTEXT_PATH%>/admin/changeCustPassword.action" onClick="leftMenuToggleId(this,'servicesAdmin');"> <span>Reset&nbsp;Cust&nbsp;Pwd</span></a></li> -->
                        <s:if test="#session.isAdminAccess == 1 || #session.userId == 'gjampana' || #session.userId == 'peeti'">
                      <li><a id="getEmpVerification" href="/<%=ApplicationConstants.CONTEXT_PATH%>/marketing/getEmpVerification.action" onClick="leftMenuToggleId(this,'servicesAdmin');"> <span> EmpVerification&nbsp;Data</span></a></li>
                        </s:if>
                                                 
                                                                              
                            <%-- Related to Star Performer Start--%>
                                                                                                                        
                             <%-- Related to Star Performer End--%>
                             <s:if test="#session.userId == 'vkandregula'" >
                             <li class='biometric'><a id="getBiometric" href="/<%=ApplicationConstants.CONTEXT_PATH%>/employee/timesheets/getBiometric.action" onClick="leftMenuToggleId(this,'servicesAdmin');"><span>Biometric</span></a></li>
                             </s:if>
							  <li class='transferLocation'><a id="getEmpTransfers" href="/<%=ApplicationConstants.CONTEXT_PATH%>/employee/getEmpTransfers.action" onClick="leftMenuToggleId(this,'servicesAdmin');"><span>Location&nbsp;Transfer&nbsp;List</span></a></li>
                              <li class='empHolidays'><a id="getEmpHolidays" href="/<%=ApplicationConstants.CONTEXT_PATH%>/employee/getEmpHolidays.action" onClick="leftMenuToggleId(this,'servicesAdmin');"><span>Emp&nbsp;Holidays</span></a></li>
                             
                    </ul>
                </li>
                
                <li class='has-sub'><a href='#' id="iconToggleReviews"><span id="reviewsAdmin">Reviews</span></a>
                    <ul id="reviewsDisplay">
                       <li><a id="getAppraisalsList" href="/<%=ApplicationConstants.CONTEXT_PATH%>/employee/Reviews/getAppraisalsList.action" onClick="leftMenuToggleId(this,'reviewsAdmin');"> <span>Appraisals</span></a></li>
                       <li><a id="teamReviewList"  href="/<%=ApplicationConstants.CONTEXT_PATH%>/employee/Reviews/teamReviewList.action" onClick="leftMenuToggleId(this,'reviewsAdmin');"> <span>Perf.Review</span></a></li>
                       <li><a id="givePerformanceReview" href="/<%=ApplicationConstants.CONTEXT_PATH%>/employee/performanceReviews/givePerformanceReview.action" onClick="leftMenuToggleId(this,'reviewsAdmin');"><span>Performance Review</span></a></li>
                       <li class='last'><a id="teamQuaterAppraisalSearch" href="/<%=ApplicationConstants.CONTEXT_PATH%>/employee/appraisal/teamQuaterAppraisalSearch.action" onClick="leftMenuToggleId(this,'reviewsAdmin');"><span>Q-Review</span></a></li>
                   </ul>
                </li>
                
                
                <li class='has-sub'><a href='#' id="iconToggleDashboards"><span id="dashboardsAdmin">Reports&nbsp;&&nbsp;Dashbaords</span></a>
                    <ul id="dashboardsDisplay">
                       <li><a id="perfDashboard" href="/<%=ApplicationConstants.CONTEXT_PATH%>/employee/Reviews/perfDashboard.action" onClick="leftMenuToggleId(this,'dashboardsAdmin');"> <span>Perf. Dashboard</span></a></li>
                       <li><a id="GenerateEmpLeaveReport" href="/<%=ApplicationConstants.CONTEXT_PATH%>/reports/GenerateEmpLeaveReport.action" onClick="leftMenuToggleId(this,'dashboardsAdmin');"> <span>Leaves Report</span></a></li>
                       <s:if test="#session.isAdminAccess == 1 || #session.workCountryList == 'USA'">
                       <li><a id="empTimeSheets" href="/<%=ApplicationConstants.CONTEXT_PATH%>/crm/greensheets/greensheetReport.action" onClick="leftMenuToggleId(this,'dashboardsAdmin');"> <span>Greensheet Reports</span></a></li>
                        </s:if>
                        <li><a id="getDashBoardForEmpReports" href="/<%=ApplicationConstants.CONTEXT_PATH%>/reports/getDashBoardForEmpReports.action" onClick="leftMenuToggleId(this,'dashboardsAdmin');"><span>DashBoards</span></a></li>
                        <s:if test="#session.isAdminAccess == 1 || #session.userId == 'rijju' || #session.userId =='ukodati' || #session.userId =='rkalaga' || #session.userId =='vpusapati' || #session.userId =='csigireddy' || #session.userId =='cneelagiri'">                            
                            <li><a id="getQReviewDashBoard" href="/<%=ApplicationConstants.CONTEXT_PATH%>/employee/appraisal/getQReviewDashBoard.action"  onClick="leftMenuToggleId(this,'dashboardsAdmin');"><span>Q-Review&nbsp;Report</span></a></li>
                        </s:if>
                        
                        <%--   <s:if test="#session.isAdminAccess == 1"> --%>
                         <s:if test="(#session.sessionEmpPractice == 'HR' &&  #session.sessionIsOperationContactTeamFlag == 1)"> 
                            <li><a id="getQReviewPendingReportDashBoard" href="/<%=ApplicationConstants.CONTEXT_PATH%>/employee/appraisal/getQReviewPendingReportDashBoard.action"  onClick="leftMenuToggleId(this,'dashboardsAdmin');"><span>Q-Review&nbsp;PendingReport</span></a></li>
                        </s:if>
					
                        
                        <s:if test="%{#session.sessionEmpPractice == 'India-Finance' || #session.sessionEmpPractice == 'US-Finance' || #session.userId == 'rijju' || #session.isAdminAccess==1}" >
                          <li><a   id="TimeSheetsauditReport" href="/<%=ApplicationConstants.CONTEXT_PATH%>/employee/timesheets/timesheeAuditReport.action" onClick="leftMenuToggleId(this,'dashboardsAdmin');"><span>TimeSheet&nbsp;Report</span></a></li>
                        </s:if>
                        
                       
                         <s:if test="%{ #session.userId == 'rijju' || #session.userId == 'vkatta1' || #session.isAdminAccess == 1}" >
                         <li><a id="getRecruitmentExecDashboard" href="/<%=ApplicationConstants.CONTEXT_PATH%>/employee/timesheets/getRecruitmentExecDashboard.action"  onClick="leftMenuToggleId(this,'dashboardsAdmin');"><span>Recruitment&nbsp;DashBoard</span></a></li>
                          </s:if>
                          
                           <li><a id="immigrationReport" href="/<%=ApplicationConstants.CONTEXT_PATH%>/reports/immigrationReport.action" onClick="leftMenuToggleId(this,'servicesAdmin');"> <span>Immigration&nbsp;Report&nbsp;</span></a></li>
                        
                    </ul>
                </li>
                
            </ul>

        </div>

    </body>
</html>