<%-- 
    Document   : ReleaseNotes
    Created on : Sept, 2017, 10:33:55 PM
    Author     : miracle
--%>

<%@page import="java.util.Map"%>
<%@ page contentType="text/html; charset=UTF-8" errorPage="../exception/ErrorDisplay.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags" %> 
<%-- <%@ taglib prefix="sx" uri="/struts-dojo-tags" %> --%>
<%@ page import="com.mss.mirage.util.ApplicationConstants"%>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="com.mss.mirage.util.ConnectionProvider"%>
<html>
    <head>
        <title>Hubble Organization Portal :: DashBoard</title>
        <link rel="stylesheet" type="text/css" href="<s:url value="/includes/css/GridStyle.css?version=1.0"/>">
        <link rel="stylesheet" type="text/css" href="<s:url value="/includes/css/mainStyle.css"/>">
        <link rel="stylesheet" type="text/css" href="<s:url value="/includes/css/tabedPanel.css"/>">
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/animatedcollapse.js"/>"></script>
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/jquery-1.2.2.pack.js"/>"></script>
        <link rel="stylesheet" type="text/css" href="<s:url value="/includes/css/leftMenu.css"/>">
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/CalendarTime.js"/>"></script>
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/AppConstants.js"/>"></script>
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/EnableEnter.js"/>"></script>
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/tabedPanel.js"/>"></script>
    <%--    <script type="text/JavaScript" src="<s:url value="/includes/javascripts/PreSalesDashBoard.js?version=1.2"/>"></script>  --%>
<script type="text/JavaScript" src="<s:url value="/includes/javascripts/projectProtofolioReport.js?version=1.2"/>"></script>

        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/employee/DateValidator.js"/>"></script>
        <s:include value="/includes/template/headerScript.html"/>
<script type="text/JavaScript" src="<s:url value="/includes/javascripts/reviews/jquery.min.js"/>"></script>
<script type="text/JavaScript" src="<s:url value="/includes/javascripts/ReleaseNotes.js"/>"></script> 

    </head>
     <s:if test="%{currentAction == 'releaseNotesAdd'}">
    <body  class="bodyGeneral"  oncontextmenu="return false;">
    </s:if>
    <s:else>
        <body  class="bodyGeneral" onload="getValuesFunction();getRoleValues();"  oncontextmenu="return false;">
    </s:else>

   
        <%!
            /* Declarations */
            String queryString = null;
            Connection connection;
            Statement stmt;
            ResultSet rs;
            int userCounter = 0;
            int activityCounter = 0;
            int accountCounter = 0;
        %>


        <!--//START MAIN TABLE : Table for template Structure-->
        <table class="templateTable1000x580" align="center" cellpadding="0" cellspacing="0">

            <!--//START HEADER : Record for Header Background and Mirage Logo-->
            <tr class="headerBg">
                <td valign="top">
                    <s:include value="/includes/template/Header.jsp"/>                    
                </td>
            </tr>
            <!--//END HEADER : Record for Header Background and Mirage Logo-->

            <!--//START DATA RECORD : Record for LeftMenu and Screen Content-->
            <tr>
                <td>
                    <table class="innerTable1000x515"  cellpadding="0" cellspacing="0">
                        <tr>
                            <!--//START DATA COLUMN : Coloumn for LeftMenu-->
                            <td width="850px;" class="leftMenuBgColor" valign="top"> 
                                <s:include value="/includes/template/LeftMenu.jsp"/>
                            </td>
                            <!--//START DATA COLUMN : Coloumn for LeftMenu-->

                            <!--//START DATA COLUMN : Coloumn for Screen Content-->
                            <td width="850px" class="cellBorder" valign="top" style="padding-left:10px;padding-top:5px;">
                                <!--//START TABBED PANNEL : -->
                                <%--      <sx:tabbedpanel id="resetPasswordPannel" cssStyle="width: 845px; height: 550px;padding-left:10px;padding-top:5px;" doLayout="true" useSelectedTabCookie="true" > 
                                    
                                    <!--//START TAB : -->
                                    <sx:div id="dashBoardTab" label="DashBoard Details" cssStyle="overflow:auto;"> --%>
                                <ul id="accountTabs" class="shadetabs" >
                                    <li><a href="#" rel="ReleaseListTab" class="selected">&nbsp;Release Notes Add</a></li>

                                </ul>
                               <div  style="border:1px solid gray; width:840px;height: 500px; overflow:auto; margin-bottom: 1em;"> 
                                    <div id="ReleaseListTab" class="tabcontent" >  
                                          <% for(int ii=1;ii<=6;ii++){
                                            String subTitle="subTitle"+ii;
                                            String purpose="purpose"+ii;
                                            String file="file"+ii;
                                            String subId="subId"+ii;
                                       String fileName="file"+ii+"FileName";
                                        String filePath="file"+ii+"FilePath";
                                       
                                          String subTitle1="subTitlex"+ii;
                                            String purpose1="purposex"+ii;
                                            String file1="filex"+ii;
                                            String subId1="subIdx"+ii;
                                       String fileName1="filex"+ii+"FileName";
                                       String filePath1="filex"+ii+"FilePath";
                                 //   out.println("filePath1---->"+filePath1);
                                         String newFilePathStorage="newFilePathStorage"+ii;
                                        %>
                                        <input type="hidden" name="<%=subTitle%>" id="<%=subTitle1%>" value="<%=request.getAttribute(subTitle)%>"/>
                                        <input type="hidden" name="<%=purpose%>" id="<%=purpose1%>" value="<%=request.getAttribute(purpose)%>"/>
                                        <input type="hidden" name="<%=file%>" id="<%=file1%>" value="<%=request.getAttribute(file)%>"/>
                                        <input type="hidden" name="<%=subId%>" id="<%=subId1%>" value="<%=request.getAttribute(subId)%>"/>
                                        <input type="hidden" name="<%=fileName%>" id="<%=fileName1%>" value="<%=request.getAttribute(fileName)%>"/>
                                        <input type="hidden" name="<%=filePath%>" id="<%=filePath1%>" value="<%=request.getAttribute(filePath)%>"/>
                                        <!--<input type="hidden" name="<%=newFilePathStorage%>" id="<%=newFilePathStorage%>" value=""/>-->
                                     <%}%>
                                 <%--       <input type="text" name="harshaa" id="harshaa" value=""/>  --%>
                                       <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                     <%--       <tr>
                                                <td height="20px" >

                                                </td>
                                            </tr>  --%>
                                           <tr>
                                                <td class="headerText">
                                                    <img alt="Home" src="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/spacer.gif" width="100%" height="13px" border="0">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <% if(request.getAttribute(ApplicationConstants.RESULT_MSG) != null){
                                                        out.println(request.getAttribute(ApplicationConstants.RESULT_MSG));
                                                    }
                                                    %>
                                                </td>
                                       </tr>
                                      
                                            <tr>
                                                                <td width="80%"  align="center">
                                                                    
                                                                 
                                                                    <s:form action="%{currentAction}" theme="simple" id="releaseNotesuploadForm" enctype="multipart/form-data" name="releaseNotesuploadForm"  onSubmit="return checkSubmissionValuesUpdate();">   

                                                                        <table cellpadding="2" id="releaseNotesuploadTable" align="center" cellspacing="0" style="margin: 20px;" border="0" width="80%">
                                                                        <s:hidden name="currentAction" id="currentAction" value="%{currentAction}"/>  
                                <s:if test="%{currentAction == 'releaseNotesAdd'}">
                               <tr>
                                             
<!--                                            <td colspan="2"></td>-->
                                            <td align="right">
                                                <s:submit id="save" value="Save" cssClass="buttonBg" onClick="return checkSubmissionValuesAdd();"/></td>
                                           
                                </tr>
                                                                        </s:if>
                                                                        <s:else>
                                                                             <tr>
                                        <%--     
                                           <%if (request.getAttribute(ApplicationConstants.RESULT_MSG) != null) {
                                       out.println(request.getAttribute(ApplicationConstants.RESULT_MSG));
                                   }%>  --%>
                                            <td align="right"><s:submit id="Update" value="Update" cssClass="buttonBg" onClick="return checkSubmissionValuesUpdate();"/></td>
                                                                        </tr>
                                                                        </s:else>
                                
                                                                            <tr id="titleRow">
                                                                             <td>
                                                                             <table cellpadding="2" cellspacing="1" border="0">
                                                                                <tr>
                                                                                    <s:hidden name="minusHiden" id="minusHiden" value="%{minusHiden}"/>
                                                                               <td class="fieldLabel" style="width: 49px;">Title :</td>&nbsp;
                                                                               <td><s:textfield name="title" id="title" value="%{title}" cssClass="inputTextBlue" theme="simple"/></td>
                                                                                  <td class="fieldLabel" style="width: 44px;">Date:</td>
                                                                                <td><s:textfield name="date" id="date" value="%{date}" cssClass="inputTextBlue" theme="simple" /><a class="underscore" href="javascript:cal5.popup();">
                                                                               <img src="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/showCalendar.gif" width="20" height="20" border="0"></a>
                                                                                     </td>
                                                                                       <td class="fieldLabel" style="width: 49px;">Role&nbsp;Access&nbsp;:</td>&nbsp;
                                                                                       <td><s:hidden name="roleDetailsList" id="roleDetailsList" value="%{roleDetailsList}" cssClass="inputTextBlue" theme="simple" />
                                                                               <td><s:select list="roleDetails" id="roleAccess" name="roleAccess" cssClass="inputSelect" value="%{#roleDetailsList}" multiple="true"/></td>
                                                                                    </tr>
                                                                             </table>
                                                                                 
                                                                         </td>
                                                                         </tr>
                                                                       <tr id="rowName0">
                                                                        <td>
                                                                            <table cellpadding="2"  cellspacing="1" border="0">
                                                                           <tr>
                                                                         <td class="fieldLabel">Sub&nbsp;Title&nbsp;:</td>
                                                                     <td><s:textfield name="subTitle0" id="subTitle0" value="%{subTitle0}" cssClass="inputTextBlue" theme="simple"/></td>
                                                                <td class="fieldLabel">Purpose:</td>
                                                                <td><s:textarea style='width: 300px; height: 50px;' name="purpose0"  value="%{purpose0}" cssClass="inputTextarea" id="purpose0" />
                                                                                </td>
                                                                                <s:hidden name="subId0" id="subId0" value="%{subId0}"/>
                                                                                
                                                                                 <div id="resultMessage"></div>
                                                                        <td class="fieldLabel">Upload:</td>                                
                                                                        <td align="left"><input type="file" name="file0" label="file" cssClass="inputTextarea" id="file0"  style='width: 75px;color: transparent;' onchange="accountExcelFileValidation(this);putFileName(this);" theme="simple"/></td> 
                                                                        <%--   <td><a href='#' onclick="getFileUpload();"><img SRC='../includes/images/uploadicon.jpg' WIDTH=25 HEIGHT=21 BORDER=0 ALTER='' title="Upload Attachment" style="margin-bottom: -1px;padding-top:4px;"/></a></td>  --%>
                                                                            <td style="color: blue;font-family: lucida-sans;font-size: 14px;height: 1.7em;margin: 0;"><%-- <s:label value="%{file0FileName}" /></td>--%>
                                                                          <%if (request.getAttribute("file0FileName") != null && !"".equals(request.getAttribute("file0FileName"))) {%>
                                                    <a href="<s:url action="downloadReleaseNotesAttachment.action">
                                              <s:param name="subId" value="%{subId0}"/>
                                              <s:param name="releaseId" value="%{releaseId}"/>
                                              <s:param name="file0FileName" value="%{file0FileName}"/>
                                              </s:url>"><img width="23" height="22" border="0" alt="download" src="../includes/images/Download.png" title="Click here to download" ></a>
                                           <%  } %>   
                                                                       <%--    <td><input type="button" value="Add File" class="buttonBg" onclick="getFileUpload()"/></td> --%>
                                                                           
                                                                           
                                                                           <td id="buttonHide" name="buttonHide" colspan="7"><a id="plus" name="plus" href="javascript:showFunction();" onClick="return checkFields();" style="align:center;">
                                                            <img alt="Show" id="pluss0" name="pluss0"
                                                                 src="<s:url value="/includes/images/plus_sign.png"/>" 
                                                                 width="18px" 
                                                                 height="21px"
                                                                 border="0" style="margin-bottom:-2px" align="center"></a>
                                                       
                                                       </td>
                                                       
                                                       <td id="buttonMinus" name="buttonMinus" colspan="7"><a id="minus" name="minus" href="javascript:hideFunction();" style="align:center;">
                                                            <img alt="Show" id="minus0" name="minus0"
                                                                 src="<s:url value="/includes/images/minus_sign.png"/>" 
                                                                 width="20px" 
                                                                 height="16px"
                                                                 style="display: none;"
                                                                 border="0" align="center"></a>
                                                    <td><s:hidden name="counter" id="counter" value="%{isInsertedValueCount}" cssClass="inputTextBlue" theme="simple"/></td>
                                                    <td><s:hidden name="columnCountValue" id="columnCountValue" value="%{columnCountValue}" cssClass="inputTextBlue" theme="simple"/></td>
                                                       <td><s:hidden name="count" id="count" value="%{count}" cssClass="inputTextBlue" theme="simple"/></td>
                                                       <td><s:hidden name="releaseId" id="releaseId" value="%{releaseId}" cssClass="inputTextBlue" theme="simple"/></td>
                                                       <td><s:hidden name="file0FileName" id="filex0FileName" value="%{file0FileName}" cssClass="inputTextBlue" theme="simple"/></td>
                                                       <td><s:hidden name="file0FilePath" id="file0FilePath" value="%{file0FilePath}" cssClass="inputTextBlue" theme="simple"/></td>
                                         <%--     <s:else>
                                              <a href="<s:url action="downloadEmployeeResume.action">
                                              <s:param name="id" value="%{currId}"/>
                                              <s:param name="empId" value="%{empId}"/>
                                              <s:param name="fileName" value="%{currentEmployee.resumeName}"/>
                                              </s:url>"><img width="18" height="14" border="0" alt="download" src="../includes/images/download_11x10.jpg" title="Click here to download"></a>
                                              </s:else>  --%>
                                                       </td>
                                                       
                                                                            </tr>   
                                              
                                                                            </table>  
                                                                        </td>
                                                                    </tr>
                                                                   <%-- dynamic fields generation start--%>
                                                                   
                                                                   
                                                                   
                                                                    <%-- dynamic fields generation end--%>
                                                                        </table>
                                                                         </td>
                                                                         </s:form>
                                     
                                       </table> 
                                        
                                                               
                                                       </tr>
                                                                     
                                                                                    
                                                                      <script type="text/JavaScript">
                                                                        var cal5 = new CalendarTime(document.forms['releaseNotesuploadForm'].elements['date']);
                                                                        cal5.year_scroll = true;
                                                                        cal5.time_comp = false;
                                                                     </script>
                                    </div>
                                </div>
                                <script type="text/javascript">

                                    var countries=new ddtabcontent("accountTabs")
                                    countries.setpersist(false)
                                    countries.setselectedClassTarget("link") //"link" or "linkparent"
                                    countries.init()

                                </script>

                            </td>
                            <!--//END DATA COLUMN : Coloumn for Screen Content-->
                        </tr>
                    </table>
                </td>
            </tr>
            <!--//END DATA RECORD : Record for LeftMenu and Body Content-->

            <!--//START FOOTER : Record for Footer Background and Content-->
            <tr class="footerBg">
                <td align="center"><s:include value="/includes/template/Footer.jsp"/></td>
            </tr>
            
          
            <!--//END FOOTER : Record for Footer Background and Content-->
        </table>
        <!--//END MAIN TABLE : Table for template Structure-->
        
  <%--      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>  --%>
    </body>

</html>

