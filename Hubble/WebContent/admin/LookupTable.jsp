 <%@ page contentType="text/html; charset=UTF-8" errorPage="../exception/ErrorDisplay.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%--<%@ taglib prefix="sx" uri="/struts-dojo-tags" %> --%>
<%@ page import="com.freeware.gridtag.*" %> 
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="com.mss.mirage.util.ConnectionProvider"%>
<%@ page import="com.mss.mirage.util.ApplicationConstants"%>
<%@ taglib uri="/WEB-INF/tlds/datagrid.tld" prefix="grd" %>
<html>
    <head>
        <title>Hubble Organization Portal :: Employee Search</title>
    
    <link rel="stylesheet" type="text/css" href="<s:url value="/includes/css/mainStyle.css"/>">
    <link rel="stylesheet" type="text/css" href="<s:url value="/includes/css/GridStyle.css"/>">
    <link rel="stylesheet" type="text/css" href="<s:url value="/includes/css/leftMenu.css"/>">
    <link rel="stylesheet" type="text/css" href="<s:url value="/includes/css/tabedPanel.css"/>">
    <script type="text/JavaScript" src="<s:url value="/includes/javascripts/DBGrid.js"/>"></script>
    <script type="text/JavaScript" src="<s:url value="/includes/javascripts/ClientValidations.js"/>"></script>
    <script type="text/JavaScript" src="<s:url value="/includes/javascripts/clientValidations/EmpSearchClientValidation.js"/>"></script>
    <script type="text/JavaScript" src="<s:url value="/includes/javascripts/AppConstants.js"/>"></script>
    <script type="text/JavaScript" src="<s:url value="/includes/javascripts/tabedPanel.js"/>"></script>
    <script type="text/JavaScript" src="<s:url value="/includes/javascripts/CalendarTime.js"/>"></script>
    <script type="text/JavaScript" src="<s:url value="/includes/javascripts/reviews/jquery.min.js"/>"></script>
    <script type="text/javascript" src="<s:url value="/includes/javascripts/reviews/jquery.js"/>"></script>   
<script type="text/JavaScript" src="<s:url value="/includes/javascripts/LookUpMgmnt.js?version=3.5"/>"></script>
   <link rel="stylesheet" type="text/css" href="<s:url value="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css"/>">
    
    <script>
   function editLookUp(tableName){
	//   alert("hiieditLookUp");
	  
	   var tableId=tableName;
	   window.location = "getLookUpColumnData.action?tableName="+tableId;
	   
   }
   
   


   function checkName()

      {

          var accountId = document.getElementById("tableId").value=""; 

          if(accountId=="" || accountId==null){

              //  alert(" Please select customer from suggestion list.");

              document.getElementById("tableName").value = "";

          }

      }


    </script>
    <s:include value="/includes/template/headerScript.html"/>
</head>
<!-- <body class="bodyGeneral" onload="getRequirementClosed()"> -->
<body class="bodyGeneral">

    <%!
        /* Declarations */
        Connection connection;
        String queryString;
        String strTmp;
        String strSortCol;
        String strSortOrd;
        String submittedFrom;
        String searchSubmitValue;
        int intSortOrd = 0;
        int intCurr;
        boolean blnSortAsc = true;
    %>

    <!--//START MAIN TABLE : Table for template Structure-->
    <table class="templateTableLogin" align="center" cellpadding="0" cellspacing="0">

        <!--//START HEADER : Record for Header Background and Mirage Logo-->
        <tr class="headerBg">
            <td valign="top">
                <s:include value="/includes/template/Header.jsp"/>                    
            </td>
        </tr>
        <!--//END HEADER : Record for Header Background and Mirage Logo-->


        <!--//START DATA RECORD : Record for LeftMenu and Screen Content-->
        <tr>
            <td>
                <table class="innerTableLogin" cellpadding="0" cellspacing="0">
                    <tr>

                        <!--//START DATA COLUMN : Coloumn for LeftMenu-->
                        <td width="150px;" class="leftMenuBgColor" valign="top">
                            <s:include value="/includes/template/LeftMenu.jsp"/>
                        </td>

                        <!--//END DATA COLUMN : Coloumn for LeftMenu-->

                        <!--//START DATA COLUMN : Coloumn for Screen Content-->
                        <td width="850px" class="cellBorder" valign="top" style="padding: 5px 5px 5px 5px;">
                            <!--//START TABBED PANNEL : -->
                            <ul id="accountTabs" class="shadetabs" >
                                <li ><a href="#" class="selected" rel="employeeSearchTab"  >LookUp Management </a></li>
                            </ul>
                            <%--<sx:tabbedpanel id="employeeSearchPannel" cssStyle="width: 840px; height: 500px;padding: 5px 5px 5px 5px;" doLayout="true"> --%>
                            <div  style="border:1px solid gray; width:840px;height: 500px; overflow:auto; margin-bottom: 1em;">   
                                <!--//START TAB : -->
                                <%--  <sx:div id="employeeSearchTab" label="Employee Search" cssStyle="overflow:auto;"  > --%>
                                <div id="employeeSearchTab" class="tabcontent"  >

                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                        <tr align="right">
                                            <td class="headerText">
                                                <img alt="Home" 
                                                     src="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/spacer.gif" 
                                                     width="100%" 
                                                     height="13px" 
                                                     border="0"/>
                                                <%if (request.getAttribute(ApplicationConstants.RESULT_MSG) != null) {
                                       out.println(request.getAttribute(ApplicationConstants.RESULT_MSG));
                                   }%>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>

                                                <s:form name="frmEmpSearch" id="frmEmpSearch" action=" " theme="simple">
                                                
                                                    <table cellpadding="1" cellspacing="1" border="0" width="650px" align="center">
                                                       
                                                           
                                                        <tr>
                                                                <td class="fieldLabel">Table Name&nbsp;:</td>
                                                                <td colspan="2">  
                                                                    <s:textfield name="tableName" id="tableName" onchange="checkName();" cssClass="inputTextBlueLarge"  style="width:83%"  value="%{tableName}"  theme="simple" onkeyup="fillCustomer();"/>
                                                                    <div id="validationMessage"></div>
                                                                    <s:hidden name="tableId" id="tableId" value="%{tableId}" /> 

                                                                </td>
                                                                <td>
                                                                    <input type="button" value="Search" class="buttonBg" onclick="getTablesList();" />
<!--                                                                <input type="button" value="Add" class="buttonBg" onclick="addProjectToCustomer();" /> -->
                                                            
                                                                </td>
                                                              


                                                            </tr>
                                                       
                                                        
                                                    </table>
                                                 
                                                </s:form>
                                            </td>

                                        </tr>
                                        <tr>
                                            <td>
                                                <%

                                                    if (request.getAttribute("submitFrom") != null) {
                                                        submittedFrom = request.getAttribute("submitFrom").toString();
                                                    }

                                                    if (!"dbGrid".equalsIgnoreCase(submittedFrom)) {
                                                        searchSubmitValue = submittedFrom;
                                                    }

                                                %>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <%
                                                    /* String Variable for storing current position of records in dbgrid*/
                                                    strTmp = request.getParameter("txtCurr");

                                                    intCurr = 1;

                                                    if (strTmp != null) {
                                                        intCurr = Integer.parseInt(strTmp);
                                                    }

                                                    /* Specifing Shorting Column */
                                                    strSortCol = request.getParameter("Colname");

                                                    if (strSortCol == null) {
                                                        strSortCol = "Fname";
                                                    }

                                                    strSortOrd = request.getParameter("txtSortAsc");
                                                    if (strSortOrd == null) {
                                                        strSortOrd = "ASC";
                                                    }


                                                    try {

                                                        /* Getting DataSource using Service Locator */
                                                        connection = ConnectionProvider.getInstance().getConnection();

                                                     

                                                        /* Sql query for retrieving resultset from DataBase */
                                                        /*queryString  =null;*/
                                                        //queryString = "SELECT ReviewType FROM tblEmpReview JOIN tblLkReviews ON (ReviewTypeId = tblLkReviews.Id)";
                                                      
                                                        if (request.getAttribute(ApplicationConstants.QUERY_STRING) != null) {
                                                            queryString = request.getAttribute(ApplicationConstants.QUERY_STRING).toString();
                                                        }


                                                  //    out.println(queryString);

                                                       
%>

                                                <s:form action="" theme="simple" name="frmDBGrid">   

                                                    <table cellpadding="0" cellspacing="0" width="100%" border="0">


                                                        <tr>
                                                            <td width="100%">

                                                               
                                                                <grd:dbgrid id="tblStat" name="tblStat" width="98" pageSize="12"
                                                                currentPage="<%=intCurr%>" border="0" cellSpacing="1" cellPadding="2"
                                                                dataMember="<%=queryString%>" dataSource="<%=connection%>" cssClass="gridTable">

                                                                    <%--    <grd:gridpager imgFirst="../../includes/images/DBGrid/First.gif" imgPrevious="../../includes/images/DBGrid/Previous.gif" 
                                                                                       imgNext="../../includes/images/DBGrid/Next.gif" imgLast="../../includes/images/DBGrid/Last.gif"
                                                                                       addImage="../../includes/images/DBGrid/Add.png"  addAction="<%=empReviewAction %>"
                                                                                       /> --%>
                                                                    <grd:gridpager imgFirst="../includes/images/DBGrid/First.gif" imgPrevious="../includes/images/DBGrid/Previous.gif" 
                                                                                   imgNext="../includes/images/DBGrid/Next.gif" imgLast="../includes/images/DBGrid/Last.gif"
                                                                                   />            

                                                                    <grd:gridsorter sortColumn="<%=strSortCol%>" sortAscending="<%=blnSortAsc%>" 
                                                                                    imageAscending="../includes/images/DBGrid/ImgAsc.gif" 
                                                                                    imageDescending="../includes/images/DBGrid/ImgDesc.gif"/>   
                                                                   <grd:rownumcolumn headerText="SNo" width="5" HAlign="right"/>
                                                                    <grd:anchorcolumn dataField="TableName" 
                                                                                      headerText="Table Name" 
                                                                                      linkUrl="javascript:editLookUp('{TableName}')" linkText="{TableName}" width="20"/>
                                                                  

                                                                  
                                                                    <grd:textcolumn dataField="Status" headerText="Status" width="18"/> 
                                                                     <grd:textcolumn dataField="CreatedBy" headerText="CreatedBy" width="18"/> 
                                                                   <grd:datecolumn dataField="CreatedDate"  headerText="CreatedDate" dataFormat="MM-dd-yyyy" width="15" /> 


                                                                   
                                                                    <%--<grd:imagecolumn headerText="Delete" width="4" HAlign="center" 
                                                                                     imageSrc="../../includes/images/DBGrid/Delete.png"
                                                                                     linkUrl="deleteReview.action?reviewId={Id}" imageBorder="0"
                                                                                     imageWidth="51" imageHeight="20" alterText="Delete"></grd:imagecolumn> 
--%>


                                                                </grd:dbgrid>
                                                               



                                                                <input type="hidden" name="txtCurr" value="<%=intCurr%>">
                                                                <input type="hidden" name="txtSortCol" value="<%=strSortCol%>">
                                                                <input type="hidden" name="txtSortAsc" value="<%=strSortOrd%>">

                                                                <input type="hidden" name="submitFrom" value="dbGrid">
                                                             
                                                            </td>
                                                        </tr>
                                                    </table>                                

                                                </s:form>

                                                <%
                                                        connection.close();
                                                        connection = null;
                                                    } catch (Exception ex) {
                                                        out.println(ex.toString());
                                                    } finally {
                                                        if (connection != null) {
                                                            connection.close();
                                                            connection = null;
                                                        }
                                                    }
                                                %>
                                            </td>
                                        </tr>
                                    </table>

                                    <!-- End Overlay -->
                                    <!-- Start Special Centered Box -->

                                    <%-- </sx:div > --%>
                                </div>
                                <!--//END TAB : -->
                                <%-- </sx:tabbedpanel> --%>
                            </div>
                            <script type="text/javascript">

                                var countries=new ddtabcontent("accountTabs")
                                countries.setpersist(false)
                                countries.setselectedClassTarget("link") //"link" or "linkparent"
                                countries.init()

                            </script>
                            <!--//END TABBED PANNEL : -->
                        </td>
                        <!--//END DATA COLUMN : Coloumn for Screen Content-->
                    </tr>
                </table>
            </td>
        </tr>
        <!--//END DATA RECORD : Record for LeftMenu and Body Content-->

        <!--//START FOOTER : Record for Footer Background and Content-->
        <tr class="footerBg">
            <td align="center"><s:include value="/includes/template/Footer.jsp"/></td>
        </tr>
        
         <tr>
            <td>

                <div style="display: none; position: absolute; top:105px;left:623px;overflow:auto;" id="menu-popup">
                    <table id="completeTable" border="1" bordercolor="#e5e4f2" style="border: 1px dashed gray;" cellpadding="0" class="cellBorder" cellspacing="0" />
                </div>

            </td>
        </tr>  
        
        <!--//END FOOTER : Record for Footer Background and Content-->

    </table>
    <!--//END MAIN TABLE : Table for template Structure-->


</body>
</html>

