<!-- 
 *******************************************************************************
 *
 * Project : New Executive Dashboard in Admin Role
 *
 * Package : admin
 *
 * Date    :  May 11 2017
 *
 * Author  : Triveni Niddara
 *
 * File    : NewExecutiveDashboard.jsp
 *
 * Copyright 2007 Miracle Software Systems, Inc. All rights reserved.
 * MIRACLE SOFTWARE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * *****************************************************************************
 */
-->
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@ page contentType="text/html; charset=UTF-8" errorPage="../exception/ErrorDisplay.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags" %> 
<%-- <%@ taglib prefix="sx" uri="/struts-dojo-tags" %> --%>
<%@ page import="com.mss.mirage.util.ApplicationConstants"%>

<html>
    <head>
        <title>Hubble Organization Portal :: Executive DashBoard</title>
        <link rel="stylesheet" type="text/css" href="<s:url value="/includes/css/GridStyle.css"/>">
        <link rel="stylesheet" type="text/css" href="<s:url value="/includes/css/mainStyle.css?version=2.1"/>">
        <link rel="stylesheet" type="text/css" href="<s:url value="/includes/css/tabedPanel.css"/>">
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/animatedcollapse.js"/>"></script>
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/jquery-1.2.2.pack.js"/>"></script>
        <link rel="stylesheet" type="text/css" href="<s:url value="/includes/css/leftMenu.css"/>">
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/CalendarTime.js"/>"></script>
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/AppConstants.js"/>"></script>
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/tabedPanel.js"/>"></script>
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/admin/NewExecutiveDashboard.js?version=1.2"/>"></script>
        <link rel="stylesheet" type="text/css" href="<s:url value="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css"/>">

      
        
        <script type="text/javascript" src="https://www.google.com/jsapi"></script> 
        <s:include value="/includes/template/headerScript.html"/>
        <script language="JavaScript">
            google.load("visualization", "1", {packages:["corechart"]});   
             animatedcollapse.addDiv('EmplyeeCountDiv', 'fade=1;persist=1;group=app');
            animatedcollapse.init();
            
             function hideSelect(){
                //document.getElementById("priorityId").style.display = 'none';
                
            }
        </script>

        <style type="text/css">
            .rcorners2 {
                border-radius: 25px;
                border: 2px solid #3e93d4;
                padding: 20px; 
                width: auto;
                height: auto;    
            }   
            .popupItem:hover {
                background: #F2F5A9;
                font: arial;
                font-size:10px;
                color: black;
            }

            .popupRow {
                background: #3E93D4;
            }

            .popupItem {
                padding: 2px;
                width: 100%;
                border: black;
                font:normal 9px verdana;
                color: white;
                text-decoration: none;
                line-height:13px;
                z-index:100;
            }

        </style>

    </head>
    <!--  <body  class="bodyGeneral" onload="hideSelect();javascript:animatedcollapse.show('EmplyeeCountDiv');getEmpolyeeCount();" oncontextmenu="return false;"> -->
    <body  class="bodyGeneral" oncontextmenu="return false;">



        <!--//START MAIN TABLE : Table for template Structure-->
        <table class="templateTable1000x580" align="center" cellpadding="0" cellspacing="0">

            <!--//START HEADER : Record for Header Background and Mirage Logo-->
            <tr class="headerBg">
            <td valign="top">
                <s:include value="/includes/template/Header.jsp"/>                    
            </td>
        </tr>
        <!--//END HEADER : Record for Header Background and Mirage Logo-->

        <!--//START DATA RECORD : Record for LeftMenu and Screen Content-->
        <tr>
        <td>
            <table class="innerTable1000x515" cellpadding="0" cellspacing="0">
                <tr>
                    <!--//START DATA COLUMN : Coloumn for LeftMenu-->
                <td width="850px;" class="leftMenuBgColor" valign="top"> 
                    <s:include value="/includes/template/LeftMenu.jsp"/>
                </td>
                <!--//START DATA COLUMN : Coloumn for LeftMenu-->

                <!--//START DATA COLUMN : Coloumn for Screen Content-->
                <td width="850px" class="cellBorder" valign="top" style="padding-left:10px;padding-top:5px;">
                    <!--//START TABBED PANNEL : -->
                        
                        <!--//START TAB : -->
                      
                    <ul id="reportsTab" class="shadetabs" >
                        <li><a href="#" rel="EmpReportsTab" class="selected">Executive DashBoard</a></li>

                    </ul>
                    <div  style="border:1px solid gray; width:840px;height:675px;overflow:auto; margin-bottom: 1em;">    
                        <br>
                        <div id="EmpReportsTab" class="tabcontent" >  


                            <table cellpadding="0" cellspacing="0" border="0" width="100%">



                                <tr>
                                <td class="homePortlet" valign="top">
                                  <%-- <div class="portletTitleBar">
                                        <div class="portletTitleLeft">Executive DashBoard</div>
                                        <div class="portletIcons">
                                            <a href="javascript:animatedcollapse.hide('EmplyeeCountDiv')" title="Minimize">
                                                <img src="../includes/images/portal/title_minimize.gif" alt="Minimize"/></a>
                                            <a href="javascript:animatedcollapse.show('EmplyeeCountDiv')" title="Maximize">
                                                <img src="../includes/images/portal/title_maximize.gif" alt="Maximize"/>
                                            </a>
                                        </div>
                                        <div class="clear"></div>
                                    </div> --%>
                                   <div style="background-color:#ffffff">
                                   <table cellpadding="0" cellspacing="0"  border="0" width="100%"> 
                                      
                                       <tr>
    <td class="fieldLabel">Select Role :</td>
    <td><s:select list="rolesMap" cssClass="inputSelect" onchange="getSelectedRoleDashboard(this.value);"/></td>
</tr>

                                       </table>
</div>
                                    <div id="AdminDashboard" >
                                        <table cellpadding="0" cellspacing="0" border="0" width="100%"> 
                                            <tr>
                                            <td width="40%" valign="top" align="center">
                                                <s:form action="employeeCount" theme="simple" name="employeeCount" id="employeeCount">  

                                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                        <tr align="right">
                                                        <td class="headerText" colspan="9">
                                                            <img src="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/spacer.gif" width="100%" height="13px" border="0">                                                        
                                                        </td>
                                                        </tr>

                                                        <tr>
                                                        <td>

                                                        </td>
                                                        </tr>

                                                       
                                                        <tr>
                                                        <td>
                                                        <div id="EmplyeeCountDiv">
                                                            <table align="center" cellpadding="2" border="0" cellspacing="1" width="100%" >
                                                                <!-- <tr>
                                                                <td class="portletTitleLeft"  style="color: green; font-size:14px">Employee Details</td>
                                                                </tr> -->  

                                                                <!-- Country Vs Count Stacked Chart Start  -->                                                                                  

                                                                <tr>
                                                                <td height="20px" align="center" colspan="9">
                                                                    <div id="loadActMessageAS" style="display:none" class="error" ><b></b></div>
                                                                    <div id="loadEmployeeCount" style="display:none" class="error" ><b>Loading Please Wait.....</b></div>
                                                                </td>
                                                                </tr>
                                                                <tr id="employeeStackedGraphIdTr">
                                                                <td colspan="2">
                                                                    <div class="rcorners2" id="employeeCountryVsCountStack" style="display:none;width: 692px;margin-left:10px;margin-top: 10px;" >
                                                                        <div id="employeeCountryVsCountChart" style="width: 550px; height: 350px;"></div>
                                                                    </div>
                                                                </td>
                                                                </tr>

                                                                <!-- Country Vs Count Stacked Chart End  -->   
                                                                
                                                                
                                                                <tr>
                                                                    <td>
                                                                    <table align="center" cellpadding="0" border="0" cellspacing="0" width="100%" >
                                                                        <tr>
                                                                            <td height="20px" align="center" >
                                                                    <div id="loadPracticePie" style="display:none" class="error" ><b>Loading Please Wait.....</b></div>
                                                                </td>
                                                                  <td height="20px" align="center">
                                                                    <div id="loadEmpSubPracticePie" style="display:none" class="error" ><b>Loading Please Wait.....</b></div>
                                                                </td>
                                                                        </tr>
                                                               <tr>
                                                                <td>
                                                                    <div class="rcorners2" id="empPracticeCountPie" style="display:none;width: 310px;" >
                                                                        <div id="empPracticeCountPieChart" style="width: 250px; height: 300px;"></div>
                                                                    </div>
                                                                </td>
                                                               
  <td>
                                                                    <div class="rcorners2" id="EmpSubPracticePie" style="display:none;width: 310px;">
                                                                        <div id="EmpSubPracticePieChart" style="width: 250px; height: 300px;"></div>
                                                                    </div>
                                                                </td>
                                                                </tr>
                                                               
                                                             </table>
                                                             </td></tr>
                                                                <tr>
                                                                    <td>
                                                                <table align="center" cellpadding="2" border="0" cellspacing="1" width="100%" >
                                                                   
                                                                 <tr id="GDCSSGPieChartsLoad" >
                                                                      <td  height="20px" align="center" colspan="4">
                                                                            <div id="projectReport" style="display:none" class="error" ><b>Loading Please Wait.....</b></div>
                                                                        </td>
                                                                        <td  height="20px" align="center" colspan="4">
                                                                            <div id="projectReport1" style="display:none" class="error" ><b>Loading Please Wait.....</b></div>
                                                                        </td>
                                                                 </tr>
                                                                 <tr id="GDCSSGPieCharts" >
                                                                       
                                                                       
                                                                        <td>
                                                                            <div class="rcorners2" id="countVsStatusPie"  style="display:none;width: 310px;margin-left:10px;" >
                                                                                <div id="countVsStatusPieChart"style="width: 250px; height: 300px;"></div>
                                                                            </div>
                                                                        </td>
                                                                        
                                                                       
                                                                        <td>
                                                                            <div class="rcorners2" id="resourcesVsMainPie" style="display:none;width: 310px;;margin-left:10px;">
                                                                                <div id="resourcesVsMainPieChart" style="width: 250px; height: 300px;"></div>
                                                                            </div>
                                                                        </td>
                                                                        </tr>
                                                               </table></td></tr>
                                                                 <tr>
                                                                    <td>
                                                                <table align="center" cellpadding="2" border="0" cellspacing="1" width="100%" >
                                                               
                                                               <tr id="SalesPieChartsLoad">
                                                                      <td  height="20px" align="center" colspan="4" >
                                                                            <div id="empOppertunitiesLoad" style="display:none" class="error" ><b>Loading Please Wait.....</b></div>
                                                                        </td>
                                                                        <td  height="20px" align="center" colspan="4" >
                                                                            <div id="empRequirementLoad" style="display:none" class="error" ><b>Loading Please Wait.....</b></div>
                                                                        </td>
                                                                 </tr>
                                                                 <tr id="SalesPieCharts" >
                                                                       
                                                                       
                                                                        <td>
                                                                            <div class="rcorners2" id="empOppertunitiesPie"  style="display:none;width: 310px;margin-left:10px;" >
                                                                                <div id="empOppertunitiesPieChart"style="width: 250px; height: 300px;"></div>
                                                                            </div>
                                                                        </td>
                                                                        
                                                                       
                                                                        <td>
                                                                            <div class="rcorners2" id="empRequirementPie" style="display:none;width: 310px;;margin-left:10px;">
                                                                                <div id="empRequirementPieChart" style="width: 250px; height: 300px;"></div>
                                                                            </div>
                                                                        </td>
                                                                        </tr>
                                                                
                                                            
                                                                
                                                                 </table>
                                                                        </td>
                                                                </tr>
                                                                
                                                                <!-- Employee Table Start  --> 
                                                                 <tr>
                                                                <td height="20px" align="center" colspan="9">
                                                                    <div id="projectReportNote" style="display:none;color: blue"></div>
                                                                </td>
                                                                </tr>
                                                                <!-- new graph details start -->
                                                                <tr>
                                                                <td height="20px" align="center" colspan="9">
                                                                    <div id="projectReporssst" style="display:none" class="error" ><b>Loading Please Wait.....</b></div>
                                                                </td>
                                                                </tr>
                                                                <tr>
                                                                <td>
                                                                     <table id="tblReportDashBoard1" align='center' cellpadding='1' cellspacing='1' border='0' class="gridTable" width='100%'>
                                                                                                   
                                                                 
                                                       
    
                                              
                                                                    </table>  



                                                                </td>
                                                                </tr>
                                                                                           
                                                                                            </table>  
                                                                                            </div> 
                                                             </td>
                                                                                            </tr>
                                                                                            <!-- Employee Table End  --> 
                                                                                            <!--  Sales Start -->
                                                                                            <tr>
                                                                                            <td>
                                                                                            <div id="SalesCountDiv">
                                                                                             <table align="center" cellpadding="2" border="0" cellspacing="1" width="100%" >
                                                           <tr><td>
                                                                                               <table align="center" cellpadding="2" border="0" cellspacing="1" width="100%" >
                                                                                                 <tr id="SalesAccPieChartsLoad">
                                                                      <td  height="20px" align="center" colspan="4" >
                                                                            <div id="SalesAccStatusLoad" style="display:none" class="error" ><b>Loading Please Wait.....</b></div>
                                                                        </td>
                                                                        <td  height="20px" align="center" colspan="4" >
                                                                            <div id="SalesAccRegionLoad" style="display:none" class="error" ><b>Loading Please Wait.....</b></div>
                                                                        </td>
                                                                 </tr>
                                                                   <tr id="SalesAccPieCharts" >
                                                                         <td>
                                                                            <div class="rcorners2" id="salesAccStatusPie"  style="display:none;width: 310px;margin-left:10px;" >
                                                                                <div id="salesAccStatusPieChart"style="width: 250px; height: 300px;"></div>
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <div class="rcorners2" id="salesAccRegionPie" style="display:none;width: 310px;;margin-left:10px;">
                                                                                <div id="salesAccRegionPieChart" style="width: 250px; height: 300px;"></div>
                                                                            </div>
                                                                        </td>
                                                                        </tr>
                                                                                                             </table>
                                                                                            </td>
                                                                                            </tr>
                                                                                            
                                                                                             <tr>
                                                                                            <td>
                                                                                               <table align="center" cellpadding="2" border="0" cellspacing="1" width="100%" >
                                                                                                 <tr id="SalesAccPieChartsLoad">
                                                                      <td  height="20px" align="center" colspan="4" >
                                                                            <div id="SalesAccTerritoryLoad" style="display:none" class="error" ><b>Loading Please Wait.....</b></div>
                                                                        </td>
                                                                        <td  height="20px" align="center" colspan="4" >
                                                                            <div id="SalesAccStateLoad" style="display:none" class="error" ><b>Loading Please Wait.....</b></div>
                                                                        </td>
                                                                 </tr>
                                                                   <tr id="SalesAccountPieCharts" >
                                                                         <td>
                                                                            <div class="rcorners2" id="salesAccTerritoryPie"  style="display:none;width: 310px;margin-left:10px;" >
                                                                                <div id="salesAccTerritoryPieChart"style="width: 250px; height: 300px;"></div>
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <div class="rcorners2" id="salesAccStatePie" style="display:none;width: 310px;;margin-left:10px;">
                                                                                <div id="salesAccStatePieChart" style="width: 250px; height: 300px;"></div>
                                                                            </div>
                                                                        </td>
                                                                        </tr>
                                                                                                             </table>
                                                                                                              </td>
                                                                                            </tr>
                                                                                                 </table>            </div>
                                                                                            </td>
                                                                                            </tr>
                                                                                            <!--  Sales End -->
                                                                                            </table>    
                                                                                           
                                                                                        </s:form>
                                                 </td>
                                                                                            </tr>
                                                                                            </table>
                                         </div>
                                                                                        </td>
                                                                                        </tr>
                                                                                        </table>

                                                                                        </div>
                                    </div>
                                                                                        </td>
                                                                                        </tr>








                                                                                        <script type="text/javascript">
                                                                                            function pagerOption2(){

                                                                                                // var paginationSize = document.getElementById("paginationOption").value;
                                                                                                                                                                                                      
                                                                                                var  recordPage=15;
                                                                                                                                                                                                  
                                                                                                $('#tblReportDashBoard1').tablePaginate({navigateType:'navigator'},recordPage,tblReportDashBoard1);

                                                                                            }
        
      
                                                                                                                                                                                                        
                                                                                                                                                                                                           
                                                                                        </script>
                                                                                        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/paginationAll.js"/>"></script>


                                                                                        <%-- customer contacts --%>

                                                                                        </table>

                                                                                        <%--     </sx:div>
                                                                                </sx:tabbedpanel> --%>
                                                                                        <!--//END TABBED PANNEL : --> 
                                                                                        </div>
                                                                                        </div>
                                                                                        <script type="text/javascript">

                                                                                            var countries=new ddtabcontent("reportsTab")
                                                                                            countries.setpersist(false)
                                                                                            countries.setselectedClassTarget("link") //"link" or "linkparent"
                                                                                            countries.init()

                                                                                        </script>

                                                                                        </td>
                                                                                        <!--//END DATA COLUMN : Coloumn for Screen Content-->
                                                                                        </tr>
                                                                                         <tr class="footerBg">
                                                                                        <td align="center"><s:include value="/includes/template/Footer.jsp"/></td>
                                                                                        </tr>
                                                                                        </table>
                                                                                        
                                                                                        <!--//END MAIN TABLE : Table for template Structure-->
                                                                                        <script>
                                                                                            $(document).ready(function() {
                                                                                              hideSelect()
                                                                                            javascript:animatedcollapse.show('EmplyeeCountDiv');
                                                                                               $("#EmplyeeCountDiv").hide();
                                                                                            });
                                                                                        </script>
                                                                                        </body>

                                                                                        </html>

