 <%@ page contentType="text/html; charset=UTF-8" errorPage="../exception/ErrorDisplay.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%--<%@ taglib prefix="sx" uri="/struts-dojo-tags" %> --%>
<%@ page import="com.freeware.gridtag.*" %> 
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="com.mss.mirage.util.ConnectionProvider"%>
<%@ page import="com.mss.mirage.util.ApplicationConstants"%>
<%@ taglib uri="/WEB-INF/tlds/datagrid.tld" prefix="grd" %>
<html>
    <head>
        <title>Hubble Organization Portal :: Employee Search</title>
    
    <link rel="stylesheet" type="text/css" href="<s:url value="/includes/css/mainStyle.css"/>">
    <link rel="stylesheet" type="text/css" href="<s:url value="/includes/css/GridStyle.css"/>">
    <link rel="stylesheet" type="text/css" href="<s:url value="/includes/css/leftMenu.css"/>">
    <link rel="stylesheet" type="text/css" href="<s:url value="/includes/css/tabedPanel.css"/>">
    <script type="text/JavaScript" src="<s:url value="/includes/javascripts/DBGrid.js"/>"></script>
    <script type="text/JavaScript" src="<s:url value="/includes/javascripts/ClientValidations.js"/>"></script>
    <script type="text/JavaScript" src="<s:url value="/includes/javascripts/clientValidations/EmpSearchClientValidation.js"/>"></script>
    <script type="text/JavaScript" src="<s:url value="/includes/javascripts/AppConstants.js"/>"></script>
    <script type="text/JavaScript" src="<s:url value="/includes/javascripts/tabedPanel.js"/>"></script>
    <script type="text/JavaScript" src="<s:url value="/includes/javascripts/CalendarTime.js"/>"></script>
    <script type="text/JavaScript" src="<s:url value="/includes/javascripts/reviews/jquery.min.js"/>"></script>
    <script type="text/javascript" src="<s:url value="/includes/javascripts/reviews/jquery.js"/>"></script>   
    <link rel="stylesheet" type="text/css" href="<s:url value="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css"/>">
 <script type="text/JavaScript" src="<s:url value="/includes/javascripts/LookUpMgmnt.js?version=3.5"/>"></script>

    

    <s:include value="/includes/template/headerScript.html"/>
    
<%--     <script> --%>
<!-- //     function getColumnsList(){ -->
<!-- //     	//   alert("hiieditLookUp"); -->
<!-- //     	var tableName=document.getElementById("tableName").value; -->
<!-- //     	   alert("TableName is====>"+tableName); -->
<!-- //     	  // var tableId=tableName; -->
<!-- //     	   window.location = "getColumnData.action?tableName="+tableName; -->
    	   
<!-- //        } -->
       
<%--     </script> --%>
</head>
<!-- <body class="bodyGeneral" onload="getRequirementClosed()"> -->
<body class="bodyGeneral">

    <%!
        /* Declarations */
        Connection connection;
        String queryString;
        String strTmp;
        String strSortCol;
        String strSortOrd;
        String submittedFrom;
        String searchSubmitValue;
        int intSortOrd = 0;
        int intCurr;
        boolean blnSortAsc = true;
        String columnNameSearch;
    %>

    <!--//START MAIN TABLE : Table for template Structure-->
    <table class="templateTableLogin" align="center" cellpadding="0" cellspacing="0">

        <!--//START HEADER : Record for Header Background and Mirage Logo-->
        <tr class="headerBg">
            <td valign="top">
                <s:include value="/includes/template/Header.jsp"/>                    
            </td>
        </tr>
        <!--//END HEADER : Record for Header Background and Mirage Logo-->


        <!--//START DATA RECORD : Record for LeftMenu and Screen Content-->
        <tr>
            <td>
                <table class="innerTableLogin" cellpadding="0" cellspacing="0">
                    <tr>

                        <!--//START DATA COLUMN : Coloumn for LeftMenu-->
                        <td width="150px;" class="leftMenuBgColor" valign="top">
                            <s:include value="/includes/template/LeftMenu.jsp"/>
                        </td>

                        <!--//END DATA COLUMN : Coloumn for LeftMenu-->

                        <!--//START DATA COLUMN : Coloumn for Screen Content-->
                        <td width="850px" class="cellBorder" valign="top" style="padding: 5px 5px 5px 5px;">
                            <!--//START TABBED PANNEL : -->
                            <ul id="accountTabs" class="shadetabs" >
                                <li ><a href="#" class="selected" rel="employeeSearchTab"  >LookUp Table Description </a></li>
                            </ul>
                            <%--<sx:tabbedpanel id="employeeSearchPannel" cssStyle="width: 840px; height: 500px;padding: 5px 5px 5px 5px;" doLayout="true"> --%>
                            <div  style="border:1px solid gray; width:840px;height: 500px; overflow:auto; margin-bottom: 1em;">   
                                <!--//START TAB : -->
                                <%--  <sx:div id="employeeSearchTab" label="Employee Search" cssStyle="overflow:auto;"  > --%>
                                <div id="employeeSearchTab" class="tabcontent"  >

<!-- overlay Start -->

                                    <div id="overlay"></div>              <!-- Start Overlay -->
                                    <div id="specialBox">


                                        <s:form theme="simple"  align="center" name="editColumnName" method="post" enctype="multipart/form-data" onsubmit="return validateForm();"   >
                                            <s:hidden id="reviewId" name="reviewId" value=""/>
                                            <s:hidden id="reviewFlag" name="reviewFlag" value=""/>
                                            <s:hidden id="addType" name="addType" value="My"/>
                                            <table align="center" border="0" cellspacing="0" style="width:100%;">
                                                <tr>                               
                                                    <td colspan="2" style="background-color: #288AD1" >
                                                        <h3 style="color:darkblue;" align="left">
                                                            <span id="headerLabel"></span>


                                                        </h3></td>
                                                    <td colspan="2" style="background-color: #288AD1" align="right">

                                                        <a href="#" onmousedown="toggleCloseOverlay()" >
                                                            <img src="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/closeButton.png" /> 

                                                        </a>  

                                                    </td></tr>
                                                <tr>
                                                    <td colspan="4">
                                                        <div id="load" style="color: green;display: none;">Loading..</div>
                                                        <div id="resultMessage"></div>
                                                    </td>
                                                </tr>
                                                <tr><td colspan="4">
                                                    <table style="width:80%;" align="center" id="overlayFields" border="0">
                                               
<!--                                                 <tr> -->
<!--                                                     <td class="fieldLabel">Column&nbsp;Name&nbsp;</td> -->
<%--                                                     <td><s:textfield id="overlayColumnName" name="overlayColumnName" cssClass="inputTextBlue" value="%{overlayColumnName}"/></td> --%>
<!--                                                     <td class="fieldLabel">Status&nbsp;:<FONT color="red"  ><em>*</em></FONT></td> -->
<%--                                                     <td><s:textfield id="overlayStatus" name="overlayStatus" cssClass="inputTextBlue" value="%{overlayStatus}"/></td> --%>
<!--                                                 </tr> -->
<!--                                                    <tr>          -->
<!--                                                     <td align="right" colspan="4"> -->
<!--                                                         <input type="button" value="Save" onclick="return addColumn();" class="buttonBg" id="addButton" style="margin-right: 61px;"/>  -->


<%--                                                         <s:submit cssClass="buttonBg"  align="right" id="save" value="Save" /> --%>
<%-- <%--                                                         <s:reset cssClass="buttonBg"  align="right" value="Reset" /> --%> 

<!--                                                     </td> -->
<!--                                                 </tr> -->
                                                  </table>
                                                    </td>
                                                </tr>


                                            </table>
                                        </s:form>              

                                       

                                    </div>

<!-- overlay end -->




                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                      <tr>
																<td class="headerText" colspan="11"><a
																	href="<s:url value="../admin/getLookupTableData.action"/>"
																	style="align: center;"> <img alt="Back to List"
																		src="<s:url value="/includes/images/backToList_66x19.gif"/>"
																		width="66px" height="19px" border="0" align="bottom"
																		style="float: right;margin-right: 20px;" ></a></td>

															</tr>
<!--                                         <tr align="right"> -->
<!--                                             <td class="headerText"> -->
<!--                                                 <img alt="Home"  -->
<%--                                                      src="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/spacer.gif"  --%>
<!--                                                      width="100%"  -->
<!--                                                      height="13px"  -->
<!--                                                      border="0"/> -->
<%--                                                 <%if (request.getAttribute(ApplicationConstants.RESULT_MSG) != null) { --%>
<!-- //                                        out.println(request.getAttribute(ApplicationConstants.RESULT_MSG)); -->
<%--                                    }%> --%>
<!--                                             </td> -->
<!--                                         </tr> -->

                                        <tr>
                                            <td>

                                                <s:form name="frmLookUpColumnAdd" action=" " theme="simple">
                                                
                                                    <table cellpadding="1" cellspacing="1" border="0" width="650px" align="center">
                                                       
                                                       <tr>
                                                       <td> <s:hidden id="tableName" name="tableName" value="%{tableName}"/></td>
                                                         <td> <s:hidden id="headerFields" name="headerFields" value="%{headerFields}"/></td>
                                                         <td> <s:hidden id="columnLength" name="columnLength" value="%{columnLength}"/></td>
                                                          <td> <s:hidden id="searchHeaderField" name="searchHeaderField" value="%{searchHeaderField}"/></td>
                                                           <%if (request.getAttribute(ApplicationConstants.LOOKUP_COLUMN_NAME) != null) { 
                                                         //   out.println(request.getAttribute(ApplicationConstants.LOOKUP_COLUMN_NAME)); 
                                                            columnNameSearch = request.getAttribute(ApplicationConstants.LOOKUP_COLUMN_NAME).toString();
                                                            }%> 
                                                       </tr>    
                                                       <tr>
                                            <td class="fieldLabel" style="display: none;">Total&nbsp;Records&nbsp;:</td>
                                            <td class="userInfoLeft" id="totalState1" style="display: none;"></td>   
                                            </tr>  
                                            <tr>
                                            <td  class="fieldLabel" style="text-align: left;width: 70px;">
                                                        Table&nbsp;Name&nbsp;:  </td>
                                               <td class="navigationText">
                                               <s:property value="%{tableName}"/>
                                                    </td>
                                                    </tr> 
                                                 <tr>
                                                                <td class="fieldLabel">Column&nbsp;Name&nbsp;:</td>
                                                                <td colspan="2">  
                                                                    <s:textfield name="columnName" id="columnName" cssClass="inputTextBlue"  style="width:83%"  value="%{columnName}"  theme="simple" />
                                                                    
                                                                  
                                                                </td>
                                                                <td>
                                                                <input type="button" value="Add" class="buttonBg" onclick="mytoggleOverlay();" />
                                                                    <input type="button" value="Search" class="buttonBg" onclick="getLookUpList( '<%=columnNameSearch %>');" />
<!--                                                                <input type="button" value="Add" class="buttonBg" onclick="addProjectToCustomer();" /> -->
                                                            
                                                                </td>
                                                              


                                                            </tr>      
                                                        
                                                    </table>
                                                </s:form>
                                            </td>

                                        </tr>
                                        
                                       <tr>
                                <td>
                                   
                                    <table id="lookUpAjaxGrid" align="center" cellpadding="2" border="0" cellspacing="1" width="50%" >

                                        <tr>
                                        <td height="20px" align="center" colspan="9">
                                            <div id="lookUpAvailableReport" style="display:none" class="error" ><b>Loading Please Wait.....</b></div>
                                        </td>
                                        </tr>


                                        <tr>
                                        <td >
                                            <div id="lookUpAvailableReport" style="display: block">
                                                <!--style="color:#0000FF;font:italic 900 12px arial;"  bgcolor='#3E93D4'-->
                                                <table id="tblLookUpReport" align='center' cellpadding='1' cellspacing='1' border='0' class="gridTable" width='700'>

                                                </table> <br>
                                                <!--<center><span id="spnFast" class="activeFile" style="display: none;"></span></center>-->
                                            </div>
                                        </td>
                                        </tr>
                                    </table>    
                                </td>
                                </tr> 
                                        
                 
                                    </table>

                                    <!-- End Overlay -->
                                    <!-- Start Special Centered Box -->

                                    <%-- </sx:div > --%>
                                </div>
                                <!--//END TAB : -->
                                <%-- </sx:tabbedpanel> --%>
                            </div>
                            <script type="text/javascript">

                                var countries=new ddtabcontent("accountTabs")
                                countries.setpersist(false)
                                countries.setselectedClassTarget("link") //"link" or "linkparent"
                                countries.init()

                            </script>
                            <!--//END TABBED PANNEL : -->
                        </td>
                        <!--//END DATA COLUMN : Coloumn for Screen Content-->
                    </tr>
                </table>
            </td>
        </tr>
        <!--//END DATA RECORD : Record for LeftMenu and Body Content-->

        <!--//START FOOTER : Record for Footer Background and Content-->
        <tr class="footerBg">
            <td align="center"><s:include value="/includes/template/Footer.jsp"/></td>
        </tr>
        <!--//END FOOTER : Record for Footer Background and Content-->

    </table>
    <!--//END MAIN TABLE : Table for template Structure-->

 <script> 
     $(document).ready(function(){
        
       getLookUpList('<%=columnNameSearch %>');
      
     });
 </script> 
<script type="text/javascript">
                                                                                                                                                                                                       
    function pagerOption(){

        // var paginationSize = document.getElementById("paginationOption").value;
                                                                                                                                                                                                      
        var  recordPage=10;
                                                                                                                                                                                                  
        $('#tblLookUpReport').tablePaginate({navigateType:'navigator'},recordPage);

    }
                                                                                                                                                                                                        
                                                                                                                                                                                                        
                                                                                                                                                                                                        
                                                                                                                                                                                                           
</script>
<script type="text/JavaScript" src="<s:url value="/includes/javascripts/paginationAll.js"/>"></script>
</body>
</html>

