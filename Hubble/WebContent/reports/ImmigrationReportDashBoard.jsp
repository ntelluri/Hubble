<%-- 
   Document   : QreviewReport
   Created on : Apr 11, 2017, 4:14:39 PM
   Author     : miracle
--%>




<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@ page contentType="text/html; charset=UTF-8"
	errorPage="../exception/ErrorDisplay.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%-- <%@ taglib prefix="sx" uri="/struts-dojo-tags" %> --%>
<%@ page import="com.mss.mirage.util.ApplicationConstants"%>
<%@ page import="java.sql.Connection"%>
<%@ page import="java.sql.Statement"%>
<%@ page import="java.sql.ResultSet"%>
<%@ page import="java.sql.SQLException"%>
<%@ page import="com.mss.mirage.util.ConnectionProvider"%>
<html>
<head>
<title>Hubble Organization Portal :: Q-Review Report DashBoard</title>
<link rel="stylesheet" type="text/css"
	href="<s:url value="/includes/css/GridStyle.css?version=2.1"/>">
<link rel="stylesheet" type="text/css"
	href="<s:url value="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css"/>">
<link rel="stylesheet" type="text/css"
	href="<s:url value="/includes/css/mainStyle.css?version=2.1"/>">
<link rel="stylesheet" type="text/css"
	href="<s:url value="/includes/css/tabedPanel.css"/>">
<script type="text/JavaScript"
	src="<s:url value="/includes/javascripts/animatedcollapse.js"/>"></script>
<script type="text/JavaScript"
	src="<s:url value="/includes/javascripts/jquery-1.2.2.pack.js"/>"></script>
<link rel="stylesheet" type="text/css"
	href="<s:url value="/includes/css/leftMenu.css"/>">
<script type="text/JavaScript"
	src="<s:url value="/includes/javascripts/CalendarTime.js"/>"></script>
<script type="text/JavaScript"
	src="<s:url value="/includes/javascripts/AppConstants.js"/>"></script>
<script type="text/JavaScript"
	src="<s:url value="/includes/javascripts/EnableEnter.js"/>"></script>
<script type="text/JavaScript"
	src="<s:url value="/includes/javascripts/ReusableContainer.js"/>"></script>
<script type="text/JavaScript"
	src="<s:url value="/includes/javascripts/tabedPanel.js"/>"></script>
<script type="text/JavaScript"
	src="<s:url value="/includes/javascripts/ImmigrationAjax.js?version=4.8"/>"></script>
<link rel="stylesheet" type="text/css"
	href="<s:url value="/includes/css/new/x0popup.min.css?version=1.0"/>">
<script type="text/JavaScript"
	src="<s:url value="/includes/javascripts/payroll/x0popup.min.js"/>"></script>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<s:include value="/includes/template/headerScript.html" />

<script language="JavaScript">
	animatedcollapse.addDiv('ImgReportDiv',
			'fade=1;speed=400;persist=1;group=app');

	animatedcollapse.init();
	function hideSelect() {
		//document.getElementById("priorityId").style.display = 'none';

	}
</script>
<style type="text/css">

.gridRowEvenDarkRed
{
	color: #000000;        
	background-color: #E3170D;
        /*background: #6C73B4 url(../images/DBGrid/bg_table_td_two.jpg) repeat  top center ;*/
	font-family: Arial,Verdana, Tahoma,"Sans Serif", "Times New Roman";
	font-size: 8pt;
	font-weight: normal;
	height: 20px;
}

.rcorners2 {
	border-radius: 25px;
	border: 2px solid #3e93d4;
	padding: 20px;
	width: auto;
	height: auto;
}

.popupItem:hover {
	background: #F2F5A9;
	font: arial;
	font-size: 10px;
	color: black;
}

.popupRow {
	background: #3E93D4;
}

.popupItem {
	padding: 2px;
	width: 100%;
	border: black;
	font: normal 9px verdana;
	color: white;
	text-decoration: none;
	line-height: 13px;
	z-index: 100;
}

#parent {
	height: auto;
	width: 780px;
	max-height: 500px;
	overflow: visible;
}
</style>
</head>
<!--  <body  class="bodyGeneral" onload="javascript:animatedcollapse.show('TasksDashBoardDiv');" oncontextmenu="return false;"> -->
<body class="bodyGeneral" oncontextmenu="return false;">

	<%!/* Declarations */
	String queryString = null;
	Connection connection;
	Statement stmt;
	ResultSet rs;
	int userCounter = 0;
	int activityCounter = 0;
	int accountCounter = 0;%>


	<!--//START MAIN TABLE : Table for template Structure-->
	<table class="templateTable1000x580" align="center" cellpadding="0"
		cellspacing="0">

		<!--//START HEADER : Record for Header Background and Mirage Logo-->
		<tr class="headerBg">
			<td valign="top"><s:include
					value="/includes/template/Header.jsp" /></td>
		</tr>
		<!--//END HEADER : Record for Header Background and Mirage Logo-->

		<!--//START DATA RECORD : Record for LeftMenu and Screen Content-->
		<tr>
			<td>
				<table class="innerTable1000x515" cellpadding="0" cellspacing="0">
					<tr>
						<!--//START DATA COLUMN : Coloumn for LeftMenu-->
						<td width="850px;" class="leftMenuBgColor" valign="top"><s:include
								value="/includes/template/LeftMenu.jsp" /></td>
						<!--//START DATA COLUMN : Coloumn for LeftMenu-->

						<!--//START DATA COLUMN : Coloumn for Screen Content-->
						<td width="850px" class="cellBorder" valign="top"
							style="padding-left: 10px; padding-top: 5px;">
							<!--//START TABBED PANNEL : --> <%--      <sx:tabbedpanel id="resetPasswordPannel" cssStyle="width: 845px; height: 550px;padding-left:10px;padding-top:5px;" doLayout="true" useSelectedTabCookie="true" > 
                        
                        <!--//START TAB : -->
                        <sx:div id="dashBoardTab" label="DashBoard Details" cssStyle="overflow:auto;"> --%>
							<ul id="imgTab" class="shadetabs">
								<li><a href="#" rel="ImgDashBoardTab" class="selected">Immigration&nbsp;Details</a></li>

							</ul>
							<div
								style="border: 1px solid gray; width: 840px; height: 750px; overflow: auto; margin-bottom: 1em;">
								<br>
								<div id="ImgDashBoardTab" class="tabcontent">

									<table cellpadding="0" cellspacing="0" border="0" width="100%">

										<%-- Tasks DashBoard start --%>

										<tr>
											<td class="homePortlet" valign="top">
												<div class="portletTitleBar">
													<div class="portletTitleLeft">Immigration Report
														DashBoard</div>
													<div class="portletIcons">
														<a href="javascript:animatedcollapse.hide('ImgReportDiv')"
															title="Minimize"> <img
															src="../includes/images/portal/title_minimize.gif"
															alt="Minimize" /></a> <a
															href="javascript:animatedcollapse.show('ImgReportDiv')"
															title="Maximize"> <img
															src="../includes/images/portal/title_maximize.gif"
															alt="Maximize" />
														</a>
													</div>
													<div class="clear"></div>
												</div>

												<div id="ImgReportDiv" style="background-color: #ffffff">
													<table cellpadding="0" cellspacing="0" border="0"
														width="100%">
														<tr>
															<td width="80%" valign="top" align="center"><s:form
																	action="" theme="simple" name="ImgDashBoard"
																	id="ImgDashBoard">

																	<table cellpadding="0" cellspacing="0" border="0"
																		width="100%">
																		<tr>
																			<td>
																				<table border="0" align="center" cellpadding="0"
																					cellspacing="0" width="700;">
																					<br>
																					<tr>

																						<td class="fieldLabel" width="13%">Cur&nbsp;Immigration&nbsp;Status:</td>

																						<td><s:select name="curImgStatus"
																								id="curImgStatus"
																								list="{'F1-OPT','F1-CPT','H1B','EAD','TN-VISA','E3','GreenCard','USCitizen'}"
																								cssClass="inputSelect" headerKey="-1"
																								headerValue="All" />
																							<input type="button" style="margin-left: 150px;"
																							value="Search" class="buttonBg" id="imgSearch"
																							onclick="getImgReportDashBoardByStatus();" /></td>

																					</tr>



																				</table>
																			</td>
																		</tr>


																		<%-- table grid --%>
																		<tr>
																			<td><br>
																				<table align="center" cellpadding="2" border="0"
																					cellspacing="1" width="50%">

																					<tr>
																						<td height="20px" align="center" colspan="9">
																							<div id="imgDetailsReport"
																								style="display: none" class="error">
																								<b>Loading Please Wait.....</b>
																							</div>
																						</td>
																					</tr>


																					<tr>
																						<td><br>
																							<div id="imgDetailsReport"
																								style="display: block">
																								<div id="parent">
																									<!--style="color:#0000FF;font:italic 900 12px arial;"  bgcolor='#3E93D4'-->
																									<table id="tblImgDetailsList" align='center' i
																										cellpadding='1' cellspacing='1' border='0'
																										class="gridTable" width='750'>
																										<script type="text/JavaScript"
																											src="<s:url value="/includes/javascripts/wz_tooltip.js"/>"></script>
																										<script type="text/JavaScript"
																											src="<s:url value="/includes/javascripts/jquery-2.1.3.js"/>"></script>
																										<script type="text/JavaScript"
																											src="<s:url value="/includes/javascripts/tableHeadFixer.js"/>"></script>

																									</table>
																								</div>
																								
																								<br>
																								<!--<center><span id="spnFast" class="activeFile" style="display: none;"></span></center>-->
																							</div></td>
																					</tr>
																				</table></td>
																		</tr>
																		<%-- table grid  end--%>

																	</table>
																</s:form></td>
														</tr>
													</table>

												</div>

											</td>
										</tr>


										<%-- Q-Review Manager DashBoard end --%>
									</table>

									<%--     </sx:div>
                                                                                                                                                                                                        </sx:tabbedpanel> --%>
									<!--//END TABBED PANNEL : -->
								</div>
							</div> <script type="text/javascript">
								var countries = new ddtabcontent("imgTab")
								countries.setpersist(false)
								countries.setselectedClassTarget("link") //"link" or "linkparent"
								countries.init()
							</script>

						</td>
						<!--//END DATA COLUMN : Coloumn for Screen Content-->
					</tr>
				</table>
			</td>
		</tr>
		<!--//END DATA RECORD : Record for LeftMenu and Body Content-->

		<!--//START FOOTER : Record for Footer Background and Content-->
		<tr class="footerBg">
			<td align="center"><s:include
					value="/includes/template/Footer.jsp" /></td>
		</tr>
		<!--//END FOOTER : Record for Footer Background and Content-->
	</table>
	<script type="text/javascript">
	
	$(window).load(function() {
	getImgReportDashBoardByStatus();
	});
	
		
	</script>
</body>

</html>

