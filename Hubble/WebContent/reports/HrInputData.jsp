<%@ page contentType="text/html; charset=UTF-8"
	errorPage="../exception/ErrorDisplay.jsp"%>
<%@ page import="java.sql.Connection"%>
<%@ page import="java.sql.DriverManager"%>
<%@ page import="java.sql.SQLException"%>
<%@ page import="com.mss.mirage.util.*,java.util.*"%>
<%@ page import="com.mss.mirage.employee.timesheets.TimeSheetVTO"%>
<%@ page import="com.freeware.gridtag.*"%>
<%@ page import="com.mss.mirage.util.ApplicationConstants"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Hubble Organization Portal ::  Insert Hr Data </title>
<link rel="stylesheet" type="text/css"
	href="<s:url value="/includes/css/mainStyle.css"/>">
<link REL="StyleSheet"
	HREF="<s:url value="/includes/css/leftMenu.css"/>" type="text/css">
<link rel="stylesheet" type="text/css"
	href="<s:url value="/includes/css/tabedPanel.css"/>">
<link rel="stylesheet" type="text/css"
	href="<s:url value="/includes/css/helpText.css"/>"></link>
<script type="text/JavaScript"
	src="<s:url value="/includes/javascripts/CalendarTime.js"/>"></script>
<script language="javascript"
	src="<s:url value="/includes/javascripts/NewTimeSheetWeekDays.js?version=1.5"/>"></script>
<%--<script type="text/JavaScript" src="<s:url value="/includes/javascripts/ClientValidations.js"/>"></script>
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/clientValidations/TimeSheetClientValidation.js"/>"></script>--%>
<script type="text/JavaScript"
	src="<s:url value="/includes/javascripts/AppConstants.js"/>"></script>
<script type="text/JavaScript"
	src="<s:url value="/includes/javascripts/EmpStandardClientValidations.js"/>"></script>
<script type="text/JavaScript"
	src="<s:url value="/includes/javascripts/tabedPanel.js"/>"></script>
<script type="text/JavaScript"
	src="<s:url value="/includes/javascripts/timesheets/newtimesheetValidations.js?ver=1.4"/>"></script>

<script type="text/JavaScript"
	src="<s:url value="/includes/javascripts/reviews/jquery.min.js"/>"></script>
<script type="text/javascript"
	src="<s:url value="/includes/javascripts/reviews/jquery.js"/>"></script>
<script src="http://code.jquery.com/jquery-1.10.2.js"></script>
<script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

<script type="text/javascript"
	src="<s:url value="/includes/javascripts/reviews/ajaxfileupload.js"/>"></script>

<s:include value="/includes/template/headerScript.html" />
<%-- <script type="text/JavaScript">
	function setTempVar1() {
		document.frmAddTimeSheet.tempVar.value = 1;

	}
	function setTempVar2() {
		document.frmAddTimeSheet.tempVar.value = 2;
	}
</script>
 --%>
</head>
<body class="bodyGeneral" oncontextmenu="return false;">
  <table class="templateTable1000x580" align="center" cellpadding="0" cellspacing="0">
            
         <%--    <!--//START HEADER : Record for Header Background and Mirage Logo-->
            <tr class="headerBg">
                <td valign="top">
                    <s:include value="/includes/template/Header.jsp"/>                    
                </td>
            </tr>
            <!--//END HEADER : Record for Header Background and Mirage Logo-->
          --%>    
            
            <!--//START DATA RECORD : Record for LeftMenu and Screen Content-->
            <tr>
                <td>
                    <table class="innerTable1000x515" cellpadding="0" cellspacing="0">
                        <tr>
                            
                            <!--//START DATA COLUMN : Coloumn for LeftMenu-->
                           <%--  <td width="150px;" class="leftMenuBgColor" valign="top">
                                <s:include value="/includes/template/LeftMenu.jsp"/>
                            </td>
                            --%> 
                            <!--//START DATA COLUMN : Coloumn for LeftMenu-->
                            
                            <!--//START DATA COLUMN : Coloumn for Screen Content-->
                            <td width="850px" class="cellBorder" valign="top" style="padding-left:10px;padding-top:5px;">
                 <!-- action="submitHrData" -->          
<s:form
																	action="submitHrData" theme="simple" name="ssss"  onsubmit=" return submitvalue()">
																	<%
																String userId=request.getParameter("userId");
																/* 	out.println(userId); */
																	%>
																	 <s:hidden id="userId" name="userId" value="%{userId}"></s:hidden> 
							<table cellpadding="0" border="0" align="center" cellspacing="0" width="50%">
							<!-- style="background-color:transparent; border:0px; padding:0px; color:green;" -->									
								<tr>
									<td > <span id="resultMessage" style="background-color:transparent; border:0px; padding:0px; color:green;"><s:property   value="%{resultMessage}"></s:property></span></td>
								</tr>
									<tr>
									 <td width="20%" class="headerTextNormal" align="center"><B>HR&nbsp;Input&nbsp;Table&nbsp;-&nbsp;KPI</B></td> 
									
									
										</tr></table>
																	
																	
								<table cellpadding="1" border="1" align="center" cellspacing="1" width="50%">
									
									
									<tr>
																<td width="20%" class="headerTextNormal" align="center"><B>Regional&nbsp;KPI</B></td>
																<td width="8%" class="headerTextNormal" align="left"><label  id="SalesTrainee">Sales&nbsp;Trainee(0&nbsp;-&nbsp;1&nbsp;y&nbsp;{min}</B></td>
																<td width="8%" class="headerTextNormal" align="left"><B id="Jrbde">Jr.BDE(1&nbsp;-&nbsp;2&nbsp;y&nbsp;{min}</B></td>
																<td width="8%" class="headerTextNormal" align="left"><B id="bde">BDE&nbsp;(2&nbsp;-&nbsp;3&nbsp;y&nbsp;{min}</B></td>
																<td width="8%" class="headerTextNormal" align="left"><B id="srbde">Sr.BDE&nbsp;(3+)&nbsp;{min}</B></td>
																
															</tr>
										<tr>
																<td width="20%" class="headerTextNormal" align="center"><B>Schd&nbsp;Calls</B></td>
																<td width="20%"><s:textfield id="schdCalls1" name="schdCalls1"  value="%{schdCalls1}"  cssClass="inputTextHours" align="left"  onkeypress="return isNumberKey(event)" onchange="iszero(this.value);"/></td>
																<td width="20%"><s:textfield id="schdCalls2" name="schdCalls2"  value="%{schdCalls2}"  cssClass="inputTextHours" align="left"  onkeypress="return isNumberKey(event)" onchange="iszero(this.value);" /></td>
																<td width="20%"><s:textfield id="schdCalls3" name="schdCalls3" value="%{schdCalls3}"   cssClass="inputTextHours" align="left" onkeypress="return isNumberKey(event)"  onchange="iszero(this.value);"/></td>
																<td width="20%"><s:textfield id="schdCalls4" name="schdCalls4" value="%{schdCalls4}"  cssClass="inputTextHours" align="left"  onkeypress="return isNumberKey(event)" onchange="iszero(this.value);" /></td>
															</tr>
															
															
															<tr>
																<td width="20%" class="headerTextNormal" align="center"><B>Calls&nbsp;Exe</B></td>
																<td width="20%"><s:textfield id="callsExe1" name="callsExe1" value="%{callsExe1}"  cssClass="inputTextHours" align="left"  onkeypress="return isNumberKey(event)" onchange="iszero(this.value);" /></td>
																<td width="20%"><s:textfield id="callsExe2" name="callsExe2"   value="%{callsExe2}" cssClass="inputTextHours" align="left"  onkeypress="return isNumberKey(event)" onchange="iszero(this.value);"/></td>
																<td width="20%"><s:textfield id="callsExe3" name="callsExe3"   value="%{callsExe3}" cssClass="inputTextHours" align="left"  onkeypress="return isNumberKey(event)" onchange="iszero(this.value);"/></td>
																<td width="20%"><s:textfield id="callsExe4" name="callsExe4"   value="%{callsExe4}" cssClass="inputTextHours" align="left"  onkeypress="return isNumberKey(event)" onchange="iszero(this.value);"/></td>
															</tr>
															
															
																<tr>
																<td width="20%" class="headerTextNormal" align="center"><B>Meetings&nbsp;Scheduled</B></td>
																<td width="20%"><s:textfield id="meetingsScheduled1" name="meetingsScheduled1" value="%{meetingsScheduled1}"    cssClass="inputTextHours" align="left"  onkeypress="return isNumberKey(event)" onchange="iszero(this.value);"/></td>
																<td width="20%"><s:textfield id="meetingsScheduled2" name="meetingsScheduled2"   value="%{meetingsScheduled2}" cssClass="inputTextHours" align="left"  onkeypress="return isNumberKey(event)" onchange="iszero(this.value);"/></td>
																<td width="20%"><s:textfield id="meetingsScheduled3" name="meetingsScheduled3"   value="%{meetingsScheduled3}" cssClass="inputTextHours" align="left"  onkeypress="return isNumberKey(event)" onchange="iszero(this.value);"/></td>
																<td width="20%"><s:textfield id="meetingsScheduled4" name="meetingsScheduled4"   value="%{meetingsScheduled4}" cssClass="inputTextHours" align="left"  onkeypress="return isNumberKey(event)" onchange="iszero(this.value);"/></td>
															</tr>
															<tr>
																<td width="20%" class="headerTextNormal" align="center"><B>Meetings&nbsp;Executed</B></td>
																<td width="20%"><s:textfield id="meetingsExecuted1" name="meetingsExecuted1"   value="%{meetingsExecuted1}" cssClass="inputTextHours" align="left"  onkeypress="return isNumberKey(event)" onchange="iszero(this.value);"/></td>
																<td width="20%"><s:textfield id="meetingsExecuted2" name="meetingsExecuted2"   value="%{meetingsExecuted2}" cssClass="inputTextHours" align="left"  onkeypress="return isNumberKey(event)" onchange="iszero(this.value);"/></td>
																<td width="20%"><s:textfield id="meetingsExecuted3" name="meetingsExecuted3"   value="%{meetingsExecuted3}" cssClass="inputTextHours" align="left"  onkeypress="return isNumberKey(event)" onchange="iszero(this.value);"/></td>
																<td width="20%"><s:textfield id="meetingsExecuted4" name="meetingsExecuted4"   value="%{meetingsExecuted4}" cssClass="inputTextHours" align="left"  onkeypress="return isNumberKey(event)" onchange="iszero(this.value);"/></td>
															</tr>
															
															
															<tr>
																<td width="20%" class="headerTextNormal" align="center"><B>Oppr&nbsp;Punched</B></td>
																<td width="20%"><s:textfield id="opprPunched1" name="opprPunched1"   value="%{opprPunched1}" cssClass="inputTextHours" align="left"  onkeypress="return isNumberKey(event)" onchange="iszero(this.value);"/></td>
																<td width="20%"><s:textfield id="opprPunched2" name="opprPunched2"   value="%{opprPunched2}" cssClass="inputTextHours" align="left"  onkeypress="return isNumberKey(event)" onchange="iszero(this.value);" /></td>
																<td width="20%"><s:textfield id="opprPunched3" name="opprPunched3"   value="%{opprPunched3}" cssClass="inputTextHours" align="left"  onkeypress="return isNumberKey(event)" onchange="iszero(this.value);"/></td>
																<td width="20%"><s:textfield id="opprPunched4" name="opprPunched4"   value="%{opprPunched4}" cssClass="inputTextHours" align="left"  onkeypress="return isNumberKey(event)" onchange="iszero(this.value);"/></td>
															</tr>
															
															<tr>
																<td width="20%" class="headerTextNormal" align="center"><B>Requirements&nbsp;Punched</B></td>
																<td width="20%"><s:textfield id="requirementspunched1" name="requirementspunched1"   value="%{requirementspunched1}" cssClass="inputTextHours" align="left"  onkeypress="return isNumberKey(event)" onchange="iszero(this.value);"/></td>
																<td width="20%"><s:textfield id="requirementspunched2" name="requirementspunched2"   value="%{requirementspunched2}" cssClass="inputTextHours" align="left"  onkeypress="return isNumberKey(event)" onchange="iszero(this.value);"/></td>
																<td width="20%"><s:textfield id="requirementspunched3" name="requirementspunched3"   value="%{requirementspunched3}" cssClass="inputTextHours" align="left"  onkeypress="return isNumberKey(event)" onchange="iszero(this.value);"/></td>
																<td width="20%"><s:textfield id="requirementspunched4" name="requirementspunched4"   value="%{requirementspunched4}" cssClass="inputTextHours" align="left"  onkeypress="return isNumberKey(event)" onchange="iszero(this.value);"/></td>
															</tr>
															
															
																<tr>
																<td width="20%" class="headerTextNormal" align="center"><B>Quotes&nbsp;Procured&nbsp;(Renewals)</B></td>
																<td width="20%"><s:textfield id="quotesProcured_Renewals1" name="quotesProcured_Renewals1"   value="%{quotesProcured_Renewals1}" cssClass="inputTextHours" align="left"  onkeypress="return isNumberKey(event)" onchange="iszero(this.value);"/></td>
																<td width="20%"><s:textfield id="quotesProcured_Renewals2" name="quotesProcured_Renewals2"   value="%{quotesProcured_Renewals2}" cssClass="inputTextHours" align="left"  onkeypress="return isNumberKey(event)" onchange="iszero(this.value);"/></td>
																<td width="20%"><s:textfield id="quotesProcured_Renewals3" name="quotesProcured_Renewals3"   value="%{quotesProcured_Renewals3}" cssClass="inputTextHours" align="left"  onkeypress="return isNumberKey(event)" onchange="iszero(this.value);"/></td>
																<td width="20%"><s:textfield id="quotesProcured_Renewals4" name="quotesProcured_Renewals4"   value="%{quotesProcured_Renewals4}" cssClass="inputTextHours" align="left"  onkeypress="return isNumberKey(event)" onchange="iszero(this.value);"/></td>
															</tr>
															
															
																<tr>
																<td width="20%" class="headerTextNormal" align="center"><B>Quotes&nbsp;Submitted&nbsp;(Renewals)</B></td>
																<td width="20%"><s:textfield id="quotesSubmitted_Renewals1" name="quotesSubmitted_Renewals1"   value="%{quotesSubmitted_Renewals1}" cssClass="inputTextHours" align="left"  onkeypress="return isNumberKey(event)" onchange="iszero(this.value);"/></td>
																<td width="20%"><s:textfield id="quotesSubmitted_Renewals2" name="quotesSubmitted_Renewals2"   value="%{quotesSubmitted_Renewals2}" cssClass="inputTextHours" align="left"  onkeypress="return isNumberKey(event)" onchange="iszero(this.value);"/></td>
																<td width="20%"><s:textfield id="quotesSubmitted_Renewals3" name="quotesSubmitted_Renewals3"   value="%{quotesSubmitted_Renewals3}" cssClass="inputTextHours" align="left" onkeypress="return isNumberKey(event)" onchange="iszero(this.value);"/></td>
																<td width="20%"><s:textfield id="quotesSubmitted_Renewals4" name="quotesSubmitted_Renewals4"   value="%{quotesSubmitted_Renewals4}" cssClass="inputTextHours" align="left"  onkeypress="return isNumberKey(event)" onchange="iszero(this.value);"/></td>
															</tr>
															
															<tr>
																<td width="20%" class="headerTextNormal" align="center"><B>Software&nbsp;Renewals&nbsp;Procured&nbsp;(Renewals)</B></td>
																<td width="20%"><s:textfield id="softwareRenewalsProcured_Renewals1" name="softwareRenewalsProcured_Renewals1"   value="%{softwareRenewalsProcured_Renewals1}" cssClass="inputTextHours" align="left" onkeypress="return isNumberKey(event)" onchange="iszero(this.value);"/></td>
																<td width="20%"><s:textfield id="softwareRenewalsProcured_Renewals2" name="softwareRenewalsProcured_Renewals2"   value="%{softwareRenewalsProcured_Renewals2}" cssClass="inputTextHours" align="left"  onkeypress="return isNumberKey(event)" onchange="iszero(this.value);"/></td>
																<td width="20%"><s:textfield id="softwareRenewalsProcured_Renewals3" name="softwareRenewalsProcured_Renewals3"   value="%{softwareRenewalsProcured_Renewals3}" cssClass="inputTextHours" align="left"  onkeypress="return isNumberKey(event)" onchange="iszero(this.value);"/></td>
																<td width="20%"><s:textfield id="softwareRenewalsProcured_Renewals4" name="softwareRenewalsProcured_Renewals4"   value="%{softwareRenewalsProcured_Renewals4}" cssClass="inputTextHours" align="left"  onkeypress="return isNumberKey(event)" onchange="iszero(this.value);"/></td>
															</tr>
															
															<tr>
																<td width="20%" class="headerTextNormal" align="center"><B>Software&nbsp;NewLicense&nbsp;Procured&nbsp;(Renewals)</B></td>
																<td width="20%"><s:textfield id="softwareNewLicenseProcured_Renewals1" name="softwareNewLicenseProcured_Renewals1"   value="%{softwareNewLicenseProcured_Renewals1}" cssClass="inputTextHours" align="left" onkeypress="return isNumberKey(event)" onchange="iszero(this.value);" /></td>
																<td width="20%"><s:textfield id="softwareNewLicenseProcured_Renewals2" name="softwareNewLicenseProcured_Renewals2"   value="%{softwareNewLicenseProcured_Renewals2}" cssClass="inputTextHours" align="left" onkeypress="return isNumberKey(event)" onchange="iszero(this.value);"/></td>
																<td width="20%"><s:textfield id="softwareNewLicenseProcured_Renewals3" name="softwareNewLicenseProcured_Renewals3"   value="%{softwareNewLicenseProcured_Renewals3}" cssClass="inputTextHours" align="left" onkeypress="return isNumberKey(event)" onchange="iszero(this.value);"/></td>
																<td width="20%"><s:textfield id="softwareNewLicenseProcured_Renewals4" name="softwareNewLicenseProcured_Renewals4"   value="%{softwareNewLicenseProcured_Renewals4}" cssClass="inputTextHours" align="left" onkeypress="return isNumberKey(event)" onchange="iszero(this.value);"/></td>
															</tr>
															
																<tr>
																<td width="20%" class="headerTextNormal" align="center"><B>Quotes&nbsp;Procured&nbsp;(Federal)</B></td>
																<td width="20%"><s:textfield id="quotesProcured_Federal1" name="quotesProcured_Federal1"   value="%{quotesProcured_Federal1}" cssClass="inputTextHours" align="left"  onkeypress="return isNumberKey(event)" onchange="iszero(this.value);"/></td>
																<td width="20%"><s:textfield id="quotesProcured_Federal2" name="quotesProcured_Federal2"   value="%{quotesProcured_Federal2}" cssClass="inputTextHours" align="left"  onkeypress="return isNumberKey(event)" onchange="iszero(this.value);"/></td>
																<td width="20%"><s:textfield id="quotesProcured_Federal3" name="quotesProcured_Federal3"   value="%{quotesProcured_Federal3}" cssClass="inputTextHours" align="left"  onkeypress="return isNumberKey(event)" onchange="iszero(this.value);"/></td>
																<td width="20%"><s:textfield id="quotesProcured_Federal4" name="quotesProcured_Federal4"   value="%{quotesProcured_Federal4}" cssClass="inputTextHours" align="left"  onkeypress="return isNumberKey(event)" onchange="iszero(this.value);"/></td>
															</tr>
															
															 <tr>
																<td width="20%" class="headerTextNormal" align="center"><B>Quotes&nbsp;Submitted&nbsp;(Federal)</B></td>
																
																	<td width="20%"><s:textfield id="quotesProcured_Federal11" name="quotesProcured_Federal11"   value="%{quotesProcured_Federal11}" cssClass="inputTextHours" align="left"  onkeypress="return isNumberKey(event)" onchange="iszero(this.value);"/></td>
																<td width="20%"><s:textfield id="quotesProcured_Federal22" name="quotesProcured_Federal22"   value="%{quotesProcured_Federal22}" cssClass="inputTextHours" align="left"  onkeypress="return isNumberKey(event)" onchange="iszero(this.value);"/></td>
																<td width="20%"><s:textfield id="quotesProcured_Federal33" name="quotesProcured_Federal33"   value="%{quotesProcured_Federal33}" cssClass="inputTextHours" align="left"  onkeypress="return isNumberKey(event)" onchange="iszero(this.value);"/></td>
																<td width="20%"><s:textfield id="quotesProcured_Federal44" name="quotesProcured_Federal44"   value="%{quotesProcured_Federal44}" cssClass="inputTextHours" align="left"  onkeypress="return isNumberKey(event)" onchange="iszero(this.value);"/></td>
																
																
																<%-- <td width="20%"><s:textfield id="quotesSubmitted_Federal1" name="quotesSubmitted_Federal1"   value="%{quotesSubmitted_Federal1}" cssClass="inputTextHours" align="left"  onkeypress="return isNumberKey(event)"/></td>
																<td width="20%"><s:textfield id="quotesSubmitted_Federal2" name="quotesSubmitted_Federal2"   value="%{quotesSubmitted_Federal2}" cssClass="inputTextHours" align="left"  onkeypress="return isNumberKey(event)"/></td>
																<td width="20%"><s:textfield id="quotesSubmitted_Federal3" name="quotesSubmitted_Federal4"   value="%{quotesSubmitted_Federal3}" cssClass="inputTextHours" align="left"  onkeypress="return isNumberKey(event)"/></td>
																<td width="20%"><s:textfield id="quotesSubmitted_Federal4" name="quotesSubmitted_Federal4"   value="%{quotesSubmitted_Federal4}" cssClass="inputTextHours" align="left"  onkeypress="return isNumberKey(event)"/></td> --%>
															</tr> 
															
															
															<tr>
																<td width="20%" class="headerTextNormal" align="center"><B>Software&nbsp;Renewals&nbsp;Procured&nbsp;(Federal)</B></td>
																<td width="20%"><s:textfield id="softwareRenewalsProcured_Federal1" name="softwareRenewalsProcured_Federal1"   value="%{softwareRenewalsProcured_Federal1}" cssClass="inputTextHours" align="left"  onkeypress="return isNumberKey(event)" onchange="iszero(this.value);"/></td>
																<td width="20%"><s:textfield id="softwareRenewalsProcured_Federal2" name="softwareRenewalsProcured_Federal2"   value="%{softwareRenewalsProcured_Federal2}" cssClass="inputTextHours" align="left"  onkeypress="return isNumberKey(event)" onchange="iszero(this.value);"/></td>
																<td width="20%"><s:textfield id="softwareRenewalsProcured_Federal3" name="softwareRenewalsProcured_Federal3"   value="%{softwareRenewalsProcured_Federal3}" cssClass="inputTextHours" align="left"  onkeypress="return isNumberKey(event)" onchange="iszero(this.value);"/></td>
																<td width="20%"><s:textfield id="softwareRenewalsProcured_Federal4" name="softwareRenewalsProcured_Federal4"   value="%{softwareRenewalsProcured_Federal4}" cssClass="inputTextHours" align="left"  onkeypress="return isNumberKey(event)" onchange="iszero(this.value);"/></td>
															</tr>
															
															
																<tr>
																<td width="20%" class="headerTextNormal" align="center"><B>Software&nbsp;NewLicense&nbsp;Procured&nbsp;(Federal)</B></td>
																<td width="20%"><s:textfield id="softwareNewLicenseProcured_Federal1" name="softwareNewLicenseProcured_Federal1"   value="%{softwareNewLicenseProcured_Federal1}" cssClass="inputTextHours" align="left"  onkeypress="return isNumberKey(event)" onchange="iszero(this.value);"/></td>
																<td width="20%"><s:textfield id="softwareNewLicenseProcured_Federal2" name="softwareNewLicenseProcured_Federal2"   value="%{softwareNewLicenseProcured_Federal2}" cssClass="inputTextHours" align="left"  onkeypress="return isNumberKey(event)" onchange="iszero(this.value);"/></td>
																<td width="20%"><s:textfield id="softwareNewLicenseProcured_Federal3" name="softwareNewLicenseProcured_Federal3"   value="%{softwareNewLicenseProcured_Federal3}" cssClass="inputTextHours" align="left"  onkeypress="return isNumberKey(event)" onchange="iszero(this.value);"/></td>
																<td width="20%"><s:textfield id="softwareNewLicenseProcured_Federal4" name="softwareNewLicenseProcured_Federal4"   value="%{softwareNewLicenseProcured_Federal4}" cssClass="inputTextHours" align="left"  onkeypress="return isNumberKey(event)" onchange="iszero(this.value);"/></td>
															</tr>
									
										<tr>
										
										<td style="background-color:transparent; border:0px; padding:0px;"></td>
													<td align="center" style="background-color:transparent; border:0px; padding:0px;">  
															<s:submit  name="submitName" id="submitId" value="Submit"
															Class="buttonBg"     /> 
															
														</td>
														
											
												</tr>					
									
	</table>
															
															
															</s:form> 


	
	</td>
	
	
	
	  </table>
             
              
              
              
                </td>
            </tr>
            <!--//END DATA RECORD : Record for LeftMenu and Body Content-->
            
        <%--     <!--//START FOOTER : Record for Footer Background and Content-->
            <tr class="footerBg">
               <td align="center">&reg; 2007  Mirage - All Rights Reserved.</td>
               <td align="center"><s:include value="/includes/template/Footer.jsp"/></td>
            </tr>
            <!--//END FOOTER : Record for Footer Background and Content-->
         --%>    
        </table>
        <!--//END MAIN TABLE : Table for template Structure-->
        
        <script type="text/javascript">
      	 
     
   
   function isNumberKey(evt){
	  // document.getElementById('resultMessage').innerHTML="";
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57)){
         
         return false; 
        }
       
        else
        	
        	 return true; 
        	
       
    } 
   function iszero(value){
	  // alert(value);
	  document.getElementById('resultMessage').innerHTML="";
	 if(value<=0){
		 
		 alert("please enter value greater than 0");
	/* 	 document.getElementById('resultMessage').value="Please enter valid numbers"; */
		 return false;
	 }
	 else 
		 return true;
	 //  alert("name"+value);
	   
   }
   
   function submitvalue(){
	  /* var schdCalls1,softwareNewLicenseProcured_Federal1,softwareRenewalsProcured_Federal1,quotesProcured_Federal11,
	   quotesProcured_Federal1,softwareNewLicenseProcured_Renewals1,softwareRenewalsProcured_Renewals1,quotesSubmitted_Renewals1
	   requirementspunched1,opprPunched1,meetingsExecuted1,meetingsScheduled1,callsExe1
	   */
	//   alert("hiiiii"+document.getElementById('resultMessage').innerHTML);
	  document.getElementById('resultMessage').innerHTML="";
	   var i;
	  var result;
	  if((document.getElementById('quotesProcured_Federal11').value<=0)||(document.getElementById('quotesProcured_Federal22').value<=0)||(document.getElementById('quotesProcured_Federal33').value<=0)||(document.getElementById('quotesProcured_Federal44').value<=0)){
		 result=false;
		 document.getElementById('resultMessage').innerHTML="Unable to update Please try again";
		 return false;
		 
		
	  }
	  else{
	    for(i=1;i<=4;i++){
	    	
		 var schdCalls= "schdCalls"+i;
		 var callsExe="callsExe"+i;
		 //alert("name"+schdCalls);
		var meetingsScheduled="meetingsScheduled"+i;
		var meetingsExecuted="meetingsExecuted"+i;
		var opprPunched="opprPunched"+i;
		var requirementspunched="requirementspunched"+i;
		var quotesProcured_Renewals="quotesProcured_Renewals"+i;
		var quotesSubmitted_Renewals="quotesSubmitted_Renewals"+i;
		var softwareRenewalsProcured_Renewals="softwareRenewalsProcured_Renewals"+i;
		var softwareNewLicenseProcured_Renewals="softwareNewLicenseProcured_Renewals"+i;
		var quotesProcured_Federal="quotesProcured_Federal"+i;
		var softwareRenewalsProcured_Federal="softwareRenewalsProcured_Federal"+i;
		var softwareNewLicenseProcured_Federal="softwareNewLicenseProcured_Federal"+i;
	//	alert("quotesProcured_Renewals"+document.getElementById(quotesProcured_Renewals).value);
		 if((document.getElementById(schdCalls).value<=0)||(document.getElementById(callsExe).value<=0)||(document.getElementById(meetingsScheduled).value<=0)||(document.getElementById(meetingsExecuted).value<=0)||(document.getElementById(opprPunched).value<=0)||(document.getElementById(requirementspunched).value<=0)||(document.getElementById(quotesProcured_Renewals).value<=0)||(document.getElementById(quotesSubmitted_Renewals).value<=0)||
				 (document.getElementById(softwareRenewalsProcured_Renewals).value<=0)||(document.getElementById(softwareNewLicenseProcured_Renewals).value<=0)||
				 (document.getElementById(quotesProcured_Federal).value<=0)||(document.getElementById(softwareRenewalsProcured_Federal).value<=0)||(document.getElementById(softwareNewLicenseProcured_Federal).value<=0)){
			 alert("value must be greater than zero");
			 result= false;	 
		 }
		 if(result===false){
			 
			 break;
		 }
	   }
	  }
	    if(result==false){
	    	 document.getElementById('resultMessage').innerHTML=" Unable to update Please try again";
	    	return false;
	    	
	    }
	    else{
	    	return true;
	    }
	  // alert("in onsubmit method");
   }
 
         </script>
	</body>
</html>