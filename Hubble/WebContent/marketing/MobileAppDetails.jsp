<%--
    Document   : pmoDashBoard
    Created on : Dec 18, 2015, 2:46:51 PM
    Author     : miracle
--%>

<%@page import="java.util.Map"%>
<%-- 
    Document   : pmoDashBoard
    Created on : Dec 17, 2015, 8:50:01 PM
    Author     : miracle
--%>


<%@ page contentType="text/html; charset=UTF-8" errorPage="../exception/ErrorDisplay.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags" %> 
<%-- <%@ taglib prefix="sx" uri="/struts-dojo-tags" %> --%>
<%@ page import="com.mss.mirage.util.ApplicationConstants"%>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="com.mss.mirage.util.ConnectionProvider"%>
<html>
    <head>
        <title>Hubble Organization Portal :: Mobile App Details</title>
        <link rel="stylesheet" type="text/css" href="<s:url value="/includes/css/GridStyle.css"/>">
        <link rel="stylesheet" type="text/css" href="<s:url value="/includes/css/mainStyle.css?ver=1.1"/>">
        <link rel="stylesheet" type="text/css" href="<s:url value="/includes/css/tabedPanel.css"/>">
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/animatedcollapse.js"/>"></script>
        
        <link rel="stylesheet" type="text/css" href="<s:url value="/includes/css/leftMenu.css"/>">
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/CalendarTime.js"/>"></script>
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/AppConstants.js"/>"></script>
        
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/EnableEnter.js"/>"></script>
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/ReusableContainer.js"/>"></script>
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/tabedPanel.js"/>"></script>
      
 <script type="text/JavaScript" src="<s:url value="/includes/javascripts/EmployeeAjax.js?version=2.0"/>"></script>
   <script type="text/javascript" src="<s:url value="/includes/javascripts/marketing/MobileApp.js?version=1.1"/>"></script>
 
        
        <%-- <script type="text/JavaScript" src="<s:url value="/includes/javascripts/payroll/payrollajaxscript.js"/>"></script>
         <script type="text/JavaScript" src="<s:url value="/includes/javascripts/payroll/payrollclientvalidations.js"/>"></script> --%>
        <s:include value="/includes/template/headerScript.html"/>

        
        <script language="JavaScript">
                
           
            //            animatedcollapse.addDiv('LeavesCountExcelReportDiv', 'fade=1;persist=1;group=app');
            //            animatedcollapse.addDiv('GenerateTimeSeetReport', 'fade=1;persist=1;group=app');
            //            animatedcollapse.addDiv('GenerateHierarchyReport', 'fade=1;persist=1;group=app');
             
            animatedcollapse.addDiv('MobileAppDetails', 'fade=1;persist=1;group=app');
          
            animatedcollapse.init();
           
            
               
   
        </script>

      


    </head>

    <body  class="bodyGeneral" oncontextmenu="return false;" > 



        <!--//START MAIN TABLE : Table for template Structure-->
        <table class="templateTable1000x580" align="center" cellpadding="0" cellspacing="0">

            <!--//START HEADER : Record for Header Background and Mirage Logo-->
            <tr class="headerBg">
            <td valign="top">
                <s:include value="/includes/template/Header.jsp"/>                    
            </td>
        </tr>
        <!--//END HEADER : Record for Header Background and Mirage Logo-->

        <!--//START DATA RECORD : Record for LeftMenu and Screen Content-->
        <tr>
        <td>
            <table class="innerTable1000x515" cellpadding="0" cellspacing="0">
                <tr>
                    <!--//START DATA COLUMN : Coloumn for LeftMenu-->
                <td width="850px;" class="leftMenuBgColor" valign="top"> 
                    <s:include value="/includes/template/LeftMenu.jsp"/>
                </td>
                <!--//START DATA COLUMN : Coloumn for LeftMenu-->

                <!--//START DATA COLUMN : Coloumn for Screen Content-->
                <td width="850px" class="cellBorder" valign="top" style="padding-left:10px;padding-top:5px;">
                    <!--//START TABBED PANNEL : -->
                    <%--      <sx:tabbedpanel id="resetPasswordPannel" cssStyle="width: 845px; height: 550px;padding-left:10px;padding-top:5px;" doLayout="true" useSelectedTabCookie="true" > 
                        
                        <!--//START TAB : -->
                        <sx:div id="dashBoardTab" label="DashBoard Details" cssStyle="overflow:auto;"> --%>
                    <ul id="reportsTab" class="shadetabs" >
                        <li><a href="#" rel="EmpReportsTab" class="selected">Mobile&nbsp;App&nbsp;Dashboard</a></li>

                    </ul>
                    <div  style="border:1px solid gray; width:840px;height:675px;overflow:auto; margin-bottom: 1em;">    
                        <br>
                        <div id="EmpReportsTab" class="tabcontent" > 
                            <table cellpadding="0" cellspacing="0" border="0" width="100%">


                                <%-- Available Employees  List Report  --%>

                                <tr>
                                <td class="homePortlet" valign="top">
                                    <div class="portletTitleBar">
                                        <div class="portletTitleLeft">Mobile App Details List</div>
                                        <div class="portletIcons">
                                            <a href="javascript:animatedcollapse.hide('MobileAppDetails')" title="Minimize">
                                                <img src="../includes/images/portal/title_minimize.gif" alt="Minimize"/></a>
                                            <a href="javascript:animatedcollapse.show('MobileAppDetails')" title="Maximize">
                                                <img src="../includes/images/portal/title_maximize.gif" alt="Maximize"/>
                                            </a>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                    <div id="MobileAppDetails" style="background-color:#ffffff">
                                        <table cellpadding="0" cellspacing="0" border="0" width="100%"> 
                                            <tr>
                                            <td width="40%" valign="top" align="center">
                                                <s:form theme="simple" name="empSearch" id="empSearch">  

                                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                        <tr align="right">
                                                        <td class="headerText" colspan="9">
                                                            <img src="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/spacer.gif" width="100%" height="13px" border="0">                                                        
                                                        </td>
                                                        </tr>

                                                        <tr>

                                                        </tr>
                                                        <tr>
                                                        <td>
                                                            <table border="0" align="center" cellpadding="0" cellspacing="0">



                                                                <tr>
                                                                <td class="fieldLabel" width="200px" align="right">Department :</td>

                                                                <td><s:select headerKey="-1" headerValue="--Please Select--" list="departmentIdList" name="departmentId" id="departmentId" cssClass="inputSelect" onchange="getPracticeDataV2();"/></td>
                                                                  <td class="fieldLabel" width="200px" align="right">Application Status :</td>

                                                                <td><s:select label="Application Status" name="activityType" id="activityType" value="%{activityType}" headerKey="-1" headerValue="--Please Select--" list="{'Download','Logged','Shared by 3rd Party'}" cssClass="inputSelect" /></td>        

                                                                
                                                              
                                                                </tr>
                                                                
                                                                <tr>
                                                                      <td class="fieldLabel" width="200px" align="right">Live in :</td>                           

                                <td><s:select label="Select Country" 
                                          name="country" id="country" headerKey="-1"            
                                          headerValue="-- Please Select --"
                                          list="countryList" cssClass="inputSelectNew" value="%{country}" onchange="getLocationsByCountry(this);"/></td>                  

                                  <td class="fieldLabel" width="200px" align="right">Location :</td>

                                                                <td><s:select label="Select Location" name="location" id="location" headerKey="-1" headerValue="--Please Select--" list="LocationsMap" cssClass="inputSelect" /></td>        

                                                                </tr>

                                                                <tr>


                                                              
                                                                <td class="fieldLabel">Resource&nbsp;Name&nbsp;:</td>
                                                                <td ><s:textfield name="empName" id="empName" value="%{empName}"    cssClass="inputTextBlue"  theme="simple" readonly="false"/>
                                                                     <td class="fieldLabel">Employee&nbsp;Id&nbsp;:</td>
                                                                <td ><s:textfield name="empId" id="empId" value="%{empId}"     cssClass="inputTextBlue"  theme="simple" readonly="false"/>
                                                                </td>    
                                                                    <%-- <s:textfield name="assignedToUID" id="assignedToUID2" value="%{assignedToUID}"    cssClass="inputTextBlue"  theme="simple" readonly="false"/>
                                                                       <div id="authorEmpValidationMessage2" style="position: absolute; overflow:hidden;"></div>  
<s:hidden name="preAssignEmpId" value="%{preAssignEmpId}" id="preAssignEmpId2"/>  --%>
                                                                </td> 

                                                                </tr>

                                                               


                                                                <tr>

                                                                <td colspan="3"></td>
                                                                <td width="200px" align="center">
                                                                    <input type="button" value="Search" class="buttonBg" onclick="getMobileAppDetails()"/>
                                                                </td> 
                                                                </tr>

                                                                <tr>

                                                                </tr>
                                                                <tr>
                                                                <td class="fieldLabel" >Total&nbsp;Records&nbsp;:</td>
                                                                <td class="userInfoLeft" id="totalState1" ></td>   
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        </tr>

                                                        <%-- table grid --%>
                                                        <tr>
                                                        <td>
                                                            <br>
                                                            <table align="center" cellpadding="2" border="0" cellspacing="1" width="50%" >

                                                                <tr>
                                                                <td height="20px" align="center" colspan="9">
                                                                    <div id="loadingMessage" style="display:none" class="error" ><b>Loading Please Wait.....</b></div>
                                                                </td>
                                                                </tr>


                                                                <tr>
                                                                <td ><br>
                                                                    <div id="mobileAppHistoryReport" style="display: block">
                                                                        <!--style="color:#0000FF;font:italic 900 12px arial;"  bgcolor='#3E93D4'-->
                                                                        <table id="tblMobileAppHistoryReport" align='center' i cellpadding='1' cellspacing='1' border='0' class="gridTable" width='700'>

                                                                        </table> <br>
                                                                        <!--<center><span id="spnFast" class="activeFile" style="display: none;"></span></center>-->
                                                                    </div>
                                                                </td>
                                                                </tr>
                                                            </table>    
                                                        </td>
                                                        </tr>
                                                        <%-- table grid  end--%>
                                                    </table>
                                                </s:form>
                                            </td>
                                            </tr>
                                        </table>

                                    </div>
                                </td>
                                </tr>

                                <%-- Available Employees List End --%>  








                          

                            <%--     </sx:div>
                            </sx:tabbedpanel> --%>
                            <!--//END TABBED PANNEL : --> 
                        </div>
                    </div>
                    <script type="text/javascript">

                        var countries=new ddtabcontent("reportsTab")
                        countries.setpersist(false)
                        countries.setselectedClassTarget("link") //"link" or "linkparent"
                        countries.init()

                    </script>
                          </table> 
                </td>
                </tr>
                
                
                
                </table>
        </td>
        </tr>
                
                 <!--//END DATA RECORD : Record for LeftMenu and Body Content-->

        <!--//START FOOTER : Record for Footer Background and Content-->
        <tr class="footerBg">
            <td align="center"><s:include value="/includes/template/Footer.jsp"/>   </td>
        </tr>
        <tr>
            <td>

                <div style="display: none; position: absolute; top:170px;left:320px;overflow:auto;" id="menu-popup">
                    <table id="completeTable" border="1" bordercolor="#e5e4f2" style="border: 1px dashed gray;" cellpadding="0" class="cellBorder" cellspacing="0" />
                </div>

           
       

        <!--//END FOOTER : Record for Footer Background and Content-->

    </table>
                </td>
                </tr>
    <!--//END MAIN TABLE : Table for template Structure-->




</body>

</html>


