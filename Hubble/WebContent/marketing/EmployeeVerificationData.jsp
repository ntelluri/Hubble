<%-- 
    Document   : EmployeeVerificationData
    Created on : Jul 29, 2015, 4:06:29 PM
    Author     : miracle
--%>


<%@page import="com.mss.mirage.util.Properties"%>
<%@ page contentType="text/html; charset=UTF-8" errorPage="../exception/ErrorDisplay.jsp"%>

<%@ taglib prefix="s" uri="/struts-tags" %>
<%--<%@ taglib prefix="sx" uri="/struts-dojo-tags" %> --%>
<%@ page import="com.freeware.gridtag.*" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="com.mss.mirage.util.ConnectionProvider"%>
<%@ page import="com.mss.mirage.util.ApplicationConstants"%>
<%@ taglib uri="/WEB-INF/tlds/datagrid.tld" prefix="grd"%>
<%@ page import="javax.servlet.http.HttpServletRequest"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Hubble Organization Portal :: Employee Verification</title>
       
        <link rel="stylesheet" type="text/css" href="<s:url value="/includes/css/mainStyle.css"/>">
        <link rel="stylesheet" type="text/css" href="<s:url value="/includes/css/GridStyle.css"/>">
       
        <link rel="stylesheet" type="text/css" href="<s:url value="/includes/css/tabedPanel.css"/>">
        
        <link rel="stylesheet" type="text/css" href="<s:url value="/includes/css/leftMenu.css"/>">
     
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/AppConstants.js"/>"></script>
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/tabedPanel.js"/>"></script>
 
      <script type="text/JavaScript" src="<s:url value="/includes/javascripts/CalendarTime.js"/>"></script>   
   
  
      <script type="text/JavaScript" src='<s:url value="/includes/javascripts/GridNavigation.js"/>'></script>
   
      <script type="text/JavaScript" src="<s:url value="/includes/javascripts/reviews/jquery.min.js"/>"></script>
    <script type="text/javascript" src="<s:url value="/includes/javascripts/reviews/jquery.js"/>"></script>   
       
   <script type="text/JavaScript" src="<s:url value="/includes/javascripts/marketing/EventsPosting.js?ver=4.4"/>"></script>  
  
   <!-- for alert related -->
   <link rel="stylesheet" type="text/css"
	href="<s:url value="/includes/css/new/x0popup.min.css?version=1.0"/>">
<script type="text/JavaScript"
	src="<s:url value="/includes/javascripts/payroll/x0popup.min.js"/>"></script>
<script type="text/JavaScript"
	src="<s:url value="/includes/javascripts/SweetAlerts/alerts.js?ver=1.0"/>"></script>
	
  <link rel="stylesheet" type="text/css"
	href="<s:url value="/includes/css/sweetAlerts/sweetAlerts.css"/>">
	
	 <!-- for alert related end-->
  
   <script>
       function getEmployeeVerificationExcelSheet(){
           
          var tableName=document.getElementById('tableName').value;
          
                window.location="websiteDataDownload.action?tableName="+tableName;
           
        }
   </script>
        <s:include value="/includes/template/headerScript.html"/>
        <%-- for issue reminder popup --%>
    </head>
    <body class="bodyGeneral" oncontextmenu="return false;">
       <s:hidden name="sessionUserId" id="sessionUserId" value="%{#session.userId}"/>
        
 <%String contextPath = request.getContextPath();
                    %>
        <!--//START MAIN TABLE : Table for template Structure-->
        <table class="templateTable1000x580" align="center" cellpadding="0" cellspacing="0">
            
            <!--//START HEADER : Record for Header Background and Mirage Logo-->
            <tr class="headerBg">
                <td valign="top">
                    <s:include value="/includes/template/Header.jsp"/>                    
                </td>
            </tr>
            <!--//END HEADER : Record for Header Background and Mirage Logo-->
            
            <!--//START DATA RECORD : Record for LeftMenu and Screen Content-->
            <tr>
                <td>
                    <table class="innerTable1000x515" cellpadding="0" cellspacing="0">
                        <tr>
                            <!--//START DATA COLUMN : Coloumn for LeftMenu-->
                            <td width="150px;" class="leftMenuBgColor" valign="top"> 
                                <s:include value="/includes/template/LeftMenu.jsp"/> 
                            </td>
                            <!--//START DATA COLUMN : Coloumn for LeftMenu-->
                            
                            <!--//START DATA COLUMN : Coloumn for Screen Content-->
                            <td width="850px" class="cellBorder" valign="top" style="padding:5px 5px ;">
                                <!--//START TABBED PANNEL : -->
                                <%
                                    //out.print("naga::"+ request.getAttribute("subnavigateTo").toString());
                                 /// session.setAttribute(ApplicationConstants.ISSUE_TO_TASK,request.getAttribute("subnavigateTo").toString());
                                %>
                                
                                <ul id="accountTabs" class="shadetabs" >
                                 
                                <%--    <% if(request.getParameter("issueList")==null)
                                       {%> --%>
                                       
                                        <li ><a href="#" class="selected" rel="issuesTab">Employee&nbsp;Verification&nbsp;Data</a></li>
                                       
                                 <%--   <%}else{%>
                                    <li ><a href="#" class="selected" rel="IssuesSearchTab">Tasks Search</a></li>
                                     <% }%> --%>
                                    
                                </ul>
                                 
                                
                                <%-- <sx:tabbedpanel id="IssuesPanel" cssStyle="width: 845px; height: 500px;padding:5px 5px;" doLayout="true"> --%>
                                <div  style="border:1px solid gray; width:845px;height: 500px; overflow:auto; margin-bottom: 1em;">
                                
                            <div id="issuesTab" class="tabcontent" >
                                                           <div id="overlay"></div> 
                                                  <div id="specialBox">
                                                       <s:form theme="simple" name="employeeVerificationForm" id="employeeVerificationForm" align="center" >
                                                           
                                                           <s:hidden name="tempTableName" id="tempTableName" value="%{tableName}"/>
                                                           
                                                           <table align="center" border="0" cellspacing="0" style="width:100%;">
                                                                  <tr>                               
                                                    <td colspan="2" style="background-color: #288AD1" >
                                                        <h3 style="color:darkblue;" align="left">
                                                            <span id="headerLabel"></span>


                                                        </h3></td>
                                                    <td colspan="2" style="background-color: #288AD1" align="right">

                                                        <a href="#" onmousedown="closeEmployeeVerificationOverlay()" >
                                                            <img src="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/closeButton.png" /> 

                                                        </a>  

                                                    </td></tr>
                                                               <tr>
                                                    <td colspan="4">
                                                        <div id="load" style="display: none;" align="center"><font size='3px' color='red'><b>Processing please wait...</b></font></div>
                                                        <div id="resultMessage"></div>
                                                    </td>
                                                </tr>    
                                                        <tr><td colspan="4">
                                                         <table style="width:80%;" align="center">
                                                             
                                                             <s:hidden id='refId' name='refId' />
                                                           
                                                                 <tr><td colspan="6" class="fieldLabelLeft"><font size='3px'><b>Verfication Agency/Employer Details</b></font>
</td></tr>
                                                              <tr>
                                                     <td class="fieldLabel" >First Name:<FONT color="red"  ><em>*</em></FONT> </td>
                                                     <td ><s:textfield name="firstName" id="firstName" cssClass="inputTextBlue" onchange="empVerifyFieldLengthValidator(this)"/></td>
                                                        <td class="fieldLabel" >Last Name:<FONT color="red"  ><em>*</em></FONT> </td>
                                                     <td><s:textfield name="lastName" id="lastName" cssClass="inputTextBlue" onchange="empVerifyFieldLengthValidator(this)"/></td>
                                                        <td class="fieldLabel" >Email:<FONT color="red"  ><em>*</em></FONT> </td>
                                                     <td><s:textfield name="email" id="email" cssClass="inputTextBlue" onchange="empVerifyFieldLengthValidator(this)"/></td>
                                                       </tr>   
                                                              <tr>
                                                     <td class="fieldLabel" >Verfication&nbsp;Agency:<FONT color="red"  ><em>*</em></FONT> </td>
                                                     <td ><s:textfield name="organization" id="organization" cssClass="inputTextBlue" onchange="empVerifyFieldLengthValidator(this)"/></td>
                                                        <td class="fieldLabel" >Designation:<FONT color="red"  ><em>*</em></FONT> </td>
                                                     <td ><s:textfield name="designation" id="designation" cssClass="inputTextBlue" onchange="empVerifyFieldLengthValidator(this)"/></td>
                                                        <td class="fieldLabel" >WorkPhone:<FONT color="red"  ><em>*</em></FONT> </td>
                                                     <td ><s:textfield name="workPhone" id="workPhone" cssClass="inputTextBlue" onchange="empVerifyFieldLengthValidator(this)"/></td>
                                                       </tr> 
                                                        <tr><td colspan="6" class="fieldLabelLeft"><font size='3px'><b>Employee Details Submitted by Background Verification Agency/Employer</b></font>
</td></tr>
                                                         <tr>
                                                     <td class="fieldLabel" >Employee Name:<FONT color="red"  ><em>*</em></FONT> </td>
                                                     <td ><s:textfield name="employeeName" id="employeeName" cssClass="inputTextBlue" onchange="empVerifyFieldLengthValidator(this)"/></td>
                                                        <td class="fieldLabel" >Designation:<FONT color="red"  ><em>*</em></FONT> </td>
                                                     <td ><s:textfield name="verifyDesignation" id="verifyDesignation" cssClass="inputTextBlue" onchange="empVerifyFieldLengthValidator(this)"/></td>
                                                        <td class="fieldLabel" >Department:<FONT color="red"  ><em>*</em></FONT> </td>
                                                     <td ><s:textfield name="department" id="department" cssClass="inputTextBlue" onchange="empVerifyFieldLengthValidator(this)"/></td>
                                                       </tr> 
                                                         <tr>
                                                     <td class="fieldLabel" >Employee ID:<FONT color="red"  ><em>*</em></FONT> </td>
                                                     <td ><s:textfield name="employeeId" id="employeeId" cssClass="inputTextBlue" onchange="empVerifyFieldLengthValidator(this)"/></td>
                                                        <td class="fieldLabel" >Employment Started:<FONT color="red"  ><em>*</em></FONT> </td>
                                                     <td ><s:textfield name="employeeStarted" id="employeeStarted" cssClass="inputTextBlue" onchange="empVerifyFieldLengthValidator(this)"/></td>
                                                        <td class="fieldLabel" >Employment Ended:<FONT color="red"  ><em>*</em></FONT> </td>
                                                     <td ><s:textfield name="employeeEnded" id="employeeEnded" cssClass="inputTextBlue" onchange="empVerifyFieldLengthValidator(this)"/></td>
                                                       </tr> 
                                                          <tr>
                                                     <td class="fieldLabel" >Employment Location:<FONT color="red"  ><em>*</em></FONT> </td>
                                                     <td ><s:textfield name="employeeLocation" id="employeeLocation" cssClass="inputTextBlue" onchange="empVerifyFieldLengthValidator(this)"/></td>
                                                     
                                                       <td class="fieldLabel" >Add Experience:<FONT color="red"  ><em>*</em></FONT> </td>
                                                     <td ><s:textfield name="experience" id="experience" cssClass="inputTextBlue" onchange="empVerifyFieldLengthValidator(this)"/></td>
                                                    
                                                      <td class="fieldLabel" >New Company Name:<FONT color="red"  ><em>*</em></FONT> </td>
                                                     <td ><s:textfield name="newCompanyName" id="newCompanyName" cssClass="inputTextBlue" onchange="empVerifyFieldLengthValidator(this)"/></td>
                                                          </tr>
                                                          
                                                          
                                                       
                                                       <tr>
                                                     <td class="fieldLabel" >Remuneretion(CTC):<FONT color="red"  ><em>*</em></FONT> </td>
                                                     <td ><s:textfield name="remuneration" id="remuneration" cssClass="inputTextBlue" onchange="empVerifyFieldLengthValidator(this)"/></td>
                                                     
                                                       <td class="fieldLabel" >Reason for Leaving:<FONT color="red"  ><em>*</em></FONT> </td>
                                                     <td >
                                                     
                                                     
                                                     <s:textarea name="reasonForLeaving" cols="20" rows="2"  cssClass="inputTextarea" id="reasonForLeaving" onchange="empVerifyFieldLengthValidator(this)"/>
                                                     </td>
                                                    <td class="fieldLabel" >Submitted date: </td>
                                                     <td ><span id="createdDate"></span></td>
                                                  
                                                          </tr>
                                                          
                                                          <tr>
                                                          <td class="fieldLabel" >Experience&nbsp;Letter: </td>
                                                          <td ><span id="experienceLetterSpan"></span></td>
                                                          <td class="fieldLabel" >Latest&nbsp;Resume: </td>
                                                          <td ><span id="latestResumeSpan"></span></td>
                                                          <td class="fieldLabel" >LOA&nbsp;: </td>
                                                          <td ><span id="loaSpan"></span></td>
                                                          
                                                          </tr>
                                                          
                                                          
                                                          
                                                          
                                                          
                                                          
                                                          <tr>
                                                          <td class="fieldLabel" >Last&nbsp;Payslip: </td>
                                                          <td ><span id="lastPayslipSpan"></span></td>
                                                          
                                                          <td class="fieldLabel" >Verification&nbsp;Status: </td>
                                                           <td ><span id="verificationStatusSpan"></span></td>
                                                          </tr>
                                                          
                                                           <tr id='rejectedTr' style="display: none;">
                                                           <td class="fieldLabel" >Rejected By:<FONT color="red"  ><em>*</em></FONT> </td>
                                                     <td ><span id="rejectedBy"></span></td>
                                                            <td class="fieldLabel" >Rejected date:<FONT color="red"  ><em>*</em></FONT> </td>
                                                     <td ><span id="rejectedDate"></span></td>
                                                          </tr>
                                                            <tr id='rejectedReasonTr' style="display: none;">
                                                          <td class="fieldLabel" >Rejected&nbsp;Reason:<FONT color="red"  ><em>*</em></FONT> </td>
                                                     <td colspan="5">
                                                     <s:textarea name="act_RejectedReason" cols="100" rows="2"  cssClass="inputTextarea" id="act_RejectedReason" />
                                                     </td>
                                                          </tr>
                                                        
                                                          </table>
                                                          <table style="width:80%; display: none;" align="center" id='verificationInputsTable'>
                                                           <tr><td colspan="6" class="fieldLabelLeft"><font size='3px'><b>Verification inputs submitted by MSS India operation team</b></font></td></tr>
                                                          
                                                          
                                                            <tr>
                                                     <td class="fieldLabel" >Candidate's Name:<FONT color="red"  ><em>*</em></FONT> </td>
                                                     <td ><s:textfield name="act_CandidateName" id="act_CandidateName" cssClass="inputTextBlue" onchange="empVerifyFieldLengthValidator(this)"/></td>
                                                     
                                                       <td class="fieldLabel" >Employee ID:<FONT color="red"  ><em>*</em></FONT> </td>
                                                     <td ><s:textfield name="act_EmployeeID" id="act_EmployeeID" cssClass="inputTextBlue" onchange="empVerifyFieldLengthValidator(this)"/></td>
                                                    
                                                      <td class="fieldLabel" >Company Name:<FONT color="red"  ><em>*</em></FONT> </td>
                                                     <td ><s:textfield name="act_ComapanyName" id="act_ComapanyName" cssClass="inputTextBlue" onchange="empVerifyFieldLengthValidator(this)"/></td>
                                                          </tr> 
                                                          
                                                            <tr>
                                                     <td class="fieldLabel" >Company Location:<FONT color="red"  ><em>*</em></FONT> </td>
                                                     <td ><s:textfield name="act_CompanyLocation" id="act_CompanyLocation" cssClass="inputTextBlue" onchange="empVerifyFieldLengthValidator(this)"/></td>
                                                     
                                                       <td class="fieldLabel" >Company Tel. No. :<FONT color="red"  ><em>*</em></FONT> </td>
                                                     <td ><s:textfield name="act_CompanyTelNum" id="act_CompanyTelNum" cssClass="inputTextBlue" onchange="empVerifyFieldLengthValidator(this)"/></td>
                                                    
                                                      <td class="fieldLabel" >Date of Joining(mm/dd/yyyy):<FONT color="red"  ><em>*</em></FONT> </td>
                                                     <td ><s:textfield name="act_DateofJoining" id="act_DateofJoining" cssClass="inputTextBlue" />
                                                        <a href="javascript:cal3.popup();">
                                                            <img src="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/showCalendar.gif"
                                                             width="20" height="20" border="0"></a>
                                                     </td>
                                                          </tr> 
                                                          
                                                          
                                                          <tr>
                                                     <td class="fieldLabel" >Date of Relieving(mm/dd/yyyy) :<FONT color="red"  ><em>*</em></FONT> </td>
                                                     <td ><s:textfield name="act_DateofRelieving" id="act_DateofRelieving" cssClass="inputTextBlue" />
                                                     <a href="javascript:cal4.popup();">
                                                            <img src="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/showCalendar.gif"
                                                             width="20" height="20" border="0"></a>
                                                             </td>
                                                     
                                                       <td class="fieldLabel" >Department :<FONT color="red"  ><em>*</em></FONT> </td>
                                                     <td ><s:textfield name="act_Department" id="act_Department" cssClass="inputTextBlue" onchange="empVerifyFieldLengthValidator(this)"/></td>
                                                    
                                                      <td class="fieldLabel" >Last Designation:<FONT color="red"  ><em>*</em></FONT> </td>
                                                     <td ><s:textfield name="act_LastDesignation" id="act_LastDesignation" cssClass="inputTextBlue" onchange="empVerifyFieldLengthValidator(this)"/></td>
                                                          </tr> 
                                                            
                                                            
                                                                
                                                          <tr>
                                                     <td class="fieldLabel" >Remuneration :<FONT color="red"  ><em>*</em></FONT> </td>
                                                     <td ><s:textfield name="act_Remuneration" id="act_Remuneration" cssClass="inputTextBlue" onchange="empVerifyFieldLengthValidator(this)"/></td>
                                                     
                                                       <td class="fieldLabel" >Last Hike Date(mm/dd/yyyy): </td>
                                                     <td ><s:textfield name="act_LastHikeDate" id="act_LastHikeDate" cssClass="inputTextBlue" />
                                                      <a href="javascript:cal5.popup();">
                                                            <img src="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/showCalendar.gif"
                                                             width="20" height="20" border="0"></a>
                                                     </td>
                                                    
                                                      <td class="fieldLabel" >Last Hike Percentage: </td>
                                                     <td ><s:textfield name="act_LastHikePercentage" id="act_LastHikePercentage" cssClass="inputTextBlue" onchange="empVerifyFieldLengthValidator(this)"/></td>
                                                          </tr> 
                                                          
                                                           <tr>
                                                       <td class="fieldLabel" >Reason for Leaving:<FONT color="red"  ><em>*</em></FONT> </td>
                                                     <td >
                                                     
                                                     
                                                     <s:textarea name="act_ReasonforLeaving" cols="20" rows="2"  cssClass="inputTextarea" id="act_ReasonforLeaving" onchange="empVerifyFieldLengthValidator(this)"/>
                                                     </td>
                                                     
                                                          <td class="fieldLabel" >Eligible for Rehire:<FONT color="red"  ><em>*</em></FONT> </td>
                                                     <td >
                                                     
                                                     
                                                     <s:textarea name="act_EligibleforRehire" cols="20" rows="2"  cssClass="inputTextarea" id="act_EligibleforRehire" onchange="empVerifyFieldLengthValidator(this)"/>
                                                     </td>
                                                    
                                                      <td class="fieldLabel" >Nature of Separation (HR Comments): </td>
                                                     <td >
                                                     
                                                     
                                                     <s:textarea name="act_NatureofSeparation" cols="20" rows="2"  cssClass="inputTextarea" id="act_NatureofSeparation" onchange="empVerifyFieldLengthValidator(this)"/>
                                                     </td>
                                                          </tr> 
                                                            
                                                            
                                                               <tr>
                                                   <td class="fieldLabel" >Additional HR Comments: </td>
                                                     <td >
                                                     
                                                     
                                                     <s:textarea name="act_AdditionalHRComments" cols="20" rows="2"  cssClass="inputTextarea" id="act_AdditionalHRComments" onchange="empVerifyFieldLengthValidator(this)"/>
                                                     </td>
                                                     
                                                     
                                                        <td class="fieldLabel" >Skill Set:<FONT color="red"  ><em>*</em></FONT> </td>
                                                     <td >
                                                     
                                                     
                                                     <s:textarea name="act_SkillSet" cols="20" rows="2"  cssClass="inputTextarea" id="act_SkillSet" onchange="empVerifyFieldLengthValidator(this)"/>
                                                     </td>
                                                    
                                                          </tr> 
                                                          <tr id='verifiedTr' style="display: none;">
                                                           <td class="fieldLabel" >Verified By:<FONT color="red"  ><em>*</em></FONT> </td>
                                                     <td ><span id="verifiedBy"></span></td>
                                                            <td class="fieldLabel" >Verified date:<FONT color="red"  ><em>*</em></FONT> </td>
                                                     <td ><span id="verifiedDate"></span></td>
                                                          </tr>
                                                          
                                                          <tr id='approvedTr' style="display: none;">
                                                           <td class="fieldLabel" >Approved By:<FONT color="red"  ><em>*</em></FONT> </td>
                                                     <td ><span id="approvedBy"></span></td>
                                                            <td class="fieldLabel" >Approved date:<FONT color="red"  ><em>*</em></FONT> </td>
                                                     <td ><span id="approvedDate"></span></td>
                                                          </tr>
                                                          
                                                          
                                                          <tr id='updateButtonTr' style="display:none;">
                                                          <td colspan="6" align="center">
                                                            <input type="button" value="Update" id='updateButton' class="buttonBg" onclick="updateEmployeeVerificationDetails()"/>
                                                                                                                                                                                                </td> 
                                                          </tr>
                                                          
                                                       
                                                               </table>
                                                           </td>
                                                       </tr>
                                                           </table>
                                                       </s:form>    
                                                         </div>
                                     
                                     
                                                           <s:form action="searchEmpVerification.action" name="frmDBGrid" id="frmDBGrid" theme="simple" onsubmit="return checkMandatory();"> 
                                            <div style="width:840px;"> 
                                               
                                                <table cellpadding="0" cellspacing="0" align="left" width="100%">
                    
                 <tr>
                        <td>
                            <table border="0" cellpadding="0" cellspacing="2" align="left" width="100%">
                                <tr>
                                    <td class="headerText" colspan="11">
                                        <img alt="Home" src="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/spacer.gif" width="100%" height="13px" border="0">
                                    </td>
                                </tr>
                                <%-- <tr>
                                                                 <td class="fieldLabel" >Search Type<FONT color="red"  ><em>*</em></FONT> </td>
                                                                 <td ><s:select id="tableName"  name="tableName"  list="#@java.util.LinkedHashMap@{'tblContactus':'ContactUs','tblEmpVerfication':'Employee Verification','tblEventAttendies':'QuarterlyMeet','tblResourceDepotDetails':'Resource Depot','tblSuggestions':'Suggestion Box'}" cssClass="inputSelect" headerKey="" headerValue="--Select Type--"/></td>
                                 </tr> --%>
                                
                                <s:hidden id="tableName" name="tableName" value="tblEmpVerfication"/>
                                <tr>
                                 <td class="fieldLabel">Employer Name :</td>
                              <td><s:textfield name="verifyEmployerName" id="verifyEmployerName" cssClass="inputTextBlue"  /></td>
                              <td class="fieldLabel">Employee Name :</td>
                              <td><s:textfield name="verifyEmployeeName" id="verifyEmployeeName" cssClass="inputTextBlue"  /></td>
                           </tr>
                               <tr>
                                  
                                                     <td class="fieldLabel">Date From:</td>
                                                    <td><s:textfield name="createdDateFrom" id="createdDateFrom" cssClass="inputTextBlue" onchange="validateTimestamp(this);"/>
                                                      <a href="javascript:cal1.popup();">
                                                            <img src="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/showCalendar.gif"
                                                             width="20" height="20" border="0"></a>
                                                        </td>
                                                         <td class="fieldLabel">To:<FONT color="red" SIZE="0.5"><em>*</em></FONT></td>
                                                    <td>
                                                        <s:textfield name="createdDateTo"  id="createdDateTo" cssClass="inputTextBlue" onchange="validateTimestamp(this);"/>
                                                        <a href="javascript:cal2.popup();">
                                                            <img src="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/showCalendar.gif"
                                                             width="20" height="20" border="0"></a>
                                                    </td>
                                                    
                                </tr> 
                         
                               <tr>
                                <td class="fieldLabel">Status :</td>
                                 <td class="inputOptionText"><s:select name="verificationStatus" id="verificationStatus" headerKey="" headerValue="All" list="#@java.util.LinkedHashMap@{'Submited':'Submited By Employer','Verified':'Verified By OperationTeam','OpsRejected':'Rejected By OperationTeam','Rejected':'Rejected','Approved':'Approved','YetToApproved':'YetToApproved'}"  
                                                                                     cssClass="inputSelect" /></td> 
                              <td class="fieldLabel">Employer Email :</td>
                              <td><s:textfield name="verifyEmail" id="verifyEmail" cssClass="inputTextBlue"  /></td>
                                
                               </tr>
                                <tr>
                                    
                                    <td></td>
                                    <td colspan="2"></td>
                                    <td >
                                       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <s:submit name="search" value="Search" cssClass="buttonBg"/>
                                        <!--<input type="button" align="right" id="search" value="Search" cssClass="buttonBg" onclick="getSearchReqList();"/>-->
                                    </td>
                                    <s:if test="#session.websiteInfoList != null"> 
                                  <%java.util.List mainListt = (java.util.List) session.getAttribute("websiteInfoList");
                                                                        if (mainListt.size() > 0) {


                                                                    %>
                                                                    
                                                                    <td colsapn="1" align="left" style="display: none;"><input type="button" name="generateExcel" id="generateExcel" class="buttonBg" value="Generate Excel"  onclick="return getEmployeeVerificationExcelSheet();"/></td>
                                                                    <% } %>
                                    </s:if>
                                </tr>
                            </table>
                        </td>
                    </tr>
                        <tr>
                                                    <td colspan="4">
                                                        <div id="loadForProcessing" style="display: none;" align="center"><font size='3px' color='red'><b>Processing please wait...</b></font></div>
                                                        <div id="resultMessage"></div>
                                                    </td>
                                                </tr> 
                                        
                       
            
         
                     <s:if test="#session.websiteInfoList != null"> 
                         <tr align="center">
                             <td>
                                  <div id="addLoadMessage" style="color: red; display: none">Loading please wait..</div>
                             </td>
                         </tr>
                    <tr>
                        <td>
                          <table id="results" cellpadding='1' cellspacing='1' border='0' class="gridTable" width='90%' align="center">
                               
                              <%java.util.List mainList = (java.util.List) session.getAttribute("websiteInfoList");
                                if(mainList.size()>0){


%>

      <tr class="gridHeader">
          
        												<th>Employee&nbsp;Name</th>
                                                         <th>Employer&nbsp;Name</th>
                                                        <th>WorkPhone</th>
                                                        <th>Organization</th>
                                                        <th>SubmittedDate</th>
                                                        <th>Verify/Approve/Reject</th>
                                                        <th>Delete</th>    

                                                  
                </tr>          <%
             //tblEventAttendies
             String verificationStatus ="";
                String randomKey="";
                String redirectionUrl = Properties.getProperty("WEBSITE.URL");
             for (int i = 0; i < mainList.size(); i++) {
                 %>
                 <tr CLASS="gridRowEven">
                            <%
                 //java.util.List subList = (java.util.List)mainList.get(i);
                            java.util.Map subList = (java.util.Map)mainList.get(i);
               //  for (int j = 0; j < subList.size(); j++) {
                     
                     %>
                    
                     <td class="title">
                         <a style="color:#C00067;" href="javascript:getEmployeeVerificationDetails(<%=subList.get("ID")%>);">
                         <%
                          out.println(subList.get("EmployeeName").toString());

%></a>
                     </td>   
                     
                     <td class="title">
                         <%
                         out.println(subList.get("FirstName").toString()+" "+subList.get("LastName").toString());

%>
                     </td>
                         
                           <td class="title">
                         <%
                         
                        out.println(subList.get("Phone"));
                                              

%>
                     </td>     <td class="title">
                         <%
                        out.println(subList.get("Organization"));
                     
%>
                     
                     </td>
                     <td class="title">
                          <%
                        out.println(subList.get("CreatedDate"));
                     
%>      
                     </td>
                     
                   
                      <td class="title">
                          <%
                          if(subList.get("VerificationStatus")!=null)
                          verificationStatus = subList.get("VerificationStatus").toString();
                          if(subList.get("RandomKey")!=null)
                          randomKey = subList.get("RandomKey").toString();
                          
                          if("Submited".equals(verificationStatus)){
                        	
                        	  
                        	  %>
                        	  <a style="color:#C00067;"  href="javascript:redirectUrl('<%=redirectionUrl%>','<%=subList.get("ID")%>','<%=randomKey%>')">
                        	 Click&nbsp;to&nbsp;Verify
                        	  </a>
                        	  <%
                          }
                          else if("Verified".equals(verificationStatus)){
                        	  %>
                        	  <a style="color:#C00067;"  href="javascript:redirectUrl('<%=redirectionUrl%>','<%=subList.get("ID")%>','<%=randomKey%>')">
                        	  Click&nbsp;to&nbsp;Approve
                        	  </a>
                        	  <%
                          }else if("OpsRejected".equals(verificationStatus)){
                        	  %>
                        	  <a style="color:#C00067;"  href="javascript:redirectUrl('<%=redirectionUrl%>','<%=subList.get("ID")%>','<%=randomKey%>')">
                        	  Click&nbsp;to&nbsp;Reject
                        	  </a>
                        	  <%
                          }else {
                        	  out.println(verificationStatus);
                          }
                          
                        
                     
%>      
                     </td>
                     
                     <!-- for deleteVerify  details aaaa-->
                     <td class="title">
                          <%
                          if(subList.get("VerificationStatus")!=null)
                          verificationStatus = subList.get("VerificationStatus").toString();
                          if(subList.get("RandomKey")!=null)
                          randomKey = subList.get("RandomKey").toString();
                          
                          if("Submited".equals(verificationStatus)){
                        	
                        	  
                        	 
                        	  //  for delete empverify  aaaaaa...
                        	  %>
                        	  <a style="color:#C00067;"  href="javascript:deleteEmpVerify(<%=subList.get("ID")%>);">
                        	 <img  src="../includes/images/DBGrid/Delete.png">
                        	  </a>
                        	  <%
                        	  
                          }
                          else if("Verified".equals(verificationStatus)){
                        	  %>
                        	  
                        	  <%
                          }else { 
                          %>
                    	
                    	  <%
                          }
                          
                        
                     
%>      
                     </td>
                     
                     
                     <%
                     //tblEventAttendies
              //   }
                %></tr><% 
                 
             }
                          }else {
             %>
                     <tr><td>
                 <%
                // String contextPath = request.getContextPath();
           // out.println("<img  border='0' align='top'  src='"+contextPath+"/includes/images/alert.gif'/><b> No Records Found to Display!</b>");
                 
                  out.println("<img  border='0' align='top'  src='"+contextPath+"/includes/images/alert.gif'/> <font color='white'><b>No records found!</b></font>");
           // }

            %>
                     </td>
           </tr>
           <%}%>
               
                        </table>
                        </td>
                    </tr>
                    
                    
                    
                     <%
                                         
             if(mainList.size()!=0){
                %>
               
            <tr>
                
                <td align="right" colspan="4" style="background-color:white;" >
                    <div align="right" id="pageNavPosition">hello</div>
             </td>
            </tr> 
        
             <%}
                 %>
                     </s:if>
           
                                               
                                                                                               
<script type="text/JavaScript">
                                             var cal1 = new CalendarTime(document.forms['frmDBGrid'].elements['createdDateFrom']);
				                 cal1.year_scroll = true;
				                 cal1.time_comp = false;

                                             var cal2 = new CalendarTime(document.forms['frmDBGrid'].elements['createdDateTo']);
				                 cal2.year_scroll = true;
				                 cal2.time_comp = false;
				                 
				                 
				                 var cal3 = new CalendarTime(document.forms['employeeVerificationForm'].elements['act_DateofJoining']);
				                 cal3.year_scroll = true;
				                 cal3.time_comp = false;
				                 
				                 
				                 var cal4 = new CalendarTime(document.forms['employeeVerificationForm'].elements['act_DateofRelieving']);
				                 cal4.year_scroll = true;
				                 cal4.time_comp = false;
				                 
				                 
				                 var cal5 = new CalendarTime(document.forms['employeeVerificationForm'].elements['act_LastHikeDate']);
				                 cal5.year_scroll = true;
				                 cal5.time_comp = false;

						
				                 function redirectUrl(baseUrl,refId,randomKey) {

url = baseUrl+"/contact/employee-verification-approval?refId="+refId+"&randomKey="+randomKey;
				                	 window.open(
				                			 url,
				                	   '_blank' // <- This is what makes it open in a new window.
				                	 );


				                	 //alert("haii");
				                	// alert(baseUrl+"/contact/employee-verification-approval?refId="+refId+"&randomKey="+randomKey);
				                	 //window.location.href=baseUrl+"/contact/employee-verification-approval?refId="+refId+"&randomKey="+randomKey;
				                 }
				                 
				                 
				                 
				                 
                                        </script>                                                            
                                        
                          
       <script type="text/javascript">
        var pager = new Pager('results', 10); 
        pager.init(); 
        pager.showPageNav('pager', 'pageNavPosition'); 
        pager.showPage(1);
    	</script>      
                                                </table>
                                            </div>    
                           </s:form>  
                                                     
                                 
                                        <%--  </sx:div > --%>
                                    </div> 
                                 <%--   <%}%> --%>
                                    <!--//END TAB : -->
                                  
                                    <%--  <sx:div id="IssuesSearchTab" label="Issues Search" > --%>
                  
                                </div>
                                    <!--//END TAB : -->
                                    <%--  </sx:tabbedpanel> --%>
                                
                                <!--//END TABBED PANNEL : -->
                                <script type="text/javascript">

var countries=new ddtabcontent("accountTabs")
countries.setpersist(false)
countries.setselectedClassTarget("link") //"link" or "linkparent"
countries.init()

 </script>            
                                </td>
                                </tr>
                    </table>
                            </td>
                            <!--//END DATA COLUMN : Coloumn for Screen Content-->
                        </tr>
                   
               
            
            <!--//END DATA RECORD : Record for LeftMenu and Body Content-->
            
            <!--//START FOOTER : Record for Footer Background and Content-->
            <tr class="footerBg">
                <td align="center"><s:include value="/includes/template/Footer.jsp"/>   </td>
            </tr>
             <tr>
                <td>
                    
                    <div style="display: none; position: absolute; top:170px;left:320px;overflow:auto;" id="menu-popup">
                        <table id="completeTable" border="1" bordercolor="#e5e4f2" style="border: 1px dashed gray;" cellpadding="0" class="cellBorder" cellspacing="0" />
                    </div>
                    
                </td>
            </tr>

            <!--//END FOOTER : Record for Footer Background and Content-->
            
        </table>
        <!--//END MAIN TABLE : Table for template Structure-->
        
        
        
        
        
    </body>
</html>
