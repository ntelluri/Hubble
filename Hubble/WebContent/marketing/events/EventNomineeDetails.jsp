<%-- 
   Document   : External Events
   Created on : Jan 9, 2017, 3:37:32 PM
   Author     : miracle
--%>



<%@page import="org.apache.commons.lang.StringEscapeUtils"%>
<%@page import="java.net.URLDecoder"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.lang.String"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Iterator"%>
<!-- 
 *******************************************************************************
 *
 * Project : Mirage V2
 *
 * Package :
 *
 * Date    :  November 20, 2007, 3:25 PM
 *
 * Author  : Rajasekhar Yenduva<ryenduva@miraclesoft.com>
 *
 * File    : IssuesList.jsp
 *
 * Copyright 2007 Miracle Software Systems, Inc. All rights reserved.
 * MIRACLE SOFTWARE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * *****************************************************************************
 */
-->
<%@ page contentType="text/html; charset=UTF-8"
	errorPage="../exception/ErrorDisplay.jsp"%>

<%@ taglib prefix="s" uri="/struts-tags"%>
<%--<%@ taglib prefix="sx" uri="/struts-dojo-tags" %> --%>
<%@ page import="com.freeware.gridtag.*"%>
<%@ page import="java.sql.Connection"%>
<%@ page import="java.sql.SQLException"%>
<%@ page import="com.mss.mirage.util.ConnectionProvider"%>
<%@ page import="com.mss.mirage.util.ApplicationConstants"%>
<%@ taglib uri="/WEB-INF/tlds/datagrid.tld" prefix="grd"%>
<%@ page import="javax.servlet.http.HttpServletRequest"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>Hubble Organization Portal :: Nominee Details</title>

<link rel="stylesheet" type="text/css"
	href="<s:url value="/includes/css/mainStyle.css?version=1.2"/>">
<link rel="stylesheet" type="text/css"
	href="<s:url value="/includes/css/GridStyle.css"/>">

<link rel="stylesheet" type="text/css"
	href="<s:url value="/includes/css/tabedPanel.css"/>">

<link rel="stylesheet" type="text/css"
	href="<s:url value="/includes/css/leftMenu.css"/>">

<script type="text/JavaScript"
	src="<s:url value="/includes/javascripts/CalendarTime.js"/>"></script>
<script type="text/JavaScript"
	src="<s:url value="/includes/javascripts/AppConstants.js"/>"></script>
<script type="text/JavaScript"
	src="<s:url value="/includes/javascripts/tabedPanel.js"/>"></script>


<link rel="stylesheet" type="text/css"
	href="<s:url value="/includes/css/new/x0popup.min.css?version=1.0"/>">
<script type="text/JavaScript"
	src="<s:url value="/includes/javascripts/payroll/x0popup.min.js"/>"></script>

<script type="text/JavaScript"
	src='<s:url value="/includes/javascripts/GridNavigation.js"/>'></script>
<%--  <script type="text/JavaScript" src="<s:url value="/includes/javascripts/EmployeeAjax.js"/>"></script> --%>
<script type="text/JavaScript"
	src="<s:url value="/includes/javascripts/reviews/jquery.min.js"/>"></script>
<script type="text/javascript"
	src="<s:url value="/includes/javascripts/reviews/jquery.js"/>"></script>

<%--   <script type="text/javascript" src="<s:url value="/includes/javascripts/reviews/ajaxfileupload.js"/>"></script>   --%>

<script type="text/JavaScript"
	src="<s:url value="/includes/javascripts/marketing/EventsPosting.js?ver=9.0"/>"></script>

<s:include value="/includes/template/headerScript.html" />
</head>
<body class="bodyGeneral" oncontextmenu="return false;">
	<script type="text/JavaScript"
		src="<s:url value="/includes/javascripts/wz_tooltip.js"/>"></script>


	<%
		String contextPath = request.getContextPath();
	%>
	<!--//START MAIN TABLE : Table for template Structure-->
	<table class="templateTable1000x580" align="center" cellpadding="0"
		cellspacing="0">

		<!--//START HEADER : Record for Header Background and Mirage Logo-->
		<tr class="headerBg">
			<td valign="top"><s:include
					value="/includes/template/Header.jsp" /></td>
		</tr>
		<!--//END HEADER : Record for Header Background and Mirage Logo-->

		<!--//START DATA RECORD : Record for LeftMenu and Screen Content-->
		<tr>
			<td>
				<table class="innerTable1000x515" cellpadding="0" cellspacing="0">
					<tr>
						<!--//START DATA COLUMN : Coloumn for LeftMenu-->
						<td width="150px;" class="leftMenuBgColor" valign="top"><s:include
								value="/includes/template/LeftMenu.jsp" /></td>
						<!--//START DATA COLUMN : Coloumn for LeftMenu-->

						<!--//START DATA COLUMN : Coloumn for Screen Content-->
						<td width="850px" class="cellBorder" valign="top"
							style="padding: 5px 5px;">
							<!--//START TABBED PANNEL : --> <%
 	//out.print("naga::"+ request.getAttribute("subnavigateTo").toString());
 	/// session.setAttribute(ApplicationConstants.ISSUE_TO_TASK,request.getAttribute("subnavigateTo").toString());
 %>

							<ul id="accountTabs" class="shadetabs">

								<%--    <% if(request.getParameter("issueList")==null)
                                           {%> --%>

								<li><a href="#" rel="conferenceTab">Nominee Details</a></li>

								<%--   <%}else{%>
                                       <li ><a href="#" class="selected" rel="IssuesSearchTab">Tasks Search</a></li>
                                        <% }%> --%>

							</ul> <%-- <sx:tabbedpanel id="IssuesPanel" cssStyle="width: 845px; height: 500px;padding:5px 5px;" doLayout="true"> --%>
							<div
								style="border: 1px solid gray; width: 845px; height: 500px; overflow: auto; margin-bottom: 1em;">


								<div id="conferenceTab" class="tabcontent">
									<div id="overlayForAddingNomineeDetails" class="overlay"></div>
									<div id="specialBoxForAddingNomineeDetails" class="specialBox"
										style="border-radius: 30px; margin-top: -35px;">

										<div id="facilityMaintenanceDiv">
											<table align="center" border="0" cellspacing="0"
												style="width: 112%; background-color: #eaeded; color: black; border-radius: 30px 30px 30px 30px; border: 0px solid black;">
												<tr>
													<td colspan="2"
														style="background-color: #288AD1; border-radius: 30px 0 0;">
														<h3 style="color: darkblue;" align="left">
															<span id="headerLabelFacility" style="margin-left: 20px;"></span>


														</h3>
													</td>
													<td colspan="2"
														style="background-color: #288AD1; border-radius: 0 30px 0 0;"
														align="right"><a href="#"
														onmousedown="toggleCloseNominee()"> <img
															src="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/closeButton.png"
															style="margin-right: 15px;" />

													</a></td>
												</tr>

												<tr>
													<td colspan="4">
														<div id="blockDivForNominee" class="transperentblockDiv"
															style="display: none; align: center; position: fixed; top: 50%; left: 48%;">
															<font
																style="font-weight: bold; position: absolute; color: #fff; font-size: 23px; top: -53px; left: 5%">
																Processing <font id="fontId">...</font>
															</font> <img src='../../includes/images/ajax-loader.gif'
																WIDTH='35%' HEIGHT='5%' alt='block'>
														</div>
														<div id="displyNomineeID" class="transperentdisplyDiv"></div>
														<div id="loadForFacilityOverLay"
															style="color: red; display: none;" align="center">Loading
															Please Wait...</div>
														<div id="resultMessageForFacility" align="right"></div>
													</td>
												</tr>
												<tr>
													<td>
														<table cellpadding="2" cellspacing="2" border="0"
															width="90%" align="center" style="padding-left: 65px;">

															<tr>
																<input type="hidden" id="confId" name="confId" />
																<input type="hidden" id="eventId" name="eventId" />
																<input type="hidden" id="nomineeLoginIdHidden" name="nomineeLoginIdHidden" />
																
																
															</tr>
															<tr>

																<td class="fieldLabel">Name:</td>
																<td colspan=""><s:textfield name="employeeName"
																		id="employeeName" value="" autocomplete="off"
																		cssClass="inputTextBlue" onkeyup="getEmplyeeNamesForNomineeAdding();"
																		placeholder="Enter Employee Name" /><label
																	id="rejectedId"></label>
																	<div class="fieldLabelLeft" id="validationMessage"></div>

																	<div
																		style="display: none; position: absolute; overflow: auto; z-index: 500;"
																		id="menu-popup">
																		<table id="completeTable" border="1"
																			bordercolor="#e5e4f2"
																			style="border: 1px dashed gray;" cellpadding="0"
																			class="cellBorder" cellspacing="0"></table>
																	</div> <s:hidden id="empLoginId" name="empLoginId" /></td>
																<td class="fieldLabel">Designation&nbsp;:</td>
																<td><s:textfield name="designation"
																		id="designation" cssClass="inputTextBlue"
																		value=""  /></td>

															</tr>
															<tr>

																<td class="fieldLabel">Phone&nbsp;:</td>
																<td><s:textfield name="workPhone"
																		id="workPhone" cssClass="inputTextBlue"
																		value=""  /></td>
																<td class="fieldLabel">Location&nbsp;:</td>
																<td><s:textfield name="location"
																		id="location" cssClass="inputTextBlue"
																		value="" readonly="true" />
																	<input type="hidden" name="nomineeEail"
																		id="nomineeEail"  />	
																		
																		</td>
															</tr>
															<tr>

																<td class="fieldLabel">Reports&nbsp;To:</td>
																<td colspan=""><s:textfield name="reportsTo"
																		id="reportsTo" cssClass="inputTextBlue" value="" readonly="true"/></td>
																<td class="fieldLabel">Nearest&nbsp;Airport&nbsp;:</td>
																<td><s:textfield name="nearestAirPort"
																		id="nearestAirPort" cssClass="inputTextBlue" value=""
																		style="width: 230px;" /></td>

															</tr>
															<tr>
																<td class="fieldLabel">Passport&nbsp;Availibility&nbsp;:</td>
																<td colspan=""><s:checkbox name="isPassportAvailibility"
																		id="isPassportAvailibility" value="%{isPassportAvailibility}" theme="simple" />
																</td>
															</tr>
															

															<tr id="saveTr">
																<td colspan="4" align="right"><button id="addRow"
																		type="button" onclick="doAddNomineeDetailsForConference();"
																		style="background-color: #3E93D4; border-radius: 4px; float: right; color: #FFF; border: 1px solid #ddd; font-size: 12px; height: 30px; margin-right: 13px;">
																		Save&nbsp;&nbsp;</button></td>
															</tr>
														</table>

													</td>
												</tr>


											</table>

										</div>
									</div>


									<s:form action="" id="frmLogisticDetails"
										name="frmLogisticDetails" theme="simple">
										<div id="overlayForLogisticDetails" class="overlay"></div>
										<div id="specialBoxForLogisticDetails" class="specialBox"
											style="border-radius: 30px; margin-top: -35px;">

											<div id="facilityMaintenanceDiv">
												<table align="center" border="0" cellspacing="0"
													style="width: 112%; background-color: #eaeded; color: black; border-radius: 30px 30px 30px 30px; border: 0px solid black;margin-top: -76px;">
													<tr>
														<td colspan="2"
															style="background-color: #288AD1; border-radius: 30px 0 0;">
															<h3 style="color: darkblue;" align="left">
																<span id="headerLabelLogistic"
																	style="margin-left: 20px;"></span>


															</h3>
														</td>
														<td colspan="2"
															style="background-color: #288AD1; border-radius: 0 30px 0 0;"
															align="right"><a href="#"
															onmousedown="toggleCloseLogistics()"> <img
																src="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/closeButton.png"
																style="margin-right: 15px;" />

														</a></td>
													</tr>

													<tr>
														<td colspan="4">
															<div id="blockDivForLogistics" class="transperentblockDiv"
															style="display: none; align: center; position: fixed; top: 50%; left: 48%;">
															<font
																style="font-weight: bold; position: absolute; color: #fff; font-size: 23px; top: -53px; left: 5%">
																Processing <font id="fontId">...</font>
															</font> <img src='../../includes/images/ajax-loader.gif'
																WIDTH='35%' HEIGHT='5%' alt='block'>
														</div>
														<div id="displyLogisticsID" class="transperentdisplyDiv"></div>
														<div id="loadForLogisticOverLay"
																style="color: red; display: none;" align="center">Loading
																Please Wait...</div>
															<div id="resultMessageForLogisticDetails" align="right"></div>
														</td>
													</tr>
													<tr>
																	<input type="hidden" id="confIdForLogistic"
																		name="confIdForLogistic" />
																	<input type="hidden" id="nomineeIdForLogistic"
																		name="nomineeIdForLogistic" />
																	<input type="hidden" id="logisticId" name="logisticId" />
																	<input type="hidden" id="flightAttach" name="flightAttach" />
																	<input type="hidden" id="transAttach" name="transAttach" />
																	<input type="hidden" id="hotelAttach" name="hotelAttach" />
																</tr>
																
																
													<tr>
														<td>
														
															<table id="tblLogisticDetails" cellpadding="2" cellspacing="2" border="0"
																width="90%" align="center" style="margin-left: 109px;">

																<tr id="label1">
																	<td class="fieldLabel" colspan=""
																		style="text-align: left; color: #3e93d4; font-weight: bold; font-size: 14px;">Flight&nbsp;Details&nbsp;:</td>
																</tr>
																<tr id="fightRowId1">
																	<td class="fieldLabel">Flight&nbsp;Code:</td>
																	<td colspan="3"><s:textfield name="flightName1"
																			id="flightName1" cssClass="inputTextBlue"
																			theme="simple" value=""
																			style="width: 162px;" /></td></tr>
																<tr id="fightRowId2">	<td class="fieldLabel">From&nbsp;City&nbsp;:</td>
																	<td><s:textfield name="flyFromCity1"
																			id="flyFromCity1" cssClass="inputTextBlue"
																			theme="simple" value=""
																			onchange=""
																			style="width: 162px;" /></td>
																			<td class="fieldLabel">To&nbsp;City&nbsp;:</td>
																	<td><s:textfield name="flyToCity1"
																			id="flyToCity1" cssClass="inputTextBlue"
																			theme="simple" value=""
																			onchange=""
																			style="width: 162px;" /></td>
																</tr>
																<tr id="fightRowId3">
																	<td class="fieldLabel">Date&nbsp;:</td>
																	<td><s:textfield name="flyDate1"
																			id="flyDate1" cssClass="inputTextBlue"
																			onchange="validateTimestamp(this);"
																			value="%{flyDate}"  style="width: 162px;" /> <a
																		href="javascript:cal1.popup();"> <img
																			src="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/showCalendar.gif"
																			width="20" height="20" border="0"></a></td>
																	<td class="fieldLabel">Time&nbsp;:</td>
																	<td colspan=""><s:textfield
																			name="flyTime1" id="flyTime1"
																			maxLength="5" placeholder="HH:MM"
																			Class="inputSelectSmall"
																			onchange="timeValidator(this);"
																			onkeyup="enterdTime(this);"
																			value="%{flyTime}" /> <s:select
																			id="flyMidDayFrom1"
																			name="flyMidDayFrom1" list="{'AM','PM'} "
																			cssClass="inputSelectSmall" /> <s:select
																			id="timeZone1" name="timeZone1"
																			list="{'IST','EST'}"  cssClass="inputSelectSmall" /></td>
																</tr>
																
																
																</table>
																<button id="addRow" type="button"
															onclick="addFlightRow(0);"
															style="background-color: #3E93D4; border-radius: 4px; float: right; color: #FFF; border: 1px solid #ddd; font-size: 12px; height: 30px; margin-right: 33px; ">
															Add&nbsp;Flight&nbsp;<i class="fa fa-plus"></i>
														</button><button id="removeRow" type="button"
															onclick="removeFlightRowData();"
															style="background-color: #3E93D4; border-radius: 4px; float: right; color: #FFF; border: 1px solid #ddd; font-size: 12px; height: 30px; margin-right: 5px; ">
															Remove&nbsp;Flight&nbsp;<i class="fa fa-plus"></i>
														</button>
																</td></tr>
																<tr><td>
																
																
																
																
																
																<table cellpadding="2" cellspacing="2" border="0"
																width="90%" align="center" style="padding-left: 65px;">

																
																<tr id="downLoadFlightTr" style="display: none;">
																<td class="fieldLabel">Flight&nbsp;Attachment&nbsp;:</td>
																<td colspan="3"><span id="flightFileName"></span></td>
                                                                </tr>
                                                                <tr id=""> <td class="fieldLabel"><span id="uploadLogoFlightSpan">:</span></td>
                                                                    <td colspan="3"><s:file name="fileFlight" label="File" id="fileFlight" cssClass="inputTextarea"  onchange="fileValidationLogisticImage(this);"/> 
                                                                    </td> 
                                                                </tr>
																<tr>
																	<td class="fieldLabel" colspan=""
																		style="text-align: left; color: #3e93d4; font-weight: bold; font-size: 14px">
																		Hotel&nbsp;Details&nbsp;:</td>
																</tr>
																<tr>

																	<td class="fieldLabel">Name&nbsp;:</td>
																	<td><s:textfield name="hotelName" id="hotelName"
																			cssClass="inputTextBlue" theme="simple"
																			style="width: 200px;" value="%{hotelName}"
																			onchange="" /></td>
																</tr>
																<tr id="eventDescriptionTr">
																	<td class="fieldLabel" valign="top">Address&nbsp;:</td>
																	<td colspan="3" valign="top"><s:textarea rows="6"
																			cols="85" name="hotelAddress"
																			cssClass="inputTextareaOverlay1"
																			onchange="checkDoubleQuotes(this);"
																			id="hotelAddress" style="width:500px;height: 58px;"
																			value="%{hotelDetails}" /></td>
																</tr>
																<tr id="downLoadHotelTr" style="display: none;">
																<td class="fieldLabel">Hotel&nbsp;Attachment&nbsp;:</td>
																<td colspan="3"><span id="hotelFileName"></span></td>
                                                                </tr>
                                                                <tr id=""> <td class="fieldLabel"><span id="uploadLogoHotelSpan">:</span></td>
                                                                    <td colspan="3"><s:file name="fileHotel" label="File" id="fileHotel" cssClass="inputTextarea"  onchange="fileValidationLogisticImage(this);"/> 
                                                                    </td> 
                                                                </tr>
                                                                <tr>
																	<td class="fieldLabel" colspan=""
																		style="text-align: left; color: #3e93d4; font-weight: bold; font-size: 14px">
																		Transport&nbsp;Details&nbsp;:</td>
																</tr>
																<tr id="">
																	<td class="fieldLabel" valign="top">Transport&nbsp;:</td>
																	<td colspan="3" valign="top"><s:textarea rows="6"
																			cols="85" name="transportDetails"
																			cssClass="inputTextareaOverlay1"
																			onchange="checkDoubleQuotes(this);"
																			id="transportDetails" style="width:500px;height: 58px;"
																			value="" /></td>
																</tr>
																<tr id="downLoadTransportTr" style="display: none;">
																<td class="fieldLabel">Transport&nbsp;Attachment&nbsp;:</td>
																<td colspan="3"><span id="transportFileName"></span></td>
                                                                </tr>
																
																<tr id=""> <td class="fieldLabel"><span id="uploadLogoTransportSpan">:</span></td>
                                                                    <td colspan="3"><s:file name="fileTransport" label="File" id="fileTransport" cssClass="inputTextarea"  onchange="fileValidationLogisticImage(this);"/> 
                                                                    </td> 
                                                                </tr>
                                                                
                                                                
																<tr id="addTrLogistics" style="display: none;">


																	<td align="right" colspan="6">
																		<button id="saveRow" type="button"
																			onclick="doAddLogisticsDetails();"
																			style="background-color: #3E93D4; border-radius: 4px; float: right; margin-right: 28px; color: #FFF; border: 1px solid #ddd; font-size: 12px; width: 90px; height: 30px;">
																			Save</button>
																	</td>
																</tr>

																<tr id="editTrLogistics" style="display: none;">
																	<td align="right" colspan="4">

																		<button id="saveRow" type="button"
																			onclick="doUpdateLogisticsDetails();"
																			style="background-color: #3E93D4; border-radius: 4px; float: right; margin-right: 28px; color: #FFF; border: 1px solid #ddd; font-size: 12px; width: 90px; height: 30px;">
																			Update</button>

																	</td>
																</tr>



															</table> 
														</td>
													</tr>


												</table>

											</div>
										</div>
									</s:form>


									<div id="nomineeDetailsOverlay" class="overlay"></div>
									<div id="nomineeDetailsSpecialBox" class="specialBox"
										style="border-radius: 36px;">

										<s:form theme="simple" align="center">

											<s:hidden name="nomineeId" id="nomineeId" />

											<%--  <s:hidden name="teamId" id="teamId" value="%{#session.teamName}"/> --%>
											<table align="center" border="0" cellspacing="0"
												style="width: 100%;">
												<tr>
													<td colspan="2"
														style="background-color: #288AD1; border-radius: 36px 0 0 0;">
														<h3 style="color: darkblue;" align="left">
															<span id="headerLabel" style="margin-left: 34px;"></span>


														</h3>
													</td>
													<td colspan="2"
														style="background-color: #288AD1; border-radius: 0 36px 0 0;"
														align="right"><a href="#"
														onmousedown="toggleOverlay()"> <img
															src="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/closeButton.png"
															style="margin-right: 15px;" />

													</a></td>
												</tr>
												<tr>
													<td colspan="4">
														<div id="load" style="color: red; display: none;"
															align="center">
															<b>Processing please wait..</b>
														</div>
														<div id="resultMessage"></div>
													</td>
												</tr>

												<tr>
													<td colspan="4">
														<table style="width: 80%;" align="center">
															<tr>
																<td class="fieldLabel">Event&nbsp;Title&nbsp;:</td>
																<td colspan="3"><s:textfield name="event_Name"
																		id="event_Name" cssClass="inputTextareaOverlay1"
																		value="%{eventTitle}"
																		style="height: 25px;width: 441px;" readonly="true" />
															</tr>
															<tr>
																<td class="fieldLabel">Name&nbsp;:</td>
																<td><s:textfield name="empName"
																		id="empName" cssClass="inputTextBlue"
																		readonly="true" /></td>

																<td class="fieldLabel">Designation&nbsp;:</td>
																<td><s:textfield name="empDesignation"
																		id="empDesignation" cssClass="inputTextBlue"
																		readonly="true" /></td>

															</tr>
															<tr>
																<td class="fieldLabel">WorkPhone&nbsp;:</td>
																<td><s:textfield name="empWorkPhone" id="empWorkPhone"
																		cssClass="inputTextBlue" readonly="true" /></td>
																<td class="fieldLabel">WorkLocation&nbsp;:</td>
																<td><s:textfield name="workLocation"
																		id="workLocation" cssClass="inputTextBlue"
																		readonly="true" /></td>

															</tr>
															<tr>
																<td class="fieldLabel">ReportsTo&nbsp;:</td>
																<td><s:textfield name="empReportsTo" id="empReportsTo"
																		cssClass="inputTextBlue" readonly="true" /></td>

																<td class="fieldLabel">NearestAirport&nbsp;:</td>
																<td><s:textfield name="nearestAirport"
																		id="nearestAirport" cssClass="inputTextBlue"
																		readonly="true" /></td>

															</tr>


															<tr>
																<td class="fieldLabel">NomineeStatus&nbsp;:</td>
																<td><s:textfield name="nomineeStatus"
																		id="nomineeStatus" cssClass="inputTextBlue"
																		readonly="true" /></td>

																<td class="fieldLabel">PassportAvailability&nbsp;:
																</td>
																<td><s:textfield name="passportAvailability"
																		id="passportAvailability" cssClass="inputTextBlue"
																		readonly="true" /></td>


															</tr>

															<tr>
																<td class="fieldLabel">GoalsOfAttending&nbsp;:</td>

																<td colspan="3"><s:textarea rows="2" cols="65"
																		id="goalsOfAttending" name="goalsOfAttending"
																		cssClass="inputTextareaOverlay1" style="width: 441px;"
																		readonly="true" /></td>



															</tr>
															<tr>
																<td class="fieldLabel">CreatedBy&nbsp;:</td>
																<td><s:textfield name="createdBy" id="createdBy"
																		cssClass="inputTextBlue" readonly="true" /></td>

																<td class="fieldLabel">CreatedDate&nbsp;:</td>
																<td><s:textfield name="createdDate"
																		id="createdDate" cssClass="inputTextBlue"
																		readonly="true" /></td>

															</tr>



														</table>
													</td>
												</tr>
												<tr>
													<td><b><div style="color: darkblue;"
																align="center" id="heading"></div></b></td>
												</tr>
												<tr>
													<td>
														<table id="tblNomineeByDetails" class="gridTable"
															width="600" cellspacing="1" cellpadding="7" border="0"
															align="center" style="margin-left: 70px;">
															<%--   <script type="text/JavaScript" src="<s:url value="/includes/javascripts/wz_tooltip.js"/>"></script> --%>
															<COLGROUP ALIGN="left">
																<COL width="5%">
																<COL width="20%">
																<COL width="10%">
																<COL width="20%">
														</table>
													</td>
												</tr>

											</table>
										</s:form>
									</div>
									<div id="employeeDetailsOverlay" class="overlay"></div>
									<div id="employeeDetailsSpecialBox" class="specialBox"
										style="border-radius: 36px;">

										<s:form theme="simple" align="center">

											<s:hidden name="nomineeId" id="nomineeId" />

											<%--  <s:hidden name="teamId" id="teamId" value="%{#session.teamName}"/> --%>
											<table align="center" border="0" cellspacing="0"
												style="width: 100%;">
												<tr>
													<td colspan="2"
														style="background-color: #288AD1; border-radius: 36px 0 0 0;">
														<h3 style="color: darkblue;" align="left">
															<span id="headerLabelForEmpDetails"
																style="margin-left: 34px;"></span>


														</h3>
													</td>
													<td colspan="2"
														style="background-color: #288AD1; border-radius: 0 36px 0 0;"
														align="right"><a href="#"
														onmousedown="toggleEmployeeOverlay()"> <img
															src="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/closeButton.png"
															style="margin-right: 15px;" />

													</a></td>
												</tr>
												<tr>
													<td colspan="4">
														<div id="loadForEmpDetails"
															style="color: green; display: none;" align="center">Loading...</div>
														<div id="resultMessageForEmpDetails"></div>
													</td>
												</tr>
												<tr>
													<td colspan="4">
														<table style="width: 80%;" align="center">
															<tr>
																<td class="fieldLabel">Employee&nbsp;Name&nbsp;:</td>
																<td colspan="3"><s:textfield name="empNameInfo" 
																		id="empNameInfo" cssClass="inputTextareaOverlay1"
																		style="height: 25px;width: 441px;" readonly="true"  />
															</tr>
															<tr>
																<td class="fieldLabel">Designation&nbsp;</td>
																<td><s:textfield name="titleTypeId"
																		id="titleTypeId" cssClass="inputTextBlue"
																		readonly="true" /></td>

																<td class="fieldLabel">Department&nbsp;:</td>
																<td><s:textfield name="departmentId"
																		id="departmentId" cssClass="inputTextBlue"
																		readonly="true" /></td>

															</tr>
															<tr>
																<td class="fieldLabel">Employee&nbspNo&nbsp;</td>
																<td><s:textfield name="empNo" id="empNo"
																		cssClass="inputTextBlue" readonly="true" /></td>

																<td class="fieldLabel">Living&nbsp;Country&nbsp;:</td>
																<td><s:textfield name="country" id="country"
																		cssClass="inputTextBlue" readonly="true" /></td>

															</tr>
															<tr>
																<td class="fieldLabel">Work&nbsp;Phone&nbsp;:</td>
																<td><s:textfield name="workPhoneNo"
																		id="workPhoneNo" cssClass="inputTextBlue"
																		readonly="true" /></td>
																<td class="fieldLabel">Work&nbsp;Location&nbsp;:</td>
																<td><s:textfield name="locationInfo" id="locationInfo"
																		cssClass="inputTextBlue" readonly="true" /></td>

															</tr>
															<tr>
																<td class="fieldLabel">ReportsTo&nbsp;:</td>
																<td><s:textfield name="empReportsToInfo"
																		id="empReportsToInfo" cssClass="inputTextBlue"
																		readonly="true"/></td>

																<td class="fieldLabel">HireDate&nbsp;:</td>
																<td><s:textfield name="hireDate" id="hireDate"
																		cssClass="inputTextBlue" readonly="true" /></td>

															</tr>


															<tr>
																<td class="fieldLabel">Practice&nbsp;:</td>
																<td><s:textfield name="practiceId" id="practiceId"
																		cssClass="inputTextBlue" readonly="true" /></td>

																<td class="fieldLabel">Operation&nbsp;Contact&nbsp;:
																</td>
																<td><s:textfield name="opsContactId"
																		id="opsContactId" cssClass="inputTextBlue"
																		readonly="true" /></td>


															</tr>


															<tr>
																<td class="fieldLabel">Email&nbsp;:</td>
																<td colspan="3"><s:textfield name="email1"
																		id="email1" cssClass="inputTextBlue"
																		style="height: 25px;width: 441px;" readonly="true" />
																</td>



															</tr>

														</table>
													</td>
												</tr>
											</table>
										</s:form>
									</div>

									<s:form action="eventNominationSearch.action"
										id="frmDBConferenceGrid" name="frmDBConferenceGrid"
										theme="simple">
										<div style="width: 840px;">

											<table cellpadding="0" cellspacing="0" align="left"
												width="100%">

												<tr>
													<td>
														<table border="0" cellpadding="0" cellspacing="1"
															align="center" width="100%">
															<tr>
																<td class="headerText" colspan="11"><img alt="Home"
																	src="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/spacer.gif"
																	width="100%" height="13px" border="0"> <a
																	href="<s:url value="../events/eventNominationSearch.action"/>"
																	style="align: center;"> <img alt="Back to List"
																		src="<s:url value="/includes/images/backToList_66x19.gif"/>"
																		width="66px" height="19px" border="0" align="bottom"
																		style="float: right"></a></td>

															</tr>
															<tr>
																<td colspan="11" align="right"><span
																	id="resultMessageForStatus" style="text-align: right;"></span>
																</td>
															</tr>
														</table>
														<table border="0" cellpadding="0" cellspacing="2"
															align="center" width="90%">
															<tr>
																<td class="fieldLabel">Event&nbsp;Title&nbsp;:</td>
																<td><s:label cssClass="navigationText"
																		value="%{eventTitle}" /></td>
																<s:hidden id="event_Id" name="event_Id" value="%{id}" />
																<td class="fieldLabel">Event&nbsp;Date&nbsp;:</td>
																<td><s:label cssClass="navigationText"
																		value="%{eventDate}" /></td>
																<td class="fieldLabel" colspan="2">Nominations&nbsp;End&nbsp;Date:</td>
																<td><s:label cssClass="navigationText"
																		value="%{nominationsExpDate}" /> <s:hidden
																		id="eventLink" name="eventLink" value="%{eventLink}" />
																	<s:hidden id="eventTitle" name="eventTitle"
																		value="%{eventTitle}" /> <s:hidden id="eventLocation"
																		name="eventLocation" value="%{eventLocation}" /> <s:hidden
																		id="eventStartDate" name="eventStartDate"
																		value="%{eventDate}" /> <s:hidden id="eventEndDate"
																		name="eventEndDate" value="%{eventEndDate}" /> <s:hidden
																		id="nominationsExpDate" name="nominationsExpDate"
																		value="%{nominationsExpDate}" /> <s:hidden
																		id="statusConference" name="statusConference"
																		value="%{statusConference}" /> <s:hidden
																		id="nominationExpired" name="nominationExpired"
																		value="%{nominationExpired}" />
																		 <s:hidden
																		id="allowNomineeAddingAccess" name="allowNomineeAddingAccess"
																		value="%{allowNomineeAddingAccess}" />
																		
																		</td>

															</tr>
														</table>
														<table border="0" cellpadding="0" cellspacing="2"
															align="left" width="90%">
															<br>
															<tr>
																<td class="fieldLabel">Name/E-mail :</td>
																<td><s:textfield name="nomineeName"
																		id="nomineeName" cssClass="inputTextBlue" value="" />
																</td>
																<td class="fieldLabel">Nomination:</td>
																<td><s:select
																		list="{'Completed','Incomplete','Approved','Rejected'}"
																		id="nominationState" name="nominationState"
																		cssClass="inputSelect" value="%{nominationState}" /></td>

																<td></td>
																<td><input type="button" value="Search"
																	onclick="eventNomineeDetailsSerach('manual');"
																	class="buttonBg" /></td>
															</tr>

														</table>
													</td>
												</tr>
												<tr>
													<td>

														<table cellpadding="0" cellspacing="0" width="100%">
															<br>
															<tr id="approveTr">
																<td colspan="4" align="right"><input type="button"
																	name="Approve" value="Approve"
																	onclick="updateNomineeStatus('Approved','Approve');"
																	class="buttonBg" id="approveButton" /> <input
																	type="button" name="reject" value="Reject"
																	onclick="updateNomineeStatus('Rejected','Reject');"
																	class="buttonBg" style="margin-right: 45px;"
																	id="rejectButton" /></td>
															</tr>
															<tr>
																<td>
																	<table width="100%" cellpadding="1" cellspacing="1"
																		border="0">
																		<tr>
																			<td>
																				<div id="loadingMessage"
																					style="color: red; display: none;" align="center">
																					<b><font size="4px">Loading...Please
																							Wait...</font></b>
																				</div>

																			</td>
																		</tr>
																		<tr>
																			<td>
																				<%-- <DIV id="loadingMessage"> </DIV> --%>

																				<TABLE id="NomineeDetailsTable" align="center"
																					cellpadding='1' cellspacing='1' border='0'
																					class="gridTable" width='90%'>
																					<COLGROUP ALIGN="left">

																						<COL width="5%">
																						<COL width="30%">
																						<COL width="25%">
																						<COL width="20%">
																						<COL width="15%">
																				</TABLE>

																			</td>
																		</tr>
																		<tr id="addTrNominee" style="display: none">


																			<td align="right" colspan="6">
																				<button id="saveRow" type="button"
																					onclick="addNomineeDetails();"
																					style="background-color: #3E93D4; border-radius: 4px; float: right; margin-right: 40px; color: #FFF; border: 1px solid #ddd; font-size: 12px; width: 100px; height: 30px;">
																					Add&nbsp;Nominee</button>
																			</td>
																		</tr>
																	</table>
																</td>
															</tr>

														</table>


													</td>
												</tr>


												<script type="text/javascript">
													var conferencePager = new ReviewPager(
															'conferenceResults',
															10);
													conferencePager.init();
													conferencePager
															.showPageNav(
																	'conferencePager',
																	'conferencePageNavPosition');
													conferencePager.showPage(1);
												</script>
											</table>
										</div>
									</s:form>


									<%--  </sx:div > --%>
								</div>
								<%--  <sx:div id="IssuesSearchTab" label="Issues Search" > --%>

							</div> <!--//END TAB : --> <%--  </sx:tabbedpanel> --%> <script
								type="text/javascript"
								src="<s:url value="/includes/javascripts/reviews/ajaxfileupload.js"/>"></script>

							<!--//END TABBED PANNEL : --> <script type="text/javascript">
								var countries = new ddtabcontent("accountTabs")
								countries.setpersist(true)
								countries.setselectedClassTarget("link") //"link" or "linkparent"
								countries.init()
							</script>
						</td>
					</tr>
				</table>
			</td>
			<!--//END DATA COLUMN : Coloumn for Screen Content-->
		</tr>



		<!--//END DATA RECORD : Record for LeftMenu and Body Content-->

		<!--//START FOOTER : Record for Footer Background and Content-->
		<tr class="footerBg">
			<td align="center"><s:include
					value="/includes/template/Footer.jsp" /></td>
		</tr>


		<!--//END FOOTER : Record for Footer Background and Content-->

	</table>
	<!--//END MAIN TABLE : Table for template Structure-->

	<script>
		$(document).ready(function() {
			eventNomineeDetailsSerach('auto');
		});
		
	</script>

<script type="text/javascript">
	function addFlightRow(flag) {
		document.getElementById("resultMessageForLogisticDetails").innerHTML ="";
		var option = "";
		var rowCount = $("#tblLogisticDetails td").closest("tr").length;
		var validData=rowCount/4;
		for(var k=1;k<=validData;k++){
			var allow=0;
		var flightname=document.getElementById("flightName"+k).value;
		var flyFromCity=document.getElementById("flyFromCity"+k).value;
		var flyToCity=document.getElementById("flyToCity"+k).value;
		var flyDate=document.getElementById("flyDate"+k).value;
		var flyTime=document.getElementById("flyTime"+k).value;
		if(flightname.trim()!='' && flyFromCity.trim()!='' && flyToCity.trim()!='' && flyDate.trim()!='' && flyTime.trim()!='')
			allow=1;
		}
		
		if(allow==1){
		
			var ids=(rowCount/4)+1;
			
			rowCount = rowCount + 1;

		if (rowCount > 1 && rowCount < 25 && flag == 0) {
		

			$('#tblLogisticDetails').append(
					
					
					'<tr id="label' + (rowCount) + '"></tr>');
			$('#label' + (rowCount))
			.html(
			"<td class='fieldLabel' colspan='' style='text-align: left; color: #3e93d4; font-weight: bold; font-size: 14px;'>Flight&nbsp;Details&nbsp;:</td>");
	
			
					$('#tblLogisticDetails').append(
							
							
							'<tr id="fightRowId' + (rowCount) + '"></tr>');

					$('#fightRowId' + (rowCount))
							.html(
							"<td class='fieldLabel'>Flight&nbsp;Code:</td>"
									+ "<td><input type='text' name='flightName"+(ids)+"' id='flightName"+(ids)+"' class='inputTextBlue' theme='simple' value='' style='width: 162px;'/></td>");
					$('#tblLogisticDetails').append(
							
							
							'<tr id="fightRowId' + (rowCount+1) + '"></tr>');

					$('#fightRowId' + (rowCount+1))
							.html(
							"<td class='fieldLabel'>From&nbsp;City&nbsp;:</td>"
									+ "<td><input type='text' name='flyFromCity"+(ids)+"' id='flyFromCity"+(ids)+"' class='inputTextBlue' theme='simple' value='' style='width: 162px;'/></td>"
									+ "<td class='fieldLabel'>To&nbsp;City&nbsp;:</td>"
									+ "<td><input type='text' name='flyToCity"+(ids)+"' id='flyToCity"+(ids)+"' class='inputTextBlue' theme='simple' value='' style='width: 162px;' /></td>");
					
					
					
					$('#tblLogisticDetails').append(
							'<tr id="fightRowId' + (rowCount+2) + '"></tr>');
					
					$('#fightRowId' + (rowCount+2))
					.html(
					"<td class='fieldLabel'>Date&nbsp;:</td>"
							+ "<td><input type='text' name='flyDate"+(ids)+"' id='flyDate"+(ids)+"' class='inputTextBlue' onchange='validateTimestamp(this);' value='' style='width: 162px;' />"
							+ "<a href='javascript:datePick(\"flyDate"+(ids)+"\");'> <img src='../../includes/images/showCalendar.gif' width='20' height='20' border='0'></a></td>"
							+" <td class='fieldLabel'>Time&nbsp;:</td>"
							+ "<td colspan=''><input type='text' name='flyTime"+(ids)+"' id='flyTime"+(ids)+"' maxLength='5' placeholder='HH:MM'	class='inputSelectSmall' onchange='timeValidator(this);' onkeyup='enterdTime(this);' 	value='' />"
							+" <select id='flyMidDayFrom"+(ids)+"' name='flyMidDayFrom"+(ids)+"' class='inputSelectSmall'><option value='AM'>AM</option><option value='PM'>PM</option></select> <select id='timeZone"+(ids)+"' name='timeZone"+(ids)+"' class='inputSelectSmall'><option value='IST'>IST</option><option value='EST'>EST</option></select> "
						
							
							+"</td>");
					
			//jQuery("#tblEntAttributes tbody").append(newRowContent);
		}else if (flag == 1) {
		

			$('#tblLogisticDetails').append(
					
					
					'<tr id="label' + (rowCount) + '"></tr>');
			$('#label' + (rowCount))
			.html(
			"<td class='fieldLabel' colspan='' style='text-align: left; color: #3e93d4; font-weight: bold; font-size: 14px;'>Flight&nbsp;Details&nbsp;:</td>");
	
			
					$('#tblLogisticDetails').append(
							
							
							'<tr id="fightRowId' + (rowCount) + '"></tr>');

					$('#fightRowId' + (rowCount))
							.html(
							"<td class='fieldLabel'>Flight&nbsp;Code:</td>"
									+ "<td><input type='text' name='flightName"+(ids)+"' id='flightName"+(ids)+"' class='inputTextBlue' theme='simple' value='' style='width: 162px;'/></td>");
					$('#tblLogisticDetails').append(
							
							
							'<tr id="fightRowId' + (rowCount+1) + '"></tr>');

					$('#fightRowId' + (rowCount+1))
							.html(
							"<td class='fieldLabel'>From&nbsp;City&nbsp;:</td>"
									+ "<td><input type='text' name='flyFromCity"+(ids)+"' id='flyFromCity"+(ids)+"' class='inputTextBlue' theme='simple' value='' style='width: 162px;'/></td>"
									+ "<td class='fieldLabel'>To&nbsp;City&nbsp;:</td>"
									+ "<td><input type='text' name='flyToCity"+(ids)+"' id='flyToCity"+(ids)+"' class='inputTextBlue' theme='simple' value='' style='width: 162px;' /></td>");
					
					
					
					$('#tblLogisticDetails').append(
							'<tr id="fightRowId' + (rowCount+2) + '"></tr>');
					
					$('#fightRowId' + (rowCount+2))
					.html(
					"<td class='fieldLabel'>Date&nbsp;:</td>"
							+ "<td><input type='text' name='flyDate"+(ids)+"' id='flyDate"+(ids)+"' class='inputTextBlue' onchange='validateTimestamp(this);' value='' style='width: 162px;' />"
							+ "<a href='javascript:datePick(\"flyDate"+(ids)+"\");'> <img src='../../includes/images/showCalendar.gif' width='20' height='20' border='0'></a></td>"
							+" <td class='fieldLabel'>Time&nbsp;:</td>"
							+ "<td colspan=''><input type='text' name='flyTime"+(ids)+"' id='flyTime"+(ids)+"' maxLength='5' placeholder='HH:MM'	class='inputSelectSmall' onchange='timeValidator(this);' onkeyup='enterdTime(this);' 	value='' />"
							+" <select id='flyMidDayFrom"+(ids)+"' name='flyMidDayFrom"+(ids)+"' class='inputSelectSmall'><option value='AM'>AM</option><option value='PM'>PM</option></select> <select id='timeZone"+(ids)+"' name='timeZone"+(ids)+"' class='inputSelectSmall'><option value='IST'>IST</option><option value='EST'>EST</option></select> "
						
							
							+"</td>");
					
			//jQuery("#tblEntAttributes tbody").append(newRowContent);
		} else{
			
			document.getElementById("resultMessageForLogisticDetails").innerHTML = "<font color=red  size='2.5'>You have created Maximun Rows!";

			return false;
		}
		}else{
			document.getElementById("resultMessageForLogisticDetails").innerHTML = "<font color=red  size='2.5'>Please Enter all the Required Data!";

			return false;
		}

	}

	</script>
	<script type='text/JavaScript'>
	var cal1 = new CalendarTime(document.forms['frmLogisticDetails'].elements['flyDate1']);
	cal1.year_scroll = true;
	cal1.time_comp = false;
	
	
	function datePick(datevar){
		var cal3 = new CalendarTime(document.forms['frmLogisticDetails'].elements[datevar]);
		cal3.year_scroll = true;
		cal3.time_comp = false;
		cal3.popup();
	}
	
/* 	var cal5 = new CalendarTime(document.forms['frmLogisticDetails'].elements['flyDepartureDate5']);
	cal5.year_scroll = true;
	cal5.time_comp = false;
	var cal7 = new CalendarTime(document.forms['frmLogisticDetails'].elements['flyDepartureDate7']);
	cal7.year_scroll = true;
	cal7.time_comp = false;
	var cal9 = new CalendarTime(document.forms['frmLogisticDetails'].elements['flyDepartureDate9']);
	cal9.year_scroll = true;
	cal9.time_comp = false;
	var cal11 = new CalendarTime(document.forms['frmLogisticDetails'].elements['flyDepartureDate11']);
	cal11.year_scroll = true;
	cal11.time_comp = false; */
	</script>

</body>
</html>

