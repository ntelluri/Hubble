 <%-- 
   Document   : External Events
   Created on : Jan 9, 2017, 3:37:32 PM
   Author     : miracle
--%>



<%@page import="org.apache.commons.lang.StringEscapeUtils"%>
<%@page import="java.net.URLDecoder"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.lang.String"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Iterator"%>
<!-- 
 *******************************************************************************
 *
 * Project : Mirage V2
 *
 * Package :
 *
 * Date    :  November 20, 2007, 3:25 PM
 *
 * Author  : Rajasekhar Yenduva<ryenduva@miraclesoft.com>
 *
 * File    : IssuesList.jsp
 *
 * Copyright 2007 Miracle Software Systems, Inc. All rights reserved.
 * MIRACLE SOFTWARE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * *****************************************************************************
 */
-->
<%@ page contentType="text/html; charset=UTF-8"
	errorPage="../exception/ErrorDisplay.jsp"%>

<%@ taglib prefix="s" uri="/struts-tags"%>
<%--<%@ taglib prefix="sx" uri="/struts-dojo-tags" %> --%>
<%@ page import="com.freeware.gridtag.*"%>
<%@ page import="java.sql.Connection"%>
<%@ page import="java.sql.SQLException"%>
<%@ page import="com.mss.mirage.util.ConnectionProvider"%>
<%@ page import="com.mss.mirage.util.ApplicationConstants"%>
<%@ taglib uri="/WEB-INF/tlds/datagrid.tld" prefix="grd"%>
<%@ page import="javax.servlet.http.HttpServletRequest"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>Hubble Organization Portal :: Event Suggestions</title>

<link rel="stylesheet" type="text/css"
	href="<s:url value="/includes/css/mainStyle.css?version=1.1"/>">
<link rel="stylesheet" type="text/css"
	href="<s:url value="/includes/css/GridStyle.css"/>">

<link rel="stylesheet" type="text/css"
	href="<s:url value="/includes/css/tabedPanel.css"/>">

<link rel="stylesheet" type="text/css"
	href="<s:url value="/includes/css/leftMenu.css"/>">

<script type="text/JavaScript"
	src="<s:url value="/includes/javascripts/CalendarTime.js"/>"></script>
<script type="text/JavaScript"
	src="<s:url value="/includes/javascripts/AppConstants.js"/>"></script>
<script type="text/JavaScript"
	src="<s:url value="/includes/javascripts/tabedPanel.js"/>"></script>

<link rel="stylesheet" type="text/css"
	href="<s:url value="/includes/css/new/x0popup.min.css?version=1.0"/>">
<script type="text/JavaScript"
	src="<s:url value="/includes/javascripts/payroll/x0popup.min.js"/>"></script>


<script type="text/JavaScript"
	src='<s:url value="/includes/javascripts/GridNavigation.js"/>'></script>
<%--  <script type="text/JavaScript" src="<s:url value="/includes/javascripts/EmployeeAjax.js"/>"></script> --%>
<script type="text/JavaScript"
	src="<s:url value="/includes/javascripts/reviews/jquery.min.js"/>"></script>
<script type="text/javascript"
	src="<s:url value="/includes/javascripts/reviews/jquery.js"/>"></script>

<%--   <script type="text/javascript" src="<s:url value="/includes/javascripts/reviews/ajaxfileupload.js"/>"></script>   --%>



<%-- for issue reminder popup --%>



<script type="text/JavaScript"
	src="<s:url value="/includes/javascripts/marketing/EventsPosting.js?ver=1.12"/>"></script>

<s:include value="/includes/template/headerScript.html" />
</head>
<body class="bodyGeneral" oncontextmenu="return false;">


	<%
		String contextPath = request.getContextPath();
	%>
	<!--//START MAIN TABLE : Table for template Structure-->
	<table class="templateTable1000x580" align="center" cellpadding="0"
		cellspacing="0">

		<!--//START HEADER : Record for Header Background and Mirage Logo-->
		<tr class="headerBg">
			<td valign="top"><s:include
					value="/includes/template/Header.jsp" /></td>
		</tr>
		<!--//END HEADER : Record for Header Background and Mirage Logo-->

		<!--//START DATA RECORD : Record for LeftMenu and Screen Content-->
		<tr>
			<td>
				<table class="innerTable1000x515" cellpadding="0" cellspacing="0">
					<tr>
						<!--//START DATA COLUMN : Coloumn for LeftMenu-->
						<td width="150px;" class="leftMenuBgColor" valign="top"><s:include
								value="/includes/template/LeftMenu.jsp" /></td>
						<!--//START DATA COLUMN : Coloumn for LeftMenu-->

						<!--//START DATA COLUMN : Coloumn for Screen Content-->
						<td width="850px" class="cellBorder" valign="top"
							style="padding: 5px 5px;">
							<!--//START TABBED PANNEL : --> <%
 	//out.print("naga::"+ request.getAttribute("subnavigateTo").toString());
 	/// session.setAttribute(ApplicationConstants.ISSUE_TO_TASK,request.getAttribute("subnavigateTo").toString());
 %>

							<ul id="accountTabs" class="shadetabs">

								<%--    <% if(request.getParameter("issueList")==null)
                                           {%> --%>

								<li><a href="#" rel="conferenceTab">Nominations</a></li>

								<%--   <%}else{%>
                                       <li ><a href="#" class="selected" rel="IssuesSearchTab">Tasks Search</a></li>
                                        <% }%> --%>

							</ul> <%-- <sx:tabbedpanel id="IssuesPanel" cssStyle="width: 845px; height: 500px;padding:5px 5px;" doLayout="true"> --%>
							<div
								style="border: 1px solid gray; width: 845px; height: 500px; overflow: auto; margin-bottom: 1em;">


								<div id="conferenceTab" class="tabcontent">
									<div id="overlayForFacilityDetails" class="overlay"></div>
									<div id="specialBoxForFacilityDetails" class="specialBox"
										style="border-radius: 30px; margin-top: -35px;">

										<div id="facilityMaintenanceDiv">
											<table align="center" border="0" cellspacing="0"
												style="width: 112%; background-color: #eaeded; color: black; border-radius: 30px 30px 30px 30px; border: 0px solid black;">
												<tr>
													<td colspan="2"
														style="background-color: #288AD1; border-radius: 30px 0 0;">
														<h3 style="color: darkblue;" align="left">
															<span id="headerLabelFacility" style="margin-left: 20px;"></span>


														</h3>
													</td>
													<td colspan="2"
														style="background-color: #288AD1; border-radius: 0 30px 0 0;"
														align="right"><a href="#"
														onmousedown="toggleCloseFacility()"> <img
															src="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/closeButton.png"
															style="margin-right: 15px;" />

													</a></td>
												</tr>

												<tr>
													<td colspan="4">
														<div id="blockDivForCoordinatorDetails"
															class="transperentblockDiv"
															style="display: none; align: center; position: fixed; top: 50%; left: 48%;">
															<font
																style="font-weight: bold; position: absolute; color: #fff; font-size: 23px; top: -53px; left: 5%">
																Processing <font id="fontId">...</font>
															</font> <img src='../../includes/images/ajax-loader.gif'
																WIDTH='35%' HEIGHT='5%' alt='block'>
														</div>
														<div id="displyCoordinatorId" class="transperentdisplyDiv"></div>
														<div id="loadForFacilityOverLay"
															style="color: red; display: none;" align="center">Loading
															Please Wait...</div>
														<div id="resultMessageForFacility" align="right"></div>
													</td>
												</tr>
												<tr>
													<td>
														<table cellpadding="2" cellspacing="2" border="0"
															width="90%" align="center" style="padding-left: 65px;">
															<tr>

																<td class="fieldLabel">Event&nbsp;Title&nbsp;:</td>
																<td colspan="3"><span id="confTitle"></span></td>
																
															</tr>
															<tr>

																<td class="fieldLabel">Event&nbsp;Start&nbsp;Date&nbsp;:</td>
																<td><span id="confStartDate"></span></td>
																<td class="fieldLabel">Event&nbsp;End&nbsp;Date&nbsp;:</td>
																<td><span id="confEndDate"></span></td>
															</tr>
															<tr>

																<td class="fieldLabel">Nomination&nbsp;End&nbsp;Date&nbsp;:</td>
																<td><span id="nomineeEndDate"></span></td>
																<td class="fieldLabel">Location&nbsp;:</td>
																<td><span id="conflocation"></span></td>
															</tr>
															<tr>
																<td class="fieldLabel" colspan=""
																	style="text-align: left; color: #3e93d4; font-weight: bold; font-size: 14px">Event&nbsp;Coordinator&nbsp;:</td>
																<input type="hidden" id="confId" name="confId" />
																<input type="hidden" id="eventId" name="eventId" />
															</tr>
															<tr>

																<td class="fieldLabel">Name&nbsp;:<FONT color="red"><em>*</em></FONT></td>
																<td colspan="3"><s:textfield name="employeeName"
																		id="employeeName" value="" style="width: 230px;"
																		autocomplete="off" cssClass="inputTextBlue"
																		onkeyup="getEmplyeeNames();"
																		placeholder="Enter Employee Name" /><label
																	id="rejectedId"></label>
																<div class="fieldLabelLeft" id="validationMessage"></div>

																	<div
																		style="display: none; position: absolute; overflow: auto; z-index: 500;"
																		id="menu-popup">
																		<table id="completeTable" border="1"
																			bordercolor="#e5e4f2"
																			style="border: 1px dashed gray;" cellpadding="0"
																			class="cellBorder" cellspacing="0"></table>
																	</div> <s:hidden id="empLoginId" name="empLoginId" /></td>

															</tr>
															<tr>

																<td class="fieldLabel">Email&nbsp;:<FONT
																	color="red"><em>*</em></FONT></td>
																<td><s:textfield name="eventCoordinatorEmail"
																		id="eventCoordinatorEmail" cssClass="inputTextBlue"
																		value="" style="width: 230px;" readonly="true" /></td>
																<td class="fieldLabel">Phone&nbsp;:<FONT
																	color="red"><em>*</em></FONT></td>
																<td><s:textfield name="eventCoordinatorPhone"
																		id="eventCoordinatorPhone" cssClass="inputTextBlue"
																		value="" /></td>
															</tr>
															<br>
															<tr>
																<td class="fieldLabel" colspan=""
																	style="text-align: left; color: #3e93d4; font-weight: bold; font-size: 14px">On-site&nbsp;Coordinator&nbsp;:</td>
															</tr>
															<tr>

																<td class="fieldLabel">Name:</td>
																<td colspan="3"><s:textfield name="onsiteCodrName"
																		id="onsiteCodrName" cssClass="inputTextBlue" value=""
																		style="width: 230px;" /></td>

															</tr>
															<tr>

																<td class="fieldLabel">Email&nbsp;:</td>
																<td><s:textfield name="onsiteCodeEmail"
																		id="onsiteCodeEmail" cssClass="inputTextBlue" value=""
																		style="width: 230px;" /></td>
																<td class="fieldLabel">Phone&nbsp;:</td>
																<td><s:textfield name="onsiteCodePhone"
																		id="onsiteCodePhone" cssClass="inputTextBlue" value="" />
																</td>
															</tr>



															<tr id="updateTr" style="display: none;">
																<td colspan="4" align="right"><button id="addRow"
																		type="button" onclick="updateCoordinatorsInfo();"
																		style="background-color: #3E93D4; border-radius: 4px; float: right; color: #FFF; border: 1px solid #ddd; font-size: 12px; height: 30px; margin-right: 13px;">
																		Update&nbsp;&nbsp;</button></td>
															</tr>


														</table>

													</td>
												</tr>


											</table>

										</div>
									</div>

									<s:form action="eventNominationSearch.action"
										id="frmDBConferenceGrid" name="frmDBConferenceGrid"
										theme="simple">
										<div style="width: 840px;">

											<table cellpadding="0" cellspacing="0" align="left"
												width="100%">

												<tr>
													<td>
														<table border="0" cellpadding="0" cellspacing="2"
															align="left" width="100%">
															<tr>
																<td class="headerText" colspan="11"><img alt="Home"
																	src="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/spacer.gif"
																	width="100%" height="13px" border="0"></td>
															</tr>
															<tr colspan="3">

																<td class="fieldLabel">Events List:</td>
																<td><s:select list="{'Upcoming','Past','All'}"
																		id="eventsListFor" name="eventsListFor"
																		cssClass="inputSelect" value="%{eventsListFor}" /></td>

																<td class="fieldLabel">Key&nbsp;Words:</td>
																<td><s:textfield name="title" id="title"
																		cssClass="inputTextBlue" value="%{title}" /></td>

															</tr>
															<tr>


																<td class="fieldLabel">Start Date From:</td>
																<td><s:textfield name="createdDateFrom"
																		id="createdDateFrom" cssClass="inputTextBlue"
																		onchange="validateTimestamp(this);" /> <a
																	href="javascript:cal1.popup();"> <img
																		src="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/showCalendar.gif"
																		width="20" height="20" border="0"></a></td>
																<td class="fieldLabel">To:<FONT color="red"
																	SIZE="0.5"><em>*</em></FONT></td>
																<td><s:textfield name="createdDateTo"
																		id="createdDateTo" cssClass="inputTextBlue"
																		onchange="validateTimestamp(this);" /> <a
																	href="javascript:cal2.popup();"> <img
																		src="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/showCalendar.gif"
																		width="20" height="20" border="0"></a></td>
															</tr>


															<tr>
																<td class="fieldLabel">Nomination:</td>
																<td><s:select list="{'Active','InActive'}"
																		id="nominationState" headerKey="" headerValue="All"
																		name="nominationState" cssClass="inputSelect"
																		value="%{nominationState}" /></td>

																<td></td>
																<td><s:submit name="search" value="Search"
																		cssClass="buttonBg" /></td>
															</tr>
														</table>
													</td>
												</tr>




												<s:if test="#session.eventNominationsList != null">
													<tr>
														<td>
															<table id="conferenceResults" cellpadding='1'
																cellspacing='1' border='0' class="gridTable" width='90%'
																align="center">
																<br>
																<%
																	java.util.List conferenceMainList = (java.util.List) session.getAttribute("eventNominationsList");
																			if (conferenceMainList.size() > 0) {
																%>

																<tr class="gridHeader">
																	<th>S.No</th>
																	<th>Event Name</th>
																	<th>Event Date</th>
																	<!-- <th>EventDesc</th> -->

																	<th>Nominations<br>Expiry Date
																	</th>
																	<th>Created By</th>
																	<th>Nominations</th>



																</tr>
																<%
																	for (int i = 0; i < conferenceMainList.size(); i++) {
																%>
																<tr CLASS="gridRowEven">
																	<%
																		//java.util.List subList = (java.util.List)mainList.get(i);
																						java.util.Map subList = (java.util.Map) conferenceMainList.get(i);
																						//  for (int j = 0; j < subList.size(); j++) {
																	%>
																	<td class="title">
																		<%
																			out.println(i + 1);
																		%>
																	</td>



																	<td class="title"><a
																		href='javascript:getCoordinatorDetailsOverLay(<%=subList.get("event_id")%>)'>
																			<%
																				out.println(subList.get("event_name"));
																								// out.println(subList.get("event_startdate").toString().split("\\ ")[0]);
																			%>
																	</a></td>

																	<td class="title">
																		<%
																			out.println(subList.get("event_startDate"));
																							// out.println(subList.get("event_startdate").toString().split("\\ ")[0]);
																		%>
																	</td>

																	<td class="title">
																		<%
																			out.println(subList.get("nomination_endDate"));
																		%>
																	</td>
																	<td class="title">
																		<%
																			out.println(subList.get("event_createdBy"));
																		%>
																	</td>

																	<td class="title" align="center"><a
																		href='eventNomineeDetails.action?id=<%=subList.get("event_id")%>'><img
																			SRC='../../includes/images/go_21x21.gif' WIDTH=18
																			HEIGHT=15 BORDER=0 ALTER='Click to Add'></a></td>





																	<%
																		//   }
																	%>
																</tr>
																<%
																	}
																			} else {
																%>
																<tr>
																	<td>
																		<%
																			// String contextPath = request.getContextPath();
																						// out.println("<img  border='0' align='top'  src='"+contextPath+"/includes/images/alert.gif'/><b> No Records Found to Display!</b>");

																						out.println("<img  border='0' align='top'  src='" + contextPath
																								+ "/includes/images/alert.gif'/> <font color='white'><b>No records found!</b></font>");
																						// }
																		%>
																	</td>
																</tr>
																<%
																	}
																%>

															</table>
														</td>
													</tr>



													<%
														if (conferenceMainList.size() != 0) {
													%>

													<tr>

														<td align="right" colspan="4"
															style="background-color: white; margin-right: 43px;">
															<div align="right" id="conferencePageNavPosition">hello</div>
														</td>
													</tr>

													<%
														}
													%>
												</s:if>



												<script type="text/JavaScript">
													var cal1 = new CalendarTime(
															document.forms['frmDBConferenceGrid'].elements['createdDateFrom']);
													cal1.year_scroll = true;
													cal1.time_comp = false;

													var cal2 = new CalendarTime(
															document.forms['frmDBConferenceGrid'].elements['createdDateTo']);
													cal2.year_scroll = true;
													cal2.time_comp = false;
												</script>
												<script type="text/javascript">
													var conferencePager = new ReviewPager(
															'conferenceResults',
															10);
													conferencePager.init();
													conferencePager
															.showPageNav(
																	'conferencePager',
																	'conferencePageNavPosition');
													conferencePager.showPage(1);
												</script>
											</table>
										</div>
									</s:form>


									<%--  </sx:div > --%>
								</div>
								<%--  <sx:div id="IssuesSearchTab" label="Issues Search" > --%>

							</div> <!--//END TAB : --> <%--  </sx:tabbedpanel> --%> <script
								type="text/javascript"
								src="<s:url value="/includes/javascripts/reviews/ajaxfileupload.js"/>"></script>

							<!--//END TABBED PANNEL : --> <script type="text/javascript">
								var countries = new ddtabcontent("accountTabs")
								countries.setpersist(true)
								countries.setselectedClassTarget("link") //"link" or "linkparent"
								countries.init()
							</script>
						</td>
					</tr>
				</table>
			</td>
			<!--//END DATA COLUMN : Coloumn for Screen Content-->
		</tr>



		<!--//END DATA RECORD : Record for LeftMenu and Body Content-->

		<!--//START FOOTER : Record for Footer Background and Content-->
		<tr class="footerBg">
			<td align="center"><s:include
					value="/includes/template/Footer.jsp" /></td>
		</tr>


		<!--//END FOOTER : Record for Footer Background and Content-->

	</table>
	<!--//END MAIN TABLE : Table for template Structure-->





</body>
<script type="text/javascript">
	$(window).load(function() {
		init();
	});
</script>

</html>


