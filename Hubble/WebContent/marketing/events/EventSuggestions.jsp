<%-- 
   Document   : External Events
   Created on : Jan 9, 2017, 3:37:32 PM
   Author     : miracle
--%>



<%@page import="org.apache.commons.lang.StringEscapeUtils"%>
<%@page import="java.net.URLDecoder"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.lang.String"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Iterator"%>
<!-- 
 *******************************************************************************
 *
 * Project : Mirage V2
 *
 * Package :
 *
 * Date    :  November 20, 2007, 3:25 PM
 *
 * Author  : Rajasekhar Yenduva<ryenduva@miraclesoft.com>
 *
 * File    : IssuesList.jsp
 *
 * Copyright 2007 Miracle Software Systems, Inc. All rights reserved.
 * MIRACLE SOFTWARE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * *****************************************************************************
 */
-->
<%@ page contentType="text/html; charset=UTF-8" errorPage="../exception/ErrorDisplay.jsp"%>

<%@ taglib prefix="s" uri="/struts-tags" %>
<%--<%@ taglib prefix="sx" uri="/struts-dojo-tags" %> --%>
<%@ page import="com.freeware.gridtag.*" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="com.mss.mirage.util.ConnectionProvider"%>
<%@ page import="com.mss.mirage.util.ApplicationConstants"%>
<%@ taglib uri="/WEB-INF/tlds/datagrid.tld" prefix="grd"%>
<%@ page import="javax.servlet.http.HttpServletRequest"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <title>Hubble Organization Portal :: Event Suggestions</title>

        <link rel="stylesheet" type="text/css" href="<s:url value="/includes/css/mainStyle.css"/>">
        <link rel="stylesheet" type="text/css" href="<s:url value="/includes/css/GridStyle.css"/>">

        <link rel="stylesheet" type="text/css" href="<s:url value="/includes/css/tabedPanel.css"/>">

        <link rel="stylesheet" type="text/css" href="<s:url value="/includes/css/leftMenu.css"/>">

        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/CalendarTime.js"/>"></script>   
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/AppConstants.js"/>"></script>
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/tabedPanel.js"/>"></script>




        <script type="text/JavaScript" src='<s:url value="/includes/javascripts/GridNavigation.js"/>'></script>
        <%--  <script type="text/JavaScript" src="<s:url value="/includes/javascripts/EmployeeAjax.js"/>"></script> --%>
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/reviews/jquery.min.js"/>"></script>
        <script type="text/javascript" src="<s:url value="/includes/javascripts/reviews/jquery.js"/>"></script>   

        <%--   <script type="text/javascript" src="<s:url value="/includes/javascripts/reviews/ajaxfileupload.js"/>"></script>   --%>



        <%-- for issue reminder popup --%>

     

        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/marketing/EventsPosting.js?ver=1.5"/>"></script>  

        <s:include value="/includes/template/headerScript.html"/>
    </head>
    <body class="bodyGeneral" oncontextmenu="return false;">


        <%String contextPath = request.getContextPath();
        %>
        <!--//START MAIN TABLE : Table for template Structure-->
        <table class="templateTable1000x580" align="center" cellpadding="0" cellspacing="0">

            <!--//START HEADER : Record for Header Background and Mirage Logo-->
            <tr class="headerBg">
                <td valign="top">
                    <s:include value="/includes/template/Header.jsp"/>                    
                </td>
            </tr>
            <!--//END HEADER : Record for Header Background and Mirage Logo-->

            <!--//START DATA RECORD : Record for LeftMenu and Screen Content-->
            <tr>
                <td>
                    <table class="innerTable1000x515" cellpadding="0" cellspacing="0">
                        <tr>
                            <!--//START DATA COLUMN : Coloumn for LeftMenu-->
                            <td width="150px;" class="leftMenuBgColor" valign="top"> 
                                <s:include value="/includes/template/LeftMenu.jsp"/> 
                            </td>
                            <!--//START DATA COLUMN : Coloumn for LeftMenu-->

                            <!--//START DATA COLUMN : Coloumn for Screen Content-->
                            <td width="850px" class="cellBorder" valign="top" style="padding:5px 5px ;">
                                <!--//START TABBED PANNEL : -->
                                <%
                                    //out.print("naga::"+ request.getAttribute("subnavigateTo").toString());
                                    /// session.setAttribute(ApplicationConstants.ISSUE_TO_TASK,request.getAttribute("subnavigateTo").toString());
%>

                                <ul id="accountTabs" class="shadetabs" >

                                    <%--    <% if(request.getParameter("issueList")==null)
                                           {%> --%>

                                    <li ><a href="#" rel="conferenceTab">Event&nbsp;Suggestions</a></li>

                                    <%--   <%}else{%>
                                       <li ><a href="#" class="selected" rel="IssuesSearchTab">Tasks Search</a></li>
                                        <% }%> --%>

                                </ul>


                                <%-- <sx:tabbedpanel id="IssuesPanel" cssStyle="width: 845px; height: 500px;padding:5px 5px;" doLayout="true"> --%>
                                <div  style="border:1px solid gray; width:845px;height: 500px; overflow:auto; margin-bottom: 1em;">

                                    
                                    <div id="conferenceTab" class="tabcontent" >
                                      
                                           <div id="eveSuggestionOverlay" ></div> 
                                        <div id="eveSuggestionSpecialBox" style="border-radius: 36px;width: 600px;">
                                            <s:form theme="simple"  align="center" name="frmEveSuggestion" id="frmEveSuggestion" >

                                                <s:hidden name="eveSuggestionId" id="eveSuggestionId"/>

                                                <s:hidden name="id" id="id"/>
                                                


                                                <table align="center" border="0" cellspacing="0" style="width:100%;" >
                                                    <tr>                               
                                                        <td colspan="2" style="background-color: #288AD1;border-radius: 36px 0 0 0;" >
                                                            <h3 style="color:darkblue;" align="left">
                                                                <span id="eveSuggestionHeaderLabel" style="margin-left: 34px;"></span>


                                                            </h3></td>
                                                        <td colspan="2" style="background-color: #288AD1;border-radius: 0 36px 0 0;" align="right">

                                                            <a href="#" onmousedown="toggleOverlayForEveSuggestion()" >
                                                                <img src="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/closeButton.png" style="margin-right: 15px;" /> 

                                                            </a>  

                                                        </td></tr>
                                                    <tr>
                                                        <td colspan="">
                                                            <div id="eveSuggestionLoad" style="color: green;display: none;">Loading..</div>
                                                            <div id="eveSuggestionResultMessage"></div>
                                                        </td>
                                                    </tr>    
                                                    <tr><td colspan="">
                                                            <table style="width:90%;margin-left: 56px;" align="center" border="0">
                                                               
                                                                <tr>
                                                                    <td align="left" class="fieldLabel" >Event&nbsp;Name:</td>
                                                                    <td><s:textfield name="event_name" id="event_name" cssClass="inputTextBlue" /></td>
                                                                    <td align="left" class="fieldLabel" >City:</td>
                                                                      <td><s:textfield name="event_city" id="event_city" cssClass="inputTextBlue" /></td>
                                                                   
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left" class="fieldLabel" >State:</td>
                                                                     <td><s:textfield name="event_state" id="event_state" cssClass="inputTextBlue" /></td>
                                                                    <td align="left" class="fieldLabel" >Country:</td>
                                                                       <td><s:textfield name="event_country" id="event_country" cssClass="inputTextBlue" /></td>
                                                                   
                                                                    </tr>
                                                                 <tr>
                                                                      <td  class="fieldLabel" >Start&nbsp;Date:</td>
                                                                     <td><s:textfield name="event_startDate" id="event_startDate" cssClass="inputTextBlue" /></td>
                                                                   
                                                                    <td class="fieldLabel">End&nbsp;Date:</td>
                                                                      <td><s:textfield name="event_endDate" id="event_endDate" cssClass="inputTextBlue" /></td>
                                                                   
                                                                    
                                                                 </tr>
                                                                

                                                                <tr>
                                                                    <td class="fieldLabel" >Comments: </td>
                                                                    <td colspan="3"><s:textarea rows="2" cols="65" id="evetnt_description" name="evetnt_description" cssClass="inputTextareaOverlay1" style="width: 374px;"/></td>
                                                                    
                                                                </tr>
                                                                <tr >


                                                                    <td class="fieldLabel" >Event&nbsp;Link: </td>
                                                                    <td colspan="3"><s:textfield name="event_link" id="event_link" style="width : 374px;" cssClass="inputTextBlueLargeAccount" onchange="return isValidUrl(this,'emeetResultMessage');" /></td>
                                                                    
                                                                </tr>
                                                               
                                                              
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </s:form>    
                                        </div>

                                        <s:form action="eventSuggestionSearch.action" id="frmDBConferenceGrid" name="frmDBConferenceGrid" theme="simple"> 
                                            <div style="width:840px;"> 

                                                <table cellpadding="0" cellspacing="0" align="left" width="100%">

                                                    <tr>
                                                        <td>
                                                            <table border="0" cellpadding="0" cellspacing="2" align="left" width="100%">
                                                                <tr>
                                                                    <td class="headerText" colspan="11">
                                                                        <img alt="Home" src="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/spacer.gif" width="100%" height="13px" border="0">
                                                                    </td>
                                                                </tr>
                                                                <tr colspan="3">

                                                                    <td class="fieldLabel">Employee&nbsp;Name:</td>
                                                                    <td><s:textfield name="empName" id="empName" cssClass="inputTextBlue" value="%{empName}"/>
                                                                          </td>
                                                                    <td class="fieldLabel">Email:</td>
                                                                    <td>
                                                                        <s:textfield name="emailId"  id="emailId" cssClass="inputTextBlue" value="%{emailId}"/>
                                                                         </td>

                                                                </tr>
                                                                <tr>


                                                                 <td class="fieldLabel">Start Date From:</td>
                                                                    <td><s:textfield name="createdDateFrom" id="createdDateFrom" cssClass="inputTextBlue" onchange="validateTimestamp(this);"/>
                                                                        <a href="javascript:cal1.popup();">
                                                                            <img src="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/showCalendar.gif"
                                                                                 width="20" height="20" border="0"></a>
                                                                    </td>
                                                                    <td class="fieldLabel">To:<FONT color="red" SIZE="0.5"><em>*</em></FONT></td>
                                                                    <td>
                                                                        <s:textfield name="createdDateTo"  id="createdDateTo" cssClass="inputTextBlue" onchange="validateTimestamp(this);"/>
                                                                        <a href="javascript:cal2.popup();">
                                                                            <img src="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/showCalendar.gif"
                                                                                 width="20" height="20" border="0"></a>
                                                                    </td>  </tr>


                                                                <tr>
                                                                       <td class="fieldLabel">Keywords:</td>
                                                                    <td>
                                                                        <s:textfield name="keywords"  id="keywords" cssClass="inputTextBlue" value="%{keywords}"/>
                                                                         </td>
                                                                         <td></td>
                                                                    <td>
                                                                        <s:submit name="search" value="Search" cssClass="buttonBg"/>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>




                                                    <s:if test="#session.eventSuggestionsList != null"> 
                                                        <tr>
                                                            <td>
                                                                <table id="conferenceResults" cellpadding='1' cellspacing='1' border='0' class="gridTable" width='90%' align="center">

                                                                    <%java.util.List conferenceMainList = (java.util.List) session.getAttribute("eventSuggestionsList");
                                                                        if (conferenceMainList.size() > 0) {


                                                                    %>

                                                                    <tr class="gridHeader">
                                                                        <th>Sno</th>
                                                                         <th>Event&nbsp;Name</th>
                                                                        <th>Employee&nbsp;Name</th>
                                                                        <!-- <th>EventDesc</th> -->

                                                                        <th>Email&nbsp;Id</th>
                                                                        <th>Phone</th>
                                                                        <th>Created&nbsp;Date</th>
                                                                       


                                                                    </tr>
                                                                    <%

                                                                        for (int i = 0; i < conferenceMainList.size(); i++) {
                                                                    %>
                                                                    <tr CLASS="gridRowEven">
                                                                        <%
                                                                            //java.util.List subList = (java.util.List)mainList.get(i);
                                                                            java.util.Map subList = (java.util.Map) conferenceMainList.get(i);
                                                                            //  for (int j = 0; j < subList.size(); j++) {

                                                                        %>
                                                                        <td class="title">
                                                                            <%
                                                                                    out.println(i+1);
                                                                            %>
                                                                        </td>



                                                                        <td class="title">
                                                                            <a style="color:#C00067;" href="javascript:getEventSuggestionDetails('<%=subList.get("event_id")%>');">
                                                                                <%
                                                                                    out.println(subList.get("event_name"));
                                                                                %></a>
                                                                        </td>    

                                                                        <td class="title">
                                                                            <%
                                                                             out.println(subList.get("EmployeeName"));
                                                                               // out.println(subList.get("event_startdate").toString().split("\\ ")[0]);
                                                                            %>
                                                                        </td>
                                                   
                                                                        <td class="title">
                                                                            <%
                                                                                out.println(subList.get("Email"));

                                                                            %>
                                                                        </td>
                                                                        <td class="title">
                                                                            <%
                                                                                out.println(subList.get("CellPhoneNo"));

                                                                            %>
                                                                        </td>
                                                                        
                                                                         <td class="title">
                                                                            <%
                                                                               out.println(subList.get("event_createdDate").toString().split("\\ ")[0]);

                                                                            %>
                                                                        </td>

                                                                        



                                                                        <%

                                                                            //   }
                                                                        %></tr><%

                                                                            }
                                                                        } else {
                                                                        %>
                                                                    <tr><td>
                                                                            <%
                                                                                // String contextPath = request.getContextPath();
                                                                                // out.println("<img  border='0' align='top'  src='"+contextPath+"/includes/images/alert.gif'/><b> No Records Found to Display!</b>");

                                                                                out.println("<img  border='0' align='top'  src='" + contextPath + "/includes/images/alert.gif'/> <font color='white'><b>No records found!</b></font>");
                                                                                // }

                                                                            %>
                                                                        </td>
                                                                    </tr>
                                                                    <%}%>

                                                                </table>
                                                            </td>
                                                        </tr>



                                                        <%

                                                            if (conferenceMainList.size() != 0) {
                                                        %>

                                                        <tr>

                                                            <td align="right" colspan="4" style="background-color:white;" >
                                                                <div align="right" id="conferencePageNavPosition">hello</div>
                                                            </td>
                                                        </tr> 

                                                        <% }
                                                        %>
                                                    </s:if>



                                                    <script type="text/JavaScript">
                                                        var cal1 = new CalendarTime(document.forms['frmDBConferenceGrid'].elements['createdDateFrom']);
                                                        cal1.year_scroll = true;
                                                        cal1.time_comp = false;

                                                        var cal2 = new CalendarTime(document.forms['frmDBConferenceGrid'].elements['createdDateTo']);
                                                        cal2.year_scroll = true;
                                                        cal2.time_comp = false;
                                                         </script>                            
                                                    <script type="text/javascript">
                                                        var conferencePager = new ReviewPager('conferenceResults', 10); 
                                                        conferencePager.init(); 
                                                        conferencePager.showPageNav('conferencePager', 'conferencePageNavPosition'); 
                                                        conferencePager.showPage(1);
                                                    </script>      
                                                </table>
                                            </div>    
                                        </s:form>  


                                        <%--  </sx:div > --%>
                                    </div>  
                                    <%--  <sx:div id="IssuesSearchTab" label="Issues Search" > --%>

                                </div>
                                <!--//END TAB : -->
                                <%--  </sx:tabbedpanel> --%>
                                <script type="text/javascript" src="<s:url value="/includes/javascripts/reviews/ajaxfileupload.js"/>"></script>  

                                <!--//END TABBED PANNEL : -->
                                <script type="text/javascript">
                                    

                                    var countries=new ddtabcontent("accountTabs")
                                    countries.setpersist(true)
                                    countries.setselectedClassTarget("link") //"link" or "linkparent"
                                    countries.init()

                                </script>            
                            </td>
                        </tr>
                    </table>
                </td>
                <!--//END DATA COLUMN : Coloumn for Screen Content-->
            </tr>



            <!--//END DATA RECORD : Record for LeftMenu and Body Content-->

            <!--//START FOOTER : Record for Footer Background and Content-->
            <tr class="footerBg">
                <td align="center"><s:include value="/includes/template/Footer.jsp"/>   </td>
            </tr>
            <tr>
                <td>

                    <div style="display: none; position: absolute; top:170px;left:320px;overflow:auto;" id="menu-popup">
                        <table id="completeTable" border="1" bordercolor="#e5e4f2" style="border: 1px dashed gray;" cellpadding="0" class="cellBorder" cellspacing="0" ></table>
                    </div>

                </td>
            </tr>

            <!--//END FOOTER : Record for Footer Background and Content-->

        </table>
        <!--//END MAIN TABLE : Table for template Structure-->





    </body>
</html>


