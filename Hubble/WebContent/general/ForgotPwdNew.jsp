<%-- 
    Document   : ForgotPwd1
    Created on : Mar 30, 2017, 6:07:18 PM
    Author     : miracle
--%>

<%@ page contentType="text/html; charset=UTF-8" errorPage="../exception/ErrorDisplay.jsp"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%@ page import="com.mss.mirage.util.ApplicationConstants"%>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html>
    
    <%-- START HEAD SECTION --%>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<% response.addHeader("X-Frame-Options", "SAMEORIGIN"); %> 
<% response.addHeader("X-XSS-Protection", "1; mode=block"); %> 
        <link rel="stylesheet" type="text/css" href="<s:url value="/includes/css/mainStyle.css"/>">
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/AppConstants.js"/>"></script>
         
         <script type="text/JavaScript" src="<s:url value="/includes/javascripts/clientValidations/ResetPasswordClientValidation.js?version=4.6"/>"></script>
           
        <script type="text/JavaScript" src="<s:url value="/includes/javascripts/ClientValidations.js?version=1.5"/>"></script>
         <script type="text/JavaScript" src="<s:url value="/includes/javascripts/jquery-1.2.2.pack.js"/>"></script>
       
        <title>Hubble Organization Portal :: Forgot Password</title>
        
       <script type="text/JavaScript">
            
        function resetEmpPwdSubmit1()
        {
        	//document.getElementById("emailCode").value="";
        	
        	newPassword= document.getElementById('newPassword').value;
        	confirmPassword=  document.getElementById("confirmPassword").value;
        	 var str1 = removeSpaces(document.getElementById('txtCaptcha').value);
             var str2 = removeSpaces(document.getElementById('txtInput').value);
              var verificationCode= document.getElementById("emailCode").value; 
             
             if(newPassword == "" || confirmPassword ==""  ||  verificationCode =="" )
            	 {
            	 alert("Enter Mandatory Fields");
            	 return false;
            	 }
             
             else if(str2 == ""){
            	 alert("Please Enter the Captcha");
            	 return false;
             }
          
             else if(newPassword != confirmPassword)
        		{
        		
        		
        		alert("newPassword and Confirm Password didn't match");
        		return false;
        		}
        	else if(str1 != str2)
        		{
        		
        		 alert("Re-Enter the Captcha");
        		return false;
        		} 
        	
        	else
        		{
        		
        		return true;
        		//document.getElementById("emailCode").value="";
        		}
        	}
     
     function   ResetForm()
     {
    	
         document.getElementById("newPassword").value="";
         document.getElementById("confirmPassword").value="";
         document.getElementById("emailCode").value=""; 
         document.getElementById('txtInput').value="";
         document.getElementById('resultMessage').value="";
         document.getElementById('newpwdSpan').value="";
         
         
     }
    
    </script> 
  
    </head>
    <%-- END HEAD SECTION --%>
    
    <%-- START BODY SECTION --%>
    <body class="bodyGeneral" oncontextmenu="return false;">
        
        <%--//START MAIN TABLE : Table for template Structure--%>
        <table class="templateTable1000x580" align="center" cellpadding="0" cellspacing="0" border="0">
            
            <%--//START HEADER : Record for Header Background and Mirage Logo--%>
            <tr class="headerBg">
                <td valign="top">
                    
                    <% if(request.getParameter("eflag")!=null && request.getParameter("eflag").equalsIgnoreCase("e")){%>
                    <s:include value="/includes/template/Header.jsp"/>
                    <%}
                    else 
                     {%>
                     <s:include value="/includes/template/Header.jsp"/>
                    <%}%>
                </td>
            </tr>
            <%--//END HEADER : Record for Header Background and Mirage Logo--%>
         
            <%--//START DATA RECORD : Record for LeftMenu and Screen Content--%>
           
                        
                   
                       
             <td width="850px" class="cellBorder" valign="center" background="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/miragewatermark_850X600.gif">
                                <!--//START TABBED PANNEL : -->
                                <sx:tabbedpanel id="resetPasswordPannel" cssStyle="width: 850px; height: 510px;padding-left:5px;padding-right:5px;padding-bottom:8px;" doLayout="true" >
                                    
                                    <!--//START TAB : -->
                                    <sx:div id="resetPasswordTab" label="Reset Password">
                                        <s:form action="resetPasswordSubmitNew" name="resetForm" theme="simple" onsubmit="return resetEmpPwdSubmit1();">
                                            <div style="padding-top:100px;" id="Div2">
                                                <table border="0" cellpadding="1" cellspacing="1" align="center" width="500px" height="150px" class="cellBorder" background="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/miragewatermark_850X600.gif">
                                                    <tr>
                                                        <td valign="top" colspan="2">
                                                            <table width="500px" cellpadding="1" cellspacing="1" border="0" align="center">
                                                                <!--START ANIMATED TEXT ROW -->
                                                                <tr>
                                                                    <td colspan="2"><h3 class="bgColor" align="center"> <script type="text/JavaScript" src="<s:url value="/includes/javascripts/ResetPasswordAnimation.js"/>"></script>
                                                                            
                                                                    </h3>
                                                                    </td>
                                                                </tr>
                                                                <!--END ANIMATED TEXT ROW -->
                                                                
                                                                <tr>
                                                                    <td align="right"><img src="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/rightArrow_16x6.gif"></td>
                                                                    <td ><font size="2px" color="#1B425E">Enter Your New Password In First Box ! </font></td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="right"><img src="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/rightArrow_16x6.gif"></td>
                                                                    <td ><font size="2px" color="#1B425E">Enter Your New Password Once Again In Second Box </font></td>
                                                                </tr>
                                                                
                                                                <tr>
                                                                    <td align="right"><img src="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/rightArrow_16x6.gif"></td>
                                                                    <td ><font size="2px" color="#1B425E"> Spaces are not allowed at the beginning and end of passwords.
                                                                    </font></td>
                                                                </tr>
                                                                
                                                                  <tr>
                                                                    <td align="right"><img src="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/rightArrow_16x6.gif"></td>
                                                                    <td ><font size="2px" color="#1B425E">Password should contain at least 8 characters and must contain at least one Upper case,one digit ,One special character.</font></td>
                                                                </tr>
                                                                
                                                            </table>
                                                        <br> </td>
                                                    </tr>
                                                    <tr>
                                                    <td colspan="2" class="headerText" align="center">Enter following details</td>
                                                    </tr>
                                                    
                                                    
                                                    
                                                    
                                                    <tr>
                                                        <td class="fieldLabel" align="right">New Password<font color="red" style="bold">*</font> :</td>
                                                        <td><s:password  name="newPassword" id="newPassword" size="30" cssClass="inputTextBlue" onchange="validatePassword(document.resetForm.newPassword.value);"/><span id="newpwdSpan" style="display:none;"></span></td>
                                                    </tr>
                                                    
                                                    <tr>
                                                        <td class="fieldLabel" align="right">Confirm Password<font color="red" style="bold">*</font> :</td>
                                                        <td><s:password name="confirmPassword" id="confirmPassword" size="30" cssClass="inputTextBlue" onchange="confirmPasswordNew();"/><span id="confirmpwdSpan" style="display:none;"></span></td>
                                                    </tr>
                                                    
                                                    
                                                    
                                                   <tr>
                                                        <td class="fieldLabel" align="right">Verification Code<font color="red" style="bold">*</font> :</td>
                                                        <td><s:textfield name="emailCode" id="emailCode" size="30" cssClass="inputTextBlue" onchange="EmailcodeValidate(document.resetForm.emailCode.value);"/></td>
                                                    </tr>
                                                     
                                                    <tr>
                                                
                                                <td class="fieldLabel"> Re-Enter Captcha &nbsp;: </td> 
                                               
                                                <td class="">
           
                 <s:textfield readonly="true" id="txtCaptcha" cssClass="inputTextBlue" style="background-image:url(../includes/images/captcha.jpg); text-align:center;border: none;font-weight:bold; font-family:Modern"/> &nbsp &nbsp 
          
         <button type="button" style="background-image:url(../includes/images/refresh1.jpg);background-size: 30px;width:30px;height: 30px;border: none"  onclick="DrawCaptcha();" /> 
       </td>
       
      
   
                                                    </tr>
                                                    
                                                    
                                                     <tr>
   
    <td class="fieldLabel"></td> 
     <td class=""><s:textfield id="txtInput" name="txtInput" cssClass="inputTextBlue"/></td>
      <td>&nbsp;</td>
       </tr> 
                                             <input type="hidden" value="<%=request.getParameter("emailId") %>" name="emailIddd" id="emailIddd"/>                            
                                               <s:hidden name="emailId" id="emailId" value="%{#request.emailId}"/>
                                                    <tr>
                                                  
                                                    
                                                             <td colspan="2" align="center"> 
                                                           
                                                             <s:submit cssClass="buttonBg" value="Submit" />
                                                             
                                                              <input type="button" class="buttonBg" value="Clear" onclick="ResetForm();"/>
                                                              
                                                        </td>
                                                    
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2" align="center">
                                                            
                                                           <% if(request.getAttribute("resultMessage") != null){
                                                            out.println(request.getAttribute("resultMessage"));
                                                            /* out.println("<a href='/Hubble/general/login.action'>Please click here to login with your new password</a>"); */
                                                            
                                                            }%>
                                                            
                                                      
                                                        </td>
                                                        
                                                       
                                                    </tr>
                                                </table>
                                            </div>
                                        </s:form>
                                    </sx:div >
                                    <!--//END TAB : -->
                                    
                                </sx:tabbedpanel>
                                <!--//END TABBED PANNEL : -->
                              
                            </td>
                        
                            <tr class="footerBg">
                <td align="center"><s:include value="/includes/template/Footer.jsp"/>   </td>
            </tr>  
                                   
                                </table>
                           
                        
                  
                        
                        
      
            
      
        
         <script type="text/javascript">
		$(window).load(function(){
		DrawCaptcha();

		});
		</script>

    </body>
    <%-- END BODY SECTION --%>
    
   </html>

