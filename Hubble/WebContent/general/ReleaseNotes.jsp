<%-- 
    Document   : ReleaseNotes
    Created on : Sep 17, 2014, 9:40:19 AM
    Author     : miracle
--%>

<%@page import="java.util.List"%>
<%@page import="java.util.Map"%>
<%@ page contentType="text/html; charset=UTF-8" errorPage="../exception/ErrorDisplay.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page import="com.mss.mirage.util.ApplicationConstants"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%--Start The Page --%>
<html>

<%--Start The Head Section --%>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>Hubble Organization Portal :: Release Notes</title>
<style type="text/css">
   LABEL[title]:hover:after{
   color: white;
   background:blue;
   }
</style>

<script src="../includes/javascripts/SpryCollapsiblePanel.js" type="text/javascript"></script>
<script src="../includes/javascripts/HelpLeftTextAnimation.js" type="text/javascript"></script>
<script src="../includes/javascripts/HelpRightTextAnimation.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="<s:url value="/includes/css/mainStyle.css"/>">
<link href="../includes/css/SpryCollapsiblePanel.css" rel="stylesheet" type="text/css" />
<link href="../includes/css/style.css" rel="stylesheet" type="text/css" />
<script type="text/JavaScript" src="<s:url value="/includes/javascripts/AppConstants.js"/>"></script>
  <script type="text/JavaScript" src="<s:url value="/includes/javascripts/reviews/jquery.min.js"/>"></script>
    <script type="text/javascript" src="<s:url value="/includes/javascripts/reviews/jquery.js"/>"></script>  
     <script src="http://code.jquery.com/jquery-1.10.2.js"></script>
        <script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
        <script>
            $(function() {
                $( document ).tooltip();
            });
        </script>
        <s:include value="/includes/template/headerScript.html"/>
    



       
<style> 
LABEL:hover {
  color: white;
  position: relative;
}
LABEL[t]:hover:after {
content: attr(t);
  padding: 4px;
  color: white;
  position: absolute;
  left: 0;
  top: 100%;
  white-space: nowrap;
  z-index: 20px;
  -moz-border-radius: 5px;
  -webkit-border-radius: 5px;
  border-radius: 5px;
  -moz-box-shadow: 0px 0px 4px #222;
  -webkit-box-shadow: 0px 0px 4px #222;
  box-shadow: 0px 0px 4px #222;
  background:#3E93D4;
}


#myDIV {   
    -webkit-animation: mymove 5s infinite; /* Chrome, Safari, Opera */
    animation: mymove 2s infinite;
}

/* Chrome, Safari, Opera */
@-webkit-keyframes mymove {
    50% {font-weight: bold;}
}

/* Standard syntax */
@keyframes mymove {
    50% {font-weight: bold;}
}

/* #2017:before{height: 100px;float:left;border: 1px solid black;background: #5dade2;content:'';width: 100px;border-radius: 50%;} */
/* #2017:after{position:absolute;left:14px; top:-6px;height:114px;box-shadow: 1px 0px 0px 0px #000, 110px 0px 0px 68px #d5d8dc;background:none;z-index:-1;content:'';width: 100px;border-radius: 50%;} */
/* #2017{overflow:hidden;border-radius:20px;position:relative;display:inline-block;} */
</style>



</head>
<%--End The Head Section --%>

<%--Start The Body Section --%>
<!-- <body class="bodyGeneral" onLoad="initVars()" oncontextmenu="return false;"> -->
<body class="bodyGeneral" oncontextmenu="return false;">



<%--//START MAIN TABLE : Table for template Structure--%>

<table class="templateTableReleaseNotes" align="center" cellpadding="0" cellspacing="0" border="1">

<%--//START HEADER : Record for Header Background and Mirage Logo--%>
<tr class="headerBg">
    <td valign="top">
        <s:include value="/includes/template/Header.jsp"/>                    
    </td>
</tr>
<%--//END HEADER : Record for Header Background and Mirage Logo--%>

<%--//START DATA RECORD : Record for LeftMenu and Screen Content--%>
<tr>
<td>
    <table class="innerTableReleaseNotes" cellpadding="0" cellspacing="0" border="0" background="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/miragewatermark_850X600.gif">
    <tr>
     <td width="150px;" class="leftMenuBgColor" valign="top">
                            <s:include value="/includes/template/LeftMenu.jsp"/>
                        </td>
        <!--START LEFT MENU -->
        <td width="150px" valign="top" align="center">
            <br><br><br><br><br><br><br><br>
            <div id="hi" style="color: #7E2217">
                <!--   Mirage Help -->
            </div>
            <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
            <div id="welcome" style="color: #7E2217">
                <!--   Mirage Help -->
            </div> 
            <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
            <div id="message" style="color: #7E2217">
                <!--   Mirage Help -->
            </div>
            
            
        </td>   
        <!--END LEFt MENU -->
        
        <!--//START DATA COLUMN : Coloumn for Screen Content-->
        <td width="700px" class="" valign="top">
        <!--Start data table -->    
        <table border="0" width="700px" align="center">
            <tr>
            <td valign="top">
                <table width="1000px" align="center" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td valign="top">
                        <div>
                        <img src="/<%=ApplicationConstants.CONTEXT_PATH%>/includes/images/releaseNotesHeader_700x150.jpg" width="100%" height="150%" />
                        </div>
                    </td>
                </tr>
             
            <tr>
                                        <td>
                                        
                                        <ul class="tree" id="tree">
                                        
                                   
                                        
                                        </ul>
<!--                                             <div id="releaseNotesDetails" style="display: block"> -->
<!--                                                 style="color:#0000FF;font:italic 900 12px arial;"  bgcolor='#3E93D4' -->
<!--                                                 <table id="tblReleaseNotes" align='center' cellpadding='1' cellspacing='1' border='0' class="gridTable" width='700'> -->

<!--                                                 </table> <br> -->
<!--                                                 <center><span id="spnFast" class="activeFile" style="display: none;"></span></center> -->
<!--                                             </div> -->
                                        </td>
                                        

                                       
                                        </tr>        
                    
                    
              
</table></td>
</tr>
</table>   
<!--End data table -->
</td> 
<!--//END DATA COLUMN : Coloumn for Screen Content-->


</tr>
</table>
</td>

</tr>
<%--//END DATA RECORD : Record for LeftMenu and Screen Content--%>

<%--//START FOOTER : Record for Footer Background and Content--%>
<tr class="footerBg">
    <td align="center"><s:include value="/includes/template/Footer.jsp"/></td>
</tr>
<%--//END FOOTER : Record for Footer Background and Content--%>

</table> 
<%--//End MAIN TABLE : Table for template Structure--%>
    	
     <script> 
     $(document).ready(function(){
        
       getReleaseNodeDetails();
      
     });

 </script> 
       
<script type="text/javascript">
		$(window).load(function(){
		initVars();

		});
		</script>

     
        <s:include value="/includes/template/headerScript.html"/>
<script type="text/JavaScript" src="<s:url value="/includes/javascripts/ReleaseNotes.js"/>"></script> 
</body>
<%--End The Body Section --%>

</html>
<%--End The Page --%>